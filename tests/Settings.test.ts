/* eslint-disable @typescript-eslint/no-unused-expressions */
import chai from 'chai';
import spies from 'chai-spies';
import { GraphicSettingsConfigDispatchers } from '../src/contexts/GraphicSettings';
import { doesQualitySettingIncludes } from '../src/contexts/GraphicSettings/helpers';
import {
	defaultGraphicSettings,
	GraphicSettings_QualityLevels,
	GraphicSettings_TextureSizes,
} from '../src/contexts/GraphicSettings/slice';
import { SoundSettingsConfigDispatcher } from '../src/contexts/SoundSettings';
import { defaultSoundSettings } from '../src/contexts/SoundSettings/slice';
import { PersistentConfiguration } from '../src/stores/persistentConfiguration';

chai.use(spies);
const { expect, spy } = chai;

describe('Graphic settings', () => {
	describe('doesQualitySettingIncludes', () => {
		it('Max includes all values', () => {
			GraphicSettings_QualityLevels.forEach((qs) =>
				expect(doesQualitySettingIncludes(qs, 'Min')).to.be.equal(true),
			);
		});
		it('Med includes Med and Min', () => {
			expect(doesQualitySettingIncludes('Med', 'Min')).to.be.equal(true);
			expect(doesQualitySettingIncludes('Med', 'Med')).to.be.equal(true);
			expect(doesQualitySettingIncludes('Med', 'Max')).to.be.equal(false);
		});
		it('Min includes Min only', () => {
			expect(doesQualitySettingIncludes('Min', 'Min')).to.be.equal(true);
			expect(doesQualitySettingIncludes('Min', 'Med')).to.be.equal(false);
			expect(doesQualitySettingIncludes('Min', 'Max')).to.be.equal(false);
		});
		it('All values include null', () => {
			expect(doesQualitySettingIncludes('Max', null)).to.be.equal(true);
			expect(doesQualitySettingIncludes('Med', null)).to.be.equal(true);
			expect(doesQualitySettingIncludes('Min', null)).to.be.equal(true);
		});
		it('No args always truthy', () => {
			expect(doesQualitySettingIncludes(null, null)).to.be.equal(true);
			expect(doesQualitySettingIncludes()).to.be.equal(true);
		});
	});

	const dispatchers = GraphicSettingsConfigDispatchers(PersistentConfiguration.dispatch);
	describe('Texture Quality Reducer', () => {
		it('sets each value', () => {
			GraphicSettings_TextureSizes.forEach((textureSize) => {
				dispatchers.setTextureSize(textureSize);
				expect(PersistentConfiguration.getState().graphicSettings.textureSize).to.be.equal(textureSize);
			});
		});
		it('sets default value with null', () => {
			dispatchers.setTextureSize(null);
			expect(PersistentConfiguration.getState().graphicSettings.textureSize).to.be.equal(
				defaultGraphicSettings.textureSize,
			);
		});
	});
	describe('Render Quality Reducer', () => {
		it('sets each value', () => {
			GraphicSettings_QualityLevels.forEach((qualityLevel) => {
				dispatchers.setQuality(qualityLevel);
				expect(PersistentConfiguration.getState().graphicSettings.qualityLevel).to.be.equal(qualityLevel);
			});
		});
		it('sets default value with null', () => {
			dispatchers.setQuality(null);
			expect(PersistentConfiguration.getState().graphicSettings.qualityLevel).to.be.equal(
				defaultGraphicSettings.qualityLevel,
			);
		});
	});
	describe('Toggle Tooltips Reducer', () => {
		it('double negation', () => {
			const oldValue = PersistentConfiguration.getState().graphicSettings.showTooltips;
			dispatchers.toggleTooltips();
			dispatchers.toggleTooltips();
			expect(PersistentConfiguration.getState().graphicSettings.showTooltips).to.be.equal(oldValue);
		});
		it('toggle', () => {
			const oldValue = PersistentConfiguration.getState().graphicSettings.showTooltips;
			dispatchers.toggleTooltips();
			expect(PersistentConfiguration.getState().graphicSettings.showTooltips).to.be.equal(!oldValue);
		});
	});
	describe('Toggle Details Reducer', () => {
		it('double negation', () => {
			const oldValue = PersistentConfiguration.getState().graphicSettings.showDetails;
			dispatchers.toggleDetails();
			dispatchers.toggleDetails();
			expect(PersistentConfiguration.getState().graphicSettings.showDetails).to.be.equal(oldValue);
		});
		it('toggle', () => {
			const oldValue = PersistentConfiguration.getState().graphicSettings.showDetails;
			dispatchers.toggleDetails();
			expect(PersistentConfiguration.getState().graphicSettings.showDetails).to.be.equal(!oldValue);
		});
	});
});

describe('Sound Settings', () => {
	const testVolumes = [0.3, 0, 1, 0.7];
	const testGreaterVolumes = [1 + Number.EPSILON, 3, Infinity];
	const testLowerVolumes = [0 - Number.EPSILON, -2, -Infinity];

	const dispatchers = SoundSettingsConfigDispatcher(PersistentConfiguration.dispatch);
	describe('Toggle Mute Reducer', () => {
		it('double negation', () => {
			const oldValue = PersistentConfiguration.getState().soundSettings.muted;
			dispatchers.toggleMute();
			dispatchers.toggleMute();
			expect(PersistentConfiguration.getState().soundSettings.muted).to.be.equal(oldValue);
		});
		it('toggle', () => {
			const oldValue = PersistentConfiguration.getState().soundSettings.muted;
			dispatchers.toggleMute();
			expect(PersistentConfiguration.getState().soundSettings.muted).to.be.equal(!oldValue);
		});
	});
	describe('Master Volume Reducer', () => {
		it('sets value within range', () => {
			testVolumes.forEach((vol) => {
				dispatchers.setMasterVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.masterVolume).to.be.equal(vol);
			});
		});
		it('limits values greater than 1 to range', () => {
			testGreaterVolumes.forEach((vol) => {
				dispatchers.setMasterVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.masterVolume).to.be.equal(1);
			});
		});
		it('setting non-zero value disables Mute', () => {
			testVolumes
				.concat(testGreaterVolumes)
				.filter(Boolean)
				.forEach((vol) => {
					if (!PersistentConfiguration.getState().soundSettings.muted) dispatchers.toggleMute(); // force mute
					dispatchers.setMasterVolume(vol);
					expect(PersistentConfiguration.getState().soundSettings.muted).to.be.equal(false);
				});
		});
		it('setting zero value enables Mute', () => {
			testVolumes
				.concat(testLowerVolumes)
				.filter((v) => v <= 0)
				.forEach((vol) => {
					if (PersistentConfiguration.getState().soundSettings.muted) dispatchers.toggleMute(); // force unmute
					dispatchers.setMasterVolume(vol);
					expect(PersistentConfiguration.getState().soundSettings.muted).to.be.equal(true);
				});
		});
		it('limits values greater lesser than 0 to range', () => {
			testLowerVolumes.forEach((vol) => {
				dispatchers.setMasterVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.masterVolume).to.be.equal(0);
			});
		});
		it('default value', () => {
			dispatchers.setMasterVolume(null);
			expect(PersistentConfiguration.getState().soundSettings.masterVolume).to.be.equal(
				defaultSoundSettings.masterVolume,
			);
		});
	});

	describe('Music Volume Reducer', () => {
		it('sets value within range', () => {
			testVolumes.forEach((vol) => {
				dispatchers.setMusicVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.musicVolume).to.be.equal(vol);
			});
		});
		it('limits values greater than 1 to range', () => {
			testGreaterVolumes.forEach((vol) => {
				dispatchers.setMusicVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.musicVolume).to.be.equal(1);
			});
		});
		it('limits values greater lesser than 0 to range', () => {
			testLowerVolumes.forEach((vol) => {
				dispatchers.setMusicVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.musicVolume).to.be.equal(0);
			});
		});
		it('default value', () => {
			dispatchers.setMusicVolume(null);
			expect(PersistentConfiguration.getState().soundSettings.musicVolume).to.be.equal(
				defaultSoundSettings.musicVolume,
			);
		});
	});

	describe('SFX Volume Reducer', () => {
		it('sets value within range', () => {
			testVolumes.forEach((vol) => {
				dispatchers.setSfxVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.sfxVolume).to.be.equal(vol);
			});
		});
		it('limits values greater than 1 to range', () => {
			testGreaterVolumes.forEach((vol) => {
				dispatchers.setSfxVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.sfxVolume).to.be.equal(1);
			});
		});
		it('limits values greater lesser than 0 to range', () => {
			testLowerVolumes.forEach((vol) => {
				dispatchers.setSfxVolume(vol);
				expect(PersistentConfiguration.getState().soundSettings.sfxVolume).to.be.equal(0);
			});
		});
		it('default value', () => {
			dispatchers.setSfxVolume(null);
			expect(PersistentConfiguration.getState().soundSettings.sfxVolume).to.be.equal(
				defaultSoundSettings.sfxVolume,
			);
		});
	});
});

import BasicMaths from '../../definitions/maths/BasicMaths';

interface IApplyModifierTestCase {
	args: Parameters<typeof BasicMaths.applyModifier>;
	output: number;
	title?: string;
}

export const ApplyModifierTestCases: IApplyModifierTestCase[] = [
	{
		title: 'Null Object',
		args: [],
		output: 0,
	},
	{
		args: [5],
		output: 5,
	},
	{
		args: [
			10,
			{
				factor: 1.5,
				bias: 5,
			},
		],
		output: 20,
	},
	{
		args: [
			10,
			{
				bias: -5,
			},
		],
		output: 5,
	},
	{
		args: [
			10,
			{
				factor: 2,
				bias: -2,
				max: 16,
			},
		],
		output: 16,
	},
	{
		args: [
			100,
			{
				factor: -0.2,
				bias: -5,
				min: -21,
				maxGuard: 120,
			},
		],
		output: -21,
	},
	{
		args: [
			100,
			{
				factor: -0.2,
				bias: -5,
				min: -21,
				maxGuard: 90,
			},
		],
		output: 100,
	},
	{
		args: [
			10,
			{
				factor: 1.2,
				bias: -15,
				min: -5,
			},
		],
		output: -3,
	},
	{
		args: [
			10,
			{
				factor: 1.2,
				bias: -15,
				max: 7,
			},
			{
				factor: 0.8,
				bias: 15,
				min: 3,
			},
		],
		output: 7,
	},
	{
		args: [
			10,
			{
				factor: 0.6,
				bias: 2,
				max: 7,
			},
			{
				factor: 0.4,
				min: 3,
			},
		],
		output: 3,
	},
	{
		args: [
			10,
			{
				factor: 1.2,
				bias: 3,
			},
			{
				factor: 1.3,
				bias: 2,
			},
		],
		output: 20,
	},
	{
		args: [
			10,
			{
				factor: 0.7,
				bias: 3,
			},
			{
				factor: 1.3,
				bias: 2,
			},
		],
		output: 15,
	},
	{
		args: [
			10,
			{
				factor: 0.7,
				bias: 3,
				max: 15,
			},
			{
				factor: 1.5,
				min: 8,
			},
		],
		output: 15,
	},
	{
		args: [
			10,
			{
				factor: 0.7,
				bias: 3,
				maxGuard: 8,
			},
			{
				factor: 1.3,
				bias: 2,
				minGuard: 0,
			},
		],
		output: 15,
	},
	{
		args: [
			10,
			{
				factor: 0.7,
				bias: 3,
				maxGuard: 8,
			},
			{
				factor: 0.5,
				minGuard: 15,
			},
		],
		output: 10,
	},
	{
		title: 'Max has high priority than Min',
		args: [
			25,
			{
				max: 20,
			},
			{ min: 30 },
		],
		output: 20,
	},
];

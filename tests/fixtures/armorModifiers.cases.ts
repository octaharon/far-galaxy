import Damage from '../../definitions/tactical/damage/damage';
import UnitDefense from '../../definitions/tactical/damage/defense';
import DefenseManager from '../../definitions/tactical/damage/DefenseManager';
import { TSerializedArmor } from '../../definitions/technologies/types/armor';

export const armorModifiersCases: (
	instance: TSerializedArmor,
) => Array<[string, UnitDefense.TArmorModifier | UnitDefense.TArmorModifier[], TSerializedArmor]> = (
	SampleInstanceSnapshot,
) => [
	['null modifier', {}, SampleInstanceSnapshot],
	[
		'baseCost modifier',
		{
			baseCost: { bias: 5, factor: 1.2 },
		},
		Object.assign({}, SampleInstanceSnapshot, {
			baseCost: SampleInstanceSnapshot.baseCost * 1.2 + 5,
		}),
	],
	[
		'single protection prop',
		{
			[Damage.damageType.magnetic]: {
				min: 5,
			},
		},
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				[Damage.damageType.magnetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.magnetic,
					),
					{ min: 5 },
				),
			}),
		}),
	],
	[
		'multiple protection props',
		{
			[Damage.damageType.magnetic]: {
				bias: -5,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.8,
			},
		},
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				[Damage.damageType.magnetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.magnetic,
					),
					{ bias: -5 },
				),
				[Damage.damageType.antimatter]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.antimatter,
					),
					{ factor: 0.8 },
				),
			}),
		}),
	],
	[
		'default prop + one specific',
		{
			default: {
				bias: -10,
				factor: 0.5,
			},
			[Damage.damageType.kinetic]: {
				max: 20,
			},
		},
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, dt),
								{
									bias: -10,
									factor: 0.5,
								},
							),
						}),
					{},
				),
				[Damage.damageType.kinetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, Damage.damageType.kinetic),
					{ max: 20 },
				),
			}),
		}),
	],
	[
		'default modifier and one specific, separately',
		[
			{
				default: {
					bias: -10,
					threshold: 5,
					factor: 0.5,
				},
			},
			{
				[Damage.damageType.magnetic]: {
					factor: 0.5,
					bias: -15,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, dt),
								{
									threshold: 5,
									bias: -10,
									factor: 0.5,
								},
							),
						}),
					{},
				),
				[Damage.damageType.magnetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.magnetic,
					),
					{
						factor: 0.5,
						bias: -15,
					},
					{
						bias: -10,
						threshold: 5,
						factor: 0.5,
					},
				),
			}),
		}),
	],
	[
		'default prop only',
		{
			default: {
				bias: -10,
				threshold: 5,
				max: 10,
			},
		},
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, dt),
								{
									bias: -10,
									threshold: 5,
									max: 10,
								},
							),
						}),
					{},
				),
			}),
		}),
	],
	[
		'two modifiers with default props only',
		[
			{
				default: {
					bias: -10,
					threshold: 5,
					max: 10,
				},
			},
			{
				default: {
					threshold: 15,
					factor: 0.8,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, dt),
								{
									bias: -10,
									threshold: 15,
									max: 10,
									factor: 0.8,
								},
							),
						}),
					{},
				),
			}),
		}),
	],
	[
		'two modifiers, cross overlapping props #1',
		[
			{
				default: {
					bias: -10,
					threshold: 5,
					max: 10,
				},
				[Damage.damageType.heat]: {
					factor: 0.4,
				},
			},
			{
				default: {
					threshold: 15,
					factor: 0.8,
				},
				[Damage.damageType.magnetic]: {
					factor: 0.4,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, dt),
								{
									bias: -10,
									threshold: 15,
									max: 10,
									factor: 0.8,
								},
							),
						}),
					{},
				),
				[Damage.damageType.heat]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, Damage.damageType.heat),
					{
						factor: 0.4,
					},
					{
						threshold: 15,
						factor: 0.8,
					},
				),
				[Damage.damageType.magnetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.magnetic,
					),
					{
						factor: 0.4,
					},
					{
						bias: -10,
						threshold: 5,
						max: 10,
					},
				),
			}),
		}),
	],
	[
		'two modifiers, cross overlapping props #2',
		[
			{
				default: {
					threshold: 5,
					factor: 1.1,
				},
				[Damage.damageType.biological]: {
					bias: -15,
					threshold: 5,
				},
				[Damage.damageType.warp]: {
					factor: 0.5,
				},
			},
			{
				default: {
					factor: 0.25,
				},
				[Damage.damageType.thermodynamic]: {
					threshold: 25,
					bias: 0,
				},
				[Damage.damageType.biological]: {
					threshold: 25,
					bias: -5,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, dt),
								{
									threshold: 5,
									factor: 1.1,
								},
								{
									factor: 0.25,
								},
							),
						}),
					{},
				),
				[Damage.damageType.biological]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.biological,
					),
					{
						bias: -20,
						threshold: 25,
					},
				),
				[Damage.damageType.thermodynamic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.thermodynamic,
					),
					{
						threshold: 25,
						factor: 1.1,
					},
				),
				[Damage.damageType.warp]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, Damage.damageType.warp),
					{
						factor: 0.125,
					},
				),
			}),
		}),
	],
	[
		'three modifiers, mixed case',
		[
			{
				default: {
					factor: 2,
				},
			},
			{
				default: {
					factor: 0.5,
					bias: -5,
				},
				[Damage.damageType.antimatter]: {
					threshold: 10,
					factor: 1,
				},
				[Damage.damageType.biological]: {
					min: -5,
					max: 10,
				},
			},
			{
				default: {
					max: 50,
					factor: 1.2,
					threshold: 2,
				},
				[Damage.damageType.biological]: {
					max: 20,
					factor: 0.5,
				},
				[Damage.damageType.heat]: {
					bias: -10,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			armor: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.armor,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, dt),
								{
									factor: 1.2,
									threshold: 2,
									bias: -5,
									max: 50,
								},
							),
						}),
					{},
				),
				[Damage.damageType.biological]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.biological,
					),
					{
						max: 10,
						min: -5,
					},
				),
				[Damage.damageType.heat]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.armor, Damage.damageType.heat),
					{
						bias: -15,
					},
				),
				[Damage.damageType.antimatter]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.armor,
						Damage.damageType.antimatter,
					),
					{
						threshold: 10,
						factor: 2.4,
						max: 50,
					},
				),
			}),
		}),
	],
];

export const armorModifiersStackCases: Array<
	[string, UnitDefense.TArmorModifier | UnitDefense.TArmorModifier[], UnitDefense.TArmorModifier]
> = [
	['returns empty object when called with no parameters', undefined, {}],
	['returns empty object when called with empty modifier', {}, {}],
	[
		'expands single modifier with min=0',
		{
			baseCost: {
				factor: 0.8,
			},
			default: {
				bias: -5,
			},
		},
		{
			baseCost: {
				factor: 0.8,
			},
			default: {
				bias: -5,
				min: 0,
			},
		},
	],
	[
		'extracts single modifier from array',
		[
			{
				baseCost: {
					factor: 0.8,
				},
				default: {
					bias: -5,
				},
			},
		],
		{
			baseCost: {
				factor: 0.8,
			},
			default: {
				bias: -5,
				min: 0,
			},
		},
	],
	[
		'stacks baseCost',
		[
			{
				baseCost: {
					factor: 0.8,
					min: 0,
					maxGuard: 10,
				},
			},
			{
				baseCost: {
					factor: 0.7,
					bias: -15,
					min: 5,
				},
			},
		],
		{
			baseCost: {
				factor: 0.5,
				bias: -15,
				min: 5,
				maxGuard: 10,
			},
		},
	],
	[
		'combines protection keys',
		[
			{
				[Damage.damageType.biological]: {
					max: 15,
					factor: 0.4,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -10,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					bias: -10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.5,
				bias: -10,
			},
			[Damage.damageType.biological]: {
				max: 15,
				factor: 0.4,
				min: 0,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.8,
				bias: -10,
				min: 0,
			},
		},
	],
	[
		'stacks protection keys',
		[
			{
				[Damage.damageType.biological]: {
					max: 15,
					factor: 0.4,
				},
				[Damage.damageType.antimatter]: {
					max: 15,
					factor: 0.9,
					threshold: 10,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -10,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.5,
				bias: -10,
			},
			[Damage.damageType.biological]: {
				max: 15,
				factor: 0.4,
				min: 0,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.72,
				bias: -10,
				max: 15,
				min: 0,
				threshold: 10,
			},
		},
	],
	[
		'applies default protection to all other protections',
		[
			{
				[Damage.damageType.biological]: {
					max: 15,
					factor: 0.5,
				},
				[Damage.damageType.antimatter]: {
					max: 25,
					factor: 0.9,
					threshold: 10,
				},
				baseCost: {
					factor: 0.9,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -10,
				},
				default: {
					factor: 0.75,
					bias: -2,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.4,
				bias: -10,
			},
			default: {
				factor: 0.75,
				bias: -2,
				min: 0,
			},
			[Damage.damageType.biological]: {
				max: 15,
				factor: 0.375,
				bias: -2,
				min: 0,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.72,
				bias: -10,
				max: 25,
				min: 0,
				threshold: 10,
			},
		},
	],
	[
		'applies default protection to all other protections in a cartesian mode',
		[
			{
				[Damage.damageType.biological]: {
					max: 50,
					factor: 0.6,
					min: -10,
				},
				[Damage.damageType.antimatter]: {
					max: 10,
					factor: 0.9,
					threshold: 10,
				},
				default: {
					factor: 0.5,
					bias: -5,
				},
				baseCost: {
					factor: 0.9,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -5,
					min: 1,
				},
				default: {
					factor: 0.9,
					min: 0,
					bias: -2,
					max: 5,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
				[Damage.damageType.kinetic]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.4,
				bias: -5,
				min: 1,
			},
			default: {
				factor: 0.45,
				bias: -7,
				max: 5,
				min: 0,
			},
			[Damage.damageType.biological]: {
				max: 5,
				factor: 0.54,
				bias: -2,
				min: -10,
			},
			[Damage.damageType.kinetic]: {
				factor: 0.4,
				threshold: 5,
				bias: -15,
				min: 0,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.72,
				bias: -10,
				max: 10,
				min: 0,
				threshold: 10,
			},
		},
	],
	[
		'complex case #1',
		[
			{
				[Damage.damageType.biological]: {
					max: 50,
					factor: 0.6,
					min: -10,
				},
				default: {
					factor: 0.6,
					bias: -5,
				},
			},
			{},
			{
				baseCost: {
					factor: 0.5,
					bias: -5,
					min: 1,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
			},
			{
				[Damage.damageType.magnetic]: {
					factor: 0.9,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.9,
				},
			},
			{
				baseCost: {
					factor: 0.8,
					bias: -5,
				},
			},
		],
		{
			baseCost: {
				factor: 0.3,
				bias: -10,
				min: 1,
			},
			default: {
				factor: 0.6,
				bias: -5,
				min: 0,
			},
			[Damage.damageType.biological]: {
				max: 50,
				factor: 0.6,
				min: -10,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.432,
				threshold: 5,
				bias: -15,
				min: 0,
			},
			[Damage.damageType.magnetic]: {
				factor: 0.54,
				bias: -5,
				min: 0,
			},
		},
	],
	[
		'complex case #2',
		[
			{},
			{
				default: {
					factor: 0.8,
					bias: -5,
				},
				[Damage.damageType.magnetic]: {
					factor: 0.9,
					min: -20,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -5,
				},
				default: {
					factor: 0.8,
					bias: -5,
					threshold: 1,
				},
			},
			{
				[Damage.damageType.magnetic]: {
					factor: 0.9,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.9,
				},
			},
			{
				[Damage.damageType.heat]: {
					factor: 0.5,
					max: 35,
				},
				baseCost: {
					max: 100,
				},
			},
		],
		{
			baseCost: {
				factor: 0.5,
				bias: -5,
				max: 100,
			},
			default: {
				factor: 0.64,
				bias: -10,
				min: 0,
				threshold: 1,
			},
			[Damage.damageType.magnetic]: {
				factor: 0.648,
				min: -20,
				bias: -5,
				threshold: 1,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.576,
				bias: -10,
				threshold: 1,
				min: 0,
			},
			[Damage.damageType.heat]: {
				factor: 0.32,
				threshold: 1,
				bias: -10,
				max: 35,
				min: 0,
			},
		},
	],
];

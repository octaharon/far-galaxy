import UnitAttack from '../../definitions/tactical/damage/attack';
import UnitDefense from '../../definitions/tactical/damage/defense';
import DefenseManager from '../../definitions/tactical/damage/DefenseManager';

export const stackDodgeCases: Array<{
	title: string;
	from: Parameters<typeof DefenseManager.stackDodge>;
	to: UnitDefense.TDodge;
}> = [
	{
		title: 'No args',
		from: [undefined],
		to: {},
	},
	{
		title: 'No args (null)',
		from: [null],
		to: {},
	},
	{
		title: 'Empty dodge',
		from: [{}],
		to: {},
	},
	{
		title: 'Returns optimized Dodge with no modifiers',
		from: [
			{
				default: 0.4,
				[UnitAttack.deliveryType.beam]: 0,
				[UnitAttack.deliveryType.cloud]: 0.4,
			},
		],
		to: {
			default: 0.4,
		},
	},
	{
		title: 'Returns optimized Dodge with no modifiers #2',
		from: [
			{
				[UnitAttack.deliveryType.beam]: 0.8,
				[UnitAttack.deliveryType.cloud]: 0.4,
				[UnitAttack.deliveryType.aerial]: 0.4,
				[UnitAttack.deliveryType.ballistic]: 0.4,
			},
		],
		to: {
			[UnitAttack.deliveryType.beam]: 0.8,
			[UnitAttack.deliveryType.cloud]: 0.4,
			[UnitAttack.deliveryType.aerial]: 0.4,
			[UnitAttack.deliveryType.ballistic]: 0.4,
		},
	},
	{
		title: 'Stacks `default` modifier with every base value',
		from: [
			{
				[UnitAttack.deliveryType.beam]: 0.8,
				[UnitAttack.deliveryType.cloud]: 0.4,
				[UnitAttack.deliveryType.aerial]: 0.4,
				[UnitAttack.deliveryType.ballistic]: 0.4,
			},
			{
				default: {
					bias: 0.2,
					factor: 1.5,
				},
			},
		],
		to: {
			default: 0.2,
			[UnitAttack.deliveryType.beam]: 1.4,
			[UnitAttack.deliveryType.cloud]: 0.8,
			[UnitAttack.deliveryType.aerial]: 0.8,
			[UnitAttack.deliveryType.ballistic]: 0.8,
		},
	},
	{
		title: 'Stacks `default` modifier with every base value and `default` base value',
		from: [
			{
				default: 0.5,
				[UnitAttack.deliveryType.beam]: 0.8,
				[UnitAttack.deliveryType.drone]: 1,
				[UnitAttack.deliveryType.bombardment]: 0.5,
			},
			{
				default: {
					factor: 2,
				},
			},
		],
		to: {
			default: 1,
			[UnitAttack.deliveryType.beam]: 1.6,
			[UnitAttack.deliveryType.drone]: 2,
		},
	},
	{
		title: 'Complex case #1',
		from: [
			{
				[UnitAttack.deliveryType.beam]: 0.5,
				[UnitAttack.deliveryType.ballistic]: 1,
			},
			{
				default: {
					bias: 0.2,
				},
			},
			{
				default: { bias: 0.1 },
				[UnitAttack.deliveryType.missile]: { bias: 1.5 },
				[UnitAttack.deliveryType.beam]: { factor: 2 },
			},
		],
		to: {
			default: 0.3,
			[UnitAttack.deliveryType.beam]: 1.2,
			[UnitAttack.deliveryType.missile]: 1.7,
			[UnitAttack.deliveryType.ballistic]: 1.3,
		},
	},
	{
		title: 'Complex case #2',
		from: [
			{
				default: -0.5,
				[UnitAttack.deliveryType.beam]: 0.5,
				[UnitAttack.deliveryType.ballistic]: 1,
			},
			{
				default: {
					bias: 0.5,
				},
				[UnitAttack.deliveryType.drone]: { bias: 2 },
			},
			{
				[UnitAttack.deliveryType.drone]: { bias: 0, min: -Infinity },
			},
			{
				[UnitAttack.deliveryType.missile]: { bias: 1 },
				[UnitAttack.deliveryType.beam]: { bias: 0.5 },
			},
		],
		to: {
			[UnitAttack.deliveryType.beam]: 1.5,
			[UnitAttack.deliveryType.missile]: 1,
			[UnitAttack.deliveryType.ballistic]: 1.5,
			[UnitAttack.deliveryType.drone]: 1.5,
		},
	},
	{
		title: 'Complex case #3',
		from: [
			{
				[UnitAttack.deliveryType.beam]: 0.5,
				[UnitAttack.deliveryType.ballistic]: 1,
				[UnitAttack.deliveryType.drone]: 3,
			},
			{
				default: {
					factor: 1.2,
					bias: 0.1,
				},
			},
			{
				[UnitAttack.deliveryType.ballistic]: {
					bias: 0.4,
				},
				[UnitAttack.deliveryType.missile]: {
					bias: 0.4,
				},
			},
			{
				default: {
					bias: 0.4,
				},
				[UnitAttack.deliveryType.beam]: { bias: 0.5 },
			},
		],
		to: {
			default: 0.5,
			[UnitAttack.deliveryType.beam]: 1.2,
			[UnitAttack.deliveryType.missile]: 0.9,
			[UnitAttack.deliveryType.ballistic]: 2.1,
			[UnitAttack.deliveryType.drone]: 4.1,
		},
	},
];

export const optimizeDodgeCases: Array<{
	title: string;
	from: UnitDefense.TDodge;
	to: UnitDefense.TDodge;
}> = [
	{
		title: 'Empty object',
		from: {},
		to: {},
	},
	{
		title: 'Default only, optimal as it is',
		from: {
			default: 0.4,
		},
		to: {
			default: 0.4,
		},
	},
	{
		title: 'Strips zero values',
		from: {
			...UnitAttack.TDeliveryTypeArray.reduce(
				(obj, dt) => ({
					...obj,
					[dt]: 0,
				}),
				{},
			),
		},
		to: {},
	},
	{
		title: 'No default value, optimal as it is',
		from: {
			[UnitAttack.deliveryType.missile]: 0.3,
			[UnitAttack.deliveryType.aerial]: 0.5,
		},
		to: {
			[UnitAttack.deliveryType.missile]: 0.3,
			[UnitAttack.deliveryType.aerial]: 0.5,
		},
	},
	{
		title: 'Extra decorator identical with default',
		from: {
			default: 0.4,
			[UnitAttack.deliveryType.drone]: 0.4,
			[UnitAttack.deliveryType.bombardment]: 0.5,
		},
		to: {
			default: 0.4,
			[UnitAttack.deliveryType.bombardment]: 0.5,
		},
	},
	{
		title: 'Two extra decorators identical with default',
		from: {
			default: 0.2,
			[UnitAttack.deliveryType.ballistic]: -0.4,
			[UnitAttack.deliveryType.beam]: 0.2,
			[UnitAttack.deliveryType.supersonic]: 0.2,
			[UnitAttack.deliveryType.bombardment]: 0.7,
		},
		to: {
			default: 0.2,
			[UnitAttack.deliveryType.ballistic]: -0.4,
			[UnitAttack.deliveryType.bombardment]: 0.7,
		},
	},
	{
		title: 'Collapses full protection collection with all identical modifiers to default',
		from: {
			...UnitAttack.TDeliveryTypeArray.reduce(
				(obj, dt) => ({
					...obj,
					[dt]: -0.5,
				}),
				{},
			),
		},
		to: {
			default: -0.5,
		},
	},
	{
		title: 'Collapses full protection collection with few identical modifiers to default, preserving outstanding values',
		from: {
			...UnitAttack.TDeliveryTypeArray.reduce(
				(obj, dt) => ({
					...obj,
					[dt]: -0.5,
				}),
				{},
			),
			[UnitAttack.deliveryType.cloud]: 0.8,
			[UnitAttack.deliveryType.supersonic]: 0.2,
		},
		to: {
			[UnitAttack.deliveryType.cloud]: 0.8,
			[UnitAttack.deliveryType.supersonic]: 0.2,
			default: -0.5,
		},
	},
	{
		title: 'Collapses to default only',
		from: {
			default: 0.55,
			[UnitAttack.deliveryType.aerial]: 0.55,
			[UnitAttack.deliveryType.drone]: 0.55,
		},
		to: {
			default: 0.55,
		},
	},
	{
		title: `Doesn't collapse zero values`,
		from: {
			default: 0.15,
			[UnitAttack.deliveryType.surface]: 0,
			[UnitAttack.deliveryType.supersonic]: 0.15,
		},
		to: {
			default: 0.15,
			[UnitAttack.deliveryType.surface]: 0,
		},
	},
	{
		title: `Mixed case`,
		from: {
			...UnitAttack.TDeliveryTypeArray.reduce(
				(obj, dt) => ({
					...obj,
					[dt]: 1.2,
				}),
				{},
			),
			[UnitAttack.deliveryType.supersonic]: 0,
			[UnitAttack.deliveryType.ballistic]: 0.4,
		},
		to: {
			default: 1.2,
			[UnitAttack.deliveryType.supersonic]: 0,
			[UnitAttack.deliveryType.ballistic]: 0.4,
		},
	},
];

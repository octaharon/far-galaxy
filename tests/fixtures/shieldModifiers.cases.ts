import BasicMaths from '../../definitions/maths/BasicMaths';
import Damage from '../../definitions/tactical/damage/damage';
import UnitDefense from '../../definitions/tactical/damage/defense';
import DefenseManager from '../../definitions/tactical/damage/DefenseManager';
import { TSerializedShield } from '../../definitions/technologies/types/shield';

export const shieldModifiersCases: (
	instance: TSerializedShield,
) => Array<[string, UnitDefense.TShieldsModifier | UnitDefense.TShieldsModifier[], TSerializedShield]> = (
	SampleInstanceSnapshot,
) => [
	['null modifier', {}, SampleInstanceSnapshot],
	[
		'baseCost modifier',
		{
			baseCost: { bias: 5, factor: 1.2 },
		},
		Object.assign({}, SampleInstanceSnapshot, {
			baseCost: SampleInstanceSnapshot.baseCost * 1.2 + 5,
		}),
	],
	[
		'overchargeDecay modifier',
		{
			overchargeDecay: { min: 50, max: 50 },
		},
		Object.assign({}, SampleInstanceSnapshot, {
			shield: {
				...SampleInstanceSnapshot.shield,
				overchargeDecay: 50,
			},
		}),
	],
	[
		'shieldAmount modifier',
		{
			shieldAmount: { bias: 25, factor: 1.5, minGuard: 50 },
		},
		Object.assign({}, SampleInstanceSnapshot, {
			shield: {
				...SampleInstanceSnapshot.shield,
				shieldAmount: BasicMaths.applyModifier(SampleInstanceSnapshot.shield.shieldAmount, {
					bias: 25,
					factor: 1.5,
					minGuard: 50,
				}),
			},
		} as TSerializedShield),
	],
	[
		'single protection prop',
		{
			[Damage.damageType.magnetic]: {
				min: 5,
			},
		},
		Object.assign({}, SampleInstanceSnapshot, {
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				[Damage.damageType.magnetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.magnetic,
					),
					{ min: 5 },
				),
			}),
		}),
	],
	[
		'multiple protection props',
		{
			[Damage.damageType.magnetic]: {
				bias: -5,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.8,
			},
		},
		Object.assign({}, SampleInstanceSnapshot, {
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				[Damage.damageType.magnetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.magnetic,
					),
					{ bias: -5 },
				),
				[Damage.damageType.antimatter]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.antimatter,
					),
					{ factor: 0.8 },
				),
			}),
		}),
	],
	[
		'default prop + one specific',
		{
			default: {
				bias: -10,
				factor: 0.5,
			},
			[Damage.damageType.kinetic]: {
				max: 20,
			},
		},
		Object.assign({}, SampleInstanceSnapshot, {
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, dt),
								{
									bias: -10,
									factor: 0.5,
								},
							),
						}),
					{},
				),
				[Damage.damageType.kinetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.kinetic,
					),
					{ max: 20 },
				),
			}),
		}),
	],
	[
		'default modifier and one specific, separately',
		[
			{
				default: {
					bias: -10,
					threshold: 5,
					factor: 0.5,
				},
			},
			{
				[Damage.damageType.magnetic]: {
					factor: 0.5,
					bias: -15,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, dt),
								{
									threshold: 5,
									bias: -10,
									factor: 0.5,
								},
							),
						}),
					{},
				),
				[Damage.damageType.magnetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.magnetic,
					),
					{
						factor: 0.5,
						bias: -15,
					},
					{
						bias: -10,
						threshold: 5,
						factor: 0.5,
					},
				),
			}),
		}),
	],
	[
		'default prop only',
		{
			default: {
				bias: -10,
				threshold: 5,
				max: 10,
			},
		},
		Object.assign({}, SampleInstanceSnapshot, {
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, dt),
								{
									bias: -10,
									threshold: 5,
									max: 10,
								},
							),
						}),
					{},
				),
			}),
		}),
	],
	[
		'two modifiers with default props only',
		[
			{
				shieldAmount: {
					factor: 0.5,
				},
				default: {
					bias: -10,
					threshold: 5,
					max: 10,
				},
			},
			{
				baseCost: {
					bias: -2,
				},
				default: {
					threshold: 15,
					factor: 0.8,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			baseCost: SampleInstanceSnapshot.baseCost - 2,
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				shieldAmount: SampleInstanceSnapshot.shield.shieldAmount * 0.5,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, dt),
								{
									bias: -10,
									threshold: 15,
									max: 10,
									factor: 0.8,
								},
							),
						}),
					{},
				),
			}),
		}),
	],
	[
		'two modifiers, cross overlapping props #1',
		[
			{
				shieldAmount: {
					factor: 0.5,
				},
				baseCost: {
					factor: 0.7,
					bias: 5,
				},
				default: {
					bias: -10,
					threshold: 5,
					max: 10,
				},
				[Damage.damageType.heat]: {
					factor: 0.4,
				},
			},
			{
				baseCost: {
					factor: 0.8,
				},
				shieldAmount: {
					factor: 2.5,
				},
				default: {
					threshold: 15,
					factor: 0.8,
				},
				[Damage.damageType.magnetic]: {
					factor: 0.4,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			baseCost: SampleInstanceSnapshot.baseCost * 0.5 + 5,
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				shieldAmount: SampleInstanceSnapshot.shield.shieldAmount * 2,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, dt),
								{
									bias: -10,
									threshold: 15,
									max: 10,
									factor: 0.8,
								},
							),
						}),
					{},
				),
				[Damage.damageType.heat]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, Damage.damageType.heat),
					{
						factor: 0.4,
					},
					{
						threshold: 15,
						factor: 0.8,
					},
				),
				[Damage.damageType.magnetic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.magnetic,
					),
					{
						factor: 0.4,
					},
					{
						bias: -10,
						threshold: 5,
						max: 10,
					},
				),
			}),
		}),
	],
	[
		'two modifiers, cross overlapping props #2',
		[
			{
				default: {
					threshold: 5,
					factor: 1.1,
				},
				overchargeDecay: {
					bias: -2,
					min: 5,
					minGuard: 5,
				},
				[Damage.damageType.biological]: {
					bias: -15,
					threshold: 5,
				},
				baseCost: {
					factor: 0.8,
				},
				[Damage.damageType.warp]: {
					factor: 0.5,
				},
			},
			{
				default: {
					factor: 0.25,
				},
				baseCost: {
					factor: 0.7,
				},
				shieldAmount: {
					bias: 30,
				},
				[Damage.damageType.thermodynamic]: {
					threshold: 25,
					bias: 0,
				},
				[Damage.damageType.biological]: {
					threshold: 25,
					bias: -5,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			baseCost: SampleInstanceSnapshot.baseCost / 2,
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				overchargeDecay: BasicMaths.applyModifier(SampleInstanceSnapshot.shield.overchargeDecay, {
					bias: -2,
					min: 5,
					minGuard: 5,
				}),
				shieldAmount: SampleInstanceSnapshot.shield.shieldAmount + 30,
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, dt),
								{
									threshold: 5,
									factor: 1.1,
								},
								{
									factor: 0.25,
								},
							),
						}),
					{},
				),
				[Damage.damageType.biological]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.biological,
					),
					{
						bias: -20,
						threshold: 25,
					},
				),
				[Damage.damageType.thermodynamic]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.thermodynamic,
					),
					{
						threshold: 25,
						factor: 1.1,
					},
				),
				[Damage.damageType.warp]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, Damage.damageType.warp),
					{
						factor: 0.125,
					},
				),
			}),
		}),
	],
	[
		'three modifiers, mixed case',
		[
			{
				default: {
					factor: 2,
				},
				shieldAmount: {
					bias: 30,
					max: 120,
				},
				baseCost: {
					bias: -5,
				},
			},
			{
				default: {
					factor: 0.5,
					bias: -5,
				},
				shieldAmount: {
					bias: 20,
					factor: 2,
					maxGuard: 50,
				},
				overchargeDecay: {
					min: 5,
				},
				[Damage.damageType.antimatter]: {
					threshold: 10,
					factor: 1,
				},
				[Damage.damageType.biological]: {
					min: -5,
					max: 10,
				},
			},
			{
				default: {
					max: 50,
					factor: 1.2,
					threshold: 2,
				},
				baseCost: {
					bias: -5,
					factor: 0.8,
				},
				overchargeDecay: {
					bias: 5,
					max: 10,
				},
				[Damage.damageType.biological]: {
					max: 20,
					factor: 0.5,
				},
				[Damage.damageType.heat]: {
					bias: -10,
				},
			},
		],
		Object.assign({}, SampleInstanceSnapshot, {
			baseCost: SampleInstanceSnapshot.baseCost * 0.8 - 10,
			shield: DefenseManager.serializeProtection({
				...SampleInstanceSnapshot.shield,
				shieldAmount: BasicMaths.applyModifier(SampleInstanceSnapshot.shield.shieldAmount, {
					bias: 50,
					factor: 2,
					max: 120,
					maxGuard: 50,
				}),
				overchargeDecay: BasicMaths.applyModifier(SampleInstanceSnapshot.shield.overchargeDecay, {
					bias: 5,
					max: 10,
					min: 5,
				}),
				...Damage.TDamageTypesArray.reduce(
					(obj, dt) =>
						Object.assign({}, obj, {
							[dt]: DefenseManager.stackDamageProtection(
								DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, dt),
								{
									factor: 1.2,
									threshold: 2,
									bias: -5,
									max: 50,
								},
							),
						}),
					{},
				),
				[Damage.damageType.biological]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.biological,
					),
					{
						max: 10,
						min: -5,
					},
				),
				[Damage.damageType.heat]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(SampleInstanceSnapshot.shield, Damage.damageType.heat),
					{
						bias: -15,
					},
				),
				[Damage.damageType.antimatter]: DefenseManager.stackDamageProtection(
					DefenseManager.getDamageProtectorDecorator(
						SampleInstanceSnapshot.shield,
						Damage.damageType.antimatter,
					),
					{
						threshold: 10,
						factor: 2.4,
						max: 50,
					},
				),
			}),
		}),
	],
];

export const shieldModifierStackCases: Array<
	[string, UnitDefense.TShieldsModifier | UnitDefense.TShieldsModifier[], UnitDefense.TShieldsModifier]
> = [
	['returns empty object when called with no parameters', undefined, {}],
	['returns empty object when called with empty modifier', {}, {}],
	[
		'expands single modifier with min=0',
		{
			baseCost: {
				factor: 0.3,
			},
			default: {
				bias: -15,
			},
		},
		{
			baseCost: {
				factor: 0.3,
			},
			default: {
				bias: -15,
				min: 0,
			},
		},
	],
	[
		'extracts single modifier from array',
		[
			{
				baseCost: {
					factor: 0.4,
				},
				default: {
					bias: -25,
				},
			},
		],
		{
			baseCost: {
				factor: 0.4,
			},
			default: {
				bias: -25,
				min: 0,
			},
		},
	],
	[
		'stacks baseCost',
		[
			{
				baseCost: {
					factor: 0.4,
					bias: -5,
					max: 0,
					minGuard: 3,
				},
			},
			{
				baseCost: {
					factor: 0.7,
					bias: -15,
					max: 5,
					minGuard: 5,
				},
			},
		],
		{
			baseCost: {
				factor: 0.1,
				bias: -20,
				max: 0,
				minGuard: 5,
			},
		},
	],
	[
		'stacks overcharge decay',
		[
			{
				overchargeDecay: {
					factor: 0.8,
					max: 5,
					min: 5,
				},
			},
			{
				overchargeDecay: {
					factor: 0.7,
					bias: -15,
					min: 5,
					minGuard: 5,
				},
			},
		],
		{
			overchargeDecay: {
				factor: 0.5,
				bias: -15,
				min: 5,
				max: 5,
				minGuard: 5,
			},
		},
	],
	[
		'stacks capacity',
		[
			{
				shieldAmount: {
					factor: 0.5,
					bias: -1,
					max: 20,
					min: 1,
				},
			},
			{
				shieldAmount: {
					factor: 2,
					bias: -12,
					min: 5,
					max: 25,
				},
			},
		],
		{
			shieldAmount: {
				factor: 1.5,
				bias: -13,
				min: 5,
				max: 20,
			},
		},
	],
	[
		'combines protection keys',
		[
			{
				[Damage.damageType.heat]: {
					threshold: 35,
					factor: 0.8,
				},
			},
			{
				baseCost: {
					factor: 0.3,
					bias: -12,
				},
				[Damage.damageType.thermodynamic]: {
					factor: 0.8,
					bias: -10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.3,
				bias: -12,
			},
			[Damage.damageType.heat]: {
				threshold: 35,
				factor: 0.8,
				min: 0,
			},
			[Damage.damageType.thermodynamic]: {
				factor: 0.8,
				bias: -10,
				min: 0,
			},
		},
	],
	[
		'stacks protection keys',
		[
			{
				[Damage.damageType.radiation]: {
					bias: -25,
					factor: 0.4,
				},
				[Damage.damageType.antimatter]: {
					max: 50,
					factor: 0.1,
					threshold: 10,
				},
				shieldAmount: {
					factor: 5,
					max: 500,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -10,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.5,
				bias: -10,
			},
			shieldAmount: {
				factor: 5,
				max: 500,
			},
			[Damage.damageType.radiation]: {
				bias: -25,
				factor: 0.4,
				min: 0,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.08,
				bias: -10,
				max: 50,
				min: 0,
				threshold: 10,
			},
		},
	],
	[
		'applies default protection to all other protections',
		[
			{
				[Damage.damageType.biological]: {
					max: 15,
					factor: 0.5,
				},
				[Damage.damageType.antimatter]: {
					max: 25,
					factor: 0.9,
					threshold: 10,
				},
				baseCost: {
					factor: 0.9,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -10,
				},
				default: {
					factor: 0.75,
					bias: -2,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.4,
				bias: -10,
			},
			default: {
				factor: 0.75,
				bias: -2,
				min: 0,
			},
			[Damage.damageType.biological]: {
				max: 15,
				factor: 0.375,
				bias: -2,
				min: 0,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.72,
				bias: -10,
				max: 25,
				min: 0,
				threshold: 10,
			},
		},
	],
	[
		'applies default protection to all other protections in a cartesian mode',
		[
			{
				[Damage.damageType.biological]: {
					max: 50,
					factor: 0.6,
					min: -10,
				},
				[Damage.damageType.antimatter]: {
					max: 10,
					factor: 0.9,
					threshold: 10,
				},
				default: {
					factor: 0.5,
					bias: -5,
				},
				baseCost: {
					factor: 0.9,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -5,
					min: 1,
				},
				default: {
					factor: 0.9,
					min: 0,
					bias: -2,
					max: 5,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
				[Damage.damageType.kinetic]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.4,
				bias: -5,
				min: 1,
			},
			default: {
				factor: 0.45,
				bias: -7,
				max: 5,
				min: 0,
			},
			[Damage.damageType.biological]: {
				max: 5,
				factor: 0.54,
				bias: -2,
				min: -10,
			},
			[Damage.damageType.kinetic]: {
				factor: 0.4,
				threshold: 5,
				bias: -15,
				min: 0,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.72,
				bias: -10,
				max: 10,
				min: 0,
				threshold: 10,
			},
		},
	],
	[
		'complex case #1',
		[
			{
				[Damage.damageType.biological]: {
					max: 50,
					factor: 0.6,
					min: -10,
				},
				default: {
					factor: 0.6,
					bias: -5,
				},
				shieldAmount: {
					factor: 2,
				},
			},
			{},
			{
				baseCost: {
					factor: 0.5,
					bias: -5,
					min: 1,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.8,
					threshold: 5,
					bias: -10,
				},
			},
			{
				[Damage.damageType.magnetic]: {
					factor: 0.9,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.9,
				},
			},
			{
				baseCost: {
					factor: 0.8,
					bias: -5,
				},
				shieldAmount: {
					bias: -15,
					min: 10,
				},
			},
		],
		{
			baseCost: {
				factor: 0.3,
				bias: -10,
				min: 1,
			},
			shieldAmount: {
				factor: 2,
				bias: -15,
				min: 10,
			},
			default: {
				factor: 0.6,
				bias: -5,
				min: 0,
			},
			[Damage.damageType.biological]: {
				max: 50,
				factor: 0.6,
				min: -10,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.432,
				threshold: 5,
				bias: -15,
				min: 0,
			},
			[Damage.damageType.magnetic]: {
				factor: 0.54,
				bias: -5,
				min: 0,
			},
		},
	],
	[
		'complex case #2',
		[
			{
				shieldAmount: {
					factor: 1.2,
					bias: 50,
				},
			},
			{},
			{
				default: {
					factor: 0.8,
					bias: -5,
				},
				[Damage.damageType.magnetic]: {
					factor: 0.9,
					min: -20,
				},
				overchargeDecay: {
					min: 15,
				},
			},
			{
				baseCost: {
					factor: 0.5,
					bias: -5,
				},
				default: {
					factor: 0.8,
					bias: -5,
					threshold: 1,
				},
			},
			{
				shieldAmount: {
					factor: 1.2,
					bias: 50,
				},
				[Damage.damageType.magnetic]: {
					factor: 0.9,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.9,
				},
			},
			{
				[Damage.damageType.heat]: {
					factor: 0.5,
					max: 35,
				},
				baseCost: {
					max: 100,
				},
			},
			{
				overchargeDecay: {
					max: 10,
				},
			},
		],
		{
			overchargeDecay: {
				max: 10,
				min: 15,
			},
			shieldAmount: {
				factor: 1.4,
				bias: 100,
			},
			baseCost: {
				factor: 0.5,
				bias: -5,
				max: 100,
			},
			default: {
				factor: 0.64,
				bias: -10,
				min: 0,
				threshold: 1,
			},
			[Damage.damageType.magnetic]: {
				factor: 0.648,
				min: -20,
				bias: -5,
				threshold: 1,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.576,
				bias: -10,
				threshold: 1,
				min: 0,
			},
			[Damage.damageType.heat]: {
				factor: 0.32,
				threshold: 1,
				bias: -10,
				max: 35,
				min: 0,
			},
		},
	],
];

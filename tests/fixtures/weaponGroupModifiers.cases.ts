import Weapons from '../../definitions/technologies/types/weapons';
import { stackWeaponGroupModifiers, stackWeaponModifiers } from '../../definitions/technologies/weapons/WeaponManager';

interface IStackWGModifiersCase {
	args: Weapons.TWeaponGroupModifier[];
	output: Weapons.TWeaponGroupModifier;
	title?: string;
}

const sampleModifiers: Weapons.TWeaponModifier[] = [
	{
		baseCost: {
			bias: 5,
		},
		baseRange: {
			factor: 1.2,
			max: 16,
			maxGuard: 16,
		},
	},
	{
		baseCost: {
			bias: -2,
			factor: 1.3,
		},
		baseAccuracy: {
			factor: 0.9,
			bias: -0.1,
		},
	},
	{
		baseRange: {
			factor: 0.7,
			min: 1,
		},
		baseRate: {
			factor: 0.7,
		},
		aoeRadiusModifier: {
			factor: 0.7,
		},
	},
	{
		priority: {
			bias: -1,
		},
	},
	{
		baseAccuracy: {
			factor: 1.6,
		},
	},
];
export const StackWGTestCases: IStackWGModifiersCase[] = [
	{
		args: [],
		output: {},
		title: 'Null Object',
	},
	{
		title: 'copies values',
		args: [
			{
				default: {
					...sampleModifiers[0],
				},
				[Weapons.TWeaponGroupType.Guns]: {
					...sampleModifiers[1],
				},
			},
		],
		output: {
			default: {
				...sampleModifiers[0],
			},
			[Weapons.TWeaponGroupType.Guns]: {
				...sampleModifiers[1],
			},
		},
	},
	{
		title: 'stacks default with every other group',
		args: [
			{
				default: {
					...sampleModifiers[4],
				},
				[Weapons.TWeaponGroupType.Bombs]: {
					...sampleModifiers[1],
				},
			},
			{
				default: {
					...sampleModifiers[3],
				},
				[Weapons.TWeaponGroupType.Guns]: {
					...sampleModifiers[0],
				},
			},
		],
		output: {
			default: stackWeaponModifiers(sampleModifiers[4], sampleModifiers[3]),
			[Weapons.TWeaponGroupType.Guns]: stackWeaponModifiers(sampleModifiers[0], sampleModifiers[4]),
			[Weapons.TWeaponGroupType.Bombs]: stackWeaponModifiers(sampleModifiers[1], sampleModifiers[3]),
		},
	},
	{
		title: 'Commutativity',
		args: [
			{
				default: {
					...sampleModifiers[3],
				},
				[Weapons.TWeaponGroupType.Guns]: {
					...sampleModifiers[0],
				},
			},
			{
				default: {
					...sampleModifiers[4],
				},
				[Weapons.TWeaponGroupType.Bombs]: {
					...sampleModifiers[1],
				},
			},
		],
		output: {
			default: stackWeaponModifiers(sampleModifiers[4], sampleModifiers[3]),
			[Weapons.TWeaponGroupType.Guns]: stackWeaponModifiers(sampleModifiers[0], sampleModifiers[4]),
			[Weapons.TWeaponGroupType.Bombs]: stackWeaponModifiers(sampleModifiers[1], sampleModifiers[3]),
		},
	},
	{
		title: 'Stacks complex case #1',
		args: [
			{
				default: {
					...sampleModifiers[0],
				},
				[Weapons.TWeaponGroupType.Rockets]: {
					...sampleModifiers[1],
				},
				[Weapons.TWeaponGroupType.Missiles]: {
					...sampleModifiers[1],
				},
			},
			{
				default: {
					...sampleModifiers[3],
				},
			},
			{
				[Weapons.TWeaponGroupType.Launchers]: {
					...sampleModifiers[2],
				},
				[Weapons.TWeaponGroupType.Missiles]: {
					...sampleModifiers[4],
				},
			},
		],
		output: {
			default: stackWeaponModifiers(sampleModifiers[0], sampleModifiers[3]),
			[Weapons.TWeaponGroupType.Rockets]: stackWeaponModifiers(sampleModifiers[1], sampleModifiers[3]),
			[Weapons.TWeaponGroupType.Missiles]: stackWeaponModifiers(
				sampleModifiers[4],
				sampleModifiers[3],
				sampleModifiers[1],
			),
			[Weapons.TWeaponGroupType.Launchers]: stackWeaponModifiers(
				sampleModifiers[2],
				sampleModifiers[3],
				sampleModifiers[0],
			),
		},
	},
	{
		title: 'Stacks complex case #2',
		args: [
			{
				[Weapons.TWeaponGroupType.Rockets]: {
					...sampleModifiers[0],
				},
				[Weapons.TWeaponGroupType.Guns]: {
					...sampleModifiers[0],
				},
			},
			{
				default: {
					...sampleModifiers[1],
				},
			},
			{
				[Weapons.TWeaponGroupType.Guns]: {
					...sampleModifiers[2],
				},
				[Weapons.TWeaponGroupType.Bombs]: {
					...sampleModifiers[3],
				},
			},
			{
				default: {
					...sampleModifiers[4],
				},
				[Weapons.TWeaponGroupType.Rockets]: {
					...sampleModifiers[1],
				},
				[Weapons.TWeaponGroupType.Bombs]: {
					...sampleModifiers[1],
				},
			},
		],
		output: {
			default: stackWeaponModifiers(sampleModifiers[1], sampleModifiers[4]),
			[Weapons.TWeaponGroupType.Rockets]: stackWeaponModifiers(
				sampleModifiers[0],
				sampleModifiers[1],
				sampleModifiers[1],
			),
			[Weapons.TWeaponGroupType.Guns]: stackWeaponModifiers(
				sampleModifiers[2],
				sampleModifiers[0],
				sampleModifiers[1],
				sampleModifiers[4],
			),
			[Weapons.TWeaponGroupType.Bombs]: stackWeaponModifiers(
				sampleModifiers[3],
				sampleModifiers[1],
				sampleModifiers[1],
			),
		},
	},
	{
		title: 'Associativity',
		args: [
			{
				[Weapons.TWeaponGroupType.Launchers]: {
					...sampleModifiers[2],
				},
				[Weapons.TWeaponGroupType.Missiles]: {
					...sampleModifiers[4],
				},
			},
			stackWeaponGroupModifiers(
				{
					default: {
						...sampleModifiers[3],
					},
				},
				{
					default: {
						...sampleModifiers[0],
					},
					[Weapons.TWeaponGroupType.Rockets]: {
						...sampleModifiers[1],
					},
					[Weapons.TWeaponGroupType.Missiles]: {
						...sampleModifiers[1],
					},
				},
			),
		],
		output: {
			default: stackWeaponModifiers(sampleModifiers[0], sampleModifiers[3]),
			[Weapons.TWeaponGroupType.Rockets]: stackWeaponModifiers(sampleModifiers[1], sampleModifiers[3]),
			[Weapons.TWeaponGroupType.Missiles]: stackWeaponModifiers(
				sampleModifiers[4],
				sampleModifiers[3],
				sampleModifiers[1],
			),
			[Weapons.TWeaponGroupType.Launchers]: stackWeaponModifiers(
				sampleModifiers[2],
				sampleModifiers[3],
				sampleModifiers[0],
			),
		},
	},
];

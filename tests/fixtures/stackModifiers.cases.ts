export interface IStackModifiersTestCase {
	args: IModifier[];
	output: IModifier;
	title?: string;
}

export const StackModifiersTestCases: IStackModifiersTestCase[] = [
	{
		args: [],
		output: {} as IModifier,
		title: 'Null Object',
	},
	{
		title: 'Returns the same values if single parameter, all properties',
		args: [
			{
				factor: 2.5,
				bias: -1,
				min: 3,
				max: 6,
			} as IModifier,
		],
		output: {
			factor: 2.5,
			bias: -1,
			min: 3,
			max: 6,
		} as IModifier,
	},
	{
		title: 'Returns the same values if single parameter, some options empty #1',
		args: [
			{
				factor: 2.5,
				min: 6,
			} as IModifier,
		],
		output: {
			factor: 2.5,
			min: 6,
		} as IModifier,
	},
	{
		title: 'Returns the same values if single parameter, some options empty #2',
		args: [
			{
				max: 10,
			} as IModifier,
		],
		output: {
			max: 10,
		} as IModifier,
	},
	{
		title: 'Correctly adds factor #1',
		args: [
			{
				factor: 1.5,
				max: 4,
			},
			{
				factor: 2.7,
			},
			{
				factor: 0.8,
			},
		] as IModifier[],
		output: {
			factor: 3,
			max: 4,
		} as IModifier,
	},
	{
		title: 'Correctly adds factor #2',
		args: [
			{
				factor: 0.2,
			},
			{
				factor: 0.3,
			},
			{
				factor: 1.1,
				bias: 3,
			},
		] as IModifier[],
		output: {
			factor: -0.4,
			bias: 3,
		} as IModifier,
	},
	{
		title: 'Correctly adds factor #3',
		args: [
			{
				factor: 1.4,
			},
			{
				factor: 0.3,
				min: -1,
			},
			{
				factor: 1.7,
			},
			{
				factor: 0.4,
			},
		] as IModifier[],
		output: {
			factor: 0.8,
			min: -1,
		} as IModifier,
	},
	{
		title: 'Correctly adds factor #4',
		args: [
			{
				factor: 2.2,
			},
			{
				bias: 1,
			},
			{
				factor: 0.7,
				min: 5,
			},
			{
				factor: -0.2,
				max: 12,
			},
		] as IModifier[],
		output: {
			factor: 0.7,
			bias: 1,
			min: 5,
			max: 12,
		} as IModifier,
	},
	{
		title: 'Correctly adds bias #1',
		args: [
			{
				bias: 7,
			},
			{
				factor: 1.2,
				bias: 1,
			},
			{
				bias: -5,
				min: 5,
			},
			{
				bias: -3,
			},
		] as IModifier[],
		output: {
			factor: 1.2,
			min: 5,
		} as IModifier,
	},
	{
		title: 'Correctly adds bias #2',
		args: [
			{
				bias: -2,
			},
			{
				max: 15,
				bias: 1,
			},
			{
				bias: 4,
			},
			{
				bias: 0,
			},
		] as IModifier[],
		output: {
			bias: 3,
			max: 15,
		} as IModifier,
	},
	{
		title: 'Correctly adds bias #3',
		args: [
			{
				bias: 15,
			},
			{
				bias: -15,
			},
			{
				bias: 12,
				factor: 2,
				min: 20,
				max: 100,
			},
			{
				bias: -2,
				factor: 0.5,
			},
		] as IModifier[],
		output: {
			factor: 1.5,
			bias: 10,
			min: 20,
			max: 100,
		} as IModifier,
	},
	{
		title: 'Correctly adds bias #4',
		args: [
			{
				bias: 5,
			},
			{
				bias: 5,
				min: 10,
			},
			{
				bias: 5,
				factor: 2,
				max: 20,
			},
			{
				factor: 1,
			},
		] as IModifier[],
		output: {
			factor: 2,
			bias: 15,
			min: 10,
			max: 20,
		} as IModifier,
	},
	{
		title: 'Correctly stacks min and max #1',
		args: [
			{
				bias: 5,
				min: 5,
			},
			{
				bias: 5,
				min: 10,
			},
			{
				bias: 5,
				factor: 2,
				min: 0,
			},
			{
				min: 5,
			},
		] as IModifier[],
		output: {
			factor: 2,
			bias: 15,
			min: 10,
		} as IModifier,
	},
	{
		title: 'Correctly stacks min and max #2',
		args: [
			{
				min: 5,
			},
			{
				max: 10,
			},
			{
				max: 7,
			},
			{
				min: 6,
			},
		] as IModifier[],
		output: {
			min: 6,
			max: 7,
		} as IModifier,
	},
	{
		title: 'Correctly stacks min and max #3',
		args: [
			{
				min: 0,
				max: 10,
			},
			{
				max: 5,
				min: 1,
			},
			{
				max: 2,
				min: 5,
			},
			{
				min: -1,
				max: 1,
			},
		] as IModifier[],
		output: {
			min: 5,
			max: 1,
		} as IModifier,
	},
	{
		title: 'Correctly stacks min and max #4',
		args: [
			{
				min: 0,
				factor: 1.2,
			},
			{
				bias: 2,
				max: 10,
			},
			{
				bias: -1,
				factor: 0.9,
				min: 3,
			},
			{
				factor: 1.4,
				bias: 9,
				min: 1,
				max: 7,
			},
		] as IModifier[],
		output: {
			factor: 1.5,
			bias: 10,
			min: 3,
			max: 7,
		} as IModifier,
	},
	{
		title: 'Correctly stacks minGuard',
		args: [
			{
				min: 1,
				minGuard: 5,
			},
			{
				factor: 1.2,
			},
			{
				max: 15,
				maxGuard: 20,
				minGuard: 10,
			},
			{
				bias: 5,
				minGuard: -5,
			},
		],
		output: {
			min: 1,
			minGuard: 10,
			maxGuard: 20,
			max: 15,
			factor: 1.2,
			bias: 5,
		},
	},
	{
		title: 'Correctly stacks maxGuard',
		args: [
			{
				min: 1,
				maxGuard: 5,
			},
			{
				factor: 1.2,
			},
			{
				max: 15,
				maxGuard: 20,
				minGuard: 10,
			},
			{
				bias: 5,
				maxGuard: -5,
			},
		],
		output: {
			min: 1,
			minGuard: 10,
			maxGuard: -5,
			max: 15,
			factor: 1.2,
			bias: 5,
		},
	},
];

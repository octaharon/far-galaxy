import AttackManager from '../../definitions/tactical/damage/AttackManager';
import {
	ATTACK_PRIORITIES,
	COMBAT_TICKS_PER_TURN,
	MOVEMENT_DODGE_BONUS_CAP,
	MOVEMENT_DODGE_MODIFIER,
} from '../../definitions/tactical/damage/constants';

export const hitChanceAdjustmentCases: Array<{
	title?: string;
	params: Parameters<typeof AttackManager.adjustAccuracyToMovement>;
	out: number;
}> = [
	{
		title: `doesn't affect accuracy if target is not moving`,
		params: [1.3, ATTACK_PRIORITIES.NORMAL, { x: 0, y: 0 }, { x: 25, y: 10 }, { x: -20, y: 40 }],
		out: 1.3,
	},
	{
		title: `doesn't affect accuracy if line of fire is collinear with target movement`,
		params: [1.5, ATTACK_PRIORITIES.LOW, { x: 5, y: 5 }, { x: 20, y: 30 }, { x: 10, y: 20 }],
		out: 1.5,
	},
	{
		title: `doesn't affect accuracy if line of fire is opposite to target movement`,
		params: [1.5, ATTACK_PRIORITIES.HIGHEST, { x: -5, y: -5 }, { x: 20, y: 30 }, { x: 10, y: 20 }],
		out: 1.5,
	},
	{
		title: `perpendicular movement at speed 1 and distance 1 reduces hit chance by ${MOVEMENT_DODGE_MODIFIER} for normal priority weapon`,
		params: [
			1,
			ATTACK_PRIORITIES.NORMAL,
			{ x: 1 / COMBAT_TICKS_PER_TURN, y: 0 },
			{ x: 10, y: 10 },
			{ x: 10, y: 11 },
		],
		out: 1 - MOVEMENT_DODGE_MODIFIER,
	},
	{
		title: `perpendicular movement at speed 5 and distance 5 applies the same reduction as the previous case`,
		params: [
			1,
			ATTACK_PRIORITIES.NORMAL,
			{ x: 5 / COMBAT_TICKS_PER_TURN, y: 0 },
			{ x: 10, y: 10 },
			{ x: 10, y: 15 },
		],
		out: 1 - MOVEMENT_DODGE_MODIFIER,
	},
	{
		title: `accuracy penalty is capped at ${MOVEMENT_DODGE_BONUS_CAP}`,
		params: [2, ATTACK_PRIORITIES.NORMAL, { x: 100, y: 0 }, { x: 10, y: 10 }, { x: 10, y: 11 }],
		out: 2 - MOVEMENT_DODGE_BONUS_CAP,
	},
	{
		title: `perpendicular movement at speed 2.5 and distance 1 reduces hit chance by ${
			MOVEMENT_DODGE_MODIFIER * 2.5
		} for normal priority weapon`,
		params: [
			1,
			ATTACK_PRIORITIES.NORMAL,
			{ x: 2.5 / COMBAT_TICKS_PER_TURN, y: 0 },
			{ x: 10, y: 10 },
			{ x: 10, y: 11 },
		],
		out: 1 - MOVEMENT_DODGE_MODIFIER * 2.5,
	},
	{
		title: `perpendicular movement at speed 1 and distance 1 reduces hit chance by ${
			0.4 * MOVEMENT_DODGE_MODIFIER
		} for highest priority weapon`,
		params: [
			1,
			ATTACK_PRIORITIES.HIGHEST,
			{ x: -1 / COMBAT_TICKS_PER_TURN, y: 0 },
			{ x: 10, y: 10 },
			{ x: 10, y: 11 },
		],
		out: 1 - 0.4 * MOVEMENT_DODGE_MODIFIER,
	},
	{
		title: `Extremely low priority is capping hit chance penalty at ${
			2 * MOVEMENT_DODGE_MODIFIER
		} for lowest priority weapon`,
		params: [1, Infinity, { x: -1 / COMBAT_TICKS_PER_TURN, y: 0 }, { x: 10, y: 10 }, { x: 10, y: 11 }],
		out: 1 - 2 * MOVEMENT_DODGE_MODIFIER,
	},
	{
		title: `Extremely high priority never reduces penalty by less than ${
			0.2 * MOVEMENT_DODGE_MODIFIER
		} for highest priority weapon`,
		params: [1, -Infinity, { x: -1 / COMBAT_TICKS_PER_TURN, y: 0 }, { x: 10, y: 10 }, { x: 10, y: 11 }],
		out: 1 - 0.2 * MOVEMENT_DODGE_MODIFIER,
	},
	{
		title: `perpendicular movement at speed 3 and distance 1 reduces hit chance by ${
			MOVEMENT_DODGE_MODIFIER * 1.2
		} for highest priority weapon`,
		params: [
			1,
			ATTACK_PRIORITIES.HIGHEST,
			{ x: 3 / COMBAT_TICKS_PER_TURN, y: 0 },
			{ x: 10, y: 10 },
			{ x: 10, y: 11 },
		],
		out: 1 - MOVEMENT_DODGE_MODIFIER * 1.2,
	},
	{
		title: `Moving at angle with _tangential_ speed 1 at distance 1 reduces hit chance by ${MOVEMENT_DODGE_MODIFIER} for normal priority weapon`,
		params: [
			1,
			ATTACK_PRIORITIES.NORMAL,
			{ x: 1 / COMBAT_TICKS_PER_TURN, y: 4 / COMBAT_TICKS_PER_TURN },
			{ x: 10, y: 11 },
			{ x: 10, y: 10 },
		],
		out: 1 - MOVEMENT_DODGE_MODIFIER,
	},
	{
		title: `Moving at angle with _tangential_ speed 3 at distance 1 reduces hit chance by ${
			MOVEMENT_DODGE_MODIFIER * 3
		} for normal priority weapon`,
		params: [
			1,
			ATTACK_PRIORITIES.NORMAL,
			{ x: 3 / COMBAT_TICKS_PER_TURN, y: -10 / COMBAT_TICKS_PER_TURN },
			{ x: 10, y: 11 },
			{ x: 10, y: 10 },
		],
		out: 1 - MOVEMENT_DODGE_MODIFIER * 3,
	},
];

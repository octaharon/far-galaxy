import Damage from '../../definitions/tactical/damage/damage';
import UnitDefense from '../../definitions/tactical/damage/defense';
import DefenseManager from '../../definitions/tactical/damage/DefenseManager';

export const addShieldsCases: Array<{
	title: string;
	args: Parameters<typeof DefenseManager.addShields>;
	to: UnitDefense.TShield;
}> = [
	{
		title: 'No args',
		args: [undefined, undefined],
		to: null,
	},
	{
		title: 'Basic shield with 0 second arg',
		args: [
			{
				shieldAmount: 20,
				currentAmount: 20,
				default: {
					factor: 0.85,
				},
			},
			undefined,
		],
		to: {
			shieldAmount: 20,
			currentAmount: 20,
			default: {
				factor: 0.85,
				min: 0,
			},
		},
	},
	{
		title: `Adds the specified capacity`,
		args: [
			{
				shieldAmount: 20,
				currentAmount: 10,
				default: {
					factor: 0.85,
				},
			},
			5,
		],
		to: {
			shieldAmount: 20,
			currentAmount: 15,
			default: {
				factor: 0.85,
				min: 0,
			},
		},
	},
	{
		title: `Doesn't add beyond capacity`,
		args: [
			{
				shieldAmount: 20,
				currentAmount: 10,
				default: {
					factor: 0.85,
				},
			},
			100,
		],
		to: {
			shieldAmount: 20,
			currentAmount: 20,
			default: {
				factor: 0.85,
				min: 0,
			},
		},
	},
	{
		title: `Adds beyond capacity if has Overcharge`,
		args: [
			{
				overchargeDecay: 5,
				shieldAmount: 20,
				currentAmount: 10,
				[Damage.damageType.kinetic]: {
					factor: 0.85,
				},
				[Damage.damageType.heat]: {
					factor: 0.85,
					bias: -5,
					min: 0,
					minGuard: -Infinity,
				},
			},
			25,
		],
		to: {
			overchargeDecay: 5,
			shieldAmount: 20,
			currentAmount: 35,
			[Damage.damageType.kinetic]: {
				factor: 0.85,
				min: 0,
			},
			[Damage.damageType.heat]: {
				factor: 0.85,
				bias: -5,
				min: 0,
			},
		},
	},
	{
		title: `Reduces capacity if value is negative`,
		args: [
			{
				shieldAmount: 20,
				currentAmount: 10,
				default: {
					bias: -5,
				},
			},
			-5,
		],
		to: {
			shieldAmount: 20,
			currentAmount: 5,
			default: {
				bias: -5,
				min: 0,
			},
		},
	},
	{
		title: `Doesn't reduce below zero`,
		args: [
			{
				overchargeDecay: 5,
				shieldAmount: 20,
				currentAmount: 10,
				default: {
					bias: -15,
				},
			},
			-25,
		],
		to: {
			overchargeDecay: 5,
			shieldAmount: 20,
			currentAmount: 0,
			default: {
				bias: -15,
				min: 0,
			},
		},
	},
];

export const optimizeProtectionCases: Array<{
	title: string;
	from: UnitDefense.TArmor & Record<string, any>;
	to: UnitDefense.TArmor & Record<string, any>;
}> = [
	{
		title: 'Empty object',
		from: {},
		to: {},
	},
	{
		title: 'Default only, optimal as it is',
		from: {
			default: {
				min: 0,
				bias: 5,
			},
		},
		to: {
			default: {
				min: 0,
				bias: 5,
			},
		},
	},
	{
		title: 'No default value, optimal as it is',
		from: {
			[Damage.damageType.kinetic]: {
				min: 0,
				bias: 5,
			},
			[Damage.damageType.antimatter]: {
				min: 0,
				factor: 1.5,
				bias: -5,
			},
		},
		to: {
			[Damage.damageType.kinetic]: {
				min: 0,
				bias: 5,
			},
			[Damage.damageType.antimatter]: {
				min: 0,
				factor: 1.5,
				bias: -5,
			},
		},
	},
	{
		title: 'Copies extraneous fields',
		from: {
			baseCost: 5,
			title: 'whatever',
			[Damage.damageType.antimatter]: {
				min: 0,
				factor: 1.5,
				bias: -5,
			},
		},
		to: {
			baseCost: 5,
			title: 'whatever',
			[Damage.damageType.antimatter]: {
				min: 0,
				factor: 1.5,
				bias: -5,
			},
		},
	},
	{
		title: 'Extra decorator identical with default',
		from: {
			default: {
				min: 0,
				bias: -5,
				factor: 0.8,
			},
			[Damage.damageType.antimatter]: {
				factor: 1.5,
				bias: -5,
			},
			[Damage.damageType.kinetic]: {
				min: 0,
				bias: -5,
				factor: 0.8,
			},
		},
		to: {
			default: {
				min: 0,
				bias: -5,
				factor: 0.8,
			},
			[Damage.damageType.antimatter]: {
				factor: 1.5,
				bias: -5,
			},
		},
	},
	{
		title: 'Two extra decorators identical with default',
		from: {
			extra: 'test value',
			default: {
				min: 0,
				bias: -5,
				factor: 0.8,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.5,
				bias: -5,
				threshold: 10,
			},
			[Damage.damageType.kinetic]: {
				min: 0,
				bias: -5,
				factor: 0.8,
			},
			[Damage.damageType.warp]: {
				min: 0,
				bias: -5,
				factor: 0.8,
			},
			[Damage.damageType.heat]: {
				max: 10,
			},
		},
		to: {
			extra: 'test value',
			default: {
				min: 0,
				bias: -5,
				factor: 0.8,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.5,
				bias: -5,
				threshold: 10,
			},
			[Damage.damageType.heat]: {
				max: 10,
			},
		},
	},
	{
		title: 'Collapses full protection collection with all identical modifiers to default',
		from: {
			extra: 'test value',
			...Damage.TDamageTypesArray.reduce(
				(obj, dt) => ({
					...obj,
					[dt]: {
						threshold: 5,
						max: 25,
						factor: 0.8,
					},
				}),
				{},
			),
		},
		to: {
			extra: 'test value',
			default: {
				threshold: 5,
				max: 25,
				factor: 0.8,
			},
		},
	},
	{
		title: 'Collapses full protection collection with few identical modifiers to default, preserving outstanding values',
		from: {
			extra: 'test value',
			...Damage.TDamageTypesArray.reduce(
				(obj, dt) => ({
					...obj,
					[dt]: {
						factor: 0.8,
					},
				}),
				{},
			),
			[Damage.damageType.biological]: {
				threshold: 10,
				min: 0,
				bias: -5,
			},
			[Damage.damageType.kinetic]: {
				factor: 0.5,
			},
		},
		to: {
			extra: 'test value',
			default: {
				factor: 0.8,
			},
			[Damage.damageType.biological]: {
				threshold: 10,
				min: 0,
				bias: -5,
			},
			[Damage.damageType.kinetic]: {
				factor: 0.5,
			},
		},
	},
	{
		title: `Doesn't collapse when has at least one empty protection`,
		from: {
			whateverKey: 'whateverValue',
			...Damage.TDamageTypesArray.reduce(
				(obj, dt) => ({
					...obj,
					[dt]: {
						factor: 0.8,
					},
				}),
				{},
			),
			[Damage.damageType.biological]: {
				max: Infinity,
				bias: 0,
			},
			[Damage.damageType.kinetic]: {
				factor: 0.5,
			},
		},
		to: {
			whateverKey: 'whateverValue',
			...Damage.TDamageTypesArray.filter((v) => v !== Damage.damageType.biological).reduce(
				(obj, dt) => ({
					...obj,
					[dt]: {
						factor: 0.8,
					},
				}),
				{},
			),
			[Damage.damageType.kinetic]: {
				factor: 0.5,
			},
		},
	},
];

import Weapons from '../../definitions/technologies/types/weapons';
import { stackWeaponModifiers } from '../../definitions/technologies/weapons/WeaponManager';

interface IStackWeaponModifiersCase {
	args: Weapons.TWeaponModifier[];
	output: Weapons.TWeaponModifier;
	title?: string;
}

const sampleModifiers: Weapons.TWeaponModifier[] = [
	{
		baseCost: {
			bias: 5,
		},
		baseRange: {
			factor: 1.2,
		},
	},
	{
		baseCost: {
			bias: -2,
			factor: 1.3,
		},
		baseAccuracy: {
			factor: 0.9,
			bias: -0.1,
		},
	},
	{
		baseRange: {
			factor: 0.7,
			min: 1,
		},
		baseRate: {
			factor: 0.7,
		},
		aoeRadiusModifier: {
			factor: 0.7,
		},
	},
	{
		priority: {
			bias: -1,
		},
	},
	{
		baseAccuracy: {
			factor: 1.6,
		},
	},
];
export const StackWGTestCases: IStackWeaponModifiersCase[] = [
	{
		args: [],
		output: {},
		title: 'Null Object',
	},
	{
		args: [sampleModifiers[0]],
		output: {
			...sampleModifiers[0],
		},
	},
	{
		args: [sampleModifiers[0], sampleModifiers[1]],
		title: '1 overlapping property',
		output: {
			...sampleModifiers[0],
			...sampleModifiers[1],
			baseCost: {
				bias: 3,
				factor: 1.3,
			},
		},
	},
	{
		args: [sampleModifiers[1], sampleModifiers[0]],
		title: 'Commutativity',
		output: {
			...sampleModifiers[1],
			...sampleModifiers[0],
			baseCost: {
				bias: 3,
				factor: 1.3,
			},
		},
	},
	{
		args: [sampleModifiers[0], sampleModifiers[2], sampleModifiers[4]],
		title: '2 overlapping properties',
		output: {
			baseCost: {
				bias: 5,
			},
			baseRange: {
				factor: 0.9,
				min: 1,
			},
			baseAccuracy: {
				factor: 1.6,
			},
			baseRate: {
				factor: 0.7,
			},
			aoeRadiusModifier: {
				factor: 0.7,
			},
		},
	},
	{
		args: [sampleModifiers[2], stackWeaponModifiers(sampleModifiers[4], sampleModifiers[0])],
		title: 'Associativity',
		output: {
			baseCost: {
				bias: 5,
			},
			baseRange: {
				factor: 0.9,
				min: 1,
			},
			baseAccuracy: {
				factor: 1.6,
			},
			baseRate: {
				factor: 0.7,
			},
			aoeRadiusModifier: {
				factor: 0.7,
			},
		},
	},
	{
		args: [sampleModifiers[2], sampleModifiers[1], sampleModifiers[3]],
		title: 'No overlapping properties',
		output: {
			baseCost: {
				bias: -2,
				factor: 1.3,
			},
			baseAccuracy: {
				factor: 0.9,
				bias: -0.1,
			},
			baseRange: {
				factor: 0.7,
				min: 1,
			},
			baseRate: {
				factor: 0.7,
			},
			aoeRadiusModifier: {
				factor: 0.7,
			},
			priority: {
				bias: -1,
			},
		},
	},
	{
		args: [...sampleModifiers],
		title: '5 sample modifiers at once',
		output: {
			baseCost: {
				bias: 3,
				factor: 1.3,
			},
			priority: {
				bias: -1,
			},
			baseAccuracy: {
				bias: -0.1,
				factor: 1.5,
			},
			aoeRadiusModifier: {
				factor: 0.7,
			},
			baseRange: {
				factor: 0.9,
				min: 1,
			},
			baseRate: {
				factor: 0.7,
			},
		},
	},
];

import { expect } from 'chai';
import 'mocha';
import Damage from '../definitions/tactical/damage/damage';
import { applyDamageTypeModifiers } from '../definitions/tactical/damage/damageModel';
import UnitChassis from '../definitions/technologies/types/chassis';

type testBody = {
	readonly dt: Parameters<typeof applyDamageTypeModifiers>;
	readonly res: Damage.TDamageUnit;
	readonly mes: string;
};
const units: Array<Partial<Record<UnitChassis.targetType, boolean>>> = [
	{
		[UnitChassis.targetType.organic]: true, //0
	},
	{
		[UnitChassis.targetType.massive]: true, //1
	},
	{
		[UnitChassis.targetType.robotic]: true, //2
	},
	{
		[UnitChassis.targetType.unique]: true, //3
	},
];

function makeTest(tests: testBody[]) {
	for (let i = 0; i < tests.length; i++) {
		it(tests[i]?.mes, () => {
			expect(applyDamageTypeModifiers(...tests[i].dt)).to.be.deep.equal(tests[i].res);
		});
	}
}

describe('Damage Models', () => {
	describe('Bio Damage test', () => {
		const tests: testBody[] = [
			{
				dt: [{ [Damage.damageType.biological]: 1 }, 1, 0, units[0]],
				res: { [Damage.damageType.biological]: 1.5 },
				mes: 'BIO Damage vs Bio target with NO SHIELD and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.biological]: 0.75 }, 1, 1, units[0]],
				res: { [Damage.damageType.biological]: 0.75 },
				mes: 'BIO Damage vs Bio target with SHIELD and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.biological]: 0.75 }, 0.49, 1, units[0]],
				res: { [Damage.damageType.biological]: 0.75 },
				mes: 'BIO Damage vs Bio target with SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.biological]: 1 }, 0.49, 0, units[0]],
				res: { [Damage.damageType.biological]: 1.5 },
				mes: 'BIO Damage vs Bio target with NO SHIELD and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.biological]: 1 }, 0.49, 0, { ...units[1], ...units[2], ...units[3] }],
				res: { [Damage.damageType.biological]: 1 },
				mes: 'BIO Damage vs non bio target with NO SHIELD and 49%hp',
			},
		];
		makeTest(tests);
	});
	describe('Heat Damage tests', () => {
		const tests: testBody[] = [
			{
				// heat VS bio

				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0, units[0]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to bio Target with NO SHIELD and 51%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0, units[0]],
				res: { [Damage.damageType.heat]: 12.5 },
				mes: 'HEA Damage to bio Target with NO SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0.1, units[0]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to bio Target with SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0.1, units[0]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to bio Target with SHIELD and 51%',
			},
			{
				// heat VS massive

				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0, units[1]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to massive Target with NO SHIELD and 51%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0, units[1]],
				res: { [Damage.damageType.heat]: 12.5 },
				mes: 'HEA Damage to massive Target with NO SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0.1, units[1]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to massive Target with SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0.1, units[1]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to massive Target with SHIELD and 51%',
			},
			{
				// heat VS robotic

				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0, units[2]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to robotic Target with NO SHIELD and 51%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0, units[2]],
				res: { [Damage.damageType.heat]: 12.5 },
				mes: 'HEA Damage to robotic Target with NO SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0.1, units[2]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to robotic Target with SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0.1, units[2]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to robotic Target with SHIELD and 51%',
			},
			{
				// heat VS unique

				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0, units[3]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to unique Target with NO SHIELD and 51%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0, units[3]],
				res: { [Damage.damageType.heat]: 12.5 },
				mes: 'HEA Damage to unique Target with NO SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0.1, units[3]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to unique Target with SHIELD and 49%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0.1, units[3]],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to unique Target with SHIELD and 51%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.51, 0.1, {}],
				res: { [Damage.damageType.heat]: 10 },
				mes: 'HEA Damage to mech Target with SHIELD and 51%',
			},
			{
				dt: [{ [Damage.damageType.heat]: 10 }, 0.49, 0, {}],
				res: { [Damage.damageType.heat]: 12.5 },
				mes: 'HEA Damage to mech Target with NO SHIELD and 49%',
			},
		];
		makeTest(tests);
	});
	describe('Annihilation damage tests', () => {
		const tests: testBody[] = [
			{
				dt: [{ [Damage.damageType.antimatter]: 1 }, 1, 0, units[3]],
				res: { [Damage.damageType.antimatter]: 1.5 },
				mes: 'ANH Damage vs unique target with NO SHIELD and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.antimatter]: 1 }, 1, 0.59, units[3]],
				res: { [Damage.damageType.antimatter]: 1.25 },
				mes: 'ANH Damage vs unique target with SHIELDS and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.antimatter]: 1 }, 1, 0.59, units[2]],
				res: { [Damage.damageType.antimatter]: 1.25 },
				mes: 'ANH Damage vs random target with SHIELDS and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.antimatter]: 1 }, 1, 0, units[2]],
				res: { [Damage.damageType.antimatter]: 1 },
				mes: 'ANH Damage vs random target with NO SHIELD and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.antimatter]: 1 }, 0.49, 0, units[1]],
				res: { [Damage.damageType.antimatter]: 1 },
				mes: 'ANH Damage vs random target with NO SHIELD and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.antimatter]: 1 }, 0.49, 1, units[0]],
				res: { [Damage.damageType.antimatter]: 1.25 },
				mes: 'ANH Damage vs random target with SHIELDS and 49%hp',
			},
		];
		makeTest(tests);
	});
	describe('Thermodynamic Damage tests', () => {
		const tests: testBody[] = [
			{
				dt: [{ [Damage.damageType.thermodynamic]: 1 }, 1, 0, { ...units[0], ...units[1] }],
				res: { [Damage.damageType.thermodynamic]: 1.25 },
				mes: 'TRM Damage vs organic and massive target with NO SHIELD and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.thermodynamic]: 1 }, 0.49, 0, { ...units[0], ...units[1] }],
				res: { [Damage.damageType.thermodynamic]: 1.25 },
				mes: 'TRM Damage vs organic and massive target with NO SHIELD and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.thermodynamic]: 1 }, 0.49, 0, units[1]],
				res: { [Damage.damageType.thermodynamic]: 1.25 },
				mes: 'TRM Damage vs massive target with NO SHIELD and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.thermodynamic]: 1 }, 0.49, 0, units[0]],
				res: { [Damage.damageType.thermodynamic]: 1.25 },
				mes: 'TRM Damage vs organic target with NO SHIELD and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.thermodynamic]: 1 }, 0.49, 0.5, { ...units[0], ...units[1] }],
				res: { [Damage.damageType.thermodynamic]: 1 },
				mes: 'TRM Damage vs organic and massive target with SHIELD and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.thermodynamic]: 1 }, 1, 0, units[3]],
				res: { [Damage.damageType.thermodynamic]: 1 },
				mes: 'TRM Damage vs uniq target with NO SHIELD and 100%hp',
			},
		];
		makeTest(tests);
	});
	describe('Kinetic Damage  tests', () => {
		const tests: testBody[] = [
			{
				dt: [{ [Damage.damageType.kinetic]: 1 }, 1, 0, Object.assign({}, ...units)],
				res: { [Damage.damageType.kinetic]: 1 },
				mes: 'KIN Damage vs all types target with NO SHIELD and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.kinetic]: 1 }, 1, 0.1, Object.assign({}, ...units)],
				res: { [Damage.damageType.kinetic]: 1.5 },
				mes: 'KIN Damage vs all types target with SHIELDS and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.kinetic]: 1 }, 0.48, 0.1, Object.assign({}, ...units)],
				res: { [Damage.damageType.kinetic]: 1.5 },
				mes: 'KIN Damage vs all types target with SHIELDS and 48%hp',
			},
			{
				dt: [{ [Damage.damageType.kinetic]: 1 }, 0.48, 0.1, Object.assign({}, ...units)],
				res: { [Damage.damageType.kinetic]: 1.5 },
				mes: 'KIN Damage vs mech target with SHIELDS and 48%hp',
			},
		];
		makeTest(tests);
	});
	describe('Radiation Damage  tests', () => {
		const tests: testBody[] = [
			{
				dt: [{ [Damage.damageType.radiation]: 1 }, 1, 0, units[0]],
				res: { [Damage.damageType.radiation]: 1.25 },
				mes: 'RAD Damage vs organic target with NO SHIELDS and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.radiation]: 1 }, 1, 0, units[2]],
				res: { [Damage.damageType.radiation]: 1.25 },
				mes: 'RAD Damage vs Robotic Target with NO SHIELDS and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.radiation]: 1 }, 1, 0, Object.assign({}, ...units)],
				res: { [Damage.damageType.radiation]: 1.25 },
				mes: 'RAD Damage vs all types target with NO SHIELD and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.radiation]: 1 }, 0.49, 0.1, Object.assign({}, ...units)],
				res: { [Damage.damageType.radiation]: 1 },
				mes: 'RAD Damage vs all types target with SHIELDS and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.radiation]: 1 }, 0.49, 0.1, {}],
				res: { [Damage.damageType.radiation]: 1 },
				mes: 'RAD Damage vs mech target with SHIELDS and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.radiation]: 1 }, 0.49, 0.1, { ...units[1], ...units[3] }],
				res: { [Damage.damageType.radiation]: 1 },
				mes: 'RAD Damage vs massive and uniq target with SHIELDS and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.radiation]: 1 }, 0.49, 0, { ...units[1], ...units[3] }],
				res: { [Damage.damageType.radiation]: 1 },
				mes: 'RAD Damage vs massive and uniq target with NO SHIELDS and 49%hp',
			},
		];
		makeTest(tests);
	});
	describe('Electromagnetic Damage  tests', () => {
		const tests: testBody[] = [
			{
				dt: [{ [Damage.damageType.magnetic]: 1 }, 1, 0, units[2]],
				res: { [Damage.damageType.magnetic]: 1.25 },
				mes: 'EMG Damage vs robotic target with NO SHIELDS and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.magnetic]: 1 }, 1, 0, units[1]],
				res: { [Damage.damageType.magnetic]: 1.25 },
				mes: 'EMG Damage vs massive target with NO SHIELDS and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.magnetic]: 1 }, 1, 0, Object.assign({}, ...units)],
				res: { [Damage.damageType.magnetic]: 1.25 },
				mes: 'EMG Damage vs all types target with NO SHIELD and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.magnetic]: 1 }, 0.49, 1, { ...units[0], ...units[3] }],
				res: { [Damage.damageType.magnetic]: 1 },
				mes: 'EMG Damage vs bio and uniq target with SHIELD and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.magnetic]: 1 }, 0.49, 1, { ...units[1], ...units[2] }],
				res: { [Damage.damageType.magnetic]: 1 },
				mes: 'EMG Damage vs robo and massive target with SHIELD and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.magnetic]: 1 }, 0.49, 0.1, Object.assign({}, ...units)],
				res: { [Damage.damageType.magnetic]: 1 },
				mes: 'EMG Damage vs all types target with SHIELDS and 49%hp',
			},
		];
		makeTest(tests);
	});
	describe('Warp Damage  tests', () => {
		const tests: testBody[] = [
			{
				dt: [{ [Damage.damageType.warp]: 1 }, 1, 0, units[1]],
				res: { [Damage.damageType.warp]: 1.5 },
				mes: 'WRP Damage vs massive target with NO SHIELDS and 100%hp',
			},
			{
				dt: [{ [Damage.damageType.warp]: 1 }, 1, 0.1, units[1]],
				res: { [Damage.damageType.warp]: 1 },
				mes: 'WRP Damage vs massive target with SHIELDS and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.warp]: 1 }, 1, 0.1, { ...units[0], ...units[2], ...units[3] }],
				res: { [Damage.damageType.warp]: 1 },
				mes: 'WRP Damage vs no massive target with SHIELDS and 49%hp',
			},
			{
				dt: [{ [Damage.damageType.warp]: 1 }, 1, 0, { ...units[0], ...units[2], ...units[3] }],
				res: { [Damage.damageType.warp]: 1 },
				mes: 'WRP Damage vs no massive target with NO SHIELDS and 49%hp',
			},
		];
		makeTest(tests);
	});
	describe('Combination damage tests', () => {
		const dmg = Damage.DamageTemplate(1);
		const combinations: testBody[] = [
			{
				dt: [dmg, 1, 1, { [UnitChassis.targetType.organic]: true }],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Organic with shields and 100%HP',
			},
			{
				dt: [dmg, 1, 0, { [UnitChassis.targetType.organic]: true }],
				res: {
					...dmg,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
				},
				mes: 'Organic without shields and 100%HP',
			},
			{
				dt: [dmg, 0.48, 0, { [UnitChassis.targetType.organic]: true }],
				res: {
					...dmg,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Organic without shields and 49%HP',
			},
			{
				dt: [dmg, 1, 1, { [UnitChassis.targetType.robotic]: true }],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Robo with shields and 100%HP',
			},
			{
				dt: [dmg, 1, 0, { [UnitChassis.targetType.robotic]: true }],
				res: { ...dmg, [Damage.damageType.radiation]: 1.25, [Damage.damageType.magnetic]: 1.25 },
				mes: 'Robo without shields and 100%HP',
			},
			{
				dt: [dmg, 0.49, 0, { [UnitChassis.targetType.robotic]: true }],
				res: {
					...dmg,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Robo without shields and 49%HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Organic and robo with shields and 100%HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
				},
				mes: 'Organic and robo without shields and 100%HP',
			},
			{
				dt: [
					dmg,
					0.49,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Organic and robo without shields and 49%HP',
			},
			{
				dt: [dmg, 1, 1, { [UnitChassis.targetType.massive]: true }],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Massive with shields and 100%HP',
			},
			{
				dt: [dmg, 1, 0, { [UnitChassis.targetType.massive]: true }],
				res: {
					...dmg,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.thermodynamic]: 1.25,
				},
				mes: 'Massive without shields and 100%HP',
			},
			{
				dt: [dmg, 0.48, 0, { [UnitChassis.targetType.massive]: true }],
				res: {
					...dmg,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.heat]: 1.25,
					[Damage.damageType.thermodynamic]: 1.25,
				},
				mes: 'Massive without shields and 48%HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Massive and Organic with shields and 100%HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.radiation]: 1.25,
				},
				mes: 'Massive and Organic without shields and 100%HP',
			},
			{
				dt: [
					dmg,
					0.49,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.heat]: 1.25,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.radiation]: 1.25,
				},
				mes: 'Massive and Organic without shields and 49%HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Robotic and massive with shields and 100%',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.warp]: 1.5,
				},
				mes: 'Robotic and massive without shields and 100%',
			},
			{
				dt: [
					dmg,
					0.48,
					0,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Robotic and massive without shields and 48%',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Organic/Robo/Massive with shields and 100%HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.warp]: 1.5,
				},
				mes: 'Organic/Robo/Massive without shields and 100%HP',
			},
			{
				dt: [
					dmg,
					0.48,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Organic/Robo/Massive without shields and 48%HP',
			},
			{
				dt: [dmg, 1, 1, { [UnitChassis.targetType.unique]: true }],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Uniq with shields and 100%HP',
			},
			{
				dt: [dmg, 1, 0, { [UnitChassis.targetType.unique]: true }],
				res: { ...dmg, [Damage.damageType.antimatter]: 1.5 },
				mes: 'Uniq without shields and 100%HP',
			},
			{
				dt: [dmg, 0.48, 0, { [UnitChassis.targetType.unique]: true }],
				res: { ...dmg, [Damage.damageType.antimatter]: 1.5, [Damage.damageType.heat]: 1.25 },
				mes: 'Uniq without shields and 48%HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Uniq and organic with shields and 100% HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
				},
				mes: 'Uniq and organic without shields and 100% HP',
			},
			{
				dt: [
					dmg,
					0.48,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Uniq and organic without shields and 48% HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Robo and uniq with shields and 100% HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
				},
				mes: 'Robo and uniq without shields and 100% HP',
			},
			{
				dt: [
					dmg,
					0.48,
					0,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Robo and uniq without shields and 48% HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Organic/Robo/Uniq with shields and 100%',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
				},
				mes: 'Organic/Robo/Uniq without shields and 100%',
			},
			{
				dt: [
					dmg,
					0.49,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Organic/Robo/Uniq without shields and 49%',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Massive and uniq with shields 100%HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.warp]: 1.5,
				},
				mes: 'Massive and uniq without shields 100%HP',
			},
			{
				dt: [
					dmg,
					0.48,
					0,
					{
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Massive and uniq without shields 48%HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Organic/Massive/Uniq with shields and 100%HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.warp]: 1.5,
				},
				mes: 'Organic/Massive/Uniq without shields and 100%HP',
			},
			{
				dt: [
					dmg,
					0.49,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Organic/Massive/Uniq without shields and 49%HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'Robo/Uniq/Massive with shields and 100%HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.antimatter]: 1.5,
				},
				mes: 'Robo/Uniq/Massive without shields and 100%HP',
			},
			{
				dt: [
					dmg,
					0.45,
					0,
					{
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'Robo/Uniq/Massive without shields and 45%HP',
			},
			{
				dt: [
					dmg,
					1,
					1,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'All types with shields and 100%HP',
			},
			{
				dt: [
					dmg,
					1,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.warp]: 1.5,
				},
				mes: 'All types without shields and 100%HP',
			},
			{
				dt: [
					dmg,
					0.48,
					0,
					{
						[UnitChassis.targetType.organic]: true,
						[UnitChassis.targetType.robotic]: true,
						[UnitChassis.targetType.massive]: true,
						[UnitChassis.targetType.unique]: true,
					},
				],
				res: {
					...dmg,
					[Damage.damageType.thermodynamic]: 1.25,
					[Damage.damageType.biological]: 1.5,
					[Damage.damageType.magnetic]: 1.25,
					[Damage.damageType.radiation]: 1.25,
					[Damage.damageType.antimatter]: 1.5,
					[Damage.damageType.warp]: 1.5,
					[Damage.damageType.heat]: 1.25,
				},
				mes: 'All types without shields and 48%HP',
			},
			{
				dt: [dmg, 1, 1, {}],
				res: { ...dmg, [Damage.damageType.kinetic]: 1.5, [Damage.damageType.antimatter]: 1.25 },
				mes: 'NoType unit with shields and 100%HP',
			},
			{
				dt: [dmg, 1, 0, {}],
				res: { ...dmg },
				mes: 'NoType unit without shields and 100%HP',
			},
			{
				dt: [dmg, 0.48, 0, {}],
				res: { ...dmg, [Damage.damageType.heat]: 1.25 },
				mes: 'NoType unit without shields and 48%HP',
			},
		];
		makeTest(combinations);
	});
});

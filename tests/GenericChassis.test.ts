import { expect } from 'chai';
import 'mocha';
import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../definitions/core/DI/injections';
import GenericFactory from '../definitions/orbital/factories/GenericFactory';
import UnitAttack from '../definitions/tactical/damage/attack';
import CharonChassis from '../definitions/technologies/chassis/all/Fortress/Charon.59002';
import BruteChassis from '../definitions/technologies/chassis/all/Infantry/Brute.10005';
import { GenericInfantryActions } from '../definitions/technologies/chassis/all/Infantry/GenericInfantry';
import ConvictorChassis from '../definitions/technologies/chassis/all/LAV/Convictor.37003';
import ChassisManager from '../definitions/technologies/chassis/ChassisManager';
import GenericChassis from '../definitions/technologies/chassis/genericChassis';
import UnitChassis, { IChassisMeta } from '../definitions/technologies/types/chassis';
import UnitDefense from '../definitions/tactical/damage/defense';
import UnitActions from '../definitions/tactical/units/actions/types';

const Precision = 10e-2;

const sampleProperties: UnitChassis.TChassisProps = {
	baseCost: 5,
	hitPoints: 25,
	equipmentSlots: 1,
	dodge: {
		default: 0.2,
	},
	speed: 3,
	actions: [GenericInfantryActions],
	scanRange: 5,
	defenseFlags: {
		[UnitDefense.defenseFlags.boosters]: true,
	},
};

const alteredProps: Partial<UnitChassis.TChassisProps> = {
	dodge: {
		...sampleProperties.dodge,
		[UnitAttack.deliveryType.missile]: 0.5,
	},
	baseCost: 10,
	equipmentSlots: 2,
	speed: 5,
};

const expectedProperties = {
	...sampleProperties,
	...alteredProps,
};

class SampleChassis extends GenericChassis {
	public baseCost: number = this.baseCost + 5;
	public speed: number = this.speed + 2;
	public equipmentSlots: number = this.equipmentSlots + 1;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.missile]: 0.5,
	};
}

const SampleMeta2 = {
	class: UnitChassis.chassisClass.bomber,
} as IChassisMeta;
const SampleExtends = new SampleChassis(sampleProperties, SampleMeta2);

const SampleMeta1 = {
	class: UnitChassis.chassisClass.launcherpad,
} as IChassisMeta;
const SampleInstance = new GenericChassis(sampleProperties, SampleMeta1);

const testFactory = new ChassisManager();

const samplePresetEquipment = new CharonChassis(null, testFactory.getItemMeta(CharonChassis.id));

const samplePresetId = samplePresetEquipment.static.id;
const samplePresetId2 = ConvictorChassis.id;
const serializedPresetEquipment = _.omit(samplePresetEquipment.serialize(), 'instanceId');

describe('GenericChassis', () => {
	describe('constructor', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleInstance).to.deep.include(sampleProperties);
		});
		it('ID starts with Type Descriptor', () => {
			const c = new GenericChassis(sampleProperties, SampleMeta1);
			expect(c.serialize())
				.to.have.property('instanceId')
				.that.match(new RegExp(`^${c.getDescriptor(DIEntityDescriptors[DIInjectableCollectibles.chassis])}`));
		});
		it('correctly passes properties to SetProps', () => {
			const c = new GenericChassis(sampleProperties, SampleMeta1);
			c.setProps(alteredProps);
			expect(c).to.deep.include(expectedProperties);
		});
		it('correctly passes slot through meta', () => {
			expect(SampleInstance).property('meta').to.include(SampleMeta1);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleInstance).property('static').to.include({ id: GenericChassis.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleInstance.serialize();
				expect(s).to.deep.include(sampleProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(GenericChassis.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
	describe('Extension class', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleExtends).to.deep.include(expectedProperties);
		});
		it('correctly passes slot through meta', () => {
			expect(SampleExtends).property('meta').to.include(SampleMeta2);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleExtends).property('static').to.include({ id: SampleChassis.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleExtends.serialize();
				expect(s).to.deep.include(expectedProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(SampleChassis.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
});

describe('ChassisManager', () => {
	it('Fills correctly', () => {
		testFactory.fill();
		const all = testFactory.getAll();
		expect(Object.keys(all)).to.have.property('length').to.be.greaterThan(0);
		expect(Object.keys(all)).to.include(samplePresetId.toString());
	});

	it('Correctly returns a constructor', () => {
		const c = testFactory.get(samplePresetId);
		expect(c).to.be.equal(samplePresetEquipment.static);
	});
	it('Correctly instantiates without props and assigns meta', () => {
		const c = testFactory.instantiate(null, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(serializedPresetEquipment);
		expect(c).to.have.property('meta').to.include({
			class: samplePresetEquipment.static.type,
		});
	});
	it('Correctly instantiates with props', () => {
		const c = testFactory.instantiate(sampleProperties, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(sampleProperties);
		expect(s).to.have.property('id').to.equal(samplePresetId);
	});
	it('Correctly unserializes and assigns a meta', () => {
		const samplePresetChassis2 = testFactory.instantiate(sampleProperties, samplePresetId2);
		const serializedPresetEquipment2 = samplePresetChassis2.serialize();
		const sampleInstanceId = 'unserialized';
		serializedPresetEquipment2.instanceId = sampleInstanceId;
		const c = testFactory.unserialize(serializedPresetEquipment2);
		expect(c).to.deep.include(sampleProperties);
		expect(c).to.have.property('meta').to.include({
			class: samplePresetChassis2.static.type,
		});
		expect(c).to.have.property('instanceId').to.equal(sampleInstanceId);
	});
	it('Correctly clones and assigns a meta', () => {
		const samplePresetEquipment2 = testFactory.clone(samplePresetEquipment);
		const serializedPresetEquipment2 = samplePresetEquipment2.serialize();
		expect(_.omit(serializedPresetEquipment2, 'instanceId')).to.deep.include(
			_.omit(serializedPresetEquipment, 'instanceId'),
		);
		expect(samplePresetEquipment2).to.have.property('meta').to.include({
			class: samplePresetEquipment.static.type,
		});
		expect(samplePresetEquipment2)
			.to.have.property('instanceId')
			.to.not.equal(samplePresetEquipment.getInstanceId());
	});
});

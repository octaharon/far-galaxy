import { expect } from 'chai';
import 'mocha';
import { stackWeaponGroupModifiers } from '../definitions/technologies/weapons/WeaponManager';
import { StackWGTestCases } from './fixtures/weaponGroupModifiers.cases';

describe('Weapon Group Modifiers', () => {
	describe('Stacking', () => {
		StackWGTestCases.forEach((t) => {
			it(t.title || `returns ${JSON.stringify(t.output)} with "${JSON.stringify(t.args)}"`, () => {
				const c = stackWeaponGroupModifiers(...t.args);
				expect(c).to.be.an('object', "Doesn't return an object");
				Object.keys(t.output).forEach((objKey) =>
					expect(c).to.have.deep.property(
						objKey,
						{
							...t.output[objKey],
						},
						`Property ${objKey} doesn't match`,
					),
				);
			});
		});
	});
});

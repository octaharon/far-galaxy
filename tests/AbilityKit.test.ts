import chai from 'chai';
import spies from 'chai-spies';
import { describe } from 'mocha';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';
import { DIInjectableCollectibles, DIInjectableSerializables } from '../definitions/core/DI/injections';
import BasicMaths from '../definitions/maths/BasicMaths';
import { AbilityKit } from '../definitions/tactical/abilities/AbilityKit';
import IrradiateAbility from '../definitions/tactical/abilities/all/Orbital/Irradiate.45008';
import SVBlindAbility from '../definitions/tactical/abilities/all/ScienceVessel/SVBlindAbility.99906';
import SVNullifyAbility from '../definitions/tactical/abilities/all/ScienceVessel/SVNullifyAbility.99904';
import Abilities from '../definitions/tactical/abilities/types';
import IAbility = Abilities.IAbility;
import IAbilityKit = Abilities.IAbilityKit;
import _ from 'underscore';

chai.use(spies);
const { expect, spy } = chai;

DIContainer.inject(DIPresetDefault);

const provider = DIContainer.getProvider(DIInjectableCollectibles.ability);
const unitOwner = DIContainer.getProvider(DIInjectableSerializables.unit).instantiate(null);
const baseOwner = DIContainer.getProvider(DIInjectableSerializables.base).instantiate(null);

describe('AbilityKit', () => {
	let ability: IAbility<any>;
	let instance: IAbilityKit;
	let props: Abilities.TAbilityKitProps;
	beforeEach(() => {
		ability = provider.instantiate(null, SVNullifyAbility.id);
		props = {
			modifiers: {
				abilityCooldownModifier: [
					{
						bias: -1,
						min: 1,
						minGuard: 1,
					},
				],
			},
			abilities: {
				[SVNullifyAbility.id]: ability.serialize(),
				[IrradiateAbility.id]: provider.instantiate(null, IrradiateAbility.id).serialize(),
			},
			order: [IrradiateAbility.id, SVNullifyAbility.id],
		};
		instance = new AbilityKit(unitOwner, props);
	});

	describe('constructor', () => {
		it('creates null object', () => {
			const t = new AbilityKit(null);
			expect(t.save()).to.deep.include({
				abilities: {},
				modifiers: {},
				order: [],
			});
		});
		it('creates object from params', () => {
			expect(instance.save()).to.deep.include(props);
			expect(instance).to.deep.include(props);
		});
		it('inherits owner', () => {
			expect(instance.owner).to.equal(unitOwner);
			const t = new AbilityKit(baseOwner);
			expect(t.owner).to.equal(baseOwner);
		});
	});

	describe('ability set', () => {
		it(`allows to add abilities`, () => {
			instance.addAbility(SVBlindAbility.id);
			expect(instance.total).to.equal(3);
			expect(instance.getAbility(SVBlindAbility.id)?.serialize()).to.deep.include({
				id: SVBlindAbility.id,
			});
			expect(instance.save().order).to.deep.equal([...props.order, SVBlindAbility.id]);
		});
		it(`allows to remove abilities`, () => {
			instance.removeAbility(SVNullifyAbility.id);
			expect(instance.total).to.equal(1);
			expect(instance.getAbility(SVNullifyAbility.id)).to.be.null;
			expect(instance.save().order).to.deep.equal([IrradiateAbility.id]);
		});
		it(`keeps abilities unique`, () => {
			const t = instance.save();
			instance.addAbility(SVNullifyAbility.id);
			instance.addAbility(IrradiateAbility.id);
			instance.addAbility(SVNullifyAbility.id);
			expect(instance.total).to.equal(2);
			expect(t).to.deep.equal(instance.save());
		});
		it('applies saved modifier to a newly added ability', () => {
			const baseAbility = new SVBlindAbility(null);
			instance.applyModifier({
				abilityRangeModifier: [
					{
						bias: 2,
					},
				],
			});
			instance.addAbility(SVBlindAbility.id);
			expect(instance.save()?.abilities?.[SVBlindAbility.id]?.cooldown).to.equal(
				BasicMaths.applyModifier(baseAbility.cooldown, props.modifiers.abilityCooldownModifier[0]),
			);
			expect(instance.save()?.abilities?.[SVBlindAbility.id]?.baseRange).to.equal(
				BasicMaths.applyModifier(baseAbility.baseRange, { bias: 2 }),
			);
			expect(instance.getAbility(SVBlindAbility.id)?.cooldownLeft).to.equal(0);
		});
	});

	describe('ability order', () => {
		it('getOrderedAbilities', () => {
			expect(instance.getOrderedAbilities().map((v) => v.serialize())).to.deep.equal([
				instance.getAbility(instance.order[0]).serialize(),
				instance.getAbility(instance.order[1]).serialize(),
			]);
		});
		it('setAbilityOrder, switch case', () => {
			instance.setAbilityOrder(instance.order[0], 1);
			expect(instance.getOrderedAbilities().map((v) => v.serialize())).to.deep.equal([
				instance.getAbility(SVNullifyAbility.id).serialize(),
				instance.getAbility(IrradiateAbility.id).serialize(),
			]);
		});
		it('setAbilityOrder, invalid id case', () => {
			instance.setAbilityOrder(1234567, 1);
			expect(instance.getOrderedAbilities().map((v) => v.serialize())).to.deep.equal([
				instance.getAbility(IrradiateAbility.id).serialize(),
				instance.getAbility(SVNullifyAbility.id).serialize(),
			]);
		});
		it('setAbilityOrder, prepend case', () => {
			instance.setAbilityOrder(SVNullifyAbility.id, 0);
			expect(instance.getOrderedAbilities().map((v) => v.serialize())).to.deep.equal([
				instance.getAbility(SVNullifyAbility.id).serialize(),
				instance.getAbility(IrradiateAbility.id).serialize(),
			]);
		});
		it('setAbilityOrder, remove case', () => {
			instance.addAbility(SVBlindAbility.id);
			instance.removeAbility(SVNullifyAbility.id);
			expect(instance.getOrderedAbilities().map((v) => v.serialize())).to.deep.equal([
				instance.getAbility(IrradiateAbility.id).serialize(),
				instance.getAbility(SVBlindAbility.id).serialize(),
			]);
		});
		it('getStableSortedAbilities actually keeps order', () => {
			instance.addAbility(SVBlindAbility.id);
			const oldCopy = instance.getStableSortedAbilities().map((v) => v.serialize());
			instance.setAbilityOrder(SVBlindAbility.id, 1);
			expect(instance.getOrderedAbilities().map((v) => v.serialize())).to.deep.equal([
				instance.getAbility(IrradiateAbility.id).serialize(),
				instance.getAbility(SVBlindAbility.id).serialize(),
				instance.getAbility(SVNullifyAbility.id).serialize(),
			]);
			instance.setAbilityOrder(SVNullifyAbility.id, 2);
			expect(instance.getStableSortedAbilities().map((v) => v.serialize())).to.deep.equal(oldCopy);
		});
		it('total count matches', () => {
			instance.save();
			expect(instance.total).to.equal(instance.getOrderedAbilities().length);
			expect(instance.total).to.equal(instance.getStableSortedAbilities().length);
			expect(instance.total).to.equal(Object.keys(instance.abilities).length);
			expect(instance.total).to.equal(2);
			instance
				.addAbility(SVBlindAbility.id)
				.removeAbility(SVNullifyAbility.id)
				.removeAbility(IrradiateAbility.id);
			expect(instance.total).to.equal(instance.getOrderedAbilities().length);
			expect(instance.total).to.equal(instance.getStableSortedAbilities().length);
			expect(instance.total).to.equal(Object.keys(instance.abilities).length);
			expect(instance.total).to.equal(1);
		});
	});

	describe('setAbilityCooldown', () => {
		it(`sets all abilities cooldown to zero, when called without params`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.setAbilityCooldown();
			expect(Math.max(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.equal(0);
		});
		it(`sets all abilities cooldown a given value, when called with 1 param`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 1)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.setAbilityCooldown(1);
			expect(Math.max(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.equal(1);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThanOrEqual(
				0,
			);
		});
		it(`does nothing when called with an invalid abilityId`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.setAbilityCooldown(1, 12345678);
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`affects only a given ability if abilityId is provided`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.setAbilityCooldown(1, SVNullifyAbility.id);
			copy.abilities[SVNullifyAbility.id].cooldownLeft = 1;
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`never goes below zero`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.setAbilityCooldown(-100);
			Object.values(copy.abilities).forEach((a) => (a.cooldownLeft = 0));
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`never applies to a disabled ability, when called with 2 params`, () => {
			instance.setAbilityEnabled(false, SVNullifyAbility.id);
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.setAbilityCooldown(15, SVNullifyAbility.id);
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`applies to all but disabled abilities, when called with less than 2 params`, () => {
			instance.setAbilityEnabled(false, SVNullifyAbility.id);
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.setAbilityCooldown();
			copy.abilities[IrradiateAbility.id].cooldownLeft = 0;
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
	});

	describe('addAbilityCooldown', () => {
		it(`reduces all cooldowns, when called without params`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 2 + 1)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.addAbilityCooldown();
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThanOrEqual(
				0,
			);
			expect(Math.max(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.lessThanOrEqual(2);
		});
		it(`reduces all abilities cooldown by a given value, when called with 1 param`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 1)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.addAbilityCooldown(2);
			Object.values(copy.abilities).forEach((a) => (a.cooldownLeft += 2));
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`does nothing when called with an invalid abilityId`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.addAbilityCooldown(-6, 12345678);
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`affects only a given ability if abilityId is provided`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 5)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.addAbilityCooldown(-3, SVNullifyAbility.id);
			copy.abilities[SVNullifyAbility.id].cooldownLeft -= 3;
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`never goes below zero`, () => {
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.addAbilityCooldown(-100);
			Object.values(copy.abilities).forEach((a) => (a.cooldownLeft = 0));
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`never applies to a disabled ability, when called with 2 params`, () => {
			instance.setAbilityEnabled(false, SVNullifyAbility.id);
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.addAbilityCooldown(45, SVNullifyAbility.id);
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
		it(`applies to all but disabled abilities, when called with less tham 2 params`, () => {
			instance.setAbilityEnabled(false, SVNullifyAbility.id);
			const copy = instance.save();
			Object.values(copy.abilities).forEach((ab) => (ab.cooldownLeft = Math.round(Math.random() * 4 + 2)));
			const instanceCopy = new AbilityKit(unitOwner, copy);
			expect(Math.min(...instanceCopy.getOrderedAbilities().map((v) => v.cooldownLeft))).to.be.greaterThan(0);
			instanceCopy.addAbilityCooldown(-15);
			copy.abilities[IrradiateAbility.id].cooldownLeft = 0;
			expect(instanceCopy.save()).to.deep.equal(copy);
		});
	});
	describe('setAbilityEnabled', () => {
		it('enables all abilities when called without params', () => {
			const copy = instance.save();
			instance.getOrderedAbilities().forEach((a) => (a.usable = false));
			instance.setAbilityEnabled();
			expect(instance.save()).to.deep.equal(copy);
		});
		it("disables all abilities when called with 'false'", () => {
			const copy = instance.save();
			instance.setAbilityEnabled(false);
			Object.values(copy.abilities).forEach((a) => (a.usable = false));
			expect(instance.save()).to.deep.equal(copy);
		});
		it(`does nothing when called with invalid abilityId`, () => {
			instance.getOrderedAbilities().forEach((a) => (a.usable = false));
			const copy = instance.save();
			instance.setAbilityEnabled(true, 12345678);
			expect(instance.save()).to.deep.equal(copy);
		});
		it(`updates only a specified ability when provided with a valid abilityId`, () => {
			instance.getOrderedAbilities().forEach((a) => (a.usable = false));
			const copy = instance.save();
			instance.setAbilityEnabled(true, IrradiateAbility.id);
			copy.abilities[IrradiateAbility.id].usable = true;
			expect(instance.save()).to.deep.equal(copy);
		});
	});
});

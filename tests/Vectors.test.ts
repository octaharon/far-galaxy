import { expect } from 'chai';
import 'mocha';
import { GeometricPrecision } from '../definitions/maths/constants';
import Vectors from '../definitions/maths/Vectors';

describe('Vectors', () => {
	describe('dotProduct', () => {
		it('returns positive value for non-zero vector', () => {
			const s = { x: -100, y: 10 };
			const funcForTest = Vectors.dotProduct(s, s);

			expect(funcForTest).to.be.greaterThan(0);
		});
		it('returns 0 for a zero vector', () => {
			const s = { x: 0, y: 0 };
			const funcForTest = Vectors.dotProduct(s, s);

			expect(funcForTest).to.be.equal(0);
		});
		it('the scalar product of a vector by itself is equal to the square of its modulus', () => {
			const s = { x: 2, y: -4 };
			const funcForTest = Vectors.dotProduct(s, s);

			expect(funcForTest).to.be.closeTo(Vectors.module()(s) ** 2, GeometricPrecision);
		});
		it('associativity', () => {
			const s = { x: 2, y: -4 };
			const c = { x: 5, y: -7 };
			const funcForTest = Vectors.dotProduct(s, c);
			const funcForTest2 = Vectors.dotProduct(c, s);

			expect(funcForTest).to.be.closeTo(funcForTest2, GeometricPrecision);
		});
		it('distribution law', () => {
			const a = { x: 2.991, y: -4.09101 };
			const b = { x: 5.1, y: -7.88 };
			const c = { x: -66, y: -7.07 };
			const ab = Vectors.add(a, b);

			expect(Vectors.dotProduct(ab, c)).to.be.closeTo(
				Vectors.dotProduct(a, c) + Vectors.dotProduct(b, c),
				GeometricPrecision,
			);
		});
		it('commutativity', () => {
			const a = { x: 2.991, y: -4.09101 };
			const b = { x: 5.1, y: -7.88 };
			const k = Math.random() * 20 + 1;
			const ak = Vectors.multiply(a, k);

			expect(Vectors.dotProduct(ak, b)).to.be.closeTo(k * Vectors.dotProduct(a, b), GeometricPrecision);
		});
		it('scalar product of two nonzero vectors is zero, then these vectors are perpendicular', () => {
			const x = Math.random() * 10 + 1;
			const x2 = Math.random() * 10 + 1;
			const a = { x, y: x };
			const b = { x: x2 * -1, y: x2 };

			expect(Vectors.dotProduct(a, b)).to.be.closeTo(0, GeometricPrecision);
		});
	});
});

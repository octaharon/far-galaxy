import { expect } from 'chai';
import 'mocha';
import BasicMaths from '../definitions/maths/BasicMaths';
import { ApplyModifierTestCases } from './fixtures/applyModifier.cases';
import { StackAbsorptionModifiersTestCases } from './fixtures/stackAbsorptionModifiers.cases';
import { StackModifiersTestCases } from './fixtures/stackModifiers.cases';
import { assertModifier, Precision } from './helpers';

describe('BasicMaths - modifiers', () => {
	describe('stackModifiers', () => {
		StackModifiersTestCases.forEach((t) => {
			it(
				t.title ||
					`returns ${BasicMaths.describeModifier(t.output, true, true)} with "${JSON.stringify(t.args)}"`,
				() => {
					const c = BasicMaths.stackModifiers(...t.args);
					assertModifier(c, t.output);
				},
			);
		});
	});
	describe('stackAbsorptionModifiers', () => {
		StackAbsorptionModifiersTestCases.forEach((t) => {
			it(
				t.title ||
					`returns ${BasicMaths.describeModifier(t.output, true, true)} with "${JSON.stringify(t.args)}"`,
				() => {
					const c = BasicMaths.stackAbsorptionModifiers(...t.args);
					assertModifier(c, t.output);
				},
			);
		});
	});
	describe('applyModifier', () => {
		ApplyModifierTestCases.forEach(function (t) {
			it(t.title || `returns ${t.output} with "${JSON.stringify(t.args)}"`, () => {
				const c = BasicMaths.applyModifier(...t.args);
				expect(c).to.be.an('number', "Doesn't return a number");
				expect(c).to.be.closeTo(t.output, Precision, `doesn't equal ${t.output}`);
			});
		});
	});
});

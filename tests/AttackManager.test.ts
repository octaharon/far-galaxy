import chai from 'chai';
import spies from 'chai-spies';
import { describe } from 'mocha';
import _ from 'underscore';
import { cloneObject } from '../definitions/core/dataTransformTools';
import { ArithmeticPrecision } from '../definitions/maths/constants';
import Distributions from '../definitions/maths/Distributions';
import UnitAttack from '../definitions/tactical/damage/attack';
import AttackManager from '../definitions/tactical/damage/AttackManager';
import { COMBAT_MAX_ATTACKS_PER_TURN, COMBAT_MIN_ATTACKS_PER_TURN } from '../definitions/tactical/damage/constants';
import Damage from '../definitions/tactical/damage/damage';
import { hitChanceAdjustmentCases } from './fixtures/accuracy.cases';
import { fpsLength } from './helpers';
import TDamageTypesArray = Damage.TDamageTypesArray;

chai.use(spies);
const { expect, spy } = chai;
describe('AttackManager', () => {
	describe('adjustAccuracyToMovement', () => {
		hitChanceAdjustmentCases.forEach(({ title, params, out }) => {
			it(title || `returns ${out} with ${JSON.stringify(params)}`, () => {
				expect(AttackManager.adjustAccuracyToMovement(...params)).to.be.closeTo(out, ArithmeticPrecision);
			});
		});
	});
	describe('getNumberOfAttacks', function () {
		const samples = 25000;
		const expectedRate = 10000; // expect 10000 operations per frame
		this.slow((fpsLength * samples) / expectedRate);
		const sampleRates = [NaN, -Infinity, 0, 0.01, 0.1, 0.2, 0.4, 1, 2, 5, 20, 150, 1e9];
		let prevAvg = -Infinity;
		sampleRates.forEach((sampleRate) => {
			describe(`for Rate equals "${sampleRate}"`, () => {
				const data: number[] = new Array(samples).fill(null);
				const stat = { avg: 0, min: Infinity, max: -Infinity, sum: 0 };
				it(`runs at least ${expectedRate} times per frame`, () => {
					for (let i = 0; i < samples; i++) {
						const v = AttackManager.getNumberOfAttacks(sampleRate);
						if (v > stat.max) stat.max = v;
						if (v < stat.min) stat.min = v;
						stat.sum += v;
						data[i] = v;
					}
					stat.avg = stat.sum / samples;
				});
				if (Number.isFinite(sampleRate) && sampleRate >= 0.1) {
					it('for rate>=0.1 total sum of attacks is more than 1 per roll', () => {
						expect(stat.sum).to.be.greaterThanOrEqual(samples);
					});
				}
				if (Number.isFinite(sampleRate))
					it('progressively increases average as rate increases', () => {
						expect(stat.avg).to.be.greaterThanOrEqual(prevAvg);
						prevAvg = stat.avg;
					});
				it('never returns a falsy value', () => {
					expect(data.filter((v) => !v).length).to.be.equal(0);
				});
				it(`never returns less than COMBAT_MIN_ATTACKS_PER_TURN=${COMBAT_MIN_ATTACKS_PER_TURN}`, () => {
					expect(stat.min).to.be.greaterThanOrEqual(COMBAT_MIN_ATTACKS_PER_TURN);
				});
				it(`never returns greater more than COMBAT_MAX_ATTACKS_PER_TURN=${COMBAT_MAX_ATTACKS_PER_TURN}`, () => {
					expect(stat.max).to.be.lessThanOrEqual(COMBAT_MAX_ATTACKS_PER_TURN);
				});
			});
		});
	});
	describe('getAttackDamage', function () {
		const damageSamplesNumber = 10;
		const damageMin = 1;
		const damageMax = 200;
		const samples = 5000;
		const expectedRate = 1000; // expect 1000 operations per frame
		const distributionArray = Object.keys(Distributions.DistributionOptions);
		const damageSamples = new Array(damageSamplesNumber).fill(null).map(() => {
			const chance = Math.random();
			const damageTypes = Damage.TDamageTypesArray.slice()
				.sort(() => Math.random() - 0.5)
				.slice(0, Math.ceil(Math.random() * TDamageTypesArray.length));
			const damageRoll = damageTypes.reduce(
				(obj, dt) =>
					Object.assign(obj, {
						[dt]: {
							min: Math.round((Math.random() * (damageMax - damageMin)) / 2 + damageMin),
							max: Math.round(
								(Math.random() * (damageMax - damageMin)) / 2 + (damageMax - damageMin) / 2 + damageMin,
							),
							distribution: distributionArray.slice().sort(() => Math.random() - 0.5)[0],
						},
					}),
				{} as Damage.TDamageDefinition,
			);
			const delivery = UnitAttack.TDeliveryTypeArray.slice().sort(() => Math.random() - 0.5)[0];
			const flags = {
				[UnitAttack.attackFlags.Disruptive]: Math.random() > 0.5,
				[UnitAttack.attackFlags.Selective]: Math.random() > 0.5,
				[UnitAttack.attackFlags.Penetrative]: Math.random() > 0.5,
			} as UnitAttack.TAttackFlags;
			switch (true) {
				case chance > 0.9:
					return {
						delivery,
						duration: Math.round(Math.random() * 5 + 1),
						[UnitAttack.attackFlags.DoT]: true,
						...flags,
						...damageRoll,
					} as UnitAttack.TDOTAttackDefinition;
				case chance > 0.75:
					return {
						delivery,
						[UnitAttack.attackFlags.Deferred]: true,
						...flags,
						...damageRoll,
					} as UnitAttack.TDeferredAttackDefinition;
				case chance > 0.5:
					return {
						delivery,
						[UnitAttack.attackFlags.AoE]: true,
						radius: Math.random() * 2 + 0.5,
						aoeType: Object.values(UnitAttack.TAoEDamageSplashTypes)[
							Math.floor(Object.values(UnitAttack.TAoEDamageSplashTypes).length * Math.random())
						],
						magnitude: Math.random() * 0.8 + 0.2,
						...flags,
						...damageRoll,
					} as UnitAttack.TAOEAttackDefinition;
				default:
					return {
						delivery,
						...damageRoll,
					} as UnitAttack.TDirectAttackDefinition;
			}
		});
		this.slow((fpsLength * samples) / expectedRate);
		damageSamples.forEach((damageDefinition) => {
			const data: UnitAttack.TAttackUnit[] = new Array(samples).fill(null);
			const clone = cloneObject(damageDefinition);
			describe(`With Definition of ${JSON.stringify(damageDefinition)}`, () => {
				it(`runs at least ${expectedRate} times per frame`, () => {
					for (let i = 0; i < samples; i++) {
						const v = AttackManager.getAttackDamage(damageDefinition);
						data[i] = v;
					}
				});
				it(`provides values within boundaries for all damage types`, () => {
					Damage.TDamageTypesArray.forEach((dt) => {
						if (!damageDefinition[dt]) {
							expect(data.filter((d) => !!d[dt]).length).to.be.equal(0);
						} else {
							expect(Math.min(...data.map((v) => v[dt] || 0))).to.be.greaterThanOrEqual(
								damageDefinition[dt].min,
							);
							expect(Math.max(...data.map((v) => v[dt] || 0))).to.be.lessThanOrEqual(
								damageDefinition[dt].max,
							);
						}
					});
				});
				it('copies non-damage related values', () => {
					const ref = _.omit(damageDefinition, ...Damage.TDamageTypesArray);
					data.forEach((d) => expect(_.omit(d, ...Damage.TDamageTypesArray)).to.be.deep.equal(ref));
				});
				it(`does not mutate source object`, () => {
					expect(damageDefinition).to.be.deep.equal(clone);
				});
			});
		});
	});
	describe('getAttackFlagsGenericFromAttack', () => {
		it('null object', () => {
			expect(AttackManager.getAttackFlagsGenericFromAttack(undefined)).to.be.deep.equal({});
		});
		it('minimal viable object', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.drone,
				}),
			).to.be.deep.equal({});
		});
		it('full attack definition without flags', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.drone,
					[Damage.damageType.kinetic]: {
						min: 15,
						max: 25,
						distribution: 'Uniform',
					},
					[Damage.damageType.magnetic]: {
						min: 10,
						max: 20,
						distribution: 'Bell',
					},
				}),
			).to.be.deep.equal({});
		});
		it('full damage unit without flags', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.drone,
					[Damage.damageType.kinetic]: 20,
					[Damage.damageType.magnetic]: 45,
				}),
			).to.be.deep.equal({});
		});
		it('Aoe Damage unit', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.missile,
					[UnitAttack.attackFlags.AoE]: true,
					[UnitAttack.attackFlags.Deferred]: false,
					[UnitAttack.attackFlags.DoT]: false,
					[Damage.damageType.kinetic]: 10,
					[Damage.damageType.magnetic]: 35,
				}),
			).to.be.deep.equal({
				[UnitAttack.attackFlags.AoE]: true,
			});
		});
		it('DoT Damage unit', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.missile,
					duration: 5,
					[UnitAttack.attackFlags.DoT]: true,
					[UnitAttack.attackFlags.AoE]: false,
					[Damage.damageType.kinetic]: 10,
					[Damage.damageType.magnetic]: 35,
				}),
			).to.be.deep.equal({
				[UnitAttack.attackFlags.DoT]: true,
			});
		});
		it('Deferred Damage unit', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.missile,
					[UnitAttack.attackFlags.Deferred]: true,
					[UnitAttack.attackFlags.DoT]: false,
					[Damage.damageType.kinetic]: 10,
					[Damage.damageType.magnetic]: 35,
				}),
			).to.be.deep.equal({
				[UnitAttack.attackFlags.Deferred]: true,
			});
		});
		it('Mixed case #1', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.supersonic,
					[UnitAttack.attackFlags.AoE]: true,
					[UnitAttack.attackFlags.Penetrative]: true,
					[UnitAttack.attackFlags.DoT]: false,
					radius: 5,
					aoeType: UnitAttack.TAoEDamageSplashTypes.cone90,
					magnitude: 0.5,
					[Damage.damageType.heat]: 14,
					[Damage.damageType.antimatter]: 14,
					[Damage.damageType.magnetic]: 14,
				}),
			).to.be.deep.equal({
				[UnitAttack.attackFlags.AoE]: true,
				[UnitAttack.attackFlags.Penetrative]: true,
			});
		});
		it('Mixed case #2', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.immediate,
					[UnitAttack.attackFlags.Deferred]: true,
					[UnitAttack.attackFlags.Disruptive]: true,
					[UnitAttack.attackFlags.Selective]: true,
					[UnitAttack.attackFlags.DoT]: false,
					radius: 5,
					aoeType: UnitAttack.TAoEDamageSplashTypes.cone90,
					magnitude: 0.5,
					[Damage.damageType.warp]: 14,
				}),
			).to.be.deep.equal({
				[UnitAttack.attackFlags.Deferred]: true,
				[UnitAttack.attackFlags.Disruptive]: true,
				[UnitAttack.attackFlags.Selective]: true,
			});
		});
		it('Mixed case #3', () => {
			expect(
				AttackManager.getAttackFlagsGenericFromAttack({
					delivery: UnitAttack.deliveryType.aerial,
					[UnitAttack.attackFlags.DoT]: true,
					duration: 5,
					[UnitAttack.attackFlags.Penetrative]: false,
					[UnitAttack.attackFlags.Selective]: true,
					[Damage.damageType.antimatter]: {
						min: 10,
						max: 50,
						distribution: 'Uniform',
					},
				}),
			).to.be.deep.equal({
				[UnitAttack.attackFlags.DoT]: true,
				[UnitAttack.attackFlags.Selective]: true,
			});
		});
	});
});

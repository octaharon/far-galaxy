import { expect } from 'chai';
import 'mocha';
import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../definitions/core/DI/injections';
import UnitAttack from '../definitions/tactical/damage/attack';
import Damage from '../definitions/tactical/damage/damage';
import GenericEquipment from '../definitions/technologies/equipment/GenericEquipment';
import EnergyWeapons from '../definitions/technologies/weapons/all/beam';
import PhoenixMissile from '../definitions/technologies/weapons/all/guided/PhoenixMissile.35006';
import GammaLaser from '../definitions/technologies/weapons/all/lasers/GammaLaser.90005';
import GenericWeapon from '../definitions/technologies/weapons/GenericWeapon';
import Weapons, { IWeapon, IWeaponMeta } from '../definitions/technologies/types/weapons';
import WeaponManager from '../definitions/technologies/weapons/WeaponManager';

const sampleProperties: Weapons.TWeaponProperties = {
	baseCost: 5,
	baseAccuracy: 0.5,
	baseRange: 10,
	baseRate: 2,
	priority: -1,
	attacks: [
		{
			delivery: UnitAttack.deliveryType.beam,
			[UnitAttack.attackFlags.Penetrative]: true,
			[Damage.damageType.magnetic]: {
				min: 10,
				max: 20,
				distribution: 'Bell',
			},
		},
	],
};

const alteredProps = {
	baseCost: 10,
	priority: 1,
	baseRange: 8,
};

const expectedProperties = {
	...sampleProperties,
	...alteredProps,
};

class SampleWeapon extends GenericWeapon {
	public static readonly id: number = 1234567890;
	public baseCost: number = this.baseCost + 5;
	public priority: number = this.priority + 2;
	public baseRange = 8;
}

const testMeta = {
	group: EnergyWeapons,
	slots: [Weapons.TWeaponSlotType.medium, Weapons.TWeaponSlotType.small],
} as IWeaponMeta;
const SampleExtends = new SampleWeapon(sampleProperties, testMeta);

const testFactory = new WeaponManager();

const SampleInstance = new GenericWeapon(sampleProperties, testMeta);

const samplePresetId2 = GammaLaser.id;

const samplePresetId: number = PhoenixMissile.id;
const samplePresetWeapon: IWeapon = new PhoenixMissile(null, testFactory.fill().getItemMeta(samplePresetId));
const serializedPresetWeapon: Partial<Weapons.TSerializedWeapon> = _.omit(samplePresetWeapon.serialize(), 'instanceId');

describe('GenericWeapon', () => {
	describe('constructor', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleInstance).to.deep.include(sampleProperties);
		});
		it('correctly passes properties to SetProps', () => {
			const c = new SampleWeapon(sampleProperties, {
				slots: [],
				group: EnergyWeapons,
			});
			c.setProps(alteredProps);
			expect(c).to.deep.include(expectedProperties);
		});
		it('ID starts with Type Descriptor', () => {
			const c = new SampleWeapon(sampleProperties, {
				slots: [],
				group: EnergyWeapons,
			});
			expect(c.serialize())
				.to.have.property('instanceId')
				.that.match(new RegExp(`^${c.getDescriptor(DIEntityDescriptors[DIInjectableCollectibles.weapon])}`));
		});
		it('correctly passes group through meta', () => {
			expect(SampleInstance).property('group').to.include({ id: EnergyWeapons.id });
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleInstance).property('static').to.include({ id: GenericWeapon.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleInstance.serialize();
				expect(s).to.deep.include(sampleProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(GenericWeapon.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
	describe('Extension class', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleExtends).to.deep.include(expectedProperties);
		});
		it('correctly passes group through meta', () => {
			expect(SampleExtends).property('group').to.include({ id: EnergyWeapons.id });
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleExtends).property('static').to.include({ id: SampleWeapon.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleExtends.serialize();
				expect(s).to.deep.include(expectedProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(SampleWeapon.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
});
describe('WeaponManager', () => {
	it('Fills correctly', () => {
		const all = testFactory.getAll();
		const meta = testFactory.getItemMeta(samplePresetId);
		expect(meta).to.be.not.empty;
		expect(Object.keys(all)).to.have.property('length').to.be.greaterThan(0);
		expect(Object.keys(all)).to.include(samplePresetId.toString());
	});
	it('Correctly returns a constructor', () => {
		const c = testFactory.get(samplePresetId);
		expect(c).to.be.equal(PhoenixMissile);
	});
	it('Correctly instantiates without props and assigns meta', () => {
		const c = testFactory.instantiate(null, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(serializedPresetWeapon);
		expect(c).to.have.property('meta').that.has.property('slots');
	});
	it('Correctly instantiates with props', () => {
		const c = testFactory.instantiate(sampleProperties, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(sampleProperties);
		expect(s).to.have.property('id').that.is.equal(samplePresetId);
	});
	it('Correctly unserializes and assigns a meta', () => {
		const samplePresetWeapon2 = testFactory.instantiate(sampleProperties, samplePresetId2);
		const serializedPresetWeapon2 = samplePresetWeapon2.serialize();
		const sampleInstanceId = 'unserialized';
		serializedPresetWeapon2.instanceId = sampleInstanceId;
		const c = testFactory.unserialize(serializedPresetWeapon2);
		expect(c).to.deep.include(sampleProperties);
		expect(c).to.have.property('meta').to.have.property('slots');
		expect(c).to.have.property('instanceId').that.is.equal(sampleInstanceId);
	});
	it('Correctly clones and assigns a meta', () => {
		const samplePresetWeapon2 = testFactory.clone(samplePresetWeapon);
		const serializedPresetWeapon2 = samplePresetWeapon2.serialize();
		expect(_.omit(serializedPresetWeapon2, 'instanceId')).to.deep.include(
			_.omit(serializedPresetWeapon, 'instanceId'),
		);
		expect(samplePresetWeapon2).to.have.property('instanceId').to.not.equal(samplePresetWeapon.getInstanceId());
		expect(samplePresetWeapon2).to.have.property('meta').that.includes({
			slots: samplePresetWeapon?.meta?.slots,
		});
	});
	it('Filters by group type', () => {
		const c = testFactory.instantiate(null, samplePresetId2);
		const l = testFactory.getWeaponsByGroup(c.meta.group.id);
		expect(l).to.include(c.static);
	});
	it('Filters by slot type', () => {
		const c = testFactory.instantiate(null, samplePresetId2);
		const l = testFactory.getWeaponsBySlot(c.meta.slots[0]);
		expect(l).to.include(c.static);
	});
});

import { expect } from 'chai';
import 'mocha';
import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../definitions/core/DI/injections';
import Damage from '../definitions/tactical/damage/damage';
import UnitDefense from '../definitions/tactical/damage/defense';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import { IShieldMeta, TShieldProps } from '../definitions/technologies/types/shield';
import ImmortalShield from '../definitions/technologies/shields/all/large/ImmortalShield.30010';
import CarapaceShield from '../definitions/technologies/shields/all/medium/CarapaceShield.20003';
import NanoShield from '../definitions/technologies/shields/all/medium/NanoShield.20007';
import SmallHullShield from '../definitions/technologies/shields/all/small/SmallHullShield.10008';
import GenericShield from '../definitions/technologies/shields/GenericShield';
import ShieldManager from '../definitions/technologies/shields/ShieldManager';
import { shieldModifiersCases } from './fixtures/shieldModifiers.cases';

const sampleProperties: TShieldProps = {
	baseCost: 5,
	shield: {
		shieldAmount: 20,
		overchargeDecay: 2,
		default: {
			bias: -5,
			factor: 0.8,
		},
		[Damage.damageType.thermodynamic]: {
			threshold: 15,
			factor: 0.3,
		},
	},
};

const alteredProps: TShieldProps = {
	baseCost: 7,
	shield: {
		...sampleProperties.shield,
		overchargeDecay: 3,
		shieldAmount: 30,
		[Damage.damageType.magnetic]: {
			min: 20,
			bias: -10,
		},
	},
};

const expectedProperties = {
	...sampleProperties,
	...alteredProps,
};

const expectSerializedProps = (t: TShieldProps) =>
	({
		...t,
		shield: DefenseManager.serializeProtection(t.shield),
	} as TShieldProps);

class SampleShield extends GenericShield {
	public static readonly id: number = 1234567;
	public baseCost: number = this.baseCost + 2;
	public shield: UnitDefense.TShieldDefinition = {
		...this.shield,
		...alteredProps.shield,
	};
}

const SampleMeta2 = {
	slot: UnitDefense.TShieldSlotType.small,
} as IShieldMeta;
const SampleExtends = new SampleShield(sampleProperties, SampleMeta2);

const SampleMeta1 = {
	slot: UnitDefense.TShieldSlotType.medium,
} as IShieldMeta;
const SampleInstance = new GenericShield(sampleProperties, SampleMeta1);

const presetShields: EnumProxy<UnitDefense.TShieldSlotType, typeof GenericShield> = {
	[UnitDefense.TShieldSlotType.small]: SmallHullShield,
	[UnitDefense.TShieldSlotType.medium]: NanoShield,
	[UnitDefense.TShieldSlotType.large]: ImmortalShield,
};
const newPresetShield = (index = UnitDefense.TShieldSlotType.small) =>
	new presetShields[index](null, {
		slot: index,
	});

const samplePresetShield = newPresetShield();

const samplePresetId = samplePresetShield.static.id;
const samplePresetId2 = CarapaceShield.id;
const serializedPresetShield = _.omit(samplePresetShield.serialize(), 'instanceId');
const testFactory = new ShieldManager();

describe('GenericShield', () => {
	describe('constructor', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleInstance).to.deep.include(sampleProperties);
		});
		it('correctly passes properties to SetProps', () => {
			const c = new GenericShield(sampleProperties, {
				slot: UnitDefense.TShieldSlotType.medium,
			});
			c.setProps(alteredProps);
			expect(c).to.deep.include(expectedProperties);
		});
		it('ID starts with Type Descriptor', () => {
			const c = new GenericShield(sampleProperties, {
				slot: UnitDefense.TShieldSlotType.medium,
			});
			expect(c.serialize())
				.to.have.property('instanceId')
				.that.match(new RegExp(`^${c.getDescriptor(DIEntityDescriptors[DIInjectableCollectibles.shield])}`));
		});
		it('correctly passes slot through meta', () => {
			expect(SampleInstance).property('meta').to.include(SampleMeta1);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleInstance).property('static').to.include({ id: GenericShield.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleInstance.serialize();
				expect(s).to.deep.include(expectSerializedProps(sampleProperties));
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(GenericShield.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
	describe('Extension class', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleExtends).to.deep.include(expectedProperties);
		});
		it('correctly passes slot through meta', () => {
			expect(SampleExtends).property('meta').to.include(SampleMeta2);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleExtends).property('static').to.include({ id: SampleShield.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleExtends.serialize();
				expect(s).to.deep.include(expectSerializedProps(expectedProperties));
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(SampleShield.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
	describe('applyModifier', () => {
		const shieldTypes = Object.keys(presetShields);
		for (let i = 0; i < shieldTypes.length; i++) {
			const SampleInstance = newPresetShield(shieldTypes[i]);
			const SampleInstanceSnapshot = SampleInstance.serialize();
			shieldModifiersCases(SampleInstanceSnapshot).forEach(([title, mod, testValue]) =>
				it(`Shield sample ${i + 1}: ${title}`, () => {
					const modifiedShield = newPresetShield(shieldTypes[i]);
					modifiedShield.applyModifier(...([] as UnitDefense.TShieldsModifier[]).concat(mod));
					testValue.shield = DefenseManager.serializeProtection(testValue.shield);
					expect(_.omit(modifiedShield.serialize(), 'instanceId')).to.deep.include(
						_.omit(testValue, 'instanceId'),
					);
				}),
			);
		}
	});
});

describe('ShieldManager', () => {
	it('Fills correctly', () => {
		testFactory.fill();
		const all = testFactory.getAll();
		expect(Object.keys(all)).to.have.property('length').to.be.greaterThan(0);
		expect(Object.keys(all)).to.include(samplePresetId.toString());
	});
	it('Correctly returns a constructor', () => {
		const c = testFactory.get(samplePresetId);
		expect(c).to.be.equal(presetShields[samplePresetShield.static.type]);
	});
	it('Correctly instantiates without props and assigns meta', () => {
		const c = testFactory.instantiate(null, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(expectSerializedProps(serializedPresetShield));
		expect(c).to.have.property('meta').to.include({
			slot: samplePresetShield.static.type,
		});
	});
	it('Correctly instantiates with props', () => {
		const c = testFactory.instantiate(sampleProperties, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(expectSerializedProps(sampleProperties));
		expect(s).to.have.property('id').to.equal(samplePresetId);
	});
	it('Correctly unserializes and assigns a meta', () => {
		const samplePresetArmor2 = testFactory.instantiate(sampleProperties, samplePresetId2);
		const serializedPresetArmor2 = samplePresetArmor2.serialize();
		const sampleInstanceId = 'unserialized';
		serializedPresetArmor2.instanceId = sampleInstanceId;
		const c = testFactory.unserialize(serializedPresetArmor2);
		expect(c).to.deep.include(expectSerializedProps(sampleProperties));
		expect(c).to.have.property('meta').to.include({
			slot: samplePresetArmor2.meta.slot,
		});
		expect(c).to.have.property('instanceId').to.equal(sampleInstanceId);
	});
	it('Correctly clones and assigns a meta', () => {
		const samplePresetShield2 = testFactory.clone(samplePresetShield);
		const serializedPresetShield2 = samplePresetShield2.serialize();
		expect(_.omit(serializedPresetShield2, 'instanceId')).to.deep.include(
			_.omit(serializedPresetShield, 'instanceId'),
		);
		expect(samplePresetShield2).to.have.property('meta').to.include({
			slot: samplePresetShield.meta.slot,
		});
		expect(samplePresetShield2).to.have.property('instanceId').to.not.equal(samplePresetShield.getInstanceId());
	});
	it('Correctly filters by slot', () => {
		const c = testFactory.instantiate(null, samplePresetId2);
		const l = testFactory.getShieldsBySlotType(c.static.type);
		expect(l).to.include(c.static);
	});
});

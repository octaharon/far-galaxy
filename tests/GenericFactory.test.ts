/* eslint-disable @typescript-eslint/no-unused-expressions */
import chai from 'chai';
import spies from 'chai-spies';
import 'mocha';
import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../definitions/core/DI/injections';
import BasicMaths from '../definitions/maths/BasicMaths';
import BioforgeFactory from '../definitions/orbital/factories/all/Bioforge.104';
import JunkyardFactory from '../definitions/orbital/factories/all/Junkyard.304';
import TitanForgeFactory from '../definitions/orbital/factories/all/Titanforge.601';
import FactoryManager, { defaultFactoryProps } from '../definitions/orbital/factories/FactoryManager';
import GenericFactory from '../definitions/orbital/factories/GenericFactory';
import {
	BASE_PRODUCTION_SPEED,
	getTierProductionSpeed,
	IFactory,
	TChassisProductionModifier,
	TFactoryProps,
	TFactoryUpgrade,
	TIER_PRODUCTION_SPEED,
	TMovementProductionModifier,
} from '../definitions/orbital/factories/types';
import { BUILDING_INSTANTIATE_ERROR_NO_OWNERS } from '../definitions/orbital/GenericBaseBuildingManager';
import UnitChassis from '../definitions/technologies/types/chassis';
import Resources from '../definitions/world/resources/types';

chai.use(spies);
const { expect, spy } = chai;

const sampleProperties: TFactoryProps = {
	baseId: '!Q2w#E4r',
	playerId: 'test-player',
	powered: false,
	maxProductionCost: 182,
	currentProductionSlot: 0,
	productionPrototypes: ['test-prototype'],
	currentProductionStock: 222,
	disabled: true,
	energyMaintenanceCost: 24,
	productionSpeed: 30,
};

const alteredProps: Partial<TFactoryProps> = {
	currentProductionSlot: 1,
	currentProductionStock: 155,
	disabled: false,
};

const expectedProperties = {
	...sampleProperties,
	...alteredProps,
};

class SampleFactory extends GenericFactory {
	public static readonly id: number = 12345670;
	public static readonly prototypeSlots: number = 2;
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.scout,
		UnitChassis.chassisClass.quadrapod,
	];
	public static readonly productionModifierByChassis?: TChassisProductionModifier = {};
	public static readonly productionModifierByMovement?: TMovementProductionModifier = {};
	public buildingCost = {
		[Resources.resourceId.proteins]: 300,
		[Resources.resourceId.ores]: 1000,
	};
	public currentProductionSlot = 1;
	public currentProductionStock = 155;
	public disabled = false;
}

const SampleExtends = new SampleFactory(sampleProperties);

const SampleInstance = new GenericFactory(sampleProperties);

const sampleFactoryOwners = {
	playerId: 'testPlayer',
	baseId: 'testBase',
};

const samplePresetFactory = new BioforgeFactory({
	...sampleFactoryOwners,
	...defaultFactoryProps,
});

const samplePresetId = samplePresetFactory.static.id;
const samplePresetId2 = JunkyardFactory.id;
const serializedPresetFactory = _.omit(samplePresetFactory.serialize(), 'instanceId');
const testFactory = new FactoryManager();

describe('GenericFactory', () => {
	describe('constructor', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleInstance).to.deep.include(sampleProperties);
		});
		it('ID starts with Type Descriptor', () => {
			const c = new GenericFactory(sampleProperties);
			expect(c.serialize())
				.to.have.property('instanceId')
				.that.match(new RegExp(`^${c.getDescriptor(DIEntityDescriptors[DIInjectableCollectibles.factory])}`));
		});
		it('correctly passes properties to SetProps', () => {
			const c = new GenericFactory(sampleProperties);
			c.setProps(alteredProps);
			expect(c).to.deep.include(expectedProperties);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleInstance).property('static').to.include({ id: GenericFactory.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleInstance.serialize();
				expect(s).to.deep.include(sampleProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('playerId');
				expect(s).to.have.property('id').to.equal(GenericFactory.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});

		it('correctly applies upgrades', () => {
			expect(SampleInstance.static)
				.to.have.property('upgrades')
				.that.has.lengthOf(GenericFactory.upgrades.length);
			const upgradeId = 1;
			const upgrade = SampleInstance.static.upgrades[upgradeId];
			SampleInstance.upgrade(upgradeId);
			const sampleKey = _.sample(Object.keys(upgrade).filter((v) => v !== 'caption')) as Exclude<
				keyof TFactoryUpgrade,
				'caption'
			>;
			expect(SampleInstance)
				.to.have.property(sampleKey)
				.that.is.equal(BasicMaths.applyModifier(sampleProperties[sampleKey], upgrade[sampleKey]));
		});
	});
	describe('Extension class', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleExtends).to.deep.include(expectedProperties);
		});
		it('implements baseProductionSpeed based on buildingTier', () => {
			expect(new BioforgeFactory().serialize()).to.deep.include({
				productionSpeed: getTierProductionSpeed(BioforgeFactory.buildingTier) + TIER_PRODUCTION_SPEED / 2,
			});
			expect(new TitanForgeFactory({}).serialize()).to.deep.include({
				productionSpeed: getTierProductionSpeed(TitanForgeFactory.buildingTier) + 2 * BASE_PRODUCTION_SPEED,
			});
		});
		it('overwrites properties through constructor', () => {
			expect(
				new TitanForgeFactory({
					productionSpeed: 1,
					energyMaintenanceCost: 113,
				}).serialize(),
			).to.deep.include({
				productionSpeed: 1,
				energyMaintenanceCost: 113,
			});
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleExtends).property('static').to.include({ id: SampleFactory.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleExtends.serialize();
				expect(s).to.deep.include(expectedProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('playerId');
				expect(s).to.have.property('id').to.equal(SampleFactory.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});

	// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/6
});

describe('FactoryManager', () => {
	describe('Generic methods', () => {
		it('Fills correctly', () => {
			testFactory.fill();
			const all = testFactory.getAll();
			expect(Object.keys(all)).to.have.property('length').to.be.greaterThan(0);
			expect(Object.keys(all)).to.include(samplePresetId.toString());
		});
		it('Correctly returns a constructor', () => {
			const c = testFactory.get(samplePresetId);
			expect(c).to.be.equal(samplePresetFactory.constructor);
		});
		it('Correctly instantiates with player and base and default Production Speed', () => {
			const c = testFactory.instantiate(sampleFactoryOwners, samplePresetId);
			const s = c.serialize();
			expect(s).to.deep.include({
				...serializedPresetFactory,
				productionSpeed: samplePresetFactory.productionSpeed,
			});
		});
		it('Correctly instantiates with props', () => {
			const c = testFactory.instantiate(sampleProperties, samplePresetId);
			const s = c.serialize();
			expect(s).to.deep.include(sampleProperties);
			expect(s).to.have.property('id').to.equal(samplePresetId);
		});
		it('Correctly unserializes', () => {
			const samplePresetFactory2 = testFactory.instantiate(sampleProperties, samplePresetId2);
			const serializedPresetFactory2 = samplePresetFactory2.serialize();
			const sampleInstanceId = 'unserialized';
			serializedPresetFactory2.instanceId = sampleInstanceId;
			const c = testFactory.unserialize(serializedPresetFactory2);
			expect(c).to.deep.include(sampleProperties);
			expect(c).to.have.property('instanceId').to.equal(sampleInstanceId);
		});
		it('Correctly clones', () => {
			const samplePresetFactory2 = testFactory.clone(samplePresetFactory);
			const serializedPresetFactory2 = samplePresetFactory2.serialize();
			expect(_.omit(serializedPresetFactory2, 'instanceId')).to.deep.include(
				_.omit(serializedPresetFactory, 'instanceId'),
			);
			expect(samplePresetFactory2)
				.to.have.property('instanceId')
				.to.not.equal(samplePresetFactory.getInstanceId());
		});
	});
	describe('store', () => {
		it('Throws without props', () => {
			expect(() => testFactory.store(testFactory.instantiate(null, samplePresetId))).to.throw(
				BUILDING_INSTANTIATE_ERROR_NO_OWNERS,
			);
		});
		it('Throws without baseId', () => {
			expect(() => testFactory.store(testFactory.instantiate({ playerId: 'whatever' }, samplePresetId))).to.throw(
				BUILDING_INSTANTIATE_ERROR_NO_OWNERS,
			);
		});
		it('Throws without playerId', () => {
			expect(() => testFactory.store(testFactory.instantiate({ baseId: 'baseId' }, samplePresetId))).to.throw(
				BUILDING_INSTANTIATE_ERROR_NO_OWNERS,
			);
		});
		it('Applies callback if called correctly', () => {
			const newFactory = testFactory.instantiate(
				{
					...sampleFactoryOwners,
					...sampleProperties,
				},
				samplePresetId,
			);
			const expectedProps = newFactory.serialize();
			const cb = spy((t: IFactory) => {
				expect(t).to.equal(newFactory);
				const ts = t.serialize();
				expect(ts).to.deep.include(expectedProps, 'has changed their props during Store');
			});
			testFactory.store(newFactory, cb);
			expect(cb).to.have.been.called.with(newFactory);
			expect(cb).to.have.been.called.once;
		});
	});
});

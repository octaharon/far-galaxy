import { expect } from 'chai';
import 'mocha';
import Geometry from '../definitions/maths/Geometry';
import {
	MAP_CENTER_RADIUS,
	MAX_MAP_DOODADS,
	MAX_MAP_RADIUS,
	MIN_MAP_RADIUS,
} from '../definitions/tactical/map/constants';
import MapGenerator from '../definitions/tactical/map/MapGenerator';
import { isCaveEntity, isHillEntity, isLakeEntity } from '../definitions/tactical/map/MapObstacles';
import { TMapParameters } from '../definitions/tactical/map';

const MG = MapGenerator.generate;
const FlatMapNoCentralZone: TMapParameters = {
	radiationLevel: 0,
	magneticField: 0.5,
	waterPresence: 0,
	spinFactor: 0,
	warpFactor: 0.5,
	atmosphericColorHex: 0xffffff,
	atmosphericDensity: 0.5,
	centralZone: false,
	flatness: 1,
	gravity: 0.5,
	surfaceColorHex: 0x000000,
	resourceFields: 5,
	temperature: 0.5,
	size: 1,
	terrainDensity: 0,
};

const FlatMapCentralZoneDoodads: TMapParameters = {
	radiationLevel: 0,
	magneticField: 0.5,
	waterPresence: 0,
	spinFactor: 0,
	warpFactor: 0.5,
	atmosphericColorHex: 0xffffff,
	atmosphericDensity: 0.5,
	temperature: 0.5,
	centralZone: true,
	flatness: 1,
	gravity: 0.5,
	surfaceColorHex: 0x000000,
	resourceFields: 5,
	size: 0.5,
	terrainDensity: 1,
};

const HillMapCentralZoneDoodads: TMapParameters = {
	radiationLevel: 0,
	magneticField: 0.5,
	waterPresence: 0,
	spinFactor: 0,
	warpFactor: 0.5,
	atmosphericColorHex: 0xffffff,
	atmosphericDensity: 0.5,
	temperature: 0.5,
	centralZone: true,
	flatness: 0,
	gravity: 0.5,
	surfaceColorHex: 0x000000,
	resourceFields: 5,
	size: 0.75,
	terrainDensity: 1,
};

const WaterHillMapNoCentralZoneDoodads: TMapParameters = {
	radiationLevel: 0,
	magneticField: 0.5,
	waterPresence: 0.5,
	spinFactor: 0.5,
	warpFactor: 0.5,
	atmosphericColorHex: 0xffffff,
	temperature: 0.1,
	atmosphericDensity: 0.5,
	centralZone: false,
	flatness: 0.5,
	gravity: 0.5,
	surfaceColorHex: 0x000000,
	resourceFields: 5,
	size: 1,
	terrainDensity: 0.5,
};

describe('Map Generator', () => {
	describe('generates a flat landmass of size 1', () => {
		const map = MG(FlatMapNoCentralZone);
		it('that inherits parameters,', () => {
			expect(map).to.have.property('parameters').that.is.deep.equal(FlatMapNoCentralZone);
		});
		it('has no entities and doodads,', () => {
			expect(map).to.have.property('doodads').that.is.an('array').that.has.property('length', 0);
			expect(map).to.have.property('terrainEntities').that.is.an('object').that.is.empty;
		});
		it('has maximum radius', () => {
			expect(map).to.have.property('radius', MAX_MAP_RADIUS);
		});
	});
	describe('generates a flat landmass of size 0.5 and terrain density 1 with central zone', () => {
		const map = MG(FlatMapCentralZoneDoodads);
		it('that inherits parameters,', () => {
			expect(map).to.have.property('parameters').that.is.deep.equal(FlatMapCentralZoneDoodads);
		});
		it('has no entities,', () => {
			expect(map).to.have.property('terrainEntities').that.is.an('object').that.is.empty;
		});
		it('has maximum amount of doodads none of which are in center zone,', () => {
			expect(map).to.have.property('doodads').that.is.an('array').that.has.property('length', MAX_MAP_DOODADS);
			expect(
				map.doodads.filter((d) =>
					Geometry.isPointInRange(d, {
						radius: MAP_CENTER_RADIUS,
						x: 0,
						y: 0,
					}),
				),
			)
				.to.be.an('array')
				.that.has.property('length', 0);
		});
		it('has correct radius', () => {
			expect(map).to.have.property('radius', (MAX_MAP_RADIUS + MIN_MAP_RADIUS) / 2);
		});
	});
	describe('generates a hilly landmass of size 0.75, flatness=0 and terrain density 1 with central zone', () => {
		const map = MG(HillMapCentralZoneDoodads);
		// console.log(map);
		it('that inherits parameters,', () => {
			expect(map).to.have.property('parameters').that.is.deep.equal(HillMapCentralZoneDoodads);
		});
		it('has hills and caves outside of central zone, and no lakes,', () => {
			expect(map).to.have.property('terrainEntities').that.is.an('object').that.is.not.empty;
			expect(Object.values(map.terrainEntities).filter((e) => !isLakeEntity(e))).to.have.lengthOf(
				Object.values(map.terrainEntities).length,
			);
			expect(
				Object.values(map.terrainEntities).filter((d) =>
					Geometry.isPointInRange(d.shape.center, {
						radius: MAP_CENTER_RADIUS,
						x: 0,
						y: 0,
					}),
				),
			)
				.to.be.an('array')
				.that.has.property('length', 0);
		});
		it('has maximum amount of doodads none of which are in center zone,', () => {
			expect(map).to.have.property('doodads').that.is.an('array').that.has.property('length', MAX_MAP_DOODADS);
			expect(
				map.doodads.filter((d) =>
					Geometry.isPointInRange(d, {
						radius: MAP_CENTER_RADIUS,
						x: 0,
						y: 0,
					}),
				),
			)
				.to.be.an('array')
				.that.has.property('length', 0);
		});
		it('has correct radius', () => {
			expect(map).to.have.property('radius', (MAX_MAP_RADIUS - MIN_MAP_RADIUS) * 0.75 + MIN_MAP_RADIUS);
		});
	});
	describe('generates a map with size 1, flatness 0.5, water presense 0.5 and terrain density 0.5 without CZ', () => {
		const map = MG(WaterHillMapNoCentralZoneDoodads);
		// console.log(map);
		it('that inherits parameters,', () => {
			expect(map).to.have.property('parameters').that.is.deep.equal(WaterHillMapNoCentralZoneDoodads);
		});
		it('has hills, caves, and lakes', () => {
			expect(map).to.have.property('terrainEntities').that.is.an('object').that.is.not.empty;
			expect(Object.values(map.terrainEntities).filter((e) => isLakeEntity(e)))
				.to.be.an('array')
				.that.has.length.greaterThan(0);
			expect(Object.values(map.terrainEntities).filter((e) => isHillEntity(e)))
				.to.be.an('array')
				.that.has.length.greaterThan(0);
			expect(Object.values(map.terrainEntities).filter((e) => isCaveEntity(e)))
				.to.be.an('array')
				.that.has.length.greaterThan(0);
		});
		it('has some amount of doodads ', () => {
			expect(map).to.have.property('doodads').that.is.an('array').that.has.length.greaterThan(0);
		});
		it('has correct radius', () => {
			expect(map).to.have.property('radius', MAX_MAP_RADIUS);
		});
	});
});

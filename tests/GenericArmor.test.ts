import { expect } from 'chai';
import 'mocha';
import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../definitions/core/DI/injections';
import Damage from '../definitions/tactical/damage/damage';
import UnitDefense from '../definitions/tactical/damage/defense';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import { TArmorProps } from '../definitions/technologies/types/armor';
import LargeGluonArmor from '../definitions/technologies/armor/all/large/LargeGluonArmor.30005';
import DefenseMatrix from '../definitions/technologies/armor/all/medium/DefenseMatrix.20005';
import SmallBioArmor from '../definitions/technologies/armor/all/small/SmallBioArmor.10007';
import SmallDefenseMatrix from '../definitions/technologies/armor/all/small/SmallDefenseMatrix.10006';
import ArmorManager from '../definitions/technologies/armor/ArmorManager';
import GenericArmor from '../definitions/technologies/armor/GenericArmor';
import { armorModifiersCases } from './fixtures/armorModifiers.cases';

const sampleProperties: TArmorProps = {
	baseCost: 5,
	armor: {
		default: {
			bias: -5,
			factor: 0.8,
		},
	},
};

const alteredProps = {
	baseCost: 12,
	armor: {
		...sampleProperties.armor,
		[Damage.damageType.magnetic]: {
			min: 20,
			bias: -10,
		},
	},
};

const expectedProperties = {
	...sampleProperties,
	...alteredProps,
};

const expectSerializedProps = (t: TArmorProps) =>
	({
		...t,
		armor: DefenseManager.serializeProtection(t.armor),
	} as TArmorProps);

class SampleArmor extends GenericArmor {
	public static readonly id: number = 1234567;
	public baseCost: number = this.baseCost + 7;
	public armor: UnitDefense.TArmor = {
		...this.armor,
		...alteredProps.armor,
	};
}

const SampleMeta2 = {
	slot: UnitDefense.TArmorSlotType.small,
};
const SampleExtends = new SampleArmor(sampleProperties, SampleMeta2);

const SampleMeta1 = {
	slot: UnitDefense.TArmorSlotType.medium,
};

const SampleInstance = new GenericArmor(sampleProperties, SampleMeta1);

const presetArmors = [LargeGluonArmor, DefenseMatrix, SmallBioArmor];
const newPresetArmor = (index = 0) =>
	new presetArmors[index](null, {
		slot: UnitDefense.TArmorSlotType.large,
	});
const samplePresetArmor = newPresetArmor();

const samplePresetId = LargeGluonArmor.id;
const samplePresetId2 = SmallDefenseMatrix.id;
const serializedPresetArmor = _.omit(samplePresetArmor.serialize(), 'instanceId');

describe('GenericArmor', () => {
	describe('constructor', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleInstance).to.deep.include(sampleProperties);
		});
		it('correctly passes properties to SetProps', () => {
			const c = new GenericArmor(sampleProperties, {
				slot: UnitDefense.TArmorSlotType.medium,
			});
			c.setProps(alteredProps);
			expect(c).to.deep.include(expectedProperties);
		});
		it('ID starts with Type Descriptor', () => {
			const c = new GenericArmor(sampleProperties, {
				slot: UnitDefense.TArmorSlotType.medium,
			});
			expect(c.serialize())
				.to.have.property('instanceId')
				.that.match(new RegExp(`^${c.getDescriptor(DIEntityDescriptors[DIInjectableCollectibles.armor])}`));
		});
		it('correctly passes slot through meta', () => {
			expect(SampleInstance).property('meta').to.include(SampleMeta1);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleInstance).property('static').to.include({ id: GenericArmor.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleInstance.serialize();
				expect(s).to.deep.include(expectSerializedProps(sampleProperties));
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(GenericArmor.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
	describe('Extension class', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleExtends).to.deep.include(expectedProperties);
		});
		it('correctly overwrites properties through the constructor', () => {
			const props = { baseCost: 11, armor: { default: { bias: -0.1 } } };
			expect(new SampleArmor(props)).to.deep.include(props);
		});
		it('correctly passes slot through meta', () => {
			expect(SampleExtends).property('meta').to.include(SampleMeta2);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleExtends).property('static').to.include({ id: SampleArmor.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleExtends.serialize();
				expect(s).to.deep.include(expectSerializedProps(expectedProperties));
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(SampleArmor.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
	describe('applyModifier', () => {
		for (let i = 0; i < presetArmors.length; i++) {
			const SampleInstance = newPresetArmor(i);
			const SampleInstanceSnapshot = SampleInstance.serialize();
			armorModifiersCases(SampleInstanceSnapshot).forEach(([title, mod, testValue]) =>
				it(`Armor sample ${i + 1}: ${title}`, () => {
					const modifiedArmor = newPresetArmor(i);
					modifiedArmor.applyModifier(...([] as UnitDefense.TArmorModifier[]).concat(mod));
					testValue.armor = DefenseManager.serializeProtection(testValue.armor);
					expect(_.omit(modifiedArmor.serialize(), 'instanceId')).to.deep.include(
						_.omit(testValue, 'instanceId'),
					);
				}),
			);
		}
	});
});

describe('ArmorManager', () => {
	const testFactory = new ArmorManager();

	it('Fills correctly', () => {
		testFactory.fill();
		const all = testFactory.getAll();
		expect(Object.keys(all)).to.have.property('length').to.be.greaterThan(0);
		expect(Object.keys(all)).to.include(samplePresetId.toString());
	});
	it('Correctly returns a constructor', () => {
		const c = testFactory.get(samplePresetId);
		expect(c).to.be.equal(LargeGluonArmor);
	});
	it('Correctly instantiates without props and assigns meta', () => {
		const c = testFactory.instantiate(null, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(expectSerializedProps(serializedPresetArmor));
		expect(c).to.have.property('meta').to.include({
			slot: LargeGluonArmor.type,
		});
	});
	it('Correctly instantiates with props', () => {
		const c = testFactory.instantiate(sampleProperties, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(expectSerializedProps(sampleProperties));
		expect(s).to.have.property('id').to.equal(samplePresetId);
	});
	it('Correctly unserializes and assigns a meta', () => {
		const samplePresetArmor2 = testFactory.instantiate(sampleProperties, samplePresetId2);
		const serializedPresetArmor2 = samplePresetArmor2.serialize();
		const sampleInstanceId = 'unserialized';
		serializedPresetArmor2.instanceId = sampleInstanceId;
		const c = testFactory.unserialize(serializedPresetArmor2);
		expect(c).to.deep.include(expectSerializedProps(sampleProperties));
		expect(c).to.have.property('meta').to.include({
			slot: samplePresetArmor2.meta.slot,
		});
		expect(c).to.have.property('instanceId').to.equal(sampleInstanceId);
	});
	it('Correctly clones and assigns a meta', () => {
		const samplePresetArmor2 = testFactory.clone(samplePresetArmor);
		const serializedPresetArmor2 = samplePresetArmor2.serialize();
		expect(_.omit(serializedPresetArmor2, 'instanceId')).to.deep.include(
			_.omit(expectSerializedProps(serializedPresetArmor), 'instanceId'),
		);
		expect(samplePresetArmor2).to.have.property('meta').to.include({
			slot: samplePresetArmor.meta.slot,
		});
		expect(samplePresetArmor2).to.have.property('instanceId').to.not.equal(samplePresetArmor.getInstanceId());
	});
	it('Correctly filters by slot', () => {
		const c = testFactory.instantiate(null, samplePresetId2);
		const l = testFactory.getArmorsBySlotType(c.static.type);
		expect(l).to.include(c.static);
	});
});

/* eslint-disable @typescript-eslint/no-unused-expressions */
import chai from 'chai';
import spies from 'chai-spies';
import { cloneObject } from '../definitions/core/dataTransformTools';
import Damage from '../definitions/tactical/damage/damage';
import UnitDefense from '../definitions/tactical/damage/defense';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import { armorModifiersStackCases } from './fixtures/armorModifiers.cases';
import { shieldModifierStackCases } from './fixtures/shieldModifiers.cases';

chai.use(spies);
const { expect, spy } = chai;

describe('stackArmorModifiers', () => {
	armorModifiersStackCases.forEach(([title, mods, output]) =>
		it(title, () => {
			expect(DefenseManager.stackArmorModifiers.call(DefenseManager, ...[].concat(mods)))
				.to.be.an('object')
				.that.is.deep.equal(output);
		}),
	);

	const aMod1: UnitDefense.TArmorModifier = {
		baseCost: {
			factor: 1.2,
			max: 20,
		},
		[Damage.damageType.biological]: {
			factor: 0.8,
			threshold: 5,
		},
		[Damage.damageType.warp]: {
			threshold: 20,
		},
	};

	const aMod2: UnitDefense.TArmorModifier = {
		default: {
			factor: 0.8,
		},
	};

	const aMod3: UnitDefense.TArmorModifier = {
		default: {
			bias: -10,
		},
		[Damage.damageType.biological]: {
			factor: 0.8,
		},
		baseCost: {
			bias: -5,
		},
	};

	it('associativity', () => {
		expect(DefenseManager.stackArmorModifiers(aMod1, aMod2, aMod3))
			.to.be.an('object')
			.that.is.deep.equal(DefenseManager.stackArmorModifiers(aMod2, aMod3, aMod1));
	});

	it('commutativity', () => {
		expect(DefenseManager.stackArmorModifiers(aMod1, DefenseManager.stackArmorModifiers(aMod2, aMod3)))
			.to.be.an('object')
			.that.is.deep.equal(
				DefenseManager.stackArmorModifiers(DefenseManager.stackArmorModifiers(aMod1, aMod2), aMod3),
			);
	});
});

describe('stackShieldModifiers', () => {
	shieldModifierStackCases.forEach(([title, mods, output]) =>
		it(title, () => {
			expect(DefenseManager.stackShieldsModifier.call(DefenseManager, ...[].concat(mods)))
				.to.be.an('object')
				.that.is.deep.equal(output);
		}),
	);

	const aMod1: UnitDefense.TShieldsModifier = {
		baseCost: {
			factor: 1.2,
			max: 20,
		},
		shieldAmount: {
			factor: 1.2,
		},
		overchargeDecay: { bias: 5 },
		[Damage.damageType.biological]: {
			factor: 0.8,
			threshold: 5,
		},
		[Damage.damageType.warp]: {
			threshold: 20,
		},
	};

	const aMod2: UnitDefense.TShieldsModifier = {
		default: {
			factor: 0.8,
		},
		overchargeDecay: {
			min: 2,
		},
		shieldAmount: {
			factor: 2,
			bias: 10,
		},
	};

	const aMod3: UnitDefense.TShieldsModifier = {
		default: {
			bias: -10,
		},
		[Damage.damageType.biological]: {
			factor: 0.8,
		},
		overchargeDecay: { bias: -5, minGuard: 10 },
		baseCost: {
			bias: -5,
		},
	};

	it('associativity', () => {
		expect(DefenseManager.stackShieldsModifier(aMod1, aMod2, aMod3))
			.to.be.an('object')
			.that.is.deep.equal(DefenseManager.stackShieldsModifier(aMod2, aMod3, aMod1));
	});

	it('commutativity', () => {
		expect(DefenseManager.stackShieldsModifier(aMod1, DefenseManager.stackShieldsModifier(aMod2, aMod3)))
			.to.be.an('object')
			.that.is.deep.equal(
				DefenseManager.stackShieldsModifier(DefenseManager.stackShieldsModifier(aMod1, aMod2), aMod3),
			);
	});
});

describe('getTotalArmor', () => {
	let wrapper: CallableFunction = null;
	const armorObj1: UnitDefense.TArmor = {
		default: {
			min: 0,
			factor: 0.8,
		},
		[Damage.damageType.magnetic]: {
			bias: -15,
			max: 20,
		},
	};
	const armorObj2: UnitDefense.TArmor = {
		[Damage.damageType.magnetic]: {
			bias: -15,
			factor: 0.8,
		},
	};
	const armorObj3: UnitDefense.TArmor = {
		[Damage.damageType.heat]: {
			factor: 0.5,
		},
	};
	beforeEach(() => {
		wrapper = spy.on(DefenseManager, 'serializeProtection');
	});

	afterEach(() => {
		expect(wrapper).to.have.been.called();
		spy.restore();
	});

	it('null object', () => {
		expect(DefenseManager.getTotalArmor()).to.be.an('object').that.is.empty;
	});
	it(`single prop, doesn't mutate source`, () => {
		const copy = cloneObject(armorObj1);
		expect(DefenseManager.getTotalArmor(armorObj1))
			.to.be.an('object')
			.that.is.deep.equal({
				...armorObj1,
				[Damage.damageType.magnetic]: { ...armorObj1[Damage.damageType.magnetic], min: 0 },
			});
		expect(copy).to.deep.equal(armorObj1);
	});
	it(`double prop, doesn't mutate sources`, () => {
		const copy1 = cloneObject(armorObj1);
		const copy2 = cloneObject(armorObj2);
		expect(DefenseManager.getTotalArmor(armorObj1, armorObj2))
			.to.be.an('object')
			.that.is.deep.equal(DefenseManager.stackArmorModifiers(armorObj1, armorObj2));
		expect(copy1).to.deep.equal(armorObj1);
		expect(copy2).to.deep.equal(armorObj2);
	});
	it(`ignores invalid params`, () => {
		const copy1 = cloneObject(armorObj1);
		const copy2 = cloneObject(armorObj2);
		const copy3 = cloneObject(armorObj3);
		expect(DefenseManager.getTotalArmor(armorObj1, null, armorObj3, {}, armorObj2, { default: { threshold: -5 } }))
			.to.be.an('object')
			.that.is.deep.equal(DefenseManager.stackArmorModifiers(armorObj1, armorObj2, armorObj3));
		expect(copy1).to.deep.equal(armorObj1);
		expect(copy2).to.deep.equal(armorObj2);
		expect(copy3).to.deep.equal(armorObj3);
	});
	it(`associativity`, () => {
		expect(DefenseManager.getTotalArmor(armorObj1, armorObj2, armorObj3))
			.to.be.an('object')
			.that.is.deep.equal(DefenseManager.getTotalArmor(armorObj3, armorObj1, armorObj2));
	});
	it(`commutativity`, () => {
		expect(DefenseManager.getTotalArmor(armorObj1, DefenseManager.getTotalArmor(armorObj2, armorObj3)))
			.to.be.an('object')
			.that.is.deep.equal(
				DefenseManager.getTotalArmor(DefenseManager.getTotalArmor(armorObj1, armorObj2), armorObj3),
			);
	});
});

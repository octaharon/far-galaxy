import { expect } from 'chai';
import 'mocha';
import InterfaceTypes from '../definitions/core/InterfaceTypes';
import { GeometricPrecision } from '../definitions/maths/constants';
import Geometry from '../definitions/maths/Geometry';
import MapGenerator from '../definitions/tactical/map/MapGenerator';
import { defaultMapParameters } from '../definitions/tactical/map';
import { fpsLength } from './helpers';

const Precision = GeometricPrecision;

describe('Geometry', () => {
	describe('getRayUnitCircleIntersection', () => {
		it('(0,0)-(3,0) with unit circle : (1,0) and (-1,0)', () => {
			const c = Geometry.getRayUnitCircleIntersection(
				1,
				{
					x: 0,
					y: 0,
				},
				{
					x: 3,
					y: 0,
				},
			);
			expect(c).to.be.an('array', "Doesn't return an array");
			expect(c).to.have.lengthOf(2, "Doesn't return two intersection points");
			expect(c[1]).to.have.property('x').which.closeTo(1, Precision, "Doesn't cross at x=1");
			expect(c[0]).to.have.property('x').which.closeTo(-1, Precision, "Doesn't cross at x=-1");
			expect(c[1]).to.have.property('y').which.closeTo(0, Precision, "Doesn't cross at y=0");
			expect(c[0]).to.have.property('y').which.closeTo(0, Precision, "Doesn't cross at y=0");
		});

		it('(0,0)-(1,1) with unit circle : (+-sqrt 1/2, +-sqrt 1/2)', () => {
			const c = Geometry.getRayUnitCircleIntersection(
				1,
				{
					x: 0,
					y: 0,
				},
				{
					x: 1,
					y: 1,
				},
			);
			expect(c).to.be.an('array', "Doesn't return an array");
			expect(c).to.have.lengthOf(2, "Doesn't return two intersection points");
			expect(c[1])
				.to.have.property('x')
				.which.closeTo(Math.sqrt(2) / 2, Precision, "Doesn't cross at x=sqrt(2)");
			expect(c[0])
				.to.have.property('x')
				.which.closeTo(-Math.sqrt(2) / 2, Precision, "Doesn't cross at x=-sqrt(2)");
			expect(c[1])
				.to.have.property('y')
				.which.closeTo(Math.sqrt(2) / 2, Precision, "Doesn't cross at y=sqrt(2)");
		});

		it('(-1,1)-(1,1) with unit circle : (0,1)', () => {
			const c = Geometry.getRayUnitCircleIntersection(
				1,
				{
					x: -1,
					y: 1,
				},
				{
					x: 1,
					y: 1,
				},
			);
			expect(c).to.be.an('array', "Doesn't return an array");
			expect(c).to.have.lengthOf(1, "Doesn't return one intersection point");
			expect(c[0]).to.have.property('x').which.closeTo(0, Precision, "Doesn't cross at x=0");
			expect(c[0]).to.have.property('y').which.closeTo(1, Precision, "Doesn't cross at y=1");
		});

		it('(0,1)-(2,0) with unit circle : (0,1) and (0.8,0.6)', () => {
			const c = Geometry.getRayUnitCircleIntersection(
				1,
				{
					x: 0,
					y: 1,
				},
				{
					x: 2,
					y: 0,
				},
			);
			expect(c).to.be.an('array', "Doesn't return an array");
			expect(c).to.have.lengthOf(2, "Doesn't return two intersection point");
			expect(c[0]).to.have.property('x').which.closeTo(0, Precision, "Doesn't cross at x=0");
			expect(c[0]).to.have.property('y').which.closeTo(1, Precision, "Doesn't cross at y=1");
			expect(c[1]).to.have.property('x').which.closeTo(0.8, Precision, "Doesn't cross at x=0.8");
			expect(c[1]).to.have.property('y').which.closeTo(0.6, Precision, "Doesn't cross at y=0.6");
		});

		it('(0,3)-(3,0) with unit circle : no intersection', () => {
			const c = Geometry.getRayUnitCircleIntersection(
				1,
				{
					x: 0,
					y: 3,
				},
				{
					x: 3,
					y: 0,
				},
			);
			expect(c).to.be.null;
		});

		it('performs at least 10000 calls per frame', () => {
			const samples = 10000;
			const sampleRadius = 50;
			const getPoint = () => (-0.5 + Math.random()) * sampleRadius * 2;
			const args = new Array(samples).fill(0, 0, samples).map((n) => ({
				r: Math.random() * sampleRadius,
				p1: {
					x: getPoint(),
					y: getPoint(),
				},
				p2: {
					x: getPoint(),
					y: getPoint(),
				},
			}));
			const startTime = Date.now();
			for (const arg of args) Geometry.getRayUnitCircleIntersection(arg.r, arg.p1, arg.p2);
			const runTime = Date.now() - startTime;
			expect(runTime).to.be.lessThan(fpsLength);
		});
	});

	describe('isPointInsideShape', function () {
		this.slow(2 * fpsLength);
		it('takes less than a frame for 1000 runs', () => {
			const shapeRadius = 10;
			const shape = MapGenerator.i(defaultMapParameters).generateCave(0, 0, shapeRadius);
			const samples = 1000;
			const args = new Array(samples).fill(0, 0, samples).map((n) => ({
				shape: shape.shape,
				point: {
					x: (-0.5 + Math.random()) * 2 * shapeRadius,
					y: (-0.5 + Math.random()) * 2 * shapeRadius,
				},
			}));
			for (const arg of args) Geometry.isPointInsideShape(arg.point, arg.shape);
		});
	});
	describe('isPointInRange', function () {
		this.slow(2 * fpsLength);
		it('takes less than a frame for 10000 runs', () => {
			const shapeRadius = 10;
			const samples = 10000;
			const args = new Array(samples).fill(0, 0, samples).map((n) => ({
				shape: {
					x: (-0.5 + Math.random()) * 2 * shapeRadius,
					y: (-0.5 + Math.random()) * 2 * shapeRadius,
					radius: shapeRadius,
				} as InterfaceTypes.ISpot,
				point: {
					x: (-0.5 + Math.random()) * 2 * shapeRadius,
					y: (-0.5 + Math.random()) * 2 * shapeRadius,
				},
			}));
			for (const arg of args) Geometry.isPointInRange(arg.point, arg.shape);
		});
	});
	describe('doesRayIntersectShape', function () {
		this.slow(2 * fpsLength);
		it('takes less than a frame for 1000 runs', () => {
			const shapeRadius = 10;
			const getPoint = () => (-0.5 + Math.random()) * shapeRadius * 2;
			const shape = MapGenerator.i(defaultMapParameters).generateCave(0, 0, shapeRadius);
			const samples = 1000;
			const args = new Array(samples).fill(0, 0, samples).map((n) => ({
				shape: shape.shape,
				p1: {
					x: getPoint(),
					y: getPoint(),
				},
				p2: {
					x: getPoint(),
					y: getPoint(),
				},
			}));
			for (const arg of args) Geometry.doesRayIntersectShape(arg.shape, arg.p1, arg.p2);
		});
	});
});

import { expect } from 'chai';

export const Precision = 10e-4;

export const assertModifier = (source: IModifier, target: IModifier) => {
	const keys = Object.keys(target);
	if (keys.length) {
		expect(target).to.be.an('object').that.has.all.keys(source);
		expect(source).to.be.an('object').that.has.all.keys(target);
	}
	// eslint-disable-next-line guard-for-in
	for (const k of keys) {
		expect(source[k]).to.be.closeTo(target[k], Precision, `doesn't match value of ${k}`);
	}
};

export const fpsLength = 1000 / 60;

export type IBasicUnitTestCase<FunctionType extends (...args: any) => any> = {
	input: Parameters<FunctionType>;
	output: ReturnType<FunctionType>;
	msg?: string;
};

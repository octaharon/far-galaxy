import { expect } from 'chai';
import 'mocha';
import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../definitions/core/DI/injections';
import MainframeTacticalModule from '../definitions/technologies/equipment/all/Support/MainframeTacticalModule.10006';
import TacticalClusterEquipment from '../definitions/technologies/equipment/all/Tactical/TacticalCluster.20009';
import EquipmentManager from '../definitions/technologies/equipment/EquipmentManager';
import GenericEquipment from '../definitions/technologies/equipment/GenericEquipment';
import { IEquipmentMeta, TEquipmentProps } from '../definitions/technologies/types/equipment';

const Precision = 10e-2;

const sampleProperties: TEquipmentProps = {
	baseCost: 5,
};

const alteredProps: TEquipmentProps = {
	baseCost: 7,
};

const expectedProperties = {
	...sampleProperties,
	...alteredProps,
};

class SampleEquipment extends GenericEquipment {
	public static readonly id: number = 1234567;
	public baseCost: number = this.baseCost + 2;
}

const SampleMeta2 = { group: 'Enhancement' } as IEquipmentMeta;
const SampleExtends = new SampleEquipment(sampleProperties, SampleMeta2);

const SampleMeta1 = { group: 'Warfare' } as IEquipmentMeta;
const SampleInstance = new GenericEquipment(sampleProperties, SampleMeta1);

const samplePresetEquipment = new TacticalClusterEquipment(null, { group: 'Tactical' });

const samplePresetId = TacticalClusterEquipment.id;
const samplePresetId2 = MainframeTacticalModule.id;
const serializedPresetEquipment = _.omit(samplePresetEquipment.serialize(), 'instanceId');

const testFactory = new EquipmentManager();

describe('GenericEquipment', () => {
	describe('constructor', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleInstance).to.deep.include(sampleProperties);
		});
		it('correctly passes properties to SetProps', () => {
			const c = new GenericEquipment(sampleProperties, { group: null });
			c.setProps(alteredProps);
			expect(c).to.deep.include(expectedProperties);
		});
		it('ID starts with Type Descriptor', () => {
			const c = new GenericEquipment(sampleProperties, {
				group: null,
			});
			expect(c.serialize())
				.to.have.property('instanceId')
				.that.match(new RegExp(`^${c.getDescriptor(DIEntityDescriptors[DIInjectableCollectibles.equipment])}`));
		});
		it('correctly passes slot through meta', () => {
			expect(SampleInstance).property('meta').to.include(SampleMeta1);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleInstance).property('static').to.include({ id: GenericEquipment.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleInstance.serialize();
				expect(s).to.deep.include(sampleProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(GenericEquipment.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
	describe('Extension class', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleExtends).to.deep.include(expectedProperties);
		});
		it('correctly passes slot through meta', () => {
			expect(SampleExtends).property('meta').to.include(SampleMeta2);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleExtends).property('static').to.include({ id: SampleEquipment.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleExtends.serialize();
				expect(s).to.deep.include(expectedProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('id').to.equal(SampleEquipment.id);
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
	});
});

describe('EquipmentManager', () => {
	it('Fills correctly', () => {
		testFactory.fill();
		const all = testFactory.getAll();
		expect(Object.keys(all)).to.have.property('length').to.be.greaterThan(0);
		expect(Object.keys(all)).to.include(samplePresetId.toString());
	});
	it('Correctly returns a constructor', () => {
		const c = testFactory.get(samplePresetId);
		expect(c).to.be.equal(samplePresetEquipment.static);
	});
	it('Correctly instantiates without props and assigns meta', () => {
		const c = testFactory.instantiate(null, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(serializedPresetEquipment);
		expect(c).to.have.property('meta').to.include({});
	});
	it('Correctly instantiates with props', () => {
		const c = testFactory.instantiate(sampleProperties, samplePresetId);
		const s = c.serialize();
		expect(s).to.deep.include(sampleProperties);
		expect(s).to.have.property('id').to.equal(samplePresetId);
	});
	it('Correctly unserializes and assigns a meta', () => {
		const samplePresetEquipment2 = testFactory.instantiate(sampleProperties, samplePresetId2);
		const serializedPresetEquipment2 = samplePresetEquipment2.serialize();
		const sampleInstanceId = 'unserialized';
		serializedPresetEquipment2.instanceId = sampleInstanceId;
		const c = testFactory.unserialize(serializedPresetEquipment2);
		expect(c).to.deep.include(sampleProperties);
		expect(c).to.have.property('meta').to.include({});
		expect(c).to.have.property('instanceId').to.equal(sampleInstanceId);
	});
	it('Correctly clones and assigns a meta', () => {
		const samplePresetEquipment2 = testFactory.clone(samplePresetEquipment);
		const serializedPresetEquipment2 = samplePresetEquipment2.serialize();
		expect(_.omit(serializedPresetEquipment2, 'instanceId')).to.deep.include(
			_.omit(serializedPresetEquipment, 'instanceId'),
		);
		expect(samplePresetEquipment2).to.have.property('meta').to.include({});
		expect(samplePresetEquipment2)
			.to.have.property('instanceId')
			.to.not.equal(samplePresetEquipment.getInstanceId());
	});
	it('Correctly filters by abilities', () => {
		const c = testFactory.instantiate(null, samplePresetId2);
		const l = testFactory.getEquipmentWithAbilities(true);
		expect(l).to.include(c.static);
		expect(l).to.not.include(samplePresetEquipment.static);
	});
});

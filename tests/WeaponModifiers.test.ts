import { expect } from 'chai';
import 'mocha';
import { stackWeaponModifiers } from '../definitions/technologies/weapons/WeaponManager';
import { StackWGTestCases } from './fixtures/weaponModifiers.cases';
import { assertModifier } from './helpers';

describe('Weapon Modifiers', () => {
	describe('Stacking', () => {
		StackWGTestCases.forEach((t) => {
			it(t.title || `returns ${JSON.stringify(t.output)} with "${JSON.stringify(t.args)}"`, () => {
				const c = stackWeaponModifiers(...t.args);
				expect(c).to.be.an('object', "Doesn't return an object");
				if (Object.keys(t.output).length) expect(c).to.have.all.keys(t.output);
				Object.keys(t.output).forEach((objKey) => assertModifier(t.output[objKey], c[objKey]));
			});
		});
	});
});

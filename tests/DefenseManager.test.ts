/* eslint-disable @typescript-eslint/no-unused-expressions */
import chai from 'chai';
import spies from 'chai-spies';
import { describe } from 'mocha';
import _ from 'underscore';
import { cloneObject } from '../definitions/core/dataTransformTools';
import Damage from '../definitions/tactical/damage/damage';
import UnitDefense from '../definitions/tactical/damage/defense';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import { optimizeDodgeCases, stackDodgeCases } from './fixtures/dodge.cases';
import { addShieldsCases, optimizeProtectionCases } from './fixtures/protection.cases';

chai.use(spies);
const { expect, spy } = chai;
const sampleProtectionDecorator: UnitDefense.IDamageProtectionDecorator = {
	min: 2,
	max: 10,
	bias: -3,
	factor: 0.8,
	threshold: 1,
	minGuard: 0,
	maxGuard: 100,
};

const hardenedProtection = Damage.TDamageTypesArray.reduce(
	(obj, dt) => Object.assign(obj, { [dt]: { min: 0, factor: 0.5 } }),
	{},
);

describe('Defense Manager', () => {
	describe('serializeProtectionDecorator', () => {
		it('No argument', () => {
			expect(DefenseManager.serializeProtectionDecorator(undefined)).to.be.null;
		});
		it('Empty object', () => {
			expect(DefenseManager.serializeProtectionDecorator({})).to.be.null;
		});
		it('Preserves all keys when values are meaningful', () => {
			expect(DefenseManager.serializeProtectionDecorator(sampleProtectionDecorator)).to.deep.equal(
				sampleProtectionDecorator,
			);
		});
		it('Ignores bias=0', () => {
			expect(
				DefenseManager.serializeProtectionDecorator({ ...sampleProtectionDecorator, bias: 0 }),
			).to.deep.equal(_.omit(sampleProtectionDecorator, 'bias'));
		});
		it('Ignores factor=1', () => {
			expect(
				DefenseManager.serializeProtectionDecorator({ ...sampleProtectionDecorator, factor: 1 }),
			).to.deep.equal(_.omit(sampleProtectionDecorator, 'factor'));
		});
		it('Ignores threshold = 0', () => {
			expect(
				DefenseManager.serializeProtectionDecorator({ ...sampleProtectionDecorator, threshold: 0 }),
			).to.deep.equal(_.omit(sampleProtectionDecorator, 'threshold'));
		});
		it('Ignores threshold < 0', () => {
			expect(
				DefenseManager.serializeProtectionDecorator({ ...sampleProtectionDecorator, threshold: -3 }),
			).to.deep.equal(_.omit(sampleProtectionDecorator, 'threshold'));
		});
		it('Replaces min = -Infinity with min = 0', () => {
			expect(
				DefenseManager.serializeProtectionDecorator({ ...sampleProtectionDecorator, min: -Infinity }),
			).to.deep.equal({ ...sampleProtectionDecorator, min: 0 });
		});
		it('Adds min = 0 if absent', () => {
			expect(DefenseManager.serializeProtectionDecorator(_.omit(sampleProtectionDecorator, 'min'))).to.deep.equal(
				{ ...sampleProtectionDecorator, min: 0 },
			);
		});
		it('Ignores minGuard = -Infinity', () => {
			expect(
				DefenseManager.serializeProtectionDecorator({ ...sampleProtectionDecorator, minGuard: -Infinity }),
			).to.deep.equal(_.omit(sampleProtectionDecorator, 'minGuard'));
		});
		it('Ignores maxGuard = Infinity', () => {
			expect(
				DefenseManager.serializeProtectionDecorator({ ...sampleProtectionDecorator, maxGuard: Infinity }),
			).to.deep.equal(_.omit(sampleProtectionDecorator, 'maxGuard'));
		});
		it('Ignores max = Infinity', () => {
			expect(
				DefenseManager.serializeProtectionDecorator({ ...sampleProtectionDecorator, max: Infinity }),
			).to.deep.equal(_.omit(sampleProtectionDecorator, 'max'));
		});
		it(`Removes null values, doesn't mutate original`, () => {
			const sample: UnitDefense.IDamageProtectionDecorator = {
				...sampleProtectionDecorator,
				max: null,
				factor: null,
				threshold: null,
			};
			const sampleCopy = cloneObject(sample);
			expect(DefenseManager.serializeProtectionDecorator(sample)).to.deep.equal(
				_.omit(sampleProtectionDecorator, 'max', 'factor', 'threshold'),
			);
			expect(sample).to.deep.equal(sampleCopy, 'original object mutated');
		});
		it('Multiple case test #1', () => {
			const sample: UnitDefense.IDamageProtectionDecorator = {
				...sampleProtectionDecorator,
				max: Infinity,
				factor: 1,
				threshold: null,
			};
			const sampleCopy = cloneObject(sample);
			expect(DefenseManager.serializeProtectionDecorator(sample)).to.deep.equal(
				_.omit(sampleProtectionDecorator, 'max', 'factor', 'threshold'),
			);
			expect(sample).to.deep.equal(sampleCopy, 'original object mutated');
		});
		it('Multiple case test #2', () => {
			const sample: UnitDefense.IDamageProtectionDecorator = {
				...sampleProtectionDecorator,
				min: null,
				bias: 0,
				maxGuard: Infinity,
				minGuard: -Infinity,
			};
			const sampleCopy = cloneObject(sample);
			expect(DefenseManager.serializeProtectionDecorator(sample)).to.deep.equal(
				Object.assign(_.omit(sampleProtectionDecorator, 'maxGuard', 'minGuard', 'bias'), { min: 0 }),
			);
			expect(sample).to.deep.equal(sampleCopy, 'original object mutated');
		});
		it('Multiple case test #3', () => {
			const sample: UnitDefense.IDamageProtectionDecorator = {
				..._.omit(sampleProtectionDecorator, 'min'),
				bias: null,
				factor: null,
				max: Infinity,
				threshold: -5,
			};
			const sampleCopy = cloneObject(sample);
			expect(DefenseManager.serializeProtectionDecorator(sampleCopy)).to.deep.equal(
				Object.assign(_.omit(sampleProtectionDecorator, 'max', 'threshold', 'bias', 'factor'), { min: 0 }),
			);
			expect(sample).to.deep.equal(sampleCopy, 'original object mutated');
		});
	});

	describe('optimizeProtection', () => {
		optimizeProtectionCases.forEach(({ title, from, to }) =>
			it(title, () => {
				expect(DefenseManager.optimizeProtection.call(DefenseManager, from))
					.to.be.an('object')
					.that.is.deep.equal(to);
			}),
		);
	});

	describe('optimizeDodge', () => {
		optimizeDodgeCases.forEach(({ title, from, to }) =>
			it(title, () => {
				expect(DefenseManager.optimizeDodge.call(DefenseManager, from))
					.to.be.an('object')
					.that.is.deep.equal(to);
			}),
		);
	});

	describe('stackDodge', () => {
		stackDodgeCases.forEach(({ title, from, to }) =>
			it(title, () => {
				expect(DefenseManager.stackDodge(...from))
					.to.be.an('object')
					.that.is.deep.equal(to);
			}),
		);
	});

	describe('addShields', () => {
		addShieldsCases.forEach(({ title, args, to }) =>
			it(title, () => {
				const from = args[0];
				const copy = cloneObject(from);
				expect(DefenseManager.addShields(...args)).to.be.deep.equal(to);
				if (from) expect(copy).to.be.deep.equal(from, 'Mutates original object');
			}),
		);
	});

	describe('serializeProtection', () => {
		let wrapper: CallableFunction = null;
		beforeEach(() => {
			wrapper = spy.on(DefenseManager, 'serializeProtectionDecorator');
		});

		afterEach(() => {
			spy.restore();
		});

		it('No argument', () => {
			const t = DefenseManager.serializeProtection(undefined);
			expect(t).to.be.an('object').that.is.empty;
		});
		it('Empty object', () => {
			const t = DefenseManager.serializeProtection({});
			expect(t).to.be.an('object').that.is.empty;
		});
		it('Preserves extraneous keys', () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 200,
				currentAmount: 20,
				caption: 'test',
				overchargeDecay: 15,
			};
			const t = DefenseManager.serializeProtection(sampleProtection);
			expect(t).to.be.an('object').that.deep.equals(sampleProtection);
		});
		it('Preserves extraneous keys along with single protection', () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 50,
				currentAmount: 50,
				caption: 'test2',
				[Damage.damageType.warp]: {
					bias: 1,
					factor: 0.5,
					minGuard: 5,
				},
			};
			const t = DefenseManager.serializeProtection(sampleProtection);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					...sampleProtection,
					[Damage.damageType.warp]: DefenseManager.serializeProtectionDecorator(
						sampleProtection[Damage.damageType.warp],
					),
				});
		});
		it('removes useless protection decorators and properties', () => {
			const sampleProtection: UnitDefense.TArmor = {
				default: {
					factor: 0.5,
					max: 10,
					bias: -5,
					threshold: -10,
					maxGuard: Infinity,
				},
				[Damage.damageType.heat]: {
					minGuard: -Infinity,
					threshold: -5,
				},
			};
			const t = DefenseManager.serializeProtection(sampleProtection);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					default: DefenseManager.serializeProtectionDecorator(sampleProtection.default),
					[Damage.damageType.heat]: {
						min: 0,
					},
				});
		});
		it('collapses identical protection to `default` and applies `serializeProtectionDecorator`', () => {
			const sampleProtectionDecorator = {
				factor: 0.8,
				threshold: 10,
			};
			const sampleDefaultProtectionDecorator = {
				min: 5,
				max: 10,
				bias: -5,
			};
			const sampleProtection: UnitDefense.TArmor = {
				default: { ...sampleDefaultProtectionDecorator },
				[Damage.damageType.warp]: { ...sampleDefaultProtectionDecorator },
				[Damage.damageType.kinetic]: { ...sampleProtectionDecorator },
				[Damage.damageType.thermodynamic]: { ...sampleProtectionDecorator },
				[Damage.damageType.heat]: { ...sampleProtectionDecorator },
			};
			const sampleSerializedProtection = DefenseManager.serializeProtectionDecorator(sampleProtectionDecorator);
			const sampleSerializedDefaultProtection = DefenseManager.serializeProtectionDecorator(
				sampleDefaultProtectionDecorator,
			);
			const t = DefenseManager.serializeProtection(sampleProtection);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					[Damage.damageType.kinetic]: { ...sampleSerializedProtection },
					[Damage.damageType.thermodynamic]: { ...sampleSerializedProtection },
					[Damage.damageType.heat]: { ...sampleSerializedProtection },
					default: { ...sampleSerializedDefaultProtection },
				});
			expect(wrapper).to.have.been.called.exactly(10);
		});
		it('sample case #1', () => {
			const optimizeSpy = spy.on(DefenseManager, 'optimizeProtection');
			const sampleProtectionDecorator = {
				bias: -15,
				factor: 0.8,
			};
			const sampleProtection: UnitDefense.TShield = {
				caption: 'shield 1',
				shieldAmount: 101,
				currentAmount: 101,
				default: { ...sampleProtectionDecorator },
				[Damage.damageType.thermodynamic]: {
					threshold: 5,
					max: 15,
				},
				[Damage.damageType.antimatter]: {
					bias: -30,
					min: -5,
				},
				[Damage.damageType.kinetic]: {
					bias: -10,
					factor: 1,
				},
			};
			const sampleCopy = cloneObject(sampleProtection);
			const t = DefenseManager.serializeProtection(sampleProtection);
			expect(optimizeSpy).to.have.been.called.exactly(1);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					..._.omit(sampleProtection, 'default'),
					[Damage.damageType.antimatter]: { ...sampleProtection[Damage.damageType.antimatter] },
					[Damage.damageType.kinetic]: _.omit(
						{ ...sampleProtection[Damage.damageType.kinetic], min: 0 },
						'factor',
					),
					[Damage.damageType.thermodynamic]: { ...sampleProtection[Damage.damageType.thermodynamic], min: 0 },
					default: {
						...sampleProtectionDecorator,
						min: 0,
					},
				} as UnitDefense.TShield);
			expect(wrapper).to.have.been.called.exactly(8);
			expect(sampleProtection).to.deep.equal(sampleCopy, `Original object mutated`);
		});
		it('sample case #2', () => {
			const optimizeSpy = spy.on(DefenseManager, 'optimizeProtection');
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 5,
				currentAmount: 10,
				overchargeDecay: 5,
				caption: 'shield 2',
				[Damage.damageType.heat]: {
					threshold: 5,
					max: 15,
					factor: 1,
					minGuard: -5,
				},
				[Damage.damageType.radiation]: {
					bias: 0,
					maxGuard: Infinity,
				},
				[Damage.damageType.biological]: {
					bias: -10,
					factor: 0.5,
				},
			};
			const sampleCopy = cloneObject(sampleProtection);
			const t = DefenseManager.serializeProtection(sampleProtection);
			expect(optimizeSpy).to.have.been.called.exactly(1);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					..._.omit(sampleProtection, 'default'),
					[Damage.damageType.heat]: _.omit({ ...sampleProtection[Damage.damageType.heat], min: 0 }, 'factor'),
					[Damage.damageType.biological]: { ...sampleProtection[Damage.damageType.biological], min: 0 },
					[Damage.damageType.radiation]: { min: 0 },
				} as UnitDefense.TShield);
			expect(wrapper).to.have.been.called.exactly(3);
			expect(sampleProtection).to.deep.equal(sampleCopy, `Original object mutated`);
		});
	});

	describe('getTotalFlags', () => {
		it('returns empty object without args', () => {
			expect(DefenseManager.getTotalFlags()).to.be.an('object').that.is.empty;
		});
		it('returns empty object with empty objects', () => {
			expect(DefenseManager.getTotalFlags()).to.be.an('object').that.is.empty;
		});
		it('counts each flag', () => {
			Object.values(UnitDefense.defenseFlags).forEach((defenseFlag) =>
				expect(
					DefenseManager.getTotalFlags({
						[defenseFlag]: true,
					}),
				)
					.to.be.an('object')
					.that.is.deep.equal({
						[defenseFlag]: true,
					}),
			);
		});
		it('ignores falsy values unless there is truthy value', () => {
			expect(
				DefenseManager.getTotalFlags(
					{
						[UnitDefense.defenseFlags.immune]: true,
						[UnitDefense.defenseFlags.warp]: false,
					},
					{
						[UnitDefense.defenseFlags.evasion]: false,
						[UnitDefense.defenseFlags.hull]: false,
						[UnitDefense.defenseFlags.boosters]: true,
					},
					{
						[UnitDefense.defenseFlags.hull]: false,
						[UnitDefense.defenseFlags.replenishing]: false,
						[UnitDefense.defenseFlags.evasion]: true,
						[UnitDefense.defenseFlags.immune]: false,
					},
				),
			)
				.to.be.an('object')
				.that.is.deep.equal({
					[UnitDefense.defenseFlags.immune]: true,
					[UnitDefense.defenseFlags.boosters]: true,
					[UnitDefense.defenseFlags.evasion]: true,
				});
		});
		it('sample case #1', () => {
			expect(
				DefenseManager.getTotalFlags(
					{
						[UnitDefense.defenseFlags.sma]: true,
						[UnitDefense.defenseFlags.warp]: true,
					},
					undefined,
					{
						[UnitDefense.defenseFlags.boosters]: false,
					},
					{},
					{
						[UnitDefense.defenseFlags.warp]: false,
						[UnitDefense.defenseFlags.unstoppable]: true,
					},
				),
			)
				.to.be.an('object')
				.that.is.deep.equal({
					[UnitDefense.defenseFlags.sma]: true,
					[UnitDefense.defenseFlags.unstoppable]: true,
					[UnitDefense.defenseFlags.warp]: true,
				});
		});
		it('sample case #2', () => {
			expect(
				DefenseManager.getTotalFlags(
					{},
					{
						[UnitDefense.defenseFlags.resilient]: true,
					},
					{
						[UnitDefense.defenseFlags.boosters]: true,
						[UnitDefense.defenseFlags.disposable]: true,
					},
					{
						[UnitDefense.defenseFlags.warp]: true,
						[UnitDefense.defenseFlags.unstoppable]: true,
					},
				),
			)
				.to.be.an('object')
				.that.is.deep.equal({
					[UnitDefense.defenseFlags.resilient]: true,
					[UnitDefense.defenseFlags.boosters]: true,
					[UnitDefense.defenseFlags.disposable]: true,
					[UnitDefense.defenseFlags.warp]: true,
					[UnitDefense.defenseFlags.unstoppable]: true,
				});
		});
		it('sample case #3', () => {
			expect(
				DefenseManager.getTotalFlags(
					{
						[UnitDefense.defenseFlags.reactive]: true,
						[UnitDefense.defenseFlags.tachyon]: false,
					},
					{
						[UnitDefense.defenseFlags.tachyon]: true,
						[UnitDefense.defenseFlags.immune]: true,
					},
					{
						[UnitDefense.defenseFlags.tachyon]: false,
						[UnitDefense.defenseFlags.unstoppable]: false,
						[UnitDefense.defenseFlags.warp]: true,
					},
				),
			)
				.to.be.an('object')
				.that.is.deep.equal({
					[UnitDefense.defenseFlags.reactive]: true,
					[UnitDefense.defenseFlags.tachyon]: true,
					[UnitDefense.defenseFlags.immune]: true,
					[UnitDefense.defenseFlags.warp]: true,
				});
		});
	});

	describe('addResilientProtection', () => {
		let wrapper: CallableFunction = null;
		beforeEach(() => {
			wrapper = spy.on(DefenseManager, 'serializeProtection');
		});

		afterEach(() => {
			expect(wrapper).to.have.been.called();
			spy.restore();
		});

		it('No arguments', () => {
			const t = DefenseManager.addResilientProtection(undefined);
			const v = DefenseManager.addResilientProtection(null);
			expect(t)
				.to.be.an('object')
				.that.is.deep.equal({ default: { min: 0 } });
			expect(v)
				.to.be.an('object')
				.that.is.deep.equal({ default: { min: 0 } });
		});
		it('Empty object, second argument = 0', () => {
			const t = DefenseManager.addResilientProtection(undefined, 0);
			const v = DefenseManager.addResilientProtection({}, 0);
			expect(t).to.be.an('object').that.is.deep.equal(DefenseManager.optimizeProtection(hardenedProtection));
			expect(v).to.be.an('object').that.is.deep.equal(DefenseManager.optimizeProtection(hardenedProtection));
		});
		it('Preserves protection with no percentage, along with extraneous keys', () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 200,
				currentAmount: 20,
				caption: 'test',
				overchargeDecay: 15,
				[Damage.damageType.heat]: {
					factor: 0.2,
					min: 15,
				},
			};
			const t = DefenseManager.addResilientProtection(sampleProtection);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({ default: { min: 0 }, ...sampleProtection });
		});
		it('Unfolds default protection and preserves extraneous keys', () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 200,
				currentAmount: 20,
				caption: 'test2',
				overchargeDecay: 15,
				default: {
					factor: 0.2,
					min: 15,
				},
			};
			const t = DefenseManager.addResilientProtection(sampleProtection);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					..._.omit(sampleProtection, 'default'),
					...DefenseManager.serializeProtection({ default: sampleProtection.default }),
				});
		});
		it(`Applies reduction, creating extra keys, if necessary`, () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 10,
				currentAmount: 30,
				overchargeDecay: 5,
				caption: 'test3',
				[Damage.damageType.warp]: {
					bias: -10,
					factor: 0.75,
				},
			};
			const t = DefenseManager.addResilientProtection(sampleProtection, 0.5);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					..._.omit(sampleProtection, 'default'),
					...DefenseManager.serializeProtection({
						default: {
							factor: 0.75,
						},
						[Damage.damageType.warp]: {
							...sampleProtection[Damage.damageType.warp],
							factor: 0.5,
						},
					}),
				});
		});
		it(`Applies reduction down to 20% factor`, () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 10,
				currentAmount: 30,
				overchargeDecay: 5,
				caption: 'test4',
				[Damage.damageType.heat]: {
					bias: -5,
					factor: 0.5,
				},
				[Damage.damageType.magnetic]: {
					bias: -10,
				},
			};
			const t = DefenseManager.addResilientProtection(sampleProtection, 0);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					..._.omit(sampleProtection, 'default'),
					...DefenseManager.serializeProtection({
						default: {
							factor: 0.5,
						},
						[Damage.damageType.magnetic]: {
							...sampleProtection[Damage.damageType.magnetic],
							factor: 0.5,
							bias: -10,
						},
						[Damage.damageType.heat]: {
							...sampleProtection[Damage.damageType.heat],
							factor: 0.2,
						},
					}),
				});
		});
		it(`Ignores negative factor protection`, () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 10,
				currentAmount: 30,
				overchargeDecay: 5,
				caption: 'test5',
				[Damage.damageType.antimatter]: {
					bias: -5,
					factor: 0.5,
				},
				[Damage.damageType.biological]: {
					factor: -0.1,
					bias: -1,
					max: 5,
				},
			};
			const sampleCopy = cloneObject(sampleProtection);
			const t = DefenseManager.addResilientProtection(sampleProtection, 0.75);
			expect(t)
				.to.be.an('object')
				.that.deep.equals({
					..._.omit(sampleProtection, 'default'),
					...DefenseManager.serializeProtection({
						default: {
							factor: 0.875,
						},
						[Damage.damageType.antimatter]: {
							...sampleProtection[Damage.damageType.antimatter],
							factor: 0.375,
							bias: -5,
						},
						[Damage.damageType.biological]: {
							...sampleProtection[Damage.damageType.biological],
							factor: -0.1,
							max: 5,
							bias: -1,
						},
					}),
				});
			expect(sampleProtection).to.deep.equal(sampleCopy, 'Original object mutated');
		});
		it(`Negative percentage is identical to zero, original is not mutated`, () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 10,
				currentAmount: 30,
				overchargeDecay: 5,
				caption: 'test6',
				default: {
					bias: -3,
					max: 2,
					factor: 0.5,
				},
				[Damage.damageType.thermodynamic]: {
					bias: 1,
					factor: 0.8,
				},
				[Damage.damageType.kinetic]: {
					threshold: 20,
					factor: -0.2,
				},
			};
			const sampleCopy = cloneObject(sampleProtection);
			expect(DefenseManager.addResilientProtection(sampleProtection, -10))
				.to.be.an('object')
				.that.deep.equals(DefenseManager.addResilientProtection(sampleProtection, 0));
			expect(sampleProtection).to.deep.equal(sampleCopy, 'Original object mutated');
		});
		it(`Percentage higher than 1 is ignored, original is not mutated`, () => {
			const sampleProtection: UnitDefense.TShield = {
				shieldAmount: 25,
				currentAmount: 10,
				overchargeDecay: 25,
				caption: 'test7',
				[Damage.damageType.biological]: {
					bias: -10,
					max: 20,
				},
				[Damage.damageType.thermodynamic]: {
					bias: 12,
					factor: 0.75,
				},
				[Damage.damageType.radiation]: {
					threshold: 25,
					factor: -0.1,
				},
			};
			const sampleCopy = cloneObject(sampleProtection);
			expect(DefenseManager.addResilientProtection(sampleProtection, 3))
				.to.be.an('object')
				.that.deep.equals(DefenseManager.addResilientProtection(sampleProtection));
			expect(sampleProtection).to.deep.equal(sampleCopy, 'Original object mutated');
		});
	});
});

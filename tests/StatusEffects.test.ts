/* eslint-disable @typescript-eslint/no-unused-expressions */
import chai from 'chai';
import spies from 'chai-spies';
import 'mocha';
import _ from 'underscore';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';
import {
	DIEntityDescriptors,
	DIInjectableCollectibles,
	DIInjectableSerializables,
} from '../definitions/core/DI/injections';
import { ArithmeticPrecision } from '../definitions/maths/constants';
import PlayerBase from '../definitions/orbital/base/PlayerBase';
import { IPlayerBase } from '../definitions/orbital/base/types';
import GenericFactory from '../definitions/orbital/factories/GenericFactory';
import GenericPlayer from '../definitions/player/GenericPlayer';
import { stackUnitEffectsModifiers } from '../definitions/prototypes/helpers';
import { IUnitEffectModifier } from '../definitions/prototypes/types';
import UnitPrototype from '../definitions/prototypes/UnitPrototype';
import { IMapCell } from '../definitions/tactical/map';
import MapCell from '../definitions/tactical/map/MapCell';
import { DEFAULT_EFFECT_POWER } from '../definitions/tactical/statuses/constants';
import EffectEvents from '../definitions/tactical/statuses/effectEvents';
import EffectManager from '../definitions/tactical/statuses/EffectManager';
import HighHopesEffect from '../definitions/tactical/statuses/Effects/all/Faction/HighHopes.70001';
import GenericStatusEffect, {
	defaultEffectMeta,
	defaultEffectProps,
	GenericMapStatusEffect,
	GenericPlayerStatusEffect,
	GenericUnitStatusEffect,
} from '../definitions/tactical/statuses/GenericStatusEffect';
import StatusEffectMeta, { MAX_EFFECT_DURATION } from '../definitions/tactical/statuses/statusEffectMeta';
import { IEffectMeta, IStatusEffect, TEffectProps, TSerializedEffect } from '../definitions/tactical/statuses/types';
import GenericUnit from '../definitions/tactical/units/GenericUnit';
import Units from '../definitions/tactical/units/types';
import GenericArmor from '../definitions/technologies/armor/GenericArmor';
import IUnit = Units.IUnit;

chai.use(spies);
const { expect, spy } = chai;

DIContainer.inject(DIPresetDefault);

class SampleUnitEffect extends GenericUnitStatusEffect {
	public static readonly id: number = 12334556;
	public static readonly baseDuration = 3;
	public static readonly durationFixed = false;
	public static readonly unremovable = false;
}

class SampleConstantUnitEffect extends GenericUnitStatusEffect {
	public static readonly id: number = 4162510;
	public static readonly baseDuration = 12;
	public static readonly durationFixed = true;
	public static readonly unremovable = true;
}

const sampleEffectOwners: Partial<IEffectMeta<StatusEffectMeta.effectAgentTypes.unit>> = {
	source: {
		type: StatusEffectMeta.effectAgentTypes.player,
		baseId: 'sample base #3151',
	},
	target: {
		type: StatusEffectMeta.effectAgentTypes.unit,
		unitId: 'sample unit #1441',
	},
};

const sampleProperties: TEffectProps<StatusEffectMeta.effectAgentTypes.unit> = {
	power: 1.15,
	priority: -23,
	duration: 4,
	meta: {
		...sampleEffectOwners,
		basePower: 1,
		modifier: {
			abilityPowerModifier: [{ factor: 1.15, minGuard: ArithmeticPrecision }],
			abilityDurationModifier: [{ bias: 1 }],
		},
		baseDuration: 5,
		turnTick: 2,
		turnAdded: 34332,
		phaseTick: 32,
	},
	state: {
		dummyField: 3,
	},
};

const alteredProps: TEffectProps<StatusEffectMeta.effectAgentTypes.unit> = {
	power: 0.15,
	priority: 3,
	duration: 2,
};

const expectedProperties = {
	...sampleProperties,
	...alteredProps,
};

describe('GenericStatusEffect', () => {
	describe('Descriptor type guards', () => {
		it('work with Units', () => {
			const c = new GenericUnit(null);
			expect(GenericStatusEffect.isPlayerTarget(c)).to.equal(false);
			expect(GenericStatusEffect.isMapTarget(c)).to.equal(false);
			expect(GenericStatusEffect.isUnitTarget(c)).to.equal(true);
		});
		it('work with Bases', () => {
			const c = new PlayerBase(null);
			expect(GenericStatusEffect.isPlayerTarget(c)).to.equal(true);
			expect(GenericStatusEffect.isMapTarget(c)).to.equal(false);
			expect(GenericStatusEffect.isUnitTarget(c)).to.equal(false);
		});
		it('work with MapCells', () => {
			const c = new MapCell(null);
			expect(GenericStatusEffect.isPlayerTarget(c)).to.equal(false);
			expect(GenericStatusEffect.isMapTarget(c)).to.equal(true);
			expect(GenericStatusEffect.isUnitTarget(c)).to.equal(false);
		});
		it(`doesn't work with anything else`, () => {
			expect(GenericStatusEffect.isPlayerTarget(new GenericStatusEffect(null))).to.equal(false);
			expect(GenericStatusEffect.isMapTarget(new UnitPrototype(null))).to.equal(false);
			expect(GenericStatusEffect.isUnitTarget(new GenericFactory(null))).to.equal(false);
		});
	});
	describe('setSource', () => {
		it('works with Units', () => {
			const c = new GenericUnit(null);
			const n = new GenericStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setSource(c);
			expect(n.meta).to.deep.include({
				source: {
					unitId: c.getInstanceId(),
					type: StatusEffectMeta.effectAgentTypes.unit,
				},
			});
		});
		it('works with Orbital Bases', () => {
			const c = new PlayerBase(null);
			const n = new GenericStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setSource(c);
			expect(n.meta).to.deep.include({
				source: {
					baseId: c.getInstanceId(),
					type: StatusEffectMeta.effectAgentTypes.player,
				},
			});
		});
		it('works with Map Zones', () => {
			const c = new MapCell(null);
			const n = new GenericStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setSource(c);
			expect(n.meta).to.deep.include({
				source: {
					mapCellId: c.getInstanceId(),
					type: StatusEffectMeta.effectAgentTypes.map,
				},
			});
		});
		it(`doesn't work with anything else`, () => {
			const c = new MapCell(null);
			const n = new GenericStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setSource(c).setSource(new GenericArmor(null));
			expect(n.meta).to.deep.include({
				source: null,
			});
		});
		it(`can be reset with null`, () => {
			const c = new MapCell(null);
			const n = new GenericStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setSource(c).setSource(null);
			expect(n.meta).to.deep.include({
				source: null,
			});
		});
	});
	describe('setTarget', () => {
		it('assigns a Unit target to UnitEffect', () => {
			const c = new GenericUnit(null);
			const n = new GenericUnitStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c);
			expect(n.meta).to.deep.include({
				target: {
					unitId: c.getInstanceId(),
					type: StatusEffectMeta.effectAgentTypes.unit,
				},
			});
		});
		it(`doesn't assign a non-Unit target to UnitEffect #1`, () => {
			const c = new GenericPlayer(null);
			const n = new GenericUnitStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c as unknown as IUnit);
			expect(n.meta).to.deep.include({
				target: null,
			});
		});
		it(`doesn't assign a non-Unit target to UnitEffect #2`, () => {
			const c = new MapCell(null);
			const n = new GenericUnitStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c as unknown as IUnit);
			expect(n.meta).to.deep.include({
				target: null,
			});
		});
		it('assigns a Base target to PlayerEffect', () => {
			const c = new PlayerBase(null);
			const n = new GenericPlayerStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c);
			expect(n.meta).to.deep.include({
				target: {
					baseId: c.getInstanceId(),
					type: StatusEffectMeta.effectAgentTypes.player,
				},
			});
		});
		it(`doesn't assign a non-Base target to PlayerEffect #1`, () => {
			const c = new GenericPlayer(null);
			const n = new GenericPlayerStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c as unknown as IPlayerBase);
			expect(n.meta).to.deep.include({
				target: null,
			});
		});
		it(`doesn't assign a non-Base target to PlayerEffect #2`, () => {
			const c = new MapCell(null);
			const n = new GenericPlayerStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c as unknown as IPlayerBase);
			expect(n.meta).to.deep.include({
				target: null,
			});
		});
		it('assigns a MapCell target to MapEffect', () => {
			const c = new MapCell(null);
			const n = new GenericMapStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c);
			expect(n.meta).to.deep.include({
				target: {
					mapCellId: c.getInstanceId(),
					type: StatusEffectMeta.effectAgentTypes.map,
				},
			});
		});
		it(`doesn't assign a non-MapCell target to MapEffect #1`, () => {
			const c = new GenericPlayer(null);
			const n = new GenericMapStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c as unknown as IMapCell);
			expect(n.meta).to.deep.include({
				target: null,
			});
		});
		it(`doesn't assign a non-MapCell target to MapEffect #2`, () => {
			const c = new GenericArmor(null);
			const n = new GenericMapStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c as unknown as IMapCell);
			expect(n.meta).to.deep.include({
				target: null,
			});
		});
		it(`can be reset with null`, () => {
			const c = new GenericUnit(null);
			const n = new GenericUnitStatusEffect(null);
			n.injectDependencies(DIContainer.getContainer());
			c.injectDependencies(DIContainer.getContainer());
			n.setTarget(c).setTarget(null);
			expect(n.meta).to.deep.include({
				target: null,
			});
		});
	});
	describe('applyDurationModifier', () => {
		it('returns 0 without params', () => {
			expect(GenericStatusEffect.applyDurationModifier(null, null)).to.equal(0);
		});
		it(`returns original value with 1 param if it's within range`, () => {
			expect(GenericStatusEffect.applyDurationModifier(6, null)).to.equal(6);
		});
		it('returns maximum duration with 1 param if it exceeds range', () => {
			expect(GenericStatusEffect.applyDurationModifier(416, null)).to.equal(MAX_EFFECT_DURATION);
		});
		it('correctly applies the modifier', () => {
			expect(
				GenericStatusEffect.applyDurationModifier(2, {
					bias: -1,
					factor: 3,
				}),
			).to.equal(5);
		});
		it(`never goes above maximum duration of ${MAX_EFFECT_DURATION}`, () => {
			expect(
				GenericStatusEffect.applyDurationModifier(2, {
					bias: 100,
					max: 100500,
					factor: 15,
				}),
			).to.equal(MAX_EFFECT_DURATION);
		});
		it('never goes below 1', () => {
			expect(
				GenericStatusEffect.applyDurationModifier(2, {
					bias: -100,
					factor: 4,
					min: -5,
				}),
			).to.equal(1);
		});
		it(`replaces negative values with -1`, () => {
			expect(
				GenericStatusEffect.applyDurationModifier(-3, {
					bias: 3,
					factor: 5,
				}),
			).to.equal(-1);
		});
		it(`doesn't affect zero value`, () => {
			expect(
				GenericStatusEffect.applyDurationModifier(0, {
					bias: 15,
					factor: 3,
				}),
			).to.equal(0);
		});
	});
});

describe('GenericUnitStatusEffect', () => {
	const SampleExtends = new SampleUnitEffect(expectedProperties);

	const SampleInstance = new GenericUnitStatusEffect(sampleProperties);

	const samplePresetId = SampleInstance.static.id;
	const samplePresetId2 = SampleUnitEffect.id;
	const testManager = new EffectManager().fill().injectDependencies(DIContainer.getContainer());

	describe('constructor', () => {
		it('null constructor', () => {
			expect(new GenericUnitStatusEffect(null)).to.deep.include(defaultEffectProps);
		});
		it('correctly passes properties to the constructor', () => {
			expect(SampleInstance).to.deep.include(sampleProperties);
		});
		it('inherits base duration when the duration is not specified', () => {
			expect(
				new SampleUnitEffect({
					power: 1,
					priority: 5,
				}).serialize(),
			).to.deep.include({
				power: 1,
				priority: 5,
				duration: SampleUnitEffect.baseDuration,
				meta: {
					...defaultEffectMeta,
					basePower: 1,
					baseDuration: SampleUnitEffect.baseDuration,
				},
			});
		});
		it('ignores specified duration if the effect has fixed duration', () => {
			expect(
				new SampleConstantUnitEffect({
					power: 2,
					priority: 2,
					duration: 3,
				}).serialize(),
			).to.deep.include({
				power: 2,
				priority: 2,
				duration: SampleConstantUnitEffect.baseDuration,
				meta: {
					...defaultEffectMeta,
					basePower: 2,
					baseDuration: SampleConstantUnitEffect.baseDuration,
				},
			});
		});
		it('ID starts with Type Descriptor', () => {
			const c = new GenericStatusEffect(sampleProperties);
			expect(c.serialize())
				.to.have.property('instanceId')
				.that.match(new RegExp(`^${c.getDescriptor(DIEntityDescriptors[DIInjectableCollectibles.effect])}`));
		});
		it('correctly passes properties to SetProps', () => {
			const c = new GenericUnitStatusEffect(sampleProperties);
			c.setProps(alteredProps);
			expect(c).to.deep.include(expectedProperties);
		});

		it('correctly passes Collectible properties', () => {
			expect(SampleInstance).property('static').to.include({ id: samplePresetId });
		});
		it('correctly serializes to Props', (done) => {
			try {
				const s = new SampleUnitEffect(sampleProperties).serialize();
				expect(s).to.deep.include(sampleProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('meta');
				expect(s).to.have.property('id').to.equal(SampleUnitEffect.id);
				done();
			} catch (e) {
				console.error(e.toString().substring(0, 100));
				done(e);
			}
		});
	});

	describe('extension class', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleExtends).to.deep.include(expectedProperties);
		});
		it('correctly passes Collectible properties', () => {
			expect(SampleExtends).property('static').to.include({ id: SampleUnitEffect.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleExtends.serialize();
				expect(s).to.deep.include(expectedProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('meta').that.deep.includes(expectedProperties.meta);
				expect(s).to.have.property('id').to.equal(samplePresetId2);
				done();
			} catch (e) {
				console.error(e.toString());
				done(e);
			}
		});
	});

	describe('extension constant class', () => {
		it('correctly passes properties to the constructor', () => {
			expect(new SampleConstantUnitEffect(null)).to.deep.include({
				...defaultEffectProps,
				duration: SampleConstantUnitEffect.baseDuration,
				power: DEFAULT_EFFECT_POWER,
			});
		});
		it('correctly passes Collectible properties', () => {
			expect(new SampleConstantUnitEffect(expectedProperties))
				.property('static')
				.to.include({ id: SampleConstantUnitEffect.id });
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				const instance = new SampleConstantUnitEffect(expectedProperties);
				s = instance.serialize();
				expect(s).to.deep.include(expectedProperties);
				expect(s).to.have.property('instanceId');
				expect(s).to.have.property('meta').that.deep.includes(expectedProperties.meta);
				expect(s).to.have.property('id').to.equal(SampleConstantUnitEffect.id);
				done();
			} catch (e) {
				console.error(e.toString());
				done(e);
			}
		});
	});

	// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/14

	describe('applyModifier', () => {
		let sampleEffect: HighHopesEffect;
		let constantEffect: SampleConstantUnitEffect;
		let snapshot: TSerializedEffect<any>;
		let constantSnapshot: TSerializedEffect<any>;

		beforeEach(() => {
			sampleEffect = new HighHopesEffect(null);
			constantEffect = new SampleConstantUnitEffect(null);
			sampleEffect.injectDependencies(DIContainer.getContainer());
			constantEffect.injectDependencies(DIContainer.getContainer());
			snapshot = sampleEffect.serialize();
			constantSnapshot = constantEffect.serialize();
		});

		afterEach(() => {
			sampleEffect = null;
			constantEffect = null;
			snapshot = null;
			constantSnapshot = null;
		});

		it(`doesn't make changes without parameters`, () => {
			sampleEffect.applyModifier(null);
			expect(sampleEffect.serialize()).to.deep.equal(snapshot);

			constantEffect.applyModifier(null);
			expect(constantEffect.serialize()).to.deep.equal(constantSnapshot);
		});

		it(`doesn't apply duration modifier to constant effect`, () => {
			constantEffect.applyModifier({
				statusDurationModifier: [
					{
						bias: 2,
					},
				],
			});
			expect(constantEffect.serialize()).to.deep.equal(constantSnapshot);
		});

		it(`applies duration modifier to a generic effect`, () => {
			sampleEffect.applyModifier({
				statusDurationModifier: [
					{
						bias: 2,
					},
				],
				abilityDurationModifier: [
					{
						bias: 3,
					},
				],
			});
			snapshot.duration += 2;
			expect(sampleEffect.serialize()).to.deep.equal(snapshot);
		});

		it(`applies power mod to both constant and generic effect`, () => {
			constantEffect.applyModifier({
				abilityPowerModifier: [
					{
						factor: 1.5,
					},
				],
			});
			constantSnapshot.power *= 1.5;
			expect(constantEffect.serialize()).to.deep.equal(constantSnapshot);
			sampleEffect.applyModifier({
				abilityPowerModifier: [
					{
						factor: 1.5,
					},
				],
			});
			snapshot.power *= 1.5;
			expect(sampleEffect.serialize()).to.deep.equal(snapshot);
		});

		it(`is saved into modifier stack`, () => {
			const modifier: IUnitEffectModifier = {
				abilityPowerModifier: [
					{
						factor: 2,
						bias: 2,
					},
				],
			};
			const modifier2: IUnitEffectModifier = {
				abilityRangeModifier: [
					{
						bias: 5,
					},
				],
			};
			sampleEffect.applyModifier(modifier, true);
			sampleEffect.applyModifier(modifier2, true);
			snapshot.power = snapshot.power * 2 + 2;
			snapshot.meta = Object.assign(snapshot.meta, {
				modifier: stackUnitEffectsModifiers(modifier, modifier2),
			});
			expect(sampleEffect.serialize()).to.deep.equal(snapshot);
		});
		it(`applies power to callback stack`, () => {
			const target = new GenericUnit({
				maxHitPoints: 100,
			});
			DIContainer.getProvider(DIInjectableSerializables.unit).store(target);
			sampleEffect.setTarget(target);
			const modifier: IUnitEffectModifier = {
				abilityPowerModifier: [
					{
						factor: 2,
					},
				],
			};
			const sampleCopy = testManager.clone(sampleEffect as IStatusEffect<StatusEffectMeta.effectAgentTypes.unit>);
			sampleCopy.applyModifier(modifier).setTarget(target);
			const state = sampleEffect.triggerEvent(EffectEvents.TUnitEffectEvents.apply, null, target);
			const stateCopy = sampleCopy.triggerEvent(EffectEvents.TUnitEffectEvents.apply, null, target);
			expect(state).to.have.property('bonusHp').that.is.greaterThan(0);
			expect(stateCopy).to.have.property('bonusHp').that.is.greaterThan(state.bonusHp);
			expect(state).to.have.property('bonusArmor').that.is.greaterThan(0);
			expect(stateCopy).to.have.property('bonusArmor').that.is.greaterThan(state.bonusArmor);
		});
	});
});

describe('EffectManager', () => {
	const source = new GenericUnit(null);
	const target = new GenericUnit(null);
	DIContainer.getProvider(DIInjectableSerializables.unit)
		.injectDependencies(DIContainer.getContainer())
		.store(source)
		.store(target);
	const SampleExtends = new HighHopesEffect(alteredProps).injectDependencies(DIContainer.getContainer());
	const samplePresetId = HighHopesEffect.id;
	const serializedPresetFactory = _.omit(SampleExtends.serialize(), 'instanceId');
	const testManager = new EffectManager().injectDependencies(DIContainer.getContainer());
	describe('Generic methods', () => {
		it('Fills correctly', () => {
			testManager.fill();
			const all = testManager.getAll();
			expect(Object.keys(all)).to.have.property('length').to.be.greaterThan(0);
			expect(Object.keys(all)).to.include(samplePresetId.toString());
		});
		it('Correctly returns a constructor', () => {
			const c = testManager.get(samplePresetId);
			expect(c).to.be.equal(SampleExtends.constructor);
		});
		it('Correctly instantiates with props and no source or target', () => {
			const c = testManager.instantiate(alteredProps, samplePresetId);
			const s = c.serialize();
			expect(c).to.deep.include({
				...alteredProps,
			});
			expect(c.meta).to.deep.include({
				baseDuration: alteredProps.duration,
				basePower: alteredProps.power,
				source: null,
				target: null,
			});
			expect(s).to.have.property('id').to.equal(samplePresetId);
		});
		it('Correctly instantiates with props, source and target', () => {
			const c = testManager.instantiate(alteredProps, samplePresetId, source, target);
			const s = c.serialize();
			expect(c).to.deep.include({
				...alteredProps,
			});
			expect(c.meta).to.deep.include({
				baseDuration: alteredProps.duration,
				basePower: alteredProps.power,
				source: testManager.getSerializedFromTarget(source),
				target: testManager.getSerializedFromTarget(target),
			});
			expect(s).to.have.property('id').to.equal(samplePresetId);
		});
		it('Correctly unserializes', () => {
			const SampleExtends2 = testManager.instantiate(alteredProps, samplePresetId, source, target);
			const serializedPresetFactory2 = SampleExtends2.serialize();
			const sampleInstanceId = 'unserialized';
			serializedPresetFactory2.instanceId = sampleInstanceId;
			const c = testManager.unserialize(serializedPresetFactory2);
			expect(c).to.deep.include({
				...alteredProps,
			});
			expect(c.meta).to.deep.include({
				source: testManager.getSerializedFromTarget(source),
				target: testManager.getSerializedFromTarget(target),
			});
			expect(c.source?.serialize()).to.have.property('instanceId').that.equals(source.getInstanceId());
			expect(c.target?.serialize()).to.have.property('instanceId').that.equals(target.getInstanceId());
			expect(c).to.have.property('instanceId').to.equal(sampleInstanceId);
		});
		it('Correctly clones', () => {
			SampleExtends.setSource(target).setTarget(source);
			const SampleExtends2 = testManager.clone(SampleExtends);
			const serializedPresetFactory2 = SampleExtends2.serialize();
			expect(_.omit(serializedPresetFactory2, 'instanceId')).to.deep.include(
				_.omit(SampleExtends.serialize(), 'instanceId'),
			);
			expect(SampleExtends2.meta).to.deep.include({
				source: testManager.getSerializedFromTarget(target),
				target: testManager.getSerializedFromTarget(source),
			});
			expect(SampleExtends2).to.have.property('instanceId').to.not.equal(SampleExtends.getInstanceId());
		});
		it('Applies store callback if called correctly', () => {
			const newEffect = testManager.instantiate(alteredProps, samplePresetId, source, target);
			const expectedProps = newEffect.serialize();
			const cb = spy((t: IStatusEffect<any>) => {
				expect(t).to.equal(newEffect);
				const ts = t.serialize();
				expect(ts).to.deep.include(expectedProps, 'has changed their props during Store');
			});
			testManager.store(newEffect, cb);
			expect(cb).to.have.been.called.with(newEffect);
			expect(cb).to.have.been.called.once;
		});
		it('passes down turn meta', () => {
			testManager.setTick(111);
			testManager.setTurn(21110, 0);
			const newEffect = testManager.instantiate(null, samplePresetId, source, target);
			const expectedProps = newEffect.serialize();
			expect(expectedProps.meta).to.deep.include({
				turnTick: 0,
				turnAdded: 21110,
				phaseTick: 111,
			});
		});
	});
});

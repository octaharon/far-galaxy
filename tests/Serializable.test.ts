import { expect } from 'chai';
import 'mocha';
import Serializable from '../definitions/core/Serializable';

type TProps = {
	a: number;
	b: string;
};
type TSerialized = TProps & IWithInstanceID;

class SampleClass extends Serializable<TProps, TSerialized> implements TProps {
	public a: number;
	public b: string;

	protected get typeDescriptor() {
		return 'TEST_CLASS';
	}

	public serialize() {
		return {
			...this.__serializeSerializable(),
			a: this.a,
			b: this.b,
		};
	}
}

const sampleProperties: TProps = {
	a: 3,
	b: 'abcd',
};

const SampleInstance = new SampleClass(sampleProperties);

describe('Serializable', () => {
	describe('constructor', () => {
		it('correctly passes properties to the constructor', () => {
			expect(SampleInstance).to.deep.include(sampleProperties);
		});
		it('correctly passes properties to SetProps', () => {
			const c = new SampleClass(sampleProperties);
			c.setProps({
				a: 5,
			});
			expect(c).to.deep.include({
				...sampleProperties,
				a: 5,
			});
		});
		it('correctly serializes to Props', (done) => {
			let s;
			try {
				s = SampleInstance.serialize();
				expect(s).to.deep.include(sampleProperties);
				expect(s).to.have.property('instanceId').not.empty;
				done();
			} catch (e) {
				console.error(e.toString().substr(0, 100));
				done(e);
			}
		});
		it('ID starts with Type Descriptor', () => {
			const c = new SampleClass(sampleProperties);
			const s = c.serialize();
			expect(s)
				.to.have.property('instanceId')
				.that.match(new RegExp(`^${SampleInstance.getDescriptor()}`));
		});
	});
});

import Weapons from '../definitions/technologies/types/weapons';
import { describeWeapon } from '../definitions/technologies/weapons/WeaponManager';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';

DIContainer.inject(DIPresetDefault);
const provider = DIContainer.getProvider(DIInjectableCollectibles.weapon);
Object.keys(provider.weaponGroups).forEach((k: Weapons.TWeaponGroupType) => {
	const wG = provider.weaponGroups[k] as Weapons.IWeaponGroup;
	console.log(`## ${wG.caption}`, '\n');
	Object.values(wG.weapons).forEach((wId: number) => {
		console.log(describeWeapon(provider.instantiate(null, wId)), '\n');
	});
});

const c = DIContainer.getProvider(DIInjectableCollectibles.chassis).get(80003);
console.log(c);

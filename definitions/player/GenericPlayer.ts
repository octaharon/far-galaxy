import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles, DIInjectableSerializables } from '../core/DI/injections';
import { isEmptyObject } from '../core/helpers';
import Serializable from '../core/Serializable';
import BasicMaths from '../maths/BasicMaths';
import Series from '../maths/Series';
import { IPlayerBase, TPlayerBaseModifier } from '../orbital/base/types';
import { TBaseBuildingType } from '../orbital/types';
import { stackPrototypeModifiers } from '../prototypes/helpers';
import { IPrototypeModifier } from '../prototypes/types';
import { getPlayerStatProperty } from '../stats/helpers';
import GameStats from '../stats/types';
import Units from '../tactical/units/types';
import UnitChassis from '../technologies/types/chassis';
import { getAllFactionIds, getFactionBaseModifiers, getFactionUnitModifiers } from '../world/factionIndex';
import PoliticalFactions from '../world/factions';
import { addResourcePayloads } from '../world/resources/helpers';
import Resources from '../world/resources/types';
import {
	CHASSIS_UPGRADE_PROJECT_COST,
	CHASSIS_UPGRADE_REPEATABLE_COST,
	DEFAULT_FACTION_RESEARCH_COST,
	DEFAULT_PLAYER_LEVEL_XP,
	DEFAULT_PLAYER_LEVEL_XP_INCREASE,
	DEFAULT_PLAYER_RESEARCH_COST,
	DEFAULT_PLAYER_RESEARCH_XP,
	PLAYER_ENGINEERING_PROJECT_REPEATABLE_COST,
	PLAYER_REPEATABLE_RESEARCH_FACTOR,
} from './constants';
import { getGlobalEngineeringProject } from './Engineering';
import { getChassisUpgrade, getChassisUpgradeProjectId, getChassisUpgradeSelectionById } from './Engineering/upgrades';
import { isArsenalEngineeringTarget } from './helpers';
import { TPlayerId } from './playerId';
import { dehydrateResearch } from './Research';
import { TPlayerResearch } from './Research/types';
import TalentRegistry from './Talents/TalentRegistry';
import { dehydrateTalent, hydrateTalent, TPlayerTalent } from './Talents/types';
import Players from './types';
import TPlayerChassisUpgradeProject = Players.TPlayerChassisUpgradeProject;
import TPlayerGlobalEngineeringProject = Players.TPlayerGlobalEngineeringProject;

export default class GenericPlayer
	extends Serializable<Players.TPlayerProps, Players.TSerializedPlayer>
	implements Players.IPlayer
{
	public id: TPlayerId = null;
	public nickname: string;
	public faction: PoliticalFactions.TFactionID;
	public arsenal: Players.TPlayerTechnologyPayload;
	public xp: number;
	public resourceSupply: Resources.TResourcePayload;
	public level: number;
	public talents: Record<number, TPlayerTalent>;
	public researches: Record<number, TPlayerResearch>;
	public selectedResearch: number | null; // @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/28
	public selectedEngineering: { type: Players.TPlayerEngineeringTarget; id: number };
	public buildingUpgradePoints: number; // @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/11
	public talentPoints: number;
	public abilitySlots: number; //@TODO https://gitlab.com/octaharon/far-galaxy/-/issues/21
	public abilityPower: number;
	public abilitiesAvailable: number[];
	public unitModifier: Players.TPlayerUnitModifier;
	public baseModifier: TPlayerBaseModifier;
	public stats: GameStats.TPlayerStats;
	public bases: Record<string, IPlayerBase>;
	public reputation: EnumProxy<PoliticalFactions.TFactionID, number>;
	public completedEngineeringProjects: TPlayerGlobalEngineeringProject[];
	public chassisUpgradeProjects: Players.TPlayerChassisUpgradeProject[];

	public constructor(p: Partial<Players.TPlayerStaticProps & Players.TPlayerDynamicProps>) {
		super(p);
		if (!p?.xp) this.xp = 0;
		if (!p?.level) this.level = 0;
		if (!p?.bases) this.bases = {};
		if (!this.unitModifier) this.unitModifier = {};
		if (!this.completedEngineeringProjects?.length) this.completedEngineeringProjects = [];
		if (!this.chassisUpgradeProjects?.length) this.chassisUpgradeProjects = [];
		if (isEmptyObject(this.reputation))
			this.reputation = getAllFactionIds().reduce(
				(obj, factionId) =>
					Object.assign(obj, {
						[factionId]: 0,
					}),
				{},
			);
		// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/22
	}

	public get basesArray() {
		return Object.values(this.bases);
	}

	public get dockedUnits() {
		return this.basesArray.reduce((arr, base) => arr.concat(base.unitsAtBay), [] as Units.IUnit[]);
	}

	public get totalGlobalEngineeringProjectsCompleted() {
		if (!this.completedEngineeringProjects.length) return 0;
		const last = this.completedEngineeringProjects[this.completedEngineeringProjects.length - 1];
		if (last.points >= last.cost) return this.completedEngineeringProjects.length;
		return this.completedEngineeringProjects.length - 1;
	}

	public get currentGlobalEngineeringProject() {
		if (!this.completedEngineeringProjects.length) return null;
		const last = this.completedEngineeringProjects[this.completedEngineeringProjects.length - 1];
		if (last.points >= last.cost) return null;
		return last;
	}

	public get currentGlobalEngineeringProjectProgress() {
		if (!this.completedEngineeringProjects.length) return null;
		const last = this.completedEngineeringProjects[this.completedEngineeringProjects.length - 1];
		if (last.points >= last.cost) return null;
		return last.points / last.cost;
	}

	public get currentChassisUpgradeProject() {
		if (!this.chassisUpgradeProjects.length) return null;
		const last = this.chassisUpgradeProjects[this.chassisUpgradeProjects.length - 1];
		if (last.points >= last.cost) return null;
		return last;
	}

	public get currentChassisUpgradeProjectProgress() {
		if (!this.chassisUpgradeProjects.length) return null;
		const last = this.chassisUpgradeProjects[this.chassisUpgradeProjects.length - 1];
		if (last.points >= last.cost) return null;
		return last.points / last.cost;
	}

	public get totalChassisUpgradeProjectsCompleted() {
		if (!this.chassisUpgradeProjects.length) return 0;
		const last = this.chassisUpgradeProjects[this.chassisUpgradeProjects.length - 1];
		if (last.points >= last.cost) return this.chassisUpgradeProjects.length;
		return this.chassisUpgradeProjects.length - 1;
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableSerializables.player];
	}

	public getNextLevelXp(level: number = this.level ?? 0): number {
		return ((2 * DEFAULT_PLAYER_LEVEL_XP + (level - 1) * DEFAULT_PLAYER_LEVEL_XP_INCREASE) * level) / 2;
	}

	public isResearchAvailable(researchId: number): boolean {
		const r = this.researches[researchId];
		if (!r) return false;
		return (
			(r.progress || 0) < r.cost &&
			(!this.researches[researchId].prerequisiteResearchIds ||
				this.researches[researchId].prerequisiteResearchIds.every((rId) => this.researches[rId].level >= 1))
		);
	}

	public switchResearch(researchId: number) {
		// @TODO implement faction research choice https://gitlab.com/octaharon/far-galaxy/-/issues/28
		if (this.isResearchAvailable(researchId)) this.selectedResearch = researchId;
		return this;
	}

	public setEngineeringTarget(type: Players.TPlayerEngineeringTarget, id: number) {
		if (isArsenalEngineeringTarget(type)) {
			this.selectedEngineering = { type, id };
		} else {
			if (type === 'project') {
				const project = getGlobalEngineeringProject(id);
				if (
					this.selectedEngineering.id === id ||
					project.minLevel > this.level ||
					(this.currentGlobalEngineeringProject && this.currentGlobalEngineeringProject.id !== id)
				)
					return this;
				if (!this.currentGlobalEngineeringProject)
					this.completedEngineeringProjects = [
						...this.completedEngineeringProjects,
						{
							id,
							points: 0,
							cost: BasicMaths.applyModifier(getGlobalEngineeringProject(id).cost, {
								factor:
									1 +
									(PLAYER_ENGINEERING_PROJECT_REPEATABLE_COST - 1) *
										this.totalGlobalEngineeringProjectsCompleted,
							}),
						},
					];
				this.selectedEngineering = { type, id };
			}
			if (type === 'upgrade') {
				const selection = getChassisUpgradeSelectionById(id);
				if (
					this.selectedEngineering.id === id ||
					(this.currentChassisUpgradeProject &&
						getChassisUpgradeProjectId(
							this.currentChassisUpgradeProject.chassisId,
							this.currentChassisUpgradeProject.upgradeId,
						) !== id) ||
					!this.isChassisUpgradeProjectAvailable(selection.chassis, selection.upgradeId)
				)
					return this;
				if (!this.currentChassisUpgradeProject)
					this.chassisUpgradeProjects = [
						...this.chassisUpgradeProjects,
						{
							points: 0,
							chassisId: selection.chassis,
							upgradeId: selection.upgradeId,
							cost: BasicMaths.applyModifier(CHASSIS_UPGRADE_PROJECT_COST, {
								factor:
									1 +
									(CHASSIS_UPGRADE_REPEATABLE_COST - 1) * this.totalChassisUpgradeProjectsCompleted,
							}),
						},
					];
				this.selectedEngineering = { type, id };
			}
		}
		return this;
	}

	public getUnitModifiers(chassisClass?: UnitChassis.chassisClass): IPrototypeModifier[] {
		const factionModifiers = getFactionUnitModifiers(this.faction);
		if (!chassisClass)
			return [this.unitModifier?.default, ...factionModifiers.map((v) => v.default)].filter(Boolean);
		return [
			this.unitModifier?.[chassisClass] || this.unitModifier?.default || null,
			...factionModifiers.map((v) => v?.[chassisClass] || v?.default || null),
		].filter(Boolean);
	}

	public getBaseModifiers(): TPlayerBaseModifier[] {
		return [this.baseModifier, ...getFactionBaseModifiers(this.faction)];
	}

	public getKnownTechnologyCount(type?: keyof Players.TPlayerTechnologyPayload): number {
		const count = _.mapObject(this.arsenal, (idMap) => Object.values(idMap).filter((v) => v >= 1).length);
		if (type) return count[type];
		return Series.sum(Object.values(count));
	}

	public investTech(researchPoints = 0, engineeringPoints = 0): this {
		if (this.selectedResearch) {
			// @TODO implement faction research research point investment https://gitlab.com/octaharon/far-galaxy/-/issues/28
			const research = this.researches[this.selectedResearch];
			research.progress = BasicMaths.applyModifier(research.progress, {
				bias: researchPoints,
				max: research.cost,
			});
			this.stats.researchOutput += researchPoints;
			if (research.progress >= research.cost) {
				this._researchComplete(this.selectedResearch);
			}
		}
		if (this.selectedEngineering) {
			this.stats.engineeringOutput += engineeringPoints;
			if (isArsenalEngineeringTarget(this.selectedEngineering.type)) {
				const v = this.arsenal[this.selectedEngineering.type][this.selectedEngineering.id];
				if (v < 1) {
					const tech = this._findResearchByTechnology(
						this.selectedEngineering.type,
						this.selectedEngineering.id,
					);
					this.arsenal[this.selectedEngineering.type][this.selectedEngineering.id] = BasicMaths.applyModifier(
						v,
						{
							bias: engineeringPoints / (tech ? tech.cost : DEFAULT_FACTION_RESEARCH_COST),
							max: 1,
						},
					);
					// blueprint obtained
					if (this.arsenal[this.selectedEngineering.type][this.selectedEngineering.id] === 1) {
						this._engineeringProjectComplete();
						if (tech && this._isAllResearchTechObtained(tech)) this._researchComplete(tech.id);
					}
				}
			} else {
				switch (this.selectedEngineering.type) {
					case 'project':
						// Global Engineering Project
						const currentEngineeringProject = this.currentGlobalEngineeringProject;
						if (currentEngineeringProject) {
							currentEngineeringProject.points += engineeringPoints;
							if (this.currentGlobalEngineeringProjectProgress >= 1)
								this._engineeringProjectComplete(this.selectedEngineering.id);
						} else this.selectedEngineering = null;
						break;
					case 'upgrade':
					default:
						// Chassis upgrade project
						const currentProject = this.currentChassisUpgradeProject;
						if (currentProject) {
							currentProject.points += engineeringPoints;
							if (this.currentChassisUpgradeProjectProgress >= 1) {
								this._chassisUpgradeProjectComplete(currentProject);
							} else this.selectedEngineering = null;
						}
				}
			}
		}
		return this;
	}

	public isChassisUpgradeProjectAvailable(chassisId: UnitChassis.chassisClass, projectId?: number): boolean {
		return (
			// there shouldn't be another ongoing chassis upgrade project
			(!this.currentChassisUpgradeProject ||
				getChassisUpgradeProjectId(chassisId, projectId) ===
					getChassisUpgradeProjectId(
						this.currentChassisUpgradeProject.chassisId,
						this.currentChassisUpgradeProject.upgradeId,
					)) &&
			// a player has completed at least 1 Reverse Engineering project
			getPlayerStatProperty(this, 'reverseEngineeringProjectsComplete') > 0 &&
			// a player has produced and ranked at least 1 Unit of this type
			Object.values(this.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(chassisId)).filter(
				(ch) =>
					getPlayerStatProperty(this, 'ranksGainedByChassisId', ch.id) > 0 &&
					getPlayerStatProperty(this, 'unitsProducedByChassisId', ch.id) > 0,
			).length > 0 &&
			// a given upgrade ID doesn't include weapons or the chassis has them
			(!projectId ||
				!getChassisUpgrade(projectId).weapon ||
				Object.values(this.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(chassisId)).some(
					(ch) => ch.weaponSlots.length > 0,
				))
		);
	}

	public addTechnologies(p: Players.TPlayerTechnologyPayload): this {
		Object.keys(p).forEach((techType: keyof Players.TPlayerTechnologyPayload) => {
			if (!this.arsenal[techType]) this.arsenal[techType] = {};
			Object.keys(p[techType]).forEach((t) => {
				const itemId = t;
				const v = this.arsenal?.[techType]?.[itemId] || 0;
				if (!this.arsenal[techType]) this.arsenal[techType] = {};
				this.arsenal[techType][itemId] = BasicMaths.applyModifier(v, {
					bias: p[techType][itemId],
					max: 1,
				});
				if (v < 1 && this.arsenal[techType][itemId] >= 1) {
					if (
						this.selectedEngineering &&
						this.selectedEngineering.type === techType &&
						this.selectedEngineering.id === itemId
					)
						this.selectedEngineering = null;
					const c = this._findResearchByTechnology(techType, itemId);
					if (c) this._completeResearchIfPossible(c.id);
				}
			});
		});
		return this;
	}

	public addReputation(faction: PoliticalFactions.TFactionID, value: number) {
		if (!Number.isFinite(value)) return this;
		this.reputation = Object.assign({}, this.reputation, {
			[faction]: this.reputation[faction] + value,
		});
		return this;
	}

	public hasEnoughResources(r: Resources.TResourcePayload): boolean {
		for (const t of Object.keys(r) as Resources.resourceId[])
			if ((this.resourceSupply[t] || 0) < r[t]) return false;
		return true;
	}

	public spendResources(r: Resources.TResourcePayload): this {
		if (!this.hasEnoughResources(r)) return this;
		for (const t of Object.keys(r) as Resources.resourceId[]) this.addResource(t, -r[t]);
		this.stats.resourcesSpentByType = addResourcePayloads(this.stats.resourcesSpentByType || {}, r);
		return this;
	}

	public addResource(id: Resources.resourceId, amount: number): this {
		this.resourceSupply[id] = BasicMaths.applyModifier(this.resourceSupply[id] || 0, {
			bias: amount,
			min: 0,
		});
		this.stats.resourcesGainedByType = addResourcePayloads(this.stats.resourcesGainedByType || {}, {
			[id]: amount,
		});
		return this;
	}

	public serialize(): Players.TSerializedPlayer {
		this._checkIfAchievementUnlocked();
		return {
			...this.__serializeSerializable(),
			resourceSupply: this.resourceSupply,
			abilitySlots: this.abilitySlots,
			id: this.id,
			nickname: this.nickname,
			faction: this.faction,
			bases: this.bases ? _.mapObject(this.bases, (b) => b.serialize()) : null,
			arsenal: this.arsenal,
			baseModifier: this.baseModifier,
			xp: this.xp,
			level: this.level,
			stats: this.stats,
			buildingUpgradePoints: this.buildingUpgradePoints,
			talentPoints: this.talentPoints,
			researches: _.mapObject(this.researches || {}, (r) => dehydrateResearch(r)),
			talents: _.mapObject(this.talents || {}, (r) => dehydrateTalent(r)),
			selectedEngineering: this.selectedEngineering,
			selectedResearch: this.selectedResearch,
			unitModifier: this.unitModifier || {},
			abilityPower: this.abilityPower,
			abilitiesAvailable: this.abilitiesAvailable,
			completedEngineeringProjects: this.completedEngineeringProjects,
			reputation: this.reputation,
		};
	}

	public addXp(xp: number) {
		this.xp += xp;
		// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/1
		this._checkIfAchievementUnlocked();
		return this;
	}

	public addResourcePayload(r: Resources.TResourcePayload) {
		if (r) Object.keys(r).forEach((resourceID) => this.addResource(resourceID, r[resourceID]));
		return this;
	}

	public isWeaponAvailable(weaponId: number) {
		return this.arsenal?.weapons?.[weaponId] >= 1;
	}

	public isArmorAvailable(armorId: number) {
		return this.arsenal?.armor?.[armorId] >= 1;
	}

	public isShieldAvailable(shieldId: number) {
		return this.arsenal?.shield?.[shieldId] >= 1;
	}

	public isChassisAvailable(chassisId: number) {
		return this.arsenal?.chassis?.[chassisId] >= 1;
	}

	public isEquipmentAvailable(equipmentId: number) {
		return this.arsenal?.equipment?.[equipmentId] >= 1;
	}

	public isBuildingAvailable(buildingType: TBaseBuildingType, buildingId: number): boolean {
		const buildingKey =
			buildingType === TBaseBuildingType.factory
				? 'factories'
				: buildingType === TBaseBuildingType.facility
				? 'facilities'
				: buildingType === TBaseBuildingType.establishment
				? 'establishments'
				: null;
		if (!buildingKey) return false;
		return this.arsenal[buildingKey][buildingId] >= 1;
	}

	public learnTalent(talentId: number): this {
		if (this.talentPoints <= 0) return this;
		if (!this.talents[talentId])
			this.talents[talentId] = hydrateTalent({
				id: talentId,
				learned: false,
				playerId: this.id,
			});
		if (!this.talents[talentId].learnable || this.talents[talentId].learned) return this;
		if ((this.talents[talentId].prerequisiteTalentIds || []).find((t) => !this.talents[t].learned)) return this;
		if (this.talents[talentId].levelReq && this.level < this.talents[talentId].levelReq) return this;
		this.talentPoints--;
		this._talentLearned(talentId);
		return this;
	}

	public isTalentAvailable(talentId: number): boolean {
		return (
			!this.talents?.[talentId]?.learned &&
			!(TalentRegistry.get(talentId).prerequisiteTalentIds ?? []).find((t) => !this.talents?.[t]?.learned) &&
			TalentRegistry.get(talentId).learnable &&
			(TalentRegistry.get(talentId).levelReq ?? 0) <= this.level
		);
	}

	public isTalentLearned(talentId: number): boolean {
		return this.talents?.[talentId]?.learned;
	}

	public learnAbility(abilityId: number, unlearn?: boolean) {
		if (unlearn) this.abilitiesAvailable = _.without(this.abilitiesAvailable, abilityId);
		else if (!this.isAbilityKnown(abilityId)) this.abilitiesAvailable.push(abilityId);
		return this;
	}

	public isAbilityKnown(abilityId: number): boolean {
		return (this.abilitiesAvailable ?? []).includes(abilityId);
	}

	public addUnitModifier(chassis: keyof Players.TPlayerUnitModifier, ...modifiers: IPrototypeModifier[]) {
		if (!this.unitModifier) {
			this.unitModifier = {};
		}
		const mod = this.unitModifier[chassis] || {};
		const newMod = stackPrototypeModifiers(...modifiers);
		this.unitModifier[chassis] = stackPrototypeModifiers(mod, newMod);
		this.dockedUnits.forEach((unit) => unit.applyPrototypeModifier(newMod));
		return this;
	}

	protected _findResearchByTechnology(type: keyof Players.TPlayerTechnologyPayload, id: number): TPlayerResearch {
		return Object.values(this.researches).find((r) => r.technologyReward?.[type]?.[id] && !r.repeatable);
	}

	protected _isAllResearchTechObtained(research: TPlayerResearch) {
		if (!research || !research.technologyReward || !Object.keys(research.technologyReward).length) return false;
		return Object.keys(research.technologyReward).reduce(
			(flag, techType: keyof Players.TPlayerTechnologyPayload) =>
				flag &&
				Object.keys(research.technologyReward[techType]).every((techId) => this.arsenal[techType][techId] >= 1),
			true,
		);
	}

	protected _engineeringProjectComplete(projectId: number = null) {
		if (projectId) {
			const last = this.completedEngineeringProjects.slice(-1)[0];
			if (last?.id !== projectId || last.points >= last.cost) return;
			this.completedEngineeringProjects = this.completedEngineeringProjects
				.slice(0, this.completedEngineeringProjects.length - 1)
				.concat({
					...last,
					points: last.cost,
				});
			getGlobalEngineeringProject(projectId).effect(this);
			this.stats.globalEngineeringProjectsCompleteById[projectId] =
				(this.stats.globalEngineeringProjectsCompleteById[projectId] || 0) + 1;
		} else {
			this.stats.reverseEngineeringProjectsComplete += 1;
		}
		this.selectedEngineering = null;
	}

	protected _chassisUpgradeProjectComplete(project: TPlayerChassisUpgradeProject) {
		this.chassisUpgradeProjects = this.chassisUpgradeProjects
			.slice(0, this.chassisUpgradeProjects.length - 1)
			.concat({
				...project,
				points: project.cost,
			});
		this.addUnitModifier(project.chassisId, getChassisUpgrade(project.upgradeId));
		this.stats.chassisUpgradeProjectsCompleteById[project.chassisId] =
			getPlayerStatProperty(this, 'chassisUpgradeProjectsCompleteById', project.chassisId) + 1;
		this.selectedEngineering = null;
	}

	protected _completeResearchIfPossible(researchId: number) {
		const research = this.researches[researchId];
		if (!research) return false;
		if (this._isAllResearchTechObtained(research) && this.isResearchAvailable(researchId))
			return this._researchComplete(researchId);
		return false;
	}

	protected _researchComplete(researchId: number) {
		const research = this.researches[researchId];
		if (!research || research.progress >= research.cost) return false;
		research.progress = research.cost;
		if (research.technologyReward) this.addTechnologies(research.technologyReward);
		if (research.reward) research.reward(this);
		if (this.selectedResearch === researchId) this.selectedResearch = null;
		research.level = (research.level || 0) + 1;
		if (research.repeatable) {
			// resetting the repeatable research
			research.cost = research.cost *= PLAYER_REPEATABLE_RESEARCH_FACTOR;
			research.progress = 0;
		}
		this.stats.researchesComplete += 1;
		this.addXp((research.cost / DEFAULT_PLAYER_RESEARCH_COST) * DEFAULT_PLAYER_RESEARCH_XP);
		return true;
	}

	protected _checkIfAchievementUnlocked() {
		TalentRegistry.getAll()
			.filter((v) => !this.isTalentLearned(v.id))
			.map((talent) => {
				if ((talent.levelReq || 0) <= this.level && talent.achievement && talent.achievement(this) >= 1)
					this._talentLearned(talent.id);
			});
	}

	protected _talentLearned(talentId: number) {
		const talent = this.talents[talentId] || (TalentRegistry.get(talentId) as TPlayerTalent);
		if (!talent || talent.learned) return false;
		talent.learned = true;
		talent.playerId = this.id;
		if (talent.reward) talent.reward(this);
		if (talent.technologyReward) this.addTechnologies(talent.technologyReward);
		this.talents[talentId] = talent;
		this.stats.talentsLearned += 1;
		return true;
	}
}

export type TPlayer = GenericPlayer;

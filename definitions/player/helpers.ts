import objectPath from 'object-path';
import BasicMaths from '../maths/BasicMaths';
import { IPrototypeModifier } from '../prototypes/types';
import Weapons from '../technologies/types/weapons';
import { defaultPlayerProps } from './constants';
import Players from './types';

export function extractPlayerUnitModifier<K extends keyof IPrototypeModifier>(
	p: Players.TPlayerUnitModifier | Players.TPlayerUnitModifier[],
	which: K,
	chassis?: keyof Players.TPlayerUnitModifier,
): IPrototypeModifier[K] {
	const t = ([].concat(p) as Players.TPlayerUnitModifier[])
		.map((v) => v[chassis || 'default'] || v.default)
		.filter(Boolean);
	return t.flatMap((v) => v[which] || []).filter(Boolean);
}

export function stackPlayerWeaponModifier(
	p: Players.IPlayer,
	path: [WithDefault<Weapons.TWeaponGroupType>, keyof Weapons.TWeaponModifier],
	...m: IModifier[]
) {
	const xpth = ['unitModifier', 'default', 'weapon', ...[].concat(path)];
	const modifier = objectPath.get(p, xpth, {});
	objectPath.set(p, xpth, BasicMaths.stackModifiers(modifier, ...m));
	return p;
}

export function isArsenalEngineeringTarget(key: any): key is keyof Players.TPlayerTechnologyPayload {
	return Object.keys(defaultPlayerProps.arsenal).includes(key);
}

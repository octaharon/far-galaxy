import { DEFAULT_EFFECT_POWER } from '../tactical/statuses/constants';
import Resources from '../world/resources/types';
import Players from './types';

export const DEFAULT_PLAYER_RESEARCH_COST = 500;
export const DEFAULT_PLAYER_LEVEL_XP = 600;
export const DEFAULT_PLAYER_LEVEL_XP_INCREASE = 300;
export const DEFAULT_PLAYER_RESEARCH_XP = 10;
export const DEFAULT_FACTION_RESEARCH_COST = 8000;
export const VOTER_FACTION_RESEARCH_COST = 4000;
export const NEWCOMER_FACTION_RESEARCH_COST = 4000;
export const DEFAULT_ENGINEERING_PROJECT_COST = 10000;
export const CHASSIS_UPGRADE_PROJECT_COST = 30000;
export const CHASSIS_UPGRADE_REPEATABLE_COST = 1.1;
export const PLAYER_REPEATABLE_RESEARCH_FACTOR = 1.4;
export const PLAYER_ENGINEERING_PROJECT_REPEATABLE_COST = 1.05;
export const PLAYER_TALENT_TIER_LEVEL = 6;
export const defaultPlayerProps: Partial<Players.TPlayerStaticProps & Players.TPlayerDynamicProps> = {
	resourceSupply: {} as Resources.TResourcePayload,
	talentPoints: 1,
	abilityPower: DEFAULT_EFFECT_POWER,
	abilitySlots: 1,
	abilitiesAvailable: [],
	buildingUpgradePoints: 0,
	baseModifier: {},
	researches: {},
	talents: {},
	arsenal: {
		weapons: {},
		factories: {},
		facilities: {},
		establishments: {},
		armor: {},
		chassis: {},
		equipment: {},
		shield: {},
	},
	stats: {},
	bases: null,
	level: 0,
	xp: 0,
};

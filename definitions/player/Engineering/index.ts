import * as EngineeringProjects from './projects';
import { TGlobalEngineeringProject } from './types';

const projectCache: Record<number, TGlobalEngineeringProject> = {};
Object.keys(EngineeringProjects).forEach((p) => {
	const project = EngineeringProjects[p];
	if (projectCache[project.id]) throw new Error(`Duplicate Global Engineering Project id: #${project.id}`);
	projectCache[project.id] = project;
});

export const getGlobalEngineeringProject = (projectId: number) => projectCache[projectId];
export const getAvailableGlobalEngineeringProjects = (playerLevel: number) =>
	Object.values(projectCache).filter((pr) => pr.minLevel <= playerLevel);

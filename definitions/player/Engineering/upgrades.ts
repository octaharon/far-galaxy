import DIContainer from '../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../core/DI/injections';
import UnitChassis from '../../technologies/types/chassis';
import Weapons from '../../technologies/types/weapons';
import { TChassisUpgradeDictionary } from './types';

export const EngineeringChassisUpgrades: TChassisUpgradeDictionary = {
	100: {
		chassis: [
			{
				hitPoints: {
					factor: 1.04,
				},
			},
		],
	},
	101: {
		chassis: [
			{
				scanRange: {
					bias: 0.2,
				},
			},
		],
	},
	102: {
		chassis: [
			{
				speed: {
					factor: 1.05,
				},
			},
		],
	},
	103: {
		chassis: [
			{
				speed: {
					factor: 1.05,
				},
			},
		],
	},
	104: {
		armor: [
			{
				default: {
					factor: 0.98,
				},
			},
		],
	},
	105: {
		shield: [
			{
				default: {
					factor: 0.98,
				},
			},
		],
	},
	106: {
		shield: [
			{
				shieldAmount: {
					factor: 1.04,
				},
			},
		],
	},
	107: {
		chassis: [
			{
				dodge: {
					default: {
						bias: 0.05,
					},
				},
			},
		],
	},
	108: {
		chassis: [
			{
				baseCost: {
					factor: 0.98,
				},
			},
		],
	},
	109: {
		unitModifier: {
			abilityPowerModifier: [
				{
					bias: 0.15,
				},
			],
			abilityRangeModifier: [
				{
					bias: 0.15,
				},
			],
		},
	},
	200: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Cannons]: {
					baseRange: { bias: 0.25 },
				},
				[Weapons.TWeaponGroupType.Railguns]: {
					baseRange: { bias: 0.25 },
				},
				[Weapons.TWeaponGroupType.Guns]: {
					baseRange: { bias: 0.25 },
				},
			},
		],
	},
	201: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Cruise]: {
					baseRange: { bias: 0.4 },
				},
				[Weapons.TWeaponGroupType.Missiles]: {
					baseRange: { bias: 0.4 },
				},
				[Weapons.TWeaponGroupType.Rockets]: {
					baseRange: { bias: 0.4 },
				},
			},
		],
	},
	202: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Lasers]: {
					baseRange: { bias: 0.15 },
				},
				[Weapons.TWeaponGroupType.Plasma]: {
					baseRange: { bias: 0.15 },
				},
				[Weapons.TWeaponGroupType.Beam]: {
					baseRange: { bias: 0.15 },
				},
			},
		],
	},
	210: {
		weapon: [
			{
				default: {
					baseRate: {
						factor: 1.04,
					},
				},
				[Weapons.TWeaponGroupType.AntiAir]: {
					damageModifier: {
						factor: 1.07,
					},
					baseRate: {
						factor: 1.04,
					},
				},
			},
		],
	},
	211: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Guns]: {
					baseRate: {
						factor: 1.07,
					},
				},
				[Weapons.TWeaponGroupType.Lasers]: {
					baseRate: {
						factor: 1.07,
					},
				},
				[Weapons.TWeaponGroupType.Missiles]: {
					baseRate: {
						factor: 1.07,
					},
				},
			},
		],
	},
	212: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Beam]: {
					baseRate: {
						factor: 1.15,
					},
				},
				[Weapons.TWeaponGroupType.Railguns]: {
					baseRate: {
						factor: 1.15,
					},
				},
				[Weapons.TWeaponGroupType.Cruise]: {
					baseRate: {
						factor: 1.15,
					},
				},
			},
		],
	},
	213: {
		weapon: [
			{
				default: {
					baseAccuracy: {
						bias: 0.04,
					},
				},
				[Weapons.TWeaponGroupType.Warp]: {
					baseRate: {
						factor: 1.07,
					},
					baseAccuracy: {
						bias: 0.04,
					},
				},
			},
		],
	},
	214: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Guns]: {
					baseAccuracy: {
						factor: 1.07,
					},
				},
				[Weapons.TWeaponGroupType.AntiAir]: {
					baseAccuracy: {
						factor: 1.07,
					},
				},
				[Weapons.TWeaponGroupType.Cannons]: {
					baseAccuracy: {
						factor: 1.07,
					},
				},
			},
		],
	},
	215: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Railguns]: {
					baseAccuracy: {
						factor: 1.1,
					},
				},
				[Weapons.TWeaponGroupType.Missiles]: {
					baseAccuracy: {
						factor: 1.1,
					},
				},
				[Weapons.TWeaponGroupType.Lasers]: {
					baseAccuracy: {
						factor: 1.1,
					},
				},
			},
		],
	},
	220: {
		weapon: [
			{
				default: {
					damageModifier: {
						factor: 1.02,
					},
				},
				[Weapons.TWeaponGroupType.AntiAir]: {
					baseRange: {
						bias: 0.25,
					},
					damageModifier: {
						factor: 1.02,
					},
				},
			},
		],
	},
	221: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Railguns]: {
					damageModifier: {
						factor: 1.04,
					},
				},
				[Weapons.TWeaponGroupType.Beam]: {
					damageModifier: {
						factor: 1.04,
					},
				},
				[Weapons.TWeaponGroupType.Cruise]: {
					damageModifier: {
						factor: 1.04,
					},
				},
			},
		],
	},
	222: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Cannons]: {
					damageModifier: {
						factor: 1.04,
					},
				},
				[Weapons.TWeaponGroupType.Lasers]: {
					damageModifier: {
						factor: 1.04,
					},
				},
				[Weapons.TWeaponGroupType.Bombs]: {
					damageModifier: {
						factor: 1.04,
					},
				},
			},
		],
	},
	223: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Guns]: {
					damageModifier: {
						factor: 1.07,
					},
				},
				[Weapons.TWeaponGroupType.Rockets]: {
					damageModifier: {
						factor: 1.07,
					},
				},
				[Weapons.TWeaponGroupType.Launchers]: {
					damageModifier: {
						factor: 1.07,
					},
				},
			},
		],
	},
	230: {
		weapon: [
			{
				default: {
					aoeRadiusModifier: {
						factor: 1.07,
					},
				},
				[Weapons.TWeaponGroupType.Warp]: {
					baseRange: {
						bias: 0.25,
					},
					aoeRadiusModifier: {
						factor: 1.07,
					},
				},
			},
		],
	},
	231: {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Rockets]: {
					aoeRadiusModifier: {
						bias: 0.2,
					},
				},
				[Weapons.TWeaponGroupType.Bombs]: {
					aoeRadiusModifier: {
						bias: 0.2,
					},
				},
				[Weapons.TWeaponGroupType.Launchers]: {
					aoeRadiusModifier: {
						bias: 0.2,
					},
				},
			},
		],
	},
};

export function getChassisUpgrade(id: number) {
	return EngineeringChassisUpgrades[id] ?? null;
}

/**
 * Creates a deterministic Chassis Upgrade ID based on class and chosen upgrade, by concatenating a base Chassis ID with local upgrade id
 */
export function getChassisUpgradeProjectId(chassis: UnitChassis.chassisClass, upgradeId: number) {
	return (
		Math.min(
			...Object.values(
				DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(chassis),
			).map((ch) => ch.id),
		) +
		1e6 * upgradeId
	);
}

/**
 * Creates a deterministic Chassis Class and upgrade id selection from player upgrade id
 */
export function getChassisUpgradeSelectionById(id: number): {
	chassis: UnitChassis.chassisClass;
	upgradeId: number;
} {
	if (!Number.isFinite(id)) return null;
	const upgradeId = Math.floor(id / 1e6);
	const chassisId = id - upgradeId * 1e6;
	const chassis = this.getProvider(DIInjectableCollectibles.chassis).get(chassisId).type;
	return { chassis, upgradeId };
}

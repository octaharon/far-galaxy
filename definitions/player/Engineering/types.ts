import { IPrototypeModifier } from '../../prototypes/types';
import UnitChassis from '../../technologies/types/chassis';
import Players from '../types';

export type TGlobalEngineeringProject = {
	minLevel: number;
	id: number;
	caption: string;
	description: string;
	cost: number;
	effect: (p: Players.IPlayer) => Players.IPlayer;
};

export type TChassisUpgradeDictionary = Record<number, IPrototypeModifier>;

export type TChassisUpgradeProject = {
	chassisId: UnitChassis.chassisClass;
	upgradeId: number;
};

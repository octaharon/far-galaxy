import { wellRounded } from '../../../src/utils/wellRounded';
import BasicMaths from '../../maths/BasicMaths';
import { TBaseBuildingType } from '../../orbital/types';
import { getAllFactionIds } from '../../world/factionIndex';
import PoliticalFactions from '../../world/factions';
import { addResourcePayloads } from '../../world/resources/helpers';
import { DEFAULT_ENGINEERING_PROJECT_COST } from '../constants';
import { TGlobalEngineeringProject } from './types';

export const Project_OptimizationImperative: TGlobalEngineeringProject = {
	id: 100,
	caption: 'Optimization Imperative',
	description: 'Gain a Building Upgrade Point',
	minLevel: 0,
	effect: (p) => {
		p.buildingUpgradePoints += 1;
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST,
};

const ScrapCollectionMaterials = 300;
export const Project_ScrapCollection: TGlobalEngineeringProject = {
	id: 101,
	caption: 'Scrap Collection',
	description: `Gain ${ScrapCollectionMaterials} Raw Materials at every Base`,
	minLevel: 0,
	effect: (p) => {
		p.basesArray.forEach((base) => base.addRawMaterials(ScrapCollectionMaterials));
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 1.2,
};

const NetworkScramblingEnergy = 1200;
export const Project_NetworkScrambling: TGlobalEngineeringProject = {
	id: 200,
	caption: 'Network Scrambling',
	description: `Gain ${NetworkScramblingEnergy} Energy at every Base`,
	minLevel: 8,
	effect: (p) => {
		p.basesArray.forEach((base) => base.addEnergy(NetworkScramblingEnergy));
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 1.2,
};

const LaborDayRepairs = 500;
export const Project_LaborDay: TGlobalEngineeringProject = {
	id: 201,
	caption: 'Labor Day',
	description: `Gain ${LaborDayRepairs} Repair Points at every Base`,
	minLevel: 8,
	effect: (p) => {
		p.basesArray.forEach((base) => base.addRepair(LaborDayRepairs));
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 1.2,
};

const EducationXPPoints = 500;
export const Project_EducationParadigm: TGlobalEngineeringProject = {
	id: 202,
	caption: 'Labor Day',
	description: `Gain ${EducationXPPoints} Repair Points at every Base`,
	minLevel: 8,
	effect: (p) => {
		p.dockedUnits.forEach((unit) => unit.addXp(EducationXPPoints));
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 1.5,
};

const ReputationGain = 5;
export const Project_MarketAutomations: TGlobalEngineeringProject = {
	id: 300,
	caption: 'Market Automations',
	description: `Gain ${ReputationGain} Reputation with each Faction except for Pirates`,
	minLevel: 12,
	effect: (p) => {
		getAllFactionIds()
			.filter((f) => f !== PoliticalFactions.TFactionID.pirates)
			.forEach((f) => p.addReputation(f, ReputationGain));
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 1.5,
};
export const Project_PerformanceBonds: TGlobalEngineeringProject = {
	id: 301,
	caption: 'Performance Bonds',
	description: `Gain ${ReputationGain} with Pirates`,
	minLevel: 12,
	effect: (p) => {
		p.addReputation(PoliticalFactions.TFactionID.pirates, ReputationGain);
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 1.5,
};

const ResearchGain = 1000;
export const Project_ScientificEngineering: TGlobalEngineeringProject = {
	id: 400,
	caption: 'Scientific Engineering',
	description: `Gain ${ResearchGain} Research Points`,
	minLevel: 15,
	effect: (p) => {
		p.investTech(ResearchGain, 0);
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 2,
};

const XPGain = 1000;
export const Project_CompetitiveModeling: TGlobalEngineeringProject = {
	id: 401,
	caption: 'Competitive Modeling',
	description: `Gain ${XPGain} Player XP`,
	minLevel: 15,
	effect: (p) => {
		p.addXp(XPGain);
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 2,
};

const ProductionFactor = 0.3;
export const Project_AcceleratedDevelopment: TGlobalEngineeringProject = {
	id: 500,
	caption: 'Accelerated Development',
	description: `Every Factory gains Production Points equal to ${wellRounded(
		ProductionFactor * 100,
		0,
	)}% of that Base Production Capacity`,
	minLevel: 20,
	effect: (p) => {
		p.basesArray.forEach((b) =>
			b.buildings[TBaseBuildingType.factory].forEach((f) => {
				if (f?.currentPrototype)
					f.currentProductionStock += b.currentEconomy.productionCapacity * ProductionFactor;
			}),
		);
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 2,
};

const ResourceFactor = 0.1;
export const Project_NeuralStimulators: TGlobalEngineeringProject = {
	id: 501,
	caption: 'Neural Stimulators',
	description: `Gain Resources equal to ${wellRounded(
		ResourceFactor * 100,
		0,
	)}% of total Buildings cost at all Bases`,
	minLevel: 20,
	effect: (p) => {
		p.addResourcePayload(addResourcePayloads(...p.basesArray.map((b) => b.totalBuildingCost)));
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 2,
};

const ModernizationFactor = 0.1;
export const Project_MassModernization: TGlobalEngineeringProject = {
	id: 600,
	caption: 'Mass Modernization',
	description: `All Docked Units are fully repaired, and their Shield Capacity and Health are increased by ${wellRounded(
		ModernizationFactor * 100,
		0,
	)}%`,
	minLevel: 25,
	effect: (p) => {
		p.dockedUnits.forEach((u) =>
			u
				.applyChassisModifier({
					hitPoints: {
						factor: 1 + ModernizationFactor,
					},
				})
				.applyShieldsModifier({
					shieldAmount: { factor: 1 + ModernizationFactor },
				})
				.addHp(u.maxHitPoints, u),
		);
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 2.5,
};

const BaseHitPointsBonus = 30;
export const Project_IntegrityAnalysis: TGlobalEngineeringProject = {
	id: 601,
	caption: 'Integrity Analysis',
	description: `Every Orbital Base gets +${BaseHitPointsBonus} Hit Points`,
	minLevel: 25,
	effect: (p) => {
		p.basesArray.forEach((b) => {
			b.maxHitPoints += BaseHitPointsBonus;
			b.currentHitPoints += BaseHitPointsBonus;
		});
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 2.5,
};

const MobilizationMaterials = 150;
const MobilizationEnergy = 500;
const MobilizationRepairs = 200;
export const Project_MobilizationForces: TGlobalEngineeringProject = {
	id: 602,
	caption: 'Mobilization Forces',
	description: `Every Orbital Base obtains ${MobilizationMaterials} Raw Materials, ${MobilizationEnergy} Energy, ${MobilizationRepairs} Repair Points`,
	minLevel: 25,
	effect: (p) => {
		p.basesArray.forEach((b) => {
			b.addRawMaterials(MobilizationMaterials).addEnergy(MobilizationEnergy).addRepair(MobilizationRepairs);
		});
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 2.5,
};

const SoftwareUpgradeCostBonus = 5;
export const Project_SoftwareUpgrade: TGlobalEngineeringProject = {
	id: 700,
	caption: 'Software Upgrade',
	description: `Every Factory has its Max Prototype Cost increased by ${SoftwareUpgradeCostBonus}`,
	minLevel: 30,
	effect: (p) => {
		p.basesArray.forEach((b) =>
			b.buildings[TBaseBuildingType.factory].filter(Boolean).forEach((f) => {
				f.maxProductionCost += SoftwareUpgradeCostBonus;
			}),
		);
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 3,
};

const ProtocolEnhancementSpeedBonus = 1;
export const Project_ProtocolEnhancement: TGlobalEngineeringProject = {
	id: 701,
	caption: 'Protocol Enhancement',
	description: `Every Factory gains +${wellRounded(ProtocolEnhancementSpeedBonus)} Production Speed permanently`,
	minLevel: 30,
	effect: (p) => {
		p.basesArray.forEach((b) =>
			b.buildings[TBaseBuildingType.factory].filter(Boolean).forEach((f) => {
				f.productionSpeed += ProtocolEnhancementSpeedBonus;
			}),
		);
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 3,
};

const GridDecommissioningBonus = 1;
export const Project_GridDecommissioning: TGlobalEngineeringProject = {
	id: 702,
	caption: 'Grid Decommissioning',
	description: `Every existing Factory has its Upkeep reduced by ${wellRounded(
		ProtocolEnhancementSpeedBonus,
	)} Energy / Turn`,
	minLevel: 30,
	effect: (p) => {
		p.basesArray.forEach((b) =>
			b.buildings[TBaseBuildingType.factory].filter(Boolean).forEach((f) => {
				f.energyMaintenanceCost = BasicMaths.applyModifier(f.energyMaintenanceCost, {
					bias: -GridDecommissioningBonus,
					min: 1,
					minGuard: 1,
				});
			}),
		);
		return p;
	},
	cost: DEFAULT_ENGINEERING_PROJECT_COST * 3,
};

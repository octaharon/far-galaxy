import BasicMaths from '../../maths/BasicMaths';
import PoliticalFactions from '../../world/factions';
import {
	DEFAULT_FACTION_RESEARCH_COST,
	NEWCOMER_FACTION_RESEARCH_COST,
	PLAYER_REPEATABLE_RESEARCH_FACTOR,
	VOTER_FACTION_RESEARCH_COST,
} from '../constants';
import { TPlayerId } from '../playerId';
import { FactionResearch, PiratesClanAssemblyProjectid, PiratesColonizationMissionProjectId } from './factionResearch';
import ResearchRegistry from './ResearchRegistry';
import {
	TFactionResearchDefinition,
	TFactionResearchHistory,
	TFactionResearchProgress,
	TPlayerHydratedResearch,
	TPlayerResearch,
} from './types';

export function dehydrateResearch(t: TPlayerResearch): TPlayerHydratedResearch {
	return {
		id: t.id,
		progress: t.progress,
		level: t.level,
		playerId: t.playerId,
	};
}
export function hydrateResearch(t: TPlayerHydratedResearch): TPlayerResearch {
	return {
		...ResearchRegistry.get(t.id),
		...t,
	};
}

const factionResearchCache = Object.keys(FactionResearch).reduce((obj, factionId) => {
	const researches = FactionResearch[factionId];
	return Object.assign(
		obj,
		Object.keys(researches).reduce(
			(acc, researchId) =>
				Object.assign(acc, {
					[researchId]: researches[researchId],
				}),
			{},
		),
	);
}, {} as Record<number, TFactionResearchDefinition>);

export function getFactionResearch(id: number): TFactionResearchDefinition {
	return factionResearchCache[id] || null;
}

export function getCurrentFactionResearch(
	history: TFactionResearchHistory,
	faction: PoliticalFactions.TFactionID,
): TFactionResearchProgress {
	const last = history?.[faction]?.slice(-1)?.[0];
	if (last && last.points < last.cost) return last;
	return null;
}

export function getFactionResearchCost(
	history: TFactionResearchHistory,
	faction: PoliticalFactions.TFactionID,
	id: number,
	numPlayers: number,
	voters = 0,
	newcomers = 0,
): number {
	const r = history[faction];
	const last = getCurrentFactionResearch(history, faction);
	if (last) r.slice(0, r.length - 1);
	let multiplier = 1 + (PLAYER_REPEATABLE_RESEARCH_FACTOR - 1) * r.length;
	if (faction === PoliticalFactions.TFactionID.pirates) {
		const colonizationMissions = r.filter((p) => p.researchId === PiratesColonizationMissionProjectId).length || 0;
		const clanAssemblyCount = r.filter((p) => p.researchId === PiratesClanAssemblyProjectid).length || 0;
		multiplier -= colonizationMissions * 0.2;
		if (id === PiratesClanAssemblyProjectid) multiplier -= 0.2 * clanAssemblyCount;
	}
	return (
		multiplier *
		((numPlayers - voters) * DEFAULT_FACTION_RESEARCH_COST +
			voters * VOTER_FACTION_RESEARCH_COST +
			newcomers * NEWCOMER_FACTION_RESEARCH_COST)
	);
}

export function selectFactionResearchCost(
	history: TFactionResearchHistory,
	faction: PoliticalFactions.TFactionID,
	id: number,
	numPlayers: number,
	voters: TPlayerId[] = null,
): TFactionResearchHistory {
	const newHistory = Object.assign({}, history);
	const research = getFactionResearch(id);
	if (
		!getCurrentFactionResearch(history, faction) &&
		research &&
		Object.values(FactionResearch[faction]).includes(research)
	) {
		newHistory[faction] = (newHistory[faction] || []).concat({
			researchId: id,
			cost: getFactionResearchCost(history, faction, id, numPlayers, voters?.length),
			points: 0,
			startingPlayers: numPlayers,
			voters,
		});
	}
	return newHistory;
}

export function adjustFactionResearch(
	history: TFactionResearchHistory,
	faction: PoliticalFactions.TFactionID,
	newcomerCount = 0,
): TFactionResearchHistory {
	const newHistory = Object.assign({}, history);
	const last = getCurrentFactionResearch(history, faction);
	const h = newHistory[faction] || [];
	if (last) {
		newHistory[faction] = h.slice(0, h.length - 1).concat(
			Object.assign({}, last, {
				points: last.points + newcomerCount * NEWCOMER_FACTION_RESEARCH_COST,
			} as TFactionResearchProgress),
		);
	}
	return newHistory;
}

export function investFactionResearch(
	history: TFactionResearchHistory,
	faction: PoliticalFactions.TFactionID,
	points: number,
): TFactionResearchHistory {
	const last = getCurrentFactionResearch(history, faction);
	if (last) {
		last.points += points;
		// @TODO call something if faction the research is completed https://gitlab.com/octaharon/far-galaxy/-/issues/28
	}
	return history;
}

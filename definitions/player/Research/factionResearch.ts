import UnitAttack from '../../tactical/damage/attack';
import Damage from '../../tactical/damage/damage';
import UnitChassis from '../../technologies/types/chassis';
import Weapons from '../../technologies/types/weapons';
import { getAllFactionIds } from '../../world/factionIndex';
import Factions, { PoliticalFactions } from '../../world/factions';
import Resources from '../../world/resources/types';
import { TFactionResearchDefinition } from './types';
export const FactionResearchIdOffset = 1e6;
export const PiratesClanAssemblyProjectid = FactionResearchIdOffset + 710;
export const PiratesColonizationMissionProjectId = FactionResearchIdOffset + 720;
export const FactionResearch: EnumProxy<Factions.TFactionID, Record<number, TFactionResearchDefinition>> = {
	[Factions.TFactionID.venaticians]: {
		[FactionResearchIdOffset + 100]: {
			caption: 'Industrialization',
			description:
				'Perform collective maintenance within existing production means. All Factories gain +1 Production Speed',
			baseModifier: {
				productionSpeed: {
					bias: 1,
				},
			},
		},
		[FactionResearchIdOffset + 101]: {
			caption: 'War Games',
			description:
				'Plan a competition between militia and PMC organizations. Gain +2% KIN Armor Protection and +2% Damage with Energy Weapons and Mass Drivers',
			unitModifier: {
				default: {
					armor: [
						{
							[Damage.damageType.kinetic]: {
								factor: 0.98,
							},
						},
					],
					weapon: [
						{
							[Weapons.TWeaponGroupType.Beam]: {
								damageModifier: {
									factor: 1.02,
								},
							},
							[Weapons.TWeaponGroupType.Railguns]: {
								damageModifier: {
									factor: 1.02,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 102]: {
			caption: 'Garbage Collection',
			description:
				'Get together to clean up some mess in the production complex. -4% Ores and Minerals consumption on every Base',
			baseModifier: {
				resourceConsumption: {
					[Resources.resourceId.ores]: {
						factor: 0.96,
					},
					[Resources.resourceId.minerals]: {
						factor: 0.96,
					},
				},
			},
		},
		[FactionResearchIdOffset + 103]: {
			caption: 'Community Service',
			description:
				'The old spacefaring tin can needs some maintenance, and a good rest is a different work, they say. +40 Base Hit Points',
			baseModifier: {
				maxHitPoints: {
					bias: 40,
				},
			},
		},
		[FactionResearchIdOffset + 104]: {
			caption: 'Virtual Summit',
			description:
				'Bring all the brightest minds in the galaxy together using quantum threads commlink technology. +1 Research Point per every running Establishment',
			baseModifier: {
				researchOutput: {
					bias: 1,
				},
			},
		},
	},
	[Factions.TFactionID.draconians]: {
		[FactionResearchIdOffset + 200]: {
			caption: 'Autonomous Augmentations',
			description:
				'Improve Orbital Bases with robotized maintenance drones. All Facilities produce +1 Repair Points',
			baseModifier: {
				repairOutput: {
					bias: 1,
				},
			},
		},
		[FactionResearchIdOffset + 201]: {
			caption: 'Novel Alchemy',
			description:
				'Experiment with matter transmutation and explosives. All Weapons gain +0.2 AOE Radius, while Bombs and Rockets also gain +3% Damage',
			unitModifier: {
				default: {
					weapon: [
						{
							default: {
								aoeRadiusModifier: {
									bias: 0.2,
								},
							},
							[Weapons.TWeaponGroupType.Rockets]: {
								damageModifier: {
									factor: 1.02,
								},
								aoeRadiusModifier: {
									bias: 0.2,
								},
							},
							[Weapons.TWeaponGroupType.Bombs]: {
								damageModifier: {
									factor: 1.02,
								},
								aoeRadiusModifier: {
									bias: 0.2,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 202]: {
			caption: 'Longevity Healthcare',
			description:
				'Issue obligatory multidialysis, DNA grooming and cellcare therapy for all citizens. Every Base Resource Consumption is reduced by 1%, and Infantry, Exosuit and Biotech Units gain +5 Hit Points',
			unitModifier: {
				[UnitChassis.chassisClass.infantry]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
				[UnitChassis.chassisClass.exosuit]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
				[UnitChassis.chassisClass.biotech]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
			},
			baseModifier: {
				resourceConsumption: {
					default: {
						factor: 0.99,
					},
				},
			},
		},
		[FactionResearchIdOffset + 203]: {
			caption: 'Organic Capacitors',
			description:
				'Collect and recycle smaller critters to supply for new generation energy cells. +2 EMP Shield Protection and +0.25 Range for Lasers and Energy Weapons',
			unitModifier: {
				default: {
					shield: [
						{
							[Damage.damageType.magnetic]: {
								bias: -2,
							},
						},
					],
					weapon: [
						{
							[Weapons.TWeaponGroupType.Lasers]: {
								baseRange: {
									bias: 0.25,
								},
							},
							[Weapons.TWeaponGroupType.Beam]: {
								baseRange: {
									bias: 0.25,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 204]: {
			caption: 'Artificial Selection',
			description:
				'Perform empire-wide eugenics projects to maintain the quality of combatant lineages. +2 BIO Armor Protection and +3% Hit Chance for Launchers and Machine Guns',
			unitModifier: {
				default: {
					armor: [
						{
							[Damage.damageType.biological]: {
								bias: -2,
							},
						},
					],
					weapon: [
						{
							[Weapons.TWeaponGroupType.Launchers]: {
								baseAccuracy: {
									bias: 0.03,
								},
							},
							[Weapons.TWeaponGroupType.Guns]: {
								baseAccuracy: {
									bias: 0.03,
								},
							},
						},
					],
				},
			},
		},
	},
	[Factions.TFactionID.stellarians]: {
		[FactionResearchIdOffset + 300]: {
			caption: 'Ontology Survey',
			description:
				'Temporarily revoke privacy to collect data for modelling of new governance abstractions. All Establishments produce +1 Engineering Points',
			baseModifier: {
				engineeringOutput: {
					bias: 1,
				},
			},
		},
		[FactionResearchIdOffset + 301]: {
			caption: 'Venture Politics',
			description:
				'Provide discrete loans to debatable local organizations to inspire civil wars and the following interventions. Bomber, Strike Aircraft and Fighter gain +5 Hit Points, while Homing Missiles and Cruise Missiles get +0.25 Range',
			unitModifier: {
				default: {
					weapon: [
						{
							[Weapons.TWeaponGroupType.Missiles]: {
								baseRange: {
									bias: 0.25,
								},
							},
							[Weapons.TWeaponGroupType.Cruise]: {
								baseRange: {
									bias: 0.25,
								},
							},
						},
					],
				},
				[UnitChassis.chassisClass.bomber]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
				[UnitChassis.chassisClass.fighter]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
				[UnitChassis.chassisClass.strike_aircraft]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 302]: {
			caption: 'Transcendental Economy',
			description: `Inspire working overtime for the greater good and the perseverance of the nation. -3 Unit Production Cost, if it's higher than 15`,
			unitModifier: {
				default: {
					chassis: [
						{
							baseCost: {
								bias: -3,
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 304]: {
			caption: 'Halogen Treaty',
			description: `Plot to arrange a unfavorable deal and manipulate a global market. +3% Halogens Production, +5% Halogens Sell Rate, +0.5 Hydrogen at every working Facility`,
			reward: (p) => {
				// @TODO Halogen Sell Rate https://gitlab.com/octaharon/far-galaxy/-/issues/27
				return p;
			},
			baseModifier: {
				resourceOutput: {
					[Resources.resourceId.halogens]: {
						factor: 1.03,
					},
					[Resources.resourceId.hydrogen]: {
						bias: 0.5,
					},
				},
			},
		},
		[FactionResearchIdOffset + 305]: {
			caption: 'Defensive Order',
			description: `Spread panic and send mixed signals to induce uncertainity and create incentives to invest more attention into defensive measures. +3 Armor Protection and Shield Protection against WRP Damage`,
			unitModifier: {
				default: {
					armor: [
						{
							[Damage.damageType.warp]: {
								bias: -3,
							},
						},
					],
					shield: [
						{
							[Damage.damageType.warp]: {
								bias: -3,
							},
						},
					],
				},
			},
		},
	},
	[Factions.TFactionID.aquarians]: {
		[FactionResearchIdOffset + 400]: {
			caption: 'Digital Genetics',
			description:
				'Invest calculation capacity into self-modelling networks. -1 Production Cost for every Armor that costs 5 or more. +5 Hit Points for every Droid, Combat Drone and PDD',
			unitModifier: {
				default: {
					armor: [
						{
							baseCost: {
								bias: -1,
								min: 5,
								minGuard: 5,
							},
						},
					],
				},
				[UnitChassis.chassisClass.droid]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
				[UnitChassis.chassisClass.pointdefense]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
				[UnitChassis.chassisClass.combatdrone]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 401]: {
			caption: 'Netrunning Warfare',
			description:
				'Stream calculation capacity into offensive cyberattacks and spreading malware. -1 Production Cost for every Armor that costs 5 or more. +5 Hit Points for every Droid, Combat Drone and PDD',
			unitModifier: {
				default: {
					unitModifier: {
						abilityRangeModifier: [
							{
								bias: 0.25,
							},
						],
						abilityPowerModifier: [
							{
								bias: 0.25,
							},
						],
					},
				},
			},
		},
		[FactionResearchIdOffset + 402]: {
			caption: 'Quantum Superconductors',
			description:
				'Spend calculation capacity on optimization of low-resistance energy circuits. Lasers and Plasma Weapons gain +3 Damage per Attack, and every Shield gets +1% to Shield Protection',
			unitModifier: {
				default: {
					weapon: [
						{
							[Weapons.TWeaponGroupType.Lasers]: {
								damageModifier: {
									bias: 3,
								},
							},
							[Weapons.TWeaponGroupType.Plasma]: {
								damageModifier: {
									bias: 3,
								},
							},
						},
					],
					shield: [
						{
							default: {
								factor: 0.99,
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 403]: {
			caption: 'Isotope Recycling',
			description:
				'Distribute calculation capacity into resource management and facilities automation. +1 Ores output and -4% Isotopes Consumption at every Base',
			baseModifier: {
				resourceConsumption: {
					[Resources.resourceId.isotopes]: {
						factor: 0.96,
					},
				},
				resourceOutput: {
					[Resources.resourceId.ores]: {
						bias: 1,
					},
				},
			},
		},
		[FactionResearchIdOffset + 404]: {
			caption: 'Cognition Network',
			description:
				'Upstream calculation capacity into combat surveillance and coordination algorithms. +3% Scan Range for all Units, and additional +0.25 Speed to Support Vehicle, Support Aircraft and Support Ship',
			unitModifier: {
				default: {
					chassis: [
						{
							scanRange: {
								factor: 1.03,
							},
						},
					],
				},
				[UnitChassis.chassisClass.sup_vehicle]: {
					chassis: [
						{
							speed: {
								bias: 0.25,
							},
						},
					],
				},
				[UnitChassis.chassisClass.sup_aircraft]: {
					chassis: [
						{
							speed: {
								bias: 0.25,
							},
						},
					],
				},
				[UnitChassis.chassisClass.sup_ship]: {
					chassis: [
						{
							speed: {
								bias: 0.25,
							},
						},
					],
				},
			},
		},
	},
	[Factions.TFactionID.anders]: {
		[FactionResearchIdOffset + 500]: {
			caption: 'Divine Duty',
			description:
				'The Supreme Leader requires us to discard our indolence and to work assiduously. +1 Raw Materials at every Facility',
			baseModifier: {
				materialOutput: {
					bias: 1,
				},
			},
		},
		[FactionResearchIdOffset + 501]: {
			caption: 'Technology Festival',
			description:
				'The Supreme Leader bestows upon us a gift of knowledge, which we embrace to become stronger. +0.25 Speed and +5 Hit Point to Tank, LAV and HAV Units',
			unitModifier: {
				[UnitChassis.chassisClass.tank]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
							speed: {
								bias: 0.25,
							},
						},
					],
				},
				[UnitChassis.chassisClass.lav]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
							speed: {
								bias: 0.25,
							},
						},
					],
				},
				[UnitChassis.chassisClass.hav]: {
					chassis: [
						{
							hitPoints: {
								bias: 5,
							},
							speed: {
								bias: 0.25,
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 502]: {
			caption: 'Transpersonal Psychoanalysis',
			description:
				'The Supreme Leader reminds us that all we are ought to become one and pleads us to spread our perception beyond our bodies. +3% Hit Chance to all Weapons',
			unitModifier: {
				default: {
					weapon: [
						{
							default: {
								baseAccuracy: {
									bias: 0.3,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 503]: {
			caption: 'Chemical Rituals',
			description:
				'The Supreme Leader brings us higher in our existence when we need it most. -3% Consumption and +3% Production of Helium and Nitrogen',
			baseModifier: {
				resourceOutput: {
					[Resources.resourceId.helium]: {
						factor: 1.03,
					},
					[Resources.resourceId.nitrogen]: {
						factor: 1.03,
					},
				},
				resourceConsumption: {
					[Resources.resourceId.helium]: {
						factor: 0.97,
					},
					[Resources.resourceId.nitrogen]: {
						factor: 0.97,
					},
				},
			},
		},
		[FactionResearchIdOffset + 505]: {
			caption: 'Metaphysics Conference',
			description:
				'The Supreme Leader shares their wisdom with those who are able to apprehend. +3 Shield Capacity for every Shield',
			unitModifier: {
				default: {
					shield: [
						{
							shieldAmount: {
								bias: 3,
							},
						},
					],
				},
			},
		},
	},
	[Factions.TFactionID.reticulans]: {
		[FactionResearchIdOffset + 600]: {
			caption: 'Asset Management',
			description:
				'Improve and execute mobilization protocols for industry. +1% Resource Output at every Base. Weapons, that costs 5 or more, have its Production Cost reduced by 2%',
			baseModifier: {
				resourceOutput: {
					default: {
						factor: 1.01,
					},
				},
			},
			unitModifier: {
				default: {
					weapon: [
						{
							default: {
								baseCost: {
									factor: 0.98,
									minGuard: 5,
									min: 5,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 601]: {
			caption: 'Academical Militarism',
			description:
				'Introduce and integrate new educational programs at strategy, logistics and exercise. +0.2 Speed to Infantry and Droid. Fortress, Man-Of-War and Mothership Units gain +10 Hit Points',
			unitModifier: {
				[UnitChassis.chassisClass.infantry]: {
					chassis: [
						{
							speed: {
								bias: 0.2,
							},
						},
					],
				},
				[UnitChassis.chassisClass.droid]: {
					chassis: [
						{
							speed: {
								bias: 0.2,
							},
						},
					],
				},
				[UnitChassis.chassisClass.fortress]: {
					chassis: [
						{
							hitPoints: {
								bias: 10,
							},
						},
					],
				},
				[UnitChassis.chassisClass.manofwar]: {
					chassis: [
						{
							hitPoints: {
								bias: 10,
							},
						},
					],
				},
				[UnitChassis.chassisClass.mothership]: {
					chassis: [
						{
							hitPoints: {
								bias: 10,
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 602]: {
			caption: 'Operational Remodelling',
			description:
				'Conceptualize and test new administrative protocols for automated warfare. +1 TRM Armor Protection, +5% Dodge vs Drones, +0.25 Range for Drone Weapons',
			unitModifier: {
				default: {
					armor: [
						{
							[Damage.damageType.thermodynamic]: {
								bias: -1,
							},
						},
					],
					chassis: [
						{
							dodge: {
								[UnitAttack.deliveryType.drone]: {
									bias: 0.05,
								},
							},
						},
					],
					weapon: [
						{
							[Weapons.TWeaponGroupType.Drone]: {
								baseRange: {
									bias: 0.25,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 603]: {
			caption: 'Assertive Deployment',
			description:
				'Regress to preliminary offensive operations and higher levels of alerts. +3% Damage and +3% Attack Rate for Launchers and Machine Guns',
			unitModifier: {
				default: {
					weapon: [
						{
							[Weapons.TWeaponGroupType.Launchers]: {
								damageModifier: {
									factor: 1.03,
								},
								baseRate: {
									factor: 1.03,
								},
							},
							[Weapons.TWeaponGroupType.Guns]: {
								damageModifier: {
									factor: 1.03,
								},
								baseRate: {
									factor: 1.03,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 604]: {
			caption: 'Tachyon Communication',
			description:
				'Switch to subspace intercom infrastructure and initiate covert engagement protocols. +0.1 Unit AP, +3% Attack Rate for Anti-Air, +2% Damage to Rockets and Bombs',
			unitModifier: {
				default: {
					unitModifier: {
						abilityPowerModifier: [
							{
								bias: 0.1,
							},
						],
					},
					weapon: [
						{
							[Weapons.TWeaponGroupType.AntiAir]: {
								baseRate: {
									factor: 1.03,
								},
							},
							[Weapons.TWeaponGroupType.Rockets]: {
								damageModifier: {
									factor: 1.02,
								},
							},
							[Weapons.TWeaponGroupType.Bombs]: {
								damageModifier: {
									factor: 1.02,
								},
							},
						},
					],
				},
			},
		},
	},
	[Factions.TFactionID.pirates]: {
		[FactionResearchIdOffset + 700]: {
			caption: 'Duel Tournament',
			description: `Let's gather and have a jolly fight! +2% Dodge to all Units. Docked Units gain 50% of next Rank. Every Pirate loses 20 Reputation with other factions. Supporters get 25 Reputation with Pirates`,
			reward: (p, isSupporter = false) => {
				if (isSupporter) p.addReputation(p.faction, 25);
				getAllFactionIds()
					.filter((f) => f !== PoliticalFactions.TFactionID.pirates)
					.forEach((f) => p.addReputation(f, -20));
				p.dockedUnits.forEach((u) => {
					const xpLeft = u.nextLevelXp - u.xp;
					if (xpLeft > 0) u.addXp(xpLeft / 2);
				});
				return p;
			},
			unitModifier: {
				default: {
					chassis: [
						{
							dodge: {
								default: {
									bias: 0.02,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 701]: {
			caption: 'Trade Forum',
			description: `Let's gather and bargain our junk! +2% Oxygen and Carbon Output for every Base. -2% Buildings Energy Consumption. Every Pirate gains 20 Reputation with other factions`,
			reward: (p) => {
				getAllFactionIds()
					.filter((f) => f !== PoliticalFactions.TFactionID.pirates)
					.forEach((f) => p.addReputation(f, 20));
				return p;
			},
			baseModifier: {
				resourceOutput: {
					[Resources.resourceId.carbon]: {
						factor: 1.02,
					},
					[Resources.resourceId.oxygen]: {
						factor: 1.02,
					},
				},
				energyConsumption: {
					factor: 0.98,
				},
			},
		},
		[PiratesColonizationMissionProjectId]: {
			caption: 'Colonization Mission',
			description: `Let's gather and settle some planet! +3% Proteins Output and +20 Energy Storage for every Base. Non-supporters lose 50 Reputation with Pirates. All future Faction Research Project has their cost reduced by 20%`,
			reward: (p, isSupporter = false) => {
				if (!isSupporter) p.addReputation(p.faction, -50);
				return p;
			},
			baseModifier: {
				resourceOutput: {
					[Resources.resourceId.proteins]: {
						factor: 1.02,
					},
				},
				maxEnergyCapacity: {
					bias: 20,
				},
			},
		},
		[FactionResearchIdOffset + 703]: {
			caption: 'Skeet Shooting',
			description: `Let's gather and have fun with firearms! +3% Hit Chance and Damage to Anti-Air. +3% Attack Rate for Cannons. All Pirates lose 10 Reputation with other factions. Supporters gain 25 Reputation with Pirates`,
			reward: (p, isSupporter = false) => {
				if (isSupporter) p.addReputation(p.faction, 25);
				getAllFactionIds()
					.filter((f) => f !== PoliticalFactions.TFactionID.pirates)
					.forEach((f) => p.addReputation(f, -10));
				return p;
			},
			unitModifier: {
				default: {
					weapon: [
						{
							[Weapons.TWeaponGroupType.AntiAir]: {
								baseAccuracy: {
									bias: 0.03,
								},
								damageModifier: {
									factor: 1.03,
								},
							},
							[Weapons.TWeaponGroupType.Cannons]: {
								baseRate: {
									factor: 1.03,
								},
							},
						},
					],
				},
			},
		},
		[FactionResearchIdOffset + 704]: {
			caption: 'Civilian Raid',
			description: `Let's gather and abduct some foreigners! Every Establishment and Facility produce +0.5 Engineering Points and +0.5 Repair Points respectively. All Pirates lose 50 Reputation with other factions. Supporters gain 25 Reputation with Pirates`,
			reward: (p, isSupporter = false) => {
				if (isSupporter) p.addReputation(p.faction, 25);
				getAllFactionIds()
					.filter((f) => f !== PoliticalFactions.TFactionID.pirates)
					.forEach((f) => p.addReputation(f, -50));
				return p;
			},
			baseModifier: {
				engineeringOutput: {
					bias: 0.5,
				},
				repairOutput: {
					bias: 0.5,
				},
			},
		},
		[FactionResearchIdOffset + 705]: {
			caption: 'Rescue Operations',
			description: `Let's gather and help out some lost traders! All Units gain +0.2 Scan Range, all Units at Orbital Bay have their Hit Points restored. All Pirates gain 25 Reputation with other factions. Supporters lose 30 Reputation with Pirates`,
			reward: (p, isSupporter = false) => {
				if (isSupporter) p.addReputation(p.faction, -30);
				p.dockedUnits.forEach((unit) => unit.addHp(unit.maxHitPoints - unit.currentHitPoints, unit));
				getAllFactionIds()
					.filter((f) => f !== PoliticalFactions.TFactionID.pirates)
					.forEach((f) => p.addReputation(f, 25));
				return p;
			},
			unitModifier: {
				default: {
					chassis: [
						{
							scanRange: {
								bias: 0.2,
							},
						},
					],
				},
			},
		},
		[PiratesClanAssemblyProjectid]: {
			caption: 'Clan Assembly',
			description: `Let's gather and elect new Lords! Every Base is fully repaired and grants the Player +500 XP. This Project cost is reduced by 20%. Non-Supporters lose 30 Reputation with Pirates, while Supporters gain 20 Reputation instead`,
			reward: (p, isSupporter = false) => {
				p.addReputation(p.faction, isSupporter ? 20 : -30);
				p.basesArray.forEach((base) => {
					base.currentHitPoints = base.currentEconomy.maxHitPoints;
					p.addXp(500);
				});
				return p;
			},
		},
	},
};

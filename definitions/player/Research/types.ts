import { IStaticCollectible } from '../../core/Collectible';
import { TPlayerBaseModifier } from '../../orbital/base/types';
import PoliticalFactions from '../../world/factions';
import { TPlayerId } from '../playerId';
import Players, { IPlayerPropertyProps } from '../types';

export type TFactionResearchDefinition = Omit<IStaticCollectible, 'id'> & {
	unitModifier?: Players.TPlayerUnitModifier;
	baseModifier?: TPlayerBaseModifier;
	reward?: (p: Players.IPlayer, isSupporter?: boolean) => Players.IPlayer;
};

export type TFactionResearchProgress = {
	researchId: number;
	cost: number;
	points: number;
	voters: TPlayerId[];
	startingPlayers: number;
};

export type TFactionResearchHistory = Record<PoliticalFactions.TFactionID, TFactionResearchProgress[]>;

export type TPlayerResearchDefinition = TFactionResearchDefinition & {
	id: number;
	cost: number;
	technologyReward?: Players.TPlayerTechnologyPayload;
	restrictedFactions?: PoliticalFactions.TFactionID[];
	exclusiveFactions?: PoliticalFactions.TFactionID[];
	prerequisiteResearchIds?: number[];
	repeatable?: boolean;
};

export type TPlayerResearch = TPlayerResearchDefinition & TPlayerHydratedResearch;

export type TPlayerHydratedResearch = IPlayerPropertyProps & {
	id: number;
	progress: number;
	level: number;
};

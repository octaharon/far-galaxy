import { TPlayerResearchDefinition } from '../types';
import * as AirResearch from './all/Aircraft';
import * as AAResearch from './all/AntiAir';
import * as ScienceResearch from './all/AppliedScience';
import * as ArmorResearch from './all/Armor';
import * as BionicsResearch from './all/Bionics';
import * as CannonsResearch from './all/Cannons';
import * as GunneryResearch from './all/Guns';
import * as InfrastructureResearch from './all/Infrastructure';
import * as LasersResearch from './all/Lasers';
import * as LaunchersResearch from './all/Launchers';
import * as MechanicsResearch from './all/Mechanics';
import * as RailgunResearch from './all/Railguns';
import * as RoboticsResearch from './all/Robotics';
import * as RocketryResearch from './all/Rocketry';
import * as ShieldResearch from './all/Shield';
import * as ShipsResearch from './all/Ships';
import * as TopResearch from './all/TopTier';
import * as WarpResearch from './all/Warp';

const researches = [
	...Object.values(RocketryResearch),
	...Object.values(LasersResearch),
	...Object.values(GunneryResearch),
	...Object.values(CannonsResearch),
	...Object.values(LaunchersResearch),
	...Object.values(RailgunResearch),
	...Object.values(AAResearch),
	...Object.values(WarpResearch),
	...Object.values(ArmorResearch),
	...Object.values(ShieldResearch),
	...Object.values(ScienceResearch),
	...Object.values(InfrastructureResearch),
	...Object.values(MechanicsResearch),
	...Object.values(RoboticsResearch),
	...Object.values(BionicsResearch),
	...Object.values(ShipsResearch),
	...Object.values(AirResearch),
	...Object.values(TopResearch),
].reduce((list, item) => {
	if (list[item.id]) throw new Error('Duplicate Research Id: ' + item.id);
	return Object.assign(list, {
		[item.id]: item,
	});
}, {} as Record<number | string, TPlayerResearchDefinition>);

export class ResearchRegistry {
	public static check() {
		Object.values(researches).some((r) =>
			r.prerequisiteResearchIds?.some((requiredResearchId) => {
				if (!this.get(requiredResearchId))
					throw new Error(`Research #${r.id} requires non-existent research #${requiredResearchId}`);
				return false;
			}),
		);
	}

	public static get(id: number | string): TPlayerResearchDefinition {
		return researches[id] || null;
	}

	public static getAll() {
		return Object.values(researches);
	}
}

ResearchRegistry.check();

export default ResearchRegistry;

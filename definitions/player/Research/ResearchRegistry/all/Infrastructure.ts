import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import AmphitheatreEstablishment from '../../../../orbital/establishments/all/Amphitheatre.500';
import HolostageEstablishment from '../../../../orbital/establishments/all/Holostage.501';
import NaniteCloudEstablishment from '../../../../orbital/establishments/all/NaniteCloud.304';
import NanocellHullEstablishment from '../../../../orbital/establishments/all/NanocellHull.300';
import NeutrinoHullEstablishment from '../../../../orbital/establishments/all/NeutrinoHull.302';
import ObservationDeckEstablishment from '../../../../orbital/establishments/all/ObservationDeck.502';
import ReactiveHullEstablishment from '../../../../orbital/establishments/all/ReactiveHull.301';
import VertebralArmorEstablishment from '../../../../orbital/establishments/all/VertebralArmor.303';
import AtomicPileFacility from '../../../../orbital/facilities/all/AtomicPile.302';
import AxialCompressorFacility from '../../../../orbital/facilities/all/AxialCompressor.203';
import ChargeDepotFacility from '../../../../orbital/facilities/all/ChargeDepot.305';
import CisternFacility from '../../../../orbital/facilities/all/Cistern.402';
import ColdFusionReactorFacility from '../../../../orbital/facilities/all/ColdFusionReactor.303';
import CompostureFacility from '../../../../orbital/facilities/all/Composture.201';
import DarkTetherFacility from '../../../../orbital/facilities/all/DarkTether.407';
import GarageFacility from '../../../../orbital/facilities/all/Garage.401';
import HyperspaceCompactorFacility from '../../../../orbital/facilities/all/HyperspaceCompactor.306';
import IncinerationPlantFacility from '../../../../orbital/facilities/all/IncinerationPlant.300';
import LivingShellFacility from '../../../../orbital/facilities/all/LivingShell.405';
import MassRelayFacility from '../../../../orbital/facilities/all/MassRelay.304';
import MineralRefineryFacility from '../../../../orbital/facilities/all/MineralRefinery.200';
import NullTransporterFacility from '../../../../orbital/facilities/all/NullTransporter.102';
import ParticleCondenserFacility from '../../../../orbital/facilities/all/ParticleCondenser.101';
import QuarkStreamlineFacility from '../../../../orbital/facilities/all/QuarkStreamline.406';
import RecyclerFacility from '../../../../orbital/facilities/all/Recycler.404';
import SiloFacility from '../../../../orbital/facilities/all/Silo.400';
import SmelteryFacility from '../../../../orbital/facilities/all/Smeltery.202';
import SolarCollectorFacility from '../../../../orbital/facilities/all/SolarCollector.301';
import TemperingCauldronFacility from '../../../../orbital/facilities/all/TemperingCauldron.204';
import VacuumCleanerFacility from '../../../../orbital/facilities/all/VacuumCleaner.100';
import WorkshopFacility from '../../../../orbital/facilities/all/Workshop.403';
import WormholeProjectorFacility from '../../../../orbital/facilities/all/WormholeProjector.103';
import UnitChassis from '../../../../technologies/types/chassis';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_ChargeDepot: TPlayerResearchDefinition = {
	id: 1500,
	caption: 'Facility: Charge Depot',
	description: ChargeDepotFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		facilities: {
			[ChargeDepotFacility.id]: 1,
		},
	},
};
export const PlayerResearch_Silo: TPlayerResearchDefinition = {
	id: 1505,
	caption: 'Facility: Silo',
	description: SiloFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_ChargeDepot.id],
	technologyReward: {
		facilities: {
			[SiloFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_IncinerationPlant: TPlayerResearchDefinition = {
	id: 1506,
	caption: 'Facility: Incineration Plant',
	description: IncinerationPlantFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_ChargeDepot.id],
	technologyReward: {
		facilities: {
			[IncinerationPlantFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_IntegratedService1: TPlayerResearchDefinition = {
	id: 1510,
	caption: 'Integrated Service I',
	description: '+3% Base Repair Output; +50 Materials Storage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_ChargeDepot.id],
	baseModifier: {
		repairOutput: {
			factor: 1.03,
		},
		maxRawMaterials: {
			bias: 50,
		},
	},
};
export const PlayerResearch_SegmentedProduction1: TPlayerResearchDefinition = {
	id: 1511,
	caption: 'Segmented Production I',
	description: '+5 Base Production Capacity; +1 Production Speed on every Factory',
	prerequisiteResearchIds: [PlayerResearch_ChargeDepot.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	baseModifier: {
		productionCapacity: {
			bias: 5,
		},
		productionSpeed: {
			bias: 1,
		},
	},
};
export const PlayerResearch_GridDynamics1: TPlayerResearchDefinition = {
	id: 1512,
	caption: 'Grid Dynamics I',
	description: '+2% Base Energy Output; +3 Unit Shield Capacity',
	prerequisiteResearchIds: [PlayerResearch_ChargeDepot.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	baseModifier: {
		energyOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Composture: TPlayerResearchDefinition = {
	id: 1520,
	caption: 'Facility: Composture',
	description: CompostureFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_IncinerationPlant.id, PlayerResearch_SegmentedProduction1.id],
	technologyReward: {
		facilities: {
			[CompostureFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_MineralRefinery: TPlayerResearchDefinition = {
	id: 1521,
	caption: 'Facility: Mineral Refinery',
	description: MineralRefineryFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_IncinerationPlant.id, PlayerResearch_IntegratedService1.id],
	technologyReward: {
		facilities: {
			[MineralRefineryFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_SolarCollector: TPlayerResearchDefinition = {
	id: 1522,
	caption: 'Facility: Solar Collector',
	description: SolarCollectorFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_IncinerationPlant.id, PlayerResearch_GridDynamics1.id],
	technologyReward: {
		facilities: {
			[SolarCollectorFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_Garage: TPlayerResearchDefinition = {
	id: 1525,
	caption: 'Facility: Garage',
	description: GarageFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_Silo.id,
		PlayerResearch_GridDynamics1.id,
		PlayerResearch_SegmentedProduction1.id,
		PlayerResearch_IntegratedService1.id,
	],
	technologyReward: {
		facilities: {
			[GarageFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_IntegratedService2: TPlayerResearchDefinition = {
	id: 1530,
	caption: 'Integrated Service II',
	description: '+3% Base Repair Output; +50 Materials Storage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_IntegratedService1.id, PlayerResearch_Silo.id],
	baseModifier: {
		repairOutput: {
			factor: 1.03,
		},
		maxRawMaterials: {
			bias: 50,
		},
	},
};
export const PlayerResearch_SegmentedProduction2: TPlayerResearchDefinition = {
	id: 1531,
	caption: 'Segmented Production II',
	description: '+5 Base Production Capacity; +1 Production Speed on every Factory',
	prerequisiteResearchIds: [PlayerResearch_SegmentedProduction1.id, PlayerResearch_Silo.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	baseModifier: {
		productionCapacity: {
			bias: 5,
		},
		productionSpeed: {
			bias: 1,
		},
	},
};
export const PlayerResearch_GridDynamics2: TPlayerResearchDefinition = {
	id: 1532,
	caption: 'Grid Dynamics II',
	description: '+2% Base Energy Output; +3 Unit Shield Capacity',
	prerequisiteResearchIds: [PlayerResearch_GridDynamics1.id, PlayerResearch_Silo.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	baseModifier: {
		energyOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_NanocellHull: TPlayerResearchDefinition = {
	id: 1533,
	caption: 'Establishment: Nanocell Hull',
	description: NanocellHullEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Garage.id],
	technologyReward: {
		establishments: {
			[NanocellHullEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_Smeltery: TPlayerResearchDefinition = {
	id: 1540,
	caption: 'Facility: Smeltery',
	description: SmelteryFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Garage.id, PlayerResearch_SegmentedProduction2.id],
	technologyReward: {
		facilities: {
			[SmelteryFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_AxialCompressor: TPlayerResearchDefinition = {
	id: 1541,
	caption: 'Facility: Axial Compressor',
	description: AxialCompressorFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Garage.id, PlayerResearch_IntegratedService2.id],
	technologyReward: {
		facilities: {
			[AxialCompressorFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_VacuumCleaner: TPlayerResearchDefinition = {
	id: 1542,
	caption: 'Facility: Vacuum Cleaner',
	description: VacuumCleanerFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Garage.id, PlayerResearch_GridDynamics2.id],
	technologyReward: {
		facilities: {
			[VacuumCleanerFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_AtomicPile: TPlayerResearchDefinition = {
	id: 1543,
	caption: 'Facility: Atomic Pile',
	description: AtomicPileFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_SolarCollector.id, PlayerResearch_GridDynamics2.id],
	technologyReward: {
		facilities: {
			[AtomicPileFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_ColdFusionReactor: TPlayerResearchDefinition = {
	id: 1544,
	caption: 'Facility: Cold Fusion Reactor',
	description: ColdFusionReactorFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_MineralRefinery.id, PlayerResearch_IntegratedService2.id],
	technologyReward: {
		facilities: {
			[ColdFusionReactorFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_Cistern: TPlayerResearchDefinition = {
	id: 1545,
	caption: 'Facility: Cistern',
	description: CisternFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Composture.id, PlayerResearch_SegmentedProduction2.id],
	technologyReward: {
		facilities: {
			[CisternFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_IntegratedService3: TPlayerResearchDefinition = {
	id: 1550,
	caption: 'Integrated Service III',
	description: '+3% Base Repair Output; +50 Materials Storage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_IntegratedService2.id],
	baseModifier: {
		repairOutput: {
			factor: 1.03,
		},
		maxRawMaterials: {
			bias: 50,
		},
	},
};
export const PlayerResearch_SegmentedProduction3: TPlayerResearchDefinition = {
	id: 1551,
	caption: 'Segmented Production III',
	description: '+5 Base Production Capacity; +1 Production Speed on every Factory',
	prerequisiteResearchIds: [PlayerResearch_SegmentedProduction2.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	baseModifier: {
		productionCapacity: {
			bias: 5,
		},
		productionSpeed: {
			bias: 1,
		},
	},
};
export const PlayerResearch_GridDynamics3: TPlayerResearchDefinition = {
	id: 1552,
	caption: 'Grid Dynamics III',
	description: '+2% Base Energy Output; +3 Unit Shield Capacity',
	prerequisiteResearchIds: [PlayerResearch_GridDynamics2.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	baseModifier: {
		energyOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_InfrastructureProject1: TPlayerResearchDefinition = {
	id: 1553,
	caption: 'Infrastructure Project I',
	description: '+1 Bay Capacity; +25 Base Hit Points',
	prerequisiteResearchIds: [
		PlayerResearch_SegmentedProduction2.id,
		PlayerResearch_GridDynamics2.id,
		PlayerResearch_IntegratedService2.id,
	],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	baseModifier: {
		bayCapacity: {
			bias: 1,
		},
		maxHitPoints: {
			bias: 25,
		},
	},
};
export const PlayerResearch_ReactiveHull: TPlayerResearchDefinition = {
	id: 1560,
	caption: 'Establishment: Reactive Hull',
	description: ReactiveHullEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProject1.id, PlayerResearch_NanocellHull.id],
	technologyReward: {
		establishments: {
			[ReactiveHullEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_NeutrinoHull: TPlayerResearchDefinition = {
	id: 1561,
	caption: 'Establishment: Neutrino Hull',
	description: NeutrinoHullEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProject1.id, PlayerResearch_NanocellHull.id],
	technologyReward: {
		establishments: {
			[NeutrinoHullEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_MassRelay: TPlayerResearchDefinition = {
	id: 1565,
	caption: 'Facility: Mass Relay',
	description: MassRelayFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_AtomicPile.id, PlayerResearch_GridDynamics3.id],
	technologyReward: {
		facilities: {
			[MassRelayFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_HyperspaceCompactor: TPlayerResearchDefinition = {
	id: 1566,
	caption: 'Facility: Hyperspace Compactor',
	description: HyperspaceCompactorFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_ColdFusionReactor.id, PlayerResearch_IntegratedService3.id],
	technologyReward: {
		facilities: {
			[HyperspaceCompactorFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_Recycler: TPlayerResearchDefinition = {
	id: 1567,
	caption: 'Facility: Recycler',
	description: RecyclerFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_Cistern.id, PlayerResearch_SegmentedProduction3.id],
	technologyReward: {
		facilities: {
			[RecyclerFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_NullTransporter: TPlayerResearchDefinition = {
	id: 1570,
	caption: 'Facility: Null Transporter',
	description: NullTransporterFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_VacuumCleaner.id,
		PlayerResearch_GridDynamics3.id,
		PlayerResearch_IntegratedService3.id,
		PlayerResearch_SegmentedProduction3.id,
	],
	technologyReward: {
		facilities: {
			[NullTransporterFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_ParticleCondenser: TPlayerResearchDefinition = {
	id: 1571,
	caption: 'Facility: Particle Condenser',
	description: ParticleCondenserFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_VacuumCleaner.id,
		PlayerResearch_GridDynamics3.id,
		PlayerResearch_IntegratedService3.id,
		PlayerResearch_SegmentedProduction3.id,
	],
	technologyReward: {
		facilities: {
			[ParticleCondenserFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_TemperingCauldron: TPlayerResearchDefinition = {
	id: 1572,
	caption: 'Facility: Tempering Cauldron',
	description: TemperingCauldronFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_Smeltery.id,
		PlayerResearch_GridDynamics3.id,
		PlayerResearch_IntegratedService3.id,
		PlayerResearch_SegmentedProduction3.id,
	],
	technologyReward: {
		facilities: {
			[TemperingCauldronFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_Workshop: TPlayerResearchDefinition = {
	id: 1573,
	caption: 'Facility: Workshop',
	description: WorkshopFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_AxialCompressor.id,
		PlayerResearch_GridDynamics3.id,
		PlayerResearch_IntegratedService3.id,
		PlayerResearch_SegmentedProduction3.id,
	],
	technologyReward: {
		facilities: {
			[WorkshopFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_Amphitheatre: TPlayerResearchDefinition = {
	id: 1574,
	caption: 'Establishment: Amphitheatre',
	description: AmphitheatreEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProject1.id, PlayerResearch_Garage.id],
	technologyReward: {
		establishments: {
			[AmphitheatreEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_InfrastructureProject2: TPlayerResearchDefinition = {
	id: 1575,
	caption: 'Infrastructure Project II',
	description: '+1 Bay Capacity; +25 Base Hit Points',
	prerequisiteResearchIds: [
		PlayerResearch_SegmentedProduction3.id,
		PlayerResearch_GridDynamics3.id,
		PlayerResearch_IntegratedService3.id,
		PlayerResearch_InfrastructureProject1.id,
	],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	baseModifier: {
		bayCapacity: {
			bias: 1,
		},
		maxHitPoints: {
			bias: 25,
		},
	},
};
export const PlayerResearch_Holostage: TPlayerResearchDefinition = {
	id: 1580,
	caption: 'Establishment: Holostage',
	description: AmphitheatreEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProject2.id, PlayerResearch_Amphitheatre.id],
	technologyReward: {
		establishments: {
			[HolostageEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_ObservationDeck: TPlayerResearchDefinition = {
	id: 1581,
	caption: 'Establishment: Observation Deck',
	description: ObservationDeckEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProject2.id, PlayerResearch_Amphitheatre.id],
	technologyReward: {
		establishments: {
			[ObservationDeckEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_VertebralArmor: TPlayerResearchDefinition = {
	id: 1582,
	caption: 'Establishment: Vertebral Armor',
	description: VertebralArmorEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProject2.id, PlayerResearch_ReactiveHull.id],
	technologyReward: {
		establishments: {
			[VertebralArmorEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_NaniteCloud: TPlayerResearchDefinition = {
	id: 1583,
	caption: 'Establishment: Nanite Cloud',
	description: NaniteCloudEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProject2.id, PlayerResearch_NeutrinoHull.id],
	technologyReward: {
		establishments: {
			[NaniteCloudEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_WormholeProjector: TPlayerResearchDefinition = {
	id: 1584,
	caption: 'Facility: Wormhole Projector',
	description: WormholeProjectorFacility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_InfrastructureProject2.id,
		PlayerResearch_ParticleCondenser.id,
		PlayerResearch_NullTransporter.id,
	],
	technologyReward: {
		facilities: {
			[WormholeProjectorFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_InfrastructureProjectRepeatable: TPlayerResearchDefinition = {
	id: 1585,
	caption: `Infrastructure Project ${INFINITY_SYMBOL}`,
	repeatable: true,
	description: '+1% Resource Output; +5 Base Hit Points',
	prerequisiteResearchIds: [
		PlayerResearch_Holostage.id,
		PlayerResearch_ObservationDeck.id,
		PlayerResearch_WormholeProjector.id,
	],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	baseModifier: {
		researchOutput: {
			factor: 1.1,
		},
		maxHitPoints: {
			bias: 5,
		},
	},
};
export const PlayerResearch_ManagedStorage: TPlayerResearchDefinition = {
	id: 1590,
	caption: `Managed Storage`,
	description: '+0.5 Speed for Droid, Scout and Infantry; Facility: Quark Streamline',
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProjectRepeatable.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: { [QuarkStreamlineFacility.id]: 0.5 },
	},
};
export const PlayerResearch_UnnaturalSelection: TPlayerResearchDefinition = {
	id: 1591,
	caption: `Unnatural Selection`,
	description: '+5% Dodge for Biotech, Swarm and Hydra; Facility: Living Shell',
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProjectRepeatable.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	unitModifier: {
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.swarm]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hydra]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[LivingShellFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_TransmutationLogistics: TPlayerResearchDefinition = {
	id: 1592,
	caption: `Transmutational Logistics`,
	description: '+5% Damage for Fighter, Rover and HAV; Facility: Dark Tether',
	prerequisiteResearchIds: [PlayerResearch_InfrastructureProjectRepeatable.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	unitModifier: {
		[UnitChassis.chassisClass.fighter]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hav]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[DarkTetherFacility.id]: 0.5,
		},
	},
};

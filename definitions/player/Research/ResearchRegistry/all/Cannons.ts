import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import PlanetaryAnchorEquipment from '../../../../technologies/equipment/all/Defensive/PlanetaryAnchor.20027';
import DianodeEquipment from '../../../../technologies/equipment/all/Enhancement/Dianode.20019';
import BallisticGuidanceModule from '../../../../technologies/equipment/all/Offensive/BallisticGuidanceModule.10005';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AntimatterShells from '../../../../technologies/weapons/all/cannons/AntimatterShells.10003';
import CumulativeShells from '../../../../technologies/weapons/all/cannons/CumulativeShells.10002';
import EMPShells from '../../../../technologies/weapons/all/cannons/EMPShells.10004';
import ExplosiveShells from '../../../../technologies/weapons/all/cannons/ExplosiveShells.10006';
import FuzeShells from '../../../../technologies/weapons/all/cannons/FuzeShells.10000';
import HeavyArtillery from '../../../../technologies/weapons/all/cannons/HeavyArtillery.10010';
import NukeShells from '../../../../technologies/weapons/all/cannons/NukeShells.10005';
import ProtonShells from '../../../../technologies/weapons/all/cannons/ProtonShells.10001';
import TauShells from '../../../../technologies/weapons/all/cannons/TauShells.10007';
import VacuumShells from '../../../../technologies/weapons/all/cannons/VacuumShells.10008';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_FuzeShells: TPlayerResearchDefinition = {
	id: 400,
	caption: 'Weapon: Fuze Shells',
	description: FuzeShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		weapons: {
			[FuzeShells.id]: 1,
		},
	},
};
export const PlayerResearch_CumulativeShells: TPlayerResearchDefinition = {
	id: 405,
	caption: 'Weapon: Cumulative Shells',
	description: CumulativeShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_FuzeShells.id],
	technologyReward: {
		weapons: {
			[CumulativeShells.id]: 0.5,
		},
	},
};
export const PlayerResearch_CannonsPrecision1: TPlayerResearchDefinition = {
	id: 410,
	caption: 'Cannon Precision I',
	description: '+3% Hit Chance for Cannons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_FuzeShells.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseAccuracy: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CannonsRate1: TPlayerResearchDefinition = {
	id: 411,
	caption: 'Cannons Attack Rate I',
	description: '+0.05 Cannons Attack Rate',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_FuzeShells.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CannonsDamage1: TPlayerResearchDefinition = {
	id: 412,
	caption: 'Cannons Damage I',
	description: '+5% Damage for Cannons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_FuzeShells.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_EMPShells: TPlayerResearchDefinition = {
	id: 415,
	caption: 'Weapon: EMP Shells',
	description: EMPShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_CumulativeShells.id, PlayerResearch_CannonsPrecision1.id],
	technologyReward: {
		weapons: {
			[EMPShells.id]: 0.5,
		},
	},
};
export const PlayerResearch_ProtonShells: TPlayerResearchDefinition = {
	id: 416,
	caption: 'Weapon: Proton Shells',
	description: ProtonShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_CumulativeShells.id, PlayerResearch_CannonsRate1.id],
	technologyReward: {
		weapons: {
			[ProtonShells.id]: 0.5,
		},
	},
};
export const PlayerResearch_VacuumShells: TPlayerResearchDefinition = {
	id: 417,
	caption: 'Weapon: Vacuum Shells',
	description: VacuumShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_CumulativeShells.id, PlayerResearch_CannonsDamage1.id],
	technologyReward: {
		weapons: {
			[VacuumShells.id]: 0.5,
		},
	},
};
export const PlayerResearch_CannonsPrecision2: TPlayerResearchDefinition = {
	id: 420,
	caption: 'Cannons Precision II',
	description: '+3% Hit Chance for Cannons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_CumulativeShells.id, PlayerResearch_CannonsPrecision1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseAccuracy: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CannonsRate2: TPlayerResearchDefinition = {
	id: 421,
	caption: 'Cannons Attack Rate II',
	description: '+0.05 Cannons Attack Rate',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_CumulativeShells.id, PlayerResearch_CannonsRate1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CannonsDamage2: TPlayerResearchDefinition = {
	id: 422,
	caption: 'Cannons Damage II',
	description: '+5% Damage for Cannons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_CumulativeShells.id, PlayerResearch_CannonsDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_TauShells: TPlayerResearchDefinition = {
	id: 430,
	caption: 'Weapon: Tau Shells',
	description: TauShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_EMPShells.id,
		PlayerResearch_CannonsRate1.id,
		PlayerResearch_CannonsPrecision2.id,
	],
	technologyReward: {
		weapons: {
			[TauShells.id]: 0.5,
		},
	},
};
export const PlayerResearch_ExplosiveShells: TPlayerResearchDefinition = {
	id: 431,
	caption: 'Weapon: Explosive Shells',
	description: ExplosiveShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_VacuumShells.id,
		PlayerResearch_CannonsPrecision1.id,
		PlayerResearch_CannonsDamage2.id,
	],
	technologyReward: {
		weapons: {
			[ExplosiveShells.id]: 0.5,
		},
	},
};
export const PlayerResearch_AntimatterShells: TPlayerResearchDefinition = {
	id: 432,
	caption: 'Weapon: Antimatter Shells',
	description: AntimatterShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_ProtonShells.id,
		PlayerResearch_CannonsRate2.id,
		PlayerResearch_CannonsDamage1.id,
	],
	technologyReward: {
		weapons: {
			[AntimatterShells.id]: 0.5,
		},
	},
};
export const PlayerResearch_CannonsPrecision3: TPlayerResearchDefinition = {
	id: 440,
	caption: 'Cannons Precision III',
	description: '+3% Hit Chance for Cannons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_ProtonShells.id, PlayerResearch_CannonsPrecision2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseAccuracy: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CannonsRate3: TPlayerResearchDefinition = {
	id: 441,
	caption: 'Cannons Attack Rate III',
	description: '+0.05 Cannons Attack Rate',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_VacuumShells.id, PlayerResearch_CannonsRate2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CannonsDamage3: TPlayerResearchDefinition = {
	id: 442,
	caption: 'Cannons Damage III',
	description: '+5% Damage for Cannons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_EMPShells.id, PlayerResearch_CannonsDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_NukeShells: TPlayerResearchDefinition = {
	id: 445,
	caption: 'Weapon: Nuke Shells',
	description: NukeShells.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [
		PlayerResearch_EMPShells.id,
		PlayerResearch_ProtonShells.id,
		PlayerResearch_VacuumShells.id,
	],
	technologyReward: {
		weapons: {
			[NukeShells.id]: 0.5,
		},
	},
};
export const PlayerResearch_HeavyArtillery: TPlayerResearchDefinition = {
	id: 450,
	caption: 'Weapon: Heavy Artillery',
	description: HeavyArtillery.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_TauShells.id,
		PlayerResearch_ExplosiveShells.id,
		PlayerResearch_AntimatterShells.id,
	],
	technologyReward: {
		weapons: {
			[HeavyArtillery.id]: 0.5,
		},
	},
};
export const PlayerResearch_CannonsPrecisionRepeatable: TPlayerResearchDefinition = {
	id: 460,
	repeatable: true,
	caption: `Cannons Precision ${INFINITY_SYMBOL}`,
	description: '+2% Hit Chance for Cannons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_NukeShells.id, PlayerResearch_CannonsPrecision3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseAccuracy: {
							bias: 0.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CannonsRateRepeatable: TPlayerResearchDefinition = {
	id: 461,
	repeatable: true,
	caption: `Cannons Attack Rate ${INFINITY_SYMBOL}`,
	description: '+5% Cannons Attack Rate',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_NukeShells.id, PlayerResearch_CannonsRate3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseRate: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CannonsDamageRepeatable: TPlayerResearchDefinition = {
	id: 462,
	repeatable: true,
	caption: `Cannons Damage ${INFINITY_SYMBOL}`,
	description: '+2% Damage for Cannons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_NukeShells.id, PlayerResearch_CannonsDamage3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AnamorphicWinding: TPlayerResearchDefinition = {
	id: 470,
	caption: 'Anamorphic Winding',
	description: '+5 Hit Points to Tank, LAV and HAV; Equipment: Dianode',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_CannonsPrecisionRepeatable.id,
		PlayerResearch_CannonsRateRepeatable.id,
		PlayerResearch_CannonsDamageRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.hav]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[DianodeEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_CoriolisDrive: TPlayerResearchDefinition = {
	id: 471,
	caption: 'Coriolis Drive',
	description: '+1 Speed to Helipad, Carrier and Motherhsip; Equipment: Planetary Anchor',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_CannonsPrecisionRepeatable.id,
		PlayerResearch_CannonsRateRepeatable.id,
		PlayerResearch_CannonsDamageRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.helipad]: {
			chassis: [
				{
					speed: {
						bias: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.carrier]: {
			chassis: [
				{
					speed: {
						bias: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.mothership]: {
			chassis: [
				{
					speed: {
						bias: 1,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[PlanetaryAnchorEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_LivingMetal: TPlayerResearchDefinition = {
	id: 472,
	caption: 'Living Metal',
	description: '+1 Range to Cannons; Equipment: Ballistic Guide',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_CannonsPrecisionRepeatable.id,
		PlayerResearch_CannonsRateRepeatable.id,
		PlayerResearch_CannonsDamageRepeatable.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseRange: {
							bias: 1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[BallisticGuidanceModule.id]: 0.5,
		},
	},
};

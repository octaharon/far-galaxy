import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../tactical/damage/attack';
import NeutrinoGeneratorEquipment from '../../../../technologies/equipment/all/Defensive/NeutrinoGenerator.20003';
import WarpGeneratorEquipment from '../../../../technologies/equipment/all/Defensive/WarpGenerator.20004';
import FlowDisruptorEquipment from '../../../../technologies/equipment/all/Support/FlowDisruptor.20011';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import GravityHammer from '../../../../technologies/weapons/all/warp/GravityHammer.60003';
import MassCondenser from '../../../../technologies/weapons/all/warp/MassCondenser.60001';
import MassDisperser from '../../../../technologies/weapons/all/warp/MassDisperser.60002';
import WarpArrayWeapon from '../../../../technologies/weapons/all/warp/WarpArray.60004';
import WarpGun from '../../../../technologies/weapons/all/warp/WarpGun.60000';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

import {
	PlayerResearch_CannonsDamage1,
	PlayerResearch_CannonsDamage2,
	PlayerResearch_CannonsDamage3,
	PlayerResearch_CannonsPrecision1,
	PlayerResearch_CannonsPrecision2,
	PlayerResearch_CannonsPrecision3,
	PlayerResearch_CannonsRate1,
	PlayerResearch_CannonsRate2,
	PlayerResearch_CannonsRate3,
	PlayerResearch_ProtonShells,
} from './Cannons';
import {
	PlayerResearch_BombsDamage1,
	PlayerResearch_BombsDamage2,
	PlayerResearch_BombsDamage3,
	PlayerResearch_BombsRate1,
	PlayerResearch_BombsRate2,
	PlayerResearch_BombsRate3,
	PlayerResearch_IncendiaryBomb,
	PlayerResearch_LaunchersRange1,
	PlayerResearch_LaunchersRange2,
	PlayerResearch_LaunchersRange3,
} from './Launchers';
import {
	PlayerResearch_Marauder,
	PlayerResearch_PlasmaDamage1,
	PlayerResearch_PlasmaDamage2,
	PlayerResearch_PlasmaDamage3,
	PlayerResearch_PlasmaPrecision1,
	PlayerResearch_PlasmaPrecision2,
	PlayerResearch_PlasmaPrecision3,
	PlayerResearch_PlasmaRange1,
	PlayerResearch_PlasmaRange2,
	PlayerResearch_PlasmaRange3,
} from './Railguns';

export const PlayerResearch_WarpRate1: TPlayerResearchDefinition = {
	id: 1100,
	caption: 'Warp Weapons Rate I',
	description: '+5% Attack Rate for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_BombsRate1.id,
		PlayerResearch_CannonsRate1.id,
		PlayerResearch_PlasmaPrecision1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseRate: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpRate2: TPlayerResearchDefinition = {
	id: 1101,
	caption: 'Warp Weapons Rate II',
	description: '+5% Attack Rate for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_BombsRate2.id,
		PlayerResearch_CannonsRate2.id,
		PlayerResearch_PlasmaPrecision2.id,
		PlayerResearch_WarpRate1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseRate: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpRate3: TPlayerResearchDefinition = {
	id: 1102,
	caption: 'Warp Weapons Attack Rate III',
	description: '+5% Attack Rate for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BombsRate3.id,
		PlayerResearch_CannonsRate3.id,
		PlayerResearch_PlasmaPrecision3.id,
		PlayerResearch_WarpRate2.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseRate: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpDamage1: TPlayerResearchDefinition = {
	id: 1110,
	caption: 'Warp Weapons Damage I',
	description: '+3% Damage for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_BombsDamage1.id,
		PlayerResearch_CannonsDamage1.id,
		PlayerResearch_PlasmaDamage1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpDamage2: TPlayerResearchDefinition = {
	id: 1111,
	caption: 'Warp Weapons Damage II',
	description: '+3% Damage for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_BombsDamage2.id,
		PlayerResearch_CannonsDamage2.id,
		PlayerResearch_PlasmaDamage2.id,
		PlayerResearch_WarpDamage1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpDamage3: TPlayerResearchDefinition = {
	id: 1112,
	caption: 'Warp Weapons Damage III',
	description: '+3% Damage for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BombsDamage3.id,
		PlayerResearch_CannonsDamage3.id,
		PlayerResearch_PlasmaDamage3.id,
		PlayerResearch_WarpDamage2.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpRange1: TPlayerResearchDefinition = {
	id: 1120,
	caption: 'Warp Weapons Range I',
	description: '+0.15 Warp Weapons Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_LaunchersRange1.id,
		PlayerResearch_CannonsPrecision1.id,
		PlayerResearch_PlasmaRange1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseRange: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpRange2: TPlayerResearchDefinition = {
	id: 1121,
	caption: 'Warp Weapons Range II',
	description: '+0.15 Warp Weapons Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_LaunchersRange2.id,
		PlayerResearch_CannonsPrecision2.id,
		PlayerResearch_PlasmaRange2.id,
		PlayerResearch_WarpRange1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseRange: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpRange3: TPlayerResearchDefinition = {
	id: 1122,
	caption: 'Warp Weapons Range III',
	description: '+0.15 Warp Weapons Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_LaunchersRange3.id,
		PlayerResearch_CannonsPrecision3.id,
		PlayerResearch_PlasmaRange3.id,
		PlayerResearch_WarpRange2.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseRange: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpProjector: TPlayerResearchDefinition = {
	id: 1105,
	caption: 'Weapon: Warp Projector',
	description: WarpGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_Marauder.id,
		PlayerResearch_IncendiaryBomb.id,
		PlayerResearch_ProtonShells.id,
	],
	technologyReward: {
		weapons: {
			[WarpGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_MassCondenser: TPlayerResearchDefinition = {
	id: 1115,
	caption: 'Weapon: Mass Condenser',
	description: MassCondenser.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_WarpProjector.id,
		PlayerResearch_WarpRate1.id,
		PlayerResearch_WarpDamage1.id,
		PlayerResearch_WarpRange1.id,
	],
	technologyReward: {
		weapons: {
			[MassCondenser.id]: 0.5,
		},
	},
};
export const PlayerResearch_WarpDisperser: TPlayerResearchDefinition = {
	id: 1125,
	caption: 'Weapon: Mass Disperser',
	description: MassDisperser.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_MassCondenser.id,
		PlayerResearch_WarpRate2.id,
		PlayerResearch_WarpDamage2.id,
		PlayerResearch_WarpRange2.id,
	],
	technologyReward: {
		weapons: {
			[MassDisperser.id]: 0.5,
		},
	},
};
export const PlayerResearch_WarpArray: TPlayerResearchDefinition = {
	id: 1135,
	caption: 'Weapon: Warp Array',
	description: WarpArrayWeapon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_WarpDisperser.id,
		PlayerResearch_WarpRate3.id,
		PlayerResearch_WarpDamage3.id,
		PlayerResearch_WarpRange3.id,
	],
	technologyReward: {
		weapons: {
			[WarpArrayWeapon.id]: 0.5,
		},
	},
};
export const PlayerResearch_WarpRateRepeatable: TPlayerResearchDefinition = {
	id: 1130,
	repeatable: true,
	caption: `Warp Weapons Attack Rate ${INFINITY_SYMBOL}`,
	description: '+2% Attack Rate for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 64,
	prerequisiteResearchIds: [PlayerResearch_WarpRate3.id, PlayerResearch_WarpRange2.id, PlayerResearch_WarpDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseRate: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpDamageRepeatable: TPlayerResearchDefinition = {
	id: 1131,
	repeatable: true,
	caption: `Warp Weapons Damage ${INFINITY_SYMBOL}`,
	description: '+2% Damage for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 64,
	prerequisiteResearchIds: [PlayerResearch_WarpRate2.id, PlayerResearch_WarpRange2.id, PlayerResearch_WarpDamage3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WarpRangeRepeatable: TPlayerResearchDefinition = {
	id: 1132,
	repeatable: true,
	caption: `Warp Weapons Range ${INFINITY_SYMBOL}`,
	description: '+0.05 Attack Range for Warp Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 64,
	prerequisiteResearchIds: [PlayerResearch_WarpRate2.id, PlayerResearch_WarpRange3.id, PlayerResearch_WarpDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseRange: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GravityHammer: TPlayerResearchDefinition = {
	id: 1140,
	caption: 'Weapon: Gravity Hammer',
	description: GravityHammer.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 64,
	prerequisiteResearchIds: [
		PlayerResearch_WarpArray.id,
		PlayerResearch_WarpRateRepeatable.id,
		PlayerResearch_WarpDamageRepeatable.id,
		PlayerResearch_WarpRangeRepeatable.id,
	],
	technologyReward: {
		weapons: {
			[GravityHammer.id]: 0.5,
		},
	},
};
export const PlayerResearch_StrongForceBending: TPlayerResearchDefinition = {
	id: 1150,
	caption: 'Strong Force Bending',
	description: `+5 Shield Capacity for Droid, Exosuit and Scout; Equipment: Flow Disruptor`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 73,
	prerequisiteResearchIds: [PlayerResearch_WarpRange3.id, PlayerResearch_WarpDamage3.id, PlayerResearch_WarpRate3.id],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.scout]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[FlowDisruptorEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_PortableBlackHole: TPlayerResearchDefinition = {
	id: 1151,
	caption: 'Portable Black Holes',
	description: `-10% Warp Weapons Cost; Equipment: Warp Generator`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 73,
	prerequisiteResearchIds: [
		PlayerResearch_WarpRateRepeatable.id,
		PlayerResearch_WarpDamageRepeatable.id,
		PlayerResearch_WarpRangeRepeatable.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Warp]: {
						baseCost: {
							factor: 0.9,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[WarpGeneratorEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_MesonCloning: TPlayerResearchDefinition = {
	id: 1152,
	caption: 'Meson Cloning',
	description: `+5% Dodge against Beam Attacks; Equipment: Neutrino Generator`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 73,
	prerequisiteResearchIds: [
		PlayerResearch_WarpRateRepeatable.id,
		PlayerResearch_WarpDamageRepeatable.id,
		PlayerResearch_WarpRangeRepeatable.id,
	],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.beam]: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[NeutrinoGeneratorEquipment.id]: 0.5,
		},
	},
};

import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../tactical/damage/attack';
import MetacongitionInterfaceEquipment from '../../../../technologies/equipment/all/Support/MetacognitionInterface.20030';
import MainframeTacticalModule from '../../../../technologies/equipment/all/Support/MainframeTacticalModule.10006';
import DroneJammerEquipment from '../../../../technologies/equipment/all/Tactical/DroneJammer.20001';
import OverloadEquipment from '../../../../technologies/equipment/all/Warfare/Overload.30001';
import ClusterShield from '../../../../technologies/shields/all/large/ClusterShield.30013';
import GluonShield from '../../../../technologies/shields/all/large/GluonShield.30014';
import ImmortalShield from '../../../../technologies/shields/all/large/ImmortalShield.30010';
import IonCurtainShield from '../../../../technologies/shields/all/large/IonCurtain.30002';
import LargeCapacitorShield from '../../../../technologies/shields/all/large/LargeCapacitorShield.30003';
import LargeHullShield from '../../../../technologies/shields/all/large/LargeHullShield.30008';
import LargeNanoShield from '../../../../technologies/shields/all/large/LargeNanoShield.30007';
import LargeNeutroniumShield from '../../../../technologies/shields/all/large/LargeNeutroniumShield.30004';
import LargePlasmaShield from '../../../../technologies/shields/all/large/LargePlasmaShield.30005';
import LargeWarpShield from '../../../../technologies/shields/all/large/LargeWarpShield.30006';
import PhasingShield from '../../../../technologies/shields/all/large/PhasingShield.30000';
import RechargingShield from '../../../../technologies/shields/all/large/RechargingShield.30009';
import TargaShield from '../../../../technologies/shields/all/large/TargaShield.30001';
import WindShield from '../../../../technologies/shields/all/large/WindShield.30011';
import AntimatterShield from '../../../../technologies/shields/all/medium/AntimatterShield.20009';
import BulwarkShield from '../../../../technologies/shields/all/medium/BulwarkShield.20000';
import CapacitorShield from '../../../../technologies/shields/all/medium/CapacitorShield.20012';
import CarapaceShield from '../../../../technologies/shields/all/medium/CarapaceShield.20003';
import HardenedShield from '../../../../technologies/shields/all/medium/HardenedShield.20011';
import HullShield from '../../../../technologies/shields/all/medium/HullShield.20008';
import LocustShield from '../../../../technologies/shields/all/medium/LocustShield.20002';
import NanoShield from '../../../../technologies/shields/all/medium/NanoShield.20007';
import NeutroniumShield from '../../../../technologies/shields/all/medium/NeutroniumShield.20004';
import PlasmaShield from '../../../../technologies/shields/all/medium/PlasmaShield.20005';
import ProactiveShield from '../../../../technologies/shields/all/medium/ProactiveShield.20001';
import QuantumShield from '../../../../technologies/shields/all/medium/QuantumShield.20010';
import WarpShield from '../../../../technologies/shields/all/medium/WarpShield.20006';
import AegisShield from '../../../../technologies/shields/all/small/AegisShield.10002';
import AShield from '../../../../technologies/shields/all/small/AShield.10009';
import HystrixShield from '../../../../technologies/shields/all/small/HystrixShield.10003';
import ReactiveShield from '../../../../technologies/shields/all/small/ReactiveShield.10001';
import RepulsiveShield from '../../../../technologies/shields/all/small/RepulsiveShield.10000';
import SmallHullShield from '../../../../technologies/shields/all/small/SmallHullShield.10008';
import SmallNanoShield from '../../../../technologies/shields/all/small/SmallNanoShield.10007';
import SmallNeutroniumShield from '../../../../technologies/shields/all/small/SmallNeutroniumShield.10004';
import SmallPlasmaShield from '../../../../technologies/shields/all/small/SmallPlasmaShield.10005';
import SmallWarpShield from '../../../../technologies/shields/all/small/SmallWarpShield.10006';
import UnitChassis from '../../../../technologies/types/chassis';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_RepulsiveShield: TPlayerResearchDefinition = {
	id: 800,
	caption: 'Shield: Repulsive Shield',
	description: RepulsiveShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		shield: {
			[RepulsiveShield.id]: 1,
		},
	},
};
export const PlayerResearch_Aegis: TPlayerResearchDefinition = {
	id: 805,
	caption: 'Shield: Aegis',
	description: AegisShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_RepulsiveShield.id],
	technologyReward: {
		shield: {
			[AegisShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_BatteryLife1: TPlayerResearchDefinition = {
	id: 810,
	caption: 'Battery Life I',
	description: '+3% Shields Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_RepulsiveShield.id],
	unitModifier: {
		default: {
			shield: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_FieldCalculus1: TPlayerResearchDefinition = {
	id: 811,
	caption: 'Field Calculus I',
	description: '+3% Shield Capacity',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_RepulsiveShield.id],
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						factor: 1.03,
					},
				},
			],
		},
	},
};
export const PlayerResearch_MediumShield: TPlayerResearchDefinition = {
	id: 812,
	caption: 'Shield: Bulwark Shield',
	description: BulwarkShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_Aegis.id],
	technologyReward: {
		shield: {
			[BulwarkShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_AShield: TPlayerResearchDefinition = {
	id: 820,
	caption: 'Shield: A Shield',
	description: AShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_FieldCalculus1.id],
	technologyReward: {
		shield: {
			[AShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_ReactiveShield: TPlayerResearchDefinition = {
	id: 821,
	caption: 'Shield: Reactive Shield',
	description: ReactiveShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_BatteryLife1.id],
	technologyReward: {
		shield: {
			[ReactiveShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeShield: TPlayerResearchDefinition = {
	id: 822,
	caption: 'Shield: Phasing Shield',
	description: PhasingShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_MediumShield.id],
	technologyReward: {
		shield: {
			[PhasingShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_ProactiveShield: TPlayerResearchDefinition = {
	id: 823,
	caption: 'Shield: Proactive Shield',
	description: ProactiveShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_MediumShield.id],
	technologyReward: {
		shield: {
			[ProactiveShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_TargaShield: TPlayerResearchDefinition = {
	id: 825,
	caption: 'Shield: TARGA',
	description: TargaShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_LargeShield.id,
		PlayerResearch_BatteryLife1.id,
		PlayerResearch_FieldCalculus1.id,
	],
	technologyReward: {
		shield: {
			[TargaShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_HystrixShield: TPlayerResearchDefinition = {
	id: 826,
	caption: 'Shield: Hystrix',
	description: HystrixShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_Aegis.id,
		PlayerResearch_BatteryLife1.id,
		PlayerResearch_FieldCalculus1.id,
	],
	technologyReward: {
		shield: {
			[HystrixShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_CarapaceShield: TPlayerResearchDefinition = {
	id: 827,
	caption: 'Shield: Carapace Shield',
	description: CarapaceShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_ProactiveShield.id,
		PlayerResearch_BatteryLife1.id,
		PlayerResearch_FieldCalculus1.id,
	],
	technologyReward: {
		shield: {
			[CarapaceShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_BatteryLife2: TPlayerResearchDefinition = {
	id: 830,
	caption: 'Battery Life II',
	description: '+3% Shields Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Aegis.id, PlayerResearch_BatteryLife1.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_FieldCalculus2: TPlayerResearchDefinition = {
	id: 831,
	caption: 'Field Calculus II',
	description: '+3% Shield Capacity',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Aegis.id, PlayerResearch_FieldCalculus1.id],
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						factor: 1.03,
					},
				},
			],
		},
	},
};
export const PlayerResearch_ClusterShield: TPlayerResearchDefinition = {
	id: 835,
	caption: 'Shield: Cluster Shield',
	description: ClusterShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_TargaShield.id],
	technologyReward: {
		shield: {
			[ClusterShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_NeutroniumShieldSmall: TPlayerResearchDefinition = {
	id: 836,
	caption: 'Shield: Small Neutronium Shield',
	description: SmallNeutroniumShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [
		PlayerResearch_HystrixShield.id,
		PlayerResearch_BatteryLife2.id,
		PlayerResearch_FieldCalculus2.id,
	],
	technologyReward: {
		shield: {
			[SmallNeutroniumShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_LocustShield: TPlayerResearchDefinition = {
	id: 837,
	caption: 'Shield: Locust Shield',
	description: LocustShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [
		PlayerResearch_CarapaceShield.id,
		PlayerResearch_BatteryLife2.id,
		PlayerResearch_FieldCalculus2.id,
	],
	technologyReward: {
		shield: {
			[LocustShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_TransmissionPower1: TPlayerResearchDefinition = {
	id: 838,
	caption: 'Transmission Power I',
	description: '+0.25 Base Ability Power; +1 Recon Level',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [
		PlayerResearch_TargaShield.id,
		PlayerResearch_BatteryLife2.id,
		PlayerResearch_FieldCalculus2.id,
	],
	baseModifier: {
		intelligenceLevel: {
			bias: 1,
		},
		abilityPower: {
			bias: 0.25,
		},
	},
};
export const PlayerResearch_BatteryLife3: TPlayerResearchDefinition = {
	id: 840,
	caption: 'Battery Life III',
	description: '+3% Shields Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_CarapaceShield.id, PlayerResearch_BatteryLife2.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_FieldCalculus3: TPlayerResearchDefinition = {
	id: 841,
	caption: 'Field Calculus III',
	description: '+3% Shield Capacity',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_CarapaceShield.id, PlayerResearch_FieldCalculus2.id],
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						factor: 1.03,
					},
				},
			],
		},
	},
};
export const PlayerResearch_LargeNeutroniumShield: TPlayerResearchDefinition = {
	id: 842,
	caption: 'Shield: Large Neutronium Shied',
	description: LargeNeutroniumShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_NeutroniumShieldSmall.id, PlayerResearch_ClusterShield.id],
	technologyReward: {
		shield: {
			[LargeNeutroniumShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_NeutroniumShield: TPlayerResearchDefinition = {
	id: 843,
	caption: 'Shield: Neutronium Shield',
	description: NeutroniumShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_LocustShield.id, PlayerResearch_NeutroniumShieldSmall.id],
	technologyReward: {
		shield: {
			[NeutroniumShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_SmallNanoShield: TPlayerResearchDefinition = {
	id: 845,
	caption: 'Shield: Small Nano Shield',
	description: SmallNanoShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_HystrixShield.id, PlayerResearch_BatteryLife3.id],
	technologyReward: {
		shield: {
			[SmallNanoShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_NanoShield: TPlayerResearchDefinition = {
	id: 846,
	caption: 'Shield: Nano Shield',
	description: NanoShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_LocustShield.id, PlayerResearch_BatteryLife3.id],
	technologyReward: {
		shield: {
			[NanoShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeNanoShield: TPlayerResearchDefinition = {
	id: 847,
	caption: 'Shield: Large Nano Shield',
	description: LargeNanoShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_ClusterShield.id, PlayerResearch_BatteryLife3.id],
	technologyReward: {
		shield: {
			[LargeNanoShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_TransmissionPower2: TPlayerResearchDefinition = {
	id: 850,
	caption: 'Transmission Power II',
	description: '+0.25 Base Ability Power; +1 Recon Level',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [
		PlayerResearch_TransmissionPower1.id,
		PlayerResearch_BatteryLife3.id,
		PlayerResearch_FieldCalculus3.id,
	],
	baseModifier: {
		intelligenceLevel: {
			bias: 1,
		},
		abilityPower: {
			bias: 0.25,
		},
	},
};
export const PlayerResearch_SmallHullShield: TPlayerResearchDefinition = {
	id: 851,
	caption: 'Shield: Small Hull Shield',
	description: SmallHullShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_HystrixShield.id, PlayerResearch_FieldCalculus3.id],
	technologyReward: {
		shield: {
			[SmallHullShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_HullShield: TPlayerResearchDefinition = {
	id: 852,
	caption: 'Shield: Hull Shield',
	description: HullShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_LocustShield.id, PlayerResearch_FieldCalculus3.id],
	technologyReward: {
		shield: {
			[HullShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeHullShield: TPlayerResearchDefinition = {
	id: 853,
	caption: 'Shield: Large Hull Shield',
	description: LargeHullShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_ClusterShield.id, PlayerResearch_FieldCalculus3.id],
	technologyReward: {
		shield: {
			[LargeHullShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_EmissionTrenching1: TPlayerResearchDefinition = {
	id: 855,
	caption: 'Emission Trenching I',
	description: '+5% Man-O-War, Mothership and Submarine Dodge; Shield: Small Plasma Shield',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 26,
	prerequisiteResearchIds: [
		PlayerResearch_TransmissionPower1.id,
		PlayerResearch_BatteryLife3.id,
		PlayerResearch_FieldCalculus3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.mothership]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.manofwar]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.submarine]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		shield: {
			[SmallPlasmaShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_Metametrics1: TPlayerResearchDefinition = {
	id: 856,
	caption: 'Metametrics I',
	description: '+5 Combat Mech, Quadrapod and Fortress Hit Points; Shield: Small Warp Shield',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 26,
	prerequisiteResearchIds: [
		PlayerResearch_TransmissionPower1.id,
		PlayerResearch_BatteryLife3.id,
		PlayerResearch_FieldCalculus3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.mecha]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.quadrapod]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.fortress]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		shield: {
			[SmallWarpShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_BatteryLife4: TPlayerResearchDefinition = {
	id: 860,
	caption: 'Battery Life IV',
	description: '+3% Shields Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_BatteryLife3.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_FieldCalculus4: TPlayerResearchDefinition = {
	id: 861,
	caption: 'Field Calculus IV',
	description: '+3% Shield Capacity',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_FieldCalculus3.id],
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						factor: 1.03,
					},
				},
			],
		},
	},
};
export const PlayerResearch_PlasmaShield: TPlayerResearchDefinition = {
	id: 862,
	caption: 'Shield: Plasma Shield',
	description: PlasmaShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_EmissionTrenching1.id, PlayerResearch_LocustShield.id],
	technologyReward: {
		shield: {
			[PlasmaShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargePlasmaShield: TPlayerResearchDefinition = {
	id: 863,
	caption: 'Shield: Large Plasma Shield',
	description: LargePlasmaShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_EmissionTrenching1.id, PlayerResearch_ClusterShield.id],
	technologyReward: {
		shield: {
			[LargePlasmaShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_WarpShield: TPlayerResearchDefinition = {
	id: 864,
	caption: 'Shield: Warp Shield',
	description: WarpShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_LocustShield.id, PlayerResearch_Metametrics1.id],
	technologyReward: {
		shield: {
			[WarpShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeWarpShield: TPlayerResearchDefinition = {
	id: 865,
	caption: 'Shield: Large Warp Shield',
	description: LargeWarpShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Metametrics1.id, PlayerResearch_ClusterShield.id],
	technologyReward: {
		shield: {
			[LargeWarpShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_SMAShield: TPlayerResearchDefinition = {
	id: 870,
	caption: 'Shield: SMA Shield',
	description: CapacitorShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_NanoShield.id,
		PlayerResearch_HullShield.id,
		PlayerResearch_FieldCalculus4.id,
	],
	technologyReward: {
		shield: {
			[CapacitorShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeSMAShield: TPlayerResearchDefinition = {
	id: 871,
	caption: 'Shield: Large SMA Shield',
	description: LargeCapacitorShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_LargeNanoShield.id,
		PlayerResearch_LargeHullShield.id,
		PlayerResearch_FieldCalculus4.id,
	],
	technologyReward: {
		shield: {
			[LargeCapacitorShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_HardenedShield: TPlayerResearchDefinition = {
	id: 872,
	caption: 'Shield: Hardened Shield',
	description: HardenedShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_NanoShield.id,
		PlayerResearch_HullShield.id,
		PlayerResearch_BatteryLife4.id,
	],
	technologyReward: {
		shield: {
			[HardenedShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_ImmortalShield: TPlayerResearchDefinition = {
	id: 873,
	caption: 'Shield: Immortal Shield',
	description: ImmortalShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_LargeNanoShield.id,
		PlayerResearch_LargeHullShield.id,
		PlayerResearch_BatteryLife4.id,
	],
	technologyReward: {
		shield: {
			[ImmortalShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_QuantumShield: TPlayerResearchDefinition = {
	id: 874,
	caption: 'Shield: Quantum Shield',
	description: QuantumShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_LocustShield.id,
		PlayerResearch_BatteryLife4.id,
		PlayerResearch_FieldCalculus4.id,
	],
	technologyReward: {
		shield: {
			[QuantumShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_RechargingShield: TPlayerResearchDefinition = {
	id: 875,
	caption: 'Shield: Recharging Shield',
	description: RechargingShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_ClusterShield.id,
		PlayerResearch_BatteryLife4.id,
		PlayerResearch_FieldCalculus4.id,
	],
	technologyReward: {
		shield: {
			[RechargingShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_BatteryLife5: TPlayerResearchDefinition = {
	id: 880,
	caption: 'Battery Life V',
	description: '+3% Shields Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_BatteryLife4.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_FieldCalculus5: TPlayerResearchDefinition = {
	id: 881,
	caption: 'Field Calculus V',
	description: '+3% Shield Capacity',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_FieldCalculus4.id],
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						factor: 1.03,
					},
				},
			],
		},
	},
};
export const PlayerResearch_TransmissionPower3: TPlayerResearchDefinition = {
	id: 882,
	caption: 'Transmission Power III',
	description: '+0.25 Base Ability Power; +1 Recon Level',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_TransmissionPower2.id,
		PlayerResearch_BatteryLife4.id,
		PlayerResearch_FieldCalculus4.id,
	],
	baseModifier: {
		intelligenceLevel: {
			bias: 1,
		},
		abilityPower: {
			bias: 0.25,
		},
	},
};
export const PlayerResearch_AntimatterShield: TPlayerResearchDefinition = {
	id: 885,
	caption: 'Shield: Antimatter Shield',
	description: AntimatterShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_NanoShield.id,
		PlayerResearch_BatteryLife5.id,
		PlayerResearch_FieldCalculus5.id,
	],
	technologyReward: {
		shield: {
			[AntimatterShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_GluonShield: TPlayerResearchDefinition = {
	id: 886,
	caption: 'Shield: Gluon Shield',
	description: GluonShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_LargePlasmaShield.id,
		PlayerResearch_BatteryLife5.id,
		PlayerResearch_FieldCalculus5.id,
	],
	technologyReward: {
		shield: {
			[GluonShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_IonCurtain: TPlayerResearchDefinition = {
	id: 887,
	caption: 'Shield: Ion Curtain',
	description: IonCurtainShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_LargeWarpShield.id,
		PlayerResearch_BatteryLife5.id,
		PlayerResearch_FieldCalculus5.id,
	],
	technologyReward: {
		shield: {
			[IonCurtainShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_EmissionTrenching2: TPlayerResearchDefinition = {
	id: 888,
	caption: 'Emission Trenching II',
	description: '+5% Man-O-War, Mothership and Submarine Dodge; Equipment: Overload Compensator',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_TransmissionPower3.id, PlayerResearch_EmissionTrenching1.id],
	unitModifier: {
		[UnitChassis.chassisClass.mothership]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.manofwar]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.submarine]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[OverloadEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_Metametrics2: TPlayerResearchDefinition = {
	id: 889,
	caption: 'Metametrics II',
	description: '+5 Combat Mech, Quadrapod and Fortress Hit Points; Equipment: Mainframe Datacore',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_Metametrics1.id, PlayerResearch_TransmissionPower3.id],
	unitModifier: {
		[UnitChassis.chassisClass.mecha]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.quadrapod]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.fortress]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[MainframeTacticalModule.id]: 0.5,
		},
	},
};
export const PlayerResearch_WindShield: TPlayerResearchDefinition = {
	id: 890,
	caption: 'Shield: WIND',
	description: WindShield.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_LargeWarpShield.id,
		PlayerResearch_LargePlasmaShield.id,
		PlayerResearch_FieldCalculus5.id,
		PlayerResearch_BatteryLife5.id,
	],
	technologyReward: {
		shield: {
			[WindShield.id]: 0.5,
		},
	},
};
export const PlayerResearch_VelocitySuppression: TPlayerResearchDefinition = {
	id: 891,
	caption: 'Velocity Suppression',
	description:
		'+10% Dodge vs Ballistic and Missile for Corvette, Rover and Tiltjet. +100 Energy Storage. Equipment: Drone Jammer',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_Metametrics2.id, PlayerResearch_EmissionTrenching2.id],
	baseModifier: {
		maxEnergyCapacity: {
			bias: 100,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.1,
						},
						[UnitAttack.deliveryType.ballistic]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.1,
						},
						[UnitAttack.deliveryType.ballistic]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.tiltjet]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.1,
						},
						[UnitAttack.deliveryType.ballistic]: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[DroneJammerEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_MatterRecognition: TPlayerResearchDefinition = {
	id: 892,
	caption: 'Matter Recognition',
	description:
		'+5% Shield Protection for Tank, Hovertank and CombatMech. +1 Ability Slot. Equipment: Metacognition Interface',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_Metametrics2.id, PlayerResearch_EmissionTrenching2.id],
	reward: (p) => {
		p.abilitySlots += 1;
		return p;
	},
	unitModifier: {
		[UnitChassis.chassisClass.tank]: {
			shield: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
		[UnitChassis.chassisClass.hovertank]: {
			shield: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
		[UnitChassis.chassisClass.mecha]: {
			shield: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[MetacongitionInterfaceEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_ShieldRepeatable: TPlayerResearchDefinition = {
	repeatable: true,
	id: 895,
	caption: `Shield Capacity ${INFINITY_SYMBOL}`,
	description: '+8% Shield Capacity on all Units',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 47,
	prerequisiteResearchIds: [PlayerResearch_Metametrics2.id, PlayerResearch_EmissionTrenching2.id],
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						factor: 1.08,
					},
				},
			],
		},
	},
};

import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import TiberiumBatteryEquipment from '../../../../technologies/equipment/all/Enhancement/TiberiumBattery.20010';
import RecoilCapacitorEquipment from '../../../../technologies/equipment/all/Offensive/RecoilCapacitor.20007';
import DoomsdayDeviceEquipment from '../../../../technologies/equipment/all/Warfare/DoomsdayDevice.30002';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import CathodeGun from '../../../../technologies/weapons/all/plasma/CathodeGun.50003';
import FlamethrowerWeapon from '../../../../technologies/weapons/all/plasma/Flamethrower.50002';
import GrapesOfWrath from '../../../../technologies/weapons/all/plasma/GrapesOfWrath.50004';
import MaradeurWeapon from '../../../../technologies/weapons/all/plasma/Marauder.50001';
import PlasmaGun from '../../../../technologies/weapons/all/plasma/PlasmaGun.50000';
import AntimatterCannon from '../../../../technologies/weapons/all/railguns/AntimatterCannon.25006';
import CoilMortar from '../../../../technologies/weapons/all/railguns/CoilMortar.25004';
import Hypercharger from '../../../../technologies/weapons/all/railguns/Hypercharger.25002';
import RadioRailgun from '../../../../technologies/weapons/all/railguns/RadioRailgun.25001';
import RailgunCannon from '../../../../technologies/weapons/all/railguns/RailgunCannon.25000';
import RapidCoilGun from '../../../../technologies/weapons/all/railguns/RapidCoilGun.25003';
import TyphoonWeapon from '../../../../technologies/weapons/all/railguns/Typhoon.25005';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_RailgunCannon: TPlayerResearchDefinition = {
	id: 600,
	caption: 'Weapon: Railgun Cannon',
	description: RailgunCannon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		weapons: {
			[RailgunCannon.id]: 1,
		},
	},
};
export const PlayerResearch_RadioRailgun: TPlayerResearchDefinition = {
	id: 605,
	caption: 'Weapon: Radium Cannon',
	description: RadioRailgun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_RailgunCannon.id],
	technologyReward: {
		weapons: {
			[RadioRailgun.id]: 0.5,
		},
	},
};
export const PlayerResearch_RailgunsRange1: TPlayerResearchDefinition = {
	id: 610,
	caption: 'Mass Drivers Range I',
	description: '+0.25 Range for Mass Drivers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_RailgunCannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsRate1: TPlayerResearchDefinition = {
	id: 611,
	caption: 'Mass Drivers Rate I',
	description: '+0.05 Mass Drivers Attack Rate',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_RailgunCannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsDamage1: TPlayerResearchDefinition = {
	id: 612,
	caption: 'Mass Drivers Damage I',
	description: '+5% Damage for Mass Drivers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_RailgunCannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RapidCoilGun: TPlayerResearchDefinition = {
	id: 615,
	caption: 'Weapon: Rapid Coil Gun',
	description: RapidCoilGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [
		PlayerResearch_RadioRailgun.id,
		PlayerResearch_RailgunsRate1.id,
		PlayerResearch_RailgunsDamage1.id,
		PlayerResearch_RailgunsRange1.id,
	],
	technologyReward: {
		weapons: {
			[RapidCoilGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_PlasmaGun: TPlayerResearchDefinition = {
	id: 616,
	caption: 'Weapon: Plasma Gun',
	description: PlasmaGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [
		PlayerResearch_RadioRailgun.id,
		PlayerResearch_RailgunsRange1.id,
		PlayerResearch_RailgunsDamage1.id,
		PlayerResearch_RailgunsRate1.id,
	],
	technologyReward: {
		weapons: {
			[PlasmaGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_RailgunsRange2: TPlayerResearchDefinition = {
	id: 620,
	caption: 'Mass Drivers Range II',
	description: '+0.25 Range for Mass Drivers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_RadioRailgun.id, PlayerResearch_RailgunsRange1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsRate2: TPlayerResearchDefinition = {
	id: 621,
	caption: 'Mass Drivers Rate II',
	description: '+0.05 Mass Drivers Attack Rate',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_RadioRailgun.id, PlayerResearch_RailgunsRate1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsDamage2: TPlayerResearchDefinition = {
	id: 622,
	caption: 'Mass Drivers Damage II',
	description: '+5% Damage for Mass Drivers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_RadioRailgun.id, PlayerResearch_RailgunsDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Typhoon: TPlayerResearchDefinition = {
	id: 630,
	caption: 'Weapon: Typhoon',
	description: TyphoonWeapon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_RapidCoilGun.id,
		PlayerResearch_RailgunsRange2.id,
		PlayerResearch_RailgunsDamage2.id,
		PlayerResearch_RailgunsRate2.id,
	],
	technologyReward: {
		weapons: {
			[TyphoonWeapon.id]: 0.5,
		},
	},
};
export const PlayerResearch_Marauder: TPlayerResearchDefinition = {
	id: 631,
	caption: 'Weapon: Marauder',
	description: MaradeurWeapon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_PlasmaGun.id,
		PlayerResearch_RailgunsRange2.id,
		PlayerResearch_RailgunsDamage2.id,
		PlayerResearch_RailgunsRate2.id,
	],
	technologyReward: {
		weapons: {
			[MaradeurWeapon.id]: 0.5,
		},
	},
};
export const PlayerResearch_PlasmaPrecision1: TPlayerResearchDefinition = {
	id: 635,
	caption: 'Plasma Precision I',
	description: '+0.05 Hit Chance for Plasma',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_PlasmaGun.id, PlayerResearch_RailgunsRate2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_PlasmaRange1: TPlayerResearchDefinition = {
	id: 636,
	caption: 'Plasma Range I',
	description: '+0.1 Plasma Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_PlasmaGun.id, PlayerResearch_RailgunsRange2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						baseRange: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_PlasmaDamage1: TPlayerResearchDefinition = {
	id: 637,
	caption: 'Plasma Damage I',
	description: '+5% Damage for Plasma',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_PlasmaGun.id, PlayerResearch_RailgunsDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsRange3: TPlayerResearchDefinition = {
	id: 640,
	caption: 'Mass Drivers Range III',
	description: '+0.25 Range for Mass Drivers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RapidCoilGun.id, PlayerResearch_RailgunsRange2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsRate3: TPlayerResearchDefinition = {
	id: 641,
	caption: 'Mass Drivers Rate III',
	description: '+0.05 Mass Drivers Attack Rate',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RapidCoilGun.id, PlayerResearch_RailgunsRate2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsDamage3: TPlayerResearchDefinition = {
	id: 642,
	caption: 'Mass Drives Damage III',
	description: '+5% Damage for Mass Drivers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RapidCoilGun.id, PlayerResearch_RailgunsDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CoilMortar: TPlayerResearchDefinition = {
	id: 650,
	caption: 'Weapon: Coil Mortar',
	description: CoilMortar.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_RailgunsDamage3.id,
		PlayerResearch_RailgunsRange3.id,
		PlayerResearch_RailgunsRate3.id,
	],
	technologyReward: {
		weapons: {
			[CoilMortar.id]: 0.5,
		},
	},
};
export const PlayerResearch_Flamethrower: TPlayerResearchDefinition = {
	id: 651,
	caption: 'Weapon: Flamethrower',
	description: FlamethrowerWeapon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_Marauder.id,
		PlayerResearch_PlasmaRange1.id,
		PlayerResearch_PlasmaPrecision1.id,
		PlayerResearch_PlasmaDamage1.id,
	],
	technologyReward: {
		weapons: {
			[FlamethrowerWeapon.id]: 0.5,
		},
	},
};
export const PlayerResearch_AntimatterCannon: TPlayerResearchDefinition = {
	id: 652,
	caption: 'Weapon: Antimatter Cannon',
	description: AntimatterCannon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_Marauder.id,
		PlayerResearch_RailgunsDamage3.id,
		PlayerResearch_RailgunsRange3.id,
		PlayerResearch_RailgunsRate3.id,
	],
	technologyReward: {
		weapons: {
			[AntimatterCannon.id]: 0.5,
		},
	},
};
export const PlayerResearch_PlasmaPrecision2: TPlayerResearchDefinition = {
	id: 656,
	caption: 'Plasma Precision II',
	description: '+5% Hit Chance for Plasma',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Marauder.id, PlayerResearch_PlasmaPrecision1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_PlasmaRange2: TPlayerResearchDefinition = {
	id: 657,
	caption: 'Plasma Range II',
	description: '+0.1 Plasma Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Marauder.id, PlayerResearch_PlasmaRange1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						baseRange: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_PlasmaDamage2: TPlayerResearchDefinition = {
	id: 658,
	caption: 'Plasma Damage II',
	description: '+5% Damage for Plasma',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Marauder.id, PlayerResearch_PlasmaDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CathodeGun: TPlayerResearchDefinition = {
	id: 660,
	caption: 'Weapon: Cathode Gun',
	description: CathodeGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_Flamethrower.id,
		PlayerResearch_PlasmaDamage2.id,
		PlayerResearch_PlasmaPrecision2.id,
		PlayerResearch_PlasmaRange2.id,
	],
	technologyReward: {
		weapons: {
			[CathodeGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_PlasmaPrecision3: TPlayerResearchDefinition = {
	id: 665,
	caption: 'Plasma Precision III',
	description: '+5% Hit Chance for Plasma',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_Flamethrower.id, PlayerResearch_PlasmaPrecision2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_PlasmaRange3: TPlayerResearchDefinition = {
	id: 666,
	caption: 'Plasma Range III',
	description: '+0.1 Plasma Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_Flamethrower.id, PlayerResearch_PlasmaRange2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						baseRange: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_PlasmaDamage3: TPlayerResearchDefinition = {
	id: 667,
	caption: 'Plasma Damage III',
	description: '+5% Damage for Plasma',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_Flamethrower.id, PlayerResearch_PlasmaDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsRangeRepeatable: TPlayerResearchDefinition = {
	id: 670,
	repeatable: true,
	caption: `Mass Drivers and Plasma Range ${INFINITY_SYMBOL}`,
	description: '+0.05 Mass Drivers and Plasma Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_RailgunsRange3.id, PlayerResearch_PlasmaRange3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRange: {
							bias: 0.05,
						},
					},
					[Weapons.TWeaponGroupType.Plasma]: {
						baseRange: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsPrecisionRepeatable: TPlayerResearchDefinition = {
	id: 671,
	repeatable: true,
	caption: `Mass Drivers and Plasma Precision ${INFINITY_SYMBOL}`,
	description: '+2% Hit Chance for Mass Drivers and Plasma',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_RailgunsRate3.id, PlayerResearch_PlasmaPrecision3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseAccuracy: {
							bias: 0.02,
						},
					},
					[Weapons.TWeaponGroupType.Plasma]: {
						baseAccuracy: {
							bias: 0.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RailgunsDamageRepeatable: TPlayerResearchDefinition = {
	id: 672,
	repeatable: true,
	caption: `Mass Drivers and Plasma Damage ${INFINITY_SYMBOL}`,
	description: '+1% Damage for Mass Drivers and Plasma',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_PlasmaDamage3.id, PlayerResearch_RailgunsDamage3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						damageModifier: {
							factor: 1.01,
						},
					},
					[Weapons.TWeaponGroupType.Plasma]: {
						damageModifier: {
							factor: 1.01,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GrapesOfWrath: TPlayerResearchDefinition = {
	id: 675,
	caption: 'Weapon: Grapes Of Wrath',
	description: GrapesOfWrath.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [
		PlayerResearch_CathodeGun.id,
		PlayerResearch_PlasmaDamage3.id,
		PlayerResearch_PlasmaPrecision3.id,
		PlayerResearch_PlasmaRange3.id,
	],
	technologyReward: {
		weapons: {
			[GrapesOfWrath.id]: 0.5,
		},
	},
};
export const PlayerResearch_Hypercharger: TPlayerResearchDefinition = {
	id: 680,
	caption: 'Weapon: Hypercharger',
	description: Hypercharger.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_CoilMortar.id,
		PlayerResearch_RailgunsDamage3.id,
		PlayerResearch_RailgunsRange3.id,
		PlayerResearch_RailgunsRate3.id,
	],
	technologyReward: {
		weapons: {
			[Hypercharger.id]: 0.5,
		},
	},
};
export const PlayerResearch_RelicCarbonification: TPlayerResearchDefinition = {
	id: 685,
	caption: 'Relic Carbonification',
	description: '+5 Hit Points for Launcher Pad, Helipad and Carrier. Equipment: Recoil Capacitor',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_RailgunsRangeRepeatable.id,
		PlayerResearch_RailgunsPrecisionRepeatable.id,
		PlayerResearch_RailgunsDamageRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.launcherpad]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.helipad]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.carrier]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[RecoilCapacitorEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_SublightZoning: TPlayerResearchDefinition = {
	id: 686,
	caption: 'Sublight Zoning',
	description: '+5% Dodge for Fighter, Bomber and Strike Aircraft. Equipment: Tiberium Battery',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_RailgunsRangeRepeatable.id,
		PlayerResearch_RailgunsPrecisionRepeatable.id,
		PlayerResearch_RailgunsDamageRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.fighter]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.5,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.bomber]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.5,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.5,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[TiberiumBatteryEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_CrystallineProjection: TPlayerResearchDefinition = {
	id: 687,
	caption: 'Crystalline Projection',
	description: '+5% Hit Chance for Biotech, Hydra and Swarm. Equipment: Doomsday Device',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_RailgunsRangeRepeatable.id,
		PlayerResearch_RailgunsPrecisionRepeatable.id,
		PlayerResearch_RailgunsDamageRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.biotech]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hydra]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.swarm]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[DoomsdayDeviceEquipment.id]: 0.5,
		},
	},
};

import AscendancySummitEstablishment from '../../../../orbital/establishments/all/AscendancySummit.503';
import BraintankEstablishment from '../../../../orbital/establishments/all/Braintank.101';
import CampusEstablishment from '../../../../orbital/establishments/all/Campus.400';
import DataWarehouseEstablishment from '../../../../orbital/establishments/all/DataWarehouse.202';
import FrequencyArrayEstablishment from '../../../../orbital/establishments/all/FrequencyArray.203';
import GrinderEstablishment from '../../../../orbital/establishments/all/Grinder.402';
import InvestigationBureauEstablishment from '../../../../orbital/establishments/all/InvestigationBureau.200';
import ManufactoryEstablishment from '../../../../orbital/establishments/all/Manufactory.403';
import ProvingGroundEstablishment from '../../../../orbital/establishments/all/ProvingGround.401';
import ReconnaissanceAgencyEstablishment from '../../../../orbital/establishments/all/ReconnaissanceAgency.201';
import ScienceLabEstablishment from '../../../../orbital/establishments/all/ScienceLab.100';
import SimulationChamber from '../../../../orbital/establishments/all/SimulationChamber.404';
import TachyonLatticeEstablishment from '../../../../orbital/establishments/all/TachyonLattice.204';
import VRNetworkEstablishment from '../../../../orbital/establishments/all/VRNetwork.102';
import PlanetCrackerFacility from '../../../../orbital/facilities/all/PlanetCracker.408';
import UnitAttack from '../../../../tactical/damage/attack';
import PolarisOmniTool from '../../../../technologies/equipment/all/Offensive/PolarisOmniTool.20014';
import AssistedTrainingModuleEquipment from '../../../../technologies/equipment/all/Support/AssistedTrainingModule.20022';
import DefenseNetworkEquipment from '../../../../technologies/equipment/all/Tactical/DefenseNetwork.10008';
import NobleGuardChipEquipment from '../../../../technologies/equipment/all/Tactical/NobleGuardChip.20015';
import UnitChassis from '../../../../technologies/types/chassis';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_ScienceLab: TPlayerResearchDefinition = {
	id: 900,
	caption: 'Establishment: Science Lab',
	description: ScienceLabEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST / 2,
	technologyReward: {
		establishments: {
			[ScienceLabEstablishment.id]: 1,
		},
	},
};
export const PlayerResearch_BrainTank: TPlayerResearchDefinition = {
	id: 905,
	caption: 'Establishment: BrainTank',
	description: BraintankEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_ScienceLab.id],
	technologyReward: {
		establishments: {
			[BraintankEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_InvestigationBureau: TPlayerResearchDefinition = {
	id: 906,
	caption: 'Establishment: Investigation Bureau',
	description: InvestigationBureauEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_ScienceLab.id],
	technologyReward: {
		establishments: {
			[InvestigationBureauEstablishment.id]: 1,
		},
	},
};
export const PlayerResearch_Campus: TPlayerResearchDefinition = {
	id: 907,
	caption: 'Establishment: Campus',
	description: CampusEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_ScienceLab.id],
	technologyReward: {
		establishments: {
			[CampusEstablishment.id]: 1,
		},
	},
};
export const PlayerResearch_CryptographicImperative1: TPlayerResearchDefinition = {
	id: 910,
	caption: 'Cryptographic Imperative I',
	description: '+3% Base Research Output; +1 Counter-Recon Level',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_ScienceLab.id],
	baseModifier: {
		researchOutput: {
			factor: 1.03,
		},
		counterIntelligenceLevel: {
			bias: 1,
		},
	},
};
export const PlayerResearch_FuzzyOptimization1: TPlayerResearchDefinition = {
	id: 911,
	caption: 'Fuzzy Optimization I',
	description: '-2% Base Energy Consumption; +0.25 Unit Ability Power',
	prerequisiteResearchIds: [PlayerResearch_ScienceLab.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	baseModifier: {
		energyConsumption: {
			factor: 0.98,
		},
	},
	unitModifier: {
		default: {
			unitModifier: {
				abilityPowerModifier: [
					{
						bias: 0.25,
					},
				],
			},
		},
	},
};
export const PlayerResearch_DarkMatterComposites1: TPlayerResearchDefinition = {
	id: 912,
	caption: 'Dark Matter Composites I',
	description: '+2% Base Engineering Output; +0.25 Unit Scan Range',
	prerequisiteResearchIds: [PlayerResearch_ScienceLab.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	baseModifier: {
		engineeringOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		default: {
			chassis: [
				{
					scanRange: {
						bias: 0.25,
					},
				},
			],
		},
	},
};
export const PlayerResearch_ReconnaissanceAgency: TPlayerResearchDefinition = {
	id: 920,
	caption: 'Establishment: Reconnaissance Agency',
	description: ReconnaissanceAgencyEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_InvestigationBureau.id, PlayerResearch_CryptographicImperative1.id],
	technologyReward: {
		establishments: {
			[ReconnaissanceAgencyEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_DataWarehouse: TPlayerResearchDefinition = {
	id: 921,
	caption: 'Establishment: Data Warehouse',
	description: DataWarehouseEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_InvestigationBureau.id, PlayerResearch_FuzzyOptimization1.id],
	technologyReward: {
		establishments: {
			[DataWarehouseEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_ProvingGrounds: TPlayerResearchDefinition = {
	id: 922,
	caption: 'Establishment: Proving Grounds',
	description: ProvingGroundEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Campus.id, PlayerResearch_DarkMatterComposites1.id],
	technologyReward: {
		establishments: {
			[ProvingGroundEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_HeuristicAnalytics: TPlayerResearchDefinition = {
	id: 925,
	caption: 'Heuristic Analytics',
	description: `Support Units gain +10% Dodge vs Missile and -2 Production Cost`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [
		PlayerResearch_CryptographicImperative1.id,
		PlayerResearch_FuzzyOptimization1.id,
		PlayerResearch_DarkMatterComposites1.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.sup_vehicle]: {
			chassis: [
				{
					baseCost: {
						bias: -2,
						min: 2,
						minGuard: 2,
					},
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					baseCost: {
						bias: -2,
						min: 2,
						minGuard: 2,
					},
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_aircraft]: {
			chassis: [
				{
					baseCost: {
						bias: -2,
						min: 2,
						minGuard: 2,
					},
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CryptographicImperative2: TPlayerResearchDefinition = {
	id: 930,
	caption: 'Cryptographic Imperative II',
	description: '+3% Base Research Output; +1 Counter-Recon Level',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_BrainTank.id, PlayerResearch_CryptographicImperative1.id],
	baseModifier: {
		researchOutput: {
			factor: 1.03,
		},
		counterIntelligenceLevel: {
			bias: 1,
		},
	},
};
export const PlayerResearch_FuzzyOptimization2: TPlayerResearchDefinition = {
	id: 931,
	caption: 'Fuzzy Optimization II',
	description: '-2% Base Energy Consumption; +0.25 Unit Ability Power',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_BrainTank.id, PlayerResearch_FuzzyOptimization1.id],

	baseModifier: {
		energyConsumption: {
			factor: 0.98,
		},
	},
	unitModifier: {
		default: {
			unitModifier: {
				abilityPowerModifier: [
					{
						bias: 0.25,
					},
				],
			},
		},
	},
};
export const PlayerResearch_DarkMatterComposites2: TPlayerResearchDefinition = {
	id: 932,
	caption: 'Dark Matter Composites II',
	description: '+2% Base Engineering Output; +0.25 Unit Scan Range',
	prerequisiteResearchIds: [PlayerResearch_BrainTank.id, PlayerResearch_DarkMatterComposites1.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	baseModifier: {
		engineeringOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		default: {
			chassis: [
				{
					scanRange: {
						bias: 0.25,
					},
				},
			],
		},
	},
};
export const PlayerResearch_VPRNetwork: TPlayerResearchDefinition = {
	id: 940,
	caption: 'Establishment: VPR Network',
	description: VRNetworkEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_FuzzyOptimization2.id, PlayerResearch_CryptographicImperative2.id],
	technologyReward: {
		establishments: {
			[VRNetworkEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_SimulationChamber: TPlayerResearchDefinition = {
	id: 941,
	caption: 'Establishment: Simulation Chamber',
	description: SimulationChamber.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_FuzzyOptimization2.id, PlayerResearch_DarkMatterComposites2.id],
	technologyReward: {
		establishments: {
			[SimulationChamber.id]: 0.5,
		},
	},
};
export const PlayerResearch_Grinder: TPlayerResearchDefinition = {
	id: 942,
	caption: 'Establishment: Grinder',
	description: GrinderEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_ProvingGrounds.id, PlayerResearch_DarkMatterComposites2.id],
	technologyReward: {
		establishments: {
			[GrinderEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_DistributedSimulation: TPlayerResearchDefinition = {
	id: 943,
	caption: 'Distributed Simulation',
	description: `Infantry, Exosuit and Combat Mech gain +15% Bombardment Dodge; Equipment: Defense Network`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_HeuristicAnalytics.id,
		PlayerResearch_CryptographicImperative2.id,
		PlayerResearch_FuzzyOptimization2.id,
		PlayerResearch_DarkMatterComposites2.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.bombardment]: {
							bias: 0.15,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.mecha]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.bombardment]: {
							bias: 0.15,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.bombardment]: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[DefenseNetworkEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_FrequencyArray: TPlayerResearchDefinition = {
	id: 945,
	caption: 'Establishment: Frequency Array',
	description: FrequencyArrayEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_ReconnaissanceAgency.id, PlayerResearch_CryptographicImperative2.id],
	technologyReward: {
		establishments: {
			[FrequencyArrayEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_TachyonLattice: TPlayerResearchDefinition = {
	id: 946,
	caption: 'Establishment: Tachyon Lattice',
	description: TachyonLatticeEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_DataWarehouse.id, PlayerResearch_FuzzyOptimization2.id],
	technologyReward: {
		establishments: {
			[TachyonLatticeEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_CryptographicImperative3: TPlayerResearchDefinition = {
	id: 950,
	caption: 'Cryptographic Imperative III',
	description: '+3% Base Research Output; +1 Counter-Recon Level',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_ProvingGrounds.id, PlayerResearch_CryptographicImperative2.id],
	baseModifier: {
		researchOutput: {
			factor: 1.03,
		},
		counterIntelligenceLevel: {
			bias: 1,
		},
	},
};
export const PlayerResearch_FuzzyOptimization3: TPlayerResearchDefinition = {
	id: 951,
	caption: 'Fuzzy Optimization III',
	description: '-2% Base Energy Consumption; +0.25 Unit Ability Power',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_ProvingGrounds.id, PlayerResearch_FuzzyOptimization2.id],

	baseModifier: {
		energyConsumption: {
			factor: 0.98,
		},
	},
	unitModifier: {
		default: {
			unitModifier: {
				abilityPowerModifier: [
					{
						bias: 0.25,
					},
				],
			},
		},
	},
};
export const PlayerResearch_DarkMatterComposites3: TPlayerResearchDefinition = {
	id: 952,
	caption: 'Dark Matter Composites III',
	description: '+2% Base Engineering Output; +0.25 Unit Scan Range',
	prerequisiteResearchIds: [PlayerResearch_ProvingGrounds.id, PlayerResearch_DarkMatterComposites2.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	baseModifier: {
		engineeringOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		default: {
			chassis: [
				{
					scanRange: {
						bias: 0.25,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Manufactory: TPlayerResearchDefinition = {
	id: 960,
	caption: 'Establishment: Manufactory',
	description: ManufactoryEstablishment.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_Grinder.id,
		PlayerResearch_DarkMatterComposites3.id,
		PlayerResearch_FuzzyOptimization3.id,
		PlayerResearch_CryptographicImperative3.id,
	],
	technologyReward: {
		establishments: {
			[ManufactoryEstablishment.id]: 0.5,
		},
	},
};
export const PlayerResearch_DeepCommand: TPlayerResearchDefinition = {
	id: 961,
	caption: 'Deep Command',
	description: `Artillery, Tank and Scout gain +0.5 Speed; Equipment: Noble Guard Chip`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_DistributedSimulation.id,
		PlayerResearch_CryptographicImperative3.id,
		PlayerResearch_FuzzyOptimization3.id,
		PlayerResearch_DarkMatterComposites3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.artillery]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[NobleGuardChipEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_Metablockchain: TPlayerResearchDefinition = {
	id: 962,
	caption: 'Metablockchain',
	description: `Battleship, Point Defense and Hovertank gain +0.5 Scan Range; Equipment: Polaris Omni-Tool`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_VPRNetwork.id,
		PlayerResearch_SimulationChamber.id,
		PlayerResearch_CryptographicImperative3.id,
		PlayerResearch_FuzzyOptimization3.id,
		PlayerResearch_DarkMatterComposites3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.battleship]: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.hovertank]: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[PolarisOmniTool.id]: 0.5,
		},
	},
};
export const PlayerResearch_HyperpackedEnergyCells: TPlayerResearchDefinition = {
	id: 970,
	caption: 'Hyperpacked Energy Cells',
	description: `+10 Shield Capacity for Cruiser, Carrier and Launcher Pad; Facility: Planet Cracker`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_DeepCommand.id, PlayerResearch_Metablockchain.id],
	unitModifier: {
		[UnitChassis.chassisClass.cruiser]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.carrier]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.launcherpad]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[PlanetCrackerFacility.id]: 0.5,
		},
	},
};
export const PlayerResearch_TransuraniumCrystal: TPlayerResearchDefinition = {
	id: 971,
	caption: 'Transuranium Crystals',
	description: `+0.5 Speed for Biotech, Hydra and Swarm; Facility: Assisted Training Module`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_DeepCommand.id, PlayerResearch_Metablockchain.id],
	unitModifier: {
		[UnitChassis.chassisClass.hydra]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.swarm]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[AssistedTrainingModuleEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_PsionicAscendancy: TPlayerResearchDefinition = {
	id: 972,
	caption: 'Psionic Ascendancy',
	description: `+5 Hit Points for Strike Aircraft, Rover and Combat Drone; Establishment: Ascendancy Summit`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_DeepCommand.id, PlayerResearch_Metablockchain.id],
	unitModifier: {
		[UnitChassis.chassisClass.strike_aircraft]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		establishments: {
			[AscendancySummitEstablishment.id]: 0.5,
		},
	},
};

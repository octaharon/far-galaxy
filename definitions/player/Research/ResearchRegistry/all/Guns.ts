import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../tactical/damage/attack';
import SharpShooterModule from '../../../../technologies/equipment/all/Offensive/SharpShooterModule.10007';
import SuperfluidDistillerEquipment from '../../../../technologies/equipment/all/Offensive/SuperfluidDistiller.30006';
import HiSecTrackingModuleEquipment from '../../../../technologies/equipment/all/Tactical/HiSecTrackingModule.10001';
import AutomatedTurretEquipment from '../../../../technologies/equipment/all/Warfare/AutomatedTurret.30004';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AACannon from '../../../../technologies/weapons/all/antiair/AACannon.70000';
import AMAACannon from '../../../../technologies/weapons/all/antiair/AMAACannon.70004';
import SilverCloudWeapon from '../../../../technologies/weapons/all/antiair/SilverCloud.70005';
import APGun from '../../../../technologies/weapons/all/machineguns/APGun.20001';
import AutoCannon from '../../../../technologies/weapons/all/machineguns/AutoCannon.20000';
import LaserAssistedGun from '../../../../technologies/weapons/all/machineguns/LaserAssistedGun.20012';
import PoisonGun from '../../../../technologies/weapons/all/machineguns/PoisonGun.20013';
import ProtonCannon from '../../../../technologies/weapons/all/machineguns/ProtonCannon.20003';
import RapidGun from '../../../../technologies/weapons/all/machineguns/RapidGun.20010';
import ShardCannon from '../../../../technologies/weapons/all/machineguns/ShardCannon.20004';
import SPARCGun from '../../../../technologies/weapons/all/machineguns/SPARC.20002';
import TripleBarrelMachineGun from '../../../../technologies/weapons/all/machineguns/TBMG.20005';
import TrackingGun from '../../../../technologies/weapons/all/machineguns/TrackingGun.20011';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_Autocannon: TPlayerResearchDefinition = {
	id: 300,
	caption: 'Weapon: Auto Cannon',
	description: AutoCannon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		weapons: {
			[AutoCannon.id]: 1,
		},
	},
};
export const PlayerResearch_APGun: TPlayerResearchDefinition = {
	id: 305,
	caption: 'Weapon: Big Bertha',
	description: APGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_Autocannon.id],
	technologyReward: {
		weapons: {
			[APGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_GunsPrecision1: TPlayerResearchDefinition = {
	id: 310,
	caption: 'MG Precision I',
	description: '+4% Hit Chance for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Autocannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseAccuracy: {
							bias: 0.04,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunsRange1: TPlayerResearchDefinition = {
	id: 311,
	caption: 'MG Range I',
	description: '+0.25 Machine Guns Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Autocannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunsDamage1: TPlayerResearchDefinition = {
	id: 312,
	caption: 'MG Damage I',
	description: '+7% Damage for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Autocannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						damageModifier: {
							factor: 1.07,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AACannon: TPlayerResearchDefinition = {
	id: 315,
	caption: 'Weapon: Anti-Aircraft Cannon',
	description: AACannon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_APGun.id, PlayerResearch_GunsDamage1.id],
	technologyReward: {
		weapons: {
			[AACannon.id]: 0.5,
		},
	},
};
export const PlayerResearch_LaserAssistedGun: TPlayerResearchDefinition = {
	id: 316,
	caption: 'Weapon: Laser Assisted Gun',
	description: LaserAssistedGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_APGun.id, PlayerResearch_GunsRange1.id],
	technologyReward: {
		weapons: {
			[LaserAssistedGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_TBMG: TPlayerResearchDefinition = {
	id: 317,
	caption: 'Weapon: TBMG',
	description: TripleBarrelMachineGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_APGun.id, PlayerResearch_GunsDamage1.id],
	technologyReward: {
		weapons: {
			[TripleBarrelMachineGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_GunsPrecision2: TPlayerResearchDefinition = {
	id: 320,
	caption: 'MG Precision II',
	description: '+3% Hit Chance for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_APGun.id, PlayerResearch_GunsPrecision1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseAccuracy: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunsRange2: TPlayerResearchDefinition = {
	id: 321,
	caption: 'MG Range II',
	description: '+0.2 Machine Guns Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_APGun.id, PlayerResearch_GunsRange1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseRange: {
							bias: 0.2,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunsDamage2: TPlayerResearchDefinition = {
	id: 322,
	caption: 'MG Damage II',
	description: '+5% Damage for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_APGun.id, PlayerResearch_GunsDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AMAACannon: TPlayerResearchDefinition = {
	id: 325,
	caption: 'Weapon: AMAA Cannon',
	description: AMAACannon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [
		PlayerResearch_AACannon.id,
		PlayerResearch_GunsRange1.id,
		PlayerResearch_GunsPrecision1.id,
	],
	technologyReward: {
		weapons: {
			[AMAACannon.id]: 0.5,
		},
	},
};
export const PlayerResearch_ShardCannon: TPlayerResearchDefinition = {
	id: 326,
	caption: 'Weapon: Shard Cannon',
	description: ShardCannon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [
		PlayerResearch_TBMG.id,
		PlayerResearch_LaserAssistedGun.id,
		PlayerResearch_GunsDamage1.id,
	],
	technologyReward: {
		weapons: {
			[ShardCannon.id]: 0.5,
		},
	},
};
export const PlayerResearch_SilverCloud: TPlayerResearchDefinition = {
	id: 330,
	caption: 'Weapon: Silver Cloud',
	description: SilverCloudWeapon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_AMAACannon.id,
		PlayerResearch_GunsDamage2.id,
		PlayerResearch_GunsRange2.id,
		PlayerResearch_GunsPrecision2.id,
	],
	technologyReward: {
		weapons: {
			[SilverCloudWeapon.id]: 0.5,
		},
	},
};
export const PlayerResearch_RapidGun: TPlayerResearchDefinition = {
	id: 331,
	caption: 'Weapon: Rapid Gun',
	description: RapidGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_ShardCannon.id,
		PlayerResearch_GunsDamage2.id,
		PlayerResearch_GunsRange2.id,
		PlayerResearch_GunsPrecision2.id,
	],
	technologyReward: {
		weapons: {
			[RapidGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_GunsPrecision3: TPlayerResearchDefinition = {
	id: 340,
	caption: 'MG Precision III',
	description: '+2% Hit Chance for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_ShardCannon.id, PlayerResearch_GunsPrecision2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseAccuracy: {
							bias: 0.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunsRange3: TPlayerResearchDefinition = {
	id: 341,
	caption: 'MG Range III',
	description: '+0.15 Machine Guns Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_ShardCannon.id, PlayerResearch_GunsRange2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseRange: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunsDamage3: TPlayerResearchDefinition = {
	id: 342,
	caption: 'MG Damage III',
	description: '+3% Damage for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_ShardCannon.id, PlayerResearch_GunsDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Chopper: TPlayerResearchDefinition = {
	id: 350,
	caption: 'Weapon: Chopper',
	description: TrackingGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RapidGun.id, PlayerResearch_GunsRange3.id],
	technologyReward: {
		weapons: {
			[TrackingGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_PoisonGun: TPlayerResearchDefinition = {
	id: 351,
	caption: 'Weapon: Poisonous Machine Gun',
	description: PoisonGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RapidGun.id, PlayerResearch_GunsDamage3.id],
	technologyReward: {
		weapons: {
			[PoisonGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_SPARC: TPlayerResearchDefinition = {
	id: 352,
	caption: 'Weapon: SPARC',
	description: SPARCGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RapidGun.id, PlayerResearch_GunsPrecision3.id],
	technologyReward: {
		weapons: {
			[SPARCGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_ProtonGun: TPlayerResearchDefinition = {
	id: 353,
	caption: 'Weapon: Proton Machine Gun',
	description: ProtonCannon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [
		PlayerResearch_GunsRange3.id,
		PlayerResearch_GunsDamage3.id,
		PlayerResearch_GunsPrecision3.id,
	],
	technologyReward: {
		weapons: {
			[ProtonCannon.id]: 0.5,
		},
	},
};
export const PlayerResearch_GunsPrecisionRepeatable: TPlayerResearchDefinition = {
	id: 360,
	repeatable: true,
	caption: `MG Precision ${INFINITY_SYMBOL}`,
	description: '+1% Hit Chance for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_ProtonGun.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseAccuracy: {
							bias: 0.01,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunsRangeRepeatable: TPlayerResearchDefinition = {
	repeatable: true,
	id: 361,
	caption: `MG Range ${INFINITY_SYMBOL}`,
	description: '+0.1 Range for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_ProtonGun.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseRange: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunsDamageRepeatable: TPlayerResearchDefinition = {
	repeatable: true,
	id: 362,
	caption: `MG Damage ${INFINITY_SYMBOL}`,
	description: '2% Damage for Machine Guns',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_ProtonGun.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_HiSecTracking: TPlayerResearchDefinition = {
	id: 370,
	caption: 'Hi-Sec Tracking',
	description: 'Infantry, Scout and Droid gain +5% Dodge. Equipment: Hi-Sec Tracking Module',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_GunsRangeRepeatable.id,
		PlayerResearch_GunsDamageRepeatable.id,
		PlayerResearch_GunsPrecisionRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[HiSecTrackingModuleEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_SentientTurrets: TPlayerResearchDefinition = {
	id: 371,
	caption: 'Sentient Turrets',
	description: 'Destroyer, Cruiser and Corvette gain +15% Dodge vs Missile Attacks. Equipment: Automated Turret',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_GunsRangeRepeatable.id,
		PlayerResearch_GunsDamageRepeatable.id,
		PlayerResearch_GunsPrecisionRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.15,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.15,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.cruiser]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.missile]: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[AutomatedTurretEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_NonNewtonianBallistics: TPlayerResearchDefinition = {
	id: 372,
	caption: 'Non-Newtonian Ballistics',
	description: 'Exosuit, Tiltjet and Corvette gain +0.4 Speed. Equipment: Sharpshooter Imprint',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_GunsRangeRepeatable.id,
		PlayerResearch_GunsDamageRepeatable.id,
		PlayerResearch_GunsPrecisionRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					speed: {
						bias: 0.4,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					speed: {
						bias: 0.4,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tiltjet]: {
			chassis: [
				{
					speed: {
						bias: 0.4,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[SharpShooterModule.id]: 0.5,
		},
	},
};
export const PlayerResearch_SubatomicLubricant: TPlayerResearchDefinition = {
	id: 373,
	caption: 'Subatomic Lubricant',
	description:
		'Guns gain +8% Attack Rate. Combat Mech and Quadrapod gain +10% Dodge. Equipment: Superfluid Distiller',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_GunsRangeRepeatable.id,
		PlayerResearch_GunsDamageRepeatable.id,
		PlayerResearch_GunsPrecisionRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.quadrapod]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.mecha]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.1,
						},
					},
				},
			],
		},
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseRate: {
							factor: 1.08,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[SuperfluidDistillerEquipment.id]: 0.5,
		},
	},
};

import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import AutomatedAssemblyFactory from '../../../../orbital/factories/all/AutomatedAssemblyYard.203';
import HeavyVehicleFactory from '../../../../orbital/factories/all/HeavyVehicleFactory.202';
import LightVehicleFactory from '../../../../orbital/factories/all/LightVehicleFactory.201';
import MachineryFactory from '../../../../orbital/factories/all/Machinery.204';
import MassiveIndustryComplex from '../../../../orbital/factories/all/MassiveIndustryComplex.600';
import MotorPlantFactory from '../../../../orbital/factories/all/Motorplant.200';
import WarfareMultiplex from '../../../../orbital/factories/all/WarfareMultiplex.205';
import ArtilleryChassis from '../../../../technologies/chassis/all/Artillery/ArtilleryPlatform.30000';
import AssaultBatteryChassis from '../../../../technologies/chassis/all/Artillery/AssaultBattery.30003';
import ColossusArtilleryChassis from '../../../../technologies/chassis/all/Artillery/Colossus.30002';
import LicornArtilleryChassis from '../../../../technologies/chassis/all/Artillery/Licorn.30001';
import GroundbreakerChassis from '../../../../technologies/chassis/all/HAV/Groundbreaker.33001';
import HAVChassis from '../../../../technologies/chassis/all/HAV/HAV.33000';
import RhinoChassis from '../../../../technologies/chassis/all/HAV/Rhino.33002';
import ConvictorChassis from '../../../../technologies/chassis/all/LAV/Convictor.37003';
import LAVChassis from '../../../../technologies/chassis/all/LAV/LAV.37000';
import LumberjackChassis from '../../../../technologies/chassis/all/LAV/Lumberjack.37001';
import WardenChassis from '../../../../technologies/chassis/all/LAV/Warden.37002';
import LeechChassis from '../../../../technologies/chassis/all/Scout/Leech.32003';
import LocustChassis from '../../../../technologies/chassis/all/Scout/Locust.32002';
import MaybugChassis from '../../../../technologies/chassis/all/Scout/Maybug.32004';
import ScoutChassis from '../../../../technologies/chassis/all/Scout/Scout.32000';
import VerminChassis from '../../../../technologies/chassis/all/Scout/Vermin.32001';
import ScannerChassis from '../../../../technologies/chassis/all/SupportVehicle/Scanner.31001';
import SupportVehicleChassis from '../../../../technologies/chassis/all/SupportVehicle/SupportVehicle.31000';
import ArmatureChassis from '../../../../technologies/chassis/all/Tank/Armature.36003';
import CrusaderChassis from '../../../../technologies/chassis/all/Tank/Crusader.36001';
import DragoonChassis from '../../../../technologies/chassis/all/Tank/Dragoon.36002';
import TankChassis from '../../../../technologies/chassis/all/Tank/Tank.36000';
import CloakDeviceEquipment from '../../../../technologies/equipment/all/Enhancement/CloakingDevice.20017';
import SabotageKitEquipment from '../../../../technologies/equipment/all/Tactical/SabotageKit.30010';
import UnitChassis from '../../../../technologies/types/chassis';
import PoliticalFactions from '../../../../world/factions';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_Scout: TPlayerResearchDefinition = {
	id: 1600,
	caption: 'Chassis: Scout',
	description: ScoutChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		chassis: {
			[ScoutChassis.id]: 1,
		},
	},
};
export const PlayerResearch_Motorplant: TPlayerResearchDefinition = {
	id: 1601,
	caption: 'Factory: Motorplant',
	description: MotorPlantFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		factories: {
			[MotorPlantFactory.id]: 1,
		},
	},
};
export const PlayerResearch_Vermin: TPlayerResearchDefinition = {
	id: 1605,
	caption: 'Chassis: Vermin',
	description: VerminChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Scout.id],
	technologyReward: {
		chassis: {
			[VerminChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Lav: TPlayerResearchDefinition = {
	id: 1606,
	caption: 'Chassis: LAV',
	description: LAVChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Scout.id, PlayerResearch_Motorplant.id],
	technologyReward: {
		chassis: {
			[LAVChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Wheelcraft1: TPlayerResearchDefinition = {
	id: 1610,
	caption: 'Wheelcraft I',
	description: '+3 Hit Points for Scout, Tank and LAV',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_Scout.id, PlayerResearch_Motorplant.id],
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_MassEducation1: TPlayerResearchDefinition = {
	id: 1611,
	caption: 'Mass Education I',
	description: '+2 Production Capacity; -1 Production Cost for Scout, Tank and LAV',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_Scout.id, PlayerResearch_Motorplant.id],
	baseModifier: {
		productionCapacity: {
			bias: 2,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Tank: TPlayerResearchDefinition = {
	id: 1615,
	caption: 'Chassis: Tank',
	description: TankChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Lav.id, PlayerResearch_Wheelcraft1.id],
	technologyReward: {
		chassis: {
			[TankChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_LightVehicleFactory: TPlayerResearchDefinition = {
	id: 1616,
	caption: 'Factory: Light Vehicle Factory',
	description: LightVehicleFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Lav.id, PlayerResearch_MassEducation1.id],
	technologyReward: {
		factories: {
			[LightVehicleFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Leech: TPlayerResearchDefinition = {
	id: 1617,
	caption: 'Chassis: Leech',
	description: LeechChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Vermin.id, PlayerResearch_MassEducation1.id],
	technologyReward: {
		chassis: {
			[LeechChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Locust: TPlayerResearchDefinition = {
	id: 1618,
	caption: 'Chassis: Locust',
	description: LocustChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Vermin.id, PlayerResearch_Wheelcraft1.id],
	technologyReward: {
		chassis: {
			[LocustChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Maybug: TPlayerResearchDefinition = {
	exclusiveFactions: [PoliticalFactions.TFactionID.venaticians],
	id: 1619,
	caption: 'Chassis: Maybug',
	description: MaybugChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Vermin.id],
	technologyReward: {
		chassis: {
			[MaybugChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Lumberjack: TPlayerResearchDefinition = {
	id: 1620,
	caption: 'Chassis: Lumberjack',
	description: LumberjackChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_Lav.id, PlayerResearch_Wheelcraft1.id, PlayerResearch_MassEducation1.id],
	technologyReward: {
		chassis: {
			[LumberjackChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Wheelcraft2: TPlayerResearchDefinition = {
	id: 1621,
	caption: 'Wheelcraft II',
	description: '+3 Hit Points for Scout, Tank and LAV',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_Wheelcraft1.id, PlayerResearch_LightVehicleFactory.id],
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_MassEducation2: TPlayerResearchDefinition = {
	id: 1622,
	caption: 'Mass Education II',
	description: '+2 Production Capacity; -1 Production Cost for Scout, Tank and LAV',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_LightVehicleFactory.id, PlayerResearch_MassEducation1.id],
	baseModifier: {
		productionCapacity: {
			bias: 2,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Armature: TPlayerResearchDefinition = {
	id: 1630,
	caption: 'Chassis: Armature',
	description: ArmatureChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Tank.id, PlayerResearch_MassEducation2.id],
	technologyReward: {
		chassis: {
			[ArmatureChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Hav: TPlayerResearchDefinition = {
	id: 1631,
	caption: 'Chassis: HAV',
	description: HAVChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Lumberjack.id, PlayerResearch_Wheelcraft2.id],
	technologyReward: {
		chassis: {
			[HAVChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Warden: TPlayerResearchDefinition = {
	id: 1632,
	caption: 'Chassis: Warden',
	description: WardenChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Lumberjack.id, PlayerResearch_MassEducation2.id],
	technologyReward: {
		chassis: {
			[WardenChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Artillery: TPlayerResearchDefinition = {
	id: 1633,
	caption: 'Chassis: SPG',
	description: ArtilleryChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Tank.id, PlayerResearch_Wheelcraft2.id, PlayerResearch_MassEducation2.id],
	technologyReward: {
		chassis: {
			[ArtilleryChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_SupportVehicle: TPlayerResearchDefinition = {
	id: 1634,
	caption: 'Chassis: Support Vehicle',
	description: SupportVehicleChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Tank.id, PlayerResearch_Wheelcraft2.id],
	technologyReward: {
		chassis: {
			[SupportVehicleChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_MultiplexedControl1: TPlayerResearchDefinition = {
	id: 1635,
	caption: 'Multiplexed Control 1',
	description: '+5% Dodge for Scout, HAV, Artillery',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Wheelcraft2.id, PlayerResearch_MassEducation2.id],
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.artillery]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},

		[UnitChassis.chassisClass.hav]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Convictor: TPlayerResearchDefinition = {
	id: 1640,
	caption: 'Chassis: Convictor',
	description: ConvictorChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Warden.id, PlayerResearch_MultiplexedControl1.id],
	technologyReward: {
		chassis: {
			[ConvictorChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Crusader: TPlayerResearchDefinition = {
	id: 1641,
	caption: 'Chassis: Crusader',
	description: CrusaderChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Armature.id, PlayerResearch_MultiplexedControl1.id],
	technologyReward: {
		chassis: {
			[CrusaderChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Groundbreaker: TPlayerResearchDefinition = {
	id: 1642,
	caption: 'Chassis: Groundbreaker',
	description: GroundbreakerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Hav.id, PlayerResearch_MultiplexedControl1.id],
	technologyReward: {
		chassis: {
			[GroundbreakerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Licorn: TPlayerResearchDefinition = {
	id: 1643,
	caption: 'Chassis: Licorn',
	description: LicornArtilleryChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Artillery.id, PlayerResearch_MultiplexedControl1.id],
	technologyReward: {
		chassis: {
			[LicornArtilleryChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Colossus: TPlayerResearchDefinition = {
	id: 1644,
	exclusiveFactions: [PoliticalFactions.TFactionID.reticulans],
	caption: 'Chassis: Colossus',
	description: ColossusArtilleryChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Artillery.id, PlayerResearch_MultiplexedControl1.id],
	technologyReward: {
		chassis: {
			[ColossusArtilleryChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_AutomatedAssemblyYard: TPlayerResearchDefinition = {
	id: 1645,
	caption: 'Factory: Automated Assembly Yard',
	description: AutomatedAssemblyFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_Warden.id, PlayerResearch_MultiplexedControl1.id],
	technologyReward: {
		factories: {
			[AutomatedAssemblyFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Machinery: TPlayerResearchDefinition = {
	id: 1646,
	caption: 'Factory: Machinery',
	description: MachineryFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_Armature.id, PlayerResearch_MultiplexedControl1.id],
	technologyReward: {
		factories: {
			[MachineryFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Wheelcraft3: TPlayerResearchDefinition = {
	id: 1650,
	caption: 'Wheelcraft III',
	description: '+3 Hit Points for Scout, Tank and LAV',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_Wheelcraft2.id],
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_MassEducation3: TPlayerResearchDefinition = {
	id: 1651,
	caption: 'Mass Education III',
	description: '+2 Production Capacity; -1 Production Cost for Scout, Tank and LAV',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_MassEducation2.id],
	baseModifier: {
		productionCapacity: {
			bias: 2,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_AssaultBattery: TPlayerResearchDefinition = {
	id: 1655,
	caption: 'Chassis: Assault Battery',
	description: AssaultBatteryChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Licorn.id, PlayerResearch_MassEducation3.id],
	technologyReward: {
		chassis: {
			[AssaultBatteryChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Rhino: TPlayerResearchDefinition = {
	id: 1656,
	caption: 'Chassis: Rhino',
	description: RhinoChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Licorn.id, PlayerResearch_Wheelcraft3.id],
	technologyReward: {
		chassis: {
			[RhinoChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Dragoon: TPlayerResearchDefinition = {
	id: 1657,
	caption: 'Chassis: Dragoon',
	description: DragoonChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Crusader.id, PlayerResearch_Wheelcraft3.id],
	technologyReward: {
		chassis: {
			[DragoonChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_MultiplexedControl2: TPlayerResearchDefinition = {
	id: 1660,
	caption: 'Multiplexed Control 2',
	description: '+5% Dodge for Scout, HAV, Artillery',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_Wheelcraft3.id, PlayerResearch_MassEducation3.id],
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.artillery]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},

		[UnitChassis.chassisClass.hav]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_HeavyVehicleFactory: TPlayerResearchDefinition = {
	id: 1661,
	caption: 'Factory: Heavy Vehicle Factory',
	description: HeavyVehicleFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_LightVehicleFactory.id,
		PlayerResearch_MassEducation3.id,
		PlayerResearch_Wheelcraft3.id,
	],
	technologyReward: {
		factories: {
			[HeavyVehicleFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Scanner: TPlayerResearchDefinition = {
	id: 1662,
	caption: 'Chassis: Scanner',
	description: ScannerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_SupportVehicle.id,
		PlayerResearch_MassEducation3.id,
		PlayerResearch_Wheelcraft3.id,
	],
	technologyReward: {
		chassis: {
			[ScannerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_WarfareMultiplex: TPlayerResearchDefinition = {
	id: 1665,
	caption: 'Factory: Warfare Multiplex',
	description: WarfareMultiplex.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_AutomatedAssemblyYard.id,
		PlayerResearch_Machinery.id,
		PlayerResearch_MultiplexedControl2.id,
	],
	technologyReward: {
		factories: {
			[WarfareMultiplex.id]: 0.5,
		},
	},
};
export const PlayerResearch_MechanicsRepeatable: TPlayerResearchDefinition = {
	id: 1666,
	caption: `Mechanics ${INFINITY_SYMBOL}`,
	repeatable: true,
	description: '+2% Damage for Scout, Artillery, Tank, LAV and HAV',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_Dragoon.id,
		PlayerResearch_Rhino.id,
		PlayerResearch_AssaultBattery.id,
		PlayerResearch_SupportVehicle.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.artillery]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hav]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_WheelDriveRepeatable: TPlayerResearchDefinition = {
	id: 1667,
	caption: `Wheel Drive ${INFINITY_SYMBOL}`,
	repeatable: true,
	description: '+2 Hit Points for Scout, Support Vehicle, Artillery, Tank, LAV and HAV',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_Dragoon.id,
		PlayerResearch_Rhino.id,
		PlayerResearch_AssaultBattery.id,
		PlayerResearch_SupportVehicle.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_vehicle]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.artillery]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.hav]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
	},
};
export const PlayerResearch_MassiveIndustrialComplex: TPlayerResearchDefinition = {
	id: 1670,
	caption: 'Factory: Massive Industrial Complex',
	description: MassiveIndustryComplex.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_WarfareMultiplex.id,
		PlayerResearch_MechanicsRepeatable.id,
		PlayerResearch_WheelDriveRepeatable.id,
	],
	technologyReward: {
		factories: {
			[MassiveIndustryComplex.id]: 0.5,
		},
	},
};
export const PlayerResearch_CovertOps: TPlayerResearchDefinition = {
	id: 1671,
	caption: 'Covert Operations',
	description: '+50 Base Energy Capacity; +2 Units Armor Protection; Equipment: Sabotage Kit',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_MechanicsRepeatable.id, PlayerResearch_WheelDriveRepeatable.id],
	baseModifier: {
		maxEnergyCapacity: {
			bias: 50,
		},
	},
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						bias: -2,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[SabotageKitEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_TransformableTemplates: TPlayerResearchDefinition = {
	id: 1672,
	caption: 'Transformable Templates',
	description: '+10 Base Production Capacity; +1 Units Effect Resistance; Equipment: Cloaking Device',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_MechanicsRepeatable.id, PlayerResearch_WheelDriveRepeatable.id],
	baseModifier: {
		productionCapacity: {
			bias: 10,
		},
	},
	unitModifier: {
		default: {
			unitModifier: {
				statusDurationModifier: [
					{
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				],
			},
		},
	},
	technologyReward: {
		equipment: {
			[CloakDeviceEquipment.id]: 0.5,
		},
	},
};

import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import CyberforgeFactory from '../../../../orbital/factories/all/Cyberforge.301';
import NexusFoundryFactory from '../../../../orbital/factories/all/NexusFoundry.302';
import RoboticsBayFactory from '../../../../orbital/factories/all/RoboticBay.300';
import SelfMaintainedFactory from '../../../../orbital/factories/all/SelfMaintainedFactory.303';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import ChaserChassis from '../../../../technologies/chassis/all/CombatDrone/Chaser.20001';
import CombatDroneChassis from '../../../../technologies/chassis/all/CombatDrone/CombatDrone.20000';
import FowlerChassis from '../../../../technologies/chassis/all/CombatDrone/Fowler.20002';
import SurveyorChassis from '../../../../technologies/chassis/all/CombatDrone/Surveyor.20003';
import DroidChassis from '../../../../technologies/chassis/all/Droid/Droid.25000';
import EnforcerChassis from '../../../../technologies/chassis/all/Droid/Enforcer.25002';
import HunterChassis from '../../../../technologies/chassis/all/Droid/Hunter.25003';
import M5Chassis from '../../../../technologies/chassis/all/Droid/M5.25004';
import T1024Chassis from '../../../../technologies/chassis/all/Droid/T-1024.25001';
import AATurretChassis from '../../../../technologies/chassis/all/PointDefense/AATurret.57003';
import GunTurretChassis from '../../../../technologies/chassis/all/PointDefense/GunTurret.57002';
import MissileTurretChassis from '../../../../technologies/chassis/all/PointDefense/MissileTurret.57001';
import PointDefenseChassis from '../../../../technologies/chassis/all/PointDefense/PointDefense.57000';
import SentinelTurrentChassis from '../../../../technologies/chassis/all/PointDefense/SentinelTurret.57004';
import QuadrapodChassis from '../../../../technologies/chassis/all/Quadrapod/Quadrapod.35000';
import RavagerChassis from '../../../../technologies/chassis/all/Quadrapod/Ravager.35002';
import TechnodromeChassis from '../../../../technologies/chassis/all/Quadrapod/Technodrome.35001';
import HawkChassis from '../../../../technologies/chassis/all/Rover/Hawk.50002';
import RoverChassis from '../../../../technologies/chassis/all/Rover/Rover.50000';
import VultureChassis from '../../../../technologies/chassis/all/Rover/Vulture.50001';
import ScienceVesselChassis from '../../../../technologies/chassis/all/ScienceVessel/ScienceVessel.99000';
import WandererChassis from '../../../../technologies/chassis/all/ScienceVessel/TheWanderer.99001';
import CryostasisGridEquipment from '../../../../technologies/equipment/all/Offensive/CryostasisGrid.20026';
import OrbitalSupportModuleEquipment from '../../../../technologies/equipment/all/Support/OrbitalSupportModule.10003';
import UnitChassis from '../../../../technologies/types/chassis';
import PoliticalFactions from '../../../../world/factions';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_Droid: TPlayerResearchDefinition = {
	id: 1700,
	caption: 'Chassis: Droid',
	description: DroidChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		chassis: {
			[DroidChassis.id]: 1,
		},
	},
};
export const PlayerResearch_RoboticsBay: TPlayerResearchDefinition = {
	id: 1701,
	caption: 'Factory: Robotics Bay',
	description: RoboticsBayFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		factories: {
			[RoboticsBayFactory.id]: 1,
		},
	},
};
export const PlayerResearch_Enforcer: TPlayerResearchDefinition = {
	id: 1705,
	caption: 'Chassis: Enforcer',
	description: EnforcerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Droid.id],
	technologyReward: {
		chassis: {
			[EnforcerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_CombatDrone: TPlayerResearchDefinition = {
	id: 1706,
	caption: 'Chassis: Combat Drone',
	description: CombatDroneChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Droid.id, PlayerResearch_RoboticsBay.id],
	technologyReward: {
		chassis: {
			[CombatDroneChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_T1024: TPlayerResearchDefinition = {
	id: 1707,
	exclusiveFactions: [PoliticalFactions.TFactionID.pirates],
	caption: 'Chassis: T-1024',
	description: T1024Chassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Droid.id],
	technologyReward: {
		chassis: {
			[T1024Chassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Robocraft1: TPlayerResearchDefinition = {
	id: 1710,
	caption: 'Robocraft I',
	description: '+3 Hit Points for Droid, PDD and Combat Drone',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_Droid.id, PlayerResearch_RoboticsBay.id],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_GlobalNetwork1: TPlayerResearchDefinition = {
	id: 1711,
	caption: 'Global Network I',
	description: '-2% Resource Consumption; -1 Production Cost for Droid, PDD and Combat Drone',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_Droid.id, PlayerResearch_RoboticsBay.id],
	baseModifier: {
		resourceConsumption: {
			default: { factor: 0.98 },
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_PDD: TPlayerResearchDefinition = {
	id: 1715,
	caption: 'Chassis: Point Defense',
	description: PointDefenseChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_CombatDrone.id, PlayerResearch_Robocraft1.id],
	technologyReward: {
		chassis: {
			[PointDefenseChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_CyberneticForge: TPlayerResearchDefinition = {
	id: 1716,
	caption: 'Factory: Cybernetic Forge',
	description: CyberforgeFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Enforcer.id, PlayerResearch_Robocraft1.id],
	technologyReward: {
		factories: {
			[CyberforgeFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Hunter: TPlayerResearchDefinition = {
	id: 1717,
	caption: 'Chassis: Hunter',
	description: HunterChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Enforcer.id, PlayerResearch_GlobalNetwork1.id],
	technologyReward: {
		chassis: {
			[HunterChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Chaser: TPlayerResearchDefinition = {
	id: 1718,
	caption: 'Chassis: Chaser',
	description: ChaserChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_CombatDrone.id, PlayerResearch_GlobalNetwork1.id],
	technologyReward: {
		chassis: {
			[ChaserChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_M5: TPlayerResearchDefinition = {
	id: 1720,
	caption: 'Chassis: M5',
	description: M5Chassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_Enforcer.id,
		PlayerResearch_Robocraft1.id,
		PlayerResearch_GlobalNetwork1.id,
	],
	technologyReward: {
		chassis: {
			[M5Chassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Robocraft2: TPlayerResearchDefinition = {
	id: 1721,
	caption: 'Robocraft II',
	description: '+3 Hit Points for Droid, PDD and Combat Drone',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_Robocraft1.id, PlayerResearch_CyberneticForge.id],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_GlobalNetwork2: TPlayerResearchDefinition = {
	id: 1722,
	caption: 'Global Network II',
	description: '-2% Resource Consumption; -1 Production Cost for Droid, PDD and Combat Drone',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CyberneticForge.id, PlayerResearch_GlobalNetwork1.id],
	baseModifier: {
		resourceConsumption: {
			default: { factor: 0.98 },
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_GunTurret: TPlayerResearchDefinition = {
	id: 1730,
	restrictedFactions: [PoliticalFactions.TFactionID.stellarians],
	caption: 'Chassis: Gun Turret',
	description: GunTurretChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_PDD.id, PlayerResearch_GlobalNetwork2.id],
	technologyReward: {
		chassis: {
			[GunTurretChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_MissileTurret: TPlayerResearchDefinition = {
	id: 1731,
	restrictedFactions: [PoliticalFactions.TFactionID.stellarians],
	caption: 'Chassis: Missile Turret',
	description: MissileTurretChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_PDD.id, PlayerResearch_Robocraft2.id],
	technologyReward: {
		chassis: {
			[MissileTurretChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Rover: TPlayerResearchDefinition = {
	id: 1732,
	caption: 'Chassis: Rover',
	description: RoverChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Hunter.id, PlayerResearch_Robocraft2.id, PlayerResearch_GlobalNetwork2.id],
	technologyReward: {
		chassis: {
			[RoverChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Fowler: TPlayerResearchDefinition = {
	id: 1733,
	caption: 'Chassis: Fowler',
	description: FowlerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Chaser.id, PlayerResearch_GlobalNetwork2.id],
	technologyReward: {
		chassis: {
			[FowlerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_SentinelTurret: TPlayerResearchDefinition = {
	id: 1734,
	exclusiveFactions: [PoliticalFactions.TFactionID.stellarians],
	caption: 'Chassis: Sentinel Turret',
	description: SentinelTurrentChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_PDD.id],
	technologyReward: {
		chassis: {
			[SentinelTurrentChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_ScienceVessel: TPlayerResearchDefinition = {
	id: 1735,
	caption: 'Chassis: Science Vessel',
	description: ScienceVesselChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_PDD.id, PlayerResearch_Robocraft2.id, PlayerResearch_GlobalNetwork2.id],
	technologyReward: {
		chassis: {
			[ScienceVesselChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_SapientNexus1: TPlayerResearchDefinition = {
	id: 1740,
	caption: 'Sapient Nexus I',
	description: '+5% Dodge for Droid, Science Vessel, Rover',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Robocraft2.id, PlayerResearch_GlobalNetwork2.id],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.science_vessel]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AATurret: TPlayerResearchDefinition = {
	id: 1741,
	restrictedFactions: [PoliticalFactions.TFactionID.stellarians],
	caption: 'Chassis: AA Turret',
	description: AATurretChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_PDD.id, PlayerResearch_Robocraft2.id, PlayerResearch_GlobalNetwork2.id],
	technologyReward: {
		chassis: {
			[AATurretChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Wanderer: TPlayerResearchDefinition = {
	id: 1742,
	exclusiveFactions: [PoliticalFactions.TFactionID.aquarians],
	caption: 'Chassis: The Wanderer',
	description: WandererChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_ScienceVessel.id],
	technologyReward: {
		chassis: {
			[WandererChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Robocraft3: TPlayerResearchDefinition = {
	id: 1745,
	caption: 'Robocraft III',
	description: '+3 Hit Points for Droid, PDD and Combat Drone',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Robocraft2.id],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_GlobalNetwork3: TPlayerResearchDefinition = {
	id: 1746,
	caption: 'Global Network III',
	description: '-2% Resource Consumption; -1 Production Cost for Droid, PDD and Combat Drone',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_GlobalNetwork2.id],
	baseModifier: {
		resourceConsumption: {
			default: { factor: 0.98 },
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Vulture: TPlayerResearchDefinition = {
	id: 1750,
	caption: 'Chassis: Vulture',
	description: VultureChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_Rover.id, PlayerResearch_SapientNexus1.id],
	technologyReward: {
		chassis: {
			[VultureChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Surveyor: TPlayerResearchDefinition = {
	id: 1751,
	caption: 'Chassis: Surveyor',
	description: SurveyorChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_Chaser.id, PlayerResearch_SapientNexus1.id],
	technologyReward: {
		chassis: {
			[SurveyorChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_NexusFoundry: TPlayerResearchDefinition = {
	id: 1752,
	caption: 'Factory: Nexus Foundry',
	description: NexusFoundryFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_CyberneticForge.id, PlayerResearch_SapientNexus1.id],
	technologyReward: {
		factories: {
			[NexusFoundryFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Quadrapod: TPlayerResearchDefinition = {
	id: 1760,
	caption: 'Chassis: Quadrapod',
	description: QuadrapodChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [
		PlayerResearch_NexusFoundry.id,
		PlayerResearch_Robocraft3.id,
		PlayerResearch_GlobalNetwork3.id,
	],
	technologyReward: {
		chassis: {
			[QuadrapodChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_SapientNexus2: TPlayerResearchDefinition = {
	id: 1761,
	caption: 'Sapient Nexus II',
	description: '+5% Dodge for Droid, Science Vessel, Rover',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_Robocraft2.id, PlayerResearch_GlobalNetwork2.id],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.science_vessel]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Hawk: TPlayerResearchDefinition = {
	id: 1762,
	caption: 'Chassis: Hawk',
	description: HawkChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [
		PlayerResearch_Vulture.id,
		PlayerResearch_Robocraft3.id,
		PlayerResearch_GlobalNetwork3.id,
	],
	technologyReward: {
		chassis: {
			[HawkChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_SelfMaintainedFactory: TPlayerResearchDefinition = {
	id: 1770,
	caption: 'Factory: Self Maintained Factory',
	description: SelfMaintainedFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_NexusFoundry.id, PlayerResearch_SapientNexus2.id],
	technologyReward: {
		factories: {
			[SelfMaintainedFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Ravager: TPlayerResearchDefinition = {
	id: 1771,
	caption: 'Chassis: Ravager',
	description: RavagerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Quadrapod.id, PlayerResearch_SapientNexus2.id],
	technologyReward: {
		chassis: {
			[RavagerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_CyberruleRepeatable: TPlayerResearchDefinition = {
	id: 1780,
	repeatable: true,
	caption: `Cyberrule ${INFINITY_SYMBOL}`,
	description: '+2% Dodge for Droid, Science Vessel, Rover, PDD, Quadrapod and Combat Dronw',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_Hawk.id,
		PlayerResearch_Ravager.id,
		PlayerResearch_Surveyor.id,
		PlayerResearch_M5.id,
		PlayerResearch_ScienceVessel.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.science_vessel]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.quadrapod]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_SiliconInfusionRepeatable: TPlayerResearchDefinition = {
	id: 1781,
	repeatable: true,
	caption: `Silicon Infusion ${INFINITY_SYMBOL}`,
	description: '+4% Hit Points for Droid, Science Vessel, Rover, PDD, Quadrapod and Combat Drone',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_Hawk.id,
		PlayerResearch_Ravager.id,
		PlayerResearch_Surveyor.id,
		PlayerResearch_M5.id,
		PlayerResearch_ScienceVessel.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.04,
					},
				},
			],
		},
		[UnitChassis.chassisClass.science_vessel]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.04,
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.04,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.04,
					},
				},
			],
		},
		[UnitChassis.chassisClass.quadrapod]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.04,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.04,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Technodrome: TPlayerResearchDefinition = {
	id: 1790,
	caption: 'Cybernetic Conversion',
	description: '+5% Damage for Droid, Rover and Combat Drone; Chassis: Technodrome',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_CyberruleRepeatable.id, PlayerResearch_SiliconInfusionRepeatable.id],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[TechnodromeChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_OrbitalUplink: TPlayerResearchDefinition = {
	id: 1791,
	caption: 'Orbital Uplink',
	description: '+1 Scan Range for PDD, Science Vessel and Quadrapod; Equipment: Orbital Uplink',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_CyberruleRepeatable.id, PlayerResearch_SiliconInfusionRepeatable.id],
	unitModifier: {
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					scanRange: {
						bias: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.science_vessel]: {
			chassis: [
				{
					scanRange: {
						bias: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.quadrapod]: {
			chassis: [
				{
					scanRange: {
						bias: 1,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[OrbitalSupportModuleEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_CryostasisGrid: TPlayerResearchDefinition = {
	id: 1792,
	caption: 'Cryostasis Grid',
	description: '+8% Shield Protection against HEA and TRM Damage; +5% Dodge vs Beam; Equipment: Cryostasis Grid',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_CyberruleRepeatable.id, PlayerResearch_SiliconInfusionRepeatable.id],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.beam]: {
							bias: 0.05,
						},
					},
				},
			],
			shield: [
				{
					[Damage.damageType.heat]: {
						factor: 0.92,
					},
					[Damage.damageType.thermodynamic]: {
						factor: 0.92,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[CryostasisGridEquipment.id]: 0.5,
		},
	},
};

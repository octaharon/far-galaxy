import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import BarracksFactory from '../../../../orbital/factories/all/Barracks.103';
import BioforgeFactory from '../../../../orbital/factories/all/Bioforge.104';
import BionicInstitute from '../../../../orbital/factories/all/BionicInstitute.101';
import CloningVatsFactory from '../../../../orbital/factories/all/CloningVats.105';
import MilitaryUniversityFactory from '../../../../orbital/factories/all/MilitaryUniversity.102';
import SpecialForcesAcademyFactory from '../../../../orbital/factories/all/SpecialForcesAcademy.106';
import TrainingHallFactory from '../../../../orbital/factories/all/TrainingHall.100';
import Damage from '../../../../tactical/damage/damage';
import BiotechChassis from '../../../../technologies/chassis/all/BioTech/Biotech.40000';
import ChimeraChassis from '../../../../technologies/chassis/all/BioTech/Chimera.40002';
import GolliwogChassis from '../../../../technologies/chassis/all/BioTech/Gollywog.40003';
import NeosapientChassis from '../../../../technologies/chassis/all/BioTech/Neosapient.40001';
import BarkerChassis from '../../../../technologies/chassis/all/Exoskeleton/Barker.15003';
import ExoskeletonChassis from '../../../../technologies/chassis/all/Exoskeleton/Exoskeleton.15000';
import StalkerChassis from '../../../../technologies/chassis/all/Exoskeleton/Stalker.15002';
import TracerChassis from '../../../../technologies/chassis/all/Exoskeleton/Tracer.15001';
import CharybdisChassis from '../../../../technologies/chassis/all/Hydra/Charybdis.43002';
import HydraChassis from '../../../../technologies/chassis/all/Hydra/Hydra.43000';
import ScyllaChassis from '../../../../technologies/chassis/all/Hydra/Scylla.43001';
import BruiserChassis from '../../../../technologies/chassis/all/Infantry/Bruiser.10004';
import BruteChassis from '../../../../technologies/chassis/all/Infantry/Brute.10005';
import ChaplainChassis from '../../../../technologies/chassis/all/Infantry/Chaplain.10006';
import XDriverChassis from '../../../../technologies/chassis/all/Infantry/XDriver.10002';
import EliteGuardianChassis from '../../../../technologies/chassis/all/Infantry/EliteGuardian.10001';
import InfantryChassis from '../../../../technologies/chassis/all/Infantry/Infantry.10000';
import QuantumOpsChassis from '../../../../technologies/chassis/all/Infantry/QuantumOps.10003';
import SwarmChassis from '../../../../technologies/chassis/all/Swarm/Swarm.45000';
import XenoswarmChassis from '../../../../technologies/chassis/all/Swarm/Xenoswarm.45001';
import BioticAmpEquipment from '../../../../technologies/equipment/all/Defensive/BioticAmp.20013';
import PsionicAmplifierEquipment from '../../../../technologies/equipment/all/Warfare/PsionicAmplifier.30007';
import UnitChassis from '../../../../technologies/types/chassis';
import PoliticalFactions from '../../../../world/factions';
import Resources from '../../../../world/resources/types';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_Infantry: TPlayerResearchDefinition = {
	id: 1800,
	caption: 'Chassis: Infantry',
	description: InfantryChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		chassis: {
			[InfantryChassis.id]: 1,
		},
	},
};
export const PlayerResearch_TrainingFacility: TPlayerResearchDefinition = {
	id: 1801,
	caption: 'Factory: Training Facility',
	description: TrainingHallFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		factories: {
			[TrainingHallFactory.id]: 1,
		},
	},
};
export const PlayerResearch_HeavyInfantry: TPlayerResearchDefinition = {
	id: 1805,
	caption: 'Chassis: Elite Guardian',
	description: EliteGuardianChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Infantry.id],
	technologyReward: {
		chassis: {
			[EliteGuardianChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Exosuit: TPlayerResearchDefinition = {
	id: 1806,
	caption: 'Chassis: Exosuit',
	description: ExoskeletonChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Infantry.id, PlayerResearch_TrainingFacility.id],
	technologyReward: {
		chassis: {
			[ExoskeletonChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Chaplain: TPlayerResearchDefinition = {
	id: 1807,
	exclusiveFactions: [PoliticalFactions.TFactionID.anders],
	caption: 'Chassis: Chaplain',
	description: ChaplainChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_Infantry.id],
	technologyReward: {
		chassis: {
			[ChaplainChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Footcraft1: TPlayerResearchDefinition = {
	id: 1810,
	caption: 'Footcraft I',
	description: '+3 Hit Points For Infantry, Exosuit and Biotech',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_Infantry.id, PlayerResearch_TrainingFacility.id],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_CrossbreedPsionics1: TPlayerResearchDefinition = {
	id: 1811,
	caption: 'Crossbreed Psionics I',
	description: '+3% Base Engineering Output; -1 Production Cost For Infantry, Exosuit and Biotech',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_Infantry.id, PlayerResearch_TrainingFacility.id],
	baseModifier: {
		engineeringOutput: {
			factor: 1.03,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Bruiser: TPlayerResearchDefinition = {
	id: 1815,
	caption: 'Chassis: Bruiser',
	description: BruiserChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Exosuit.id, PlayerResearch_CrossbreedPsionics1.id],
	technologyReward: {
		chassis: {
			[BruiserChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Barracks: TPlayerResearchDefinition = {
	id: 1816,
	caption: 'Factory: Baracks',
	description: BarracksFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_HeavyInfantry.id, PlayerResearch_Footcraft1.id],
	technologyReward: {
		factories: {
			[BarracksFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_BionicInstitute: TPlayerResearchDefinition = {
	id: 1817,
	caption: 'Factory: Bionic Institute',
	description: BionicInstitute.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_HeavyInfantry.id, PlayerResearch_CrossbreedPsionics1.id],
	technologyReward: {
		factories: {
			[BionicInstitute.id]: 0.5,
		},
	},
};
export const PlayerResearch_Stalker: TPlayerResearchDefinition = {
	id: 1818,
	caption: 'Chassis: Stalker',
	description: StalkerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Exosuit.id, PlayerResearch_Footcraft1.id],
	technologyReward: {
		chassis: {
			[StalkerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Footcraft2: TPlayerResearchDefinition = {
	id: 1820,
	caption: 'Footcraft II',
	description: '+3 Hit Points For Infantry, Exosuit and Biotech',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_Footcraft1.id, PlayerResearch_Bruiser.id],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_CrossbreedPsionics2: TPlayerResearchDefinition = {
	id: 1821,
	caption: 'Crossbreed Psionics II',
	description: '+3% Base Engineering Output; -1 Production Cost For Infantry, Exosuit and Biotech',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_Bruiser.id, PlayerResearch_CrossbreedPsionics1.id],
	baseModifier: {
		engineeringOutput: {
			factor: 1.03,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_XDriver: TPlayerResearchDefinition = {
	id: 1830,
	restrictedFactions: [PoliticalFactions.TFactionID.draconians],
	caption: 'Chassis: X Driver',
	description: XDriverChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Footcraft2.id, PlayerResearch_CrossbreedPsionics2.id],
	technologyReward: {
		chassis: {
			[XDriverChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_SpecialForcesAcademy: TPlayerResearchDefinition = {
	id: 1831,
	caption: 'Factory: Special Forces Academy',
	description: SpecialForcesAcademyFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_Barracks.id,
		PlayerResearch_Footcraft2.id,
		PlayerResearch_CrossbreedPsionics2.id,
	],
	technologyReward: {
		factories: {
			[SpecialForcesAcademyFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Biotech: TPlayerResearchDefinition = {
	id: 1832,
	caption: 'Chassis: Biotech',
	description: BiotechChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_BionicInstitute.id,
		PlayerResearch_Footcraft2.id,
		PlayerResearch_CrossbreedPsionics2.id,
	],
	technologyReward: {
		chassis: {
			[BiotechChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Barker: TPlayerResearchDefinition = {
	id: 1833,
	caption: 'Chassis: Barker',
	description: BarkerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_Stalker.id,
		PlayerResearch_CrossbreedPsionics2.id,
		PlayerResearch_Footcraft2.id,
	],
	technologyReward: {
		chassis: {
			[BarkerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Brute: TPlayerResearchDefinition = {
	id: 1834,
	exclusiveFactions: [PoliticalFactions.TFactionID.draconians],
	caption: 'Chassis: Brute',
	description: BruteChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Bruiser.id],
	technologyReward: {
		chassis: {
			[BruteChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_GeneSplicing1: TPlayerResearchDefinition = {
	id: 1840,
	caption: 'Gene Splicing I',
	description: '+5% Damage for Biotech, Swarm and Hydra',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Footcraft2.id, PlayerResearch_CrossbreedPsionics2.id],
	unitModifier: {
		[UnitChassis.chassisClass.biotech]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.swarm]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hydra]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MilitaryUniversity: TPlayerResearchDefinition = {
	id: 1841,
	caption: 'Factory: Military University',
	description: MilitaryUniversityFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Barracks.id, PlayerResearch_Stalker.id, PlayerResearch_Bruiser.id],
	technologyReward: {
		factories: {
			[MilitaryUniversityFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Bioforge: TPlayerResearchDefinition = {
	id: 1842,
	caption: 'Factory: Bioforge',
	description: BioforgeFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Biotech.id],
	technologyReward: {
		factories: {
			[BioforgeFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_QuantumOps: TPlayerResearchDefinition = {
	id: 1843,
	caption: 'Chassis: Quantum Ops',
	description: QuantumOpsChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_SpecialForcesAcademy.id, PlayerResearch_XDriver.id],
	technologyReward: {
		chassis: {
			[QuantumOpsChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Neosapient: TPlayerResearchDefinition = {
	id: 1850,
	caption: 'Chassis: Neosapient',
	description: NeosapientChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_GeneSplicing1.id, PlayerResearch_Biotech.id],
	technologyReward: {
		chassis: {
			[NeosapientChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Swarm: TPlayerResearchDefinition = {
	id: 1851,
	caption: 'Chassis: Swarm',
	description: SwarmChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_GeneSplicing1.id, PlayerResearch_Bioforge.id],
	technologyReward: {
		chassis: {
			[SwarmChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Hydra: TPlayerResearchDefinition = {
	id: 1852,
	caption: 'Chassis: Hydra',
	description: HydraChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_GeneSplicing1.id, PlayerResearch_Bioforge.id],
	technologyReward: {
		chassis: {
			[HydraChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Gollywog: TPlayerResearchDefinition = {
	id: 1853,
	exclusiveFactions: [PoliticalFactions.TFactionID.draconians],
	caption: 'Chassis: Gollywog',
	description: GolliwogChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_GeneSplicing1.id, PlayerResearch_Biotech.id],
	technologyReward: {
		chassis: {
			[GolliwogChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Footcraft3: TPlayerResearchDefinition = {
	id: 1854,
	caption: 'Footcraft III',
	description: '+3 Hit Points For Infantry, Exosuit and Biotech',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Footcraft2.id],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_CrossbreedPsionics3: TPlayerResearchDefinition = {
	id: 1855,
	caption: 'Crossbreed Psionics III',
	description: '+3% Base Engineering Output; -1 Production Cost For Infantry, Exosuit and Biotech',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_CrossbreedPsionics2.id],
	baseModifier: {
		engineeringOutput: {
			factor: 1.03,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Chimaera: TPlayerResearchDefinition = {
	id: 1860,
	caption: 'Chassis: Chimaera',
	description: ChimeraChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_Neosapient.id,
		PlayerResearch_CrossbreedPsionics3.id,
		PlayerResearch_Footcraft3.id,
	],
	technologyReward: {
		chassis: {
			[ChimeraChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Tracer: TPlayerResearchDefinition = {
	id: 1861,
	caption: 'Chassis: Tracer',
	description: TracerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_Barker.id,
		PlayerResearch_CrossbreedPsionics3.id,
		PlayerResearch_Footcraft3.id,
	],
	technologyReward: {
		chassis: {
			[TracerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_GeneSplicing2: TPlayerResearchDefinition = {
	id: 1865,
	caption: 'Gene Splicing II',
	description: '+5% Damage for Biotech, Swarm and Hydra',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_GeneSplicing1.id,
		PlayerResearch_Footcraft3.id,
		PlayerResearch_CrossbreedPsionics3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.biotech]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.swarm]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hydra]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_XenoSwarm: TPlayerResearchDefinition = {
	id: 1870,
	caption: 'Chassis: Xenoswarm',
	description: XenoswarmChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_GeneSplicing2.id, PlayerResearch_Swarm.id],
	technologyReward: {
		chassis: {
			[XenoswarmChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Scylla: TPlayerResearchDefinition = {
	id: 1871,
	caption: 'Chassis: Scylla',
	description: ScyllaChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_GeneSplicing2.id, PlayerResearch_Hydra.id],
	technologyReward: {
		chassis: {
			[ScyllaChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_CloningVats: TPlayerResearchDefinition = {
	id: 1872,
	caption: 'Factory: Cloning Vats',
	description: CloningVatsFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_Bioforge.id, PlayerResearch_GeneSplicing2.id],
	technologyReward: {
		factories: {
			[CloningVatsFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_BiomasteryRepeatable: TPlayerResearchDefinition = {
	id: 1880,
	repeatable: true,
	caption: `Biomastery ${INFINITY_SYMBOL}`,
	description: '+2 Hit Points For Infantry, Exosuit, Biotech, Swarm and Hydra',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_XenoSwarm.id, PlayerResearch_Scylla.id],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.swarm]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.hydra]: {
			chassis: [
				{
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
	},
};
export const PlayerResearch_CellularAugmentationRepeatable: TPlayerResearchDefinition = {
	id: 1881,
	repeatable: true,
	caption: `Cellular Augmentation ${INFINITY_SYMBOL}`,
	description: '+2% Damage For Infantry, Exosuit, Biotech, Swarm and Hydra',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_XenoSwarm.id, PlayerResearch_Scylla.id],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.swarm]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hydra]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Charibdis: TPlayerResearchDefinition = {
	id: 1885,
	caption: 'Chassis: Charybdis',
	description: CharybdisChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_BiomasteryRepeatable.id, PlayerResearch_CellularAugmentationRepeatable.id],
	technologyReward: {
		chassis: {
			[CharybdisChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_AmplifiedHydrolysis: TPlayerResearchDefinition = {
	id: 1890,
	caption: `Amplified Hydrolysis`,
	description: '+3% Hydrogen Output; +5% Armor Protection vs RAD; Equipment: Biotic Amp',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_BiomasteryRepeatable.id, PlayerResearch_BiomasteryRepeatable.id],
	technologyReward: {
		equipment: {
			[BioticAmpEquipment.id]: 0.5,
		},
	},
	baseModifier: {
		resourceOutput: {
			[Resources.resourceId.hydrogen]: {
				factor: 1.03,
			},
		},
	},
	unitModifier: {
		default: {
			armor: [
				{
					[Damage.damageType.radiation]: {
						factor: 0.95,
					},
				},
			],
		},
	},
};
export const PlayerResearch_PsionicConvergence: TPlayerResearchDefinition = {
	id: 1891,
	caption: `Psionic Convergence`,
	description: '+5% Hit Chance for Infantry, Exosuit and Combat Mech; Equipment: Psionic Amplifier',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_BiomasteryRepeatable.id, PlayerResearch_BiomasteryRepeatable.id],
	technologyReward: {
		equipment: {
			[PsionicAmplifierEquipment.id]: 0.5,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.mecha]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};

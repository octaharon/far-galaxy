import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import ECCMModuleEquipment from '../../../../technologies/equipment/all/Defensive/ECCMModule.10002';
import ProtonGeneratorEquipment from '../../../../technologies/equipment/all/Defensive/ProtonGenerator.20002';
import EleriumBatteryEquipment from '../../../../technologies/equipment/all/Offensive/EleriumBattery.20008';
import HiggsPumpEquipment from '../../../../technologies/equipment/all/Support/HiggsPump.30011';
import NanofoilCoatingEquipment from '../../../../technologies/equipment/all/Warfare/NanofoilCoating.30009';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AALaser from '../../../../technologies/weapons/all/antiair/AALaser.70003';
import DeathRay from '../../../../technologies/weapons/all/beam/DeathRay.80003';
import LightningGun from '../../../../technologies/weapons/all/beam/LightningGun.80001';
import MicrowavePhaseArrayWeapon from '../../../../technologies/weapons/all/beam/MWPA.80000';
import QuantumExciter from '../../../../technologies/weapons/all/beam/QuantumExciter.80004';
import SolarBeam from '../../../../technologies/weapons/all/beam/SolarBeam.80005';
import StarBeam from '../../../../technologies/weapons/all/beam/StarBeam.80006';
import ThetisRay from '../../../../technologies/weapons/all/beam/ThetisRay.80007';
import THORGun from '../../../../technologies/weapons/all/beam/THORGun.80002';
import SeekerMissile from '../../../../technologies/weapons/all/guided/SeekerMissile.35001';
import GammaLaser from '../../../../technologies/weapons/all/lasers/GammaLaser.90005';
import GovernorGun from '../../../../technologies/weapons/all/lasers/Governor.90003';
import HELLGun from '../../../../technologies/weapons/all/lasers/HELL.90000';
import PALMGun from '../../../../technologies/weapons/all/lasers/PALM.90001';
import PrismGun from '../../../../technologies/weapons/all/lasers/PrismGun.90004';
import XRayLaser from '../../../../technologies/weapons/all/lasers/XRayLaser.90002';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_HellGun: TPlayerResearchDefinition = {
	id: 200,
	caption: 'Weapon: HE-LL',
	description: HELLGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		weapons: {
			[HELLGun.id]: 1,
		},
	},
};
export const PlayerResearch_PALM: TPlayerResearchDefinition = {
	id: 205,
	caption: 'Weapon: PALM',
	description: SeekerMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_HellGun.id],
	technologyReward: {
		weapons: {
			[PALMGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_LaserPrecision1: TPlayerResearchDefinition = {
	id: 210,
	caption: 'Lasers Precision I',
	description: '+3% Hit Chance for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_HellGun.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseAccuracy: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaserRate1: TPlayerResearchDefinition = {
	id: 211,
	caption: 'Lasers Attack Rate I',
	description: '+0.05 Attack Rate for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_HellGun.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaserDamage1: TPlayerResearchDefinition = {
	id: 212,
	caption: 'Lasers Damage I',
	description: '+5% Damage for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_HellGun.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MWPA: TPlayerResearchDefinition = {
	id: 215,
	caption: 'Weapon: Superheater',
	description: MicrowavePhaseArrayWeapon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_PALM.id, PlayerResearch_LaserDamage1.id],
	technologyReward: {
		weapons: {
			[MicrowavePhaseArrayWeapon.id]: 0.5,
		},
	},
};
export const PlayerResearch_Governor: TPlayerResearchDefinition = {
	id: 216,
	caption: 'Weapon: Governor',
	description: GovernorGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_PALM.id, PlayerResearch_LaserRate1.id],
	technologyReward: {
		weapons: {
			[GovernorGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_XRayLaser: TPlayerResearchDefinition = {
	id: 217,
	caption: 'Weapon: X-Ray Laser',
	description: XRayLaser.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_PALM.id, PlayerResearch_LaserPrecision1.id],
	technologyReward: {
		weapons: {
			[XRayLaser.id]: 0.5,
		},
	},
};
export const PlayerResearch_LaserPrecision2: TPlayerResearchDefinition = {
	id: 220,
	caption: 'Lasers Precision II',
	description: '+3% Hit Chance for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_LaserPrecision1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseAccuracy: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaserRate2: TPlayerResearchDefinition = {
	id: 221,
	caption: 'Lasers Attack Rate II',
	description: '+0.05 Attack Rate for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_LaserRate1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaserDamage2: TPlayerResearchDefinition = {
	id: 222,
	caption: 'Lasers Damage II',
	description: '+5% Damage for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_LaserDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamsPrecision1: TPlayerResearchDefinition = {
	id: 223,
	caption: 'Energy Weapons Precision I',
	description: '+5% Hit Chance for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_MWPA.id, PlayerResearch_LaserPrecision1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamsRange1: TPlayerResearchDefinition = {
	id: 224,
	caption: 'Energy Weapons Range I',
	description: '+5% Range for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_MWPA.id, PlayerResearch_LaserRate1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						baseRange: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamsDamage1: TPlayerResearchDefinition = {
	id: 225,
	caption: 'Energy Weapons Damage I',
	description: '+2% Damage for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_MWPA.id, PlayerResearch_LaserDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GammaLaser: TPlayerResearchDefinition = {
	id: 230,
	caption: 'Weapon: Gamma Laser',
	description: GammaLaser.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_LaserPrecision1.id,
		PlayerResearch_LaserDamage1.id,
		PlayerResearch_LaserRate1.id,
		PlayerResearch_XRayLaser.id,
	],
	technologyReward: {
		weapons: {
			[GammaLaser.id]: 0.5,
		},
	},
};
export const PlayerResearch_LightningGun: TPlayerResearchDefinition = {
	id: 231,
	caption: 'Weapon: Lighting Gun',
	description: LightningGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_LaserPrecision1.id,
		PlayerResearch_LaserDamage1.id,
		PlayerResearch_LaserRate1.id,
		PlayerResearch_MWPA.id,
	],
	technologyReward: {
		weapons: {
			[LightningGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_SolarBeam: TPlayerResearchDefinition = {
	id: 232,
	caption: 'Weapon: Solar Beam',
	description: SolarBeam.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_BeamsPrecision1.id,
		PlayerResearch_BeamsDamage1.id,
		PlayerResearch_BeamsRange1.id,
		PlayerResearch_MWPA.id,
	],
	technologyReward: {
		weapons: {
			[SolarBeam.id]: 0.5,
		},
	},
};
export const PlayerResearch_Prism: TPlayerResearchDefinition = {
	id: 240,
	caption: 'Weapon: Prism',
	description: PrismGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [
		PlayerResearch_LaserPrecision2.id,
		PlayerResearch_LaserDamage2.id,
		PlayerResearch_LaserRate2.id,
		PlayerResearch_GammaLaser.id,
	],
	technologyReward: {
		weapons: {
			[PrismGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_LaserPrecision3: TPlayerResearchDefinition = {
	id: 241,
	caption: 'Lasers Precision III',
	description: '+3% Hit Chance for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_LaserPrecision2.id, PlayerResearch_XRayLaser.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseAccuracy: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaserRate3: TPlayerResearchDefinition = {
	id: 242,
	caption: 'Lasers Attack Rate III',
	description: '+0.05 Attack Rate for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_LaserRate2.id, PlayerResearch_Governor.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaserDamage3: TPlayerResearchDefinition = {
	id: 243,
	caption: 'Lasers Damage III',
	description: '+5% Damage for Lasers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_LaserDamage2.id, PlayerResearch_GammaLaser.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamsPrecision2: TPlayerResearchDefinition = {
	id: 250,
	caption: 'Energy Weapons Precision II',
	description: '+5% Hit Chance for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_SolarBeam.id, PlayerResearch_BeamsPrecision1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamsRange2: TPlayerResearchDefinition = {
	id: 251,
	caption: 'Energy Weapons Range II',
	description: '+5% Range for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_SolarBeam.id, PlayerResearch_BeamsRange1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						baseRange: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamsDamage2: TPlayerResearchDefinition = {
	id: 252,
	caption: 'Energy Weapons Damage II',
	description: '+2% Damage for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_SolarBeam.id, PlayerResearch_BeamsDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_THOR: TPlayerResearchDefinition = {
	id: 253,
	caption: 'Weapon: THOR',
	description: THORGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_LaserPrecision2.id,
		PlayerResearch_LaserDamage2.id,
		PlayerResearch_LaserRate2.id,
		PlayerResearch_LightningGun.id,
	],
	technologyReward: {
		weapons: {
			[THORGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_StarBeam: TPlayerResearchDefinition = {
	id: 260,
	caption: 'Weapon: Star Beam',
	description: StarBeam.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_BeamsPrecision2.id,
		PlayerResearch_BeamsDamage2.id,
		PlayerResearch_BeamsRange2.id,
		PlayerResearch_SolarBeam.id,
	],
	technologyReward: {
		weapons: {
			[StarBeam.id]: 0.5,
		},
	},
};
export const PlayerResearch_AALaser: TPlayerResearchDefinition = {
	id: 261,
	caption: 'Weapon: AALPD',
	description: AALaser.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_BeamsPrecision2.id,
		PlayerResearch_LaserDamage2.id,
		PlayerResearch_LaserRate2.id,
		PlayerResearch_BeamsRange2.id,
	],
	technologyReward: {
		weapons: {
			[AALaser.id]: 0.5,
		},
	},
};
export const PlayerResearch_BeamsPrecision3: TPlayerResearchDefinition = {
	id: 265,
	caption: 'Energy Weapons Precision III',
	description: '+5% Hit Chance for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_THOR.id, PlayerResearch_BeamsPrecision2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamsRange3: TPlayerResearchDefinition = {
	id: 266,
	caption: 'Energy Weapons Range III',
	description: '+5% Range for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_THOR.id, PlayerResearch_BeamsRange2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						baseRange: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamsDamage3: TPlayerResearchDefinition = {
	id: 267,
	caption: 'Energy Weapons Damage III',
	description: '+2% Damage for Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_THOR.id, PlayerResearch_BeamsDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_DeathRay: TPlayerResearchDefinition = {
	id: 270,
	caption: 'Weapon: Death Ray',
	description: DeathRay.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 47,
	prerequisiteResearchIds: [
		PlayerResearch_BeamsPrecision3.id,
		PlayerResearch_BeamsDamage3.id,
		PlayerResearch_BeamsRange3.id,
		PlayerResearch_StarBeam.id,
	],
	technologyReward: {
		weapons: {
			[DeathRay.id]: 0.5,
		},
	},
};
export const PlayerResearch_QuantumExciter: TPlayerResearchDefinition = {
	id: 271,
	caption: 'Weapon: Quantum Exciter',
	description: QuantumExciter.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 47,
	prerequisiteResearchIds: [
		PlayerResearch_BeamsPrecision3.id,
		PlayerResearch_BeamsDamage3.id,
		PlayerResearch_BeamsRange3.id,
		PlayerResearch_Prism.id,
	],
	technologyReward: {
		weapons: {
			[QuantumExciter.id]: 0.5,
		},
	},
};
export const PlayerResearch_BeamPrecisionRepeatable: TPlayerResearchDefinition = {
	id: 280,
	repeatable: true,
	caption: `Beams Precision ${INFINITY_SYMBOL}`,
	description: '+1% Hit Chance for Lasers and Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_LaserPrecision3.id, PlayerResearch_BeamsPrecision3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseAccuracy: {
							bias: 0.01,
						},
					},
					[Weapons.TWeaponGroupType.Beam]: {
						baseAccuracy: {
							bias: 0.01,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamRateRepeatable: TPlayerResearchDefinition = {
	id: 281,
	repeatable: true,
	caption: `Beams Rate ${INFINITY_SYMBOL}`,
	description: '+0.01 Attack Rate for Lasers and Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_LaserRate3.id, PlayerResearch_BeamsRange3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseRate: {
							bias: 0.01,
						},
					},
					[Weapons.TWeaponGroupType.Beam]: {
						baseRate: {
							bias: 0.01,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BeamDamageRepeatable: TPlayerResearchDefinition = {
	id: 282,
	repeatable: true,
	caption: `Beams Damage ${INFINITY_SYMBOL}`,
	description: '+2% Damage for Lasers and Energy Weapons',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_LaserDamage3.id, PlayerResearch_BeamsDamage3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						damageModifier: {
							factor: 1.02,
						},
					},
					[Weapons.TWeaponGroupType.Beam]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_ThetisRay: TPlayerResearchDefinition = {
	id: 285,
	caption: 'Weapon: Thetis Ray',
	description: ThetisRay.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BeamPrecisionRepeatable.id,
		PlayerResearch_BeamDamageRepeatable.id,
		PlayerResearch_BeamRateRepeatable.id,
		PlayerResearch_DeathRay.id,
	],
	technologyReward: {
		weapons: {
			[ThetisRay.id]: 0.5,
		},
	},
};
export const PlayerResearch_BeamsBoost: TPlayerResearchDefinition = {
	id: 290,
	caption: 'Nanofoil Mirrors',
	description: '+10% Attack Rate for Energy Weapons; Equipment: Nanofoil Coating',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BeamPrecisionRepeatable.id,
		PlayerResearch_BeamDamageRepeatable.id,
		PlayerResearch_BeamRateRepeatable.id,
	],
	technologyReward: {
		equipment: {
			[NanofoilCoatingEquipment.id]: 0.5,
		},
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LasersBoost: TPlayerResearchDefinition = {
	id: 291,
	caption: 'Hawking Condenser',
	description: '+10% Lasers Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BeamPrecisionRepeatable.id,
		PlayerResearch_BeamDamageRepeatable.id,
		PlayerResearch_BeamRateRepeatable.id,
	],
	technologyReward: {
		equipment: {
			[HiggsPumpEquipment.id]: 0.5,
		},
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseRange: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_VacuumCoolant: TPlayerResearchDefinition = {
	id: 295,
	caption: `Vacuum Coolant`,
	description: `+0.5 Speed to Combat Drone, Rover and Hovertank; Equipment: ECCM Module`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BeamPrecisionRepeatable.id,
		PlayerResearch_BeamsDamage3.id,
		PlayerResearch_QuantumExciter.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.hovertank]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[ECCMModuleEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_EleriumCapacitor: TPlayerResearchDefinition = {
	id: 296,
	caption: `Elerium Capacitor`,
	description: `+5 Shield Capacity to Tank, LAV and HAV; Equipment: Elerium Battery`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BeamDamageRepeatable.id,
		PlayerResearch_LaserRate3.id,
		PlayerResearch_QuantumExciter.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.hav]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.lav]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tank]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[EleriumBatteryEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_NeutrinoGenerator: TPlayerResearchDefinition = {
	id: 297,
	caption: `Proton Generator`,
	description: `+0.5 Scan Range to PDD and Fortress; Equipment: Proton Generator`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BeamRateRepeatable.id,
		PlayerResearch_BeamsRange3.id,
		PlayerResearch_QuantumExciter.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.fortress]: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[ProtonGeneratorEquipment.id]: 0.5,
		},
	},
};

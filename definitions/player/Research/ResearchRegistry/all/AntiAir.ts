import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import GravitonCatapultEquipment from '../../../../technologies/equipment/all/Offensive/GravitonCatapult.20021';
import TacticalClusterEquipment from '../../../../technologies/equipment/all/Tactical/TacticalCluster.20009';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import CarapaceWeapon from '../../../../technologies/weapons/all/antiair/Carapace.70006';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';
import {
	PlayerResearch_AACannon,
	PlayerResearch_GunsDamage1,
	PlayerResearch_GunsDamage2,
	PlayerResearch_GunsDamage3,
	PlayerResearch_GunsPrecision1,
	PlayerResearch_GunsPrecision2,
	PlayerResearch_GunsPrecision3,
	PlayerResearch_GunsRange1,
	PlayerResearch_GunsRange2,
	PlayerResearch_GunsRange3,
} from './Guns';
import {
	PlayerResearch_BeamsRange1,
	PlayerResearch_BeamsRange2,
	PlayerResearch_BeamsRange3,
	PlayerResearch_LaserDamage1,
	PlayerResearch_LaserDamage2,
	PlayerResearch_LaserDamage3,
	PlayerResearch_LaserPrecision1,
	PlayerResearch_LaserPrecision2,
	PlayerResearch_LaserPrecision3,
} from './Lasers';
import {
	PlayerResearch_AAMissile,
	PlayerResearch_CruiseRange1,
	PlayerResearch_CruiseRange2,
	PlayerResearch_CruiseRange3,
	PlayerResearch_MissileDamage1,
	PlayerResearch_MissileDamage2,
	PlayerResearch_MissileDamage3,
	PlayerResearch_MissilePrecision1,
	PlayerResearch_MissilePrecision2,
	PlayerResearch_MissilePrecision3,
} from './Rocketry';

export const PlayerResearch_AAPrecision1: TPlayerResearchDefinition = {
	id: 1000,
	caption: 'AA Precision I',
	description: '+5% Hit Chance for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_GunsPrecision1.id,
		PlayerResearch_LaserPrecision1.id,
		PlayerResearch_MissilePrecision1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AAPrecision2: TPlayerResearchDefinition = {
	id: 1001,
	caption: 'AA Precision II',
	description: '+5% Hit Chance for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_GunsPrecision2.id,
		PlayerResearch_LaserPrecision2.id,
		PlayerResearch_MissilePrecision2.id,
		PlayerResearch_AAPrecision1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AAPrecision3: TPlayerResearchDefinition = {
	id: 1002,
	caption: 'AA Precision III',
	description: '+5% Hit Chance for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_GunsPrecision3.id,
		PlayerResearch_LaserPrecision3.id,
		PlayerResearch_MissilePrecision3.id,
		PlayerResearch_AAPrecision2.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AADamage1: TPlayerResearchDefinition = {
	id: 1010,
	caption: 'AA Damage I',
	description: '+5% Damage for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_GunsDamage1.id,
		PlayerResearch_MissileDamage1.id,
		PlayerResearch_LaserDamage1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AADamage2: TPlayerResearchDefinition = {
	id: 1011,
	caption: 'AA Damage II',
	description: '+5% Damage for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_GunsDamage2.id,
		PlayerResearch_MissileDamage2.id,
		PlayerResearch_LaserDamage2.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AADamage3: TPlayerResearchDefinition = {
	id: 1012,
	caption: 'AA Damage IШI',
	description: '+5% Damage for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_GunsDamage3.id,
		PlayerResearch_MissileDamage3.id,
		PlayerResearch_LaserDamage3.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AARange1: TPlayerResearchDefinition = {
	id: 1020,
	caption: 'AA Range I',
	description: '+0.25 Anti-Air Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_GunsRange1.id,
		PlayerResearch_CruiseRange1.id,
		PlayerResearch_BeamsRange1.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AARange2: TPlayerResearchDefinition = {
	id: 1021,
	caption: 'AA Range II',
	description: '+0.25 Anti-Air Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_GunsRange2.id,
		PlayerResearch_CruiseRange2.id,
		PlayerResearch_BeamsRange2.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AARange3: TPlayerResearchDefinition = {
	id: 1022,
	caption: 'AA Range III',
	description: '+0.25 Anti-Air Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_GunsRange3.id,
		PlayerResearch_CruiseRange3.id,
		PlayerResearch_BeamsRange3.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Carapace: TPlayerResearchDefinition = {
	id: 1050,
	caption: 'Weapon: Carapace ADS',
	description: CarapaceWeapon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_AACannon.id,
		PlayerResearch_AAMissile.id,
		PlayerResearch_AARange1.id,
		PlayerResearch_AADamage1.id,
		PlayerResearch_AAPrecision1.id,
	],
	technologyReward: {
		weapons: {
			[CarapaceWeapon.id]: 0.5,
		},
	},
};
export const PlayerResearch_AAPrecisionRepeatable: TPlayerResearchDefinition = {
	id: 1030,
	repeatable: true,
	caption: `AA Precision ${INFINITY_SYMBOL}`,
	description: '+2% Hit Chance for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 64,
	prerequisiteResearchIds: [PlayerResearch_AAPrecision3.id, PlayerResearch_AARange2.id, PlayerResearch_AADamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseAccuracy: {
							bias: 0.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AAPDamageRepeatable: TPlayerResearchDefinition = {
	id: 1031,
	repeatable: true,
	caption: `AA Damage ${INFINITY_SYMBOL}`,
	description: '+2% Damage for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 64,
	prerequisiteResearchIds: [PlayerResearch_AAPrecision2.id, PlayerResearch_AARange2.id, PlayerResearch_AADamage3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AAPRangeRepeatable: TPlayerResearchDefinition = {
	id: 1032,
	repeatable: true,
	caption: `AA Range ${INFINITY_SYMBOL}`,
	description: '+0.1 Range for Anti-Air',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 64,
	prerequisiteResearchIds: [PlayerResearch_AAPrecision2.id, PlayerResearch_AARange3.id, PlayerResearch_AADamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseRange: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GravitonCapacitors: TPlayerResearchDefinition = {
	id: 1040,
	caption: 'Graviton Capacitors',
	description: `+5 Shield Capacity for Hovertank, Point Defense and Fortress; Equipment: Graviton Catapult`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_AARange3.id, PlayerResearch_AADamage3.id, PlayerResearch_AAPrecision3.id],
	unitModifier: {
		[UnitChassis.chassisClass.hovertank]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.fortress]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			shield: [
				{
					shieldAmount: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[GravitonCatapultEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_SubspaceConnectivity: TPlayerResearchDefinition = {
	id: 1041,
	caption: 'Subspace Connectivity',
	description: `+0.5 Scan Range for Support Units; Equipment: Tactical Cluster`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_AAPrecisionRepeatable.id,
		PlayerResearch_AAPDamageRepeatable.id,
		PlayerResearch_AAPRangeRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.sup_vehicle]: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_aircraft]: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[TacticalClusterEquipment.id]: 0.5,
		},
	},
};

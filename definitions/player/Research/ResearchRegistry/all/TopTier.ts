import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import JunkyardFactory from '../../../../orbital/factories/all/Junkyard.304';
import TitanForgeFactory from '../../../../orbital/factories/all/Titanforge.601';
import MobilizationAbility from '../../../../tactical/abilities/all/Player/Mobilization.60025';
import BehemothChassis from '../../../../technologies/chassis/all/CombatMech/Behemoth.18001';
import CombatMechChassis from '../../../../technologies/chassis/all/CombatMech/CombatMech.18000';
import JupiterChassis from '../../../../technologies/chassis/all/CombatMech/Jupiter.18002';
import SCVChassis from '../../../../technologies/chassis/all/CombatMech/SCV.18003';
import CharonChassis from '../../../../technologies/chassis/all/Fortress/Charon.59002';
import FortressChassis from '../../../../technologies/chassis/all/Fortress/Fortess.59000';
import FortrexChassis from '../../../../technologies/chassis/all/Fortress/Fortrex.59001';
import AdmiralsPrideChassis from '../../../../technologies/chassis/all/Manofwar/AdmiralsPride.69501';
import FlagShipChassis from '../../../../technologies/chassis/all/Manofwar/Manofwar.69500';
import MatriarchChassis from '../../../../technologies/chassis/all/MotherShip/Matriarch.89002';
import MothershipChassis from '../../../../technologies/chassis/all/MotherShip/Mothership.89000';
import VindicatorChassis from '../../../../technologies/chassis/all/MotherShip/Vindicator.89001';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AssaultDrone from '../../../../technologies/weapons/all/drones/AssaultDrone.65002';
import ChemDrone from '../../../../technologies/weapons/all/drones/ChemDrone.65005';
import DisruptorDrone from '../../../../technologies/weapons/all/drones/DisruptorDrone.65004';
import HunterSeekerDrone from '../../../../technologies/weapons/all/drones/HunterSeekerDrone.65003';
import LaserDrone from '../../../../technologies/weapons/all/drones/LaserDrone.65001';
import MissileDrone from '../../../../technologies/weapons/all/drones/MissileDrone.65000';
import PegasusDrone from '../../../../technologies/weapons/all/drones/PegasusDrone.65006';
import PoliticalFactions from '../../../../world/factions';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

import {
	PlayerResearch_AirDomination1,
	PlayerResearch_AirDomination2,
	PlayerResearch_AirDomination3,
	PlayerResearch_StrikeSupremacy1,
} from './Aircraft';
import { PlayerResearch_HullIntegrity2, PlayerResearch_StrongMaterials2 } from './Armor';
import { PlayerResearch_CrossbreedPsionics2, PlayerResearch_Exosuit, PlayerResearch_GeneSplicing1 } from './Bionics';
import { PlayerResearch_LaserRate2 } from './Lasers';
import { PlayerResearch_BombsArea2 } from './Launchers';
import { PlayerResearch_MassEducation2, PlayerResearch_MultiplexedControl1 } from './Mechanics';
import { PlayerResearch_GlobalNetwork2, PlayerResearch_Quadrapod, PlayerResearch_SapientNexus1 } from './Robotics';
import { PlayerResearch_MissileRange2 } from './Rocketry';
import { PlayerResearch_BatteryLife2, PlayerResearch_TransmissionPower2 } from './Shield';
import { PlayerResearch_FleetCommando1 } from './Ships';

export const PlayerResearch_AutomatedProduction: TPlayerResearchDefinition = {
	id: 2000,
	caption: 'Automated Production',
	description: '+1 Factory Production Speed',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_CrossbreedPsionics2.id,
		PlayerResearch_GlobalNetwork2.id,
		PlayerResearch_MassEducation2.id,
	],
	baseModifier: {
		productionSpeed: {
			bias: 1,
		},
	},
};
export const PlayerResearch_Junkyard: TPlayerResearchDefinition = {
	id: 2010,
	caption: 'Factory: Junkyard',
	description: JunkyardFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_AutomatedProduction.id],
	technologyReward: {
		factories: {
			[JunkyardFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_MissileDrone: TPlayerResearchDefinition = {
	id: 2011,
	caption: 'Weapon: Missile Drone',
	description: MissileDrone.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_AutomatedProduction.id, PlayerResearch_MissileRange2.id],
	technologyReward: {
		weapons: {
			[MissileDrone.id]: 0.5,
		},
	},
};
export const PlayerResearch_Mobilization: TPlayerResearchDefinition = {
	id: 2012,
	caption: 'Ability: Mobilization',
	description: MobilizationAbility.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_AutomatedProduction.id],
	reward: (p) => p.learnAbility(MobilizationAbility.id),
};
export const PlayerResearch_MassiveProduction: TPlayerResearchDefinition = {
	id: 2020,
	caption: 'Massive Production',
	description: '+15 Production Capacity',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_AutomatedProduction.id,
		PlayerResearch_SapientNexus1.id,
		PlayerResearch_MultiplexedControl1.id,
		PlayerResearch_GeneSplicing1.id,
	],
	baseModifier: {
		productionCapacity: {
			bias: 15,
		},
	},
};
export const PlayerResearch_DroneAugmentation1: TPlayerResearchDefinition = {
	id: 2021,
	caption: 'Drone Augmentation I',
	description: '+4% Drone Weapons Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_MissileDrone.id, PlayerResearch_AirDomination1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Drone]: {
						damageModifier: {
							bias: 1.04,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaserDrone: TPlayerResearchDefinition = {
	id: 2022,
	caption: 'Weapon: Laser Drone',
	description: LaserDrone.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_MissileDrone.id, PlayerResearch_LaserRate2.id],
	technologyReward: {
		weapons: {
			[LaserDrone.id]: 0.5,
		},
	},
};
export const PlayerResearch_TitanForge: TPlayerResearchDefinition = {
	id: 2030,
	caption: 'Factory: Titan Forge',
	description: TitanForgeFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 21,
	prerequisiteResearchIds: [PlayerResearch_Junkyard.id, PlayerResearch_MassiveProduction.id],
	technologyReward: {
		factories: {
			[TitanForgeFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_CombatMech: TPlayerResearchDefinition = {
	id: 2031,
	caption: 'Chassis: Combat Mech',
	description: CombatMechChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 21,
	prerequisiteResearchIds: [
		PlayerResearch_Exosuit.id,
		PlayerResearch_StrongMaterials2.id,
		PlayerResearch_MassiveProduction.id,
	],
	technologyReward: {
		chassis: {
			[CombatMechChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Fortress: TPlayerResearchDefinition = {
	id: 2032,
	caption: 'Chassis: Fortress',
	description: FortressChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 21,
	prerequisiteResearchIds: [
		PlayerResearch_Quadrapod.id,
		PlayerResearch_BatteryLife2.id,
		PlayerResearch_MassiveProduction.id,
	],
	technologyReward: {
		chassis: {
			[FortressChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_DroneAugmentation2: TPlayerResearchDefinition = {
	id: 2040,
	caption: 'Drone Augmentation II',
	description: '+4% Drone Weapons Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_LaserDrone.id, PlayerResearch_AirDomination2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Drone]: {
						damageModifier: {
							bias: 1.04,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_TitanQuest1: TPlayerResearchDefinition = {
	id: 2041,
	caption: 'Titan Quest I',
	description: '+10 Shield Capacity for Fortress, Quadrapod, Combat Mech, Mothership and Man-of-War',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_CombatMech.id, PlayerResearch_Fortress.id],
	unitModifier: {
		[UnitChassis.chassisClass.fortress]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.quadrapod]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.mothership]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.mecha]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.manofwar]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Charon: TPlayerResearchDefinition = {
	id: 2042,
	caption: 'Chassis: Charon',
	exclusiveFactions: [PoliticalFactions.TFactionID.reticulans],
	description: CharonChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Fortress.id],
	technologyReward: {
		chassis: {
			[CharonChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Fortrex: TPlayerResearchDefinition = {
	id: 2043,
	caption: 'Chassis: Fort-REX',
	description: FortressChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Fortress.id],
	technologyReward: {
		chassis: {
			[FortrexChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Jupiter: TPlayerResearchDefinition = {
	id: 2044,
	caption: 'Chassis: Jupiter',
	description: JupiterChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_CombatMech.id],
	technologyReward: {
		chassis: {
			[JupiterChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Behemoth: TPlayerResearchDefinition = {
	id: 2045,
	exclusiveFactions: [PoliticalFactions.TFactionID.venaticians],
	caption: 'Chassis: Behemoth',
	description: BehemothChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_CombatMech.id],
	technologyReward: {
		chassis: {
			[BehemothChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Mothership: TPlayerResearchDefinition = {
	id: 2050,
	caption: 'Chassis: Mothership',
	description: MothershipChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_StrikeSupremacy1.id,
		PlayerResearch_MissileDrone.id,
		PlayerResearch_MassiveProduction.id,
	],
	technologyReward: {
		chassis: {
			[MothershipChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Manofwar: TPlayerResearchDefinition = {
	id: 2051,
	caption: 'Chassis: Man-Of-War',
	description: FlagShipChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_FleetCommando1.id,
		PlayerResearch_MissileDrone.id,
		PlayerResearch_MassiveProduction.id,
	],
	technologyReward: {
		chassis: {
			[FlagShipChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_ParagonCoordination: TPlayerResearchDefinition = {
	id: 2052,
	caption: 'Paragon Coordination',
	description: '+1 Ability Slot; +5 %Hit Points for Fortress and Combat Mech; +200 Raw Materials Storage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_TitanQuest1.id, PlayerResearch_Mobilization.id],
	baseModifier: {
		maxRawMaterials: {
			bias: 200,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.fortress]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.05,
					},
				},
			],
		},
		[UnitChassis.chassisClass.mecha]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.05,
					},
				},
			],
		},
	},
	reward: (p) => {
		p.abilitySlots += 1;
		return p;
	},
};
export const PlayerResearch_ChemDrone: TPlayerResearchDefinition = {
	id: 2053,
	caption: 'Weapon: Anti-Personel Drone',
	description: ChemDrone.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_LaserDrone.id, PlayerResearch_BombsArea2.id],
	technologyReward: {
		weapons: {
			[ChemDrone.id]: 0.5,
		},
	},
};
export const PlayerResearch_DroneAugmentation3: TPlayerResearchDefinition = {
	id: 2054,
	caption: 'Drone Augmentation III',
	description: '+4% Drone Weapons Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_LaserDrone.id, PlayerResearch_AirDomination3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Drone]: {
						damageModifier: {
							bias: 1.04,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_TitanQuest2: TPlayerResearchDefinition = {
	id: 2060,
	caption: 'Titan Quest II',
	description: '+10 Shield Capacity for Fortress, Quadrapod, Combat Mech, Mothership and Man-of-War',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_TitanQuest1.id, PlayerResearch_Mothership.id, PlayerResearch_Manofwar.id],
	unitModifier: {
		[UnitChassis.chassisClass.fortress]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.quadrapod]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.mothership]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.mecha]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.manofwar]: {
			shield: [
				{
					shieldAmount: {
						bias: 10,
					},
				},
			],
		},
	},
};
export const PlayerResearch_SCV: TPlayerResearchDefinition = {
	id: 2061,
	caption: 'Chassis: SCV',
	description: SCVChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_CombatMech.id, PlayerResearch_TitanQuest1.id],
	technologyReward: {
		chassis: {
			[SCVChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Matriarch: TPlayerResearchDefinition = {
	id: 2062,
	caption: 'Chassis: Matriarch',
	description: MatriarchChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_Mothership.id,
		PlayerResearch_DroneAugmentation2.id,
		PlayerResearch_TitanQuest1.id,
	],
	technologyReward: {
		chassis: {
			[MatriarchChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Vindicator: TPlayerResearchDefinition = {
	id: 2063,
	exclusiveFactions: [PoliticalFactions.TFactionID.reticulans],
	caption: 'Chassis: Vindicator',
	description: VindicatorChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_Mothership.id],
	technologyReward: {
		chassis: {
			[VindicatorChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_AdmiralsPride: TPlayerResearchDefinition = {
	id: 2064,
	exclusiveFactions: [PoliticalFactions.TFactionID.pirates],
	caption: `Chassis: Admiral's Pride`,
	description: AdmiralsPrideChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_Mothership.id],
	technologyReward: {
		chassis: {
			[AdmiralsPrideChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_HunterSeekerDrone: TPlayerResearchDefinition = {
	id: 2065,
	caption: 'Weapon: Hunter-Seeker Drone',
	description: HunterSeekerDrone.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_ChemDrone.id, PlayerResearch_DroneAugmentation2.id],
	technologyReward: {
		weapons: {
			[HunterSeekerDrone.id]: 0.5,
		},
	},
};
export const PlayerResearch_DisruptorDrone: TPlayerResearchDefinition = {
	id: 2070,
	caption: 'Weapon: Disruptor Drone',
	description: DisruptorDrone.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_ChemDrone.id, PlayerResearch_DroneAugmentation3.id],
	technologyReward: {
		weapons: {
			[DisruptorDrone.id]: 0.5,
		},
	},
};
export const PlayerResearch_DroneAugmentationRepeatable: TPlayerResearchDefinition = {
	id: 2071,
	caption: `Drone Predominance ${INFINITY_SYMBOL}`,
	description: '+2% Drone Weapons Damage; +0.1 Weapons Drone AOE Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_TitanQuest1.id, PlayerResearch_DroneAugmentation3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Drone]: {
						damageModifier: {
							bias: 1.02,
						},
						aoeRadiusModifier: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_TitanQuestRepeatable: TPlayerResearchDefinition = {
	id: 2072,
	caption: `Atlas Shrugged ${INFINITY_SYMBOL}`,
	description: '+2% Hit Points and Dodge for Combat Mech, Fortress, Mothership and Man-of-war',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_TitanQuest2.id,
		PlayerResearch_HullIntegrity2.id,
		PlayerResearch_TransmissionPower2.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.mecha]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.02,
					},
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.fortress]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.02,
					},
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.manofwar]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.02,
					},
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.mothership]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.02,
					},
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_AssaultDrone: TPlayerResearchDefinition = {
	id: 2080,
	caption: 'Weapon: Assault Drone',
	description: AssaultDrone.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_DroneAugmentationRepeatable.id, PlayerResearch_HunterSeekerDrone.id],
	technologyReward: {
		weapons: {
			[AssaultDrone.id]: 0.5,
		},
	},
};
export const PlayerResearch_PegasusDrone: TPlayerResearchDefinition = {
	id: 2081,
	caption: 'Weapon: Pegasus Drone',
	description: PegasusDrone.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_DroneAugmentationRepeatable.id, PlayerResearch_HunterSeekerDrone.id],
	technologyReward: {
		weapons: {
			[PegasusDrone.id]: 0.5,
		},
	},
};
export const PlayerResearch_DurableMaterials: TPlayerResearchDefinition = {
	id: 2082,
	caption: 'Durable Materials',
	description: '+2 Armor Protection and +1 Effect Resistance for All Units',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_TitanQuestRepeatable.id, PlayerResearch_DroneAugmentationRepeatable.id],
	unitModifier: {
		default: {
			unitModifier: {
				statusDurationModifier: [
					{
						bias: -1,
						min: 0,
						minGuard: 0,
					},
				],
			},
			armor: [
				{
					default: {
						bias: -2,
					},
				},
			],
		},
	},
};

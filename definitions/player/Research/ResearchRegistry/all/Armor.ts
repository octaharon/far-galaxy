import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import InertArmor from '../../../../technologies/armor/all/large/InertArmor.30011';
import LargeBioArmor from '../../../../technologies/armor/all/large/LargeBioArmor.30007';
import LargeCompositeArmor from '../../../../technologies/armor/all/large/LargeCompositeArmor.30002';
import LargeDefenseMatrix from '../../../../technologies/armor/all/large/LargeDefenseMatrix.30004';
import LargeGluonArmor from '../../../../technologies/armor/all/large/LargeGluonArmor.30005';
import LargeKevlarArmor from '../../../../technologies/armor/all/large/LargeKevlarArmor.30000';
import LargeNanoArmor from '../../../../technologies/armor/all/large/LargeNanoArmor.30001';
import LargeQuantumArmor from '../../../../technologies/armor/all/large/LargeQuantumArmor.30003';
import LargeRaidArmor from '../../../../technologies/armor/all/large/LargeRaidArmor.30008';
import LargeReactiveArmor from '../../../../technologies/armor/all/large/LargeReactiveArmor.30006';
import ProbabilityArmor from '../../../../technologies/armor/all/large/ProbabilityArmor.30010';
import SMESArmor from '../../../../technologies/armor/all/large/SMESArmor.30009';
import BioArmor from '../../../../technologies/armor/all/medium/BioArmor.20004';
import CompositeArmor from '../../../../technologies/armor/all/medium/CompositeArmor.20002';
import DefenseMatrix from '../../../../technologies/armor/all/medium/DefenseMatrix.20005';
import ElectronArmor from '../../../../technologies/armor/all/medium/ElectronArmor.20006';
import GluonArmor from '../../../../technologies/armor/all/medium/GluonArmor.20009';
import KevlarArmor from '../../../../technologies/armor/all/medium/KevlarArmor.20000';
import NanoArmor from '../../../../technologies/armor/all/medium/NanoArmor.20001';
import PositronArmor from '../../../../technologies/armor/all/medium/PositronArmor.20007';
import QuantumArmor from '../../../../technologies/armor/all/medium/QuantumArmor.20003';
import RaidArmor from '../../../../technologies/armor/all/medium/RaidArmor.20010';
import ReactiveArmor from '../../../../technologies/armor/all/medium/ReactiveArmor.20008';
import AdvancedPowerArmor from '../../../../technologies/armor/all/small/AdvancedPowerArmor.10010';
import CompositePlatingArmor from '../../../../technologies/armor/all/small/CompositePlating.10003';
import CorsairArmor from '../../../../technologies/armor/all/small/CorsairArmor.10009';
import KevlarPlatingArmor from '../../../../technologies/armor/all/small/KevlarPlating.10000';
import NanoPlatingArmor from '../../../../technologies/armor/all/small/NanoPlating.10001';
import PowerArmor from '../../../../technologies/armor/all/small/PowerArmor.10004';
import SmallBioArmor from '../../../../technologies/armor/all/small/SmallBioArmor.10007';
import SmallDefenseMatrix from '../../../../technologies/armor/all/small/SmallDefenseMatrix.10006';
import SmallGluonArmor from '../../../../technologies/armor/all/small/SmallGluonArmor.10008';
import SmallReactiveArmor from '../../../../technologies/armor/all/small/SmallReactiveArmor.10005';
import ThickKevlarPlatingArmor from '../../../../technologies/armor/all/small/ThickKevlarPlating.10002';
import FusionCoreEquipment from '../../../../technologies/equipment/all/Enhancement/FusionCore.20012';
import AugmentedFrameEquipment from '../../../../technologies/equipment/all/Enhancement/AugmentedFrame.20006';
import MobileHQEquipment from '../../../../technologies/equipment/all/Tactical/MobileHQ.30008';
import BacterialFarmEquipment from '../../../../technologies/equipment/all/Warfare/BacterialFarm.30005';
import UnitChassis from '../../../../technologies/types/chassis';
import PoliticalFactions from '../../../../world/factions';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_SmallArmor: TPlayerResearchDefinition = {
	id: 700,
	caption: 'Armor: Kevlar Plating',
	description: KevlarPlatingArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		armor: {
			[KevlarPlatingArmor.id]: 1,
		},
	},
};
export const PlayerResearch_CarbonFoamPlating: TPlayerResearchDefinition = {
	id: 705,
	caption: 'Armor: Carbon Foam Plating',
	description: ThickKevlarPlatingArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_SmallArmor.id],
	technologyReward: {
		armor: {
			[ThickKevlarPlatingArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_StrongMaterials1: TPlayerResearchDefinition = {
	id: 710,
	caption: 'Strong Materials I',
	description: '+3% Armor Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_SmallArmor.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_LightMaterials1: TPlayerResearchDefinition = {
	id: 711,
	caption: 'Light Materials I',
	description: '+3% Dodge',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_SmallArmor.id],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MediumArmor: TPlayerResearchDefinition = {
	id: 712,
	caption: 'Armor: Kevlar Armor',
	description: KevlarArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_CarbonFoamPlating.id],
	technologyReward: {
		armor: {
			[KevlarArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_NanocellPlating: TPlayerResearchDefinition = {
	id: 720,
	caption: 'Armor: Nanocell Plating',
	description: NanoPlatingArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_LightMaterials1.id],
	technologyReward: {
		armor: {
			[NanoPlatingArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_PowerArmor: TPlayerResearchDefinition = {
	id: 721,
	caption: 'Armor: Power Armor',
	description: PowerArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_StrongMaterials1.id],
	technologyReward: {
		armor: {
			[PowerArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_CompositeArmor: TPlayerResearchDefinition = {
	id: 722,
	caption: 'Armor: Composite Armor',
	description: CompositeArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_MediumArmor.id],
	technologyReward: {
		armor: {
			[CompositeArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeArmor: TPlayerResearchDefinition = {
	id: 725,
	caption: 'Armor: Large Kevlar Armor',
	description: LargeKevlarArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_MediumArmor.id,
		PlayerResearch_StrongMaterials1.id,
		PlayerResearch_LightMaterials1.id,
	],
	technologyReward: {
		armor: {
			[LargeKevlarArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_CompositePlating: TPlayerResearchDefinition = {
	id: 726,
	caption: 'Armor: Composite Plating',
	description: CompositePlatingArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_CarbonFoamPlating.id,
		PlayerResearch_StrongMaterials1.id,
		PlayerResearch_LightMaterials1.id,
	],
	technologyReward: {
		armor: {
			[CompositePlatingArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_NanoArmor: TPlayerResearchDefinition = {
	id: 727,
	caption: 'Armor: Nano Armor',
	description: NanoArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [
		PlayerResearch_MediumArmor.id,
		PlayerResearch_StrongMaterials1.id,
		PlayerResearch_LightMaterials1.id,
	],
	technologyReward: {
		armor: {
			[NanoArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_StrongMaterials2: TPlayerResearchDefinition = {
	id: 730,
	caption: 'Strong Materials II',
	description: '+3% Armor Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_CarbonFoamPlating.id, PlayerResearch_StrongMaterials1.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_LightMaterials2: TPlayerResearchDefinition = {
	id: 731,
	caption: 'Light Materials II',
	description: '+3% Dodge',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_CarbonFoamPlating.id, PlayerResearch_LightMaterials1.id],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LargeCompositeArmor: TPlayerResearchDefinition = {
	id: 735,
	caption: 'Armor: Large Composite Armor',
	description: LargeCompositeArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_LargeArmor.id],
	technologyReward: {
		armor: {
			[LargeCompositeArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_ReactiveArmorSmall: TPlayerResearchDefinition = {
	id: 736,
	caption: 'Armor: Small Reactive Armor',
	description: SmallReactiveArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [
		PlayerResearch_CompositePlating.id,
		PlayerResearch_StrongMaterials2.id,
		PlayerResearch_LightMaterials2.id,
	],
	technologyReward: {
		armor: {
			[SmallReactiveArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_QuantumArmor: TPlayerResearchDefinition = {
	id: 737,
	caption: 'Armor: Quantum Armor',
	description: QuantumArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [
		PlayerResearch_CompositeArmor.id,
		PlayerResearch_StrongMaterials2.id,
		PlayerResearch_LightMaterials2.id,
	],
	technologyReward: {
		armor: {
			[QuantumArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_HullIntegrity1: TPlayerResearchDefinition = {
	id: 738,
	caption: 'Hull Integrity I',
	description: '+50 Base Hit Points',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [
		PlayerResearch_LargeArmor.id,
		PlayerResearch_StrongMaterials2.id,
		PlayerResearch_LightMaterials2.id,
	],
	baseModifier: {
		maxHitPoints: {
			bias: 50,
		},
	},
};
export const PlayerResearch_StrongMaterials3: TPlayerResearchDefinition = {
	id: 740,
	caption: 'Strong Materials III',
	description: '+3% Armor Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_NanoArmor.id, PlayerResearch_StrongMaterials2.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_LightMaterials3: TPlayerResearchDefinition = {
	id: 741,
	caption: 'Light Materials III',
	description: '+3% Dodge',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_NanoArmor.id, PlayerResearch_LightMaterials2.id],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LargeNanoArmor: TPlayerResearchDefinition = {
	id: 742,
	caption: 'Armor: Large Nano Armor',
	description: LargeNanoArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_LargeCompositeArmor.id,
		PlayerResearch_LightMaterials2.id,
		PlayerResearch_StrongMaterials2.id,
	],
	technologyReward: {
		armor: {
			[LargeNanoArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_ReactiveArmor: TPlayerResearchDefinition = {
	id: 745,
	caption: 'Armor: Reactive Armor',
	description: ReactiveArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_ReactiveArmorSmall.id, PlayerResearch_LightMaterials3.id],
	technologyReward: {
		armor: {
			[ReactiveArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_AdvancedPowerArmor: TPlayerResearchDefinition = {
	id: 746,
	caption: 'Armor: Advanced Power Armor',
	description: AdvancedPowerArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_PowerArmor.id, PlayerResearch_StrongMaterials3.id],
	technologyReward: {
		armor: {
			[AdvancedPowerArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeQuantumArmor: TPlayerResearchDefinition = {
	id: 747,
	caption: 'Armor: Large Quantum Armor',
	description: LargeQuantumArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [
		PlayerResearch_LargeNanoArmor.id,
		PlayerResearch_LightMaterials3.id,
		PlayerResearch_StrongMaterials3.id,
	],
	technologyReward: {
		armor: {
			[LargeQuantumArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_CorsairArmor: TPlayerResearchDefinition = {
	id: 748,
	exclusiveFactions: [PoliticalFactions.TFactionID.pirates],
	caption: 'Armor: Corsair Armor',
	description: CorsairArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [
		PlayerResearch_ReactiveArmorSmall.id,
		PlayerResearch_LightMaterials3.id,
		PlayerResearch_StrongMaterials3.id,
	],
	technologyReward: {
		armor: {
			[CorsairArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_HullIntegrity2: TPlayerResearchDefinition = {
	id: 750,
	caption: 'Hull Integrity II',
	description: '+50 Base Hit Points',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [
		PlayerResearch_HullIntegrity1.id,
		PlayerResearch_StrongMaterials3.id,
		PlayerResearch_LightMaterials3.id,
	],
	baseModifier: {
		maxHitPoints: {
			bias: 50,
		},
	},
};
export const PlayerResearch_OrganicMatters1: TPlayerResearchDefinition = {
	id: 751,
	caption: 'Organic Matters I',
	description: '+3 Infantry, Exosuit and Biotech Hit Points; Armor: Small Bio Armor',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [
		PlayerResearch_HullIntegrity1.id,
		PlayerResearch_StrongMaterials3.id,
		PlayerResearch_LightMaterials3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
	technologyReward: {
		armor: {
			[SmallBioArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_WaveCommune1: TPlayerResearchDefinition = {
	id: 752,
	caption: 'Wave Commune I',
	description: '+3 Droid, Combat Drone and Rover Hit Points; Armor: Small Gluon Armor',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [
		PlayerResearch_HullIntegrity1.id,
		PlayerResearch_StrongMaterials3.id,
		PlayerResearch_LightMaterials3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
	technologyReward: {
		armor: {
			[SmallGluonArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_StrongMaterials4: TPlayerResearchDefinition = {
	id: 760,
	caption: 'Strong Materials IV',
	description: '+3% Armor Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_LargeNanoArmor.id, PlayerResearch_StrongMaterials3.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_LightMaterials4: TPlayerResearchDefinition = {
	id: 761,
	caption: 'Light Materials IV',
	description: '+3% Dodge',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_LargeNanoArmor.id, PlayerResearch_LightMaterials3.id],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LargeReactiveArmor: TPlayerResearchDefinition = {
	id: 762,
	caption: 'Armor: Large Reactive Armor',
	description: LargeReactiveArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_ReactiveArmor.id, PlayerResearch_LargeNanoArmor.id],
	technologyReward: {
		armor: {
			[LargeReactiveArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_MediumBioArmor: TPlayerResearchDefinition = {
	id: 763,
	caption: 'Armor: Bio Armor',
	description: BioArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_OrganicMatters1.id, PlayerResearch_QuantumArmor.id],
	technologyReward: {
		armor: {
			[BioArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeBioArmor: TPlayerResearchDefinition = {
	id: 764,
	caption: 'Armor: Large Bio Armor',
	description: LargeBioArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_OrganicMatters1.id, PlayerResearch_LargeNanoArmor.id],
	technologyReward: {
		armor: {
			[LargeBioArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_MediumGluonArmor: TPlayerResearchDefinition = {
	id: 765,
	caption: 'Armor: Gluon Armor',
	description: GluonArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_WaveCommune1.id, PlayerResearch_QuantumArmor.id],
	technologyReward: {
		armor: {
			[GluonArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeGluonArmor: TPlayerResearchDefinition = {
	id: 766,
	caption: 'Armor: Large Gluon Armor',
	description: LargeGluonArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_WaveCommune1.id, PlayerResearch_LargeNanoArmor.id],
	technologyReward: {
		armor: {
			[LargeGluonArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_RaidArmor: TPlayerResearchDefinition = {
	id: 770,
	exclusiveFactions: [PoliticalFactions.TFactionID.pirates],
	caption: 'Armor: Raid Armor',
	description: RaidArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_CorsairArmor.id,
		PlayerResearch_QuantumArmor.id,
		PlayerResearch_LightMaterials4.id,
		PlayerResearch_StrongMaterials4.id,
	],
	technologyReward: {
		armor: {
			[RaidArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeRaidArmor: TPlayerResearchDefinition = {
	id: 771,
	exclusiveFactions: [PoliticalFactions.TFactionID.pirates],
	caption: 'Armor: Large Raid Armor',
	description: LargeRaidArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_CorsairArmor.id,
		PlayerResearch_LargeQuantumArmor.id,
		PlayerResearch_LightMaterials4.id,
		PlayerResearch_StrongMaterials4.id,
	],
	technologyReward: {
		armor: {
			[LargeRaidArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_PositronArmor: TPlayerResearchDefinition = {
	id: 772,
	caption: 'Armor: Positron Armor',
	description: PositronArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_QuantumArmor.id, PlayerResearch_LightMaterials4.id],
	technologyReward: {
		armor: {
			[PositronArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_ElectronArmor: TPlayerResearchDefinition = {
	id: 773,
	caption: 'Armor: Electron Armor',
	description: ElectronArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_QuantumArmor.id, PlayerResearch_StrongMaterials4.id],
	technologyReward: {
		armor: {
			[ElectronArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_ProbabilityArmor: TPlayerResearchDefinition = {
	id: 774,
	caption: 'Armor: Probability Armor',
	description: ProbabilityArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_LargeQuantumArmor.id, PlayerResearch_LightMaterials4.id],
	technologyReward: {
		armor: {
			[ProbabilityArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_InertArmor: TPlayerResearchDefinition = {
	id: 775,
	caption: 'Armor: Inert Armor',
	description: InertArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_LargeQuantumArmor.id, PlayerResearch_StrongMaterials4.id],
	technologyReward: {
		armor: {
			[InertArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_StrongMaterials5: TPlayerResearchDefinition = {
	id: 780,
	caption: 'Strong Materials V',
	description: '+3% Armor Protection',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_StrongMaterials4.id],
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						factor: 0.97,
					},
				},
			],
		},
	},
};
export const PlayerResearch_LightMaterials5: TPlayerResearchDefinition = {
	id: 781,
	caption: 'Light Materials V',
	description: '+3% Dodge',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_LightMaterials4.id],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_HullIntegrity3: TPlayerResearchDefinition = {
	id: 782,
	caption: 'Hull Integrity III',
	description: '+50 Base Hit Points',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_HullIntegrity2.id,
		PlayerResearch_StrongMaterials4.id,
		PlayerResearch_LightMaterials4.id,
	],
	baseModifier: {
		maxHitPoints: {
			bias: 50,
		},
	},
};
export const PlayerResearch_SmallDefenseMatrix: TPlayerResearchDefinition = {
	id: 785,
	caption: 'Armor: Small Defense Matrix',
	description: SmallDefenseMatrix.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_CompositePlating.id,
		PlayerResearch_StrongMaterials5.id,
		PlayerResearch_LightMaterials5.id,
	],
	technologyReward: {
		armor: {
			[SmallDefenseMatrix.id]: 0.5,
		},
	},
};
export const PlayerResearch_MediumDefenseMatrix: TPlayerResearchDefinition = {
	id: 786,
	caption: 'Armor: Defense Matrix',
	description: DefenseMatrix.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_QuantumArmor.id,
		PlayerResearch_StrongMaterials5.id,
		PlayerResearch_LightMaterials5.id,
	],
	technologyReward: {
		armor: {
			[DefenseMatrix.id]: 0.5,
		},
	},
};
export const PlayerResearch_LargeDefenseMatrix: TPlayerResearchDefinition = {
	id: 787,
	caption: 'Armor: Large Defense Matrix',
	description: LargeDefenseMatrix.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_LargeQuantumArmor.id,
		PlayerResearch_StrongMaterials5.id,
		PlayerResearch_LightMaterials5.id,
	],
	technologyReward: {
		armor: {
			[LargeDefenseMatrix.id]: 0.5,
		},
	},
};
export const PlayerResearch_OrganicMatters2: TPlayerResearchDefinition = {
	id: 788,
	caption: 'Organic Matters II',
	description: '+3 Infantry, Exosuit and Biotech Hit Points; Equipment: Bacterial Farm',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_HullIntegrity3.id, PlayerResearch_OrganicMatters1.id],
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[BacterialFarmEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_WaveCommune2: TPlayerResearchDefinition = {
	id: 789,
	caption: 'Wave Commune II',
	description: '+3 Droid, Combat Drone and Rover Hit Points; Equipment: MobileHQ',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_WaveCommune1.id, PlayerResearch_HullIntegrity3.id],
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[MobileHQEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_SMESArmor: TPlayerResearchDefinition = {
	id: 790,
	caption: 'Armor: SMA Armor',
	description: SMESArmor.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_LargeBioArmor.id,
		PlayerResearch_LargeGluonArmor.id,
		PlayerResearch_LightMaterials5.id,
		PlayerResearch_StrongMaterials5.id,
	],
	technologyReward: {
		armor: {
			[SMESArmor.id]: 0.5,
		},
	},
};
export const PlayerResearch_FibrousAnnihilation: TPlayerResearchDefinition = {
	id: 791,
	caption: 'Fibrous Annihilation',
	description: '+10 Artillery, Launcher Pad and Fortress Hit Points. +25 Base Hit Points. Equipment: Polymer Frame',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_WaveCommune2.id, PlayerResearch_OrganicMatters2.id],
	baseModifier: {
		maxHitPoints: {
			bias: 25,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.artillery]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.fortress]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.launcherpad]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[AugmentedFrameEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_AlloyRecombination: TPlayerResearchDefinition = {
	id: 792,
	caption: 'Alloy Recombination',
	description:
		'+5% Armor Protection for Destroyer, Battleship and Cruiser. +5% Base Repair Output. Equipment: Fusion Core',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [PlayerResearch_WaveCommune2.id, PlayerResearch_OrganicMatters2.id],
	baseModifier: {
		repairOutput: {
			factor: 1.05,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.destroyer]: {
			armor: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
		[UnitChassis.chassisClass.cruiser]: {
			armor: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
		[UnitChassis.chassisClass.battleship]: {
			armor: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[FusionCoreEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_ArmorRepeatable: TPlayerResearchDefinition = {
	repeatable: true,
	id: 795,
	caption: `Dodge ${INFINITY_SYMBOL}`,
	description: '+2% Dodge on all Units',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 59,
	prerequisiteResearchIds: [PlayerResearch_OrganicMatters2.id, PlayerResearch_WaveCommune2.id],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
				},
			],
		},
	},
};

import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import AeronauticalComplexFactory from '../../../../orbital/factories/all/AeronauticalComplex.500';
import AirforgeFactory from '../../../../orbital/factories/all/Airforge.502';
import HoverforgeFactory from '../../../../orbital/factories/all/Hoverforge.501';
import VentilationAbility from '../../../../tactical/abilities/all/Player/Ventilation.60020';
import UnitAttack from '../../../../tactical/damage/attack';
import BlackjackChassis from '../../../../technologies/chassis/all/Bomber/Blackjack.80003';
import BomberChassis from '../../../../technologies/chassis/all/Bomber/Bomber.80000';
import MaulerChassis from '../../../../technologies/chassis/all/Bomber/Mauler.80001';
import MirageChassis from '../../../../technologies/chassis/all/Bomber/Mirage.80002';
import CarrierChassis from '../../../../technologies/chassis/all/Carrier/Carrier.88000';
import CloudOrcaChassis from '../../../../technologies/chassis/all/Carrier/CloudOrca.88002';
import LeviathanChassis from '../../../../technologies/chassis/all/Carrier/Leviathan.88001';
import FalconChassis from '../../../../technologies/chassis/all/Fighter/Falcon.82002';
import FighterChassis from '../../../../technologies/chassis/all/Fighter/Fighter.82000';
import MantisChassis from '../../../../technologies/chassis/all/Fighter/Mantis.82001';
import HeliconChassis from '../../../../technologies/chassis/all/Helipad/Helicon.89501';
import HelipadChassis from '../../../../technologies/chassis/all/Helipad/Helipad.89500';
import DefenderOfFaithChassis from '../../../../technologies/chassis/all/HoverTank/DefenderOfFaith.55003';
import HovertankChassis from '../../../../technologies/chassis/all/HoverTank/Hovertank.55000';
import MammothChassis from '../../../../technologies/chassis/all/HoverTank/Mammoth.55001';
import RamraiderChassis from '../../../../technologies/chassis/all/HoverTank/Ramraider.55002';
import LauncherPadChassis from '../../../../technologies/chassis/all/LauncherPad/LauncherPad.86000';
import MangonelChassis from '../../../../technologies/chassis/all/LauncherPad/Mangonel.86002';
import TrebuchetChassis from '../../../../technologies/chassis/all/LauncherPad/Trebuchet.86001';
import EagleChassis from '../../../../technologies/chassis/all/StrikeAircraft/Eagle.85001';
import OspreyChassis from '../../../../technologies/chassis/all/StrikeAircraft/Osprey.85002';
import SparrowChassis from '../../../../technologies/chassis/all/StrikeAircraft/Sparrow.85003';
import StrikeAircraftChassis from '../../../../technologies/chassis/all/StrikeAircraft/StrikeAircraft.85000';
import ImposterChassis from '../../../../technologies/chassis/all/SupportAircraft/Imposter.81001';
import SupportAircraftChassis from '../../../../technologies/chassis/all/SupportAircraft/SupportAircraft.81000';
import AirwolfChassis from '../../../../technologies/chassis/all/Titljet/Airwolf.84001';
import BluethunderChassis from '../../../../technologies/chassis/all/Titljet/BlueThunder.84002';
import FlyingFoxChassis from '../../../../technologies/chassis/all/Titljet/FlyingFox.84003';
import TiltjetChassis from '../../../../technologies/chassis/all/Titljet/Tiltjet.84000';
import GroundingDeviceEquipment from '../../../../technologies/equipment/all/Support/GroundingDevice.20023';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import PoliticalFactions from '../../../../world/factions';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';
import { PlayerResearch_Footcraft2 } from './Bionics';
import { PlayerResearch_Wheelcraft2 } from './Mechanics';
import { PlayerResearch_Rover } from './Robotics';

export const PlayerResearch_AeronauticsComplex: TPlayerResearchDefinition = {
	id: 1300,
	caption: 'Factory: Aeronautics Complex',
	description: AeronauticalComplexFactory.description,
	prerequisiteResearchIds: [PlayerResearch_Rover.id, PlayerResearch_Wheelcraft2.id, PlayerResearch_Footcraft2.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	technologyReward: {
		factories: {
			[AeronauticalComplexFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Tiltjet: TPlayerResearchDefinition = {
	id: 1305,
	caption: 'Chassis: Tiltjet',
	description: TiltjetChassis.description,
	prerequisiteResearchIds: [PlayerResearch_AeronauticsComplex.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	technologyReward: {
		chassis: {
			[TiltjetChassis.id]: 1,
		},
	},
};
export const PlayerResearch_SupportAircraft: TPlayerResearchDefinition = {
	id: 1306,
	caption: 'Chassis: Support Aircraft',
	description: SupportAircraftChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_AeronauticsComplex.id],
	technologyReward: {
		chassis: {
			[SupportAircraftChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Hovertank: TPlayerResearchDefinition = {
	id: 1310,
	caption: 'Chassis: Hovertank',
	description: HovertankChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_AeronauticsComplex.id],
	technologyReward: {
		chassis: {
			[HovertankChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_AirDomination1: TPlayerResearchDefinition = {
	id: 1311,
	caption: 'Air Domination I',
	description: '+5% Damage for Tiltjet, Hovertank and Strike Aircraft',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_AeronauticsComplex.id],
	unitModifier: {
		[UnitChassis.chassisClass.tiltjet]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hovertank]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BattleMomentum1: TPlayerResearchDefinition = {
	id: 1312,
	caption: 'Battle Momentum I',
	description: '+10% Aerial Dodge for Support Aircraft, Tiltjet and Strike Aircraft',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_AeronauticsComplex.id],
	unitModifier: {
		[UnitChassis.chassisClass.sup_aircraft]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.tiltjet]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Mammoth: TPlayerResearchDefinition = {
	id: 1315,
	caption: 'Chassis: Mammoth',
	description: MammothChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 9,
	prerequisiteResearchIds: [PlayerResearch_Hovertank.id, PlayerResearch_BattleMomentum1.id],
	technologyReward: {
		chassis: {
			[MammothChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Hoverforge: TPlayerResearchDefinition = {
	id: 1316,
	caption: 'Factory: Hoverforge',
	description: HoverforgeFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 9,
	prerequisiteResearchIds: [PlayerResearch_AirDomination1.id, PlayerResearch_Hovertank.id],
	technologyReward: {
		factories: {
			[HoverforgeFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_StrikeAircraft: TPlayerResearchDefinition = {
	id: 1317,
	caption: 'Chassis: Strike Aircrat',
	description: AirforgeFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 9,
	prerequisiteResearchIds: [PlayerResearch_SupportAircraft.id, PlayerResearch_Tiltjet.id],
	technologyReward: {
		chassis: {
			[StrikeAircraftChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Airwolf: TPlayerResearchDefinition = {
	id: 1318,
	caption: 'Chassis: Airwolf',
	description: AirwolfChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 9,
	prerequisiteResearchIds: [PlayerResearch_Tiltjet.id, PlayerResearch_AirDomination1.id],
	technologyReward: {
		chassis: {
			[AirwolfChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_DefenderOfFaith: TPlayerResearchDefinition = {
	id: 1319,
	exclusiveFactions: [PoliticalFactions.TFactionID.anders],
	caption: 'Chassis: Defender Of Faith',
	description: DefenderOfFaithChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 9,
	prerequisiteResearchIds: [PlayerResearch_Hovertank.id],
	technologyReward: {
		chassis: {
			[DefenderOfFaithChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_AirDomination2: TPlayerResearchDefinition = {
	id: 1320,
	caption: 'Air Domination II',
	description: '+5% Damage for Tiltjet, Hovertank and Strike Aircraft',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_AirDomination1.id, PlayerResearch_Hoverforge.id],
	unitModifier: {
		[UnitChassis.chassisClass.tiltjet]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hovertank]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BattleMomentum2: TPlayerResearchDefinition = {
	id: 1321,
	caption: 'Battle Momentum II',
	description: '+10% Aerial Dodge for Support Aircraft, Tiltjet and Strike Aircraft',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Hoverforge.id, PlayerResearch_BattleMomentum1.id],
	unitModifier: {
		[UnitChassis.chassisClass.sup_aircraft]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.tiltjet]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BlueThunder: TPlayerResearchDefinition = {
	id: 1330,
	caption: 'Chassis: Blue Thunder',
	description: BluethunderChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_AirDomination2.id, PlayerResearch_Airwolf.id],
	technologyReward: {
		chassis: {
			[BluethunderChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Airforge: TPlayerResearchDefinition = {
	id: 1331,
	caption: 'Factory: Airforge',
	description: AirforgeFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_StrikeAircraft.id, PlayerResearch_Hoverforge.id],
	technologyReward: {
		factories: {
			[AirforgeFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_LauncherPad: TPlayerResearchDefinition = {
	id: 1332,
	caption: 'Chassis: Launcher Pad',
	description: LauncherPadChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_AirDomination2.id, PlayerResearch_Hoverforge.id],
	technologyReward: {
		chassis: {
			[LauncherPadChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Ramraider: TPlayerResearchDefinition = {
	id: 1333,
	caption: 'Chassis: Ramraider',
	description: RamraiderChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Mammoth.id, PlayerResearch_BattleMomentum2.id],
	technologyReward: {
		chassis: {
			[RamraiderChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_StrikeSupremacy1: TPlayerResearchDefinition = {
	id: 1340,
	caption: 'Strike Supremacy I',
	description: '+10% Hit Chance for Bomber, Fighter and Launcher Pad',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_AirDomination2.id, PlayerResearch_BattleMomentum2.id],
	unitModifier: {
		[UnitChassis.chassisClass.bomber]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 1.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.fighter]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 1.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.launcherpad]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 1.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Carrier: TPlayerResearchDefinition = {
	id: 1341,
	caption: 'Chassis: Carrier',
	description: CarrierChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_Airforge.id,
		PlayerResearch_AirDomination2.id,
		PlayerResearch_BattleMomentum2.id,
	],
	technologyReward: {
		chassis: {
			[CarrierChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Bomber: TPlayerResearchDefinition = {
	id: 1342,
	caption: 'Chassis: Bomber',
	description: BomberChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Airforge.id, PlayerResearch_AirDomination2.id],
	technologyReward: {
		chassis: {
			[BomberChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Fighter: TPlayerResearchDefinition = {
	id: 1343,
	caption: 'Chassis: Fighter',
	description: FighterChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_Airforge.id, PlayerResearch_BattleMomentum2.id],
	technologyReward: {
		chassis: {
			[FighterChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Eagle: TPlayerResearchDefinition = {
	id: 1344,
	caption: 'Chassis: Eagle',
	description: EagleChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_StrikeAircraft.id,
		PlayerResearch_BattleMomentum2.id,
		PlayerResearch_AirDomination2.id,
	],
	technologyReward: {
		chassis: {
			[EagleChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_FlyingFox: TPlayerResearchDefinition = {
	id: 1350,
	caption: 'Chassis: Flying Fox',
	description: FlyingFoxChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 21,
	prerequisiteResearchIds: [PlayerResearch_BlueThunder.id, PlayerResearch_StrikeSupremacy1.id],
	technologyReward: {
		chassis: {
			[FlyingFoxChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Mauler: TPlayerResearchDefinition = {
	id: 1351,
	caption: 'Chassis: Mauler',
	description: MaulerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 21,
	prerequisiteResearchIds: [PlayerResearch_Bomber.id, PlayerResearch_StrikeSupremacy1.id],
	technologyReward: {
		chassis: {
			[MaulerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Trebuchet: TPlayerResearchDefinition = {
	id: 1352,
	caption: 'Chassis: Trebuchet',
	description: TrebuchetChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 21,
	prerequisiteResearchIds: [PlayerResearch_LauncherPad.id, PlayerResearch_StrikeSupremacy1.id],
	technologyReward: {
		chassis: {
			[TrebuchetChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Mantis: TPlayerResearchDefinition = {
	id: 1353,
	exclusiveFactions: [PoliticalFactions.TFactionID.aquarians],
	caption: 'Chassis: Mantis',
	description: MantisChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 21,
	prerequisiteResearchIds: [PlayerResearch_Fighter.id],
	technologyReward: {
		chassis: {
			[MantisChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_AirDomination3: TPlayerResearchDefinition = {
	id: 1360,
	caption: 'Air Domination III',
	description: '+5% Damage for Tiltjet, Hovertank and Strike Aircraft',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_AirDomination2.id, PlayerResearch_Airforge.id],
	unitModifier: {
		[UnitChassis.chassisClass.tiltjet]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.hovertank]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BattleMomentum3: TPlayerResearchDefinition = {
	id: 1361,
	caption: 'Battle Momentum III',
	description: '+10% Aerial Dodge for Support Aircraft, Tiltjet and Strike Aircraft',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Airforge.id, PlayerResearch_BattleMomentum2.id],
	unitModifier: {
		[UnitChassis.chassisClass.sup_aircraft]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.tiltjet]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Osprey: TPlayerResearchDefinition = {
	id: 1362,
	caption: 'Chassis: Osprey',
	description: OspreyChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_StrikeAircraft.id,
		PlayerResearch_BattleMomentum2.id,
		PlayerResearch_AirDomination2.id,
	],
	technologyReward: {
		chassis: {
			[OspreyChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Helipad: TPlayerResearchDefinition = {
	id: 1363,
	caption: 'Chassis: Helipad',
	description: HelipadChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_LauncherPad.id, PlayerResearch_Carrier.id],
	technologyReward: {
		chassis: {
			[HelipadChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Leviathan: TPlayerResearchDefinition = {
	id: 1370,
	caption: 'Chassis: Leviathan',
	description: LeviathanChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_Carrier.id, PlayerResearch_BattleMomentum3.id],
	technologyReward: {
		chassis: {
			[LeviathanChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_CloudOrca: TPlayerResearchDefinition = {
	id: 1371,
	caption: 'Chassis: Cloud Orca',
	description: CloudOrcaChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [PlayerResearch_Carrier.id, PlayerResearch_AirDomination3.id],
	technologyReward: {
		chassis: {
			[CloudOrcaChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_StrikeSupremacy2: TPlayerResearchDefinition = {
	id: 1373,
	caption: 'Strike Supremacy II',
	description: '+10% Hit Chance for Bomber, Fighter and Launcher Pad',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_AirDomination3.id,
		PlayerResearch_BattleMomentum3.id,
		PlayerResearch_StrikeSupremacy1.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.bomber]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 1.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.fighter]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 1.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.launcherpad]: {
			weapon: [
				{
					default: {
						baseAccuracy: {
							bias: 1.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Mirage: TPlayerResearchDefinition = {
	id: 1375,
	caption: 'Chassis: Mirage',
	description: MirageChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_Mauler.id,
		PlayerResearch_AirDomination3.id,
		PlayerResearch_BattleMomentum3.id,
	],
	technologyReward: {
		chassis: {
			[MirageChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Mangonel: TPlayerResearchDefinition = {
	id: 1376,
	caption: 'Chassis: Mangonel',
	description: MangonelChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_LauncherPad.id,
		PlayerResearch_BattleMomentum3.id,
		PlayerResearch_AirDomination3.id,
	],
	technologyReward: {
		chassis: {
			[MangonelChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Sparrow: TPlayerResearchDefinition = {
	id: 1377,
	caption: 'Chassis: Sparrow',
	description: SparrowChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_Osprey.id,
		PlayerResearch_BattleMomentum3.id,
		PlayerResearch_AirDomination3.id,
	],
	technologyReward: {
		chassis: {
			[SparrowChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_AerobaticsRepeatable: TPlayerResearchDefinition = {
	id: 1380,
	caption: `Aerobatics ${INFINITY_SYMBOL}`,
	repeatable: true,
	description: '+3 Hit Points for Support Aircraft, Tiltjet, Strike Aircraft, Fighter and Bomber',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_Mirage.id,
		PlayerResearch_Fighter.id,
		PlayerResearch_Sparrow.id,
		PlayerResearch_FlyingFox.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.sup_aircraft]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tiltjet]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.fighter]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.bomber]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_SchemedCollisionRepeatable: TPlayerResearchDefinition = {
	id: 1381,
	caption: `Schemed Collision ${INFINITY_SYMBOL}`,
	repeatable: true,
	description: '+2 Hit Points and +2% Dodge for Hovertank, Launcher Pad, Helipad and Carrier',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_Ramraider.id,
		PlayerResearch_Mangonel.id,
		PlayerResearch_Helipad.id,
		PlayerResearch_Carrier.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.hovertank]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.launcherpad]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.helipad]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.carrier]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.02,
						},
					},
					hitPoints: {
						bias: 2,
					},
				},
			],
		},
	},
};
export const PlayerResearch_DemolitionRepeatable: TPlayerResearchDefinition = {
	id: 1382,
	caption: `Demolition ${INFINITY_SYMBOL}`,
	repeatable: true,
	description: '+2% Damage for Hovertank, Strike Aircraft, Tiltjet, Helipad and Carrier',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_FlyingFox.id,
		PlayerResearch_Helipad.id,
		PlayerResearch_Ramraider.id,
		PlayerResearch_StrikeSupremacy2.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.hovertank]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.tiltjet]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.carrier]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.helipad]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Imposter: TPlayerResearchDefinition = {
	id: 1383,
	caption: 'Chassis: Support Aircraft',
	description: ImposterChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_SupportAircraft.id, PlayerResearch_StrikeSupremacy2.id],
	technologyReward: {
		chassis: {
			[ImposterChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Blackjack: TPlayerResearchDefinition = {
	id: 1384,
	caption: 'Chassis: Blackjack',
	description: BlackjackChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_Mirage.id, PlayerResearch_StrikeSupremacy2.id],
	technologyReward: {
		chassis: {
			[BlackjackChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Falcon: TPlayerResearchDefinition = {
	id: 1385,
	caption: 'Chassis: Falcon',
	description: FalconChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [PlayerResearch_Fighter.id, PlayerResearch_StrikeSupremacy2.id],
	technologyReward: {
		chassis: {
			[FalconChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_AerostaticMaterials: TPlayerResearchDefinition = {
	id: 1390,
	caption: 'Aerostatic Materials',
	description: `+0.2 Speed to all Units; Chassis: Helicon`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_Helipad.id,
		PlayerResearch_SchemedCollisionRepeatable.id,
		PlayerResearch_SchemedCollisionRepeatable.id,
		PlayerResearch_DemolitionRepeatable.id,
	],
	unitModifier: {
		default: {
			chassis: [
				{
					speed: {
						bias: 0.2,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[HeliconChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_SuborbitalNavigation: TPlayerResearchDefinition = {
	id: 1391,
	caption: 'Suborbital Navigation',
	description: `+1 Scan Range for Support Aircraft, Strike Aircraft and Hovertank; Equipment: Grounding Device`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_SchemedCollisionRepeatable.id,
		PlayerResearch_SchemedCollisionRepeatable.id,
		PlayerResearch_DemolitionRepeatable.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.sup_aircraft]: {
			chassis: [
				{
					scanRange: {
						bias: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.strike_aircraft]: {
			chassis: [
				{
					scanRange: {
						bias: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.hovertank]: {
			chassis: [
				{
					scanRange: {
						bias: 1,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[GroundingDeviceEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_AmplifiedVentilation: TPlayerResearchDefinition = {
	id: 1392,
	caption: 'Amplified Ventilation',
	description: `+10% Attack Rate for Bombs and Drone Weapons; Player Ability: Ventilation`,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_SchemedCollisionRepeatable.id,
		PlayerResearch_SchemedCollisionRepeatable.id,
		PlayerResearch_DemolitionRepeatable.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						baseRate: {
							factor: 1.1,
						},
					},
					[Weapons.TWeaponGroupType.Drone]: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	reward: (p) => p.learnAbility(VentilationAbility.id),
};

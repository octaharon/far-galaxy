import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import NavalEngineeringCorpus from '../../../../orbital/factories/all/NavalEngineeringCorpus.402';
import NaviforgeFactory from '../../../../orbital/factories/all/Navyforge.404';
import ShipyardFactory from '../../../../orbital/factories/all/Shipyard.401';
import SubmarinePool from '../../../../orbital/factories/all/SubmarinePool.403';
import WaterDockFactory from '../../../../orbital/factories/all/WaterDock.400';
import AlterationAbility from '../../../../tactical/abilities/all/Player/Alteration.60030';
import BattleshipChassis from '../../../../technologies/chassis/all/Battleship/Battleship.62000';
import DreadnoughtChassis from '../../../../technologies/chassis/all/Battleship/Dreadnought.62002';
import IroncladChassis from '../../../../technologies/chassis/all/Battleship/Ironclad.62001';
import CorvetteChassis from '../../../../technologies/chassis/all/Corvette/Corvette.60000';
import NeptuneChassis from '../../../../technologies/chassis/all/Corvette/Neptune.60002';
import NereidChassis from '../../../../technologies/chassis/all/Corvette/Nereid.60003';
import TritonChassis from '../../../../technologies/chassis/all/Corvette/Triton.60001';
import AtheneChassis from '../../../../technologies/chassis/all/Cruiser/Athene.65003';
import AuroraChassis from '../../../../technologies/chassis/all/Cruiser/Aurora.65002';
import CruiserChassis from '../../../../technologies/chassis/all/Cruiser/Cruiser.65000';
import PoseidonChassis from '../../../../technologies/chassis/all/Cruiser/Poseidon.65001';
import DestroyerChassis from '../../../../technologies/chassis/all/Destroyer/Destroyer.64000';
import HammerheadChassis from '../../../../technologies/chassis/all/Destroyer/Hammerhead.64002';
import SeamothChassis from '../../../../technologies/chassis/all/Destroyer/Seamoth.64001';
import AnglerChassis from '../../../../technologies/chassis/all/Submarine/Angler.66001';
import SawfishChassis from '../../../../technologies/chassis/all/Submarine/Sawfish.66002';
import SubmarineChassis from '../../../../technologies/chassis/all/Submarine/Submarine.66000';
import NettleChassis from '../../../../technologies/chassis/all/SupportShip/Nettle.61001';
import SupportShipChassis from '../../../../technologies/chassis/all/SupportShip/SupportShip.61000';
import MioplasmaticReflectorEquipment from '../../../../technologies/equipment/all/Enhancement/MioplasmaticReflector.20020';
import UnitChassis from '../../../../technologies/types/chassis';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

import { PlayerResearch_TrainingFacility } from './Bionics';
import { PlayerResearch_Motorplant } from './Mechanics';
import { PlayerResearch_RoboticsBay } from './Robotics';

export const PlayerResearch_WaterDock: TPlayerResearchDefinition = {
	id: 1900,
	caption: 'Factory: Water Dock',
	description: WaterDockFactory.description,
	prerequisiteResearchIds: [
		PlayerResearch_TrainingFacility.id,
		PlayerResearch_Motorplant.id,
		PlayerResearch_RoboticsBay.id,
	],
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		factories: {
			[WaterDockFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Corvette: TPlayerResearchDefinition = {
	id: 1905,
	caption: 'Chassis: Corvette',
	description: CorvetteChassis.description,
	prerequisiteResearchIds: [PlayerResearch_WaterDock.id],
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	technologyReward: {
		chassis: {
			[CorvetteChassis.id]: 1,
		},
	},
};
export const PlayerResearch_SupportShip: TPlayerResearchDefinition = {
	id: 1906,
	caption: 'Chassis: Support Ship',
	description: SupportShipChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_WaterDock.id],
	technologyReward: {
		chassis: {
			[SupportShipChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Destroyer: TPlayerResearchDefinition = {
	id: 1910,
	caption: 'Chassis: Destroyer',
	description: DestroyerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_Corvette.id],
	technologyReward: {
		chassis: {
			[DestroyerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Shipcraft1: TPlayerResearchDefinition = {
	id: 1911,
	caption: 'Shipcraft I',
	description: '+3 Hit Points for Corvette, Destroyer and Support Ship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_WaterDock.id],
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_SubdimensionalSonars1: TPlayerResearchDefinition = {
	id: 1912,
	caption: 'Subdimensional Sonars I',
	description: '+2% Base Energy Output; -1 Production Cost for Corvette, Destroyer and Support Ship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 3,
	prerequisiteResearchIds: [PlayerResearch_WaterDock.id],
	baseModifier: {
		energyOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Hammerhead: TPlayerResearchDefinition = {
	id: 1915,
	caption: 'Chassis: Hammerhead',
	description: HammerheadChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Destroyer.id, PlayerResearch_SubdimensionalSonars1.id],
	technologyReward: {
		chassis: {
			[HammerheadChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Shipyard: TPlayerResearchDefinition = {
	id: 1916,
	caption: 'Factory: Shipyard',
	description: ShipyardFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Shipcraft1.id, PlayerResearch_Destroyer.id],
	technologyReward: {
		factories: {
			[ShipyardFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_SubmarinePool: TPlayerResearchDefinition = {
	id: 1917,
	caption: 'Factory: Submarine Pool',
	description: SubmarinePool.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_SupportShip.id, PlayerResearch_Corvette.id],
	technologyReward: {
		factories: {
			[SubmarinePool.id]: 0.5,
		},
	},
};
export const PlayerResearch_Neptune: TPlayerResearchDefinition = {
	id: 1918,
	caption: 'Chassis: Neptune',
	description: NeptuneChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [PlayerResearch_Corvette.id, PlayerResearch_Shipcraft1.id],
	technologyReward: {
		chassis: {
			[NeptuneChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Shipcraft2: TPlayerResearchDefinition = {
	id: 1920,
	caption: 'Shipcraft II',
	description: '+3 Hit Points for Corvette, Destroyer and Support Ship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_Shipcraft1.id, PlayerResearch_Shipyard.id],
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_SubdimensionalSonars2: TPlayerResearchDefinition = {
	id: 1921,
	caption: 'Subdimensional Sonars II',
	description: '+2% Base Energy Output; -1 Production Cost for Corvette, Destroyer and Support Ship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_Shipyard.id, PlayerResearch_SubdimensionalSonars1.id],
	baseModifier: {
		energyOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Nereid: TPlayerResearchDefinition = {
	id: 1930,
	caption: 'Chassis: Nereid',
	description: NereidChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Shipcraft2.id, PlayerResearch_Neptune.id],
	technologyReward: {
		chassis: {
			[NereidChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Submarine: TPlayerResearchDefinition = {
	id: 1931,
	caption: 'Chassis: Submarine',
	description: SubmarineChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_SubmarinePool.id, PlayerResearch_SubdimensionalSonars2.id],
	technologyReward: {
		chassis: {
			[SubmarineChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Cruiser: TPlayerResearchDefinition = {
	id: 1932,
	caption: 'Chassis: Cruiser',
	description: CruiserChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Shipcraft2.id, PlayerResearch_Shipyard.id],
	technologyReward: {
		chassis: {
			[CruiserChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Seamoth: TPlayerResearchDefinition = {
	id: 1933,
	caption: 'Chassis: Seamoth',
	description: SeamothChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_Hammerhead.id, PlayerResearch_SubdimensionalSonars2.id],
	technologyReward: {
		chassis: {
			[SeamothChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_FleetCommando1: TPlayerResearchDefinition = {
	id: 1940,
	caption: 'Fleet Commando I',
	description: '+5% Damage for Submarine, Cruiser and Battleship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Shipcraft2.id, PlayerResearch_SubdimensionalSonars2.id],
	unitModifier: {
		[UnitChassis.chassisClass.submarine]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.cruiser]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.battleship]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Battleship: TPlayerResearchDefinition = {
	id: 1941,
	caption: 'Chassis: Battleship',
	description: BattleshipChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_Shipcraft2.id, PlayerResearch_SubdimensionalSonars2.id],
	technologyReward: {
		chassis: {
			[BattleshipChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Triton: TPlayerResearchDefinition = {
	id: 1950,
	caption: 'Chassis: Triton',
	description: TritonChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_FleetCommando1.id, PlayerResearch_Nereid.id],
	technologyReward: {
		chassis: {
			[TritonChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Poseidon: TPlayerResearchDefinition = {
	id: 1951,
	caption: 'Chassis: Poseidon',
	description: PoseidonChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_FleetCommando1.id, PlayerResearch_Cruiser.id],
	technologyReward: {
		chassis: {
			[PoseidonChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Angler: TPlayerResearchDefinition = {
	id: 1952,
	caption: 'Chassis: Angler',
	description: AnglerChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [PlayerResearch_FleetCommando1.id, PlayerResearch_Submarine.id],
	technologyReward: {
		chassis: {
			[AnglerChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Shipcraft3: TPlayerResearchDefinition = {
	id: 1955,
	caption: 'Shipcraft III',
	description: '+3 Hit Points for Corvette, Destroyer and Support Ship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_Shipcraft2.id],
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_SubdimensionalSonars3: TPlayerResearchDefinition = {
	id: 1956,
	caption: 'Subdimensional Sonars III',
	description: '+2% Base Energy Output; -1 Production Cost for Corvette, Destroyer and Support Ship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 19,
	prerequisiteResearchIds: [PlayerResearch_SubdimensionalSonars2.id],
	baseModifier: {
		energyOutput: {
			factor: 1.02,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					baseCost: {
						bias: -1,
						minGuard: 1,
						min: 1,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Aurora: TPlayerResearchDefinition = {
	id: 1960,
	caption: 'Chassis: Aurora',
	description: AuroraChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Poseidon.id, PlayerResearch_Shipcraft3.id],
	technologyReward: {
		chassis: {
			[AuroraChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Athene: TPlayerResearchDefinition = {
	id: 1961,
	caption: 'Chassis: Athene',
	description: AtheneChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Poseidon.id, PlayerResearch_SubdimensionalSonars3.id],
	technologyReward: {
		chassis: {
			[AtheneChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Sawfish: TPlayerResearchDefinition = {
	id: 1962,
	caption: 'Chassis: Sawfish',
	description: SawfishChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Angler.id, PlayerResearch_SubdimensionalSonars3.id],
	technologyReward: {
		chassis: {
			[SawfishChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Ironclad: TPlayerResearchDefinition = {
	id: 1963,
	caption: 'Chassis: Ironclad',
	description: IroncladChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_Battleship.id, PlayerResearch_Shipcraft3.id],
	technologyReward: {
		chassis: {
			[IroncladChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_NavalEngineeringCorpus: TPlayerResearchDefinition = {
	id: 1964,
	caption: 'Factory: Naval Engineering Corpus',
	description: NavalEngineeringCorpus.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_SubmarinePool.id, PlayerResearch_Shipyard.id],
	technologyReward: {
		factories: {
			[NavalEngineeringCorpus.id]: 0.5,
		},
	},
};
export const PlayerResearch_FleetCommando2: TPlayerResearchDefinition = {
	id: 1965,
	caption: 'Fleet Commando II',
	description: '+5% Damage for Submarine, Cruiser and Battleship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_FleetCommando1.id,
		PlayerResearch_Shipcraft3.id,
		PlayerResearch_SubdimensionalSonars3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.submarine]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.cruiser]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.battleship]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Dreadnought: TPlayerResearchDefinition = {
	id: 1970,
	caption: 'Chassis: Dreadnought',
	description: DreadnoughtChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_FleetCommando2.id, PlayerResearch_Battleship.id],
	technologyReward: {
		chassis: {
			[DreadnoughtChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_Navyforge: TPlayerResearchDefinition = {
	id: 1971,
	caption: 'Factory: Navyforge',
	description: NaviforgeFactory.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_NavalEngineeringCorpus.id, PlayerResearch_FleetCommando2.id],
	technologyReward: {
		factories: {
			[NaviforgeFactory.id]: 0.5,
		},
	},
};
export const PlayerResearch_Nettle: TPlayerResearchDefinition = {
	id: 1972,
	caption: 'Chassis: Nettle',
	description: NettleChassis.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_SupportShip.id, PlayerResearch_FleetCommando2.id],
	technologyReward: {
		chassis: {
			[NettleChassis.id]: 0.5,
		},
	},
};
export const PlayerResearch_SeafaringRepeatable: TPlayerResearchDefinition = {
	id: 1980,
	repeatable: true,
	caption: `Seafaring ${INFINITY_SYMBOL}`,
	description: '+3 Hit Points For Corvette, Destroyer, Support Ship, Submarine, Cruiser and Battleship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_Navyforge.id,
		PlayerResearch_Angler.id,
		PlayerResearch_Ironclad.id,
		PlayerResearch_Poseidon.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.submarine]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.cruiser]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
		[UnitChassis.chassisClass.battleship]: {
			chassis: [
				{
					hitPoints: {
						bias: 3,
					},
				},
			],
		},
	},
};
export const PlayerResearch_QuantumFloatingRepeatable: TPlayerResearchDefinition = {
	id: 1981,
	repeatable: true,
	caption: `Quantum Floating ${INFINITY_SYMBOL}`,
	description: '+0.2 Speed For Corvette, Destroyer, Support Ship, Submarine, Cruiser and Battleship',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 43,
	prerequisiteResearchIds: [
		PlayerResearch_Navyforge.id,
		PlayerResearch_Angler.id,
		PlayerResearch_Ironclad.id,
		PlayerResearch_Poseidon.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					speed: {
						bias: 0.2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					speed: {
						bias: 0.2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.submarine]: {
			chassis: [
				{
					speed: {
						bias: 0.2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					speed: {
						bias: 0.2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.cruiser]: {
			chassis: [
				{
					speed: {
						bias: 0.2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.battleship]: {
			chassis: [
				{
					speed: {
						bias: 0.2,
					},
				},
			],
		},
	},
};
export const PlayerResearch_Alteration: TPlayerResearchDefinition = {
	id: 1990,
	caption: 'Tension Alteration',
	description: '+5% Dodge for Destroyer, Cruiser and Support Ship; Ability: Alteration',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 47,
	prerequisiteResearchIds: [PlayerResearch_SeafaringRepeatable.id, PlayerResearch_QuantumFloatingRepeatable.id],
	reward: (p) => p.learnAbility(AlterationAbility.id),
	unitModifier: {
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.destroyer]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.cruiser]: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MioplasmaCoating: TPlayerResearchDefinition = {
	id: 1991,
	caption: 'Mioplasma Coating',
	description: '+5% Armor Protection for Submarine, Corvette and Battleship; Equipment: Mioplasmatic Reflector',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 47,
	prerequisiteResearchIds: [PlayerResearch_SeafaringRepeatable.id, PlayerResearch_QuantumFloatingRepeatable.id],
	technologyReward: {
		equipment: {
			[MioplasmaticReflectorEquipment.id]: 0.5,
		},
	},
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			armor: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
		[UnitChassis.chassisClass.submarine]: {
			armor: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
		[UnitChassis.chassisClass.battleship]: {
			armor: [
				{
					default: {
						factor: 0.95,
					},
				},
			],
		},
	},
};
export const PlayerResearch_SpontaneousGluonification: TPlayerResearchDefinition = {
	id: 1992,
	caption: 'Spontaneous Gluonification',
	description: '+0.25 Unit Ability Power and +1 Ability Strength',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 47,
	prerequisiteResearchIds: [PlayerResearch_SeafaringRepeatable.id, PlayerResearch_QuantumFloatingRepeatable.id],
	unitModifier: {
		default: {
			unitModifier: {
				abilityPowerModifier: [
					{
						bias: 0.25,
					},
				],
				abilityDurationModifier: [
					{
						bias: 1,
					},
				],
			},
		},
	},
};

import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import PowerRecirculatorEquipment from '../../../../technologies/equipment/all/Enhancement/PowerRecirculator.20016';
import SimulationMatrixEquipment from '../../../../technologies/equipment/all/Tactical/SimulationMatrix.20005';
import OlympiusReactorEquipment from '../../../../technologies/equipment/all/Warfare/OlympiusReactor.20025';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AAMissile from '../../../../technologies/weapons/all/antiair/AAMissile.70001';
import SAMDefence from '../../../../technologies/weapons/all/antiair/SAMDefence.70002';
import BlackWidowMissile from '../../../../technologies/weapons/all/guided/BlackWidow.35003';
import GlidingMissile from '../../../../technologies/weapons/all/guided/GlidingMissile.35005';
import GoliathMissile from '../../../../technologies/weapons/all/guided/Goliath.35004';
import HammerfallMissile from '../../../../technologies/weapons/all/guided/Hammerfall.35000';
import PhoenixMissile from '../../../../technologies/weapons/all/guided/PhoenixMissile.35006';
import SeekerMissile from '../../../../technologies/weapons/all/guided/SeekerMissile.35001';
import SerpentMissile from '../../../../technologies/weapons/all/guided/SerpentMissile.35002';
import AntimatterMissile from '../../../../technologies/weapons/all/missiles/AntimatterMissile.30002';
import CorrosiveMissile from '../../../../technologies/weapons/all/missiles/CorrosiveMissile.30001';
import JerichoMissile from '../../../../technologies/weapons/all/missiles/Jericho.30005';
import NapalmMissile from '../../../../technologies/weapons/all/missiles/NapalmMissile.30007';
import NeutronMissile from '../../../../technologies/weapons/all/missiles/NeutronMissile.30009';
import ProtonMissile from '../../../../technologies/weapons/all/missiles/ProtonMissile.30004';
import SmallMissile from '../../../../technologies/weapons/all/missiles/SmallMissile.30000';
import SwarmMissile from '../../../../technologies/weapons/all/missiles/SwarmMissile.30006';
import TacticalMissile from '../../../../technologies/weapons/all/missiles/TacticalMissile.30003';
import HellfireRocket from '../../../../technologies/weapons/all/rockets/Hellfire.38001';
import RocketArtillery from '../../../../technologies/weapons/all/rockets/RocketArtillery.38000';
import SS18Rocket from '../../../../technologies/weapons/all/rockets/SS-18.38002';
import TiamatRocket from '../../../../technologies/weapons/all/rockets/Tiamat.38003';
import WarpRocket from '../../../../technologies/weapons/all/rockets/WarpArtillery.38004';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_SmallMissile: TPlayerResearchDefinition = {
	id: 100,
	caption: 'Weapon: Small Missile',
	description: SmallMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		weapons: {
			[SmallMissile.id]: 1,
		},
	},
};
export const PlayerResearch_SeekerMissile: TPlayerResearchDefinition = {
	id: 110,
	caption: 'Weapon: Seeker Missile',
	description: SeekerMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_SmallMissile.id],
	technologyReward: {
		weapons: {
			[SeekerMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_CorrosiveMissile: TPlayerResearchDefinition = {
	id: 111,
	caption: 'Weapon: Corrosive Missile',
	description: CorrosiveMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_SmallMissile.id],
	technologyReward: {
		weapons: {
			[CorrosiveMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_NapalmMissile: TPlayerResearchDefinition = {
	id: 112,
	caption: 'Weapon: Napalm Missile',
	description: NapalmMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 1.5,
	prerequisiteResearchIds: [PlayerResearch_SmallMissile.id],
	technologyReward: {
		weapons: {
			[NapalmMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_MissilePrecision1: TPlayerResearchDefinition = {
	id: 113,
	caption: 'Missile Precision I',
	description: '+5% Hit Chance for Missiles',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_SmallMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissileRange1: TPlayerResearchDefinition = {
	id: 114,
	caption: 'Missile Range I',
	description: '+0.25 Range for Missiles',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_SmallMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissileDamage1: TPlayerResearchDefinition = {
	id: 115,
	caption: 'Missile Damage I',
	description: '+3% Damage for Missiles',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_SmallMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketArtillery: TPlayerResearchDefinition = {
	id: 120,
	caption: 'Weapon: Rocket Artillery',
	description: RocketArtillery.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamage1.id,
		PlayerResearch_MissileRange1.id,
		PlayerResearch_MissilePrecision1.id,
	],
	technologyReward: {
		weapons: {
			[RocketArtillery.id]: 0.5,
		},
	},
};
export const PlayerResearch_SerpentMissile: TPlayerResearchDefinition = {
	id: 121,
	caption: 'Weapon: S.E.R.P.E.N.T.',
	description: SerpentMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [
		PlayerResearch_SeekerMissile.id,
		PlayerResearch_CorrosiveMissile.id,
		PlayerResearch_NapalmMissile.id,
	],
	technologyReward: {
		weapons: {
			[SerpentMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_TacticalMissile: TPlayerResearchDefinition = {
	id: 122,
	caption: 'Weapon: Tactical Missile',
	description: TacticalMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_MissileDamage1.id, PlayerResearch_SeekerMissile.id],
	technologyReward: {
		weapons: {
			[TacticalMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_MissileDamage2: TPlayerResearchDefinition = {
	id: 123,
	caption: 'Missile Damage II',
	description: '+3% Missiles Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_MissileDamage1.id, PlayerResearch_NapalmMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissileRange2: TPlayerResearchDefinition = {
	id: 124,
	caption: 'Missile Range II',
	description: '+0.25 Missiles Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_MissileRange1.id, PlayerResearch_SeekerMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissilePrecision2: TPlayerResearchDefinition = {
	id: 125,
	caption: 'Missile Precision II',
	description: '+5% Hit Chance for Missiles',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_MissilePrecision1.id, PlayerResearch_CorrosiveMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruiseDamage1: TPlayerResearchDefinition = {
	id: 126,
	caption: 'Cruise Missiles Damage I',
	description: '+3% Cruise Missiles Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_MissileDamage1.id, PlayerResearch_SeekerMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruiseRange1: TPlayerResearchDefinition = {
	id: 127,
	caption: 'Cruise Missiles Range I',
	description: '+0.25 Cruise Missiles Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_MissileRange1.id, PlayerResearch_SeekerMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruisePrecision1: TPlayerResearchDefinition = {
	id: 128,
	caption: 'Cruise Missiles Precision I',
	description: '+10% Hit Chance for Cruise Missiles',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_MissilePrecision1.id, PlayerResearch_SeekerMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseAccuracy: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketDamage1: TPlayerResearchDefinition = {
	id: 130,
	caption: 'Rockets Damage I',
	description: '+5% Rockets Missiles Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CruiseDamage1.id, PlayerResearch_RocketArtillery.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketRange1: TPlayerResearchDefinition = {
	id: 131,
	caption: 'Rockets Range I',
	description: '+0.25 Rockets Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CruiseRange1.id, PlayerResearch_RocketArtillery.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketPrecision1: TPlayerResearchDefinition = {
	id: 132,
	caption: 'Rockets Precision  I',
	description: '+10% Rockets Hit Chance',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CruisePrecision1.id, PlayerResearch_RocketArtillery.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						baseAccuracy: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruiseDamage2: TPlayerResearchDefinition = {
	id: 133,
	caption: 'Cruise Missiles Damage II',
	description: '+3% Cruise Missiles Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CruiseDamage1.id, PlayerResearch_SerpentMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruisePrecision2: TPlayerResearchDefinition = {
	id: 134,
	caption: 'Cruise Missiles Precision II',
	description: '+10% Cruise Missiles Hit Chance',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CruisePrecision1.id, PlayerResearch_SerpentMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						baseAccuracy: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruiseRange2: TPlayerResearchDefinition = {
	id: 135,
	caption: 'Cruise Missiles Range II',
	description: '+0.25 Cruise Missiles Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CruiseRange1.id, PlayerResearch_SerpentMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BlackWidow: TPlayerResearchDefinition = {
	id: 140,
	caption: 'Weapon: Black Widow',
	description: BlackWidowMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_CruiseDamage1.id,
		PlayerResearch_CruiseRange1.id,
		PlayerResearch_CruisePrecision1.id,
	],
	technologyReward: {
		weapons: {
			[BlackWidowMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_Hammerfall: TPlayerResearchDefinition = {
	id: 141,
	caption: 'Weapon: Hammerfall',
	description: HammerfallMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_CruiseDamage2.id,
		PlayerResearch_CruiseRange2.id,
		PlayerResearch_CruisePrecision2.id,
	],
	technologyReward: {
		weapons: {
			[HammerfallMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_NeutronMissile: TPlayerResearchDefinition = {
	id: 142,
	caption: 'Weapon: Neutron Missile',
	description: NeutronMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamage1.id,
		PlayerResearch_MissileRange1.id,
		PlayerResearch_MissilePrecision1.id,
	],
	technologyReward: {
		weapons: {
			[NeutronMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_Hellfire: TPlayerResearchDefinition = {
	id: 143,
	caption: 'Weapon: Hellfire Rocket',
	description: HellfireRocket.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_RocketDamage1.id,
		PlayerResearch_RocketPrecision1.id,
		PlayerResearch_RocketRange1.id,
	],
	technologyReward: {
		weapons: {
			[HellfireRocket.id]: 0.5,
		},
	},
};
export const PlayerResearch_AAMissile: TPlayerResearchDefinition = {
	id: 144,
	caption: 'Weapon: AA Missile',
	description: AAMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamage2.id,
		PlayerResearch_MissileRange2.id,
		PlayerResearch_MissilePrecision2.id,
	],
	technologyReward: {
		weapons: {
			[AAMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_RocketDamage2: TPlayerResearchDefinition = {
	id: 150,
	caption: 'Rockets Damage II',
	description: '+5% Rockets Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_RocketDamage1.id, PlayerResearch_RocketArtillery.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketRange2: TPlayerResearchDefinition = {
	id: 151,
	caption: 'Rockets Range II',
	description: '+0.25 Rockets Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_RocketRange1.id, PlayerResearch_RocketArtillery.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketPrecision2: TPlayerResearchDefinition = {
	id: 152,
	caption: 'Rockets Precision II',
	description: '+10% Rockets Hit Chance',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_RocketPrecision1.id, PlayerResearch_RocketArtillery.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						baseAccuracy: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissileDamage3: TPlayerResearchDefinition = {
	id: 153,
	caption: 'Missile Damage III',
	description: '+3% Missiles Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_MissileDamage2.id, PlayerResearch_NeutronMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissileRange3: TPlayerResearchDefinition = {
	id: 154,
	caption: 'Missile Range III',
	description: '+0.25 Missiles Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_MissileRange2.id, PlayerResearch_NeutronMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissilePrecision3: TPlayerResearchDefinition = {
	id: 155,
	caption: 'Missile Precision III',
	description: '+5% Hit Chance for Missiles',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_MissilePrecision2.id, PlayerResearch_NeutronMissile.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruiseDamage3: TPlayerResearchDefinition = {
	id: 156,
	caption: 'Cruise Missiles Damage III',
	description: '+3% Cruise Missiles Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_CruiseDamage2.id, PlayerResearch_BlackWidow.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						damageModifier: {
							factor: 1.03,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruisePrecision3: TPlayerResearchDefinition = {
	id: 157,
	caption: 'Cruise Missiles Precision III',
	description: '+10% Cruise Missiles Hit Chance',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_CruisePrecision2.id, PlayerResearch_BlackWidow.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						baseAccuracy: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruiseRange3: TPlayerResearchDefinition = {
	id: 158,
	caption: 'Cruise Missiles Range III',
	description: '+0.25 Cruise Missiles Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [PlayerResearch_CruiseRange2.id, PlayerResearch_BlackWidow.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_SwarmMissiles: TPlayerResearchDefinition = {
	id: 160,
	caption: 'Weapon: Swarm Missiles',
	description: SwarmMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamage3.id,
		PlayerResearch_MissileRange3.id,
		PlayerResearch_MissilePrecision3.id,
	],
	technologyReward: {
		weapons: {
			[SwarmMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_PhoenixMissile: TPlayerResearchDefinition = {
	id: 161,
	caption: 'Weapon: Phoenix Missile',
	description: PhoenixMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_CruiseDamage3.id,
		PlayerResearch_CruiseRange3.id,
		PlayerResearch_CruisePrecision3.id,
	],
	technologyReward: {
		weapons: {
			[PhoenixMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_GlidingMissile: TPlayerResearchDefinition = {
	id: 162,
	caption: 'Weapon: Gliding Missile',
	description: GlidingMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_CruiseDamage3.id,
		PlayerResearch_CruiseRange3.id,
		PlayerResearch_CruisePrecision3.id,
	],
	technologyReward: {
		weapons: {
			[GlidingMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_ProtonMissile: TPlayerResearchDefinition = {
	id: 163,
	caption: 'Weapon: Proton Missile',
	description: ProtonMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamage3.id,
		PlayerResearch_MissileRange3.id,
		PlayerResearch_MissilePrecision3.id,
	],
	technologyReward: {
		weapons: {
			[ProtonMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_SS18: TPlayerResearchDefinition = {
	id: 164,
	caption: 'Weapon: SS-18',
	description: SS18Rocket.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_RocketDamage2.id,
		PlayerResearch_RocketPrecision2.id,
		PlayerResearch_RocketRange2.id,
	],
	technologyReward: {
		weapons: {
			[SS18Rocket.id]: 0.5,
		},
	},
};
export const PlayerResearch_RocketDamage3: TPlayerResearchDefinition = {
	id: 170,
	caption: 'Rockets Damage III',
	description: '+5% Rockets Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RocketDamage2.id, PlayerResearch_SS18.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketRange3: TPlayerResearchDefinition = {
	id: 171,
	caption: 'Rockets Range III',
	description: '+0.25 Rockets Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RocketRange2.id, PlayerResearch_SS18.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						baseRange: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketPrecision3: TPlayerResearchDefinition = {
	id: 172,
	caption: 'Rockets Precision III',
	description: '+10% Rockets Hit Chance',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_RocketPrecision2.id, PlayerResearch_SS18.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						baseAccuracy: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_Tiamat: TPlayerResearchDefinition = {
	id: 180,
	caption: 'Weapon: Tiamat',
	description: TiamatRocket.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_RocketDamage3.id,
		PlayerResearch_RocketPrecision3.id,
		PlayerResearch_CruisePrecision3.id,
		PlayerResearch_Hellfire.id,
	],
	technologyReward: {
		weapons: {
			[TiamatRocket.id]: 0.5,
		},
	},
};
export const PlayerResearch_WarpArtillery: TPlayerResearchDefinition = {
	id: 181,
	caption: 'Weapon: Warp Artillery',
	description: WarpRocket.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 17,
	prerequisiteResearchIds: [
		PlayerResearch_RocketRange3.id,
		PlayerResearch_CruiseRange3.id,
		PlayerResearch_RocketDamage3.id,
		PlayerResearch_Hellfire.id,
	],
	technologyReward: {
		weapons: {
			[WarpRocket.id]: 0.5,
		},
	},
};
export const PlayerResearch_AntimatterMissile: TPlayerResearchDefinition = {
	id: 182,
	caption: 'Weapon: Antimatter Missile',
	description: AntimatterMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_NeutronMissile.id,
		PlayerResearch_ProtonMissile.id,
		PlayerResearch_CruiseDamage3.id,
		PlayerResearch_Hellfire.id,
	],
	technologyReward: {
		weapons: {
			[AntimatterMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_SAMDefense: TPlayerResearchDefinition = {
	id: 183,
	caption: 'Weapon: SAM Defense',
	description: SAMDefence.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_RocketRange3.id,
		PlayerResearch_CruisePrecision3.id,
		PlayerResearch_AAMissile.id,
	],
	technologyReward: {
		weapons: {
			[SAMDefence.id]: 0.5,
		},
	},
};
export const PlayerResearch_GoliathMissile: TPlayerResearchDefinition = {
	id: 184,
	caption: 'Weapon: Goliath Missile',
	description: GoliathMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_Hammerfall.id,
		PlayerResearch_RocketPrecision3.id,
		PlayerResearch_PhoenixMissile.id,
	],
	technologyReward: {
		weapons: {
			[GoliathMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_Jericho: TPlayerResearchDefinition = {
	id: 185,
	caption: 'Weapon: Jericho',
	description: JerichoMissile.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_Hammerfall.id,
		PlayerResearch_BlackWidow.id,
		PlayerResearch_CruiseRange3.id,
	],
	technologyReward: {
		weapons: {
			[JerichoMissile.id]: 0.5,
		},
	},
};
export const PlayerResearch_MissileDamageRepeatable: TPlayerResearchDefinition = {
	id: 190,
	caption: `Rocketry Damage ${INFINITY_SYMBOL}`,
	repeatable: true,
	description: '+1% Rockets, Cruise Missiles and Homing Missiles Damage',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_RocketDamage3.id,
		PlayerResearch_CruiseDamage3.id,
		PlayerResearch_MissileDamage3.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						damageModifier: {
							factor: 1.01,
						},
					},
					[Weapons.TWeaponGroupType.Missiles]: {
						damageModifier: {
							factor: 1.01,
						},
					},
					[Weapons.TWeaponGroupType.Missiles]: {
						damageModifier: {
							factor: 1.01,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissileHitChanceRepeatable: TPlayerResearchDefinition = {
	id: 191,
	caption: `Rocketry Precision ${INFINITY_SYMBOL}`,
	description: '+2% Rockets, Cruise Missiles and Homing Missiles Hit Chance',
	repeatable: true,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_RocketPrecision3.id,
		PlayerResearch_CruisePrecision3.id,
		PlayerResearch_MissilePrecision3.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						baseAccuracy: {
							base: 0.02,
						},
					},
					[Weapons.TWeaponGroupType.Missiles]: {
						baseAccuracy: {
							base: 0.02,
						},
					},
					[Weapons.TWeaponGroupType.Missiles]: {
						baseAccuracy: {
							base: 0.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissileRangeRepeatable: TPlayerResearchDefinition = {
	id: 192,
	caption: `Rocketry Range ${INFINITY_SYMBOL}`,
	description: '+2% Rockets, Cruise Missiles and Homing Missiles Range',
	repeatable: true,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [
		PlayerResearch_RocketRange3.id,
		PlayerResearch_CruiseRange3.id,
		PlayerResearch_MissileRange3.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						baseRange: {
							factor: 1.02,
						},
					},
					[Weapons.TWeaponGroupType.Missiles]: {
						baseRange: {
							base: 1.02,
						},
					},
					[Weapons.TWeaponGroupType.Missiles]: {
						baseRange: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_MissileSpeed: TPlayerResearchDefinition = {
	id: 193,
	caption: `Zero Element Fuel`,
	description: 'Missiles have Highest Priority and cost 3 Raw Materials less (down to a minimum of 3)',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamageRepeatable.id,
		PlayerResearch_MissileHitChanceRepeatable.id,
		PlayerResearch_MissileRangeRepeatable.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseCost: {
							bias: -3,
							min: 3,
							minGuard: 3,
						},
						priority: {
							min: ATTACK_PRIORITIES.HIGHEST,
							max: ATTACK_PRIORITIES.HIGHEST,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_RocketRadius: TPlayerResearchDefinition = {
	id: 194,
	caption: `Warped Warheads`,
	description: '+0.25 Rockets AOE Radius',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamageRepeatable.id,
		PlayerResearch_MissileHitChanceRepeatable.id,
		PlayerResearch_MissileRangeRepeatable.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						aoeRadiusModifier: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_CruiseRate: TPlayerResearchDefinition = {
	id: 195,
	caption: `Quantum Satchels`,
	description: '+15% Cruise Missiles Fire Rate',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamageRepeatable.id,
		PlayerResearch_MissileHitChanceRepeatable.id,
		PlayerResearch_MissileRangeRepeatable.id,
	],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						baseRate: {
							factor: 1.15,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_PropulsionGateways: TPlayerResearchDefinition = {
	id: 196,
	caption: `Propulsion Gateways`,
	description: '+5% Dodge for Combat Drone, Support Vehicle and Rover. Equipment: Olympius Reactor',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_MissileDamageRepeatable.id,
		PlayerResearch_RocketPrecision3.id,
		PlayerResearch_CruiseRange3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.sup_vehicle]: {
			chassis: [
				{
					dodge: {
						bias: 0.05,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					dodge: {
						bias: 0.05,
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					dodge: {
						bias: 0.05,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[OlympiusReactorEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_ProtonAfterburners: TPlayerResearchDefinition = {
	id: 197,
	caption: `Proton Afterburners`,
	description: '+0.5 Speed for Tiltjet, Bomber and Fighter. Equipment: Power Recirculator',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_MissileRangeRepeatable.id,
		PlayerResearch_MissilePrecision3.id,
		PlayerResearch_CruiseDamage3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.tiltjet]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.fighter]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.bomber]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[PowerRecirculatorEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_SimulationMatrix: TPlayerResearchDefinition = {
	id: 198,
	caption: `Simulation Matrix`,
	description: '+10% Artillery, Launcher Pad and Helipad Attack Rate. Equipment: Simulation Matrix',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_MissileHitChanceRepeatable.id,
		PlayerResearch_MissileRange3.id,
		PlayerResearch_RocketDamage3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.artillery]: {
			weapon: [
				{
					default: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.launcherpad]: {
			weapon: [
				{
					default: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
		[UnitChassis.chassisClass.helipad]: {
			weapon: [
				{
					default: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[SimulationMatrixEquipment.id]: 0.5,
		},
	},
};

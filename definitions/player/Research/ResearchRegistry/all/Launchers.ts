import { INFINITY_SYMBOL } from '../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../tactical/damage/attack';
import VoidTransporterEquipment from '../../../../technologies/equipment/all/Enhancement/VoidTransporter.20018';
import TensorCoprocessorEquipment from '../../../../technologies/equipment/all/Support/TensorCoprocessor.20024';
import SuicideDeviceEquipment from '../../../../technologies/equipment/all/Warfare/SuicideDevice.30003';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AntimatterBomb from '../../../../technologies/weapons/all/bombs/AntimatterBomb.40007';
import BlackHoleBomb from '../../../../technologies/weapons/all/bombs/BlackHoleBomb.40002';
import ClusterBomb from '../../../../technologies/weapons/all/bombs/ClusterBomb.40004';
import ConventionalBomb from '../../../../technologies/weapons/all/bombs/ConventionalBomb.40000';
import LaserGuidedBomb from '../../../../technologies/weapons/all/bombs/LaserGuidedBomb.40006';
import NailBomb from '../../../../technologies/weapons/all/bombs/NailBomb.40009';
import NapalmBomb from '../../../../technologies/weapons/all/bombs/NapalmBomb.40003';
import NuclearBomb from '../../../../technologies/weapons/all/bombs/NuclearBomb.40001';
import ReaperBomb from '../../../../technologies/weapons/all/bombs/ReaperBomb.40008';
import ToadBomb from '../../../../technologies/weapons/all/bombs/ToadMine.40005';
import BreachGun from '../../../../technologies/weapons/all/projectiles/BreachGun.15004';
import CorrosiveMines from '../../../../technologies/weapons/all/projectiles/CorrosiveMines.15002';
import FlakeCannon from '../../../../technologies/weapons/all/projectiles/FlakeCannon.15000';
import GravityMines from '../../../../technologies/weapons/all/projectiles/GravityMines.15003';
import GravMortarWeapon from '../../../../technologies/weapons/all/projectiles/GravMortar.15001';
import PoisonMines from '../../../../technologies/weapons/all/projectiles/PoisonMines.15005';
import PoliticalFactions from '../../../../world/factions';
import { DEFAULT_PLAYER_RESEARCH_COST } from '../../../constants';
import { TPlayerResearchDefinition } from '../../types';

export const PlayerResearch_FlakeCannon: TPlayerResearchDefinition = {
	id: 500,
	caption: 'Weapon: Flake Cannon',
	description: FlakeCannon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST,
	technologyReward: {
		weapons: {
			[FlakeCannon.id]: 1,
		},
	},
};
export const PlayerResearch_CorrosiveMines: TPlayerResearchDefinition = {
	id: 505,
	caption: 'Weapon: Corrosive Mines',
	description: CorrosiveMines.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 2,
	prerequisiteResearchIds: [PlayerResearch_FlakeCannon.id],
	technologyReward: {
		weapons: {
			[CorrosiveMines.id]: 0.5,
		},
	},
};
export const PlayerResearch_LaunchersRange1: TPlayerResearchDefinition = {
	id: 510,
	caption: 'Launcher Range I',
	description: '+0.1 Range for Launchers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_FlakeCannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						baseRange: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersArea1: TPlayerResearchDefinition = {
	id: 511,
	caption: 'Launchers Splash I',
	description: '+0.15 Launchers AOE Radius',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_FlakeCannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						aoeRadiusModifier: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersDamage1: TPlayerResearchDefinition = {
	id: 512,
	caption: 'Launchers Damage I',
	description: '+5% Damage for Launchers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_FlakeCannon.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_PoisonMines: TPlayerResearchDefinition = {
	id: 515,
	caption: 'Weapon: Poison Mines',
	description: PoisonMines.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [
		PlayerResearch_CorrosiveMines.id,
		PlayerResearch_LaunchersArea1.id,
		PlayerResearch_LaunchersDamage1.id,
		PlayerResearch_LaunchersRange1.id,
	],
	technologyReward: {
		weapons: {
			[PoisonMines.id]: 0.5,
		},
	},
};
export const PlayerResearch_ConventionalBomb: TPlayerResearchDefinition = {
	id: 516,
	caption: 'Weapon: Conventional Bomb',
	description: ConventionalBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 5,
	prerequisiteResearchIds: [
		PlayerResearch_CorrosiveMines.id,
		PlayerResearch_LaunchersRange1.id,
		PlayerResearch_LaunchersDamage1.id,
		PlayerResearch_LaunchersArea1.id,
	],
	technologyReward: {
		weapons: {
			[ConventionalBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_LaunchersRange2: TPlayerResearchDefinition = {
	id: 520,
	caption: 'Launcher Range II',
	description: '+0.1 Range for Launchers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CorrosiveMines.id, PlayerResearch_LaunchersRange1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						baseRange: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersArea2: TPlayerResearchDefinition = {
	id: 521,
	caption: 'Launchers Splash II',
	description: '+0.15 Launchers AOE Radius',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CorrosiveMines.id, PlayerResearch_LaunchersArea1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						aoeRadiusModifier: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersDamage2: TPlayerResearchDefinition = {
	id: 522,
	caption: 'Launchers Damage II',
	description: '+5% Damage for Launchers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 7,
	prerequisiteResearchIds: [PlayerResearch_CorrosiveMines.id, PlayerResearch_LaunchersDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_GravMortar: TPlayerResearchDefinition = {
	id: 530,
	caption: 'Weapon: Grav Mortar',
	description: GravMortarWeapon.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_PoisonMines.id,
		PlayerResearch_LaunchersRange2.id,
		PlayerResearch_LaunchersDamage2.id,
		PlayerResearch_LaunchersArea2.id,
	],
	technologyReward: {
		weapons: {
			[GravMortarWeapon.id]: 0.5,
		},
	},
};
export const PlayerResearch_IncendiaryBomb: TPlayerResearchDefinition = {
	id: 531,
	caption: 'Weapon: Incendiary Bomb',
	description: NapalmBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_ConventionalBomb.id,
		PlayerResearch_LaunchersRange2.id,
		PlayerResearch_LaunchersDamage2.id,
		PlayerResearch_LaunchersArea2.id,
	],
	technologyReward: {
		weapons: {
			[NapalmBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_BreachGun: TPlayerResearchDefinition = {
	id: 532,
	exclusiveFactions: [PoliticalFactions.TFactionID.pirates],
	caption: 'Weapon: Breach Gun',
	description: BreachGun.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 11,
	prerequisiteResearchIds: [
		PlayerResearch_PoisonMines.id,
		PlayerResearch_LaunchersRange2.id,
		PlayerResearch_LaunchersDamage2.id,
		PlayerResearch_LaunchersArea2.id,
	],
	technologyReward: {
		weapons: {
			[BreachGun.id]: 0.5,
		},
	},
};
export const PlayerResearch_BombsRate1: TPlayerResearchDefinition = {
	id: 535,
	caption: 'Bombs Rate I',
	description: '+0.05 Attack Rate for Bombs',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_ConventionalBomb.id, PlayerResearch_LaunchersRange2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BombsArea1: TPlayerResearchDefinition = {
	id: 536,
	caption: 'Bombs Splash I',
	description: '+0.1 Bombs AOE Radius',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 13,
	prerequisiteResearchIds: [PlayerResearch_ConventionalBomb.id, PlayerResearch_LaunchersArea2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						aoeRadiusModifier: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BombsDamage1: TPlayerResearchDefinition = {
	id: 537,
	caption: 'Bombs Damage I',
	description: '+10% Damage for Bombs',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 4,
	prerequisiteResearchIds: [PlayerResearch_ConventionalBomb.id, PlayerResearch_LaunchersDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						damageModifier: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersRange3: TPlayerResearchDefinition = {
	id: 540,
	caption: 'Launchers Range III',
	description: '+0.1 Range for Launchers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_PoisonMines.id, PlayerResearch_LaunchersRange2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						baseRange: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersArea3: TPlayerResearchDefinition = {
	id: 541,
	caption: 'Launchers Splash III',
	description: '+0.15 Launchers AOE Radius',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_PoisonMines.id, PlayerResearch_LaunchersArea2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						aoeRadiusModifier: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersDamage3: TPlayerResearchDefinition = {
	id: 542,
	caption: 'Launchers Damage III',
	description: '+5% Damage for Launchers',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 23,
	prerequisiteResearchIds: [PlayerResearch_PoisonMines.id, PlayerResearch_LaunchersDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_NailBomb: TPlayerResearchDefinition = {
	id: 545,
	caption: 'Weapon: Nail Bomb',
	description: NailBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_IncendiaryBomb.id, PlayerResearch_BombsArea1.id],
	technologyReward: {
		weapons: {
			[NailBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_ClusterBomb: TPlayerResearchDefinition = {
	id: 546,
	caption: 'Weapon: Cluster Bomb',
	description: ClusterBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_IncendiaryBomb.id, PlayerResearch_BombsDamage1.id],
	technologyReward: {
		weapons: {
			[ClusterBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_LaserGuidedBomb: TPlayerResearchDefinition = {
	id: 547,
	caption: 'Weapon: Laser Guided Bomb',
	description: LaserGuidedBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_IncendiaryBomb.id, PlayerResearch_BombsRate1.id],
	technologyReward: {
		weapons: {
			[LaserGuidedBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_GravityMines: TPlayerResearchDefinition = {
	id: 550,
	caption: 'Weapon: Gravity Mines',
	description: GravityMines.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_LaunchersDamage3.id,
		PlayerResearch_LaunchersRange3.id,
		PlayerResearch_LaunchersArea3.id,
	],
	technologyReward: {
		weapons: {
			[GravityMines.id]: 0.5,
		},
	},
};
export const PlayerResearch_ReaperBomb: TPlayerResearchDefinition = {
	id: 551,
	caption: 'Weapon: Reaper Bomb',
	description: ReaperBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [
		PlayerResearch_IncendiaryBomb.id,
		PlayerResearch_BombsDamage1.id,
		PlayerResearch_BombsRate1.id,
		PlayerResearch_BombsArea1.id,
	],
	technologyReward: {
		weapons: {
			[ReaperBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_BombsRate2: TPlayerResearchDefinition = {
	id: 556,
	caption: 'Bombs Rate II',
	description: '+0.05 Attack Rate for Bombs',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_IncendiaryBomb.id, PlayerResearch_BombsRate1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BombsArea2: TPlayerResearchDefinition = {
	id: 557,
	caption: 'Bombs Splash II',
	description: '+0.1 Bombs AOE Radius',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_IncendiaryBomb.id, PlayerResearch_BombsArea1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						aoeRadiusModifier: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BombsDamage2: TPlayerResearchDefinition = {
	id: 558,
	caption: 'Bombs Damage II',
	description: '+10% Damage for Bombs',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 29,
	prerequisiteResearchIds: [PlayerResearch_IncendiaryBomb.id, PlayerResearch_BombsDamage1.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						damageModifier: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_ToadMine: TPlayerResearchDefinition = {
	id: 560,
	caption: 'Weapon: Toad Mine',
	description: ToadBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 31,
	prerequisiteResearchIds: [
		PlayerResearch_ReaperBomb.id,
		PlayerResearch_BombsDamage2.id,
		PlayerResearch_BombsRate2.id,
		PlayerResearch_BombsArea2.id,
	],
	technologyReward: {
		weapons: {
			[ToadBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_BombsRate3: TPlayerResearchDefinition = {
	id: 565,
	caption: 'Bombs Rate III',
	description: '+0.05 Attack Rate for Bombs',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_ReaperBomb.id, PlayerResearch_BombsRate2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						baseRate: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BombsArea3: TPlayerResearchDefinition = {
	id: 566,
	caption: 'Bombs Splash III',
	description: '+0.1 Bombs AOE Radius',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_ReaperBomb.id, PlayerResearch_BombsArea2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						aoeRadiusModifier: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_BombsDamage3: TPlayerResearchDefinition = {
	id: 567,
	caption: 'Bombs Damage III',
	description: '+10% Damage for Bombs',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 37,
	prerequisiteResearchIds: [PlayerResearch_ReaperBomb.id, PlayerResearch_BombsDamage2.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						damageModifier: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersRangeRepeatable: TPlayerResearchDefinition = {
	id: 570,
	repeatable: true,
	caption: `Launchers and Bombs Range ${INFINITY_SYMBOL}`,
	description: '+0.05 Launchers Attack Range',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_LaunchersRange3.id, PlayerResearch_BombsRate3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						baseRange: {
							bias: 0.05,
						},
					},
					[Weapons.TWeaponGroupType.Bombs]: {
						baseRange: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersAreaRepeatable: TPlayerResearchDefinition = {
	id: 571,
	repeatable: true,
	caption: `Launchers and Bombs Splash ${INFINITY_SYMBOL}`,
	description: '+0.05 Launchers and Bombs AOE Radius',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_LaunchersArea3.id, PlayerResearch_BombsArea3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						aoeRadiusModifier: {
							bias: 0.05,
						},
					},
					[Weapons.TWeaponGroupType.Bombs]: {
						aoeRadiusModifier: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_LaunchersDamageRepeatable: TPlayerResearchDefinition = {
	id: 572,
	repeatable: true,
	caption: `Launchers and Bombs Damage ${INFINITY_SYMBOL}`,
	description: '+2% Damage for Launchers and Bombs',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [PlayerResearch_BombsDamage3.id, PlayerResearch_LaunchersDamage3.id],
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						damageModifier: {
							factor: 1.02,
						},
					},
					[Weapons.TWeaponGroupType.Bombs]: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
};
export const PlayerResearch_NuclearBomb: TPlayerResearchDefinition = {
	id: 575,
	caption: 'Weapon: Nuclear Bomb',
	description: NuclearBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [
		PlayerResearch_LaserGuidedBomb.id,
		PlayerResearch_BombsDamage3.id,
		PlayerResearch_BombsRate3.id,
		PlayerResearch_BombsArea3.id,
	],
	technologyReward: {
		weapons: {
			[NuclearBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_AntimatterBomb: TPlayerResearchDefinition = {
	id: 576,
	caption: 'Weapon: Antimatter Bomb',
	description: AntimatterBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [
		PlayerResearch_NailBomb.id,
		PlayerResearch_BombsDamage3.id,
		PlayerResearch_BombsRate3.id,
		PlayerResearch_BombsArea3.id,
	],
	technologyReward: {
		weapons: {
			[AntimatterBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_BlackholeBomb: TPlayerResearchDefinition = {
	id: 577,
	caption: 'Weapon: Black Hole Bomb',
	description: BlackHoleBomb.description,
	cost: DEFAULT_PLAYER_RESEARCH_COST * 41,
	prerequisiteResearchIds: [
		PlayerResearch_ClusterBomb.id,
		PlayerResearch_BombsDamage3.id,
		PlayerResearch_BombsRate3.id,
		PlayerResearch_BombsArea3.id,
	],
	technologyReward: {
		weapons: {
			[BlackHoleBomb.id]: 0.5,
		},
	},
};
export const PlayerResearch_ReverseKinematics: TPlayerResearchDefinition = {
	id: 580,
	caption: 'Reverse Kinematics',
	description: '+10% Dodge vs Bombardment. +0.5 Bombs Range. Equipment: Tensor Coprocessor',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BombsDamage3.id,
		PlayerResearch_BombsRate3.id,
		PlayerResearch_BombsArea3.id,
	],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.bombardment]: {
							bias: 0.1,
						},
					},
				},
			],
			weapon: [
				{
					[Weapons.TWeaponGroupType.Bombs]: {
						baseRange: {
							bias: 0.5,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[TensorCoprocessorEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_NullPayload: TPlayerResearchDefinition = {
	id: 581,
	caption: 'Null Payload Adapter',
	description: '+10% Dodge vs Projectile. +0.5 Launchers Range. Equipment: Hadron Inverter',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BombsDamage3.id,
		PlayerResearch_BombsRate3.id,
		PlayerResearch_BombsArea3.id,
	],
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.ballistic]: {
							bias: 0.1,
						},
					},
				},
			],
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						baseRange: {
							bias: 0.5,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[SuicideDeviceEquipment.id]: 0.5,
		},
	},
};
export const PlayerResearch_StringDetachment: TPlayerResearchDefinition = {
	id: 582,
	caption: 'Strings Detachment',
	description: 'Scout, Rover and Tiltjet gain +2 Armor Protection. Equipment: Void Transporter',
	cost: DEFAULT_PLAYER_RESEARCH_COST * 53,
	prerequisiteResearchIds: [
		PlayerResearch_BombsDamage3.id,
		PlayerResearch_BombsRate3.id,
		PlayerResearch_BombsArea3.id,
	],
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			armor: [
				{
					default: {
						bias: -2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			armor: [
				{
					default: {
						bias: -2,
					},
				},
			],
		},
		[UnitChassis.chassisClass.tiltjet]: {
			armor: [
				{
					default: {
						bias: -2,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[VoidTransporterEquipment.id]: 0.5,
		},
	},
};

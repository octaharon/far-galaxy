import _ from 'underscore';
import { DIInjectableCollectibles, DIInjectableSerializables } from '../core/DI/injections';
import InterfaceTypes from '../core/InterfaceTypes';
import { SerializableFactory } from '../core/Serializable';
import ArtilleryChassis from '../technologies/chassis/all/Artillery/ArtilleryPlatform.30000';
import ColossusArtilleryChassis from '../technologies/chassis/all/Artillery/Colossus.30002';
import LicornArtilleryChassis from '../technologies/chassis/all/Artillery/Licorn.30001';
import Weapons from '../technologies/types/weapons';
import PoliticalFactions from '../world/factions';
import { defaultPlayerProps } from './constants';
import GenericPlayer from './GenericPlayer';
import { TPlayerId } from './playerId';
import { hydrateResearch } from './Research';
import {
	PlayerTalent_Marksman,
	PlayerTalent_Mechanist,
	PlayerTalent_Warchief,
} from './Talents/TalentRegistry/all/startingTalents';
import { PlayerTalent_Surveyor } from './Talents/TalentRegistry/all/Tier1';
import { PlayerTalent_Rocketeer, PlayerTalent_Scholar } from './Talents/TalentRegistry/all/Tier2';
import { hydrateTalent } from './Talents/types';
import Players from './types';

export const PlayerTestID = 'debug_player_';
type TestPlayerDictionary = {
	[key in PoliticalFactions.TFactionID]?: Players.IPlayer;
};
const testPlayerInstance: TestPlayerDictionary = {};

export class PlayerManager
	extends SerializableFactory<
		Players.TPlayerStaticProps & Players.TPlayerDynamicProps,
		Players.TSerializedPlayer,
		Players.IPlayer
	>
	implements Players.IPlayerFactory
{
	protected __playerIdCache: InterfaceTypes.TDictionary<string> = {};

	public getByPlayerId(playerId: TPlayerId) {
		return this.__objectCache?.[this.__playerIdCache?.[playerId]] || null;
	}

	public find<T extends Players.IPlayer>(instanceId: TPlayerId) {
		return (Object.values(testPlayerInstance).find((v) => v.id === instanceId) ||
			this.getByPlayerId(instanceId) ||
			super.find(instanceId)) as T;
	}

	public instantiate(props: Partial<Players.TPlayerStaticProps & Players.TPlayerDynamicProps>) {
		const c = this.__instantiateSerializable(
			{
				...defaultPlayerProps,
				...props,
			},
			GenericPlayer,
		);
		this.__playerIdCache[c.id] = c.getInstanceId();
		return c;
	}

	public getTestPlayer(
		faction: PoliticalFactions.TFactionID = PoliticalFactions.TFactionID.none,
		withArsenal = true,
	): Players.IPlayer {
		const playerId = PlayerTestID + faction;
		if (testPlayerInstance[faction]) return testPlayerInstance[faction];
		testPlayerInstance[faction] = this.instantiate({
			id: playerId,
			nickname: 'Test Player',
			level: 14,
			xp: 12000,
			talents: {
				[PlayerTalent_Mechanist.id]: {
					...PlayerTalent_Mechanist,
					playerId,
					learned: true,
				},
				[PlayerTalent_Marksman.id]: {
					...PlayerTalent_Marksman,
					playerId,
					learned: true,
				},
				[PlayerTalent_Surveyor.id]: {
					playerId,
					...PlayerTalent_Surveyor,
					learned: true,
				},
				[PlayerTalent_Scholar.id]: {
					playerId,
					...PlayerTalent_Scholar,
					learned: true,
				},
				[PlayerTalent_Rocketeer.id]: {
					playerId,
					...PlayerTalent_Rocketeer,
					learned: true,
				},
			},
			stats: {
				damageDoneByWeaponTypes: {
					[Weapons.TWeaponGroupType.Lasers]: 4543,
					[Weapons.TWeaponGroupType.Beam]: 1749,
					[Weapons.TWeaponGroupType.Rockets]: 267,
					[Weapons.TWeaponGroupType.Guns]: 1243,
				},
				unitsKilledWithChassisId: {
					[ArtilleryChassis.id]: 14,
					[LicornArtilleryChassis.id]: 5,
				},
			},
			faction,
			arsenal: {
				weapons: withArsenal
					? _.mapObject(this.getProvider(DIInjectableCollectibles.weapon).getAll(), () => 1)
					: {},
				armor: withArsenal
					? _.mapObject(this.getProvider(DIInjectableCollectibles.armor).getAll(), () => 1)
					: {},
				shield: withArsenal
					? _.mapObject(this.getProvider(DIInjectableCollectibles.shield).getAll(), () => 1)
					: {},
				chassis: withArsenal
					? _.mapObject(this.getProvider(DIInjectableCollectibles.chassis).getAll(), (chassis) =>
							this.getProvider(DIInjectableCollectibles.chassis).isChassisAvailableToFaction(
								chassis,
								faction,
							)
								? 1
								: 0,
					  )
					: {},
				equipment: withArsenal
					? _.mapObject(this.getProvider(DIInjectableCollectibles.equipment).getAll(), () => 1)
					: {},
				establishments: withArsenal
					? _.mapObject(this.getProvider(DIInjectableCollectibles.establishment).getAll(), () => 1)
					: {},
				facilities: withArsenal
					? _.mapObject(this.getProvider(DIInjectableCollectibles.facility).getAll(), () => 1)
					: {},
				factories: withArsenal
					? _.mapObject(this.getProvider(DIInjectableCollectibles.factory).getAll(), () => 1)
					: {},
			},
		});
		this.store(testPlayerInstance[faction]);
		return testPlayerInstance[faction];
	}

	public unserialize(t: Players.TSerializedPlayer) {
		if (!t) return null;
		const c = this.instantiate({
			...t,
			talents: _.mapObject(t.talents, (t) => hydrateTalent(t)),
			researches: _.mapObject(t.researches, (t) => hydrateResearch(t)),
			bases: _.mapObject(t.bases, (b) => this.getProvider(DIInjectableSerializables.base).unserialize(b)),
		});
		c.setInstanceId(t.instanceId);
		this.__playerIdCache[c.id] = c.getInstanceId();
		return c;
	}
}

export default PlayerManager;

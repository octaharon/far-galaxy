import { Collectible, IStaticCollectible, TCollectible, TSerializedCollectible } from '../core/Collectible';
import { DIInjectableSerializables } from '../core/DI/injections';
import { TPlayerId } from './playerId';
import Players, { IPlayerProperty, IPlayerPropertyProps } from './types';

export type TCollectibleProperty<
	PropType extends IPlayerPropertyProps,
	SerializedType extends PropType & TSerializedCollectible,
	StaticType extends IStaticCollectible,
	ObjectType extends PlayerCollectibleProperty<PropType, SerializedType, StaticType>,
> = TCollectible<ObjectType, StaticType>;

export abstract class PlayerCollectibleProperty<
		PropType extends IPlayerPropertyProps,
		SerializedType extends PropType & TSerializedCollectible,
		StaticType extends IStaticCollectible = IStaticCollectible,
	>
	extends Collectible<PropType, SerializedType, StaticType>
	implements IPlayerProperty
{
	public playerId: TPlayerId;

	public get static() {
		return this.constructor as TCollectibleProperty<PropType, SerializedType, StaticType, this>;
	}

	public setOwner(pId: TPlayerId) {
		this.playerId = pId;
		return this;
	}

	public get owner(): Players.IPlayer {
		try {
			return this.getProvider(DIInjectableSerializables.player).find(this.playerId);
		} catch (e) {
			console.error(e);
			return null;
		}
	}

	doesBelongToPlayer(playerId: TPlayerId): boolean {
		return this.playerId === playerId;
	}

	isFriendlyToPlayer(playerId: TPlayerId): boolean {
		// @TODO Later add support for multiplayer alliances
		return this.playerId === playerId;
	}

	protected __serializePlayerCollectibleProperty() {
		return {
			...this.__serializeCollectible(),
			playerId: this.playerId,
		};
	}
}

export default PlayerCollectibleProperty;

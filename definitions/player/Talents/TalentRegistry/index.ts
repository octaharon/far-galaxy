import Players from '../../types';
import { TPlayerTalentDefinition } from '../types';
import * as StartingTalents from './all/startingTalents';
import * as Tier1Talents from './all/Tier1';
import * as Tier2Talents from './all/Tier2';
import * as Tier3Talents from './all/Tier3';
import * as Tier4Talents from './all/Tier4';
import * as Tier5Talents from './all/Tier5';
import * as Tier6Talents from './all/Tier6';

const talents = [
	...Object.values(StartingTalents),
	...Object.values(Tier1Talents),
	...Object.values(Tier2Talents),
	...Object.values(Tier3Talents),
	...Object.values(Tier4Talents),
	...Object.values(Tier5Talents),
	...Object.values(Tier6Talents),
].reduce((list, item) => {
	if (list[item.id]) throw new Error('Duplicate Talent Id: ' + item.id);
	return Object.assign(list, {
		[item.id]: item,
	});
}, {} as Record<number | string, TPlayerTalentDefinition>);

export class TalentRegistry {
	public static get(id: number): TPlayerTalentDefinition {
		return {
			achievement: (p: Players.IPlayer) => (!!p.talents[id] && p.talents[id].learned ? 1 : 0),
			levelReq: 1,
			learnable: true,
			prerequisiteTalentIds: [],
			...(talents[id] || null),
		};
	}

	public static getAll() {
		return Object.values(talents);
	}
}

export default TalentRegistry;

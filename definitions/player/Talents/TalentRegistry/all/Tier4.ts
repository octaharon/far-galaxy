import Series from '../../../../maths/Series';
import FrequencyArrayEstablishment from '../../../../orbital/establishments/all/FrequencyArray.203';
import ManufactoryEstablishment from '../../../../orbital/establishments/all/Manufactory.403';
import NaniteCloudEstablishment from '../../../../orbital/establishments/all/NaniteCloud.304';
import SimulationChamber from '../../../../orbital/establishments/all/SimulationChamber.404';
import TachyonLatticeEstablishment from '../../../../orbital/establishments/all/TachyonLattice.204';
import VertebralArmorEstablishment from '../../../../orbital/establishments/all/VertebralArmor.303';
import VRNetworkEstablishment from '../../../../orbital/establishments/all/VRNetwork.102';
import NullTransporterFacility from '../../../../orbital/facilities/all/NullTransporter.102';
import ParticleCondenserFacility from '../../../../orbital/facilities/all/ParticleCondenser.101';
import BioforgeFactory from '../../../../orbital/factories/all/Bioforge.104';
import HoverforgeFactory from '../../../../orbital/factories/all/Hoverforge.501';
import JunkyardFactory from '../../../../orbital/factories/all/Junkyard.304';
import ShipyardFactory from '../../../../orbital/factories/all/Shipyard.401';
import SpecialForcesAcademyFactory from '../../../../orbital/factories/all/SpecialForcesAcademy.106';
import SubmarinePool from '../../../../orbital/factories/all/SubmarinePool.403';
import { getPlayerStatProperty } from '../../../../stats/helpers';
import CrumbleAbility from '../../../../tactical/abilities/all/Player/Crumble.60008';
import EquinoxBarrage from '../../../../tactical/abilities/all/Player/EquinoxBarrage.60009';
import EqualizerAbility from '../../../../tactical/abilities/all/Player/GreatEqualizer.60011';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitActions from '../../../../tactical/units/actions/types';
import InertArmor from '../../../../technologies/armor/all/large/InertArmor.30011';
import LargeCompositeArmor from '../../../../technologies/armor/all/large/LargeCompositeArmor.30002';
import LargeQuantumArmor from '../../../../technologies/armor/all/large/LargeQuantumArmor.30003';
import ProbabilityArmor from '../../../../technologies/armor/all/large/ProbabilityArmor.30010';
import SMESArmor from '../../../../technologies/armor/all/large/SMESArmor.30009';
import CompositeArmor from '../../../../technologies/armor/all/medium/CompositeArmor.20002';
import ElectronArmor from '../../../../technologies/armor/all/medium/ElectronArmor.20006';
import PositronArmor from '../../../../technologies/armor/all/medium/PositronArmor.20007';
import QuantumArmor from '../../../../technologies/armor/all/medium/QuantumArmor.20003';
import AdvancedPowerArmor from '../../../../technologies/armor/all/small/AdvancedPowerArmor.10010';
import CompositePlatingArmor from '../../../../technologies/armor/all/small/CompositePlating.10003';
import ThickKevlarPlatingArmor from '../../../../technologies/armor/all/small/ThickKevlarPlating.10002';
import AssaultBatteryChassis from '../../../../technologies/chassis/all/Artillery/AssaultBattery.30003';
import LicornArtilleryChassis from '../../../../technologies/chassis/all/Artillery/Licorn.30001';
import DreadnoughtChassis from '../../../../technologies/chassis/all/Battleship/Dreadnought.62002';
import ChimeraChassis from '../../../../technologies/chassis/all/BioTech/Chimera.40002';
import BomberChassis from '../../../../technologies/chassis/all/Bomber/Bomber.80000';
import FowlerChassis from '../../../../technologies/chassis/all/CombatDrone/Fowler.20002';
import SurveyorChassis from '../../../../technologies/chassis/all/CombatDrone/Surveyor.20003';
import BehemothChassis from '../../../../technologies/chassis/all/CombatMech/Behemoth.18001';
import JupiterChassis from '../../../../technologies/chassis/all/CombatMech/Jupiter.18002';
import TritonChassis from '../../../../technologies/chassis/all/Corvette/Triton.60001';
import CruiserChassis from '../../../../technologies/chassis/all/Cruiser/Cruiser.65000';
import DestroyerChassis from '../../../../technologies/chassis/all/Destroyer/Destroyer.64000';
import M5Chassis from '../../../../technologies/chassis/all/Droid/M5.25004';
import TracerChassis from '../../../../technologies/chassis/all/Exoskeleton/Tracer.15001';
import FighterChassis from '../../../../technologies/chassis/all/Fighter/Fighter.82000';
import MantisChassis from '../../../../technologies/chassis/all/Fighter/Mantis.82001';
import CharonChassis from '../../../../technologies/chassis/all/Fortress/Charon.59002';
import FortressChassis from '../../../../technologies/chassis/all/Fortress/Fortess.59000';
import GroundbreakerChassis from '../../../../technologies/chassis/all/HAV/Groundbreaker.33001';
import RhinoChassis from '../../../../technologies/chassis/all/HAV/Rhino.33002';
import DefenderOfFaithChassis from '../../../../technologies/chassis/all/HoverTank/DefenderOfFaith.55003';
import MammothChassis from '../../../../technologies/chassis/all/HoverTank/Mammoth.55001';
import RamraiderChassis from '../../../../technologies/chassis/all/HoverTank/Ramraider.55002';
import HydraChassis from '../../../../technologies/chassis/all/Hydra/Hydra.43000';
import BruteChassis from '../../../../technologies/chassis/all/Infantry/Brute.10005';
import QuantumOpsChassis from '../../../../technologies/chassis/all/Infantry/QuantumOps.10003';
import LauncherPadChassis from '../../../../technologies/chassis/all/LauncherPad/LauncherPad.86000';
import ConvictorChassis from '../../../../technologies/chassis/all/LAV/Convictor.37003';
import WardenChassis from '../../../../technologies/chassis/all/LAV/Warden.37002';
import AdmiralsPrideChassis from '../../../../technologies/chassis/all/Manofwar/AdmiralsPride.69501';
import VindicatorChassis from '../../../../technologies/chassis/all/MotherShip/Vindicator.89001';
import GunTurretChassis from '../../../../technologies/chassis/all/PointDefense/GunTurret.57002';
import MissileTurretChassis from '../../../../technologies/chassis/all/PointDefense/MissileTurret.57001';
import QuadrapodChassis from '../../../../technologies/chassis/all/Quadrapod/Quadrapod.35000';
import HawkChassis from '../../../../technologies/chassis/all/Rover/Hawk.50002';
import WandererChassis from '../../../../technologies/chassis/all/ScienceVessel/TheWanderer.99001';
import StrikeAircraftChassis from '../../../../technologies/chassis/all/StrikeAircraft/StrikeAircraft.85000';
import SubmarineChassis from '../../../../technologies/chassis/all/Submarine/Submarine.66000';
import SwarmChassis from '../../../../technologies/chassis/all/Swarm/Swarm.45000';
import CrusaderChassis from '../../../../technologies/chassis/all/Tank/Crusader.36001';
import BluethunderChassis from '../../../../technologies/chassis/all/Titljet/BlueThunder.84002';
import { airChassisList, hoverChassisList, navalChassisList } from '../../../../technologies/chassis/constants';
import MetacongitionInterfaceEquipment from '../../../../technologies/equipment/all/Support/MetacognitionInterface.20030';
import VoidTransporter20018 from '../../../../technologies/equipment/all/Enhancement/VoidTransporter.20018';
import AssistedTrainingModuleEquipment from '../../../../technologies/equipment/all/Support/AssistedTrainingModule.20022';
import NobleGuardChipEquipment from '../../../../technologies/equipment/all/Tactical/NobleGuardChip.20015';
import SabotageKitEquipment from '../../../../technologies/equipment/all/Tactical/SabotageKit.30010';
import SimulationMatrixEquipment from '../../../../technologies/equipment/all/Tactical/SimulationMatrix.20005';
import BacterialFarmEquipment from '../../../../technologies/equipment/all/Warfare/BacterialFarm.30005';
import SuicideDeviceEquipment from '../../../../technologies/equipment/all/Warfare/SuicideDevice.30003';
import ImmortalShield from '../../../../technologies/shields/all/large/ImmortalShield.30010';
import LargeHullShield from '../../../../technologies/shields/all/large/LargeHullShield.30008';
import LargeNanoShield from '../../../../technologies/shields/all/large/LargeNanoShield.30007';
import LargeNeutroniumShield from '../../../../technologies/shields/all/large/LargeNeutroniumShield.30004';
import WindShield from '../../../../technologies/shields/all/large/WindShield.30011';
import HullShield from '../../../../technologies/shields/all/medium/HullShield.20008';
import NanoShield from '../../../../technologies/shields/all/medium/NanoShield.20007';
import NeutroniumShield from '../../../../technologies/shields/all/medium/NeutroniumShield.20004';
import ReactiveShield from '../../../../technologies/shields/all/small/ReactiveShield.10001';
import SmallHullShield from '../../../../technologies/shields/all/small/SmallHullShield.10008';
import SmallNanoShield from '../../../../technologies/shields/all/small/SmallNanoShield.10007';
import SmallNeutroniumShield from '../../../../technologies/shields/all/small/SmallNeutroniumShield.10004';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AALaser from '../../../../technologies/weapons/all/antiair/AALaser.70003';
import CarapaceWeapon from '../../../../technologies/weapons/all/antiair/Carapace.70006';
import SilverCloudWeapon from '../../../../technologies/weapons/all/antiair/SilverCloud.70005';
import LightningGun from '../../../../technologies/weapons/all/beam/LightningGun.80001';
import QuantumExciter from '../../../../technologies/weapons/all/beam/QuantumExciter.80004';
import ThetisRay from '../../../../technologies/weapons/all/beam/ThetisRay.80007';
import ClusterBomb from '../../../../technologies/weapons/all/bombs/ClusterBomb.40004';
import LaserGuidedBomb from '../../../../technologies/weapons/all/bombs/LaserGuidedBomb.40006';
import NailBomb from '../../../../technologies/weapons/all/bombs/NailBomb.40009';
import NapalmBomb from '../../../../technologies/weapons/all/bombs/NapalmBomb.40003';
import EMPShells from '../../../../technologies/weapons/all/cannons/EMPShells.10004';
import TauShells from '../../../../technologies/weapons/all/cannons/TauShells.10007';
import VacuumShells from '../../../../technologies/weapons/all/cannons/VacuumShells.10008';
import GlidingMissile from '../../../../technologies/weapons/all/guided/GlidingMissile.35005';
import HammerfallMissile from '../../../../technologies/weapons/all/guided/Hammerfall.35000';
import PALMGun from '../../../../technologies/weapons/all/lasers/PALM.90001';
import XRayLaser from '../../../../technologies/weapons/all/lasers/XRayLaser.90002';
import LaserAssistedGun from '../../../../technologies/weapons/all/machineguns/LaserAssistedGun.20012';
import TrackingGun from '../../../../technologies/weapons/all/machineguns/TrackingGun.20011';
import CorrosiveMissile from '../../../../technologies/weapons/all/missiles/CorrosiveMissile.30001';
import JerichoMissile from '../../../../technologies/weapons/all/missiles/Jericho.30005';
import NeutronMissile from '../../../../technologies/weapons/all/missiles/NeutronMissile.30009';
import CathodeGun from '../../../../technologies/weapons/all/plasma/CathodeGun.50003';
import GrapesOfWrath from '../../../../technologies/weapons/all/plasma/GrapesOfWrath.50004';
import PoisonMines from '../../../../technologies/weapons/all/projectiles/PoisonMines.15005';
import RadioRailgun from '../../../../technologies/weapons/all/railguns/RadioRailgun.25001';
import TyphoonWeapon from '../../../../technologies/weapons/all/railguns/Typhoon.25005';
import HellfireRocket from '../../../../technologies/weapons/all/rockets/Hellfire.38001';
import TiamatRocket from '../../../../technologies/weapons/all/rockets/Tiamat.38003';
import { PLAYER_TALENT_TIER_LEVEL } from '../../../constants';
import Players from '../../../types';
import { TPlayerTalentDefinition } from '../../types';
import {
	PlayerTalent_BGH,
	PlayerTalent_Financier,
	PlayerTalent_IntoTheSeas,
	PlayerTalent_IntoTheSkies,
	PlayerTalent_Warlord,
} from './Tier3';

export const PlayerTalent_CLevel: TPlayerTalentDefinition = {
	id: 500,
	caption: 'C-Level (+10 Production Capacity)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Reach Player Level 15',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, p.level / 15);
	},
	baseModifier: {
		productionCapacity: {
			bias: 10,
		},
	},
	technologyReward: {
		chassis: {
			[BruteChassis.id]: 0.1,
			[VindicatorChassis.id]: 0.1,
			[AdmiralsPrideChassis.id]: 0.1,
			[CharonChassis.id]: 0.1,
			[BehemothChassis.id]: 0.1,
			[WandererChassis.id]: 0.1,
			[DefenderOfFaithChassis.id]: 0.1,
		},
	},
};
export const PlayerTalent_Talented: TPlayerTalentDefinition = {
	id: 501,
	caption: 'Talented (+2 Production Speed)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Gain 30 Talents',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'talentsLearned') / 30);
	},
	baseModifier: {
		productionSpeed: {
			bias: 2,
		},
	},
	technologyReward: {
		equipment: {
			[NobleGuardChipEquipment.id]: 0.1,
		},
		establishments: {
			[FrequencyArrayEstablishment.id]: 0.5,
		},
		factories: {
			[SpecialForcesAcademyFactory.id]: 0.5,
		},
		chassis: {
			[QuantumOpsChassis.id]: 0.5,
		},
		weapons: {
			[PALMGun.id]: 0.3,
		},
	},
};
export const PlayerTalent_Researcher: TPlayerTalentDefinition = {
	id: 502,
	caption: 'Researcher (+1 Establishments Resource Output)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Complete 30 Researches',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'researchesComplete') / 30);
	},
	baseModifier: {
		researchOutput: {
			bias: 1,
		},
	},
	technologyReward: {
		equipment: {
			[AssistedTrainingModuleEquipment.id]: 0.1,
		},
		establishments: {
			[VRNetworkEstablishment.id]: 0.5,
		},
		factories: {
			[BioforgeFactory.id]: 0.5,
		},
		chassis: {
			[LicornArtilleryChassis.id]: 0.5,
		},
		weapons: {
			[VacuumShells.id]: 0.3,
		},
	},
};
export const PlayerTalent_Engineer: TPlayerTalentDefinition = {
	id: 503,
	caption: 'Engineer (+1 Establishments Engineering Output)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Complete 30 any Engineering Projects',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			(getPlayerStatProperty(p, 'reverseEngineeringProjectsComplete') +
				getPlayerStatProperty(p, 'chassisUpgradeProjectsCompleteById') +
				getPlayerStatProperty(p, 'globalEngineeringProjectsCompleteById')) /
				30,
		);
	},
	baseModifier: {
		engineeringOutput: {
			bias: 1,
		},
	},
	technologyReward: {
		equipment: {
			[MetacongitionInterfaceEquipment.id]: 0.1,
		},
		establishments: {
			[ManufactoryEstablishment.id]: 0.5,
		},
		factories: {
			[JunkyardFactory.id]: 0.5,
		},
		chassis: {
			[SurveyorChassis.id]: 0.5,
		},
		weapons: {
			[SilverCloudWeapon.id]: 0.3,
		},
	},
};
export const PlayerTalent_Mayor: TPlayerTalentDefinition = {
	id: 504,
	caption: 'Mayor (+3 Facilities Energy Output)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Build 20 Buildings',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			(getPlayerStatProperty(p, 'factoriesBuilt') +
				getPlayerStatProperty(p, 'establishmentsBuilt') +
				getPlayerStatProperty(p, 'facilitiesBuilt')) /
				20,
		);
	},
	baseModifier: {
		energyOutput: {
			bias: 3,
		},
	},
	technologyReward: {
		equipment: {
			[SimulationMatrixEquipment.id]: 0.1,
		},
		facilities: {
			[ParticleCondenserFacility.id]: 0.5,
		},
		establishments: {
			[SimulationChamber.id]: 0.5,
		},
		chassis: {
			[ConvictorChassis.id]: 0.5,
		},
		weapons: {
			[LightningGun.id]: 0.3,
		},
	},
};
export const PlayerTalent_Sustainer: TPlayerTalentDefinition = {
	id: 505,
	caption: 'Sustainer (+1 Facilities Repair Output)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Repair 8000 Hit Points',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'repairSpent') / 8000);
	},
	baseModifier: {
		repairOutput: {
			bias: 1,
		},
	},
	technologyReward: {
		equipment: {
			[VoidTransporter20018.id]: 0.1,
		},
		facilities: {
			[NullTransporterFacility.id]: 0.5,
		},
		establishments: {
			[TachyonLatticeEstablishment.id]: 0.5,
		},
		chassis: {
			[RhinoChassis.id]: 0.5,
		},
		weapons: {
			[CorrosiveMissile.id]: 0.3,
		},
	},
};
export const PlayerTalent_Destroyer: TPlayerTalentDefinition = {
	id: 510,
	caption: 'Destroyer (Crumble Ability)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Destroy 100 Doodads',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'doodadsDestroyed') / 100);
	},
	reward(p: Players.IPlayer) {
		return p.learnAbility(CrumbleAbility.id);
	},
	technologyReward: {
		equipment: {
			[SuicideDeviceEquipment.id]: 0.1,
		},
		establishments: {
			[NaniteCloudEstablishment.id]: 0.5,
		},
		armor: {
			[CompositePlatingArmor.id]: 0.3,
			[CompositeArmor.id]: 0.3,
			[LargeCompositeArmor.id]: 0.3,
		},
	},
};
export const PlayerTalent_Saboteur: TPlayerTalentDefinition = {
	id: 511,
	caption: 'Saboteur (Great Equalizer Ability)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Use Delay 100 Times',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'actionsIssued', UnitActions.unitActionTypes.delay) / 100);
	},
	reward(p: Players.IPlayer) {
		return p.learnAbility(EqualizerAbility.id);
	},
	technologyReward: {
		equipment: {
			[SabotageKitEquipment.id]: 0.1,
		},
		establishments: {
			[VertebralArmorEstablishment.id]: 0.5,
		},
		shield: {
			[LargeHullShield.id]: 0.3,
			[SmallHullShield.id]: 0.3,
			[HullShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_Champion: TPlayerTalentDefinition = {
	id: 512,
	caption: 'Champion (Equinox Barrage Ability)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Perform 100 Orbital Attacks',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'orbitalAttacksPerformed') / 100);
	},
	reward(p: Players.IPlayer) {
		return p.learnAbility(EquinoxBarrage.id);
	},
	technologyReward: {
		chassis: {
			[JupiterChassis.id]: 0.5,
			[M5Chassis.id]: 0.5,
			[TracerChassis.id]: 0.5,
			[MammothChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Rifleman: TPlayerTalentDefinition = {
	id: 520,
	caption: 'Rifleman (+5% Machine Guns Hit Chance)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 100 Units with Machine Guns',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Guns) / 100);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[LaserAssistedGun.id]: 0.3,
			[CathodeGun.id]: 0.3,
		},
		chassis: {
			[CrusaderChassis.id]: 0.5,
			[GunTurretChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_GunLayer: TPlayerTalentDefinition = {
	id: 521,
	caption: 'Gunlayer (+10% Launchers Attack Rate)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 100 Units with Launchers',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Launchers) / 100,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[PoisonMines.id]: 0.3,
			[NailBomb.id]: 0.3,
		},
		chassis: {
			[WardenChassis.id]: 0.5,
			[RamraiderChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Suppressor: TPlayerTalentDefinition = {
	id: 522,
	caption: 'Suppressor (+10% Homing Missiles Damage)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 100 Units with Homing Missiles',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Missiles) / 100,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						damageModifier: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[JerichoMissile.id]: 0.3,
			[NapalmBomb.id]: 0.3,
		},
		chassis: {
			[GroundbreakerChassis.id]: 0.5,
			[FowlerChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Breacher: TPlayerTalentDefinition = {
	id: 523,
	caption: 'Breacher (+10% Cruise Missiles Damage)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 100 Units with Cruise Missiles',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Cruise) / 100);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cruise]: {
						damageModifier: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[HammerfallMissile.id]: 0.3,
		},
		chassis: {
			[AssaultBatteryChassis.id]: 0.5,
			[HawkChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Leadsprayer: TPlayerTalentDefinition = {
	id: 524,
	caption: 'Leadsprayer (+10% Cannons Attack Rate)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 100 Units with Cannons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Cannons) / 100,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[EMPShells.id]: 0.3,
		},
		armor: {
			[InertArmor.id]: 0.3,
		},
		shield: {
			[ImmortalShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_Piercer: TPlayerTalentDefinition = {
	id: 525,
	caption: 'Piercer (+10% Mass Drivers Attack Rate)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 100 Units with Mass Drivers',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Railguns) / 100,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[RadioRailgun.id]: 0.3,
			[HellfireRocket.id]: 0.3,
		},
		armor: {
			[AdvancedPowerArmor.id]: 0.3,
		},
		shield: {
			[ReactiveShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_Heater: TPlayerTalentDefinition = {
	id: 526,
	caption: 'Heater (+10% Lasers Hit Chance)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 100 Units with Lasers',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Lasers) / 100);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseAccuracy: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[XRayLaser.id]: 0.3,
			[AALaser.id]: 0.3,
		},
		shield: {
			[LargeNeutroniumShield.id]: 0.3,
			[SmallNeutroniumShield.id]: 0.3,
			[NeutroniumShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_Burner: TPlayerTalentDefinition = {
	id: 527,
	caption: 'Burner (+10% Energy Weapons Attack Rate)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 60 Units with Energy Weapons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Beam) / 60);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						baseRate: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[QuantumExciter.id]: 0.3,
			[TrackingGun.id]: 0.3,
		},
		armor: {
			[ElectronArmor.id]: 0.3,
			[PositronArmor.id]: 0.3,
			[ProbabilityArmor.id]: 0.3,
		},
	},
};
export const PlayerTalent_Coiler: TPlayerTalentDefinition = {
	id: 528,
	caption: 'Coiler (+0.5 Plasma Attack Range)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 60 Units with Plasma Weapons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.Plasma) / 60);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						baseRange: {
							bias: 0.5,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[GrapesOfWrath.id]: 0.3,
			[ThetisRay.id]: 0.3,
		},
		shield: {
			[NanoShield.id]: 0.3,
			[SmallNanoShield.id]: 0.3,
			[LargeNanoShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_Zenithar: TPlayerTalentDefinition = {
	id: 529,
	caption: 'Zenithar (+0.5 Anti-Air Range)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Kill 60 Units with Anti-Air Weapons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'unitsKilledByWeaponTypes', Weapons.TWeaponGroupType.AntiAir) / 100,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.AntiAir]: {
						baseRange: {
							bias: 0.5,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[CarapaceWeapon.id]: 0.3,
			[NeutronMissile.id]: 0.3,
		},
		armor: {
			[QuantumArmor.id]: 0.3,
			[LargeQuantumArmor.id]: 0.3,
			[ThickKevlarPlatingArmor.id]: 0.3,
		},
	},
};
export const PlayerTalent_Midshipman: TPlayerTalentDefinition = {
	id: 530,
	caption: 'Midship Man (+0.5 Corvette and Support Ship Speed)',
	learnable: true,
	prerequisiteTalentIds: [PlayerTalent_IntoTheSeas.id, PlayerTalent_Financier.id],
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Deal 5000 Damage with Naval Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(navalChassisList.map((c) => getPlayerStatProperty(p, 'damageDoneByClass', c))) / 5000,
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[CruiserChassis.id]: 0.5,
			[DestroyerChassis.id]: 0.5,
			[TritonChassis.id]: 0.5,
		},
		factories: {
			[ShipyardFactory.id]: 0.5,
		},
	},
};
export const PlayerTalent_Mariner: TPlayerTalentDefinition = {
	id: 531,
	caption: 'Mariner (+10 Corvette and Support Ship Hit Points)',
	learnable: true,
	prerequisiteTalentIds: [PlayerTalent_IntoTheSeas.id, PlayerTalent_Warlord.id],
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Travel 15000 Distance with Naval Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'distanceCoveredByMovementType', UnitChassis.movementType.naval) / 15000,
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.corvette]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_ship]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[DreadnoughtChassis.id]: 0.5,
			[SubmarineChassis.id]: 0.5,
		},
		factories: {
			[SubmarinePool.id]: 0.5,
		},
	},
};
export const PlayerTalent_Aviator: TPlayerTalentDefinition = {
	id: 532,
	caption: 'Aviator (+1 Titljet and Support Aircraft Speed)',
	learnable: true,
	prerequisiteTalentIds: [PlayerTalent_IntoTheSkies.id, PlayerTalent_Financier.id],
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Deal 5000 Damage with Aerial Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(airChassisList.map((c) => getPlayerStatProperty(p, 'damageDoneByClass', c))) / 5000,
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.tiltjet]: {
			chassis: [
				{
					speed: {
						bias: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.sup_aircraft]: {
			chassis: [
				{
					speed: {
						bias: 1,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[BluethunderChassis.id]: 0.5,
			[FighterChassis.id]: 0.5,
			[MantisChassis.id]: 1,
			[BomberChassis.id]: 0.5,
		},
		factories: {
			[HoverforgeFactory.id]: 0.5,
		},
	},
};
export const PlayerTalent_Splasher: TPlayerTalentDefinition = {
	id: 533,
	caption: 'Splasher (+5% Bombs and Rockets Damage)',
	learnable: true,
	prerequisiteTalentIds: [PlayerTalent_IntoTheSkies.id, PlayerTalent_Warlord.id],
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Travel 25000 Distance with Aerial Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'distanceCoveredByMovementType', UnitChassis.movementType.air) / 25000,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Rockets]: {
						damageModifier: {
							factor: 1.05,
						},
					},
					[Weapons.TWeaponGroupType.Bombs]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[StrikeAircraftChassis.id]: 0.5,
		},
		weapons: {
			[ClusterBomb.id]: 0.3,
			[LaserGuidedBomb.id]: 0.3,
			[TiamatRocket.id]: 0.3,
		},
	},
};
export const PlayerTalent_Colossus: TPlayerTalentDefinition = {
	id: 534,
	caption: 'Colossus (+10 Point Defense and Hovertank Points)',
	learnable: true,
	prerequisiteTalentIds: [PlayerTalent_BGH.id, PlayerTalent_Warlord.id],
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Deal 15000 Damage with Hover Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(hoverChassisList.map((c) => getPlayerStatProperty(p, 'damageDoneByClass', c))) / 15000,
		);
	},

	unitModifier: {
		[UnitChassis.chassisClass.hovertank]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.pointdefense]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[QuadrapodChassis.id]: 0.5,
			[FortressChassis.id]: 0.5,
		},
		armor: {
			[SMESArmor.id]: 0.3,
		},
		shield: {
			[WindShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_Farstriker: TPlayerTalentDefinition = {
	id: 535,
	caption: 'Farstriker (+0.25 AOE Radius)',
	learnable: true,
	prerequisiteTalentIds: [PlayerTalent_BGH.id, PlayerTalent_Financier.id],
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Deal 15000 Damage with Area Attacks',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByAttackType', UnitAttack.attackFlags.AoE) / 15000);
	},

	unitModifier: {
		default: {
			weapon: [
				{
					default: {
						aoeRadiusModifier: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[LauncherPadChassis.id]: 0.5,
			[MissileTurretChassis.id]: 0.5,
		},
		weapons: {
			[TauShells.id]: 0.3,
			[GlidingMissile.id]: 0.3,
			[TyphoonWeapon.id]: 0.3,
		},
	},
};
export const PlayerTalent_Zoomaster: TPlayerTalentDefinition = {
	id: 536,
	caption: 'Zoomaster (+10 Exosuit and Biotech Hit Points )',
	learnable: true,
	prerequisiteTalentIds: [PlayerTalent_Warlord.id, PlayerTalent_BGH.id],
	levelReq: PLAYER_TALENT_TIER_LEVEL * 3,
	description: 'Deal 15000 Damage with Organic Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByTargetType', UnitChassis.targetType.organic) / 15000);
	},
	unitModifier: {
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
		[UnitChassis.chassisClass.biotech]: {
			chassis: [
				{
					hitPoints: {
						bias: 10,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[ChimeraChassis.id]: 0.5,
			[HydraChassis.id]: 0.5,
			[SwarmChassis.id]: 0.5,
		},
		equipment: {
			[BacterialFarmEquipment.id]: 0.1,
		},
	},
};

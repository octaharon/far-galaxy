import BasicMaths from '../../../../maths/BasicMaths';
import MotorPlantFactory from '../../../../orbital/factories/all/Motorplant.200';
import RoboticsBayFactory from '../../../../orbital/factories/all/RoboticBay.300';
import TrainingFacilityFactory from '../../../../orbital/factories/all/TrainingHall.100';
import DroidChassis from '../../../../technologies/chassis/all/Droid/Droid.25000';
import T1024Chassis from '../../../../technologies/chassis/all/Droid/T-1024.25001';
import InfantryChassis from '../../../../technologies/chassis/all/Infantry/Infantry.10000';
import MaybugChassis from '../../../../technologies/chassis/all/Scout/Maybug.32004';
import ScoutChassis from '../../../../technologies/chassis/all/Scout/Scout.32000';
import FuzeShells from '../../../../technologies/weapons/all/cannons/FuzeShells.10000';
import HELLGun from '../../../../technologies/weapons/all/lasers/HELL.90000';
import AutoCannon from '../../../../technologies/weapons/all/machineguns/AutoCannon.20000';
import SmallMissile from '../../../../technologies/weapons/all/missiles/SmallMissile.30000';
import FlakeCannon from '../../../../technologies/weapons/all/projectiles/FlakeCannon.15000';
import RailgunCannon from '../../../../technologies/weapons/all/railguns/RailgunCannon.25000';
import { TPlayerTalentDefinition } from '../../types';

export const PlayerTalent_Warchief: TPlayerTalentDefinition = {
	id: 100,
	caption: 'Warchief (Infantry Chassis and Training Hall Factory)',
	description: 'Invest into Infantry production',
	learnable: true,
	levelReq: 0,
	achievement: (p) =>
		(BasicMaths.clipTo1(p?.arsenal?.factories[TrainingFacilityFactory.id] || 0) +
			BasicMaths.clipTo1(p?.arsenal?.chassis[InfantryChassis.id] || 0)) *
		0.5,
	technologyReward: {
		chassis: {
			[InfantryChassis.id]: 1,
		},
		factories: {
			[TrainingFacilityFactory.id]: 1,
		},
	},
};

export const PlayerTalent_Mechanist: TPlayerTalentDefinition = {
	id: 101,
	caption: 'Mechanic (Scout Chassis and Motorplant Factory)',
	learnable: true,
	levelReq: 0,
	description: 'Invest into Mechanized production',
	achievement: (p) =>
		(BasicMaths.clipTo1(p?.arsenal?.factories[MotorPlantFactory.id] || 0) +
			BasicMaths.clipTo1(p?.arsenal?.chassis[ScoutChassis.id] || 0)) *
		0.5,
	technologyReward: {
		chassis: {
			[ScoutChassis.id]: 1,
			[MaybugChassis.id]: 1,
		},
		factories: {
			[MotorPlantFactory.id]: 1,
		},
	},
};

export const PlayerTalent_Humanist: TPlayerTalentDefinition = {
	id: 102,
	caption: 'Humanist (Droid Chassis and Robotics Bay Factory)',
	learnable: true,
	levelReq: 0,
	description: 'Invest into Robotic production',
	achievement: (p) =>
		(BasicMaths.clipTo1(p?.arsenal?.factories[RoboticsBayFactory.id] || 0) +
			BasicMaths.clipTo1(p?.arsenal?.chassis[DroidChassis.id] || 0)) *
		0.5,
	technologyReward: {
		chassis: {
			[DroidChassis.id]: 1,
			[T1024Chassis.id]: 1,
		},
		factories: {
			[RoboticsBayFactory.id]: 1,
		},
	},
};

export const PlayerTalent_Intruder: TPlayerTalentDefinition = {
	id: 110,
	caption: 'Intruder (Fuze Shells and HE-LL Weapons)',
	learnable: true,
	levelReq: 0,
	achievement: (p) =>
		(BasicMaths.clipTo1(p?.arsenal?.weapons[FuzeShells.id] || 0) +
			BasicMaths.clipTo1(p?.arsenal?.weapons[RailgunCannon.id] || 0)) *
		0.5,
	description: 'Invest into Cannons and Lasers',
	technologyReward: {
		weapons: {
			[FuzeShells.id]: 1,
			[HELLGun.id]: 1,
		},
	},
};

export const PlayerTalent_Marksman: TPlayerTalentDefinition = {
	id: 111,
	caption: 'Marksman (Small Missile and Rail Cannon Weapons)',
	learnable: true,
	levelReq: 0,
	achievement: (p) =>
		(BasicMaths.clipTo1(p?.arsenal?.weapons[SmallMissile.id] || 0) +
			BasicMaths.clipTo1(p?.arsenal?.weapons[AutoCannon.id] || 0)) *
		0.5,
	description: 'Invest into Missiles and Railguns',
	technologyReward: {
		weapons: {
			[SmallMissile.id]: 1,
			[RailgunCannon.id]: 1,
		},
	},
};

export const PlayerTalent_Trooper: TPlayerTalentDefinition = {
	id: 112,
	caption: 'Trooper (HMG-XXII and Flake Cannon Weapons)',
	learnable: true,
	levelReq: 0,
	achievement: (p) =>
		(BasicMaths.clipTo1(p?.arsenal?.weapons[HELLGun.id] || 0) +
			BasicMaths.clipTo1(p?.arsenal?.weapons[FlakeCannon.id] || 0)) /
		2,
	description: 'Invest into Machine Guns and Launchers',
	technologyReward: {
		weapons: {
			[AutoCannon.id]: 1,
			[FlakeCannon.id]: 1,
		},
	},
};

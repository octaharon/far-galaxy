import DIContainer from '../../../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../core/DI/injections';
import { ArithmeticPrecision } from '../../../../maths/constants';
import Series from '../../../../maths/Series';
import { getPlayerUnitsAtBayByChassis } from '../../../../orbital/base/helpers';
import AmphitheatreEstablishment from '../../../../orbital/establishments/all/Amphitheatre.500';
import CampusEstablishment from '../../../../orbital/establishments/all/Campus.400';
import InvestigationBureauEstablishment from '../../../../orbital/establishments/all/InvestigationBureau.200';
import ScienceLabEstablishment from '../../../../orbital/establishments/all/ScienceLab.100';
import BarracksFactory from '../../../../orbital/factories/all/Barracks.103';
import BionicInstituteFactory from '../../../../orbital/factories/all/BionicInstitute.101';
import CyberforgeFactory from '../../../../orbital/factories/all/Cyberforge.301';
import LightVehicleFactory from '../../../../orbital/factories/all/LightVehicleFactory.201';
import MachineryFactory from '../../../../orbital/factories/all/Machinery.204';
import { getPlayerStatProperty, getStatProperty, getUnitStatProperty } from '../../../../stats/helpers';
import ExploitAbility from '../../../../tactical/abilities/all/Player/Exploit.60001';
import JeopardyAbility from '../../../../tactical/abilities/all/Player/Jeopardy.60002';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { getMaxUnitLevel } from '../../../../tactical/units/helpers';
import LargeKevlarArmor from '../../../../technologies/armor/all/large/LargeKevlarArmor.30000';
import KevlarArmor from '../../../../technologies/armor/all/medium/KevlarArmor.20000';
import KevlarPlatingArmor from '../../../../technologies/armor/all/small/KevlarPlating.10000';
import EnforcerChassis from '../../../../technologies/chassis/all/Droid/Enforcer.25002';
import HunterChassis from '../../../../technologies/chassis/all/Droid/Hunter.25003';
import ExoskeletonChassis from '../../../../technologies/chassis/all/Exoskeleton/Exoskeleton.15000';
import BruiserChassis from '../../../../technologies/chassis/all/Infantry/Bruiser.10004';
import ChaplainChassis from '../../../../technologies/chassis/all/Infantry/Chaplain.10006';
import EliteGuardianChassis from '../../../../technologies/chassis/all/Infantry/EliteGuardian.10001';
import LocustChassis from '../../../../technologies/chassis/all/Scout/Locust.32002';
import VerminChassis from '../../../../technologies/chassis/all/Scout/Vermin.32001';
import SupportVehicleChassis from '../../../../technologies/chassis/all/SupportVehicle/SupportVehicle.31000';
import PlanetaryAnchorEquipment from '../../../../technologies/equipment/all/Defensive/PlanetaryAnchor.20027';
import PolarisOmniTool from '../../../../technologies/equipment/all/Offensive/PolarisOmniTool.20014';
import RecoilCapacitorEquipment from '../../../../technologies/equipment/all/Offensive/RecoilCapacitor.20007';
import SharpShooterModule from '../../../../technologies/equipment/all/Offensive/SharpShooterModule.10007';
import AugmentedFrameEquipment from '../../../../technologies/equipment/all/Enhancement/AugmentedFrame.20006';
import FlowDisruptorEquipment from '../../../../technologies/equipment/all/Support/FlowDisruptor.20011';
import MainframeTacticalModule from '../../../../technologies/equipment/all/Support/MainframeTacticalModule.10006';
import TensorCoprocessorEquipment from '../../../../technologies/equipment/all/Support/TensorCoprocessor.20024';
import PhasingShield from '../../../../technologies/shields/all/large/PhasingShield.30000';
import BulwarkShield from '../../../../technologies/shields/all/medium/BulwarkShield.20000';
import RepulsiveShield from '../../../../technologies/shields/all/small/RepulsiveShield.10000';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import ExplosiveShells from '../../../../technologies/weapons/all/cannons/ExplosiveShells.10006';
import SeekerMissile from '../../../../technologies/weapons/all/guided/SeekerMissile.35001';
import PrismGun from '../../../../technologies/weapons/all/lasers/PrismGun.90004';
import RapidGun from '../../../../technologies/weapons/all/machineguns/RapidGun.20010';
import NapalmMissile from '../../../../technologies/weapons/all/missiles/NapalmMissile.30007';
import GravMortarWeapon from '../../../../technologies/weapons/all/projectiles/GravMortar.15001';
import RapidCoilGun from '../../../../technologies/weapons/all/railguns/RapidCoilGun.25003';
import Players from '../../../types';
import { TPlayerTalentDefinition } from '../../types';

export const PlayerTalent_Chieftain: TPlayerTalentDefinition = {
	id: 200,
	caption: 'Chieftain (Exoskeleton and Barracks)',
	learnable: true,
	description: 'Kill 12 Units with Infantry',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				Object.keys(
					DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(
						UnitChassis.chassisClass.infantry,
					),
				).map((id) => getPlayerStatProperty(p, 'unitsKilledWithChassisId', id)),
			) / 12,
		);
	},
	technologyReward: {
		chassis: {
			[EliteGuardianChassis.id]: 0.5,
			[ExoskeletonChassis.id]: 0.5,
		},
		factories: {
			[BarracksFactory.id]: 0.7,
		},
	},
};

export const PlayerTalent_Slaughterer: TPlayerTalentDefinition = {
	id: 201,
	caption: 'Slaughterer (+5% Infantry Damage)',
	learnable: false,
	description: 'Deal 1000 damage to Organic units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneToTargetType', UnitChassis.targetType.organic) / 1000);
	},
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[BruiserChassis.id]: 0.5,
			[ChaplainChassis.id]: 1,
		},
		factories: {
			[BionicInstituteFactory.id]: 0.7,
		},
	},
};

export const PlayerTalent_Surveyor: TPlayerTalentDefinition = {
	id: 202,
	caption: 'Surveyor (+15% Scout Hit Points)',
	learnable: false,
	description: 'Deal 1000 damage with Scout units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByClass', UnitChassis.chassisClass.scout) / 1000);
	},
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					hitPoints: {
						factor: 1.15,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[LocustChassis.id]: 0.5,
		},
		factories: {
			[LightVehicleFactory.id]: 0.7,
		},
	},
};

export const PlayerTalent_DroidCommander: TPlayerTalentDefinition = {
	id: 205,
	caption: 'Droid Commander (+5% Droid damage)',
	learnable: false,
	description: 'Deal 1000 damage to Robotic units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, ((p.stats || {}).damageDoneToTargetType?.[UnitChassis.targetType.robotic] || 0) / 1000);
	},
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[EnforcerChassis.id]: 0.5,
		},
		equipment: {
			[TensorCoprocessorEquipment.id]: 0.1,
		},
	},
};

export const PlayerTalent_Tactician: TPlayerTalentDefinition = {
	id: 203,
	caption: 'Tactician (+2 Scout Scan Range)',
	learnable: true,
	description: 'Have a level 4 Scout in your Unit Bay',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getMaxUnitLevel(getPlayerUnitsAtBayByChassis(p, UnitChassis.chassisClass.scout)) / 4);
	},
	unitModifier: {
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					scanRange: {
						bias: 2,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[VerminChassis.id]: 0.5,
		},
		factories: {
			[MachineryFactory.id]: 0.7,
		},
	},
};

export const PlayerTalent_Overmind: TPlayerTalentDefinition = {
	id: 204,
	caption: 'Overmind (+1 Droid Speed)',
	learnable: true,
	description: 'Have a level 4 Droid in your Unit Bay',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getMaxUnitLevel(getPlayerUnitsAtBayByChassis(p, UnitChassis.chassisClass.droid)) / 4);
	},
	unitModifier: {
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					speed: {
						bias: 1,
						minGuard: ArithmeticPrecision,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[HunterChassis.id]: 0.5,
		},
		factories: {
			[CyberforgeFactory.id]: 0.7,
		},
	},
};

export const PlayerTalent_MissileMastery: TPlayerTalentDefinition = {
	id: 210,
	caption: 'Missile Mastery (+0.5 Cruise Missiles and Homing Missiles  Range)',
	learnable: false,
	description: 'Deal 1500 damage with Homing or Cruise Missiles',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				[Weapons.TWeaponGroupType.Missiles, Weapons.TWeaponGroupType.Cruise].map((k) =>
					getPlayerStatProperty(p, 'damageDoneByWeaponTypes', k),
				),
			) / 1500,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						baseRange: {
							bias: 0.5,
						},
					},
					[Weapons.TWeaponGroupType.Cruise]: {
						baseRange: {
							bias: 0.5,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[SeekerMissile.id]: 0.3,
		},
	},
};

export const PlayerTalent_GunMastery: TPlayerTalentDefinition = {
	id: 211,
	caption: 'Machine Gun Mastery (+2 Machine Guns Damage)',
	learnable: false,
	description: 'Deal 1000 Damage with Machine Guns',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Guns) / 1000);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						damageModifier: {
							bias: 2,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[RapidGun.id]: 0.3,
		},
	},
};

export const PlayerTalent_RailgunMastery: TPlayerTalentDefinition = {
	id: 212,
	caption: 'Railgun Mastery (+20% Hit Chance for Mass Drivers)',
	learnable: false,
	description: 'Deal 1000 Damage with Mass Drivers',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Railguns) / 1500,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseAccuracy: {
							bias: 0.2,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[RapidCoilGun.id]: 0.3,
		},
	},
};

export const PlayerTalent_LaserMastery: TPlayerTalentDefinition = {
	id: 213,
	caption: 'Laser Mastery (+0.5 Laser Range)',
	learnable: false,
	description: 'Deal 1000 Damage with Lasers',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getStatProperty(p?.stats, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Lasers) / 1000,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseRange: {
							bias: 0.5,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[PrismGun.id]: 0.3,
		},
	},
};

export const PlayerTalent_BallisticMastery: TPlayerTalentDefinition = {
	id: 214,
	caption: 'Ballistic Mastery (+20% Cannons Hit Chance)',
	learnable: false,
	description: 'Deal 1500 damage with Cannon Weapons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Cannons) / 1000,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseAccuracy: {
							bias: 0.2,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[ExplosiveShells.id]: 0.3,
		},
	},
};

export const PlayerTalent_LauncherMastery: TPlayerTalentDefinition = {
	id: 215,
	caption: 'Launcher Mastery (+10% Launcher Damage)',
	learnable: false,
	description: 'Deal 1500 damage with Launcher Weapons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Launchers) / 1500,
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						damageModifier: {
							factor: 1.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[GravMortarWeapon.id]: 0.3,
		},
	},
};

export const PlayerTalent_MasterArmsman: TPlayerTalentDefinition = {
	id: 216,
	caption: 'Master Armsman (+2% damage)',
	learnable: true,
	description: 'Deal 10000 Damage with Weapons',
	prerequisiteTalentIds: [
		PlayerTalent_BallisticMastery.id,
		PlayerTalent_GunMastery.id,
		PlayerTalent_LaserMastery.id,
		PlayerTalent_LauncherMastery.id,
		PlayerTalent_RailgunMastery.id,
		PlayerTalent_MissileMastery.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes') / 10000);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.02,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[SharpShooterModule.id]: 0.1,
		},
	},
};

export const PlayerTalent_Demoman: TPlayerTalentDefinition = {
	id: 220,
	caption: 'Demolition man (+5% Heat Damage Reduction on Armor)',
	learnable: true,
	description: 'Deal 2000 Heat damage to Hit Points',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'healthDamageDoneByType', Damage.damageType.heat) / 2000);
	},
	unitModifier: {
		default: {
			armor: [
				{
					[Damage.damageType.heat]: {
						factor: 0.95,
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[NapalmMissile.id]: 0.3,
		},
	},
};

export const PlayerTalent_ArmorMastery: TPlayerTalentDefinition = {
	id: 221,
	caption: 'Armor Mastery (-5% Armor Cost)',
	learnable: false,
	description: 'Deal 5000 damage to Armor',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneToArmor') / 5000);
	},
	unitModifier: {
		default: {
			armor: [
				{
					baseCost: {
						factor: 0.95,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[AugmentedFrameEquipment.id]: 0.1,
		},
	},
};

export const PlayerTalent_ShieldMastery: TPlayerTalentDefinition = {
	id: 222,
	caption: 'Shield Mastery (-5% Shield Cost)',
	learnable: false,
	description: 'Deal 3500 damage to Shields',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneToShields') / 3500);
	},
	unitModifier: {
		default: {
			shield: [
				{
					baseCost: {
						factor: 0.95,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[FlowDisruptorEquipment.id]: 0.1,
		},
	},
};

export const PlayerTalent_Groundbreaker: TPlayerTalentDefinition = {
	id: 223,
	caption: 'Groundbreaker (+10 Production Capacity at Factories)',
	learnable: true,
	description: 'Traverse 10000 Distance on ground',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'distanceCoveredByMovementType', UnitChassis.movementType.ground) / 10000,
		);
	},
	baseModifier: {
		productionCapacity: {
			bias: 10,
		},
	},
	technologyReward: {
		equipment: {
			[PolarisOmniTool.id]: 0.1,
		},
	},
};

export const PlayerTalent_Nailwalker: TPlayerTalentDefinition = {
	id: 230,
	caption: 'Nailwalker (+15% Dodge vs Surface Attacks)',
	learnable: true,
	description: 'Have a unit that lives for 10 turns without taking damage',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, p?.stats?.turnsRowWithoutTakingDamage / 10 || 0);
	},
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						[UnitAttack.deliveryType.surface]: {
							bias: 0.15,
						},
					},
				},
			],
		},
	},
};

export const PlayerTalent_Steward: TPlayerTalentDefinition = {
	id: 231,
	caption: 'Steward (-5% Infantry, Scout and Droid Cost)',
	learnable: true,
	description: 'Win 15 Battles',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, p?.stats?.battlesWon / 15 || 0);
	},
	unitModifier: {
		[UnitChassis.chassisClass.infantry]: {
			chassis: [
				{
					baseCost: {
						factor: 0.95,
					},
				},
			],
		},
		[UnitChassis.chassisClass.droid]: {
			chassis: [
				{
					baseCost: {
						factor: 0.95,
					},
				},
			],
		},
		[UnitChassis.chassisClass.scout]: {
			chassis: [
				{
					baseCost: {
						factor: 0.95,
					},
				},
			],
		},
	},
	technologyReward: {
		establishments: {
			[AmphitheatreEstablishment.id]: 1,
		},
	},
};

export const PlayerTalent_Scrapper: TPlayerTalentDefinition = {
	id: 232,
	caption: 'Scrapper (+5% Repair Output)',
	learnable: true,
	description: 'Produce 20 units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'unitsProducedByChassisId') / 20);
	},
	baseModifier: {
		repairOutput: {
			factor: 1.05,
		},
	},
	technologyReward: {
		establishments: {
			[CampusEstablishment.id]: 1,
		},
	},
};

export const PlayerTalent_Wiseman: TPlayerTalentDefinition = {
	id: 233,
	caption: 'Wiseman (+1 Recon Level)',
	learnable: true,
	description: 'Have a Unit at Bay that previously survived 10 turns without taking damage',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Math.max(
				...getPlayerUnitsAtBayByChassis(p).map(
					(u) => getUnitStatProperty(u, 'turnsRowWithoutTakingDamage') || 0,
				),
			) / 10,
		);
	},
	baseModifier: {
		intelligenceLevel: {
			bias: 1,
		},
	},
	technologyReward: {
		establishments: {
			[InvestigationBureauEstablishment.id]: 1,
		},
	},
};

export const PlayerTalent_Suprematist: TPlayerTalentDefinition = {
	id: 234,
	caption: 'Suprematist (+1 Counter-Recon Level)',
	learnable: true,
	description: 'Be a target of an enemy Abilities 25 times',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'spellsTaken') / 25);
	},
	baseModifier: {
		counterIntelligenceLevel: {
			bias: 1,
		},
	},
	technologyReward: {
		chassis: {
			[SupportVehicleChassis.id]: 0.5,
		},
		equipment: {
			[MainframeTacticalModule.id]: 0.1,
		},
	},
};

export const PlayerTalent_Protector: TPlayerTalentDefinition = {
	id: 240,
	learnable: true,
	caption: 'Protector (+35 Base Hit Points)',
	description: 'Dodge 100 Attacks',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'attacksDodgedByType') / 100);
	},
	baseModifier: {
		maxHitPoints: {
			bias: 35,
		},
	},
	technologyReward: {
		armor: {
			[KevlarPlatingArmor.id]: 0.3,
		},
		shield: {
			[RepulsiveShield.id]: 0.3,
		},
	},
};

export const PlayerTalent_Survivor: TPlayerTalentDefinition = {
	id: 241,
	learnable: true,
	caption: 'Survivor (+1 Ability Slot)',
	description: 'Take 2500 Damage',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageTakenByType') / 2500);
	},
	reward(p: Players.IPlayer) {
		p.abilitySlots += 1;
		return p;
	},
	technologyReward: {
		armor: {
			[KevlarArmor.id]: 0.3,
		},
		shield: {
			[BulwarkShield.id]: 0.3,
		},
	},
};

export const PlayerTalent_Manufacturer: TPlayerTalentDefinition = {
	id: 242,
	learnable: true,
	caption: 'Manufacturer (Seize Droids Ability)',
	description: 'Build 4 Factories',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'factoriesBuilt') / 4);
	},
	reward(p: Players.IPlayer) {
		return p.learnAbility(JeopardyAbility.id);
	},
	technologyReward: {
		armor: {
			[LargeKevlarArmor.id]: 0.3,
		},
		shield: {
			[PhasingShield.id]: 0.3,
		},
	},
};

export const PlayerTalent_Challenger: TPlayerTalentDefinition = {
	id: 243,
	learnable: true,
	caption: 'Challenger (Exploit Ability)',
	description: 'Pick 25 Salvage',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'salvagePicked') / 25);
	},
	reward(p: Players.IPlayer) {
		return p.learnAbility(ExploitAbility.id);
	},
	technologyReward: {
		equipment: {
			[PlanetaryAnchorEquipment.id]: 0.1,
			[RecoilCapacitorEquipment.id]: 0.1,
		},
		establishments: {
			[ScienceLabEstablishment.id]: 1,
		},
	},
};

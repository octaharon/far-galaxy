import objectPath from 'object-path';
import Series from '../../../../maths/Series';
import BraintankEstablishment from '../../../../orbital/establishments/all/Braintank.101';
import DataWarehouseEstablishment from '../../../../orbital/establishments/all/DataWarehouse.202';
import NanocellHullEstablishment from '../../../../orbital/establishments/all/NanocellHull.300';
import ReconnaissanceAgencyEstablishment from '../../../../orbital/establishments/all/ReconnaissanceAgency.201';
import ChargeDepotFacility from '../../../../orbital/facilities/all/ChargeDepot.305';
import CompostureFacility from '../../../../orbital/facilities/all/Composture.201';
import GarageFacility from '../../../../orbital/facilities/all/Garage.401';
import IncinerationPlantFacility from '../../../../orbital/facilities/all/IncinerationPlant.300';
import MineralRefineryFacility from '../../../../orbital/facilities/all/MineralRefinery.200';
import SiloFacility from '../../../../orbital/facilities/all/Silo.400';
import SolarCollectorFacility from '../../../../orbital/facilities/all/SolarCollector.301';
import AutomatedAssemblyFactory from '../../../../orbital/factories/all/AutomatedAssemblyYard.203';
import { BaseBuildingOrder } from '../../../../orbital/types';
import { getPlayerStatProperty, getTotalStatValue } from '../../../../stats/helpers';
import { AbilityPowerUIToken } from '../../../../tactical/statuses/constants';
import LargeNanoArmor from '../../../../technologies/armor/all/large/LargeNanoArmor.30001';
import NanoArmor from '../../../../technologies/armor/all/medium/NanoArmor.20001';
import ReactiveArmor from '../../../../technologies/armor/all/medium/ReactiveArmor.20008';
import NanoPlatingArmor from '../../../../technologies/armor/all/small/NanoPlating.10001';
import SmallReactiveArmor from '../../../../technologies/armor/all/small/SmallReactiveArmor.10005';
import ArtilleryChassis from '../../../../technologies/chassis/all/Artillery/ArtilleryPlatform.30000';
import ColossusArtilleryChassis from '../../../../technologies/chassis/all/Artillery/Colossus.30002';
import BiotechChassis from '../../../../technologies/chassis/all/BioTech/Biotech.40000';
import GolliwogChassis from '../../../../technologies/chassis/all/BioTech/Gollywog.40003';
import CombatDroneChassis from '../../../../technologies/chassis/all/CombatDrone/CombatDrone.20000';
import CombatMechChassis from '../../../../technologies/chassis/all/CombatMech/CombatMech.18000';
import BarkerChassis from '../../../../technologies/chassis/all/Exoskeleton/Barker.15003';
import LAVChassis from '../../../../technologies/chassis/all/LAV/LAV.37000';
import PointDefenseChassis from '../../../../technologies/chassis/all/PointDefense/PointDefense.57000';
import SentinelTurrentChassis from '../../../../technologies/chassis/all/PointDefense/SentinelTurret.57004';
import RoverChassis from '../../../../technologies/chassis/all/Rover/Rover.50000';
import LeechChassis from '../../../../technologies/chassis/all/Scout/Leech.32003';
import TankChassis from '../../../../technologies/chassis/all/Tank/Tank.36000';
import BioticAmpEquipment from '../../../../technologies/equipment/all/Defensive/BioticAmp.20013';
import FusionCoreEquipment from '../../../../technologies/equipment/all/Enhancement/FusionCore.20012';
import AugmentedFrameEquipment from '../../../../technologies/equipment/all/Enhancement/AugmentedFrame.20006';
import HiSecTrackingModuleEquipment from '../../../../technologies/equipment/all/Tactical/HiSecTrackingModule.10001';
import MobileHQEquipment from '../../../../technologies/equipment/all/Tactical/MobileHQ.30008';
import AutomatedTurretEquipment from '../../../../technologies/equipment/all/Warfare/AutomatedTurret.30004';
import DoomsdayDeviceEquipment from '../../../../technologies/equipment/all/Warfare/DoomsdayDevice.30002';
import NanofoilCoatingEquipment from '../../../../technologies/equipment/all/Warfare/NanofoilCoating.30009';
import TargaShield from '../../../../technologies/shields/all/large/TargaShield.30001';
import CarapaceShield from '../../../../technologies/shields/all/medium/CarapaceShield.20003';
import ProactiveShield from '../../../../technologies/shields/all/medium/ProactiveShield.20001';
import AegisShield from '../../../../technologies/shields/all/small/AegisShield.10002';
import AShield from '../../../../technologies/shields/all/small/AShield.10009';
import HystrixShield from '../../../../technologies/shields/all/small/HystrixShield.10003';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AACannon from '../../../../technologies/weapons/all/antiair/AACannon.70000';
import MicrowavePhaseArrayWeapon from '../../../../technologies/weapons/all/beam/MWPA.80000';
import CumulativeShells from '../../../../technologies/weapons/all/cannons/CumulativeShells.10002';
import HeavyArtillery from '../../../../technologies/weapons/all/cannons/HeavyArtillery.10010';
import GoliathMissile from '../../../../technologies/weapons/all/guided/Goliath.35004';
import GovernorGun from '../../../../technologies/weapons/all/lasers/Governor.90003';
import APGun from '../../../../technologies/weapons/all/machineguns/APGun.20001';
import ShardCannon from '../../../../technologies/weapons/all/machineguns/ShardCannon.20004';
import TripleBarrelMachineGun from '../../../../technologies/weapons/all/machineguns/TBMG.20005';
import SwarmMissile from '../../../../technologies/weapons/all/missiles/SwarmMissile.30006';
import TacticalMissile from '../../../../technologies/weapons/all/missiles/TacticalMissile.30003';
import PlasmaGun from '../../../../technologies/weapons/all/plasma/PlasmaGun.50000';
import CorrosiveMines from '../../../../technologies/weapons/all/projectiles/CorrosiveMines.15002';
import CoilMortar from '../../../../technologies/weapons/all/railguns/CoilMortar.25004';
import RocketArtillery from '../../../../technologies/weapons/all/rockets/RocketArtillery.38000';
import { PLAYER_TALENT_TIER_LEVEL } from '../../../constants';
import Players from '../../../types';
import { TPlayerTalentDefinition } from '../../types';
import {
	PlayerTalent_ArmorMastery,
	PlayerTalent_DroidCommander,
	PlayerTalent_Overmind,
	PlayerTalent_Protector,
	PlayerTalent_ShieldMastery,
} from './Tier1';

export const PlayerTalent_General: TPlayerTalentDefinition = {
	id: 300,
	caption: 'General (+1 Armor Damage Reduction)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 5000 damage to Mechanical Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneToTargetType', 'default') / 5000);
	},
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						bias: -1,
						minGuard: 0,
						min: 0,
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[GarageFacility.id]: 0.5,
		},
		chassis: {
			[LAVChassis.id]: 0.5,
			[TankChassis.id]: 0.5,
			[LeechChassis.id]: 0.5,
		},
	},
};

export const PlayerTalent_Navigator: TPlayerTalentDefinition = {
	id: 301,
	caption: 'Navigator (+0.5 Scan Range)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 5000 damage to Robotic Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneToTargetType', UnitChassis.targetType.robotic) / 5000);
	},
	unitModifier: {
		default: {
			chassis: [
				{
					scanRange: {
						bias: 0.5,
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[SolarCollectorFacility.id]: 0.5,
		},
		chassis: {
			[CombatDroneChassis.id]: 0.5,
			[PointDefenseChassis.id]: 0.5,
			[SentinelTurrentChassis.id]: 1,
		},
	},
};

export const PlayerTalent_Dictator: TPlayerTalentDefinition = {
	id: 302,
	caption: 'Dictator (+5 Hit Points)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 5000 damage to Organic Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneToTargetType', UnitChassis.targetType.organic) / 5000);
	},
	unitModifier: {
		default: {
			chassis: [
				{
					hitPoints: {
						bias: 5,
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[ChargeDepotFacility.id]: 0.5,
		},
		chassis: {
			[BarkerChassis.id]: 0.5,
			[CombatMechChassis.id]: 0.5,
			[GolliwogChassis.id]: 1,
			[BiotechChassis.id]: 0.5,
		},
	},
};

export const PlayerTalent_Industrious: TPlayerTalentDefinition = {
	id: 303,
	caption: 'Industrious (+3% Material Output)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Produce 50 units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getTotalStatValue(p?.stats?.unitsProducedByChassisId) / 50);
	},
	baseModifier: {
		materialOutput: {
			factor: 1.03,
		},
	},
	technologyReward: {
		factories: {
			[AutomatedAssemblyFactory.id]: 0.5,
		},
		shield: {
			[HystrixShield.id]: 0.3,
			[ProactiveShield.id]: 0.3,
		},
		facilities: {
			[MineralRefineryFacility.id]: 0.5,
		},
	},
};

export const PlayerTalent_Prudent: TPlayerTalentDefinition = {
	id: 304,
	caption: 'Prudent (+3% Energy Output)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Spend 5000 Raw Materials',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'rawMaterialsSpent') / 5000);
	},
	baseModifier: {
		energyOutput: {
			factor: 1.03,
		},
	},
	technologyReward: {
		weapons: {
			[TacticalMissile.id]: 0.3,
		},
		facilities: {
			[SiloFacility.id]: 0.5,
			[CompostureFacility.id]: 0.5,
		},
		equipment: {
			[AutomatedTurretEquipment.id]: 0.1,
		},
	},
};

export const PlayerTalent_Scholar: TPlayerTalentDefinition = {
	id: 305,
	caption: 'Scholar (+3% Research Output)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Obtain 30 different blueprints',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, p.getKnownTechnologyCount() / 30);
	},
	baseModifier: {
		researchOutput: {
			factor: 1.03,
		},
	},
	technologyReward: {
		establishments: {
			[BraintankEstablishment.id]: 0.5,
		},
		facilities: {
			[IncinerationPlantFacility.id]: 0.5,
			[GarageFacility.id]: 0.5,
		},
	},
};

export const PlayerTalent_Governor: TPlayerTalentDefinition = {
	id: 306,
	caption: 'Governor (+3% Engineering Output)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Have a base with 3 buildings of each type',
	achievement: (p: Players.IPlayer) => {
		const count = BaseBuildingOrder.map((type) =>
			Math.min(0, ...Object.values(p?.bases || {}).map((b) => b.getBuildingCount(type))),
		);
		return Series.sum(count) / 9;
	},
	baseModifier: {
		engineeringOutput: {
			factor: 1.03,
		},
	},
	technologyReward: {
		equipment: {
			[MobileHQEquipment.id]: 0.1,
		},
		establishments: {
			[ReconnaissanceAgencyEstablishment.id]: 0.5,
		},
		armor: {
			[SmallReactiveArmor.id]: 0.3,
			[ReactiveArmor.id]: 0.3,
		},
	},
};

export const PlayerTalent_Netrunner: TPlayerTalentDefinition = {
	id: 307,
	caption: `Netrunner (+0.25 Unit ${AbilityPowerUIToken})`,
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Use Abilities 100 times',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			(getPlayerStatProperty(p, 'defensiveAbilitiesCast') + getPlayerStatProperty(p, 'offensiveAbilitiesCast')) /
				100,
		);
	},
	unitModifier: {
		default: {
			unitModifier: {
				abilityPowerModifier: [
					{
						bias: 0.25,
					},
				],
			},
		},
	},
	technologyReward: {
		establishments: {
			[DataWarehouseEstablishment.id]: 0.5,
		},
		equipment: {
			[HiSecTrackingModuleEquipment.id]: 0.1,
		},
	},
};

export const PlayerTalent_Rocketeer: TPlayerTalentDefinition = {
	id: 310,
	caption: 'Rocketeer (+5% Missiles Damage)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 5000 damage with Missiles',
	achievement: (p: Players.IPlayer) => {
		return (
			Math.min(
				1,
				Series.sum(
					[Weapons.TWeaponGroupType.Missiles, Weapons.TWeaponGroupType.Cruise].map((type) =>
						getPlayerStatProperty(p, 'damageDoneByWeaponTypes', type),
					),
				),
			) / 5000
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Missiles]: {
						damageModifier: {
							factor: 1.05,
						},
					},
					[Weapons.TWeaponGroupType.Cruise]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[SwarmMissile.id]: 0.3,
			[GoliathMissile.id]: 0.3,
		},
	},
};

export const PlayerTalent_Jolter: TPlayerTalentDefinition = {
	id: 311,
	caption: 'Major Laser (+5% Laser Attack Rate)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 3500 damage with Lasers',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Lasers)) / 3500;
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Lasers]: {
						baseRate: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[MicrowavePhaseArrayWeapon.id]: 0.3,
			[GovernorGun.id]: 0.3,
		},
	},
};

export const PlayerTalent_Skirmisher: TPlayerTalentDefinition = {
	id: 312,
	caption: 'Skirmisher (+5% Machine Gun Hit Chance)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 3500 damage with Machine Guns',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Guns)) / 3500;
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Guns]: {
						baseAccuracy: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[AACannon.id]: 0.3,
			[TripleBarrelMachineGun.id]: 0.3,
		},
	},
};

export const PlayerTalent_Sniper: TPlayerTalentDefinition = {
	id: 313,
	caption: 'Sniper (+5% Railgun Attack Rate)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 3500 damage with Railguns',
	achievement: (p: Players.IPlayer) => {
		return (
			Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Railguns)) / 3500
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Railguns]: {
						baseRate: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[PlasmaGun.id]: 0.3,
			[CoilMortar.id]: 0.3,
		},
	},
};

export const PlayerTalent_Artillerist: TPlayerTalentDefinition = {
	id: 314,
	caption: 'Artillerist (+0.25 Launchers AoE Range)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 5000 damage with Launchers',
	achievement: (p: Players.IPlayer) => {
		return (
			Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Launchers)) / 5000
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						aoeRadiusModifier: {
							bias: 0.25,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[HeavyArtillery.id]: 0.3,
		},
		chassis: {
			[ArtilleryChassis.id]: 0.5,
			[ColossusArtilleryChassis.id]: 1,
		},
	},
};

export const PlayerTalent_Fusilier: TPlayerTalentDefinition = {
	id: 315,
	caption: 'Fusilier (+5% Cannon Damage)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deal 5000 damage with Cannons',
	achievement: (p: Players.IPlayer) => {
		return (
			Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Cannons)) / 5000
		);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Launchers]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[RocketArtillery.id]: 0.3,
		},
		equipment: {
			[DoomsdayDeviceEquipment.id]: 0.1,
		},
	},
};

export const PlayerTalent_Defender: TPlayerTalentDefinition = {
	id: 320,
	caption: 'Defender (+5% Shield Capacity)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Lose 5000 Shield Capacity',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'shieldsLost') / 5000);
	},
	prerequisiteTalentIds: [PlayerTalent_Protector.id, PlayerTalent_ShieldMastery.id],
	unitModifier: {
		default: {
			shield: [
				{
					shieldAmount: {
						factor: 1.05,
					},
				},
			],
		},
	},
	technologyReward: {
		shield: {
			[AegisShield.id]: 0.3,
			[TargaShield.id]: 0.3,
			[CarapaceShield.id]: 0.3,
		},
	},
};

export const PlayerTalent_Endurant: TPlayerTalentDefinition = {
	id: 321,
	caption: 'Endurant (+3% Hit Points)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Lose 10000 Hit Points',
	prerequisiteTalentIds: [PlayerTalent_Protector.id, PlayerTalent_ArmorMastery.id],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'healthLost') / 10000);
	},
	unitModifier: {
		default: {
			chassis: [
				{
					hitPoints: {
						factor: 1.05,
					},
				},
			],
		},
	},
	technologyReward: {
		armor: {
			[NanoPlatingArmor.id]: 0.3,
			[NanoArmor.id]: 0.3,
			[LargeNanoArmor.id]: 0.3,
		},
	},
};

export const PlayerTalent_Daredevil: TPlayerTalentDefinition = {
	id: 322,
	caption: 'Daredevil (+5% Scan Range)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Deploy Units worth of 7000 Raw Materials',
	prerequisiteTalentIds: [PlayerTalent_Overmind.id, PlayerTalent_DroidCommander.id],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'totalDeployedUnitsCost') / 7000);
	},
	unitModifier: {
		default: {
			chassis: [
				{
					scanRange: {
						factor: 1.05,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[RoverChassis.id]: 0.5,
		},
		equipment: {
			[NanofoilCoatingEquipment.id]: 0.1,
		},
		weapons: {
			[APGun.id]: 0.3,
		},
	},
};

export const PlayerTalent_ExpertArmsman: TPlayerTalentDefinition = {
	id: 350,
	caption: 'Expert Armsman (+5% damage)',
	learnable: true,
	description: 'Deal 25000 Damage with Weapons',
	prerequisiteTalentIds: [
		PlayerTalent_Rocketeer.id,
		PlayerTalent_Jolter.id,
		PlayerTalent_Skirmisher.id,
		PlayerTalent_Artillerist.id,
		PlayerTalent_Sniper.id,
		PlayerTalent_Fusilier.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes') / 25000);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		weapons: {
			[CumulativeShells.id]: 0.3,
			[CorrosiveMines.id]: 0.3,
			[ShardCannon.id]: 0.3,
		},
	},
};

export const PlayerTalent_ExpertArmorsmith: TPlayerTalentDefinition = {
	id: 351,
	caption: 'Risk Manager (+5% Dodge)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL,
	description: 'Lose 50 units',
	prerequisiteTalentIds: [PlayerTalent_Defender.id, PlayerTalent_Endurant.id],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, objectPath.get(p, ['stats', 'unitsLost'], 0) / 50);
	},
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		shield: {
			[AShield.id]: 0.3,
		},
		establishments: {
			[NanocellHullEstablishment.id]: 0.5,
		},
		equipment: {
			[BioticAmpEquipment.id]: 0.1,
			[AugmentedFrameEquipment.id]: 0.1,
			[FusionCoreEquipment.id]: 0.1,
		},
	},
};

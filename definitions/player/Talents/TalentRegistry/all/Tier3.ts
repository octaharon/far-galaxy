import DIContainer from '../../../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../core/DI/injections';
import Series from '../../../../maths/Series';
import { getPlayerUnitsAtBayByChassis } from '../../../../orbital/base/helpers';
import GrinderEstablishment from '../../../../orbital/establishments/all/Grinder.402';
import HolostageEstablishment from '../../../../orbital/establishments/all/Holostage.501';
import NanocellHullEstablishment from '../../../../orbital/establishments/all/NanocellHull.300';
import NeutrinoHullEstablishment from '../../../../orbital/establishments/all/NeutrinoHull.302';
import ObservationDeckEstablishment from '../../../../orbital/establishments/all/ObservationDeck.502';
import ProvingGroundEstablishment from '../../../../orbital/establishments/all/ProvingGround.401';
import ReactiveHullEstablishment from '../../../../orbital/establishments/all/ReactiveHull.301';
import AtomicPileFacility from '../../../../orbital/facilities/all/AtomicPile.302';
import AxialCompressorFacility from '../../../../orbital/facilities/all/AxialCompressor.203';
import CisternFacility from '../../../../orbital/facilities/all/Cistern.402';
import ColdFusionReactorFacility from '../../../../orbital/facilities/all/ColdFusionReactor.303';
import SmelteryFacility from '../../../../orbital/facilities/all/Smeltery.202';
import VacuumCleanerFacility from '../../../../orbital/facilities/all/VacuumCleaner.100';
import WorkshopFacility from '../../../../orbital/facilities/all/Workshop.403';
import AeronauticalComplexFactory from '../../../../orbital/factories/all/AeronauticalComplex.500';
import CloningVatsFactory from '../../../../orbital/factories/all/CloningVats.105';
import HeavyVehicleFactory from '../../../../orbital/factories/all/HeavyVehicleFactory.202';
import MilitaryUniversityFactory from '../../../../orbital/factories/all/MilitaryUniversity.102';
import NexusFoundryFactory from '../../../../orbital/factories/all/NexusFoundry.302';
import SelfMaintainedFactory from '../../../../orbital/factories/all/SelfMaintainedFactory.303';
import WarfareMultiplex from '../../../../orbital/factories/all/WarfareMultiplex.205';
import WaterDockFactory from '../../../../orbital/factories/all/WaterDock.400';
import { IPrototypeModifier } from '../../../../prototypes/types';
import { getPlayerStatProperty } from '../../../../stats/helpers';
import SegmentationFaultAbility from '../../../../tactical/abilities/all/Player/SegmentationFault.60000';
import Damage from '../../../../tactical/damage/damage';
import { AbilityPowerUIToken } from '../../../../tactical/statuses/constants';
import { getMaxUnitLevel } from '../../../../tactical/units/helpers';
import LargeGluonArmor from '../../../../technologies/armor/all/large/LargeGluonArmor.30005';
import LargeReactiveArmor from '../../../../technologies/armor/all/large/LargeReactiveArmor.30006';
import GluonArmor from '../../../../technologies/armor/all/medium/GluonArmor.20009';
import PowerArmor from '../../../../technologies/armor/all/small/PowerArmor.10004';
import SmallGluonArmor from '../../../../technologies/armor/all/small/SmallGluonArmor.10008';
import ChaserChassis from '../../../../technologies/chassis/all/CombatDrone/Chaser.20001';
import SCVChassis from '../../../../technologies/chassis/all/CombatMech/SCV.18003';
import CorvetteChassis from '../../../../technologies/chassis/all/Corvette/Corvette.60000';
import StalkerChassis from '../../../../technologies/chassis/all/Exoskeleton/Stalker.15002';
import HAVChassis from '../../../../technologies/chassis/all/HAV/HAV.33000';
import HovertankChassis from '../../../../technologies/chassis/all/HoverTank/Hovertank.55000';
import XDriverChassis from '../../../../technologies/chassis/all/Infantry/XDriver.10002';
import LumberjackChassis from '../../../../technologies/chassis/all/LAV/Lumberjack.37001';
import AATurretChassis from '../../../../technologies/chassis/all/PointDefense/AATurret.57003';
import VultureChassis from '../../../../technologies/chassis/all/Rover/Vulture.50001';
import ScienceVesselChassis from '../../../../technologies/chassis/all/ScienceVessel/ScienceVessel.99000';
import SupportAircraftChassis from '../../../../technologies/chassis/all/SupportAircraft/SupportAircraft.81000';
import SupportShipChassis from '../../../../technologies/chassis/all/SupportShip/SupportShip.61000';
import DragoonChassis from '../../../../technologies/chassis/all/Tank/Dragoon.36002';
import TiltjetChassis from '../../../../technologies/chassis/all/Titljet/Tiltjet.84000';
import { groundChassisList, hoverChassisList } from '../../../../technologies/chassis/constants';
import ECCMModuleEquipment from '../../../../technologies/equipment/all/Defensive/ECCMModule.10002';
import DianodeEquipment from '../../../../technologies/equipment/all/Enhancement/Dianode.20019';
import MioplasmaticReflectorEquipment from '../../../../technologies/equipment/all/Enhancement/MioplasmaticReflector.20020';
import PowerRecirculatorEquipment from '../../../../technologies/equipment/all/Enhancement/PowerRecirculator.20016';
import TiberiumBatteryEquipment from '../../../../technologies/equipment/all/Enhancement/TiberiumBattery.20010';
import BallisticGuidanceModule from '../../../../technologies/equipment/all/Offensive/BallisticGuidanceModule.10005';
import EleriumBatteryEquipment from '../../../../technologies/equipment/all/Offensive/EleriumBattery.20008';
import GroundingDeviceEquipment from '../../../../technologies/equipment/all/Support/GroundingDevice.20023';
import OrbitalSupportModuleEquipment from '../../../../technologies/equipment/all/Support/OrbitalSupportModule.10003';
import DefenseNetworkEquipment from '../../../../technologies/equipment/all/Tactical/DefenseNetwork.10008';
import OlympiusReactorEquipment from '../../../../technologies/equipment/all/Warfare/OlympiusReactor.20025';
import ClusterShield from '../../../../technologies/shields/all/large/ClusterShield.30013';
import LargePlasmaShield from '../../../../technologies/shields/all/large/LargePlasmaShield.30005';
import HardenedShield from '../../../../technologies/shields/all/medium/HardenedShield.20011';
import PlasmaShield from '../../../../technologies/shields/all/medium/PlasmaShield.20005';
import SmallPlasmaShield from '../../../../technologies/shields/all/small/SmallPlasmaShield.10005';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import AAMissile from '../../../../technologies/weapons/all/antiair/AAMissile.70001';
import SAMDefence from '../../../../technologies/weapons/all/antiair/SAMDefence.70002';
import SolarBeam from '../../../../technologies/weapons/all/beam/SolarBeam.80005';
import ConventionalBomb from '../../../../technologies/weapons/all/bombs/ConventionalBomb.40000';
import NukeShells from '../../../../technologies/weapons/all/cannons/NukeShells.10005';
import PhoenixMissile from '../../../../technologies/weapons/all/guided/PhoenixMissile.35006';
import PoisonGun from '../../../../technologies/weapons/all/machineguns/PoisonGun.20013';
import FlamethrowerWeapon from '../../../../technologies/weapons/all/plasma/Flamethrower.50002';
import SS18Rocket from '../../../../technologies/weapons/all/rockets/SS-18.38002';
import Resources from '../../../../world/resources/types';
import { PLAYER_TALENT_TIER_LEVEL } from '../../../constants';
import Players from '../../../types';
import { TPlayerTalentDefinition } from '../../types';
import {
	PlayerTalent_Challenger,
	PlayerTalent_Chieftain,
	PlayerTalent_Manufacturer,
	PlayerTalent_Survivor,
} from './Tier1';
import {
	PlayerTalent_Daredevil,
	PlayerTalent_Dictator,
	PlayerTalent_ExpertArmorsmith,
	PlayerTalent_ExpertArmsman,
	PlayerTalent_General,
	PlayerTalent_Governor,
	PlayerTalent_Industrious,
	PlayerTalent_Navigator,
	PlayerTalent_Netrunner,
	PlayerTalent_Prudent,
	PlayerTalent_Scholar,
} from './Tier2';
import TPlayerUnitModifier = Players.TPlayerUnitModifier;

export const PlayerTalent_Castellan: TPlayerTalentDefinition = {
	id: 400,
	caption: 'Castellan (Gain 2 Damage Threshold on Shields and Armor)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Repair 7500 Hit Points',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'repairSpent') / 7500);
	},
	unitModifier: {
		default: {
			armor: [
				{
					default: {
						threshold: 2,
					},
				},
			],
			shield: [
				{
					default: {
						threshold: 2,
					},
				},
			],
		},
	},
	technologyReward: {
		armor: {
			[LargeReactiveArmor.id]: 0.3,
		},
		shield: {
			[ClusterShield.id]: 0.3,
		},
	},
};

export const PlayerTalent_Strategist: TPlayerTalentDefinition = {
	id: 401,
	caption: 'Strategist (+1 Artillery Scan Range, +0.5 Cannons range)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Kill 25 enemies with Artillery units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Object.keys(
				DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(
					UnitChassis.chassisClass.artillery,
				),
			).reduce((acc, id) => acc + getPlayerStatProperty(p, 'unitsKilledWithChassisId', id), 0) / 25,
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.artillery]: {
			chassis: [
				{
					scanRange: {
						bias: 1,
					},
				},
			],
		},
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Cannons]: {
						baseRange: {
							bias: 0.5,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[SmelteryFacility.id]: 0.5,
		},
		equipment: {
			[BallisticGuidanceModule.id]: 0.1,
		},
	},
};

export const PlayerTalent_Radiant: TPlayerTalentDefinition = {
	id: 410,
	learnable: false,
	caption: 'Radiant (+5% Energy Weapons Damage)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Deal 5000 Damage with Energy Weapons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Beam) / 5000);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Beam]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[PowerRecirculatorEquipment.id]: 0.1,
		},
		weapons: {
			[SolarBeam.id]: 0.3,
		},
	},
};

export const PlayerTalent_Desolator: TPlayerTalentDefinition = {
	id: 411,
	learnable: false,
	caption: 'Desolator (+5% Plasma Damage)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Deal 5000 Damage with Plasma Weapons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Plasma) / 5000);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Plasma]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[TiberiumBatteryEquipment.id]: 0.1,
		},
		weapons: {
			[FlamethrowerWeapon.id]: 0.3,
		},
	},
};

export const PlayerTalent_Outlander: TPlayerTalentDefinition = {
	id: 420,
	learnable: true,
	caption: 'Outlander (+15 Combat Drone and Rover Hit Points)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Kill 40 enemies with Combat Drones Or Rovers',
	achievement: (p: Players.IPlayer) => {
		return (
			Math.min(
				1,
				Series.sum(
					[UnitChassis.chassisClass.combatdrone, UnitChassis.chassisClass.rover]
						.flatMap((type) =>
							Object.keys(
								DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(type),
							),
						)
						.map((id) => getPlayerStatProperty(p, 'unitsKilledWithChassisId', id)),
				),
			) / 40
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					hitPoints: {
						bias: 15,
					},
				},
			],
		},
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					hitPoints: {
						bias: 15,
					},
				},
			],
		},
	},
	technologyReward: {
		equipment: {
			[GroundingDeviceEquipment.id]: 0.1,
		},
		chassis: {
			[VultureChassis.id]: 0.5,
			[ChaserChassis.id]: 0.5,
		},
	},
};

export const PlayerTalent_Grinder: TPlayerTalentDefinition = {
	id: 421,
	learnable: true,
	caption: 'Grinder (+5 Armor Absorption for Point Defense units)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Kill 25 enemies with Point Defense',
	achievement: (p: Players.IPlayer) => {
		return (
			Math.min(
				1,
				Series.sum(
					[UnitChassis.chassisClass.pointdefense]
						.flatMap((type) =>
							Object.keys(
								DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(type),
							),
						)
						.map((id) => getPlayerStatProperty(p, 'unitsKilledWithChassisId', id)),
				),
			) / 25
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.pointdefense]: {
			armor: [
				{
					default: {
						bias: -5,
					},
				},
			],
		},
	},
	technologyReward: {
		establishments: {
			[GrinderEstablishment.id]: 0.5,
		},
		chassis: {
			[AATurretChassis.id]: 0.5,
		},
		weapons: {
			[AAMissile.id]: 0.3,
		},
	},
};

export const PlayerTalent_Jaeger: TPlayerTalentDefinition = {
	id: 422,
	learnable: true,
	caption: 'Jaeger (+1 Exosuit and Combat Mech speed)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Kill 40 enemies with Exosuits and Combat Mechs',
	achievement: (p: Players.IPlayer) => {
		return (
			Math.min(
				1,
				Series.sum(
					[UnitChassis.chassisClass.exosuit, UnitChassis.chassisClass.mecha]
						.flatMap((type) =>
							Object.keys(
								DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(type),
							),
						)
						.map((id) => getPlayerStatProperty(p, 'unitsKilledWithChassisId', id)),
				),
			) / 40
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.exosuit]: {
			chassis: [
				{
					speed: {
						bias: 1,
					},
				},
			],
		},
		[UnitChassis.chassisClass.mecha]: {
			chassis: [
				{
					speed: {
						bias: 1,
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[CisternFacility.id]: 0.5,
		},
		chassis: {
			[StalkerChassis.id]: 0.5,
			[SCVChassis.id]: 0.5,
		},
	},
};

export const PlayerTalent_Highlander: TPlayerTalentDefinition = {
	id: 431,
	learnable: true,
	caption: 'Highlander (-10% Rover Cost)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Traverse 5000 units of distance with Hover Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'distanceCoveredByMovementType', UnitChassis.movementType.hover) / 5000,
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.rover]: {
			chassis: [
				{
					baseCost: {
						factor: 0.9,
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[HovertankChassis.id]: 0.3,
		},
		factories: {
			[SelfMaintainedFactory.id]: 0.5,
		},
	},
};

export const PlayerTalent_Wayfarer: TPlayerTalentDefinition = {
	id: 430,
	learnable: true,
	caption: 'Wayfarer (-5% Tank, HAV and LAV Cost)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Traverse 7500 units of distance with Ground Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getPlayerStatProperty(p, 'distanceCoveredByMovementType', UnitChassis.movementType.ground) / 7500,
		);
	},
	unitModifier: [UnitChassis.chassisClass.hav, UnitChassis.chassisClass.lav, UnitChassis.chassisClass.tank].reduce(
		(obj, chassis) =>
			Object.assign(obj, {
				[chassis]: {
					chassis: [
						{
							baseCost: {
								factor: 0.95,
							},
						},
					],
				} as IPrototypeModifier,
			}),
		{} as TPlayerUnitModifier,
	),
	technologyReward: {
		establishments: {
			[ProvingGroundEstablishment.id]: 0.5,
		},
		facilities: {
			[WorkshopFacility.id]: 0.5,
		},
	},
};

export const PlayerTalent_IntoTheSeas: TPlayerTalentDefinition = {
	id: 440,
	learnable: true,
	caption: 'Into the Seas (+200 XP)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Produce 75 Ground Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				groundChassisList
					.flatMap((type) =>
						Object.keys(
							DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(type),
						),
					)
					.map((id) => getPlayerStatProperty(p, 'unitsProducedByChassisId', id)),
			) / 75,
		);
	},
	reward: (p) => {
		return p.addXp(200);
	},
	prerequisiteTalentIds: [PlayerTalent_Wayfarer.id, PlayerTalent_Highlander.id],
	technologyReward: {
		chassis: {
			[CorvetteChassis.id]: 0.5,
			[SupportShipChassis.id]: 0.5,
		},
		factories: {
			[WaterDockFactory.id]: 0.5,
		},
		weapons: {
			[SAMDefence.id]: 0.3,
		},
	},
};

export const PlayerTalent_IntoTheSkies: TPlayerTalentDefinition = {
	id: 441,
	learnable: true,
	caption: 'Into the Skies (+200 XP)',
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	prerequisiteTalentIds: [PlayerTalent_Wayfarer.id, PlayerTalent_Highlander.id],
	description: 'Produce 50 Hover Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				hoverChassisList
					.flatMap((type) =>
						Object.keys(
							DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(type),
						),
					)
					.map((id) => getPlayerStatProperty(p, 'unitsProducedByChassisId', id)),
			) / 75,
		);
	},
	reward: (p) => {
		return p.addXp(200);
	},
	technologyReward: {
		chassis: {
			[TiltjetChassis.id]: 0.5,
			[SupportAircraftChassis.id]: 0.5,
		},
		factories: {
			[AeronauticalComplexFactory.id]: 0.5,
		},
		weapons: {
			[ConventionalBomb.id]: 0.3,
		},
	},
};

export const PlayerTalent_ScienceOverwhelming: TPlayerTalentDefinition = {
	id: 443,
	learnable: true,
	caption: `Science Overwhelming (+0.25 Base ${AbilityPowerUIToken})`,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Have a level 5 Support Unit at Bay',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			getMaxUnitLevel(
				getPlayerUnitsAtBayByChassis(p).filter((u) =>
					[
						UnitChassis.chassisClass.sup_vehicle,
						UnitChassis.chassisClass.sup_aircraft,
						UnitChassis.chassisClass.sup_ship,
					].includes(u.chassisType),
				),
			) / 5,
		);
	},
	baseModifier: {
		abilityPower: {
			bias: 0.25,
		},
	},
	technologyReward: {
		chassis: {
			[ScienceVesselChassis.id]: 0.5,
		},
		equipment: {
			[ECCMModuleEquipment.id]: 0.1,
			[DefenseNetworkEquipment.id]: 0.1,
		},
	},
};

export const PlayerTalent_Warlord: TPlayerTalentDefinition = {
	id: 450,
	caption: 'Warlord (+10 Ground Units health)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Have a level 8 Unit at bay',
	prerequisiteTalentIds: [
		PlayerTalent_General.id,
		PlayerTalent_Navigator.id,
		PlayerTalent_Dictator.id,
		PlayerTalent_Chieftain.id,
		PlayerTalent_ExpertArmorsmith.id,
		PlayerTalent_ExpertArmsman.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getMaxUnitLevel(getPlayerUnitsAtBayByChassis(p)) / 8);
	},
	unitModifier: groundChassisList.reduce(
		(obj, chassisType) =>
			Object.assign(obj, {
				[chassisType]: {
					chassis: [
						{
							hitPoints: {
								bias: 10,
							},
						},
					],
				} as IPrototypeModifier,
			}),
		{} as TPlayerUnitModifier,
	),
	technologyReward: {
		chassis: {
			[DragoonChassis.id]: 0.5,
			[LumberjackChassis.id]: 0.5,
			[HAVChassis.id]: 0.5,
		},
		factories: {
			[HeavyVehicleFactory.id]: 0.5,
			[MilitaryUniversityFactory.id]: 0.5,
		},
	},
};

export const PlayerTalent_Financier: TPlayerTalentDefinition = {
	id: 451,
	caption: 'Financier (+5% base Resource Output)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Gain 15 Building Upgrades',
	prerequisiteTalentIds: [
		PlayerTalent_Manufacturer.id,
		PlayerTalent_Scholar.id,
		PlayerTalent_Governor.id,
		PlayerTalent_Prudent.id,
		PlayerTalent_Daredevil.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'buildingUpgradesGained') / 15);
	},
	baseModifier: {
		resourceOutput: {
			default: {
				factor: 1.05,
			},
		},
	},
	technologyReward: {
		factories: {
			[CloningVatsFactory.id]: 0.5,
		},
		facilities: {
			[VacuumCleanerFacility.id]: 0.5,
		},
		establishments: {
			[ObservationDeckEstablishment.id]: 0.5,
		},
	},
};

export const PlayerTalent_BGH: TPlayerTalentDefinition = {
	id: 452,
	caption: 'Big Game Hunter (Segmentation Fault Ability)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Deal 5000 Damage to Massive Units',
	prerequisiteTalentIds: [
		PlayerTalent_Netrunner.id,
		PlayerTalent_Challenger.id,
		PlayerTalent_Survivor.id,
		PlayerTalent_Industrious.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneToTargetType', UnitChassis.targetType.massive) / 5000);
	},
	reward(p: Players.IPlayer): Players.IPlayer {
		return p.learnAbility(SegmentationFaultAbility.id);
	},
	technologyReward: {
		factories: {
			[WarfareMultiplex.id]: 0.5,
			[NexusFoundryFactory.id]: 0.5,
		},
		establishments: {
			[NanocellHullEstablishment.id]: 0.5,
			[HolostageEstablishment.id]: 0.5,
		},
	},
};

export const PlayerTalent_IntegratedDefense: TPlayerTalentDefinition = {
	id: 470,
	learnable: false,
	caption: `Integrated Defense (+45 Base Hit Points)`,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Take 5000 Damage of each type',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Damage.TDamageTypesArray.reduce(
				(value, dt) => Math.min(value, getPlayerStatProperty(p, 'damageTakenByType', dt)),
				Infinity,
			) / 5000,
		);
	},
	baseModifier: {
		maxHitPoints: {
			bias: 45,
		},
	},
	technologyReward: {
		equipment: {
			[OrbitalSupportModuleEquipment.id]: 0.1,
		},
		facilities: {
			[ColdFusionReactorFacility.id]: 0.5,
		},
		establishments: {
			[ReactiveHullEstablishment.id]: 0.5,
			[NeutrinoHullEstablishment.id]: 0.5,
		},
	},
};

export const PlayerTalent_GasGiant: TPlayerTalentDefinition = {
	id: 471,
	learnable: false,
	caption: `Gas Giant (+5% Hydrogen, Helium and Nitrogen Output)`,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Spend 50000 units of either Hydrogen, Helium or Nitrogen ',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			[Resources.resourceId.helium, Resources.resourceId.hydrogen, Resources.resourceId.nitrogen].reduce(
				(value, dt) => Math.max(value, getPlayerStatProperty(p, 'resourcesSpentByType', dt)),
				0,
			) / 50000,
		);
	},
	baseModifier: {
		resourceOutput: {
			[Resources.resourceId.helium]: {
				factor: 1.05,
			},
			[Resources.resourceId.hydrogen]: {
				factor: 1.05,
			},
			[Resources.resourceId.nitrogen]: {
				factor: 1.05,
			},
		},
	},
	technologyReward: {
		equipment: {
			[OlympiusReactorEquipment.id]: 0.1,
		},
		facilities: {
			[AxialCompressorFacility.id]: 0.5,
		},
		shield: {
			[SmallPlasmaShield.id]: 0.3,
			[PlasmaShield.id]: 0.3,
			[LargePlasmaShield.id]: 0.3,
		},
	},
};

export const PlayerTalent_OreKing: TPlayerTalentDefinition = {
	id: 472,
	learnable: false,
	caption: `Ore King (+5% Carbon, Metals and Oxygen Output)`,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Spend 25000 worth of either Carbon, Metals or Oxygen ',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			[Resources.resourceId.carbon, Resources.resourceId.ores, Resources.resourceId.oxygen].reduce(
				(value, dt) => Math.max(value, getPlayerStatProperty(p, 'resourcesSpentByType', dt)),
				0,
			) / 25000,
		);
	},
	baseModifier: {
		resourceOutput: {
			[Resources.resourceId.carbon]: {
				factor: 1.05,
			},
			[Resources.resourceId.ores]: {
				factor: 1.05,
			},
			[Resources.resourceId.oxygen]: {
				factor: 1.05,
			},
		},
	},
	technologyReward: {
		equipment: {
			[DianodeEquipment.id]: 0.1,
		},
		facilities: {
			[SmelteryFacility.id]: 0.5,
		},
		armor: {
			[SmallGluonArmor.id]: 0.3,
			[GluonArmor.id]: 0.3,
			[LargeGluonArmor.id]: 0.3,
		},
	},
};

export const PlayerTalent_FalloutBoy: TPlayerTalentDefinition = {
	id: 473,
	learnable: false,
	caption: `Fallout Boy (+5% Isotopes and Halogens Output)`,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Spend 5000 worth of either Isotopes or Halogens',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			[Resources.resourceId.isotopes, Resources.resourceId.halogens].reduce(
				(value, dt) => Math.max(value, getPlayerStatProperty(p, 'resourcesSpentByType', dt)),
				0,
			) / 5000,
		);
	},
	baseModifier: {
		resourceOutput: {
			[Resources.resourceId.isotopes]: {
				factor: 1.05,
			},
			[Resources.resourceId.halogens]: {
				factor: 1.05,
			},
		},
	},
	technologyReward: {
		equipment: {
			[EleriumBatteryEquipment.id]: 0.1,
		},
		facilities: {
			[AtomicPileFacility.id]: 0.5,
		},
		weapons: {
			[NukeShells.id]: 0.3,
			[SS18Rocket.id]: 0.3,
			[PhoenixMissile.id]: 0.3,
		},
	},
};

export const PlayerTalent_Lifebringer: TPlayerTalentDefinition = {
	id: 474,
	learnable: false,
	caption: `Lifebringer (+5% Proteins and Minerals Output)`,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 2,
	description: 'Spend 10000 worth of either Proteins or Minerals',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			[Resources.resourceId.proteins, Resources.resourceId.minerals].reduce(
				(value, dt) => Math.max(value, getPlayerStatProperty(p, 'resourcesSpentByType', dt)),
				0,
			) / 10000,
		);
	},
	baseModifier: {
		resourceOutput: {
			[Resources.resourceId.proteins]: {
				factor: 1.05,
			},
			[Resources.resourceId.minerals]: {
				factor: 1.05,
			},
		},
	},
	technologyReward: {
		equipment: {
			[MioplasmaticReflectorEquipment.id]: 0.1,
		},
		armor: {
			[PowerArmor.id]: 0.3,
		},
		chassis: {
			[XDriverChassis.id]: 0.5,
		},
		shield: {
			[HardenedShield.id]: 0.3,
		},
		weapons: {
			[PoisonGun.id]: 0.3,
		},
	},
};

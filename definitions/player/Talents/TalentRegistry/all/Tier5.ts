import DIContainer from '../../../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../core/DI/injections';
import { ArithmeticPrecision } from '../../../../maths/constants';
import Series from '../../../../maths/Series';
import AscendancySummitEstablishment from '../../../../orbital/establishments/all/AscendancySummit.503';
import DarkTetherFacility from '../../../../orbital/facilities/all/DarkTether.407';
import HyperspaceCompactorFacility from '../../../../orbital/facilities/all/HyperspaceCompactor.306';
import LivingShellFacility from '../../../../orbital/facilities/all/LivingShell.405';
import MassRelayFacility from '../../../../orbital/facilities/all/MassRelay.304';
import PlanetCrackerFacility from '../../../../orbital/facilities/all/PlanetCracker.408';
import QuarkStreamlineFacility from '../../../../orbital/facilities/all/QuarkStreamline.406';
import RecyclerFacility from '../../../../orbital/facilities/all/Recycler.404';
import TemperingCauldronFacility from '../../../../orbital/facilities/all/TemperingCauldron.204';
import AirforgeFactory from '../../../../orbital/factories/all/Airforge.502';
import MassiveIndustryComplex from '../../../../orbital/factories/all/MassiveIndustryComplex.600';
import NaviforgeFactory from '../../../../orbital/factories/all/Navyforge.404';
import { IPrototypeModifier } from '../../../../prototypes/types';
import { getBaseStatProperty, getPlayerStatProperty } from '../../../../stats/helpers';
import UnitAttack from '../../../../tactical/damage/attack';
import LargeDefenseMatrix from '../../../../technologies/armor/all/large/LargeDefenseMatrix.30004';
import DefenseMatrix from '../../../../technologies/armor/all/medium/DefenseMatrix.20005';
import SmallDefenseMatrix from '../../../../technologies/armor/all/small/SmallDefenseMatrix.10006';
import BattleshipChassis from '../../../../technologies/chassis/all/Battleship/Battleship.62000';
import NeosapientChassis from '../../../../technologies/chassis/all/BioTech/Neosapient.40001';
import MirageChassis from '../../../../technologies/chassis/all/Bomber/Mirage.80002';
import NeptuneChassis from '../../../../technologies/chassis/all/Corvette/Neptune.60002';
import PoseidonChassis from '../../../../technologies/chassis/all/Cruiser/Poseidon.65001';
import SeamothChassis from '../../../../technologies/chassis/all/Destroyer/Seamoth.64001';
import FalconChassis from '../../../../technologies/chassis/all/Fighter/Falcon.82002';
import FortrexChassis from '../../../../technologies/chassis/all/Fortress/Fortrex.59001';
import HelipadChassis from '../../../../technologies/chassis/all/Helipad/Helipad.89500';
import ScyllaChassis from '../../../../technologies/chassis/all/Hydra/Scylla.43001';
import TrebuchetChassis from '../../../../technologies/chassis/all/LauncherPad/Trebuchet.86001';
import RavagerChassis from '../../../../technologies/chassis/all/Quadrapod/Ravager.35002';
import EagleChassis from '../../../../technologies/chassis/all/StrikeAircraft/Eagle.85001';
import AnglerChassis from '../../../../technologies/chassis/all/Submarine/Angler.66001';
import ScannerChassis from '../../../../technologies/chassis/all/SupportVehicle/Scanner.31001';
import XenoswarmChassis from '../../../../technologies/chassis/all/Swarm/Xenoswarm.45001';
import FlyingFoxChassis from '../../../../technologies/chassis/all/Titljet/FlyingFox.84003';
import { airChassisList, hoverChassisList, navalChassisList } from '../../../../technologies/chassis/constants';
import ProtonGeneratorEquipment from '../../../../technologies/equipment/all/Defensive/ProtonGenerator.20002';
import WarpGeneratorEquipment from '../../../../technologies/equipment/all/Defensive/WarpGenerator.20004';
import CryostasisGridEquipment from '../../../../technologies/equipment/all/Offensive/CryostasisGrid.20026';
import GravitonCatapultEquipment from '../../../../technologies/equipment/all/Offensive/GravitonCatapult.20021';
import SuperfluidDistillerEquipment from '../../../../technologies/equipment/all/Offensive/SuperfluidDistiller.30006';
import HiggsPumpEquipment from '../../../../technologies/equipment/all/Support/HiggsPump.30011';
import DroneJammerEquipment from '../../../../technologies/equipment/all/Tactical/DroneJammer.20001';
import TacticalClusterEquipment from '../../../../technologies/equipment/all/Tactical/TacticalCluster.20009';
import OverloadEquipment from '../../../../technologies/equipment/all/Warfare/Overload.30001';
import PsionicAmplifierEquipment from '../../../../technologies/equipment/all/Warfare/PsionicAmplifier.30007';
import GluonShield from '../../../../technologies/shields/all/large/GluonShield.30014';
import LargeWarpShield from '../../../../technologies/shields/all/large/LargeWarpShield.30006';
import RechargingShield from '../../../../technologies/shields/all/large/RechargingShield.30009';
import AntimatterShield from '../../../../technologies/shields/all/medium/AntimatterShield.20009';
import LocustShield from '../../../../technologies/shields/all/medium/LocustShield.20002';
import QuantumShield from '../../../../technologies/shields/all/medium/QuantumShield.20010';
import WarpShield from '../../../../technologies/shields/all/medium/WarpShield.20006';
import SmallWarpShield from '../../../../technologies/shields/all/small/SmallWarpShield.10006';
import UnitChassis from '../../../../technologies/types/chassis';
import AMAACannon from '../../../../technologies/weapons/all/antiair/AMAACannon.70004';
import DeathRay from '../../../../technologies/weapons/all/beam/DeathRay.80003';
import StarBeam from '../../../../technologies/weapons/all/beam/StarBeam.80006';
import AntimatterBomb from '../../../../technologies/weapons/all/bombs/AntimatterBomb.40007';
import AntimatterShells from '../../../../technologies/weapons/all/cannons/AntimatterShells.10003';
import ProtonShells from '../../../../technologies/weapons/all/cannons/ProtonShells.10001';
import LaserDrone from '../../../../technologies/weapons/all/drones/LaserDrone.65001';
import MissileDrone from '../../../../technologies/weapons/all/drones/MissileDrone.65000';
import BlackWidowMissile from '../../../../technologies/weapons/all/guided/BlackWidow.35003';
import SerpentMissile from '../../../../technologies/weapons/all/guided/SerpentMissile.35002';
import GammaLaser from '../../../../technologies/weapons/all/lasers/GammaLaser.90005';
import ProtonCannon from '../../../../technologies/weapons/all/machineguns/ProtonCannon.20003';
import SPARCGun from '../../../../technologies/weapons/all/machineguns/SPARC.20002';
import AntimatterMissile from '../../../../technologies/weapons/all/missiles/AntimatterMissile.30002';
import ProtonMissile from '../../../../technologies/weapons/all/missiles/ProtonMissile.30004';
import MaradeurWeapon from '../../../../technologies/weapons/all/plasma/Marauder.50001';
import GravityMines from '../../../../technologies/weapons/all/projectiles/GravityMines.15003';
import AntimatterCannon from '../../../../technologies/weapons/all/railguns/AntimatterCannon.25006';
import Hypercharger from '../../../../technologies/weapons/all/railguns/Hypercharger.25002';
import WarpRocket from '../../../../technologies/weapons/all/rockets/WarpArtillery.38004';
import WarpGun from '../../../../technologies/weapons/all/warp/WarpGun.60000';
import { PLAYER_TALENT_TIER_LEVEL } from '../../../constants';
import Players from '../../../types';
import { TPlayerTalentDefinition } from '../../types';
import { PlayerTalent_Warlord } from './Tier3';
import {
	PlayerTalent_Aviator,
	PlayerTalent_Breacher,
	PlayerTalent_Burner,
	PlayerTalent_Champion,
	PlayerTalent_CLevel,
	PlayerTalent_Coiler,
	PlayerTalent_Colossus,
	PlayerTalent_Destroyer,
	PlayerTalent_Engineer,
	PlayerTalent_Farstriker,
	PlayerTalent_GunLayer,
	PlayerTalent_Heater,
	PlayerTalent_Leadsprayer,
	PlayerTalent_Mariner,
	PlayerTalent_Mayor,
	PlayerTalent_Midshipman,
	PlayerTalent_Piercer,
	PlayerTalent_Researcher,
	PlayerTalent_Rifleman,
	PlayerTalent_Saboteur,
	PlayerTalent_Splasher,
	PlayerTalent_Suppressor,
	PlayerTalent_Sustainer,
	PlayerTalent_Talented,
	PlayerTalent_Zenithar,
	PlayerTalent_Zoomaster,
} from './Tier4';

export const PlayerTalent_Manifold: TPlayerTalentDefinition = {
	id: 600,
	caption: 'Manifold (+0.25 Unit Ability Power)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	description: 'Produce 75 Hover Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				hoverChassisList
					.flatMap((cl) =>
						Object.keys(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(cl)),
					)
					.map((id) => getPlayerStatProperty(p, 'unitsProducedByChassisId', id)),
			) / 75,
		);
	},
	unitModifier: {
		default: {
			unitModifier: {
				abilityPowerModifier: [
					{
						bias: 0.25,
					},
				],
			},
		},
	},
	technologyReward: {
		facilities: {
			[TemperingCauldronFacility.id]: 0.5,
		},
		factories: {
			[MassiveIndustryComplex.id]: 0.5,
		},
		equipment: {
			[CryostasisGridEquipment.id]: 0.1,
		},
	},
};
export const PlayerTalent_Sublime: TPlayerTalentDefinition = {
	id: 601,
	caption: 'Sublime (+0.25 Unit Ability Power)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	description: 'Use 1000 Offensive Abilities',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'offensiveAbilitiesCast') / 1000);
	},
	unitModifier: {
		default: {
			unitModifier: {
				abilityPowerModifier: [
					{
						bias: 0.25,
					},
				],
			},
		},
	},
	technologyReward: {
		facilities: {
			[RecyclerFacility.id]: 0.5,
		},
		equipment: {
			[TacticalClusterEquipment.id]: 0.1,
		},
		weapons: {
			[ProtonCannon.id]: 0.3,
		},
	},
};
export const PlayerTalent_Impregnable: TPlayerTalentDefinition = {
	id: 602,
	caption: 'Impregnable (+0.25 Base Ability Power)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	description: 'Use 1000 Defensive Abilities',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'defensiveAbilitiesCast') / 1000);
	},
	baseModifier: {
		abilityPower: {
			bias: 0.25,
		},
	},
	technologyReward: {
		facilities: {
			[HyperspaceCompactorFacility.id]: 0.5,
		},
		equipment: {
			[PsionicAmplifierEquipment.id]: 0.1,
		},
		weapons: {
			[ProtonMissile.id]: 0.3,
		},
	},
};
export const PlayerTalent_Sagacious: TPlayerTalentDefinition = {
	id: 603,
	caption: 'Sagacious (+0.25 Base Ability Power)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	description: 'Have a Base that took at least 5000 Damage',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Math.max(...Object.values(p?.bases ?? {}).map((b) => getBaseStatProperty(b, 'hullDamageTakenByType'))) /
				5000,
		);
	},
	baseModifier: {
		abilityPower: {
			bias: 0.25,
		},
	},
	technologyReward: {
		facilities: {
			[MassRelayFacility.id]: 0.5,
		},
		equipment: {
			[OverloadEquipment.id]: 0.1,
		},
		weapons: {
			[ProtonShells.id]: 0.3,
		},
	},
};
export const PlayerTalent_Ascendant: TPlayerTalentDefinition = {
	id: 604,
	caption: 'Ascendant (+10% Dodge)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	description: 'Have 150 different Blueprints',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				Object.values(p.arsenal)
					.map((t) => Object.values(t).filter((i) => i >= 1))
					.map((t) => t.length),
			) / 150,
		);
	},
	unitModifier: {
		default: {
			chassis: [
				{
					dodge: {
						default: {
							bias: 0.1,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		establishments: {
			[AscendancySummitEstablishment.id]: 0.5,
		},
		equipment: {
			[SuperfluidDistillerEquipment.id]: 0.1,
		},
		weapons: {
			[DeathRay.id]: 0.3,
		},
	},
};
export const PlayerTalent_Voyager: TPlayerTalentDefinition = {
	id: 610,
	caption: 'Voyager (+2 Armor Protection for Naval Units)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [PlayerTalent_Midshipman.id, PlayerTalent_Mariner.id, PlayerTalent_Farstriker.id],
	description: 'Produce 50 Naval Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				navalChassisList
					.flatMap((cl) =>
						Object.keys(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(cl)),
					)
					.map((id) => getPlayerStatProperty(p, 'unitsProducedByChassisId', id)),
			) / 50,
		);
	},
	unitModifier: {
		...navalChassisList.reduce(
			(obj, cl) =>
				Object.assign(obj, {
					[cl]: {
						armor: [
							{
								default: {
									bias: -2,
									min: 0,
								},
							},
						],
					} as IPrototypeModifier,
				}),
			{},
		),
	},
	technologyReward: {
		factories: {
			[NaviforgeFactory.id]: 0.5,
		},
		chassis: {
			[BattleshipChassis.id]: 0.3,
			[AnglerChassis.id]: 0.3,
			[PoseidonChassis.id]: 0.3,
			[SeamothChassis.id]: 0.3,
			[NeptuneChassis.id]: 0.3,
		},
	},
};
export const PlayerTalent_Skyman: TPlayerTalentDefinition = {
	id: 611,
	caption: 'Skyman (+2 Armor Protection for Air Units)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [PlayerTalent_Splasher.id, PlayerTalent_Aviator.id, PlayerTalent_Farstriker.id],
	description: 'Produce 50 Aerial Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				airChassisList
					.flatMap((cl) =>
						Object.keys(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAllByChassisClass(cl)),
					)
					.map((id) => getPlayerStatProperty(p, 'unitsProducedByChassisId', id)),
			) / 50,
		);
	},
	unitModifier: {
		...airChassisList.reduce(
			(obj, cl) =>
				Object.assign(obj, {
					[cl]: {
						armor: [
							{
								default: {
									bias: -2,
									min: 0,
								},
							},
						],
					} as IPrototypeModifier,
				}),
			{},
		),
	},
	technologyReward: {
		factories: {
			[AirforgeFactory.id]: 0.5,
		},
		chassis: {
			[EagleChassis.id]: 0.5,
			[FlyingFoxChassis.id]: 0.5,
			[FalconChassis.id]: 0.5,
			[HelipadChassis.id]: 0.5,
			[MirageChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Controller: TPlayerTalentDefinition = {
	id: 612,
	caption: 'Controller (+0.5 Quadrapod and Combat Drone speed)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [PlayerTalent_Farstriker.id, PlayerTalent_Colossus.id],
	description: 'Produce 150 Robotic Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				Object.values(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll())
					.filter((ch) =>
						DIContainer.getProvider(DIInjectableCollectibles.chassis).isChassisOfTargetType(
							ch,
							UnitChassis.targetType.robotic,
						),
					)
					.map((ch) => getPlayerStatProperty(p, 'unitsProducedByChassisId', ch.id)),
			) / 150,
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.quadrapod]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
						minGuard: ArithmeticPrecision,
					},
				},
			],
		},
		[UnitChassis.chassisClass.combatdrone]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
						minGuard: ArithmeticPrecision,
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[DarkTetherFacility.id]: 0.5,
		},
		chassis: {
			[RavagerChassis.id]: 0.5,
			[TrebuchetChassis.id]: 0.5,
			[FortrexChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Breeder: TPlayerTalentDefinition = {
	id: 613,
	caption: 'Breeder (+0.5 Hydra and Swarm speed)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [PlayerTalent_Zoomaster.id, PlayerTalent_Colossus.id],
	description: 'Produce 150 Organic Units',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				Object.values(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll())
					.filter((ch) =>
						DIContainer.getProvider(DIInjectableCollectibles.chassis).isChassisOfTargetType(
							ch,
							UnitChassis.targetType.organic,
						),
					)
					.map((ch) => getPlayerStatProperty(p, 'unitsProducedByChassisId', ch.id)),
			) / 150,
		);
	},
	unitModifier: {
		[UnitChassis.chassisClass.hydra]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
						minGuard: ArithmeticPrecision,
					},
				},
			],
		},
		[UnitChassis.chassisClass.swarm]: {
			chassis: [
				{
					speed: {
						bias: 0.5,
						minGuard: ArithmeticPrecision,
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[LivingShellFacility.id]: 0.5,
		},
		chassis: {
			[NeosapientChassis.id]: 0.3,
			[ScyllaChassis.id]: 0.3,
			[XenoswarmChassis.id]: 0.3,
		},
	},
};
export const PlayerTalent_Crusher: TPlayerTalentDefinition = {
	id: 620,
	caption: 'Crusher (+5% Damage)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [
		PlayerTalent_Rifleman.id,
		PlayerTalent_GunLayer.id,
		PlayerTalent_Suppressor.id,
		PlayerTalent_Breacher.id,
		PlayerTalent_Leadsprayer.id,
		PlayerTalent_Piercer.id,
		PlayerTalent_Heater.id,
		PlayerTalent_Burner.id,
		PlayerTalent_Coiler.id,
		PlayerTalent_Zenithar.id,
	],
	description: 'Deal 100000 Damage with Weapons',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes') / 100000);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					default: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		facilities: {
			[PlanetCrackerFacility.id]: 0.5,
		},
		weapons: {
			[BlackWidowMissile.id]: 0.3,
			[GammaLaser.id]: 0.3,
			[SPARCGun.id]: 0.3,
			[Hypercharger.id]: 0.3,
			[GravityMines.id]: 0.3,
			[MaradeurWeapon.id]: 0.3,
		},
	},
};
export const PlayerTalent_Paragon: TPlayerTalentDefinition = {
	id: 621,
	caption: 'Paragon (+1 Recon and Counter-Recon Level)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [
		PlayerTalent_CLevel.id,
		PlayerTalent_Talented.id,
		PlayerTalent_Researcher.id,
		PlayerTalent_Engineer.id,
		PlayerTalent_Mayor.id,
		PlayerTalent_Sustainer.id,
	],
	description: 'Take 100000 Damage',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageTakenByType') / 100000);
	},
	baseModifier: {
		intelligenceLevel: {
			bias: 1,
		},
		counterIntelligenceLevel: {
			bias: 1,
		},
	},
	technologyReward: {
		facilities: {
			[QuarkStreamlineFacility.id]: 0.5,
		},
		equipment: {
			[ProtonGeneratorEquipment.id]: 0.1,
			[GravitonCatapultEquipment.id]: 0.1,
			[HiggsPumpEquipment.id]: 0.1,
		},
		shield: {
			[LocustShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_WarpOverlord: TPlayerTalentDefinition = {
	id: 630,
	caption: 'Warp Overlord (+1000 XP)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [
		PlayerTalent_Coiler.id,
		PlayerTalent_Piercer.id,
		PlayerTalent_Burner.id,
		PlayerTalent_Destroyer.id,
		PlayerTalent_Saboteur.id,
		PlayerTalent_Champion.id,
		PlayerTalent_Warlord.id,
	],
	description: 'Deal 25000 Damage with DOT or Deferred Attacks',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			(getPlayerStatProperty(p, 'damageDoneByAttackType', UnitAttack.attackFlags.DoT) +
				getPlayerStatProperty(p, 'damageDoneByAttackType', UnitAttack.attackFlags.Deferred)) /
				25000,
		);
	},
	reward(p: Players.IPlayer): Players.IPlayer {
		return p.addXp(1000);
	},
	technologyReward: {
		weapons: {
			[WarpGun.id]: 0.3,
			[WarpRocket.id]: 0.3,
		},
		equipment: {
			[WarpGeneratorEquipment.id]: 0.1,
		},
		shield: {
			[WarpShield.id]: 0.3,
			[SmallWarpShield.id]: 0.3,
			[LargeWarpShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_CloudStormer: TPlayerTalentDefinition = {
	id: 631,
	caption: 'Cloudstormer (+1000 XP)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [
		PlayerTalent_Suppressor.id,
		PlayerTalent_Breacher.id,
		PlayerTalent_Leadsprayer.id,
		PlayerTalent_Destroyer.id,
		PlayerTalent_Saboteur.id,
		PlayerTalent_Champion.id,
		PlayerTalent_Warlord.id,
	],
	description: 'Deal 25000 Damage with Penetrative or Disruptive Attacks',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			(getPlayerStatProperty(p, 'damageDoneByAttackType', UnitAttack.attackFlags.Penetrative) +
				getPlayerStatProperty(p, 'damageDoneByAttackType', UnitAttack.attackFlags.Disruptive)) /
				25000,
		);
	},
	reward(p: Players.IPlayer): Players.IPlayer {
		return p.addXp(1000);
	},
	technologyReward: {
		weapons: {
			[MissileDrone.id]: 0.3,
			[LaserDrone.id]: 0.3,
		},
		equipment: {
			[DroneJammerEquipment.id]: 0.1,
		},
		armor: {
			[SmallDefenseMatrix.id]: 0.3,
			[DefenseMatrix.id]: 0.3,
			[LargeDefenseMatrix.id]: 0.3,
		},
	},
};
export const PlayerTalent_Annihilator: TPlayerTalentDefinition = {
	id: 632,
	caption: 'Annihilator (+1000 XP)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [
		PlayerTalent_GunLayer.id,
		PlayerTalent_Rifleman.id,
		PlayerTalent_Heater.id,
		PlayerTalent_Destroyer.id,
		PlayerTalent_Saboteur.id,
		PlayerTalent_Champion.id,
		PlayerTalent_Warlord.id,
	],
	description: 'Deal 40000 Damage to Shields',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneToShields') / 40000);
	},
	reward(p: Players.IPlayer): Players.IPlayer {
		return p.addXp(1000);
	},
	technologyReward: {
		weapons: {
			[AntimatterBomb.id]: 0.3,
			[AntimatterMissile.id]: 0.3,
			[AntimatterShells.id]: 0.3,
			[AntimatterCannon.id]: 0.3,
			[AMAACannon.id]: 0.3,
		},
		shield: {
			[AntimatterShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_Sovereign: TPlayerTalentDefinition = {
	id: 640,
	caption: 'Sovereign (+1 Effect Penetration)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	description: 'Lose 75000 Shield Capacity',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'shieldsLost') / 75000);
	},
	unitModifier: {
		default: {
			unitModifier: {
				abilityDurationModifier: [
					{
						bias: 1,
					},
				],
			},
		},
	},
	technologyReward: {
		shield: {
			[QuantumShield.id]: 0.3,
			[GluonShield.id]: 0.3,
		},
		chassis: {
			[ScannerChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Steadfast: TPlayerTalentDefinition = {
	id: 641,
	caption: 'Steadfast (+1 Effect Resistance)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	description: 'Dodge 5000 Attacks',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'attacksDodgedByType') / 5000);
	},
	unitModifier: {
		default: {
			unitModifier: {
				statusDurationModifier: [
					{
						bias: -1,
					},
				],
			},
		},
	},
	technologyReward: {
		weapons: {
			[SerpentMissile.id]: 0.3,
			[StarBeam.id]: 0.3,
		},
		shield: {
			[RechargingShield.id]: 0.3,
		},
	},
};
export const PlayerTalent_Craftologist: TPlayerTalentDefinition = {
	id: 650,
	caption: 'Craftologist (+50 Hit Points and 500 Repair Points each Base)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [PlayerTalent_Researcher.id, PlayerTalent_Engineer.id],
	description: 'Complete 15 Chassis Upgrade Project',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'chassisUpgradeProjectsCompleteById') / 15);
	},
	baseModifier: {
		maxHitPoints: {
			bias: 50,
		},
	},
	reward: (p) => {
		p.basesArray.forEach((b) => b.addRepair(500));
		return p;
	},
};
export const PlayerTalent_Materialist: TPlayerTalentDefinition = {
	id: 651,
	caption: 'Materialist (+50 Hit Points and 500 Repair Points each Base)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 4,
	prerequisiteTalentIds: [PlayerTalent_Researcher.id, PlayerTalent_Engineer.id],
	description: 'Complete 15 Global Engineering Project',
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'globalEngineeringProjectsCompleteById') / 15);
	},
	baseModifier: {
		maxHitPoints: {
			bias: 50,
		},
	},
	reward: (p) => {
		p.basesArray.forEach((b) => b.addRepair(500));
		return p;
	},
};

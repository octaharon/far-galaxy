import DIContainer from '../../../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../core/DI/injections';
import Series from '../../../../maths/Series';
import { getPlayerUnitsAtBayByChassis } from '../../../../orbital/base/helpers';
import WormholeProjectorFacility from '../../../../orbital/facilities/all/WormholeProjector.103';
import NavalEngineeringCorpus from '../../../../orbital/factories/all/NavalEngineeringCorpus.402';
import TitanForgeFactory from '../../../../orbital/factories/all/Titanforge.601';
import { IPrototypeModifier } from '../../../../prototypes/types';
import { getPlayerStatProperty } from '../../../../stats/helpers';
import ApocalypseAbility from '../../../../tactical/abilities/all/Player/Apocalypse.60010';
import Units from '../../../../tactical/units/types';
import LargeBioArmor from '../../../../technologies/armor/all/large/LargeBioArmor.30007';
import BioArmor from '../../../../technologies/armor/all/medium/BioArmor.20004';
import SmallBioArmor from '../../../../technologies/armor/all/small/SmallBioArmor.10007';
import IroncladChassis from '../../../../technologies/chassis/all/Battleship/Ironclad.62001';
import BlackjackChassis from '../../../../technologies/chassis/all/Bomber/Blackjack.80003';
import MaulerChassis from '../../../../technologies/chassis/all/Bomber/Mauler.80001';
import CarrierChassis from '../../../../technologies/chassis/all/Carrier/Carrier.88000';
import CloudOrcaChassis from '../../../../technologies/chassis/all/Carrier/CloudOrca.88002';
import LeviathanChassis from '../../../../technologies/chassis/all/Carrier/Leviathan.88001';
import NereidChassis from '../../../../technologies/chassis/all/Corvette/Nereid.60003';
import AtheneChassis from '../../../../technologies/chassis/all/Cruiser/Athene.65003';
import AuroraChassis from '../../../../technologies/chassis/all/Cruiser/Aurora.65002';
import HammerheadChassis from '../../../../technologies/chassis/all/Destroyer/Hammerhead.64002';
import HeliconChassis from '../../../../technologies/chassis/all/Helipad/Helicon.89501';
import CharybdisChassis from '../../../../technologies/chassis/all/Hydra/Charybdis.43002';
import MangonelChassis from '../../../../technologies/chassis/all/LauncherPad/Mangonel.86002';
import FlagShipChassis from '../../../../technologies/chassis/all/Manofwar/Manofwar.69500';
import MatriarchChassis from '../../../../technologies/chassis/all/MotherShip/Matriarch.89002';
import MothershipChassis from '../../../../technologies/chassis/all/MotherShip/Mothership.89000';
import TechnodromeChassis from '../../../../technologies/chassis/all/Quadrapod/Technodrome.35001';
import OspreyChassis from '../../../../technologies/chassis/all/StrikeAircraft/Osprey.85002';
import SparrowChassis from '../../../../technologies/chassis/all/StrikeAircraft/Sparrow.85003';
import SawfishChassis from '../../../../technologies/chassis/all/Submarine/Sawfish.66002';
import ImposterChassis from '../../../../technologies/chassis/all/SupportAircraft/Imposter.81001';
import NettleChassis from '../../../../technologies/chassis/all/SupportShip/Nettle.61001';
import ArmatureChassis from '../../../../technologies/chassis/all/Tank/Armature.36003';
import AirwolfChassis from '../../../../technologies/chassis/all/Titljet/Airwolf.84001';
import {
	airChassisList,
	groundChassisList,
	hoverChassisList,
	navalChassisList,
} from '../../../../technologies/chassis/constants';
import NeutrinoGeneratorEquipment from '../../../../technologies/equipment/all/Defensive/NeutrinoGenerator.20003';
import CloakDeviceEquipment from '../../../../technologies/equipment/all/Enhancement/CloakingDevice.20017';
import IonCurtain30002 from '../../../../technologies/shields/all/large/IonCurtain.30002';
import LargeCapacitorShield from '../../../../technologies/shields/all/large/LargeCapacitorShield.30003';
import CapacitorShield from '../../../../technologies/shields/all/medium/CapacitorShield.20012';
import UnitChassis from '../../../../technologies/types/chassis';
import Weapons from '../../../../technologies/types/weapons';
import THORGun from '../../../../technologies/weapons/all/beam/THORGun.80002';
import BlackHoleBomb from '../../../../technologies/weapons/all/bombs/BlackHoleBomb.40002';
import NuclearBomb from '../../../../technologies/weapons/all/bombs/NuclearBomb.40001';
import ReaperBomb from '../../../../technologies/weapons/all/bombs/ReaperBomb.40008';
import ToadBomb from '../../../../technologies/weapons/all/bombs/ToadMine.40005';
import AssaultDrone from '../../../../technologies/weapons/all/drones/AssaultDrone.65002';
import ChemDrone from '../../../../technologies/weapons/all/drones/ChemDrone.65005';
import DisruptorDrone from '../../../../technologies/weapons/all/drones/DisruptorDrone.65004';
import HunterSeekerDrone from '../../../../technologies/weapons/all/drones/HunterSeekerDrone.65003';
import PegasusDrone from '../../../../technologies/weapons/all/drones/PegasusDrone.65006';
import GravityHammer from '../../../../technologies/weapons/all/warp/GravityHammer.60003';
import MassCondenser from '../../../../technologies/weapons/all/warp/MassCondenser.60001';
import MassDisperser from '../../../../technologies/weapons/all/warp/MassDisperser.60002';
import WarpArrayWeapon from '../../../../technologies/weapons/all/warp/WarpArray.60004';
import { PLAYER_TALENT_TIER_LEVEL } from '../../../constants';
import Players from '../../../types';
import { TPlayerTalentDefinition } from '../../types';
import { PlayerTalent_Engineer, PlayerTalent_Researcher } from './Tier4';
import {
	PlayerTalent_Annihilator,
	PlayerTalent_Ascendant,
	PlayerTalent_Breeder,
	PlayerTalent_CloudStormer,
	PlayerTalent_Controller,
	PlayerTalent_Craftologist,
	PlayerTalent_Crusher,
	PlayerTalent_Impregnable,
	PlayerTalent_Manifold,
	PlayerTalent_Materialist,
	PlayerTalent_Paragon,
	PlayerTalent_Sagacious,
	PlayerTalent_Skyman,
	PlayerTalent_Sovereign,
	PlayerTalent_Steadfast,
	PlayerTalent_Sublime,
	PlayerTalent_Voyager,
	PlayerTalent_WarpOverlord,
} from './Tier5';
import IUnit = Units.IUnit;

export const PlayerTalent_Shatterer: TPlayerTalentDefinition = {
	id: 700,
	caption: 'Shatterer (Apocalypse Ability)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Destroy 1000 Units',
	prerequisiteTalentIds: [
		PlayerTalent_Skyman.id,
		PlayerTalent_Voyager.id,
		PlayerTalent_Sovereign.id,
		PlayerTalent_Steadfast.id,
		PlayerTalent_Sublime.id,
		PlayerTalent_Impregnable.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'unitsKilledByTargetType') / 1000);
	},
	reward(p: Players.IPlayer): Players.IPlayer {
		return p.learnAbility(ApocalypseAbility.id);
	},
	technologyReward: {
		facilities: {
			[WormholeProjectorFacility.id]: 0.5,
		},
		weapons: {
			[THORGun.id]: 0.3,
		},
		shield: {
			[IonCurtain30002.id]: 0.3,
		},
		chassis: {
			[CharybdisChassis.id]: 0.5,
			[MangonelChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Belligerent: TPlayerTalentDefinition = {
	id: 701,
	caption: 'Belligerent (+0.1 Attack Rate for Ground and Hover Units)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Perform 1000 Unit Deploys',
	prerequisiteTalentIds: [
		PlayerTalent_WarpOverlord.id,
		PlayerTalent_CloudStormer.id,
		PlayerTalent_Annihilator.id,
		PlayerTalent_Breeder.id,
		PlayerTalent_Manifold.id,
		PlayerTalent_Ascendant.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'unitsDeployedByChassisId') / 1000);
	},
	unitModifier: [...groundChassisList, ...hoverChassisList].reduce(
		(obj, ch) =>
			Object.assign(obj, {
				[ch]: {
					weapon: [
						{
							default: {
								baseRate: {
									bias: 0.1,
								},
							},
						},
					],
				} as IPrototypeModifier,
			}),
		{},
	),
	technologyReward: {
		weapons: {
			[HunterSeekerDrone.id]: 0.3,
			[GravityHammer.id]: 0.3,
		},
		armor: {
			[SmallBioArmor.id]: 0.3,
			[BioArmor.id]: 0.3,
			[LargeBioArmor.id]: 0.3,
		},
	},
};
export const PlayerTalent_Schemer: TPlayerTalentDefinition = {
	id: 702,
	caption: 'Schemer (+1 Ability Slot)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Use 5000 Abilities',
	prerequisiteTalentIds: [
		PlayerTalent_Sublime.id,
		PlayerTalent_Impregnable.id,
		PlayerTalent_Crusher.id,
		PlayerTalent_Paragon.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			(getPlayerStatProperty(p, 'defensiveAbilitiesCast') + getPlayerStatProperty(p, 'offensiveAbilitiesCast')) /
				5000,
		);
	},
	technologyReward: {
		chassis: {
			[NettleChassis.id]: 0.5,
			[ImposterChassis.id]: 0.5,
		},
		equipment: {
			[CloakDeviceEquipment.id]: 0.1,
		},
	},
};

export const PlayerTalent_Celestial: TPlayerTalentDefinition = {
	id: 710,
	caption: 'Celestial (+5% Shield Capacity for Aerial Units)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Get 250 kills with Aerial Units',
	prerequisiteTalentIds: [PlayerTalent_Skyman.id, PlayerTalent_CloudStormer.id, PlayerTalent_Crusher.id],
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				Object.values(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll())
					.filter((ch) =>
						DIContainer.getProvider(DIInjectableCollectibles.chassis).isChassisOfMovementType(
							ch,
							UnitChassis.movementType.air,
						),
					)
					.map((ch) => getPlayerStatProperty(p, 'unitsKilledWithChassisId', ch.id)),
			) / 250,
		);
	},
	unitModifier: airChassisList.reduce(
		(obj, ch) =>
			Object.assign(obj, {
				[ch]: {
					shield: [
						{
							shieldAmount: {
								factor: 1.05,
							},
						},
					],
				} as IPrototypeModifier,
			}),
		{},
	),
	technologyReward: {
		weapons: {
			[ToadBomb.id]: 0.3,
			[NuclearBomb.id]: 0.3,
			[AssaultDrone.id]: 0.3,
			[ChemDrone.id]: 0.3,
		},
		chassis: {
			[CarrierChassis.id]: 0.5,
			[MaulerChassis.id]: 0.5,
			[SparrowChassis.id]: 0.5,
			[AirwolfChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Abyssal: TPlayerTalentDefinition = {
	id: 711,
	caption: 'Abyssal (+5% Shield Capacity for Naval Units)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Get 250 kills with Naval Units',
	prerequisiteTalentIds: [PlayerTalent_Voyager.id, PlayerTalent_WarpOverlord.id, PlayerTalent_Crusher.id],
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				Object.values(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll())
					.filter((ch) =>
						DIContainer.getProvider(DIInjectableCollectibles.chassis).isChassisOfMovementType(
							ch,
							UnitChassis.movementType.naval,
						),
					)
					.map((ch) => getPlayerStatProperty(p, 'unitsKilledWithChassisId', ch.id)),
			) / 250,
		);
	},
	unitModifier: navalChassisList.reduce(
		(obj, ch) =>
			Object.assign(obj, {
				[ch]: {
					shield: [
						{
							shieldAmount: {
								factor: 1.05,
							},
						},
					],
				} as IPrototypeModifier,
			}),
		{},
	),
	technologyReward: {
		equipment: {
			[NeutrinoGeneratorEquipment.id]: 0.1,
		},
		weapons: {
			[MassCondenser.id]: 0.3,
			[MassDisperser.id]: 0.3,
		},
		factories: {
			[NavalEngineeringCorpus.id]: 0.5,
		},
		chassis: {
			[NereidChassis.id]: 0.5,
			[IroncladChassis.id]: 0.5,
			[AtheneChassis.id]: 0.5,
			[HammerheadChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Admiral: TPlayerTalentDefinition = {
	id: 712,
	caption: 'Admiral (+0.5 Scan Range for Naval and Aerial Units)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Produce 100 Massive Units',
	prerequisiteTalentIds: [
		PlayerTalent_Voyager.id,
		PlayerTalent_Skyman.id,
		PlayerTalent_Paragon.id,
		PlayerTalent_Controller.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Series.sum(
				Object.values(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll())
					.filter((ch) =>
						DIContainer.getProvider(DIInjectableCollectibles.chassis).isChassisOfTargetType(
							ch,
							UnitChassis.targetType.massive,
						),
					)
					.map((ch) => getPlayerStatProperty(p, 'unitsProducedByChassisId', ch.id)),
			) / 100,
		);
	},
	unitModifier: [...airChassisList, ...navalChassisList].reduce(
		(obj, ch) =>
			Object.assign(obj, {
				[ch]: {
					chassis: [
						{
							scanRange: {
								bias: 0.5,
							},
						},
					],
				} as IPrototypeModifier,
			}),
		{},
	),
	technologyReward: {
		shield: {
			[CapacitorShield.id]: 0.3,
			[LargeCapacitorShield.id]: 0.3,
		},
		factories: {
			[TitanForgeFactory.id]: 0.5,
		},
		chassis: {
			[MothershipChassis.id]: 0.5,
			[FlagShipChassis.id]: 0.5,
			[TechnodromeChassis.id]: 0.5,
			[HeliconChassis.id]: 0.5,
		},
	},
};

export const PlayerTalent_CloudMarshal: TPlayerTalentDefinition = {
	id: 720,
	caption: 'Cloud Marshal (+2% Aerial Units Damage)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Have an Aerial Unit at Bay that survived 50 Turns in Combat',
	prerequisiteTalentIds: [
		PlayerTalent_Shatterer.id,
		PlayerTalent_Belligerent.id,
		PlayerTalent_Celestial.id,
		PlayerTalent_Abyssal.id,
		PlayerTalent_Admiral.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			Math.max(
				...airChassisList
					.reduce((list, ch) => list.concat(getPlayerUnitsAtBayByChassis(p, ch)), [] as IUnit[])
					.map((u) => u.stats.turnsSurvived / 50),
			),
		);
	},
	unitModifier: airChassisList.reduce(
		(obj, ch) =>
			Object.assign(obj, {
				[ch]: {
					weapon: [
						{
							default: {
								damageModifier: {
									factor: 1.02,
								},
							},
						},
					],
				} as IPrototypeModifier,
			}),
		{},
	),
	technologyReward: {
		weapons: {
			[BlackHoleBomb.id]: 0.3,
			[ReaperBomb.id]: 0.3,
			[PegasusDrone.id]: 0.3,
		},
		chassis: {
			[CloudOrcaChassis.id]: 0.5,
			[LeviathanChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_Warmonger: TPlayerTalentDefinition = {
	id: 721,
	caption: 'Warmonger (+5% Drone Weapons Damage)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Deal 10000 Damage with Drone Weapons',
	prerequisiteTalentIds: [
		PlayerTalent_Shatterer.id,
		PlayerTalent_Belligerent.id,
		PlayerTalent_Celestial.id,
		PlayerTalent_Abyssal.id,
		PlayerTalent_Admiral.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'damageDoneByWeaponTypes', Weapons.TWeaponGroupType.Drone) / 10000);
	},
	unitModifier: {
		default: {
			weapon: [
				{
					[Weapons.TWeaponGroupType.Drone]: {
						damageModifier: {
							factor: 1.05,
						},
					},
				},
			],
		},
	},
	technologyReward: {
		chassis: {
			[BlackjackChassis.id]: 0.5,
			[OspreyChassis.id]: 0.5,
			[AuroraChassis.id]: 0.5,
			[SawfishChassis.id]: 0.5,
			[ArmatureChassis.id]: 0.5,
		},
	},
};
export const PlayerTalent_UltimateInvader: TPlayerTalentDefinition = {
	id: 722,
	caption: 'Ultimate Invader (+5000 XP)',
	learnable: false,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	description: 'Learn 100 Talents',
	prerequisiteTalentIds: [
		PlayerTalent_Shatterer.id,
		PlayerTalent_Belligerent.id,
		PlayerTalent_Celestial.id,
		PlayerTalent_Abyssal.id,
		PlayerTalent_Admiral.id,
	],
	achievement: (p: Players.IPlayer) => {
		return Math.min(1, getPlayerStatProperty(p, 'talentsLearned') / 100);
	},
	reward(p: Players.IPlayer): Players.IPlayer {
		return p.addXp(5000);
	},
	technologyReward: {
		chassis: {
			[MatriarchChassis.id]: 0.5,
		},
		weapons: {
			[DisruptorDrone.id]: 0.3,
			[WarpArrayWeapon.id]: 0.3,
		},
	},
};

export const PlayerTalent_Architect: TPlayerTalentDefinition = {
	id: 730,
	caption: 'Architect (+1 Bay Capacity)',
	learnable: true,
	levelReq: PLAYER_TALENT_TIER_LEVEL * 5,
	prerequisiteTalentIds: [
		PlayerTalent_Materialist.id,
		PlayerTalent_Sagacious.id,
		PlayerTalent_Craftologist.id,
		PlayerTalent_Ascendant.id,
	],
	description: 'Complete 100 different Engineering Projects',
	achievement: (p: Players.IPlayer) => {
		return Math.min(
			1,
			(getPlayerStatProperty(p, 'reverseEngineeringProjectsComplete') +
				p.totalChassisUpgradeProjectsCompleted +
				p.totalGlobalEngineeringProjectsCompleted) /
				100,
		);
	},
	baseModifier: {
		bayCapacity: {
			bias: 1,
		},
	},
};

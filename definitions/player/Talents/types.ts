import { IStaticCollectible } from '../../core/Collectible';
import { TPlayerBaseModifier } from '../../orbital/base/types';
import Players, { IPlayerPropertyProps } from '../types';
import TalentRegistry from './TalentRegistry';

export type TPlayerTalentDefinition = IStaticCollectible & {
	levelReq?: number;
	learnable?: boolean;
	prerequisiteTalentIds?: number[];
	technologyReward?: Players.TPlayerTechnologyPayload;
	unitModifier?: Players.TPlayerUnitModifier;
	baseModifier?: TPlayerBaseModifier;
	achievement?: (p: Players.IPlayer) => number;
	reward?: (p: Players.IPlayer) => Players.IPlayer;
};

export type TPlayerTalent = TPlayerTalentDefinition & TPlayerHydratedTalent;

export type TPlayerHydratedTalent = IPlayerPropertyProps & {
	learned: boolean;
	id: number;
};

export function hydrateTalent(t: TPlayerHydratedTalent): TPlayerTalent {
	return {
		...TalentRegistry.get(t.id),
		...t,
	};
}

export function dehydrateTalent(t: TPlayerTalent): TPlayerHydratedTalent {
	return {
		id: t.id,
		learned: t.learned,
		playerId: t.playerId,
	};
}

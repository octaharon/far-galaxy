import InterfaceTypes from '../core/InterfaceTypes';
import { ISerializable, ISerializableFactory } from '../core/Serializable';
import { IPlayerBase, TPlayerBaseModifier, TSerializedPlayerBase } from '../orbital/base/types';
import { TBaseBuildingType } from '../orbital/types';
import { IPrototypeModifier } from '../prototypes/types';
import GameStats from '../stats/types';
import Units from '../tactical/units/types';
import UnitChassis from '../technologies/types/chassis';
import { PoliticalFactions } from '../world/factions';
import Resources from '../world/resources/types';
import { TPlayerId } from './playerId';
import { TPlayerHydratedResearch, TPlayerResearch } from './Research/types';
import { TPlayerHydratedTalent, TPlayerTalent } from './Talents/types';

export namespace Players {
	// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/10
	export type TPlayerUnitModifier = EnumProxyWithDefault<UnitChassis.chassisClass, IPrototypeModifier>;

	export type TPlayersEngineeringSpecialProjectType = 'project' | 'upgrade';
	export type TPlayerEngineeringTarget = keyof TPlayerTechnologyPayload | TPlayersEngineeringSpecialProjectType;
	export type TPlayerGlobalEngineeringProject = { id: number; points: number; cost: number };
	export type TPlayerChassisUpgradeProject = {
		chassisId: UnitChassis.chassisClass;
		upgradeId: number;
		points: number;
		cost: number;
	};
	export type TPlayerStaticProps = {
		nickname: string;
		abilitySlots: number; // amount of abilities a player can prepare to be used in a battle
		abilitiesAvailable: number[]; // ids of abilities that a Player can chose from, regardless of base
		abilityPower: number;
		faction: PoliticalFactions.TFactionID; // faction / alliance
		arsenal: TPlayerTechnologyPayload; // availability of unit components
		id: TPlayerId;
		xp: number;
		level: number;
		selectedResearch?: number | null; // whatever research points are being spent on at the moment
		selectedEngineering?: { type: TPlayerEngineeringTarget; id: number }; // whatever technology is reversed-engineered now
		completedEngineeringProjects?: TPlayerGlobalEngineeringProject[];
		chassisUpgradeProjects?: TPlayerChassisUpgradeProject[];
		talentPoints?: number; // amount of talents a player can learn
		buildingUpgradePoints?: number; // amount of building upgrades a player can make
		stats: GameStats.TPlayerStats;
		resourceSupply: Resources.TResourcePayload; // stockpile of resources
		baseModifier: TPlayerBaseModifier; // all bases modifier
		unitModifier: TPlayerUnitModifier; // all units modifier
		reputation: EnumProxy<PoliticalFactions.TFactionID, number>;
	};

	export type TPlayerDynamicProps = {
		bases: Record<string, IPlayerBase>; // all player bases
		talents: Record<number, TPlayerTalent>; // current state of talents
		researches: Record<number, TPlayerResearch>; // current state of researches
	};

	export type TPlayerProps = TPlayerDynamicProps & TPlayerStaticProps;

	export type TPlayerSerializedProps = {
		bases: Record<string, TSerializedPlayerBase>;
		talents: Record<number | string, TPlayerHydratedTalent>;
		researches: Record<number | string, TPlayerHydratedResearch>;
	};
	export type TSerializedPlayer = TPlayerStaticProps & TPlayerSerializedProps & IWithInstanceID;
	export type TPlayerTechnologyPayload = {
		// { entityId : percent }. 1 means player owns the technology
		chassis?: InterfaceTypes.TIdMap<number>;
		factories?: InterfaceTypes.TIdMap<number>;
		facilities?: InterfaceTypes.TIdMap<number>;
		establishments?: InterfaceTypes.TIdMap<number>;
		weapons?: InterfaceTypes.TIdMap<number>;
		armor?: InterfaceTypes.TIdMap<number>;
		shield?: InterfaceTypes.TIdMap<number>;
		equipment?: InterfaceTypes.TIdMap<number>;
	};

	export interface IPlayer extends TPlayerProps, ISerializable<TPlayerProps, TSerializedPlayer> {
		totalGlobalEngineeringProjectsCompleted: number;
		totalChassisUpgradeProjectsCompleted: number;
		currentGlobalEngineeringProject: TPlayerGlobalEngineeringProject;
		currentChassisUpgradeProject: TPlayerChassisUpgradeProject;
		currentGlobalEngineeringProjectProgress: number;
		currentChassisUpgradeProjectProgress: number;
		basesArray: IPlayerBase[];
		dockedUnits: Units.IUnit[];

		getNextLevelXp: (level?: number) => number;

		setEngineeringTarget(type: TPlayerEngineeringTarget, id: number): this;

		investTech(researchPoints: number, engineeringPoints: number): this;

		switchResearch(researchId: string | number): this;

		isResearchAvailable(researchId: string | number): boolean;

		isChassisUpgradeProjectAvailable(chassisId: UnitChassis.chassisClass): boolean;

		isTalentAvailable(id: number): boolean;

		isTalentLearned(id: number): boolean;

		learnTalent(talentId: number): this;

		addTechnologies(p: TPlayerTechnologyPayload): this;

		addXp(xp: number): this;

		addReputation(faction: PoliticalFactions.TFactionID, value: number): this;

		addUnitModifier(chassis: keyof TPlayerUnitModifier, ...modifiers: IPrototypeModifier[]): this;

		addResource(id: Resources.resourceId, amount: number): this;

		addResourcePayload(r: Resources.TResourcePayload): this;

		hasEnoughResources(r: Resources.TResourcePayload): boolean;

		spendResources(r: Resources.TResourcePayload): this;

		isWeaponAvailable(weaponId: number): boolean;

		isArmorAvailable(armorId: number): boolean;

		isShieldAvailable(shieldId: number): boolean;

		isChassisAvailable(chassisId: number): boolean;

		isEquipmentAvailable(equipmentId: number): boolean;

		isBuildingAvailable(buildingType: TBaseBuildingType, buildingId: number): boolean;

		getUnitModifiers(chassisClass?: UnitChassis.chassisClass): IPrototypeModifier[];

		getBaseModifiers(): TPlayerBaseModifier[];

		learnAbility(abilityId: number, unlearn?: boolean): this;

		isAbilityKnown(abilityId: number): boolean;

		getKnownTechnologyCount(type?: keyof TPlayerTechnologyPayload): number;
	}

	export interface IPlayerFactory
		extends ISerializableFactory<TPlayerStaticProps & TPlayerDynamicProps, TSerializedPlayer, IPlayer> {
		getByPlayerId(id: TPlayerId): IPlayer;

		getTestPlayer(faction: PoliticalFactions.TFactionID, withArsenal?: boolean): IPlayer;
	}
}

export default Players;
export type IPlayerPropertyProps = {
	playerId: TPlayerId;
};
export interface IPlayerProperty extends IPlayerPropertyProps {
	owner?: Players.IPlayer;
	setOwner(pId: TPlayerId): this;
	doesBelongToPlayer?(playerId: TPlayerId): boolean;
	isFriendlyToPlayer?(playerId: TPlayerId): boolean;
}

import _ from 'underscore';
import UnitDefense from '../../tactical/damage/defense';
import { TPrototypeProps } from '../../prototypes/types';
import Weapons from '../../technologies/types/weapons';
import DIContainer from '../../core/DI/DIContainer';
import { DIInjectableCollectibles, DIInjectableSerializables } from '../../core/DI/injections';
import PiratesDefaultPlayer from './Pirates';

// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/9
export function generatePiratePrototype(chassisId = _.sample(Object.keys(PiratesDefaultPlayer.arsenal.chassis))) {
	const samplePrototypeProps = {
		playerId: PiratesDefaultPlayer.id,
	} as TPrototypeProps;

	const samplePrototype = DIContainer.getProvider(DIInjectableSerializables.prototype).instantiate(
		samplePrototypeProps,
	);

	// Sampling items
	samplePrototype.setChassis(chassisId);
	// console.log(samplePrototype.chassis.static);

	(samplePrototype.chassis.static.armorSlots || []).forEach(
		(slotType: UnitDefense.TArmorSlotType, slotIndex: number) => {
			let armorId: number;
			do {
				armorId = _.sample(Object.keys(PiratesDefaultPlayer.arsenal.armor));
			} while (
				!DIContainer.getProvider(DIInjectableCollectibles.armor).doesArmorFitSlot(
					DIContainer.getProvider(DIInjectableCollectibles.armor).get(armorId),
					slotType,
				)
			);
			// console.log('sampled ',armorId);
			samplePrototype.setArmor(slotIndex, armorId);
		},
	);

	(samplePrototype.chassis.static.shieldSlots || []).forEach(
		(slotType: UnitDefense.TShieldSlotType, slotIndex: number) => {
			let shieldId;
			do {
				shieldId = _.sample(Object.keys(PiratesDefaultPlayer.arsenal.shield));
			} while (
				!DIContainer.getProvider(DIInjectableCollectibles.shield).doesShieldFitSlot(
					DIContainer.getProvider(DIInjectableCollectibles.shield).get(shieldId),
					slotType,
				)
			);
			// console.log('sampled ',armorId);
			samplePrototype.setShield(slotIndex, shieldId);
		},
	);

	(samplePrototype.chassis.static.weaponSlots || []).forEach(
		(slotType: Weapons.TWeaponSlotType, slotIndex: number) => {
			let weaponId;
			do {
				weaponId = _.sample(Object.keys(PiratesDefaultPlayer.arsenal.weapons));
			} while (!DIContainer.getProvider(DIInjectableCollectibles.weapon).doesWeaponFitSlot(weaponId, slotType));
			// console.log('sampled ',armorId);
			samplePrototype.setWeapon(slotIndex, weaponId);
		},
	);

	new Array(samplePrototype.chassis.equipmentSlots).fill(0).forEach((slotIndex: number) => {
		const equipmentId = _.sample(Object.keys(PiratesDefaultPlayer.arsenal.equipment));
		// console.log('sampled ',armorId);
		samplePrototype.setEquipment(slotIndex, equipmentId);
	});

	return samplePrototype;
}

export function generatePirateFortress() {
	return generatePiratePrototype(59000);
}

export function generatePirateTurret() {
	return generatePiratePrototype(57000);
}

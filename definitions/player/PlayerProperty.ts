import { DIInjectableSerializables } from '../core/DI/injections';
import Serializable, { ISerializable } from '../core/Serializable';
import { TPlayerId } from './playerId';
import Players, { IPlayerProperty, IPlayerPropertyProps } from './types';

// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/8

export default abstract class PlayerProperty<
		PropType extends IPlayerPropertyProps,
		SerializedType extends IWithInstanceID,
	>
	extends Serializable<PropType, SerializedType>
	implements IPlayerProperty, ISerializable<PropType, SerializedType>
{
	public playerId: TPlayerId;

	public setOwner(pId: TPlayerId) {
		this.playerId = pId;
		return this;
	}

	public get owner(): Players.IPlayer {
		try {
			return this.getProvider(DIInjectableSerializables.player).getByPlayerId(this.playerId);
		} catch (e) {
			console.error(e);
			return null;
		}
	}

	doesBelongToPlayer(playerId: TPlayerId): boolean {
		return this.playerId === playerId;
	}

	isFriendlyToPlayer(playerId: TPlayerId): boolean {
		return this.playerId === playerId;
	}

	protected __serializePlayerProperty() {
		return {
			...this.__serializeSerializable(),
			playerId: this.playerId,
		};
	}
}

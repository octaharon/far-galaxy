import { TPlayerBaseModifier } from '../orbital/base/types';
import Players from '../player/types';
import Abilities from '../tactical/abilities/types';
import PoliticalFactions from './factions';
import AndersDefinition from './factions/anders';
import AquariusFaction from './factions/aquarius';
import { DraconiansFaction } from './factions/draconians';
import PiratesFaction from './factions/pirates';
import ReticulansFaction from './factions/reticulans';
import StellarFaction from './factions/stellarians';
import UnalliedFaction from './factions/unallied';
import VenaticiansFaction from './factions/venaticians';

export const FactionList: Record<PoliticalFactions.TFactionID, PoliticalFactions.IFaction> = {
	[PoliticalFactions.TFactionID.none]: UnalliedFaction,
	[PoliticalFactions.TFactionID.anders]: AndersDefinition,
	[PoliticalFactions.TFactionID.aquarians]: AquariusFaction,
	[PoliticalFactions.TFactionID.pirates]: PiratesFaction,
	[PoliticalFactions.TFactionID.reticulans]: ReticulansFaction,
	[PoliticalFactions.TFactionID.draconians]: DraconiansFaction,
	[PoliticalFactions.TFactionID.venaticians]: VenaticiansFaction,
	[PoliticalFactions.TFactionID.stellarians]: StellarFaction,
};

export const getAllFactionIds = () =>
	Object.values(PoliticalFactions.TFactionID).filter((v) => v !== PoliticalFactions.TFactionID.none);
export const getFaction = (f: PoliticalFactions.TFactionID) => FactionList[f] ?? null;
export const getFactionUnitModifiers = (f: PoliticalFactions.TFactionID) =>
	(getFaction(f)?.features || []).filter((v) => v.type === 'unit') as Players.TPlayerUnitModifier[];
export const getFactionBaseModifiers = (f: PoliticalFactions.TFactionID) =>
	(getFaction(f)?.features || []).filter((v) => v.type === 'base') as TPlayerBaseModifier[];
export const getFactionAbilities = (f: PoliticalFactions.TFactionID) =>
	(getFaction(f)?.features || []).filter((v) => v.type === 'ability') as Abilities.TAbilityCollection;
export const getFactionTechnologies = (f: PoliticalFactions.TFactionID) =>
	(getFaction(f)?.features || []).filter((v) => v.type === 'technology') as Players.TPlayerTechnologyPayload[];

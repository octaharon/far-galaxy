import { TPlayerBaseModifier } from '../orbital/base/types';
import Players from '../player/types';
import Abilities from '../tactical/abilities/types';
import Resources from './resources/types';

export namespace PoliticalFactions {
	export enum TFactionID {
		none = 'fct_unallied',
		venaticians = 'fct_venaticians',
		anders = 'fct_anders',
		stellarians = 'fct_stellarians',
		draconians = 'fct_draconians',
		reticulans = 'fct_reticulans',
		aquarians = 'fct_aquarians',
		pirates = 'fct_pirates',
	}

	export type TFactionResourceExchangeRate = EnumProxy<Resources.resourceId, EnumProxy<Resources.resourceId, number>>;

	export type TFactionFeature =
		| ({ type: 'base' } & Readonly<TPlayerBaseModifier>)
		| ({ type: 'unit' } & Readonly<Players.TPlayerUnitModifier>)
		| ({ type: 'ability' } & Readonly<Abilities.TAbilityCollectionItem>)
		| ({ type: 'technology' } & Readonly<Players.TPlayerTechnologyPayload>);

	export interface IFaction {
		id: TFactionID;
		captionShort: string;
		captionLong: string;
		description: string;
		baseColor: string;
		secondaryColor: string;
		features?: TFactionFeature[];
		resourceExchangeRate?: TFactionResourceExchangeRate; // @TODO Market resource ratios https://gitlab.com/octaharon/far-galaxy/-/issues/27
		extra?: any;
	}
}

export default PoliticalFactions;

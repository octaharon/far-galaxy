import { TPlayerId } from '../player/playerId';
import { TFactionResearchHistory } from '../player/Research/types';
import Players from '../player/types';
import { TStarSystemProps } from '../space/Systems';

export type TUniverse = {
	factionResearch: TFactionResearchHistory;
	players: Record<TPlayerId, Players.IPlayer>;
	systems: TStarSystemProps[];
};
export default TUniverse;

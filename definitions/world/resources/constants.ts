import Resources from './types';

export const ResourceCaptions: EnumProxy<Resources.resourceId, string> = {
	[Resources.resourceId.hydrogen]: 'Hydrogen',
	[Resources.resourceId.carbon]: 'Carbon',
	[Resources.resourceId.helium]: 'Helium',
	[Resources.resourceId.oxygen]: 'Oxygen',
	[Resources.resourceId.proteins]: 'Organics',
	[Resources.resourceId.halogens]: 'Halogens',
	[Resources.resourceId.ores]: 'Metals',
	[Resources.resourceId.isotopes]: 'Isotopes',
	[Resources.resourceId.minerals]: 'Minerals',
	[Resources.resourceId.nitrogen]: 'Nitrogen',
};
export const ResourceDictionary: Resources.TResourceDictionary = {
	[Resources.resourceId.carbon]: {
		id: Resources.resourceId.carbon,
		description: `Carbon comes in so many forms and shapes, but civilizations strive for only one, mostly: it's graphene. It takes huge amounts of carbon to produce it, despite the source form, while the leftovers are often used in other branches of chemical industry. Also some primitive nations just burn it for heating`,
		color: '#313331',
		relativeFrequency: 0.106,
		baseQuantity: {
			min: 400,
			max: 1000,
			distribution: 'Minimal',
		},
	},
	[Resources.resourceId.ores]: {
		id: Resources.resourceId.ores,
		description: `Various metallic ores are scattered through almost all of celestial bodies. When it comes to creating
            something bulky and solid, like a vehicle, or something fine and intricate, like electronics, those are essential`,
		color: '#b0d4e5',
		relativeFrequency: 0.102,
		baseQuantity: {
			min: 150,
			max: 800,
			distribution: 'MostlyMinimal',
		},
	},
	[Resources.resourceId.minerals]: {
		id: Resources.resourceId.minerals,
		description: `Minerals are various in compound, but with certain chemical processes it doesn't really matter.
            So they are bought by companies to be transformed into the most basic ingredients for any sort of production`,
		color: '#421729',
		relativeFrequency: 0.071,
		baseQuantity: {
			min: 100,
			max: 160,
			distribution: 'Bell',
		},
	},
	[Resources.resourceId.hydrogen]: {
		id: Resources.resourceId.hydrogen,
		description: `The most abundant element in the universe sometimes means the most universal.
            The core material for any chemical industry and fission devices`,
		color: '#ef9eef',
		relativeFrequency: 0.156,
		baseQuantity: {
			min: 900,
			max: 2500,
			distribution: 'Uniform',
		},
	},
	[Resources.resourceId.nitrogen]: {
		id: Resources.resourceId.nitrogen,
		description: `Too few protons to be synthesized with fission, yet too many not to use it after all
            those ages of technological advancements. Still collected and sold widely, mostly in its liquid form`,
		color: '#f2a41d',
		relativeFrequency: 0.132,
		baseQuantity: {
			min: 280,
			max: 525,
			distribution: 'Bell',
		},
	},
	[Resources.resourceId.oxygen]: {
		id: Resources.resourceId.oxygen,
		description: `Oxygen is the handiest source material ever. One can breath it or use it to burn anything`,
		color: '#f9f8e8',
		relativeFrequency: 0.12,
		baseQuantity: {
			min: 280,
			max: 525,
			distribution: 'Bell',
		},
	},
	[Resources.resourceId.helium]: {
		id: Resources.resourceId.helium,
		description: `Not only turns your voice funny, but also serves as a main source of nuclear fuel for most energy devices`,
		color: '#7da9db',
		relativeFrequency: 0.14,
		baseQuantity: {
			min: 1200,
			max: 5000,
			distribution: 'Parabolic',
		},
	},
	[Resources.resourceId.proteins]: {
		id: Resources.resourceId.proteins,
		description: `All living beings need food, air and water to survive, and those things, as well as most others we use for our daily needs, are made of 4 basic elements. Still it's tremendously difficult to synthesize most of those from ground up, so it's still important to collect and process proteins on location before distributing and recycling it.`,
		color: '#43c43e',
		relativeFrequency: 0.072,
		baseQuantity: {
			min: 25,
			max: 45,
			distribution: 'Uniform',
		},
	},
	[Resources.resourceId.halogens]: {
		id: Resources.resourceId.halogens,
		description: `Not as widely used as other gases, but still essential for chemical and military
            industry, halogenes are collected all over the galaxy in any modifications, and valued by many`,
		color: '#10a0a5',
		relativeFrequency: 0.05,
		baseQuantity: {
			min: 120,
			max: 180,
			distribution: 'Maximal',
		},
	},
	[Resources.resourceId.isotopes]: {
		id: Resources.resourceId.isotopes,
		description: `Fusion is somewhat an obsolete technology now, but some of radioactive elements possess
            other useful properties, so are still required by both militaries and civilians`,
		color: '#30a886',
		relativeFrequency: 0.051,
		baseQuantity: {
			min: 11,
			max: 25,
			distribution: 'Uniform',
		},
	},
};

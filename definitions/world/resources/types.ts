import Distributions from '../../maths/Distributions';

export namespace Resources {
	export enum resourceId {
		ores = 'res_ores',
		carbon = 'res_carbon',
		proteins = 'res_proteins',
		isotopes = 'res_isotopes',
		hydrogen = 'res_hydrogen',
		helium = 'res_helium',
		nitrogen = 'res_nitrogen',
		oxygen = 'res_oxygen',
		halogens = 'res_halogens',
		minerals = 'res_minerals',
	}

	export type TResourceUnion = `${resourceId}`;
	export type TResourceGeneric<T> = EnumProxy<resourceId, T>;
	export type TResourcePayload = TResourceGeneric<number>;
	export type TResourceDeposit = TResourceGeneric<Distributions.ISeedValue>;
	export type TResourceModifier = TResourceGeneric<IModifier> & { default?: IModifier };

	export interface IResourceDescription {
		description?: string;
		id: resourceId;
		color: string;
		relativeFrequency: number; // how often is this resource present in the universe, in relative measures
		baseQuantity: Distributions.ISeedValue; // how much spawns in one deposit
	}

	export type TResourceDictionary = TResourceGeneric<IResourceDescription>;
}

export default Resources;

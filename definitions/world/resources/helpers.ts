import BasicMaths from '../../maths/BasicMaths';
import { ResourceCaptions } from './constants';
import Resources from './types';

export function applyResourceModifier(
	p: Resources.TResourcePayload,
	...m: Resources.TResourceModifier[]
): Resources.TResourcePayload {
	return Object.keys(p).reduce(
		(acc, resourceType: Resources.resourceId) => ({
			...acc,
			[resourceType]: BasicMaths.applyModifier(
				p[resourceType],
				...m.map((m) => m[resourceType] || m.default || null).filter((v) => !!v),
			),
		}),
		{},
	) as Resources.TResourcePayload;
}

export function addResourcePayloads(...p: Resources.TResourcePayload[]): Resources.TResourcePayload {
	return p
		.filter((v) => !!v)
		.reduce((acc, payload) => {
			Object.keys(payload).forEach((k) => {
				acc[k] = (acc[k] || 0) + payload[k];
			});
			return acc;
		}, {});
}

export function resourceTemplate(num = 0): Resources.TResourcePayload {
	return Object.values(Resources.resourceId).reduce(
		(acc, rid) => ({
			...acc,
			[rid]: num,
		}),
		{},
	);
}

export function isEmptyResourcePayload(p: Resources.TResourcePayload): boolean {
	return !p || Object.keys(ResourceCaptions).reduce((v, rKey) => v && !p[rKey], true);
}

export const getResourceCaption = (r: Resources.resourceId) => ResourceCaptions[r] || 'All Resources';

export const describeResourcePayload = (r: Resources.TResourcePayload = {}) =>
	Object.keys(r)
		.map((t: Resources.resourceId) => `${r[t]} ${getResourceCaption(t)}`)
		.join(', ');

export const describeResourceModifier = (r: Resources.TResourceModifier = {}) =>
	Object.keys(r)
		.map((t: Resources.resourceId) => `${BasicMaths.describeModifier(r[t])} ${getResourceCaption(t)}`)
		.join(', ');

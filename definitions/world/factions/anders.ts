import MotorPlantFactory from '../../orbital/factories/all/Motorplant.200';
import { IPrototypeModifier } from '../../prototypes/types';
import HighHopesAbility from '../../tactical/abilities/all/Factions/HighHopes.70001';
import IndoctrinationAbility from '../../tactical/abilities/all/Factions/Indoctrination.70002';
import DefenderOfFaithChassis from '../../technologies/chassis/all/HoverTank/DefenderOfFaith.55003';
import ChaplainChassis from '../../technologies/chassis/all/Infantry/Chaplain.10006';
import QuadrapodChassis from '../../technologies/chassis/all/Quadrapod/Quadrapod.35000';
import RavagerChassis from '../../technologies/chassis/all/Quadrapod/Ravager.35002';
import ScoutChassis from '../../technologies/chassis/all/Scout/Scout.32000';
import UnitChassis from '../../technologies/types/chassis';
import SmallMissile from '../../technologies/weapons/all/missiles/SmallMissile.30000';
import PoliticalFactions from '../factions';
import Resources from '../resources/types';
import IFaction = PoliticalFactions.IFaction;
import TFactionID = PoliticalFactions.TFactionID;

const defaultModifier: IPrototypeModifier = {
	unitModifier: {
		abilityPowerModifier: [
			{
				bias: -0.25,
				min: 0,
				minGuard: 0,
			},
		],
	},
	chassis: [
		{
			baseCost: {
				bias: -2,
				minGuard: 2,
			},
			hitPoints: {
				bias: 5,
			},
		},
	],
};

const specialModifier: IPrototypeModifier = {
	unitModifier: defaultModifier.unitModifier,
	chassis: [
		...defaultModifier.chassis,
		{
			scanRange: {
				bias: 2,
			},
		},
	],
	weapon: [
		{
			default: {
				damageModifier: {
					factor: 1.15,
				},
			},
		},
	],
};

const AndersDefinition: IFaction = {
	id: TFactionID.anders,
	captionShort: 'Anders',
	captionLong: 'Anders Congregation',
	baseColor: '#34A798',
	secondaryColor: '#768692',
	description: `A quasi-religious autocracy, where citizens are taught devotion, zealotry and unity from the very childhood,
          and driven by belief in their own superiority to other factions. The symbol and the deity of the society is
          the Supreme Leader, who once was a human, but sacrificed his life and will for clinical immortality and
          continuous servitude. The title is passed only if current Leader dies, which never
          happened since Anders Congregation was formed from smaller states in Sculptor Dwarf Galaxy.
          The residence of His Highness is an artificial satellite which is known to orbit somewhere in
           that galaxy, and it's actual location is being well protected. Unprecedented indoctrination
          and propaganda technologies efficiently suppress ambitions and any sort of doubts, yielding high morale and combat efficiency,
          while definitely making any diplomacy with the Congregation a challenge, as they see agreement to foreign terms as infidelity,
          do not accept other values but theirs and even don't recognize citizens of any other faction to be equal to themselves.
          The Congregation seeks to seamlessly integrate worlds and systems by widely advertising their values and offering huge
          benefits to the newcomers. The values themselves stand for are supremacy, strong identity, loyalty and ideological conversion.

          High productivity of loyal workers is balanced by lack of individuality in the management, and willingness to die for the cause is compensated by endurance of the soldiers.
          Also Congregats mostly lack flexibility and tend to stick to long term planning`,
	features: [
		{
			type: 'ability',
			abilityId: HighHopesAbility.id,
		},
		{
			type: 'ability',
			abilityId: IndoctrinationAbility.id,
		},
		{
			type: 'technology',
			chassis: {
				[DefenderOfFaithChassis.id]: 1,
				[ChaplainChassis.id]: 1,
				[ScoutChassis.id]: 1,
				[QuadrapodChassis.id]: -1,
				[RavagerChassis.id]: -1,
			},
			factories: {
				[MotorPlantFactory.id]: 1,
			},
			weapons: {
				[SmallMissile.id]: 1,
			},
		},
		{
			type: 'base',
			abilityPower: {
				bias: 0.25,
			},
			counterIntelligenceLevel: {
				bias: -2,
			},
			repairOutput: {
				bias: 3,
			},
			maxRawMaterials: {
				bias: 80,
			},
			resourceConsumption: {
				[Resources.resourceId.proteins]: {
					factor: 0.92,
				},
			},
			bayCapacity: {
				bias: 1,
			},
		},
		{
			type: 'unit',
			[UnitChassis.chassisClass.scout]: { ...specialModifier },
			[UnitChassis.chassisClass.mecha]: { ...specialModifier },
			[UnitChassis.chassisClass.exosuit]: { ...specialModifier },
			default: { ...defaultModifier },
		},
	],
	extra: [
		'+5 Hit Points and -1 Production Cost to every Chassis',
		'-0.1 Ability Power for every Unit',
		'+0.15 Base Ability Power ',
		'-8% Base Proteins Consumption',
		'+3 Repair Points at every Facility in Output Mode',
		'+1 Bay Capacity at every Base',
		'+80 Raw Materials Capacity for Base Storage',
		'-5% Base Carbon Consumption',
		'+10% Trade buy resource rate', // @ TODO Trade Mechanics
		'-1 Counter Recon Level',
		'Unique unit: Defender of Faith; A heavily armored hover tank',
		'Exclusive unit: Chaplain; An infantry which gives bonuses to neighbouring units',
		'Unavailable units: Quadrapod. These devices are too costly to produce in Congregation infrastructure',
		'Unique base ability: High Hopes',
		'Unique base ability: Indoctrination',
	],
};

export default AndersDefinition;

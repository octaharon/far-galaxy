import { ArithmeticPrecision } from '../../maths/constants';
import MotorPlantFactory from '../../orbital/factories/all/Motorplant.200';
import RoboticsBayFactory from '../../orbital/factories/all/RoboticBay.300';
import TrainingHallFactory from '../../orbital/factories/all/TrainingHall.100';
import SacrificeAbility from '../../tactical/abilities/all/Factions/Sacrifice.70008';
import SliceAndDiceAbility from '../../tactical/abilities/all/Factions/SliceAndDice.70007';
import DroidChassis from '../../technologies/chassis/all/Droid/Droid.25000';
import T1024Chassis from '../../technologies/chassis/all/Droid/T-1024.25001';
import InfantryChassis from '../../technologies/chassis/all/Infantry/Infantry.10000';
import AdmiralsPrideChassis from '../../technologies/chassis/all/Manofwar/AdmiralsPride.69501';
import ScienceVesselChassis from '../../technologies/chassis/all/ScienceVessel/ScienceVessel.99000';
import ScoutChassis from '../../technologies/chassis/all/Scout/Scout.32000';
import HELLGun from '../../technologies/weapons/all/lasers/HELL.90000';
import AutoCannon from '../../technologies/weapons/all/machineguns/AutoCannon.20000';
import SmallMissile from '../../technologies/weapons/all/missiles/SmallMissile.30000';
import PoliticalFactions from '../factions';
import Resources from '../resources/types';
import IFaction = PoliticalFactions.IFaction;
import TFactionID = PoliticalFactions.TFactionID;

const PiratesFaction: IFaction = {
	id: TFactionID.pirates,
	captionShort: 'Pirates',
	captionLong: 'Pirate Clans',
	baseColor: '#3F2021',
	secondaryColor: '#DB0B5B',
	description: `Throughout history they have been called differently: barbarians, terrorists, savages, pirates. Those who don't like to share and see the world differently. Those, who deny order and collaboration. In modern times they are as big as all the other Factions and possess own production complex, research facilities, economical entities and a vast infrastructure. They fight for themselves with unique technologies and do not welcome strangers`,
	extra: [
		'Base Ability: Slice and Dice, deal 30 BIO Damage to every Unit in Area',
		'Base Ability: Sacrifice, place an effect on the Unit, which grants it +30% Damage and +3 Speed for 2 Turns, then deals 100% of its Max Hit Points as ANH Damage. If the unit is Massive, deal 50% of max Hit Points instead',
		"Unique Unit: Admiral's Pride, an improved Man-of-War with a signature Safety Dome Ability",
		'Exclusive Unit: T-1024, a Droid with a Cloak device ',
		'Unavailable Chassis: Science Vessel and its variants',
		'-10% Chassis costs',
		'+1 Status Effect Duration on friendly Units',
		'+5% Shield Capacity on all Shields',
		'-5% Base Isotopes Consumption',
		'-5% Base Halogens Consumption',
		'-5% Base Nitrogen Consumption',
		'-10% Base Research Points Output',
		'-10% Base Engineering Points Output',
		'+15 Base Production Capacity',
	],
	features: [
		{
			type: 'ability',
			abilityId: SliceAndDiceAbility.id,
		},
		{
			type: 'ability',
			abilityId: SacrificeAbility.id,
		},
		{
			type: 'technology',
			chassis: {
				[AdmiralsPrideChassis.id]: 1,
				[T1024Chassis.id]: 1,
				[ScoutChassis.id]: 1,
				[DroidChassis.id]: 1,
				[InfantryChassis.id]: 1,
				[ScienceVesselChassis.id]: -1,
			},
			factories: {
				[MotorPlantFactory.id]: 1,
				[TrainingHallFactory.id]: 1,
				[RoboticsBayFactory.id]: 1,
			},
			weapons: {
				[SmallMissile.id]: 1,
				[AutoCannon.id]: 1,
				[HELLGun.id]: 1,
			},
		},
		{
			type: 'base',
			productionCapacity: {
				bias: 15,
			},
			researchOutput: {
				factor: 0.9,
			},
			engineeringOutput: {
				factor: 0.9,
			},
			resourceConsumption: {
				[Resources.resourceId.nitrogen]: {
					factor: 0.95,
				},
				[Resources.resourceId.halogens]: {
					factor: 0.95,
				},
				[Resources.resourceId.isotopes]: {
					factor: 0.95,
				},
			},
		},
		{
			type: 'unit',
			default: {
				unitModifier: {
					statusDurationModifier: [
						{
							bias: 1,
						},
					],
					abilityPowerModifier: [
						{
							factor: 0.8,
						},
					],
				},
				chassis: [
					{
						baseCost: {
							factor: 0.9,
						},
						speed: {
							bias: 1,
							minGuard: ArithmeticPrecision,
							min: 0,
						},
						dodge: { default: { bias: 0.1 } },
					},
				],
				shield: [
					{
						shieldAmount: {
							factor: 1.05,
						},
					},
				],
			},
		},
	],
};

export default PiratesFaction;

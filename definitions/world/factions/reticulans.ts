import MotorPlantFactory from '../../orbital/factories/all/Motorplant.200';
import ProvisionalDefenseAbility from '../../tactical/abilities/all/Factions/ProvisionalDefense.70009';
import ThetaProjectionAbility from '../../tactical/abilities/all/Factions/ThetaProjection.70010';
import ColossusArtilleryChassis from '../../technologies/chassis/all/Artillery/Colossus.30002';
import CharonChassis from '../../technologies/chassis/all/Fortress/Charon.59002';
import LauncherPadChassis from '../../technologies/chassis/all/LauncherPad/LauncherPad.86000';
import MangonelChassis from '../../technologies/chassis/all/LauncherPad/Mangonel.86002';
import TrebuchetChassis from '../../technologies/chassis/all/LauncherPad/Trebuchet.86001';
import ScoutChassis from '../../technologies/chassis/all/Scout/Scout.32000';
import UnitChassis from '../../technologies/types/chassis';
import Damage from '../../tactical/damage/damage';
import { IPrototypeModifier } from '../../prototypes/types';
import FlakeCannon from '../../technologies/weapons/all/projectiles/FlakeCannon.15000';
import PoliticalFactions from '../factions';
import Resources         from '../resources/types';
import IFaction = PoliticalFactions.IFaction;
import TFactionID = PoliticalFactions.TFactionID;

const defaultWeaponModifier: IPrototypeModifier = {
	weapon: [
		{
			default: {
				baseCost: {
					bias: -2,
				},
				aoeRadiusModifier: {
					bias: 1,
				},
			},
		},
	],
};
const unitModifier: IPrototypeModifier = {
	shield: [
		{
			[Damage.damageType.kinetic]: {
				factor: 0.75,
			},
			[Damage.damageType.antimatter]: {
				factor: 0.75,
			},
			shieldAmount: {
				bias: 10,
			},
		},
	],
	weapon: [
		{
			default: {
				baseCost: {
					bias: -2,
				},
				baseRate: {
					factor: 1.25,
				},
				aoeRadiusModifier: {
					bias: 1,
				},
			},
		},
	],
};

const ReticulansFaction: IFaction = {
	id: TFactionID.reticulans,
	captionShort: 'Reticulans',
	captionLong: 'Reticulum Combat Corpus',
	secondaryColor: '#4EC3E0',
	baseColor: '#F4AF23',
	description: `A paramilitary hyperstate based at Reticulum II galaxy, developed from several oldest economical alliances at area, which once had an imprudence of investing into creating own private defense force, that expectedly gained enough power to subdue them and establish their own rule over the region`,
	extra: [
		'-1 Research Points at every Establishment',
		'+1 Engineering Points at every Establishment',
		'+2 Bay Capacity at every Base',
		'-8% Base Oxygen Consumption',
		'-2 Production Cost for every Weapon',
		'+1 AoE Radius for all Weapons',
		'Base Ability: Theta Projection, a focused Circle Area weapon that does 40-100 Minimized EMG Damage as Beam in radius of 3. Gain 25 Energy at Base Storage per each Unit destroyed',
		'Base Ability: Provisional Defense, applies an effect to Unit that heals it for 50% of Max Hit Points in the end of turn. If a Unit is destroyed before, gain +25 Production Points at every Factory, that has a selected Prototype',
		'Unique Unit: CHARON, a Fortress with Hangar Bay and better Armor',
		'Exclusive Unit: Colossus, an Artillery Chassis with a Titan Weapon Mount',
	],
	features: [
		{
			type: 'ability',
			abilityId: ThetaProjectionAbility.id,
		},
		{
			type: 'ability',
			abilityId: ProvisionalDefenseAbility.id,
		},
		{
			type: 'technology',
			chassis: {
				[CharonChassis.id]: 1,
				[ScoutChassis.id]: 1,
				[ColossusArtilleryChassis.id]: 1,
				[LauncherPadChassis.id]: -1,
				[MangonelChassis.id]: -1,
				[TrebuchetChassis.id]: -1,
			},
			factories: {
				[MotorPlantFactory.id]: 1,
			},
			weapons: {
				[FlakeCannon.id]: 1,
			},
		},
		{
			type: 'base',
			engineeringOutput: {
				bias: 3,
			},
			researchOutput: {
				bias: -2,
			},
			bayCapacity: {
				bias: 2,
			},
			resourceConsumption: {
				[Resources.resourceId.oxygen]: {
					factor: 0.92,
				},
			},
		},
		{
			type: 'unit',
			default: defaultWeaponModifier,
			[UnitChassis.chassisClass.lav]: unitModifier,
			[UnitChassis.chassisClass.hav]: unitModifier,
			[UnitChassis.chassisClass.hovertank]: unitModifier,
		},
	],
};

export default ReticulansFaction;

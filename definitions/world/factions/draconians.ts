import TrainingHallFactory from '../../orbital/factories/all/TrainingHall.100';
import { IPrototypeModifier } from '../../prototypes/types';
import QuarkLiftAbility from '../../tactical/abilities/all/Factions/QuarkLift.70006';
import SupplyDropAbility from '../../tactical/abilities/all/Factions/SupplyDrop.70005';
import GolliwogChassis from '../../technologies/chassis/all/BioTech/Gollywog.40003';
import BruteChassis from '../../technologies/chassis/all/Infantry/Brute.10005';
import XDriverChassis from '../../technologies/chassis/all/Infantry/XDriver.10002';
import EliteGuardianChassis from '../../technologies/chassis/all/Infantry/EliteGuardian.10001';
import InfantryChassis from '../../technologies/chassis/all/Infantry/Infantry.10000';
import UnitChassis from '../../technologies/types/chassis';
import FuzeShells from '../../technologies/weapons/all/cannons/FuzeShells.10000';
import PoliticalFactions from '../factions';
import Resources from '../resources/types';
import IFaction = PoliticalFactions.IFaction;
import TFactionID = PoliticalFactions.TFactionID;

const unitModifier: IPrototypeModifier = {
	weapon: [
		{
			default: {
				damageModifier: {
					factor: 1.2,
				},
				baseAccuracy: {
					bias: 0.3,
				},
			},
		},
	],
	chassis: [
		{
			hitPoints: {
				bias: 10,
			},
			speed: {
				bias: 2,
			},
		},
	],
};

export const DraconiansFaction: IFaction = {
	id: TFactionID.draconians,
	captionShort: 'Draconians',
	captionLong: 'Draco Dwarf Protectorate',
	secondaryColor: '#025F1D',
	baseColor: '#F6EB61',
	description: `Another former galactic corporation, which has been building  power and wealth over millennia,
        until they eventually dominated the quark transport market thus becoming one the most significant powers in the Universe,
        comparable to other hyperstates on scale.
        An entry threshold was always quite high for Draconians, but after the independence declaration they became straightforward
        egalitarian, severely filtering those willing to move to Draco Dwarf and demanding extensive servitude from those who do.
        Recently Draconians have become the first faction in the Universe which completely stopped reproducing naturally in favor of genetic engineering. Due to those
        facts every person is highly valuable, and almost noone is willing to participate in resource battles, except for few clans,
        which dedicate their existence to upbringing new, genetically bred, pilots for exosuits and mechs.

        To be a real draconian means always to strive for the maximum influence, while retaining dignity and quality of everything done,
        including introduction of new members to the society.
          `,
	features: [
		{
			type: 'ability',
			abilityId: SupplyDropAbility.id,
		},
		{
			type: 'ability',
			abilityId: QuarkLiftAbility.id,
		},
		{
			type: 'technology',
			chassis: {
				[BruteChassis.id]: 1,
				[InfantryChassis.id]: 1,
				[GolliwogChassis.id]: 1,
				[EliteGuardianChassis.id]: -1,
				[XDriverChassis.id]: -1,
			},
			factories: {
				[TrainingHallFactory.id]: 1,
			},
			weapons: {
				[FuzeShells.id]: 1,
			},
		},
		{
			type: 'base',
			resourceOutput: {
				[Resources.resourceId.nitrogen]: {
					factor: 1.1,
				},
			},
			abilityPower: {
				factor: 0.8,
				min: 0,
				minGuard: 0,
			},
			maxRawMaterials: { bias: 50 },
			materialOutput: {
				factor: 1.05,
			},
			counterIntelligenceLevel: {
				bias: 1,
			},
			productionCapacity: {
				factor: 0.9,
			},
		},
		{
			type: 'unit',
			[UnitChassis.chassisClass.infantry]: {
				...unitModifier,
			},
			[UnitChassis.chassisClass.biotech]: {
				...unitModifier,
			},
			[UnitChassis.chassisClass.swarm]: {
				...unitModifier,
			},
		},
	],
	extra: [
		'+40% Hit Chance for all Mecha Chassis',
		'+10% Mecha Production Cost',
		'+2 Armor Protection for Mech and Biotech Chassis',
		'+2 Speed for Mech and Biotech Chassis',
		'+10% Base Nitrogen Output',
		'-10% Production Capacity',
		'-0.25 Base Ability Power',
		'+5% Raw Materials at every Base',
		'+50 Raw Materials Storage at every Base',
		'+1 Counter-Recon Level',
		'Unique Unit: Brawler, a genetically modified Infantry with a signature Mycotoxin Ability',
		'Exclusive unit: Golliwog, a light scouting Biotech',
		`Unavailable units: Elite Guardians and X Drivers`,
		`Unique base ability: Supply Drop; restore 35% of Shield Capacity in Radius 3 and grant them +50% Attack Rate temporarily`,
		"Unique base ability: Quark Lift; Steal 50 Raw Materials from another Players' Base Storage. For every absent piece deal 2 WRP Damage to Base Hit Points",
	],
};

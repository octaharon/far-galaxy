import { fastMerge } from '../../core/dataTransformTools';
import RoboticsBayFactory from '../../orbital/factories/all/RoboticBay.300';
import SpinInversionAbility from '../../tactical/abilities/all/Factions/SpinInversion.70011';
import WarpRefractionAbility from '../../tactical/abilities/all/Factions/WarpRefraction.70012';
import DroidChassis from '../../technologies/chassis/all/Droid/Droid.25000';
import VindicatorChassis from '../../technologies/chassis/all/MotherShip/Vindicator.89001';
import AATurretChassis from '../../technologies/chassis/all/PointDefense/AATurret.57003';
import GunTurretChassis from '../../technologies/chassis/all/PointDefense/GunTurret.57002';
import MissileTurretChassis from '../../technologies/chassis/all/PointDefense/MissileTurret.57001';
import SentinelTurrentChassis from '../../technologies/chassis/all/PointDefense/SentinelTurret.57004';
import UnitChassis from '../../technologies/types/chassis';
import { IPrototypeModifier } from '../../prototypes/types';
import HELLGun from '../../technologies/weapons/all/lasers/HELL.90000';
import Weapons from '../../technologies/types/weapons';
import PoliticalFactions from '../factions';
import Resources from '../resources/types';
import IFaction = PoliticalFactions.IFaction;
import TFactionID = PoliticalFactions.TFactionID;
import TWeaponGroupType = Weapons.TWeaponGroupType;

const unitModifier: IPrototypeModifier = {
	unitModifier: {
		abilityPowerModifier: [
			{
				bias: 0.25,
			},
		],
	},
	chassis: [
		{
			dodge: {
				default: {
					bias: 0.05,
				},
			},
		},
	],
	armor: [
		{
			baseCost: {
				bias: 3,
			},
		},
	],
	shield: [
		{
			baseCost: {
				bias: 3,
			},
		},
	],
	weapon: [
		{
			[TWeaponGroupType.Rockets]: {
				baseCost: {
					factor: 0.85,
				},
			},
			[TWeaponGroupType.Bombs]: {
				baseCost: {
					factor: 0.85,
				},
			},
		},
	],
};

const advancedUnitModifier: IPrototypeModifier = {
	...unitModifier,
	chassis: [
		{
			hitPoints: {
				factor: 1.1,
			},
		},
	],
	shield: [
		{
			shieldAmount: {
				bias: 20,
			},
			baseCost: {
				bias: 3,
			},
		},
	],
};

const StellarFaction: IFaction = {
	id: TFactionID.stellarians,
	captionShort: 'Stellarians',
	captionLong: 'Stellar One Hypercorp',
	baseColor: '#10069F',
	secondaryColor: '#110f25',
	description: `One of the first intergalactic corporations, named ambitiously, that launched Starwarp missions
        to Canis Major Dwarf Galaxy (CMa Dwarf) and started researching and processing dark matter, which predictably
        led to extreme success and exponential growth of the warp technology market and the corporation itself.
        So eventually they got they own security service, surveillance, established their
        own colonies in the whole star clusters for their employees, and at some point they, obviously, have declared independence.
        Now this is the biggest interstellar business empire, spanning over the whole NGC 2808 cluster,
        and any contract they will ever offer to anyone is lifetime.
        All children born on the SPA premises is considered an employee after they are 5 years old.
        Ironically, 5 years is slightly less than the minimal amount of time required to leave the CMa Dwarf galaxy without using Starwarp.

        Working for the Stellarians is not that bad though, as they have special trade agreements with most parties in the known Universe,
        and offer exceptional educational and insurance benefits to employees.
          `,
	extra: [
		'+1 Production Cost on every Shield and Armor',
		'-10% Production Cost on Rocket and Bombs weapons',
		'+20 Shield Capacity and +10% Hit Points for Droids, Rovers and Submarines',
		'+5% Dodge on every Chassis',
		'+1 Energy Consumption in every Building',
		'+1 Recon level',
		'+5% Base Halogens and Helium output',
		'+10% Trade sell resource rate', // @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/27
		'Exclusive unit: Sentinel Turret - an improved point defense with an equipment slot and longer scan range',
		'Unique unit: The Vindicator - the most advanced Mothership in the Universe',
		`Unavailable units: AA Turret, Gun Turret, Missile Turret`,
		`Unique base ability: Spin Inversion; an AOE which removes any Warp, Unstoppable and Tachyon Perks,
            as well as Warp and Antimatter Armor from all units in an area`,
		'Unique base ability: Warp Refraction; Every time a Unit is Attacked, return 50% of the Damage to the Attacker as WRP',
	],
	features: [
		{
			type: 'ability',
			abilityId: WarpRefractionAbility.id,
		},
		{
			type: 'ability',
			abilityId: SpinInversionAbility.id,
		},
		{
			type: 'technology',
			chassis: {
				[VindicatorChassis.id]: 1,
				[SentinelTurrentChassis.id]: 1,
				[DroidChassis.id]: 1,
				[AATurretChassis.id]: -1,
				[MissileTurretChassis.id]: -1,
				[GunTurretChassis.id]: -1,
			},
			factories: {
				[RoboticsBayFactory.id]: 1,
			},
			weapons: {
				[HELLGun.id]: 1,
			},
		},
		{
			type: 'unit',
			[UnitChassis.chassisClass.droid]: advancedUnitModifier,
			[UnitChassis.chassisClass.rover]: advancedUnitModifier,
			[UnitChassis.chassisClass.submarine]: advancedUnitModifier,
			default: unitModifier,
		},
		{
			type: 'base',
			intelligenceLevel: {
				bias: 1,
			},
			abilityPower: {
				bias: -0.25,
				min: 0,
				minGuard: 0,
			},
			resourceOutput: {
				[Resources.resourceId.halogens]: {
					factor: 1.05,
				},
				[Resources.resourceId.helium]: {
					factor: 1.05,
				},
			},
			energyConsumption: {
				bias: 1,
			},
			researchOutput: {
				factor: 1.05,
			},
		},
	],
};

export default StellarFaction;

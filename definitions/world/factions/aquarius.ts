import RoboticsBayFactory from '../../orbital/factories/all/RoboticBay.300';
import { IPrototypeModifier } from '../../prototypes/types';
import ClusterWipeAbility from '../../tactical/abilities/all/Factions/ClusterWipe.70004';
import NetworkTwistAbility from '../../tactical/abilities/all/Factions/NetworkTwist.70003';
import DroidChassis from '../../technologies/chassis/all/Droid/Droid.25000';
import MantisChassis from '../../technologies/chassis/all/Fighter/Mantis.82001';
import WandererChassis from '../../technologies/chassis/all/ScienceVessel/TheWanderer.99001';
import EagleChassis from '../../technologies/chassis/all/StrikeAircraft/Eagle.85001';
import OspreyChassis from '../../technologies/chassis/all/StrikeAircraft/Osprey.85002';
import SparrowChassis from '../../technologies/chassis/all/StrikeAircraft/Sparrow.85003';
import StrikeAircraftChassis from '../../technologies/chassis/all/StrikeAircraft/StrikeAircraft.85000';
import UnitChassis from '../../technologies/types/chassis';
import AutoCannon from '../../technologies/weapons/all/machineguns/AutoCannon.20000';
import PoliticalFactions from '../factions';
import Resources         from '../resources/types';
import IFaction = PoliticalFactions.IFaction;
import TFactionID = PoliticalFactions.TFactionID;

const AbilityModifier: IPrototypeModifier = {
	unitModifier: {
		abilityPowerModifier: [
			{
				factor: 1.25,
			},
		],
		abilityRangeModifier: [
			{
				bias: 1,
			},
		],
	},
};

const AquariusUnitModifier: IPrototypeModifier = {
	chassis: [
		{
			baseCost: {
				factor: 0.9,
			},
			hitPoints: {
				bias: 20,
			},
		},
	],
	...AbilityModifier,
};

const AquariusFaction: IFaction = {
	id: TFactionID.aquarians,
	captionShort: 'Aquarians',
	captionLong: 'Aquarius Concord',
	secondaryColor: '#D539B5',
	baseColor: '#9FAEE5',
	description: `Highly secretive society of Aquarius Dwarf resides in the most isolated and, supposedly, one of the youngest and richest galaxies, and, according to rumors, mostly consists of cybernetic citizens. They are known masters of reconnaissance that trade military and diplomatic secrets for exclusive deals on ores and transuranic elements or use them to blackmail people and force them to serve their robotic masters`,
	extra: [
		'+2 Counter-Recon Level',
		'+2 Recon Level',
		'-20% Energy Base Storage',
		'+10% Base Ores Output',
		'+5% Base Isotopes Output',
		'-10% Production Cost for Support Ship, Support Vehicle and Support Aircraft Chassis',
		'+20% Hit Points for Support Ship, Support Vehicle and Support Aircraft Chassis',
		'+1 Range on all Abilities',
		'+0.25 Ability Power for Base Abilities',
		'+25% Unit Ability Power',
		'Unique Base Ability: Network Twist, permanently reduce Dodge by 25% and remove Evasion Module and Hull Module in Area of 4',
		'Unique Base Ability: Cluster Wipe, a Unit takes 50 EMP Damage for every Effect it has',
		'Unique unit: The Wanderer, a custom science vessel with 3 equipment slots, L armor and L shield',
		'Exclusive unit: Mantis, a highly advanced Robotic fighter plane',
		'Unavailable unit: Strike Aircraft and its variants',
	],
	features: [
		{
			type: 'ability',
			abilityId: NetworkTwistAbility.id,
		},
		{
			type: 'ability',
			abilityId: ClusterWipeAbility.id,
		},
		{
			type: 'technology',
			chassis: {
				[DroidChassis.id]: 1,
				[MantisChassis.id]: 1,
				[WandererChassis.id]: 1,
				[StrikeAircraftChassis.id]: -1,
				[EagleChassis.id]: -1,
				[SparrowChassis.id]: -1,
				[OspreyChassis.id]: -1,
			},
			factories: {
				[RoboticsBayFactory.id]: 1,
			},
			weapons: {
				[AutoCannon.id]: 1,
			},
		},
		{
			type: 'base',
			abilityPower: { bias: 0.25 },
			resourceOutput: {
				[Resources.resourceId.ores]: {
					factor: 1.1,
				},
				[Resources.resourceId.isotopes]: {
					factor: 1.05,
				},
			},
			counterIntelligenceLevel: {
				bias: 2,
			},
			intelligenceLevel: {
				bias: 2,
			},
			maxEnergyCapacity: {
				factor: 0.8,
			},
		},
		{
			type: 'unit',
			[UnitChassis.chassisClass.sup_vehicle]: { ...AquariusUnitModifier },
			[UnitChassis.chassisClass.sup_aircraft]: { ...AquariusUnitModifier },
			[UnitChassis.chassisClass.sup_ship]: { ...AquariusUnitModifier },
			default: AbilityModifier,
		},
	],
};

export default AquariusFaction;

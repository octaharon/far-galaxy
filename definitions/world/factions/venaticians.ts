import MotorPlantFactory from '../../orbital/factories/all/Motorplant.200';
import LaborFeatAbility from '../../tactical/abilities/all/Factions/LaborFeat.70014';
import NanoboostAbility from '../../tactical/abilities/all/Factions/Nanoboost.70013';
import BehemothChassis from '../../technologies/chassis/all/CombatMech/Behemoth.18001';
import FlagShipChassis from '../../technologies/chassis/all/Manofwar/Manofwar.69500';
import MatriarchChassis from '../../technologies/chassis/all/MotherShip/Matriarch.89002';
import MothershipChassis from '../../technologies/chassis/all/MotherShip/Mothership.89000';
import MaybugChassis from '../../technologies/chassis/all/Scout/Maybug.32004';
import ScoutChassis from '../../technologies/chassis/all/Scout/Scout.32000';
import UnitChassis from '../../technologies/types/chassis';
import { IPrototypeModifier } from '../../prototypes/types';
import RailgunCannon from '../../technologies/weapons/all/railguns/RailgunCannon.25000';
import Weapons from '../../technologies/types/weapons';
import PoliticalFactions from '../factions';
import Resources         from '../resources/types';
import IFaction = PoliticalFactions.IFaction;
import TFactionID = PoliticalFactions.TFactionID;

const venaticianDroneBonus = {
	[Weapons.TWeaponGroupType.Drone]: {
		baseCost: {
			factor: 0.9,
		},
	},
};

const venaticianArmorBonus: IPrototypeModifier = {
	armor: [
		{
			baseCost: {
				bias: -3,
				minGuard: 3,
			},
		},
	],
};

const venaticianUnitBonus: IPrototypeModifier = {
	...venaticianArmorBonus,
	chassis: [
		{
			hitPoints: {
				bias: 20,
			},
		},
	],
	weapon: [
		{
			default: {
				baseRange: {
					bias: 1,
				},
			},
		},
		venaticianDroneBonus,
	],
};

const VenaticiansFaction: IFaction = {
	id: TFactionID.venaticians,
	captionShort: 'Venaticians',
	captionLong: 'Venatician Domain',
	secondaryColor: '#FF6900',
	baseColor: '#671E75',
	description: `Vast interstellar community comprised of numerous small corpolitical entities,
        with an administrative center in a galaxy of Canes Venatici II, thus giving the name. The community is governed
        by fully automated system and has a flat hierarchy in every branch of society. In Venatician agenda
        human life costs as much as the particular human contributes to the society, which leaves no space for
        freeloaders and dependents, and despite significant differences in the ideology of comprised states,
        the solidity of economical system forces all members of community to follow common goals. These goals include,
        but not limited to prosperity, impervious defendability, resource efficiency and sustainability. Venaticians do not
        possess ambitions to conquer and expand to the rest of Universe, but will fiercely fight for what is theirs.

        Due to that the civilization of Venaticians can boast some serious industrial advancements over other factions,
        while having strong accent on defensive tactics, both military and economical.
        Flat hierarchy, on the other side, slows down the professional growth of officers and limits the capabilities of guilds.`,
	extra: [
		'+120 Base Max Hit Points',
		'+300 Base Energy Storage',
		'-10% cost for Armor',
		'+0.5 Ability Power for Base Abilities',
		'-3 cost for Drone weapons',
		'+5% Base Ores and Minerals Output',
		'+20% guild fee', // @ TODO Guild mechanics
		'Unique Chassis: Behemoth; a giant heavy duty combat mech with limited movement',
		'Exclusive Chassis: Maybug; an improved and better protected version of Scout',
		'Unavailable Chassis: Mothership, Man-Of-War. Lack of strong hierarchy means there is no space for preferences',
		'Unique base ability: Labor Feat; Produce a bulk of Repair Points and stack minor Shield Protection on all Units at Orbital Bay',
		'Unique base ability: Nanoboost; Temporarily increase Production Speed at all Factories',
	],
	features: [
		{
			type: 'ability',
			abilityId: NanoboostAbility.id,
		},
		{
			type: 'ability',
			abilityId: LaborFeatAbility.id,
		},
		{
			type: 'technology',
			chassis: {
				[ScoutChassis.id]: 1,
				[MaybugChassis.id]: 1,
				[BehemothChassis.id]: 1,
				[MothershipChassis.id]: -1,
				[MatriarchChassis.id]: -1,
				[FlagShipChassis.id]: -1,
			},
			factories: {
				[MotorPlantFactory.id]: 1,
			},
			weapons: {
				[RailgunCannon.id]: 1,
			},
		},
		{
			type: 'unit',
			default: {
				...venaticianArmorBonus,
				weapon: [venaticianDroneBonus],
			},
			[UnitChassis.chassisClass.artillery]: {
				...venaticianUnitBonus,
			},
			[UnitChassis.chassisClass.launcherpad]: {
				...venaticianUnitBonus,
			},
			[UnitChassis.chassisClass.helipad]: {
				...venaticianUnitBonus,
			},
		},
		{
			type: 'base',
			maxHitPoints: {
				bias: 120,
			},
			abilityPower: { bias: 0.5 },
			maxEnergyCapacity: {
				bias: 300,
			},
			resourceOutput: {
				[Resources.resourceId.ores]: {
					factor: 1.05,
				},
				[Resources.resourceId.minerals]: {
					factor: 1.05,
				},
			},
		},
	],
};

export default VenaticiansFaction;

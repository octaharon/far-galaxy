import PoliticalFactions from '../factions';
import IFaction = PoliticalFactions.IFaction;
import TFactionID = PoliticalFactions.TFactionID;

const UnalliedFaction: IFaction = {
	id: TFactionID.none,
	captionShort: 'Unallied',
	captionLong: 'No Alliances',
	baseColor: '#34A798',
	secondaryColor: '#768692',
	description: `This entity does not belong to any known Factions`,
	features: [],
	extra: [],
};

export default UnalliedFaction;

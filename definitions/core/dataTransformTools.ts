import $ from './InterfaceTypes';
import _ from 'underscore';
export function partialObject<T, K extends keyof T>(sourceObject: T, keys: K[]): Partial<T> {
	return keys.reduce((obj, key) => Object.assign(obj, { [key]: sourceObject[key] }), {});
}

export function partialReduce<T, K extends keyof T, R>(
	sourceObject: T,
	keys: K[],
	initialValue: R,
	reducer: (acc: R, k: any) => R,
): R {
	return keys.reduce((acc, k) => reducer(acc, sourceObject[k]), initialValue);
}

export function guardMerge<T>(target: any, source: any): T {
	let k: string;
	for (k in source) if (source.hasOwnProperty(k)) target[k] = source[k];
	return target;
}

export function fastMerge<T extends $.TDictionary = $.TDictionary>(obj1: T, ...objects: Array<Partial<T>>): T;
export function fastMerge<T extends $.TDictionary = $.TDictionary>(...objects: Array<Partial<T>>): Partial<T> {
	const output = {} as T;
	let i: keyof T = '';
	for (const obj of objects)
		for (i in obj) {
			if (obj.hasOwnProperty(i)) output[i] = obj[i];
		}
	return output;
}

// https://stackoverflow.com/a/40294058/7100389
export function cloneObject<T extends object>(obj: T, hash = new WeakMap()): T {
	if (!obj) return obj;
	if (Object(obj) !== obj) return obj;
	if (hash.has(obj)) return hash.get(obj);
	const result =
		obj instanceof Set
			? new Set(obj)
			: obj instanceof Map
			? new Map(Array.from(obj, ([key, val]) => [key, cloneObject(val, hash)]))
			: obj instanceof Date
			? new Date(obj)
			: obj instanceof RegExp
			? new RegExp(obj.source, obj.flags)
			: obj.constructor
			? new (obj.constructor as ClassConstructor<typeof obj>)()
			: Object.create(null);
	hash.set(obj, result);
	return Object.assign(result, ...Object.keys(obj).map((key) => ({ [key]: cloneObject(obj[key], hash) })));
}

export function instantiate<T>(className: string, constructorArgs?: any[]): T {
	let global;
	try {
		// eslint-disable-next-line no-new-func,no-eval
		global = Function('return this')() || this;
	} catch (e) {
		global = window;
	}
	return new global[className](...constructorArgs) as T;
}

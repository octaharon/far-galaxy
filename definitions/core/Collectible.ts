import InterfaceTypes from './InterfaceTypes';
import Serializable, { ISerializable, ISerializableFactory, SerializableFactory, TSerializable } from './Serializable';

export type IStaticCollectible = {
	id: number;
	caption?: string;
	description?: string;
};

export type TCollectible<ObjectType, StaticType extends IStaticCollectible = IStaticCollectible> = IStaticCollectible &
	TSerializable<ObjectType, StaticType>;
export type TSerializedCollectible = {
	id: number;
} & IWithInstanceID;

export interface ICollectible<
	PropType extends {},
	SerializedType extends TSerializedCollectible,
	StaticType extends IStaticCollectible = IStaticCollectible,
> extends ISerializable<PropType, SerializedType> {
	caption: string;
	description: string;
	static: StaticType;
}

export interface ICollectibleFactory<
	PropType extends {},
	SerializedType extends PropType & TSerializedCollectible,
	ObjectType extends ICollectible<PropType, SerializedType, StaticType>,
	StaticType extends IStaticCollectible = IStaticCollectible,
> extends ISerializableFactory<PropType, SerializedType, ObjectType> {
	get<T extends TCollectible<ObjectType, StaticType>>(id: number): T;

	instantiate(props: Partial<PropType>, id: number, ...args: any[]): ObjectType;

	fill(): ICollectibleFactory<PropType, SerializedType, ObjectType, StaticType>;

	getAll<T extends TCollectible<ObjectType, StaticType>>(): InterfaceTypes.TIdMap<T>;

	getIds(): number[];
}

export abstract class Collectible<
		PropType extends {},
		SerializedType extends TSerializedCollectible & PropType,
		StaticType extends IStaticCollectible,
	>
	extends Serializable<PropType, SerializedType>
	implements ICollectible<PropType, SerializedType, StaticType>
{
	['constructor']: TCollectible<this, StaticType>;
	public static readonly id: number = 0;
	public static readonly caption?: string = 'Generic';
	public static readonly description?: string = 'Generic';

	public get caption() {
		// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/2
		return this.static.caption;
	}

	public get description() {
		// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/2
		return this.static.description;
	}

	public get static() {
		return this.constructor;
	}

	public getDescriptor(typeDescriptor = this.typeDescriptor) {
		return typeDescriptor;
	}

	protected __serializeCollectible() {
		return {
			id: this.static.id,
			...this.__serializeSerializable(),
		};
	}
}

export default Collectible;

export abstract class CollectibleFactory<
		PropType extends {},
		SerializedType extends PropType & TSerializedCollectible,
		ObjectType extends ICollectible<PropType, SerializedType, StaticType>,
		StaticType extends IStaticCollectible = IStaticCollectible,
	>
	extends SerializableFactory<PropType, SerializedType, ObjectType>
	implements ICollectibleFactory<PropType, SerializedType, ObjectType, StaticType>
{
	protected __constructors: InterfaceTypes.TIdMap<TCollectible<ObjectType, StaticType>> = {};

	public unserialize(t: SerializedType): ObjectType {
		if (!t) return null;
		const f = this.instantiate(t, t.id);
		if (!f) return null;
		f.setProps(t);
		f.setInstanceId(t.instanceId);
		return f;
	}

	public clone(o: ObjectType): ObjectType {
		const c = o.serialize();
		c.instanceId = o.generateInstanceId();
		return this.unserialize(c);
	}

	public get<T extends TCollectible<ObjectType, StaticType>>(id: number): T {
		const c = this.__constructors[id] as T;
		if (!c) throw new Error(`Can't find item ${id} in ${this.constructor.name}`);
		return c;
	}

	public getAll<T extends TCollectible<ObjectType, StaticType>>(): InterfaceTypes.TIdMap<T> {
		return this.__constructors as InterfaceTypes.TIdMap<T>;
	}

	public getIds() {
		return Object.keys(this.__constructors ?? {}).map(Number);
	}

	public abstract fill(): ICollectibleFactory<PropType, SerializedType, ObjectType, StaticType>;

	protected __fillItems<C extends TCollectible<ObjectType, StaticType>>(
		objects: C[],
		callback?: (item: C) => any,
	): CollectibleFactory<PropType, SerializedType, ObjectType, StaticType> {
		for (const o of objects) {
			if (!Number.isFinite(o.id) || o.id <= 0) throw new Error(`Possibly invalid ID: ${o.name} has ID ${o.id}`);
			if (this.__constructors[o.id]) {
				throw new Error(
					`Possible collision: ${o.name} and ${this.__constructors[o.id].name} share the same ID: ${o.id}`,
				);
			}
			this.__constructors[o.id] = o;
			if (callback) {
				callback(o);
			}
		}
		return this;
	}

	protected __instantiateCollectible(props: Partial<PropType>, id: number, ...args: any[]) {
		const instance = this.__constructors[id];
		if (instance) {
			return this.__instantiateSerializable(props, instance, ...args);
		} else throw new Error(`Invalid Collectible ID: ${id}`);
	}
}

export function doesCollectibleSetIncludeItem(id: number | string, idList: Array<number | string> = []) {
	for (let i = 0; i < idList?.length; i++) if (id && idList[i] && String(idList[i]) === String(id)) return true;
	return false;
}

export function findCollectibleItemInSet(id: number | string, idList: Array<number | string> = []) {
	for (let i = 0; i < idList?.length; i++) if (id && idList[i] && String(idList[i]) === String(id)) return i;
	return -1;
}

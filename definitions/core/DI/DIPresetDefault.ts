import PlayerBaseManager from '../../orbital/base/PlayerBaseManager';
import EstablishmentManager from '../../orbital/establishments/EstablishmentManager';
import FacilityManager from '../../orbital/facilities/FacilityManager';
import FactoryManager from '../../orbital/factories/FactoryManager';
import PlayerManager from '../../player/PlayerManager';
import AbilityManager from '../../tactical/abilities/AbilityManager';
import ChassisManager from '../../technologies/chassis/ChassisManager';
import ArmorManager from '../../technologies/armor/ArmorManager';
import ShieldManager from '../../technologies/shields/ShieldManager';
import EquipmentManager from '../../technologies/equipment/EquipmentManager';
import PrototypeManager from '../../prototypes/PrototypeManager';
import UnitManager from '../../tactical/units/UnitManager';
import WeaponManager from '../../technologies/weapons/WeaponManager';
import CombatMapManager from '../../tactical/map/CombatMapManager';
import MapCellManager from '../../tactical/map/MapCellManager';
import SalvageManager from '../../tactical/salvage/SalvageManager';
import AuraManager from '../../tactical/statuses/Auras/AuraManager';
import EffectManager from '../../tactical/statuses/EffectManager';
import { DIInjectableCollectibles, DIInjectableSerializables } from './injections';
import { DIGlobalContainerPreset } from './types';

export const DIPresetDefault: DIGlobalContainerPreset = {
	[DIInjectableSerializables.mapcell]: MapCellManager,
	[DIInjectableSerializables.unit]: UnitManager,
	[DIInjectableSerializables.prototype]: PrototypeManager,
	[DIInjectableSerializables.player]: PlayerManager,
	[DIInjectableSerializables.map]: CombatMapManager,
	[DIInjectableSerializables.salvage]: SalvageManager,
	[DIInjectableSerializables.base]: PlayerBaseManager,
	[DIInjectableCollectibles.ability]: AbilityManager,
	[DIInjectableCollectibles.effect]: EffectManager,
	[DIInjectableCollectibles.aura]: AuraManager,
	[DIInjectableCollectibles.chassis]: ChassisManager,
	[DIInjectableCollectibles.equipment]: EquipmentManager,
	[DIInjectableCollectibles.weapon]: WeaponManager,
	[DIInjectableCollectibles.shield]: ShieldManager,
	[DIInjectableCollectibles.armor]: ArmorManager,
	[DIInjectableCollectibles.factory]: FactoryManager,
	[DIInjectableCollectibles.facility]: FacilityManager,
	[DIInjectableCollectibles.establishment]: EstablishmentManager,
};

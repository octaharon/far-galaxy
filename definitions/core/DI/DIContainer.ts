import _ from 'underscore';
import { ICollectibleFactory } from '../Collectible';
import AbstractStorage from '../Storage';
import { EntityStorage } from './EntityStorage';
import { DIInjectableCollectibles, DIInjectableSerializables } from './injections';
import { DIContainerKey, DIGlobalContainer, DIGlobalContainerPreset, DIProvider } from './types';

export default class DIContainer {
	public static i() {
		if (!DIContainer.instance) {
			DIContainer.instance = new DIContainer();
		}
		return DIContainer.instance;
	}

	public static inject(preset: DIGlobalContainerPreset) {
		_.mapObject(preset, <T extends DIContainerKey>(factory: ClassConstructor<DIProvider<T>>, type: T) => {
			// @ts-ignore
			const f = (DIContainer.container[type] = new factory());
			f.injectDependencies(DIContainer.container);
			if ((Object.values(DIInjectableCollectibles) as string[]).includes(type)) {
				(f as ICollectibleFactory<any, any, any, any>).fill();
			}
		});
		return this;
	}

	public static getContainer(): DIGlobalContainer {
		if (!DIContainer.instance) return DIContainer.container;
	}

	public static getProvider<T extends keyof DIGlobalContainer>(type: T): DIGlobalContainer[T] {
		if (this.container) return this.container[type];
		throw new Error(`Provider for ${type} is not loaded`);
		return null;
	}

	public static getLocalStorage(StorageLoader?: () => Storage) {
		if (!DIContainer.#localStorage) DIContainer.#localStorage = new AbstractStorage(StorageLoader);
		return DIContainer.#localStorage;
	}

	public static getEntityStorage(StorageLoader?: () => Storage) {
		if (!DIContainer.#entityStorage) DIContainer.#entityStorage = new EntityStorage(StorageLoader);
		return DIContainer.#entityStorage;
	}

	public static injectLocalStorage(s: () => Storage) {
		return (DIContainer.#localStorage = new AbstractStorage(s));
	}

	public static injectEntityStorage(s: () => Storage) {
		return (DIContainer.#entityStorage = new EntityStorage(s));
	}

	static #localStorage: AbstractStorage = null;
	static #entityStorage: EntityStorage = null;
	protected static instance: DIContainer = null;
	protected static container = (Object.values(DIInjectableCollectibles) as string[])
		.concat(Object.values(DIInjectableSerializables) as string[])
		.reduce(
			(o, k) =>
				Object.assign(o, {
					[k]: null,
				}),
			{},
		) as DIGlobalContainer;

	private constructor() {}
}

import AbstractStorage from '../Storage';
import { DIContainerKey, DIContainerValue } from './types';

/**
 * Supposed as an adapter to both LocalStorage and server API, designated specifically for Serializables
 *
 * @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/3
 */
type TCollection<T extends DIContainerKey> = Record<string, DIContainerValue<T>>;
type TStorage = { [key in DIContainerKey]?: TCollection<key> };

export class EntityStorage extends AbstractStorage {
	#entityStorage: TStorage = {};

	public saveEntity<T extends DIContainerKey>(type: T, entity: DIContainerValue<T>) {
		const id = entity?.getInstanceId();
		if (!id) return false;
		const storage: TCollection<T> = this.#entityStorage?.[type] ?? {};
		storage[id] = entity;
		this.#entityStorage = Object.assign(this.#entityStorage ?? {}, {
			[type]: storage,
		});
		this.setItem(id, JSON.stringify(entity.serialize()));
		return true;
	}

	public deleteEntity<T extends DIContainerKey>(type: T, entity: DIContainerValue<T>) {
		const id = entity?.getInstanceId();
		if (!id || !this.#entityStorage?.[type]?.[id]) return false;
		delete this.#entityStorage[type][id];
		this.removeItem(id);
		return true;
	}

	public getEntity<T extends DIContainerKey>(type: T, id: string) {
		if (!id || !this.#entityStorage?.[type]?.[id]) return null;
		return this.#entityStorage?.[type]?.[id] as DIContainerValue<T>;
	}
}

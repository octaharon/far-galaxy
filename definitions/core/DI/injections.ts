export enum DIInjectableSerializables {
	unit = 'di_unit',
	player = 'di_player',
	mapcell = 'di_mapcell',
	prototype = 'di_prototype',
	map = 'di_map',
	salvage = 'di_salvage',
	base = 'di_base',
	action = 'di_action',
}

export enum DIInjectableCollectibles {
	effect = 'di_effect',
	ability = 'di_ability',
	aura = 'di_aura',
	armor = 'di_armor',
	weapon = 'di_weapon',
	equipment = 'di_equipment',
	chassis = 'di_chassis',
	shield = 'di_shield',
	factory = 'di_factory',
	establishment = 'di_establishment',
	facility = 'di_facility',
}

export const DIEntityDescriptors: Record<DIInjectableCollectibles | DIInjectableSerializables, string> = {
	[DIInjectableCollectibles.aura]: '#AU',
	[DIInjectableCollectibles.effect]: '#EF',
	[DIInjectableCollectibles.ability]: '#AB',
	[DIInjectableCollectibles.armor]: '#AR',
	[DIInjectableCollectibles.weapon]: '#WE',
	[DIInjectableCollectibles.equipment]: '#EQ',
	[DIInjectableCollectibles.chassis]: '#CH',
	[DIInjectableCollectibles.shield]: '#SH',
	[DIInjectableCollectibles.facility]: '#FC',
	[DIInjectableCollectibles.establishment]: '#ES',
	[DIInjectableCollectibles.factory]: '#FA',
	[DIInjectableSerializables.unit]: 'UN',
	[DIInjectableSerializables.player]: 'PL',
	[DIInjectableSerializables.map]: 'MP',
	[DIInjectableSerializables.mapcell]: 'MC',
	[DIInjectableSerializables.salvage]: 'SV',
	[DIInjectableSerializables.base]: 'BA',
	[DIInjectableSerializables.prototype]: 'PR',
	[DIInjectableSerializables.action]: 'AC',
};

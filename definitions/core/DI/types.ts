import { IArmor, IArmorFactory } from '../../technologies/types/armor';
import { IShield, IShieldFactory } from '../../technologies/types/shield';
import UnitActions from '../../tactical/units/actions/types';
import { CollectibleFactory } from '../Collectible';
import { SerializableFactory } from '../Serializable';
import { IPlayerBase, IPlayerBaseFactory } from '../../orbital/base/types';
import { IEstablishment, IEstablishmentProvider } from '../../orbital/establishments/types';
import { IFacility, IFacilityProvider } from '../../orbital/facilities/types';
import { IFactory, IFactoryProvider } from '../../orbital/factories/types';
import Players from '../../player/types';
import Abilities from '../../tactical/abilities/types';
import { IChassis, IChassisFactory } from '../../technologies/types/chassis';
import { IEquipment, IEquipmentFactory } from '../../technologies/types/equipment';
import { IPrototype, IPrototypeFactory } from '../../prototypes/types';
import Units from '../../tactical/units/types';
import { IWeapon, IWeaponFactory } from '../../technologies/types/weapons';
import { ICombatMap, ICombatMapFactory, IMapCell, IMapCellFactory } from '../../tactical/map';
import Salvage from '../../tactical/salvage/types';
import { IAura, IAuraFactory } from '../../tactical/statuses/Auras/types';
import { IStatusEffect, IStatusEffectFactory } from '../../tactical/statuses/types';
import { DIInjectableCollectibles, DIInjectableSerializables } from './injections';
import IUnitAction = UnitActions.IUnitAction;

export interface IDISerializables {
	[DIInjectableSerializables.mapcell]: IMapCell;
	[DIInjectableSerializables.unit]: Units.IUnit;
	[DIInjectableSerializables.player]: Players.IPlayer;
	[DIInjectableSerializables.prototype]: IPrototype;
	[DIInjectableSerializables.map]: ICombatMap;
	[DIInjectableSerializables.salvage]: Salvage.ISalvage;
	[DIInjectableSerializables.base]: IPlayerBase;
	[DIInjectableSerializables.action]: IUnitAction<any>;
}

export interface IDICollectibles {
	[DIInjectableCollectibles.ability]: Abilities.TAbilityOption;
	[DIInjectableCollectibles.armor]: IArmor;
	[DIInjectableCollectibles.shield]: IShield;
	[DIInjectableCollectibles.weapon]: IWeapon;
	[DIInjectableCollectibles.equipment]: IEquipment;
	[DIInjectableCollectibles.chassis]: IChassis;
	[DIInjectableCollectibles.effect]: IStatusEffect<any>;
	[DIInjectableCollectibles.aura]: IAura;
	[DIInjectableCollectibles.facility]: IFacility;
	[DIInjectableCollectibles.establishment]: IEstablishment;
	[DIInjectableCollectibles.factory]: IFactory;
	[DIInjectableCollectibles.establishment]: IEstablishment;
	[DIInjectableCollectibles.facility]: IFacility;
}

export type DISerializableType<T extends DIInjectableSerializables> = IDISerializables[T];
export type DICollectibleType<T extends DIInjectableCollectibles> = IDICollectibles[T];
export type DIContainerKey = DIInjectableSerializables | DIInjectableCollectibles;
export type DIContainerValue<K extends DIContainerKey> = K extends DIInjectableSerializables
	? IDISerializables[K]
	: K extends DIInjectableCollectibles
	? IDICollectibles[K]
	: never;
export type DIInjectable = DISerializableType<any> | DICollectibleType<any>;
export type DIMappedConstant<T extends DIInjectable> = T extends DISerializableType<infer X>
	? X
	: T extends DICollectibleType<infer Y>
	? Y
	: never;

export type DIProvider<T extends DIContainerKey> = T extends DIInjectableSerializables.unit
	? Units.IUnitFactory
	: T extends DIInjectableSerializables.prototype
	? IPrototypeFactory
	: T extends DIInjectableSerializables.mapcell
	? IMapCellFactory
	: T extends DIInjectableSerializables.player
	? Players.IPlayerFactory
	: T extends DIInjectableSerializables.map
	? ICombatMapFactory
	: T extends DIInjectableSerializables.salvage
	? Salvage.ISalvageFactory
	: T extends DIInjectableSerializables.base
	? IPlayerBaseFactory
	: T extends DIInjectableCollectibles.effect
	? IStatusEffectFactory
	: T extends DIInjectableCollectibles.shield
	? IShieldFactory
	: T extends DIInjectableCollectibles.armor
	? IArmorFactory
	: T extends DIInjectableCollectibles.chassis
	? IChassisFactory
	: T extends DIInjectableCollectibles.weapon
	? IWeaponFactory
	: T extends DIInjectableCollectibles.equipment
	? IEquipmentFactory
	: T extends DIInjectableCollectibles.aura
	? IAuraFactory
	: T extends DIInjectableCollectibles.ability
	? Abilities.IAbilityFactory
	: T extends DIInjectableCollectibles.factory
	? IFactoryProvider
	: T extends DIInjectableCollectibles.establishment
	? IEstablishmentProvider
	: T extends DIInjectableCollectibles.facility
	? IFacilityProvider
	: T extends DIInjectableSerializables
	? DISerializableProvider<T>
	: T extends DIInjectableCollectibles
	? DICollectibleProvider<T>
	: never;

export type DICollectibleProvider<T extends DIInjectableCollectibles> = CollectibleFactory<
	any,
	any,
	DICollectibleType<T>,
	DICollectibleType<T>['static']
>;
export type DISerializableProvider<T extends DIInjectableSerializables> = SerializableFactory<
	any,
	any,
	DISerializableType<T>
>;

export type DICollectibleContainer = {
	[K in DIInjectableCollectibles]: DIProvider<K>;
};

export type DISerializableContainer = {
	[K in DIInjectableSerializables]: DIProvider<K>;
};

export type DIGlobalContainer = DICollectibleContainer & DISerializableContainer;

export type DIGlobalContainerPreset = {
	[K in keyof DIGlobalContainer]?: ClassConstructor<DIProvider<K>>;
};

export interface IDIContainerDependency {
	injectDependencies(container: DIGlobalContainer): this;
	DIContainer: DIGlobalContainer;
}

export abstract class DIContainerDependency implements IDIContainerDependency {
	protected __DIContainer: DIGlobalContainer;

	public injectDependencies(container: DIGlobalContainer) {
		this.__DIContainer = container;
		return this;
	}

	public get DIContainer() {
		return this.__DIContainer ?? null;
	}

	protected getProvider<T extends keyof DIGlobalContainer>(type: T): DIGlobalContainer[T] {
		if (this.DIContainer) {
			const c = this.DIContainer[type];
			c.injectDependencies(this.DIContainer);
			return c;
		}
		return null;
	}
}

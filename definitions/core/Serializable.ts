import digest from '../../src/utils/digest';

import { DIContainerDependency, DIGlobalContainer } from './DI/types';
import InterfaceTypes from './InterfaceTypes';

export const SERIALIZABLE_ENTITY_PREFIX = '#S-';

export abstract class Serializable<PropType extends {}, SerializedType extends IWithInstanceID>
	extends DIContainerDependency
	implements ISerializable<PropType, SerializedType>
{
	protected __instanceId: string;
	#hasId = false;

	public constructor(p: Partial<PropType> = null, ...args: any[]) {
		super();
		this.setProps(p);
	}

	public getInstanceId() {
		if (!this.__instanceId) {
			this.__instanceId = this.generateInstanceId(true);
			this.#hasId = true;
		}
		return this.__instanceId;
	}

	public getDescriptor(typeDescriptor: string = this.typeDescriptor) {
		return SERIALIZABLE_ENTITY_PREFIX + typeDescriptor;
	}

	// Should be implemented as a getter to prevent early value binding
	protected abstract readonly typeDescriptor: string;

	public generateInstanceId(cold = false) {
		return [
			this.getDescriptor() || this.constructor.name,
			digest(cold ? this.typeDescriptor : this.serialize(), true),
		].join('-');
	}

	public setInstanceId(id: string) {
		return (this.__instanceId = id);
	}

	public setProps(p: Partial<PropType>) {
		if (p) Object.assign(this, p);
		if (!this.#hasId) this.__instanceId = this.generateInstanceId(true);
		return this;
	}

	public abstract serialize(): SerializedType;

	protected __serializeSerializable(): IWithInstanceID {
		return {
			instanceId: this.getInstanceId(),
		};
	}
}

export interface ISerializable<PropType extends {}, SerializedType extends IWithInstanceID>
	extends DIContainerDependency {
	getInstanceId(): string;
	setInstanceId(id: string): string;
	generateInstanceId(): string;
	serialize(): SerializedType;
	setProps(p: Partial<PropType>): ISerializable<PropType, SerializedType>;
	getDescriptor(): string;
}

export type TSerializable<ObjectType, StaticType extends {} = {}> = StaticType & ClassConstructor<ObjectType>;

export default Serializable;

export interface ISerializableFactory<
	PropType extends {},
	SerializedType extends IWithInstanceID,
	ObjectType extends ISerializable<PropType, SerializedType>,
> extends DIContainerDependency {
	unserialize(snapshot: SerializedType, ...args: any[]): ObjectType;

	instantiate(props: Partial<PropType>, ...args: any[]): ObjectType;

	destroy(instanceId: string): ISerializableFactory<PropType, SerializedType, ObjectType>;

	store(
		o: ObjectType,
		cacheFunction?: (a: ObjectType) => void,
	): ISerializableFactory<PropType, SerializedType, ObjectType>;

	flush(): ISerializableFactory<PropType, SerializedType, ObjectType>;

	clone(o: ObjectType, ...args: any[]): ObjectType;

	find<T extends ObjectType = ObjectType>(instanceId: string): T;
}

export abstract class SerializableFactory<
		PropType extends {},
		SerializedType extends IWithInstanceID,
		ObjectType extends ISerializable<PropType, SerializedType>,
	>
	extends DIContainerDependency
	implements ISerializableFactory<PropType, SerializedType, ObjectType>
{
	protected __objectCache: InterfaceTypes.TDictionary<ObjectType> = {};

	public constructor() {
		super();
	}

	public abstract unserialize(snapshot: SerializedType, ...args: any[]): ObjectType;

	public abstract instantiate(props: PropType, ...args: any[]): ObjectType;

	public destroy(instanceId: string) {
		this.__objectCache[instanceId] = null;
		delete this.__objectCache[instanceId];
		return this;
	}

	public store(o: ObjectType, callback?: (a: ObjectType) => void) {
		this.__objectCache[o.getInstanceId()] = o;
		if (callback) callback(o);
		return this;
	}

	public getProvider<T extends keyof DIGlobalContainer>(type: T): DIGlobalContainer[T] {
		return super.getProvider(type);
	}

	public flush() {
		Object.keys(this.__objectCache).map(this.destroy);
		return this;
	}

	public clone(o: ObjectType, ...args: any[]): ObjectType {
		const c = o.serialize();
		c.instanceId = o.generateInstanceId();
		return this.unserialize(c, ...args);
	}

	public find<T extends ObjectType = ObjectType>(instanceId: string): T {
		const c = this.__objectCache[instanceId] as T;
		if (!c) {
			console.trace(`Can't find #${instanceId} at ${this.constructor.name}`);
			return null;
		}
		return c;
	}

	protected inject(t: ObjectType): ObjectType {
		t.injectDependencies(this.__DIContainer);
		return t;
	}

	protected __instantiateSerializable(
		props: Partial<PropType>,
		constructor: ClassConstructor<ObjectType>,
		...args: any[]
	) {
		if (constructor) {
			const c = new constructor(props, ...args);
			c.setProps(props);
			return this.inject(c);
		}
		return null;
	}
}

import _ from 'underscore';
import BasicMaths from '../maths/BasicMaths';
import UnitAttack from '../tactical/damage/attack';
import Damage from '../tactical/damage/damage';
import UnitDefense from '../tactical/damage/defense';

export const isEmptyModifierProperty = (k: any) => {
	return isNaN(k) || k === null;
};

export const isEmptyModifier = (d: IModifier) => {
	return (
		!d ||
		!Object.keys(d).length ||
		((isEmptyModifierProperty(d.bias) || d.bias === 0) &&
			(isEmptyModifierProperty(d.factor) || d.factor === 1) &&
			(isEmptyModifierProperty(d.min) || d.min === -Infinity) &&
			(isEmptyModifierProperty(d.max) || d.max === Infinity))
	);
};

export const isValidDodgeValue = (n: number) => Number.isFinite(n);

// return true if the protection is virtually useless
export const isEmptyProtection = (d: UnitDefense.IDamageProtectionDecorator) => {
	return !d || (isEmptyModifier(d) && (isEmptyModifierProperty(d.threshold) || d.threshold === 0));
};

// return true if the protection actually only exists to prevent negative damage: {min: 0}
export const isDefaultProtection = (d: UnitDefense.IDamageProtectionDecorator) =>
	d?.min === 0 && Object.keys(d).length === 1;

export const isNotValidModifierValue = (value: any, key?: keyof IModifier) => {
	return (
		isEmptyModifierProperty(value) ||
		value === undefined ||
		(key === 'factor' && value === 1) ||
		(key === 'bias' && value === 0)
	);
};

export function isValidModifier<K extends IModifier>(k: K): boolean {
	return (
		!!k &&
		['bias', 'factor', 'min', 'max', 'threshold', 'minGuard', 'maxGuard'].reduce(
			(acc, modKey: keyof IModifier) => !isNotValidModifierValue(k?.[modKey], modKey) || acc,
			false,
		)
	);
}

export function isEmptyProtectionContainer<T>(d: Damage.TDamageGenericWithDefault<T>): boolean {
	return !d || (isEmptyProtection(d?.default) && Damage.TDamageTypesArray.every((dt) => isEmptyProtection(d[dt])));
}

export function isEmptyDodgeObject(d: UnitDefense.TDodge): boolean {
	return (
		!d || (!isValidDodgeValue(d.default) && UnitAttack.TDeliveryTypeArray.every((dt) => !isValidDodgeValue(d[dt])))
	);
}

export function trimNumbers<T extends object>(g: T) {
	return _.mapObject(g, (v) => (Number.isFinite(v) ? BasicMaths.roundToPrecision(v) : v)) as T;
}

export function isEmptyObject<T extends object>(g: T) {
	return !!g || (_.isEmpty(g) && _.isObject(g));
}

import ServerStorage from 'node-storage-shim';

const defaultStorageGetter: () => Storage =
	typeof window !== 'undefined' ? () => window.localStorage : () => new ServerStorage();

export default class AbstractStorage<T extends string = string> implements Storage {
	protected readonly storage: Storage;

	public constructor(getStorage: () => Storage = defaultStorageGetter) {
		this.storage = getStorage();
	}

	public get length() {
		return this.storage.length;
	}

	public getItem(key: T): string | null {
		return this.storage.getItem(key);
	}

	public setItem(key: T, value: string): void {
		this.storage.setItem(key, value);
	}

	public removeItem(key: T): void {
		this.storage.removeItem(key);
	}

	public clearItems(keys: T[]): void {
		keys.forEach((key) => this.removeItem(key));
	}

	public clear(): void {
		this.storage.clear();
	}

	public key(index: number): string | null {
		return this.storage.key(index);
	}
}

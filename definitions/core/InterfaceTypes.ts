export namespace InterfaceTypes {
	export type TDictionary<T = any> = {
		[key: string]: T;
	};

	export type TIdMap<T = any> = Record<number, T>;

	export type TList<T = any> = T[];

	export interface IScaleFactor {
		scaleX: number;
		scaleY: number;
	}

	export interface ITranslateFactor {
		translateX: number;
		translateY: number;
	}

	export interface IRotateFactor {
		rotate: number;
		rotateCX: number;
		rotateCY: number;
	}

	export type ITransformFactor = Partial<IScaleFactor> & Partial<ITranslateFactor> & Partial<IRotateFactor>;

	export interface IComplexNumberAlgebraic {
		real: number;
		imag: number;
	}

	export interface IComplexNumberEuler {
		phi: number;
		r: number;
	}

	export type IComplexNumber = IComplexNumberAlgebraic | IComplexNumberEuler;

	export interface IVector {
		x: number;
		y: number;
	}

	export interface ISpot extends IVector {
		radius: number;
	}

	export interface ILine {
		// y = kx + b
		k: number;
		b: number;
	}

	export interface IParametricLine {
		// ax+by+c=0
		a: number;
		b: number;
		c: number;
	}

	export interface IPolar {
		r: number;
		alpha: number;
	}

	export type TCircle<T extends IVector | IPolar> = {
		center: T;
		radius: number;
	};

	export interface IAngularArc {
		start: number; // radians
		end: number; // radians
	}

	export type TArc<T extends IVector | IPolar> = TCircle<T> & IAngularArc;

	export type TPolarShape<T extends IVector | IPolar> = {
		radiusArray: number[];
		center: T;
	};

	export type TPolygon = IVector[];
	export type TQuadrigon = [IVector, IVector, IVector, IVector];

	export interface IValueWithBounds extends IBounds {
		x: number;
	}

	export type FnMapSignature<T, K> = (arg: T) => K;
	export type FnTranslateSignature<T> = FnMapSignature<T, T>;

	export type FnTransformFunctionSignature = (f: FnTranslateSignature<number>, transform: ITransformFactor) => FnTranslateSignature<number>;

	export type FnNumericTranslate = FnTranslateSignature<number>;

	export type FnValueWithBoundsMap = FnMapSignature<IValueWithBounds, number>;

	export interface IDurationDecorator {
		duration?: number;
	}
}
export default InterfaceTypes;

import { TPlayerId } from '../../player/playerId';
import { TPlanetProps } from '../Planets';

export type TStarSystemSharedProps = {
	warpFactor: number;
	mass?: number;
	age?: number;
	starlightColorHex?: number; // hex
	radiationLevel: number; // c=0, reduces scan range and slowly damages units with Radiation
};

export type TStarSystemBaseProps = {
	name?: string;
	escapeVelocity: number;
	radius: number;
	starTemperature: number; //0-1
	planetCount?: number;
};

export type TStarSystemProps = TStarSystemSharedProps &
	TStarSystemBaseProps & {
		planets?: Array<
			TPlanetProps & {
				regionsControl?: Record<
					string,
					{
						playerId: TPlayerId;
						turnStart: number;
						turnsLeft: number;
					}
				>;
			}
		>;
	};

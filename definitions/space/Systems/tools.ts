import * as faker from 'faker';
import { generateStarColor } from '../../../src/components/Symbols/PlanetImage/colorGenerators';
import { applyDistribution } from '../../maths/Distributions';
import { MAX_STAR_PLANETS, MIN_STAR_PLANETS } from '../Planets/constants';
import { generatePlanet, generateRandomTerrestrialTitle } from '../Planets/generators';
import { TStarSystemProps } from './index';

export const generateStarName = () => {
	return generateRandomTerrestrialTitle(
		[
			faker.lorem
				.words(10)
				.split(' ')
				.filter((v) => v.length >= 4),
			faker.vehicle.model(),
			faker.company.companyName(),
		],
		Math.round(1 + Math.random()),
	);
};

export const generatePlanetsForSystem = (system: TStarSystemProps) => {
	const distances = new Array(system.planetCount || MAX_STAR_PLANETS)
		.fill(null)
		.map(() =>
			applyDistribution({
				distribution: 'Uniform',
			}),
		)
		.sort();
	return distances.map((distance, ordinal) => generatePlanet(distance, ordinal + 1, system));
};

export const generateSystem = (withPlanets = false) => {
	const system = {
		name: generateStarName(),
		radiationLevel: applyDistribution({ distribution: 'MostlyMinimal', min: 0, max: 0.5 }),
		radius: applyDistribution({ distribution: 'MostlyMaximal' }),
		starTemperature: applyDistribution({ distribution: 'Bell' }),
		warpFactor: applyDistribution({ distribution: 'MostlyMinimal' }),
		age: applyDistribution({ distribution: 'MostlyMaximal' }),
		mass: applyDistribution({ distribution: 'Uniform' }),
		planetCount: Math.round(
			applyDistribution({ distribution: 'Bell', min: MIN_STAR_PLANETS, max: MAX_STAR_PLANETS }),
		),
		escapeVelocity: applyDistribution({ distribution: 'Uniform' }),
	} as TStarSystemProps;
	//console.log(system);
	system.starlightColorHex = generateStarColor(system);
	if (withPlanets) system.planets = generatePlanetsForSystem(system);
	return system;
};

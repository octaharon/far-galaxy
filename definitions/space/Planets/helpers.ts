import { TWithGraphicSettingsProps } from '../../../src/contexts/GraphicSettings';
import { numericHash } from '../../../src/utils/digest';
import { TMapTerrainType, TPlanetColorKeys, TPlanetProps, TPlanetType } from './index';

export function getTerrainTypeFromProps(p: Partial<TPlanetProps>): TMapTerrainType {
	const freqs = getPlanetDeterminedRadix(p, 2, 10);
	switch (true) {
		case p.type === TPlanetType.asteroid:
			return freqs[4] ? TMapTerrainType.gravel : TMapTerrainType.lowlands;
		case p.type === TPlanetType.ocean:
			return freqs[3] ? TMapTerrainType.ice : TMapTerrainType.snow;
		case [TPlanetType.ice, TPlanetType.frozen].includes(p.type):
			return freqs[7] ? TMapTerrainType.frozen : freqs[1] ? TMapTerrainType.ice : TMapTerrainType.snow;
		case [TPlanetType.chtonian].includes(p.type):
			return freqs[3] ? TMapTerrainType.gravel : TMapTerrainType.dirt;
		case [TPlanetType.rogue].includes(p.type):
			return freqs[3]
				? TMapTerrainType.lowlands
				: freqs[1]
				? TMapTerrainType.landscape
				: TMapTerrainType.mountains;
		case [TPlanetType.proto].includes(p.type):
			return freqs[4] ? TMapTerrainType.desert : freqs[7] ? TMapTerrainType.rock : TMapTerrainType.earthy;
		case [TPlanetType.volcanic].includes(p.type):
			return freqs[3] ? TMapTerrainType.lava : freqs[6] ? TMapTerrainType.volcano : TMapTerrainType.hot;
		case [TPlanetType.hycean].includes(p.type):
			return freqs[5] ? TMapTerrainType.jungle : freqs[2] ? TMapTerrainType.hot : TMapTerrainType.forest;
		case [TPlanetType.terra].includes(p.type):
			return freqs[5] ? TMapTerrainType.landscape : freqs[2] ? TMapTerrainType.gas : TMapTerrainType.mountains;
		case [TPlanetType.pulsar].includes(p.type):
			return TMapTerrainType.peacock;
		case [TPlanetType.carbon].includes(p.type):
			return freqs[0] ? TMapTerrainType.dirt : TMapTerrainType.lowlands;
		case [TPlanetType.tomb].includes(p.type):
			return freqs[3] ? TMapTerrainType.lowlands : TMapTerrainType.sulphur;
		case [TPlanetType.arid, TPlanetType.hollow].includes(p.type):
			return freqs[2] ? TMapTerrainType.sand : freqs[5] ? TMapTerrainType.desert : TMapTerrainType.snow;
		case [TPlanetType.condensed, TPlanetType.giant].includes(p.type):
			return freqs[3] ? TMapTerrainType.gas : TMapTerrainType.goo;
		case p.temperature > 0.85:
			return TMapTerrainType.desert;
		case p.atmosphericDensity < 0.3:
			switch (true) {
				case p.waterPresence > 0.5 && p.temperature <= 0.25:
					return TMapTerrainType.ice;
				case p.temperature >= 0.6 + p.atmosphericDensity:
					return TMapTerrainType.desert;
				default:
					return TMapTerrainType.rock;
			}
		case p.temperature > 0.8:
			switch (true) {
				case p.waterPresence <= 0.2:
					return TMapTerrainType.rock;
				case p.atmosphericDensity <= 0.25 || p.gravity <= 0.25:
					return TMapTerrainType.sand;
				case p.waterPresence >= 0.5:
					return TMapTerrainType.jungle;
				case p.radiationLevel >= 0.1:
					return TMapTerrainType.desert;
				default:
					return TMapTerrainType.gravel;
			}
		case p.temperature >= 0.5:
			switch (true) {
				case p.atmosphericDensity < 0.4:
					return p.waterPresence >= 0.5 ? TMapTerrainType.dirt : TMapTerrainType.gravel;
				case p.waterPresence > 0.1 && p.atmosphericDensity >= 0.25:
					return TMapTerrainType.forest;
				case p.atmosphericDensity >= 0.25:
					return TMapTerrainType.lowlands;
				default:
					return TMapTerrainType.gravel;
			}
		case p.temperature >= 0.25:
			switch (true) {
				case p.waterPresence > 0.5:
					return TMapTerrainType.snow;
				case p.waterPresence > 0.2:
					return TMapTerrainType.dirt;
				case p.gravity > 0.4:
					return TMapTerrainType.gravel;
				default:
					return TMapTerrainType.rock;
			}
		default:
			switch (true) {
				case p.waterPresence >= 0.2:
					return TMapTerrainType.ice;
				case p.gravity > 0.4:
					return TMapTerrainType.gravel;
				default:
					return TMapTerrainType.rock;
			}
	}
}

export const getPlanetHash = (p: { planet: TPlanetProps } & TWithGraphicSettingsProps) =>
	numericHash([...Object.values(p.planet), p?.graphicSettings?.textureSize].join(','));

export function getPlanetShininess(p: TPlanetProps): number {
	switch (true) {
		case p.type === TPlanetType.pulsar:
			return 0;
		case p.type === TPlanetType.carbon:
			return 2 - p.distanceFromStar;
		case p.type === TPlanetType.ice:
		case p.type === TPlanetType.frozen:
		case p.type === TPlanetType.ocean:
		case p.type === TPlanetType.rogue:
		case p.type === TPlanetType.hycean:
			return 1 - p.distanceFromStar / 3;
		case p.type === TPlanetType.giant:
		case p.type === TPlanetType.volcanic:
		case p.type === TPlanetType.tomb:
		case p.type === TPlanetType.terra:
			return 0.1;
		case p.type === TPlanetType.chtonian:
		case p.type === TPlanetType.arid:
		case p.type === TPlanetType.hollow:
		case p.type === TPlanetType.asteroid:
			return 1 - p.distanceFromStar;
		default:
			return (1 - 0.3 * p.distanceFromStar - 0.4 * p.atmosphericDensity) ** 0.5;
	}
}

export function getTerrainEmissiveIntensityAmplifier(p: TPlanetProps): number {
	switch (p.type) {
		case TPlanetType.tomb:
			return 3;
		case TPlanetType.pulsar:
			return 0.4 + p.radiationLevel * 0.6;
		case TPlanetType.volcanic:
		case TPlanetType.terra:
			return 0.1 + p.radiationLevel * 0.2 + p.temperature * 0.3;
		case TPlanetType.ocean:
		case TPlanetType.rogue:
		case TPlanetType.chtonian:
			return 1 + p.radiationLevel * 0.2 + p.temperature * 0.3;
		case TPlanetType.hycean:
		case TPlanetType.arid:
		case TPlanetType.proto:
			return p.radiationLevel * 0.4 + p.temperature * 0.3;
		case TPlanetType.giant:
			return ((p.temperature * 0.1 + p.radiationLevel + 0.3 + p.magneticField * 0.1) * 0.15) ** 0.5;
		case TPlanetType.ice:
		case TPlanetType.frozen:
		case TPlanetType.condensed:
			return 1.4 * p.radiationLevel;
		case TPlanetType.asteroid:
			return 0.1;
		case TPlanetType.hollow:
			return 0.1 + 0.15 * p.radiationLevel;
		default:
			return 0;
	}
}

/**
 * Returns a set of <base> pseudo-random (while in fact deterministic) values from 0 to <radix>-1, base on <planet> properties.
 *
 * @param planet
 * @param radix
 * @param base
 *
 * @return Array numbers from 0 to <radix>-1
 */
export function getPlanetDeterminedRadix(planet: Partial<TPlanetProps>, radix = 4, base = 12): number[] {
	return new Array(base)
		.fill(null)
		.map((d, ix) => {
			return (Math.sin((((ix + 3) * Math.PI) / (Math.ceil(base / 2) + 1)) * 2) + 1) * 0.5;
		})
		.map((wavelet, ix, arr) => {
			return (
				Math.round(
					(planet?.magneticField ?? 0) * 83 * 97 * arr[(ix - 2 + base) % base] +
						(planet?.gravity ?? 1) * 108 * 119 * arr[(ix - 1 + base) % base] +
						(planet?.age ?? 2) * wavelet * 125 * 137 +
						(planet?.mass ?? 3) * arr[(ix + 1 + base) % base] * 141 * 169 +
						(planet?.distanceFromStar ?? 4) * arr[(ix + 2 + base) % base] * 197 * 207,
				) % radix
			);
		});
}

export const colorIntToHex = (v: number) => `#${v.toString(16).padStart(6, '0')}`;
export const colorHexToInt = (v: string) => parseInt(v?.replace(/[^a-f0-9]+/is, ''), 16);

export function getPlanetWithHexColors(planet: Partial<TPlanetProps>) {
	return {
		...planet,
		starlightColorHex: Number.isFinite(planet?.starlightColorHex)
			? colorIntToHex(planet.starlightColorHex)
			: String(planet?.starlightColorHex),
		atmosphericColorHex: Number.isFinite(planet?.atmosphericColorHex)
			? colorIntToHex(planet.atmosphericColorHex)
			: String(planet?.atmosphericColorHex),
		surfaceColorHex: Number.isFinite(planet?.surfaceColorHex)
			? colorIntToHex(planet.surfaceColorHex)
			: String(planet?.surfaceColorHex),
	};
}

export function getPlanetWithIntColors(planet: Partial<ReturnType<typeof getPlanetWithHexColors>>) {
	return {
		...planet,
		starlightColorHex: Number.isFinite(planet?.starlightColorHex)
			? Number(planet?.starlightColorHex)
			: colorHexToInt(planet?.starlightColorHex),
		atmosphericColorHex: Number.isFinite(planet?.atmosphericColorHex)
			? Number(planet.atmosphericColorHex)
			: colorHexToInt(planet?.atmosphericColorHex),
		surfaceColorHex: Number.isFinite(planet?.surfaceColorHex)
			? Number(planet.surfaceColorHex)
			: colorHexToInt(planet?.surfaceColorHex),
	};
}

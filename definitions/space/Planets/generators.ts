import faker from 'faker';
import _ from 'underscore';
import { ucWords } from '../../../src/utils/capitalize';
import { generatePlanetColors } from '../../../src/components/Symbols/PlanetImage/colorGenerators';
import { getLettersWithoutRomans } from '../../../src/utils/getRandomLetter';
import romanize from '../../../src/utils/romanize';
import { weightedSample } from '../../../src/utils/weightedRandom';
import BasicMaths from '../../maths/BasicMaths';
import { applyDistribution } from '../../maths/Distributions';
import { applyResourceModifier, resourceTemplate } from '../../world/resources/helpers';
import { TStarSystemProps } from '../Systems';
import { DefaultPlanetProps, MAX_PLANET_ZONES, PlanetInnateProperties, PlanetResourceBonuses } from './constants';
import { PlanetTypeCaptions, TPlanetProps, TPlanetType } from './index';

const stopWords = ['mr', 'mrs', 'dr', 'and', 'pt', 'pvt', 'lt', 'llc', 'gmbh'];
export const generateRandomTerrestrialTitle = (
	words: Array<string | string[]>,
	wordCount: number = words?.length || 0,
) =>
	ucWords(
		_.sample(
			words
				.flatMap((s) => (typeof s === 'string' ? s.split(' ') : s))
				.map((v) => v.replace(/[^\w]+/g, '').toLowerCase())
				.filter((v) => v.length >= 2 && !stopWords.includes(v)),
			wordCount,
		).join(' '),
	);
export const getRandomPlanetType = () => {
	return _.sample(Object.values(TPlanetType));
};
export const generatePlanetName = (type?: TPlanetType, starName?: string, ordinalNumber?: number) => {
	const random = Math.random();
	const nameLength = Math.ceil(Math.random() * 2);
	const starPrefix = starName ? starName.split(' ').slice(0, 2).join(' ') : '';
	switch (true) {
		case type === TPlanetType.tomb:
			return generateRandomTerrestrialTitle(
				[faker.internet.domainWord(), faker.name.firstName(), faker.name.lastName()],
				3,
			);
		case type === TPlanetType.rogue || random > 0.85:
			return `${PlanetTypeCaptions[type]} ${faker.vehicle.vrm()}`;
		case !!starPrefix && random > 0.4 && ordinalNumber >= 1:
			return `${starPrefix} ${romanize(ordinalNumber)}`;
		case !!starPrefix && random < 0.15:
			return `${starPrefix} ${_.sample(getLettersWithoutRomans())}`;
		default:
			return generateRandomTerrestrialTitle(
				[
					faker.animal.bird(),
					faker.lorem.words(1),
					faker.name.lastName(),
					faker.address.streetName(),
					faker.vehicle.color(),
					faker.company.companyName(),
				],
				nameLength,
			);
	}
};
export const generatePlanetTypeFromDistance: (
	relativeDistance: number,
	starTemperature?: number,
	warpFactor?: number,
) => TPlanetType = (relativeDistance, starTemperature, warpFactor) => {
	const randomValue = applyDistribution({
		distribution: 'Uniform',
	});
	switch (true) {
		case randomValue >= 0.995:
			return TPlanetType.tomb;
		case randomValue <= 0.1:
			return TPlanetType.asteroid;
		case relativeDistance <= 0.1:
			return weightedSample({
				[TPlanetType.pulsar]: BasicMaths.mapRange({ min: 0.5, max: 1 }, { min: 1, max: 15 }, warpFactor),
				[TPlanetType.hycean]: BasicMaths.mapRange({ min: 0.2, max: 0.5 }, { min: 1, max: 5 }, starTemperature),
				[TPlanetType.chtonian]: BasicMaths.mapRange(
					{ min: 0.25, max: 1 },
					{ min: 1, max: 10 },
					starTemperature,
				),
				[TPlanetType.volcanic]: BasicMaths.mapRange({ min: 0, max: 1 }, { min: 1, max: 4 }, starTemperature),
				[TPlanetType.carbon]: BasicMaths.mapRange({ min: 0, max: 0.25 }, { min: 3, max: 1 }, starTemperature),
				[TPlanetType.frozen]: BasicMaths.mapRange({ min: 0, max: 0.5 }, { min: 2, max: 1 }, starTemperature),
				[TPlanetType.arid]: 2,
			} as Partial<Record<TPlanetType, number>>);
		case relativeDistance <= 0.25:
			return weightedSample({
				[TPlanetType.proto]: BasicMaths.mapRange({ min: 0.2, max: 0.8 }, { min: 1, max: 3 }, starTemperature),
				[TPlanetType.chtonian]: BasicMaths.mapRange({ min: 0.75, max: 1 }, { min: 1, max: 1 }, starTemperature),
				[TPlanetType.volcanic]: BasicMaths.mapRange({ min: 0, max: 1 }, { min: 1, max: 2 }, starTemperature),
				[TPlanetType.terra]: BasicMaths.mapRange({ min: 0.25, max: 0.5 }, { min: 2, max: 2 }, starTemperature),
				[TPlanetType.ocean]: BasicMaths.mapRange({ min: 0.25, max: 0.75 }, { min: 3, max: 3 }, starTemperature),
				[TPlanetType.carbon]: BasicMaths.mapRange({ min: 0, max: 0.5 }, { min: 1, max: 1 }, starTemperature),
				[TPlanetType.arid]: 3,
				[TPlanetType.hycean]: 1,
			} as Partial<Record<TPlanetType, number>>);
		case relativeDistance <= 0.5:
			return weightedSample({
				[TPlanetType.hollow]: 1,
				[TPlanetType.volcanic]: BasicMaths.mapRange({ min: 0.25, max: 1 }, { min: 3, max: 5 }, starTemperature),
				[TPlanetType.terra]: BasicMaths.mapRange({ min: 0.3, max: 0.7 }, { min: 10, max: 10 }, starTemperature),
				[TPlanetType.arid]: BasicMaths.mapRange({ min: 0.2, max: 1 }, { min: 1, max: 15 }, starTemperature),
				[TPlanetType.carbon]: BasicMaths.mapRange({ min: 0, max: 0.25 }, { min: 2, max: 0 }, starTemperature),
				[TPlanetType.frozen]: BasicMaths.mapRange({ min: 0, max: 0.25 }, { min: 2, max: 0 }, starTemperature),
				[TPlanetType.ocean]: BasicMaths.mapRange({ min: 0.2, max: 0.75 }, { min: 1, max: 10 }, starTemperature),
				[TPlanetType.hycean]: BasicMaths.mapRange({ min: 0.25, max: 1 }, { min: 10, max: 1 }, starTemperature),
			} as Partial<Record<TPlanetType, number>>);
		case relativeDistance <= 0.65:
			return weightedSample({
				[TPlanetType.arid]: BasicMaths.mapRange({ min: 0.5, max: 1 }, { min: 1, max: 3 }, starTemperature),
				[TPlanetType.ocean]: BasicMaths.mapRange({ min: 0.25, max: 0.75 }, { min: 1, max: 4 }, starTemperature),
				[TPlanetType.terra]: BasicMaths.mapRange({ min: 0.45, max: 0.65 }, { min: 2, max: 4 }, starTemperature),
				[TPlanetType.condensed]: BasicMaths.mapRange(
					{ min: 0.25, max: 1 },
					{ min: 1, max: 7 },
					starTemperature,
				),
				[TPlanetType.giant]: BasicMaths.mapRange({ min: 0.25, max: 1 }, { min: 1, max: 4 }, starTemperature),
				[TPlanetType.frozen]: BasicMaths.mapRange({ min: 0, max: 0.75 }, { min: 3, max: 1 }, starTemperature),
				[TPlanetType.ice]: BasicMaths.mapRange({ min: 0, max: 0.75 }, { min: 3, max: 0 }, starTemperature),
			} as Partial<Record<TPlanetType, number>>);
		case relativeDistance <= 0.8:
			return weightedSample({
				[TPlanetType.condensed]: 3,
				[TPlanetType.proto]: 1,
				[TPlanetType.giant]: BasicMaths.mapRange({ min: 0, max: 1 }, { min: 1, max: 5 }, starTemperature),
				[TPlanetType.frozen]: BasicMaths.mapRange({ min: 0, max: 1 }, { min: 7, max: 1 }, starTemperature),
				[TPlanetType.ice]: BasicMaths.mapRange({ min: 0, max: 1 }, { min: 5, max: 1 }, starTemperature),
			} as Partial<Record<TPlanetType, number>>);
		case relativeDistance >= 0.9:
			return weightedSample({
				[TPlanetType.condensed]: 3,
				[TPlanetType.hollow]: 1,
				[TPlanetType.rogue]: 1,
				[TPlanetType.frozen]: BasicMaths.mapRange({ min: 0, max: 1 }, { min: 5, max: 2 }, starTemperature),
				[TPlanetType.ice]: BasicMaths.mapRange({ min: 0, max: 1 }, { min: 3, max: 1 }, starTemperature),
			} as Partial<Record<TPlanetType, number>>);
		default:
			return weightedSample({
				[TPlanetType.frozen]: 1,
				[TPlanetType.carbon]: 1,
				[TPlanetType.condensed]: 2,
				[TPlanetType.rogue]: 1,
				[TPlanetType.proto]: 1,
				[TPlanetType.asteroid]: 2,
			} as Partial<Record<TPlanetType, number>>);
	}
};
export const generatePlanetByType = (
	planetType: TPlanetType,
	ordinal: number,
	distance?: number,
	system?: TStarSystemProps,
) => {
	const planetSeeds = {
		...DefaultPlanetProps,
		...(PlanetInnateProperties?.[planetType] || {}),
	};
	const planetSeedableProps = _.mapObject(planetSeeds, (seed) =>
		applyDistribution({
			...seed,
			x: Math.random(),
		}),
	);
	planetSeedableProps.radiationLevel = BasicMaths.clipTo1(
		(planetSeedableProps.radiationLevel ?? 0) + system?.radiationLevel ?? 0,
	);
	const props = {
		type: planetType,
		name: generatePlanetName(planetType, system?.name, ordinal),
		distanceFromStar: distance ?? Math.random(),
		...planetSeedableProps,
		warpFactor: (planetSeedableProps.warpFactor ?? 1) * Math.sqrt(1 - distance),
		starlightColorHex: system?.starlightColorHex ?? 16777215,
		regions: MAX_PLANET_ZONES,
	} as TPlanetProps;
	Object.assign(props, generatePlanetColors(props));
	return props;
};
export const generatePlanet = (distance: number, ordinal: number, system?: TStarSystemProps): TPlanetProps => {
	const planetType = generatePlanetTypeFromDistance(
		distance,
		system?.starTemperature ?? 0.5,
		system?.warpFactor ?? 0.5,
	);
	return generatePlanetByType(planetType, ordinal, distance, system);
};
export const generatePlanetResourceDistribution = (p: TPlanetType) => {
	return applyResourceModifier(resourceTemplate(1), PlanetResourceBonuses[p] || {});
};

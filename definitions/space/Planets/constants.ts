import Distributions from '../../maths/Distributions';
import Resources from '../../world/resources/types';
import { TPlanetSurfaceProps, TPlanetType } from './index';
import ISeedValue = Distributions.ISeedValue;

type TPlanetPropModifier = {
	[K in keyof TPlanetSurfaceProps as TPlanetSurfaceProps[K] extends number ? K : never]?: ISeedValue;
} & { size?: ISeedValue };

export const MAX_STAR_AGE = 1e12;
export const MIN_STAR_MASS = 1e7;
export const MAX_STAR_MASS = 1e14;
export const MAX_STAR_PLANETS = 32;
export const MIN_STAR_PLANETS = 4;
export const MIN_STAR_TEMPERATURE = 5e4;
export const MAX_STAR_TEMPERATURE = 1e7;
export const MIN_PLANET_MASS = 1e5;
export const MAX_PLANET_MASS = 1e10;
export const MIN_PLANET_SIZE = 1e5;
export const MAX_PLANET_SIZE = 1e8;
export const MIN_PLANET_DISTANCE = 1e3;
export const MAX_PLANET_DISTANCE = 1e8;
export const MAX_PLANET_ZONES = 40;

export const DefaultPlanetProps: TPlanetPropModifier = {
	magneticField: { min: 0, max: 1, distribution: 'Bell' },
	atmosphericDensity: { min: 0, max: 1, distribution: 'Gaussian' },
	gravity: { min: 0, max: 1, distribution: 'Gaussian' },
	temperature: { min: 0.25, max: 0.75, distribution: 'Bell' },
	waterPresence: { min: 0.2, max: 0.6, distribution: 'Bell' },
	spinFactor: { min: 0.25, max: 0.75, distribution: 'Bell' },
	radiationLevel: { min: 0, max: 0.1, distribution: 'Minimal' },
	warpFactor: { min: 0, max: 0.1, distribution: 'Uniform' },
	mass: { min: 0, max: 1, distribution: 'Maximal' },
	age: { min: 0, max: 1, distribution: 'MostlyMaximal' },
	size: { min: 0, max: 0.8, distribution: 'Parabolic' },
};

export const PlanetInnateProperties: Partial<Record<TPlanetType, TPlanetPropModifier>> = {
	[TPlanetType.tomb]: {
		waterPresence: { min: 0, max: 0.2, distribution: 'Uniform' },
		magneticField: { min: 0.75, max: 1, distribution: 'Maximal' },
		radiationLevel: { min: 1, max: 1, distribution: 'Uniform' },
		atmosphericDensity: { min: 0, max: 0.5, distribution: 'Uniform' },
		temperature: { min: 0, max: 0.2, distribution: 'Uniform' },
		age: { min: 0.8, max: 1, distribution: 'Uniform' },
	},
	[TPlanetType.giant]: {
		mass: { min: 0.5, max: 1, distribution: 'Bell' },
		size: { min: 0.75, max: 1, distribution: 'Maximal' },
		waterPresence: { min: 1, max: 1, distribution: 'Uniform' },
		atmosphericDensity: { min: 0.5, max: 1, distribution: 'Bell' },
		temperature: { min: 0.25, max: 0.5, distribution: 'Uniform' },
		gravity: { min: 0.8, max: 1, distribution: 'Uniform' },
		spinFactor: { min: 0, max: 0.75, distribution: 'Bell' },
		magneticField: { min: 0, max: 0.5, distribution: 'Minimal' },
	},
	[TPlanetType.condensed]: {
		waterPresence: { min: 0.75, max: 1, distribution: 'Maximal' },
		atmosphericDensity: { min: 0.5, max: 1, distribution: 'Bell' },
		temperature: { min: 0, max: 0.4, distribution: 'Uniform' },
		gravity: { min: 0.5, max: 0.9, distribution: 'Uniform' },
		magneticField: { min: 0, max: 0.5, distribution: 'Uniform' },
	},
	[TPlanetType.hycean]: {
		waterPresence: { min: 0.85, max: 1, distribution: 'Bell' },
		atmosphericDensity: { min: 0.5, max: 1, distribution: 'Uniform' },
		temperature: { min: 0.5, max: 0.75, distribution: 'Uniform' },
		gravity: { min: 0.4, max: 0.85, distribution: 'Uniform' },
		magneticField: { min: 0, max: 0.5, distribution: 'Uniform' },
	},
	[TPlanetType.ocean]: {
		waterPresence: { min: 0.9, max: 1, distribution: 'Bell' },
		atmosphericDensity: { min: 0.25, max: 0.75, distribution: 'Maximal' },
		temperature: { min: 0.2, max: 0.5, distribution: 'Uniform' },
		gravity: { min: 0.4, max: 0.85, distribution: 'Uniform' },
		magneticField: { min: 0, max: 0.25, distribution: 'Uniform' },
		size: { min: 0.25, max: 0.75, distribution: 'Maximal' },
	},
	[TPlanetType.terra]: {
		waterPresence: { min: 0.5, max: 0.8, distribution: 'Bell' },
		atmosphericDensity: { min: 0.25, max: 0.75, distribution: 'Gaussian' },
		temperature: { min: 0.4, max: 0.6, distribution: 'Uniform' },
		gravity: { min: 0.5, max: 0.75, distribution: 'Uniform' },
		magneticField: { min: 0.25, max: 0.5, distribution: 'Uniform' },
		size: { min: 0.35, max: 0.65, distribution: 'Maximal' },
		radiationLevel: { min: 0, max: 0.15, distribution: 'Uniform' },
		warpFactor: { min: 0, max: 0.1, distribution: 'Minimal' },
	},
	[TPlanetType.ice]: {
		mass: { min: 0.5, max: 1, distribution: 'Bell' },
		size: { min: 0.75, max: 1, distribution: 'Maximal' },
		waterPresence: { min: 0, max: 0.25, distribution: 'Minimal' },
		atmosphericDensity: { min: 0, max: 0.5, distribution: 'Uniform' },
		temperature: { min: 0, max: 0.25, distribution: 'Uniform' },
		gravity: { min: 0.5, max: 1, distribution: 'Uniform' },
		spinFactor: { min: 0, max: 0.75, distribution: 'Bell' },
		magneticField: { min: 0.25, max: 0.75, distribution: 'Uniform' },
	},
	[TPlanetType.frozen]: {
		waterPresence: { min: 0, max: 0, distribution: 'Bell' },
		atmosphericDensity: { min: 0, max: 0.5, distribution: 'Uniform' },
		temperature: { min: 0, max: 0.25, distribution: 'Uniform' },
		gravity: { min: 0.5, max: 1, distribution: 'Uniform' },
		spinFactor: { min: 0, max: 0.75, distribution: 'Bell' },
		magneticField: { min: 0.25, max: 0.75, distribution: 'Uniform' },
	},
	[TPlanetType.chtonian]: {
		mass: { min: 0.5, max: 1, distribution: 'Bell' },
		waterPresence: { min: 0, max: 0, distribution: 'Uniform' },
		atmosphericDensity: { min: 0, max: 0, distribution: 'Uniform' },
		temperature: { min: 0.75, max: 1, distribution: 'Maximal' },
		gravity: { min: 0.5, max: 1, distribution: 'Maximal' },
		spinFactor: { min: 0.5, max: 1, distribution: 'Bell' },
		magneticField: { min: 0.7, max: 1, distribution: 'Uniform' },
		radiationLevel: { min: 0.25, max: 1, distribution: 'Maximal' },
		size: { min: 0.15, max: 0.5, distribution: 'Bell' },
	},
	[TPlanetType.arid]: {
		waterPresence: { min: 0, max: 0, distribution: 'Uniform' },
		atmosphericDensity: { min: 0, max: 0.5, distribution: 'Uniform' },
		temperature: { min: 0.75, max: 1, distribution: 'Uniform' },
		gravity: { min: 0.25, max: 0.75, distribution: 'Parabolic' },
	},
	[TPlanetType.volcanic]: {
		size: { min: 0.25, max: 0.5, distribution: 'Bell' },
		waterPresence: { min: 0, max: 0.25, distribution: 'Minimal' },
		atmosphericDensity: { min: 0.5, max: 1, distribution: 'Uniform' },
		temperature: { min: 0.75, max: 1, distribution: 'Maximal' },
		magneticField: { min: 0.5, max: 1, distribution: 'Minimal' },
		radiationLevel: { min: 0, max: 0.5, distribution: 'Bell' },
	},
	[TPlanetType.proto]: {
		waterPresence: { min: 0, max: 0, distribution: 'Bell' },
		atmosphericDensity: { min: 0, max: 0.25, distribution: 'Minimal' },
		temperature: { min: 0, max: 0.25, distribution: 'Uniform' },
		gravity: { min: 0.25, max: 1, distribution: 'Minimal' },
		spinFactor: { min: 0.75, max: 1, distribution: 'Bell' },
		magneticField: { min: 0, max: 1, distribution: 'Minimal' },
		radiationLevel: { min: 0.25, max: 0.5, distribution: 'Uniform' },
		mass: { min: 0, max: 0.5, distribution: 'Uniform' },
		size: { min: 0.1, max: 0.25, distribution: 'Minimal' },
	},
	[TPlanetType.asteroid]: {
		mass: { min: 0, max: 0.5, distribution: 'Minimal' },
		waterPresence: { min: 0, max: 0, distribution: 'Bell' },
		atmosphericDensity: { min: 0, max: 0, distribution: 'Uniform' },
		temperature: { min: 0, max: 0.25, distribution: 'Uniform' },
		gravity: { min: 0, max: 0.25, distribution: 'Minimal' },
		spinFactor: { min: 0, max: 0.75, distribution: 'Minimal' },
		magneticField: { min: 0, max: 0.5, distribution: 'Minimal' },
		radiationLevel: { min: 0.25, max: 0.75, distribution: 'Bell' },
		size: { min: 0, max: 0.1, distribution: 'Minimal' },
	},
	[TPlanetType.pulsar]: {
		size: { min: 0.1, max: 0.25, distribution: 'Bell' },
		waterPresence: { min: 0, max: 0.75, distribution: 'Minimal' },
		atmosphericDensity: { min: 0, max: 0.0, distribution: 'Minimal' },
		gravity: { min: 1, max: 1, distribution: 'Uniform' },
		spinFactor: { min: 0.75, max: 1, distribution: 'Maximal' },
		magneticField: { min: 0.75, max: 1, distribution: 'Uniform' },
		warpFactor: { min: 0.75, max: 1, distribution: 'Uniform' },
		radiationLevel: { min: 0.75, max: 1, distribution: 'Maximal' },
	},
	[TPlanetType.hollow]: {
		size: { min: 0.65, max: 0.85, distribution: 'Bell' },
		mass: { min: 0, max: 1, distribution: 'Minimal' },
		waterPresence: { min: 0, max: 0, distribution: 'Bell' },
		atmosphericDensity: { min: 0, max: 1, distribution: 'Uniform' },
		gravity: { min: 0, max: 0.25, distribution: 'Minimal' },
		spinFactor: { min: 0, max: 0.25, distribution: 'Uniform' },
		magneticField: { min: 0, max: 0.5, distribution: 'Uniform' },
	},
	[TPlanetType.carbon]: {
		waterPresence: { min: 0, max: 0, distribution: 'Uniform' },
		atmosphericDensity: { min: 0, max: 1, distribution: 'Bell' },
		temperature: { min: 0, max: 0.25, distribution: 'Uniform' },
		gravity: { min: 0.5, max: 1, distribution: 'Bell' },
		mass: { min: 0, max: 1, distribution: 'Bell' },
		spinFactor: { min: 0.25, max: 1, distribution: 'Maximal' },
		magneticField: { min: 0, max: 1, distribution: 'Maximal' },
		radiationLevel: { min: 0.5, max: 1, distribution: 'Bell' },
	},
	[TPlanetType.rogue]: {
		size: { min: 0, max: 0.25, distribution: 'Uniform' },
		mass: { min: 0, max: 0.5, distribution: 'Minimal' },
		atmosphericDensity: { min: 0.25, max: 0.75, distribution: 'Bell' },
		temperature: { min: 0, max: 0.25, distribution: 'Uniform' },
		waterPresence: { min: 0, max: 0.25, distribution: 'Minimal' },
	},
};

export const PlanetResourceBonuses: Record<TPlanetType, Resources.TResourceModifier> = {
	[TPlanetType.tomb]: {
		[Resources.resourceId.isotopes]: {
			factor: 10,
		},
		[Resources.resourceId.carbon]: {
			factor: 5,
		},
		[Resources.resourceId.helium]: {
			factor: 5,
		},
		[Resources.resourceId.ores]: {
			factor: 2,
		},
		[Resources.resourceId.minerals]: {
			factor: 2,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 1,
		},
		[Resources.resourceId.halogens]: {
			factor: 1,
		},
	},
	[TPlanetType.condensed]: {
		[Resources.resourceId.halogens]: {
			factor: 1,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 15,
		},
		[Resources.resourceId.helium]: {
			factor: 10,
		},
		[Resources.resourceId.ores]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 1,
		},
	},
	[TPlanetType.carbon]: {
		[Resources.resourceId.carbon]: {
			factor: 14,
		},
		[Resources.resourceId.ores]: {
			factor: 5,
		},
		[Resources.resourceId.oxygen]: {
			factor: 5,
		},
		[Resources.resourceId.isotopes]: {
			factor: 1,
		},
		[Resources.resourceId.minerals]: {
			factor: 1,
		},
	},
	[TPlanetType.ice]: {
		[Resources.resourceId.hydrogen]: {
			factor: 7,
		},
		[Resources.resourceId.helium]: {
			factor: 5,
		},
		[Resources.resourceId.carbon]: {
			factor: 2,
		},
		[Resources.resourceId.oxygen]: {
			factor: 5,
		},
		[Resources.resourceId.ores]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 3,
		},
		[Resources.resourceId.proteins]: {
			factor: 2,
		},
	},
	[TPlanetType.proto]: {
		[Resources.resourceId.isotopes]: {
			factor: 2,
		},
		[Resources.resourceId.ores]: {
			factor: 5,
		},
		[Resources.resourceId.helium]: {
			factor: 2,
		},
		[Resources.resourceId.minerals]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 3,
		},
	},
	[TPlanetType.arid]: {
		[Resources.resourceId.minerals]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 5,
		},
		[Resources.resourceId.carbon]: {
			factor: 3,
		},
		[Resources.resourceId.halogens]: {
			factor: 1,
		},
		[Resources.resourceId.proteins]: {
			factor: 1,
		},
	},
	[TPlanetType.asteroid]: {
		[Resources.resourceId.helium]: {
			factor: 2,
		},
		[Resources.resourceId.oxygen]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 1,
		},
		[Resources.resourceId.minerals]: {
			factor: 4,
		},
		[Resources.resourceId.ores]: {
			factor: 5,
		},
		[Resources.resourceId.carbon]: {
			factor: 3,
		},
	},
	[TPlanetType.terra]: {
		[Resources.resourceId.isotopes]: {
			factor: 1,
		},
		[Resources.resourceId.minerals]: {
			factor: 1,
		},
		[Resources.resourceId.ores]: {
			factor: 3,
		},
		[Resources.resourceId.carbon]: {
			factor: 3,
		},
		[Resources.resourceId.oxygen]: {
			factor: 7,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 12,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 1,
		},
		[Resources.resourceId.helium]: {
			factor: 1,
		},
		[Resources.resourceId.proteins]: {
			factor: 5,
		},
		[Resources.resourceId.halogens]: {
			factor: 1,
		},
	},
	[TPlanetType.rogue]: {
		[Resources.resourceId.isotopes]: {
			factor: 1,
		},
		[Resources.resourceId.minerals]: {
			factor: 1,
		},
		[Resources.resourceId.ores]: {
			factor: 1,
		},
		[Resources.resourceId.carbon]: {
			factor: 1,
		},
		[Resources.resourceId.oxygen]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 1,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 1,
		},
		[Resources.resourceId.helium]: {
			factor: 1,
		},
		[Resources.resourceId.proteins]: {
			factor: 1,
		},
		[Resources.resourceId.halogens]: {
			factor: 1,
		},
	},
	[TPlanetType.giant]: {
		[Resources.resourceId.isotopes]: {
			factor: 1,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 5,
		},
		[Resources.resourceId.helium]: {
			factor: 4,
		},
		[Resources.resourceId.oxygen]: {
			factor: 1,
		},
		[Resources.resourceId.halogens]: {
			factor: 2,
		},
		[Resources.resourceId.carbon]: {
			factor: 2,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 3,
		},
	},
	[TPlanetType.pulsar]: {
		[Resources.resourceId.isotopes]: {
			factor: 3,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 1,
		},
		[Resources.resourceId.helium]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 5,
		},
		[Resources.resourceId.carbon]: {
			factor: 5,
		},
		[Resources.resourceId.ores]: {
			factor: 5,
		},
		[Resources.resourceId.minerals]: {
			factor: 1,
		},
	},
	[TPlanetType.ocean]: {
		[Resources.resourceId.oxygen]: {
			factor: 7,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 3,
		},
		[Resources.resourceId.helium]: {
			factor: 4,
		},
		[Resources.resourceId.halogens]: {
			factor: 1,
		},
		[Resources.resourceId.proteins]: {
			factor: 5,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 2,
		},
		[Resources.resourceId.carbon]: {
			factor: 1,
		},
	},
	[TPlanetType.volcanic]: {
		[Resources.resourceId.minerals]: {
			factor: 1,
		},
		[Resources.resourceId.oxygen]: {
			factor: 2,
		},
		[Resources.resourceId.carbon]: {
			factor: 2,
		},
		[Resources.resourceId.isotopes]: {
			factor: 1,
		},
		[Resources.resourceId.ores]: {
			factor: 1,
		},
		[Resources.resourceId.halogens]: {
			factor: 2,
		},
		[Resources.resourceId.proteins]: {
			factor: 1,
		},
	},
	[TPlanetType.frozen]: {
		[Resources.resourceId.minerals]: {
			factor: 2,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 1,
		},
		[Resources.resourceId.helium]: {
			factor: 2,
		},
		[Resources.resourceId.oxygen]: {
			factor: 3,
		},
		[Resources.resourceId.carbon]: {
			factor: 1,
		},
		[Resources.resourceId.ores]: {
			factor: 2,
		},
	},
	[TPlanetType.chtonian]: {
		[Resources.resourceId.minerals]: {
			factor: 3,
		},
		[Resources.resourceId.ores]: {
			factor: 8,
		},
		[Resources.resourceId.helium]: {
			factor: 3,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 1,
		},
		[Resources.resourceId.halogens]: {
			factor: 1,
		},
		[Resources.resourceId.isotopes]: {
			factor: 2,
		},
		[Resources.resourceId.carbon]: {
			factor: 2,
		},
	},
	[TPlanetType.hollow]: {
		[Resources.resourceId.minerals]: {
			factor: 1,
		},
		[Resources.resourceId.ores]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 4,
		},
		[Resources.resourceId.carbon]: {
			factor: 3,
		},
		[Resources.resourceId.proteins]: {
			factor: 3,
		},
	},
	[TPlanetType.hycean]: {
		[Resources.resourceId.halogens]: {
			factor: 1,
		},
		[Resources.resourceId.proteins]: {
			factor: 1,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 7,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 4,
		},
		[Resources.resourceId.helium]: {
			factor: 3,
		},
		[Resources.resourceId.oxygen]: {
			factor: 5,
		},
		[Resources.resourceId.isotopes]: {
			factor: 1,
		},
	},
};

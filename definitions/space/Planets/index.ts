import { TStarSystemSharedProps } from '../Systems';

export type TPlanetSpaceProps = TStarSystemSharedProps & {
	warpFactor: number; // c = 0.5, bigger values decrease the duration of effects and cd's, increase  rate of weapons and
	// unit speed, if >0.75 Massive units can't be deployed
	spinFactor: number; // c = 0.5, bigger numbers decrease the precision of Ballistic and Bombardment weapons due
	// to Coriolis force
	atmosphericColorHex: number; // air light hex color
	atmosphericDensity: number; // c=0.5, bigger number reduces AoE radius; reduces Beam damage; increases Cloud
	// and Aerial dodge, reduces Ballistic, Aerial, Missile and Supersonic range,
	// reduces movement speed.
	// If <0.25 or >0.75 Flying units can't be deployed
	gravity: number; // c=0.5,  range of Ballistic, Missile, Aerial and Bombardment weapons
};

export type TPlanetSurfaceProps = TPlanetSpaceProps & {
	magneticField: number; //  c=0.5, bigger numbers decrease precision of Beam and range of abilities, if <0.25
	// Hover units can't be deployed
	surfaceColorHex: number; // ground particles hex color
	waterPresence: number; // c=0.5, from no water to almost only water
	temperature: number; // c=0.5, affects terrain and resources, higher values decrease Heat Protection on Armor
};

export enum TPlanetType {
	arid = 'plnt_ard',
	terra = 'plnt_ter',
	frozen = 'plnt_fro',
	ice = 'plnt_ice',
	condensed = 'plnt_cnd',
	giant = 'plnt_gas',
	tomb = 'plnt_tmb',
	chtonian = 'plnt_cht',
	pulsar = 'plnt_pls',
	hycean = 'plnt_hyc',
	ocean = 'plnt_ocn',
	volcanic = 'plnt_vul',
	asteroid = 'plnt_ast',
	carbon = 'plnt_crb',
	hollow = 'plnt_hol',
	rogue = 'plnt_rog',
	proto = 'plnt_prt',
}

export const PlanetTypeCaptions: EnumProxyWithDefault<TPlanetType, string> = {
	default: 'Planet',
	[TPlanetType.arid]: 'Arid World',
	[TPlanetType.terra]: 'Terrestrial Planet',
	[TPlanetType.frozen]: 'Frozen World',
	[TPlanetType.ice]: 'Ice Giant',
	[TPlanetType.condensed]: 'Condensed World',
	[TPlanetType.giant]: 'Gas Giant',
	[TPlanetType.tomb]: 'Tomb World',
	[TPlanetType.chtonian]: 'Chthonian World',
	[TPlanetType.pulsar]: 'Pulsar Planet',
	[TPlanetType.hycean]: 'Hycean World',
	[TPlanetType.ocean]: 'Ocean Planet',
	[TPlanetType.volcanic]: 'Volcanic World',
	[TPlanetType.asteroid]: 'Asteroid',
	[TPlanetType.carbon]: 'Carbon Planet',
	[TPlanetType.hollow]: 'Hollow World',
	[TPlanetType.rogue]: 'Rogue Planet',
	[TPlanetType.proto]: 'Protoplanet',
};

// this is more complex than union type intentionally, to validate the existence of keys in TPlanetSurfaceProps
export type TPlanetColorKeys = keyof Pick<
	TPlanetSurfaceProps,
	'atmosphericColorHex' | 'surfaceColorHex' | 'starlightColorHex'
>;

export type TPlanetProps = TPlanetSurfaceProps & {
	name?: string;
	type: TPlanetType;
	size: number;
	distanceFromStar?: number;
	regions?: number;
};

export enum TMapTerrainType {
	cold = 'trn_cld',
	desert = 'trn_des',
	dirt = 'trn_drt',
	forest = 'trn_fst',
	frozen = 'trn_frz',
	gas = 'trn_gas',
	goo = 'trn_goo',
	gravel = 'trn_grv',
	hot = 'trn_hot',
	ice = 'trn_ice',
	jungle = 'trn_jng',
	landscape = 'trn_lnd',
	lava = 'trn_lav',
	lowlands = 'trn_lwl',
	mountains = 'trn_mnt',
	rock = 'trn_rck',
	sand = 'trn_snd',
	snow = 'trn_snw',
	peacock = 'trn_pck',
	craters = 'trn_crt',
	earthy = 'trn_ert',
	slime = 'trn_slm',
	vines = 'trn_vns',
	stones = 'trn_stn',
	sulphur = 'trn_sph',
	volcano = 'trn_vol',
}

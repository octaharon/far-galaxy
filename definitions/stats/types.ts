import { TPlayerId } from '../player/playerId';
import UnitAttack from '../tactical/damage/attack';
import Damage from '../tactical/damage/damage';
import UnitDefense from '../tactical/damage/defense';
import UnitActions from '../tactical/units/actions/types';
import UnitChassis from '../technologies/types/chassis';
import Weapons from '../technologies/types/weapons';
import Resources from '../world/resources/types';

namespace GameStats {
	export type TUnitStats = Partial<{
		healthDamageDoneByType: EnumProxy<Damage.damageType, number>;
		damageDoneByWeaponTypes: EnumProxy<Weapons.TWeaponGroupType, number>;
		damageDoneByDeliveryType: EnumProxy<UnitAttack.deliveryType, number>;
		unitsKilledByDeliveryType: EnumProxy<UnitAttack.deliveryType, number>;
		unitsKilledByWeaponTypes: EnumProxy<Weapons.TWeaponGroupType, number>;
		unitsKilledByTargetClass: EnumProxy<UnitChassis.chassisClass, number>;
		unitsKilledByTargetType: EnumProxyWithDefault<UnitChassis.targetType, number>;
		distanceCoveredByMovementType: EnumProxy<UnitChassis.movementType, number>;
		damageDoneToClass: EnumProxy<UnitChassis.chassisClass, number>;
		damageDoneToShields: EnumProxy<UnitDefense.TShieldSlotType, number>;
		damageDoneToTargetType: EnumProxyWithDefault<UnitChassis.targetType, number>;
		offensiveAbilitiesCast: number;
		defensiveAbilitiesCast: number;
		spellsTaken: number;
		orbitalAttacksPerformed: number;
		orbitalDamageDone: number;
		unitKilledByAttackType: EnumProxy<UnitAttack.attackFlags, number>;
		damageDoneByAttackType: EnumProxy<UnitAttack.attackFlags, number>;
		attacksDodgedByType: EnumProxy<UnitAttack.deliveryType, number>;
		breachAttacksDone: number;
		doodadsDestroyed: number;
		accuracyByDeliveryType: EnumProxy<UnitAttack.deliveryType, number>;
		accuracyByWeaponType: EnumProxy<Weapons.TWeaponGroupType, number>;
		damageDoneToArmor: EnumProxy<UnitDefense.TArmorSlotType, number>;
		shieldsLost: number;
		damageTakenByType: EnumProxy<Damage.damageType, number>;
		turnsDisabled: number;
		healthLost: number;
		turnsSurvived: number; // only counts when Deployed
		turnsRowWithoutTakingDamage: number; // only counts when Deployed
		actionsIssued: EnumProxy<UnitActions.unitActionTypes, number>;
	}>;

	export type TProductionStats = Partial<{
		productionOutput: number;
		researchOutput: number;
		engineeringOutput: number;
		factoriesBuilt: number;
		establishmentsBuilt: number;
		facilitiesBuilt: number;
		repairOutput: number;
		repairSpent: number;
		energyProduced: number;
		energySpent: number;
		maxEnergyStored: number;
		maxMaterialsStored: number;
		rawMaterialsOutput: number;
		rawMaterialsSpent: number;
		unitsProducedByChassisId: Record<number, number>;
		unitsDeployedByChassisId: Record<number, number>;
	}>;

	export type TBaseStats = TProductionStats &
		Partial<{
			turnsDisabled: number;
			healthLost: number;
			hullDamageTakenByType: EnumProxy<Damage.damageType, number>;
			spellsTaken: number;
			offensiveAbilitiesCast: number;
			defensiveAbilitiesCast: number;
			battlesWon: number;
			battlesLost: number;
			battlesTotal: number;
			resourcesGainedByType: EnumProxy<Resources.resourceId, number>;
			resourcesSpentByType: EnumProxy<Resources.resourceId, number>;
			salvagePicked: number;
		}>;

	export type TVictoryConditionStats = {
		unitHitPointsPerSector: number[];
		unitHitPointsInCentralArea: number;
		totalDeployedUnitsCost: number;
		totalLostUnitsCost: number;
	};

	export type TTurnStats = Record<
		TPlayerId,
		TUnitStats &
			TProductionStats &
			TVictoryConditionStats & {
				unitsLostByChassisId: Record<number, number>;
				xpGained: number;
			}
	>;

	export type TPlayerStats = TUnitStats &
		TBaseStats &
		Partial<{
			basesLost: number;
			basesAcquired: number;
			totalDeployedUnitsCost: number;
			totalLostUnitsCost: number;
			totalResourcesAcquired: Resources.TResourcePayload;
			unitsLostByChassisId: Record<number, number>;
			unitsKilledWithChassisId: Record<number, number>;
			damageDoneByClass: EnumProxy<UnitChassis.chassisClass, number>;
			damageDoneByTargetType: EnumProxy<UnitChassis.targetType, number>;
			buildingUpgradesGained: number;
			talentPointsGained: number;
			talentsLearned: number;
			researchesComplete: number;
			reverseEngineeringProjectsComplete: number;
			globalEngineeringProjectsCompleteById: Record<number, number>;
			chassisUpgradeProjectsCompleteById: Partial<Record<UnitChassis.chassisClass, number>>;
			ranksGainedByChassisId: Record<number, number>;
		}>;

	export type TEnumerableStatType = TPlayerStats | ValueOf<TTurnStats>;
}

export default GameStats;

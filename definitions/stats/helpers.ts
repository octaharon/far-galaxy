import _ from 'underscore';
import { fastMerge } from '../core/dataTransformTools';
import Series from '../maths/Series';
import { IPlayerBase } from '../orbital/base/types';
import { TPlayerId } from '../player/playerId';
import Players from '../player/types';
import Units from '../tactical/units/types';
import GameStats from './types';

export function getTotalStatValue<T extends Record<any, number> | EnumProxy<any, number>>(object: T) {
	if (!object || !Object.keys(object).length) return 0;
	return Series.sum(Object.values(object));
}

/**
 * Extracts a statistical value from a GameStats storage object
 *
 * @param {TPlayerStats|TUnitStats|TBaseStats|TTurnStats[playerId]} state any stat object for Unit, Base, Player or
 *   Turn (per-player)
 * @param {string} key first lookup index
 * @param {string|number|enum} subkey secondary lookup index
 * @returns {number} a sum of child values, if it's a dictionary; value, if it's numeric; 0, if not found or not a
 *   valid number
 */
export function getStatProperty<
	T extends GameStats.TEnumerableStatType,
	K extends keyof T,
	L extends keyof T[K] | undefined,
>(state: T, key: K, subkey: L = null) {
	const value = subkey ? state?.[key]?.[subkey] : state?.[key];
	if (_.isObject(value)) return Series.sum(Object.values(value));
	if (!Number.isFinite(value)) return 0;
	return value;
}

export function getPlayerStatProperty<
	K extends keyof GameStats.TPlayerStats,
	L extends keyof GameStats.TPlayerStats[K],
>(player: Players.IPlayer, primaryKey: K, secondaryKey: L = null) {
	return getStatProperty(player?.stats, primaryKey, secondaryKey);
}

export function getBaseStatProperty<K extends keyof GameStats.TBaseStats, L extends keyof GameStats.TBaseStats[K]>(
	base: IPlayerBase,
	primaryKey: K,
	secondaryKey: L = null,
) {
	return getStatProperty(base?.stats, primaryKey, secondaryKey);
}

export function getUnitStatProperty<K extends keyof GameStats.TUnitStats, L extends keyof GameStats.TUnitStats[K]>(
	unit: Units.IUnit,
	primaryKey: K,
	secondaryKey: L = null,
) {
	return getStatProperty(unit?.stats, primaryKey, secondaryKey);
}
export function getTurnStatProperty<
	K extends keyof ValueOf<GameStats.TTurnStats>,
	L extends keyof ValueOf<GameStats.TTurnStats>[K],
>(turnStats: GameStats.TTurnStats, playerId: TPlayerId = null, primaryKey: K, secondaryKey: L = null) {
	return playerId
		? (secondaryKey ? turnStats?.[playerId]?.[primaryKey]?.[secondaryKey] : turnStats?.[playerId]?.[primaryKey]) ??
				0
		: Series.sum(
				Object.values(turnStats).flatMap(
					(e) => ((secondaryKey ? e?.[primaryKey]?.[secondaryKey] : e?.[primaryKey]) as number) ?? 0,
				),
		  );
}

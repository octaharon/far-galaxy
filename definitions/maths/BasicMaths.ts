import gaussian from 'gaussian';
import _ from 'underscore';
import { wellRounded } from '../../src/utils/wellRounded';
import { isNotValidModifierValue, trimNumbers } from '../core/helpers';
import $ from '../core/InterfaceTypes';
import { MaxBoundaryUIToken, MinBoundaryUIToken } from '../tactical/statuses/constants';
import Series from './Series';

const Precision = 10e-6;

namespace BasicMaths {
	/**
	 * Limits a value within given boundaries
	 * @param {InterfaceTypes.IValueWithBounds} args
	 * @returns {number}
	 */
	export const clipValue: $.FnValueWithBoundsMap = (args) => Math.max(args.min, Math.min(args.max, args.x)) ?? args.x;

	/**
	 * Limits a value to 0..1
	 * @param {number} x
	 * @returns {number}
	 */
	export const clipTo1 = (x: number) =>
		clipValue({
			x,
			max: 1,
			min: 0,
		});

	/**
	 * Interpolates a value within a range to 0..1 using a linear or exponential metric
	 *
	 * @param args {x,min,max}
	 * @param order >=1
	 */
	export const normalizeValue = (args: $.IValueWithBounds, order = 1) => {
		if (order <= 0) return null;
		return clipTo1(
			Math.pow(args.x ** order - args.min ** order, 1 / order) /
				Math.pow(args.max ** order - args.min ** order, 1 / order),
		);
	};

	export const mapRange = (from: IBounds, to: IBounds, value: number) =>
		!Number.isFinite(value) || value > from.max || value < from.min
			? value
			: lerp({
					...to,
					x: normalizeValue({
						...from,
						x: value,
					}),
			  });

	/**
	 * Linearly interpolates a 0..1 value onto a given range
	 * @param {InterfaceTypes.IValueWithBounds} args
	 * @returns {number}
	 */
	export const lerp = (args: $.IValueWithBounds) =>
		clipValue({
			...args,
			x: args.min * (1 - args.x) + args.max * args.x,
		});

	/**
	 * Create a logarithmic interpolator of a 0..1 value onto a given range
	 * @param {number} logBase
	 * @returns {IValueWithBounds => number}
	 */
	export const logInterp: (base: number) => $.FnValueWithBoundsMap = (logBase: number) => (args) => {
		const l = (x: number) => (x >= 0 ? Math.log(x + 1) / Math.log(logBase) : 0);
		const max = l(args.max);
		const min = l(args.min);
		return lerp({
			...args,
			x: normalizeValue({
				min,
				max,
				x: l(lerp(args)),
			}),
		});
	};

	/**
	 * Interpolates a 0..1 value onto a given range using S-curve
	 * @param {InterfaceTypes.IValueWithBounds} args
	 * @returns {number}
	 */
	export const scurve: $.FnValueWithBoundsMap = (args) =>
		args.min +
		clipValue({
			x: -2 * Math.pow(args.x, 3) + 3 * Math.pow(args.x, 2),
			min: 0,
			max: 1,
		}) *
			(args.max - args.min);

	/**
	 *
	 * @param f function of 1 argument to be transformed
	 * @param transform  linear transformation object
	 */
	export const transformFunction: $.FnTransformFunctionSignature = (f, transform) => (x) => {
		if (!isNaN(transform.translateX)) x -= transform.translateX;
		if (transform.scaleX) x /= transform.scaleX;
		let y = f(x);
		if (transform.scaleY) y *= transform.scaleY;
		if (!isNaN(transform.translateY)) y += transform.translateY;
		return y;
	};

	export const createEasingFunction: (factor: number) => $.FnNumericTranslate = (factor) =>
		transformFunction((x) => (x < 0 ? -1 : 1) * Math.pow(clipTo1(Math.abs(x)), factor), {
			translateX: 0.5,
			translateY: 0.5,
			scaleX: 0.5,
			scaleY: 0.5,
		});

	export interface IGaussianDistributionProperties {
		mean: number;
		variance: number;
	}

	// Inverse gaussian distribution
	export function normsInv(p: number, mean: number, sigma: number): number {
		if (p < 0 || p > 1) {
			throw new Error('The probability P must be bigger than 0 and smaller than 1');
		}
		if (sigma < 0) {
			throw new Error('The standard deviation sigma must be positive');
		}

		if (p === 0) {
			return -Infinity;
		}
		if (p === 1) {
			return Infinity;
		}
		if (sigma === 0) {
			return mean;
		}
		let r;
		let val;
		const q = p - 0.5;
		// ALGORITHM AS241  APPL. STATIST. (1988) VOL. 37, NO. 3
		if (Math.abs(q) <= 0.425) {
			/* 0.075 <= p <= 0.925 */
			r = 0.180625 - q * q;
			val =
				(q *
					(((((((r * 2509.0809287301226727 + 33430.575583588128105) * r + 67265.770927008700853) * r +
						45921.953931549871457) *
						r +
						13731.693765509461125) *
						r +
						1971.5909503065514427) *
						r +
						133.14166789178437745) *
						r +
						3.387132872796366608)) /
				(((((((r * 5226.495278852854561 + 28729.085735721942674) * r + 39307.89580009271061) * r +
					21213.794301586595867) *
					r +
					5394.1960214247511077) *
					r +
					687.1870074920579083) *
					r +
					42.313330701600911252) *
					r +
					1);
		} else {
			/* closer than 0.075 from {0,1} boundary */
			/* r = min(p, 1-p) < 0.075 */
			if (q > 0) r = 1 - p;
			else r = p;
			r = Math.sqrt(-Math.log(r));
			/* r = sqrt(-log(r))  <==>  min(p, 1-p) = exp( - r^2 ) */
			if (r <= 5) {
				/* <==> min(p,1-p) >= exp(-25) ~= 1.3888e-11 */
				r += -1.6;
				val =
					(((((((r * 7.7454501427834140764e-4 + 0.0227238449892691845833) * r + 0.24178072517745061177) * r +
						1.27045825245236838258) *
						r +
						3.64784832476320460504) *
						r +
						5.7694972214606914055) *
						r +
						4.6303378461565452959) *
						r +
						1.42343711074968357734) /
					(((((((r * 1.05075007164441684324e-9 + 5.475938084995344946e-4) * r + 0.0151986665636164571966) *
						r +
						0.14810397642748007459) *
						r +
						0.68976733498510000455) *
						r +
						1.6763848301838038494) *
						r +
						2.05319162663775882187) *
						r +
						1);
			} else {
				/* very close to  0 or 1 */
				r += -5;
				val =
					(((((((r * 2.01033439929228813265e-7 + 2.71155556874348757815e-5) * r + 0.0012426609473880784386) *
						r +
						0.026532189526576123093) *
						r +
						0.29656057182850489123) *
						r +
						1.7848265399172913358) *
						r +
						5.4637849111641143699) *
						r +
						6.6579046435011037772) /
					(((((((r * 2.04426310338993978564e-15 + 1.4215117583164458887e-7) * r + 1.8463183175100546818e-5) *
						r +
						7.868691311456132591e-4) *
						r +
						0.0148753612908506148525) *
						r +
						0.13692988092273580531) *
						r +
						0.59983220655588793769) *
						r +
						1);
			}
			if (q < 0.0) {
				val = -val;
			}
		}
		return mean + sigma * val;
	}

	/**
	 * Sample a random value with skewed normal distribution. Positive skew moves mean to Min bound.
	 *
	 * @param min number
	 * @param max number
	 * @param skew coefficient
	 * @param sigma
	 * @returns number
	 */
	export function skewedNormalRandom(min: number, max: number, skew: number, sigma = 0.001) {
		let num = 0;
		const interp = (u: number, v: number) => Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
		// resample between 0 and 1 if out of range
		while (num > 1 - sigma / 2 || num < sigma / 2) {
			const u = Math.random() * (1 - sigma / 2) + sigma / 2; // Converting [0,1) to (0,1)
			const v = Math.random() * (1 - sigma / 2) + sigma * 2;
			num = interp(u, v);
			num = BasicMaths.normalizeValue({
				x: num,
				min: interp(sigma * 5, 0.5),
				max: interp(sigma * 5, 0),
			}); // Translate to 0 -> 1
		}
		num = Math.pow(num, skew); // Skew
		num *= max - min; // Stretch to fill range
		num += min; // offset to min
		return num;
	}

	export const createGaussian = (props: IGaussianDistributionProperties) => gaussian(props.mean, props.variance);

	const standardDistribution = createGaussian({
		mean: 0.5,
		variance: 0.15,
	});
	const gMin = standardDistribution.cdf(0);
	const gMax = standardDistribution.cdf(1);

	export const createGaussianCDF: (props: IGaussianDistributionProperties) => $.FnNumericTranslate = (props) => {
		const g = createGaussian(props);
		const max = Math.max(gMin, gMax);
		const min = Math.min(gMin, gMax);
		return (x) =>
			normalizeValue({
				x: g.cdf(x),
				max,
				min,
			});
	};

	export const createGaussianPDF: (props: IGaussianDistributionProperties) => $.FnNumericTranslate = (props) => {
		const g = createGaussian(props);
		const gmax = g.pdf(props.mean);
		const gmin = g.pdf(props.mean - 5 * props.variance);
		return (x) =>
			normalizeValue({
				x: g.pdf(x),
				max: gmax,
				min: gmin,
			});
	};

	export function roundToPrecision(v: number, precision = Precision, digits: number = null, ceil = false): number {
		if (!Number.isFinite(v)) return v;
		const nDigits = digits ?? Math.max(0, -Math.floor(Math.log10(precision)));
		const value = (ceil ? Math.ceil(v / precision) : Math.round(v / precision)) * precision;
		return parseFloat(value.toFixed(nDigits));
	}

	export function fractionModulo(v: number, divider: number) {
		if (!Number.isFinite(divider) || divider === 0) return v;
		return v - Math.floor(v / divider) * divider;
	}

	export function applyModifier(v = 0, ...m: IModifier[]): number {
		let t: IModifier[] = [].concat(m).filter((x) => !!x);
		if (!t.length) return v;
		t = t.filter(
			(mod) =>
				v > (isNotValidModifierValue(mod.minGuard) ? -Infinity : mod.minGuard) &&
				v < (isNotValidModifierValue(mod.maxGuard) ? Infinity : mod.maxGuard),
		) as IModifier[]; // modifiers with failed min/max boundary are not applied
		if (!t.length) return v;
		const p = stackModifiers<IModifier>(...t);
		if (isNotValidModifierValue(p.factor)) p.factor = 1;
		if (isNotValidModifierValue(p.bias)) p.bias = 0;
		v = v * p.factor + p.bias;
		if (!isNotValidModifierValue(p.min) && p.min > v) v = p.min;
		if (!isNotValidModifierValue(p.max) && p.max < v) v = p.max;
		return v;
	}

	const pluckValue = <T extends IModifier>(m: T[], k: keyof IModifier) =>
		_.pluck(m || [], k).filter((v) => !isNotValidModifierValue(v));

	/**
	 * Stack IModifiers by common rules:
	 * bias=sum(...bias)
	 * factor=sum( ...percentage)
	 * min=max(...min)
	 * max=min(...max)
	 *
	 * @param m
	 */
	export function stackModifiers<T extends IModifier = IModifier>(...m: T[]): T {
		return trimNumbers(
			_.omit(
				{
					factor: 1 + Series.sum(pluckValue(m, 'factor').map((v) => v - 1)),
					bias: Series.sum(pluckValue(m, 'bias')),
					min: Math.max(-Infinity, ...pluckValue(m, 'min')),
					max: Math.min(Infinity, ...pluckValue(m, 'max')),
					minGuard: Math.max(-Infinity, ...pluckValue(m, 'minGuard')),
					maxGuard: Math.min(Infinity, ...pluckValue(m, 'maxGuard')),
				},
				(value, key) =>
					(key === 'factor' && value === 1) ||
					(key === 'bias' && value === 0) ||
					(['max', 'maxGuard'].includes(key) && value === Infinity) ||
					(['min', 'minGuard'].includes(key) && value === -Infinity),
			) as T,
		);
	}

	/**
	 * Stack IModifiers with product of factors:
	 * bias=sum(...bias)
	 * factor=product(...factors)
	 * min=min(...min)
	 * max=min(...max)
	 * minGuard is the first one
	 * maxGuard is the first one
	 * @param m
	 */
	export function stackAbsorptionModifiers<T extends IModifier = IModifier>(...m: T[]): T {
		let min = Math.min(Infinity, ...pluckValue(m, 'min'));
		if (min >= Infinity) min = 0;
		return trimNumbers(
			_.omit(
				{
					factor: Series.product(pluckValue(m, 'factor')) || 1,
					bias: Series.sum(pluckValue(m, 'bias')) || 0,
					min,
					max: Math.min(Infinity, ...pluckValue(m, 'max')),
					minGuard: pluckValue(m, 'minGuard')[0] || -Infinity,
					maxGuard: pluckValue(m, 'maxGuard')[0] || Infinity,
				},
				(value, key) =>
					(key === 'factor' && value === 1) ||
					(key === 'bias' && value === 0) ||
					(['max', 'maxGuard'].includes(key) && value === Infinity) ||
					(['min', 'minGuard'].includes(key) && value === -Infinity),
			) as T,
		);
	}

	export function describeModifier<T extends IModifier = IModifier>(
		m: T,
		glue?: false,
		force?: boolean,
		negate?: boolean,
		precision?: number,
	): string[];
	export function describeModifier<T extends IModifier = IModifier>(
		m: T,
		glue: true,
		force?: boolean,
		negate?: boolean,
		precision?: number,
	): string;
	export function describeModifier<T extends IModifier = IModifier>(
		m: T,
		glue = false,
		force = false,
		negate = false,
		precision: number = null,
	): string | string[] {
		const r = [] as string[];
		const t = [] as string[];
		if (!m) return glue ? '' : [];
		let { bias, factor } = m;
		const { min, max } = m;
		if (negate) {
			bias *= -1;
			factor = 2 - factor;
		}

		if ((Number.isFinite(bias) && bias !== 0) || force)
			r.push(`${bias >= 0 ? '+' : ''}${bias && precision ? wellRounded(bias, 3) : bias || 0}`);
		if ((Number.isFinite(factor) && factor !== 1) || force)
			r.push(`${factor >= 1 ? '+' : ''}${((factor - 1) * 100).toFixed(0)}%`);
		if ((!negate && Number.isFinite(min)) || force)
			r.push(`${MinBoundaryUIToken}${precision ? wellRounded(min, precision) : min}`);
		if ((!negate && Number.isFinite(max)) || force)
			r.push(`${MaxBoundaryUIToken}${precision ? wellRounded(max, precision) : max}`);
		return glue ? r.join('/').replace(/\s+/, ' ').trim() : r;
	}

	export function describePercentileModifier<T extends IModifier = IModifier>(v: T, glue?: false): string[];
	export function describePercentileModifier<T extends IModifier = IModifier>(v: T, glue: true): string;
	export function describePercentileModifier<T extends IModifier = IModifier>(v: T, glue = false) {
		if (!v) return null;
		const p = [];
		if (v.bias) p.push(`${v.bias > 0 ? '+' : ''}${wellRounded(v.bias * 100, 0)}%`);
		if (v.factor) p.push(`x${wellRounded(v.factor, 2)}`);
		if (Number.isFinite(v.min)) p.push(`${MinBoundaryUIToken}${wellRounded(v.min * 100, 0)}%`);
		if (Number.isFinite(v.max)) p.push(`${MaxBoundaryUIToken}${wellRounded(v.max * 100, 0)}%`);
		return glue ? p.join('/') : p;
	}
}
export default BasicMaths;

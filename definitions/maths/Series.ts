import $ from '../core/InterfaceTypes';

namespace Series {
    export type TSeries = $.TList<number> ;
    export type FnSeriesReducer<T> = (acc: T, value: number) => T;
    export type FnSeriesGenericMethod<T> = (series: TSeries) => T;
    export type FnSeriesIterator<T> = (value: number) => T;
    export const seriesSummatorFn: FnSeriesReducer<number> = (s, v) => !isNaN(v) ? s + (v || 0) : s;
    export const seriesMultiplicatorFn: FnSeriesReducer<number> = (s, v) => !isNaN(v) ? s * v : s;
    export const sum: FnSeriesGenericMethod<number> = (arr) => arr.reduce(seriesSummatorFn, 0);
    export const product: FnSeriesGenericMethod<number> = (arr) => arr.length ?
        arr.reduce(seriesMultiplicatorFn, 1) :
        0;
    export const avg: FnSeriesGenericMethod<number> = (arr) => sum(arr) / arr.length;

}

export default Series;
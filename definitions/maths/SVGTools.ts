import Geometry from './Geometry';
import Vectors from './Vectors';

export namespace SVGTools {
	export function describeArc(cx: number, cy: number, radius: number, startAngle = 0, endAngle = Math.PI * 2) {
		const start = Vectors.add(
			Geometry.polar2vector({
				r: radius,
				alpha: endAngle,
			}),
			{
				x: cx,
				y: cy,
			},
		);
		const end = Vectors.add(
			Geometry.polar2vector({
				r: radius,
				alpha: startAngle,
			}),
			{
				x: cx,
				y: cy,
			},
		);

		const largeArcFlag = Math.abs(endAngle - startAngle) <= Math.PI ? '0' : '1';

		return ['M', start.x, start.y, 'A', radius, radius, 0, largeArcFlag, 0, end.x, end.y].join(' ');
	}

	export function describeRing(
		cx: number,
		cy: number,
		innerRadius: number,
		outerRadius: number,
		startAngle = 0,
		endAngle = Math.PI * 2,
	) {
		const innerStart = Vectors.add(
			Geometry.polar2vector({
				r: innerRadius,
				alpha: startAngle,
			}),
			{
				x: cx,
				y: cy,
			},
		);
		const outerStart = Vectors.add(
			Geometry.polar2vector({
				r: outerRadius,
				alpha: startAngle,
			}),
			{
				x: cx,
				y: cy,
			},
		);
		const innerEnd = Vectors.add(
			Geometry.polar2vector({
				r: innerRadius,
				alpha: endAngle,
			}),
			{
				x: cx,
				y: cy,
			},
		);
		const outerEnd = Vectors.add(
			Geometry.polar2vector({
				r: outerRadius,
				alpha: endAngle,
			}),
			{
				x: cx,
				y: cy,
			},
		);

		const largeArcFlag = Math.abs(endAngle - startAngle) <= Math.PI ? '0' : '1';

		//M 100 300
		// A 100 100 0 0 1 300 300
		// L 250 300
		// A 50 50 0 0 0 150 300
		// L 100 300
		// Z"

		return [
			'M',
			innerStart.x,
			innerStart.y,
			'A',
			innerRadius,
			innerRadius,
			0,
			largeArcFlag,
			0,
			innerEnd.x,
			innerEnd.y,
			'L',
			outerEnd.x,
			outerEnd.y,
			'A',
			outerRadius,
			outerRadius,
			0,
			0,
			largeArcFlag,
			outerStart.x,
			outerStart.y,
			'Z',
		]
			.join(' ')
			.trim();
	}

	export function describeSector(cx: number, cy: number, radius: number, startAngle: number, endAngle: number) {
		return describeArc(cx, cy, radius, startAngle, endAngle) + ' ' + ['L', cx, cy, 'Z'].join(' ').trim();
	}
}

export default SVGTools;

import $ from '../core/InterfaceTypes';
import { ArithmeticPrecision } from './constants';
import Series from './Series';

namespace Vectors {
	export type FnVectorMap = $.FnTranslateSignature<$.IVector>;
	export type FnScalarMap = $.FnMapSignature<$.IVector, number>;
	export type FnVectorOperatorSignature<T> = (X: $.IVector, Y: $.IVector) => T;
	export type FnVectorOperator = FnVectorOperatorSignature<$.IVector>;
	export type FnScalarOperator = FnVectorOperatorSignature<number>;

	export function isNullVector(v: $.IVector) {
		return !v || module()(v) < ArithmeticPrecision;
	}

	export function isValidVector(v: any): v is $.IVector {
		return !!v && Number.isFinite(v['x']) && Number.isFinite(v['y']);
	}

	export function module(order = 2): FnScalarMap {
		return (v: $.IVector) => Math.pow(Math.pow(v.x, order) + Math.pow(v.y, order), 1 / order);
	}

	export function multiply(v: $.IVector, ...k: number[]): $.IVector {
		return {
			x: v.x * Series.product(k || []),
			y: v.y * Series.product(k || []),
		};
	}

	export function create(from: $.IVector, to: $.IVector): $.IVector {
		return add(to, inverse(from));
	}

	export function angleCosine(v1: $.IVector, v2: $.IVector): number {
		return dotProduct(v1, v2) / module()(v1) / module()(v2);
	}

	export function dotProduct(v1: $.IVector, v2: $.IVector): number {
		return v1.x * v2.x + v1.y * v2.y;
	}

	/**
	 * Returns a vector, each coordinate being a multiplication of respective coordinates of all vectors
	 * https://en.wikipedia.org/wiki/Hadamard_product_(matrices)
	 *
	 * @param vectors: IVector[]
	 * return IVector
	 */
	export function hadamard(...vectors: $.IVector[]): $.IVector {
		return vectors
			.filter((v) => isValidVector(v))
			.reduce((acc, v) => ({ x: acc.x * v.x, y: acc.y * v.y }), { x: 1, y: 1 });
	}

	export function normal(order = 2): FnScalarMap {
		return (v: $.IVector) => Math.pow(Math.abs(Math.pow(v.x, order) - Math.pow(v.y, order)), 1 / order);
	}

	export function projection(source: $.IVector, to: $.IVector): $.IVector {
		const asq = module()(to);
		return multiply(to, dotProduct(source, to) / asq);
	}

	export function rotate(vector: $.IVector, origin: $.IVector, angle: number): $.IVector {
		const radians = (Math.PI / 180) * angle;
		const cos = Math.cos(radians);
		const sin = Math.sin(radians);
		const nx = cos * (vector.x - origin.x) + sin * (vector.y - origin.y) + origin.x;
		const ny = cos * (vector.y - origin.y) - sin * (vector.x - origin.x) + origin.y;
		return { x: nx, y: ny };
	}

	export function inverse(v: $.IVector): $.IVector {
		return {
			x: -v.x,
			y: -v.y,
		};
	}

	export function add(...v: $.IVector[]): $.IVector {
		return {
			x: v.reduce((s, a) => s + a.x, 0),
			y: v.reduce((s, a) => s + a.y, 0),
		};
	}

	export function distance(v1: $.IVector, v2: $.IVector): number {
		return module()(create(v1, v2));
	}
}

export default Vectors;

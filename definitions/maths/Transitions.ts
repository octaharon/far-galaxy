import $  from '../core/InterfaceTypes';
import $$ from "./BasicMaths";

namespace Transitions {

    export type TransitionFunctionType = $.FnNumericTranslate;
    const Linear: TransitionFunctionType = (x) => $$.clipValue({
        x,
        min: 0,
        max: 1
    });
    export const TransitionOptions: $.TDictionary<TransitionFunctionType> = {
        Linear,
        Sine:    (x) => Linear(Math.sin(Math.PI / 2 * Linear(x))),
        InvSine: (x) => 1 - TransitionOptions.Sine(1 - x)
    };
    export type TTransition = keyof typeof TransitionOptions;
}

export default Transitions;
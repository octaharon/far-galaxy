export default class PerlinNoise {
    protected perm = new Int8Array(257);

    private constructor(rand = Math.random) {
        for (let i = 0; i < 256; i++) {
            this.perm[i] = i & 1 ? 1 : -1;
        }

        for (let i = 0; i < 256; i++) {
            const j = (rand() * 4294967296) & 255;

            [this.perm[i], this.perm[j]] = [this.perm[j], this.perm[i]];
        }
        this.perm[256] = this.perm[0];
    }

    public i(rand = Math.random) {
        return new PerlinNoise(rand);
    }

    public noise(x: number) {
        let sum = 0;

        sum += (1 + this.noise1d(x)) * 0.25;
        sum += (1 + this.noise1d(x * 2)) * 0.125;
        sum += (1 + this.noise1d(x * 4)) * 0.0625;
        sum += (1 + this.noise1d(x * 8)) * 0.03125;

        return sum;
    }

    protected noise1d(x: number) {
        const x0 = x | 0;
        const x1 = x - x0;
        const xi = x0 & 255;
        const fx = (3 - 2 * x1) * x1 * x1;
        const a = x1 * this.perm[xi];
        const b = (x1 - 1) * this.perm[xi + 1];

        return a + fx * (b - a);
    }
}

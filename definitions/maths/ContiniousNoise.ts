const MAX_VERTICES = 256;
const MAX_VERTICES_MASK = MAX_VERTICES - 1;
/**
 * Linear interpolation function.
 * @param a The lower integer value
 * @param b The upper integer value
 * @param t The value between the two
 * @returns {number}
 */
const lerp = (a: number, b: number, t: number): number => {
    return a * (1 - t) + b * t;
};
export default class ContiniousNoise {
    protected r: number[] = [];

    public constructor(public amplitude: number = 1, public scale: number = 1) {
        for (let i = 0; i < MAX_VERTICES; ++i) {
            this.r.push(Math.random());
        }
    }

    public getVal(x: number) {
        const scaledX = x * this.scale;
        const xFloor = Math.floor(scaledX);
        const t = scaledX - xFloor;
        const tRemapSmoothstep = t * t * (3 - 2 * t);

        /// Modulo using &
        const xMin = xFloor & MAX_VERTICES_MASK;
        const xMax = (xMin + 1) & MAX_VERTICES_MASK;

        const y = lerp(this.r[xMin], this.r[xMax], tRemapSmoothstep);

        return y * this.amplitude;
    }
}

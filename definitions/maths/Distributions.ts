import $ from '../core/InterfaceTypes';
import $$ from './BasicMaths';

const muTreshold = 10e-6;
const gMean = 0.5;
const gVar = 0.15;
const gInv = (x: number) => $$.normsInv(x, gMean, gVar);
export const pMin = gInv(muTreshold);
export const pMax = gInv(1 - muTreshold);
const gCDF = $$.createGaussianCDF({
	mean: gMean,
	variance: gVar,
});
const gPDF = $$.createGaussianPDF({
	mean: gMean,
	variance: gVar,
});

namespace Distributions {
	export type DistributionFunctionType = $.FnNumericTranslate;

	export interface IDistributionCalleeArgs extends $.IValueWithBounds {
		distribution: TDistribution;
	}

	export type FnDistributionCallee = (args: IDistributionCalleeArgs) => number;

	const Uniform: DistributionFunctionType = (x) => $$.clipTo1(x);

	export const DistributionOptions = {
		Uniform,
		Parabolic: gCDF,
		MostlyMinimal: (x: number) => {
			return Uniform(Math.pow(x, 4));
		},
		MostlyMaximal: (x: number) => 1 - DistributionOptions.MostlyMinimal(x),
		Maximal: (x: number) => {
			return Uniform(Math.pow(x, 1 / 2));
		},
		Minimal: (x: number) => 1 - DistributionOptions.Maximal(x),
		Gaussian: (x: number) =>
			$$.clipTo1(
				gInv(
					$$.normalizeValue({
						x,
						max: 1 - muTreshold,
						min: muTreshold,
					}),
				),
			),
		Bell: (x: number) =>
			$$.clipValue({
				min: 0,
				max: 1,
				x: $$.createEasingFunction(1.5)(x),
			}),
	};

	export type TDistribution = keyof typeof DistributionOptions;

	export type ISeedValue<K extends IBounds = IBounds> = Partial<K> & {
		distribution: Distributions.TDistribution;
	};
}

export const applyDistribution = (args: Distributions.ISeedValue<$.IValueWithBounds>, weight?: number) =>
	$$.lerp({
		max: args.max ?? 1,
		min: args.min ?? 0,
		x: Number.isFinite(weight)
			? $$.clipTo1(weight)
			: Distributions.DistributionOptions[args.distribution]($$.clipTo1(args.x ?? Math.random())),
	});

export const DistributionStatParams: {
	[K in Distributions.TDistribution]: {
		avg: number;
		q20: number;
		q50: number;
		q80: number;
		q40: number;
		q60: number;
	};
} = {
	Uniform: {
		avg: 0.5,
		q20: 0.2,
		q50: 0.5,
		q80: 0.2,
		q40: 0.4,
		q60: 0.4,
	},
	Parabolic: {
		avg: 0.5,
		q20: 0.25,
		q50: 0.5,
		q80: 0.25,
		q40: 0.42,
		q60: 0.42,
	},
	MostlyMinimal: {
		avg: 0.2,
		q20: 0.67,
		q50: 0.84,
		q80: 0.05,
		q40: 0.8,
		q60: 0.12,
	},
	MostlyMaximal: {
		avg: 0.8,
		q20: 0.05,
		q50: 0.16,
		q80: 0.67,
		q40: 0.12,
		q60: 0.8,
	},
	Maximal: {
		avg: 0.67,
		q20: 0.04,
		q50: 0.25,
		q80: 0.33,
		q40: 0.16,
		q60: 0.64,
	},
	Minimal: {
		avg: 0.33,
		q20: 0.33,
		q50: 0.75,
		q80: 0.04,
		q40: 0.64,
		q60: 0.16,
	},
	Gaussian: {
		avg: 0.5,
		q20: 0.03,
		q50: 0.5,
		q80: 0.03,
		q40: 0.25,
		q60: 0.25,
	},
	Bell: {
		avg: 0.5,
		q20: 0.13,
		q50: 0.5,
		q80: 0.13,
		q40: 0.33,
		q60: 0.33,
	},
};

export const getDistributionAverage = (args: Distributions.ISeedValue) => {
	return args.min + (args.max - args.min) * DistributionStatParams[args.distribution].avg;
};

export const getDistributionSpectrum = (a: Distributions.ISeedValue): $.IValueWithBounds[] => {
	let min = a.min;
	const r: $.IValueWithBounds[] = [];
	const stats = DistributionStatParams[a.distribution];
	const fractions = [0.2, 0.4, 0.6, 0.8, 1];
	const chances = [stats.q20, stats.q40 - stats.q20, 1 - stats.q40 - stats.q60, stats.q60 - stats.q80, stats.q80];
	for (let i = 0; i < fractions.length; i++) {
		const max = (a.max - a.min) * fractions[i] + a.min;
		r.push({
			min,
			max,
			x: chances[i],
		});
		min = max;
	}
	return r;
};

export default Distributions;

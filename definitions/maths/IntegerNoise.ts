import BasicMaths from './BasicMaths';

export class IntegerNoise {
	protected seed = 0;

	public static i(seed = 0, rand = Math.random): IntegerNoise {
		return new IntegerNoise(seed, rand);
	}

	private constructor(seed = 0, rand = Math.random) {
		this.seed =
			seed ?? Math.round(Number.MIN_SAFE_INTEGER + rand() * (Number.MAX_SAFE_INTEGER - Number.MIN_SAFE_INTEGER));
	}

	public noise() {
		if (this.seed === Number.MAX_SAFE_INTEGER) this.seed = Number.MIN_SAFE_INTEGER;
		this.seed++;
		return this._noise(this.seed);
	}

	public coherentNoise(x: number) {
		const ix = Math.floor(x);
		const n0 = this._noise(ix);
		const n1 = this._noise(ix + 1);
		const weight = x - ix;
		return BasicMaths.lerp({
			x: weight,
			min: n0,
			max: n1,
		});
	}

	protected _noise(n: number) {
		{
			n = (n >> 13) ^ n;
			const nn = (n * (n * n * 60493 + 19990303) + 1376312589) & 0x7fffffff;
			return 1.0 - nn / 1073741824.0;
		}
	}
}

export default IntegerNoise;

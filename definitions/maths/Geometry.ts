import memoize from 'memoize-one';
import _ from 'underscore';
import $ from '../core/InterfaceTypes';
import BasicMaths from './BasicMaths';
import { GeometricPrecision } from './constants';
import Vectors from './Vectors';

const between = (left: number, x: number, right: number): boolean => {
	return left - GeometricPrecision <= x && x <= right + GeometricPrecision;
};

const segment_intersection = memoize(
	(x1: number, y1: number, x2: number, y2: number, x3: number, y3: number, x4: number, y4: number): $.IVector => {
		const d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
		const x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
		const y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;
		if (isNaN(x) || isNaN(y)) {
			return null;
		} else {
			if (x1 >= x2) {
				if (!between(x2, x, x1)) {
					return null;
				}
			} else {
				if (!between(x1, x, x2)) {
					return null;
				}
			}
			if (y1 >= y2) {
				if (!between(y2, y, y1)) {
					return null;
				}
			} else {
				if (!between(y1, y, y2)) {
					return null;
				}
			}
			if (x3 >= x4) {
				if (!between(x4, x, x3)) {
					return null;
				}
			} else {
				if (!between(x3, x, x4)) {
					return null;
				}
			}
			if (y3 >= y4) {
				if (!between(y4, y, y3)) {
					return null;
				}
			} else {
				if (!between(y3, y, y4)) {
					return null;
				}
			}
		}
		return { x, y };
	},
	(newArgs: number[], oldArgs: number[]) => _.isEqual(newArgs, oldArgs),
);

export namespace Geometry {
	export function polar2vector(p: $.IPolar): $.IVector {
		return {
			x: p.r * Math.cos(p.alpha),
			y: p.r * Math.sin(p.alpha),
		};
	}

	export function vector2polar(v: $.IVector): $.IPolar {
		return {
			r: Vectors.module()(v),
			alpha: Math.atan2(v.y, v.x),
		};
	}

	export function polarShapeToArcArray<T extends $.IVector | $.IPolar>(p: $.TPolarShape<T>): Array<$.TArc<T>> {
		const scale = (Math.PI * 2) / p.radiusArray.length;
		return p.radiusArray.map(
			(v, ix) =>
				({
					center: p.center,
					radius: v,
					start: ix * scale - GeometricPrecision * 0.1,
					end: (ix + 1) * scale + GeometricPrecision * 0.1,
				} as $.TArc<T>),
		);
	}

	export function isPolar(t: $.IPolar | $.IVector): t is $.IPolar {
		// @ts-ignore
		return Object.keys(t).includes('alpha');
	}

	export function interpolatePointToRay(p: $.IVector, start: $.IVector, end: $.IVector) {
		return BasicMaths.normalizeValue({
			min: Math.min(start.x, end.x),
			max: Math.max(start.x, end.x),
			x: p.x,
		});
	}

	export function polarShapePoint<T extends $.IVector | $.IPolar>(p: $.TPolarShape<T>, arcIndex: number): $.IVector {
		if (!between(0, arcIndex, p.radiusArray.length - 1) || p.radiusArray.length <= 0) return null;
		const t: $.IVector = isPolar(p.center) ? polar2vector(p.center) : (p.center as $.IVector);
		return Vectors.add(
			t,
			polar2vector({
				r: p.radiusArray[arcIndex],
				alpha: BasicMaths.lerp({
					min: 0,
					max: 2 * Math.PI,
					x: Math.round(arcIndex / (p.radiusArray.length - 1)),
				}),
			}),
		);
	}

	export function doesRayIntersectShape(p: $.TPolarShape<$.IVector>, p1: $.IVector, p2: $.IVector): $.IVector[] {
		const ret = [] as $.IVector[];

		// transform ray to local coords and then to polars
		const s1 = Vectors.create(p.center, p1);
		const s2 = Vectors.create(p.center, p2);
		const t1 = Geometry.vector2polar(s1);
		const t2 = Geometry.vector2polar(s2);
		const t = t1.alpha > t2.alpha ? [t2.alpha, t1.alpha] : [t1.alpha, t2.alpha];
		const step = (Math.PI * 2) / p.radiusArray.length;

		// localize indexes to check intersection with
		const points = [] as $.IPolar[];
		for (let i = 0; i < p.radiusArray.length; i++) {
			const r = p.radiusArray[i];
			const polar = {
				alpha: step * i,
				r,
			} as $.IPolar;
			if (polar.alpha > t[0] - step && polar.alpha < t[1] + step) points.push(polar);
		}

		// find projection of center onto segment

		const normalPoint = Vectors.add(p1, Vectors.projection(Vectors.create(p1, p.center), Vectors.create(p1, p2)));

		// transform it into shape local coordinates
		const normalLocal = Vectors.create(p.center, normalPoint);
		const segmentLocal = Vectors.create(s1, s2);
		for (const point of points) {
			// find projection of shape radius onto segment and offset it by normal distance
			const projectedRadius = Vectors.add(
				normalLocal,
				Vectors.projection(Geometry.polar2vector(point), segmentLocal),
			);
			// if the projection is inside shape radius, consider the intersection
			if (Vectors.module()(projectedRadius) <= point.r) return [Geometry.polar2vector(point)];
		}

		/*for (let i = 0; i < p.radiusArray.length; i++) {
         const nextIndex = (i < p.radiusArray.length - 1) ? i + 1 : 0;
         const intersection = whereDoRaysCross(p1, p2, polarShapePoint(p, i), polarShapePoint(p, nextIndex));
         if (intersection)
         r.push(intersection);
         }*/
		if (ret.length) return ret;
		return null;
	}

	export function areVectorsClockwise(v1: $.IVector, v2: $.IVector) {
		return -v1.x * v2.y + v1.y * v2.x > 0;
	}

	export function isPointInsideSector(p: $.IVector, v1: $.IVector, v2: $.IVector) {
		const [start, end] = areVectorsClockwise(v1, v2) ? [v2, v1] : [v1, v2];
		return (
			!areVectorsClockwise(start, p) &&
			areVectorsClockwise(end, p) &&
			Vectors.module()(p) < Math.min(Vectors.module()(v1), Vectors.module()(v2))
		);
	}

	export function generateImpactSector(
		source: $.IVector,
		target: $.IVector,
		range: number,
		widthDegrees: number,
		local = false,
	): [$.IVector, $.IVector] {
		const impactVector = Vectors.create(source, target); // the vector from source to
		// target, which is also a new
		// coordinate center
		const impactDirection = vector2polar(impactVector);
		const startArm = polar2vector({
			r: range,
			alpha: impactDirection.alpha - (Math.PI / 360) * widthDegrees,
		});
		const endArm = polar2vector({
			r: range,
			alpha: impactDirection.alpha + (Math.PI / 360) * widthDegrees,
		});
		if (local) return [startArm, endArm];
		return [Vectors.add(target, startArm), Vectors.add(target, endArm)];
	}

	export function generateImpactRectangle(
		source: $.IVector,
		target: $.IVector,
		collinearWidth: number,
		tangentialWidth: number,
		alongImpact = false,
		local = false,
	): $.TQuadrigon {
		const impactVector = Vectors.create(source, target);
		const impactDirection = vector2polar(impactVector);
		const collinearDistance = polar2vector({
			r: collinearWidth / 2,
			alpha: impactDirection.alpha,
		});
		const tangentialDistanceClockwise = polar2vector({
			r: tangentialWidth / 2,
			alpha: impactDirection.alpha - Math.PI / 2,
		});
		let p = new Array(4).fill(null) as $.TQuadrigon;
		p.push(Vectors.add(collinearDistance, tangentialDistanceClockwise));
		p.push(Vectors.add(Vectors.inverse(collinearDistance), tangentialDistanceClockwise));
		p.push(Vectors.add(collinearDistance, Vectors.inverse(tangentialDistanceClockwise)));
		p.push(Vectors.add(Vectors.inverse(collinearDistance), Vectors.inverse(tangentialDistanceClockwise)));
		if (alongImpact) p = p.map((v) => Vectors.add(v, collinearDistance)) as $.TQuadrigon;
		if (local) return p;
		return p.map((v) => Vectors.add(v, target)) as $.TQuadrigon;
	}

	export function isPointInsidePolygon(p: $.IVector, rec: $.TPolygon): boolean {
		const shp = rec.concat(rec[0]);
		for (let i = 0; i < rec.length; i++) {
			const leftAngledVector = Vectors.create(shp[0], shp[i + 1]);
			const azimuth = Vectors.create(shp[0], p);
			if (!areVectorsClockwise(leftAngledVector, azimuth)) return false;
		}
		return true;
	}

	export function getRayUnitCircleIntersection(radius: number, p1: $.IVector, p2: $.IVector): $.IVector[] | null {
		const line = getLineFromPoints(p1, p2);
		const a = line.k;
		const b = -1;
		const c = line.b;
		const r = radius;

		const x0 = (-a * c) / (a * a + b * b);
		const y0 = (-b * c) / (a * a + b * b);
		if (c * c > r * r * (a * a + b * b) + GeometricPrecision) return null;
		else if (Math.abs(c * c - r * r * (a * a + b * b)) < GeometricPrecision) {
			return [{ x: x0, y: y0 }];
		} else {
			const d = r * r - (c * c) / (a * a + b * b);
			const mult = Math.sqrt(d / (a * a + b * b));
			const ax = x0 + b * mult;
			const bx = x0 - b * mult;
			const ay = y0 - a * mult;
			const by = y0 + a * mult;
			return [
				{
					x: ax,
					y: ay,
				},
				{
					x: bx,
					y: by,
				},
			];
		}
	}

	export function isPointInTriangle(p: $.IVector, v1: $.IVector, v2: $.IVector, v3: $.IVector) {
		const A = (1 / 2) * (-v2.y * v3.x + v1.y * (-v2.x + v3.x) + v1.x * (v2.y - v3.y) + v2.x * v3.y);
		const sign = A < 0 ? -1 : 1;
		const s = (v1.y * v3.x - v1.x * v3.y + (v3.y - v1.y) * p.x + (v1.x - v3.x) * p.y) * sign;
		const t = (v1.x * v2.y - v1.y * v2.x + (v1.y - v2.y) * p.x + (v2.x - v1.x) * p.y) * sign;

		return s > 0 && t > 0 && s + t < 2 * A * sign;
	}

	export function arcToVectors(arc: $.TArc<$.IVector>): $.IVector[] {
		const start = polar2vector({
			r: arc.radius,
			alpha: arc.end,
		});
		const end = polar2vector({
			r: arc.radius,
			alpha: arc.start,
		});

		return [Vectors.add(arc.center, start), Vectors.add(arc.center, end)];
	}

	export function getLineFromPoints(p1: $.IVector, p2: $.IVector): $.ILine {
		const k = (p2.y - p1.y) / (p2.x - p1.x);
		const b = p2.y - k * p2.x;
		return { k, b };
	}

	export function getParametricLineFromPoints(p1: $.IVector, p2: $.IVector): $.IParametricLine {
		return {
			a: p1.y - p2.y,
			b: p2.x - p1.x,
			c: p1.x * p2.y - p2.x * p1.y,
		};
	}

	export function doesRayIntersectCircle(spot: $.ISpot, p1: $.IVector, p2: $.IVector): $.IVector[] {
		const localP1 = Vectors.create(spot, p1);
		const localP2 = Vectors.create(spot, p2);
		const c = getRayUnitCircleIntersection(spot.radius, localP1, localP2);
		if (!c || !c.length) return null;
		return c.map((p) => Vectors.add(p, spot));
	}

	export function doesLineIntersectCircle(line: $.ILine, circle: $.ISpot): boolean {
		const a = line.k;
		const b = -1;
		const c = line.b;
		const d = Math.abs(a * circle.x + b * circle.y + c) / Math.sqrt(a * a + b + b);
		return d <= circle.radius;
	}

	export function whereDoRaysCross(a1: $.IVector, a2: $.IVector, b1: $.IVector, b2: $.IVector): $.IVector {
		return segment_intersection(a1.x, a1.y, a2.x, a2.y, b1.x, b1.y, b2.x, b2.y);
	}

	export function isPointInRange(point: $.IVector, area: $.ISpot): boolean {
		return Vectors.module()(Vectors.create(area, point)) - area.radius <= GeometricPrecision;
	}

	export function isPointInsideShape(x: $.IVector, t: $.TPolarShape<$.IVector>, within = 0): boolean {
		const localVector = Vectors.create(t.center, x);
		const localPolar = vector2polar(localVector);
		const rad = [].concat(t.radiusArray).map((v) => v + within);
		while (localPolar.alpha > Math.PI * 2) localPolar.alpha -= Math.PI * 2;
		while (localPolar.alpha < 0) localPolar.alpha += Math.PI * 2;
		const arcIndex = Math.floor(
			BasicMaths.normalizeValue({
				x: localPolar.alpha,
				max: Math.PI * 2,
				min: 0,
			}) *
				(rad.length - 1),
		);
		const prevIndex = arcIndex > 0 ? arcIndex - 1 : rad.length - 1;
		const nextIndex = arcIndex < rad.length - 1 ? arcIndex + 1 : 0;
		const d = { ...t, radiusArray: rad };
		const c1 = polarShapePoint(d, arcIndex);
		const c2 = polarShapePoint(d, nextIndex);
		const c3 = polarShapePoint(d, prevIndex);
		return isPointInTriangle(localVector, c1, c2, d.center) || isPointInTriangle(localVector, c1, c3, d.center);
	}
}

export default Geometry;

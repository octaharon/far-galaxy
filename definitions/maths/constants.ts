export const GeometricPrecision = 1e-3;
export const ArithmeticPrecision = 1e-5;

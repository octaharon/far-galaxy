import _ from 'underscore';
import BasicMaths from '../maths/BasicMaths';
import DefenseManager from '../tactical/damage/DefenseManager';
import { MinBoundaryUIToken } from '../tactical/statuses/constants';
import { ActionCaptions } from '../tactical/units/actions/constants';
import {
	describeArmorModifier,
	describeShieldModifier,
	describeWeaponGroupModifier,
} from '../technologies/chassis/modifiers';
import { TChassisTypedSlot } from '../technologies/types/chassis';
import { IPrototypeModifier, IUnitEffectModifier } from './types';

export const stackUnitEffectsModifiers = (...mod: IUnitEffectModifier[]): IUnitEffectModifier => {
	const cleanMods = (mod || []).filter(Boolean);
	return _.uniq(cleanMods.flatMap((v) => Object.keys(v))).reduce(
		(obj, k: keyof IUnitEffectModifier) =>
			Object.assign(obj, {
				[k]: cleanMods.reduce((list, modifier) => list.concat(modifier[k] ?? []), []),
			}),
		{},
	);
};

const isNotEmptyProperty = (v: any): boolean => !!v && !(Object.keys(v).length === 0 && v.constructor === Object);

export const stackPrototypeModifiers = (...modifiers: IPrototypeModifier[]): IPrototypeModifier => {
	const mod = {} as IPrototypeModifier;

	for (const m of modifiers.filter(Boolean)) {
		mod.armor = (mod.armor || []).concat(m?.armor).filter(isNotEmptyProperty);
		mod.weapon = (mod.weapon || []).concat(m?.weapon).filter(isNotEmptyProperty);
		mod.shield = (mod.shield || []).concat(m?.shield).filter(isNotEmptyProperty);
		mod.chassis = (mod.chassis || []).concat(m?.chassis).filter(isNotEmptyProperty);
		if (m.unitModifier) mod.unitModifier = stackUnitEffectsModifiers(m.unitModifier, mod.unitModifier);
	}
	return mod;
};

export const describePrototypeSlots = <T extends TChassisTypedSlot>(slots: T[] = []) => {
	const hash = slots.reduce(
		(obj: { [key in T]: number }, slotType) =>
			Object.assign(obj, {
				[slotType]: (obj[slotType] || 0) + 1,
			}),
		{} as { [key in T]: number },
	);
	return (Object.keys(hash) as T[])
		.reduce((arr: string[], slotType) => [...arr, `${hash[slotType]} x ${slotType}`], [] as string[])
		.join(', ');
};

export const describePrototypeModifier = (m: IPrototypeModifier, infix?: string) => {
	const s: string[] = [];
	if (m?.chassis?.length)
		m.chassis.forEach((mod) => {
			if (mod.speed) s.push(`${BasicMaths.describeModifier(mod.speed, true)} ${infix} Speed`);
			if (mod.hitPoints) s.push(`${BasicMaths.describeModifier(mod.hitPoints, true)} ${infix} Hit Points`);
			if (mod.baseCost) s.push(`${BasicMaths.describeModifier(mod.baseCost, true)} ${infix} Prototype Cost`);
			if (mod.dodge)
				s.push(
					...DefenseManager.describeDodgeModifier(mod.dodge)
						.split(',')
						.map((s) => s.trim())
						.map((s) => `${s} ${infix} Dodge`.replace('All Attacks', '')),
				);
			if (mod.actionCount) s.push(`+${mod.actionCount} ${infix} Action Phases`);
			if (mod.defenseFlags)
				s.push(
					...DefenseManager.describeDefenseFlags(mod.defenseFlags)
						.split(',')
						.map((t) => `${infix} ${t.trim()}`),
				);
			if (mod.scanRange) s.push(`${BasicMaths.describeModifier(mod.scanRange, true)} ${infix} Scan Range`);
			if (mod.actions) s.push(...mod.actions.map((act) => `${infix} ${ActionCaptions[act]} Action`));
		});
	if (m?.weapon?.length) {
		m.weapon.forEach((mod) => {
			s.push(
				...describeWeaponGroupModifier(mod)
					.split(', ')
					.map((arr) => arr.replace('All Weapons', 'Weapon').replace('Other Weapons', 'Other').split(' '))
					.map((t) => `${t[0]} ${infix} ${t.slice(1).join(' ')}`),
			);
		});
	}
	if (m?.shield?.length)
		m.shield.forEach((mod) => {
			s.push(
				...describeShieldModifier(mod)
					.split(',')
					.map((t) => t.trim().split(' '))
					.map((t) => `${t[0]} ${infix} ${t.slice(1).join(' ')}`),
			);
		});
	if (m?.armor?.length)
		m.armor.forEach((mod) => {
			s.push(
				...describeArmorModifier(mod)
					.split(',')
					.map((t) => t.trim().split(' '))
					.map((t) => `${t[0]} ${infix} ${t.slice(1).join(' ')}`),
			);
		});
	if (m?.unitModifier?.statusDurationModifier?.length)
		m.unitModifier.statusDurationModifier.forEach((mod) =>
			s.push(`${BasicMaths.describeModifier(mod, true, false, true)} ${infix} Effect Resistance`),
		);
	if (m?.unitModifier?.abilityDurationModifier?.length)
		m.unitModifier.abilityDurationModifier.forEach((mod) =>
			s.push(`${BasicMaths.describeModifier(mod, true)} ${infix} Effect Penetration`),
		);
	if (m?.unitModifier?.abilityPowerModifier?.length)
		m.unitModifier.abilityPowerModifier.forEach((mod) =>
			s.push(`${BasicMaths.describeModifier(mod, true)} ${infix} Ability Power`),
		);
	if (m?.unitModifier?.abilityRangeModifier?.length)
		m.unitModifier.abilityRangeModifier.forEach((mod) =>
			s.push(`${BasicMaths.describeModifier(mod, true)} ${infix} Ability Range`),
		);
	if (m?.unitModifier?.abilityCooldownModifier?.length)
		m.unitModifier.abilityCooldownModifier.forEach((mod) =>
			s.push(`${BasicMaths.describeModifier(mod, true)} ${infix} Ability Cooldown`),
		);
	return s.map((t) => t.replace(/\s+/gi, ' ').replace(`/${MinBoundaryUIToken}1 `, ' ').trim());
};

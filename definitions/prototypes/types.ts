import { ISerializable, ISerializableFactory } from '../core/Serializable';
import { IPlayerProperty, IPlayerPropertyProps } from '../player/types';
import UnitDefense from '../tactical/damage/defense';
import { TAuraApplianceList } from '../tactical/statuses/Auras/types';
import { TEffectApplianceList } from '../tactical/statuses/types';
import UnitActions from '../tactical/units/actions/types';
import { IUnitPartDecorator } from '../tactical/units/UnitPart';
import { IArmor, TSerializedArmor } from '../technologies/types/armor';
import UnitChassis, { IChassis } from '../technologies/types/chassis';
import { IEquipment, TSerializedEquipment } from '../technologies/types/equipment';
import { IShield, TSerializedShield } from '../technologies/types/shield';
import Weapons, { IWeapon } from '../technologies/types/weapons';

export interface IPrototypeModifier {
	chassis?: UnitChassis.TUnitChassisModifier[];
	armor?: UnitDefense.TArmorModifier[];
	weapon?: Weapons.TWeaponGroupModifier[];
	shield?: UnitDefense.TShieldsModifier[];
	unitModifier?: IUnitEffectModifier;
}

export type TUnitModifierKeys =
	| 'abilityRangeModifier'
	| 'abilityCooldownModifier'
	| 'statusDurationModifier'
	| 'abilityDurationModifier'
	| 'abilityPowerModifier';

export type IUnitEffectModifier = Partial<Record<TUnitModifierKeys, IModifier[]>>;

export type TPrototypeProps = IUnitPartDecorator &
	IPlayerPropertyProps & {
		caption: string;
		baseCost: number;
		modifiers: IPrototypeModifier;
	};
export type TPrototypeDynamicProps = {
	chassis: IChassis;
	weapons: IWeapon[];
	armor: IArmor[];
	shields: IShield[];
	equipment: IEquipment[];
};

export interface IPrototype
	extends TPrototypeProps,
		TPrototypeDynamicProps,
		IPlayerProperty,
		ISerializable<TPrototypeProps, TSerializedPrototype> {
	final: IPrototype;
	cost: number;

	addModifier(m: IPrototypeModifier): IPrototype;

	clone(instanceId?: string): IPrototype;

	applyModifiers(): IPrototype;

	setChassis(chassisId: number): IPrototype;

	setArmor(slot: number, armorId: number): IPrototype;

	setShield(slot: number, shieldId: number): IPrototype;

	setWeapon(slot: number, weaponId: number): IPrototype;

	setEquipment(slot: number, eqId: number): IPrototype;

	getChassis(): IChassis;

	getArmor(slot: number): IArmor;

	getShield(slot: number): IShield;

	getWeapon(slot: number): IWeapon;

	getEquipment(slot: number): IEquipment;

	hasWeaponAtSlot(slot?: number): boolean;

	hasArmorAtSlot(slot?: number): boolean;

	hasShieldAtSlot(slot?: number): boolean;

	hasEquipmentAtSlot(slot?: number): boolean;

	getEffectiveActions(): UnitActions.TUnitActionOptions[];

	canPerformOrbitalAttack(): boolean;

	getBaseEffects(): TEffectApplianceList;
	getBaseAuras(): TAuraApplianceList;
	getBaseAbilities(): number[];

	update(reassemble?: boolean): IPrototype;

	reset(): IPrototype;
}

export type TPrototypeSerializedProps = {
	chassis: UnitChassis.TSerializedChassis;
	weapons: Weapons.TSerializedWeapon[];
	armor: TSerializedArmor[];
	shields: TSerializedShield[];
	equipment: TSerializedEquipment[];
};
export type TSerializedPrototype = TPrototypeProps & IWithInstanceID & TPrototypeSerializedProps;

export type IPrototypeFactory = ISerializableFactory<TPrototypeProps, TSerializedPrototype, IPrototype> & {
	createFromHash(hash: string, props: Partial<TPrototypeProps>): IPrototype;
	getHash(prototype: IPrototype): string;
};

import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles, DIInjectableSerializables } from '../core/DI/injections';
import BasicMaths from '../maths/BasicMaths';
import PlayerProperty from '../player/PlayerProperty';
import { TAuraApplianceList } from '../tactical/statuses/Auras/types';
import { TEffectApplianceList } from '../tactical/statuses/types';
import {
	doesActionRequireSeveralWeapons,
	doesActionRequireSpeed,
	doesActionRequireWeapon,
	doesActionUseAbility,
	isOrbitalAttackAction,
} from '../tactical/units/actions/ActionManager';
import { ORBITAL_ATTACK_RANGE } from '../tactical/units/actions/constants';
import UnitActions from '../tactical/units/actions/types';
import { IArmor, TArmorItem } from '../technologies/types/armor';
import { IChassis } from '../technologies/types/chassis';
import { IEquipment } from '../technologies/types/equipment';
import { IShield, TShieldItem } from '../technologies/types/shield';
import { IWeapon } from '../technologies/types/weapons';
import { stackPrototypeModifiers } from './helpers';
import { IPrototype, IPrototypeModifier, TPrototypeProps, TSerializedPrototype } from './types';

export class UnitPrototype extends PlayerProperty<TPrototypeProps, TSerializedPrototype> implements IPrototype {
	public caption = 'New prototype';
	public chassis: IChassis;
	public weapons: IWeapon[];
	public armor: IArmor[];
	public shields: IShield[];
	public equipment: IEquipment[];
	public modifiers: IPrototypeModifier;
	public baseCost: number;

	protected __reassembling = false;

	constructor(p: TPrototypeProps) {
		super(p);
		if (!this.weapons) this.weapons = [];
		if (!this.armor) this.armor = [];
		if (!this.shields) this.shields = [];
		if (!this.equipment) this.equipment = [];
		if (!this.modifiers) this.modifiers = {};
	}

	public get cost() {
		return this.getCosts();
	}

	public get final() {
		return this.clone(this.getInstanceId() + '+').applyModifiers();
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableSerializables.prototype];
	}

	public clone(instanceId: string = this.generateInstanceId()) {
		const c = this.getProvider(DIInjectableSerializables.prototype).clone(this);
		c.setInstanceId(instanceId);
		return c;
	}

	public serialize(): TSerializedPrototype {
		return {
			...this.__serializePlayerProperty(),
			caption: this.caption,
			chassis: this.chassis ? this.chassis.serialize() : null,
			weapons: (this.weapons || []).map((t) => (t ? t.serialize() : null)),
			armor: (this.armor || []).map((t) => (t ? t.serialize() : null)),
			equipment: (this.equipment || []).filter(Boolean).map((t) => (t ? t.serialize() : null)),
			shields: (this.shields || []).map((t) => (t ? t.serialize() : null)),
			baseCost: this.baseCost,
			modifiers: this.modifiers || {},
		};
	}

	public getEffectiveActions(): UnitActions.TUnitActionOptions[] {
		return (this.chassis?.actions || []).map((actions) =>
			[
				...actions,
				...(this.canPerformOrbitalAttack() && actions.some(isOrbitalAttackAction)
					? [UnitActions.unitActionTypes.orbital_attack]
					: []),
			].filter(
				(action) =>
					(this.chassis?.speed > 0 || !doesActionRequireSpeed(action)) &&
					(this.weapons?.filter(Boolean)?.length >= 2 || !doesActionRequireSeveralWeapons(action)) &&
					(!!this.weapons?.filter(Boolean)?.length ||
						!doesActionRequireWeapon(action) ||
						(this.getBaseAbilities().length && doesActionUseAbility(action))),
			),
		);
	}

	public canPerformOrbitalAttack(): boolean {
		return this.final.weapons?.filter(Boolean)?.some((w) => w.baseRange >= ORBITAL_ATTACK_RANGE);
	}

	public addModifier(m: IPrototypeModifier): UnitPrototype {
		if (!m) return this;
		this.modifiers = stackPrototypeModifiers(this.modifiers, m);
		//console.log(`adding modifier`, m, this.modifiers);
		return this;
	}

	/**
	 * Burn in modifiers and clear the modifier stack
	 */
	public applyModifiers() {
		if (!this.getChassis()) return this;
		this.chassis.applyModifier(...(this.modifiers.chassis || []).filter((v) => !!v));
		this.armor = this.armor.map((a) => a?.applyModifier(...(this.modifiers.armor || [])) || null);
		this.weapons = this.weapons.map((a, ix) =>
			a
				? a
						.applySlotModifier(this.chassis.static.weaponSlots[ix])
						.applyGroupModifier(...(this.modifiers.weapon || []))
				: null,
		);
		this.shields = this.shields.map((a) => a?.applyModifier(...(this.modifiers.shield || [])) || null);
		this.getCosts();
		this.modifiers = {
			// @FIXME somehow unit modifier gets lost when stacking Chassis modifier and Faction modifier
			unitModifier: this.modifiers.unitModifier || {},
		};
		return this;
	}

	/**
	 * Return all Effects to be applied to a Unit after production
	 */
	getBaseEffects(): TEffectApplianceList {
		return ([] as TEffectApplianceList)
			.concat(
				this.getChassis()?.static?.effects,
				(this.equipment || []).flatMap((e) => e?.static?.effects),
			)
			.filter((eff) => this.getProvider(DIInjectableCollectibles.effect).get(eff.effectId));
	}

	getBaseAuras(): TAuraApplianceList {
		return ([] as TAuraApplianceList)
			.concat(
				this.getChassis()?.static?.auras,
				(this.equipment || []).flatMap((e) => e?.static?.auras),
			)
			.filter((eff) => this.getProvider(DIInjectableCollectibles.aura).get(eff.id));
	}

	getBaseAbilities() {
		return ([] as number[])
			.concat(
				this.getChassis()?.static?.abilities,
				(this.equipment || []).flatMap((e) => e?.static?.abilities),
			)
			.filter((abilityId) => this.getProvider(DIInjectableCollectibles.ability).get(abilityId));
	}

	public setChassis(chassisId: number): UnitPrototype {
		this.reset();
		if (!chassisId) return this;
		this.chassis = this.getProvider(DIInjectableCollectibles.chassis).instantiate(null, chassisId);
		if (this.chassis.static.unitEffectModifier)
			this.addModifier({
				unitModifier: this.chassis.static.unitEffectModifier,
			});
		return this.update();
	}

	public setArmor(slot: number, armorId: number): UnitPrototype {
		if (
			!this.getChassis()?.static?.armorSlots[slot] ||
			(armorId &&
				!this.getProvider(DIInjectableCollectibles.armor).doesArmorFitSlot(
					this.getProvider(DIInjectableCollectibles.armor).get<TArmorItem>(armorId),
					this.chassis.static.armorSlots[slot],
				))
		)
			throw new Error("This armor doesn't fit here");
		this.armor[slot] = null;
		if (!armorId) return this;
		this.armor[slot] = this.getProvider(DIInjectableCollectibles.armor).instantiate(null, armorId);
		return this.update();
	}

	public setEquipment(slot: number, equipmentId: number): UnitPrototype {
		if (
			this.chassis.equipmentSlots < slot ||
			slot < 0 ||
			(equipmentId && !this.getProvider(DIInjectableCollectibles.equipment).get(equipmentId))
		)
			throw new Error("This equipment doesn't fit here");
		this.equipment[slot] = null;
		if (!equipmentId) return this;
		this.equipment[slot] = this.getProvider(DIInjectableCollectibles.equipment).instantiate(null, equipmentId);
		return this.update();
	}

	public setShield(slot: number, shieldId: number) {
		if (
			!this.chassis.static.shieldSlots[slot] ||
			(shieldId &&
				!this.getProvider(DIInjectableCollectibles.shield).doesShieldFitSlot(
					this.getProvider(DIInjectableCollectibles.shield).get<TShieldItem>(shieldId),
					this.chassis.static.shieldSlots[slot],
				))
		)
			throw new Error("This shield doesn't fit here");
		this.shields[slot] = null;
		if (!shieldId) return this;
		const shield = this.getProvider(DIInjectableCollectibles.shield).instantiate(null, shieldId);
		this.shields[slot] = shield;
		return this.update();
	}

	public setWeapon(slot: number, weaponId: number) {
		if (weaponId) {
			const weaponMeta = this.getProvider(DIInjectableCollectibles.weapon).getItemMeta(weaponId);
			if (!weaponMeta) throw new Error('Weapon not found');
			if (!weaponMeta.slots.includes(this.chassis.static.weaponSlots[slot]))
				throw new Error("This isWeapon doesn't fit here");
		}
		this.weapons[slot] = null;
		if (!weaponId) return this;
		this.weapons[slot] = this.getProvider(DIInjectableCollectibles.weapon).instantiate(null, weaponId);
		return this.update();
	}

	public hasArmorAtSlot(slot = 0): boolean {
		return slot >= 0 ? !!this.armor?.[slot] : this.armor?.some(Boolean);
	}

	public hasEquipmentAtSlot(slot = 0): boolean {
		return slot >= 0 ? !!this.equipment?.[slot] : this.equipment?.some(Boolean);
	}

	public hasShieldAtSlot(slot = 0): boolean {
		return slot >= 0 ? !!this.shields?.[slot] : this.shields?.some(Boolean);
	}

	public hasWeaponAtSlot(slot = 0): boolean {
		return slot >= 0 ? !!this.weapons?.[slot] : this.weapons?.some(Boolean);
	}

	public getArmor(slot: number): IArmor {
		return this.armor?.[slot] || null;
	}

	public getChassis(): IChassis {
		return this.chassis || null;
	}

	public getEquipment(slot: number): IEquipment {
		return this.equipment?.[slot] || null;
	}

	public getShield(slot: number): IShield {
		return this.shields?.[slot] || null;
	}

	public getWeapon(slot: number): IWeapon {
		return this.weapons?.[slot] || null;
	}

	public update(reassemble = false) {
		if (reassemble && !this.__reassembling) this.reassemble();
		this.collectModifiers();
		this.generateCaption();
		this.getCosts();
		return this;
	}

	public reset() {
		this.weapons = [];
		this.armor = [];
		this.shields = [];
		this.baseCost = 0;
		this.equipment = [];
		this.chassis = null;
		this.modifiers = {};
		return this;
	}

	protected collectModifiers() {
		this.modifiers = {};
		//Chassis Mods
		if (this.getChassis())
			this.addModifier({
				armor: [this.getChassis().static?.armorModifier || null],
				weapon: [this.getChassis().static?.weaponModifier || null],
				shield: [this.getChassis().static?.shieldModifier || null],
				chassis: [
					{
						defenseFlags: Object.keys(this.getChassis().defenseFlags || {})
							.filter((v) => !!this.getChassis()?.defenseFlags?.[v])
							.reduce(
								(obj, k) =>
									Object.assign(obj, {
										[k]: true,
									}),
								{},
							),
					},
				],
			});
		(this.shields || []).filter(Boolean).forEach((shield) => {
			if (Object.keys(shield.static.flags || {}).length)
				Object.keys(shield.static.flags)
					.filter((k) => !!shield.static.flags[k])
					.forEach((k) =>
						this.addModifier({
							chassis: [
								{
									defenseFlags: {
										[k]: true,
									},
								},
							],
						}),
					);
		});
		// Armor Mods
		(this.armor || []).filter(Boolean).forEach((armor) => {
			this.addModifier({
				chassis: [
					Object.keys(armor.static.flags || {}).length
						? {
								defenseFlags: Object.keys(armor.static.flags)
									.filter((k) => !!armor.static.flags[k])
									.reduce(
										(obj, k) =>
											Object.assign(obj, {
												[k]: true,
											}),
										{},
									),
						  }
						: null,
					armor.static.dodgeModifier
						? {
								dodge: armor.static.dodgeModifier,
						  }
						: null,
					armor.static.healthModifier
						? {
								hitPoints: armor.static.healthModifier,
						  }
						: null,
				],
			});
		});
		// Equipment Mods
		(this.equipment || []).filter(Boolean).forEach((eq) => {
			this.addModifier(eq.static.prototypeModifier || null);
		});
		// Player and Faction Mods
		if (this.owner)
			this.owner
				.getUnitModifiers(this.getChassis()?.static?.type || null)
				.forEach((mod) => this.addModifier(mod));
		return this;
	}

	protected generateCaption() {
		let t: string[] = [];
		if (this.shields && this.shields[0])
			t = t.concat(
				this.shields[0].static.caption
					.trim()
					.split(' ')
					.filter((s) => !s.match(/shield|large|medium|small|giant/is) && s.length > 2)[0] || '',
			);
		if (this.armor && this.armor[0])
			t = t.concat(
				this.armor[0].static.caption
					.trim()
					.split(' ')
					.filter((s) => !s.match(/armor|large|medium|small|giant/is) && s.length > 2)[0] || '',
			);
		if (this.weapons && this.weapons[0])
			t = t.concat(this.weapons[0].static.caption.replace(/heavy/gi, '').trim().replace(/\s.*$/is, ''));
		if (this.equipment && this.equipment[0])
			t = t.concat(this.equipment[0].static.caption.trim().replace(/\s.*$/is, ''));
		if (this.chassis)
			t = t.concat(
				this.chassis.static.caption
					.split(' ')
					.filter((v) => v.length > 2)
					.map((v) => v.replace(/['"]|makeshift|support|science/gi, '')),
			);
		return (this.caption = _.uniq(
			t
				.map((v) => {
					const t = v.trim();
					if (t.match(/^[A-Z\s-]+$/)) return t;
					else return t.toLocaleLowerCase();
				})
				.filter((v) => !['the', 'a', 'over', 'at', 'elite'].includes(v)),
		)
			.filter(Boolean)
			.join(' '));
	}

	protected reassemble() {
		if (!this.chassis) return this.reset();
		this.__reassembling = true;
		const chassis = this.chassis.static.id;
		const armors = this.chassis.static.armorSlots.map((v, ix) =>
			this.armor[ix] ? this.armor[ix].static.id : null,
		);
		const shields = this.chassis.static.shieldSlots.map((v, ix) =>
			this.shields[ix] ? this.shields[ix].static.id : null,
		);
		const weapons = this.chassis.static.weaponSlots.map((v, ix) =>
			this.weapons[ix] ? this.weapons[ix].static.id : null,
		);
		const equipments = this.equipment.filter(Boolean).map((v) => v.static.id);
		this.reset();
		this.setChassis(chassis);
		equipments.forEach((equipment, slot) => this.setEquipment(slot, equipment));
		armors.forEach((armor, slot) => this.setArmor(slot, armor));
		shields.forEach((shield, slot) => this.setShield(slot, shield));
		weapons.forEach((weapon, slot) => this.setWeapon(slot, weapon));
		this.__reassembling = false;
		return this;
	}

	protected getCostModifiers() {
		return (this.modifiers.chassis || []).flatMap((chmod) => chmod.baseCost || null).filter(Boolean);
	}

	protected getCosts(): number {
		let cost = 0;
		if (this.chassis) cost += this.chassis.baseCost;
		this.armor
			.filter(Boolean)
			.forEach(
				(a) =>
					(cost += BasicMaths.applyModifier(a.baseCost, ..._.pluck(this.modifiers?.armor || [], 'baseCost'))),
			);
		this.weapons.filter(Boolean).forEach(
			(a) =>
				(cost += BasicMaths.applyModifier(
					a.baseCost,
					...(this.modifiers?.weapon || [])
						.map((v) => v[a.meta?.group?.id] || v.default || null)
						.filter(Boolean)
						.map((v) => v.baseCost),
				)),
		);
		this.shields
			.filter(Boolean)
			.forEach(
				(a) =>
					(cost += BasicMaths.applyModifier(
						a.baseCost,
						..._.pluck(this.modifiers?.shield || [], 'baseCost'),
					)),
			);
		this.equipment.filter(Boolean).forEach((a) => (cost += a.baseCost));
		this.baseCost = BasicMaths.applyModifier(cost, ...this.getCostModifiers());
		return this.baseCost;
	}
}

export default UnitPrototype;

import { SerializableFactory } from '../core/Serializable';
import { DIInjectableCollectibles } from '../core/DI/injections';
import { IChassis } from '../technologies/types/chassis';
import { IPrototype, IPrototypeFactory, TPrototypeDynamicProps, TPrototypeProps, TSerializedPrototype } from './types';
import { UnitPrototype } from './UnitPrototype';

const prototypeHashMap: Record<keyof TPrototypeDynamicProps, keyof IPrototype> = {
	chassis: 'setChassis',
	armor: 'setArmor',
	equipment: 'setEquipment',
	shields: 'setShield',
	weapons: 'setWeapon',
};
const prototypeHashOrder: Array<keyof typeof prototypeHashMap> = [
	'chassis',
	'armor',
	'equipment',
	'shields',
	'weapons',
];

const getHashSubLength = (chassis: IChassis, key: keyof typeof prototypeHashMap): number =>
	key === 'chassis'
		? 1
		: (
				{
					armor: chassis?.static?.armorSlots?.length || 0,
					weapons: chassis?.static?.weaponSlots?.length || 0,
					shields: chassis?.static?.shieldSlots?.length || 0,
					equipment: chassis?.equipmentSlots || 0,
				} as Record<keyof TPrototypeDynamicProps, number>
		  )[key];
const assignToPrototype = (prototype: IPrototype, key: keyof typeof prototypeHashMap) =>
	({
		armor: (id: number = null, indexOffset = 0) => prototype.setArmor(indexOffset, id),
		equipment: (id: number = null, indexOffset = 0) => prototype.setEquipment(indexOffset, id),
		shields: (id: number = null, indexOffset = 0) => prototype.setShield(indexOffset, id),
		weapons: (id: number = null, indexOffset = 0) => prototype.setWeapon(indexOffset, id),
		chassis: (id: number = null, indexOffset = 0) => prototype.setChassis(id),
	}[key]);

const prototypeHashPrefix = 'P';
const prototypeHashDelimiter = ':';

export class PrototypeManager
	extends SerializableFactory<TPrototypeProps, TSerializedPrototype, IPrototype>
	implements IPrototypeFactory
{
	public instantiate(props: Partial<TPrototypeProps>) {
		const c = this.__instantiateSerializable(props, UnitPrototype);
		return c;
	}

	public unserialize(t: TSerializedPrototype) {
		const c = this.instantiate(t);
		c.setInstanceId(t.instanceId);
		c.chassis = this.getProvider(DIInjectableCollectibles.chassis).unserialize(t.chassis);
		c.armor = t.armor.map((a) => this.getProvider(DIInjectableCollectibles.armor).unserialize(a));
		c.weapons = t.weapons.map((w) => this.getProvider(DIInjectableCollectibles.weapon).unserialize(w));
		c.shields = t.shields.map((s) => this.getProvider(DIInjectableCollectibles.shield).unserialize(s));
		c.equipment = t.equipment.map((e) => this.getProvider(DIInjectableCollectibles.equipment).unserialize(e));
		return c.update(true);
	}

	createFromHash(hash: string, props: Partial<TPrototypeProps> = {}): IPrototype {
		const parseValue = (id: string) => parseInt(id, 36);
		const c = this.instantiate(props);
		if (!hash || !hash.match(new RegExp(`^${prototypeHashPrefix}([${prototypeHashDelimiter}]\\w+)*$`, 'is')))
			return c;
		try {
			let prototypeHashIndex = 0;
			const items = hash.split(prototypeHashDelimiter).slice(1).map(parseValue);
			while (prototypeHashIndex < prototypeHashOrder.length) {
				const hashKey = prototypeHashOrder[prototypeHashIndex];
				const loopLength = getHashSubLength(c.getChassis(), hashKey);
				for (let i = 0; i < loopLength; i++) assignToPrototype(c, hashKey)(items.shift() || null, i);
				prototypeHashIndex++;
			}
			return c.update(true);
		} catch (e) {
			console.error(e);
			return this.instantiate(props);
		}
	}

	getHash(prototype: IPrototype): string {
		const convertValue = (id: number | string) => parseInt(`${id}`, 10).toString(36).toUpperCase();
		if (!prototype) return null;
		const hash = [prototypeHashPrefix];
		const chassis = prototype.getChassis();
		if (!chassis) return null;
		prototypeHashOrder.forEach((hashKey) => {
			const hashLength = getHashSubLength(chassis, hashKey);
			const items = prototype?.[hashKey];
			for (let i = 0; i < hashLength; i++) {
				const item = Array.isArray(items) ? items[i] : items;
				hash.push(convertValue(item ? item.static.id : 0));
			}
		});
		return hash.join(prototypeHashDelimiter);
	}
}

export default PrototypeManager;

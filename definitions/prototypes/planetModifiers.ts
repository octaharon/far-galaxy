import BasicMaths from '../maths/BasicMaths';
import { ArithmeticPrecision } from '../maths/constants';
import { TPlanetSurfaceProps } from '../space/Planets';
import UnitAttack from '../tactical/damage/attack';
import Damage from '../tactical/damage/damage';
import Weapons from '../technologies/types/weapons';
import { IPrototypeModifier } from './types';

export function getPlanetPrototypeModifier(planet: TPlanetSurfaceProps): IPrototypeModifier {
	const c = {} as IPrototypeModifier;
	c.unitModifier = {
		abilityCooldownModifier: [
			{
				factor: BasicMaths.lerp({
					min: 1.2,
					max: 0.8,
					x: planet.warpFactor,
				}),
				minGuard: 1,
			},
		],
		abilityRangeModifier: [
			{
				factor: BasicMaths.lerp({
					min: 1.25,
					max: 0.75,
					x: planet.magneticField,
				}),
				minGuard: ArithmeticPrecision,
			},
		],
		statusDurationModifier: [
			{
				bias: BasicMaths.lerp({
					min: -2,
					max: 2,
					x: planet.warpFactor,
				}),
				minGuard: 1,
				min: 1,
			},
		],
	};
	c.chassis = [
		{
			scanRange: {
				factor: BasicMaths.lerp({
					min: 1,
					max: 0.5,
					x: planet.radiationLevel,
				}),
			},
			speed: {
				factor: BasicMaths.lerp({
					max: 0.8,
					min: 1.2,
					x: planet.warpFactor,
				}),
				bias: BasicMaths.lerp({
					min: 1,
					max: -1,
					x: planet.atmosphericDensity,
				}),
			},
			dodge: {
				[UnitAttack.deliveryType.surface]: {
					bias: BasicMaths.lerp({
						min: -0.5,
						max: 0.5,
						x: planet.atmosphericDensity,
					}),
				},
				[UnitAttack.deliveryType.cloud]: {
					bias: BasicMaths.lerp({
						min: -0.5,
						max: 0.5,
						x: planet.atmosphericDensity,
					}),
				},
			},
		},
	];

	const BeamModifier: Weapons.TWeaponModifier = {
		damageModifier: {
			factor: BasicMaths.lerp({
				min: 1.2,
				max: 0.8,
				x: planet.atmosphericDensity,
			}),
		},
		baseAccuracy: {
			bias: BasicMaths.lerp({
				min: 0.2,
				max: -0.2,
				x: planet.magneticField,
			}),
		},
	};

	const BallisticModifier: Weapons.TWeaponModifier = {
		baseAccuracy: {
			bias: BasicMaths.lerp({
				min: 0.2,
				max: -0.2,
				x: planet.spinFactor,
			}),
		},
		baseRange: {
			bias: BasicMaths.lerp({
				min: 2,
				max: -2,
				x: planet.atmosphericDensity,
			}),
			factor: BasicMaths.lerp({
				min: 1.2,
				max: 0.8,
				x: planet.gravity,
			}),
			minGuard: 1,
			min: 1,
		},
	};
	const RailgunModifier: Weapons.TWeaponModifier = {
		baseRange: {
			bias: BasicMaths.lerp({
				min: 2,
				max: -2,
				x: planet.atmosphericDensity,
			}),
			minGuard: 1,
			min: 1,
		},
	};
	const MissileAndAerialModifier: Weapons.TWeaponModifier = {
		baseRange: {
			bias: BasicMaths.lerp({
				min: 1,
				max: -1,
				x: planet.atmosphericDensity,
			}),
			factor: BasicMaths.lerp({
				min: 1.2,
				max: 0.8,
				x: planet.gravity,
			}),
			minGuard: 1,
			min: 1,
		},
	};

	const BombsModifier: Weapons.TWeaponModifier = {
		baseAccuracy: {
			bias: BasicMaths.lerp({
				min: 0.2,
				max: -0.2,
				x: planet.spinFactor,
			}),
		},
		baseRange: {
			factor: BasicMaths.lerp({
				min: 1.2,
				max: 0.8,
				x: planet.gravity,
			}),
		},
	};

	c.weapon = [
		{
			default: {
				aoeRadiusModifier: {
					factor: BasicMaths.lerp({
						min: 1.5,
						max: 0.5,
						x: planet.atmosphericDensity,
					}),
				},
				baseRate: {
					factor: BasicMaths.lerp({
						min: 0.9,
						max: 1.1,
						x: planet.warpFactor,
					}),
				},
			},
			[Weapons.TWeaponGroupType.Rockets]: MissileAndAerialModifier,
			[Weapons.TWeaponGroupType.Missiles]: MissileAndAerialModifier,
			[Weapons.TWeaponGroupType.Cruise]: MissileAndAerialModifier,
			[Weapons.TWeaponGroupType.AntiAir]: MissileAndAerialModifier,
			[Weapons.TWeaponGroupType.Guns]: BallisticModifier,
			[Weapons.TWeaponGroupType.Cannons]: BallisticModifier,
			[Weapons.TWeaponGroupType.Launchers]: BallisticModifier,
			[Weapons.TWeaponGroupType.Beam]: BeamModifier,
			[Weapons.TWeaponGroupType.Lasers]: BeamModifier,
			[Weapons.TWeaponGroupType.Bombs]: BombsModifier,
			[Weapons.TWeaponGroupType.Plasma]: RailgunModifier,
			[Weapons.TWeaponGroupType.Railguns]: RailgunModifier,
		},
	];
	c.armor = [
		{
			[Damage.damageType.heat]: {
				bias: BasicMaths.lerp({
					min: 0,
					max: 10,
					x: -0.4 + planet.temperature * 1.4,
				}),
			},
		},
	];
	return c;
}

declare module '*.scss' {
	const content: { [className: string]: string };
	export = content;
}
declare module '*.png' {
	const content: any;
	export default content;
}
declare module '*.svg' {
	const content: any;
	export default content;
}
declare module '*.jpeg' {
	const content: any;
	export default content;
}
declare module '*.jpg' {
	const content: any;
	export default content;
}

declare module '*.webp' {
	const content: any;
	export default content;
}

declare module '*.glb' {
	const content: any;
	export default content;
}

declare module '*.gltf' {
	const content: any;
	export default content;
}

declare module '*.dae' {
	const content: any;
	export default content;
}

declare module '*.ogg' {
	const content: any;
	export default content;
}

declare module '*.mp3' {
	const content: any;
	export default content;
}

declare module '*.mp4' {
	const content: any;
	export default content;
}

declare module '*.avif' {
	const content: any;
	export default content;
}

declare type GenericArrayType<T> = T extends Array<infer X> ? X : null;
declare type ClassConstructor<T> = new (...args: any) => T;
declare type Abstract<T> = CallableFunction & { prototype: T };
declare type Class<T> = Abstract<T> | ClassConstructor<T>;

declare interface IBounds {
	max: number;
	min: number;
}

declare interface IModifier extends Partial<IBounds> {
	factor?: number; // multiplication factor
	bias?: number; // addition factor
	minGuard?: number; // if the incoming value is less than this, the modifier is not applied
	maxGuard?: number; // if the incoming value is more than this, the modifier is not applied
}

declare type Dictionary<T = any> = Record<string, T>;

declare type IConstructorOf<ClassName> = new (...args: any[]) => ClassName;
declare type ValueOf<T> = T[keyof T];

declare type Proxy<T, U> = {
	[P in keyof T]?: U;
};

declare type EnumProxy<T extends string, U> = {
	[P in T]?: U;
};

declare type ObjectKeys<T> = T extends Record<infer R, any>
	? R[]
	: T extends EnumProxy<infer R, any>
	? R[]
	: T extends object
	? Array<keyof T>
	: T extends number
	? []
	: T extends any[] | string
	? string[]
	: never;

declare interface ObjectConstructor {
	keys<T>(o: T): ObjectKeys<T>;
	entries<T, K extends string | number | symbol>(o: Record<K, T>): Array<[K, T]>;
}

declare type WithDefault<T> = T | 'default';

declare type EnumProxyWithDefault<T extends string, U> = EnumProxy<T, U> & { default?: U };

declare type DecoratorOf<TModel> = { [P in keyof TModel]?: TModel[P] };
declare type PartialDecoratorOf<TModel, K extends keyof TModel> = { [P in K]?: TModel[P] };
declare type ModifierOf<TModel, K extends keyof TModel> = { [P in K]?: IModifier };

declare type Join<K, P> = K extends string | number
	? P extends string | number
		? `${K}${'' extends P ? '' : '.'}${P}`
		: never
	: never;

type Prev = [never, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, ...Array<0>];

declare type Paths<T, D extends number = 10> = [D] extends [never]
	? never
	: T extends object
	? {
			[K in keyof T]-?: K extends string | number ? `${K}` | Join<K, Paths<T[K], Prev[D]>> : never;
	  }[keyof T]
	: '';

declare type Leaves<T, D extends number = 10> = [D] extends [never]
	? never
	: T extends object
	? { [K in keyof T]-?: Join<K, Leaves<T[K], Prev[D]>> }[keyof T]
	: '';

declare interface IWithInstanceID {
	instanceId: string;
}

declare interface IWithInstanceFactory<ObjectType> {
	factory?: ClassConstructor<ObjectType>;
}

declare interface ISingletonStatic<ObjectType> {
	i(): ObjectType;
}

declare module 'react-sticky-mouse-tooltip' {
	export default React.Component;
}

declare module 'image-outline' {
	type TImageOutline = (path: string, cb: (err?: Error, polygon?: Array<{ x: number; y: number }>) => any) => void;
	const getImageOutline: TImageOutline;
	export default getImageOutline;
}

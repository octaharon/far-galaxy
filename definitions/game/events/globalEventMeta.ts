import InterfaceTypes from '../../core/InterfaceTypes';
import Players from '../../player/types';
import Abilities from '../../tactical/abilities/types';
import EffectEvents from '../../tactical/statuses/effectEvents';
import Units from '../../tactical/units/types';
import { IMapCell } from '../../tactical/map';
import Salvage from '../../tactical/salvage/types';
import StatusEffectMeta from '../../tactical/statuses/statusEffectMeta';
import GameData from '../types';

export namespace GameEvents {
	export enum TGameEvents {
		gameStart = 'game_event_start',
		gameStageTransition = 'game_event_stage_transition',
		gameEnd = 'game_event_end',
		unitDeployed = 'game_event_unit_deployed',
		unitKilled = 'game_event_unit_killed',
		unitAttacked = 'game_event_unit_attacked',
		unitAttacks = 'game_event_unit_attacks',
		unitMoves = 'game_event_unit_moves',
		baseAttacked = 'game_event_base_attacked',
		playerRetreats = 'game_event_player_retreats',
		playerWins = 'game_event_player_wins',
		playerBuildsUnit = 'game_event_player_builds_unit',
		playerCasts = 'game_event_player_casts',
		playerSpell = 'game_event_player_spell',
		mapSpell = 'game_event_map_spell',
		unitSpell = 'game_event_unit_spell',
		unitCast = 'game_event_unit_cast',
	}

	export type TGameEventMeta<T extends TGameEvents> = EffectEvents.TGlobalEffectMeta & T extends TGameEvents.gameStart
		? {}
		: T extends TGameEvents.gameEnd
		? {}
		: T extends TGameEvents.gameStageTransition
		? {
				stage: GameData.TTurnStages;
				turnIndex: number;
		  }
		: T extends TGameEvents.unitDeployed
		? {
				unit: Units.IUnit;
		  }
		: T extends TGameEvents.unitKilled
		? {
				unit: Units.IUnit;
				salvage: Salvage.ISalvage;
				source: StatusEffectMeta.TStatusEffectTarget;
		  }
		: T extends TGameEvents.baseAttacked
		? {
				player: Players.IPlayer;
				damage: number;
				source: StatusEffectMeta.TStatusEffectTarget;
		  }
		: T extends TGameEvents.unitAttacked
		? {
				unit: Units.IUnit;
				source: StatusEffectMeta.TStatusEffectTarget;
		  }
		: T extends TGameEvents.unitAttacks
		? {
				unit: Units.IUnit;
				target: Units.IUnit;
		  }
		: T extends TGameEvents.unitMoves
		? {
				unit: Units.IUnit;
				from: InterfaceTypes.IVector;
				to: InterfaceTypes.IVector;
		  }
		: T extends TGameEvents.playerBuildsUnit
		? {
				unit: Units.IUnit;
		  }
		: T extends TGameEvents.playerRetreats
		? {
				player: Players.IPlayer;
		  }
		: T extends TGameEvents.playerWins
		? {
				player: Players.IPlayer;
		  }
		: T extends TGameEvents.playerCasts
		? {
				player: Players.IPlayer;
				target: StatusEffectMeta.TStatusEffectTarget;
				ability: Abilities.TAbilityOption;
		  }
		: T extends TGameEvents.playerSpell
		? {
				player: Players.IPlayer;
				source: StatusEffectMeta.TStatusEffectTarget;
				ability: Abilities.TAbilityOption;
		  }
		: T extends TGameEvents.unitCast
		? {
				unit: Units.IUnit;
				target: StatusEffectMeta.TStatusEffectTarget;
				ability: Abilities.TAbilityOption;
		  }
		: T extends TGameEvents.unitSpell
		? {
				unit: Units.IUnit;
				source: StatusEffectMeta.TStatusEffectTarget;
				ability: Abilities.TAbilityOption;
		  }
		: T extends TGameEvents.mapSpell
		? {
				source: StatusEffectMeta.TStatusEffectTarget;
				location: IMapCell;
				ability: Abilities.IAbility<Abilities.abilityTargetTypes.map>;
		  }
		: never;

	export type TGameEventDefinition<T extends TGameEvents> = {
		event: T;
		meta: TGameEventMeta<T>;
	};

	export function isGameEventDefinition<T extends TGameEvents>(
		event: TGameEventDefinition<any>,
		type: T,
	): event is TGameEventDefinition<T> {
		return event.event === type;
	}
}

export default GameEvents;

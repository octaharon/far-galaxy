import EffectEvents from '../../tactical/statuses/effectEvents';
import StatusEffectMeta from '../../tactical/statuses/statusEffectMeta';
import { DIInjectableCollectibles } from '../../core/DI/injections';
import { DIContainerDependency } from '../../core/DI/types';
import GameData from '../types';
import GameEvents from './globalEventMeta';
import TTurnStages = GameData.TTurnStages;

const turnEventsMapping: Partial<Record<TTurnStages, Record<StatusEffectMeta.effectAgentTypes, any>>> = {
	[GameData.TTurnStages.turnEndStage]: {
		[StatusEffectMeta.effectAgentTypes.unit]: EffectEvents.TUnitEffectEvents.turnEnd,
		[StatusEffectMeta.effectAgentTypes.player]: EffectEvents.TPlayerEffectEvents.turnEnd,
		[StatusEffectMeta.effectAgentTypes.map]: EffectEvents.TMapEffectEvents.turnEnd,
	},
	[GameData.TTurnStages.turnStartStage]: {
		[StatusEffectMeta.effectAgentTypes.unit]: EffectEvents.TUnitEffectEvents.turnStart,
		[StatusEffectMeta.effectAgentTypes.player]: EffectEvents.TPlayerEffectEvents.turnStart,
		[StatusEffectMeta.effectAgentTypes.map]: EffectEvents.TMapEffectEvents.turnStart,
	},
};

export default class EventBus extends DIContainerDependency {
	protected __instance: EventBus;

	protected constructor() {
		super();
	}

	public i(): EventBus {
		if (!this.__instance) this.__instance = new EventBus();
		return this.__instance;
	}

	public propagateEvent(e: GameEvents.TGameEventDefinition<any>, game: GameData.IGameContainer): EventBus {
		switch (e.event) {
			case GameEvents.TGameEvents.gameStageTransition:
				return this.reflectStageTransitionEvent(e.event, game);
			// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/4
			default:
				return this;
		}
	}

	/**
	 * Map a Stage Transition to Unit Event
	 *
	 * @param gameEvent
	 * @param game
	 */
	protected reflectStageTransitionEvent(
		gameEvent: GameEvents.TGameEventDefinition<GameEvents.TGameEvents.gameStageTransition>,
		game: GameData.IGameContainer,
	) {
		if (gameEvent.meta.stage === GameData.TTurnStages.turnStartStage) {
			this.getProvider(DIInjectableCollectibles.effect).currentTurn = gameEvent.meta.turnIndex;
		}
		const state: EffectEvents.TGlobalEffectMeta &
			EffectEvents.TMapContextEffectMeta &
			EffectEvents.TUnitContextEffectMeta = {
			phaseIndex: 0,
			phase: null,
			map: game.map,
			...gameEvent.meta,
		};
		switch (true) {
			case Object.keys(turnEventsMapping).includes(gameEvent.meta.stage):
				game.map.getDeployedUnits().forEach((unit) =>
					unit.callEffectEvent({
						type: StatusEffectMeta.effectAgentTypes.unit,
						event: turnEventsMapping[gameEvent.meta.stage as keyof typeof turnEventsMapping][
							StatusEffectMeta.effectAgentTypes.unit
						],
						...state,
					}),
				);
				game.map.getMapCells().forEach((cell) =>
					cell.callEffectEvent({
						type: StatusEffectMeta.effectAgentTypes.map,
						event: turnEventsMapping[gameEvent.meta.stage as keyof typeof turnEventsMapping][
							StatusEffectMeta.effectAgentTypes.map
						],
						...state,
					}),
				);
				Object.values(game.bases).forEach((base) =>
					base.callEffectEvent({
						type: StatusEffectMeta.effectAgentTypes.player,
						event: turnEventsMapping[gameEvent.meta.stage as keyof typeof turnEventsMapping][
							StatusEffectMeta.effectAgentTypes.player
						],
						...state,
					}),
				);
				break;
			default:
		}
		return this;
	}
}

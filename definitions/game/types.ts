import { ISerializable } from '../core/Serializable';
import { IPlayerBase, TSerializedPlayerBase } from '../orbital/base/types';
import { TPlayerId } from '../player/playerId';
import GameStats from '../stats/types';
import { ICombatMap, TSerializedCombatMap } from '../tactical/map';

export namespace GameData {
	export enum TTurnStages {
		turnStartStage = 'game_turn_start',
		deployStage = 'game_stage_deploy',
		planningStage = 'game_stage_planning',
		executionStage = 'game_stage_execution',
		turnEndStage = 'game_turn_end',
	}

	export interface TGameScalarProps {
		turnIndex: number;
		stage: TTurnStages;
		stats: GameStats.TTurnStats[];
		drawVotes: Record<TPlayerId, boolean>;
	}

	export interface TGameContainerProps extends TGameScalarProps {
		bases: Record<TPlayerId, IPlayerBase>;
		map: ICombatMap;
	}

	export type TSerializedGameContainer = IWithInstanceID &
		TGameScalarProps & {
			bases: Record<TPlayerId, TSerializedPlayerBase[]>;
			map: TSerializedCombatMap;
		};

	export type TGameContainerInstantiateProps = {
		chosenAbilities: Record<TPlayerId, number[]>;
	};

	export interface IGameContainer
		extends TGameContainerProps,
			ISerializable<TGameContainerProps, TSerializedGameContainer> {}
}

export default GameData;

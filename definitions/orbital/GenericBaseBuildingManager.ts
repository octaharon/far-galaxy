import DIContainer from '../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../core/DI/injections';
import { IEstablishment, IEstablishmentProvider } from './establishments/types';
import { IFacility, IFacilityProvider } from './facilities/types';
import { IFactory, IFactoryProvider } from './factories/types';
import { IBaseBuildingManager, TBaseBuildingProps, TBaseBuildingType, TTypedBaseBuilding } from './types';

export const BUILDING_INSTANTIATE_ERROR_NO_OWNERS = 'A Building with no owners detected';

class GenericBaseBuildingManager implements IBaseBuildingManager {
	public postStore<T extends TTypedBaseBuilding>(b: T): T {
		if (!b.playerId || !b.baseId) throw new Error(BUILDING_INSTANTIATE_ERROR_NO_OWNERS);
		b.setOwner(b.playerId);
		b.setBase(b.baseId);
		return b;
	}

	public isFactory(b: any): b is IFactory {
		return b?.static?.type === TBaseBuildingType.factory;
	}

	public isFacility(b: any): b is IFacility {
		return b?.static?.type === TBaseBuildingType.facility;
	}

	public isEstablishment(b: any): b is IEstablishment {
		return b?.static?.type === TBaseBuildingType.establishment;
	}

	public getProvider(t: TBaseBuildingType.factory): IFactoryProvider;
	public getProvider(t: TBaseBuildingType.facility): IFacilityProvider;
	public getProvider(t: TBaseBuildingType.establishment): IEstablishmentProvider;
	public getProvider<K extends TBaseBuildingType = TBaseBuildingType>(
		g: K,
	): K extends TBaseBuildingType.factory
		? IFactoryProvider
		: K extends TBaseBuildingType.facility
		? IFacilityProvider
		: K extends TBaseBuildingType.establishment
		? IEstablishmentProvider
		: never;

	public getProvider<K extends TBaseBuildingType = TBaseBuildingType>(g: K) {
		switch (g) {
			case TBaseBuildingType.factory:
				return DIContainer.getProvider(DIInjectableCollectibles.factory);
			case TBaseBuildingType.facility:
				return DIContainer.getProvider(DIInjectableCollectibles.facility);
			case TBaseBuildingType.establishment:
				return DIContainer.getProvider(DIInjectableCollectibles.establishment);
			default:
				return null;
		}
	}

	public preStore<T extends Partial<TBaseBuildingProps>>(b: T): T {
		if (!b.playerId || !b.baseId) throw new Error(BUILDING_INSTANTIATE_ERROR_NO_OWNERS);
		return b;
	}
}

export const BuildingManager = new GenericBaseBuildingManager();
export default BuildingManager;

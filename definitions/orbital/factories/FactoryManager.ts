import { CollectibleFactory } from '../../core/Collectible';
import { DIInjectableSerializables } from '../../core/DI/injections';
import BasicMaths from '../../maths/BasicMaths';
import BuildingManager from '../GenericBaseBuildingManager';
import FactoryList from './index';
import {
	getTierProductionSpeed,
	IFactory,
	IFactoryBuildingStatic,
	IFactoryProvider,
	TFactoryProps,
	TSerializedFactory,
} from './types';

export const defaultFactoryProps: Partial<TFactoryProps> = {
	disabled: true,
	powered: false,
	currentProductionSlot: 0,
	currentProductionStock: 0,
};

export class FactoryManager
	extends CollectibleFactory<TFactoryProps, TSerializedFactory, IFactory, IFactoryBuildingStatic>
	implements IFactoryProvider
{
	public instantiate(props: Partial<TFactoryProps> = {}, id: number) {
		return this.__instantiateCollectible(
			{
				...defaultFactoryProps,
				...props,
			},
			id,
		).init();
	}

	public store(o: IFactory, callback?: (a: IFactory) => void) {
		super.store(BuildingManager.preStore(o), (v) => callback(BuildingManager.postStore(v)));
		return this;
	}

	public getBaseProductionSpeed(id: number) {
		return getTierProductionSpeed(this.get(id)?.buildingTier || 0);
	}

	public fill() {
		this.__fillItems(FactoryList);
		return this;
	}
}

export default FactoryManager;

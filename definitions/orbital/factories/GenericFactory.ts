import { DIEntityDescriptors, DIInjectableCollectibles, DIInjectableSerializables } from '../../core/DI/injections';
import $$ from '../../maths/BasicMaths';
import { IPrototype, IPrototypeModifier } from '../../prototypes/types';
import Units from '../../tactical/units/types';
import UnitChassis from '../../technologies/types/chassis';
import { GenericBaseBuilding } from '../GenericBaseBuilding';
import { TBaseBuildingType } from '../types';
import {
	BASE_PRODUCTION_SPEED,
	FACTORY_MIN_PRODUCTION_COST,
	getTierProductionSpeed,
	IFactory,
	IFactoryBuildingStatic,
	ORBITAL_PRODUCTION_SWITCH_PENALTY,
	TChassisProductionModifier,
	TFactoryProps,
	TFactoryUpgrade,
	TIER_PRODUCTION_SPEED,
	TMovementProductionModifier,
	TSerializedFactory,
} from './types';

export class GenericFactory
	extends GenericBaseBuilding<
		TBaseBuildingType.factory,
		TFactoryProps,
		TSerializedFactory,
		IFactoryBuildingStatic,
		TFactoryUpgrade
	>
	implements IFactory
{
	public static readonly prototypeSlots: number = 1;
	public static readonly type: TBaseBuildingType.factory = TBaseBuildingType.factory;
	public static readonly id: number = 0;
	public static readonly productionType: UnitChassis.chassisClass[] = [UnitChassis.chassisClass.scout];
	public static readonly productionCapacity: IModifier = {
		bias: 5,
	};
	public static readonly productionModifierByChassis?: TChassisProductionModifier = {};
	public static readonly upgrades: TFactoryUpgrade[] = [
		{
			caption: 'Eco friendly',
			energyMaintenanceCost: {
				bias: -3,
				min: 5,
				minGuard: 5,
			},
			productionSpeed: {
				bias: -1,
				min: BASE_PRODUCTION_SPEED,
				minGuard: BASE_PRODUCTION_SPEED,
			},
		},
		{
			caption: 'Scalable production',
			maxProductionCost: {
				bias: 12,
			},
			energyMaintenanceCost: {
				bias: 2,
			},
		},
		{
			caption: 'Rapid prototyping',
			maxProductionCost: {
				bias: -8,
				min: FACTORY_MIN_PRODUCTION_COST,
				minGuard: FACTORY_MIN_PRODUCTION_COST,
			},
			productionSpeed: {
				bias: TIER_PRODUCTION_SPEED / 2,
			},
		},
	];
	public maxProductionCost: number;
	public productionSpeed: number;
	public currentProductionSlot: number;
	public productionPrototypes: string[]; // prototype instance IDs
	public currentProductionStock: number;

	public get baseProductionSpeed() {
		return getTierProductionSpeed(this.static.buildingTier);
	}

	public get currentProductionSpeed() {
		return $$.applyModifier(
			this.productionSpeed || this.baseProductionSpeed,
			(
				this.static.productionModifierByChassis?.[this.currentPrototype?.chassis?.static?.type] ||
				this.static.productionModifierByChassis?.default ||
				{}
			).productionSpeed || {},
			this.owner?.baseModifier?.productionSpeed,
		);
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.factory];
	}

	public get currentPrototype() {
		const p = this.getProvider(DIInjectableSerializables.prototype).find(
			this.productionPrototypes?.[this.currentProductionSlot],
		);
		if (!p) return null;
		return p;
	}

	public init() {
		if (!this.currentProductionStock) this.currentProductionStock = 0;
		if (!Number.isFinite(this.currentProductionSlot)) this.currentProductionSlot = null;
		if (!this.productionPrototypes) this.productionPrototypes = [];
		if (!Number.isFinite(this.maxProductionCost)) this.maxProductionCost = FACTORY_MIN_PRODUCTION_COST;
		if (!Number.isFinite(this.productionSpeed)) this.productionSpeed = this.baseProductionSpeed;
		return this;
	}

	public getProductionModifierForPrototype(prototype: IPrototype): IPrototypeModifier {
		if (!this.static.productionModifierByChassis) return null;
		const chType = prototype.chassis?.static?.type;
		return (
			(this.static.productionModifierByChassis[chType] || this.static.productionModifierByChassis.default)
				?.prototypeModifier ?? null
		);
	}

	public setPrototypeSlot(slot: number, prototype?: IPrototype) {
		if (slot <= 0 || slot > this.static.prototypeSlots) return this;
		slot--;
		if (this.productionPrototypes[slot])
			this.getProvider(DIInjectableSerializables.prototype).destroy(this.productionPrototypes[slot]);
		this.productionPrototypes[slot] = null;
		if (!prototype) return this;
		if (
			!this.static.productionType.includes(prototype.chassis.static.type) ||
			this.maxProductionCost < prototype.cost
		)
			return this;
		const newPrototype = this.getProvider(DIInjectableSerializables.prototype)
			.clone(prototype)
			.addModifier(this.getProductionModifierForPrototype(prototype));
		this.getProvider(DIInjectableSerializables.prototype).store(newPrototype);
		this.productionPrototypes[slot] = newPrototype.getInstanceId();
		return this;
	}

	public setProductionSlot(slot: number, lossModifier?: IModifier) {
		if (slot <= 0 || slot > this.static.prototypeSlots || slot === this.currentProductionSlot) return this;
		slot--;
		if (this.productionPrototypes[this.currentProductionSlot] && this.currentProductionStock) {
			let loss = this.currentProductionStock * ORBITAL_PRODUCTION_SWITCH_PENALTY;
			if (lossModifier) loss = $$.applyModifier(loss, lossModifier);
			this.currentProductionStock = Math.max(0, this.currentProductionStock - loss);
		}
		this.currentProductionSlot = slot;
		return this;
	}

	public serialize(): TSerializedFactory {
		return {
			...this.__serializeBuilding(),
			productionSpeed: this.productionSpeed,
			currentProductionStock: this.currentProductionStock,
			currentProductionSlot: this.currentProductionSlot,
			maxProductionCost: this.maxProductionCost,
			productionPrototypes: this.productionPrototypes,
		};
	}

	public productionCycle(): Units.IUnit {
		if (this.disabled) return null;
		const buildingPrototype = this.currentPrototype;
		const base = this.getProvider(DIInjectableSerializables.base).find(this.baseId);
		const productionSpeed = this.currentProductionSpeed;
		if (
			!buildingPrototype ||
			base.rawMaterials < productionSpeed ||
			base.unitsAtBay.length >= base.currentEconomy.bayCapacity
		) {
			this.disabled = true;
			return null;
		}
		base.rawMaterials -= productionSpeed;
		this.currentProductionStock += productionSpeed;
		if (this.currentProductionStock > buildingPrototype.cost) {
			this.currentProductionStock -= buildingPrototype.cost;
			this.currentProductionStock *= 1 - ORBITAL_PRODUCTION_SWITCH_PENALTY;
			return this.getProvider(DIInjectableSerializables.unit).createFromPrototype(buildingPrototype);
		}
		return null;
	}
}

export default GenericFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { BASE_PRODUCTION_SPEED } from '../types';

export class NexusFoundryFactory extends GenericFactory {
	public static readonly id: number = 302;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Nexus Foundry';
	public static readonly description: string = `A full-scale cybernetic plant with 3 conveyor lines and a huge assembly hall for massive units`;
	public static readonly prototypeSlots: number = 3;
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};
	public static readonly energyConsumptionModifier: IModifier = {
		factor: 0.95,
	};
	public static readonly materialOutputModifier: IModifier = {
		factor: 1.1,
	};
	public static resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.halogens]: {
			factor: 1.05,
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.combatdrone,
		UnitChassis.chassisClass.quadrapod,
		UnitChassis.chassisClass.fortress,
		UnitChassis.chassisClass.pointdefense,
		UnitChassis.chassisClass.science_vessel,
		UnitChassis.chassisClass.mecha,
	];

	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1550,
		[Resources.resourceId.ores]: 9000,
		[Resources.resourceId.helium]: 270,
		[Resources.resourceId.nitrogen]: 750,
		[Resources.resourceId.isotopes]: 300,
		[Resources.resourceId.halogens]: 200,
		[Resources.resourceId.proteins]: 160,
		[Resources.resourceId.minerals]: 600,
		[Resources.resourceId.hydrogen]: 1500,
	};
	public maxProductionCost = 220;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.3;
	public productionSpeed: number = this.baseProductionSpeed + BASE_PRODUCTION_SPEED;
}

export default NexusFoundryFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier } from '../types';

export class MachineryFactory extends GenericFactory {
	public static readonly id: number = 204;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Machinery';
	public static readonly description: string = `4 workshops full of hardworking people, ready to quickly adapt and rapidly assemble any machinery, if it's not too complex. And the quality might differ. But hey, it's cheap!`;
	public static readonly prototypeSlots: number = 4;
	public static readonly maxRawMaterials: IModifier = {
		bias: 40,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.ores]: {
			factor: 1.03,
		},
		[Resources.resourceId.carbon]: {
			factor: 1.05,
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.lav,
		UnitChassis.chassisClass.scout,
		UnitChassis.chassisClass.sup_vehicle,
		UnitChassis.chassisClass.tank,
	];
	public static readonly productionModifierByChassis?: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				unitModifier: {
					statusDurationModifier: [
						{
							bias: -1,
							minGuard: 1,
							min: 1,
						},
					],
				},
				chassis: [
					{
						baseCost: {
							factor: 0.85,
						},
						hitPoints: {
							bias: -5,
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2300,
		[Resources.resourceId.minerals]: 3900,
		[Resources.resourceId.ores]: 2700,
		[Resources.resourceId.oxygen]: 240,
		[Resources.resourceId.nitrogen]: 250,
	};
	public maxProductionCost = 140;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.9;
}

export default MachineryFactory;

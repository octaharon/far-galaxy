import UnitChassis    from '../../../technologies/types/chassis';
import { Resources }  from '../../../world/resources/types';
import GenericFactory from '../GenericFactory';

export class RoboticsBayFactory extends GenericFactory {
	public static readonly id: number = 300;
	public static readonly buildingTier: number = 1;
	public static readonly caption: string = 'Robotics bay';
	public static readonly description: string = `An advanced production facility equipped with AI labs and specialized in building robotic units`;
	public static readonly prototypeSlots: number = 1;
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.combatdrone,
		UnitChassis.chassisClass.droid,
		UnitChassis.chassisClass.science_vessel,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 4000,
		[Resources.resourceId.ores]: 1800,
		[Resources.resourceId.oxygen]: 200,
		[Resources.resourceId.isotopes]: 100,
		[Resources.resourceId.hydrogen]: 2500,
	};
	public maxProductionCost = 90;
	public energyMaintenanceCost = 12;
}

export default RoboticsBayFactory;

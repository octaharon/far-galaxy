import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class NavalEngineeringCorpus extends GenericFactory {
	public static readonly id: number = 402;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Naval engineering corpus';
	public static readonly description: string = `The biggest and the most advanced shipbuilding facility`;
	public static readonly prototypeSlots: number = 2;
	public static readonly materialOutputModifier: IModifier = {
		factor: 1.05,
	};
	public static readonly engineeringOutputModifier: IModifier = {
		factor: 1.05,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.corvette,
		UnitChassis.chassisClass.sup_ship,
		UnitChassis.chassisClass.manofwar,
		UnitChassis.chassisClass.destroyer,
		UnitChassis.chassisClass.battleship,
		UnitChassis.chassisClass.cruiser,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 5000,
		[Resources.resourceId.ores]: 2200,
		[Resources.resourceId.oxygen]: 1450,
		[Resources.resourceId.minerals]: 1450,
		[Resources.resourceId.halogens]: 440,
		[Resources.resourceId.helium]: 1050,
		[Resources.resourceId.hydrogen]: 3300,
	};
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				chassis: [
					{
						scanRange: { bias: 1 },
						dodge: {
							default: { bias: 0.1 },
						},
					},
				],
				unitModifier: {
					abilityPowerModifier: [
						{
							bias: 0.5,
						},
					],
				},
			},
		},
	};
	public maxProductionCost = 230;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.25;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED;
}

export default NavalEngineeringCorpus;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { BASE_PRODUCTION_SPEED, TChassisProductionModifier } from '../types';

export class TitanForgeFactory extends GenericFactory {
	public static readonly id: number = 601;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Titan Forge';
	public static readonly maxRawMaterials: IModifier = {
		bias: 100,
	};
	public static readonly description: string = `The most advanced production complex, purposed specifically for colossal war machines`;
	public static readonly prototypeSlots: number = 2;
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.fortress,
		UnitChassis.chassisClass.manofwar,
		UnitChassis.chassisClass.carrier,
		UnitChassis.chassisClass.helipad,
		UnitChassis.chassisClass.quadrapod,
		UnitChassis.chassisClass.mecha,
		UnitChassis.chassisClass.mothership,
	];
	public static readonly bayCapacity: IModifier = {
		bias: 1,
	};
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				unitModifier: {
					abilityPowerModifier: [
						{
							factor: 0.85,
						},
					],
				},
				chassis: [
					{
						baseCost: {
							factor: 1.1,
						},
						hitPoints: {
							factor: 1.1,
						},
					},
				],
				weapon: [
					{
						default: {
							baseRange: {
								bias: 1,
							},
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 14000,
		[Resources.resourceId.ores]: 7500,
		[Resources.resourceId.nitrogen]: 2000,
		[Resources.resourceId.minerals]: 4300,
		[Resources.resourceId.isotopes]: 560,
		[Resources.resourceId.halogens]: 1950,
	};
	public maxProductionCost = 300;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.4;
	public productionSpeed: number = this.baseProductionSpeed + 2 * BASE_PRODUCTION_SPEED;
}

export default TitanForgeFactory;

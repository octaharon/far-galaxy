import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class CloningVatsFactory extends GenericFactory {
	public static readonly id: number = 105;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Cloning Vats';
	public static readonly description: string = `A bionic lab equipped to rapidly produce any biotechnological units with as few flaws as possible`;
	public static readonly prototypeSlots: number = 4;
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.proteins]: {
			factor: 0.97,
		},
		[Resources.resourceId.carbon]: {
			factor: 0.95,
		},
	};
	public static readonly researchOutputModifier: IModifier = {
		bias: 4,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 15,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.biotech,
		UnitChassis.chassisClass.swarm,
		UnitChassis.chassisClass.hydra,
		UnitChassis.chassisClass.infantry,
	];
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				unitModifier: {
					abilityPowerModifier: [
						{
							factor: 0.85,
						},
					],
				},
			},
		},
		[UnitChassis.chassisClass.infantry]: {
			productionSpeed: {
				bias: TIER_PRODUCTION_SPEED * 1.5,
			},
		},
		[UnitChassis.chassisClass.biotech]: {
			productionSpeed: {
				bias: TIER_PRODUCTION_SPEED * 1.5,
			},
		},
	};

	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 5000,
		[Resources.resourceId.minerals]: 600,
		[Resources.resourceId.ores]: 4250,
		[Resources.resourceId.isotopes]: 150,
		[Resources.resourceId.halogens]: 400,
		[Resources.resourceId.proteins]: 250,
	};
	public maxProductionCost = 200;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.9;
}

export default CloningVatsFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier } from '../types';

export class SubmarinePool extends GenericFactory {
	public static readonly id: number = 403;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Submarine pool';
	public static readonly description: string = `A specialized construction dock for submarines, which can occasionally be used for building small ships like corvettes.`;
	public static readonly prototypeSlots: number = 2;
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 350,
	};
	public static readonly bayCapacity: IModifier = {
		bias: 1,
	};
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 0.97,
		},
		[Resources.resourceId.oxygen]: {
			factor: 0.97,
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.corvette,
		UnitChassis.chassisClass.sup_ship,
		UnitChassis.chassisClass.submarine,
	];
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		[UnitChassis.chassisClass.sup_ship]: {
			prototypeModifier: {
				unitModifier: {
					abilityPowerModifier: [
						{
							bias: 0.5,
						},
					],
				},
				chassis: [
					{
						scanRange: {
							bias: 3,
						},
						speed: {
							bias: 3,
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 500,
		[Resources.resourceId.ores]: 2200,
		[Resources.resourceId.oxygen]: 900,
		[Resources.resourceId.minerals]: 780,
		[Resources.resourceId.helium]: 330,
		[Resources.resourceId.hydrogen]: 3500,
	};
	public maxProductionCost = 160;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.85;
}

export default SubmarinePool;

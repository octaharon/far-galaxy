import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TIER_PRODUCTION_SPEED } from '../types';

export class HeavyVehicleFactory extends GenericFactory {
	public static readonly id: number = 202;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Heavy vehicle factory';
	public static readonly description: string = `An advanced factory with 3 production facilities, capable of producing all combat vehicles`;
	public static readonly prototypeSlots: number = 3;
	public static readonly repairSpeedModifier: IModifier = {
		bias: 4,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 25,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.scout,
		UnitChassis.chassisClass.lav,
		UnitChassis.chassisClass.sup_vehicle,
		UnitChassis.chassisClass.tank,
		UnitChassis.chassisClass.artillery,
		UnitChassis.chassisClass.hav,
		UnitChassis.chassisClass.mecha,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.halogens]: 450,
		[Resources.resourceId.isotopes]: 150,
		[Resources.resourceId.minerals]: 1800,
		[Resources.resourceId.ores]: 6000,
		[Resources.resourceId.carbon]: 700,
		[Resources.resourceId.oxygen]: 360,
		[Resources.resourceId.nitrogen]: 1280,
	};
	public maxProductionCost = 210;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.2;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default HeavyVehicleFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TIER_PRODUCTION_SPEED } from '../types';

export class ShipyardFactory extends GenericFactory {
	public static readonly id: number = 401;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Shipyard';
	public static readonly description: string = `Military production facility for building naval units`;
	public static readonly prototypeSlots: number = 2;
	public static readonly maxRawMaterials: IModifier = {
		bias: 75,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 10,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.helium]: {
			factor: 1.03,
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.corvette,
		UnitChassis.chassisClass.sup_ship,
		UnitChassis.chassisClass.destroyer,
		UnitChassis.chassisClass.battleship,
		UnitChassis.chassisClass.cruiser,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 4500,
		[Resources.resourceId.ores]: 1400,
		[Resources.resourceId.oxygen]: 400,
		[Resources.resourceId.isotopes]: 100,
		[Resources.resourceId.halogens]: 320,
		[Resources.resourceId.hydrogen]: 2230,
	};
	public maxProductionCost = 170;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.8;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default ShipyardFactory;

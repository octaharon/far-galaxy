import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TIER_PRODUCTION_SPEED } from '../types';

export class BarracksFactory extends GenericFactory {
	public static readonly id: number = 103;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Barracks';
	public static readonly description: string = `A training establishment for infantry, having enough space for several running projects,
    though not very technologically advanced`;
	public static readonly prototypeSlots: number = 3;
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.oxygen]: {
			factor: 0.97,
		},
	};
	public static readonly repairSpeedModifier: IModifier = {
		bias: 2,
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.infantry,
		UnitChassis.chassisClass.exosuit,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1400,
		[Resources.resourceId.minerals]: 1200,
		[Resources.resourceId.ores]: 1800,
		[Resources.resourceId.oxygen]: 200,
		[Resources.resourceId.isotopes]: 15,
		[Resources.resourceId.hydrogen]: 900,
		[Resources.resourceId.proteins]: 300,
	};
	public maxProductionCost = 110;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.65;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED;
}

export default BarracksFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier } from '../types';

export class SelfMaintainedFactory extends GenericFactory {
	public static readonly id: number = 303;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Self-Maintained Factory';
	public static readonly description: string = `A huge, fully robotized workshop which builds other robots. Everything is AI-controlled, all human factor excluded, imagine the quality!`;
	public static readonly prototypeSlots: number = 2;
	public static readonly energyOutputModifier: IModifier = {
		bias: 2,
	};
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 250,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.combatdrone,
		UnitChassis.chassisClass.rover,
		UnitChassis.chassisClass.pointdefense,
		UnitChassis.chassisClass.droid,
		UnitChassis.chassisClass.quadrapod,
	];
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				weapon: [
					{
						default: {
							damageModifier: {
								factor: 1.05,
							},
						},
					},
				],
				chassis: [
					{
						scanRange: {
							factor: 1.1,
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 5500,
		[Resources.resourceId.ores]: 4250,
		[Resources.resourceId.halogens]: 1200,
		[Resources.resourceId.minerals]: 1430,
		[Resources.resourceId.isotopes]: 230,
	};
	public maxProductionCost = 170;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT;
}

export default SelfMaintainedFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TIER_PRODUCTION_SPEED } from '../types';

export class CyberforgeFactory extends GenericFactory {
	public static readonly id: number = 301;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Cybernetic Forge';
	public static readonly description: string = `Military grade robots and cybernetic warfare are produced here`;
	public static readonly prototypeSlots: number = 2;
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.ores]: {
			factor: 0.97,
		},
		[Resources.resourceId.isotopes]: {
			factor: 0.95,
		},
	};
	public static readonly energyConsumptionModifier: IModifier = {
		bias: -1,
		minGuard: 1,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.combatdrone,
		UnitChassis.chassisClass.pointdefense,
		UnitChassis.chassisClass.droid,
		UnitChassis.chassisClass.rover,
		UnitChassis.chassisClass.science_vessel,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 7200,
		[Resources.resourceId.helium]: 280,
		[Resources.resourceId.ores]: 800,
		[Resources.resourceId.isotopes]: 150,
		[Resources.resourceId.hydrogen]: 5000,
	};
	public maxProductionCost = 150;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.8;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default CyberforgeFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TIER_PRODUCTION_SPEED } from '../types';

export class AirforgeFactory extends GenericFactory {
	public static readonly id: number = 502;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Airborne Forge';
	public static readonly description: string = `A fully automated production facility designated specifically for combat aircrafts`;
	public static readonly prototypeSlots: number = 2;
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.oxygen]: {
			factor: 0.95,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 0.97,
		},
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 2,
	};
	public static readonly researchOutputModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly productionCapacity: IModifier = null;
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.sup_aircraft,
		UnitChassis.chassisClass.fighter,
		UnitChassis.chassisClass.strike_aircraft,
		UnitChassis.chassisClass.bomber,
		UnitChassis.chassisClass.tiltjet,
		UnitChassis.chassisClass.carrier,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 4500,
		[Resources.resourceId.helium]: 720,
		[Resources.resourceId.carbon]: 5200,
		[Resources.resourceId.oxygen]: 820,
		[Resources.resourceId.isotopes]: 400,
	};
	public maxProductionCost = 220;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.25;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED;
}

export default AirforgeFactory;

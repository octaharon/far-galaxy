import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { BASE_PRODUCTION_SPEED, TChassisProductionModifier } from '../types';

export class AutomatedAssemblyFactory extends GenericFactory {
	public static readonly id: number = 203;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Automated Assembly Yard';
	public static readonly description: string = `Fully robotized assembly facility for combat vehicles, capable of flawlessly reproducing almost any blueprint in a timely manner`;
	public static readonly prototypeSlots: number = 1;
	public static readonly materialOutputModifier: IModifier = {
		bias: 1,
	};
	public static readonly maxRawMaterials: IModifier = {
		bias: 125,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.lav,
		UnitChassis.chassisClass.sup_vehicle,
		UnitChassis.chassisClass.droid,
		UnitChassis.chassisClass.scout,
	];
	public static readonly productionModifierByChassis?: TChassisProductionModifier = {
		[UnitChassis.chassisClass.sup_vehicle]: {
			prototypeModifier: {
				chassis: [
					{
						baseCost: {
							factor: 0.9,
						},
					},
				],
				unitModifier: {
					abilityPowerModifier: [
						{
							bias: 1,
						},
					],
					abilityRangeModifier: [
						{
							bias: 1,
							min: 1,
							minGuard: 1,
						},
					],
				},
			},
		},
		[UnitChassis.chassisClass.scout]: {
			productionSpeed: {
				factor: 1.15,
			},
			prototypeModifier: {
				chassis: [
					{
						hitPoints: {
							bias: 15,
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1800,
		[Resources.resourceId.minerals]: 2200,
		[Resources.resourceId.ores]: 1500,
		[Resources.resourceId.oxygen]: 420,
		[Resources.resourceId.nitrogen]: 2750,
		[Resources.resourceId.helium]: 900,
		[Resources.resourceId.isotopes]: 80,
		[Resources.resourceId.halogens]: 280,
	};
	public maxProductionCost = 170;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.75;
	public productionSpeed: number = this.baseProductionSpeed + BASE_PRODUCTION_SPEED;
}

export default AutomatedAssemblyFactory;

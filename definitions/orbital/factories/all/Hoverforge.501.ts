import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class HoverforgeFactory extends GenericFactory {
	public static readonly id: number = 501;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Hovercraft Forge';
	public static readonly description: string = `A fully automated production facility designated specifically for hover combat vehicles`;
	public static readonly prototypeSlots: number = 3;
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.helium]: {
			factor: 0.95,
		},
		[Resources.resourceId.minerals]: {
			factor: 0.97,
		},
	};
	public static readonly productionCapacity: IModifier = {
		bias: 15,
	};
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				unitModifier: {
					abilityPowerModifier: [
						{
							bias: 0.25,
						},
					],
				},
			},
		},
		[UnitChassis.chassisClass.sup_aircraft]: {
			prototypeModifier: {
				unitModifier: {
					abilityPowerModifier: [
						{
							bias: 1,
						},
					],
					abilityRangeModifier: [
						{
							bias: 1,
							min: 1,
							minGuard: 1,
						},
					],
					abilityDurationModifier: [
						{
							bias: 1,
							min: 1,
							minGuard: 1,
						},
					],
				},
			},
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.rover,
		UnitChassis.chassisClass.hovertank,
		UnitChassis.chassisClass.launcherpad,
		UnitChassis.chassisClass.tiltjet,
		UnitChassis.chassisClass.sup_aircraft,
		UnitChassis.chassisClass.helipad,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 1800,
		[Resources.resourceId.nitrogen]: 1900,
		[Resources.resourceId.halogens]: 400,
		[Resources.resourceId.helium]: 800,
		[Resources.resourceId.hydrogen]: 4450,
		[Resources.resourceId.isotopes]: 650,
		[Resources.resourceId.carbon]: 2200,
	};
	public maxProductionCost = 180;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.1;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default HoverforgeFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TIER_PRODUCTION_SPEED } from '../types';

export class LightVehicleFactory extends GenericFactory {
	public static readonly id: number = 201;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Light vehicle factory';
	public static readonly description: string = `A factory equipped to produce all but most heavy combat vehicles, capable of maintaining two projects at a time`;
	public static readonly prototypeSlots: number = 2;
	public static readonly repairSpeedModifier: IModifier = {
		bias: 2,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 10,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.scout,
		UnitChassis.chassisClass.lav,
		UnitChassis.chassisClass.sup_vehicle,
		UnitChassis.chassisClass.tank,
		UnitChassis.chassisClass.artillery,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.oxygen]: 1000,
		[Resources.resourceId.minerals]: 1800,
		[Resources.resourceId.carbon]: 400,
		[Resources.resourceId.ores]: 4000,
		[Resources.resourceId.oxygen]: 160,
		[Resources.resourceId.nitrogen]: 900,
	};
	public maxProductionCost = 160;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.65;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default LightVehicleFactory;

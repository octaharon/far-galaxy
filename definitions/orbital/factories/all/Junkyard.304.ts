import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class JunkyardFactory extends GenericFactory {
	public static readonly id: number = 304;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Junkyard';
	public static readonly description: string = `All the best engineers, tinkerers and software alchemists gather here and play with robots. Like a sandbox for old nerdy guys.
    If you ask them well, they can probably assemble few combat droids for you too`;
	public static readonly prototypeSlots: number = 4;
	public static readonly repairSpeedModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.combatdrone,
		UnitChassis.chassisClass.exosuit,
		UnitChassis.chassisClass.droid,
		UnitChassis.chassisClass.scout,
		UnitChassis.chassisClass.science_vessel,
	];
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		[UnitChassis.chassisClass.droid]: {
			productionSpeed: {
				factor: 1.25,
			},
			prototypeModifier: {
				chassis: [
					{
						baseCost: {
							bias: -15,
							minGuard: 15,
							min: 1,
						},
						speed: {
							bias: 1,
						},
					},
				],
			},
		},
		default: {
			prototypeModifier: {
				chassis: [
					{
						speed: {
							bias: 1,
						},
					},
				],
			},
		},
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 1,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1200,
		[Resources.resourceId.oxygen]: 400,
		[Resources.resourceId.minerals]: 760,
		[Resources.resourceId.proteins]: 850,
		[Resources.resourceId.nitrogen]: 1700,
		[Resources.resourceId.isotopes]: 240,
	};
	public maxProductionCost = 160;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.8;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED;
}

export default JunkyardFactory;

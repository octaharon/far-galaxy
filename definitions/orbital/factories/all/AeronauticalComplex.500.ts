import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TIER_PRODUCTION_SPEED } from '../types';

export class AeronauticalComplexFactory extends GenericFactory {
	public static readonly id: number = 500;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Aeronautics Complex';
	public static readonly description: string = `An advanced production facility with a wind tunnel and special equipment to produce small flying vehicles`;
	public static readonly prototypeSlots: number = 2;
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.halogens]: {
			factor: 1.03,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 1.03,
		},
	};
	public static readonly energyOutputModifier: IModifier = {
		bias: 2,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 5,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.rover,
		UnitChassis.chassisClass.sup_aircraft,
		UnitChassis.chassisClass.tiltjet,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2250,
		[Resources.resourceId.ores]: 900,
		[Resources.resourceId.nitrogen]: 900,
		[Resources.resourceId.minerals]: 900,
		[Resources.resourceId.isotopes]: 30,
		[Resources.resourceId.helium]: 1900,
	};
	public maxProductionCost = 140;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.9;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default AeronauticalComplexFactory;

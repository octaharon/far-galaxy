import { ArithmeticPrecision } from '../../../maths/constants';
import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { BASE_PRODUCTION_SPEED, TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class NaviforgeFactory extends GenericFactory {
	public static readonly id: number = 404;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Maritime Forge';
	public static readonly description: string = `A shipbuilding facility with fully automated production cycle, which can maintain a plenty of naval projects and assemble them at a spectacular rate`;
	public static readonly prototypeSlots: number = 4;
	public static readonly maxRawMaterials: IModifier = {
		bias: 50,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.corvette,
		UnitChassis.chassisClass.sup_ship,
		UnitChassis.chassisClass.destroyer,
		UnitChassis.chassisClass.battleship,
		UnitChassis.chassisClass.cruiser,
	];
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.helium]: {
			factor: 1.02,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 1.03,
		},
	};
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				chassis: [
					{
						baseCost: {
							bias: -5,
							minGuard: 10,
						},
						speed: {
							bias: 1,
						},
						hitPoints: {
							bias: 5,
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2300,
		[Resources.resourceId.ores]: 900,
		[Resources.resourceId.oxygen]: 3300,
		[Resources.resourceId.minerals]: 220,
		[Resources.resourceId.isotopes]: 550,
		[Resources.resourceId.hydrogen]: 5000,
	};
	public maxProductionCost = 200;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.35;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED * 1.5;
}

export default NaviforgeFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class WarfareMultiplex extends GenericFactory {
	public static readonly id: number = 205;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Warfare Multiplex';
	public static readonly description: string = `Top-class vehicle engineering facility with the best available stuff, capable of building pretty much anything which traverses the ground,
    and they always manage somehow to get the most out of any combat platform`;
	public static readonly prototypeSlots: number = 2;
	public static readonly maxRawMaterials: IModifier = {
		bias: 50,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 35,
	};
	public static resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 1.05,
		},
		[Resources.resourceId.nitrogen]: {
			factor: 1.03,
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.scout,
		UnitChassis.chassisClass.rover,
		UnitChassis.chassisClass.lav,
		UnitChassis.chassisClass.sup_vehicle,
		UnitChassis.chassisClass.tank,
		UnitChassis.chassisClass.launcherpad,
		UnitChassis.chassisClass.artillery,
		UnitChassis.chassisClass.hav,
		UnitChassis.chassisClass.mecha,
		UnitChassis.chassisClass.hovertank,
	];
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				chassis: [
					{
						baseCost: {
							bias: 10,
						},
						hitPoints: {
							bias: 15,
						},
					},
				],
				armor: [
					{
						default: {
							factor: 0.9,
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2900,
		[Resources.resourceId.isotopes]: 700,
		[Resources.resourceId.halogens]: 600,
		[Resources.resourceId.minerals]: 4200,
		[Resources.resourceId.ores]: 4800,
		[Resources.resourceId.oxygen]: 950,
		[Resources.resourceId.nitrogen]: 900,
	};
	public maxProductionCost = 220;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.45;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default WarfareMultiplex;

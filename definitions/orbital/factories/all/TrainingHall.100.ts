import UnitChassis                     from '../../../technologies/types/chassis';
import { Resources }                   from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';

export class TrainingHallFactory extends GenericFactory {
	public static readonly id: number = 100;
	public static readonly buildingTier: number = 1;
	public static readonly caption: string = 'Training Hall';
	public static readonly description: string = `A shooting range, a military lectorium and simulation training compounds naturally attract all the fiercest mercenaries around,
    but also could be used for training operatives`;
	public static readonly prototypeSlots: number = 1;
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.infantry,
		UnitChassis.chassisClass.exosuit,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1500,
		[Resources.resourceId.minerals]: 1000,
		[Resources.resourceId.ores]: 400,
		[Resources.resourceId.proteins]: 120,
		[Resources.resourceId.oxygen]: 175,
	};
	public maxProductionCost = 65;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.4;
}

export default TrainingHallFactory;

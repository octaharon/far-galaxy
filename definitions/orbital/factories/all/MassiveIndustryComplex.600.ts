import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { BASE_PRODUCTION_SPEED, TChassisProductionModifier } from '../types';

export class MassiveIndustryComplex extends GenericFactory {
	public static readonly id: number = 600;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Industrial Complex';
	public static readonly description: string = `A huge construction workshop, enabling production of massive war machines. One of few production buildings big enough to house a Carrier, a Fortress or a Mothership.`;
	public static readonly prototypeSlots: number = 3;
	public static readonly engineeringOutputModifier: IModifier = {
		factor: 1.2,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 40,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.minerals]: {
			factor: 1.05,
		},
		[Resources.resourceId.isotopes]: {
			factor: 1.05,
		},
	};
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				weapon: [{ default: { baseAccuracy: { bias: 0.15 } } }],
				unitModifier: {
					statusDurationModifier: [
						{
							bias: -1,
							min: 1,
							minGuard: 1,
						},
					],
				},
			},
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.fortress,
		UnitChassis.chassisClass.manofwar,
		UnitChassis.chassisClass.carrier,
		UnitChassis.chassisClass.helipad,
		UnitChassis.chassisClass.mothership,
		UnitChassis.chassisClass.launcherpad,
		UnitChassis.chassisClass.pointdefense,
		UnitChassis.chassisClass.quadrapod,
		UnitChassis.chassisClass.battleship,
		UnitChassis.chassisClass.hav,
		UnitChassis.chassisClass.mecha,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 9000,
		[Resources.resourceId.ores]: 5700,
		[Resources.resourceId.nitrogen]: 1900,
		[Resources.resourceId.minerals]: 1250,
		[Resources.resourceId.isotopes]: 400,
		[Resources.resourceId.helium]: 2400,
	};
	public maxProductionCost = 235;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.5;
	public productionSpeed: number = this.baseProductionSpeed + BASE_PRODUCTION_SPEED;
}

export default MassiveIndustryComplex;

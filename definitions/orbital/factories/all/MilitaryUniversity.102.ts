import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class MilitaryUniversityFactory extends GenericFactory {
	public static readonly id: number = 102;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Military University';
	public static readonly description: string = `Highest grade operatives are forged here`;
	public static readonly prototypeSlots: number = 3;
	public static readonly engineeringOutputModifier: IModifier = {
		bias: 2,
	};
	public static readonly researchOutputModifier: IModifier = {
		bias: 2,
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.infantry,
		UnitChassis.chassisClass.exosuit,
		UnitChassis.chassisClass.droid,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1000,
		[Resources.resourceId.minerals]: 1200,
		[Resources.resourceId.ores]: 7500,
		[Resources.resourceId.oxygen]: 600,
		[Resources.resourceId.isotopes]: 20,
		[Resources.resourceId.nitrogen]: 800,
		[Resources.resourceId.proteins]: 200,
		[Resources.resourceId.halogens]: 170,
	};
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				shield: [
					{
						shieldAmount: {
							bias: 10,
						},
					},
				],
				chassis: [
					{
						dodge: {
							default: {
								bias: 0.1,
							},
						},
					},
				],
			},
		},
	};
	public maxProductionCost = 175;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.95;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default MilitaryUniversityFactory;

import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';

export class MotorPlantFactory extends GenericFactory {
	public static readonly id: number = 200;
	public static readonly buildingTier: number = 1;
	public static readonly caption: string = 'Motorworks';
	public static readonly description: string = `Basic vehicle production facility with a single conveyor`;
	public static readonly prototypeSlots: number = 1;
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.scout,
		UnitChassis.chassisClass.lav,
		UnitChassis.chassisClass.sup_vehicle,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2200,
		[Resources.resourceId.minerals]: 1000,
		[Resources.resourceId.ores]: 2500,
		[Resources.resourceId.oxygen]: 100,
		[Resources.resourceId.nitrogen]: 400,
	};
	public maxProductionCost = 80;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.5;
}

export default MotorPlantFactory;

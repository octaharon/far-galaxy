import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class SpecialForcesAcademyFactory extends GenericFactory {
	public static readonly id: number = 106;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Special Forces Academy';
	public static readonly description: string = `A military establishment preparing best special operatives in class`;
	public static readonly prototypeSlots: number = 2;
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.infantry,
		UnitChassis.chassisClass.exosuit,
	];
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				unitModifier: {
					abilityPowerModifier: [
						{
							bias: 1,
						},
					],
				},
				chassis: [
					{
						scanRange: {
							bias: 2,
						},
						hitPoints: {
							bias: 10,
						},
					},
				],
				weapon: [
					{
						default: {
							damageModifier: {
								factor: 1.15,
							},
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 5500,
		[Resources.resourceId.minerals]: 2500,
		[Resources.resourceId.ores]: 1700,
		[Resources.resourceId.oxygen]: 200,
		[Resources.resourceId.isotopes]: 30,
		[Resources.resourceId.proteins]: 130,
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 1,
	};
	public maxProductionCost = 200;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.75;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default SpecialForcesAcademyFactory;

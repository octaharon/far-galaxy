import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';

export class WaterDockFactory extends GenericFactory {
	public static readonly id: number = 400;
	public static readonly buildingTier: number = 1;
	public static readonly caption: string = 'Water dock';
	public static readonly description: string = `A small water dock for boat construction`;
	public static readonly prototypeSlots: number = 1;
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.corvette,
		UnitChassis.chassisClass.sup_ship,
		UnitChassis.chassisClass.cruiser,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2400,
		[Resources.resourceId.ores]: 1200,
		[Resources.resourceId.oxygen]: 800,
		[Resources.resourceId.isotopes]: 10,
		[Resources.resourceId.hydrogen]: 1800,
	};
	public maxProductionCost = 100;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.85;
}

export default WaterDockFactory;

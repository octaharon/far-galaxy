import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier, TIER_PRODUCTION_SPEED } from '../types';

export class BioforgeFactory extends GenericFactory {
	public static readonly id: number = 104;
	public static readonly buildingTier: number = 3;
	public static readonly caption: string = 'Bionic Forge';
	public static readonly description: string = `A military lab packed with all sorts of new experimental technologies, preparing the best soldiers
    and breeding the evilest bio-units in the known Universe`;
	public static readonly prototypeSlots: number = 2;
	public static readonly materialOutputModifier: IModifier = {
		bias: 2,
	};
	public static readonly repairSpeedModifier: IModifier = {
		bias: 2,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.proteins]: {
			factor: 1.05,
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.biotech,
		UnitChassis.chassisClass.swarm,
		UnitChassis.chassisClass.hydra,
	];
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				unitModifier: {
					abilityPowerModifier: [
						{
							bias: 0.5,
						},
					],
				},
				chassis: [
					{
						hitPoints: {
							factor: 1.05,
						},
					},
				],
			},
		},
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2500,
		[Resources.resourceId.minerals]: 2000,
		[Resources.resourceId.ores]: 3600,
		[Resources.resourceId.oxygen]: 100,
		[Resources.resourceId.isotopes]: 250,
		[Resources.resourceId.nitrogen]: 1000,
		[Resources.resourceId.halogens]: 250,
	};
	public maxProductionCost = 210;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.2;
	public productionSpeed: number = this.baseProductionSpeed + TIER_PRODUCTION_SPEED / 2;
}

export default BioforgeFactory;

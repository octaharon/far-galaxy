import UnitChassis from '../../../technologies/types/chassis';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFactory from '../GenericFactory';
import { TChassisProductionModifier } from '../types';

export class BionicInstitute extends GenericFactory {
	public static readonly id: number = 101;
	public static readonly buildingTier: number = 2;
	public static readonly caption: string = 'Bionic Institute';
	public static readonly description: string = `A dedicated military research establishment that prepares highly advanced operatives and produces experimental bionic units`;
	public static readonly prototypeSlots: number = 1;
	public static readonly researchOutputModifier: IModifier = {
		bias: 2,
	};
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.proteins]: {
			factor: 0.97,
		},
	};
	public static readonly productionType: UnitChassis.chassisClass[] = [
		UnitChassis.chassisClass.infantry,
		UnitChassis.chassisClass.exosuit,
		UnitChassis.chassisClass.biotech,
	];
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2800,
		[Resources.resourceId.minerals]: 1500,
		[Resources.resourceId.ores]: 4000,
		[Resources.resourceId.oxygen]: 200,
		[Resources.resourceId.isotopes]: 20,
		[Resources.resourceId.nitrogen]: 420,
	};
	public static readonly productionModifierByChassis: TChassisProductionModifier = {
		default: {
			prototypeModifier: {
				armor: [{ default: { factor: 0.9 } }],
				unitModifier: {
					abilityDurationModifier: [{ bias: 1 }],
					abilityPowerModifier: [
						{
							bias: 0.25,
						},
					],
				},
			},
		},
	};
	public maxProductionCost = 150;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.7;
}

export default BionicInstitute;

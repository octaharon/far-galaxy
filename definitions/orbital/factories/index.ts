import GenericFactory from './GenericFactory';
import { TFactory } from './types';

let FactoryList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), FactoryList);
FactoryList = FactoryList.filter(
	(module: any) => module.default.prototype instanceof GenericFactory && module.default.id > 0,
).map((module: any) => module.default);
export default FactoryList as TFactory[];

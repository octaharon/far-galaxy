import { ICollectibleFactory, TCollectible } from '../../core/Collectible';
import { IPrototype, IPrototypeModifier } from '../../prototypes/types';
import Units from '../../tactical/units/types';
import UnitChassis from '../../technologies/types/chassis';
import {
	IBaseBuilding,
	IBaseBuildingStatic,
	IBaseBuildingUpgrade,
	IPlayerBaseCycleModifier,
	TBaseBuildingProps,
	TBaseBuildingType,
	TSerializedBaseBuilding,
} from '../types';

export const BASE_PRODUCTION_SPEED = 8;
export const TIER_PRODUCTION_SPEED = 4;
export const ORBITAL_PRODUCTION_SWITCH_PENALTY = 0.25;
export const FACTORY_MIN_PRODUCTION_COST = 50;
export type TProductionModifier = {
	productionSpeed?: IModifier;
	prototypeModifier?: IPrototypeModifier;
};
export type TFactoryID = number;
export type TFactoryModifiableProps = TBaseBuildingProps & {
	productionSpeed: number;
	maxProductionCost: number;
};
export type TFactoryProps = TFactoryModifiableProps & {
	currentProductionStock: number;
	currentProductionSlot: number;
	productionPrototypes: string[]; // Prototype IDs
};
export type TSerializedFactory = TFactoryProps & TSerializedBaseBuilding;

export interface IFactory
	extends TFactoryProps,
		IBaseBuilding<
			TBaseBuildingType.factory,
			TFactoryProps,
			TSerializedFactory,
			IFactoryBuildingStatic,
			TFactoryUpgrade
		> {
	currentPrototype: IPrototype;
	baseProductionSpeed: number;
	currentProductionSpeed: number;

	setProductionSlot(slot: number, lossModifier?: IModifier): this;

	getProductionModifierForPrototype(prototype: IPrototype): IPrototypeModifier;

	productionCycle(): Units.IUnit;

	setPrototypeSlot(slot: number, prototype?: IPrototype): this;

	init(): this;
}

export type TFactory = TCollectible<IFactory, IFactoryBuildingStatic>;
export type TChassisProductionModifier = EnumProxyWithDefault<UnitChassis.chassisClass, TProductionModifier>;
export type TMovementProductionModifier = EnumProxyWithDefault<UnitChassis.movementType, TProductionModifier>;

export interface IFactoryMeta {
	factory: TFactory;
}

export type TFactoryUpgrade = IBaseBuildingUpgrade<TFactoryModifiableProps>;

export interface IFactoryBuildingStatic
	extends IBaseBuildingStatic<TBaseBuildingType.factory, TFactoryProps, TFactoryUpgrade> {
	productionType: UnitChassis.chassisClass[];
	productionModifierByChassis?: TChassisProductionModifier;
	prototypeSlots: number;
}

export interface IFactoryProvider
	extends ICollectibleFactory<TFactoryProps, TSerializedFactory, IFactory, IFactoryBuildingStatic> {
	getBaseProductionSpeed(id: number): number;
}

export const getTierProductionSpeed = (tier: number) => {
	return TIER_PRODUCTION_SPEED * (Math.max(1, tier) - 1) + BASE_PRODUCTION_SPEED;
};

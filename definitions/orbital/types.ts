import { ICollectible, ICollectibleFactory, IStaticCollectible, TSerializedCollectible } from '../core/Collectible';
import { DIInjectableCollectibles } from '../core/DI/injections';
import { DIProvider } from '../core/DI/types';
import { IPlayerProperty, IPlayerPropertyProps } from '../player/types';
import UnitDefense from '../tactical/damage/defense';
import PoliticalFactions from '../world/factions';
import Resources from '../world/resources/types';
import { IPlayerBase } from './base/types';
import { IEstablishment } from './establishments/types';
import { IFacility } from './facilities/types';
import { IFactory } from './factories/types';

export type TBaseBuildingProps = IPlayerPropertyProps & {
	baseId: string;
	energyMaintenanceCost: number;
	disabled: boolean;
	powered: boolean;
};
export type TSerializedBaseBuilding = TBaseBuildingProps & TSerializedCollectible;

export enum TBaseBuildingType {
	'factory' = 'bb_factory', // production
	'facility' = 'bb_facility', // maintenance and microeconomics
	'establishment' = 'bb_establishment', // research and macro
}

export const BaseBuildingOrder = [
	TBaseBuildingType.facility,
	TBaseBuildingType.establishment,
	TBaseBuildingType.factory,
];

export type IPlayerBaseCycleModifier = {
	abilityPowerModifier?: IModifier;
	bayCapacity: IModifier;
	intelligenceLevel: IModifier;
	productionCapacity: IModifier;
	repairSpeedModifier?: IModifier;
	engineeringOutputModifier?: IModifier;
	researchOutputModifier?: IModifier;
	energyConsumptionModifier?: IModifier;
	energyOutputModifier?: IModifier;
	maxEnergyCapacity: IModifier;
	materialOutputModifier?: IModifier;
	maxRawMaterials: IModifier;
	resourceOutputModifier?: Resources.TResourceModifier;
	resourceConsumptionModifier?: Resources.TResourceModifier;
};

export type IPlayerBaseFacilityModifier = {
	maxHitPoints: IModifier;
};
export type IPlayerBaseEstablishmentModifier = {
	counterIntelligenceLevel: IModifier;
	armor: UnitDefense.TArmorModifier;
};

export type IBaseBuildingStatic<
	T extends TBaseBuildingType,
	PropType extends TBaseBuildingProps,
	UpgradeType extends IBaseBuildingUpgrade<PropType>,
> = IStaticCollectible &
	IPlayerBaseCycleModifier & {
		readonly buildingTier: number;
		readonly unlockable: boolean;
		readonly type: T;
		readonly buildingCost: Resources.TResourcePayload;
		readonly restrictedToFaction: PoliticalFactions.TFactionID;
		readonly upgrades: UpgradeType[];
		readonly abilities: number[];
	};

export type IBaseBuildingUpgrade<PropType extends TBaseBuildingProps> = {
	[K in Exclude<
		keyof PropType,
		keyof IPlayerProperty | 'disabled' | 'baseId' | 'powered'
	>]?: PropType[K] extends number ? IModifier : never;
} & {
	caption?: string;
};
type TBuildingProvider<T extends TBaseBuildingType> = T extends TBaseBuildingType.facility
	? DIProvider<DIInjectableCollectibles.facility>
	: T extends TBaseBuildingType.factory
	? DIProvider<DIInjectableCollectibles.factory>
	: T extends TBaseBuildingType.establishment
	? DIProvider<DIInjectableCollectibles.establishment>
	: never;
export type TTypedBaseBuilding = IFactory | IEstablishment | IFacility;

export interface IBaseBuildingManager {
	preInstantiate?<T extends Partial<TBaseBuildingProps>>(b: T): T;

	postInstantiate?<T extends TTypedBaseBuilding>(b: T): T;

	preStore?<T extends Partial<TBaseBuildingProps>>(b: T): T;

	postStore?<T extends TTypedBaseBuilding>(b: T): T;

	isFactory(b: any): b is IFactory;

	isEstablishment(b: any): b is IEstablishment;

	isFacility(b: any): b is IFacility;
}

export type IBaseBuildingProvider = ICollectibleFactory<any, any, any, IBaseBuildingStatic<any, any, any>>;

export interface IBaseBuilding<
	T extends TBaseBuildingType,
	PropType extends TBaseBuildingProps,
	SerializedType extends TSerializedBaseBuilding,
	StaticType extends IBaseBuildingStatic<T, PropType, UpgradeType>,
	UpgradeType extends IBaseBuildingUpgrade<PropType>,
> extends IPlayerProperty,
		ICollectible<PropType, SerializedType, StaticType>,
		TBaseBuildingProps {
	base: IPlayerBase;

	setBase(baseId: string): this;

	upgrade(index: number): this;

	getDemolishMaterialsValue(): number;

	toggle(): this;

	isOperational(): boolean;

	isWorking(): boolean;
}

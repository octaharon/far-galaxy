import { DIInjectableSerializables } from '../core/DI/injections';
import BasicMaths from '../maths/BasicMaths';
import PlayerCollectibleProperty from '../player/PlayerCollectibleProperty';
import PoliticalFactions                      from '../world/factions';
import Resources                              from '../world/resources/types';
import { BUILDING_DEMOLISH_CONVERSION_RATIO } from './base/constants';
import {
	IBaseBuilding,
	IBaseBuildingStatic,
	IBaseBuildingUpgrade,
	TBaseBuildingProps,
	TBaseBuildingType,
	TSerializedBaseBuilding,
} from './types';

export abstract class GenericBaseBuilding<
		T extends TBaseBuildingType,
		PropType extends TBaseBuildingProps,
		SerializedType extends PropType & TSerializedBaseBuilding,
		StaticType extends IBaseBuildingStatic<T, PropType, UpgradeType>,
		UpgradeType extends IBaseBuildingUpgrade<PropType>,
	>
	extends PlayerCollectibleProperty<PropType, SerializedType, StaticType>
	implements TBaseBuildingProps, IBaseBuilding<T, PropType, SerializedType, StaticType, UpgradeType>
{
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 0;
	public static readonly unlockable: boolean = false;
	public static readonly restrictedToFaction: PoliticalFactions.TFactionID = null;
	public powered: boolean;
	public type: T;
	public disabled: boolean;
	public energyMaintenanceCost: number;
	public baseId: string;

	public get base() {
		if (!this.baseId) return null;
		return this.getProvider(DIInjectableSerializables.base)?.find(this.baseId);
	}

	public get owner() {
		if (!this.playerId) return null;
		return this.getProvider(DIInjectableSerializables.player)?.find(this.playerId);
	}

	public setBase(baseId: string) {
		this.baseId = baseId;
		return this;
	}

	public isOperational() {
		return !this.disabled;
	}

	public isWorking() {
		return this.isOperational() && this.powered;
	}

	/**
	 * When a building is demolished, this amount of Raw Materials are stored at Base
	 */
	public getDemolishMaterialsValue(): number {
		return Math.round(
			this.static.buildingTier *
				Object.keys(this.static.buildingCost).reduce(
					(acc, r: Resources.resourceId) =>
						acc + this.static.buildingCost[r] / BUILDING_DEMOLISH_CONVERSION_RATIO[r],
					0,
				),
		);
	}

	public toggle() {
		if (!this.disabled) this.powered = !this.powered;
		return this;
	}

	/**
	 * Apply a building upgrade
	 * @param u
	 */
	public upgrade(u: number) {
		if (this.static.upgrades && this.static.upgrades[u]) {
			const upgrade = this.static.upgrades[u];
			Object.keys(upgrade).forEach((prop) => {
				if (prop === 'caption') return;
				if (prop in this) {
					// @ts-ignore
					this[prop] = BasicMaths.applyModifier(this[prop], upgrade[prop]);
				}
			});
		}
		return this;
	}

	protected __serializeBuilding(): TSerializedBaseBuilding {
		return {
			...this.__serializePlayerCollectibleProperty(),
			powered: this.powered,
			baseId: this.baseId,
			disabled: this.disabled,
			energyMaintenanceCost: this.energyMaintenanceCost,
		};
	}
}

export default GenericBaseBuilding;

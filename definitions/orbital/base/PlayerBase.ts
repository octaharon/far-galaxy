import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles, DIInjectableSerializables } from '../../core/DI/injections';
import { DIGlobalContainer } from '../../core/DI/types';
import GameData from '../../game/types';
import BasicMaths from '../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../maths/constants';
import Series from '../../maths/Series';
import { TPlayerId } from '../../player/playerId';
import PlayerProperty from '../../player/PlayerProperty';
import { TPlanetSurfaceProps } from '../../space/Planets';
import { getBaseStatProperty } from '../../stats/helpers';
import GameStats from '../../stats/types';
import { AbilityKit } from '../../tactical/abilities/AbilityKit';
import Abilities from '../../tactical/abilities/types';
import UnitAttack from '../../tactical/damage/attack';
import Damage from '../../tactical/damage/damage';
import DamageManager from '../../tactical/damage/DamageManager';
import { applyAttackUnitToDefense } from '../../tactical/damage/damageModel';
import UnitDefense from '../../tactical/damage/defense';
import DefenseManager from '../../tactical/damage/DefenseManager';
import Salvage from '../../tactical/salvage/types';
import { DEFAULT_EFFECT_POWER } from '../../tactical/statuses/constants';
import EffectEvents from '../../tactical/statuses/effectEvents';
import StatusEffectMeta from '../../tactical/statuses/statusEffectMeta';
import { IStatusEffect, TEffectApplianceItem } from '../../tactical/statuses/types';
import Units from '../../tactical/units/types';
import UnitChassis from '../../technologies/types/chassis';
import { getFactionAbilities } from '../../world/factionIndex';
import { addResourcePayloads, applyResourceModifier, resourceTemplate } from '../../world/resources/helpers';
import Resources from '../../world/resources/types';
import BuildingManager from '../GenericBaseBuildingManager';
import { TBaseBuildingType } from '../types';
import { BASE_TIER_LIMIT, MAX_DEPLOYS_PER_TURN, REPAIR_TO_MATERIALS_CONVERSION } from './constants';
import {
	IPlayerBase,
	TBaseBuildingSerializedArray,
	TBaseTypedBuilding,
	TBaseTypedBuildingDefinition,
	TPlayerBaseComputedProps,
	TPlayerBaseProps,
	TSerializedPlayerBase,
} from './types';

export const defaultPlayerBaseProps: Partial<TPlayerBaseProps> = {
	unitsAtBay: [],
	productionCapacity: 50,
	productionCapacityLeft: 50,
	maxHitPoints: 800,
	currentHitPoints: 800,
	deploys: 0,
	armor: {
		default: {
			bias: -15,
			min: 0,
		},
	},
	intelligenceLevel: 1,
	stats: {
		battlesLost: 0,
		battlesWon: 0,
		defensiveAbilitiesCast: 0,
		engineeringOutput: 0,
		establishmentsBuilt: 0,
		facilitiesBuilt: 0,
		factoriesBuilt: 0,
		healthLost: 0,
		hullDamageTakenByType: Damage.DamageTemplate(0),
		offensiveAbilitiesCast: 0,
		productionOutput: 0,
		rawMaterialsOutput: 0,
		rawMaterialsSpent: 0,
		repairOutput: 0,
		researchOutput: 0,
		spellsTaken: 0,
		turnsDisabled: 0,
		unitsProducedByChassisId: {},
		resourcesGainedByType: resourceTemplate(),
		resourcesSpentByType: resourceTemplate(),
	},
	bayCapacity: 3,
	maxRawMaterials: 150,
	rawMaterials: 150,
	counterIntelligenceLevel: 0,
	energyCapacity: 0,
	maxEnergyCapacity: 150,
	energyProducedThisTurn: 0,
	buildings: {
		[TBaseBuildingType.factory]: [],
		[TBaseBuildingType.establishment]: [],
		[TBaseBuildingType.facility]: [],
	},
	buildingSlots: {
		[TBaseBuildingType.factory]: 1,
		[TBaseBuildingType.establishment]: 0,
		[TBaseBuildingType.facility]: 1,
	} as EnumProxy<TBaseBuildingType, number>,
};

export default class PlayerBase extends PlayerProperty<TPlayerBaseProps, TSerializedPlayerBase> implements IPlayerBase {
	public rawMaterials: number;
	public unitsAtBay: Units.IUnit[];
	public intelligenceLevel: number;
	public counterIntelligenceLevel: number;
	// amount of raw materials that is instantly shared by all producing factories at the start of combat
	public productionCapacity: number;
	public productionCapacityLeft: number;
	public maxRawMaterials: number;
	public deploys: number;
	public energyCapacity: number;
	public maxEnergyCapacity: number;
	public bayCapacity: number;
	public energyProducedThisTurn: number;
	public maxHitPoints: number;
	public stats: GameStats.TBaseStats;
	public buildingSlots: EnumProxy<TBaseBuildingType, number>;
	public buildings: {
		[K in TBaseBuildingType]: Array<TBaseTypedBuilding<K>>;
	};
	public currentHitPoints: number;
	public armor: UnitDefense.TArmor;
	public effects: Array<IStatusEffect<StatusEffectMeta.effectAgentTypes.player>>;
	public readonly effectTargetType = StatusEffectMeta.effectAgentTypes.player;
	public kit: Abilities.IAbilityKit;

	public injectDependencies(container: DIGlobalContainer) {
		super.injectDependencies(container);
		if (this.kit) this.kit.injectDependencies(this.DIContainer);
		return this;
	}

	public addAbility(abilityId: number) {
		this.kit.addAbility(abilityId);
		return this;
	}

	public clearAbilities() {
		this.kit.clearAbilities();
		return this;
	}

	public removeAbility(abilityId: number) {
		this.kit.removeAbility(abilityId);
		return this;
	}

	public constructor(p: TPlayerBaseProps) {
		super(p);
		this.kit = new AbilityKit(this, null);
	}

	public get totalBuildingCost(): Resources.TResourcePayload {
		return addResourcePayloads(...this.getAllBuildings().map((b) => b.static.buildingCost));
	}

	public get currentEconomy(): TPlayerBaseComputedProps {
		return {
			maxEnergyCapacity: BasicMaths.applyModifier(this.maxEnergyCapacity, ...this.getEnergyCapacityModifier()),
			availableAbilities: ([] as number[])
				.concat(
					getFactionAbilities(this.owner.faction).map((v) => v.abilityId),
					this.owner.abilitiesAvailable ?? [],
					this.getAllBuildings().flatMap((a) => a.static.abilities),
				)
				.filter(Boolean),
			armor: DefenseManager.stackDamageProtectionDefinition(
				this.armor,
				DefenseManager.stackArmorModifiers(
					...this.buildings[TBaseBuildingType.establishment].map((b) => b.static.armor),
				),
			),
			bayCapacity: BasicMaths.applyModifier(
				this.bayCapacity,
				this.owner.baseModifier.bayCapacity,
				...this.getAllBuildings().map((b) => b.static.bayCapacity),
			),
			maxHitPoints: BasicMaths.applyModifier(
				this.maxHitPoints,
				this.owner.baseModifier.maxHitPoints,
				...this.buildings[TBaseBuildingType.facility].map((b) => b.static.maxHitPoints),
			),
			counterIntelligenceLevel: BasicMaths.applyModifier(
				this.counterIntelligenceLevel,
				this.owner.baseModifier.counterIntelligenceLevel,
				...this.buildings[TBaseBuildingType.establishment].map((b) =>
					b.disabled ? null : b.static.counterIntelligenceLevel,
				),
			),
			intelligenceLevel: BasicMaths.applyModifier(
				this.counterIntelligenceLevel,
				this.owner.baseModifier.intelligenceLevel,
				...this.getAllBuildings().map((b) => (b.disabled ? null : b.static.intelligenceLevel)),
			),
			energyOutput: Series.sum(
				this.buildings[TBaseBuildingType.facility].map((b) =>
					BasicMaths.applyModifier(
						b.disabled || !b.outputMode ? 0 : b.energyOutput,
						{ min: 0, minGuard: 0 },
						...this.getEnergyOutputModifier(),
					),
				),
			),
			energyConsumption: Series.sum(
				this.getAllBuildings().map((b) =>
					BasicMaths.applyModifier(
						b.disabled ? 0 : b.energyMaintenanceCost,
						{ min: 0, minGuard: 0 },
						...this.getEnergyConsumptionModifier(),
					),
				),
			),
			engineeringOutput: Series.sum(
				this.buildings[TBaseBuildingType.establishment].map((b) =>
					BasicMaths.applyModifier(
						b.disabled ? 0 : b.engineeringOutput,
						{ min: 0, minGuard: 0 },
						...this.getEnergyOutputModifier(),
					),
				),
			),
			researchOutput: Series.sum(
				this.buildings[TBaseBuildingType.establishment].map((b) =>
					BasicMaths.applyModifier(
						b.disabled ? 0 : b.researchOutput,
						{ min: 0, minGuard: 0 },
						...this.getResearchOutputModifier(),
					),
				),
			),
			materialOutput: Series.sum(
				this.buildings[TBaseBuildingType.facility].map((b) =>
					BasicMaths.applyModifier(
						b.disabled || !b.outputMode ? 0 : b.materialOutput,
						{ min: 0, minGuard: 0 },
						...this.getMaterialOutputModifier(),
					),
				),
			),
			repairOutput: Series.sum(
				this.buildings[TBaseBuildingType.facility].map((b) =>
					BasicMaths.applyModifier(
						b.disabled || !b.outputMode ? 0 : b.repairOutput,
						{ min: 0, minGuard: ArithmeticPrecision },
						...this.getRepairOutputModifier(),
					),
				),
			),
			maxRawMaterials: BasicMaths.applyModifier(
				this.maxRawMaterials,
				this.owner.baseModifier.maxRawMaterials,
				...this.getAllBuildings().map((b) => b.static.maxRawMaterials),
			),
			productionCapacity: BasicMaths.applyModifier(
				this.productionCapacity,
				this.owner.baseModifier.productionCapacity,
				...this.getAllBuildings().map((b) => b.static.productionCapacity),
			),
			resourceOutput: applyResourceModifier(
				addResourcePayloads(
					...this.buildings[TBaseBuildingType.facility].map((b) =>
						b.disabled || b.outputMode ? {} : b.static.resourceProduction,
					),
				),
				...this.getResourceOutputModifier(),
			),
			resourceConsumption: applyResourceModifier(
				addResourcePayloads(
					...this.buildings[TBaseBuildingType.facility].map((b) =>
						b.disabled ? {} : b.static.resourceConsumption,
					),
				),
				...this.getResourceConsumptionModifier(),
			),
			abilityPower: BasicMaths.applyModifier(
				this.owner.abilityPower ?? DEFAULT_EFFECT_POWER,
				{ min: 0, minGuard: 0 },
				...this.getAbilityPowerModifier(),
			),
		};
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableSerializables.base];
	}

	public addRawMaterials(amount: number) {
		this.rawMaterials = BasicMaths.applyModifier(this.rawMaterials || 0, {
			bias: BasicMaths.applyModifier(amount, ...this.getMaterialOutputModifier()),
			min: 0,
			max: this.currentEconomy.maxRawMaterials,
		});
		return this;
	}

	public addEnergy(amount: number) {
		amount = BasicMaths.applyModifier(amount, ...this.getEnergyOutputModifier());
		this.energyProducedThisTurn += amount;
		return this;
	}

	/**
	 * Distribute energy, disabling unpowered buildings if necessary and accumulating the remainders
	 * @returns {this}
	 */
	public energyCycle() {
		if (this.energyProducedThisTurn) {
			const buildingsWithMaintenance = this.getAllBuildings().filter((b) => b.energyMaintenanceCost > 0);
			buildingsWithMaintenance.sort((x, y) => (y.energyMaintenanceCost || 0) - (x.energyMaintenanceCost || 0));
			for (const building of buildingsWithMaintenance) {
				if (this.currentEconomy.energyConsumption <= this.energyProducedThisTurn) break;
				building.disabled = true;
			}
		}
		const energyLeft = this.energyProducedThisTurn - this.currentEconomy.energyConsumption;
		if (energyLeft)
			this.energyCapacity = BasicMaths.applyModifier(this.energyCapacity || 0, {
				bias: energyLeft,
				max: this.currentEconomy.maxEnergyCapacity,
			});
		this.energyProducedThisTurn = 0;
		return this;
	}

	public addResource(id: Resources.resourceId, amount: number) {
		this.owner.addResource(
			id,
			BasicMaths.applyModifier(amount, ...this.getResourceOutputModifier().map((r) => r[id] || {})),
		);
		return this;
	}

	public spendResource(payload: Resources.TResourcePayload) {
		const adjustedPayload = applyResourceModifier(payload, ...this.getResourceConsumptionModifier());
		if (!this.owner.hasEnoughResources(adjustedPayload)) return false;
		this.owner.spendResources(adjustedPayload);
		return true;
	}

	public getBuildingCount<T extends TBaseBuildingType>(buildingType?: T): number {
		return buildingType
			? this.buildings[buildingType].filter(Boolean).length
			: Series.sum(
					Object.values(this.buildings).map((b: Array<TBaseTypedBuilding<any>>) => b.filter(Boolean).length),
			  );
	}

	public addDeploy(): this {
		this.deploys = BasicMaths.applyModifier(this.deploys || 0, {
			bias: 1,
			max: MAX_DEPLOYS_PER_TURN,
		});
		return this;
	}

	public deployUnit(unit: Units.IUnit) {
		if (!this.deploys) return null;
		if (!this.unitsAtBay.find((u) => u.getInstanceId() === unit.getInstanceId())) return null;
		this.deploys--;
		this.removeUnitFromBay(unit);
		return unit;
	}

	public getBuildingDefinition<T extends TBaseBuildingType>(buildingType: T, id: number) {
		const p = this.getBuildingProvider(buildingType);
		return (p.get as (id: number) => TBaseTypedBuildingDefinition<T>)(id);
	}

	public getBuildingComputedCost<T extends TBaseBuildingType>(
		buildingType: T,
		id: number,
	): Resources.TResourcePayload {
		return applyResourceModifier(
			this.getBuildingDefinition(buildingType, id).buildingCost,
			...this.getResourceConsumptionModifier(),
		);
	}

	isBuildingAvailableToBuild<T extends TBaseBuildingType>(buildingType: T, id: number): boolean {
		const buildingTier = this.getBuildingDefinition(buildingType, id).buildingTier;
		if (
			this.buildingSlots[buildingType] <= this.buildings[buildingType].length ||
			this.buildings[buildingType].map((v) => v.static.buildingTier).filter((v) => v !== buildingTier).length >=
				BASE_TIER_LIMIT ||
			(this.owner && !this.owner.hasEnoughResources(this.getBuildingComputedCost(buildingType, id)))
		) {
			return false;
		}
		return true;
	}

	public addBuilding<T extends TBaseBuildingType>(buildingType: T, id: number) {
		const price = this.getBuildingComputedCost(buildingType, id);
		if (!this.isBuildingAvailableToBuild(buildingType, id)) {
			console.error(`Can't build ${buildingType}:${id} at Base ${this.getInstanceId()}`);
			return this;
		}
		this.owner?.spendResources(price);
		(this.buildings[buildingType] as Array<TBaseTypedBuilding<T>>).push(
			this.getBuildingProvider(buildingType).instantiate(null, id) as TBaseTypedBuilding<T>,
		);
		return this;
	}

	public demolishBuilding<T extends TBaseBuildingType>(buildingType: T, buildingId: string) {
		const stack = this.buildings[buildingType] as Array<TBaseTypedBuilding<T>>;
		const removedBuildingIndex = stack.findIndex((b) => b.getInstanceId() === buildingId);
		if (removedBuildingIndex < 0) return this;
		const removedBuilding = stack[removedBuildingIndex];
		this.addRawMaterials(removedBuilding.getDemolishMaterialsValue());
		this.buildings[buildingType].splice(removedBuildingIndex, 1);
		this.getBuildingProvider(buildingType).destroy(removedBuilding.getInstanceId());
		return this;
	}

	public serialize(): TSerializedPlayerBase {
		return {
			...this.__serializePlayerProperty(),
			rawMaterials: this.rawMaterials,
			maxRawMaterials: this.maxRawMaterials,
			bayCapacity: this.bayCapacity,
			intelligenceLevel: this.intelligenceLevel,
			productionCapacity: this.productionCapacity,
			productionCapacityLeft: this.productionCapacityLeft,
			counterIntelligenceLevel: this.counterIntelligenceLevel,
			currentHitPoints: this.currentHitPoints,
			maxEnergyCapacity: this.maxEnergyCapacity,
			energyCapacity: this.energyCapacity,
			maxHitPoints: this.maxHitPoints,
			stats: this.stats,
			deploys: this.deploys,
			unitsAtBay: this.unitsAtBay.map((u) => u.serialize()),
			buildingSlots: this.buildingSlots,
			armor: this.armor,
			energyProducedThisTurn: this.energyProducedThisTurn,
			effects: this.effects.map((v) => v.serialize()),
			kit: this.kit.save(),
			buildings: _.mapObject(
				this.buildings,
				<T extends TBaseBuildingType>(buildings: Array<TBaseTypedBuilding<T>>) =>
					buildings.map((b) => b.serialize()),
			) as TBaseBuildingSerializedArray,
		};
	}

	public putUnitToBay(unit: Units.IUnit): boolean {
		if (this.unitsAtBay.length >= this.bayCapacity) return false;
		unit.location = null;
		this.unitsAtBay.push(unit);
		return true;
	}

	public removeUnitFromBay(unit: Units.IUnit): boolean {
		if (this.unitsAtBay.length) {
			const i = this.unitsAtBay.findIndex((u) => u.getInstanceId() === unit.getInstanceId());
			if (i > 0) {
				this.unitsAtBay.splice(i, 1);
				return true;
			}
		}
		return false;
	}

	public applyAttackUnit(a: UnitAttack.TAttackUnit, source?: StatusEffectMeta.TStatusEffectTarget) {
		const result = applyAttackUnitToDefense(a, {
			armor: this.armor,
		});
		if (result.breach) {
			const dmg = DamageManager.getTotalDamageValue(result.throughDamage);
			this.dealHullDamage(dmg, source?.playerId);
		}
		return this;
	}

	public dealHullDamage(dmg: number, sourcePlayerId: TPlayerId) {
		this.currentHitPoints = this.currentHitPoints - dmg;
		if (this.currentHitPoints < 0) this.__baseDestroyed(sourcePlayerId);
		return this;
	}

	public canUnitBeDeployedToPlanet(unit: Units.IUnit, planetProps: TPlanetSurfaceProps): boolean {
		switch (true) {
			case planetProps.warpFactor >= 0.75 && unit.targetFlags[UnitChassis.targetType.massive]:
				return false;
			case planetProps.magneticField <= 0.25 && unit.movement === UnitChassis.movementType.hover:
				return false;
			case (planetProps.atmosphericDensity <= 0.25 || planetProps.atmosphericDensity >= 0.75) &&
				unit.movement === UnitChassis.movementType.air:
				return false;
			case planetProps.waterPresence <= 0 && unit.movement === UnitChassis.movementType.naval:
				return false;
			default:
				return true;
		}
	}

	public hasEffect(effectId: number): boolean {
		return this.effects.filter((ef) => ef.static.id === effectId).length > 0;
	}

	public claimSalvage(salvage: Salvage.ISalvage) {
		this.addRepair(salvage?.payload?.rawMaterials || 0);
		this.owner?.addTechnologies(salvage?.payload?.technologies);
		this.owner?.addResourcePayload(salvage?.payload?.resources);
		this.stats.salvagePicked = getBaseStatProperty(this, 'salvagePicked') + 1;
		return this;
	}

	public addRepair(repairPoints: number) {
		let rp = BasicMaths.applyModifier(repairPoints, ...this.getRepairOutputModifier());
		if (rp < 0) return this;
		const missingHp = this.currentEconomy.maxHitPoints - this.currentHitPoints;
		if (missingHp) {
			if (rp < missingHp) {
				this.currentHitPoints += rp;
				return this;
			}
			rp -= missingHp;
			this.currentHitPoints = this.currentEconomy.maxHitPoints;
		}
		let bayIndex = 0;
		while (rp > 0 && bayIndex < this.currentEconomy.bayCapacity) {
			const u = this.unitsAtBay[bayIndex];
			bayIndex++;
			if (!u) continue;
			const missingUnitHp = u.maxHitPoints - u.currentHitPoints;
			if (missingUnitHp) {
				if (rp < missingUnitHp) {
					u.currentHitPoints += rp;
					return this;
				}
				rp -= missingUnitHp;
				u.maxHitPoints = u.currentHitPoints;
			}
		}
		if (rp > 0)
			// converting the remainings to materials
			this.addRawMaterials(rp * REPAIR_TO_MATERIALS_CONVERSION);
		return this;
	}

	public instantiateAbilities(
		chosenAbilities: ValueOf<GameData.TGameContainerInstantiateProps['chosenAbilities']>,
	): Abilities.TAbilityOption[] {
		return []
			.concat(chosenAbilities, this.currentEconomy.availableAbilities)
			.filter(Boolean)
			.map((abilityId) => this.getProvider(DIInjectableCollectibles.ability).instantiate(null, abilityId));
	}

	public applyEffect(eff: TEffectApplianceItem, source: StatusEffectMeta.TStatusEffectTarget = this): this {
		if (this.getProvider(DIInjectableCollectibles.effect).get(eff.effectId).targetType !== this.effectTargetType)
			return null;
		const newEffect = this.getProvider(
			DIInjectableCollectibles.effect,
		).instantiate<StatusEffectMeta.effectAgentTypes.player>(eff.props, eff.effectId, source, this);
		if (source === this && this.kit) newEffect.power = this.kit.abilityPower;
		return this.addEffect(newEffect);
	}

	public getStatusEffects(): Array<IStatusEffect<StatusEffectMeta.effectAgentTypes.player>> {
		return this.effects;
	}

	public addEffect(effect: IStatusEffect<StatusEffectMeta.effectAgentTypes.player>) {
		if (effect.static.targetType !== StatusEffectMeta.effectAgentTypes.player) return this;
		// @TODO
		return this;
	}

	public removeEffect(effectId: string) {
		const removedEffect = this.effects.find((e) => e.getInstanceId() === effectId);
		if (removedEffect && !removedEffect.static.unremovable) {
			removedEffect.state = removedEffect.triggerEvent(EffectEvents.TPlayerEffectEvents.expire);
			this.effects = this.effects.filter((e) => e.getInstanceId() === effectId);
		}
		return this;
	}

	public callEffectEvent<T extends EffectEvents.TPlayerEffectEvents>(
		payload?: EffectEvents.TEffectEventMeta<StatusEffectMeta.effectAgentTypes.player, T>,
	) {
		this.getStatusEffects()
			.sort((a, b) => a.priority - b.priority)
			.forEach((eff) => {
				eff.triggerEvent(payload.event, payload);
			});
		return this;
	}

	protected getAbilityPowerModifier() {
		return this.getAllBuildings()
			.map((b) => b.static.abilityPowerModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.abilityPower))
			.filter(Boolean);
	}

	protected getResourceConsumptionModifier(): Resources.TResourceModifier[] {
		return this.getAllBuildings()
			.map((b) => b.static.resourceConsumptionModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.resourceConsumption))
			.filter(Boolean);
	}

	protected getMaterialOutputModifier() {
		return this.getAllBuildings()
			.map((b) => b.static.materialOutputModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.materialOutput))
			.filter(Boolean);
	}

	protected getEnergyOutputModifier() {
		return this.getAllBuildings()
			.map((b) => b.static.energyOutputModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.energyOutput))
			.filter(Boolean);
	}

	protected getEnergyConsumptionModifier() {
		return this.getAllBuildings()
			.map((b) => b.static.energyConsumptionModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.energyConsumption))
			.filter(Boolean);
	}

	protected getResourceOutputModifier(): Resources.TResourceModifier[] {
		return this.getAllBuildings()
			.map((b) => b.static.resourceOutputModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.resourceOutput))
			.filter(Boolean);
	}

	protected getRepairOutputModifier() {
		return this.getAllBuildings()
			.map((b) => b.static.repairSpeedModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.repairOutput))
			.filter(Boolean);
	}

	protected getEnergyCapacityModifier() {
		return this.getAllBuildings()
			.map((b) => b.static.maxEnergyCapacity)
			.concat(this.owner.getBaseModifiers().map((m) => m.maxEnergyCapacity))
			.filter(Boolean);
	}

	protected getEngineeringOutputModifier() {
		return this.getAllBuildings()
			.map((b) => b.static.engineeringOutputModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.engineeringOutput))
			.filter(Boolean);
	}

	protected getResearchOutputModifier() {
		return this.getAllBuildings()
			.map((b) => b.static.researchOutputModifier)
			.concat(this.owner.getBaseModifiers().map((m) => m.researchOutput))
			.filter(Boolean);
	}

	protected getBuildingProvider<T extends TBaseBuildingType>(type: T) {
		return BuildingManager.getProvider(type);
	}

	protected getAllBuildings(): Array<TBaseTypedBuilding<any>> {
		return Object.keys(this.buildings)
			.reduce((arr, type: TBaseBuildingType) => arr.concat(this.buildings[type]), [])
			.filter(Boolean);
	}

	protected __baseDestroyed(sourcePlayerId: TPlayerId) {
		// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/12
	}
}

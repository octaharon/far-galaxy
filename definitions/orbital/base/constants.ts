import Resources from '../../world/resources/types';

// N units of resource gets converted into Raw Materials
export const BUILDING_DEMOLISH_CONVERSION_RATIO: Resources.TResourcePayload = {
	[Resources.resourceId.halogens]: 25,
	[Resources.resourceId.minerals]: 100,
	[Resources.resourceId.ores]: 100,
	[Resources.resourceId.carbon]: 200,
	[Resources.resourceId.hydrogen]: 200,
	[Resources.resourceId.helium]: 20,
	[Resources.resourceId.isotopes]: 10,
	[Resources.resourceId.proteins]: 10,
	[Resources.resourceId.oxygen]: 25,
	[Resources.resourceId.nitrogen]: 150,
};
export const REPAIR_TO_MATERIALS_CONVERSION = 0.4;
export const MAX_DEPLOYS_PER_TURN = 3;
export const DEPLOY_ZONE_RADIUS = 4;
export const BASE_TIER_LIMIT = 4;
export const FACILITY_ENERGY_BASE_OUTPUT = 40;
export const PLAYER_BASE_MAX_BUILDING_SLOTS = 7;
export const SALVAGE_RAW_MATERIALS_FACTOR = 0.5;

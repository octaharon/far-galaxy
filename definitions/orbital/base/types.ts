import { ISerializableFactory } from '../../core/Serializable';
import GameData from '../../game/types';
import { TPlayerId } from '../../player/playerId';
import { IPlayerProperty, IPlayerPropertyProps } from '../../player/types';
import { TPlanetSurfaceProps } from '../../space/Planets';
import GameStats from '../../stats/types';
import Abilities from '../../tactical/abilities/types';
import UnitAttack from '../../tactical/damage/attack';
import UnitDefense from '../../tactical/damage/defense';
import Salvage from '../../tactical/salvage/types';
import StatusEffectMeta from '../../tactical/statuses/statusEffectMeta';
import Units from '../../tactical/units/types';
import Resources from '../../world/resources/types';
import {
	IEstablishment,
	IEstablishmentProvider,
	TEstablishment,
	TSerializedEstablishment,
} from '../establishments/types';
import { IFacility, IFacilityProvider, TFacility, TSerializedFacility } from '../facilities/types';
import { IFactory, IFactoryProvider, TFactory, TSerializedFactory } from '../factories/types';
import { TBaseBuildingType } from '../types';

export type TPlayerBaseStaticProps = {
	rawMaterials: number;
	maxRawMaterials: number;
	bayCapacity: number;
	intelligenceLevel: number;
	counterIntelligenceLevel: number;
	productionCapacity: number;
	productionCapacityLeft: number;
	energyCapacity: number;
	maxEnergyCapacity: number;
	energyProducedThisTurn: number;
	maxHitPoints: number;
	deploys: number;
	currentHitPoints: number;
	armor: UnitDefense.TArmor;
	buildingSlots: EnumProxy<TBaseBuildingType, number>;
	stats: GameStats.TBaseStats;
} & IPlayerPropertyProps;

export type TBaseTypedBuilding<T extends TBaseBuildingType> = T extends TBaseBuildingType.establishment
	? IEstablishment
	: T extends TBaseBuildingType.facility
	? IFacility
	: T extends TBaseBuildingType.factory
	? IFactory
	: never;

export type TBaseTypedBuildingDefinition<T extends TBaseBuildingType> = T extends TBaseBuildingType.factory
	? TFactory
	: T extends TBaseBuildingType.establishment
	? TEstablishment
	: T extends TBaseBuildingType.facility
	? TFacility
	: never;
export type TBaseTypedSerializedBuilding<T extends TBaseBuildingType> = T extends TBaseBuildingType.establishment
	? TSerializedEstablishment
	: T extends TBaseBuildingType.facility
	? TSerializedFacility
	: T extends TBaseBuildingType.factory
	? TSerializedFactory
	: never;

export type TBaseTypedProvider<T extends TBaseBuildingType> = T extends TBaseBuildingType.establishment
	? IEstablishmentProvider
	: T extends TBaseBuildingType.facility
	? IFacilityProvider
	: T extends TBaseBuildingType.factory
	? IFactoryProvider
	: never;

export function isFacilityBuildingType(type: TBaseBuildingType): type is TBaseBuildingType.facility {
	return type === TBaseBuildingType.facility;
}

export function isEstablishmentBuildingType(type: TBaseBuildingType): type is TBaseBuildingType.establishment {
	return type === TBaseBuildingType.establishment;
}

export function isFactoryBuildingType(type: TBaseBuildingType): type is TBaseBuildingType.factory {
	return type === TBaseBuildingType.factory;
}

export type TBaseBuildingArray = {
	[K in TBaseBuildingType]: Array<TBaseTypedBuilding<K>>;
};

export type TBaseBuildingSerializedArray = {
	[K in TBaseBuildingType]: Array<TBaseTypedSerializedBuilding<K>>;
};

export type TPlayerBaseDynamicProps = StatusEffectMeta.TEffectOwner<StatusEffectMeta.effectAgentTypes.player> & {
	unitsAtBay: Units.IUnit[];
	buildings: TBaseBuildingArray;
};

export type TPlayerBaseProps = TPlayerBaseDynamicProps & TPlayerBaseStaticProps;
export type TPlayerBaseSerializedProps = {
	unitsAtBay: Units.TSerializedUnit[];
	buildings: TBaseBuildingSerializedArray;
};

export type TSerializedPlayerBase = Abilities.ISerializedWithAbilityKit &
	TPlayerBaseSerializedProps &
	TPlayerBaseStaticProps &
	StatusEffectMeta.TSerializedEffectOwner<StatusEffectMeta.effectAgentTypes.player>;

export interface IPlayerBase
	extends Abilities.IWithAbilityKit,
		TPlayerBaseProps,
		TPlayerBaseDynamicProps,
		StatusEffectMeta.IStatusEffectTarget<
			TPlayerBaseProps & TPlayerBaseDynamicProps,
			TSerializedPlayerBase,
			StatusEffectMeta.effectAgentTypes.player
		>,
		IPlayerProperty {
	currentEconomy: TPlayerBaseComputedProps;
	totalBuildingCost: Resources.TResourcePayload;

	getBuildingDefinition<T extends TBaseBuildingType>(buildingType: T, id: number): TBaseTypedBuildingDefinition<T>;

	getBuildingCount<T extends TBaseBuildingType>(buildingType?: T): number;

	getBuildingComputedCost<T extends TBaseBuildingType>(buildingType: T, id: number): Resources.TResourcePayload;

	isBuildingAvailableToBuild<T extends TBaseBuildingType>(buildingType: T, id: number): boolean;

	addBuilding(buildingType: TBaseBuildingType, id: number): this;

	addDeploy(): this;

	energyCycle(): this;

	addEnergy(amount: number): this;

	addRepair(repairPoints: number): this;

	deployUnit(unit: Units.IUnit): Units.IUnit;

	addRawMaterials(amount: number): this;

	addResource(id: Resources.resourceId, amount: number): this;

	spendResource(payload: Resources.TResourcePayload): boolean;

	demolishBuilding(buildingType: TBaseBuildingType, buildingId: string): this;

	putUnitToBay(unit: Units.IUnit): boolean;

	removeUnitFromBay(unit: Units.IUnit): boolean;

	applyAttackUnit(a: UnitAttack.TAttackUnit): this;

	dealHullDamage(dmg: number, sourcePlayerId: TPlayerId): this;

	canUnitBeDeployedToPlanet(unit: Units.IUnit, planetProps: TPlanetSurfaceProps): boolean;

	instantiateAbilities(
		chosenAbilities: ValueOf<GameData.TGameContainerInstantiateProps['chosenAbilities']>,
	): Abilities.TAbilityOption[];

	claimSalvage(salvage: Salvage.ISalvage): this;
}

export type IPlayerBaseFactory = ISerializableFactory<TPlayerBaseProps, TSerializedPlayerBase, IPlayerBase>;

export type TPlayerBaseComputedProps = {
	energyConsumption: number;
	energyOutput: number;
	researchOutput: number;
	maxRawMaterials: number;
	resourceOutput: Resources.TResourcePayload;
	resourceConsumption: Resources.TResourcePayload;
	materialOutput: number;
	maxEnergyCapacity: number;
	repairOutput: number;
	engineeringOutput: number;
	productionCapacity: number;
	maxHitPoints: number;
	bayCapacity: number;
	intelligenceLevel: number;
	counterIntelligenceLevel: number;
	armor: UnitDefense.TArmor;
	abilityPower: number;
	availableAbilities: number[];
};
export type TPlayerBaseModifier = Partial<
	{
		[K in keyof TPlayerBaseComputedProps]: TPlayerBaseComputedProps[K] extends number
			? IModifier
			: TPlayerBaseComputedProps[K] extends Resources.TResourcePayload
			? Resources.TResourceModifier
			: never;
	} & {
		productionSpeed: IModifier;
	}
>;

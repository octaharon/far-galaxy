import BasicMaths from '../../maths/BasicMaths';
import Players from '../../player/types';
import { describePrototypeModifier } from '../../prototypes/helpers';
import DefenseManager from '../../tactical/damage/DefenseManager';
import Units from '../../tactical/units/types';
import { ChassisClassTitles } from '../../technologies/chassis';
import UnitChassis from '../../technologies/types/chassis';
import { describeResourceModifier } from '../../world/resources/helpers';
import { IFactory } from '../factories/types';
import BuildingManager from '../GenericBaseBuildingManager';
import { IPlayerBaseCycleModifier, TTypedBaseBuilding } from '../types';
import { IPlayerBase } from './types';
import IPlayer = Players.IPlayer;
import IUnit = Units.IUnit;

export const getUnitsAtBayByChassis = (base: IPlayerBase, chassisType: UnitChassis.chassisClass = null): IUnit[] =>
	base?.unitsAtBay?.filter(chassisType ? (u) => u.chassisType === chassisType : Boolean) || [];
export const getPlayerUnitsAtBayByChassis = (player: IPlayer, chassisType: UnitChassis.chassisClass = null): IUnit[] =>
	Object.values(player?.bases ?? {}).flatMap((b) => getUnitsAtBayByChassis(b, chassisType));
export const describeBaseCycleModifier = (mod: IPlayerBaseCycleModifier) => {
	const r: string[] = [];
	if (mod.abilityPowerModifier)
		r.push(`${BasicMaths.describeModifier(mod.abilityPowerModifier, true)} Base Ability Power`);
	if (mod.productionCapacity)
		r.push(`${BasicMaths.describeModifier(mod.productionCapacity, true)} Production Capacity`);
	if (mod.bayCapacity) r.push(`${BasicMaths.describeModifier(mod.bayCapacity, true)} Bay Capacity`);
	if (mod.energyConsumptionModifier)
		r.push(`${BasicMaths.describeModifier(mod.energyConsumptionModifier, true)} Energy Consumption`);
	if (mod.maxRawMaterials) r.push(`${BasicMaths.describeModifier(mod.maxRawMaterials, true)} Raw Materials Storage`);
	if (mod.maxEnergyCapacity) r.push(`${BasicMaths.describeModifier(mod.maxEnergyCapacity, true)} Energy Storage`);
	if (mod.energyOutputModifier)
		r.push(`${BasicMaths.describeModifier(mod.energyOutputModifier, true)} Energy Output`);
	if (mod.engineeringOutputModifier)
		r.push(`${BasicMaths.describeModifier(mod.engineeringOutputModifier, true)} Engineering Output`);
	if (mod.researchOutputModifier)
		r.push(`${BasicMaths.describeModifier(mod.researchOutputModifier, true)} Research Output`);
	if (mod.intelligenceLevel) r.push(`${BasicMaths.describeModifier(mod.intelligenceLevel, true)} Recon Level`);
	if (mod.materialOutputModifier)
		r.push(`${BasicMaths.describeModifier(mod.materialOutputModifier, true)} Raw Materials Output`);
	if (mod.repairSpeedModifier) r.push(`${BasicMaths.describeModifier(mod.repairSpeedModifier, true)} Repair Output`);
	if (mod.resourceConsumptionModifier)
		r.push(
			...describeResourceModifier(mod.resourceConsumptionModifier)
				.split(', ')
				.map((s) => s + ' Consumption'),
		);
	if (mod.resourceOutputModifier)
		r.push(
			...describeResourceModifier(mod.resourceOutputModifier)
				.split(', ')
				.map((s) => s + ' Output'),
		);
	return r;
};

export function describeBuildingModifier(building: TTypedBaseBuilding) {
	const r = describeBaseCycleModifier(building.static);
	if (BuildingManager.isFacility(building)) {
		if (building.static.maxHitPoints)
			r.push(`${BasicMaths.describeModifier(building.static.maxHitPoints)} Base Hit Points`);
	}
	if (BuildingManager.isEstablishment(building)) {
		if (building.static.counterIntelligenceLevel) {
			r.push(`${BasicMaths.describeModifier(building.static.counterIntelligenceLevel)} Counter-Recon Level`);
		}
		if (building.static.armor) {
			r.push(
				...DefenseManager.describeProtection(building.static.armor)
					.split('|')
					.map((s) => `${s.trim()} Armor`),
			);
		}
	}
	return r;
}

export function describeFactoryUnitModifiers(building: IFactory) {
	const s: string[] = [];
	if (!building?.static?.productionModifierByChassis) return s;
	const ch = Object.keys(building.static.productionModifierByChassis);
	const defaultOnly = ch.length === 1 && ch.includes('default');
	ch.forEach((c) => {
		const chassisTitle = c === 'default' ? (defaultOnly ? '' : 'Other Units') : ChassisClassTitles[c];
		const mod = building.static.productionModifierByChassis[c];
		if (mod?.productionSpeed)
			s.push(`${BasicMaths.describeModifier(mod.productionSpeed)} ${chassisTitle} Production Speed`);
		if (mod?.prototypeModifier) {
			s.push(...describePrototypeModifier(mod.prototypeModifier, chassisTitle));
		}
	});
	return s.map((t) => t.replace(/\s+/gi, ' ').trim()).filter(Boolean);
}

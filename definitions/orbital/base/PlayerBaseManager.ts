import { DIInjectableCollectibles, DIInjectableSerializables } from '../../core/DI/injections';
import { SerializableFactory } from '../../core/Serializable';
import { AbilityKit } from '../../tactical/abilities/AbilityKit';
import { TBaseBuildingType } from '../types';
import PlayerBase, { defaultPlayerBaseProps } from './PlayerBase';
import { IPlayerBase, IPlayerBaseFactory, TPlayerBaseProps, TSerializedPlayerBase } from './types';

class PlayerBaseManager
	extends SerializableFactory<TPlayerBaseProps, TSerializedPlayerBase, IPlayerBase>
	implements IPlayerBaseFactory
{
	public instantiate(props: Partial<TPlayerBaseProps>) {
		return this.inject(
			this.__instantiateSerializable(
				{
					...defaultPlayerBaseProps,
					...(props || {}),
				},
				PlayerBase,
			),
		);
	}

	public unserialize(t: TSerializedPlayerBase) {
		if (!t) return null;
		const c = this.instantiate({
			...t,
			effects: t.effects.map((e) => this.getProvider(DIInjectableCollectibles.effect).unserialize(e)),
			buildings: {
				[TBaseBuildingType.establishment]: (t.buildings[TBaseBuildingType.establishment] || []).map((b) =>
					this.getProvider(DIInjectableCollectibles.establishment).unserialize(b),
				),
				[TBaseBuildingType.facility]: (t.buildings[TBaseBuildingType.facility] || []).map((b) =>
					this.getProvider(DIInjectableCollectibles.facility).unserialize(b),
				),
				[TBaseBuildingType.factory]: (t.buildings[TBaseBuildingType.factory] || []).map((b) =>
					this.getProvider(DIInjectableCollectibles.factory).unserialize(b),
				),
			},
			unitsAtBay: t.unitsAtBay.map((u) => this.getProvider(DIInjectableSerializables.unit).unserialize(u)),
		});
		c.kit = new AbilityKit(c, t.kit);
		c.setInstanceId(t.instanceId);
		return c;
	}
}

export default PlayerBaseManager;

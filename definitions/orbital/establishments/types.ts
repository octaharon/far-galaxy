import { ICollectibleFactory, TCollectible } from '../../core/Collectible';
import {
	IBaseBuilding,
	IBaseBuildingStatic,
	IBaseBuildingUpgrade,
	IPlayerBaseCycleModifier,
	IPlayerBaseEstablishmentModifier,
	TBaseBuildingProps,
	TBaseBuildingType,
	TSerializedBaseBuilding,
} from '../types';

export type TEstablishmentID = number;
export type TEstablishmentProps = {
	researchOutput: number;
	engineeringOutput: number;
} & TBaseBuildingProps;
export type TSerializedEstablishment = TEstablishmentProps & TSerializedBaseBuilding;

export interface IEstablishment
	extends TEstablishmentProps,
		IBaseBuilding<
			TBaseBuildingType.establishment,
			TEstablishmentProps,
			TSerializedEstablishment,
			IEstablishmentStatic,
			TEstablishmentUpgrade
		> {}

export type TEstablishment = TCollectible<IEstablishment, IEstablishmentStatic>;

export type TEstablishmentUpgrade = IBaseBuildingUpgrade<TEstablishmentProps>;

export interface IEstablishmentMeta {}

export interface IEstablishmentStatic
	extends IBaseBuildingStatic<TBaseBuildingType.establishment, TEstablishmentProps, TEstablishmentUpgrade>,
		IPlayerBaseEstablishmentModifier {}

export type IEstablishmentProvider = ICollectibleFactory<
	TEstablishmentProps,
	TSerializedEstablishment,
	IEstablishment,
	IEstablishmentStatic
>;

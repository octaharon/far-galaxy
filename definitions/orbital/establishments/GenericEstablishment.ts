import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import { GenericBaseBuilding } from '../GenericBaseBuilding';
import { TBaseBuildingType } from '../types';
import {
	IEstablishment,
	IEstablishmentStatic,
	TEstablishmentProps,
	TEstablishmentUpgrade,
	TSerializedEstablishment,
} from './types';

export class GenericEstablishment
	extends GenericBaseBuilding<
		TBaseBuildingType.establishment,
		TEstablishmentProps,
		TSerializedEstablishment,
		IEstablishmentStatic,
		TEstablishmentUpgrade
	>
	implements IEstablishment
{
	public static readonly type = TBaseBuildingType.establishment;
	public static readonly id: number = 0;
	public static readonly researchOutputModifier: IModifier = {
		bias: 2,
	};
	public static readonly upgrades: TEstablishmentUpgrade[] = [
		{
			caption: 'Sprint',
			researchOutput: {
				factor: 1.2,
			},
			engineeringOutput: {
				bias: -5,
				min: 0,
				minGuard: 0,
			},
			energyMaintenanceCost: {
				bias: 2,
			},
		},
		{
			caption: 'Grooming',
			engineeringOutput: {
				bias: 2,
			},
			researchOutput: {
				bias: 2,
			},
			energyMaintenanceCost: {
				factor: 1.15,
			},
		},
		{
			caption: 'Retrospective',
			researchOutput: {
				bias: -5,
				min: 0,
				minGuard: 0,
			},
			engineeringOutput: {
				factor: 1.1,
			},
			energyMaintenanceCost: {
				factor: 0.9,
				min: 10,
				minGuard: 10,
			},
		},
	];
	public engineeringOutput: number;
	public researchOutput: number;

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.establishment];
	}

	public serialize(): TSerializedEstablishment {
		return {
			...this.__serializeBuilding(),
			engineeringOutput: this.engineeringOutput,
			researchOutput: this.researchOutput,
		};
	}
}

export default GenericEstablishment;

import GenericEstablishment from './GenericEstablishment';
import { TEstablishment } from './types';

let EstablishmentList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), EstablishmentList);
EstablishmentList = EstablishmentList.filter(
	(module: any) => module.default.prototype instanceof GenericEstablishment && module.default.id > 0,
).map((module: any) => module.default);
export default EstablishmentList as TEstablishment[];

import { CollectibleFactory } from '../../core/Collectible';
import BuildingManager from '../GenericBaseBuildingManager';
import EstablishmentList from './index';
import {
	IEstablishment,
	IEstablishmentProvider,
	IEstablishmentStatic,
	TEstablishmentProps,
	TSerializedEstablishment,
} from './types';

export const defaultEstablishmentProps: Partial<TEstablishmentProps> = {
	disabled: false,
};

export class EstablishmentManager
	extends CollectibleFactory<TEstablishmentProps, TSerializedEstablishment, IEstablishment, IEstablishmentStatic>
	implements IEstablishmentProvider
{
	public instantiate(props: Partial<TEstablishmentProps> = {}, id: number) {
		const c = this.__instantiateCollectible(
			{
				...defaultEstablishmentProps,
				...props,
			},
			id,
		);
		return c;
	}

	public store(o: IEstablishment, callback?: (a: IEstablishment) => void) {
		super.store(BuildingManager.preStore(o), (v) => callback(BuildingManager.postStore(v)));
		return this;
	}

	public fill() {
		this.__fillItems(EstablishmentList);
		return this;
	}
}

export default EstablishmentManager;

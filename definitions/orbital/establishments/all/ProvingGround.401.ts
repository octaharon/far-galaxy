import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class ProvingGroundEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 401;
	public static readonly caption: string = 'Proving Grounds';
	public static readonly description: string = `Advanced training Establishment performing automated combat tests and simulations`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 3490,
		[Resources.resourceId.oxygen]: 700,
		[Resources.resourceId.helium]: 650,
		[Resources.resourceId.halogens]: 150,
		[Resources.resourceId.carbon]: 2250,
	};
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.minerals]: {
			factor: 0.95,
		},
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.minerals]: {
			factor: 1.05,
		},
	};
	public static readonly productionCapacity: IModifier = {
		bias: 10,
	};
	public static readonly bayCapacity: IModifier = {
		bias: 1,
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.85;
	public engineeringOutput = 35;
}

export default ProvingGroundEstablishment;

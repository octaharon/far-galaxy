import WeatherStormAbility from '../../../tactical/abilities/all/Orbital/WeatherStorm.60004';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class FrequencyArrayEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 203;
	public static readonly caption: string = 'Frequency Array';
	public static readonly description: string = `Densely packed collection of various transmission and survey devices, providing a deeper look into what's happening on the planet surface`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 6000,
		[Resources.resourceId.helium]: 1050,
		[Resources.resourceId.oxygen]: 1350,
		[Resources.resourceId.nitrogen]: 2950,
		[Resources.resourceId.hydrogen]: 1950,
		[Resources.resourceId.minerals]: 4000,
		[Resources.resourceId.isotopes]: 60,
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 5,
	};
	public static readonly energyConsumptionModifier: IModifier = {
		factor: 1.05,
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.25,
	};
	public static readonly abilities: number[] = [WeatherStormAbility.id];
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.carbon]: {
			factor: 1.03,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 1.03,
		},
	};
	public researchOutput = 12;
	public engineeringOutput = 10;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.35;
}

export default FrequencyArrayEstablishment;

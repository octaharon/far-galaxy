import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class HolostageEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 501;
	public static readonly caption: string = 'Holostage';
	public static readonly description: string = `Holographic staging establishment that heavily boosts the overall productivity of the Base and provides additional storage space`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.proteins]: 750,
		[Resources.resourceId.isotopes]: 150,
		[Resources.resourceId.helium]: 450,
		[Resources.resourceId.carbon]: 3200,
		[Resources.resourceId.oxygen]: 2500,
	};
	public static readonly maxRawMaterials: IModifier = {
		bias: 50,
	};
	public static readonly energyConsumptionModifier: IModifier = {
		factor: 0.95,
	};
	public static readonly materialOutputModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly engineeringOutputModifier: IModifier = {
		bias: 3,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 15,
	};
	public static readonly researchOutputModifier: IModifier = null;
	public static resourceConsumptionModifier: Resources.TResourceModifier = {
		default: {
			factor: 0.97,
		},
	};
	public static resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.halogens]: {
			factor: 1.05,
		},
		[Resources.resourceId.oxygen]: {
			factor: 1.05,
		},
		[Resources.resourceId.proteins]: {
			factor: 1.05,
		},
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.8;
	public engineeringOutput = 6;
	public researchOutput = 10;
}

export default HolostageEstablishment;

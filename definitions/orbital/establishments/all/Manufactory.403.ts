import MinefieldAbility from '../../../tactical/abilities/all/Orbital/Minefield.45007';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class ManufactoryEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 403;
	public static readonly caption: string = 'Manufactory';
	public static readonly description: string = `Huge plant full of technological toys and conceptual machines for engineering works. Grants Minefield Ability`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 3600,
		[Resources.resourceId.ores]: 5270,
		[Resources.resourceId.halogens]: 630,
		[Resources.resourceId.proteins]: 150,
		[Resources.resourceId.carbon]: 4160,
		[Resources.resourceId.helium]: 640,
		[Resources.resourceId.oxygen]: 500,
	};
	public static readonly energyConsumptionModifier: IModifier = {
		bias: -1,
	};
	public static readonly repairSpeedModifier: IModifier = {
		bias: 3,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 35,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.isotopes]: {
			factor: 1.05,
		},
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.1,
	};
	public static readonly abilities: number[] = [MinefieldAbility.id];

	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.25;
	public engineeringOutput = 50;
}

export default ManufactoryEstablishment;

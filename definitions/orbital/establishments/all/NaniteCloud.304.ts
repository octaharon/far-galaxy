import CelestialFlowAbility from '../../../tactical/abilities/all/Orbital/CelestialFlow.60005';
import UnitDefense from '../../../tactical/damage/defense';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class NaniteCloudEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 304;
	public static readonly caption: string = 'Nanite Cloud';
	public static readonly description: string = `A storage for Autonomous microbots that protect the Base from Orbital Attack and help to repair units and hull. Also grants a Celestial Flow ability`;
	public static readonly researchOutputModifier: IModifier = null;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 3300,
		[Resources.resourceId.carbon]: 4340,
		[Resources.resourceId.ores]: 4200,
		[Resources.resourceId.helium]: 300,
		[Resources.resourceId.proteins]: 140,
		[Resources.resourceId.isotopes]: 250,
	};
	public static readonly repairSpeedModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 400,
	};
	public static readonly counterIntelligenceLevel: IModifier = {
		bias: 2,
	};
	public static readonly abilities: number[] = [CelestialFlowAbility.id];
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.proteins]: {
			factor: 1.05,
		},
		[Resources.resourceId.minerals]: {
			factor: 1.05,
		},
	};
	public static readonly armor: UnitDefense.TArmorModifier = {
		default: {
			bias: -15,
		},
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.5,
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.6;
	public researchOutput = 12;
}

export default NaniteCloudEstablishment;

import UnitDefense from '../../../tactical/damage/defense';
import { Resources } from '../../../world/resources/types';
import GenericEstablishment from '../GenericEstablishment';

export class NanocellHullEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 300;
	public static readonly caption: string = 'Nanocell Hull';
	public static readonly description: string = `An exterior protection for the Base that provides universal damage protection from Orbital Attack`;
	public static readonly researchOutputModifier: IModifier = null;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.nitrogen]: 2200,
		[Resources.resourceId.halogens]: 400,
		[Resources.resourceId.helium]: 150,
		[Resources.resourceId.minerals]: 1180,
	};
	public static readonly repairSpeedModifier: IModifier = {
		factor: 0.9,
	};
	public static readonly armor: UnitDefense.TArmorModifier = {
		default: {
			factor: 0.9,
		},
	};
	public static readonly counterIntelligenceLevel: IModifier = {
		bias: 1,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.helium]: {
			factor: 1.03,
		},
	};
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 150,
	};
	public energyMaintenanceCost = 0;
	public researchOutput = 1;
}

export default NanocellHullEstablishment;

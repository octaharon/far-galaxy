import ForceFieldAbility from '../../../tactical/abilities/all/Orbital/ForceField.45003';
import Damage from '../../../tactical/damage/damage';
import UnitDefense from '../../../tactical/damage/defense';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class TachyonLatticeEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 204;
	public static readonly caption: string = 'Tachyon Lattice';
	public static readonly description: string = `Orbital tachyon field projector that scrambles enemies' communication and surveillance, offers the Base some protection from energy weapons and grants a Force Field ability`;
	public static readonly researchOutputModifier: IModifier = null;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.halogens]: 1200,
		[Resources.resourceId.ores]: 2450,
		[Resources.resourceId.nitrogen]: 1250,
		[Resources.resourceId.minerals]: 4700,
		[Resources.resourceId.helium]: 650,
		[Resources.resourceId.isotopes]: 460,
	};
	public static readonly counterIntelligenceLevel: IModifier = {
		bias: 5,
	};
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 400,
	};
	public static readonly abilities: number[] = [ForceFieldAbility.id];
	public static resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.helium]: {
			factor: 1.03,
		},
		[Resources.resourceId.oxygen]: {
			factor: 1.03,
		},
	};
	public static readonly armor: UnitDefense.TArmorModifier = {
		[Damage.damageType.heat]: {
			factor: 0.7,
		},
		[Damage.damageType.magnetic]: {
			factor: 0.7,
		},
		[Damage.damageType.radiation]: {
			factor: 0.7,
		},
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.1,
	};
	public researchOutput = 8;
	public engineeringOutput = 20;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.2;
}

export default TachyonLatticeEstablishment;

import Damage from '../../../tactical/damage/damage';
import UnitDefense from '../../../tactical/damage/defense';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class ReactiveHullEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 301;
	public static readonly caption: string = 'Reactive Hull';
	public static readonly description: string = `An exterior protection for the Base that provides universal protection from Orbital Attacks and some extra defense against explosives`;
	public static readonly researchOutputModifier: IModifier = null;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 1320,
		[Resources.resourceId.carbon]: 1950,
		[Resources.resourceId.halogens]: 250,
		[Resources.resourceId.oxygen]: 250,
		[Resources.resourceId.ores]: 2500,
	};
	public static readonly repairSpeedModifier: IModifier = {
		factor: 0.95,
	};
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 250,
	};
	public static readonly counterIntelligenceLevel: IModifier = {
		bias: 1,
	};
	public static readonly armor: UnitDefense.TArmorModifier = {
		default: {
			factor: 0.9,
		},
		[Damage.damageType.thermodynamic]: {
			bias: -15,
			factor: 0.8,
		},
		[Damage.damageType.heat]: {
			bias: -15,
			factor: 0.8,
		},
		[Damage.damageType.kinetic]: {
			bias: -15,
			factor: 0.8,
		},
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.helium]: {
			factor: 1.05,
		},
	};
	public researchOutput = 4;
	public engineeringOutput = 8;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.5;
}

export default ReactiveHullEstablishment;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class ScienceLabEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 100;
	public static readonly caption: string = 'Science Lab';
	public static readonly description: string = `Dedicated research complex`;
	public static readonly researchOutputModifier: IModifier = {
		bias: 5,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.helium]: 100,
		[Resources.resourceId.minerals]: 2200,
		[Resources.resourceId.ores]: 1400,
		[Resources.resourceId.oxygen]: 400,
		[Resources.resourceId.isotopes]: 100,
		[Resources.resourceId.nitrogen]: 500,
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 1,
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.4;
	public researchOutput = 12;
}

export default ScienceLabEstablishment;

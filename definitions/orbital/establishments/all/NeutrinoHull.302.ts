import Damage from '../../../tactical/damage/damage';
import UnitDefense from '../../../tactical/damage/defense';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class NeutrinoHullEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 302;
	public static readonly caption: string = 'Neutrino Hull';
	public static readonly description: string = `An exterior protection for the Base that provides universal protection from Orbital Attacks and some extra defense against destructuring effects`;
	public static readonly researchOutputModifier: IModifier = null;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 600,
		[Resources.resourceId.carbon]: 1850,
		[Resources.resourceId.hydrogen]: 2200,
		[Resources.resourceId.nitrogen]: 3800,
		[Resources.resourceId.ores]: 1600,
	};
	public static readonly repairSpeedModifier: IModifier = {
		factor: 0.95,
	};
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 250,
	};
	public static readonly counterIntelligenceLevel: IModifier = {
		bias: 2,
	};
	public static readonly armor: UnitDefense.TArmorModifier = {
		default: {
			factor: 0.9,
		},
		[Damage.damageType.antimatter]: {
			bias: -25,
			factor: 0.85,
		},
		[Damage.damageType.warp]: {
			bias: -25,
			factor: 0.85,
		},
		[Damage.damageType.biological]: {
			bias: -25,
			factor: 0.85,
		},
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.minerals]: {
			factor: 1.05,
		},
	};
	public researchOutput = 10;
	public engineeringOutput = 5;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.9;
}

export default NeutrinoHullEstablishment;

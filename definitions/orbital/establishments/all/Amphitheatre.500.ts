import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class AmphitheatreEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 500;
	public static readonly caption: string = 'Amphitheatre';
	public static readonly description: string = `Entertainment establishment that generally increases productivity of the staff`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.proteins]: 640,
		[Resources.resourceId.minerals]: 2200,
		[Resources.resourceId.isotopes]: 100,
		[Resources.resourceId.ores]: 1200,
		[Resources.resourceId.oxygen]: 450,
	};
	public static readonly materialOutputModifier: IModifier = {
		factor: 1.05,
	};
	public static resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.carbon]: {
			factor: 1.03,
		},
		[Resources.resourceId.ores]: {
			factor: 1.03,
		},
		[Resources.resourceId.isotopes]: {
			factor: 1.03,
		},
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.5;
	public engineeringOutput = 3;
	public researchOutput = 3;
}

export default AmphitheatreEstablishment;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class GrinderEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 402;
	public static readonly caption: string = 'Grinder';
	public static readonly description: string = `Utilization plant that provides plenty of supply for engineering works`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 2100,
		[Resources.resourceId.carbon]: 3250,
		[Resources.resourceId.nitrogen]: 1620,
		[Resources.resourceId.helium]: 420,
		[Resources.resourceId.hydrogen]: 3850,
		[Resources.resourceId.isotopes]: 200,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.halogens]: {
			factor: 1.05,
		},
	};
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.oxygen]: {
			factor: 0.97,
		},
	};
	public static readonly engineeringOutputModifier: IModifier = {
		bias: 2,
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.1,
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT;
	public engineeringOutput = 22;
	public researchOutput = 5;
}

export default GrinderEstablishment;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class InvestigationBureauEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 200;
	public static readonly caption: string = 'Investigation Bureau';
	public static readonly description: string = `An establishment focused on gathering information about enemy technologies`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.nitrogen]: 200,
		[Resources.resourceId.carbon]: 500,
		[Resources.resourceId.proteins]: 200,
		[Resources.resourceId.ores]: 2280,
		[Resources.resourceId.oxygen]: 440,
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 2,
	};
	public static readonly counterIntelligenceLevel: IModifier = {
		bias: 1,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.carbon]: {
			factor: 1.03,
		},
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.75;
	public engineeringOutput = 4;
	public researchOutput = 4;
}

export default InvestigationBureauEstablishment;

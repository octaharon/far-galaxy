import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class SimulationChamber extends GenericEstablishment {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 404;
	public static readonly caption: string = 'Simulation Chamber';
	public static readonly description: string = `Augmented reality simulations can be conducted at this establishment, providing for all kinds of scientific and engineering experiments`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.isotopes]: 900,
		[Resources.resourceId.ores]: 3700,
		[Resources.resourceId.helium]: 500,
		[Resources.resourceId.proteins]: 250,
		[Resources.resourceId.oxygen]: 800,
		[Resources.resourceId.nitrogen]: 750,
	};
	public static readonly energyConsumptionModifier: IModifier = {
		bias: -1,
	};
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.proteins]: {
			factor: 0.95,
		},
	};
	public static readonly bayCapacity: IModifier = {
		bias: 1,
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.25,
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.4;
	public engineeringOutput = 25;
	public researchOutput = 20;
}

export default SimulationChamber;

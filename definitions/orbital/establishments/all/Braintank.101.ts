import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class BraintankEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 101;
	public static readonly caption: string = 'Braintank';
	public static readonly description: string = `Advanced research and scientific complex`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.helium]: 500,
		[Resources.resourceId.minerals]: 4120,
		[Resources.resourceId.isotopes]: 100,
		[Resources.resourceId.nitrogen]: 740,
		[Resources.resourceId.oxygen]: 340,
		[Resources.resourceId.halogens]: 475,
	};
	public static readonly researchOutputModifier: IModifier = {
		bias: 5,
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 2,
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.75;
	public researchOutput = 30;
}

export default BraintankEstablishment;

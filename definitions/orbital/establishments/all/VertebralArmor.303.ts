import UnitDefense from '../../../tactical/damage/defense';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class VertebralArmorEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 303;
	public static readonly caption: string = 'Vertebral Armor';
	public static readonly description: string = `Dense chine going through the hull of the Base provides significant protection versus Orbital Attacks`;
	public static readonly researchOutputModifier: IModifier = null;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 5600,
		[Resources.resourceId.carbon]: 2250,
		[Resources.resourceId.helium]: 400,
		[Resources.resourceId.oxygen]: 900,
		[Resources.resourceId.nitrogen]: 1200,
		[Resources.resourceId.minerals]: 2750,
	};
	public static readonly maxRawMaterials: IModifier = {
		bias: 50,
	};
	public static readonly bayCapacity: IModifier = {
		bias: 1,
	};
	public static readonly armor: UnitDefense.TArmorModifier = {
		default: {
			factor: 0.8,
		},
	};
	public static resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.minerals]: {
			factor: 1.05,
		},
	};
	public static readonly energyConsumptionModifier: IModifier = {
		factor: 0.9,
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};
	public engineeringOutput = 13;
	public researchOutput = 5;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.75;
}

export default VertebralArmorEstablishment;

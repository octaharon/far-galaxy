import { Resources }                   from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class ReconnaissanceAgencyEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 201;
	public static readonly caption: string = 'Reconnaissance Agency';
	public static readonly description: string = `The gathering of data scientists and security specialists that work hard on providing combat info on opponents and maintain a security firewall that counteracts enemy intelligence`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.nitrogen]: 1200,
		[Resources.resourceId.minerals]: 2240,
		[Resources.resourceId.ores]: 2350,
		[Resources.resourceId.proteins]: 400,
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 4,
	};
	public static readonly counterIntelligenceLevel: IModifier = {
		bias: 3,
	};
	public static repairSpeedModifier: IModifier = {
		bias: 1,
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.1,
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.7;
	public engineeringOutput = 5;
	public researchOutput = 5;
}

export default ReconnaissanceAgencyEstablishment;

import InspireAbility from '../../../tactical/abilities/all/Orbital/Inspire.45006';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';
import { IEstablishment } from '../types';

export class AscendancySummitEstablishment extends GenericEstablishment implements IEstablishment {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 503;
	public static readonly caption: string = 'Ascendancy Summit';
	public static readonly description: string = `Literally the pinnacle of the Base. An establishment that provides huge boosts to all the other projects and grants an unique Inspire ability.`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.isotopes]: 300,
		[Resources.resourceId.hydrogen]: 8500,
		[Resources.resourceId.helium]: 1350,
		[Resources.resourceId.minerals]: 4200,
		[Resources.resourceId.ores]: 2350,
		[Resources.resourceId.oxygen]: 4000,
		[Resources.resourceId.nitrogen]: 1200,
		[Resources.resourceId.proteins]: 400,
		[Resources.resourceId.halogens]: 1370,
	};
	public static readonly abilities = [InspireAbility.id];
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.1,
	};
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 250,
	};
	public static readonly maxRawMaterials: IModifier = {
		bias: 75,
	};
	public static readonly materialOutputModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly repairSpeedModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly researchOutputModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly energyOutputModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly engineeringOutputModifier: IModifier = {
		factor: 1.1,
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 3,
	};
	public static resourceConsumptionModifier: Resources.TResourceModifier = {
		default: {
			factor: 0.95,
		},
	};

	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.05;
}

export default AscendancySummitEstablishment;

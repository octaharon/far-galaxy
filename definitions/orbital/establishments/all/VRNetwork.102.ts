import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class VRNetworkEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 102;
	public static readonly caption: string = 'VPR Network';
	public static readonly description: string = `The most advanced research establishment`;
	public static readonly researchOutputModifier: IModifier = {
		factor: 1.15,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.isotopes]: 150,
		[Resources.resourceId.carbon]: 3540,
		[Resources.resourceId.ores]: 6250,
		[Resources.resourceId.minerals]: 3300,
		[Resources.resourceId.nitrogen]: 900,
		[Resources.resourceId.oxygen]: 1800,
	};
	public static readonly intelligenceLevel: IModifier = {
		bias: 2,
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.3;
	public researchOutput = 40;
}

export default VRNetworkEstablishment;

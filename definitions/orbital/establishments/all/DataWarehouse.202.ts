import MainframeJammerAbility from '../../../tactical/abilities/all/ElectronicWarfare/MainframeJammerAbility.50001';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class DataWarehouseEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 202;
	public static readonly caption: string = 'Data Warehouse';
	public static readonly description: string = `All sorts of digital warfare are researched, developed and counteracted here by a group of cybersecurity enthusiasts`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.nitrogen]: 1800,
		[Resources.resourceId.halogens]: 300,
		[Resources.resourceId.isotopes]: 100,
		[Resources.resourceId.hydrogen]: 2200,
		[Resources.resourceId.ores]: 1600,
		[Resources.resourceId.proteins]: 500,
	};
	public static readonly abilities: number[] = [MainframeJammerAbility.id];
	public static readonly counterIntelligenceLevel: IModifier = {
		bias: 8,
	};
	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 0.95,
		},
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.1,
	};
	public researchOutput = 12;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.1;
}

export default DataWarehouseEstablishment;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class ObservationDeckEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 502;
	public static readonly caption: string = 'Observation Deck';
	public static readonly description: string = `A great place to gather and peer into the endless space, which helps to increases the morales of the crew. At any other time works well as a warehouse`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.halogens]: 450,
		[Resources.resourceId.minerals]: 2250,
		[Resources.resourceId.carbon]: 1200,
		[Resources.resourceId.helium]: 920,
		[Resources.resourceId.oxygen]: 3000,
		[Resources.resourceId.ores]: 1500,
	};
	public static readonly maxRawMaterials: IModifier = {
		bias: 50,
	};
	public static readonly energyConsumptionModifier: IModifier = {
		bias: -1,
		minGuard: 1,
	};
	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.25,
	};
	public static resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.carbon]: {
			factor: 1.1,
		},
		[Resources.resourceId.helium]: {
			factor: 1.05,
		},
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.75;
	public researchOutput = 14;
}

export default ObservationDeckEstablishment;

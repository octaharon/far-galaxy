import { Resources }                   from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericEstablishment from '../GenericEstablishment';

export class CampusEstablishment extends GenericEstablishment {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 400;
	public static readonly caption: string = 'Campus';
	public static readonly description: string = `An establishment for technicians who can reverse engineer almost anything`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.isotopes]: 15,
		[Resources.resourceId.proteins]: 140,
		[Resources.resourceId.nitrogen]: 1080,
		[Resources.resourceId.carbon]: 2200,
		[Resources.resourceId.oxygen]: 650,
	};
	public static resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 1.03,
		},
	};
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.4;
	public engineeringOutput = 15;
}

export default CampusEstablishment;

import { ICollectibleFactory, TCollectible } from '../../core/Collectible';
import Resources from '../../world/resources/types';
import {
	IBaseBuilding,
	IBaseBuildingStatic,
	IBaseBuildingUpgrade,
	IPlayerBaseCycleModifier,
	IPlayerBaseFacilityModifier,
	TBaseBuildingProps,
	TBaseBuildingType,
	TSerializedBaseBuilding,
} from '../types';

export type TFacilityID = number;
export type TFacilityProps = {
	energyOutput: number;
	repairOutput: number;
	materialOutput: number;
	outputMode: boolean; // Facility buildings alternate between raw materials production/repair and resource output
} & TBaseBuildingProps;
export type TSerializedFacility = TFacilityProps & TSerializedBaseBuilding;

export interface IFacility
	extends TFacilityProps,
		IBaseBuilding<
			TBaseBuildingType.facility,
			TFacilityProps,
			TSerializedFacility,
			IFacilityStatic,
			TFacilityUpgrade
		> {
	isResourceModeEnabled(): boolean;

	switchOutput(): this;

	productionCycle(): this;
}

export type TFacilityUpgrade = IBaseBuildingUpgrade<TFacilityProps>;

export type TFacility = TCollectible<IFacility, IFacilityStatic>;

export interface IFacilityMeta {}

export interface IFacilityStatic
	extends IBaseBuildingStatic<TBaseBuildingType.facility, TFacilityProps, TFacilityUpgrade>,
		IPlayerBaseFacilityModifier {
	resourceProduction?: Resources.TResourcePayload;
	resourceConsumption?: Resources.TResourcePayload;
}

export type IFacilityProvider = ICollectibleFactory<TFacilityProps, TSerializedFacility, IFacility, IFacilityStatic>;

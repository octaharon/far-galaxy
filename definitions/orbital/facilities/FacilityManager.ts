import { CollectibleFactory } from '../../core/Collectible';
import { TEstablishmentProps } from '../establishments/types';
import BuildingManager from '../GenericBaseBuildingManager';
import FacilityList from './index';
import { IFacility, IFacilityProvider, IFacilityStatic, TFacilityProps, TSerializedFacility } from './types';

export const defaultFacilityProps: Partial<TFacilityProps> = {
	disabled: false,
	outputMode: true,
};

export class FacilityManager
	extends CollectibleFactory<TFacilityProps, TSerializedFacility, IFacility, IFacilityStatic>
	implements IFacilityProvider
{
	public instantiate(props: Partial<TEstablishmentProps> = {}, id: number) {
		const c = this.__instantiateCollectible(
			{
				...defaultFacilityProps,
				...props,
			},
			id,
		);
		return c;
	}

	public store(o: IFacility, callback?: (a: IFacility) => void) {
		super.store(BuildingManager.preStore(o), (v) => callback(BuildingManager.postStore(v)));
		return this;
	}

	public fill() {
		this.__fillItems(FacilityList);
		return this;
	}
}

export default FacilityManager;

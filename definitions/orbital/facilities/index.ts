import GenericFacility from './GenericFacility';
import { TFacility } from './types';

let FacilityList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), FacilityList);
FacilityList = FacilityList.filter(
	(module: any) => module.default.prototype instanceof GenericFacility && module.default.id > 0,
).map((module: any) => module.default);
export default FacilityList as TFacility[];

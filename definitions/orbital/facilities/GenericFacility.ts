import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import Resources                                         from '../../world/resources/types';
import { GenericBaseBuilding }                           from '../GenericBaseBuilding';
import { TBaseBuildingType } from '../types';
import { IFacility, IFacilityStatic, TFacilityProps, TFacilityUpgrade, TSerializedFacility } from './types';

export class GenericFacility
	extends GenericBaseBuilding<
		TBaseBuildingType.facility,
		TFacilityProps,
		TSerializedFacility,
		IFacilityStatic,
		TFacilityUpgrade
	>
	implements IFacility
{
	public static readonly type = TBaseBuildingType.facility;
	public static readonly id: number = 0;
	public static readonly energyOutputModifier: IModifier = {
		bias: 5,
		minGuard: 1,
	};
	public static readonly upgrades: TFacilityUpgrade[] = [
		{
			caption: 'Recycle',
			repairOutput: {
				bias: 2,
			},
			energyMaintenanceCost: {
				factor: 0.9,
				min: 1,
				minGuard: 1,
			},
			materialOutput: {
				bias: -3,
				min: 0,
				minGuard: 0,
			},
		},
		{
			caption: 'Overhaul',
			energyOutput: {
				bias: 3,
				minGuard: 1,
				min: 1,
			},
			repairOutput: {
				bias: -1,
				min: 0,
				minGuard: 0,
			},
			energyMaintenanceCost: {
				factor: 1.1,
			},
			materialOutput: {
				bias: 3,
				min: 0,
				minGuard: 0,
			},
		},
		{
			caption: 'Optimize',
			energyOutput: {
				bias: 5,
			},
			repairOutput: {
				bias: -3,
				min: 0,
				minGuard: 0,
			},
			materialOutput: {
				bias: -2,
				min: 0,
				minGuard: 0,
			},
		},
	];
	public energyOutput: number;
	public materialOutput: number;
	public outputMode: boolean;
	public repairOutput: number;

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.facility];
	}

	public isResourceModeEnabled(): boolean {
		return !!Object.keys(this.static.resourceProduction || {}).length;
	}

	public switchOutput() {
		if (!this.isResourceModeEnabled()) this.outputMode = true;
		else this.outputMode = !this.outputMode;
		return this;
	}

	public productionCycle() {
		const p = this.owner;
		const b = this.base;
		if (!p || !b) return this;
		if (!b.spendResource(this.static.resourceConsumption)) return this;
		if (this.outputMode) {
			if (this.materialOutput) b.addRawMaterials(this.materialOutput);
			if (this.repairOutput) b.addRepair(this.repairOutput);
		} else
			_.mapObject(this.static.resourceProduction || {}, (num, rid: Resources.resourceId) =>
				b.addResource(rid, num),
			);
		return this;
	}

	public serialize(): TSerializedFacility {
		return {
			...this.__serializeBuilding(),
			energyOutput: this.energyOutput,
			materialOutput: this.materialOutput,
			outputMode: this.outputMode,
			repairOutput: this.repairOutput,
		};
	}
}

export default GenericFacility;

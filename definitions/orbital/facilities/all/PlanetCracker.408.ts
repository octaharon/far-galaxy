import OrbitalStrikeAbility from '../../../tactical/abilities/all/Orbital/OrbitalStrikeAbility.45000';
import PlanetCrackerAbility from '../../../tactical/abilities/all/Orbital/PlanetCracker.45005';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class PlanetCrackerFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 408;
	public static readonly caption: string = 'Planet Cracker';
	public static readonly description: string = `Gigantic planetary weapon mighty enough to change the landscape forever`;
	public static readonly energyOutputModifier: IModifier = null;

	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.isotopes]: 1530,
		[Resources.resourceId.ores]: 6250,
		[Resources.resourceId.hydrogen]: 3750,
		[Resources.resourceId.carbon]: 7500,
		[Resources.resourceId.nitrogen]: 2100,
		[Resources.resourceId.helium]: 300,
	};

	public static readonly abilities: number[] = [PlanetCrackerAbility.id, OrbitalStrikeAbility.id];

	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.isotopes]: {
			factor: 0.97,
		},
	};

	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};

	public static readonly researchOutputModifier: IModifier = {
		bias: 3,
	};

	public outputMode = true;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.5;
}

export default PlanetCrackerFacility;

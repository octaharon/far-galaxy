import ShieldFunnelAbility from '../../../tactical/abilities/all/Orbital/ShieldFunnel.60007';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class MassRelayFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 304;
	public static readonly caption: string = 'Mass Relay';
	public static readonly description: string = `Bleeding edge technology which can inverse the physical charge of matter and shuffle pretty much anything for energy`;

	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 3300,
		[Resources.resourceId.minerals]: 1950,
		[Resources.resourceId.halogens]: 2550,
		[Resources.resourceId.nitrogen]: 800,
		[Resources.resourceId.carbon]: 3500,
		[Resources.resourceId.isotopes]: 400,
	};

	public static readonly intelligenceLevel: IModifier = {
		bias: 1,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 5,
		[Resources.resourceId.proteins]: 1,
		[Resources.resourceId.helium]: 1,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.halogens]: 0.5,
		[Resources.resourceId.minerals]: 1,
		[Resources.resourceId.oxygen]: 2,
		[Resources.resourceId.ores]: 0.2,
	};

	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};

	public static readonly abilities: number[] = [ShieldFunnelAbility.id];

	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 8.5;
	public energyMaintenanceCost = 0;
}

export default MassRelayFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class IncinerationPlantFacility extends GenericFacility {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 300;
	public static readonly caption: string = 'Incineration Plant';
	public static readonly description: string = `A power plant that burns organic materials to produce power. Can be switched to collect Carbon and Nitrogen instead `;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 2200,
		[Resources.resourceId.carbon]: 850,
		[Resources.resourceId.proteins]: 200,
		[Resources.resourceId.minerals]: 1850,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.carbon]: {
			factor: 1.05,
		},
	};
	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.proteins]: 0.5,
		[Resources.resourceId.oxygen]: 2,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.nitrogen]: 1,
		[Resources.resourceId.minerals]: 0.1,
		[Resources.resourceId.carbon]: 0.5,
	};

	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 3;
	public energyMaintenanceCost = 0;
}

export default IncinerationPlantFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class CompostureFacility extends GenericFacility {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 201;
	public static readonly caption: string = 'Composture';
	public static readonly description: string = `Resource transmutation facility that decomposes proteins into primary elements`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 1460,
		[Resources.resourceId.oxygen]: 700,
		[Resources.resourceId.halogens]: 300,
		[Resources.resourceId.carbon]: 1650,
	};

	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.oxygen]: {
			factor: 1.03,
		},
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.proteins]: 2,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.oxygen]: 0.2,
		[Resources.resourceId.nitrogen]: 0.5,
		[Resources.resourceId.hydrogen]: 2,
		[Resources.resourceId.carbon]: 1,
	};

	public materialOutput = 15;
	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 2;
}

export default CompostureFacility;

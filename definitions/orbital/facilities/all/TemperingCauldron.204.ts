import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class TemperingCauldronFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 204;
	public static readonly caption: string = 'Tempering Cauldron';
	public static readonly description: string = `A facility for quenching metals into complex compounds that can be consumed directly or used for production and repairs`;
	public static readonly productionCapacity: IModifier = {
		bias: 25,
	};
	public static readonly maxRawMaterials: IModifier = {
		bias: 100,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.hydrogen]: 2500,
		[Resources.resourceId.carbon]: 3250,
		[Resources.resourceId.nitrogen]: 1440,
		[Resources.resourceId.minerals]: 1700,
		[Resources.resourceId.ores]: 4900,
		[Resources.resourceId.oxygen]: 2200,
		[Resources.resourceId.halogens]: 450,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.ores]: {
			factor: 1.05,
		},
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 3,
		[Resources.resourceId.halogens]: 2,
		[Resources.resourceId.helium]: 2,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 2,
		[Resources.resourceId.isotopes]: 0.5,
	};

	public materialOutput = 75;
	public repairOutput = 30;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.25;
}

export default TemperingCauldronFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class ChargeDepotFacility extends GenericFacility {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 305;
	public static readonly caption: string = 'Charge Depot';
	public static readonly description: string = `A huge battery to store the excesses of energy so it can maintain your base for a while if power plants go down. Passively produces tiny amounts of energy from magnetic induction`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1450,
		[Resources.resourceId.halogens]: 200,
		[Resources.resourceId.minerals]: 3680,
		[Resources.resourceId.helium]: 650,
	};

	public static readonly energyConsumptionModifier: IModifier = {
		factor: 0.95,
	};

	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.carbon]: {
			factor: 0.97,
		},
	};

	public static readonly maxEnergyCapacity: IModifier = {
		bias: 1500,
	};

	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};

	public outputMode = true;

	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 0.2;
	public energyMaintenanceCost = 0;
}

export default ChargeDepotFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class MineralRefineryFacility extends GenericFacility {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 200;
	public static readonly caption: string = 'Mineral Refinery';
	public static readonly description: string = `Resource transmutation facility which breaks down Minerals into Ores and organic elements or produces Raw Materials from them`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 1200,
		[Resources.resourceId.oxygen]: 900,
		[Resources.resourceId.hydrogen]: 2200,
		[Resources.resourceId.nitrogen]: 800,
		[Resources.resourceId.carbon]: 1150,
	};

	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.halogens]: {
			factor: 0.97,
		},
		[Resources.resourceId.minerals]: {
			factor: 0.95,
		},
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 3,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 0.2,
		[Resources.resourceId.oxygen]: 0.5,
		[Resources.resourceId.hydrogen]: 2,
		[Resources.resourceId.halogens]: 0.1,
	};

	public materialOutput = 35;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.6;
}

export default MineralRefineryFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class DarkTetherFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 407;
	public static readonly caption: string = 'Dark Tether';
	public static readonly description: string = `Massive Higgs Field generator that extracts electrical charge from the vacuum itself, channelling energy to Board systems`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.isotopes]: 400,
		[Resources.resourceId.minerals]: 4000,
		[Resources.resourceId.halogens]: 740,
		[Resources.resourceId.ores]: 5840,
		[Resources.resourceId.proteins]: 700,
		[Resources.resourceId.oxygen]: 400,
	};

	public static readonly energyOutputModifier: IModifier = {
		factor: 1.15,
	};

	public static readonly intelligenceLevel: IModifier = {
		bias: 1,
	};

	public static readonly maxHitPoints: IModifier = {
		bias: 250,
	};

	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 1.05,
		},
		[Resources.resourceId.helium]: {
			factor: 1.05,
		},
	};

	public outputMode = true;

	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 1.5;
	public energyMaintenanceCost = 0;
}

export default DarkTetherFacility;

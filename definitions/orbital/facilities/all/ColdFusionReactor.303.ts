import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class ColdFusionReactorFacility extends GenericFacility {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 303;
	public static readonly caption: string = 'Cold Fusion Reactor';
	public static readonly description: string = `The philosopher stone of the 3rd millenia, this device can combine lighter elements into heavier isotopes, possibly converting excesses to energy`;
	public static readonly energyConsumptionModifier: IModifier = {
		factor: 0.95,
	};

	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.helium]: 550,
		[Resources.resourceId.carbon]: 4250,
		[Resources.resourceId.nitrogen]: 800,
		[Resources.resourceId.oxygen]: 700,
		[Resources.resourceId.halogens]: 150,
		[Resources.resourceId.minerals]: 600,
		[Resources.resourceId.ores]: 3600,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.nitrogen]: 5,
		[Resources.resourceId.helium]: 3,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.hydrogen]: 1,
		[Resources.resourceId.isotopes]: 0.1,
		[Resources.resourceId.carbon]: 2,
		[Resources.resourceId.oxygen]: 0.5,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.isotopes]: {
			factor: 1.03,
		},
	};

	public static readonly maxEnergyCapacity: IModifier = {
		bias: 350,
	};

	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 4.5;
	public energyMaintenanceCost = 0;
}

export default ColdFusionReactorFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class AxialCompressorFacility extends GenericFacility {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 203;
	public static readonly caption: string = 'Axial Compressor';
	public static readonly description: string = `Gas separation facility that heats Halogens to a state of quark-gluon plasma and then recomposes them into lightweight elements or produces spare materials for production and repairs`;
	public static readonly energyOutputModifier: IModifier = {
		factor: 0.97,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 2650,
		[Resources.resourceId.hydrogen]: 1500,
		[Resources.resourceId.minerals]: 1920,
		[Resources.resourceId.nitrogen]: 620,
		[Resources.resourceId.carbon]: 900,
		[Resources.resourceId.helium]: 900,
		[Resources.resourceId.halogens]: 400,
		[Resources.resourceId.isotopes]: 100,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.isotopes]: {
			factor: 1.03,
		},
		[Resources.resourceId.helium]: {
			factor: 1.03,
		},
		[Resources.resourceId.halogens]: {
			factor: 1.03,
		},
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.halogens]: 1,
		[Resources.resourceId.carbon]: 5,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.helium]: 0.5,
		[Resources.resourceId.oxygen]: 0.2,
		[Resources.resourceId.hydrogen]: 2,
		[Resources.resourceId.nitrogen]: 0.5,
	};

	public materialOutput = 35;
	public repairOutput = 75;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.9;
}

export default AxialCompressorFacility;

import MytochondrialMutationAbility from '../../../tactical/abilities/all/Orbital/MytochondrialMutation.37005';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class LivingShellFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 405;
	public static readonly caption: string = 'Living Shell';
	public static readonly description: string = `An experimental biotechnology to embed a complex microbe network into the hull of a Base, that can be restored significantly faster. A huge bacterial colony produces Energy and Proteins from basic Resources, when Powered and seems to develop its own intelligence, which collects information during Combat`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 2000,
		[Resources.resourceId.oxygen]: 600,
		[Resources.resourceId.proteins]: 2100,
		[Resources.resourceId.halogens]: 100,
		[Resources.resourceId.nitrogen]: 1600,
		[Resources.resourceId.helium]: 250,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 0.5,
		[Resources.resourceId.nitrogen]: 0.25,
		[Resources.resourceId.oxygen]: 0.25,
		[Resources.resourceId.hydrogen]: 1,
	};

	public static maxHitPoints: IModifier = {
		bias: 700,
	};

	public static readonly repairSpeedModifier?: IModifier = {
		factor: 1.1,
	};

	public static readonly abilities: number[] = [MytochondrialMutationAbility.id];

	public static readonly intelligenceLevel: IModifier = {
		bias: 4,
	};
	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.proteins]: {
			factor: 1.1,
		},
	};

	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.proteins]: 1,
	};

	public repairOutput = 45;
	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 2.5;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.8;
}

export default LivingShellFacility;

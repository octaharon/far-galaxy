import IrradiateAbility from '../../../tactical/abilities/all/Orbital/Irradiate.45008';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class ParticleCondenserFacility extends GenericFacility {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 101;
	public static readonly caption: string = 'Particle Condenser';
	public static readonly description: string = `Improved resource collection Facility that uses localized black holes juggling to absorb any matter from large chunk of space around`;
	public static readonly maxEnergyCapacity: IModifier = {
		bias: 150,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 2500,
		[Resources.resourceId.helium]: 900,
		[Resources.resourceId.nitrogen]: 4200,
		[Resources.resourceId.oxygen]: 640,
		[Resources.resourceId.minerals]: 1550,
	};

	public static readonly abilities: number[] = [IrradiateAbility.id];

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 1.03,
		},
	};

	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.hydrogen]: 2,
		[Resources.resourceId.helium]: 0.2,
		[Resources.resourceId.ores]: 0.1,
		[Resources.resourceId.carbon]: 0.2,
		[Resources.resourceId.oxygen]: 0.1,
		[Resources.resourceId.halogens]: 0.1,
		[Resources.resourceId.nitrogen]: 0.4,
	};

	public materialOutput = 9;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.3;
}

export default ParticleCondenserFacility;

import HijackAbility from '../../../tactical/abilities/all/Orbital/HijackAbility.45002';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class WormholeProjectorFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 103;
	public static readonly caption: string = 'Wormhole Projector';
	public static readonly description: string = `The most advanced resource collection facility, that can collect tiny amounts of pretty much everything from local star system and possibly turn it into Raw Materials`;
	public static readonly engineeringOutputModifier: IModifier = {
		bias: 1,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 5700,
		[Resources.resourceId.halogens]: 500,
		[Resources.resourceId.isotopes]: 400,
		[Resources.resourceId.hydrogen]: 2400,
		[Resources.resourceId.nitrogen]: 950,
		[Resources.resourceId.helium]: 750,
		[Resources.resourceId.proteins]: 600,
	};

	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.hydrogen]: 3,
		[Resources.resourceId.helium]: 0.4,
		[Resources.resourceId.carbon]: 1,
		[Resources.resourceId.halogens]: 0.2,
		[Resources.resourceId.nitrogen]: 0.4,
		[Resources.resourceId.isotopes]: 0.1,
		[Resources.resourceId.ores]: 0.5,
		[Resources.resourceId.minerals]: 0.6,
		[Resources.resourceId.proteins]: 0.2,
		[Resources.resourceId.oxygen]: 1,
	};

	public static readonly intelligenceLevel: IModifier = {
		bias: 2,
	};

	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.15,
	};

	public static readonly abilities: number[] = [HijackAbility.id];

	public materialOutput = 9;
	public repairOutput = 16;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.6;
}

export default WormholeProjectorFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class SmelteryFacility extends GenericFacility {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 202;
	public static readonly caption: string = 'Smeltery';
	public static readonly description: string = `A smeltery can melt ores into primary elements or cast alloys to produce Raw Materials and Repair units`;
	public static readonly repairSpeedModifier: IModifier = {
		factor: 1.05,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.isotopes]: 75,
		[Resources.resourceId.ores]: 2840,
		[Resources.resourceId.oxygen]: 2000,
		[Resources.resourceId.carbon]: 900,
		[Resources.resourceId.hydrogen]: 900,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 2,
		[Resources.resourceId.hydrogen]: 15,
		[Resources.resourceId.oxygen]: 1,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.nitrogen]: 0.5,
		[Resources.resourceId.minerals]: 1,
		[Resources.resourceId.halogens]: 0.2,
		[Resources.resourceId.isotopes]: 0.2,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.ores]: {
			factor: 1.03,
		},
	};

	public materialOutput = 60;
	public repairOutput = 25;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.9;
}

export default SmelteryFacility;

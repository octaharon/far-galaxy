import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class SolarCollectorFacility extends GenericFacility {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 301;
	public static readonly caption: string = 'Solar Collector';
	public static readonly description: string = `An array of panels that collects energy virtually for free. Can be switched to produce tiny amounts of Hydrogen from interstellar alpha radiation`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 1330,
		[Resources.resourceId.nitrogen]: 950,
		[Resources.resourceId.minerals]: 2900,
		[Resources.resourceId.halogens]: 160,
		[Resources.resourceId.hydrogen]: 2080,
		[Resources.resourceId.carbon]: 400,
	};

	public static readonly energyConsumptionModifier: IModifier = {
		factor: 0.97,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.hydrogen]: 0.5,
	};

	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 0.75;
	public energyMaintenanceCost = 0;
}

export default SolarCollectorFacility;

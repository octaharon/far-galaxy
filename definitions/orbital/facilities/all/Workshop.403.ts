import RetreatAbility from '../../../tactical/abilities/all/Orbital/Retreat.45001';
import { Resources } from '../../../world/resources/types';
import GenericFacility from '../GenericFacility';

export class WorkshopFacility extends GenericFacility {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 403;
	public static readonly caption: string = 'Workshop';
	public static readonly description: string = `A large facility for smith-and-fit works which can hold units and repair them`;
	public static readonly energyOutputModifier: IModifier = null;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 4700,
		[Resources.resourceId.helium]: 245,
		[Resources.resourceId.nitrogen]: 3900,
		[Resources.resourceId.carbon]: 1940,
		[Resources.resourceId.oxygen]: 900,
	};

	public static readonly bayCapacity: IModifier = {
		bias: 2,
	};

	public static readonly productionCapacity: IModifier = {
		bias: 40,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 1,
		[Resources.resourceId.halogens]: 0.2,
	};

	public static readonly abilities: number[] = [RetreatAbility.id];

	public outputMode = true;

	public repairOutput = 80;
	public energyMaintenanceCost = 22;
}

export default WorkshopFacility;

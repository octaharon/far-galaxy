import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class SiloFacility extends GenericFacility {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 400;
	public static readonly caption: string = 'Silo';
	public static readonly description: string = `A storage facility that extends the base production capability`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1800,
		[Resources.resourceId.hydrogen]: 2600,
		[Resources.resourceId.helium]: 400,
	};

	public static readonly maxRawMaterials: IModifier = {
		bias: 250,
	};
	public static readonly productionCapacity: IModifier = {
		bias: 15,
	};

	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 0.97,
		},
	};

	public outputMode = true;

	public repairOutput = 5;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.25;
}

export default SiloFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class VacuumCleanerFacility extends GenericFacility {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 100;
	public static readonly caption: string = 'Vacuum Cleaner';
	public static readonly description: string = `A facility that absorbs all possible particles from outer space thus making the vacuum even less populated. The resources collected can be converted to Raw Materials`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 2000,
		[Resources.resourceId.carbon]: 2000,
		[Resources.resourceId.isotopes]: 100,
		[Resources.resourceId.halogens]: 500,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 1.03,
		},
	};

	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.helium]: 0.4,
		[Resources.resourceId.ores]: 0.1,
		[Resources.resourceId.carbon]: 0.3,
		[Resources.resourceId.oxygen]: 0.2,
		[Resources.resourceId.halogens]: 0.1,
		[Resources.resourceId.nitrogen]: 0.2,
	};

	public materialOutput = 3;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.95;
}

export default VacuumCleanerFacility;

import OrbitalEnergyTransferAbility from '../../../tactical/abilities/all/Orbital/OrbitalEnergyTransfer.60006';
import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class HyperspaceCompactorFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 306;
	public static readonly caption: string = 'Hyperspace Compactor';
	public static readonly description: string = `A prototype of emerging technology which can store huge amounts of energy inside timespace pockets and collect tiny amounts of subatomic matter from vacuum fluctuations inside`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 11900,
		[Resources.resourceId.oxygen]: 2800,
		[Resources.resourceId.proteins]: 450,
		[Resources.resourceId.isotopes]: 150,
		[Resources.resourceId.ores]: 2800,
		[Resources.resourceId.nitrogen]: 850,
	};

	public static readonly abilities: number[] = [OrbitalEnergyTransferAbility.id];

	public static readonly researchOutputModifier: IModifier = {
		factor: 1.05,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		default: {
			factor: 1.03,
		},
	};

	public static readonly maxEnergyCapacity: IModifier = {
		bias: 1000,
	};

	public outputMode = true;

	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 2;
	public energyMaintenanceCost = 0;
}

export default HyperspaceCompactorFacility;

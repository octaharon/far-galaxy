import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class GarageFacility extends GenericFacility {
	public static readonly buildingTier: number = 1;
	public static readonly id: number = 401;
	public static readonly caption: string = 'Garage';
	public static readonly description: string = `An attachment to the Bay which allows for more space and reparations`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.hydrogen]: 1400,
		[Resources.resourceId.helium]: 200,
		[Resources.resourceId.carbon]: 1200,
		[Resources.resourceId.nitrogen]: 740,
		[Resources.resourceId.isotopes]: 50,
		[Resources.resourceId.ores]: 1500,
		[Resources.resourceId.oxygen]: 500,
	};

	public static readonly bayCapacity: IModifier = {
		bias: 1,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.minerals]: 0.5,
	};

	public outputMode = true;

	public repairOutput = 35;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.4;
}

export default GarageFacility;

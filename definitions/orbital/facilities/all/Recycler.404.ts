import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class RecyclerFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 404;
	public static readonly caption: string = 'Recycler';
	public static readonly description: string = `Automated utilization complex that provides the base with some extra materials and repairs`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 3200,
		[Resources.resourceId.minerals]: 8500,
		[Resources.resourceId.helium]: 650,
		[Resources.resourceId.halogens]: 150,
		[Resources.resourceId.hydrogen]: 2240,
		[Resources.resourceId.oxygen]: 1900,
	};

	public static readonly productionCapacity: IModifier = {
		bias: 25,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.oxygen]: 2,
		[Resources.resourceId.helium]: 4,
	};

	public static readonly resourceConsumptionModifier: Resources.TResourceModifier = {
		[Resources.resourceId.carbon]: {
			factor: 0.97,
		},
		[Resources.resourceId.ores]: {
			factor: 0.97,
		},
		[Resources.resourceId.minerals]: {
			factor: 0.97,
		},
	};

	public outputMode = true;

	public repairOutput = 60;
	public materialOutput = 40;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.2;
}

export default RecyclerFacility;

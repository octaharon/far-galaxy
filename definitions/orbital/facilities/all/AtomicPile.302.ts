import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class AtomicPileFacility extends GenericFacility {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 302;
	public static readonly caption: string = 'Atomic Pile';
	public static readonly description: string = `A fission reactor that produces power or ores by breaking down isotopes`;
	public static readonly productionCapacity: IModifier = {
		factor: 1.1,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.helium]: 650,
		[Resources.resourceId.nitrogen]: 900,
		[Resources.resourceId.isotopes]: 450,
		[Resources.resourceId.ores]: 600,
		[Resources.resourceId.minerals]: 2240,
		[Resources.resourceId.carbon]: 4650,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.isotopes]: 0.5,
		[Resources.resourceId.carbon]: 12,
	};
	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.ores]: 0.2,
		[Resources.resourceId.minerals]: 0.5,
		[Resources.resourceId.halogens]: 0.5,
		[Resources.resourceId.helium]: 1,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.helium]: {
			factor: 1.03,
		},
		[Resources.resourceId.hydrogen]: {
			factor: 1.05,
		},
	};

	public static readonly maxEnergyCapacity: IModifier = {
		bias: 250,
	};
	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 6;
	public energyMaintenanceCost = 0;
}

export default AtomicPileFacility;

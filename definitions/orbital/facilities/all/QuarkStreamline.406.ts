import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class QuarkStreamlineFacility extends GenericFacility {
	public static readonly buildingTier: number = 3;
	public static readonly id: number = 406;
	public static readonly caption: string = 'Quark Streamline';
	public static readonly description: string = `The most advanced transmutation facility available. Provides considerable amounts of either energy and materials or halogenes from light elements`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 5000,
		[Resources.resourceId.oxygen]: 650,
		[Resources.resourceId.nitrogen]: 980,
		[Resources.resourceId.hydrogen]: 9000,
		[Resources.resourceId.minerals]: 2480,
		[Resources.resourceId.ores]: 3150,
		[Resources.resourceId.halogens]: 1600,
		[Resources.resourceId.isotopes]: 320,
	};

	public static engineeringOutputModifier: IModifier = {
		bias: 3,
	};

	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.halogens]: 0.5,
		[Resources.resourceId.carbon]: 4,
	};

	public static readonly productionCapacity: IModifier = {
		bias: 15,
	};

	public static readonly resourceConsumption: Resources.TResourcePayload = {
		[Resources.resourceId.helium]: 1,
		[Resources.resourceId.nitrogen]: 3,
		[Resources.resourceId.ores]: 0.5,
	};

	public static readonly abilityPowerModifier: IModifier = {
		bias: 0.25,
	};

	public energyOutput = FACILITY_ENERGY_BASE_OUTPUT * 4;
	public materialOutput = 50;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.4;
}

export default QuarkStreamlineFacility;

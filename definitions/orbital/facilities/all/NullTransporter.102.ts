import DeployBeaconAbility             from '../../../tactical/abilities/all/Orbital/DeployBeacon.45004';
import { Resources }                   from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class NullTransporterFacility extends GenericFacility {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 102;
	public static readonly caption: string = 'Null Transporter';
	public static readonly description: string = `Passive resource collector that reaches everywhere enough mass is present, draining elements right from the nearest star, though not very efficient at producing Raw materials`;
	public static readonly repairSpeedModifier: IModifier = {
		bias: 2,
	};
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 4700,
		[Resources.resourceId.halogens]: 1200,
		[Resources.resourceId.minerals]: 2530,
		[Resources.resourceId.ores]: 1280,
		[Resources.resourceId.proteins]: 230,
	};

	public static readonly abilities: number[] = [DeployBeaconAbility.id];

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 1.03,
		},
	};

	public static readonly resourceProduction: Resources.TResourcePayload = {
		[Resources.resourceId.hydrogen]: 1,
		[Resources.resourceId.helium]: 0.5,
		[Resources.resourceId.carbon]: 0.2,
		[Resources.resourceId.halogens]: 0.4,
		[Resources.resourceId.nitrogen]: 0.2,
		[Resources.resourceId.isotopes]: 0.1,
	};

	public materialOutput = 4;
	public repairOutput = 10;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 1.35;
}

export default NullTransporterFacility;

import { Resources } from '../../../world/resources/types';
import { FACILITY_ENERGY_BASE_OUTPUT } from '../../base/constants';
import GenericFacility from '../GenericFacility';

export class CisternFacility extends GenericFacility {
	public static readonly buildingTier: number = 2;
	public static readonly id: number = 402;
	public static readonly caption: string = 'Cistern';
	public static readonly description: string = `An exterior tank for storage and protection which also provides tiny amounts of scrap materials from deterioration in outer space or direct damage`;
	public static readonly buildingCost: Resources.TResourcePayload = {
		[Resources.resourceId.carbon]: 1650,
		[Resources.resourceId.ores]: 4930,
		[Resources.resourceId.helium]: 150,
		[Resources.resourceId.oxygen]: 600,
		[Resources.resourceId.nitrogen]: 450,
		[Resources.resourceId.proteins]: 300,
	};

	public static readonly maxRawMaterials: IModifier = {
		bias: 350,
	};
	public static readonly maxHitPoints: IModifier = {
		bias: 250,
	};

	public static readonly materialOutputModifier?: IModifier = {
		bias: 5,
	};

	public static readonly resourceOutputModifier: Resources.TResourceModifier = {
		[Resources.resourceId.hydrogen]: {
			factor: 1.03,
		},
	};

	public outputMode = true;

	public materialOutput = 2;
	public repairOutput = 10;
	public energyMaintenanceCost = FACILITY_ENERGY_BASE_OUTPUT * 0.55;
}

export default CisternFacility;

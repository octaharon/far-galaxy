import UnitDefense from '../../tactical/damage/defense';
import { UnitPartFactory } from '../../tactical/units/UnitPart';
import { IArmor, IArmorFactory, IArmorMeta, TArmorItem, TArmorProps, TSerializedArmor } from '../types/armor';
import ArmorList from './index';

export class ArmorManager
	extends UnitPartFactory<TArmorProps, TSerializedArmor, IArmor, IArmorMeta>
	implements IArmorFactory
{
	protected __lookupBySlotType: EnumProxy<UnitDefense.TArmorSlotType, number[]> = {};

	public instantiate(props: Partial<TArmorProps>, id: number): IArmor {
		return super.instantiate(props, id);
	}

	public doesArmorFitSlot(a: TArmorItem, slot: UnitDefense.TArmorSlotType): boolean {
		return a.type === slot;
	}

	public getArmorsBySlotType(slot: UnitDefense.TArmorSlotType): TArmorItem[] {
		return (this.__lookupBySlotType[slot] || []).map((id) => this.get<TArmorItem>(id));
	}

	public fill() {
		this.__fillItems(
			ArmorList,
			(armorType) =>
				({
					slot: armorType.type,
				} as IArmorMeta),
			(armorType) => {
				if (!this.__lookupBySlotType[armorType.type]) this.__lookupBySlotType[armorType.type] = [];
				this.__lookupBySlotType[armorType.type].push(armorType.id);
			},
		);
		return this;
	}
}

export default ArmorManager;

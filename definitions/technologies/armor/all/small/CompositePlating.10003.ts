import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	SmallArmor_Bias_Max,
	SmallArmor_Factor_Max,
	SmallArmor_Factor_Min,
	SmallArmor_Threshold_Min,
} from '../../GenericArmor';

const protection = {
	bias: SmallArmor_Bias_Max,
	factor: SmallArmor_Factor_Max,
	threshold: SmallArmor_Threshold_Min,
	min: 0,
	minGuard: 0,
};

export class CompositePlatingArmor extends GenericArmor {
	public static readonly id = 10003;
	public static readonly caption = 'Composite Plating';
	public static readonly description = `The most advanced universal armor plating on the market with extra resistance to explosions`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.1,
		},
	};
	public baseCost = 15;
	public armor = {
		default: {
			factor: SmallArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
	};
}

export default CompositePlatingArmor;

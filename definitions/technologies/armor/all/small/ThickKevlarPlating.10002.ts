import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	SmallArmor_Bias_Max,
	SmallArmor_Bias_Med,
	SmallArmor_Bias_Min,
	SmallArmor_Factor_Max,
	SmallArmor_Factor_Min,
	SmallArmor_Threshold_Max,
} from '../../GenericArmor';

export class ThickKevlarPlatingArmor extends GenericArmor {
	public static readonly id = 10002;
	public static readonly caption = 'Carbon Foam Plating';
	public static readonly description = `A heavy plating made of carbon foam that offers a full protection with outstanding resistance to projectiles,
    but also compromises the maneuverability of the owner`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly dodgeModifier = {
		default: {
			bias: -0.2,
		},
	} as UnitDefense.TDodgeModifier;
	public baseCost = 16;
	public armor = {
		default: {
			bias: SmallArmor_Bias_Med,
			min: 0,
		},
		[Damage.damageType.kinetic]: {
			factor: SmallArmor_Factor_Max,
			bias: SmallArmor_Bias_Max,
			threshold: SmallArmor_Threshold_Max,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default ThickKevlarPlatingArmor;

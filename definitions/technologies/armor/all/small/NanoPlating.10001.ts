import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { SmallArmor_Bias_Min, SmallArmor_Factor_Med } from '../../GenericArmor';

export class NanoPlatingArmor extends GenericArmor {
	public static readonly id = 10001;
	public static readonly caption = 'Nanocell plating';
	public static readonly description = `A plating made of quantum locked atomic carbon. Offers substantial protection
    while still being light enough not to limit any movement`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public baseCost = 13;
	public armor = {
		default: {
			factor: SmallArmor_Factor_Med,
			bias: SmallArmor_Bias_Min,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default NanoPlatingArmor;

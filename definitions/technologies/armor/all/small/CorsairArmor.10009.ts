import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	SmallArmor_Bias_Max,
	SmallArmor_Bias_Med,
	SmallArmor_Factor_Max,
	SmallArmor_Factor_Min,
} from '../../GenericArmor';

const protection = {
	bias: SmallArmor_Bias_Max,
	factor: SmallArmor_Factor_Max,
	min: 0,
	minGuard: 0,
};

export class CorsairArmor extends GenericArmor {
	public static readonly id = 10009;
	public static readonly caption = 'Corsair Armor';
	public static readonly description = `An armor plating, developed by few scientists working for some Pirate clan.
    Rude, simple, but quite efficient versus certain types of ammunition and very affordable`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.1,
		},
	};
	public baseCost = 21;
	public armor = {
		default: {
			factor: SmallArmor_Factor_Min,
			bias: SmallArmor_Bias_Med,
			min: 0,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.biological]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default CorsairArmor;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	SmallArmor_Bias_Max,
	SmallArmor_Bias_Min,
	SmallArmor_Factor_Max,
	SmallArmor_Factor_Min,
} from '../../GenericArmor';

export class SmallBioArmor extends GenericArmor {
	public static readonly id = 10007;
	public static readonly caption = 'Small Bio Armor';
	public static readonly description = `A genetically constructed bacteria is grown into a colony,
    which behaves like a non-newtonian liquid and can transform absorbed energy into heat to boost the own growth, which resists chemicals extremely well, but also makes tracking easier. Somehow it's possible to make an armor plating out of this substance. Worst case
    scenario someone has to wear a bacteria colony as a suit. Gross!`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly healthModifier: IModifier = {
		bias: 10,
	};
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		[UnitAttack.deliveryType.missile]: {
			bias: -0.5,
		},
		[UnitAttack.deliveryType.drone]: {
			bias: -0.5,
		},
		[UnitAttack.deliveryType.bombardment]: {
			bias: -0.5,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.resilient]: true,
	};
	public baseCost = 24;
	public armor = {
		default: {
			bias: SmallArmor_Bias_Min,
			factor: SmallArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.biological]: {
			bias: SmallArmor_Bias_Max,
			factor: SmallArmor_Factor_Max - 0.1,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default SmallBioArmor;

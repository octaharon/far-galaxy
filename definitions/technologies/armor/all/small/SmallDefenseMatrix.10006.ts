import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { SmallArmor_Bias_Min } from '../../GenericArmor';

export class SmallDefenseMatrix extends GenericArmor {
	public static readonly id = 10006;
	public static readonly caption = 'Tachyon Matrix';
	public static readonly description = `One of the first practical applications of tachyon field
    projectors, which don't offer too much armor, but greatly increase dodging capabilities`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: 0.4,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.tachyon]: true,
	};
	public baseCost = 20;
	public armor = {
		default: {
			bias: SmallArmor_Bias_Min,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default SmallDefenseMatrix;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	SmallArmor_Bias_Max,
	SmallArmor_Factor_Med,
	SmallArmor_Threshold_Max,
	SmallArmor_Threshold_Min,
} from '../../GenericArmor';

const protection = {
	bias: SmallArmor_Bias_Max,
	threshold: SmallArmor_Threshold_Max,
	factor: SmallArmor_Factor_Med,
	min: 0,
	minGuard: 0,
};

export class SmallGluonArmor extends GenericArmor {
	public static readonly id = 10008;
	public static readonly caption = 'Small Gluon Armor';
	public static readonly description = `A thin layer of quark-gluon plasma suspended in a lattice of gravity fields around the host.
    Most low-energy particles, including mesons sprays coming from annihilation, lose a lot of impact while passing through this, while
    macroscopic damage factors go through it almost seamlessly`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		[UnitAttack.deliveryType.beam]: {
			bias: -0.5,
		},
	};
	public static readonly healthModifier: IModifier = {
		factor: 0.9,
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.warp]: true,
	};
	public baseCost = 21;
	public armor = {
		default: {
			threshold: SmallArmor_Threshold_Min,
		},
		[Damage.damageType.antimatter]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
		[Damage.damageType.radiation]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default SmallGluonArmor;

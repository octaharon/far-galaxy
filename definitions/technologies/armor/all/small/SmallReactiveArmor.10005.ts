import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { SmallArmor_Bias_Med, SmallArmor_Bias_Min, SmallArmor_Factor_Med } from '../../GenericArmor';

const protection = {
	bias: SmallArmor_Bias_Med,
	factor: SmallArmor_Factor_Med,
	min: 0,
	minGuard: 0,
};

export class SmallReactiveArmor extends GenericArmor {
	public static readonly id = 10005;
	public static readonly caption = 'Small Reactive Armor';
	public static readonly description = `A sandwitch plating with the middle explosive layer,
    which counteracts most known types of armor piercing ammunition`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public baseCost = 19;
	public armor = {
		default: {
			bias: SmallArmor_Bias_Min,
			min: 0,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.kinetic]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default SmallReactiveArmor;

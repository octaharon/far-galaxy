import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { SmallArmor_Bias_Max, SmallArmor_Bias_Med } from '../../GenericArmor';

export class KevlarPlatingArmor extends GenericArmor {
	public static readonly id = 10000;
	public static readonly caption = 'Light Kevlar Plating';
	public static readonly description =
		'A plating made of processed carbon fibers. Light, somewhat efficient, but not too much';
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.15,
		},
	};
	public baseCost = 10;
	public armor = {
		default: {
			bias: SmallArmor_Bias_Med,
			min: 0,
		},
		[Damage.damageType.kinetic]: {
			bias: SmallArmor_Bias_Max,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default KevlarPlatingArmor;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { SmallArmor_Bias_Max, SmallArmor_Factor_Med } from '../../GenericArmor';

const protection = {
	bias: SmallArmor_Bias_Max,
	factor: SmallArmor_Factor_Med - 0.05,
	min: 0,
	minGuard: 0,
};

export class AdvancedPowerArmor extends GenericArmor {
	public static readonly id = 10010;
	public static readonly caption = 'Advanced Power Armor';
	public static readonly description = `An advanced sarcophagus type armor,
    designed to effectively resist the damaging factors of nuclear weapons and harsh environments. Has a SMA capacitor system, capable of transferring some of absorbed energy to the attached weapon systems, but is very bulky`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.35,
		},
		[UnitAttack.deliveryType.aerial]: {
			bias: -1,
		},
	};
	public static readonly healthModifier: IModifier = {
		factor: 1.15,
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.sma]: true,
	};
	public baseCost = 24;
	public armor = {
		default: {
			min: 0,
		},
		[Damage.damageType.radiation]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
		[Damage.damageType.biological]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default AdvancedPowerArmor;

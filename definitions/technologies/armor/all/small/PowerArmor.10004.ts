import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { SmallArmor_Bias_Max, SmallArmor_Factor_Med } from '../../GenericArmor';

const protection = {
	bias: SmallArmor_Bias_Max,
	factor: SmallArmor_Factor_Med + 0.05,
	min: 0,
	minGuard: 0,
};

export class PowerArmor extends GenericArmor {
	public static readonly id = 10004;
	public static readonly caption = 'Power Armor';
	public static readonly description = `An active sarcophagus type armor, designed to effectively resist the damaging factors inside a nuclear warzone`;
	public static readonly type = UnitDefense.TArmorSlotType.small;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		[UnitAttack.deliveryType.beam]: {
			bias: -0.5,
		},
		[UnitAttack.deliveryType.ballistic]: {
			bias: -0.5,
		},
		[UnitAttack.deliveryType.supersonic]: {
			bias: -0.5,
		},
	};
	public static readonly healthModifier: IModifier = {
		factor: 1.08,
	};
	public baseCost = 16;
	public armor = {
		[Damage.damageType.radiation]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
		[Damage.damageType.biological]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default PowerArmor;

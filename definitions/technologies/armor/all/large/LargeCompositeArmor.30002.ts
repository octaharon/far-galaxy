import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	LargeArmor_Bias_Max,
	LargeArmor_Bias_Med,
	LargeArmor_Factor_Max,
	LargeArmor_Factor_Min,
} from '../../GenericArmor';

const protection = {
	factor: LargeArmor_Factor_Max,
	bias: LargeArmor_Bias_Max,
	min: 0,
	minGuard: 0,
};

export class LargeCompositeArmor extends GenericArmor {
	public static readonly id = 30002;
	public static readonly caption = 'Large Composite Armor';
	public static readonly description = `The biggest version of composite carbon armor, offering
    substantial universal damage protection, but being especially good against explosions`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.2,
		},
	};
	public baseCost = 35;
	public armor = {
		default: {
			bias: LargeArmor_Bias_Med,
			factor: LargeArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
	};
}

export default LargeCompositeArmor;

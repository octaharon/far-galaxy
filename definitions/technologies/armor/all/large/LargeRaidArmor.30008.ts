import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	LargeArmor_Bias_Max,
	LargeArmor_Bias_Med,
	LargeArmor_Factor_Max,
	LargeArmor_Factor_Min,
} from '../../GenericArmor';

const protection = {
	bias: LargeArmor_Bias_Max,
	factor: LargeArmor_Factor_Max,
	min: 0,
	minGuard: 0,
};

export class LargeRaidArmor extends GenericArmor {
	public static readonly id = 30008;
	public static readonly caption = 'Large Raid Armor';
	public static readonly description = `The biggest version of custom Pirates armor, offering
    fantastic protection from some damage factors, while being one of the cheapest options on the market`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.2,
		},
	};
	public baseCost = 40;
	public armor = {
		default: {
			factor: LargeArmor_Factor_Min,
			bias: LargeArmor_Bias_Med,
			min: 0,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.biological]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default LargeRaidArmor;

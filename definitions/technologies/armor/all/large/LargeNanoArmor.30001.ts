import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { LargeArmor_Bias_Min, LargeArmor_Factor_Med } from '../../GenericArmor';

export class LargeNanoArmor extends GenericArmor {
	public static readonly id = 30001;
	public static readonly caption = 'Large Nano Armor';
	public static readonly description = `The biggest version of atomic fiber armor, offering the best universal damage protection and improved mobility`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public baseCost = 34;
	public armor = {
		default: {
			bias: LargeArmor_Bias_Min,
			factor: LargeArmor_Factor_Med,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default LargeNanoArmor;

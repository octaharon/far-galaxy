import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { LargeArmor_Bias_Med, LargeArmor_Factor_Max, LargeArmor_Threshold_Max } from '../../GenericArmor';

export class InertArmor extends GenericArmor {
	public static readonly id = 30011;
	public static readonly caption = 'Inert Armor';
	public static readonly description = `An armor plating made of foamed supercooled carbon,
    which requires a lot of power to keep temperature, but offers absolutely outstanding protection
    against less powerful weapons. The first impact on this armor releases some significant energy, which can be collected
    and wired to shield, if any. The system is extremely bulky, heavily limiting the dodging capabilities of the host`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.5,
		},
	};
	public static readonly healthModifier: IModifier = {
		factor: 1.15,
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.boosters]: true,
	};
	public baseCost = 45;
	public armor = {
		default: {
			threshold: LargeArmor_Threshold_Max,
			bias: LargeArmor_Bias_Med,
		},
		[Damage.damageType.heat]: {
			threshold: LargeArmor_Threshold_Max,
			bias: LargeArmor_Bias_Med,
			factor: LargeArmor_Factor_Max,
		},
	} as UnitDefense.TArmor;
}

export default InertArmor;

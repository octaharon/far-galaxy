import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	LargeArmor_Bias_Max,
	LargeArmor_Bias_Med,
	LargeArmor_Factor_Med,
	LargeArmor_Factor_Min,
} from '../../GenericArmor';

export class LargeKevlarArmor extends GenericArmor {
	public static readonly id = 30000;
	public static readonly caption = 'Heavy Kevlar Plating';
	public static readonly description = `The monolith version of carbon fiber armor, offering substantial universal damage protection, but portable only by the biggest of Chassis`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly dodgeModifier = {
		default: { bias: -0.25 },
	} as UnitDefense.TDodgeModifier;
	public baseCost = 30;
	public armor = {
		default: {
			bias: LargeArmor_Bias_Med,
			factor: LargeArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.kinetic]: {
			bias: LargeArmor_Bias_Max,
			factor: LargeArmor_Factor_Med,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default LargeKevlarArmor;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	LargeArmor_Bias_Max,
	LargeArmor_Bias_Min,
	LargeArmor_Factor_Max,
	LargeArmor_Factor_Min,
} from '../../GenericArmor';

export class LargeBioArmor extends GenericArmor {
	public static readonly id = 30007;
	public static readonly caption = 'Large Bio Armor';
	public static readonly description = `The largest version of bacteria armor, extremely resistant to chemicals.
    It strengthens when damaged, but also makes some weapon tracking easier`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly healthModifier: IModifier = {
		bias: 20,
	};
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		[UnitAttack.deliveryType.missile]: {
			bias: -0.5,
		},
		[UnitAttack.deliveryType.drone]: {
			bias: -0.5,
		},
		[UnitAttack.deliveryType.bombardment]: {
			bias: -0.5,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.resilient]: true,
	};
	public baseCost = 44;
	public armor = {
		default: {
			bias: LargeArmor_Bias_Min,
			factor: LargeArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.biological]: {
			bias: LargeArmor_Bias_Max,
			factor: LargeArmor_Factor_Max - 0.1,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default LargeBioArmor;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	LargeArmor_Bias_Max,
	LargeArmor_Factor_Max,
	LargeArmor_Threshold_Max,
	LargeArmor_Threshold_Med,
} from '../../GenericArmor';

const protection = {
	bias: LargeArmor_Bias_Max,
	factor: LargeArmor_Factor_Max,
	threshold: LargeArmor_Threshold_Max,
	min: 0,
};

export class LargeGluonArmor extends GenericArmor {
	public static readonly id = 30005;
	public static readonly caption = 'Large Gluon Armor';
	public static readonly description = `The largest version of quark-gluon plasma armor,
    which completely stops most particle beams, while offering little protection from other damaging factors`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly healthModifier: IModifier = {
		factor: 0.9,
	};
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		[UnitAttack.deliveryType.beam]: {
			bias: -0.5,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.warp]: true,
	};
	public baseCost = 40;
	public armor = {
		default: {
			threshold: LargeArmor_Threshold_Med,
		},
		[Damage.damageType.antimatter]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
		[Damage.damageType.radiation]: {
			...protection,
		},
	};
}

export default LargeGluonArmor;

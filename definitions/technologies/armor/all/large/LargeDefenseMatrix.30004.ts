import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { LargeArmor_Factor_Min } from '../../GenericArmor';

export class LargeDefenseMatrix extends GenericArmor {
	public static readonly id = 30004;
	public static readonly caption = 'Large Tachyon Matrix';
	public static readonly description = `The biggest version of tachyon field projector, which is not exactly an
    armor, but rather a displacement device, offering great dodging capabilities`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: 0.4,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.tachyon]: true,
	};
	public baseCost = 43;
	public armor = {
		default: {
			factor: LargeArmor_Factor_Min,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default LargeDefenseMatrix;

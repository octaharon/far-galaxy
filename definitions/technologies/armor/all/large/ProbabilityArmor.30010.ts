import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { LargeArmor_Bias_Max, LargeArmor_Factor_Med } from '../../GenericArmor';

const protection = {
	factor: LargeArmor_Factor_Med,
	bias: LargeArmor_Bias_Max,
	min: 0,
	minGuard: 0,
};

export class ProbabilityArmor extends GenericArmor {
	public static readonly id = 30010;
	public static readonly caption = 'Probability Armor';
	public static readonly description = `One of experimental technologies, involving materials based on quantum entanglement,
    which collapses at points of excitement, absorbing some of excited particles and making focusing of gravity waves much harder.
    Also those materials have the potential to dissipate incoming damage completely`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: 0.2,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.warp]: true,
	};
	public baseCost = 44;
	public armor = {
		[Damage.damageType.warp]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
		[Damage.damageType.radiation]: {
			...protection,
		},
		default: {
			min: 0,
			minGuard: 0,
			bias: LargeArmor_Bias_Max,
		},
	} as UnitDefense.TArmor;
}

export default ProbabilityArmor;

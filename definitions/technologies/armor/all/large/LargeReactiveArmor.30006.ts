import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { LargeArmor_Bias_Med, LargeArmor_Bias_Min, LargeArmor_Factor_Med } from '../../GenericArmor';

const protection = {
	bias: LargeArmor_Bias_Med,
	factor: LargeArmor_Factor_Med,
	min: 0,
	minGuard: 0,
};

export class LargeReactiveArmor extends GenericArmor {
	public static readonly id = 30006;
	public static readonly caption = 'Large Reactive Armor';
	public static readonly description = `The biggest version of exploding reactive armor,
    which counteracts most known types of armor piercing ammunition`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public baseCost = 39;
	public armor = {
		default: {
			bias: LargeArmor_Bias_Min,
			min: 0,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.kinetic]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default LargeReactiveArmor;

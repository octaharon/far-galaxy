import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	LargeArmor_Bias_Max,
	LargeArmor_Bias_Med,
	LargeArmor_Factor_Max,
	LargeArmor_Factor_Min,
	LargeArmor_Threshold_Max,
} from '../../GenericArmor';

export class LargeQuantumArmor extends GenericArmor {
	public static readonly id = 30003;
	public static readonly caption = 'Large Quantum Armor';
	public static readonly description = `The biggest version of fluid carbon armor, offering
    the best projectile damage reduction in class and a solid uniform protection`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly dodgeModifier = {
		default: { bias: -0.3 },
	} as UnitDefense.TDodgeModifier;
	public baseCost = 37;
	public armor = {
		default: {
			bias: LargeArmor_Bias_Max,
			factor: LargeArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.kinetic]: {
			bias: LargeArmor_Bias_Med,
			factor: LargeArmor_Factor_Max,
			min: 0,
			threshold: LargeArmor_Threshold_Max,
		},
	} as UnitDefense.TArmor;
}

export default LargeQuantumArmor;

import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	LargeArmor_Bias_Med,
	LargeArmor_Factor_Max,
	LargeArmor_Factor_Min,
	LargeArmor_Threshold_Med,
	LargeArmor_Threshold_Min,
} from '../../GenericArmor';

const protection = {
	bias: LargeArmor_Bias_Med,
	threshold: LargeArmor_Threshold_Med,
	factor: LargeArmor_Factor_Max,
	min: 0,
	minGuard: 0,
};

export class SMESArmor extends GenericArmor {
	public static readonly id = 30009;
	public static readonly caption = 'SMA Armor';
	public static readonly description = `The biggest version of superconductor magnetic array ever created
    connected to the most advanced military composite armor`;
	public static readonly type = UnitDefense.TArmorSlotType.large;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.25,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.sma]: true,
	};
	public baseCost = 43;
	public armor = {
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.antimatter]: {
			...protection,
		},
		default: {
			factor: LargeArmor_Factor_Min,
			threshold: LargeArmor_Threshold_Min,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default SMESArmor;

import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { MediumArmor_Bias_Med, MediumArmor_Bias_Min, MediumArmor_Factor_Med } from '../../GenericArmor';

const protection = {
	bias: MediumArmor_Bias_Med,
	factor: MediumArmor_Factor_Med,
	min: 0,
	minGuard: 0,
};

export class ReactiveArmor extends GenericArmor {
	public static readonly id = 20008;
	public static readonly caption = 'Reactive Armor';
	public static readonly description = `A bigger version of exploding reactive armor,
    which counteracts most known types of armor piercing ammunition`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public baseCost = 30;
	public armor = {
		default: {
			bias: MediumArmor_Bias_Min,
			min: 0,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.kinetic]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default ReactiveArmor;

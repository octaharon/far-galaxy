import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { MediumArmor_Factor_Min } from '../../GenericArmor';

export class DefenseMatrix extends GenericArmor {
	public static readonly id = 20005;
	public static readonly caption = 'Tachyon Matrix';
	public static readonly description = `A bigger version of tachyon field projector, which is not exactly
    armor, but rather a displacement device, offering great dodge capabilities`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: 0.4,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.tachyon]: true,
	};
	public baseCost = 32;
	public armor = {
		default: {
			factor: MediumArmor_Factor_Min,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default DefenseMatrix;

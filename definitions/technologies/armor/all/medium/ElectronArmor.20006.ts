import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { MediumArmor_Bias_Max, MediumArmor_Factor_Med } from '../../GenericArmor';

const protection = {
	bias: MediumArmor_Bias_Max,
	factor: MediumArmor_Factor_Med,
	min: 0,
	minGuard: 0,
};

export class ElectronArmor extends GenericArmor {
	public static readonly id = 20006;
	public static readonly caption = 'Electron Armor';
	public static readonly description = `Magnetically shaped electron cloud
    protects from most directed energy weapons by dissipating electromagnetic waves due to Compton scattering effect and also deactivates most chemicals by saturating ions.`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public baseCost = 30;
	public armor = {
		[Damage.damageType.radiation]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
		[Damage.damageType.biological]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default ElectronArmor;

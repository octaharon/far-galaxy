import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	MediumArmor_Bias_Max,
	MediumArmor_Bias_Med,
	MediumArmor_Factor_Max,
	MediumArmor_Factor_Min,
} from '../../GenericArmor';

const protection = {
	factor: MediumArmor_Factor_Max,
	bias: MediumArmor_Bias_Max,
	min: 0,
	minGuard: 0,
};

export class CompositeArmor extends GenericArmor {
	public static readonly id = 20002;
	public static readonly caption = 'Composite Armor';
	public static readonly description = `The most advanced version of carbon armor, combining
    fiber and atomic plating for even better protection against explosions`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public baseCost = 25;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.15,
		},
	};
	public armor = {
		default: {
			bias: MediumArmor_Bias_Med,
			factor: MediumArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
	};
}

export default CompositeArmor;

import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { MediumArmor_Factor_Max, MediumArmor_Threshold_Med } from '../../GenericArmor';

const protection = {
	factor: MediumArmor_Factor_Max,
	threshold: MediumArmor_Threshold_Med,
	min: 0,
	minGuard: 0,
};

export class PositronArmor extends GenericArmor {
	public static readonly id = 20007;
	public static readonly caption = 'Positron Armor';
	public static readonly description = `An advanced version of electron armor, using positrons instead and
    storing dissipated annihilation energy in a SMA, which is then used to improve the firepower of
    connected weapons.`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.sma]: true,
	};
	public baseCost = 34;
	public armor = {
		[Damage.damageType.radiation]: {
			...protection,
		},
		[Damage.damageType.heat]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
		[Damage.damageType.antimatter]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default PositronArmor;

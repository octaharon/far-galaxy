import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	MediumArmor_Bias_Max,
	MediumArmor_Factor_Max,
	MediumArmor_Threshold_Max,
	MediumArmor_Threshold_Med,
} from '../../GenericArmor';

const protection = {
	bias: MediumArmor_Bias_Max,
	factor: MediumArmor_Factor_Max,
	threshold: MediumArmor_Threshold_Max,
	min: 0,
	minGuard: 0,
};

export class GluonArmor extends GenericArmor {
	public static readonly id = 20009;
	public static readonly caption = 'Gluon Armor';
	public static readonly description = `A thicker layer of quark-gluon plasma suspended in a lattice of gravity fields around the host,
    which stops most low energy particles in their tracks, while offering little protection from other damaging factors`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.warp]: true,
	};
	public static readonly healthModifier: IModifier = {
		factor: 0.9,
	};
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		[UnitAttack.deliveryType.beam]: {
			bias: -0.5,
		},
	};
	public baseCost = 33;
	public armor = {
		default: {
			threshold: MediumArmor_Threshold_Med,
		},
		[Damage.damageType.antimatter]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
		[Damage.damageType.radiation]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default GluonArmor;

import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, { MediumArmor_Bias_Med, MediumArmor_Bias_Min, MediumArmor_Factor_Med } from '../../GenericArmor';

export class NanoArmor extends GenericArmor {
	public static readonly id = 20001;
	public static readonly caption = 'Nano Armor';
	public static readonly description = `A plated atomic carbon armor, offering solid universal protection
    while not being as bulky compared as basic kevlar armor.`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public baseCost = 24;
	public armor = {
		default: {
			bias: MediumArmor_Bias_Min,
			factor: MediumArmor_Factor_Med,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default NanoArmor;

import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	MediumArmor_Bias_Max,
	MediumArmor_Bias_Med,
	MediumArmor_Factor_Max,
	MediumArmor_Factor_Min,
	MediumArmor_Threshold_Max,
} from '../../GenericArmor';

export class QuantumArmor extends GenericArmor {
	public static readonly id = 20003;
	public static readonly caption = 'Quantum Armor';
	public static readonly description = `One of many versions of carbon foam armor, being
    a bit clumsy but providing exceptional defense against projectiles`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public static readonly dodgeModifier = {
		default: {
			bias: -0.25,
		},
	} as UnitDefense.TDodgeModifier;
	public baseCost = 28;
	public armor = {
		default: {
			bias: MediumArmor_Bias_Max,
			factor: MediumArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.kinetic]: {
			bias: MediumArmor_Bias_Med,
			threshold: MediumArmor_Threshold_Max,
			factor: MediumArmor_Factor_Max,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default QuantumArmor;

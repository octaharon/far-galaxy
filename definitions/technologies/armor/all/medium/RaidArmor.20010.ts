import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	MediumArmor_Bias_Max,
	MediumArmor_Bias_Med,
	MediumArmor_Factor_Max,
	MediumArmor_Factor_Min,
} from '../../GenericArmor';

const protection = {
	bias: MediumArmor_Bias_Max,
	factor: MediumArmor_Factor_Max,
	min: 0,
	minGuard: 0,
};

export class RaidArmor extends GenericArmor {
	public static readonly id = 20010;
	public static readonly caption = 'Raid Armor';
	public static readonly description = `A development of Pirates scientific complex, offering
    formidable protection from some damage factors, while being one of the most cost-efficient options on the market`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		default: {
			bias: -0.15,
		},
	};
	public baseCost = 32;
	public armor = {
		default: {
			factor: MediumArmor_Factor_Min,
			bias: MediumArmor_Bias_Med,
			min: 0,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.biological]: {
			...protection,
		},
		[Damage.damageType.magnetic]: {
			...protection,
		},
	} as UnitDefense.TArmor;
}

export default RaidArmor;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	MediumArmor_Bias_Max,
	MediumArmor_Bias_Min,
	MediumArmor_Factor_Max,
	MediumArmor_Factor_Min,
} from '../../GenericArmor';

export class BioArmor extends GenericArmor {
	public static readonly id = 20004;
	public static readonly caption = 'Bio Armor';
	public static readonly description = `A larger version of bacteria armor, extremely good versus chemicals and becoming
    stronger when damaged, but it also makes weapon tracking easier`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public static readonly healthModifier: IModifier = {
		bias: 15,
	};
	public static readonly dodgeModifier: UnitDefense.TDodgeModifier = {
		[UnitAttack.deliveryType.missile]: {
			bias: -0.5,
		},
		[UnitAttack.deliveryType.drone]: {
			bias: -0.5,
		},
		[UnitAttack.deliveryType.bombardment]: {
			bias: -0.5,
		},
	};
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.resilient]: true,
	};
	public baseCost = 33;
	public armor = {
		default: {
			bias: MediumArmor_Bias_Min,
			factor: MediumArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.biological]: {
			bias: MediumArmor_Bias_Max,
			factor: MediumArmor_Factor_Max - 0.1,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default BioArmor;

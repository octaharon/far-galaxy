import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericArmor, {
	MediumArmor_Bias_Max,
	MediumArmor_Bias_Med,
	MediumArmor_Factor_Med,
	MediumArmor_Factor_Min,
} from '../../GenericArmor';

export class KevlarArmor extends GenericArmor {
	public static readonly id = 20000;
	public static readonly caption = 'Kevlar Plating';
	public static readonly description = `3D-printed plates of carbon fiber, providing solid protection against most damaging factors and being a versatile source of Armor for various Chassis`;
	public static readonly type = UnitDefense.TArmorSlotType.medium;
	public static readonly dodgeModifier = {
		default: {
			bias: -0.2,
		},
	} as UnitDefense.TDodgeModifier;
	public baseCost = 20;
	public armor = {
		default: {
			bias: MediumArmor_Bias_Med,
			factor: MediumArmor_Factor_Min,
			min: 0,
		},
		[Damage.damageType.kinetic]: {
			bias: MediumArmor_Bias_Max,
			factor: MediumArmor_Factor_Med,
			min: 0,
		},
	} as UnitDefense.TArmor;
}

export default KevlarArmor;

import { TArmorItem } from '../types/armor';
import GenericArmor from './GenericArmor';

let ArmorList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), ArmorList);
ArmorList = ArmorList.filter(
	(module: any) => module.default.prototype instanceof GenericArmor && module.default.id > 0,
).map((module: any) => module.default);
export default ArmorList as Array<TArmorItem<GenericArmor>>;

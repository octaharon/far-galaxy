import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import $$ from '../../maths/BasicMaths';
import UnitDefense from '../../tactical/damage/defense';
import { DefenseManager } from '../../tactical/damage/DefenseManager';
import UnitPart from '../../tactical/units/UnitPart';
import { IArmor, IArmorMeta, IArmorStatic, TArmorProps, TSerializedArmor } from '../types/armor';

export class GenericArmor extends UnitPart<TArmorProps, TSerializedArmor, IArmorMeta, IArmorStatic> implements IArmor {
	public static readonly id: number = 0;
	public static readonly type: UnitDefense.TArmorSlotType;
	public static readonly dodgeModifier?: UnitDefense.TDodgeModifier;
	public static readonly healthModifier?: IModifier;
	public static readonly flags?: UnitDefense.TDefenseFlags;
	public armor: UnitDefense.TArmor;

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.armor];
	}

	public get compiled(): UnitDefense.TArmor {
		return {
			...this.armor,
		};
	}

	public serialize(): TSerializedArmor {
		return {
			...this.__serializeUnitPart(),
			armor: DefenseManager.serializeProtection(this.armor),
		};
	}

	public applyModifier<T extends UnitDefense.TArmorModifier>(...m: T[]): GenericArmor {
		const t = DefenseManager.stackArmorModifiers(...(m || []));
		if (!t) return this;
		this.baseCost = $$.applyModifier(this.baseCost, t.baseCost);
		this.armor = DefenseManager.serializeProtection(DefenseManager.stackDamageProtectionDefinition(this.armor, t));
		return this;
	}
}

export const SmallArmor_Bias_Min = -3;
export const SmallArmor_Bias_Med = -5;
export const SmallArmor_Bias_Max = -10;
export const MediumArmor_Bias_Min = -5;
export const MediumArmor_Bias_Med = -8;
export const MediumArmor_Bias_Max = -15;
export const LargeArmor_Bias_Min = -8;
export const LargeArmor_Bias_Med = -15;
export const LargeArmor_Bias_Max = -25;

export const SmallArmor_Threshold_Min = 3;
export const SmallArmor_Threshold_Med = 10;
export const SmallArmor_Threshold_Max = 15;
export const MediumArmor_Threshold_Min = 5;
export const MediumArmor_Threshold_Med = 15;
export const MediumArmor_Threshold_Max = 25;
export const LargeArmor_Threshold_Min = 10;
export const LargeArmor_Threshold_Med = 25;
export const LargeArmor_Threshold_Max = 40;

export const SmallArmor_Factor_Min = 0.9;
export const SmallArmor_Factor_Med = 0.8;
export const SmallArmor_Factor_Max = 0.7;
export const MediumArmor_Factor_Min = 0.85;
export const MediumArmor_Factor_Med = 0.7;
export const MediumArmor_Factor_Max = 0.6;
export const LargeArmor_Factor_Min = 0.75;
export const LargeArmor_Factor_Med = 0.6;
export const LargeArmor_Factor_Max = 0.5;

export default GenericArmor;

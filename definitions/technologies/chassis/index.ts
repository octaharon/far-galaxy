import { TChassis } from '../types/chassis';
import GenericChassis from './genericChassis';

let ChassisList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), ChassisList);
ChassisList = ChassisList.filter(
	(module: any) => module.default.prototype instanceof GenericChassis && module.default.id > 0,
).map((module: any) => module.default);
export default ChassisList as TChassis[];
export { ChassisClassTitles } from './constants';

import UnitActions from '../../tactical/units/actions/types';
import UnitChassis from '../types/chassis';

export const GenericAircraftActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
] as UnitActions.TUnitActionOptions;
export const GenericAircraftActionPhases = 3;

export const groundChassisList = [
	UnitChassis.chassisClass.infantry,
	UnitChassis.chassisClass.droid,
	UnitChassis.chassisClass.scout,
	UnitChassis.chassisClass.tank,
	UnitChassis.chassisClass.artillery,
	UnitChassis.chassisClass.sup_vehicle,
	UnitChassis.chassisClass.biotech,
	UnitChassis.chassisClass.exosuit,
	UnitChassis.chassisClass.lav,
	UnitChassis.chassisClass.hav,
	UnitChassis.chassisClass.quadrapod,
	UnitChassis.chassisClass.mecha,
];
export const hoverChassisList = [
	UnitChassis.chassisClass.science_vessel,
	UnitChassis.chassisClass.pointdefense,
	UnitChassis.chassisClass.combatdrone,
	UnitChassis.chassisClass.rover,
	UnitChassis.chassisClass.hovertank,
	UnitChassis.chassisClass.launcherpad,
	UnitChassis.chassisClass.fortress,
];
export const navalChassisList = [
	UnitChassis.chassisClass.corvette,
	UnitChassis.chassisClass.sup_ship,
	UnitChassis.chassisClass.destroyer,
	UnitChassis.chassisClass.submarine,
	UnitChassis.chassisClass.hydra,
	UnitChassis.chassisClass.cruiser,
	UnitChassis.chassisClass.battleship,
	UnitChassis.chassisClass.manofwar,
];
export const airChassisList = [
	UnitChassis.chassisClass.tiltjet,
	UnitChassis.chassisClass.strike_aircraft,
	UnitChassis.chassisClass.sup_aircraft,
	UnitChassis.chassisClass.fighter,
	UnitChassis.chassisClass.bomber,
	UnitChassis.chassisClass.swarm,
	UnitChassis.chassisClass.carrier,
	UnitChassis.chassisClass.helipad,
	UnitChassis.chassisClass.mothership,
];
export const orderedChassisList: UnitChassis.chassisClass[] = [].concat(
	groundChassisList,
	navalChassisList,
	hoverChassisList,
	airChassisList,
);
export const ChassisClassTitles: EnumProxyWithDefault<UnitChassis.chassisClass, string> = {
	[UnitChassis.chassisClass.droid]: 'Droid',
	[UnitChassis.chassisClass.sup_ship]: 'Support Ship',
	[UnitChassis.chassisClass.sup_aircraft]: 'Support Aircraft',
	[UnitChassis.chassisClass.sup_vehicle]: 'Support Vehicle',
	[UnitChassis.chassisClass.biotech]: 'Biotech',
	[UnitChassis.chassisClass.mecha]: 'Mecha',
	[UnitChassis.chassisClass.tank]: 'Tank',
	[UnitChassis.chassisClass.artillery]: 'Artillery',
	[UnitChassis.chassisClass.scout]: 'Scout',
	[UnitChassis.chassisClass.science_vessel]: 'Science Vessel',
	[UnitChassis.chassisClass.quadrapod]: 'Quadrapod',
	[UnitChassis.chassisClass.hovertank]: 'Hovertank',
	[UnitChassis.chassisClass.exosuit]: 'Exosuit',
	[UnitChassis.chassisClass.infantry]: 'Infantry',
	[UnitChassis.chassisClass.combatdrone]: 'Combat Drone',
	[UnitChassis.chassisClass.launcherpad]: 'Launcher Pad',
	[UnitChassis.chassisClass.carrier]: 'Carrier',
	[UnitChassis.chassisClass.helipad]: 'Helipad',
	[UnitChassis.chassisClass.hydra]: 'Hydra',
	[UnitChassis.chassisClass.fortress]: 'Fortress',
	[UnitChassis.chassisClass.battleship]: 'Battleship',
	[UnitChassis.chassisClass.bomber]: 'Bomber',
	[UnitChassis.chassisClass.corvette]: 'Corvette',
	[UnitChassis.chassisClass.destroyer]: 'Destroyer',
	[UnitChassis.chassisClass.cruiser]: 'Cruiser',
	[UnitChassis.chassisClass.fighter]: 'Fighter',
	[UnitChassis.chassisClass.hav]: 'Heavy Assault Vehicle',
	[UnitChassis.chassisClass.lav]: 'Light Assault Vehicle',
	[UnitChassis.chassisClass.manofwar]: 'Man-Of-War',
	[UnitChassis.chassisClass.mothership]: 'Mothership',
	[UnitChassis.chassisClass.pointdefense]: 'Point Defense Drone',
	[UnitChassis.chassisClass.rover]: 'Rover',
	[UnitChassis.chassisClass.strike_aircraft]: 'Strike Aircraft',
	[UnitChassis.chassisClass.submarine]: 'Submarine',
	[UnitChassis.chassisClass.swarm]: 'Swarm',
	[UnitChassis.chassisClass.tiltjet]: 'Tiltjet',
	[UnitChassis.chassisClass.none]: 'Unknown Chassis',
	default: 'Chassis',
};

export const TargetTypeOrderWithDefault: Array<'default' | UnitChassis.targetType> = [
	'default',
	UnitChassis.targetType.organic,
	UnitChassis.targetType.robotic,
	UnitChassis.targetType.massive,
	UnitChassis.targetType.unique,
];

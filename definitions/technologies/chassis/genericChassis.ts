import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import BasicMaths from '../../maths/BasicMaths';
import Series from '../../maths/Series';
import { IUnitEffectModifier } from '../../prototypes/types';
import UnitDefense from '../../tactical/damage/defense';
import DefenseManager from '../../tactical/damage/DefenseManager';
import { TAuraApplianceList } from '../../tactical/statuses/Auras/types';
import { TEffectApplianceList } from '../../tactical/statuses/types';
import { addActionPhase, grantAction } from '../../tactical/units/actions/ActionManager';
import UnitActions from '../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../tactical/units/levels/levelingFunctions';
import UnitTalents from '../../tactical/units/levels/unitTalents';
import UnitPart from '../../tactical/units/UnitPart';
import PoliticalFactions from '../../world/factions';
import UnitChassis, { IChassis, IChassisMeta, IChassisStatic } from '../types/chassis';
import Weapons from '../types/weapons';

export class GenericChassis
	extends UnitPart<UnitChassis.TChassisProps, UnitChassis.TSerializedChassis, IChassisMeta, IChassisStatic>
	implements IChassis
{
	public static readonly type: UnitChassis.chassisClass = null;
	public static readonly flags: UnitChassis.TTargetFlags = {};
	public static readonly movement: UnitChassis.movementType = UnitChassis.movementType.ground;
	public static readonly restrictedFactions?: PoliticalFactions.TFactionID[] = [];
	public static readonly exclusiveFactions?: PoliticalFactions.TFactionID[] = [];
	public static readonly weaponSlots: Weapons.TWeaponSlotType[] = [];
	public static readonly armorSlots: UnitDefense.TArmorSlotType[] = [];
	public static readonly shieldSlots: UnitDefense.TShieldSlotType[] = [];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {};
	public static readonly armorModifier?: UnitDefense.TArmorModifier = {};
	public static readonly shieldModifier?: UnitDefense.TShieldsModifier = {};
	public static readonly effects: TEffectApplianceList = [];
	public static readonly auras: TAuraApplianceList = [];
	public static readonly unitEffectModifier: IUnitEffectModifier = {};
	public static readonly abilities: number[] = [];
	public static readonly levelTree: UnitTalents.ITalentTree = {
		levelingFunction: getLevelingFunction(TLevelingFunction.Linear100),
		levelingEffectFunction: (level, unit) => unit,
		talentOptions: [],
	};
	public hitPoints: number;
	public actions: UnitActions.TUnitActionOptions[];
	public speed: number;
	public scanRange: number;
	public equipmentSlots: number;
	public dodge: UnitDefense.TDodge;
	public defenseFlags: UnitDefense.TDefenseFlags;

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.chassis];
	}

	public serialize(): UnitChassis.TSerializedChassis {
		Object.keys(this.dodge).forEach((k: keyof UnitDefense.TDodge) => {
			if (!this.dodge[k]) delete this.dodge[k];
		});
		return {
			...this.__serializeUnitPart(),
			scanRange: this.scanRange,
			equipmentSlots: this.equipmentSlots,
			defenseFlags: this.defenseFlags,
			dodge: this.dodge,
			speed: this.speed,
			hitPoints: this.hitPoints,
			actions: this.actions,
		};
	}

	public applyModifier(...m: UnitChassis.TUnitChassisModifier[]): IChassis {
		m = [].concat(m).filter((v) => !!v) as UnitChassis.TUnitChassisModifier[];
		if (!m.length) return this;
		this.hitPoints = BasicMaths.applyModifier(this.hitPoints, ..._.pluck(m, 'hitPoints'));
		this.dodge = DefenseManager.stackDodge(this.dodge, ..._.pluck(m, 'dodge'));
		this.baseCost = BasicMaths.applyModifier(this.baseCost, ..._.pluck(m, 'baseCost'));
		this.scanRange = BasicMaths.applyModifier(this.scanRange, ..._.pluck(m, 'scanRange'));
		this.speed = BasicMaths.applyModifier(this.speed, ..._.pluck(m, 'speed'));
		const actionOptions = _.uniq(_.flatten(_.pluck(m, 'actions').filter(Boolean)));
		const actionMod = m.reduce((value, mod) => (value += mod.actionCount || 0), 0);
		if (actionOptions && actionOptions.length) this.actions = grantAction(this.actions, ...actionOptions);
		if (actionMod !== 0) this.actions = addActionPhase(this.actions, actionMod);
		const flags = _.pluck(m, 'defenseFlags');
		flags.forEach((f) => {
			this.defenseFlags = {
				...(this.defenseFlags || {}),
				...f,
			};
		});
		this.equipmentSlots = Math.max(0, (this.equipmentSlots || 0) + Series.sum(_.pluck(m, 'equipmentSlots')) || 0);
		return this;
	}
}

export default GenericChassis;

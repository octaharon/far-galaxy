import SmokeScreenAbility from '../../../../tactical/abilities/all/Units/SmokeScreen.60003';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import Weapons from '../../../types/weapons';
import GenericDroid from './GenericDroid';

export class M5Chassis extends GenericDroid {
	public static readonly id = 25004;
	public static readonly caption = 'M5 Droid';
	public static readonly description = `Military support droid, having weapon mount reduced in favor of military equipment rack and a better sensor`;
	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		default: {
			bias: -2,
		},
	};
	public static readonly abilities: number[] = [SmokeScreenAbility.id];
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.small] as Weapons.TWeaponSlotType[];
	public static readonly levelTree = {
		...GenericDroid.levelTree,
		levelingFunction: getLevelingFunction(TLevelingFunction.Prodigy),
	};
	public baseCost: number = this.baseCost + 8;
	public equipmentSlots: number = (this.equipmentSlots || 0) + 1;
	public scanRange: number = this.scanRange + 4;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.defend);
}

export default M5Chassis;

import GenericDroid from './GenericDroid';

export class DroidChassis extends GenericDroid {
	public static readonly id = 25000;
	public static readonly caption = 'Droid';
	public static readonly description = `Basic robotic unit with a fine-tuned AI, capable of independent operation. Small, basic, only good in numbers`;
}

export default DroidChassis;

import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import PoliticalFactions from '../../../../world/factions';
import GenericDroid from './GenericDroid';
import TFactionID = PoliticalFactions.TFactionID;

export class T1024Chassis extends GenericDroid {
	public static readonly id = 25001;
	public static readonly caption = 'T-1024 Droid';
	public static readonly description = `A custom droid series, produced by Pirates, that has enhanced transportation elements and a cloaking module`;
	public static readonly exclusiveFactions: TFactionID[] = [TFactionID.pirates];
	public static readonly armorModifier = {} as UnitDefense.TArmorModifier;
	public baseCost: number = this.baseCost + 3;
	public speed: number = this.speed + 2;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.cloak);
}

export default T1024Chassis;

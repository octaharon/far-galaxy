import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericDroid from './GenericDroid';

export class EnforcerChassis extends GenericDroid {
	public static readonly id = 25002;
	public static readonly caption = 'Enforcer Droid';
	public static readonly description = `A police model of droid, possessing an integrated machine gun slot and best protection in class`;
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Guns]: {
			baseRate: {
				bias: 0.4,
			},
			baseAccuracy: {
				bias: 0.4,
			},
		},
	};
	public static readonly armorModifier = {
		default: {
			bias: -4,
			threshold: 4,
			min: 0, // but never negative
		},
	} as UnitDefense.TArmorModifier;
	public baseCost: number = this.baseCost + 4;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.defend);
}

export default EnforcerChassis;

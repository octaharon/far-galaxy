import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericDroid from './GenericDroid';

export class HunterChassis extends GenericDroid {
	public static readonly id = 25003;
	public static readonly caption = 'Hunter Droid';
	public static readonly description = `Military droid purposed to rapidly approach and take down important enemies`;
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			damageModifier: {
				factor: 1.1,
			},
		},
	};
	public static readonly armorModifier = {} as UnitDefense.TArmorModifier;
	public speed: number = this.speed + 3;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
	public baseCost: number = this.baseCost + 4;
}

export default HunterChassis;

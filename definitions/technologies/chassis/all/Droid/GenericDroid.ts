import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import InfantryRankTree from '../../../../tactical/units/levels/RankTrees/InfantryRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericDroidActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericDroidActionPhases = 2;

export class GenericDroid extends GenericChassis {
	public static readonly levelTree = InfantryRankTree;
	public static readonly type = UnitChassis.chassisClass.droid;
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.medium] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {
		default: {
			bias: 2, // +2 incoming damage,
			min: 0, // but never negative
		},
	} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public hitPoints = 30;
	public speed = 5;
	public baseCost = 10;
	public scanRange = 7;
	public dodge = {
		default: 0.5,
	} as UnitDefense.TDodge;
	public actions = createActionOptions(GenericDroidActions, GenericDroidActionPhases);
}

export default GenericDroid;

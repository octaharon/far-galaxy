import { fastMerge } from '../../../../core/dataTransformTools';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericCarrier from './GenericCarrier';

export class LeviathanChassis extends GenericCarrier {
	public static readonly id = 88001;
	public static readonly caption = 'Leviathan';
	public static readonly description = `An aerostat variety of carrier airship, having even less mobility and exposing significant thermal signatures which makes it an easy target for high-precision weapons. Extra lifting power provides for a better energy source and a magnetic hull, though.`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = fastMerge(GenericCarrier.weaponModifier, {
		default: {
			baseRange: {
				bias: 2,
			},
			baseRate: {
				bias: 0.2,
			},
		},
	});
	public hitPoints: number = this.hitPoints + 5;
	public baseCost: number = this.baseCost + 5;
	public speed: number = this.speed - 4;
	public defenseFlags = {
		[UnitDefense.defenseFlags.hull]: true,
	};
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.aerial]: -1.25,
		[UnitAttack.deliveryType.ballistic]: 0.25,
		[UnitAttack.deliveryType.supersonic]: -0.5,
		[UnitAttack.deliveryType.missile]: -0.5,
		[UnitAttack.deliveryType.drone]: -0.5,
	};
	public scanRange: number = this.scanRange + 2;
}

export default LeviathanChassis;

import GenericCarrier from './GenericCarrier';

export class CarrierChassis extends GenericCarrier {
	public static readonly id = 88000;
	public static readonly caption = 'Carrier';
	public static readonly description = `A huge aircraft purposed accommodate two automated assembly hangars for small unmanned air vehicles, as well as a cargo bay, allowing to transport Units onboard. The hull is equipped with magnetic repulsion field generator, granting Shield Overcharge and HULL Module`;
}

export default CarrierChassis;

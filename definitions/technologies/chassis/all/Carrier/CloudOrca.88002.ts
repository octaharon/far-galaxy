import FieldRepairsAbility from '../../../../tactical/abilities/all/Units/FieldRepairs.29103';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantAction, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericCarrier, { GenericCarrierActionPhases, GenericCarrierActions } from './GenericCarrier';

export class CloudOrcaChassis extends GenericCarrier {
	public static readonly id = 88002;
	public static readonly caption = 'Cloud Orca';
	public static readonly description = `A swifter version of carrier airship, having only 1 hangar bay and unable to carry other Units, exchanging that for greater mobility and better survivability`;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.hangar] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [FieldRepairsAbility.id];
	public hitPoints: number = this.hitPoints - 10;
	public baseCost: number = this.baseCost + 3;
	public speed: number = this.speed + 4;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.aerial]: 0,
		[UnitAttack.deliveryType.ballistic]: 1,
		[UnitAttack.deliveryType.supersonic]: 0.5,
	};
	public defenseFlags = {
		[UnitDefense.defenseFlags.evasion]: true,
	};
	public actions: UnitActions.TUnitActionOptions[] = revokeAction(
		grantAction(
			createActionOptions(GenericCarrierActions, GenericCarrierActionPhases + 1),
			UnitActions.unitActionTypes.delay,
		),
		UnitActions.unitActionTypes.barrage,
	);
}

export default CloudOrcaChassis;

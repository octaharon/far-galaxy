import UnitLiftAbility from '../../../../tactical/abilities/all/Units/UnitLift.29201';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AircraftRankTree from '../../../../tactical/units/levels/RankTrees/AircraftRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericCarrierActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.retaliate,
] as UnitActions.TUnitActionOptions;

export const GenericCarrierActionPhases = 2;

export class GenericCarrier extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.carrier;
	public static readonly levelTree = AircraftRankTree;
	public static readonly movement = UnitChassis.movementType.air as UnitChassis.movementType;
	public static readonly flags = {
		[UnitChassis.targetType.massive]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.hangar,
		Weapons.TWeaponSlotType.hangar,
	] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [UnitLiftAbility.id];
	public static readonly shieldModifier = {
		overchargeDecay: {
			min: 5,
		},
		default: {
			threshold: 6,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 170;
	public baseCost = 32;
	public speed = 10;
	public scanRange = 5;
	public dodge: UnitDefense.TDodge = {
		[UnitAttack.deliveryType.aerial]: -1,
		[UnitAttack.deliveryType.ballistic]: 0.5,
		[UnitAttack.deliveryType.bombardment]: 10,
		[UnitAttack.deliveryType.surface]: 10,
	};
	public actions: UnitActions.TUnitActionOptions[] = createActionOptions(
		GenericCarrierActions,
		GenericCarrierActionPhases,
	);
}

export default GenericCarrier;

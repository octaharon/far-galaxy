import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import PoliticalFactions from '../../../../world/factions';
import Weapons from '../../../types/weapons';
import GenericPointDefense from './GenericPointDefense';
import TFactionID = PoliticalFactions.TFactionID;

const modifier = {
	baseCost: {
		bias: -3,
		min: 5,
	},
	damageModifier: {
		factor: 1.25,
	},
	baseRate: {
		factor: 1.5,
	},
};

export class AATurretChassis extends GenericPointDefense {
	public static readonly id = 57003;
	public static readonly caption = 'AA turret';
	public static readonly description = `A point defense unit, tailored to be most efficient with anti-air weapons and extra bombing protection. However, the design defect reduces effective scanning range and exposes vulnerabilities in shield generator`;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [TFactionID.stellarians];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: {
				factor: 1.5,
			},
		},
		[Weapons.TWeaponGroupType.AntiAir]: modifier,
	};
	public static readonly shieldModifier = {
		...GenericPointDefense.shieldModifier,
		[Damage.damageType.magnetic]: { factor: 1.1 },
		[Damage.damageType.radiation]: { factor: 1.1 },
	} as UnitDefense.TShieldsModifier;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.bombardment]: 10,
	};
	public baseCost: number = this.baseCost + 7;
	public scanRange: number = this.scanRange - 2;
}

export default AATurretChassis;

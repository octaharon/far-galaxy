import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import PoliticalFactions from '../../../../world/factions';
import Weapons from '../../../types/weapons';
import GenericPointDefense from './GenericPointDefense';
import TFactionID = PoliticalFactions.TFactionID;

const modifier: Weapons.TWeaponModifier = {
	baseCost: {
		bias: -3,
		min: 5,
	},
	damageModifier: {
		factor: 1.2,
	},
	baseRange: {
		bias: 4,
	},
};

export class GunTurretChassis extends GenericPointDefense {
	public static readonly id = 57002;
	public static readonly caption = 'Gun turret';
	public static readonly description = `A point defense unit, tailored to be most efficient with automatic weapons and equipped with slightly better sensors. Please note it has few known flaws in coolant subsystem`;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [TFactionID.stellarians];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: { factor: 1.5 },
		},
		[Weapons.TWeaponGroupType.Guns]: modifier,
	};
	public static readonly armorModifier = {
		[Damage.damageType.heat]: {
			factor: 1.25,
		},
	} as UnitDefense.TArmorModifier;
	public scanRange: number = this.scanRange + 2;
	public baseCost: number = this.baseCost + 7;
}

export default GunTurretChassis;

import GenericPointDefense from './GenericPointDefense';

export class PointDefenseChassis extends GenericPointDefense {
	public static readonly id = 57000;
	public static readonly caption = 'Point defense';
	public static readonly description = `Light and powerful robotic turret with twin weapon mounts, barely able to move, but offering
    high sustain and zone control`;
}

export default PointDefenseChassis;

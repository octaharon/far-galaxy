import UnitDefense from '../../../../tactical/damage/defense';
import DecoyClusterAura from '../../../../tactical/statuses/Auras/all/DecoyCluster.5011';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import PoliticalFactions from '../../../../world/factions';
import GenericPointDefense from './GenericPointDefense';
import TFactionID = PoliticalFactions.TFactionID;

export class SentinelTurrentChassis extends GenericPointDefense {
	public static readonly id = 57004;
	public static readonly caption = 'Sentinel Turret';
	public static readonly description = `Stellar One Hypercorp is a number one organization in Universe on guard of human rights and freedom. Whenever the democracy is threatened, loyal Sentinels are there to serve and protect it`;
	public static readonly exclusiveFactions: PoliticalFactions.TFactionID[] = [TFactionID.stellarians];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly auras: TAuraApplianceList = [
		{
			id: DecoyClusterAura.id,
		},
	];
	public static readonly armorModifier = {
		default: {
			factor: 0.85,
		},
	} as UnitDefense.TArmorModifier;
	public baseCost: number = this.baseCost + 12;
	public hitPoints: number = this.hitPoints + 40;
	public scanRange: number = this.scanRange + 2;
	public equipmentSlots: number = (this.equipmentSlots || 0) + 1;
	public dodge: UnitDefense.TDodge = {
		default: -0.2,
	};
}

export default SentinelTurrentChassis;

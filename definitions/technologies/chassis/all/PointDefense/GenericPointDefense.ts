import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import DefensiveRankTree from '../../../../tactical/units/levels/RankTrees/DefensiveRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericPointDefenseAction = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericPointDefenseActionPhases = 2;

export class GenericPointDefense extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.pointdefense;
	public static readonly levelTree = DefensiveRankTree;
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.hover as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.medium,
		Weapons.TWeaponSlotType.medium,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [
		UnitDefense.TShieldSlotType.small,
		UnitDefense.TShieldSlotType.small,
	] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		shieldAmount: {
			bias: 20,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 120;
	public speed = 0;
	public baseCost = 20;
	public scanRange = 10;
	public dodge = {
		default: -0.7,
	} as UnitDefense.TDodge;
	public equipmentSlots = 0;
	public actions = createActionOptions(GenericPointDefenseAction, GenericPointDefenseActionPhases);
}

export default GenericPointDefense;

import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import PoliticalFactions from '../../../../world/factions';
import Weapons from '../../../types/weapons';
import GenericPointDefense from './GenericPointDefense';
import TFactionID = PoliticalFactions.TFactionID;

const modifier = {
	baseCost: {
		bias: -3,
		min: 5,
	},
	baseRange: {
		bias: 1,
	},
	baseRate: {
		factor: 1.25,
	},
};

export class MissileTurretChassis extends GenericPointDefense {
	public static readonly id = 57001;
	public static readonly caption = 'Missile turret';
	public static readonly description = `A point defense unit, tailored to be most efficient with missile weapons, but less armored and sturdy than most analogues.`;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [TFactionID.stellarians];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: { baseCost: 1.5 },
		[Weapons.TWeaponGroupType.Missiles]: modifier,
		[Weapons.TWeaponGroupType.Rockets]: modifier,
		[Weapons.TWeaponGroupType.Cruise]: modifier,
	};
	public static readonly armorModifier = {
		[Damage.damageType.kinetic]: { factor: 1.1 },
		[Damage.damageType.thermodynamic]: { factor: 1.1 },
	} as UnitDefense.TArmorModifier;
	public hitPoints: number = this.hitPoints - 10;
	public baseCost: number = this.baseCost + 8;
}

export default MissileTurretChassis;

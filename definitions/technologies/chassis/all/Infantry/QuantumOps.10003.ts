import _ from 'underscore';
import SwapAbility from '../../../../tactical/abilities/all/Unique/Swap.37001';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase, grantAction, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import { UnitActions } from '../../../../tactical/units/actions/types';
import GenericInfantry from './GenericInfantry';

export class QuantumOpsChassis extends GenericInfantry {
	public static readonly id = 10003;
	public static readonly caption = 'Quantum Ops';
	public static readonly description = `An experimental military technology,
    only available for testing with specially trained combat operatives,
    granting unmatched tactical advantages`;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public static readonly abilities: number[] = [SwapAbility.id];
	public baseCost: number = this.baseCost + 8;
	public hitPoints: number = this.hitPoints + 10;
	public speed: number = this.speed + 1;
	public dodge: UnitDefense.TDodge = _.mapObject(this.dodge, (v: number) => v + 0.1);
	public actions: UnitActions.TUnitActionOptions[] = grantActionOnPhase(
		grantAction(addActionPhase(this.actions, 2), UnitActions.unitActionTypes.blink),
		UnitActions.unitActionTypes.warp,
	);
}

export default QuantumOpsChassis;

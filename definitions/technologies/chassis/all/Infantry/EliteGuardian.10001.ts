import UnitDefense from '../../../../tactical/damage/defense';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import InfantryRankTree from '../../../../tactical/units/levels/RankTrees/InfantryRankTree';
import PoliticalFactions from '../../../../world/factions';
import GenericInfantry from './GenericInfantry';

export class EliteGuardianChassis extends GenericInfantry {
	public static readonly id = 10001;
	public static readonly caption = 'Elite Guardian';
	public static readonly description = `Robust and well-equipped version of Infantry, lacking most of their weaknesses but also less agile at learning new tricks`;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [
		PoliticalFactions.TFactionID.draconians,
	];
	public static readonly levelTree = Object.assign({}, InfantryRankTree, {
		levelingFunction: getLevelingFunction(TLevelingFunction.Linear100),
	});
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public baseCost: number = this.baseCost + 6;
	public hitPoints: number = this.hitPoints + 15;
	public scanRange: number = this.scanRange - 1;
	public speed: number = this.speed + 1;
	public static readonly armorModifier = {};
}

export default EliteGuardianChassis;

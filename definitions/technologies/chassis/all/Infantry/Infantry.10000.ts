import GenericInfantry from './GenericInfantry';

export class InfantryChassis extends GenericInfantry {
	public static readonly id = 10000;
	public static readonly caption = 'Infantry';
	public static readonly description = `There are still people out there who would like to see the world in flames.
    Just give them a gun and some bleeding edge military tech, and you've got yourself an admirer, eager to carry
    your will with help of demolition and violence`;
}

export default InfantryChassis;

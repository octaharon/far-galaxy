import DecomposeAbility from '../../../../tactical/abilities/all/Unique/Decompose.37006';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericInfantry from './GenericInfantry';
import TFactionID = PoliticalFactions.TFactionID;

export class BruteChassis extends GenericInfantry {
	public static readonly id = 10005;
	public static readonly caption = 'Brute';
	public static readonly description = `While biologically are almost humans, by their masters these specimen are considered nor a person neither a citizen. Creatures like this one specifically bred to follow orders, kill and crush everything that stands in its way, and possesses an unique Decompose ability`;
	public static readonly exclusiveFactions: TFactionID[] = [TFactionID.draconians];
	public static readonly flags: UnitChassis.TTargetFlags = {
		...GenericInfantry.flags,
		[UnitChassis.targetType.massive]: true,
		[UnitChassis.targetType.unique]: true,
	};
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.large] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {
		default: {
			bias: -5,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly abilities: number[] = [DecomposeAbility.id];
	public baseCost: number = this.baseCost + 12;
	public hitPoints: number = this.hitPoints + 80;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
}

export default BruteChassis;

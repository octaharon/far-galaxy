import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import InfantryRankTree from '../../../../tactical/units/levels/RankTrees/InfantryRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericInfantryActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericInfantryActionPhases = 1;

export class GenericInfantry extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.infantry;
	public static readonly levelTree = InfantryRankTree;
	public static readonly flags = {
		[UnitChassis.targetType.organic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.small] as Weapons.TWeaponSlotType[];
	public static readonly armorModifier = {
		default: {
			factor: 1.1, // +10% incoming damage,
			min: 0, // can't go negative
		},
	} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public hitPoints = 30;
	public speed = 4;
	public baseCost = 5;
	public scanRange = 9;
	public dodge = {
		default: 0.4,
		[UnitAttack.deliveryType.cloud]: 0.75,
		[UnitAttack.deliveryType.surface]: 0.75,
	} as UnitDefense.TDodge;
	public actions: UnitActions.TUnitActionOptions[] = createActionOptions(
		GenericInfantryActions,
		GenericInfantryActionPhases,
	);
}

export default GenericInfantry;

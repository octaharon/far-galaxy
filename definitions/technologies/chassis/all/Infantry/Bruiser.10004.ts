import GasGrenadeAbility from '../../../../tactical/abilities/all/Unique/GasGrenade.12009';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericInfantry from './GenericInfantry';

export class BruiserChassis extends GenericInfantry {
	public static readonly id = 10004;
	public static readonly caption = 'Bruiser';
	public static readonly description = `Extremely durable infantry unit, involving some genetic
    engineering and life-long pharmaceutical treatment. Besides extreme physical qualities exhibits an unique Gas Grenade Ability`;
	public static readonly armorModifier = {
		default: {
			threshold: 1,
			bias: -2,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly abilities: number[] = [GasGrenadeAbility.id];
	public baseCost: number = this.baseCost + 10;
	public hitPoints: number = this.hitPoints + 30;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
}

export default BruiserChassis;

import SVSurveyAbility from '../../../../tactical/abilities/all/ScienceVessel/SVSurveyAbility.99910';
import GasGrenadeAbility from '../../../../tactical/abilities/all/Unique/GasGrenade.12009';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import PoliticalFactions from '../../../../world/factions';
import Weapons from '../../../types/weapons';
import GenericInfantry from './GenericInfantry';

export class XDriverChassis extends GenericInfantry {
	public static readonly id = 10002;
	public static readonly caption = 'X Driver';
	public static readonly description = `A mercenary equipped with 3D printer technology and extensive FPV training is capable of delivering extremely sensitive blows from outstanding range at quantities`;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [
		PoliticalFactions.TFactionID.draconians,
	];
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.hangar] as Weapons.TWeaponSlotType[];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Drone]: {
			baseCost: {
				factor: 1.2,
			},
			baseRate: {
				max: 0.1,
				min: 0.1,
			},
		},
		[Weapons.TWeaponGroupType.Bombs]: {
			baseRate: {
				max: 0.1,
				min: 0.1,
			},
		},
	};
	public dodge = {
		default: 0.4,
		[UnitAttack.deliveryType.bombardment]: 1.5,
		[UnitAttack.deliveryType.cloud]: 1.5,
		[UnitAttack.deliveryType.missile]: -0.5,
		[UnitAttack.deliveryType.drone]: -0.5,
	} as UnitDefense.TDodge;
	public baseCost: number = this.baseCost + 12;
	public hitPoints: number = this.hitPoints - 5;
	public scanRange: number = this.scanRange + 1;
	public static readonly armorModifier = {};
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(
		revokeAction(this.actions, UnitActions.unitActionTypes.attack_move),
	);
	public static readonly abilities: number[] = [SVSurveyAbility.id];
}

export default XDriverChassis;

import UnitDefense from '../../../../tactical/damage/defense';
import ChaplainSermonAura from '../../../../tactical/statuses/Auras/all/ChaplainSermon.5001';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import PoliticalFactions from '../../../../world/factions';
import GenericInfantry from './GenericInfantry';

export class ChaplainChassis extends GenericInfantry {
	public static readonly id = 10006;
	public static readonly caption = 'Chaplain';
	public static readonly description = `High-level combatant of Anders Congregation, operating both as a squad leader and a military technician on the battlefield, not only being dangerous enough on itself but also improving combat capabilities of nearby friendly units`;
	public static readonly exclusiveFactions: PoliticalFactions.TFactionID[] = [PoliticalFactions.TFactionID.anders];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public static readonly auras: TAuraApplianceList = [
		{
			id: ChaplainSermonAura.id,
		},
	];
	public baseCost: number = this.baseCost + 8;
	public hitPoints: number = this.hitPoints + 10;
	public scanRange: number = this.scanRange + 2;
	public speed: number = this.speed + 2;
}

export default ChaplainChassis;

import GenericHovertank from './GenericHovertank';

export class HovertankChassis extends GenericHovertank {
	public static readonly id = 55000;
	public static readonly caption = 'Hovertank';
	public static readonly description = `Heavy armored vehicle on a grav cushion, which can cross almost any terrain and solve
    all sorts of combat tasks`;
}

export default HovertankChassis;

import GroundingShotAbility from '../../../../tactical/abilities/all/Units/GroundingShot.29009';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AssaultRankTree from '../../../../tactical/units/levels/RankTrees/AssaultRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericHovertankActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericHovertankActionPhases = 3;

export class GenericHovertank extends GenericChassis {
	public static readonly abilities: number[] = [GroundingShotAbility.id];
	public static readonly levelTree = AssaultRankTree;
	public static readonly type = UnitChassis.chassisClass.hovertank;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.hover as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.large,
		Weapons.TWeaponSlotType.small,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {
		default: {
			factor: 0.9, // +10% damage reduction
		},
	} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 140;
	public speed = 8;
	public baseCost = 26;
	public scanRange = 7;
	public dodge = {} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericHovertankActions, GenericHovertankActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericHovertank;

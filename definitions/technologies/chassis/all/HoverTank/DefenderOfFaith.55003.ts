import UnitDefense from '../../../../tactical/damage/defense';
import DefenderOfFaithAura from '../../../../tactical/statuses/Auras/all/DefenderOfFaith.5003';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import GenericHovertank from './GenericHovertank';
import TFactionID = PoliticalFactions.TFactionID;

export class DefenderOfFaithChassis extends GenericHovertank {
	public static readonly id = 55003;
	public static readonly caption = 'Defender Of Faith';
	public static readonly description = `Extremely advanced hovertank with improved energy uplink and custom-fitted armor. Only one unit of this type can be deployed to surface at a time`;
	public static readonly exclusiveFactions: TFactionID[] = [TFactionID.anders];
	public static readonly flags: UnitChassis.TTargetFlags = {
		...GenericHovertank.flags,
		[UnitChassis.targetType.unique]: true,
	};

	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		...GenericHovertank.armorModifier,
		default: {
			...GenericHovertank.armorModifier.default,
			threshold: 15,
			factor: 0.85,
			min: 0,
		},
	};
	public static readonly shieldModifier: UnitDefense.TShieldsModifier = {
		...GenericHovertank.shieldModifier,
		shieldAmount: {
			bias: 40,
		},
		default: {
			threshold: 15,
			factor: 0.85,
			min: 0,
		},
	};
	public static readonly auras: TAuraApplianceList = [
		{
			id: DefenderOfFaithAura.id,
		},
	];
	public dodge = {
		default: 0.3,
	} as UnitDefense.TDodge;
	public equipmentSlots: number = 1 + (this.equipmentSlots ?? 0);
	public baseCost: number = this.baseCost + 7;
	public speed: number = this.speed + 1;
	public hitPoints: number = this.hitPoints + 45;
}

export default DefenderOfFaithChassis;

import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericHovertank from './GenericHovertank';

export class RamraiderChassis extends GenericHovertank {
	public static readonly id = 55002;
	public static readonly caption = 'Ramraider';
	public static readonly description = `Hovertank variation tuned for close combat and rapid assault on enemy lines`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Launchers]: {
			damageModifier: {
				factor: 1.25,
			},
			baseRate: {
				bias: 0.15,
			},
			baseCost: {
				bias: -3,
			},
		},
		[Weapons.TWeaponGroupType.Plasma]: {
			damageModifier: {
				factor: 1.25,
			},
			baseRate: {
				bias: 0.15,
			},
			baseCost: {
				bias: -3,
			},
		},
	};
	public speed: number = this.speed + 3;
	public baseCost: number = this.baseCost + 7;
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.resilient]: true,
	};
}

export default RamraiderChassis;

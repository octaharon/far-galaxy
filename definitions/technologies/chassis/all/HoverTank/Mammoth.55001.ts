import UnitDefense from '../../../../tactical/damage/defense';
import GenericHovertank from './GenericHovertank';

export class MammothChassis extends GenericHovertank {
	public static readonly id = 55001;
	public static readonly caption = 'Mammoth';
	public static readonly description = `Customized hovertank with a heavier hull and supreme protection against weaker weapons`;
	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		...GenericHovertank.armorModifier,
		default: {
			...GenericHovertank.armorModifier.default,
			threshold: 12,
		},
	};
	public baseCost: number = this.baseCost + 3;
	public hitPoints: number = this.hitPoints + 20;
}

export default MammothChassis;

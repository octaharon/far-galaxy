import { revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import Weapons from '../../../types/weapons';
import GenericHelipad from './GenericHelipad';

export class HeliconChassis extends GenericHelipad {
	public static readonly id = 89501;
	public static readonly caption = 'Helicon';
	public static readonly description = `Customized heli-launcher with a giant weapon mount instead of twin large ones
    and some advanced software`;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.extra] as Weapons.TWeaponSlotType[];
	public static readonly levelTree = {
		...GenericHelipad.levelTree,
		levelingFunction: getLevelingFunction(TLevelingFunction.Fast100),
	};
	public baseCost: number = this.baseCost + 4;
	public scanRange: number = this.scanRange + 3;
	public actions: UnitActions.TUnitActionOptions[] = revokeAction(this.actions, UnitActions.unitActionTypes.barrage);
}

export default HeliconChassis;

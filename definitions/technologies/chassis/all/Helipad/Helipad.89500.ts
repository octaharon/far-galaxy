import GenericHelipad from './GenericHelipad';

export class HelipadChassis extends GenericHelipad {
	public static readonly id = 89500;
	public static readonly caption = 'Helipad';
	public static readonly description = `Someone had this crazy idea of putting an artillery on top of a rotorblade platform. The resulting monstrous aircraft
    can deliver immense damage over a long range, but suffers from low mobility and evasion. `;
}

export default HelipadChassis;

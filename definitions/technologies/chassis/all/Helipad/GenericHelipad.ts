import CloudControlAbility from '../../../../tactical/abilities/all/Units/CloudControl.29023';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import ArtilleryRankTree from '../../../../tactical/units/levels/RankTrees/ArtilleryRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericHelipadActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.suppress,
] as UnitActions.TUnitActionOptions;

export const GenericHelipadActionPhases = 2;

export class GenericHelipad extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.helipad;
	public static readonly levelTree = ArtilleryRankTree;
	public static readonly movement = UnitChassis.movementType.air as UnitChassis.movementType;
	public static readonly flags = {
		[UnitChassis.targetType.massive]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.large,
		Weapons.TWeaponSlotType.large,
	] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [CloudControlAbility.id];
	public static readonly shieldModifier = {
		shieldAmount: {
			factor: 1.1,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			baseRange: {
				bias: 2,
			},
		},
	};
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 150;
	public baseCost = 30;
	public speed = 5;
	public scanRange = 7;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		default: -0.5,
		[UnitAttack.deliveryType.aerial]: -1,
	};
	public actions: UnitActions.TUnitActionOptions[] = grantActionOnPhase(
		createActionOptions(GenericHelipadActions, GenericHelipadActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericHelipad;

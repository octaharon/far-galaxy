import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import DefensiveRankTree from '../../../../tactical/units/levels/RankTrees/DefensiveRankTree';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';
import TFactionID = PoliticalFactions.TFactionID;

export const GenericMothershipActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericMothershipActionPhases = 3;

export class GenericMothership extends GenericChassis {
	public static readonly levelTree = DefensiveRankTree;
	public static readonly type = UnitChassis.chassisClass.mothership;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [TFactionID.venaticians];
	public static readonly flags = {
		[UnitChassis.targetType.massive]: true,
		[UnitChassis.targetType.unique]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.air as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.air,
		Weapons.TWeaponSlotType.air,
		Weapons.TWeaponSlotType.hangar,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		default: {
			factor: 0.9,
			min: 0, // but never heal
		},
		shieldAmount: {
			factor: 1.5,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [
		UnitDefense.TArmorSlotType.large,
		UnitDefense.TArmorSlotType.large,
	] as UnitDefense.TArmorSlotType[];
	public hitPoints = 220;
	public speed = 12;
	public baseCost = 40;
	public scanRange = 9;
	public dodge = {
		default: -0.3,
		[UnitAttack.deliveryType.aerial]: -0.5,
		[UnitAttack.deliveryType.surface]: 10,
		[UnitAttack.deliveryType.bombardment]: 10,
	} as UnitDefense.TDodge;
	public defenseFlags = {
		[UnitDefense.defenseFlags.replenishing]: true,
		[UnitDefense.defenseFlags.evasion]: true,
	} as UnitDefense.TDefenseFlags;
	public equipmentSlots = 1;
	public actions = grantActionOnPhase(
		createActionOptions(GenericMothershipActions, GenericMothershipActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericMothership;

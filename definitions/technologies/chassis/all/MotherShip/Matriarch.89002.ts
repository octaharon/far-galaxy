import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import DroneJammerAura from '../../../../tactical/statuses/Auras/all/DroneJammer.5004';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import Weapons from '../../../types/weapons';
import GenericMothership from './GenericMothership';

export class MatriarchChassis extends GenericMothership {
	public static readonly id = 89002;
	public static readonly caption = 'Matriarch';
	public static readonly description = `A customized Mothership modification, designed to outperform any foes that
    rely on drone weapons`;
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		...GenericMothership.weaponModifier,
		[Weapons.TWeaponGroupType.Drone]: {
			baseRange: {
				bias: 4,
			},
			baseRate: {
				bias: 0.3,
			},
		},
	};
	public static readonly auras: TAuraApplianceList = [
		{
			id: DroneJammerAura.id,
		},
	];
	public baseCost: number = this.baseCost + 6;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.drone]: 2,
	};
}

export default MatriarchChassis;

import TimeBombAbility from '../../../../tactical/abilities/all/Unique/TimeBomb.37010';
import Damage from '../../../../tactical/damage/damage';
import { addActionPhase, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import PoliticalFactions from '../../../../world/factions';
import GenericMothership from './GenericMothership';
import TFactionID = PoliticalFactions.TFactionID;

export class VindicatorChassis extends GenericMothership {
	public static readonly id = 89001;
	public static readonly caption = 'The Vindicator';
	public static readonly description = `A customized mothership design built with the most advanced warp technologies available for Stellarians`;
	public static readonly exclusiveFactions: PoliticalFactions.TFactionID[] = [TFactionID.stellarians];
	public static readonly abilities: number[] = [TimeBombAbility.id];
	public static readonly shieldModifier = {
		...GenericMothership.shieldModifier,
		[Damage.damageType.warp]: {
			factor: 0.8,
			threshold: 15,
		},
	};
	public static readonly salvageable = false; // can't be obtained
	public baseCost: number = this.baseCost + 5;
	public speed: number = this.speed + 4;
	public hitPoints: number = this.hitPoints + 25;
	public actions: UnitActions.TUnitActionOptions[] = grantActionOnPhase(
		addActionPhase(this.actions),
		UnitActions.unitActionTypes.warp,
	);
}

export default VindicatorChassis;

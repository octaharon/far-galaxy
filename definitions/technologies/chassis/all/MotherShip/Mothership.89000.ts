import GenericMothership from './GenericMothership';

export class MothershipChassis extends GenericMothership {
	public static readonly id = 89000;
	public static readonly caption = 'Mothership';
	public static readonly description = `An enormous flying vessel with plenty of space for heavy armaments and equipment, equipped with
    the best in class shield regenerator. The energy supply required to operate such a massive unit is so high that only one of these can
    be maintained at a time`;
}

export default MothershipChassis;

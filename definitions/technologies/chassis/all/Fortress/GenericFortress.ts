import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import DefensiveRankTree from '../../../../tactical/units/levels/RankTrees/DefensiveRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericFortressActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.delay,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericFortressActionPhases = 2;

export class GenericFortress extends GenericChassis {
	public static readonly levelTree = DefensiveRankTree;
	public static readonly type = UnitChassis.chassisClass.fortress;
	public static readonly flags = {
		[UnitChassis.targetType.massive]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.hover as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.extra,
		Weapons.TWeaponSlotType.small,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [
		UnitDefense.TShieldSlotType.large,
		UnitDefense.TShieldSlotType.medium,
	] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {
		default: {
			threshold: 10,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [
		UnitDefense.TArmorSlotType.large,
		UnitDefense.TArmorSlotType.medium,
	] as UnitDefense.TArmorSlotType[];
	public hitPoints = 300;
	public speed = 0;
	public baseCost = 38;
	public scanRange = 11;
	public defenseFlags = {
		[UnitDefense.defenseFlags.replenishing]: true,
	} as UnitDefense.TDefenseFlags;
	public dodge = {
		default: -0.7,
		[UnitAttack.deliveryType.surface]: 10,
		[UnitAttack.deliveryType.drone]: 1,
	} as UnitDefense.TDodge;
	public equipmentSlots = 1;
	public actions = createActionOptions(GenericFortressActions, GenericFortressActionPhases);
}

export default GenericFortress;

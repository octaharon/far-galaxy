import WearAndTearAbility from '../../../../tactical/abilities/all/Unique/WearAndTear.37004';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericFortress from './GenericFortress';

export class FortrexChassis extends GenericFortress {
	public static readonly id = 59001;
	public static readonly caption = 'Fort-REX';
	public static readonly description = `Gigantic point defense platform, purposed to orchestrate the invasion and customized to offer better defense versus aerial units and drones. Its equipment bay also have a preinstalled module which grants unique Wear and Tear ability`;
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.AntiAir]: {
			baseRate: {
				factor: 1.2,
			},
			baseAccuracy: {
				bias: 1,
			},
		},
	};
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.extra,
		Weapons.TWeaponSlotType.small,
		Weapons.TWeaponSlotType.small,
	] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [WearAndTearAbility.id];
	public baseCost: number = this.baseCost + 6;
	public equipmentSlots = 0;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.drone]: 0.8 - this.dodge.default,
		[UnitAttack.deliveryType.bombardment]: 0.8 - this.dodge.default,
	};
}

export default FortrexChassis;

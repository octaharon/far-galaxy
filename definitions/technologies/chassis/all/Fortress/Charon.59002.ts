import UnitDefense from '../../../../tactical/damage/defense';
import LeadershipAura from '../../../../tactical/statuses/Auras/all/Leadership.5006';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericFortress from './GenericFortress';
import TFactionID = PoliticalFactions.TFactionID;

export class CharonChassis extends GenericFortress {
	public static readonly id = 59002;
	public static readonly caption = 'CHARON';
	public static readonly description = `Enormous defensive weapon platform, offering unpreceded firepower and endurance with its secondary weapon replaced by a hangar bay`;
	public static readonly exclusiveFactions: TFactionID[] = [TFactionID.reticulans];
	public static readonly flags: UnitChassis.TTargetFlags = {
		...GenericFortress.flags,
		[UnitChassis.targetType.unique]: true,
	};
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.hangar,
		Weapons.TWeaponSlotType.extra,
	] as Weapons.TWeaponSlotType[];
	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		default: {
			threshold: 15,
			bias: -5,
		},
	};
	public static readonly auras: TAuraApplianceList = [
		{
			id: LeadershipAura.id,
		},
	];
	public scanRange: number = this.scanRange + 2;
	public hitPoints: number = this.hitPoints + 40;
	public baseCost: number = this.baseCost + 5;
}

export default CharonChassis;

import GenericFortress from './GenericFortress';

export class FortressChassis extends GenericFortress {
	public static readonly id = 59000;
	public static readonly caption = 'Fortress';
	public static readonly description = `A megalomaniac dream implemented - huge armored combat platform with a titan weapon mount. Advanced defensive technologies, an equipment bay and a gravity cushion make Fortress an impassable point of defense, and also an immovable one.`;
}

export default FortressChassis;

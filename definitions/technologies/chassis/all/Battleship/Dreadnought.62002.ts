import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericBattleship from './GenericBattleship';

export class DreadnoughtChassis extends GenericBattleship {
	public static readonly id = 62002;
	public static readonly caption = 'Dreadnought';
	public static readonly description = `The most advanced battleship, equipped with a better powerplant, blue-chip tracking software and a best-in-class radar, but weakened armor and hull`;
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			baseAccuracy: {
				bias: 0.25,
			},
		},
	};
	public static readonly armorModifier = {
		[Damage.damageType.kinetic]: {
			factor: 1.2,
		},
		[Damage.damageType.heat]: {
			factor: 1.2,
		},
	} as UnitDefense.TArmorModifier;
	public baseCost: number = this.baseCost + 8;
	public scanRange: number = this.scanRange + 4;
	public hitPoints: number = this.hitPoints - 10;
	public speed: number = this.speed + 2;
}

export default DreadnoughtChassis;

import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import NavalRankTree from '../../../../tactical/units/levels/RankTrees/NavalRankTree';
import GenericBattleship from './GenericBattleship';

export class IroncladChassis extends GenericBattleship {
	public static readonly id = 62001;
	public static readonly caption = 'Ironclad';
	public static readonly levelTree = Object.assign({}, NavalRankTree, {
		levelingFunction: getLevelingFunction(TLevelingFunction.Fast50),
	});
	public static readonly description = `Slow, sturdy and heavily armed, this ship is almost indestructible, but pretty easy to track and target`;
	public static readonly armorSlots = [
		UnitDefense.TArmorSlotType.large,
		UnitDefense.TArmorSlotType.large,
	] as UnitDefense.TArmorSlotType[];
	public baseCost: number = this.baseCost + 5;
	public speed: number = this.speed - 1;
	public hitPoints: number = this.hitPoints + 20;
	public dodge = {
		[UnitAttack.deliveryType.bombardment]: -1,
		default: -0.5,
	} as UnitDefense.TDodge;
}

export default IroncladChassis;

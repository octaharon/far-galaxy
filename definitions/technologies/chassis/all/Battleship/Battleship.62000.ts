import GenericBattleship from './GenericBattleship';

export class BattleshipChassis extends GenericBattleship {
	public static readonly id = 62000;
	public static readonly caption = 'Battleship';
	public static readonly description = `Heavily armed and armored ship, a naval counterpart of artillery. With immense firepower comes low mobility and vulnerability,
    so usually these require some escort of faster and smaller ships to protect from fast approaching planes and corvettes`;
}

export default BattleshipChassis;

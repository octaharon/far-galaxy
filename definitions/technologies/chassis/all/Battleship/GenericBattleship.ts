import SiegeModeAbility from '../../../../tactical/abilities/all/Units/SiegeMode.29102';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import NavalRankTree from '../../../../tactical/units/levels/RankTrees/NavalRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericBattleshipActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.watch,
	UnitActions.unitActionTypes.barrage,
] as UnitActions.TUnitActionOptions;

export const GenericBattleshipActionPhases = 2;

export class GenericBattleship extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.battleship;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly levelTree = NavalRankTree;
	public static readonly movement = UnitChassis.movementType.naval as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.naval,
		Weapons.TWeaponSlotType.naval,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {
		default: {
			factor: 0.9,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public static readonly abilities: number[] = [SiegeModeAbility.id];
	public hitPoints = 140;
	public speed = 6;
	public baseCost = 27;
	public scanRange = 6;
	public dodge = {
		[UnitAttack.deliveryType.bombardment]: -1,
	} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericBattleshipActions, GenericBattleshipActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericBattleship;

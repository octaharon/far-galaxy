import StrafeShotAbility from '../../../../tactical/abilities/all/Units/StrafeShot.29004';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericExoskeleton, { GenericExoskeletonActionPhases, GenericExoskeletonActions } from './GenericExoskeleton';

export class TracerChassis extends GenericExoskeleton {
	public static readonly abilities: number[] = [StrafeShotAbility.id];
	public static readonly id = 15001;
	public static readonly caption = 'Tracer';
	public static readonly description = `Infantry exosuit with a plasma propulsion thrusters attached, providing better agility and a capability to jump over small terrain entities, but less tolerant to damage`;
	public actions: UnitActions.TUnitActionOptions[] = grantActionOnPhase(
		grantActionOnPhase(
			createActionOptions(GenericExoskeletonActions, GenericExoskeletonActionPhases + 1),
			UnitActions.unitActionTypes.delay,
		),
		UnitActions.unitActionTypes.blink,
	);
	public speed: number = this.speed + 2;
	public hitPoints: number = this.hitPoints - 10;
	public baseCost: number = this.baseCost + 10;
	public dodge = {
		default: 0.5,
	} as UnitDefense.TDodge;
}

export default TracerChassis;

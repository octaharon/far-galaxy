import DetectorShotAbility from '../../../../tactical/abilities/all/Units/DetectorShot.29006';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import InfantryRankTree from '../../../../tactical/units/levels/RankTrees/InfantryRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericExoskeletonActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.watch,
	UnitActions.unitActionTypes.retaliate,
] as UnitActions.TUnitActionOptions;

export const GenericExoskeletonActionPhases = 2;

export class GenericExoskeleton extends GenericChassis {
	public static readonly abilities: number[] = [DetectorShotAbility.id];
	public static readonly type = UnitChassis.chassisClass.exosuit;
	public static readonly levelTree = InfantryRankTree;
	public static readonly flags = {
		[UnitChassis.targetType.organic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.small,
		Weapons.TWeaponSlotType.small,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		default: {
			factor: 0.9, // +10% shield damage reduction,
			min: 0,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 65;
	public speed = 7;
	public baseCost = 20;
	public scanRange = 5;
	public dodge = {
		default: 0.3,
	} as UnitDefense.TDodge;
	public equipmentSlots = 1;
	public actions = grantActionOnPhase(
		createActionOptions(GenericExoskeletonActions, GenericExoskeletonActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericExoskeleton;

import GenericExoskeleton from './GenericExoskeleton';

export class ExoskeletonChassis extends GenericExoskeleton {
	public static readonly id = 15000;
	public static readonly caption = 'Exosuit';
	public static readonly description = `An exterior armor suit for infantry turns it into much more sustainable and versatile combat unit`;
}

export default ExoskeletonChassis;

import BuckleShotAbility from '../../../../tactical/abilities/all/Units/BuckleShot.29001';
import Weapons from '../../../types/weapons';
import GenericExoskeleton from './GenericExoskeleton';

export class BarkerChassis extends GenericExoskeleton {
	public static readonly abilities: number[] = [BuckleShotAbility.id];
	public static readonly id = 15003;
	public static readonly caption = 'Barker';
	public static readonly description = `Customized infantry exoskeleton with a bigger weapon mount and a stronger plating, being bulkier but slower`;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.medium] as Weapons.TWeaponSlotType[];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			damageModifier: {
				bias: 5,
			},
			baseRate: {
				bias: 0.1,
			},
		},
	};
	public hitPoints: number = this.hitPoints + 15;
	public speed: number = this.speed - 1;
	public baseCost: number = this.baseCost + 5;
}

export default BarkerChassis;

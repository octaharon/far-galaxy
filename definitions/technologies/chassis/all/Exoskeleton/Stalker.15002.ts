import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GammaFresnelEffect from '../../../../tactical/statuses/Effects/all/Unique/GammaFresnel.37014';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericExoskeleton from './GenericExoskeleton';

export class StalkerChassis extends GenericExoskeleton {
	public static readonly id = 15002;
	public static readonly caption = 'Stalker';
	public static readonly description = `Infantry exosuit with a better radar and an additional protection against
    background radiation, designed to operate on planets with little or no atmosphere`;
	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		[Damage.damageType.radiation]: {
			bias: -5,
		},
		[Damage.damageType.magnetic]: {
			bias: -5,
			min: 0,
		},
	};
	public static readonly shieldModifier: UnitDefense.TShieldsModifier = {
		[Damage.damageType.radiation]: {
			bias: -15,
			min: 0,
			threshold: 10,
		},
		[Damage.damageType.magnetic]: {
			bias: -15,
			min: 0,
			threshold: 10,
		},
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: GammaFresnelEffect.id,
		},
	];
	public scanRange: number = this.scanRange + 2;
	public hitPoints: number = this.hitPoints + 15;
	public baseCost: number = this.baseCost + 11;
	public equipmentSlots = 0;
}

export default StalkerChassis;

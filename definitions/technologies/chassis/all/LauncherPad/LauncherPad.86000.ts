import GenericLaunchPad from './GenericLaunchPad';

export class LauncherPadChassis extends GenericLaunchPad {
	public static readonly id = 86000;
	public static readonly caption = 'Launcher Pad';
	public static readonly description = `Heavy artillery on a gravity cushion. As modern as it gets, but very slow`;
}

export default LauncherPadChassis;

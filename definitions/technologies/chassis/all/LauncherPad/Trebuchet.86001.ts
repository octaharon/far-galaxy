import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericLaunchPad from './GenericLaunchPad';

export class TrebuchetChassis extends GenericLaunchPad {
	public static readonly id = 86001;
	public static readonly caption = 'Trebuchet';
	public static readonly description = `Cannon artillery on a gravity cushion, sinister but fragile`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Cannons]: {
			damageModifier: {
				factor: 1.15,
			},
			baseAccuracy: {
				bias: 0.5,
			},
			baseRange: {
				bias: 2,
			},
		},
		default: {
			baseCost: {
				factor: 1.5,
			},
		},
	};
	public static readonly armorModifier = {
		default: { factor: 1.1 },
	} as UnitDefense.TArmorModifier;
	public baseCost: number = this.baseCost + 7;
	public hitPoints: number = this.hitPoints - 15;
	public scanRange: number = this.scanRange + 3;
}

export default TrebuchetChassis;

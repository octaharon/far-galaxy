import BuckleShotAbility from '../../../../tactical/abilities/all/Units/BuckleShot.29001';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import ArtilleryRankTree from '../../../../tactical/units/levels/RankTrees/ArtilleryRankTree';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericLaunchPadActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.move,
] as UnitActions.TUnitActionOptions;

export const GenericLaunchPadActionPhases = 1;

export class GenericLaunchPad extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.launcherpad;
	public static readonly levelTree = ArtilleryRankTree;
	public static readonly abilities: number[] = [BuckleShotAbility.id];
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [
		PoliticalFactions.TFactionID.reticulans,
	];
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.hover as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.extra] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		overchargeDecay: {
			min: 10,
		},
		shieldAmount: {
			factor: 1.5,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 100;
	public speed = 3;
	public baseCost = 20;
	public scanRange = 4;
	public dodge = {
		default: -0.3,
		[UnitAttack.deliveryType.surface]: 10,
	} as UnitDefense.TDodge;
	public actions = createActionOptions(GenericLaunchPadActions, GenericLaunchPadActionPhases);
}

export default GenericLaunchPad;

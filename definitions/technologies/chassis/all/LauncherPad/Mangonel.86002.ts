import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericLaunchPad from './GenericLaunchPad';

export class MangonelChassis extends GenericLaunchPad {
	public static readonly id = 86002;
	public static readonly caption = 'Mangonel';
	public static readonly description = `Heavy artillery on a gravity cushion, enhanced with a bleeding edge electronics and drone suppression system, but with a downgraded shield generator`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			baseAccuracy: {
				bias: 0.75,
			},
			baseRate: {
				bias: 0.12,
				max: 1,
				maxGuard: 1,
			},
		},
	};
	public static readonly shieldModifier = {
		shieldAmount: {
			factor: 1.25,
		},
		default: {
			factor: 1.1,
		},
	} as UnitDefense.TShieldsModifier;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.drone]: 2,
	};
	public speed: number = this.speed + 3;
	public baseCost: number = this.baseCost + 8;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.defend);
}

export default MangonelChassis;

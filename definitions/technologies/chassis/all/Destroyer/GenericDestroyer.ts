import OnslaughtAbility from '../../../../tactical/abilities/all/Units/Onslaught.29101';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import NavalRankTree from '../../../../tactical/units/levels/RankTrees/NavalRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericDestroyerActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericDestroyerActionPhases = 3;

export class GenericDestroyer extends GenericChassis {
	public static readonly levelTree = NavalRankTree;
	public static readonly type = UnitChassis.chassisClass.destroyer;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.naval as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.naval,
		Weapons.TWeaponSlotType.small,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public static readonly abilities: number[] = [OnslaughtAbility.id];
	public hitPoints = 110;
	public speed = 9;
	public baseCost = 30;
	public scanRange = 6;
	public dodge = {
		[UnitAttack.deliveryType.bombardment]: -0.5,
		[UnitAttack.deliveryType.drone]: 1,
		[UnitAttack.deliveryType.missile]: 0.5,
	} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericDestroyerActions, GenericDestroyerActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericDestroyer;

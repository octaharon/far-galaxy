import BuckleShotAbility from '../../../../tactical/abilities/all/Units/BuckleShot.29001';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericDestroyer from './GenericDestroyer';

export class SeamothChassis extends GenericDestroyer {
	public static readonly id = 64001;
	public static readonly caption = 'Seamoth';
	public static readonly description = `Improved destroyer warship equipped with Reactive defense and heavier hull`;
	public static readonly abilities: number[] = [...GenericDestroyer.abilities, BuckleShotAbility.id];
	public baseCost: number = this.baseCost + 5;
	public speed: number = this.speed + 1;
	public defenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	} as UnitDefense.TDefenseFlags;
}

export default SeamothChassis;

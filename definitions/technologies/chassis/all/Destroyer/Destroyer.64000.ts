import DisarmAbility from '../../../../tactical/abilities/all/Units/Disarm.29010';
import GenericDestroyer from './GenericDestroyer';

export class DestroyerChassis extends GenericDestroyer {
	public static readonly id = 64000;
	public static readonly caption = 'Destroyer';
	public static readonly description = `Fast, maneuverable, long-endurance warship intended to escort larger vessels and defend them against short-range attackers`;
	public static readonly abilities: number[] = [...GenericDestroyer.abilities, DisarmAbility.id];
}

export default DestroyerChassis;

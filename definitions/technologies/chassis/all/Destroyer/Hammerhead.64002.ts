import AssaultShotAbility from '../../../../tactical/abilities/all/Units/AssaultShot.29002';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import AssaultRankTree from '../../../../tactical/units/levels/RankTrees/AssaultRankTree';
import GenericDestroyer from './GenericDestroyer';

export class HammerheadChassis extends GenericDestroyer {
	public static readonly id = 64002;
	public static readonly caption = 'Hammerhead';
	public static readonly description = `Modified destroyer ship with an experimental navigation module, very similar to those of the assault units. Offers
    the best damage and sonar in class`;
	public static readonly levelTree = {
		...AssaultRankTree,
		levelingFunction: getLevelingFunction(TLevelingFunction.Fast150),
	};
	public static readonly abilities: number[] = [...GenericDestroyer.abilities, AssaultShotAbility.id];
	public baseCost: number = this.baseCost + 6;
	public hitPoints: number = this.hitPoints + 15;
	public scanRange: number = this.scanRange + 2;
}

export default HammerheadChassis;

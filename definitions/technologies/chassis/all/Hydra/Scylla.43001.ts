import WildGrowthAbility from '../../../../tactical/abilities/all/Bionic/WildGrowth.12006';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import SpicySporesAura from '../../../../tactical/statuses/Auras/all/SpicySpores.5013';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericHydra from './GenericHydra';

export class ScyllaChassis extends GenericHydra {
	public static readonly id = 43001;
	public static readonly caption = 'Scylla';
	public static readonly description = `A giant weapon platform and an equipment bay mounted on top of a naval bacteria colony, designed to survive heavy radiation
    and other hostile environments, but having less intricate recoil control and targeting subsystem`;
	public static readonly armorModifier?: UnitDefense.TArmorModifier = {
		...GenericHydra.armorModifier,
		[Damage.damageType.radiation]: {
			threshold: 20,
			factor: 0.7,
		},
	};

	public static readonly auras: TAuraApplianceList = [
		{
			id: SpicySporesAura.id,
			props: {
				range: 3,
			},
		},
	];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			damageModifier: {
				factor: 0.9,
			},
			baseAccuracy: {
				bias: -0.3,
			},
		},
	};
	public static readonly abilities: number[] = [WildGrowthAbility.id];
	public hitPoints: number = this.hitPoints + 30;
	public baseCost: number = this.baseCost + 8;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.resilient]: true,
	};
}

export default ScyllaChassis;

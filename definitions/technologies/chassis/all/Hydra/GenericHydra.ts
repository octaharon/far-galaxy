import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import SpicySporesAura from '../../../../tactical/statuses/Auras/all/SpicySpores.5013';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import BiotechRankTree from '../../../../tactical/units/levels/RankTrees/BiotechRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericHydraActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.delay,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;
export const GenericHydraActionPhases = 2;

export class GenericHydra extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.hydra;
	public static readonly levelTree = {
		...BiotechRankTree,
		levelingFunction: getLevelingFunction(TLevelingFunction.Fast150),
	};
	public static readonly flags = {
		[UnitChassis.targetType.organic]: true,
		[UnitChassis.targetType.massive]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.naval as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.extra] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		overchargeDecay: {
			min: 15,
		},
		default: {
			bias: -10,
			min: 0,
		},
		shieldAmount: {
			factor: 3,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly auras: TAuraApplianceList = [
		{
			id: SpicySporesAura.id,
		},
	];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 135;
	public speed = 7;
	public baseCost = 39;
	public scanRange = 7;
	public dodge = {
		default: -0.5,
		[UnitAttack.deliveryType.bombardment]: -1,
		[UnitAttack.deliveryType.cloud]: -1,
	} as UnitDefense.TDodge;
	public defenseFlags = {
		[UnitDefense.defenseFlags.unstoppable]: true,
	} as UnitDefense.TDefenseFlags;
	public equipmentSlots = 1;
	public actions = createActionOptions(GenericHydraActions, GenericHydraActionPhases);
}

export default GenericHydra;

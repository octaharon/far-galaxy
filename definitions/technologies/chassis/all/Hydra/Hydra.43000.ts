import NeuralParasiteAbility from '../../../../tactical/abilities/all/Bionic/NeuralParasite.12005';
import GenericHydra from './GenericHydra';

export class HydraChassis extends GenericHydra {
	public static readonly id = 43000;
	public static readonly caption = 'Hydra';
	public static readonly description = `A genetically engineered bacteria colony, capable of surviving in most liquids occuring in space. The colony
    is grown upon a giant weapon platform with a shield generator, forming enormous bubble like structures and narrow tentacles, which allow it to stay afloat
    and to traverse lakes and oceans. Bacteria reproduce and die so fast, that most lasting effects do not persist on them. At the same time,
    the overall creature is so huge and slow, that it can barely evade incoming attacks.`;
	public static readonly abilities: number[] = [NeuralParasiteAbility.id];
}

export default HydraChassis;

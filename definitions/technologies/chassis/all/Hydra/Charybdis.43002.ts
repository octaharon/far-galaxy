import EnzymeInhibitorsAbility from '../../../../tactical/abilities/all/Bionic/EnzymeInhibitors.12004';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericHydra from './GenericHydra';

export class CharybdisChassis extends GenericHydra {
	public static readonly id = 43002;
	public static readonly caption = 'Charybdis';
	public static readonly description = `A giant weapon platform and an equipment bay floating atop of a genetically constructed unicellular colony,
    having a specific mutation, allowing it to absorb energy from shots and emission and store it to charge the weapon systems. Energy dissipation reduces shield capabilities though`;
	public static readonly abilities: number[] = [EnzymeInhibitorsAbility.id];
	public static readonly shieldModifier = {
		...GenericHydra.shieldModifier,
		baseCost: {
			factor: 1.2,
		},
		default: {
			factor: 1.1,
		},
	} as UnitDefense.TShieldsModifier;
	public baseCost: number = this.baseCost + 6;
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.sma]: true,
	};
}

export default CharybdisChassis;

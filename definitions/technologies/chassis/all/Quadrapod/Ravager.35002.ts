import StrafeShotAbility from '../../../../tactical/abilities/all/Units/StrafeShot.29004';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase, grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericQuadrapod from './GenericQuadrapod';

export class RavagerChassis extends GenericQuadrapod {
	public static readonly id = 35002;
	public static readonly caption = 'Ravager';
	public static readonly description = `A modified version of Quadrapod, which is lighter, quicker, and mounts 2 medium weapons instead. This nifty engineering has some drawbacks, including lower survivabilty for higher cost, of course`;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.medium,
		Weapons.TWeaponSlotType.medium,
	] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [StrafeShotAbility.id];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: {
				factor: 1.25,
			},
			baseRate: {
				factor: 1.25,
			},
		},
	};
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		addActionPhase(this.actions),
		UnitActions.unitActionTypes.barrage,
	);
	public speed: number = this.speed + 3;
	public hitPoints: number = this.hitPoints - 15;
	public baseCost: number = this.baseCost + 7;
	public dodge: UnitDefense.TDodge = {
		[UnitAttack.deliveryType.bombardment]: -0.5,
		default: 0.4,
	};
}

export default RavagerChassis;

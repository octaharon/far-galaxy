import TacticalNodeAbility from '../../../../tactical/abilities/all/Unique/TacticalNode.37008';
import DisablingShotAbility from '../../../../tactical/abilities/all/Units/DisablingShot.29000';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase, grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericQuadrapod from './GenericQuadrapod';

export class TechnodromeChassis extends GenericQuadrapod {
	public static readonly id = 35001;
	public static readonly caption = 'Technodrome';
	public static readonly description = `An officer of quadrapod army, this huge war machine combines locomotion with tracks, being more agile and fit for electronic warfare, though it's slower and particularly vulnerable to certain damage sources`;
	public static readonly abilities: number[] = [DisablingShotAbility.id, TacticalNodeAbility.id];
	public static readonly armorModifier = {
		[Damage.damageType.thermodynamic]: {
			factor: 1.25,
		},
		[Damage.damageType.antimatter]: {
			factor: 1.25,
		},
	} as UnitDefense.TArmorModifier;
	public baseCost: number = this.baseCost + 10;
	public speed: number = this.speed - 1;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		addActionPhase(this.actions),
		UnitActions.unitActionTypes.delay,
	);
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		default: 0.1,
		[UnitAttack.deliveryType.bombardment]: -0.25,
		[UnitAttack.deliveryType.surface]: -2,
	};
}

export default TechnodromeChassis;

import HowitzerShotAbility from '../../../../tactical/abilities/all/Units/SkyShot.29005';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AssaultRankTree from '../../../../tactical/units/levels/RankTrees/AssaultRankTree';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericQuadrapodActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.watch,
	UnitActions.unitActionTypes.attack_move,
] as UnitActions.TUnitActionOptions;

export const GenericQuadrapodActionPhases = 2;

export class GenericQuadrapod extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.quadrapod;
	public static readonly levelTree = AssaultRankTree;
	public static readonly abilities: number[] = [HowitzerShotAbility.id];
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [PoliticalFactions.TFactionID.anders];
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
		[UnitChassis.targetType.massive]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.large] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			damageModifier: {
				factor: 1.1,
			},
		},
	};
	public static readonly shieldModifier = {
		shieldAmount: {
			bias: 50,
		},
		default: {
			bias: -5,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 160;
	public speed = 6;
	public baseCost = 28;
	public scanRange = 7;
	public dodge = {
		[UnitAttack.deliveryType.bombardment]: -1,
		[UnitAttack.deliveryType.surface]: -0.5,
		[UnitAttack.deliveryType.drone]: 1,
		default: 0.25,
	} as UnitDefense.TDodge;
	public defenseFlags = {
		[UnitDefense.defenseFlags.replenishing]: true,
	} as UnitDefense.TDefenseFlags;
	public equipmentSlots = 1;
	public actions = createActionOptions(GenericQuadrapodActions, GenericQuadrapodActionPhases);
}

export default GenericQuadrapod;

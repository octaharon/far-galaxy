import GenericQuadrapod from './GenericQuadrapod';

export class QuadrapodChassis extends GenericQuadrapod {
	public static readonly id = 35000;
	public static readonly caption = 'Quadrapod';
	public static readonly description = `An universal robotic unit, a bit bulky, but very resilient and suitable for various combat tasks`;
}

export default QuadrapodChassis;

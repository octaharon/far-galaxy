import SymbioticTissueAbility from '../../../../tactical/abilities/all/Bionic/SymbioticTissue.12008';
import GenericSwarm from './GenericSwarm';

export class SwarmChassis extends GenericSwarm {
	public static readonly id = 45000;
	public static readonly caption = 'Swarm';
	public static readonly description = `A flock of genetically constructed flying creatures controlled through neural implants, carrying a frame with
    air weapon mount and some shield generator`;
	public static readonly abilities: number[] = [SymbioticTissueAbility.id];
}

export default SwarmChassis;

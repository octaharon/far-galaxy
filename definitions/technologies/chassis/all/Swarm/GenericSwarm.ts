import UnitDefense from '../../../../tactical/damage/defense';
import CloudOfBladesAura from '../../../../tactical/statuses/Auras/all/CloudOfBlades.5007';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import BiotechRankTree from '../../../../tactical/units/levels/RankTrees/BiotechRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericSwarmActions: UnitActions.TUnitActionOptions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.watch,
];

export const GenericSwarmActionPhases = 2;

export class GenericSwarm extends GenericChassis {
	public static readonly levelTree = BiotechRankTree;
	public static readonly type = UnitChassis.chassisClass.swarm;
	public static readonly flags = {
		[UnitChassis.targetType.organic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.air as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.air] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		overchargeDecay: {
			min: 10,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public static readonly auras: TAuraApplianceList = [
		{
			id: CloudOfBladesAura.id,
		},
	];
	public hitPoints = 135;
	public speed = 7;
	public baseCost = 29;
	public scanRange = 7;
	public dodge: UnitDefense.TDodge = {
		default: 0.5,
	};
	public defenseFlags = {
		[UnitDefense.defenseFlags.evasion]: true,
	} as UnitDefense.TDefenseFlags;
	public actions = createActionOptions(GenericSwarmActions, GenericSwarmActionPhases);
}

export default GenericSwarm;

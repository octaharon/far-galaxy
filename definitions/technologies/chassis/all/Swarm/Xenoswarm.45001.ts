import CryptobiosisAbility from '../../../../tactical/abilities/all/Bionic/Cryptobiosis.12007';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericSwarm from './GenericSwarm';

export class XenoswarmChassis extends GenericSwarm {
	public static readonly id = 45001;
	public static readonly caption = 'Xenoswarm';
	public static readonly description = `A strain of swarm chassis with innate resistance to radiation and improved adaptation to harsh conditions, but slightly more bulky and easy to hit`;
	public static readonly armorModifier?: UnitDefense.TArmorModifier = {
		...GenericSwarm.armorModifier,
		[Damage.damageType.radiation]: {
			threshold: 20,
		},
	};
	public static readonly abilities: number[] = [CryptobiosisAbility.id];
	public hitPoints: number = this.hitPoints + 15;
	public baseCost: number = this.baseCost + 4;
	public speed: number = this.speed - 1;
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.resilient]: true,
	};
	public dodge: UnitDefense.TDodge = {
		default: 0.25,
	};
	public actions: UnitActions.TUnitActionOptions[] = grantActionOnPhase(
		addActionPhase(this.actions),
		UnitActions.unitActionTypes.delay,
	);
}

export default XenoswarmChassis;

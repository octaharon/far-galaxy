import StrafeShotAbility from '../../../../tactical/abilities/all/Units/StrafeShot.29004';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericCombatDrone, { GenericCombatDroneActionPhases, GenericCombatDroneActions } from './GenericCombatDrone';

export class ChaserChassis extends GenericCombatDrone {
	public static readonly id = 20001;
	public static readonly caption = 'Chaser';
	public static readonly description = `Customized combat drone tailored to fight other robots and drone carriers`;
	public static readonly armorModifier?: UnitDefense.TArmorModifier = {
		default: {
			bias: -3,
			threshold: 5,
		},
	};
	public static readonly abilities: number[] = [StrafeShotAbility.id];
	public speed: number = this.speed + 1;
	public baseCost: number = this.baseCost + 5;
	public dodge = {
		default: 0.6,
		[UnitAttack.deliveryType.drone]: 2,
	} as UnitDefense.TDodge;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		createActionOptions(GenericCombatDroneActions, GenericCombatDroneActionPhases),
		UnitActions.unitActionTypes.attack_move,
		UnitActions.unitActionTypes.barrage,
	);
}

export default ChaserChassis;

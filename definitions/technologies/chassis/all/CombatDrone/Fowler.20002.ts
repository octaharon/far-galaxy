import BuckleShotAbility from '../../../../tactical/abilities/all/Units/BuckleShot.29001';
import Weapons from '../../../types/weapons';
import GenericCombatDrone from './GenericCombatDrone';

export class FowlerChassis extends GenericCombatDrone {
	public static readonly id = 20002;
	public static readonly caption = 'Fowler';
	public static readonly description = `Modified version of Combat Drone, equipped with one weapon and a stronger hull`;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.medium] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [BuckleShotAbility.id];
	public hitPoints: number = this.hitPoints + 50;
	public baseCost: number = this.baseCost + 3;
	public scanRange: number = this.scanRange + 2;
}

export default FowlerChassis;

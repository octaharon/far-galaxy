import BlastingShotAbility from '../../../../tactical/abilities/all/Units/BlastingShot.29007';
import GenericCombatDrone from './GenericCombatDrone';

export class CombatDroneChassis extends GenericCombatDrone {
	public static readonly id = 20000;
	public static readonly abilities: number[] = [BlastingShotAbility.id];
	public static readonly caption = 'Combat Drone';
	public static readonly description = `Autonomous combat robot, equipped with a gravity cushion, two small weapon racks
    and a shield augmentor. It can't withstand heavy fire, but it's quick, dodgy and capable of multitasking.`;
}

export default CombatDroneChassis;

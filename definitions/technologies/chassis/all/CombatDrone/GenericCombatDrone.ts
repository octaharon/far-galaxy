import BlastingShotAbility from '../../../../tactical/abilities/all/Units/BlastingShot.29007';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AssaultRankTree from '../../../../tactical/units/levels/RankTrees/AssaultRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericCombatDroneActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.warp,
	UnitActions.unitActionTypes.watch,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.intercept,
] as UnitActions.TUnitActionOptions;

export const GenericCombatDroneActionPhases = 2;

export class GenericCombatDrone extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.combatdrone;
	public static readonly levelTree = AssaultRankTree;
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.hover as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.small,
		Weapons.TWeaponSlotType.small,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		overchargeDecay: {
			min: 5,
		},
		shieldAmount: {
			factor: 2.0,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 80;
	public speed = 12;
	public baseCost = 23;
	public scanRange = 10;
	public dodge = {
		default: 0.6,
		[UnitAttack.deliveryType.drone]: 1,
	} as UnitDefense.TDodge;
	public defenseFlags = {
		[UnitDefense.defenseFlags.hull]: true,
	} as UnitDefense.TDefenseFlags;
	public equipmentSlots = 0;
	public actions = grantActionOnPhase(
		createActionOptions(GenericCombatDroneActions, GenericCombatDroneActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericCombatDrone;

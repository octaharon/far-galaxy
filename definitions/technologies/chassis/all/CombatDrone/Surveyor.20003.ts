import DetectorShotAbility from '../../../../tactical/abilities/all/Units/DetectorShot.29006';
import AdaptiveRecyclingEffect from '../../../../tactical/statuses/Effects/all/Unique/AdaptiveRecycling.37016';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import Weapons from '../../../types/weapons';
import GenericCombatDrone from './GenericCombatDrone';

export class SurveyorChassis extends GenericCombatDrone {
	public static readonly id = 20003;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.small] as Weapons.TWeaponSlotType[];
	public static readonly caption = 'Surveyor';
	public static readonly description = `A modified version of a geological survey drone turned into a combat unit with one weapon mount, with another one being sacrificed for an Equipment Slot. Possesses a unique Adaptive Recycling perk.`;
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: AdaptiveRecyclingEffect.id,
		},
	];
	public static readonly abilities: number[] = [DetectorShotAbility.id];
	public baseCost: number = this.baseCost + 2;
	public equipmentSlots: number = this.equipmentSlots + 1;
}

export default SurveyorChassis;

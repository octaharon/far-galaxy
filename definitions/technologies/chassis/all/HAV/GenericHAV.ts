import ExecutionShotAbility from '../../../../tactical/abilities/all/Units/ExecutionShot.29003';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AssaultRankTree from '../../../../tactical/units/levels/RankTrees/AssaultRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericHAVActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericHAVActionPhases = 2;

export class GenericHeavyAssaultVehicle extends GenericChassis {
	public static readonly abilities: number[] = [ExecutionShotAbility.id];
	public static readonly type = UnitChassis.chassisClass.hav;
	public static readonly levelTree = AssaultRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.large] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		default: {
			factor: 0.9, // +10% damage reduction
		},
		shieldAmount: {
			bias: 30,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public hitPoints = 120;
	public speed = 7;
	public baseCost = 17;
	public scanRange = 7;
	public dodge = {
		default: 0,
	} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericHAVActions, GenericHAVActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericHeavyAssaultVehicle;

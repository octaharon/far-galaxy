import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericHeavyAssaultVehicle, { GenericHAVActionPhases, GenericHAVActions } from './GenericHAV';

export class GroundbreakerChassis extends GenericHeavyAssaultVehicle {
	public static readonly id = 33001;
	public static readonly caption = 'Groundbreaker';
	public static readonly description = `A bulldozer type assault vehicle that is virtually and literally unstoppable, though slow`;
	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		default: {
			factor: 0.85,
			threshold: 15,
		},
	};
	public static readonly shieldModifier: UnitDefense.TShieldsModifier = {
		...GenericHeavyAssaultVehicle.shieldModifier,
		shieldAmount: {
			factor: 1.5,
		},
	};
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public actions = grantAction(
		createActionOptions(GenericHAVActions, GenericHAVActionPhases - 1),
		UnitActions.unitActionTypes.delay,
	);
	public baseCost: number = this.baseCost + 8;
	public speed: number = this.speed - 2;
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.unstoppable]: true,
	};
	public dodge = {
		default: -0.5,
	} as UnitDefense.TDodge;
}

export default GroundbreakerChassis;

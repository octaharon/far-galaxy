import GenericHeavyAssaultVehicle from './GenericHAV';

export class HAVChassis extends GenericHeavyAssaultVehicle {
	public static readonly id = 33000;
	public static readonly caption = 'HAV';
	public static readonly description = `Heavy assault vehicles are balanced combat units, compatible with the most devastating weapons,
    having some substantial defense, and yet agile enough`;
}

export default HAVChassis;

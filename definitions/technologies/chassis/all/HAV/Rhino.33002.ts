import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericHeavyAssaultVehicle from './GenericHAV';

export class RhinoChassis extends GenericHeavyAssaultVehicle {
	public static readonly id = 33002;
	public static readonly caption = 'Rhino';
	public static readonly description = `A customized heavy assault vehicle, stocked to efficiently duel the biggest foes, but its shield generator could use some quality`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Warp]: {
			baseRange: {
				bias: 5,
			},
			baseRate: {
				factor: 1.2,
			},
		},
		default: {
			baseCost: {
				factor: 1.5,
			},
		},
	};
	public static readonly shieldModifier = {
		shieldAmount: {
			bias: 10,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public baseCost: number = this.baseCost + 6;
	public hitPoints: number = this.hitPoints + 25;
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.resilient]: true,
	};
}

export default RhinoChassis;

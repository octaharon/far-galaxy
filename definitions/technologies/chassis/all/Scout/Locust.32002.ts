import Weapons from '../../../types/weapons';
import GenericScout from './GenericScout';

export class LocustChassis extends GenericScout {
	public static readonly id = 32002;
	public static readonly caption = 'Locust';
	public static readonly description = `A scout vehicle fine tuned for close combat and best compatible with Launchers weapons.
    Given extremely fragile armor though it's likely gonna be a suicide mission`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: {
				bias: 5,
			},
		},
		[Weapons.TWeaponGroupType.Launchers]: {
			damageModifier: {
				factor: 1.2,
			},
			baseRate: {
				bias: 0.15,
			},
			baseCost: {
				bias: -3,
			},
			baseAccuracy: {
				bias: 0.4,
			},
		},
	};
	public speed: number = this.speed + 2;
	public hitPoints: number = this.hitPoints - 10;
	public baseCost: number = this.baseCost + 3;
}

export default LocustChassis;

import UnitDefense from '../../../../tactical/damage/defense';
import GenericScout from './GenericScout';

export class ScoutChassis extends GenericScout {
	public static readonly id = 32000;
	public static readonly caption = 'Scout';
	public static readonly description = `Fast and light vehicle, used mostly for surveillance and distraction rather than
    for heavy fire`;
	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		...GenericScout.armorModifier,
		default: {
			...(GenericScout.armorModifier.default || {}),
			bias: 1,
		},
	};
}

export default ScoutChassis;

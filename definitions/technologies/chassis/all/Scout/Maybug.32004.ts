import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import PoliticalFactions from '../../../../world/factions';
import GenericScout from './GenericScout';
import TFactionID = PoliticalFactions.TFactionID;

export class MaybugChassis extends GenericScout {
	public static readonly id = 32004;
	public static readonly caption = 'Maybug';
	public static readonly description = `A scout vehicle with heavily improved protection and drone suppression system,
    surviving long enough to provide targeting information to heavier units`;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public static readonly exclusiveFactions: PoliticalFactions.TFactionID[] = [TFactionID.venaticians];
	public baseCost: number = this.baseCost + 3;
	public defenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	} as UnitDefense.TDefenseFlags;
	public hitPoints: number = this.hitPoints + 15;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.attack);
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.drone]: 2,
	};
}

export default MaybugChassis;

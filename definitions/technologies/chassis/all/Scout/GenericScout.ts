import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import VehicleRankTree from '../../../../tactical/units/levels/RankTrees/VehicleRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericScoutActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericScoutActionPhases = 2;

export class GenericScout extends GenericChassis {
	public static readonly levelTree = VehicleRankTree;
	public static readonly type = UnitChassis.chassisClass.scout;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.small] as Weapons.TWeaponSlotType[];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public hitPoints = 50;
	public speed = 10;
	public baseCost = 12;
	public scanRange = 11;
	public dodge = {
		default: 0.2,
		[UnitAttack.deliveryType.bombardment]: 0.5,
		[UnitAttack.deliveryType.drone]: 0.5,
	} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericScoutActions, GenericScoutActionPhases),
		UnitActions.unitActionTypes.attack,
		1,
	);
}

export default GenericScout;

import UnitDefense from '../../../../tactical/damage/defense';
import GenericScout from './GenericScout';

export class VerminChassis extends GenericScout {
	public static readonly id = 32001;
	public static readonly caption = 'Vermin';
	public static readonly description = `Customized scout vehicle with a military armor frame and better sonar`;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public baseCost: number = this.baseCost + 4;
	public scanRange: number = this.scanRange + 2;
}

export default VerminChassis;

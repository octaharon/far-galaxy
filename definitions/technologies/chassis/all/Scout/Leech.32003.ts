import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase, grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import UnitChassis from '../../../types/chassis';
import GenericFighter from '../Fighter/GenericFighter';
import GenericScout from './GenericScout';

export class LeechChassis extends GenericScout {
	public static readonly id = 32003;
	public static readonly caption = 'Leech';
	public static readonly description = `Robotized scout vehicle with extremely advanced tactical capabilities,
    but vulnerable to electromagnetic damage`;
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly levelTree = {
		...GenericFighter.levelTree,
		levelingFunction: getLevelingFunction(TLevelingFunction.Prodigy),
	};
	public baseCost: number = this.baseCost + 5;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		default: 0.5,
		[UnitAttack.deliveryType.drone]: 1,
	};
	public speed: number = this.speed + 1;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		addActionPhase(this.actions),
		UnitActions.unitActionTypes.attack,
		UnitActions.unitActionTypes.delay,
	);
}

export default LeechChassis;

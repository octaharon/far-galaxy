import GenericArtillery from './GenericArtillery';

export class ArtilleryChassis extends GenericArtillery {
	public static readonly id = 30000;
	public static readonly caption = 'SPG';
	public static readonly description = `A long range weapon platform on a tracked chassis`;
}

export default ArtilleryChassis;

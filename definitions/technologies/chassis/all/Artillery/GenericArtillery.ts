import HowitzerShotAbility from '../../../../tactical/abilities/all/Units/SkyShot.29005';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import ArtilleryRankTree from '../../../../tactical/units/levels/RankTrees/ArtilleryRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericArtilleryActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.delay,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.suppress,
] as UnitActions.TUnitActionOptions;

export const GenericArtilleryActionPhases = 1;

export class GenericArtillery extends GenericChassis {
	public static readonly abilities: number[] = [HowitzerShotAbility.id];
	public static readonly type = UnitChassis.chassisClass.artillery;
	public static readonly levelTree = ArtilleryRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.large] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public hitPoints = 75;
	public speed = 6;
	public baseCost = 13;
	public scanRange = 9;
	public dodge = {} as UnitDefense.TDodge;
	public actions = createActionOptions(GenericArtilleryActions, GenericArtilleryActionPhases);
}

export default GenericArtillery;

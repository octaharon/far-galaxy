import AssaultShotAbility from '../../../../tactical/abilities/all/Units/AssaultShot.29002';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericArtillery, { GenericArtilleryActionPhases, GenericArtilleryActions } from './GenericArtillery';

export class AssaultBatteryChassis extends GenericArtillery {
	public static readonly id = 30003;
	public static readonly caption = 'Assault Battery';
	public static readonly description = `Less mobile version of self-propelled artillery with two weapon platforms, capable of executing shoot-and-scoot maneuver and barrage shots.`;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public static readonly abilities: number[] = [AssaultShotAbility.id];
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.large,
		Weapons.TWeaponSlotType.large,
	] as Weapons.TWeaponSlotType[];
	public baseCost: number = this.baseCost + 6;
	public speed: number = this.speed - 2;
	public actions = grantAction(
		createActionOptions(GenericArtilleryActions, GenericArtilleryActionPhases + 1),
		UnitActions.unitActionTypes.barrage,
	);
	public dodge = {
		default: -0.3,
	} as UnitDefense.TDodge;
}

export default AssaultBatteryChassis;

import SiegeModeAbility from '../../../../tactical/abilities/all/Units/SiegeMode.29102';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import PoliticalFactions from '../../../../world/factions';
import Weapons from '../../../types/weapons';
import GenericArtillery from './GenericArtillery';
import TFactionID = PoliticalFactions.TFactionID;

export class ColossusArtilleryChassis extends GenericArtillery {
	public static readonly id = 30002;
	public static readonly caption = 'Colossus';
	public static readonly description = `A tracked artillery chassis modified to be used with extremely large armament systems.`;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.extra] as Weapons.TWeaponSlotType[];
	public static readonly exclusiveFactions: TFactionID[] = [TFactionID.reticulans];
	public static readonly abilities: number[] = [SiegeModeAbility.id];
	public baseCost: number = this.baseCost + 5;
	public scanRange: number = this.scanRange + 3;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.defend);
}

export default ColossusArtilleryChassis;

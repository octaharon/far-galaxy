import Weapons from '../../../types/weapons';
import GenericArtillery from './GenericArtillery';

export class LicornArtilleryChassis extends GenericArtillery {
	public static readonly id = 30001;
	public static readonly caption = 'Licorne';
	public static readonly description = `An exclusive design of self-propelled artillery, offering best in class precision and damage`;

	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			baseAccuracy: {
				bias: 0.35,
			},
			damageModifier: {
				factor: 1.15,
			},
		},
	};
	public baseCost: number = this.baseCost + 10;
	public hitPoints: number = this.hitPoints + 15;
}

export default LicornArtilleryChassis;

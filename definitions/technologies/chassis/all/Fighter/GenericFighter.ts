import FlaresAbility from '../../../../tactical/abilities/all/Unique/Flares.37003';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import { GenericAircraft } from '../../genericAircraft';

export class GenericFighter extends GenericAircraft {
	public static readonly type = UnitChassis.chassisClass.fighter;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.air] as Weapons.TWeaponSlotType[];
	public static readonly shieldModifier = {
		shieldAmount: {
			factor: 2,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly abilities: number[] = [FlaresAbility.id];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Missiles]: {
			baseRange: {
				bias: 3,
			},
			baseRate: {
				factor: 1.1,
			},
		},
		[Weapons.TWeaponGroupType.Cruise]: {
			baseRange: {
				bias: 3,
			},
			baseRate: {
				factor: 1.1,
			},
		},
		[Weapons.TWeaponGroupType.Bombs]: {
			damageModifier: {
				factor: 0.5,
			},
		},
	};
	public static readonly armorSlots = [] as UnitDefense.TArmorSlotType[];
	public hitPoints: number = this.hitPoints - 10;
	public speed: number = this.speed + 3;
	public baseCost: number = this.baseCost + 3;
	public defenseFlags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.evasion]: true,
	};
	public dodge: UnitDefense.TDodge = {
		[UnitAttack.deliveryType.ballistic]: 1,
		[UnitAttack.deliveryType.missile]: 1,
		[UnitAttack.deliveryType.aerial]: 0.3,
	};
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		this.actions,
		UnitActions.unitActionTypes.intercept,
		UnitActions.unitActionTypes.protect,
	);
}

export default GenericFighter;

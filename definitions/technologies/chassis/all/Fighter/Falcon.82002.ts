import UnitDefense from '../../../../tactical/damage/defense';
import GenericFighter from './GenericFighter';

export class FalconChassis extends GenericFighter {
	public static readonly id = 82002;
	public static readonly caption = 'Falcon';
	public static readonly description = `Slightly armored fighter plane with more power distributed towards shield regenerator and sonar, rather than engines`;
	public static readonly shieldModifier = {
		...GenericFighter.shieldModifier,
		overchargeDecay: {
			min: 5,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		default: {
			factor: 0.9,
		},
	};
	public defenseFlags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.boosters]: true,
	};
	public speed: number = this.speed - 4;
	public scanRange: number = this.scanRange + 2;
	public baseCost: number = this.baseCost + 8;
	public hitPoints: number = this.hitPoints + 10;
}

export default FalconChassis;

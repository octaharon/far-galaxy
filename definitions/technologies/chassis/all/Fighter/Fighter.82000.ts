import GenericFighter from './GenericFighter';

export class FighterChassis extends GenericFighter {
	public static readonly id = 82000;
	public static readonly caption = 'Makeshift Fighter';
	public static readonly description = `A light plane, designed mostly to fight another planes and to protect slower wingmates. It's the fastest and the most hard to hit plane class available,
    but due to it's smaller size it can't accommodate a full-sized bomber bay or a proper armor`;
}

export default FighterChassis;

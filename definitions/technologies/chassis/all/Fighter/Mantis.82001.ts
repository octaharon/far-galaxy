import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import GenericFighter from './GenericFighter';
import TFactionID = PoliticalFactions.TFactionID;

export class MantisChassis extends GenericFighter {
	public static readonly id = 82001;
	public static readonly caption = 'Mantis';
	public static readonly description = `Aquarians Fighter class aircraft, fully AI controlled. With extreme speed and maneuverability, this aircraft can wreck havoc within enemy lines while staying out of reach of most air defense systems and other planes`;
	public static readonly exclusiveFactions: TFactionID[] = [TFactionID.aquarians];
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly levelTree = {
		...GenericFighter.levelTree,
		levelingFunction: getLevelingFunction(TLevelingFunction.Prodigy),
	};
	public speed: number = this.speed + 5;
	public baseCost: number = this.baseCost + 3;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.aerial]: 0.4,
		default: 0.4,
	};
	public defenseFlags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.tachyon]: true,
	};
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
}

export default MantisChassis;

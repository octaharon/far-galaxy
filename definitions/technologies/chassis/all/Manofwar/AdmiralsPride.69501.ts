import { stackUnitEffectsModifiers } from '../../../../prototypes/helpers';
import { IUnitEffectModifier } from '../../../../prototypes/types';
import SafetyDomeAbility from '../../../../tactical/abilities/all/Unique/SafetyDome.37009';
import LeadershipAura from '../../../../tactical/statuses/Auras/all/Leadership.5006';
import StealthBreakerAura from '../../../../tactical/statuses/Auras/all/StealthBreaker.5009';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import PoliticalFactions from '../../../../world/factions';
import Weapons from '../../../types/weapons';
import GenericManofwarChassis from './GenericManofwar';
import TFactionID = PoliticalFactions.TFactionID;

export class AdmiralsPrideChassis extends GenericManofwarChassis {
	public static readonly id = 69501;
	public static readonly caption = `Admiral's Pride`;
	public static readonly description = `One and only, a custom flagship stuffed with the most advanced electronics and armament systems.`;
	public static readonly exclusiveFactions: TFactionID[] = [TFactionID.pirates];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			baseAccuracy: {
				factor: 1.5,
			},
		},
	};
	public static readonly auras: TAuraApplianceList = [
		{
			id: LeadershipAura.id,
			props: { range: LeadershipAura.baseRange + 1 },
		},
		{
			id: StealthBreakerAura.id,
			props: { range: StealthBreakerAura.baseRange - 1 },
		},
	];
	public static readonly unitEffectModifier: IUnitEffectModifier = stackUnitEffectsModifiers(
		GenericManofwarChassis.unitEffectModifier,
		{
			abilityPowerModifier: [
				{
					bias: 0.5,
				},
			],
		},
	);
	public static readonly abilities: number[] = [SafetyDomeAbility.id];
	public baseCost: number = this.baseCost + 12;
	public scanRange: number = this.scanRange + 4;
	public hitPoints: number = this.hitPoints + 30;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
}

export default AdmiralsPrideChassis;

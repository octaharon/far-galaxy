import FieldRepairsAbility from '../../../../tactical/abilities/all/Units/FieldRepairs.29103';
import GenericManofwarChassis from './GenericManofwar';

export class FlagShipChassis extends GenericManofwarChassis {
	public static readonly id = 69500;
	public static readonly caption = 'Man-of-War';
	public static readonly description = `The biggest and the most beautiful gunboat in the fleet with unmatched firepower and defensive capabilities. Due to immense energy
    throughput it's possible to maintain only one of these at a time`;
	public static readonly abilities: number[] = [FieldRepairsAbility.id];
}

export default FlagShipChassis;

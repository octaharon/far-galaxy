import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import LeadershipAura from '../../../../tactical/statuses/Auras/all/Leadership.5006';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import DefensiveRankTree from '../../../../tactical/units/levels/RankTrees/DefensiveRankTree';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';
import TFactionID = PoliticalFactions.TFactionID;

export const GenericFlagShipAction = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.warp,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericFlagShipActionPhases = 2;

export class GenericManofwarChassis extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.manofwar;
	public static readonly levelTree = DefensiveRankTree;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [TFactionID.venaticians];
	public static readonly flags = {
		[UnitChassis.targetType.massive]: true,
		[UnitChassis.targetType.unique]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.naval as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.naval,
		Weapons.TWeaponSlotType.hangar,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [
		UnitDefense.TShieldSlotType.large,
		UnitDefense.TShieldSlotType.large,
	] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {
		default: {
			factor: 0.7,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public static readonly auras: TAuraApplianceList = [
		{
			id: LeadershipAura.id,
		},
	];
	public hitPoints = 240;
	public speed = 5;
	public baseCost = 36;
	public scanRange = 11;
	public dodge = {
		[UnitAttack.deliveryType.bombardment]: -1,
		[UnitAttack.deliveryType.drone]: 1,
		[UnitAttack.deliveryType.cloud]: 1,
	} as UnitDefense.TDodge;
	public defenseFlags = {
		[UnitDefense.defenseFlags.boosters]: true,
		[UnitDefense.defenseFlags.warp]: true,
	} as UnitDefense.TDefenseFlags;
	public equipmentSlots = 1;
	public actions = grantActionOnPhase(
		createActionOptions(GenericFlagShipAction, GenericFlagShipActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericManofwarChassis;

import BlastingShotAbility from '../../../../tactical/abilities/all/Units/BlastingShot.29007';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericCorvette from './GenericCorvette';

export class NeptuneChassis extends GenericCorvette {
	public static readonly id = 60002;
	public static readonly caption = 'Neptune';
	public static readonly description = `Neptune-class corvettes are the fastest naval units and are equipped by a gravity field generator, which
    offers both protection from matter displacement weapons and more energy output on Warp weapons, making it the best suited ship to fight bigger targets. However, it's pretty easily overwhelmed by smaller foes`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Warp]: {
			baseRange: {
				bias: 1,
			},
			baseCost: {
				bias: -4,
				min: 10,
			},
			damageModifier: {
				bias: 15,
			},
		},
		default: {
			baseCost: {
				factor: 1.1,
			},
		},
	};
	public static readonly shieldModifier: UnitDefense.TShieldsModifier = {
		overchargeDecay: {
			min: 5,
		},
		[Damage.damageType.warp]: {
			factor: 0.8,
		},
	};
	public static readonly abilities: number[] = [BlastingShotAbility.id];
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.warp]: true,
	};
	public dodge = {
		[UnitAttack.deliveryType.drone]: -1,
		[UnitAttack.deliveryType.cloud]: -1,
		default: 0.15,
	} as UnitDefense.TDodge;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		revokeAction(this.actions, UnitActions.unitActionTypes.protect),
		UnitActions.unitActionTypes.intercept,
	);
	public speed: number = this.speed + 3;
	public scanRange: number = this.scanRange - 2;
	public baseCost: number = this.baseCost + 9;
}

export default NeptuneChassis;

import { IUnitEffectModifier } from '../../../../prototypes/types';
import FocusedEMIAbility from '../../../../tactical/abilities/all/Units/FocusedEMI.29022';
import UnitDefense from '../../../../tactical/damage/defense';
import StealthBreakerAura from '../../../../tactical/statuses/Auras/all/StealthBreaker.5009';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { addActionPhase, grantAction, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericCorvette from './GenericCorvette';

export class NereidChassis extends GenericCorvette {
	public static readonly id = 60003;
	public static readonly caption = 'Nereid';
	public static readonly description = `Nereid-class corvettes are designed to be the lightest and most versatile among siblings, excelling at electronic warfare rather than gunfights, being immune to lasting damage effects, but not so great with Weapons`;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly abilities: number[] = [FocusedEMIAbility.id];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			baseAccuracy: {
				bias: -0.2,
			},
			damageModifier: {
				factor: 0.95,
			},
		},
	};
	public static readonly unitEffectModifier: IUnitEffectModifier = {
		abilityDurationModifier: [{ bias: 1, minGuard: 1 }],
		abilityRangeModifier: [{ bias: 1, minGuard: 1 }],
	};
	public static readonly auras: TAuraApplianceList = [
		{
			id: StealthBreakerAura.id,
			props: {
				range: StealthBreakerAura.baseRange + 1,
			},
		},
	];
	public defenseFlags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.unstoppable]: true,
	};
	public hitPoints: number = this.hitPoints - 15;
	public speed: number = this.speed + 2;
	public scanRange: number = this.scanRange + 1;
	public baseCost: number = this.baseCost + 7;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		revokeAction(addActionPhase(this.actions), UnitActions.unitActionTypes.suppress),
		UnitActions.unitActionTypes.defend,
	);
}

export default NereidChassis;

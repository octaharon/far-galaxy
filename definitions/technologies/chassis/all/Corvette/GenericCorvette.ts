import UnitDefense from '../../../../tactical/damage/defense';
import StealthBreakerAura from '../../../../tactical/statuses/Auras/all/StealthBreaker.5009';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import NavalRankTree from '../../../../tactical/units/levels/RankTrees/NavalRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericCorvetteActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.watch,
	UnitActions.unitActionTypes.retaliate,
] as UnitActions.TUnitActionOptions;

export const GenericCorvetteActionPhases = 2;

export class GenericCorvette extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.corvette;
	public static readonly levelTree = NavalRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.naval as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.medium] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public static readonly auras: TAuraApplianceList = [
		{
			id: StealthBreakerAura.id,
		},
	];
	public hitPoints = 100;
	public speed = 9;
	public baseCost = 21;
	public scanRange = 11;
	public dodge = {} as UnitDefense.TDodge;
	public equipmentSlots = 1;
	public actions = grantActionOnPhase(
		createActionOptions(GenericCorvetteActions, GenericCorvetteActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericCorvette;

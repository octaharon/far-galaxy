import CloudControlAbility from '../../../../tactical/abilities/all/Units/CloudControl.29023';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericCorvette from './GenericCorvette';

export class TritonChassis extends GenericCorvette {
	public static readonly id = 60001;
	public static readonly caption = 'Triton';
	public static readonly description = `Triton-class corvettes are tailored to fight drone clouds and approaching aerial targets, and are generally the most well-protected ships in class, though not so efficient against bigger rivals`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.AntiAir]: {
			baseRange: {
				bias: 2,
			},
			baseRate: {
				factor: 1.2,
			},
			baseCost: {
				bias: -2,
				min: 1,
				minGuard: 1,
			},
		},
		default: {
			baseCost: {
				factor: 1.1,
			},
		},
	};
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public static readonly abilities: number[] = [CloudControlAbility.id];
	public hitPoints: number = this.hitPoints + 15;
	public dodge = {
		[UnitAttack.deliveryType.drone]: 2,
		[UnitAttack.deliveryType.bombardment]: 2,
	} as UnitDefense.TDodge;
	public baseCost: number = this.baseCost + 8;
	public actions: UnitActions.TUnitActionOptions[] = revokeAction(
		grantAction(this.actions, UnitActions.unitActionTypes.intercept),
		UnitActions.unitActionTypes.protect,
	);
}

export default TritonChassis;

import DisarmAbility from '../../../../tactical/abilities/all/Units/Disarm.29010';
import GenericCorvette from './GenericCorvette';

export class CorvetteChassis extends GenericCorvette {
	public static readonly id = 60000;
	public static readonly caption = 'Corvette';
	public static readonly description = `Fast, lightly armored ship targeted for surveillance and support operations.
    It has the best sonar among all ships and an equipment bay, which is often used for submarine hunting`;
	public static readonly abilities: number[] = [DisarmAbility.id];
}

export default CorvetteChassis;

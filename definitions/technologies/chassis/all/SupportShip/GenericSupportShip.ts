import DefragmentationAbility from '../../../../tactical/abilities/all/ElectronicWarfare/Defragmentation.50004';
import SVSurveyAbility from '../../../../tactical/abilities/all/ScienceVessel/SVSurveyAbility.99910';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import SyncreticEvolutionAura from '../../../../tactical/statuses/Auras/all/SyncreticEvolution.5010';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import NavalRankTree from '../../../../tactical/units/levels/RankTrees/NavalRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericSupportShipActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.warp,
] as UnitActions.TUnitActionOptions;

export const GenericSupportShipActionPhases = 2;

export class GenericSupportShip extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.sup_ship;
	public static readonly levelTree = NavalRankTree;
	public static readonly movement = UnitChassis.movementType.naval as UnitChassis.movementType;
	public static readonly weaponSlots = [] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public static readonly abilities: number[] = [DefragmentationAbility.id, SVSurveyAbility.id];
	public static readonly auras: TAuraApplianceList = [
		{
			id: SyncreticEvolutionAura.id,
		},
	];
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.boosters]: true,
	};
	public hitPoints = 100;
	public speed = 9;
	public baseCost = 25;
	public scanRange = 13;
	public dodge = {
		[UnitAttack.deliveryType.bombardment]: -1,
		[UnitAttack.deliveryType.surface]: 10,
		[UnitAttack.deliveryType.aerial]: 10,
		[UnitAttack.deliveryType.drone]: 2,
	} as UnitDefense.TDodge;
	public equipmentSlots = 2;
	public actions: UnitActions.TUnitActionOptions[] = revokeAction(
		createActionOptions(GenericSupportShipActions, GenericSupportShipActionPhases),
		UnitActions.unitActionTypes.barrage,
		UnitActions.unitActionTypes.attack_move,
	);
}

export default GenericSupportShip;

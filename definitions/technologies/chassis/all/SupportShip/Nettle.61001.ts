import { IUnitEffectModifier } from '../../../../prototypes/types';
import StasisMatrixAbility from '../../../../tactical/abilities/all/Stasis/StasisMatrixAbility.20001';
import SmokeScreenAbility from '../../../../tactical/abilities/all/Units/SmokeScreen.60003';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericSupportShip from './GenericSupportShip';

export class NettleChassis extends GenericSupportShip {
	public static readonly id = 61001;
	public static readonly caption = 'Nettle';
	public static readonly description = `Combat support vessel with an extensible arsenal of tactical Abilites, that can turn the tides of battle with its highly advanced cybernetic and support abilities`;
	public static readonly unitEffectModifier: IUnitEffectModifier = {
		abilityPowerModifier: [
			{
				bias: 0.75,
			},
		],
		abilityRangeModifier: [{ bias: 2 }],
		abilityCooldownModifier: [
			{
				factor: 0.7,
				minGuard: 1,
				min: 1,
			},
		],
	};
	public static readonly abilities: number[] = [StasisMatrixAbility.id, SmokeScreenAbility.id];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.tachyon]: true,
	};
	public hitPoints: number = this.hitPoints - 10;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions, 2);
	public speed: number = this.speed + 3;
	public scanRange: number = this.scanRange + 3;
	public baseCost: number = this.baseCost + 12;
}

export default NettleChassis;

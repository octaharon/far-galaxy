import GenericSupportShip from './GenericSupportShip';

export class SupportShipChassis extends GenericSupportShip {
	public static readonly id = 61000;
	public static readonly caption = 'Support Ship';
	public static readonly description = `Surveillance naval unit with an equipment bay and the best in class sonar, having an unique Ability to take control over Organic units`;
}

export default SupportShipChassis;

import MycotoxinAbility from '../../../../tactical/abilities/all/Bionic/Mycotoxin.12002';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import ImposingPresenceAura from '../../../../tactical/statuses/Auras/all/ImposingPresence.5012';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import Weapons from '../../../types/weapons';
import GenericBioTech from './GenericBioTech';

export class ChimeraChassis extends GenericBioTech {
	public static readonly id = 40002;
	public static readonly caption = 'Chimaera';
	public static readonly description = `One of the fiercest artificial lifeforms ever created: a combination of other projects which failed due to lack of sustainable control. Stronger and slower than most competitors, it can mount a large weapon a tactical smoke thrower. Nevertheless, a distinct heat signature makes it an easy target for high precision weapons`;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.large] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [MycotoxinAbility.id];
	public static readonly auras: TAuraApplianceList = [
		{
			id: ImposingPresenceAura.id,
			props: {
				range: 5,
			},
		},
	];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {};
	public baseCost: number = this.baseCost + 10;
	public speed: number = this.speed - 1;
	public hitPoints: number = this.hitPoints + 10;
	public dodge = {
		...this.dodge,
		default: 0.15,
		[UnitAttack.deliveryType.missile]: -0.5,
		[UnitAttack.deliveryType.beam]: -0.5,
		[UnitAttack.deliveryType.drone]: -0.5,
	} as UnitDefense.TDodge;
}

export default ChimeraChassis;

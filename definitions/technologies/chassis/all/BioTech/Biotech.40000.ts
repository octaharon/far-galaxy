import CausticIsomersAbility from '../../../../tactical/abilities/all/Bionic/CausticIsomers.50005';
import GenericBioTech from './GenericBioTech';

export class BiotechChassis extends GenericBioTech {
	public static readonly id = 40000;
	public static readonly caption = 'Biotech';
	public static readonly description = `Some call it an abomination, others - a pinnacle of genetic engineering. Whatever it is, it can take orders,
    it can adapt to environment and it is smart enough to carry a weapon or a few, but everything it does is still pretty mediocre.`;
	public static readonly abilities: number[] = [CausticIsomersAbility.id];
}

export default BiotechChassis;

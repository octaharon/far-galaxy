import DisarmAbility from '../../../../tactical/abilities/all/Units/Disarm.29010';
import HoundAroundAura from '../../../../tactical/statuses/Auras/all/HoundAround.5008';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions, grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import PoliticalFactions from '../../../../world/factions';
import Weapons from '../../../types/weapons';
import GenericBioTech, { GenericBioTechActionPhases, GenericBioTechActions } from './GenericBioTech';
import TFactionID = PoliticalFactions.TFactionID;

export class GolliwogChassis extends GenericBioTech {
	public static readonly id = 40003;
	public static readonly caption = 'Golliwog';
	public static readonly description = `Some genetic experiments go wrong, some go wrong on purpose. This creature is an example: a mutation of a Biotech destined to be a faster, weaker scouting option, ended up being the ugliest abomination of all`;
	public static readonly exclusiveFactions: TFactionID[] = [TFactionID.draconians];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: { factor: 1.1 },
		},
		[Weapons.TWeaponGroupType.Lasers]: {
			baseCost: { factor: 1.1 },
			baseRange: {
				bias: 1,
			},
			baseAccuracy: {
				bias: 0.25,
			},
		},
		[Weapons.TWeaponGroupType.AntiAir]: {
			baseCost: { factor: 1.1 },
			baseRange: {
				bias: 1,
			},
			baseAccuracy: {
				bias: 0.25,
			},
		},
	};
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.small,
		Weapons.TWeaponSlotType.small,
	] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [DisarmAbility.id];
	public static readonly auras: TAuraApplianceList = [
		{
			id: HoundAroundAura.id,
		},
	];

	public baseCost: number = this.baseCost - 3;
	public speed: number = this.speed + 5;
	public hitPoints: number = this.hitPoints - 40;
	public scanRange: number = this.scanRange + 2;
	public equipmentSlots = 1;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		createActionOptions(GenericBioTechActions, GenericBioTechActionPhases + 1),
		UnitActions.unitActionTypes.delay,
		UnitActions.unitActionTypes.intercept,
	);
}

export default GolliwogChassis;

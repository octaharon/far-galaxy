import ElectrocutionAbility from '../../../../tactical/abilities/all/Bionic/Electrocute.37000';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericBioTech from './GenericBioTech';

export class NeosapientChassis extends GenericBioTech {
	public static readonly id = 40001;
	public static readonly caption = 'Neosapient';
	public static readonly description = `Smarter than most artificial lifeforms, this biotech can operate complex devices and evade dangers much better than predecessors, but it's extremely vulnerable to high temperatures and not so resilient in general`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {};
	public static readonly armorModifier = {
		[Damage.damageType.heat]: {
			factor: 1.6,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly abilities: number[] = [ElectrocutionAbility.id];
	public hitPoints: number = this.hitPoints - 10;
	public baseCost: number = this.baseCost + 9;
	public speed: number = this.speed + 1;
	public equipmentSlots: number = 1 + (this.equipmentSlots ?? 0);
	public defenseFlags = {} as UnitDefense.TDefenseFlags;
	public dodge = {
		default: 0.35,
	} as UnitDefense.TDodge;
}

export default NeosapientChassis;

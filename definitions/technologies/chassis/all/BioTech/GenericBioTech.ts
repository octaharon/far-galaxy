import UnitDefense from '../../../../tactical/damage/defense';
import ImposingPresenceAura from '../../../../tactical/statuses/Auras/all/ImposingPresence.5012';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import BiotechRankTree from '../../../../tactical/units/levels/RankTrees/BiotechRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericBioTechActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericBioTechActionPhases = 3;

export class GenericBioTech extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.biotech;
	public static readonly levelTree = BiotechRankTree;
	public static readonly flags = {
		[UnitChassis.targetType.organic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.medium,
		Weapons.TWeaponSlotType.medium,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		shieldAmount: {
			bias: 20,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.AntiAir]: {
			baseAccuracy: {
				bias: -0.3,
			},
		},
		[Weapons.TWeaponGroupType.Lasers]: {
			baseAccuracy: {
				bias: -0.3,
			},
		},
		[Weapons.TWeaponGroupType.Railguns]: {
			baseAccuracy: {
				bias: -0.3,
			},
		},
	} as Weapons.TWeaponGroupModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public static readonly auras: TAuraApplianceList = [
		{
			id: ImposingPresenceAura.id,
		},
	];
	public hitPoints = 100;
	public speed = 7;
	public baseCost = 21;
	public scanRange = 6;
	public dodge = {
		default: 0.1,
	} as UnitDefense.TDodge;
	public defenseFlags = {
		[UnitDefense.defenseFlags.resilient]: true,
	} as UnitDefense.TDefenseFlags;
	public actions = grantActionOnPhase(
		createActionOptions(GenericBioTechActions, GenericBioTechActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericBioTech;

import UnitDefense from '../../../../tactical/damage/defense';
import TrailblazerStatusEffect from '../../../../tactical/statuses/Effects/all/Unique/Trailblazer.37012';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericTiltjetChassis from './GenericTiltjet';

export class BluethunderChassis extends GenericTiltjetChassis {
	public static readonly id = 84002;
	public static readonly caption = 'Blue Thunder';
	public static readonly description = `A customized tiltjet with Trailblazer perk and an improved radar`;
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: TrailblazerStatusEffect.id,
		},
	];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public baseCost: number = this.baseCost + 6;
	public scanRange: number = this.scanRange + 3;
}

export default BluethunderChassis;

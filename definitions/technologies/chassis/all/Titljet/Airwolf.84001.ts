import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericTiltjetChassis from './GenericTiltjet';

const WModifier = {
	baseRange: {
		bias: 2,
	},
	baseAccuracy: {
		bias: 0.4,
	},
	baseRate: {
		bias: 0.2,
	},
};

export class AirwolfChassis extends GenericTiltjetChassis {
	public static readonly id = 84001;
	public static readonly caption = 'Airwolf';
	public static readonly description = `Long range VTOL with enhanced maneuverability and better missile tracking equipment`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Bombs]: {
			baseRate: {
				factor: 0.6,
			},
		},
		[Weapons.TWeaponGroupType.Missiles]: WModifier,
		[Weapons.TWeaponGroupType.Rockets]: WModifier,
		[Weapons.TWeaponGroupType.Cruise]: WModifier,
	};
	public baseCost: number = this.baseCost + 7;
	public speed: number = this.speed + 3;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
}

export default AirwolfChassis;

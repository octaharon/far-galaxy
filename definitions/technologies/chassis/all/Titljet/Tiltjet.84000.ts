import GenericTiltjetChassis from './GenericTiltjet';

export class TiltjetChassis extends GenericTiltjetChassis {
	public static readonly id = 84000;
	public static readonly caption = 'Tiltjet';
	public static readonly description = `Light aerial strike unit, rapidly leveling and offering
    substantial firepower. Tiltjets require at least somewhat dense atmosphere to be deployed,
    and they are much more vulnerable to air defense systems as they typically traverse on lower altitudes`;
}

export default TiltjetChassis;

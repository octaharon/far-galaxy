import CircuitBreakerAbility from '../../../../tactical/abilities/all/Unique/CircuitBreaker.37007';
import UnitDefense from '../../../../tactical/damage/defense';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericTiltjetChassis from './GenericTiltjet';

export class FlyingFoxChassis extends GenericTiltjetChassis {
	public static readonly id = 84003;
	public static readonly caption = 'Neopteryx';
	public static readonly description = `A stealth VTOL aircraft with a built-in shield regenerator and a powerful Circuit Breaker ability`;
	public static readonly abilities: number[] = [...GenericTiltjetChassis.abilities, CircuitBreakerAbility.id];
	public baseCost: number = this.baseCost + 8;
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.replenishing]: true,
	};
	public actions: UnitActions.TUnitActionOptions[] = this.actions.map((v) =>
		v.concat(UnitActions.unitActionTypes.cloak),
	);
}

export default FlyingFoxChassis;

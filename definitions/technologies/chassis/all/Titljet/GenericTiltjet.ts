import TossAttackAbility from '../../../../tactical/abilities/all/Units/TossAttack.29021';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import UnitActions from '../../../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import AircraftRankTree from '../../../../tactical/units/levels/RankTrees/AircraftRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import { GenericAircraft } from '../../genericAircraft';

export const GenericTiltjetActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.protect,
] as UnitActions.TUnitActionOptions;

export class GenericTiltjetChassis extends GenericAircraft {
	public static readonly type = UnitChassis.chassisClass.tiltjet;
	public static readonly levelTree = {
		...AircraftRankTree,
		levelingFunction: getLevelingFunction(TLevelingFunction.Fast150),
	};
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.air] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly shieldModifier = {
		overchargeDecay: {
			min: 5,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Guns]: {
			damageModifier: {
				factor: 1.2,
			},
		},
		[Weapons.TWeaponGroupType.Bombs]: {
			baseRate: {
				factor: 0.6,
			},
		},
	};
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public static readonly abilities: number[] = [TossAttackAbility.id];
	public baseCost: number = this.baseCost + 1;
	public speed = 14;
	public dodge: UnitDefense.TDodge = {
		default: 0.3,
		[UnitAttack.deliveryType.aerial]: -0.5,
	};
	public actions = [
		GenericTiltjetActions.slice().concat([UnitActions.unitActionTypes.delay]),
		GenericTiltjetActions.slice(),
	] as UnitActions.TUnitActionOptions[];
}

export default GenericTiltjetChassis;

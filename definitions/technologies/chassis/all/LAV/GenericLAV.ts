import AssaultShotAbility from '../../../../tactical/abilities/all/Units/AssaultShot.29002';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AssaultRankTree from '../../../../tactical/units/levels/RankTrees/AssaultRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericLAVActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericLAVActionPhases = 3;

export class GenericLightAssaultVehicle extends GenericChassis {
	public static readonly abilities: number[] = [AssaultShotAbility.id];
	public static readonly type = UnitChassis.chassisClass.lav;
	public static readonly levelTree = AssaultRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.medium] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public hitPoints = 75;
	public speed = 8;
	public baseCost = 14;
	public scanRange = 7;
	public dodge = {
		default: 0.1,
	} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericLAVActions, GenericLAVActionPhases),
		UnitActions.unitActionTypes.delay,
	);
	public equipmentSlots = 0;
}

export default GenericLightAssaultVehicle;

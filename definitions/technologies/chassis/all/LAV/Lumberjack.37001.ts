import DisablingShotAbility from '../../../../tactical/abilities/all/Units/DisablingShot.29000';
import UnitDefense from '../../../../tactical/damage/defense';
import TrailblazerStatusEffect from '../../../../tactical/statuses/Effects/all/Unique/Trailblazer.37012';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import { revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericLightAssaultVehicle from './GenericLAV';

export class LumberjackChassis extends GenericLightAssaultVehicle {
	public static readonly id = 37001;
	public static readonly caption = 'Lumberjack';
	public static readonly description = `Customized assault vehicle with better power source, providing
    for shield generator and the engine, but lighter carcass`;
	public static readonly abilities: number[] = [DisablingShotAbility.id];

	public static readonly shieldSlots: UnitDefense.TShieldSlotType[] = [UnitDefense.TShieldSlotType.medium];
	public static readonly armorSlots: UnitDefense.TArmorSlotType[] = [UnitDefense.TArmorSlotType.small];
	public static readonly shieldModifier: UnitDefense.TShieldsModifier = {
		...GenericLightAssaultVehicle.shieldModifier,
		shieldAmount: {
			bias: 10,
		},
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: TrailblazerStatusEffect.id,
		},
	];
	public static readonly armorModifier = {
		default: { factor: 1.1 },
	} as UnitDefense.TArmorModifier;
	public speed: number = this.speed + 2;
	public baseCost: number = this.baseCost + 8;
	public actions: UnitActions.TUnitActionOptions[] = revokeAction(this.actions, UnitActions.unitActionTypes.protect);
}

export default LumberjackChassis;

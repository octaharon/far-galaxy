import GenericLightAssaultVehicle from './GenericLAV';

export class LAVChassis extends GenericLightAssaultVehicle {
	public static readonly id = 37000;
	public static readonly caption = 'LAV';
	public static readonly description = `Light assault vehicle is a cheap and universal damage dealer, providing a compromise between
    price, mobility and firepower`;
}

export default LAVChassis;

import GroundingShotAbility from '../../../../tactical/abilities/all/Units/GroundingShot.29009';
import FrictionlessEffect from '../../../../tactical/statuses/Effects/all/Unique/Frictionless.37011';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericLightAssaultVehicle from './GenericLAV';

export class WardenChassis extends GenericLightAssaultVehicle {
	public static readonly id = 37002;
	public static readonly caption = 'Warden';
	public static readonly description = `A modification of light assault vehicle designed to exercise zone control
    and deny enemies from reaching a location, being bulkier and slower though`;
	public static readonly abilities: number[] = [GroundingShotAbility.id];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		...GenericLightAssaultVehicle.weaponModifier,
		default: {
			baseRange: {
				bias: 1,
			},
		},
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: FrictionlessEffect.id,
		},
	];
	public hitPoints: number = this.hitPoints + 15;
	public speed: number = this.speed - 2;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		this.actions,
		UnitActions.unitActionTypes.retaliate,
		UnitActions.unitActionTypes.intercept,
	);
	public baseCost: number = this.baseCost + 12;
}

export default WardenChassis;

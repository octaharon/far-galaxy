import BlunderbussShotAbility from '../../../../tactical/abilities/all/Units/BlunderbussShot.29020';
import UnitDefense from '../../../../tactical/damage/defense';
import UnitActions from '../../../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import AssaultRankTree from '../../../../tactical/units/levels/RankTrees/AssaultRankTree';
import GenericLightAssaultVehicle from './GenericLAV';

export class ConvictorChassis extends GenericLightAssaultVehicle {
	public static readonly id = 37003;
	public static readonly caption = 'Convictor';
	public static readonly description = `A special project of light assault vehicle with a cloak generator and an Equipment Slot, lacking durability and progressing through Ranks slower than most`;
	public static readonly abilities: number[] = [BlunderbussShotAbility.id];
	public static readonly levelTree = Object.assign({}, AssaultRankTree, {
		levelingFunction: getLevelingFunction(TLevelingFunction.Fast150),
	});
	public actions: UnitActions.TUnitActionOptions[] = this.actions.map((v) =>
		v.concat(UnitActions.unitActionTypes.cloak),
	);
	public baseCost: number = this.baseCost + 7;
	public dodge: UnitDefense.TDodge = {
		default: 0.25,
	};
	public hitPoints: number = this.hitPoints - 15;
	public equipmentSlots: number = (this.equipmentSlots ?? 0) + 1;
}

export default ConvictorChassis;

import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import VehicleRankTree from '../../../../tactical/units/levels/RankTrees/VehicleRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericMechActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.watch,
	UnitActions.unitActionTypes.attack,
] as UnitActions.TUnitActionOptions;
export const GenericMechActionPhases = 2;

export class GenericCombatMech extends GenericChassis {
	public static readonly levelTree = VehicleRankTree;
	public static readonly type = UnitChassis.chassisClass.mecha;
	public static readonly flags = {
		[UnitChassis.targetType.organic]: true,
		[UnitChassis.targetType.massive]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.large,
		Weapons.TWeaponSlotType.small,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {
		[Damage.damageType.biological]: {
			bias: -10,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly shieldModifier = {
		default: {
			threshold: 5,
		},
		shieldModifier: {
			factor: 1.2,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [
		UnitDefense.TArmorSlotType.large,
		UnitDefense.TArmorSlotType.medium,
	] as UnitDefense.TArmorSlotType[];
	public hitPoints = 120;
	public speed = 6;
	public baseCost = 24;
	public scanRange = 8;
	public dodge = {
		default: -0.3,
	} as UnitDefense.TDodge;
	public equipmentSlots = 0;
	public actions = grantActionOnPhase(
		createActionOptions(GenericMechActions, GenericMechActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericCombatMech;

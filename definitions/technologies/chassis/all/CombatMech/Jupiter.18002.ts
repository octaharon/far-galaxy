import StrafeShotAbility from '../../../../tactical/abilities/all/Units/StrafeShot.29004';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericCombatMech from './GenericCombatMech';
import unitActionTypes = UnitActions.unitActionTypes;

export class JupiterChassis extends GenericCombatMech {
	public static readonly id = 18002;
	public static readonly caption = 'Jupiter Mech';
	public static readonly description = `A god of combat mechas, this massive creation of engineering thought boasts a wide range of compatible weapons and best in class electronics,
    tuned to be most efficient against aerial targets`;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.medium,
		Weapons.TWeaponSlotType.medium,
	] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [StrafeShotAbility.id];
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.AntiAir]: {
			damageModifier: {
				factor: 1.2,
			},
			baseRange: {
				bias: 2,
			},
			baseAccuracy: {
				bias: 0.35,
			},
		},
		default: {
			baseAccuracy: {
				bias: 0.2,
			},
		},
	};
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, unitActionTypes.barrage);
	public baseCost: number = this.baseCost + 7;
}

export default JupiterChassis;

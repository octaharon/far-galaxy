import HowitzerShotAbility from '../../../../tactical/abilities/all/Units/SkyShot.29005';
import UnitDefense from '../../../../tactical/damage/defense';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericCombatMech from './GenericCombatMech';

export class BehemothChassis extends GenericCombatMech {
	public static readonly id = 18001;
	public static readonly caption = 'Behemoth';
	public static readonly description = `An overfitted combat mech with one Titan Weapon mount and even larger shield regenerator`;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.extra] as Weapons.TWeaponSlotType[];
	public static readonly flags = {
		...GenericCombatMech.flags,
		[UnitChassis.targetType.unique]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly abilities: number[] = [HowitzerShotAbility.id];

	public static readonly exclusiveFactions: PoliticalFactions.TFactionID[] = [
		PoliticalFactions.TFactionID.venaticians,
	];
	public static readonly shieldModifier = {
		default: {
			factor: 0.9,
		},
		shieldAmount: {
			factor: 1.75,
		},
	} as UnitDefense.TShieldsModifier;
	public defenseFlags = {
		[UnitDefense.defenseFlags.replenishing]: true,
	} as UnitDefense.TDefenseFlags;
	public speed: number = this.speed - 2;
	public hitPoints: number = this.hitPoints + 15;
	public baseCost: number = this.baseCost + 8;
}

export default BehemothChassis;

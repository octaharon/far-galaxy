import DisarmAbility from '../../../../tactical/abilities/all/Units/Disarm.29010';
import GenericCombatMech from './GenericCombatMech';

export class CombatMechChassis extends GenericCombatMech {
	public static readonly id = 18000;
	public static readonly caption = 'Combat Mech';
	public static readonly description = `A huge self-moving combat mechanism, operated by a cybernetically augmented pilot put inside a air-filtered chamber.
    It's quite slow and clumsy, but offers great firepower for the price.`;
	public static readonly abilities: number[] = [DisarmAbility.id];
}

export default CombatMechChassis;

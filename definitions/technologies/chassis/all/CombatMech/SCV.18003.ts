import QuantumTunnelAbility from '../../../../tactical/abilities/all/Units/QuantumTunnel.37002';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericCombatMech from './GenericCombatMech';

export class SCVChassis extends GenericCombatMech {
	public static readonly id = 18003;
	public static readonly caption = 'SCV';
	public static readonly description = `Specialized Combat Vehicle is a modified Combat Mech, tailored for tactical operations with the most advanced equipment
    rather than bigger weapons, and maneuvering around the battlefield faster than similar units`;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.large] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [QuantumTunnelAbility.id];
	public equipmentSlots: number = this.equipmentSlots + 1;
	public scanRange: number = this.scanRange + 3;
	public hitPoints: number = this.hitPoints - 15;
	public speed: number = this.speed + 3;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
	public baseCost: number = this.baseCost + 4;
	public dodge: UnitDefense.TDodge = {
		default: 0.1,
		[UnitAttack.deliveryType.drone]: 1,
	};
}

export default SCVChassis;

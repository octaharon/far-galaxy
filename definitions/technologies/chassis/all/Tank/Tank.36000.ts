import GenericTank from './GenericTank';

export class TankChassis extends GenericTank {
	public static readonly id = 36000;
	public static readonly caption = 'Tank';
	public static readonly description = `Heavy armored vehicle on tracks, with somewhat universal armament and equipment`;
}

export default TankChassis;

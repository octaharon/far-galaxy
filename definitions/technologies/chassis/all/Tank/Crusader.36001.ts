import StrafeShotAbility from '../../../../tactical/abilities/all/Units/StrafeShot.29004';
import FortitudeStatusEffect from '../../../../tactical/statuses/Effects/all/Unique/Fortitude.37013';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import Weapons from '../../../types/weapons';
import GenericTank from './GenericTank';

export class CrusaderChassis extends GenericTank {
	public static readonly id = 36001;
	public static readonly caption = 'Crusader';
	public static readonly description = `Mobile and better protected version of Tank best suiter for close combat with Machine Guns`;
	public static readonly abilities: number[] = [StrafeShotAbility.id];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: {
				factor: 1.5,
			},
		},
		[Weapons.TWeaponGroupType.Guns]: {
			damageModifier: {
				factor: 1.4,
			},
			baseRate: {
				bias: 0.15,
			},
			baseCost: {
				bias: -2,
			},
			baseAccuracy: {
				bias: 0.4,
			},
		},
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: FortitudeStatusEffect.id,
		},
	];
	public speed: number = this.speed + 3;
	public baseCost: number = this.baseCost + 5;
}

export default CrusaderChassis;

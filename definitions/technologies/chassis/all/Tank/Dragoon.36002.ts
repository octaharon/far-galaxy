import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericTank from './GenericTank';

export class DragoonChassis extends GenericTank {
	public static readonly id = 36002;
	public static readonly caption = 'Dragoon';
	public static readonly description = `Customized tank with a mortar weapon mount and supreme protection, capable of precisely shooting at full throttle`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: {
				factor: 1.5,
			},
		},
		[Weapons.TWeaponGroupType.Launchers]: {
			damageModifier: {
				factor: 1.2,
			},
			baseRate: {
				bias: 0.4,
			},
			baseCost: {
				bias: -4,
			},
			baseAccuracy: {
				bias: 0.5,
			},
		},
	};
	public static readonly armorModifier = {
		...GenericTank.armorModifier,
		default: {
			...(GenericTank.armorModifier?.default || {}),
			threshold: 5,
		},
	} as UnitDefense.TArmorModifier;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		this.actions,
		UnitActions.unitActionTypes.attack_move,
		UnitActions.unitActionTypes.attack,
	);
	public baseCost: number = this.baseCost + 4;
}

export default DragoonChassis;

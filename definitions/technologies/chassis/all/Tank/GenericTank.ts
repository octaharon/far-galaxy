import BlunderbussShotAbility from '../../../../tactical/abilities/all/Units/BlunderbussShot.29020';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import VehicleRankTree from '../../../../tactical/units/levels/RankTrees/VehicleRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericTankActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.protect,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.watch,
	UnitActions.unitActionTypes.defend,
] as UnitActions.TUnitActionOptions;

export const GenericTankActionPhases = 2;

export class GenericTank extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.tank;
	public static readonly abilities: number[] = [BlunderbussShotAbility.id];
	public static readonly levelTree = VehicleRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.medium] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {
		default: {
			bias: -2,
			factor: 0.95,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [
		UnitDefense.TArmorSlotType.medium,
		UnitDefense.TArmorSlotType.medium,
	] as UnitDefense.TArmorSlotType[];
	public hitPoints = 140;
	public speed = 5;
	public baseCost = 14;
	public scanRange = 6;
	public dodge = {
		default: -0.1,
		[UnitAttack.deliveryType.bombardment]: -1,
	} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericTankActions, GenericTankActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericTank;

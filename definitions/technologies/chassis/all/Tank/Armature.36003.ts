import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import DroneJammerAura from '../../../../tactical/statuses/Auras/all/DroneJammer.5004';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import VehicleRankTree from '../../../../tactical/units/levels/RankTrees/VehicleRankTree';
import GenericTank from './GenericTank';

export class ArmatureChassis extends GenericTank {
	public static readonly id = 36003;
	public static readonly caption = 'Armature';
	public static readonly description = `Anti-drone tank with Reactive Defense and great Drones suppression capabilities, but slower and less agile`;
	public static readonly levelTree = Object.assign({}, VehicleRankTree, {
		levelingFunction: getLevelingFunction(TLevelingFunction.Linear150),
	});

	public static readonly auras: TAuraApplianceList = [
		{
			id: DroneJammerAura.id,
		},
	];
	public baseCost: number = this.baseCost + 7;
	public speed: number = this.speed - 1;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.drone]: 2,
	};
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public hitPoints: number = this.hitPoints + 15;
}

export default ArmatureChassis;

import DecoyShotAbility from '../../../../tactical/abilities/all/Units/DecoyShot.29008';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import DroneJammerAura from '../../../../tactical/statuses/Auras/all/DroneJammer.5004';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import Weapons from '../../../types/weapons';
import GenericCruiser from './GenericCruiser';

export class AuroraChassis extends GenericCruiser {
	public static readonly id = 65002;
	public static readonly caption = 'Aurora';
	public static readonly description = `Cruiser fine-tuned to operate tactical weapons and suppress drone clouds, while being less sturdy than other similar vessels`;
	public static readonly auras: TAuraApplianceList = [
		{
			id: DroneJammerAura.id,
			props: {
				range: 4,
			},
		},
	];
	public static readonly abilities: number[] = [DecoyShotAbility.id];
	public static readonly shieldModifier = {
		default: {
			factor: 1.1,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: {
				factor: 1.2,
			},
		},
		[Weapons.TWeaponGroupType.Rockets]: {
			damageModifier: {
				factor: 1.1,
			},
			baseRate: {
				bias: 0.1,
			},
		},
		[Weapons.TWeaponGroupType.Cruise]: {
			damageModifier: {
				factor: 1.1,
			},
			baseRate: {
				bias: 0.1,
			},
		},
		[Weapons.TWeaponGroupType.Missiles]: {
			damageModifier: {
				factor: 1.2,
			},
			baseRate: {
				bias: 0.2,
			},
			baseRange: {
				bias: 2,
			},
		},
	};
	public baseCost: number = this.baseCost + 6;
	public hitPoints: number = this.hitPoints - 15;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.drone]: 2,
	};
}

export default AuroraChassis;

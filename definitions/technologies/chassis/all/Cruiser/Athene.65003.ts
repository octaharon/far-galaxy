import QuantumTunnelAbility from '../../../../tactical/abilities/all/Units/QuantumTunnel.37002';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericCruiser from './GenericCruiser';

export class AtheneChassis extends GenericCruiser {
	public static readonly id = 65003;
	public static readonly caption = 'Athene';
	public static readonly description = `Athene-class cruiser has a warp generator installed, offering better protection from energy weapons and an option
    to drop out of the timeline, when it's handy. However, the generator interferes with sonar and is easily detected by targeting systems`;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly abilities: number[] = [QuantumTunnelAbility.id];
	public static readonly shieldModifier: UnitDefense.TShieldsModifier = {
		shieldAmount: {
			factor: 1.2,
		},
	};
	public baseCost: number = this.baseCost + 10;
	public speed: number = this.speed + 1;
	public scanRange: number = this.scanRange - 2;
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.warp]: true,
	};
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.beam]: 0.5,
		[UnitAttack.deliveryType.missile]: -1,
		[UnitAttack.deliveryType.drone]: -1,
		[UnitAttack.deliveryType.cloud]: 1,
	};
	public actions: UnitActions.TUnitActionOptions[] = revokeAction(
		grantAction(this.actions, UnitActions.unitActionTypes.warp),
		UnitActions.unitActionTypes.intercept,
	);
}

export default AtheneChassis;

import StasisMatrixAbility from '../../../../tactical/abilities/all/Stasis/StasisMatrixAbility.20001';
import UnitDefense from '../../../../tactical/damage/defense';
import QuantumProjectionEffect from '../../../../tactical/statuses/Effects/all/Unique/QuantumProjection.37015';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericCruiser from './GenericCruiser';

export class PoseidonChassis extends GenericCruiser {
	public static readonly id = 65001;
	public static readonly caption = 'Poseidon';
	public static readonly description = `Expensive refitted version of a cruiser with stronger defense and superior cyberwarfare capabilities`;
	public static readonly armorModifier = {
		default: {
			bias: -8,
			threshold: 5,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly abilities: number[] = [StasisMatrixAbility.id];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: QuantumProjectionEffect.id,
		},
	];
	public baseCost: number = this.baseCost + 8;
}

export default PoseidonChassis;

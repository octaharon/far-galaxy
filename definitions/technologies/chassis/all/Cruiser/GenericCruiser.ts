import FocusedEMIAbility from '../../../../tactical/abilities/all/Units/FocusedEMI.29022';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import NavalRankTree from '../../../../tactical/units/levels/RankTrees/NavalRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericCruiserActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.intercept,
] as UnitActions.TUnitActionOptions;

export const GenericCruiserActionPhases = 2;

export class GenericCruiser extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.cruiser;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly levelTree = NavalRankTree;
	public static readonly movement = UnitChassis.movementType.naval as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.naval] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly abilities: number[] = [FocusedEMIAbility.id];
	public static readonly armorModifier = {
		default: {
			bias: -5,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public hitPoints = 135;
	public speed = 7;
	public baseCost = 25;
	public scanRange = 10;
	public dodge = {
		[UnitAttack.deliveryType.bombardment]: -1,
	} as UnitDefense.TDodge;
	public equipmentSlots = 1;
	public actions = grantActionOnPhase(
		createActionOptions(GenericCruiserActions, GenericCruiserActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericCruiser;

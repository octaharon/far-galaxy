import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericCruiser from './GenericCruiser';

export class CruiserChassis extends GenericCruiser {
	public static readonly id = 65000;
	public static readonly caption = 'Cruiser';
	public static readonly description = `Heavily armored, somewhat slowly moving ship with a long range weapon platform, an equipment bay and an improved sonar.
    A backbone of the fleet`;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.protect);
}

export default CruiserChassis;

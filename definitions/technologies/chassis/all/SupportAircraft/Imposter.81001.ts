import { IUnitEffectModifier } from '../../../../prototypes/types';
import CausticIsomersAbility from '../../../../tactical/abilities/all/Bionic/CausticIsomers.50005';
import QuantumTunnelAbility from '../../../../tactical/abilities/all/Units/QuantumTunnel.37002';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import UnitChassis from '../../../types/chassis';
import GenericSupportAircraft from './GenericSupportAircraft';

export class ImposterChassis extends GenericSupportAircraft {
	public static readonly id = 81001;
	public static readonly caption = 'Imposter';
	public static readonly description = `Support Airborne vessel with bionic core, contained within a standard shell, but providing outstanding Ability options`;
	public static readonly abilities: number[] = [CausticIsomersAbility.id, QuantumTunnelAbility.id];
	public static readonly flags = {
		[UnitChassis.targetType.organic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly unitEffectModifier: IUnitEffectModifier = {
		abilityRangeModifier: [
			{
				bias: 3,
			},
		],
		abilityPowerModifier: [
			{
				bias: 0.5,
			},
		],
		abilityDurationModifier: [
			{
				bias: 1,
			},
		],
	};
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.delay);
	public hitPoints: number = this.hitPoints + 10;
	public baseCost: number = this.baseCost + 7;
}

export default ImposterChassis;

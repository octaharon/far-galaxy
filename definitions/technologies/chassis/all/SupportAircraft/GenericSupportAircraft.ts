import MendingStreamAbility from '../../../../tactical/abilities/all/ElectronicWarfare/MendingStreamAbility.50003';
import SVSurveyAbility from '../../../../tactical/abilities/all/ScienceVessel/SVSurveyAbility.99910';
import UnitDefense from '../../../../tactical/damage/defense';
import SyncreticEvolutionAura from '../../../../tactical/statuses/Auras/all/SyncreticEvolution.5010';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { grantAction, grantActionOnPhase, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AircraftRankTree from '../../../../tactical/units/levels/RankTrees/AircraftRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import { GenericAircraft } from '../../genericAircraft';

export class GenericSupportAircraft extends GenericAircraft {
	public static readonly levelTree = AircraftRankTree;
	public static readonly type = UnitChassis.chassisClass.sup_aircraft;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly weaponSlots = [] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [SVSurveyAbility.id, MendingStreamAbility.id];
	public static readonly auras: TAuraApplianceList = [
		{
			id: SyncreticEvolutionAura.id,
		},
	];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {};
	public static readonly shieldModifier = {
		overchargeDecay: {
			min: 5,
		},
		shieldAmount: {
			factor: 1.4,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public scanRange = 15;
	public hitPoints: number = this.hitPoints - 5;
	public defenseFlags = {};
	public equipmentSlots = 2;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(
		revokeAction(
			grantActionOnPhase(this.actions, UnitActions.unitActionTypes.delay),
			UnitActions.unitActionTypes.barrage,
			UnitActions.unitActionTypes.attack_move,
		),
		UnitActions.unitActionTypes.defend,
	);
}

export default GenericSupportAircraft;

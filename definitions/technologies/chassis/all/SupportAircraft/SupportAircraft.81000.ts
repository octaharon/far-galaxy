import GenericSupportAircraft from './GenericSupportAircraft';

export class SupportAircraftChassis extends GenericSupportAircraft {
	public static readonly id = 81000;
	public static readonly caption = 'Support Aircraft';
	public static readonly description = `Surveillance and support aircraft with no combat weapon mounts, but possessing an equipment bay onboard and an unique Mending Stream Ability`;
}

export default SupportAircraftChassis;

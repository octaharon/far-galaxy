import BlunderbussShotAbility from '../../../../tactical/abilities/all/Units/BlunderbussShot.29020';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericRover from './GenericRover';

export class HawkChassis extends GenericRover {
	public static readonly id = 50002;
	public static readonly caption = 'Hawk';
	public static readonly description = `Customized rover unit with twin machine gun racks and a better sonar, but slightly reduced shield generators`;
	public static readonly weaponSlots: Weapons.TWeaponSlotType[] = [
		Weapons.TWeaponSlotType.small,
		Weapons.TWeaponSlotType.small,
	];
	public static readonly shieldModifier = {
		shieldAmount: {
			factor: 0.85,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly weaponModifier?: Weapons.TWeaponGroupModifier = {
		default: {
			baseCost: {
				factor: 1.5,
			},
		},
		[Weapons.TWeaponGroupType.Guns]: {
			baseRange: {
				bias: 2,
			},
			baseAccuracy: {
				bias: 0.4,
			},
		},
	};
	public static readonly abilities: number[] = [BlunderbussShotAbility.id];
	public baseCost: number = this.baseCost + 6;
	public scanRange: number = this.scanRange + 2;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.barrage);
}

export default HawkChassis;

import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import { VehicleRankTree } from '../../../../tactical/units/levels/RankTrees/VehicleRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericRoverActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.retaliate,
	UnitActions.unitActionTypes.suppress,
	UnitActions.unitActionTypes.watch,
] as UnitActions.TUnitActionOptions;

export const GenericRoverActionPhases = 3;

export class GenericRover extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.rover;
	public static readonly levelTree = VehicleRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.hover as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.medium] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public hitPoints = 75;
	public speed = 9;
	public baseCost = 18;
	public scanRange = 7;
	public defenseFlags = {
		[UnitDefense.defenseFlags.evasion]: true,
	} as UnitDefense.TDefenseFlags;
	public dodge = {
		default: 0.15,
	} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericRoverActions, GenericRoverActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericRover;

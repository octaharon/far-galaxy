import DisablingShotAbility from '../../../../tactical/abilities/all/Units/DisablingShot.29000';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericRover from './GenericRover';

export class RoverChassis extends GenericRover {
	public static readonly id = 50000;
	public static readonly caption = 'Rover';
	public static readonly description = `Light scout unit on a grav cushion, suitable for navigating densely crossed terrain and
    swift enough to evade conventional weapons`;
	public static readonly abilities: number[] = [DisablingShotAbility.id];
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.protect);
}

export default RoverChassis;

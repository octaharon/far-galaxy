import GroundingShotAbility from '../../../../tactical/abilities/all/Units/GroundingShot.29009';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericRover from './GenericRover';

export class VultureChassis extends GenericRover {
	public static readonly id = 50001;
	public static readonly caption = 'Vulture';
	public static readonly description = `Customized rover unit with drastically improved speed and maneuverability, but reduced damage resilience`;
	public static readonly armorModifier = {
		default: {
			factor: 1.1,
		},
	} as UnitDefense.TArmorModifier;
	public static readonly abilities: number[] = [GroundingShotAbility.id];
	public baseCost: number = this.baseCost + 3;
	public speed: number = this.speed + 4;
	public hitPoints: number = this.hitPoints - 10;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		default: 0.7,
	};
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.defend);
}

export default VultureChassis;

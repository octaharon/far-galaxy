import { ArithmeticPrecision } from '../../../../maths/constants';
import { IUnitEffectModifier } from '../../../../prototypes/types';
import SVHallucinationAbility from '../../../../tactical/abilities/all/ScienceVessel/SVHallucination.99911';
import SVSurveyAbility from '../../../../tactical/abilities/all/ScienceVessel/SVSurveyAbility.99910';
import UnitDefense from '../../../../tactical/damage/defense';
import SVNanoCloudAura from '../../../../tactical/statuses/Auras/all/SVNanoCloud.5002';
import SyncreticEvolutionAura from '../../../../tactical/statuses/Auras/all/SyncreticEvolution.5010';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import GenericScienceVessel from './GenericScienceVessel';

export class WandererChassis extends GenericScienceVessel {
	public static readonly id = 99001;
	public static readonly caption = 'The Wanderer';
	public static readonly description = `Aquarius Accord is famous for their exquisite surveillance technologies and electronics, which allows them to manufacture extremely durable and functional research bots for mass market`;
	public static readonly exclusiveFactions: PoliticalFactions.TFactionID[] = [PoliticalFactions.TFactionID.aquarians];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly auras: TAuraApplianceList = [
		{
			id: SyncreticEvolutionAura.id,
			props: {
				range: 5,
			},
		},
		{
			id: SVNanoCloudAura.id,
			props: {
				range: 5,
			},
		},
	];
	public static readonly abilities: number[] = [SVSurveyAbility.id, SVHallucinationAbility.id];
	public static readonly unitEffectModifier: IUnitEffectModifier = {
		...GenericScienceVessel.unitEffectModifier,
		abilityPowerModifier: [{ factor: 2, minGuard: ArithmeticPrecision }],
	};
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
		[UnitChassis.targetType.unique]: true,
	} as UnitChassis.TTargetFlags;
	public speed: number = this.speed + 2;
	public hitPoints: number = this.hitPoints + 15;
	public baseCost: number = this.baseCost + 3;
	public equipmentSlots = 3;
}

export default WandererChassis;

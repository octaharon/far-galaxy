import { ArithmeticPrecision } from '../../../../maths/constants';
import { IUnitEffectModifier } from '../../../../prototypes/types';
import SVHallucinationAbility from '../../../../tactical/abilities/all/ScienceVessel/SVHallucination.99911';
import SVSurveyAbility from '../../../../tactical/abilities/all/ScienceVessel/SVSurveyAbility.99910';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import SVNanoCloudAura from '../../../../tactical/statuses/Auras/all/SVNanoCloud.5002';
import SyncreticEvolutionAura from '../../../../tactical/statuses/Auras/all/SyncreticEvolution.5010';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import ScienceVesselRankTree from '../../../../tactical/units/levels/RankTrees/ScienceVesselRankTree';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericScienceActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.warp,
] as UnitActions.TUnitActionOptions;

export const GenericScienceVesselActionPhases = 3;

export class GenericScienceVessel extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.science_vessel;
	public static readonly levelTree = ScienceVesselRankTree;
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [PoliticalFactions.TFactionID.pirates];
	public static readonly abilities: number[] = [SVSurveyAbility.id, SVHallucinationAbility.id];
	public static readonly movement = UnitChassis.movementType.hover as UnitChassis.movementType;
	public static readonly weaponSlots = [] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly unitEffectModifier: IUnitEffectModifier = {
		abilityRangeModifier: [{ bias: 2, minGuard: ArithmeticPrecision }],
	};
	public static readonly shieldModifier = {
		overchargeDecay: {
			min: 10,
		},
		shieldAmount: {
			factor: 2.0,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly auras: TAuraApplianceList = [
		{
			id: SyncreticEvolutionAura.id,
		},
		{
			id: SVNanoCloudAura.id,
		},
	];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public hitPoints = 55;
	public speed = 12;
	public baseCost = 21;
	public scanRange = 14;
	public dodge = {
		default: 0.6,
		[UnitAttack.deliveryType.drone]: 1,
	} as UnitDefense.TDodge;
	public defenseFlags = {
		[UnitDefense.defenseFlags.hull]: true,
	} as UnitDefense.TDefenseFlags;
	public equipmentSlots = 2;

	public actions = grantActionOnPhase(
		createActionOptions(GenericScienceActions, GenericScienceVesselActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericScienceVessel;

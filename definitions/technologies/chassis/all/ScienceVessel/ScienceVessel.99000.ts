import GenericScienceVessel from './GenericScienceVessel';

export class ScienceVesselChassis extends GenericScienceVessel {
	public static readonly id = 99000;
	public static readonly caption = 'Science Vessel';
	public static readonly description = `Automated field laboratory stuffed with all sorts of fancy bleeding edge technologies. Science Vessel can't maintain any weapons, but gets Abilities with levels instead, and also heals all friendly units around oneself every turn`;
}

export default ScienceVesselChassis;

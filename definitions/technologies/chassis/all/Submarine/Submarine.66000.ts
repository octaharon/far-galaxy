import GenericSubmarine from './GenericSubmarine';

export class SubmarineChassis extends GenericSubmarine {
	public static readonly id = 66000;
	public static readonly caption = 'Submarine';
	public static readonly description = `Submersible ship, designed to launch missile weapons at target,
    staying undetected until the very moment of attack`;
}

export default SubmarineChassis;

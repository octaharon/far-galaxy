import FocusedEMIAbility from '../../../../tactical/abilities/all/Units/FocusedEMI.29022';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import { getLevelingFunction, TLevelingFunction } from '../../../../tactical/units/levels/levelingFunctions';
import Weapons from '../../../types/weapons';
import { stackWeaponGroupModifiers } from '../../../weapons/WeaponManager';
import GenericSubmarine from './GenericSubmarine';

export class SawfishChassis extends GenericSubmarine {
	public static readonly id = 66002;
	public static readonly caption = 'Sawfish';
	public static readonly description = `Cybernetic submarine with drone suppression system and extreme maneuverability, but also with certain manufacturing expenses.`;
	public static readonly levelTree = {
		...GenericSubmarine.levelTree,
		levelingFunction: getLevelingFunction(TLevelingFunction.Prodigy),
	};
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = stackWeaponGroupModifiers(
		GenericSubmarine.weaponModifier,
		{ default: { baseCost: { factor: 1.2 } } },
	);
	public static readonly abilities: number[] = [FocusedEMIAbility.id];
	public baseCost: number = this.baseCost + 9;
	public speed: number = this.speed - 1;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.drone]: 2,
	};
	public defenseFlags: UnitDefense.TDefenseFlags = {
		...this.defenseFlags,
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
}

export default SawfishChassis;

import SVSubjugateAbility from '../../../../tactical/abilities/all/ScienceVessel/SVSubjugationAbility.99907';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericSubmarine from './GenericSubmarine';

export class AnglerChassis extends GenericSubmarine {
	public static readonly id = 66001;
	public static readonly caption = 'Angler';
	public static readonly description = `Customized submarine with the energy rewired to support the biggest available shield generator and provide extra throttle in favor or scan range and hull integrity`;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.large] as UnitDefense.TShieldSlotType[];
	public static readonly abilities: number[] = [SVSubjugateAbility.id];
	public baseCost: number = this.baseCost + 4;
	public hitPoints: number = this.hitPoints - 15;
	public scanRange: number = this.scanRange - 2;
	public speed: number = this.speed + 3;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.suppress);
}

export default AnglerChassis;

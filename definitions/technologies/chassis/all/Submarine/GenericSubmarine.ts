import { fastMerge } from '../../../../core/dataTransformTools';
import DecoyShotAbility from '../../../../tactical/abilities/all/Units/DecoyShot.29008';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import NavalRankTree from '../../../../tactical/units/levels/RankTrees/NavalRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericSubmarineActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.intercept,
	UnitActions.unitActionTypes.watch,
	UnitActions.unitActionTypes.cloak,
] as UnitActions.TUnitActionOptions;

export const GenericSubmarineActionPhases = 2;

const SubmarineWeaponModifier = {
	baseCost: {
		bias: -4,
	},
	baseRange: {
		bias: 3,
	},
	baseRate: {
		bias: 0.3,
	},
} as Weapons.TWeaponModifier;

export class GenericSubmarine extends GenericChassis {
	public static readonly levelTree = NavalRankTree;
	public static readonly type = UnitChassis.chassisClass.submarine;
	public static readonly flags = {
		[UnitChassis.targetType.robotic]: true,
	} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.naval as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.large] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly weaponModifier = {
		[Weapons.TWeaponGroupType.Missiles]: fastMerge<Weapons.TWeaponModifier>(SubmarineWeaponModifier),
		[Weapons.TWeaponGroupType.Rockets]: fastMerge<Weapons.TWeaponModifier>(SubmarineWeaponModifier),
		[Weapons.TWeaponGroupType.Cruise]: fastMerge<Weapons.TWeaponModifier>(SubmarineWeaponModifier),
	} as Weapons.TWeaponGroupModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public static readonly abilities: number[] = [DecoyShotAbility.id];
	public hitPoints = 90;
	public speed = 7;
	public baseCost = 25;
	public scanRange = 11;
	public dodge = {
		[UnitAttack.deliveryType.bombardment]: 10,
		[UnitAttack.deliveryType.surface]: 10,
	} as UnitDefense.TDodge;
	public actions = grantActionOnPhase(
		createActionOptions(GenericSubmarineActions, GenericSubmarineActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericSubmarine;

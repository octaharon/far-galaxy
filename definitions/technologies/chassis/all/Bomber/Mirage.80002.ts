import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericBomber, { GenericBomberActionPhases, GenericBomberActions } from './GenericBomber';

export class MirageChassis extends GenericBomber {
	public static readonly id = 80002;
	public static readonly caption = 'Mirage';
	public static readonly description = `A bomber class plane which had its hull lightened to provide for better speed and maneuverability`;
	public static readonly shieldModifier = {
		shieldAmount: {
			bias: 30,
		},
		overchargeDecay: { min: 5 },
	} as UnitDefense.TShieldsModifier;
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public baseCost: number = this.baseCost + 3;
	public hitPoints: number = this.hitPoints - 15;
	public speed: number = this.speed + 2;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		default: 0.2,
		[UnitAttack.deliveryType.aerial]: 0.2,
	};
	public actions = grantActionOnPhase(
		createActionOptions(GenericBomberActions, GenericBomberActionPhases + 1),
		UnitActions.unitActionTypes.delay,
	);
}

export default MirageChassis;

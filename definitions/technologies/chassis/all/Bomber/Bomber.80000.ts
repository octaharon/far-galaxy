import GenericBomber from './GenericBomber';

export class BomberChassis extends GenericBomber {
	public static readonly id = 80000;
	public static readonly caption = 'Bomber';
	public static readonly description = `A slowest aircraft designed to deliver powerful weapons while guarded by faster jets`;
}

export default BomberChassis;

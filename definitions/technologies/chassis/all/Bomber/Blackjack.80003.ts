import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericBomber from './GenericBomber';

export class BlackjackChassis extends GenericBomber {
	public static readonly id = 80003;
	public static readonly caption = 'Blackjack';
	public static readonly description = `A customized bomber that is designated to operate without air support. It's also equipped with the latest electronic warfare and drone suppression systems`;
	public static readonly shieldModifier = {
		shieldAmount: {
			bias: 50,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public baseCost: number = this.baseCost + 5;
	public hitPoints: number = this.hitPoints + 15;
	public scanRange: number = this.scanRange + 3;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		default: 0.1,
		[UnitAttack.deliveryType.drone]: 2,
		[UnitAttack.deliveryType.aerial]: 0.1,
	};
}

export default BlackjackChassis;

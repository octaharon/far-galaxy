import UnitDefense from '../../../../tactical/damage/defense';
import GenericBomber from './GenericBomber';

export class MaulerChassis extends GenericBomber {
	public static readonly id = 80001;
	public static readonly caption = 'Mauler';
	public static readonly description = `The heaviest and sturdiest version of Bomber class aircrafts`;
	public static readonly armorModifier: UnitDefense.TArmorModifier = {
		default: {
			bias: -5,
		},
	};
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.large] as UnitDefense.TArmorSlotType[];
	public baseCost: number = this.baseCost + 5;
	public hitPoints: number = this.hitPoints + 35;
	public speed: number = this.speed - 2;
}

export default MaulerChassis;

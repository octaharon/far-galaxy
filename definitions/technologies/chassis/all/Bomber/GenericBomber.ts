import AirInterdictionAbility from '../../../../tactical/abilities/all/Units/AirInterdicton.29104';
import UnitAttack from '../../../../tactical/damage/attack';
import UnitDefense from '../../../../tactical/damage/defense';
import AboveTheFightEffect from '../../../../tactical/statuses/Effects/all/Unit/AboveTheFight.29105';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import { createActionOptions, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AircraftRankTree from '../../../../tactical/units/levels/RankTrees/AircraftRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import { GenericAircraft } from '../../genericAircraft';

export const GenericBomberActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.barrage,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.attack_move,
	UnitActions.unitActionTypes.move,
] as UnitActions.TUnitActionOptions;

export const GenericBomberActionPhases = 3;

export class GenericBomber extends GenericAircraft {
	public static readonly type = UnitChassis.chassisClass.bomber;
	public static readonly levelTree = AircraftRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly weaponSlots = [
		Weapons.TWeaponSlotType.air,
		Weapons.TWeaponSlotType.air,
	] as Weapons.TWeaponSlotType[];
	public static readonly shieldModifier = {
		shieldAmount: {
			bias: 60,
		},
	} as UnitDefense.TShieldsModifier;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Bombs]: {
			damageModifier: {
				factor: 1.25,
			},
			baseRange: {
				bias: 1,
			},
			baseAccuracy: {
				min: 2.5,
			},
			baseCost: {
				factor: 0.8,
			},
		},
		default: {
			baseRange: {
				factor: 0.6,
			},
			baseCost: {
				factor: 1.5,
			},
		},
	};
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.medium] as UnitDefense.TArmorSlotType[];
	public static readonly abilities: number[] = [AirInterdictionAbility.id];
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: AboveTheFightEffect.id,
		},
	];
	public scanRange = 5;
	public baseCost: number = this.baseCost + 5;
	public speed: number = this.speed - 3;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		[UnitAttack.deliveryType.aerial]: -0.5,
	};
	public actions = grantActionOnPhase(
		createActionOptions(GenericBomberActions, GenericBomberActionPhases),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericBomber;

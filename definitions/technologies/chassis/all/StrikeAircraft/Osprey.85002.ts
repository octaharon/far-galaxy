import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericStrikeAircraft from './GenericStrikeAircraft';

export class OspreyChassis extends GenericStrikeAircraft {
	public static readonly id = 85002;
	public static readonly caption = 'SA "Osprey"';
	public static readonly description = `Experimental ground attack plane with conceptual magnetic coils, providing
    better compatibility with energy weapons and a damage absorption capacitor`;
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		...GenericStrikeAircraft.weaponModifier,
		[Weapons.TWeaponGroupType.Beam]: {
			baseRate: {
				factor: 1.2,
			},
			baseAccuracy: {
				bias: 0.6,
			},
		},
		[Weapons.TWeaponGroupType.Lasers]: {
			baseRate: {
				factor: 1.4,
			},
			baseAccuracy: {
				bias: 0.35,
			},
		},
	};
	public defenseFlags = {
		[UnitDefense.defenseFlags.sma]: true,
	};
	public baseCost: number = this.baseCost + 8;
}

export default OspreyChassis;

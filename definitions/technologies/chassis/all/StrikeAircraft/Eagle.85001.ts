import UnitDefense from '../../../../tactical/damage/defense';
import GenericStrikeAircraft from './GenericStrikeAircraft';

export class EagleChassis extends GenericStrikeAircraft {
	public static readonly id = 85001;
	public static readonly caption = 'SA "Eagle"';
	public static readonly description = `Customized strike aircraft with improved evasion capabilities`;
	public defenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
		[UnitDefense.defenseFlags.evasion]: true,
	};
	public baseCost: number = this.baseCost + 6;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		default: 0.5,
	};
}

export default EagleChassis;

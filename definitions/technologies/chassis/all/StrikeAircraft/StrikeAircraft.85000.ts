import GenericStrikeAircraft from './GenericStrikeAircraft';

export class StrikeAircraftChassis extends GenericStrikeAircraft {
	public static readonly id = 85000;
	public static readonly caption = 'Strike Aircraft';
	public static readonly description = `This class of lighter flying vehicles is designed for close air support and digital warfare missions, following heavier aircrafts with an equipment bay installed of one Weapon Slot`;
}

export default StrikeAircraftChassis;

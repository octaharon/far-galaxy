import ThresholdAltitudeAbility from '../../../../tactical/abilities/all/Units/ThresholdAltitude.29100';
import UnitDefense from '../../../../tactical/damage/defense';
import { grantAction, grantActionOnPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import AircraftRankTree from '../../../../tactical/units/levels/RankTrees/AircraftRankTree';
import PoliticalFactions from '../../../../world/factions';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import { GenericAircraft } from '../../genericAircraft';

export class GenericStrikeAircraft extends GenericAircraft {
	public static readonly levelTree = AircraftRankTree;
	public static readonly type = UnitChassis.chassisClass.strike_aircraft;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly restrictedFactions: PoliticalFactions.TFactionID[] = [
		PoliticalFactions.TFactionID.aquarians,
	];
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.air] as Weapons.TWeaponSlotType[];
	public static readonly shieldModifier = {} as UnitDefense.TShieldsModifier;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly abilities: number[] = [ThresholdAltitudeAbility.id];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Bombs]: {
			damageModifier: {
				factor: 1.2,
			},
		},
		[Weapons.TWeaponGroupType.Rockets]: {
			damageModifier: {
				factor: 1.2,
			},
		},
	};
	public baseCost: number = this.baseCost + 2;
	public equipmentSlots = 1;
	public actions: UnitActions.TUnitActionOptions[] = grantActionOnPhase(
		grantAction(this.actions, UnitActions.unitActionTypes.intercept, UnitActions.unitActionTypes.suppress),
		UnitActions.unitActionTypes.delay,
	);
}

export default GenericStrikeAircraft;

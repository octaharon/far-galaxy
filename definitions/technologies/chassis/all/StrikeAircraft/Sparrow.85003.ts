import UnitDefense from '../../../../tactical/damage/defense';
import { addActionPhase } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericStrikeAircraft from './GenericStrikeAircraft';

export class SparrowChassis extends GenericStrikeAircraft {
	public static readonly id = 85003;
	public static readonly caption = 'SA "Sparrow"';
	public static readonly description = `Customized strike aircraft with lighter hull but considerably improved maneuverability`;
	public baseCost: number = this.baseCost + 2;
	public hitPoints: number = this.hitPoints - 15;
	public dodge: UnitDefense.TDodge = {
		...this.dodge,
		default: 0.2,
	};
	public speed: number = this.speed + 3;
	public actions: UnitActions.TUnitActionOptions[] = addActionPhase(this.actions);
}

export default SparrowChassis;

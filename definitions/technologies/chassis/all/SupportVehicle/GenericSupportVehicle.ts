import NegativeSurgeAbility from '../../../../tactical/abilities/all/ElectronicWarfare/NegativeSurgeAbility.50002';
import SVSurveyAbility from '../../../../tactical/abilities/all/ScienceVessel/SVSurveyAbility.99910';
import UnitDefense from '../../../../tactical/damage/defense';
import SyncreticEvolutionAura from '../../../../tactical/statuses/Auras/all/SyncreticEvolution.5010';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { createActionOptions, grantAction, revokeAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import { VehicleRankTree } from '../../../../tactical/units/levels/RankTrees/VehicleRankTree';
import UnitChassis from '../../../types/chassis';
import Weapons from '../../../types/weapons';
import GenericChassis from '../../genericChassis';

export const GenericSupportActions = [
	UnitActions.unitActionTypes.skip,
	UnitActions.unitActionTypes.move,
	UnitActions.unitActionTypes.defend,
	UnitActions.unitActionTypes.attack,
	UnitActions.unitActionTypes.delay,
] as UnitActions.TUnitActionOptions;

export const GenericSupportVehicleActionPhases = 2;

export class GenericSupportVehicle extends GenericChassis {
	public static readonly type = UnitChassis.chassisClass.sup_vehicle;
	public static readonly levelTree = VehicleRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.ground as UnitChassis.movementType;
	public static readonly weaponSlots = [] as Weapons.TWeaponSlotType[];
	public static readonly abilities: number[] = [NegativeSurgeAbility.id, SVSurveyAbility.id];
	public static readonly auras: TAuraApplianceList = [
		{
			id: SyncreticEvolutionAura.id,
		},
	];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.medium] as UnitDefense.TShieldSlotType[];
	public static readonly armorModifier = {} as UnitDefense.TArmorModifier;
	public static readonly armorSlots = [] as UnitDefense.TArmorSlotType[];
	public hitPoints = 55;
	public speed = 9;
	public baseCost = 12;
	public scanRange = 12;
	public dodge = {} as UnitDefense.TDodge;
	public equipmentSlots = 1;
	public actions: UnitActions.TUnitActionOptions[] = revokeAction(
		grantAction(
			createActionOptions(GenericSupportActions, GenericSupportVehicleActionPhases),
			UnitActions.unitActionTypes.delay,
		),
		UnitActions.unitActionTypes.barrage,
		UnitActions.unitActionTypes.attack_move,
	);
}

export default GenericSupportVehicle;

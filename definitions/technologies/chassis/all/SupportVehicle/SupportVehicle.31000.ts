import GenericSupportVehicle from './GenericSupportVehicle';

export class SupportVehicleChassis extends GenericSupportVehicle {
	public static readonly id = 31000;
	public static readonly caption = 'Support Vehicle';
	public static readonly description = `Surveillance and support light vehicle stuffed with all sorts of high-tech devices and an unique Negative Surge Ability`;
}

export default SupportVehicleChassis;

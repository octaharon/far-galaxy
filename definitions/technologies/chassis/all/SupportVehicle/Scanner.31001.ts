import { IUnitEffectModifier } from '../../../../prototypes/types';
import SVCleanseAbility from '../../../../tactical/abilities/all/ScienceVessel/SVCleanseAbility.99905';
import SVSurveyAbility from '../../../../tactical/abilities/all/ScienceVessel/SVSurveyAbility.99910';
import UnitDefense from '../../../../tactical/damage/defense';
import SyncreticEvolutionAura from '../../../../tactical/statuses/Auras/all/SyncreticEvolution.5010';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import { grantAction } from '../../../../tactical/units/actions/ActionManager';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericSupportVehicle from './GenericSupportVehicle';

export class ScannerChassis extends GenericSupportVehicle {
	public static readonly id = 31001;
	public static readonly caption = 'Scanner';
	public static readonly description = `Light surveillance version of Support Vehicle with more cybernetic warfare than ever and a Cloaking device`;
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public static readonly armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public static readonly unitEffectModifier: IUnitEffectModifier = {
		abilityPowerModifier: [
			{
				bias: 0.25,
			},
		],
		abilityDurationModifier: [
			{
				bias: 1,
			},
		],
	};
	public static readonly abilities: number[] = [SVCleanseAbility.id, SVSurveyAbility.id];
	public static readonly auras: TAuraApplianceList = [
		{
			id: SyncreticEvolutionAura.id,
			props: {
				range: 5,
			},
		},
	];
	public hitPoints: number = this.hitPoints - 25;
	public speed: number = this.speed + 3;
	public scanRange: number = this.scanRange + 4;
	public equipmentSlots = 2;
	public actions: UnitActions.TUnitActionOptions[] = grantAction(this.actions, UnitActions.unitActionTypes.cloak);
	public baseCost: number = this.baseCost + 8;
}

export default ScannerChassis;

import UnitChassis from '../types/chassis';

export const isTargetType = (t: any): t is UnitChassis.targetType => Object.values(UnitChassis.targetType).includes(t);

export const isOfTargetType =
	<T extends UnitChassis.targetType | 'default'>(type: T) =>
	(flags: UnitChassis.TTargetFlags): flags is UnitChassis.TOfTargetType<T> => {
		if (isTargetType(type)) {
			return !!flags[type];
		}
		return (
			!isOfTargetType(UnitChassis.targetType.robotic)(flags) &&
			!isOfTargetType(UnitChassis.targetType.organic)(flags)
		);
	};

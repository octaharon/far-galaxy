import BasicMaths from '../../maths/BasicMaths';
import { UnitPartFactory } from '../../tactical/units/UnitPart';
import { getAllFactionIds } from '../../world/factionIndex';
import PoliticalFactions from '../../world/factions';
import UnitChassis, { IChassis, IChassisFactory, IChassisMeta, IChassisStatic, TChassis } from '../types/chassis';

import { TEquipmentProps } from '../types/equipment';
import ChassisList from './index';

const defaultChassisProps: UnitChassis.TChassisProps = {
	baseCost: 0,
	actions: [],
	defenseFlags: {},
	scanRange: 0,
	speed: 0,
	equipmentSlots: 0,
	dodge: {},
	hitPoints: 1,
};

export class ChassisManager
	extends UnitPartFactory<
		UnitChassis.TChassisProps,
		UnitChassis.TSerializedChassis,
		IChassis,
		IChassisMeta,
		IChassisStatic
	>
	implements IChassisFactory
{
	protected static _instance: ChassisManager = null;
	protected __lookupByClass: EnumProxy<UnitChassis.chassisClass, number[]> = {};
	protected __lookupByMovement: EnumProxy<UnitChassis.movementType, number[]> = {};
	protected __lookupByFlags: EnumProxy<UnitChassis.targetType, number[]> = {};
	protected __lookupByFaction: EnumProxy<PoliticalFactions.TFactionID, number[]> = {};

	public instantiate(props: TEquipmentProps = defaultChassisProps, id: number) {
		const c = super.instantiate(props, id);
		if (!c.defenseFlags) c.defenseFlags = {};
		return c;
	}

	public isChassisAvailableToFaction(c: TChassis, f: PoliticalFactions.TFactionID) {
		return f === PoliticalFactions.TFactionID.none || (this.__lookupByFaction[f] || []).includes(c.id);
	}

	public isChassisOfClass(c: TChassis, t: UnitChassis.chassisClass) {
		return c?.type === t;
	}

	public isChassisOfMovementType(c: TChassis, t: UnitChassis.movementType) {
		return c?.movement === t;
	}

	public isChassisOfTargetType(c: TChassis, t: UnitChassis.targetType) {
		return c?.flags?.[t] || false;
	}

	public isAir(chassisId: number): boolean {
		return this.__constructors[chassisId]?.movement === UnitChassis.movementType.air;
	}

	public isGround(chassisId: number): boolean {
		return this.__constructors[chassisId]?.movement === UnitChassis.movementType.ground;
	}

	public isHover(chassisId: number): boolean {
		return this.__constructors[chassisId]?.movement === UnitChassis.movementType.hover;
	}

	public isNaval(chassisId: number): boolean {
		return this.__constructors[chassisId]?.movement === UnitChassis.movementType.naval;
	}

	public isMassive(chassisId: number): boolean {
		return this.isChassisOfTargetType(this.__constructors[chassisId], UnitChassis.targetType.massive);
	}

	public isOrganic(chassisId: number): boolean {
		return this.isChassisOfTargetType(this.__constructors[chassisId], UnitChassis.targetType.organic);
	}

	public isRobotic(chassisId: number): boolean {
		return this.isChassisOfTargetType(this.__constructors[chassisId], UnitChassis.targetType.robotic);
	}

	public isUnique(chassisId: number): boolean {
		return this.isChassisOfTargetType(this.__constructors[chassisId], UnitChassis.targetType.unique);
	}

	public fill() {
		this.__fillItems<TChassis>(
			ChassisList,
			(c) =>
				({
					class: c.type,
				} as IChassisMeta),
			(chassis: TChassis) => {
				this.__lookupByClass[chassis.type] = (this.__lookupByClass[chassis.type] || []).concat(chassis.id);
				this.__lookupByMovement[chassis.movement] = (this.__lookupByMovement[chassis.movement] || []).concat(
					chassis.id,
				);
				Object.keys(chassis.flags).forEach((k: UnitChassis.targetType) => {
					this.__lookupByFlags[k] = (this.__lookupByFlags[k] || []).concat(chassis.id);
				});
				const factions: PoliticalFactions.TFactionID[] = chassis.exclusiveFactions?.length
					? chassis.exclusiveFactions
					: chassis.restrictedFactions?.length
					? getAllFactionIds().filter((v) => !chassis.restrictedFactions.includes(v))
					: getAllFactionIds();
				//console.log(chassis.id, chassis.exclusiveFactions, chassis.restrictedFactions, factions);
				factions.forEach((f: PoliticalFactions.TFactionID) => {
					this.__lookupByFaction[f] = (this.__lookupByFaction[f] || []).concat(chassis.id);
				});
			},
		);
		return this;
	}

	public getAllByChassisClass(type: UnitChassis.chassisClass) {
		return Object.fromEntries(
			Object.entries(this.getAll()).filter(([, chassisConstructor]) =>
				this.isChassisOfClass(chassisConstructor, type),
			),
		);
	}

	public getBaseChassis(c: IChassisStatic): IChassisStatic {
		const type = c.type;
		let id = c.id;
		let i = 0;
		let ch = c;
		const get = (n: number) => {
			try {
				return this.get(n);
			} catch (err) {
				return null;
			}
		};
		while (id && get(id) && get(id).type === type) {
			ch = this.get(id);
			id = BasicMaths.roundToPrecision(c.id, Math.pow(10, ++i));
		}
		return ch;
	}
}

export default ChassisManager;

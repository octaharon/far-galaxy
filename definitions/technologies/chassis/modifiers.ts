import BasicMaths from '../../maths/BasicMaths';
import UnitDefense from '../../tactical/damage/defense';
import DefenseManager from '../../tactical/damage/DefenseManager';
import Weapons from '../types/weapons';
import WeaponGroups from '../weapons/Groups';
import { describeWeaponModifier } from '../weapons/WeaponManager';
import describeModifier = BasicMaths.describeModifier;

export const describeArmorModifier = (a: UnitDefense.TArmorModifier) =>
	a && Object.keys(a).length ? `${DefenseManager.describeProtection(a)} Armor Protection` : '';
export const describeShieldModifier = (a: UnitDefense.TShieldsModifier) => {
	const innerPart: string[] = [];
	if (a?.shieldAmount) innerPart.push(`${describeModifier(a.shieldAmount)} Capacity`);
	if (a?.overchargeDecay) innerPart.push(`${describeModifier(a.overchargeDecay)} Overcharge Decay`);
	if (a?.baseCost) innerPart.push(`${describeModifier(a.baseCost)} Production Cost`);
	const protection = DefenseManager.describeProtection(a).trim();
	const traits = innerPart.map((v) => v.replace(' ', ' Shield ')).join(', ');
	return innerPart.length
		? protection.length
			? `${protection} Shield Protection, ${traits}`
			: `${traits}`.replace(/^\s*;\s*/g, '')
		: protection.length
		? `${protection} Shield Protection`
		: '';
};
export const describeWeaponGroupModifier = (a: Weapons.TWeaponGroupModifier) => {
	if (a.default && Object.keys(a).length === 1) {
		return describeWeaponModifier(a.default, 'All Weapons', true);
	}
	return Object.keys(a)
		.map((weaponGroup) => {
			const groupTitle = WeaponGroups.find((wg) => wg.id === weaponGroup)?.caption ?? 'Other Weapons';
			return describeWeaponModifier(a[weaponGroup], groupTitle, true);
		})
		.join(', ');
};

import UnitAttack from '../../tactical/damage/attack';
import UnitDefense from '../../tactical/damage/defense';
import { createActionOptions } from '../../tactical/units/actions/ActionManager';
import AircraftRankTree from '../../tactical/units/levels/RankTrees/AircraftRankTree';
import UnitChassis from '../types/chassis';
import Weapons from '../types/weapons';
import { GenericAircraftActionPhases, GenericAircraftActions } from './constants';
import { GenericChassis } from './genericChassis';

export class GenericAircraft extends GenericChassis {
	public static readonly levelTree = AircraftRankTree;
	public static readonly flags = {} as UnitChassis.TTargetFlags;
	public static readonly movement = UnitChassis.movementType.air as UnitChassis.movementType;
	public static readonly weaponSlots = [Weapons.TWeaponSlotType.air] as Weapons.TWeaponSlotType[];
	public static readonly shieldSlots = [UnitDefense.TShieldSlotType.small] as UnitDefense.TShieldSlotType[];
	public static readonly weaponModifier: Weapons.TWeaponGroupModifier = {
		[Weapons.TWeaponGroupType.Bombs]: {
			damageModifier: {
				factor: 0.85,
			},
		},
	};
	public static readonly armorModifier = {} as UnitDefense.TArmorModifier;
	public hitPoints = 75;
	public speed = 22;
	public baseCost = 26;
	public scanRange = 7;
	public armorSlots = [UnitDefense.TArmorSlotType.small] as UnitDefense.TArmorSlotType[];
	public dodge = {
		[UnitAttack.deliveryType.ballistic]: 1,
		[UnitAttack.deliveryType.aerial]: -0.25,
		[UnitAttack.deliveryType.supersonic]: 0.5,
		[UnitAttack.deliveryType.surface]: 10,
		[UnitAttack.deliveryType.bombardment]: 10,
	} as UnitDefense.TDodge;
	public actions = createActionOptions(GenericAircraftActions, GenericAircraftActionPhases);
}

export default GenericAircraft;

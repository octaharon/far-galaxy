import _ from 'underscore';
import { UnitPartFactory } from '../../tactical/units/UnitPart';
import {
	IEquipment,
	IEquipmentFactory,
	IEquipmentMeta,
	TEquipment,
	TEquipmentProps,
	TSerializedEquipment,
} from '../types/equipment';
import { EquipmentGroups, TEquipmentGroup, TEquipmentGroupMeta } from './groups';
import EquipmentList, { equipmentIndex } from './index';

export type TEquipmentList = TEquipment[];

export class EquipmentManager
	extends UnitPartFactory<TEquipmentProps, TSerializedEquipment, IEquipment, IEquipmentMeta>
	implements IEquipmentFactory
{
	protected static _instance: EquipmentManager = null;
	protected __groups: Record<TEquipmentGroup, number[]> = null;

	public getEquipmentWithAbilities(usable: boolean): TEquipmentList {
		return Object.values(this.__constructors as TEquipment[]).filter(
			(v) => usable !== (!v.abilities || v.abilities.length === 0),
		);
	}

	getGroups(): TEquipmentGroup[] {
		return Object.keys(this.__groups);
	}

	getByGroup(groupId: TEquipmentGroup): number[] {
		return this.__groups[groupId] || [];
	}

	getGroupById(equipmentId: number): TEquipmentGroup {
		return this.getItemMeta(equipmentId)?.group;
	}

	getGroupMeta(groupId: TEquipmentGroup): TEquipmentGroupMeta {
		return (
			EquipmentGroups[groupId] || {
				caption: 'Equipment',
				description:
					" allows to install various hi-tech devices and augmentations that permanently alter tUnit's properties, including Weapon, Armor, Shields or even yielding it permanent Effects, Auras and Abilities",
			}
		);
	}

	public fill() {
		this.__groups = _.mapObject(equipmentIndex, (v, g: TEquipmentGroup) =>
			v.map((t) => {
				this.__lookupById[t.id] = {
					group: g,
				};
				return t.id;
			}),
		);
		this.__fillItems(EquipmentList, (e) => this.__lookupById[e.id]);
		return this;
	}
}

export default EquipmentManager;

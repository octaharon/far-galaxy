import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitDefense from '../../../../tactical/damage/defense';
import FrictionlessEffect from '../../../../tactical/statuses/Effects/all/Unique/Frictionless.37011';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class CryostasisGridEquipment extends GenericEquipment {
	public static readonly id = 20026;
	public static readonly caption = 'Cryostasis Grid';
	public static readonly description =
		'External supercooled coil generator can significantly improve all magnetic devices, when connected to Chassis. Grants SMA Module and a Frictionless effect to the Unit, and its Mass Drivers and Plasma Weapons receive +0.2 Attack Rate and +1 Attack Range';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.sma]: true,
				},
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Railguns]: {
					baseRange: {
						bias: 1,
					},
					baseRate: {
						bias: 0.2,
					},
				},
				[Weapons.TWeaponGroupType.Plasma]: {
					baseRange: {
						bias: 1,
					},
					baseRate: {
						bias: 0.2,
					},
				},
			},
		],
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: FrictionlessEffect.id,
		},
	];
	public baseCost = 37;
}

export default CryostasisGridEquipment;

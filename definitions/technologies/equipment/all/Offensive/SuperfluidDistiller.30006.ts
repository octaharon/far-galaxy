import { IPrototypeModifier } from '../../../../prototypes/types';
import DissipateHeatAbility from '../../../../tactical/abilities/all/ElectronicWarfare/DissipateHeat.50009';
import UnitDefense from '../../../../tactical/damage/defense';
import { AbilityPowerUIToken } from '../../../../tactical/statuses/constants';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class SuperfluidDistillerEquipment extends GenericEquipment {
	public static readonly id = 30006;
	public static readonly caption = 'Superfluid Distiller';
	public static readonly description = `Top notch commercial subspace condenser is essentialy an SMA Module that produces tiny amounts of frictionless coolant, which improve most electromechanical systems of any Chassis for extra 10% Unit Cost. Guns and Anti-Air Weapons gain +25% Attack Rate, while the Unit itself is granted +25% ${AbilityPowerUIToken}.`;
	public static readonly abilities: number[] = [DissipateHeatAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Guns]: {
					baseRate: {
						bias: 0.5,
					},
				},
				[Weapons.TWeaponGroupType.AntiAir]: {
					baseRate: {
						bias: 0.25,
					},
				},
			},
		],
		unitModifier: {
			abilityPowerModifier: [
				{
					bias: 0.25,
				},
			],
		},
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.sma]: true,
				},
				baseCost: {
					factor: 1.1,
				},
			},
		],
	};

	public baseCost = 35;
}

export default SuperfluidDistillerEquipment;

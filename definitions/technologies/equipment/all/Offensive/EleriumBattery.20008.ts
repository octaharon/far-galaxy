import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class EleriumBatteryEquipment extends GenericEquipment {
	public static readonly id = 20008;
	public static readonly caption = 'Elerium Battery';
	public static readonly description =
		'Intricate MHD converter that recuperates dissipated energy from connected ion generators, namely Plasma Weapons, which gain +1 Range and 50% Hit Chance. Obtained power is used for Self-Restoring Shields and +2 Scan Range';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.replenishing]: true,
				},
				scanRange: {
					bias: 2,
				},
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Plasma]: {
					baseAccuracy: {
						bias: 0.5,
					},
					baseRange: {
						bias: 1,
					},
				},
			},
		],
	};
	public baseCost = 30;
}

export default EleriumBatteryEquipment;

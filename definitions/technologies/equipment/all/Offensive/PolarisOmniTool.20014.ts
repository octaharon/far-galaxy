import { IPrototypeModifier } from '../../../../prototypes/types';
import GenericEquipment from '../../GenericEquipment';

export class PolarisOmniTool extends GenericEquipment {
	public static readonly id = 20014;
	public static readonly caption = 'Polaris Omni-Tool';
	public static readonly description =
		'Multipurpose sentient support robot, that seamlessly integrates into combat control circuits of any Chassis, providing an extra Action Phase, +15 Shield Capacity, +1 Scan Range and +10% Attack Rate for all attached Weapons';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				scanRange: {
					bias: 1,
				},
				actionCount: 1,
			},
		],
		shield: [
			{
				shieldAmount: {
					bias: 15,
				},
			},
		],
		weapon: [
			{
				default: {
					baseRate: {
						factor: 1.1,
					},
				},
			},
		],
	};
	public baseCost = 42;
}

export default PolarisOmniTool;

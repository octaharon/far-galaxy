import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class GravitonCatapultEquipment extends GenericEquipment {
	public static readonly id = 20021;
	public static readonly caption = 'Graviton Catapult';
	public static readonly description =
		'Externally connected acceleration device can grant few extra moves here and there, allowing the unit to use Barrage, Attack/Use and Intercept on any Action Phase. It also connects to small caliber barrels, like Launchers and Anti-Air Weapons, increasing their Range by 2 and Hit Chance - by 25%';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				actions: [
					UnitActions.unitActionTypes.intercept,
					UnitActions.unitActionTypes.barrage,
					UnitActions.unitActionTypes.attack,
				],
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Launchers]: {
					baseRange: {
						bias: 2,
					},
					baseAccuracy: {
						bias: 0.25,
					},
				},
				[Weapons.TWeaponGroupType.AntiAir]: {
					baseRange: {
						bias: 2,
					},
					baseAccuracy: {
						bias: 0.25,
					},
				},
			},
		],
	};
	public baseCost = 29;
}

export default GravitonCatapultEquipment;

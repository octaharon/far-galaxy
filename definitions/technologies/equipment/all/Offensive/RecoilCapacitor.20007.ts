import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class RecoilCapacitorEquipment extends GenericEquipment {
	public static readonly id = 20007;
	public static readonly caption = 'Recoil Capacitor';
	public static readonly description =
		'Coiled higgs battery, while can be used as Shield Boosters, also recuperates dissipated kinetic energy of Chassis. When connected to Guns, Cannons and Mass Drivers,  grants +10% Damage and +40% Hit Chance.';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.boosters]: true,
				},
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Guns]: {
					damageModifier: {
						factor: 1.1,
					},
					baseAccuracy: {
						bias: 0.4,
					},
				},
				[Weapons.TWeaponGroupType.Cannons]: {
					damageModifier: {
						factor: 1.1,
					},
					baseAccuracy: {
						bias: 0.4,
					},
				},
				[Weapons.TWeaponGroupType.Railguns]: {
					damageModifier: {
						factor: 1.1,
					},
					baseAccuracy: {
						bias: 0.4,
					},
				},
			},
		],
	};
	public baseCost = 32;
}

export default RecoilCapacitorEquipment;

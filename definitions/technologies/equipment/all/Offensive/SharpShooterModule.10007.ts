import SharpshooterStatusEffect from '../../../../tactical/statuses/Effects/all/Guidance/SharpShooter.02001';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class SharpShooterModule extends GenericEquipment {
	public static readonly id = 10007;
	public static readonly caption = 'Sharpshooter Imprint';
	public static readonly description =
		'Cybernetic implant with best shooting practices and training, compiled into a single engram, adaptable for any Chassis. Increases Hit Chance every Turn while the bearer is alive and Deployed';
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: SharpshooterStatusEffect.id,
		},
	];
	public baseCost = 20;
}

export default SharpShooterModule;

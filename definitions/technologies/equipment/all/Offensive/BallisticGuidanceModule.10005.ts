import BasicMaths from '../../../../maths/BasicMaths';
import { IPrototypeModifier } from '../../../../prototypes/types';
import BallisticGuidanceAbility from '../../../../tactical/abilities/all/GuidanceAbilities/BallisticGuidanceAbility.10001';
import GenericEquipment from '../../GenericEquipment';

const BallisticGuidancePrototypeModifier: IPrototypeModifier = {
	weapon: [
		{
			default: {
				baseAccuracy: {
					bias: 0.25,
				},
			},
		},
	],
};

export class BallisticGuidanceModule extends GenericEquipment {
	public static readonly id = 10005;
	public static readonly caption = 'Ballistic Guide';
	public static readonly description = `Supercharged targeting computer that adds ${BasicMaths.describePercentileModifier(
		BallisticGuidancePrototypeModifier.weapon[0].default.baseAccuracy,
	)} Hit Chance for all Weapons and allows Chassis to continuously stack Hit Chance and Attack Range`;
	public static readonly abilities: number[] = [BallisticGuidanceAbility.id];
	public static readonly prototypeModifier = BallisticGuidancePrototypeModifier;
	public baseCost = 31;
}

export default BallisticGuidanceModule;

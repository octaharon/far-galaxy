import { IPrototypeModifier } from '../../../../prototypes/types';
import ShutdownAbility      from '../../../../tactical/abilities/all/ElectronicWarfare/Shutdown.50006';
import ElectrocutionAbility from '../../../../tactical/abilities/all/Bionic/Electrocute.37000';
import UnitAttack           from '../../../../tactical/damage/attack';
import GenericEquipment from '../../GenericEquipment';

export class OverloadEquipment extends GenericEquipment {
	public static readonly id = 30001;
	public static readonly caption = 'Overload Compensator';
	public static readonly description =
		'High density precision capacitor absorbs gigawatts of electromagnetic energy, reducing Unit Ability Cooldowns by 1 and increasing Dodge against Beam Weapons by +35%, and then can release the charge at foes with powerful Abilities.';
	public static readonly abilities: number[] = [ShutdownAbility.id, ElectrocutionAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				dodge: {
					[UnitAttack.deliveryType.beam]: {
						bias: 0.35,
					},
				},
			},
		],
		unitModifier: {
			abilityCooldownModifier: [
				{
					bias: -1,
					minGuard: 1,
					min: 1,
				},
			],
		},
	};
	public baseCost = 40;
}

export default OverloadEquipment;

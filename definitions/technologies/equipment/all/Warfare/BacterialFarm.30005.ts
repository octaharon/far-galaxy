import { IPrototypeModifier } from '../../../../prototypes/types';
import InfestAbility from '../../../../tactical/abilities/all/Bionic/Infest.12001';
import MildewAbility from '../../../../tactical/abilities/all/ElectronicWarfare/Mildew.50008';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericEquipment from '../../GenericEquipment';

export class BacterialFarmEquipment extends GenericEquipment {
	public static readonly id = 30005;
	public static readonly caption = 'Bacterial Farm';
	public static readonly description =
		'A bioreactor purposed for blazing fast reproduction of designer bacteria, which, with certain non-trivial engineering, are used to perform offensive Abilities and to provide a Chassis with Resilient Defense';
	public static readonly abilities: number[] = [MildewAbility.id, InfestAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.resilient]: true,
				},
			},
		],
	};

	public baseCost = 34;
}

export default BacterialFarmEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import BulletStormAbility from '../../../../tactical/abilities/all/ElectronicWarfare/BulletStorm.50011';
import SnipeShotAbility from '../../../../tactical/abilities/all/ElectronicWarfare/SnipeShot.50012';
import UnitAttack from '../../../../tactical/damage/attack';
import GenericEquipment from '../../GenericEquipment';

export class AutomatedTurretEquipment extends GenericEquipment {
	public static readonly id = 30004;
	public static readonly caption = 'Automated Turret';
	public static readonly description =
		'Sidekick weapon installed into an Equipment slot, capable of shooting bullets at multiple targets with formidable precision, thus adding offensive Abilities to the Chassis and increasing its Dodge against Ballistic, Missile and Bombardment Attacks by +50%';
	public static readonly abilities: number[] = [SnipeShotAbility.id, BulletStormAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				dodge: {
					[UnitAttack.deliveryType.ballistic]: {
						bias: 0.5,
					},
					[UnitAttack.deliveryType.missile]: {
						bias: 0.5,
					},
					[UnitAttack.deliveryType.bombardment]: {
						bias: 0.5,
					},
				},
			},
		],
	};

	public baseCost = 32;
}

export default AutomatedTurretEquipment;

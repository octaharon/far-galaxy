import { IPrototypeModifier } from '../../../../prototypes/types';
import HadronDestructionAbility from '../../../../tactical/abilities/all/SelfDestruct/HadronDestruction.25002';
import Damage from '../../../../tactical/damage/damage';
import AnnihilationDoomsdayDeviceEffect from '../../../../tactical/statuses/Effects/all/SelfDestruct/ANHDoomsdayDevice.12501';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class SuicideDeviceEquipment extends GenericEquipment {
	public static readonly id = 30003;
	public static readonly caption = 'Hadron Inverter';
	public static readonly description =
		'A portable spin inversion device that provides additional ANH Shield Protection. Explodes with 450 ANH Damage in 2.5 Radius when overloaded or destroyed, leaving no salvage behind.';
	public static readonly abilities: number[] = [HadronDestructionAbility.id];
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: AnnihilationDoomsdayDeviceEffect.id,
		},
	];
	public static readonly prototypeModifier: IPrototypeModifier = {
		shield: [
			{
				[Damage.damageType.antimatter]: {
					factor: 0.8,
				},
			},
		],
	};

	public baseCost = 31;
}

export default SuicideDeviceEquipment;

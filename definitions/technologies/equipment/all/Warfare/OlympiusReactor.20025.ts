import { IPrototypeModifier } from '../../../../prototypes/types';
import MoltenCoreAbility from '../../../../tactical/abilities/all/SelfDestruct/MoltenCore.25000';
import UnitDefense from '../../../../tactical/damage/defense';
import MoltenCoreEffect from '../../../../tactical/statuses/Effects/all/SelfDestruct/MoltenCore.12502';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class OlympiusReactorEquipment extends GenericEquipment {
	public static readonly id = 20025;
	public static readonly caption = 'Olympius Reactor';
	public static readonly description =
		'High energy subspace pump provides the bearer with Shield Boosters and +2 Speed. The reactor is quite sensitive to running conditions and explodes on Unit death, or can be set off manually at any time';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.boosters]: true,
				},
				speed: {
					bias: 2,
					minGuard: 0.1,
				},
			},
		],
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: MoltenCoreEffect.id,
		},
	];
	public static readonly abilities: number[] = [MoltenCoreAbility.id];

	public baseCost = 36;
}

export default OlympiusReactorEquipment;

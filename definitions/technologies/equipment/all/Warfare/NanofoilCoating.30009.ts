import { IPrototypeModifier } from '../../../../prototypes/types';
import ChargeLeakageAbility from '../../../../tactical/abilities/all/ElectronicWarfare/ChargeLeakage.50014';
import UnstableConnectionAbility from '../../../../tactical/abilities/all/ElectronicWarfare/UnstableConnection.50015';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericEquipment from '../../GenericEquipment';

export class NanofoilCoatingEquipment extends GenericEquipment {
	public static readonly id = 30009;
	public static readonly caption = 'Nanofoil Coating';
	public static readonly description =
		'Top-tech semiconductor material infusion for Armor, that renders it with properties of Hull Module and provides it with +1 Effect Resistance. The hull gathers enough energy to manipulate the electric charge of nearby conductors with Abilities';
	public static readonly abilities: number[] = [ChargeLeakageAbility.id, UnstableConnectionAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.hull]: true,
				},
			},
		],
		unitModifier: {
			statusDurationModifier: [
				{
					bias: -1,
				},
			],
		},
	};
	public baseCost = 37;
}

export default NanofoilCoatingEquipment;

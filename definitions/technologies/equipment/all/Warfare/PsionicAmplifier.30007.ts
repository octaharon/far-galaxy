import { IPrototypeModifier } from '../../../../prototypes/types';
import BurnoutAbility from '../../../../tactical/abilities/all/ElectronicWarfare/Burnout.50017';
import PsionicBladesAbility from '../../../../tactical/abilities/all/ElectronicWarfare/PsionicBlades.50007';
import Damage from '../../../../tactical/damage/damage';
import { AbilityPowerUIToken } from '../../../../tactical/statuses/constants';
import GenericEquipment from '../../GenericEquipment';

export class PsionicAmplifierEquipment extends GenericEquipment {
	public static readonly id = 30007;
	public static readonly caption = 'Psionic Amplifier';
	public static readonly description = `Experimental device than tinkers with the very essence of matter and spacetime and lets even support units deliver some destruction with Psionic Blades and/or Burnout Abilities. Also grants +0.5 ${AbilityPowerUIToken} and +15% Armor Damage Reduction versus WRP, ANH and EMG`;
	public static readonly abilities: number[] = [PsionicBladesAbility.id, BurnoutAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		armor: [
			{
				[Damage.damageType.warp]: {
					factor: 0.85,
				},
				[Damage.damageType.antimatter]: {
					factor: 0.85,
				},
				[Damage.damageType.magnetic]: {
					factor: 0.85,
				},
			},
		],
		unitModifier: {
			abilityPowerModifier: [
				{
					bias: 0.5,
				},
			],
		},
	};

	public baseCost = 39;
}

export default PsionicAmplifierEquipment;

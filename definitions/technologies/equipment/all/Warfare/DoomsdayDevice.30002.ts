import DoomsdayDeviceAbility from '../../../../tactical/abilities/all/SelfDestruct/DoomsdayDeviceAbility.25001';
import DoomsdayDeviceEffect from '../../../../tactical/statuses/Effects/all/SelfDestruct/DoomsdayDevice.12500';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class DoomsdayDeviceEquipment extends GenericEquipment {
	public static readonly id = 30002;
	public static readonly caption = 'Doomsday Device';
	public static readonly description =
		'Creates a movable munition out of the Unit with a massive charge, that explodes for 200 HEA and TRM Damage in 2.5 Radius, leaving no technologies in the Salvage. The detonator is plugged into vitals monitor and is triggered either when the Unit is destroyed, or manually.';
	public static readonly abilities: number[] = [DoomsdayDeviceAbility.id];
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: DoomsdayDeviceEffect.id,
		},
	];

	public baseCost = 24;
}

export default DoomsdayDeviceEquipment;

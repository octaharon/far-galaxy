import { IPrototypeModifier } from '../../../../prototypes/types';
import SuddenStrikeAbility from '../../../../tactical/abilities/all/ElectronicWarfare/SuddenStrike.50010';
import LeadershipAura from '../../../../tactical/statuses/Auras/all/Leadership.5006';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericEquipment from '../../GenericEquipment';

export class MobileHQEquipment extends GenericEquipment {
	public static readonly id = 30008;
	public static readonly caption = 'Mobile HQ';
	public static readonly description =
		'Double sentient strategic computers, one of which is installed on a Chassis, providing for offensive support and fire coordination. Another is a modern endurance drone that provides extra vision and can drop smaller munitions. Grants Sudden Strike Ability, Delay Action in all phases, +1 Scan Range, and also increases Damage of nearby Units by 10%.';
	public static readonly abilities: number[] = [SuddenStrikeAbility.id];
	public static readonly auras: TAuraApplianceList = [
		{
			id: LeadershipAura.id,
		},
	];
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				scanRange: {
					bias: 1,
				},
				actions: [UnitActions.unitActionTypes.delay],
			},
		],
	};

	public baseCost = 38;
}

export default MobileHQEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import DefenseNetworkAura from '../../../../tactical/statuses/Auras/all/DefenseNetwork.5005';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericEquipment from '../../GenericEquipment';

export class DefenseNetworkEquipment extends GenericEquipment {
	public static readonly id = 10008;
	public static readonly caption = 'Defense Network';
	public static readonly description =
		'Almost prehistorical by this day, this tactical processor can reconnect orbital power channels and is still widely used by smaller squads, merging them into a sustainable security force. Provides Retaliate and Protect Actions, as well as +1 Effect Resistance. Grants +10% Shield Capacity to all nearby Friendly Units and restores 20% of those Shields every Turn End.';
	public static readonly auras: TAuraApplianceList = [
		{
			id: DefenseNetworkAura.id,
		},
	];
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			statusDurationModifier: [{ bias: -1, minGuard: 1, min: 1 }],
		},
		chassis: [
			{
				actions: [UnitActions.unitActionTypes.retaliate, UnitActions.unitActionTypes.protect],
			},
		],
	};
	public baseCost = 30;
}

export default DefenseNetworkEquipment;

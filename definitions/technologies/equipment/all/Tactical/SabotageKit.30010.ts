import { IPrototypeModifier } from '../../../../prototypes/types';
import PanicAttackAbility from '../../../../tactical/abilities/all/ElectronicWarfare/PanicAttack.50013';
import CamouflageStatusEffect from '../../../../tactical/statuses/Effects/all/ElectronicWarfare/Camouflage.03008';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class SabotageKitEquipment extends GenericEquipment {
	public static readonly id = 30010;
	public static readonly caption = 'Sabotage Kit';
	public static readonly description =
		'An oldie but goodie: a microcontroller package that contains versatile camouflaging, diversion and ambushing tactical programs, including malicious psyops viruses. Gives a Unit an edge in special operations, granting Suppress Action in all Action Phases and increasing the Attack Range of Guns and Launchers by +1.';
	public static readonly abilities: number[] = [PanicAttackAbility.id];
	public static readonly effects: TEffectApplianceList = [{ effectId: CamouflageStatusEffect.id }];

	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				actions: [UnitActions.unitActionTypes.suppress],
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Guns]: {
					baseRange: {
						bias: 1,
					},
				},
				[Weapons.TWeaponGroupType.Launchers]: {
					baseRange: {
						bias: 1,
					},
				},
			},
		],
	};
	public baseCost = 25;
}

export default SabotageKitEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitAttack from '../../../../tactical/damage/attack';
import DroneJammerAura from '../../../../tactical/statuses/Auras/all/DroneJammer.5004';
import { TAuraApplianceList } from '../../../../tactical/statuses/Auras/types';
import GenericEquipment from '../../GenericEquipment';

export class DroneJammerEquipment extends GenericEquipment {
	public static readonly id = 20001;
	public static readonly caption = 'Drone Jammer';
	public static readonly description =
		'High power targeted signal suppressor that provides +100% Dodge vs Drone to Units within 5 Radius. Additional scanning power complements Chassis innate capabilities, providing +200% Dodge vs Drone and +10% Scan Range';
	public static readonly auras: TAuraApplianceList = [
		{
			id: DroneJammerAura.id,
		},
	];
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				dodge: {
					[UnitAttack.deliveryType.drone]: {
						bias: 2,
					},
				},
				scanRange: {
					factor: 1.1,
				},
			},
		],
	};
	public baseCost = 28;
}

export default DroneJammerEquipment;

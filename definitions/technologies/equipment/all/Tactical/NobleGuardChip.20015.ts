import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericEquipment from '../../GenericEquipment';

export class NobleGuardChipEquipment extends GenericEquipment {
	public static readonly id = 20015;
	public static readonly caption = 'Noble Guard Chip';
	public static readonly description =
		'Being the only product of a militech startup, this advance ballistic computer implements exceptional countermeasures against projectiles, effectively being a Reactive Defense Module, which also Limits incoming KIN Damage to the Shields by 35 and allows to perform Protect Action.';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				actions: [UnitActions.unitActionTypes.protect],
				defenseFlags: {
					[UnitDefense.defenseFlags.reactive]: true,
				},
			},
		],
		shield: [
			{
				[Damage.damageType.kinetic]: {
					max: 35,
				},
			},
		],
	};
	public baseCost = 22;
}

export default NobleGuardChipEquipment;

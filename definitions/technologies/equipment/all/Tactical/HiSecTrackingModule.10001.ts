import { IPrototypeModifier } from '../../../../prototypes/types';
import HiSecTrackingStatusEffect from '../../../../tactical/statuses/Effects/all/Guidance/HiSecTracking.02006';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class HiSecTrackingModuleEquipment extends GenericEquipment {
	public static readonly id = 10001;
	public static readonly caption = 'Hi-Sec Tracker';
	public static readonly description =
		'Based on commercial hunting equipment, with upgraded chipset this device grants gets +15% Dodge, +1 Attack Range and Ability Range to the Unit. Additionally, its Weapons get +3% Hit Chance with every successful Attack';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				dodge: {
					default: {
						bias: 0.15,
					},
				},
			},
		],
		weapon: [
			{
				default: {
					baseRange: {
						bias: 1,
					},
				},
			},
		],
		unitModifier: {
			abilityRangeModifier: [
				{
					bias: 1,
				},
			],
		},
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: HiSecTrackingStatusEffect.id,
		},
	];
	public baseCost = 34;
}

export default HiSecTrackingModuleEquipment;

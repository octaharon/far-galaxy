import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitAttack from '../../../../tactical/damage/attack';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class SimulationMatrixEquipment extends GenericEquipment {
	public static readonly id = 20005;
	public static readonly caption = 'Simulation Matrix';
	public static readonly description =
		'A crystal ball of 4th millennium, a quark computer which can sometimes predict the future by plotting the exact plan, leading to it. Grants an extra Action Phase and +70% Dodge vs Ballistic, Missile and Aerial, and Homing, Cruise Missiles and Rocket Weapons get +1.5 Attack Range.';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				actionCount: 1,
				dodge: {
					[UnitAttack.deliveryType.missile]: {
						bias: 0.7,
					},
					[UnitAttack.deliveryType.ballistic]: {
						bias: 0.7,
					},
					[UnitAttack.deliveryType.aerial]: {
						bias: 0.7,
					},
				},
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Missiles]: {
					baseRange: {
						bias: 1.5,
					},
				},
				[Weapons.TWeaponGroupType.Cruise]: {
					baseRange: {
						bias: 1.5,
					},
				},
				[Weapons.TWeaponGroupType.Rockets]: {
					baseRange: {
						bias: 1.5,
					},
				},
			},
		],
	};
	public baseCost = 34;
}

export default SimulationMatrixEquipment;

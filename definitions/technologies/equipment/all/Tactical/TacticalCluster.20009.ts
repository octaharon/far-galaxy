import { IPrototypeModifier } from '../../../../prototypes/types';
import { AbilityPowerUIToken } from '../../../../tactical/statuses/constants';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericEquipment from '../../GenericEquipment';

export class TacticalClusterEquipment extends GenericEquipment {
	public static readonly id = 20009;
	public static readonly caption = 'Tactical Supercluster';
	public static readonly description = `This cheap quantum predictor is packed with handpicked battle algorithms and can be fitted to any Chassis, improving it with Protect, Intercept, Suppress, Retaliate and Delay actions on any phase, as well as additional +15% ${AbilityPowerUIToken}`;
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			abilityPowerModifier: [
				{
					factor: 1.15,
				},
			],
		},
		chassis: [
			{
				actions: [
					UnitActions.unitActionTypes.protect,
					UnitActions.unitActionTypes.intercept,
					UnitActions.unitActionTypes.suppress,
					UnitActions.unitActionTypes.retaliate,
					UnitActions.unitActionTypes.delay,
				],
			},
		],
	};
	public baseCost = 26;
}

export default TacticalClusterEquipment;

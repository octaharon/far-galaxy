import { IPrototypeModifier } from '../../../../prototypes/types';
import FortitudeStatusEffect from '../../../../tactical/statuses/Effects/all/Unique/Fortitude.37013';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class TensorCoprocessorEquipment extends GenericEquipment {
	public static readonly id = 20024;
	public static readonly caption = 'Tensor Coprocessor';
	public static readonly description =
		'A bunch of extra vector computation capacity never hurts, especially against computer warfare. The Unit gets +1 Effect Penetration, gains XP when targeted by an Ability, and stacks Dodge when moving';
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: FortitudeStatusEffect.id,
		},
	];
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			abilityDurationModifier: [
				{
					bias: 1,
				},
			],
		},
	};
	public baseCost = 36;
}

export default TensorCoprocessorEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import NaniteRayAbility from '../../../../tactical/abilities/all/ElectronicWarfare/NaniteRay.50018';
import ShutdownAbility         from '../../../../tactical/abilities/all/ElectronicWarfare/Shutdown.50006';
import ElectrocutionAbility    from '../../../../tactical/abilities/all/Bionic/Electrocute.37000';
import { AbilityPowerUIToken } from '../../../../tactical/statuses/constants';
import RansomwareEffect from '../../../../tactical/statuses/Effects/all/SelfDestruct/Ransomware.12503';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class MetacongitionInterfaceEquipment extends GenericEquipment {
	public static readonly id = 20030;
	public static readonly caption = 'Metacognition Interface';
	public static readonly description = `Computer-controlled nanite cloud designed for AR-augmented exoplanet exploration. Grants +75% ${AbilityPowerUIToken}, allows to restore Hit Points to other Units, and makes this unit's Salvage harmful for the collector`;
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			abilityPowerModifier: [
				{
					factor: 1.75,
				},
			],
		},
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: RansomwareEffect.id,
		},
	];
	public static readonly abilities: number[] = [NaniteRayAbility.id];
	public baseCost = 32;
}

export default MetacongitionInterfaceEquipment;

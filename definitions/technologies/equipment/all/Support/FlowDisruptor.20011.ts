import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericEquipment from '../../GenericEquipment';

export class FlowDisruptorEquipment extends GenericEquipment {
	public static readonly id = 20011;
	public static readonly caption = 'Flow Disruptor';
	public static readonly description =
		'Extravagant device of unknown origin, designed to displace and dissipate high-energy streams of particles. Allows for all Shields to be Overcharged with Decay of 5, grants additional 25% RAD Shield Protection, but reduces Shield Capacity by 10%. Also grants +10% Ability Power, +1 Effect Resistance and +50% Dodge against Beam attacks';
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			abilityPowerModifier: [
				{
					factor: 1.1,
				},
			],
			statusDurationModifier: [
				{
					bias: -1,
					min: 0,
					minGuard: 0,
				},
			],
		},
		chassis: [
			{
				dodge: {
					[UnitAttack.deliveryType.beam]: {
						bias: 0.5,
					},
				},
			},
		],
		shield: [
			{
				[Damage.damageType.radiation]: {
					factor: 0.75,
				},
				overchargeDecay: {
					min: 5,
					bias: Infinity,
					max: 5,
				},
				shieldAmount: {
					factor: 0.9,
				},
			},
		],
	};
	public baseCost = 29;
}

export default FlowDisruptorEquipment;

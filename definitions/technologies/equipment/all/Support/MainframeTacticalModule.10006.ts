import { IPrototypeModifier } from '../../../../prototypes/types';
import MainframeGuidanceAbility from '../../../../tactical/abilities/all/GuidanceAbilities/MainFrameGuidanceAbility.10004';
import GenericEquipment from '../../GenericEquipment';

export class MainframeTacticalModule extends GenericEquipment {
	public static readonly id = 10006;
	public static readonly caption = 'Mainframe Datacore';
	public static readonly description =
		'Additional onboard supercomputer processes additional amounts of intelligence data and can significantly reduce computational overload of friendly Units, expanding their combat capabilities. Grants +4 Scan Range and Mainframe Guidance Ability';
	public static readonly abilities: number[] = [MainframeGuidanceAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				scanRange: {
					bias: 4,
				},
			},
		],
	};
	public baseCost = 33;
}

export default MainframeTacticalModule;

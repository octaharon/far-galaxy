import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import GroundingStatusEffect from '../../../../tactical/statuses/Effects/all/Guidance/GroundingEffect.02009';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class GroundingDeviceEquipment extends GenericEquipment {
	public static readonly id = 20023;
	public static readonly caption = 'Grounding Device';
	public static readonly description =
		'A lightning rod: after millenia of engineering science now comes in the simplest yet very efficient design, capable of shortcutting energy streams where needed most, decreasing Ability Cooldowns by 1 Turn every time the Unit itself is targeted with an Ability. The Unit also gains +1 Effect Penetration and +10% EMG Armor Protection.';
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			abilityDurationModifier: [
				{
					bias: 1,
					minGuard: 1,
				},
			],
		},
		armor: [
			{
				[Damage.damageType.magnetic]: {
					factor: 0.9,
				},
			},
		],
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: GroundingStatusEffect.id,
		},
	];
	public baseCost = 24;
}

export default GroundingDeviceEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import OrbitalTargetingAbility from '../../../../tactical/abilities/all/GuidanceAbilities/OrbitalGuidanceAbility.10003';
import GenericEquipment from '../../GenericEquipment';

export class OrbitalSupportModuleEquipment extends GenericEquipment {
	public static readonly id = 10003;
	public static readonly caption = 'Orbital Uplink';
	public static readonly description =
		'Relays control over the Unit to orbital command, that can channel additional power to combat systems. Provides +0.25 Ability Power, +10% Dodge and an Ability to increase Hit Chance and Attack Range on any unit';
	public static readonly abilities: number[] = [OrbitalTargetingAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			abilityPowerModifier: [{ bias: 0.25 }],
		},
		chassis: [
			{
				dodge: {
					default: {
						bias: 0.1,
					},
				},
			},
		],
	};
	public baseCost = 32;
}

export default OrbitalSupportModuleEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import SubspaceHysteresisAbility from '../../../../tactical/abilities/all/ElectronicWarfare/SubspaceHysteresis.50016';
import Damage from '../../../../tactical/damage/damage';
import GenericEquipment from '../../GenericEquipment';

export class HiggsPumpEquipment extends GenericEquipment {
	public static readonly id = 30011;
	public static readonly caption = 'Higgs Pump';
	public static readonly description =
		'Unmatched military technology that meddles with timespace itself and puts the bearer one step ahead of competitors. Grants an extra Action Phase, Subspace Hysteresis Ability and 50 WRP Threshold on Armor';
	public static readonly abilities: number[] = [SubspaceHysteresisAbility.id];
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				actionCount: 1,
			},
		],
		armor: [
			{
				[Damage.damageType.warp]: {
					threshold: 50,
				},
			},
		],
	};
	public baseCost = 36;
}

export default HiggsPumpEquipment;

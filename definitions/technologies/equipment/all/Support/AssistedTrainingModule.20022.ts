import { IPrototypeModifier } from '../../../../prototypes/types';
import AssistedTrainingEffect from '../../../../tactical/statuses/Effects/all/Guidance/AssistedTraining.02008';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class AssistedTrainingModuleEquipment extends GenericEquipment {
	public static readonly id = 20022;
	public static readonly caption = 'Assisted Training Module';
	public static readonly description = `Tactical AI simulates various combat conditions and feedbacks into control chains of the Chassis, providing 0.25 Ability Power and reduce Effect Duration of incoming Effects by 1 Turn. ${AssistedTrainingEffect.description}`;
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			abilityPowerModifier: [
				{
					bias: 0.25,
				},
			],
			statusDurationModifier: [
				{
					bias: -1,
					min: 1,
					minGuard: 1,
				},
			],
		},
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: AssistedTrainingEffect.id,
		},
	];
	public baseCost = 37;
}

export default AssistedTrainingModuleEquipment;

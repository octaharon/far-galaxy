import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitDefense from '../../../../tactical/damage/defense';
import UnitActions from '../../../../tactical/units/actions/types';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class WarpGeneratorEquipment extends GenericEquipment {
	public static readonly id = 20004;
	public static readonly caption = 'Warp Generator';
	public static readonly description =
		'The most common type of a portable Mass Nullifier that is capable of producing localized gravity anomalies, providing the Chassis with Warp Defense and a Warp Action on any Action Phase. It can also be interconnected with most similar military devices, yielding +10% Damage for Warp Weapons then';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.warp]: true,
				},
				actions: [UnitActions.unitActionTypes.warp],
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Warp]: {
					damageModifier: {
						factor: 1.1,
					},
				},
			},
		],
	};
	public baseCost = 29;
}

export default WarpGeneratorEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import GenericEquipment from '../../GenericEquipment';

export class BioticAmpEquipment extends GenericEquipment {
	public static readonly id = 20013;
	public static readonly caption = 'Biotic Amp';
	public static readonly description =
		'Originating somewhere in Venatician labs, this device utilizes energy, produced by a synthetic bacteria, and channels it to the Chassis, providing +15 Hit Points, +30% BIO Damage Reduction on Armor and +5% Damage on all Weapons';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				hitPoints: {
					bias: 15,
				},
			},
		],
		armor: [
			{
				[Damage.damageType.biological]: {
					factor: 0.7,
				},
			},
		],
		weapon: [
			{
				default: {
					damageModifier: {
						factor: 1.05,
					},
				},
			},
		],
	};
	public baseCost = 27;
}

export default BioticAmpEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitAttack from '../../../../tactical/damage/attack';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class ProtonGeneratorEquipment extends GenericEquipment {
	public static readonly id = 20002;
	public static readonly caption = 'Proton Generator';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				dodge: {
					[UnitAttack.deliveryType.beam]: {
						bias: 0.4,
					},
				},
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Lasers]: {
					baseRate: {
						factor: 1.25,
					},
				},
				[Weapons.TWeaponGroupType.Beam]: {
					baseRate: {
						factor: 1.25,
					},
				},
			},
		],
		shield: [
			{
				shieldAmount: {
					bias: 20,
				},
			},
		],
	};
	public static readonly description =
		'Pretty antique device that draws power from the tiniest amounts of surrounding Hydrogen and generates certain types of magnetic fields, when mounted to a Chassis. Grants +25% Attack Rate on Lasers and Energy Weapons, +40% Dodge vs Beam and increase Shield Capacity, if any, by 20';
	public baseCost = 33;
}

export default ProtonGeneratorEquipment;

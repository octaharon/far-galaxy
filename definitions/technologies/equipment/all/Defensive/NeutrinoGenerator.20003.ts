import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class NeutrinoGeneratorEquipment extends GenericEquipment {
	public static readonly id = 20003;
	public static readonly caption = 'Neutrino Generator';
	public static readonly description =
		'Straight from the black market, a portable version of FLUX Generator, that collects power from omnipresent neutrino beams and transfers it to the Chassis. Grants +50% Attack Rate on Warp Weapons, and additional 20% of Antimatter and WRP Damage absorption on Armor and Shield';
	public static readonly prototypeModifier: IPrototypeModifier = {
		weapon: [
			{
				[Weapons.TWeaponGroupType.Warp]: {
					baseRate: {
						factor: 1.5,
					},
				},
			},
		],
		shield: [
			{
				[Damage.damageType.antimatter]: {
					factor: 0.8,
				},
				[Damage.damageType.warp]: {
					factor: 0.8,
				},
			},
		],
		armor: [
			{
				[Damage.damageType.antimatter]: {
					factor: 0.8,
				},
				[Damage.damageType.warp]: {
					factor: 0.8,
				},
			},
		],
	};
	public baseCost = 27;
}

export default NeutrinoGeneratorEquipment;

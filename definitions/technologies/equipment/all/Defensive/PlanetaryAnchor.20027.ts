import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitDefense from '../../../../tactical/damage/defense';
import AdaptiveRecyclingEffect from '../../../../tactical/statuses/Effects/all/Unique/AdaptiveRecycling.37016';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class PlanetaryAnchorEquipment extends GenericEquipment {
	public static readonly id = 20027;
	public static readonly caption = 'Planetary Anchor';
	public static readonly description = `A deployable passive beacon that takes a while to charge before providing the bearer's Shields with steady supply of power. Grants Adaptive Recycling Effect and Resilient Defense, but -15% Dodge`;
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				defenseFlags: {
					[UnitDefense.defenseFlags.resilient]: true,
				},
				dodge: {
					default: {
						bias: -0.15,
					},
				},
			},
		],
	};
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: AdaptiveRecyclingEffect.id,
		},
	];
	public baseCost = 23;
}

export default PlanetaryAnchorEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import { AbilityPowerUIToken } from '../../../../tactical/statuses/constants';
import ECCMModuleStatusEffect from '../../../../tactical/statuses/Effects/all/Guidance/ECCMModule.02007';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import GenericEquipment from '../../GenericEquipment';

export class ECCMModuleEquipment extends GenericEquipment {
	public static readonly id = 10002;
	public static readonly caption = 'ECCM Module';
	public static readonly description = `The best available commercial ECM jammer that can be mounted almost anywhere and would significantly improve combat capabilities of special forces. Grants +20% ${AbilityPowerUIToken}.${ECCMModuleStatusEffect.description} and retaliates to those who target this Unit with Abilities`;
	public static readonly effects: TEffectApplianceList = [
		{
			effectId: ECCMModuleStatusEffect.id,
		},
	];
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			abilityPowerModifier: [
				{
					factor: 1.2,
				},
			],
		},
	};
	public baseCost = 30;
}

export default ECCMModuleEquipment;

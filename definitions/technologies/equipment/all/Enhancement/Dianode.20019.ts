import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import GenericEquipment from '../../GenericEquipment';

export class DianodeEquipment extends GenericEquipment {
	public static readonly id = 20019;
	public static readonly caption = 'Dianode';
	public static readonly description =
		'A complex superconductor device dating few technological generations back, that still can absorb virtually endless amounts of heat. With certain tinkering can be fitted to almost any Chassis, providing +40 Hit Points but increasing Unit Cost by 15%. However, Instead of taking Heat Damage to Hit Points, this Unit heals from it up to 5 Hit Points per Attack';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				baseCost: {
					factor: 1.15,
				},
				hitPoints: {
					bias: 40,
				},
			},
		],
		armor: [
			{
				[Damage.damageType.heat]: {
					factor: -1,
					min: -5,
				},
			},
		],
	};
	public baseCost = 20;
}

export default DianodeEquipment;

import { IPrototypeModifier } from '../../../../prototypes/types';
import GenericEquipment from '../../GenericEquipment';

export class AugmentedFrameEquipment extends GenericEquipment {
	public static readonly id = 20006;
	public static readonly caption = 'Polymer Frame';
	public static readonly description =
		'Optimized materials are made into flexible yet robust carcass that supports the Chassis, rendering the Unit slightly more agile. Grants +2 Speed, +1 Effect Resistance and +25 Hit Points';
	public static readonly prototypeModifier: IPrototypeModifier = {
		unitModifier: {
			statusDurationModifier: [
				{
					bias: -1,
					min: 1,
					minGuard: 1,
				},
			],
		},
		chassis: [
			{
				speed: {
					bias: 2,
					minGuard: 0.1,
				},
				hitPoints: {
					bias: 25,
				},
			},
		],
	};
	public baseCost = 25;
}

export default AugmentedFrameEquipment;

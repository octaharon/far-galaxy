import { IPrototypeModifier } from '../../../../prototypes/types';
import UnitAttack from '../../../../tactical/damage/attack';
import CamouflageStatusEffect from '../../../../tactical/statuses/Effects/all/ElectronicWarfare/Camouflage.03008';
import { TEffectApplianceList } from '../../../../tactical/statuses/types';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericEquipment from '../../GenericEquipment';

export class CloakDeviceEquipment extends GenericEquipment {
	public static readonly id = 20017;
	public static readonly caption = 'Cloaking Device';
	public static readonly description =
		'Lab-quality field projector that actively distorts visible light around Chassis and jams scanning and targeting devices. Allows to enter Cloaked mode, increases Damage from Cloak, Intercept and Suppress Actions. Increases Dodge by 75% versus Drone and Missile attacks';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				dodge: {
					[UnitAttack.deliveryType.missile]: {
						bias: 0.75,
					},
					[UnitAttack.deliveryType.drone]: {
						bias: 0.75,
					},
				},
				actions: [UnitActions.unitActionTypes.cloak],
			},
		],
	};
	public static readonly effects: TEffectApplianceList = [{ effectId: CamouflageStatusEffect.id }];
	public baseCost = 31;
}

export default CloakDeviceEquipment;

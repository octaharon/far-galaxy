import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import GenericEquipment from '../../GenericEquipment';

export class PowerRecirculatorEquipment extends GenericEquipment {
	public static readonly id = 20016;
	public static readonly caption = 'Power Recirculator';
	public static readonly description =
		'Sentient power source that micromanages energy flows in and out of every Chassis subsystem and actively absorbs certain forms of energy in proximity. Reduces the Unit Cost by 10%, and Shields will restore themselves from incoming EMG Damage, up to 10 Capacity per weapon, but their total Capacity is reduced by 10';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				baseCost: {
					factor: 0.9,
				},
			},
		],
		shield: [
			{
				shieldAmount: {
					bias: -10,
					minGuard: 10,
				},
				[Damage.damageType.magnetic]: {
					min: -10,
					factor: -1,
				},
			},
		],
	};
	public baseCost = 27;
}

export default PowerRecirculatorEquipment;

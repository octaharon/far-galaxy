import { IPrototypeModifier } from '../../../../prototypes/types';
import GenericEquipment from '../../GenericEquipment';

export class FusionCoreEquipment extends GenericEquipment {
	public static readonly id = 20012;
	public static readonly caption = 'Fusion Core';
	public static readonly description =
		'Portable plutonium generator that can be adapted to various Chassis and will certainly put it slightly higher in the competition. Provides an extra Action Phase, additional 5% Damage Reduction on Armor, +15% Hit Points and +0.5 AoE Radius';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				actionCount: 1,
				hitPoints: {
					factor: 1.15,
				},
			},
		],
		weapon: [
			{
				default: {
					aoeRadiusModifier: {
						bias: 0.5,
					},
				},
			},
		],
		armor: [
			{
				default: {
					factor: 0.95,
				},
			},
		],
	};
	public baseCost = 39;
}

export default FusionCoreEquipment;

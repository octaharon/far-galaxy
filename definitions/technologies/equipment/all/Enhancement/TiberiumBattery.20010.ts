import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import { AbilityPowerUIToken } from '../../../../tactical/statuses/constants';
import GenericEquipment from '../../GenericEquipment';

export class TiberiumBatteryEquipment extends GenericEquipment {
	public static readonly id = 20010;
	public static readonly caption = 'Tiberium Battery';
	public static readonly description = `A big enough chunk of a mysterious spaceborn crystal suspended in zero gravity container. When connected to a Chassis, it resonates with certain onboard circuits. Grants +2 Scan Range, +0.5 ${AbilityPowerUIToken} and +30% KIN and HEA Shield Protection`;
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				scanRange: {
					bias: 2,
				},
			},
		],
		unitModifier: {
			abilityPowerModifier: [
				{
					bias: 0.5,
				},
			],
		},
		shield: [
			{
				[Damage.damageType.kinetic]: {
					factor: 0.7,
				},
				[Damage.damageType.heat]: {
					factor: 0.7,
				},
			},
		],
	};
	public baseCost = 26;
}

export default TiberiumBatteryEquipment;

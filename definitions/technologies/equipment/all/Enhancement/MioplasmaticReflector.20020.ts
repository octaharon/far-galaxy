import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import Weapons from '../../../types/weapons';
import GenericEquipment from '../../GenericEquipment';

export class MioplasmaticReflectorEquipment extends GenericEquipment {
	public static readonly id = 20020;
	public static readonly caption = 'Mioplasmatic Reflector';
	public static readonly description =
		'High maintenance power circuit which imbues any Chassis with a Tachyon Defense module, along with +2 Attack Range for Energy Weapons and Lasers, but also increases Unit Cost by 15%. Near-field tachyon field also absorbs up to 15 ANH Damage into Hit Points, negating the rest of it';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				baseCost: {
					factor: 1.15,
				},
				defenseFlags: {
					[UnitDefense.defenseFlags.tachyon]: true,
				},
			},
		],
		armor: [
			{
				[Damage.damageType.antimatter]: {
					factor: -1,
					min: -15,
				},
			},
		],
		weapon: [
			{
				[Weapons.TWeaponGroupType.Beam]: {
					baseRange: {
						bias: 2,
					},
				},
				[Weapons.TWeaponGroupType.Lasers]: {
					baseRange: {
						bias: 2,
					},
				},
			},
		],
	};
	public baseCost = 35;
}

export default MioplasmaticReflectorEquipment;

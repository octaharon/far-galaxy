import { IPrototypeModifier } from '../../../../prototypes/types';
import Damage from '../../../../tactical/damage/damage';
import UnitActions from '../../../../tactical/units/actions/types';
import GenericEquipment from '../../GenericEquipment';

export class VoidTransporterEquipment extends GenericEquipment {
	public static readonly id = 20018;
	public static readonly caption = 'Void Transporter';
	public static readonly description =
		'A small but powerful higgs field modulator that passively draws energy from vaccum and nearby timespace anomalies. Allows the Unit to perform Blink, increases Speed by +2 and allows Shields to be recharged by incoming WRP damage, up to 20 Capacity per Attack. Alas, its high power demand also increases Unit Cost by 25%';
	public static readonly prototypeModifier: IPrototypeModifier = {
		chassis: [
			{
				baseCost: {
					factor: 1.25,
				},
				speed: {
					bias: 2,
					minGuard: 0.1,
				},
				actions: [UnitActions.unitActionTypes.blink],
			},
		],
		shield: [
			{
				[Damage.damageType.warp]: {
					factor: -1,
					min: -20,
				},
			},
		],
	};
	public baseCost = 30;
}

export default VoidTransporterEquipment;

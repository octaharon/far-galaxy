import _ from 'underscore';
import { TEquipment } from '../types/equipment';
import GenericEquipment from './GenericEquipment';
import { EquipmentGroups, TEquipmentGroup } from './groups';

let EquipmentList = [] as any;
export const equipmentIndex = _.mapObject(EquipmentGroups, () => [] as TEquipment[]);

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => {
		const mod = r(key);
		const eqGroup = key.split('/')[1] as TEquipmentGroup;
		if (equipmentIndex[eqGroup] && mod?.default?.id) equipmentIndex[eqGroup].push(mod.default);
		cache.push(mod);
	});
}

requireAll(require.context('./all/', true, /\.ts$/), EquipmentList);
EquipmentList = EquipmentList.filter(
	(module: any) => module.default.prototype instanceof GenericEquipment && module.default.id > 0,
).map((module: any) => module.default);
export default EquipmentList as TEquipment[];

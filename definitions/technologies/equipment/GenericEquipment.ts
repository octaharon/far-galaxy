import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import { IPrototypeModifier } from '../../prototypes/types';
import { TAuraApplianceList } from '../../tactical/statuses/Auras/types';
import { TEffectApplianceList } from '../../tactical/statuses/types';
import UnitActions from '../../tactical/units/actions/types';
import UnitPart, { IUnitPart } from '../../tactical/units/UnitPart';
import { IEquipmentMeta, IEquipmentStatic, TEquipmentProps, TSerializedEquipment } from '../types/equipment';

export class GenericEquipment
	extends UnitPart<TEquipmentProps, TSerializedEquipment, IEquipmentMeta, IEquipmentStatic>
	implements IUnitPart<TEquipmentProps, TSerializedEquipment, IEquipmentMeta, IEquipmentStatic>
{
	public static readonly effects: TEffectApplianceList = [];
	public static readonly abilities: number[] = [];
	public static readonly actions: UnitActions.TUnitActionOptions = [];
	public static readonly auras: TAuraApplianceList = [];
	public static readonly prototypeModifier: IPrototypeModifier = {};

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.equipment];
	}

	public serialize(): TSerializedEquipment {
		return {
			...this.__serializeUnitPart(),
		};
	}
}

export default GenericEquipment;

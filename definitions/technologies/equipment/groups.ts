export type TEquipmentGroupMeta = {
	caption: string;
	description?: string;
};

export const EquipmentGroups = {
	Defensive: {
		caption: 'Protectives',
		description:
			'are Equipment modules best suited, supposedly, for defensive units and/or protective operations, suggesting certain Armor and Shields bonuses',
	},
	Enhancement: {
		caption: 'Augments',
		description:
			'are a miracle of cybernetics and engineering that grants unique combination of qualities to the Unit, when installed as Equipment',
	},
	Offensive: {
		caption: 'Warfare',
		description:
			'is Equipment devices supposed to be used at offensive operations, which usually provides some Weapon bonuses',
	},
	Tactical: {
		caption: 'Combat AI',
		description: `is a combination of communication and computation hardware that takes an Equipment slot and can drastically change Unit's combat style and tactics, making a special operative Unit of practically any Chassis`,
	},
	Support: {
		caption: 'Support Module',
		description: `are Equipment devices that bests suits Support Units, providing universal bonuses for Units, regardless of their armaments`,
	},
	Warfare: {
		caption: 'Ordnance',
		description: `is a special kind of Weapons that is installed into Equipment slot, granting a Unit means of doing Damage with Abilities`,
	},
};

export type TEquipmentGroup = keyof typeof EquipmentGroups;

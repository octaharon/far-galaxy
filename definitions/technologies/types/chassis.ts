import { TCollectible } from '../../core/Collectible';
import InterfaceTypes from '../../core/InterfaceTypes';
import { IUnitEffectModifier } from '../../prototypes/types';
import UnitDefense from '../../tactical/damage/defense';
import { TAuraApplianceList } from '../../tactical/statuses/Auras/types';
import { TEffectApplianceList } from '../../tactical/statuses/types';
import UnitActions from '../../tactical/units/actions/types';
import UnitTalents from '../../tactical/units/levels/unitTalents';
import UnitDecorators from '../../tactical/units/UnitDecorators';
import {
	IStaticUnitPart,
	IUnitPart,
	IUnitPartDecorator,
	IUnitPartFactory,
	TSerializedUnitPart,
} from '../../tactical/units/UnitPart';
import PoliticalFactions from '../../world/factions';
import Weapons from './weapons';
import TIdMap = InterfaceTypes.TIdMap;

export namespace UnitChassis {
	export enum movementType {
		ground = 'mov_ground',
		air = 'mov_air', // ignores obstacles
		naval = 'mov_naval',
		hover = 'mov_hover', // counts as ground but moves over water as well
	}

	export enum targetType {
		organic = 'target_bio',
		robotic = 'target_robo',
		massive = 'target_massive',
		unique = 'target_unique',
	}

	export enum chassisClass {
		none = 'ch_none_debug_only',
		artillery = 'ch_artillery',
		quadrapod = 'ch_quadrapod',
		battleship = 'ch_battleship',
		biotech = 'ch_biotech',
		bomber = 'ch_bomber',
		manofwar = 'ch_manofwar',
		carrier = 'ch_carrier',
		combatdrone = 'ch_combatdrone',
		mecha = 'ch_mecha',
		corvette = 'ch_corvette',
		cruiser = 'ch_cruiser',
		destroyer = 'ch_destroyer',
		droid = 'ch_droid',
		exosuit = 'ch_exo',
		fighter = 'ch_fighter',
		fortress = 'ch_fortress',
		hav = 'ch_hav',
		tiltjet = 'ch_tiltjet',
		helipad = 'ch_helipad',
		hovertank = 'ch_hovertank',
		hydra = 'ch_hydra',
		infantry = 'ch_infantry',
		launcherpad = 'ch_launcher_pad',
		lav = 'ch_lav',
		mothership = 'ch_mothership',
		pointdefense = 'ch_pd',
		rover = 'ch_rover',
		science_vessel = 'ch_science_vessel',
		scout = 'ch_scout',
		strike_aircraft = 'ch_strike_aircraft',
		submarine = 'ch_submarine',
		sup_aircraft = 'ch_sup_aircraft',
		sup_ship = 'ch_sup_ship',
		sup_vehicle = 'ch_sup_vehicle',
		swarm = 'ch_swarm',
		tank = 'ch_tank',
	}

	export type TTargetFlags = EnumProxy<targetType, boolean>;

	export type TOfTargetType<T extends targetType | 'default'> = T extends 'default'
		? {
				[targetType.organic]: false;
				[targetType.robotic]: false;
		  }
		: {
				[key in T]: true;
		  };

	export type TUnitChassisDecorator = UnitDecorators.IUnitArmorModifier &
		UnitDecorators.IUnitShieldsModifier &
		UnitDecorators.IUnitWeaponModifier &
		UnitDecorators.IUnitDodgeDecorator;

	export type TChassisProps = IUnitPartDecorator &
		UnitDefense.TDefenseFlagsDecorator & {
			hitPoints: number;
			actions: UnitActions.TUnitActionOptions[];
			speed: number;
			scanRange: number;
			dodge: UnitDefense.TDodge;
			equipmentSlots: number;
		};

	export type TSerializedChassis = TChassisProps & TSerializedUnitPart;

	export type TUnitChassisModifier = Partial<
		UnitDecorators.ICostModifier &
			UnitDefense.TDefenseFlagsDecorator &
			UnitDecorators.IUnitHitPointsModifier &
			UnitDecorators.IUnitScanRangeModifier &
			UnitDecorators.IUnitSpeedModifier &
			UnitDecorators.IUnitDodgeModifier & {
				actions: UnitActions.TUnitActionOptions;
				actionCount: number;
			}
	>;

	export type TChassisClassModifier = EnumProxy<chassisClass, TUnitChassisModifier> & {
		default?: TUnitChassisModifier;
	};
}

export default UnitChassis;

export type IChassisStatic = IStaticUnitPart & {
	type: UnitChassis.chassisClass;
	flags: UnitChassis.TTargetFlags;
	movement: UnitChassis.movementType;
	restrictedFactions?: PoliticalFactions.TFactionID[];
	exclusiveFactions?: PoliticalFactions.TFactionID[];
	weaponSlots: Weapons.TWeaponSlotType[];
	armorSlots: UnitDefense.TArmorSlotType[];
	shieldSlots: UnitDefense.TShieldSlotType[];
	weaponModifier?: Weapons.TWeaponGroupModifier;
	armorModifier?: UnitDefense.TArmorModifier;
	shieldModifier?: UnitDefense.TShieldsModifier;
	unitEffectModifier?: IUnitEffectModifier;
	effects: TEffectApplianceList;
	auras: TAuraApplianceList;
	abilities: number[];
	levelTree: UnitTalents.ITalentTree;
	baseChassis: IChassisStatic;
};

export interface IChassisMeta {
	class: UnitChassis.chassisClass;
}

export interface IChassis
	extends UnitChassis.TChassisProps,
		IUnitPart<UnitChassis.TChassisProps, UnitChassis.TSerializedChassis, IChassisMeta, IChassisStatic> {
	applyModifier(...m: UnitChassis.TUnitChassisModifier[]): IChassis;
}

export interface IChassisFactory
	extends IUnitPartFactory<
		UnitChassis.TChassisProps,
		UnitChassis.TSerializedChassis,
		IChassis,
		IChassisMeta,
		IChassisStatic
	> {
	isChassisAvailableToFaction(c: TChassis, f: PoliticalFactions.TFactionID): boolean;

	isChassisOfClass(c: TChassis, t: UnitChassis.chassisClass): boolean;

	isChassisOfMovementType(c: TChassis, t: UnitChassis.movementType): boolean;

	isChassisOfTargetType(c: TChassis, t: UnitChassis.targetType): boolean;

	getBaseChassis(c: IChassisStatic): IChassisStatic;

	getAllByChassisClass(type: UnitChassis.chassisClass): TIdMap<IChassisStatic>;

	isMassive(chassisId: number): boolean;

	isUnique(chassisId: number): boolean;

	isOrganic(chassisId: number): boolean;

	isRobotic(chassisId: number): boolean;

	isGround(chassisId: number): boolean;

	isHover(chassisId: number): boolean;

	isNaval(chassisId: number): boolean;

	isAir(chassisId: number): boolean;
}

export type TChassis = TCollectible<IChassis, IChassisStatic>;
export type TChassisList = TChassis[];

export type TChassisTypedSlot = Weapons.TWeaponSlotType | UnitDefense.TArmorSlotType | UnitDefense.TShieldSlotType;

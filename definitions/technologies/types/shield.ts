import { TCollectible } from '../../core/Collectible';
import UnitDefense from '../../tactical/damage/defense';
import {
	IStaticUnitPart,
	IUnitPart,
	IUnitPartDecorator,
	IUnitPartFactory,
	TSerializedUnitPart,
} from '../../tactical/units/UnitPart';
import TShield = UnitDefense.TShield;

export type IShieldStatic = IStaticUnitPart & {
	type: UnitDefense.TShieldSlotType;
	flags?: UnitDefense.TDefenseFlags;
};
export type TShieldProps = IUnitPartDecorator & {
	shield: UnitDefense.TShieldDefinition;
};
export type TSerializedShield = TShieldProps & TSerializedUnitPart;

export interface IShieldMeta {
	slot: UnitDefense.TShieldSlotType;
}

export interface IShield extends IUnitPart<TShieldProps, TSerializedShield, IShieldMeta, IShieldStatic>, TShieldProps {
	static: TShieldItem;
	compiled: TShield;

	applyModifier(...m: UnitDefense.TShieldsModifier[]): IShield;
}

export type TShieldItem<ObjectType extends IShield = IShield> = TCollectible<ObjectType, IShieldStatic>;

export interface IShieldFactory
	extends IUnitPartFactory<TShieldProps, TSerializedShield, IShield, IShieldMeta, IShieldStatic> {
	doesShieldFitSlot(a: TShieldItem, slot: UnitDefense.TShieldSlotType): boolean;

	getShieldsBySlotType(slot: UnitDefense.TShieldSlotType): TShieldItem[];

	getShieldsBySlotType(slot: UnitDefense.TShieldSlotType): TShieldItem[];
}

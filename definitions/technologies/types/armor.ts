import { TCollectible } from '../../core/Collectible';
import UnitDefense from '../../tactical/damage/defense';
import {
	IStaticUnitPart,
	IUnitPart,
	IUnitPartDecorator,
	IUnitPartFactory,
	TSerializedUnitPart,
} from '../../tactical/units/UnitPart';

export type IArmorStatic = IStaticUnitPart & {
	type: UnitDefense.TArmorSlotType;
	dodgeModifier?: UnitDefense.TDodgeModifier;
	healthModifier?: IModifier;
	flags?: UnitDefense.TDefenseFlags;
};
export type TArmorProps = IUnitPartDecorator & {
	armor: UnitDefense.TArmor;
};
export type TSerializedArmor = TArmorProps & TSerializedUnitPart;

export interface IArmorMeta {
	slot: UnitDefense.TArmorSlotType;
}

export interface IArmor extends IUnitPart<TArmorProps, TSerializedArmor, IArmorMeta, IArmorStatic>, TArmorProps {
	static: TArmorItem;
	compiled: UnitDefense.TArmor;

	applyModifier(...m: UnitDefense.TArmorModifier[]): IArmor;
}

export interface IArmorFactory
	extends IUnitPartFactory<TArmorProps, TSerializedArmor, IArmor, IArmorMeta, IArmorStatic> {
	doesArmorFitSlot(a: TArmorItem, slot: UnitDefense.TArmorSlotType): boolean;

	getArmorsBySlotType(slot: UnitDefense.TArmorSlotType): TArmorItem[];
}

export type TArmorItem<ObjectType extends IArmor = IArmor> = TCollectible<ObjectType, IArmorStatic>;

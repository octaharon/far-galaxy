import { TCollectible } from '../../core/Collectible';
import { IPrototypeModifier } from '../../prototypes/types';
import { TAuraApplianceList } from '../../tactical/statuses/Auras/types';
import { TEffectApplianceList } from '../../tactical/statuses/types';
import UnitActions from '../../tactical/units/actions/types';
import {
	IStaticUnitPart,
	IUnitPart,
	IUnitPartDecorator,
	IUnitPartFactory,
	IUnitPartModifier,
	TSerializedUnitPart,
} from '../../tactical/units/UnitPart';
import { TEquipmentGroup, TEquipmentGroupMeta } from '../equipment/groups';

export type TEquipmentProps = {} & IUnitPartDecorator;
export type TSerializedEquipment = TEquipmentProps & TSerializedUnitPart;

export interface IEquipmentMeta {
	group: TEquipmentGroup;
}

export type IEquipmentStatic = IStaticUnitPart & {
	effects: TEffectApplianceList;
	abilities: number[];
	actions: UnitActions.TUnitActionOptions;
	auras: TAuraApplianceList;
	prototypeModifier: IPrototypeModifier;
};

export type IEquipmentModifier = IUnitPartModifier & {};

export type IEquipment = IUnitPart<TEquipmentProps, TSerializedEquipment, IEquipmentMeta, IEquipmentStatic>;

export type IEquipmentFactory = IUnitPartFactory<
	TEquipmentProps,
	TSerializedEquipment,
	IEquipment,
	IEquipmentMeta,
	IEquipmentStatic
> & {
	getByGroup(groupId: TEquipmentGroup): number[];
	getGroups(): TEquipmentGroup[];
	getGroupById(equipmentId: number): TEquipmentGroup;
	getGroupMeta(groupId: TEquipmentGroup): TEquipmentGroupMeta;
};

export type TEquipment<ObjectType extends IEquipment = IEquipment> = TCollectible<ObjectType, IEquipmentStatic>;

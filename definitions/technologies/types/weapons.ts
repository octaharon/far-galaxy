import { TCollectible } from '../../core/Collectible';
import $ from '../../core/InterfaceTypes';
import UnitAttack from '../../tactical/damage/attack';
import Damage from '../../tactical/damage/damage';
import {
	IStaticUnitPart,
	IUnitPart,
	IUnitPartDecorator,
	IUnitPartFactory,
	IUnitPartModifier,
	TSerializedUnitPart,
} from '../../tactical/units/UnitPart';

namespace Weapons {
	export type TWeaponDecorator = {
		baseRate: number;
		priority: number;
		baseRange: number;
		baseAccuracy: number;
	};

	export interface IWithAttacks {
		attacks: UnitAttack.TAttackDefinition[];
	}

	export type TWeaponProperties = TWeaponDecorator & IWithAttacks & IUnitPartDecorator;

	export type TSerializedWeapon = TSerializedUnitPart & Weapons.TWeaponProperties;

	export type TWeaponModifier = IUnitPartModifier &
		Proxy<TWeaponDecorator, IModifier> &
		Partial<Damage.IDamageModifierProperty> & {
			aoeRadiusModifier?: IModifier;
		};
	export type TWeaponModifierKey = keyof TWeaponModifier;
	export type TWeaponGroupGeneric<T> = EnumProxy<TWeaponGroupType, T>;
	export type TWeaponGroupModifier = EnumProxyWithDefault<TWeaponGroupType, TWeaponModifier>;

	export type TWeaponGroupAttributeValue = 'verylow' | 'low' | 'normal' | 'high' | 'veryhigh' | 'default';
	export interface IWeaponGroup {
		caption?: string;
		readonly id: TWeaponGroupType;
		aliases?: Array<string | RegExp>;
		description?: Partial<
			Record<'rate' | 'range' | 'precision' | 'damage', TWeaponGroupAttributeValue> & {
				text: string;
				lineOfSight: boolean;
			}
		>;
		playerDisabled?: boolean;
		weapons: number[];
	}

	export type TSlotModifier = EnumProxy<TWeaponGroupType, TWeaponModifier>;

	export enum TWeaponSlotType {
		small = 'wp_slot_small',
		medium = 'wp_slot_medium',
		large = 'wp_slot_large',
		extra = 'wp_slot_xl',
		air = 'wp_slot_air',
		naval = 'wp_slot_naval',
		hangar = 'wp_slot_hangar',
	}

	export enum TWeaponGroupType {
		Cannons = 'wp_type_cannon',
		Launchers = 'wp_type_launchers',
		Missiles = 'wp_type_missiles',
		Railguns = 'wp_type_railguns',
		Cruise = 'wp_type_cruise',
		Guns = 'wp_type_machineguns',
		Rockets = 'wp_type_rockets',
		Bombs = 'wp_type_bombs',
		Lasers = 'wp_type_lasers',
		Plasma = 'wp_type_plasma',
		Beam = 'wp_type_beam',
		Warp = 'wp_type_warp',
		AntiAir = 'wp_type_anti_air',
		Drone = 'wp_type_drone',
	}

	export interface IWeaponSlot {
		name?: string;
		description?: string;
		abbr?: string;
		groups?: TSlotModifier;
		globalModifier?: TWeaponModifier;
	}
}

export default Weapons;

export interface IWeaponMeta {
	slots: Weapons.TWeaponSlotType[];
	group: Weapons.IWeaponGroup;
}

export type IWeaponStatic = IStaticUnitPart & {
	groupType: Weapons.TWeaponGroupType;
};

export interface IWeapon
	extends IUnitPart<Weapons.TWeaponProperties, Weapons.TSerializedWeapon, IWeaponMeta, IWeaponStatic>,
		Weapons.TWeaponProperties {
	compiled: UnitAttack.IAttackInterface;

	applyModifier(...m: Weapons.TWeaponModifier[]): IWeapon;

	applyGroupModifier(...m: Weapons.TWeaponGroupModifier[]): IWeapon;

	applySlotModifier(t: Weapons.TWeaponSlotType): IWeapon;

	getDamagePerTurn(force?: boolean): Damage.TDamageUnit;

	getAttacksQuantityMinMax(rate?: number): [number, number];
}

export interface IWeaponFactory
	extends IUnitPartFactory<
		Weapons.TWeaponProperties,
		Weapons.TSerializedWeapon,
		IWeapon,
		IWeaponMeta,
		IWeaponStatic
	> {
	weaponGroups: EnumProxy<Weapons.TWeaponGroupType, Weapons.IWeaponGroup>;

	getSlotSuffix(slot: Weapons.TWeaponSlotType): string;

	getSlotAbbrSuffix(slot: Weapons.TWeaponSlotType): string;

	getWeaponsByGroup(group: Weapons.TWeaponGroupType): TWeapon[];

	getGroupMeta(group: Weapons.TWeaponGroupType): Weapons.IWeaponGroup;

	getWeaponSlotModifiers(id: number, slot: Weapons.TWeaponSlotType): Weapons.TWeaponModifier[];

	getWeaponsBySlot(slot: Weapons.TWeaponSlotType): TWeapon[];

	doesWeaponFitSlot(weaponId: number, slot: Weapons.TWeaponSlotType): boolean;

	getWeaponCompatibleSlots(weaponId: number): Weapons.TWeaponSlotType[];
}

export type TWeapon<WeaponType extends IWeapon = IWeapon> = TCollectible<WeaponType, IWeaponStatic>;
export type TWeapons = TWeapon[];
export type TWeaponMap = $.TIdMap<TWeapon>;

import { AAWeapons } from './all/antiair';
import { EnergyWeapons } from './all/beam';
import BombWeapons from './all/bombs';
import { CannonWeapons } from './all/cannons';
import { DroneWeapons } from './all/drones';
import { GuidedMissileWeapons } from './all/guided';
import { LaserWeapons } from './all/lasers';
import { MachineGunWeapons } from './all/machineguns';
import { MissileWeapons } from './all/missiles';
import { PlasmaWeapons } from './all/plasma';
import { ProjectileWeapons } from './all/projectiles';
import { RailgunWeapons } from './all/railguns';
import { RocketWeapons } from './all/rockets';
import { WarpWeapons } from './all/warp';

export const WeaponGroups = [
	MissileWeapons,
	MachineGunWeapons,
	BombWeapons,
	GuidedMissileWeapons,
	ProjectileWeapons,
	RailgunWeapons,
	CannonWeapons,
	RocketWeapons,
	PlasmaWeapons,
	LaserWeapons,
	WarpWeapons,
	AAWeapons,
	EnergyWeapons,
	DroneWeapons,
];
export default WeaponGroups;

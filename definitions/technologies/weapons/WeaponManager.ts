import _ from 'underscore';
import BasicMaths from '../../maths/BasicMaths';
import AttackManager from '../../tactical/damage/AttackManager';
import { UnitPartFactory } from '../../tactical/units/UnitPart';
import Weapons, { IWeapon, IWeaponFactory, IWeaponMeta, TWeapon, TWeaponMap, TWeapons } from '../types/weapons';
import WeaponGroups from './Groups';
import WeaponsList from './index';
import WeaponSlots from './weaponSlots';

export default class WeaponManager
	extends UnitPartFactory<Weapons.TWeaponProperties, Weapons.TSerializedWeapon, IWeapon, IWeaponMeta>
	implements IWeaponFactory
{
	public weaponGroups: EnumProxy<Weapons.TWeaponGroupType, Weapons.IWeaponGroup> = {};
	protected __lookupBySlot: EnumProxy<Weapons.TWeaponSlotType, number[]> = {};

	public getSlotSuffix(slot: Weapons.TWeaponSlotType): string {
		const c = WeaponSlots[slot].abbr;
		if (c) return `(${c})`;
		return '';
	}

	public getSlotAbbrSuffix(slot: Weapons.TWeaponSlotType): string {
		const c = WeaponSlots[slot].abbr;
		if (c) return `(${c.substr(0, 1).toLocaleUpperCase()})`;
		return '';
	}

	public getWeaponsByGroup(group: Weapons.TWeaponGroupType): TWeapon[] {
		return this.weaponGroups[group].weapons.map((id) => this.get(id));
	}

	public getGroupMeta(group: Weapons.TWeaponGroupType): Weapons.IWeaponGroup {
		return this.weaponGroups[group];
	}

	public getWeaponSlotModifiers(id: number, slot: Weapons.TWeaponSlotType): Weapons.TWeaponModifier[] {
		const meta = this.getItemMeta(id);
		const r = [] as Weapons.TWeaponModifier[];
		if (!meta) return null;
		if (!meta.slots.includes(slot)) return null;
		const g = WeaponSlots[slot].groups[meta.group.id];
		if (!_.isEmpty(g)) r.push(g);
		if (!_.isEmpty(WeaponSlots[slot].globalModifier)) r.push(WeaponSlots[slot].globalModifier);
		return r;
	}

	public getWeaponsBySlot(slot: Weapons.TWeaponSlotType): TWeapon[] {
		return (this.__lookupBySlot[slot] || []).map((t) => this.get(t));
	}

	public doesWeaponFitSlot(weaponId: number, slot: Weapons.TWeaponSlotType) {
		return this.__lookupById[weaponId] && this.__lookupById[weaponId].slots.includes(slot);
	}

	public getWeaponCompatibleSlots(weaponId: number): Weapons.TWeaponSlotType[] {
		return this.__lookupById[weaponId].slots;
	}

	public fill() {
		// eslint-disable-next-line @typescript-eslint/no-this-alias
		const self = this;
		const items = WeaponsList;
		self.weaponGroups = WeaponGroups.reduce(
			(o, group: Weapons.IWeaponGroup) => ({
				...o,
				[group.id]: {
					...group,
					weapons: [],
				},
			}),
			{},
		);
		this.__fillItems<TWeapon>(
			items,
			(w) =>
				({
					slots: Object.keys(WeaponSlots).filter((k: Weapons.TWeaponSlotType) =>
						Object.keys(WeaponSlots[k].groups).includes(w.groupType),
					),
					group: this.weaponGroups[w.groupType],
				} as IWeaponMeta),
			(w) => {
				this.weaponGroups[w.groupType].weapons.push(w.id);
				(
					Object.keys(WeaponSlots).filter((k: Weapons.TWeaponSlotType) =>
						Object.keys(WeaponSlots[k].groups).includes(w.groupType),
					) as Weapons.TWeaponSlotType[]
				).forEach((slotType) => {
					if (!this.__lookupBySlot[slotType]) this.__lookupBySlot[slotType] = [];
					this.__lookupBySlot[slotType].push(w.id);
				});
			},
		);
		return this;
	}
}

export function createWeaponDictionary(list: TWeapons) {
	return list.sort((a, b) => a.id - b.id).reduce((o, w) => ({ ...o, [w.id]: w }), {}) as TWeaponMap;
}

export const stackWeaponModifiers = (...t: Weapons.TWeaponModifier[]) =>
	t.reduce(
		(obj, wm) =>
			Object.keys(wm).reduce(
				(o, wma) =>
					Object.assign(o, {
						[wma]: BasicMaths.stackModifiers(o[wma] || {}, wm[wma]),
					}),
				obj,
			),
		{} as Weapons.TWeaponModifier,
	);

export const stackWeaponGroupModifiers = (...t: Weapons.TWeaponGroupModifier[]) => {
	const modKeys: Partial<Record<keyof Weapons.TWeaponGroupModifier, boolean>> = {};
	(t || []).forEach((wgm) => Object.keys(wgm).forEach((wg) => (modKeys[wg] = true)));
	return Object.keys(modKeys).reduce((obj, wgm) => {
		obj[wgm] = stackWeaponModifiers(...t.map((t) => t[wgm] || t.default || {}));
		return obj;
	}, {} as Weapons.TWeaponGroupModifier);
};

export function describeWeapon(w: IWeapon) {
	const wT = w.static;
	return `${wT.caption} #${wT.id}
    Accuracy: ${w.baseAccuracy}
    Rate: ${w.baseRate}
    Cost: ${w.baseCost}
    Range: ${w.baseRange}
    Attacks: \n${w.attacks.map((v) => AttackManager.describeAttack(v)).join('')}`;
}

export function describeWeaponModifier(w: Weapons.TWeaponModifier, groupName?: string, join?: true): string;
export function describeWeaponModifier(w: Weapons.TWeaponModifier, groupName?: string, join?: false): string[];
export function describeWeaponModifier(w: Weapons.TWeaponModifier, groupName = '', join = true): string | string[] {
	const r = [];
	if (w.baseCost)
		r.push(`${BasicMaths.describeModifier(w.baseCost)} ${groupName} Production Cost`.replace(/\s+/, ' '));
	if (w.baseRange) r.push(`${BasicMaths.describeModifier(w.baseRange)} ${groupName} Range`.replace(/\s+/, ' '));
	if (w.baseRate) r.push(`${BasicMaths.describeModifier(w.baseRate)} ${groupName} Attack Rate`.replace(/\s+/, ' '));
	if (w.baseAccuracy)
		r.push(
			`${BasicMaths.describeModifier({
				...w.baseAccuracy,
				factor: 1 + w.baseAccuracy.bias,
				bias: 0,
			})} ${groupName} Hit Chance`.replace(/\s+/, ' '),
		);
	if (w.damageModifier)
		r.push(`${BasicMaths.describeModifier(w.damageModifier)} ${groupName} Damage`.replace(/\s+/, ' '));
	if (w.aoeRadiusModifier)
		r.push(`${BasicMaths.describeModifier(w.aoeRadiusModifier)} ${groupName} AoE Radius`.replace(/\s+/, ' '));
	if (w.priority)
		r.push(`${w.priority.bias > 0 ? 'Lower' : 'Higher'} ${groupName} Attack Priority`.replace(/\s+/, ' '));
	return join ? r.join(', ') : r;
}

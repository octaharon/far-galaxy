import { ATTACK_PRIORITIES } from '../../tactical/damage/constants';
import { ORBITAL_ATTACK_RANGE } from '../../tactical/units/actions/constants';
import Weapons from '../types/weapons';

export const WeaponSlots: EnumProxy<Weapons.TWeaponSlotType, Weapons.IWeaponSlot> = {
	[Weapons.TWeaponSlotType.small]: {
		name: 'Small Weapon',
		abbr: 'Small',
		globalModifier: {
			damageModifier: {
				factor: 0.9,
			},
			baseRange: {
				bias: -1,
				minGuard: 3,
				min: 3,
			},
			aoeRadiusModifier: {
				factor: 0.5,
				min: 0.25,
				minGuard: 0.25,
			},
		},
		groups: {
			[Weapons.TWeaponGroupType.Missiles]: {
				baseRange: {
					bias: -1,
					minGuard: 6,
					min: 5,
				},
				baseRate: {
					bias: -0.1,
					min: 0.1,
				},
			},
			[Weapons.TWeaponGroupType.Cannons]: {
				aoeRadiusModifier: {
					factor: 0.5,
					min: 0.25,
					minGuard: 0.25,
				},
				priority: {
					bias: 2,
					max: ATTACK_PRIORITIES.LOWEST,
				},
			},
			[Weapons.TWeaponGroupType.AntiAir]: {
				baseRange: {
					bias: -2,
					minGuard: 1,
					min: 1,
				},
				priority: {
					bias: 2,
					max: ATTACK_PRIORITIES.LOWEST,
				},
			},
			[Weapons.TWeaponGroupType.Launchers]: {
				baseCost: {
					bias: -1,
				},
				baseAccuracy: {
					bias: 0.4,
				},
				priority: {
					bias: 3,
					maxGuard: ATTACK_PRIORITIES.LOW,
					max: ATTACK_PRIORITIES.LOWEST,
				},
				aoeRadiusModifier: {
					bias: -0.5,
					min: 0.75,
					minGuard: 0.75,
				},
			},
			[Weapons.TWeaponGroupType.Guns]: {},
			[Weapons.TWeaponGroupType.Lasers]: {},
		},
	},
	[Weapons.TWeaponSlotType.medium]: {
		name: 'Medium Weapon platform',
		abbr: 'Medium',
		globalModifier: {},
		groups: {
			[Weapons.TWeaponGroupType.Cannons]: {},
			[Weapons.TWeaponGroupType.Warp]: {},
			[Weapons.TWeaponGroupType.Guns]: {},
			[Weapons.TWeaponGroupType.AntiAir]: {},
			[Weapons.TWeaponGroupType.Lasers]: {},
			[Weapons.TWeaponGroupType.Plasma]: {
				damageModifier: {
					factor: 0.75,
				},
				baseRange: {
					bias: -3,
					min: 5,
					minGuard: 5,
				},
				priority: {
					bias: ATTACK_PRIORITIES.LOWEST,
					max: ATTACK_PRIORITIES.LOWEST,
				},
			},
			[Weapons.TWeaponGroupType.Missiles]: {},
			[Weapons.TWeaponGroupType.Cruise]: {},
			[Weapons.TWeaponGroupType.Beam]: {},
			[Weapons.TWeaponGroupType.Railguns]: {
				damageModifier: {
					factor: 0.9,
				},
				baseCost: {
					bias: 3,
				},
				priority: {
					bias: ATTACK_PRIORITIES.LOW,
					max: ATTACK_PRIORITIES.LOWEST,
				},
			},
			[Weapons.TWeaponGroupType.Launchers]: {},
		},
	},
	[Weapons.TWeaponSlotType.large]: {
		name: 'Large Weapon platform',
		abbr: 'Large',
		globalModifier: {
			baseCost: {
				factor: 1.5,
			},
			baseRate: {
				factor: 0.85,
				min: 0.25,
				minGuard: 0.25,
			},
			baseRange: {
				bias: 1,
			},
			damageModifier: {
				factor: 1.2,
				bias: 10,
			},
			aoeRadiusModifier: {
				factor: 1.5,
				max: 3,
				maxGuard: 3,
			},
		},
		groups: {
			[Weapons.TWeaponGroupType.Missiles]: {},
			[Weapons.TWeaponGroupType.Cruise]: {},
			[Weapons.TWeaponGroupType.Rockets]: {},
			[Weapons.TWeaponGroupType.Warp]: {},
			[Weapons.TWeaponGroupType.Plasma]: {},
			[Weapons.TWeaponGroupType.Beam]: {},
			[Weapons.TWeaponGroupType.Railguns]: {},
			[Weapons.TWeaponGroupType.Cannons]: {
				damageModifier: {
					bias: 5,
				},
			},
		},
	},
	[Weapons.TWeaponSlotType.extra]: {
		name: 'Titan mount',
		abbr: 'Titan',
		globalModifier: {
			baseCost: {
				factor: 2,
			},
			baseRange: {
				factor: 1.3,
				max: 20,
				maxGuard: 20,
			},
			damageModifier: {
				factor: 1.5,
				bias: 25,
			},
			baseRate: {
				factor: 0.75,
				min: 0.15,
				minGuard: 0.15,
			},
			aoeRadiusModifier: {
				factor: 2,
				max: 4,
				maxGuard: 4,
			},
		},
		groups: {
			[Weapons.TWeaponGroupType.Cruise]: {},
			[Weapons.TWeaponGroupType.Rockets]: {},
			[Weapons.TWeaponGroupType.Railguns]: {},
			[Weapons.TWeaponGroupType.Plasma]: {},
			[Weapons.TWeaponGroupType.Beam]: {},
			[Weapons.TWeaponGroupType.Cannons]: {
				damageModifier: {
					bias: 15,
				},
			},
		},
	},
	[Weapons.TWeaponSlotType.air]: {
		name: 'Airplane Weapon Rack',
		abbr: 'Airplane Rack',
		globalModifier: {
			baseCost: {
				bias: 3,
			},
			damageModifier: {
				factor: 1.1,
			},
		},
		groups: {
			[Weapons.TWeaponGroupType.Guns]: {
				damageModifier: {
					factor: 1.3,
				},
				baseRange: {
					bias: 4,
				},
			},
			[Weapons.TWeaponGroupType.Missiles]: {
				baseAccuracy: {
					bias: 0.5,
				},
				baseRange: {
					bias: 2,
				},
			},
			[Weapons.TWeaponGroupType.Beam]: {
				baseRange: {
					min: 3,
					minGuard: 3,
					bias: -2,
				},
			},
			[Weapons.TWeaponGroupType.Cruise]: {
				baseAccuracy: {
					bias: 0.5,
				},
				baseRange: {
					bias: 2,
				},
			},
			[Weapons.TWeaponGroupType.Lasers]: {
				baseRange: {
					min: 3,
					minGuard: 3,
					bias: -2,
				},
			},
			[Weapons.TWeaponGroupType.Rockets]: {
				baseAccuracy: {
					bias: 0.5,
				},
				baseRange: {
					bias: 2,
				},
			},
			[Weapons.TWeaponGroupType.Bombs]: {},
		},
	},
	[Weapons.TWeaponSlotType.naval]: {
		name: 'Naval Weapon mount',
		abbr: 'Naval',
		globalModifier: {
			baseCost: {
				bias: 7,
			},
			baseRange: {
				bias: 4,
				max: ORBITAL_ATTACK_RANGE * 2,
				maxGuard: ORBITAL_ATTACK_RANGE,
			},
			priority: {
				bias: ATTACK_PRIORITIES.HIGHEST,
				min: ATTACK_PRIORITIES.HIGH,
				minGuard: ATTACK_PRIORITIES.HIGH,
			},
			baseAccuracy: {
				bias: 0.25,
			},
			baseRate: {
				minGuard: 0.2,
				min: 0.2,
				factor: 0.8,
			},
			damageModifier: {
				factor: 1.3,
			},
			aoeRadiusModifier: {
				bias: 1,
				max: 5,
				maxGuard: 5,
			},
		},
		groups: {
			[Weapons.TWeaponGroupType.AntiAir]: {},
			[Weapons.TWeaponGroupType.Railguns]: {
				baseCost: {
					factor: 0.9,
				},
				damageModifier: {
					factor: 1.1,
				},
			},
			[Weapons.TWeaponGroupType.Beam]: {},
			[Weapons.TWeaponGroupType.Rockets]: {},
			[Weapons.TWeaponGroupType.Missiles]: {},
			[Weapons.TWeaponGroupType.Cannons]: {
				damageModifier: {
					factor: 1.2,
				},
			},
			[Weapons.TWeaponGroupType.Warp]: {},
			[Weapons.TWeaponGroupType.Lasers]: {},
			[Weapons.TWeaponGroupType.Plasma]: {},
			[Weapons.TWeaponGroupType.Cruise]: {},
		},
	},
	[Weapons.TWeaponSlotType.hangar]: {
		name: 'Carrier dock',
		abbr: 'Carrier',
		globalModifier: {
			baseRange: {
				min: 12, // set  range to 12
			},
			priority: {
				bias: ATTACK_PRIORITIES.LOW,
				min: ATTACK_PRIORITIES.LOW,
			},
		},
		groups: {
			[Weapons.TWeaponGroupType.Bombs]: {
				damageModifier: {
					factor: 1.5,
				},
				baseCost: {
					factor: 1.5,
				},
				baseRange: {
					bias: +8,
					max: 8,
				},
				baseRate: {
					factor: 0.8,
				},
				aoeRadiusModifier: {
					bias: 1,
				},
			},
			[Weapons.TWeaponGroupType.Drone]: {},
		},
	},
};

export default WeaponSlots;

import { TWeapons } from '../types/weapons';
import GenericWeapon from './GenericWeapon';

let WeaponList = [] as TWeapons;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), WeaponList);
WeaponList = WeaponList.filter(
	(module: any) => module.default.prototype instanceof GenericWeapon && module.default.id > 0,
).map((module: any) => module.default);
export default WeaponList as TWeapons;

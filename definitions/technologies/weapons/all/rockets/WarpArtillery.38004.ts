import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRocket from './GenericRocket';

export class WarpRocket extends GenericRocket {
	public static caption = 'Warp Artillery';
	public static description = `Rocket artillery with BHG warheads.
    Painful to reload, but the range is superior to most weapons`;
	public static id = 38004;
	public baseCost: number = this.baseCost + 6;
	public baseRange: number = this.baseRange - 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.radiation]: {
				min: 15,
				max: 45,
				distribution: 'Bell',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			..._.omit(this.attacks[1], Damage.damageType.thermodynamic),
			[Damage.damageType.warp]: {
				min: 40,
				max: 50,
				distribution: 'Parabolic',
			},
			delivery: UnitAttack.deliveryType.immediate,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default WarpRocket;

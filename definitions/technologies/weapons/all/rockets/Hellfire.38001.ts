import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRocket from './GenericRocket';

export class HellfireRocket extends GenericRocket {
	public static caption = 'Hellfire rocket';
	public static description = `Extremely long range unguided rocket, which explodes in front of target,
    showering an area behind it with blazing flames`;
	public static id = 38001;
	public baseCost: number = this.baseCost + 7;
	public baseRange: number = this.baseRange + 2;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.heat]: {
				min: 10,
				max: 25,
				distribution: 'Bell',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			..._.omit(this.attacks[1], Damage.damageType.thermodynamic),
			[Damage.damageType.heat]: {
				min: 40,
				max: 60,
				distribution: 'Parabolic',
			},
			delivery: UnitAttack.deliveryType.supersonic,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default HellfireRocket;

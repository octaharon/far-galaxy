import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRocket from './GenericRocket';

export class RocketArtillery extends GenericRocket {
	public static caption = 'Rocket artillery';
	public static description = `A long range unguided rocket, delivering a fragmentation warhead
    right to the enemy's doorstep`;
	public static id = 38000;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.kinetic]: {
				min: 10,
				max: 25,
				distribution: 'Bell',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			..._.omit(this.attacks[1], Damage.damageType.thermodynamic),
			[Damage.damageType.kinetic]: {
				min: 40,
				max: 60,
				distribution: 'Parabolic',
			},
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default RocketArtillery;

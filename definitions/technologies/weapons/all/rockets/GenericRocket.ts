import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericRocket extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Rockets;
	public baseAccuracy = 1.6;
	public baseRate = 0.2;
	public baseCost = 18;
	public priority = ATTACK_PRIORITIES.LOWEST;
	public baseRange = 14;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {
				min: 10,
				max: 40,
				distribution: 'MostlyMinimal',
			},
			delivery: UnitAttack.deliveryType.missile,
		} as UnitAttack.TAttackDefinition,
		{
			[Damage.damageType.thermodynamic]: {
				min: 40,
				max: 60,
				distribution: 'Parabolic',
			},
			delivery: UnitAttack.deliveryType.bombardment,
			[UnitAttack.attackFlags.AoE]: true,
			radius: 2,
			magnitude: 0.7,
			aoeType: UnitAttack.TAoEDamageSplashTypes.cone180,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default GenericRocket;

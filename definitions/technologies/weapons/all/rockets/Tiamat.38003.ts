import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRocket from './GenericRocket';

export class TiamatRocket extends GenericRocket {
	public static caption = 'Tiamat MRLS';
	public static description = `Rocket artillery equipped with thermobaric warheads, despite the price,
    offers the best combination of range, rate of fire and damage`;
	public static id = 38003;
	public baseCost: number = this.baseCost + 8;
	public baseRate = 0.4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.kinetic]: {
				min: 5,
				max: 20,
				distribution: 'Maximal',
			},
			[Damage.damageType.heat]: {
				min: 5,
				max: 20,
				distribution: 'Maximal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			...this.attacks[1],
			delivery: UnitAttack.deliveryType.cloud,
			[Damage.damageType.thermodynamic]: {
				min: 30,
				max: 40,
				distribution: 'Minimal',
			},
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default TiamatRocket;

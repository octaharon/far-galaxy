import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRocket from './GenericRocket';

export class SS18Rocket extends GenericRocket {
	public static caption = 'SS-18';
	public static description = `Rocket artillery with multiple guided nuclear warheads. Delightful!`;
	public static id = 38002;
	public baseCost: number = this.baseCost + 4;
	public baseRange: number = this.baseRange - 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.magnetic]: {
				min: 10,
				max: 25,
				distribution: 'Bell',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			..._.omit(this.attacks[1], Damage.damageType.thermodynamic),
			[Damage.damageType.radiation]: {
				min: 40,
				max: 60,
				distribution: 'Parabolic',
			},
			radius: 3,
			delivery: UnitAttack.deliveryType.missile,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default SS18Rocket;

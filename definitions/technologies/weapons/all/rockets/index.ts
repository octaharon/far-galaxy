import Weapons from '../../../types/weapons';

export const RocketWeapons: Weapons.IWeaponGroup = {
	caption: 'Rockets',
	aliases: ['Rocket Weapons'],
	description: {
		text: 'Really big missiles that deliver various warheads with high precision at a very long Range and with a significant AOE Damage. At bigger versions they shoot far enough to enable Orbital Attack for the bearer',
		lineOfSight: false,
		damage: 'normal',
		precision: 'high',
		range: 'veryhigh',
		rate: 'verylow',
	},
	id: Weapons.TWeaponGroupType.Rockets,
	weapons: [],
};
export default RocketWeapons;

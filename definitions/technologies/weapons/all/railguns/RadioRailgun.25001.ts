import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRailgun from './GenericRailgun';

export class RadioRailgun extends GenericRailgun {
	public static caption = 'Radium Cannon';
	public static description = `A precision railgun firing slugs with radioactive core,
    which break upon hitting a target`;
	public static id = 25001;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.kinetic]: {
				min: 55,
				max: 70,
				distribution: 'Gaussian',
			},
			[Damage.damageType.radiation]: {
				min: 10,
				max: 20,
				distribution: 'Uniform',
			},
		},
		{
			[Damage.damageType.radiation]: {
				min: 8,
				max: 16,
				distribution: 'Uniform',
			},
			[UnitAttack.attackFlags.DoT]: true,
			delivery: UnitAttack.deliveryType.immediate,
			duration: 2,
		} as UnitAttack.TDOTAttackDefinition,
	];
}

export default RadioRailgun;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRailgun from './GenericRailgun';

export class Hypercharger extends GenericRailgun {
	public static caption = 'Hypercharger';
	public static description = `Imagine a cyclotron for inch-sized particles of carbon.
    Imagine those particles get pumped up with dozens C of electrical charge while spinning.
    Imaging releasing them into target at startling speed of 9 Mach and blasting through everything
    One thing you can be sure of is that it's gonna hurt`;
	public static id = 25002;
	public baseCost: number = this.baseCost + 9;
	public baseRange: number = this.baseRange + 1;
	public baseAccuracy: number = this.baseAccuracy - 0.3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.kinetic]: {
				min: 30,
				max: 70,
				distribution: 'Gaussian',
			},
			[Damage.damageType.magnetic]: {
				min: 30,
				max: 50,
				distribution: 'Uniform',
			},
			[Damage.damageType.heat]: {
				min: 10,
				max: 20,
				distribution: 'Uniform',
			},
		},
	];
}

export default Hypercharger;

import Weapons from '../../../types/weapons';

export const RailgunWeapons: Weapons.IWeaponGroup = {
	caption: 'Mass Drivers',
	aliases: ['Railguns', 'Mass Driver Weapons', 'Railgun Weapons'],
	description: {
		text: 'Also known as railguns, these weapons offer a greatly increased ballistic damage and pierce through targets in direct sight, while they are harder to Dodge and are exceptionally good against Shields',
		lineOfSight: true,
		damage: 'high',
		precision: 'normal',
		range: 'normal',
		rate: 'low',
	},
	id: Weapons.TWeaponGroupType.Railguns,
	weapons: [],
};
export default RailgunWeapons;

import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericRailgun extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Railguns;
	public baseAccuracy = 1.1;
	public baseRate = 0.4;
	public baseCost = 11;
	public priority: number = ATTACK_PRIORITIES.HIGH;
	public baseRange = 9;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {},
			[UnitAttack.attackFlags.Penetrative]: true,
			delivery: UnitAttack.deliveryType.supersonic,
		} as UnitAttack.TAttackDefinition,
	];
}

export default GenericRailgun;

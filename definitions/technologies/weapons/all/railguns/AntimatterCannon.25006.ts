import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRailgun from './GenericRailgun';

export class AntimatterCannon extends GenericRailgun {
	public static caption = 'Antimatter Cannon';
	public static description = `A mass driver which fires semi-stable anti-carbon fibers. Upon impact they annihilate badly`;
	public static id = 25006;
	public baseCost: number = this.baseCost + 4;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		[Damage.damageType.kinetic]: {
			min: 40,
			max: 60,
			distribution: 'Gaussian',
		},
		[Damage.damageType.antimatter]: {
			min: 40,
			max: 50,
			distribution: 'Bell',
		},
	}));
}

export default AntimatterCannon;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRailgun from './GenericRailgun';

export class RailgunCannon extends GenericRailgun {
	public static caption = 'Rail Cannon';
	public static description = `All new is a well forgotten old. This is no exception, except for an exceptional range, precision and damage.
        A metal slug travels at supersonic speed and penetrates everything on the way`;
	public static id = 25000;
	public baseRate: number = this.baseRate + 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		[Damage.damageType.kinetic]: {
			min: 55,
			max: 75,
			distribution: 'Gaussian',
		},
	}));
}

export default RailgunCannon;

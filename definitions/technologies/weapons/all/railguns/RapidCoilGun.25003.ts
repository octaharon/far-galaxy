import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRailgun from './GenericRailgun';

export class RapidCoilGun extends GenericRailgun {
	public static caption = 'Rapid Coil Gun';
	public static description = `A rapid fire rotary mass driver, which boasts fantastic precision and fire rate but lacks range`;
	public static id = 25003;
	public baseRange: number = this.baseRange - 2;
	public baseAccuracy: number = this.baseAccuracy - 0.3;
	public baseRate = 1.6;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		[Damage.damageType.kinetic]: {
			min: 25,
			max: 60,
			distribution: 'Gaussian',
		},
	}));
}

export default RapidCoilGun;

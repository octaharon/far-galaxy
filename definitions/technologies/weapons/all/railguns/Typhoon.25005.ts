import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRailgun from './GenericRailgun';

export class TyphoonWeapon extends GenericRailgun {
	public static caption = 'Typhoon';
	public static description = `A mass driver variation which fires bursts of carbon needles that shower a certain area at supersonic velocities.`;
	public static id = 25005;
	public baseAccuracy: number = this.baseAccuracy + 0.3;
	public baseCost: number = this.baseCost + 7;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {
				min: 45,
				max: 70,
				distribution: 'Maximal',
			},
			[UnitAttack.attackFlags.AoE]: true,
			delivery: UnitAttack.deliveryType.supersonic,
			radius: 2,
			magnitude: 1,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default TyphoonWeapon;

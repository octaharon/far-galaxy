import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericRailgun from './GenericRailgun';

export class CoilMortar extends GenericRailgun {
	public static caption = 'Coil mortar';
	public static description = `The artillery variant of a mass driver, launching hypersonic projectiles at extreme Range and for comparably outstanding Damage. Those come for a price of the slowest rate of fire possible and little to none penetration, compared to other Mass Drivers`;
	public static id = 25004;
	public baseRange: number = this.baseRange + 3;
	public baseAccuracy: number = this.baseAccuracy + 0.5;
	public baseRate = 0.12;
	public baseCost: number = this.baseCost + 6;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.supersonic,
			[Damage.damageType.kinetic]: {
				min: 145,
				max: 195,
				distribution: 'Gaussian',
			},
		},
	];
}

export default CoilMortar;

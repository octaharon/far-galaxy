import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class ProtonCannon extends GenericMachineGun {
	public static caption = 'Proton Machine Gun';
	public static description = `High rate automatic weapon with some Tritium-charged ammunition,
        emitting a bulk of radiation into target on impact`;
	public static id = 20003;
	public baseCost: number = this.baseCost + 4;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map(
		() =>
			({
				[Damage.damageType.kinetic]: {
					min: 3,
					max: 8,
					distribution: 'Gaussian',
				},
				[Damage.damageType.radiation]: {
					min: 4,
					max: 6,
					distribution: 'Minimal',
				},
				delivery: UnitAttack.deliveryType.ballistic,
			} as UnitAttack.TAttackDefinition),
	);
}

export default ProtonCannon;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class AutoCannon extends GenericMachineGun {
	public static caption = 'HMG-XXII';
	public static description = `Low recoil heavy machine gun, that rapidly shoots bursts of 22mm rounds with poor precision and range`;
	public static id = 20000;
	public baseAccuracy: number = this.baseAccuracy + 0.1;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map(
		() =>
			({
				[Damage.damageType.kinetic]: {
					min: 4,
					max: 10,
					distribution: 'Gaussian',
				},
				delivery: UnitAttack.deliveryType.ballistic,
			} as UnitAttack.TAttackDefinition),
	);
}

export default AutoCannon;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class TrackingGun extends GenericMachineGun {
	public static caption = 'Chopper';
	public static description = `Medium rate automatic weapon with advanced tracking and targeting firmware,
        yielding slightly higher precision and damage compared to a regular machine gun.`;
	public static id = 20011;
	public baseCost: number = this.baseCost + 7;
	public baseAccuracy: number = this.baseAccuracy + 0.35;
	public baseRate = 1.2;
	public baseRange: number = this.baseRange + 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(2).fill(0).map(
		(d) =>
			({
				[Damage.damageType.kinetic]: {
					min: 10,
					max: 15,
					distribution: 'Maximal',
				},
				delivery: UnitAttack.deliveryType.ballistic,
			} as UnitAttack.TAttackDefinition),
	);
}

export default TrackingGun;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class SPARCGun extends GenericMachineGun {
	public static caption = 'SPARC';
	public static description = `Streamed Particle Acceleration Rotary Cannon. As deadly as it sounds.`;
	public static id = 20002;
	public baseCost: number = this.baseCost + 8;
	public baseRange: number = this.baseRange - 0.5;
	public baseAccuracy: number = this.baseAccuracy + 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map(
		(d) =>
			({
				[Damage.damageType.kinetic]: {
					min: 3,
					max: 8,
					distribution: 'Gaussian',
				},
				[Damage.damageType.antimatter]: {
					min: 4,
					max: 6,
					distribution: 'Gaussian',
				},
				delivery: UnitAttack.deliveryType.ballistic,
			} as UnitAttack.TAttackDefinition),
	);
}

export default SPARCGun;

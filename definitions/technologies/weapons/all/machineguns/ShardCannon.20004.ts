import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class ShardCannon extends GenericMachineGun {
	public static caption = 'Shard Cannon';
	public static description = `An intricate device which autonomously produces and stores tiny crystals of any volatile gas available,
                      which then shot out in sprays, causing a lot of materials to turn into dust when
                      coming to contact with the substance. The range is limited, but the fire rate is impressive`;
	public static id = 20004;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map(
		(d) =>
			({
				[Damage.damageType.kinetic]: {
					min: 3,
					max: 8,
					distribution: 'Minimal',
				},
				[Damage.damageType.biological]: {
					min: 4,
					max: 6,
					distribution: 'Uniform',
				},
				delivery: UnitAttack.deliveryType.ballistic,
			} as UnitAttack.TAttackDefinition),
	);
}

export default ShardCannon;

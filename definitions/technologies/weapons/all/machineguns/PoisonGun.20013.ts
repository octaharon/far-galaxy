import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class PoisonGun extends GenericMachineGun {
	public static caption = 'Poisonous Machine Gun';
	public static description = `High rate automatic weapon, applying biochemical compound to bullets, which
        takes some time to absorb and to melt target structures`;
	public static id = 20013;
	public baseCost: number = this.baseCost + 8;
	public baseAccuracy: number = this.baseAccuracy + 0.1;
	public baseRange: number = this.baseRange - 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3)
		.fill(0)
		.map(
			(d) =>
				({
					[Damage.damageType.kinetic]: {
						min: 1,
						max: 5,
						distribution: 'Minimal',
					},
					[Damage.damageType.biological]: {
						min: 4,
						max: 6,
						distribution: 'Minimal',
					},
					delivery: UnitAttack.deliveryType.ballistic,
				} as UnitAttack.TAttackDefinition),
		)
		.concat([
			{
				[Damage.damageType.radiation]: {
					min: 1,
					max: 10,
					distribution: 'Parabolic',
				},
				[UnitAttack.attackFlags.DoT]: true,
				duration: 3,
				delivery: UnitAttack.deliveryType.immediate,
			} as UnitAttack.TDOTAttackDefinition,
		]);
}

export default PoisonGun;

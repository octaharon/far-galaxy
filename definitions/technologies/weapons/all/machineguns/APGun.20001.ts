import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class APGun extends GenericMachineGun {
	public static caption = 'Big Bertha';
	public static description = `High caliber automatic weapon with armor piercing ammunition`;
	public static id = 20001;
	public baseCost: number = this.baseCost + 2;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map(
		() =>
			({
				[Damage.damageType.kinetic]: {
					min: 3,
					max: 8,
					distribution: 'Minimal',
				},
				[Damage.damageType.thermodynamic]: {
					min: 4,
					max: 6,
					distribution: 'Minimal',
				},
				delivery: UnitAttack.deliveryType.ballistic,
			} as UnitAttack.TAttackDefinition),
	);
}

export default APGun;

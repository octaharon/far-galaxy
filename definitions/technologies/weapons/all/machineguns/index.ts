import Weapons from '../../../types/weapons';

export const MachineGunWeapons: Weapons.IWeaponGroup = {
	caption: 'Machine Guns',
	aliases: ['Guns', 'MG Weapons'],
	description: {
		text: 'Automatic low caliber weapons provide a heavy barrage of ballistic firepower, very helpful against targets with weaker Shields, but they can become completely useless against heavily protected targets',
		lineOfSight: true,
		damage: 'low',
		precision: 'low',
		range: 'verylow',
		rate: 'veryhigh',
	},
	id: Weapons.TWeaponGroupType.Guns,
	weapons: [],
};
export default MachineGunWeapons;

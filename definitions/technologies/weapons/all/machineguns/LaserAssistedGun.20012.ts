import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class LaserAssistedGun extends GenericMachineGun {
	public static caption = 'Firespitter';
	public static description = `High rate automatic weapon that fires bursts of darts, which are pre-ignited midair with high power laser and then arrive at the target as a chunk
        of burning molten metal. Extra pressure from heating rays grants this design an extra Attack Range with a little penalty to Attack Rate`;
	public static id = 20012;
	public baseCost: number = this.baseCost + 6;
	public baseRange: number = this.baseRange + 1.5;
	public baseRate: number = this.baseRate - 0.5;
	public baseAccuracy: number = this.baseAccuracy + 0.05;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map(
		(d) =>
			({
				[Damage.damageType.kinetic]: {
					min: 3,
					max: 5,
					distribution: 'Uniform',
				},
				[Damage.damageType.heat]: {
					min: 4,
					max: 6,
					distribution: 'Uniform',
				},
				delivery: UnitAttack.deliveryType.ballistic,
			} as UnitAttack.TAttackDefinition),
	);
}

export default LaserAssistedGun;

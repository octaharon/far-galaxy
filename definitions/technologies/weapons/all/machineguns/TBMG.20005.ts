import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

const damageTypes = [Damage.damageType.magnetic, Damage.damageType.heat, Damage.damageType.thermodynamic];

export class TripleBarrelMachineGun extends GenericMachineGun {
	public static caption = 'TBMG';
	public static description = `High rate triple barrels machine gun with multiple ammunition types
        to ensure the maximum versatility against different targets`;
	public static id = 20005;
	public baseAccuracy: number = this.baseAccuracy - 0.05;
	public baseRate: number = this.baseRate - 0.5;
	public baseCost: number = this.baseCost + 5;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3)
		.fill({
			[Damage.damageType.kinetic]: {
				min: 3,
				max: 8,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.ballistic,
		} as UnitAttack.TAttackDefinition)
		.map((o, ix) => ({
			...o,
			[damageTypes[ix]]: {
				min: 3,
				max: 8,
				distribution: 'Uniform',
			},
		}));
}

export default TripleBarrelMachineGun;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericMachineGun from './GenericMachineGun';

export class RapidGun extends GenericMachineGun {
	public static caption = 'Rivet Gun';
	public static description = `Short-range high-rate automatic cannon, firing basic metal slugs. Very high firepower despite very basic implementation.`;
	public static id = 20010;
	public baseRate: number = this.baseRate + 1;
	public baseCost: number = this.baseCost + 11;
	public baseRange: number = this.baseRange - 0.5;
	public baseAccuracy: number = this.baseAccuracy - 0.15;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(5).fill(0).map(
		(d) =>
			({
				[Damage.damageType.kinetic]: {
					min: 6,
					max: 10,
					distribution: 'MostlyMinimal',
				},
				delivery: UnitAttack.deliveryType.ballistic,
			} as UnitAttack.TAttackDefinition),
	);
}

export default RapidGun;

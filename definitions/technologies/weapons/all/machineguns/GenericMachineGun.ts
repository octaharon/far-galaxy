import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericMachineGun extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Guns;
	public baseAccuracy = 0.75;
	public baseRate = 3;
	public baseCost = 7;
	public priority = ATTACK_PRIORITIES.NORMAL;
	public baseRange = 4.5;
	public attacks: UnitAttack.TAttackDefinition[];
}

export default GenericMachineGun;

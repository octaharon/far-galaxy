import Weapons from '../../../types/weapons';

export const GuidedMissileWeapons: Weapons.IWeaponGroup = {
	caption: 'Cruise Missiles',
	aliases: ['Cruise Weapons', 'Cruise'],
	description: {
		text: 'Warheads with an active guidance system have great range and accuracy and can deliver critical damage. They are designed to snipe out single targets at distance',
		lineOfSight: false,
		damage: 'veryhigh',
		precision: 'veryhigh',
		range: 'high',
		rate: 'verylow',
	},
	id: Weapons.TWeaponGroupType.Cruise,
	weapons: [],
} as Weapons.IWeaponGroup;
export default GuidedMissileWeapons;

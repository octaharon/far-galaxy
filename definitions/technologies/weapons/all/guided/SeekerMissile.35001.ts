import GenericGuidedMissile from './GenericGuidedMissile';

export class SeekerMissile extends GenericGuidedMissile {
	public static caption = 'Seeker Missile';
	public static description = `Hypersonic missile with combined guidance systems and the longest engagement range. A conventional armor piercing payload makes it very reliable though not too efficient of a weapon`;
	public static id = 35001;
	public baseRange: number = this.baseRange + 1.5;
}

export default SeekerMissile;

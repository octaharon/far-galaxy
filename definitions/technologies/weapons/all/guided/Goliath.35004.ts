import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericGuidedMissile from './GenericGuidedMissile';

export class GoliathMissile extends GenericGuidedMissile {
	public static caption = 'Goliath Missile';
	public static description = `A supreme precision high fire rate missile system,
    firing flocks of thermobaric warheads and capable of delivering immense damage to random targets in an area`;
	public static id = 35004;
	public baseCost: number = this.baseCost + 8;
	public baseRate = 0.7;
	public baseRange: number = this.baseRange - 3;
	public baseAccuracy: number = this.baseAccuracy - 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.missile,
			[Damage.damageType.kinetic]: {
				min: 15,
				max: 25,
				distribution: 'Bell',
			},
		},
		{
			delivery: UnitAttack.deliveryType.supersonic,
			[Damage.damageType.thermodynamic]: {
				min: 15,
				max: 60,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.heat]: {
				min: 15,
				max: 60,
				distribution: 'MostlyMinimal',
			},
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			magnitude: 1.75,
			radius: 1.5,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default GoliathMissile;

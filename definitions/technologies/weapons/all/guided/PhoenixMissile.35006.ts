import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericGuidedMissile from './GenericGuidedMissile';

export class PhoenixMissile extends GenericGuidedMissile {
	public static caption = 'Phoenix missiles';
	public static description = `A relatively weak payload in this missiles is compensated
    by extremely advanced electronic equipment and a supply of fuel for missile to
    circle around the target until it finds the perfect moment and angle to engage,
    allowing it to bypass most conventional defenses`;
	public static id = 35006;
	public baseRange: number = this.baseRange - 1;
	public baseCost: number = this.baseCost + 5;
	public baseAccuracy: number = this.baseAccuracy + 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.heat]: {
				min: 40,
				max: 80,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.thermodynamic]: {
				min: 25,
				max: 100,
				distribution: 'MostlyMinimal',
			},
			[UnitAttack.attackFlags.Disruptive]: true,
			delivery: UnitAttack.deliveryType.missile,
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default PhoenixMissile;

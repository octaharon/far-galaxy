import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericGuidedMissile from './GenericGuidedMissile';

export class GlidingMissile extends GenericGuidedMissile {
	public static caption = 'Gliding Missile';
	public static description = `A missile which dives on the ground from a steep angle like a pile driver,
    creating a strong shockwave on impact, as well as delivering massive damage to the primary target`;
	public static id = 35005;
	public baseCost: number = this.baseCost + 6;
	public baseRange: number = this.baseRange - 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.heat),
			[Damage.damageType.kinetic]: {
				min: 20,
				max: 45,
				distribution: 'Bell',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.thermodynamic]: {
				min: 40,
				max: 80,
				distribution: 'MostlyMaximal',
			},
			delivery: UnitAttack.deliveryType.surface,
			[UnitAttack.attackFlags.AoE]: true,
			[UnitAttack.attackFlags.Disruptive]: true,
			radius: 4,
			magnitude: 1,
			aoeType: UnitAttack.TAoEDamageSplashTypes.lineBehind,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default GlidingMissile;

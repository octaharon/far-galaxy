import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericGuidedMissile from './GenericGuidedMissile';

export class BlackWidowMissile extends GenericGuidedMissile {
	public static caption = 'Black Widow';
	public static description = `It's called black because this missile carries a
    somewhat unique warhead, blasting target with a direct beam of protons and anti-protons`;
	public static id = 35003;
	public baseCost: number = this.baseCost + 2;
	public baseAccuracy: number = this.baseAccuracy - 0.4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.heat),
			[Damage.damageType.radiation]: {
				min: 15,
				max: 75,
				distribution: 'Gaussian',
			},
			[Damage.damageType.antimatter]: {
				min: 15,
				max: 75,
				distribution: 'Bell',
			},
		},
	];
}

export default BlackWidowMissile;

import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericGuidedMissile from './GenericGuidedMissile';

export class SerpentMissile extends GenericGuidedMissile {
	public static caption = 'S.E.R.P.E.N.T.';
	public static description = `A surgical precision missile with a gravity wave generator,
    which creates extreme matter deformations in a very small area. Supreme efficiency
    versus bigger targets, while still presenting considerable threat to others, though
    damage is highly variative depending on target geometry and missile approach angle`;
	public static id = 35002;
	public baseCost: number = this.baseCost + 3;
	public baseAccuracy: number = this.baseAccuracy + 0.4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.heat),
			[Damage.damageType.warp]: {
				min: 30,
				max: 200,
				distribution: 'MostlyMinimal',
			},
		},
	];
}

export default SerpentMissile;

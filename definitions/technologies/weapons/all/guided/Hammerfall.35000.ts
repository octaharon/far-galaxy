import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericGuidedMissile from './GenericGuidedMissile';

export class HammerfallMissile extends GenericGuidedMissile {
	public static caption = 'Hammerfall';
	public static description = `A big missile for big foes, designed to snipe heavily shielded targets.
    Extremely long range, high precision, outstanding damage and a tremendous price.
    If you can afford it, you're already a winner`;
	public static id = 35000;
	public baseCost: number = this.baseCost + 10;
	public baseRange: number = this.baseRange + 1;
	public baseRate = 0.1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {
				min: 30,
				max: 100,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.thermodynamic]: {
				min: 30,
				max: 100,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.antimatter]: {
				min: 30,
				max: 100,
				distribution: 'MostlyMinimal',
			},
			delivery: UnitAttack.deliveryType.missile,
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default HammerfallMissile;

import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericGuidedMissile extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Cruise;
	public baseAccuracy = 2;
	public baseRate = 0.15;
	public baseCost = 16;
	public priority = ATTACK_PRIORITIES.LOWER;
	public baseRange = 12;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.thermodynamic]: {
				min: 20,
				max: 30,
				distribution: 'Uniform',
			},
			[Damage.damageType.heat]: {
				min: 40,
				max: 70,
				distribution: 'Bell',
			},
			delivery: UnitAttack.deliveryType.missile,
		} as UnitAttack.TAttackDefinition,
	];
}

export default GenericGuidedMissile;

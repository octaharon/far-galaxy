import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericDrone extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Drone;
	public baseAccuracy = 2.5;
	public baseRate = 3.5;
	public baseCost = 18;
	public priority = ATTACK_PRIORITIES.LOWER;
	public baseRange = 10;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.drone,
			[UnitAttack.attackFlags.Selective]: true,
		} as UnitAttack.TAttackDefinition,
	];
}

export default GenericDrone;

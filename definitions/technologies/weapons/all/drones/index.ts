import Weapons from '../../../types/weapons';

export const DroneWeapons: Weapons.IWeaponGroup = {
	caption: 'Drones',
	aliases: ['Drone Weapons'],
	description: {
		text: 'Being one of the most universally efficient weapon designs, tiny unmanned flying vehicles, that swarm the target, are terrifying for most opponents, but can be specifically countered with various Drone Jammer devices. A host Unit requires a Carrier slot to install a continuous production line.',
		lineOfSight: false,
		damage: 'normal',
		precision: 'veryhigh',
		range: 'high',
		rate: 'veryhigh',
	},
	id: Weapons.TWeaponGroupType.Drone,
	weapons: [],
};
export default DroneWeapons;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericDrone } from './GenericDrone';

export class DisruptorDrone extends GenericDrone {
	public static caption = 'Disruptor UAV';
	public static description = `A reproducible UAV that drops black hole warheads at targets, leaving nothing but empty surface behind`;
	public static id = 65004;
	public baseCost: number = this.baseCost + 8;
	public baseRate: number = this.baseRate * 0.7;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.drone,
			[Damage.damageType.antimatter]: {
				min: 25,
				max: 30,
				distribution: 'Uniform',
			},
			[Damage.damageType.warp]: {
				min: 35,
				max: 50,
				distribution: 'Minimal',
			},
			radius: 1,
			magnitude: 0.5,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			[UnitAttack.attackFlags.Penetrative]: true,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default DisruptorDrone;

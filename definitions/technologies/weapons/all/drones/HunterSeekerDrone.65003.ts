import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericDrone } from './GenericDrone';

export class HunterSeekerDrone extends GenericDrone {
	public static caption = 'Hunter-Seeker';
	public static description = `A reproducible UAV armed with smart cluster warheads`;
	public static id = 65003;
	public baseCost: number = this.baseCost + 5;
	public baseRate: number = this.baseRate * 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.drone,
			[Damage.damageType.kinetic]: {
				min: 5,
				max: 15,
				distribution: 'Uniform',
			},
			[Damage.damageType.heat]: {
				min: 5,
				max: 15,
				distribution: 'Uniform',
			},
			[Damage.damageType.magnetic]: {
				min: 5,
				max: 15,
				distribution: 'Uniform',
			},
			radius: 3,
			magnitude: 3,
			aoeType: UnitAttack.TAoEDamageSplashTypes.chain,
			[UnitAttack.attackFlags.AoE]: true,
			[UnitAttack.attackFlags.Selective]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default HunterSeekerDrone;

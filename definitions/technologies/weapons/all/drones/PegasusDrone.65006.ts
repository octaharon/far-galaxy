import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericDrone } from './GenericDrone';

export class PegasusDrone extends GenericDrone {
	public static caption = 'Pegasus UAV';
	public static description = `A reproducible autonomous aircraft with top-tier electronics
    reconnaissance equipment, that scans the target thoroughly before shooting plasma
    bursts precisely at most vulnerable spots. That allows to bypass most of defenses at a price of significantly
    lower fire rate`;
	public static id = 65006;
	public baseCost: number = this.baseCost + 9;
	public baseRate: number = this.baseRate * 0.8;
	public baseAccuracy: number = this.baseAccuracy + 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.antimatter]: {
				min: 15,
				max: 50,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.magnetic]: {
				min: 15,
				max: 50,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.thermodynamic]: {
				min: 15,
				max: 50,
				distribution: 'MostlyMinimal',
			},
			[UnitAttack.attackFlags.Disruptive]: true,
			[UnitAttack.attackFlags.Selective]: true,
			delivery: UnitAttack.deliveryType.drone,
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default PegasusDrone;

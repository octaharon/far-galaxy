import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericDrone } from './GenericDrone';

export class ChemDrone extends GenericDrone {
	public static caption = 'Anti-Personnel Drones';
	public static description = `Autonomous UAV production bay, fitting crafts with bicomponent
    chemical warheads that deal extra damage after appliance. The compound is extremely toxic for living beings`;
	public static id = 65005;
	public baseCost: number = this.baseCost + 4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.drone,
			[Damage.damageType.biological]: {
				min: 15,
				max: 100,
				distribution: 'Minimal',
			},
			[Damage.damageType.heat]: {
				min: 10,
				max: 50,
				distribution: 'Minimal',
			},
			[UnitAttack.attackFlags.DoT]: true,
			duration: 3,
		} as UnitAttack.TDOTAttackDefinition,
	];
}

export default ChemDrone;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericDrone } from './GenericDrone';

export class MissileDrone extends GenericDrone {
	public static caption = 'Missile Drones';
	public static description = `A drone bay which autonomously produces small UAV fitted with explosive missiles`;
	public static id = 65000;
	public baseAccuracy: number = this.baseAccuracy - 0.75;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map((v) => ({
		...this.attacks[0],
		[Damage.damageType.kinetic]: {
			min: 5,
			max: 15,
			distribution: 'Bell',
		},
		[Damage.damageType.thermodynamic]: {
			min: 5,
			max: 10,
			distribution: 'Uniform',
		},
	}));
}

export default MissileDrone;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericDrone } from './GenericDrone';

export class LaserDrone extends GenericDrone {
	public static caption = 'Laser Drones';
	public static description = `A drone bay which autonomously produces small UAV fitted with x-ray lasers`;
	public static id = 65001;
	public baseAccuracy: number = this.baseAccuracy + 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map((v) => ({
		...this.attacks[0],
		[Damage.damageType.magnetic]: {
			min: 5,
			max: 15,
			distribution: 'Bell',
		},
		[Damage.damageType.radiation]: {
			min: 5,
			max: 10,
			distribution: 'Uniform',
		},
	}));
}

export default LaserDrone;

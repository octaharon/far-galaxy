import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericDrone } from './GenericDrone';

export class AssaultDrone extends GenericDrone {
	public static caption = 'Assault Drones';
	public static description = `A manufacturing pipeline which fits strike drones with Meson bombs.
    The lack of precision is slightly compensated by AoE Damage`;
	public static id = 65002;
	public baseCost: number = this.baseCost + 5;
	public baseAccuracy: number = this.baseAccuracy - 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.antimatter]: {
				min: 15,
				max: 30,
				distribution: 'Uniform',
			},
			[Damage.damageType.radiation]: {
				min: 15,
				max: 30,
				distribution: 'Uniform',
			},
			radius: 2,
			magnitude: 1.6,
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			[UnitAttack.attackFlags.AoE]: true,
			[UnitAttack.attackFlags.Selective]: true,
			delivery: UnitAttack.deliveryType.drone,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default AssaultDrone;

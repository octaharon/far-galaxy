import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class ProtonMissile extends GenericMissile {
	public static id = 30004;
	public static caption = 'Proton Missile';
	public static description = `Medium range fire-and-forget missile with some plasma payload.
    Due to design error this model is prone to various malfunctions, but
    it's still the best value per price, so they're produced broadly`;
	public baseCost: number = this.baseCost + 4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.magnetic]: {
				min: 25,
				max: 80,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.kinetic]: {
				min: 20,
				max: 30,
				distribution: 'Uniform',
			},
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default ProtonMissile;

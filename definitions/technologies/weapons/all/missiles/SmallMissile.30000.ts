import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class SmallMissile extends GenericMissile {
	public static id = 30000;
	public static caption = 'Small Missile';
	public static description = `CLGP with small explosive warhead`;
	public baseRange: number = this.baseRange - 1;
	public baseAccuracy: number = this.baseAccuracy + 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.thermodynamic]: {
				min: 40,
				max: 60,
				distribution: 'MostlyMinimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default SmallMissile;

import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericMissile extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Missiles;
	public baseAccuracy = 1.3;
	public baseRate = 0.4;
	public baseCost = 14;
	public priority = ATTACK_PRIORITIES.NORMAL;
	public baseRange = 10.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {
				min: 10,
				max: 20,
				distribution: 'MostlyMinimal',
			},
			delivery: UnitAttack.deliveryType.missile,
		} as UnitAttack.TAttackDefinition,
	];
}

export default GenericMissile;

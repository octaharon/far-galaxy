import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class NapalmMissile extends GenericMissile {
	public static id = 30007;
	public static caption = 'Scorcher Missile';
	public static description = `A tactical missile with a highly flammable liquid payload.
    Creates a local department of hell at location of your choice`;
	public baseCost: number = this.baseCost + 6;
	public baseRange: number = this.baseRange - 1.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.heat]: {
				min: 10,
				max: 50,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.biological]: {
				min: 10,
				max: 30,
				distribution: 'Uniform',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			radius: 1.5,
			magnitude: 0.6,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			[Damage.damageType.heat]: {
				min: 10,
				max: 25,
				distribution: 'Gaussian',
			},
			[Damage.damageType.biological]: {
				min: 10,
				max: 25,
				distribution: 'Gaussian',
			},
			delivery: UnitAttack.deliveryType.surface,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default NapalmMissile;

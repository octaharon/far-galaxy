import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class NeutronMissile extends GenericMissile {
	public static id = 30009;
	public static caption = 'Neutron Missile';
	public static description = `A cluster warhead with "dirty bomb" design,
    exploding in the air and polluting everything in a huge area`;
	public baseCost: number = this.baseCost + 9;
	public baseRange: number = this.baseRange - 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.biological]: {
				min: 15,
				max: 30,
				distribution: 'Bell',
			},
			[Damage.damageType.radiation]: {
				min: 15,
				max: 30,
				distribution: 'Bell',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			radius: 2,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			[UnitAttack.attackFlags.AoE]: true,
			[Damage.damageType.radiation]: {
				min: 20,
				max: 40,
				distribution: 'Gaussian',
			},
			delivery: UnitAttack.deliveryType.bombardment,
		} as UnitAttack.TAOEAttackDefinition,
		{
			[Damage.damageType.radiation]: {
				min: 12,
				max: 19,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.immediate,
			duration: 3,
			[UnitAttack.attackFlags.DoT]: true,
		} as UnitAttack.TDOTAttackDefinition,
	];
}

export default NeutronMissile;

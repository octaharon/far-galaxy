import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class JerichoMissile extends GenericMissile {
	public static id = 30005;
	public static caption = 'Jericho Launcher';
	public static description = `Some crazy genius created a warhead capable of creating a
    nanoscale black hole at location, which soon explodes with a bunch of particles
    The only thing was missing is a self-propelled guided missile, and once it happened.
    Now it's a signature weapon of Pirates' lords`;
	public baseCost: number = this.baseCost + 10;
	public baseAccuracy: number = this.baseAccuracy - 0.4;
	public baseRange: number = this.baseRange - 1.5;
	public baseRate: number = this.baseRate + 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.warp]: {
				min: 15,
				max: 40,
				distribution: 'MostlyMinimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			radius: 1.5,
			magnitude: 0.6,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			[Damage.damageType.warp]: {
				min: 15,
				max: 30,
				distribution: 'Bell',
			},
			[Damage.damageType.magnetic]: {
				min: 10,
				max: 30,
				distribution: 'Bell',
			},
			delivery: UnitAttack.deliveryType.beam,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default JerichoMissile;

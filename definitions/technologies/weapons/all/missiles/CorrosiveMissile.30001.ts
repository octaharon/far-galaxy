import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class CorrosiveMissile extends GenericMissile {
	public static id = 30001;
	public static caption = 'Corrosive Missile';
	public static description = `CLGP with small chemical warhead carrying a mixture of two compounds, one of which
    activates slightly later`;
	public baseCost: number = this.baseCost + 2;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.biological]: {
				min: 15,
				max: 30,
				distribution: 'Bell',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.biological]: {
				min: 15,
				max: 30,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.immediate,
			[UnitAttack.attackFlags.Deferred]: true,
		} as UnitAttack.TDeferredAttackDefinition,
	];
}

export default CorrosiveMissile;

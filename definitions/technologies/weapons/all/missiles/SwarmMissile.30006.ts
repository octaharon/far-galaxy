import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class SwarmMissile extends GenericMissile {
	public static id = 30006;
	public static caption = 'Swarm Missiles';
	public static description = `A moving cloud of small missiles with thermobaric warheads,
    almost impossible to intercept or evade. The damage is as supreme as the production cost is.
    To make it affordable the fuel supply has to be cut, limiting effective targeting range`;
	public baseCost: number = this.baseCost + 10;
	public baseAccuracy = 1.8;
	public baseRange: number = this.baseRange - 4.5;
	public baseRate: number = this.baseRate - 0.1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			delivery: UnitAttack.deliveryType.missile,
			[Damage.damageType.thermodynamic]: {
				min: 10,
				max: 50,
				distribution: 'Uniform',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[UnitAttack.attackFlags.AoE]: true,
			radius: 2,
			magnitude: 2.5,
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			[Damage.damageType.heat]: {
				min: 15,
				max: 35,
				distribution: 'Gaussian',
			},
			[Damage.damageType.thermodynamic]: {
				min: 15,
				max: 35,
				distribution: 'Gaussian',
			},
			delivery: UnitAttack.deliveryType.cloud,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default SwarmMissile;

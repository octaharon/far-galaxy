import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class TacticalMissile extends GenericMissile {
	public static id = 30003;
	public static caption = 'Tactical Missile';
	public static description = `Anti-personnel guided missile, powerful enough to destroy isolated targets. Sometimes.`;
	public baseCost: number = this.baseCost + 6;
	public baseAccuracy: number = this.baseAccuracy + 0.4;
	public baseRate = 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.radiation]: {
				min: 30,
				max: 90,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.heat]: {
				min: 30,
				max: 45,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.magnetic]: {
				min: 30,
				max: 45,
				distribution: 'MostlyMinimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default TacticalMissile;

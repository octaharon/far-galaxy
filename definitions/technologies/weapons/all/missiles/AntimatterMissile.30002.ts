import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericMissile } from './GenericMissile';

export class AntimatterMissile extends GenericMissile {
	public static id = 30002;
	public static caption = 'Antimatter Missile';
	public static description = `CLGP with small gluon warhead, which annihilates on impact`;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			[Damage.damageType.antimatter]: {
				min: 40,
				max: 60,
				distribution: 'Minimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default AntimatterMissile;

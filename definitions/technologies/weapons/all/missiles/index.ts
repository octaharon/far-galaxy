import Weapons from '../../../types/weapons';

export const MissileWeapons: Weapons.IWeaponGroup = {
	caption: 'Homing Missiles',
	aliases: ['Homing Weapons', 'Homing'],
	description: {
		text: 'Self-propelled projectiles are a golden classic of warfare, versatile and efficient at many tasks, but never exceptional at any of them',
		lineOfSight: false,
		damage: 'normal',
		precision: 'high',
		range: 'high',
		rate: 'low',
	},
	id: Weapons.TWeaponGroupType.Missiles,
	weapons: [],
};
export default MissileWeapons;

import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import GenericProjectile from './GenericProjectile';

export class GravMortarWeapon extends GenericProjectile {
	public static caption = 'Grav Mortar';
	public static description = `A ballistic launcher that can offer explosive damage at substantial range even when reduced to size of a personal weapon`;
	public static id = 15001;
	public baseCost: number = this.baseCost + 8;
	public baseRate = 0.2;
	public priority: number = ATTACK_PRIORITIES.LOW;
	public baseRange: number = this.baseRange + 2;
	public baseAccuracy = 1.3;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.concat({
		...this.attacks[0],
		...({
			[Damage.damageType.kinetic]: {
				min: 20,
				max: 30,
				distribution: 'Uniform',
			},
			[Damage.damageType.thermodynamic]: {
				min: 40,
				max: 60,
				distribution: 'Bell',
			},
			[Damage.damageType.heat]: {
				min: 15,
				max: 45,
				distribution: 'Bell',
			},
			[UnitAttack.attackFlags.AoE]: true,
			magnitude: 0.8,
			radius: 1.5,
			delivery: UnitAttack.deliveryType.supersonic,
			aoeType: UnitAttack.TAoEDamageSplashTypes.cone90,
		} as UnitAttack.TAOEAttackDefinition),
	});
}

export default GravMortarWeapon;

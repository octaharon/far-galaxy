import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericProjectile from './GenericProjectile';

export class GravityMines extends GenericProjectile {
	public static caption = 'Gravity Mines';
	public static description = `Short range weapon of mass destruction, creating
    a local gravity anomality at the point of impact, which sometimes can be empowered drastically
    by energy sources around`;
	public static id = 15003;
	public baseCost: number = this.baseCost + 4;
	public baseRange = 6;
	public baseAccuracy = 1.1;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.concat({
		...this.attacks[0],
		...({
			[Damage.damageType.kinetic]: {
				min: 15,
				max: 25,
				distribution: 'Bell',
			},
			[Damage.damageType.warp]: {
				min: 10,
				max: 50,
				distribution: 'Minimal',
			},
			[Damage.damageType.magnetic]: {
				min: 10,
				max: 50,
				distribution: 'Minimal',
			},
			[UnitAttack.attackFlags.AoE]: true,
			magnitude: 1,
			radius: 1,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			delivery: UnitAttack.deliveryType.beam,
		} as UnitAttack.TAOEAttackDefinition),
	});
}

export default GravityMines;

import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericProjectile extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Launchers;
	public baseAccuracy = 0.9;
	public baseRate = 0.7;
	public baseCost = 10;
	public priority = ATTACK_PRIORITIES.LOW;
	public baseRange = 6;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {
				min: 5,
				max: 10,
				distribution: 'Bell',
			},
			delivery: UnitAttack.deliveryType.bombardment,
		},
	];
}

export default GenericProjectile;

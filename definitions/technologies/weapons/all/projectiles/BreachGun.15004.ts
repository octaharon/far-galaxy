import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericProjectile from './GenericProjectile';

export class BreachGun extends GenericProjectile {
	public static caption = 'BREACH Gun';
	public static description = `Pirates have reversed this weapon from some mischievously acquired blueprints and, at first, used it to suppress planetary defense lines. Now it's evolved into a mountable zone control weapon that is great against densely packed groups of foes`;
	public static id = 15004;
	public baseCost: number = this.baseCost + 3;
	public baseRange = 6;
	public baseRate = 0.4;
	public baseAccuracy = 0.85;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.bombardment,
			[Damage.damageType.kinetic]: {
				min: 25,
				max: 55,
				distribution: 'Uniform',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			delivery: UnitAttack.deliveryType.ballistic,
			[UnitAttack.attackFlags.AoE]: true,
			[Damage.damageType.thermodynamic]: {
				min: 25,
				max: 40,
				distribution: 'Gaussian',
			},
			radius: 1.5,
			magnitude: 1,
			aoeType: UnitAttack.TAoEDamageSplashTypes.lineAside,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default BreachGun;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericProjectile from './GenericProjectile';

export class CorrosiveMines extends GenericProjectile {
	public static caption = 'Corrosive Mines';
	public static description = `Short range weapon of mass destruction,
        releasing a variety of volatile chemical agents in an area around impact.
        Due to unstable character of the compound it's not very reilable at times,
        but at other times it's damage can be critical`;
	public static id = 15002;
	public baseCost: number = this.baseCost + 4;
	public baseRange = 6;
	public baseAccuracy = 0.7;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.concat(
		{
			...this.attacks[0],
			...({
				[Damage.damageType.kinetic]: {
					min: 15,
					max: 35,
					distribution: 'MostlyMinimal',
				},
				[Damage.damageType.biological]: {
					min: 10,
					max: 50,
					distribution: 'Bell',
				},
				[UnitAttack.attackFlags.AoE]: true,
				magnitude: 0.6,
				radius: 2,
				aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
				delivery: UnitAttack.deliveryType.cloud,
			} as UnitAttack.TAOEAttackDefinition),
		},
		{
			delivery: UnitAttack.deliveryType.immediate,
			[UnitAttack.attackFlags.Deferred]: true,
			[Damage.damageType.heat]: {
				min: 10,
				max: 50,
				distribution: 'Minimal',
			},
		} as UnitAttack.TAOEAttackDefinition,
	);
}

export default CorrosiveMines;

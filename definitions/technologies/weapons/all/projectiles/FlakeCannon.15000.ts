import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericProjectile from './GenericProjectile';

export class FlakeCannon extends GenericProjectile {
	public static caption = 'Flake Cannon';
	public static description = `Slow, humongous and deadly weapon firing chunks of highly ionized and overheated
        metallic particles, that burn through most things at very short distance`;
	public static id = 15000;
	public baseCost = 10;
	public baseRate = 0.35;
	public baseRange = 0.5;
	public baseAccuracy = 1.1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.bombardment,
			[Damage.damageType.kinetic]: {
				min: 10,
				max: 20,
				distribution: 'Uniform',
			},
			[Damage.damageType.heat]: {
				min: 10,
				max: 25,
				distribution: 'MostlyMaximal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			delivery: UnitAttack.deliveryType.cloud,
			[Damage.damageType.kinetic]: {
				min: 15,
				max: 55,
				distribution: 'Maximal',
			},
			[Damage.damageType.magnetic]: {
				min: 20,
				max: 35,
				distribution: 'Uniform',
			},
			[Damage.damageType.heat]: {
				min: 20,
				max: 35,
				distribution: 'Uniform',
			},
			[UnitAttack.attackFlags.AoE]: true,
			magnitude: 0.5,
			radius: 4,
			aoeType: UnitAttack.TAoEDamageSplashTypes.cone90,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default FlakeCannon;

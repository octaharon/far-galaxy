import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericProjectile from './GenericProjectile';

export class PoisonMines extends GenericProjectile {
	public static caption = 'Poison Grenades';
	public static description = `This anti-infantry short-range weapon creates a cloud of gas in the area which
    is extremely toxic for living beings, poisoning them for considerable amount of time`;
	public static id = 15005;
	public baseCost: number = this.baseCost + 7;
	public baseRange = 6;
	public baseAccuracy = 0.9;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.bombardment,
			[Damage.damageType.kinetic]: {
				min: 15,
				max: 35,
				distribution: 'Bell',
			},
			[Damage.damageType.biological]: {
				min: 15,
				max: 50,
				distribution: 'MostlyMinimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.biological]: {
				min: 10,
				max: 15,
				distribution: 'MostlyMaximal',
			},
			delivery: UnitAttack.deliveryType.cloud,
			[UnitAttack.attackFlags.AoE]: true,
			[UnitAttack.attackFlags.DoT]: true,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			magnitude: 1,
			radius: 1.5,
			duration: 5,
		} as UnitAttack.TAOEAttackDefinition,
		{
			[Damage.damageType.thermodynamic]: {
				min: 10,
				max: 25,
				distribution: 'Uniform',
			},
			[UnitAttack.attackFlags.AoE]: true,
			radius: 1.5,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			magnitude: 0.5,
			delivery: UnitAttack.deliveryType.ballistic,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default PoisonMines;

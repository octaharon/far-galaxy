import Weapons from '../../../types/weapons';

export const ProjectileWeapons: Weapons.IWeaponGroup = {
	caption: 'Launchers',
	aliases: ['Launcher Weapons'],
	description: {
		text: 'Medium-caliber zone control artillery that only come in lesser models, still being pretty good response to heavily armored Foes, especially Naval ones. A good choice for frontline Units, that delivers cheap AoE Damage of various kinds with Bombardment Attacks',
		lineOfSight: false,
		damage: 'high',
		range: 'low',
		rate: 'low',
	},
	id: Weapons.TWeaponGroupType.Launchers,
	weapons: [],
};
export default ProjectileWeapons;

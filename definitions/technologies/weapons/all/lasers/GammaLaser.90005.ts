import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericLaser from './GenericLaser';

export class GammaLaser extends GenericLaser {
	public static caption = 'Gamma Laser';
	public static description = `Any electromagnetic waves can be focused and cohered, gamma rays are
    no exception. So it's a shortest wavelength laser available, yielding some range and precision
    advantage over conventional lasers, as well as serious radiation damage`;
	public static id = 90005;
	public baseAccuracy: number = this.baseAccuracy + 1.2;
	public baseRange: number = this.baseRange + 1;
	public baseRate = 0.25;
	public baseCost: number = this.baseCost + 5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.heat]: {
				min: 20,
				max: 40,
				distribution: 'MostlyMaximal',
			},
			[Damage.damageType.radiation]: {
				min: 15,
				max: 45,
				distribution: 'Minimal',
			},
		},
	];
}

export default GammaLaser;

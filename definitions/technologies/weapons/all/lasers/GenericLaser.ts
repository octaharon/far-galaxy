import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericLaser extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Lasers;
	public baseAccuracy = 1.3;
	public baseRate = 0.5;
	public baseCost = 13;
	public priority = ATTACK_PRIORITIES.HIGHER;
	public baseRange = 9.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.heat]: {
				min: 25,
				max: 40,
				distribution: 'MostlyMaximal',
			},
		},
	];
}

export default GenericLaser;

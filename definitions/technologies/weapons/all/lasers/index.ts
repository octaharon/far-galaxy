import Weapons from '../../../types/weapons';

export const LaserWeapons: Weapons.IWeaponGroup = {
	caption: 'Lasers',
	aliases: ['Laser Weapons'],
	description: {
		text: 'A dedicated kind of energy weapons that offers significant HEA Damage in direct sight, being superior at finishing off wounded targets',
		lineOfSight: true,
		damage: 'normal',
		precision: 'high',
		range: 'normal',
		rate: 'normal',
	},
	id: Weapons.TWeaponGroupType.Lasers,
	weapons: [],
};
export default LaserWeapons;

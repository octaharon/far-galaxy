import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericLaser from './GenericLaser';

export class GovernorGun extends GenericLaser {
	public static caption = 'The Governor';
	public static description = `Laser rotary cannon. Like a minigun, right, but firing lasers.
    With slightly worse precision it offers an outstanding firepower, and it looks cool.`;
	public static id = 90003;
	public baseAccuracy: number = this.baseAccuracy - 0.1;
	public baseRate: number = this.baseRate + 0.7;
	public baseCost: number = this.baseCost + 6;
	public baseRange: number = this.baseRange - 1.5;
	public attacks: UnitAttack.TAttackDefinition[] = Array(3)
		.fill(0)
		.map((v: number) => ({
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.heat]: {
				min: 6,
				max: 24,
				distribution: 'Maximal',
			},
		}));
}

export default GovernorGun;

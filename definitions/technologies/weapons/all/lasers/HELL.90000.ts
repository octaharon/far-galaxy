import GenericLaser from './GenericLaser';

export class HELLGun extends GenericLaser {
	public static caption = 'HE-LL';
	public static description = `High Energy Liquid Laser. It's really not as dangerous as it sounds, but very reliable`;
	public static id = 90000;
}

export default HELLGun;

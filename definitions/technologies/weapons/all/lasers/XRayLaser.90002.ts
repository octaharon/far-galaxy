import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericLaser from './GenericLaser';

export class XRayLaser extends GenericLaser {
	public static caption = 'X-Ray Laser';
	public static description = `Have you ever seen an invisible laser? Well, there is one.
    Shorter wavelength means less range in favor of precision and damage.`;
	public static id = 90002;
	public baseCost: number = this.baseCost + 3;
	public baseRange: number = this.baseRange - 1;
	public baseAccuracy: number = this.baseAccuracy + 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.heat]: {
				min: 20,
				max: 40,
				distribution: 'Bell',
			},
			[Damage.damageType.magnetic]: {
				min: 5,
				max: 15,
				distribution: 'Minimal',
			},
			[Damage.damageType.radiation]: {
				min: 5,
				max: 15,
				distribution: 'Maximal',
			},
		},
	];
}

export default XRayLaser;

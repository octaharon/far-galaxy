import GenericLaser from './GenericLaser';

export class PALMGun extends GenericLaser {
	public static caption = 'PALM';
	public static description = `Phased Array Laser Mount: Extremely accurate laser weapon with moderate damage and increased range`;
	public static id = 90001;
	public baseCost: number = this.baseCost + 4;
	public baseAccuracy = 2;
	public baseRange: number = this.baseRange + 1.5;
	public baseRate: number = this.baseRate - 0.1;
}

export default PALMGun;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericLaser from './GenericLaser';

export class PrismGun extends GenericLaser {
	public static caption = 'PRISM';
	public static description = `Laser weapon platform purposed to provide barrage fire in area`;
	public static id = 90004;
	public baseCost: number = this.baseCost + 4;
	public baseRange: number = this.baseRange - 4;
	public baseRate: number = this.baseRate + 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.heat]: {
				min: 25,
				max: 50,
				distribution: 'MostlyMaximal',
			},
			magnitude: 0.7,
			radius: 3,
			aoeType: UnitAttack.TAoEDamageSplashTypes.cone180,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default PrismGun;

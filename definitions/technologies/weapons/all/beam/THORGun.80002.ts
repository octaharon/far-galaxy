import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBeam from './GenericBeam';

export class THORGun extends GenericBeam {
	public static caption = 'THOR';
	public static description = `Advanced version of Lightning Gun, which highlights few targets
    in an area, allowing the discharge to travel between them. Fear the thunder!`;
	public static id = 80002;
	public baseCost: number = this.baseCost + 7;
	public baseRange: number = this.baseRange - 1.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.magnetic]: {
				min: 45,
				max: 85,
				distribution: 'Bell',
			},
			[Damage.damageType.heat]: {
				min: 20,
				max: 40,
				distribution: 'Parabolic',
			},
			radius: 3,
			magnitude: 2,
			aoeType: UnitAttack.TAoEDamageSplashTypes.chain,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default THORGun;

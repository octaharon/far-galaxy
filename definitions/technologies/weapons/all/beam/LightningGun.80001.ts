import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBeam from './GenericBeam';

export class LightningGun extends GenericBeam {
	public static caption = 'Lighting Gun';
	public static description = `First shoots a laser at target to ionize the air
    and create a pathway for electrons, then dumps the charge off capacitor.
    We have taken what nature had invented and made it better.
    More accurate. Deadlier.`;
	public static id = 80001;
	public baseCost: number = this.baseCost + 6;
	public baseRate = 0.25;
	public baseRange: number = this.baseRange + 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.magnetic]: {
				min: 90,
				max: 130,
				distribution: 'Bell',
			},
			[Damage.damageType.radiation]: {
				min: 25,
				max: 45,
				distribution: 'Parabolic',
			},
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default LightningGun;

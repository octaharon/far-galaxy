import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBeam from './GenericBeam';

export class MicrowavePhaseArrayWeapon extends GenericBeam {
	public static caption = 'Superheater';
	public static description = `High precision microwave phase array makes for a top class directed energy weapon that
    penetrates through smaller entities and BBQ's them up!`;
	public static id = 80000;
	public baseCost: number = this.baseCost;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.heat]: {
				min: 40,
				max: 80,
				distribution: 'Bell',
			},
			[Damage.damageType.magnetic]: {
				min: 20,
				max: 40,
				distribution: 'Parabolic',
			},
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default MicrowavePhaseArrayWeapon;

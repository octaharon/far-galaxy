import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBeam from './GenericBeam';

export class QuantumExciter extends GenericBeam {
	public static caption = 'Quantum Exciter';
	public static description = `A Directed Energy Weapon emitting modulated microwaves, creating local subatomic anomalies along the way`;
	public static id = 80004;
	public baseCost: number = this.baseCost + 4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.warp]: {
				min: 40,
				max: 60,
				distribution: 'Bell',
			},
			[Damage.damageType.biological]: {
				min: 20,
				max: 40,
				distribution: 'Parabolic',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			delivery: UnitAttack.deliveryType.immediate,
			[UnitAttack.attackFlags.Deferred]: true,
			[Damage.damageType.biological]: {
				min: 8,
				max: 24,
				distribution: 'Maximal',
			},
			[Damage.damageType.warp]: {
				min: 8,
				max: 32,
				distribution: 'Minimal',
			},
		} as UnitAttack.TDeferredAttackDefinition,
	];
}

export default QuantumExciter;

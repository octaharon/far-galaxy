import Weapons from '../../../types/weapons';

export const EnergyWeapons = {
	caption: 'Energy Weapons',
	aliases: ['DEW'],
	description: {
		text: 'Directed energy weapons fire a projected energy beam at the target, being exceptionally good against unshielded Units',
		lineOfSight: true,
		damage: 'high',
		precision: 'normal',
		range: 'normal',
	},
	id: Weapons.TWeaponGroupType.Beam,
	weapons: [],
} as Weapons.IWeaponGroup;
export default EnergyWeapons;

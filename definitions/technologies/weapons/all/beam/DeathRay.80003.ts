import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBeam from './GenericBeam';

export class DeathRay extends GenericBeam {
	public static caption = 'Death Ray';
	public static description = `A antineutrino laser phased array, made basically of rocks and sticks. A somewhat inefficient design, but a handful of crude firepower. A massive discharge burns through anything, leaving only flames and desolation behind`;
	public static id = 80003;
	public baseCost: number = this.baseCost + 8;
	public baseRate = 0.18;
	public baseAccuracy: number = this.baseAccuracy - 0.15;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.antimatter]: {
				min: 90,
				max: 130,
				distribution: 'Bell',
			},
			[Damage.damageType.radiation]: {
				min: 25,
				max: 65,
				distribution: 'Parabolic',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[UnitAttack.attackFlags.DoT]: true,
			duration: 5,
			[Damage.damageType.radiation]: {
				min: 20,
				max: 40,
				distribution: 'Maximal',
			},
			[Damage.damageType.heat]: {
				min: 20,
				max: 40,
				distribution: 'Minimal',
			},
			delivery: UnitAttack.deliveryType.immediate,
		} as UnitAttack.TDOTAttackDefinition,
	];
}

export default DeathRay;

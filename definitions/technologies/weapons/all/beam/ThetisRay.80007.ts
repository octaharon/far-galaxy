import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBeam from './GenericBeam';

export class ThetisRay extends GenericBeam {
	public static caption = 'THETIS';
	public static description = `Transportable Higgs Energy Targeting / Infiltration System.
    A bleeding edge computerized weapon which autonomously locates the most vulnerable
    spots on target and creates a matter displacement there,
    bypassing most conventional defenses. To emphasize that,
    the name was given: Thetis was the one who charmed an arrow
    which later hit Achilles in his notorious heel`;
	public static id = 80007;
	public baseCost: number = this.baseCost + 6;
	public baseRange: number = this.baseRange - 1;
	public baseAccuracy: number = this.baseAccuracy + 0.4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[UnitAttack.attackFlags.Disruptive]: true,
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.warp]: {
				min: 20,
				max: 50,
				distribution: 'Bell',
			},
			[Damage.damageType.biological]: {
				min: 20,
				max: 50,
				distribution: 'Bell',
			},
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default ThetisRay;

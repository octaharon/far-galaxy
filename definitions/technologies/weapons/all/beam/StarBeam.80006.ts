import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBeam from './GenericBeam';

export class StarBeam extends GenericBeam {
	public static caption = 'Star Beam';
	public static description = `A rapid fire directed energy weapon,
    continiously bursting through targets in a line. Nevertheless, because
    each burst carries less energy, the beam dissipates faster, limiting
    effective range and accuracy`;
	public static id = 80006;
	public baseCost: number = this.baseCost + 7;
	public baseRange: number = this.baseRange - 3;
	public baseRate = 4;
	public baseAccuracy: number = this.baseAccuracy - 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(3).fill(0).map(
		() =>
			({
				...this.attacks[0],
				delivery: UnitAttack.deliveryType.beam,
				[Damage.damageType.magnetic]: {
					min: 10,
					max: 20,
					distribution: 'Bell',
				},
				[Damage.damageType.heat]: {
					min: 10,
					max: 20,
					distribution: 'Parabolic',
				},
			} as UnitAttack.TDirectAttackDefinition),
	);
}

export default StarBeam;

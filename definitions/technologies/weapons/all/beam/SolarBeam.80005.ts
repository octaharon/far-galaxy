import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBeam from './GenericBeam';

export class SolarBeam extends GenericBeam {
	public static caption = 'Solar Beam';
	public static description = `A directed energy weapon with piped capacitors,
    allowing to recharge faster, but limiting maximum power output`;
	public static id = 80005;
	public baseCost: number = this.baseCost + 1;
	public baseRate: number = this.baseRate + 0.3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			...this.attacks[0],
			delivery: UnitAttack.deliveryType.beam,
			[Damage.damageType.magnetic]: {
				min: 30,
				max: 70,
				distribution: 'Bell',
			},
			[Damage.damageType.heat]: {
				min: 20,
				max: 40,
				distribution: 'Parabolic',
			},
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default SolarBeam;

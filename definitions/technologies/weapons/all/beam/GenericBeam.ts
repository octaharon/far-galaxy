import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericBeam extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Beam;
	public baseAccuracy = 1.2;
	public baseRate = 0.5;
	public baseCost = 18;
	public priority = ATTACK_PRIORITIES.HIGHER;
	public baseRange = 8;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.beam,
			[UnitAttack.attackFlags.Penetrative]: true,
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default GenericBeam;

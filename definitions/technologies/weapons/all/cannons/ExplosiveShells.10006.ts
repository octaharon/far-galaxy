import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericShells from './GenericShells';

export class ExplosiveShells extends GenericShells {
	public static caption = 'Explosive shells';
	public static description = `High explosives in a fragmentation casing,
        delivered right at your foes by artillery - as simple as that. You can run but you can't hide...`;
	public static id = 10006;
	public baseCost: number = this.baseCost + 4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {
				min: 5,
				max: 35,
				distribution: 'MostlyMaximal',
			},
			delivery: UnitAttack.deliveryType.ballistic,
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.kinetic]: {
				min: 5,
				max: 15,
				distribution: 'Uniform',
			},
			[Damage.damageType.heat]: {
				min: 5,
				max: 15,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.bombardment,
			[UnitAttack.attackFlags.AoE]: true,
			radius: 1,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default ExplosiveShells;

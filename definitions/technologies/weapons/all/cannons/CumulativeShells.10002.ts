import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericShells from './GenericShells';

export class CumulativeShells extends GenericShells {
	public static caption = 'Cumulative shells';
	public static description = "Artillery firing HEAT warheads. It's a hell to be inside anything hit by this.";
	public static id = 10002;
	public baseCost: number = this.baseCost + 7;
	public baseRange: number = this.baseRange - 1;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		[Damage.damageType.kinetic]: {
			min: 5,
			max: 45,
			distribution: 'Bell',
		},
		[Damage.damageType.thermodynamic]: {
			min: 15,
			max: 45,
			distribution: 'Maximal',
		},
	}));
}

export default CumulativeShells;

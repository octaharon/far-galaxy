import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { ExplosiveShells } from './ExplosiveShells.10006';

export class TauShells extends ExplosiveShells {
	public static caption = 'Tau shells';
	public static description = `Each of these warheads carries a microscopic quasistable black hole,
        which collapses on hit, causing some local gravitational anomalies and occasional particle showers.
        It's highly recommended to keep all living beings as far as possible when it happens`;
	public static id = 10007;
	public baseCost: number = this.baseCost + 7;
	public baseRate: number = this.baseRate - 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = [this.attacks[0]].concat([
		{
			[Damage.damageType.warp]: {
				min: 15,
				max: 50,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.antimatter]: {
				min: 25,
				max: 45,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.beam,
			[UnitAttack.attackFlags.AoE]: true,
			radius: 1,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
		} as UnitAttack.TAOEAttackDefinition,
	]);
}

export default TauShells;

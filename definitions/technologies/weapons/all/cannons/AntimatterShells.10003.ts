import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericShells from './GenericShells';

export class AntimatterShells extends GenericShells {
	public static caption = 'Antimatter shells';
	public static description = `Good old artillery with a new flavour: a flavour of annihilation.`;
	public static id = 10003;
	public baseCost: number = this.baseCost + 6;

	public baseAccuracy: number = this.baseAccuracy + 0.15;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		[Damage.damageType.kinetic]: {
			min: 5,
			max: 45,
			distribution: 'Bell',
		},
		[Damage.damageType.antimatter]: {
			min: 15,
			max: 30,
			distribution: 'Maximal',
		},
	}));
}

export default AntimatterShells;

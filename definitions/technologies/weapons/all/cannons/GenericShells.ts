import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericShells extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Cannons;
	public baseAccuracy = 0.85;
	public baseRate = 0.5;
	public baseCost = 8;
	public priority = ATTACK_PRIORITIES.NORMAL;
	public baseRange = 7.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {
				min: 5,
				max: 45,
				distribution: 'Bell',
			},
			delivery: UnitAttack.deliveryType.ballistic,
		} as UnitAttack.TAttackDefinition,
	];
}

export default GenericShells;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { ExplosiveShells } from './ExplosiveShells.10006';

export class VacuumShells extends ExplosiveShells {
	public static caption = 'Thermobaric shells';
	public static description = `There are only two good things with vacuum: a vacuum cleaner and a vacuum bomb. Luckily this is one of them.`;
	public static id = 10008;
	public baseCost: number = this.baseCost + 6;
	public baseRate: number = this.baseRate - 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = [this.attacks[0]].concat([
		{
			[Damage.damageType.thermodynamic]: {
				min: 5,
				max: 35,
				distribution: 'Uniform',
			},
			[Damage.damageType.heat]: {
				min: 25,
				max: 50,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.cloud,
			[UnitAttack.attackFlags.AoE]: true,
			radius: 1,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
		} as UnitAttack.TAOEAttackDefinition,
	]);
}

export default VacuumShells;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericShells from './GenericShells';

export class ProtonShells extends GenericShells {
	public static caption = 'Proton shells';
	public static description = `A small fission charge in these shells radiates some Alpha-rays directly into target
        in addition to a kinetic impact.
        Surely that doesn't make the target feel better`;
	public static id = 10001;
	public baseCost: number = this.baseCost + 4;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		[Damage.damageType.kinetic]: {
			min: 10,
			max: 45,
			distribution: 'Bell',
		},
		[Damage.damageType.radiation]: {
			min: 4,
			max: 25,
			distribution: 'Maximal',
		},
	}));
}

export default ProtonShells;

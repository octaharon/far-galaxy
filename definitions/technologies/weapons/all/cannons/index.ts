import Weapons from '../../../types/weapons';

export const CannonWeapons = {
	caption: 'Cannons',
	aliases: ['Cannon Weapons'],
	description: {
		text: 'These are jack-of-all-trades in any arsenal, providing direct laying fire with ballistic ammunition, that is especially good versus Shields',
		lineOfSight: true,
		damage: 'low',
		precision: 'low',
		range: 'low',
		rate: 'normal',
	},
	id: Weapons.TWeaponGroupType.Cannons,
	weapons: [],
} as Weapons.IWeaponGroup;
export default CannonWeapons;

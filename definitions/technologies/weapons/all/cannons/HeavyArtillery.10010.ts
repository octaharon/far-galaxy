import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericShells from './GenericShells';

export class HeavyArtillery extends GenericShells {
	public static caption = 'Heavy Artillery';
	public static description =
		'Long range artillery isWeapon with HEAT shells, which can keep most enemies afar for a price of low fire rate';
	public static id = 10010;
	public baseCost: number = this.baseCost + 10;
	public baseRate = 0.25;
	public baseRange: number = this.baseRange + 4;
	public baseAccuracy = 1.1;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		...{
			[Damage.damageType.kinetic]: {
				min: 15,
				max: 55,
				distribution: 'Bell',
			},
			[Damage.damageType.thermodynamic]: {
				min: 25,
				max: 80,
				distribution: 'Minimal',
			},
		},
	}));
}

export default HeavyArtillery;

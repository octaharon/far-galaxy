import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { ExplosiveShells } from './ExplosiveShells.10006';

export class NukeShells extends ExplosiveShells {
	public static caption = 'Nuke shells';
	public static id = 10005;
	public static description = `Just a nuke small enough to fit into conventional artillery shells.
        Did you ever want to have a small Chernobyl of yours? Now you finally can.`;
	public baseRange: number = this.baseRange + 1;
	public baseRate: number = this.baseRate - 0.3;
	public baseAccuracy: number = this.baseAccuracy + 0.25;
	public attacks: UnitAttack.TAttackDefinition[] = [this.attacks[0]].concat([
		{
			[Damage.damageType.radiation]: {
				min: 25,
				max: 45,
				distribution: 'Uniform',
			},
			[Damage.damageType.magnetic]: {
				min: 20,
				max: 50,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.beam,
			[UnitAttack.attackFlags.AoE]: true,
			radius: 3,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
		} as UnitAttack.TAOEAttackDefinition,
	]);
	public baseCost: number = this.baseCost + 10;
}

export default NukeShells;

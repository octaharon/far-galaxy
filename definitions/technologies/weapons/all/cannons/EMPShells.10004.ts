import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericShells from './GenericShells';

export class EMPShells extends GenericShells {
	public static caption = 'EMP Shells';
	public static description = `Ever thought of making a shell with a capacitor effect?
        And with the latest advancements in nanotechnologies we can now create a tightly packed coil
        in that small volume, allowing the capacitor to gain a humongous charge while travelling,
        effectively turning the projectile into ball lightning. Rubber shoes won't save 'em`;
	public static id = 10004;
	public baseCost: number = this.baseCost + 4;
	public baseRange: number = this.baseRange - 1;
	public baseAccuracy: number = this.baseAccuracy + 0.15;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		[Damage.damageType.kinetic]: {
			min: 5,
			max: 35,
			distribution: 'Bell',
		},
		[Damage.damageType.magnetic]: {
			min: 5,
			max: 35,
			distribution: 'Maximal',
		},
	}));
}

export default EMPShells;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericShells from './GenericShells';

export class FuzeShells extends GenericShells {
	public static caption = 'Fuze shells';
	public static description = `Classical artillery firing APDS made of the most common metals available.
         Not too much firepower, but still better than a stick`;
	public static id = 10000;
	public baseAccuracy: number = this.baseAccuracy + 0.15;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.map((o) => ({
		...o,
		[Damage.damageType.kinetic]: {
			min: 10,
			max: 45,
			distribution: 'Bell',
		},
	}));
}

export default FuzeShells;

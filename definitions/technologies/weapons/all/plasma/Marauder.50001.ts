import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericPlasma from './GenericPlasma';

export class MaradeurWeapon extends GenericPlasma {
	public static id = 50001;
	public static caption = 'MARAUDER';
	public static description = `Magnetically accelerated ring to achieve ultrahigh directed energy and radiation.
    Damn right, it's a plasma mass driver: deadly even at high range, but no as accurate as its less heavy counterparts`;
	public baseCost: number = this.baseCost + 9;
	public baseRange: number = this.baseRange + 5;
	public baseRate: number = this.baseRate - 0.3;
	public baseAccuracy: number = this.baseAccuracy + 0.1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.heat),
			[Damage.damageType.kinetic]: {
				min: 40,
				max: 120,
				distribution: 'Minimal',
			},
			[Damage.damageType.heat]: {
				min: 30,
				max: 50,
				distribution: 'Gaussian',
			},
			[UnitAttack.attackFlags.Penetrative]: true,
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default MaradeurWeapon;

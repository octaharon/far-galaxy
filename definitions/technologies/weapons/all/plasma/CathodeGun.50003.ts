import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericPlasma from './GenericPlasma';

export class CathodeGun extends GenericPlasma {
	public static id = 50003;
	public static caption = 'Cathode Cannon';
	public static description = `A highly advanced positron plasma railgun,
    having enough energy to cut through anything and occasionally set it on fire.
    Maintaining this power is costly, recharge time is immense and the range is limited
    due to dissipation and blooming.`;
	public baseCost: number = this.baseCost + 5;
	public baseRate = 0.3;
	public baseAccuracy: number = this.baseAccuracy + 0.4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.heat),
			[Damage.damageType.antimatter]: {
				min: 50,
				max: 150,
				distribution: 'MostlyMinimal',
			},
			[Damage.damageType.magnetic]: {
				min: 20,
				max: 40,
				distribution: 'Gaussian',
			},
			[UnitAttack.attackFlags.Penetrative]: true,
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.heat]: {
				min: 30,
				max: 45,
				distribution: 'Uniform',
			},
			[Damage.damageType.radiation]: {
				min: 10,
				max: 20,
				distribution: 'Gaussian',
			},
			[UnitAttack.attackFlags.Deferred]: true,
			delivery: UnitAttack.deliveryType.immediate,
		} as UnitAttack.TDeferredAttackDefinition,
	];
}

export default CathodeGun;

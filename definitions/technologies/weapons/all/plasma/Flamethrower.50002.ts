import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericPlasma from './GenericPlasma';

export class FlamethrowerWeapon extends GenericPlasma {
	public static id = 50002;
	public static caption = 'Flamethrower';
	public static description = `A coil version of a flamethrower using overheated ionic liquid for ammunition,
    which sticks to the target and slowly discharges, being almost impossible to douse`;
	public baseCost: number = this.baseCost + 2;
	public baseRange: number = this.baseRange - 2;
	public baseRate: number = this.baseRate * 2;
	public baseAccuracy: number = this.baseAccuracy + 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.heat),
			delivery: UnitAttack.deliveryType.cloud,
			[Damage.damageType.heat]: {
				min: 20,
				max: 70,
				distribution: 'Minimal',
			},
			[Damage.damageType.biological]: {
				min: 20,
				max: 30,
				distribution: 'Gaussian',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[UnitAttack.attackFlags.DoT]: true,
			[Damage.damageType.heat]: {
				min: 10,
				max: 20,
				distribution: 'Uniform',
			},
			[Damage.damageType.thermodynamic]: {
				min: 10,
				max: 20,
				distribution: 'Gaussian',
			},
			delivery: UnitAttack.deliveryType.immediate,
			duration: 2,
		} as UnitAttack.TDOTAttackDefinition,
	];
}

export default FlamethrowerWeapon;

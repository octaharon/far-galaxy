import Weapons from '../../../types/weapons';

export const PlasmaWeapons: Weapons.IWeaponGroup = {
	caption: 'Plasma Weapons',
	aliases: ['Plasma'],
	description: {
		text: 'Overheated ions can pierce through everything in direct sight, delivering lethal blows to weakened enemies with HEA Damage. Some varieties are also efficient against Shields',
		lineOfSight: true,
		damage: 'veryhigh',
		precision: 'low',
		range: 'low',
	},
	id: Weapons.TWeaponGroupType.Plasma,
	weapons: [],
};
export default PlasmaWeapons;

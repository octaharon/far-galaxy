import GenericPlasma from './GenericPlasma';

export class PlasmaGun extends GenericPlasma {
	public static id = 50000;
	public static caption = 'Plasma gun';
	public static description = `A coil cannon which evaporates it's slug into a molten plazma donut,
    travelling at supersonic speeds but dissipating quickly. Burns most targets to dust, assuming
    they got hit in the first place`;
	public baseRange: number = this.baseRange + 1.5;
}

export default PlasmaGun;

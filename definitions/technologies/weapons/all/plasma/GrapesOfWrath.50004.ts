import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import GenericPlasma from './GenericPlasma';

export class GrapesOfWrath extends GenericPlasma {
	public static id = 50004;
	public static caption = 'Grapes Of Wrath';
	public static description = `Behold: plasma grapeshot cannon.
    It's hard to say what is more ridiculous: the damage or the production cost.
    If this was an RPG game, it definitely would be a legendary weapon!`;
	public baseCost: number = this.baseCost + 15;
	public baseRate = 0.2;
	public baseRange = 3;
	public priority: number = ATTACK_PRIORITIES.HIGH;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.heat]: {
				min: 20,
				max: 100,
				distribution: 'MostlyMaximal',
			},
			[Damage.damageType.magnetic]: {
				min: 50,
				max: 70,
				distribution: 'Gaussian',
			},
			[UnitAttack.attackFlags.AoE]: true,
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			magnitude: 2,
			radius: 1.5,
			delivery: UnitAttack.deliveryType.supersonic,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default GrapesOfWrath;

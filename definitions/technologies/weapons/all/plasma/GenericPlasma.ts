import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericPlasma extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Plasma;
	public baseAccuracy = 0.9;
	public baseRate = 0.6;
	public baseCost = 14;
	public priority = ATTACK_PRIORITIES.LOWEST;
	public baseRange = 6.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.heat]: {
				min: 50,
				max: 150,
				distribution: 'MostlyMinimal',
			},
			delivery: UnitAttack.deliveryType.supersonic,
		} as UnitAttack.TAttackDefinition,
	];
}

export default GenericPlasma;

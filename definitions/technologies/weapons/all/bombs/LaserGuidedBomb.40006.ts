import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class LaserGuidedBomb extends GenericBomb {
	public static id = 40006;
	public static caption = 'Laser guided bomb';
	public static description = `An aviation bomb with assisted laser guidance and a fragmentary warhead`;
	public baseCost: number = this.baseCost + 2;
	public baseAccuracy: number = this.baseAccuracy + 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.thermodynamic),
			[Damage.damageType.thermodynamic]: {
				min: 60,
				max: 100,
				distribution: 'Minimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.kinetic]: {
				min: 20,
				max: 40,
				distribution: 'Bell',
			},
			radius: 2,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			delivery: UnitAttack.deliveryType.ballistic,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default LaserGuidedBomb;

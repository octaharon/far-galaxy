import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class NailBomb extends GenericBomb {
	public static id = 40009;
	public static caption = 'Nail bomb';
	public static description = `An intricate device, which explodes into dozens of sharp needles, travelling at
    supersonic speeds and heating up almost to the point of melting. Due to the size of this warhead it's only
    available as an aviation bomb, making it even more cumbersome than deadly`;
	public baseCost: number = this.baseCost + 6;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.thermodynamic),
			[Damage.damageType.thermodynamic]: {
				min: 20,
				max: 120,
				distribution: 'Gaussian',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.kinetic]: {
				min: 20,
				max: 120,
				distribution: 'Minimal',
			},
			[Damage.damageType.heat]: {
				min: 20,
				max: 40,
				distribution: 'Bell',
			},
			radius: 1.5,
			magnitude: 1,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			delivery: UnitAttack.deliveryType.supersonic,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default NailBomb;

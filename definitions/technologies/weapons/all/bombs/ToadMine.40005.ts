import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class ToadBomb extends GenericBomb {
	public static id = 40005;
	public static caption = 'Toad Mine';
	public static description = `An aviation bomb carrying several clamped directed charges,
    each exploding in a way so the rest are propelled towards closest target,
    leaping up to 3 times and dealing damage each jump`;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.heat]: {
				min: 20,
				max: 50,
				distribution: 'Bell',
			},
			[Damage.damageType.thermodynamic]: {
				min: 30,
				max: 40,
				distribution: 'Uniform',
			},
			radius: 1.5,
			magnitude: 3,
			aoeType: UnitAttack.TAoEDamageSplashTypes.chain,
			delivery: UnitAttack.deliveryType.bombardment,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default ToadBomb;

import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class ClusterBomb extends GenericBomb {
	public static id = 40004;
	public static caption = 'Cluster Bomb';
	public static description = `An aviation bomb with a bunch of smaller warheads inside, which
    spread like an umbrella above the target area, hitting random targets`;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.thermodynamic]: {
				min: 30,
				max: 50,
				distribution: 'Bell',
			},
			[Damage.damageType.kinetic]: {
				min: 10,
				max: 40,
				distribution: 'Uniform',
			},
			radius: 3,
			magnitude: 1.75,
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			delivery: UnitAttack.deliveryType.bombardment,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default ClusterBomb;

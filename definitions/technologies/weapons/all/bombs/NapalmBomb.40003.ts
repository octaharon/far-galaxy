import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class NapalmBomb extends GenericBomb {
	public static id = 40003;
	public static caption = 'Incendiary Bomb';
	public static description = `An aviation bomb with highly flammable liquid payload`;
	public baseCost: number = this.baseCost + 2;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.thermodynamic),
			[Damage.damageType.heat]: {
				min: 60,
				max: 100,
				distribution: 'Minimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.heat]: {
				min: 20,
				max: 50,
				distribution: 'Bell',
			},
			[Damage.damageType.biological]: {
				min: 10,
				max: 100,
				distribution: 'MostlyMinimal',
			},
			radius: 1,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			delivery: UnitAttack.deliveryType.surface,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default NapalmBomb;

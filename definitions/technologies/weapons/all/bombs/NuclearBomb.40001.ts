import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class NuclearBomb extends GenericBomb {
	public static id = 40001;
	public static caption = 'Nuclear bomb';
	public static description = `Medium sized aviation bomb with a tactical nuclear payload`;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.thermodynamic),
			[Damage.damageType.magnetic]: {
				min: 20,
				max: 100,
				distribution: 'Minimal',
			},
			[Damage.damageType.heat]: {
				min: 10,
				max: 30,
				distribution: 'Uniform',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.radiation]: {
				min: 20,
				max: 40,
				distribution: 'Uniform',
			},
			[Damage.damageType.heat]: {
				min: 20,
				max: 40,
				distribution: 'Bell',
			},
			radius: 1.5,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			delivery: UnitAttack.deliveryType.cloud,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default NuclearBomb;

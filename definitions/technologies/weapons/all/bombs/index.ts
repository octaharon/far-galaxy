import Weapons from '../../../types/weapons';

export const BombWeapons: Weapons.IWeaponGroup = {
	caption: 'Bombs',
	aliases: ['Bomb Weapons'],
	description: {
		text: 'Coming in a wide variety of damaging factors, bombs provide area damage and are especially good versus Naval units, but can only be carried by Aerial units',
		lineOfSight: false,
		precision: 'normal',
		range: 'verylow',
		rate: 'low',
	},
	id: Weapons.TWeaponGroupType.Bombs,
	weapons: [],
};
export default BombWeapons;

import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericBomb extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Bombs;
	public baseAccuracy = 1.3;
	public baseRate = 0.3;
	public baseCost = 12;
	public priority = ATTACK_PRIORITIES.LOWER;
	public baseRange = 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.thermodynamic]: {
				min: 60,
				max: 100,
				distribution: 'Minimal',
			},
			delivery: UnitAttack.deliveryType.bombardment,
		} as UnitAttack.TAttackDefinition,
		{
			[Damage.damageType.thermodynamic]: {
				min: 20,
				max: 40,
				distribution: 'Minimal',
			},
			[Damage.damageType.heat]: {
				min: 20,
				max: 40,
				distribution: 'Minimal',
			},
			radius: 1.5,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			delivery: UnitAttack.deliveryType.cloud,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default GenericBomb;

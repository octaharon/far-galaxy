import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class BlackHoleBomb extends GenericBomb {
	public static id = 40002;
	public static caption = 'Black Hole Bomb';
	public static description = `A long sought dream of many has finally come true:
    you can drop a black hole onto your foes and see what actually happens to them!`;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.thermodynamic),
			[Damage.damageType.warp]: {
				min: 20,
				max: 100,
				distribution: 'MostlyMinimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.magnetic]: {
				min: 20,
				max: 50,
				distribution: 'Bell',
			},
			[Damage.damageType.warp]: {
				min: 10,
				max: 100,
				distribution: 'Minimal',
			},
			radius: 4,
			magnitude: 0.25,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			delivery: UnitAttack.deliveryType.immediate,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default BlackHoleBomb;

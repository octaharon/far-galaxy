import GenericBomb from './GenericBomb';

export class ConventionalBomb extends GenericBomb {
	public static id = 40000;
	public static caption = 'Conventional bomb';
	public static description = `Classical aviation bomb with an explosive payload`;
	public baseCost: number = this.baseCost + 1;
}

export default ConventionalBomb;

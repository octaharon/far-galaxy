import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class AntimatterBomb extends GenericBomb {
	public static id = 40007;
	public static caption = 'Antimatter bomb';
	public static description = `Extreme precision bomb with a gluon warhead`;
	public baseCost: number = this.baseCost + 2;
	public baseAccuracy: number = this.baseAccuracy + 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.thermodynamic),
			[Damage.damageType.antimatter]: {
				min: 60,
				max: 100,
				distribution: 'Minimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.magnetic]: {
				min: 20,
				max: 40,
				distribution: 'Bell',
			},
			radius: 2,
			magnitude: 0.6,
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			delivery: UnitAttack.deliveryType.beam,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default AntimatterBomb;

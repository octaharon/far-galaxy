import _ from 'underscore';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import GenericBomb from './GenericBomb';

export class ReaperBomb extends GenericBomb {
	public static id = 40008;
	public static caption = 'Reaper mine';
	public static description = `An aviation wheel-shaped bomb which travels a bit along the trajectory,
    leaving a trail of nanospray, which is ignited afterwards, creating a shaped thermobaric explosion`;
	public baseCost: number = this.baseCost + 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			..._.omit(this.attacks[0], Damage.damageType.thermodynamic),
			[Damage.damageType.kinetic]: {
				min: 30,
				max: 70,
				distribution: 'MostlyMinimal',
			},
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.heat]: {
				min: 40,
				max: 60,
				distribution: 'Bell',
			},
			[Damage.damageType.thermodynamic]: {
				min: 30,
				max: 80,
				distribution: 'Parabolic',
			},
			radius: 4,
			magnitude: 0.8,
			aoeType: UnitAttack.TAoEDamageSplashTypes.lineBehind,
			delivery: UnitAttack.deliveryType.surface,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default ReaperBomb;

import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import { GenericAA } from './GenericAA';

export class AACannon extends GenericAA {
	public static caption = 'Anti-Aircraft Cannon';
	public static description = `Twin barrel autocannon firing solid projectiles with arguable precision`;
	public static id = 70000;
	public priority = ATTACK_PRIORITIES.NORMAL;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(2).fill(0).map(
		() =>
			({
				[Damage.damageType.kinetic]: {
					min: 15,
					max: 25,
					distribution: 'Uniform',
				},
				delivery: UnitAttack.deliveryType.aerial,
			} as UnitAttack.TDirectAttackDefinition),
	);
}

export default AACannon;

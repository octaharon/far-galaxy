import Weapons from '../../../types/weapons';

export const AAWeapons: Weapons.IWeaponGroup = {
	caption: 'Anti-Aircraft',
	aliases: ['Anti-Air Weapons', 'Anti-Air'],
	description: {
		text: 'AA weapons can only damage Aerial targets but they are extremely efficient at this application',
		lineOfSight: false,
		damage: 'high',
		range: 'normal',
		rate: 'high',
	},
	id: Weapons.TWeaponGroupType.AntiAir,
	weapons: [],
};
export default AAWeapons;

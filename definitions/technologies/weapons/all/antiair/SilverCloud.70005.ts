import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericAA } from './GenericAA';

export class SilverCloudWeapon extends GenericAA {
	public static caption = 'Silver Cloud';
	public static description = `A projectile which explodes in the air,
    suspending a lot of tiny electrified metal particles
    in the air, which stick to anything passing through and discharge at target over a short while`;
	public static id = 70005;
	public baseCost: number = this.baseCost + 2;
	public baseRange: number = this.baseRange + 3;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.magnetic]: {
				min: 28,
				max: 40,
				distribution: 'Gaussian',
			},
			radius: 4,
			magnitude: 0.5,
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			delivery: UnitAttack.deliveryType.aerial,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
		{
			[Damage.damageType.magnetic]: {
				min: 11,
				max: 15,
				distribution: 'Uniform',
			},
			[UnitAttack.attackFlags.DoT]: true,
			delivery: UnitAttack.deliveryType.aerial,
			duration: 2,
		} as UnitAttack.TDOTAttackDefinition,
	];
}

export default SilverCloudWeapon;

import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import { GenericAA } from './GenericAA';

export class AMAACannon extends GenericAA {
	public static caption = 'AMAAC';
	public static description = `Anti-matter anti-aircraft cannon. Basically a AA gun,
    firing annihilating rounds at aircrafts`;
	public static id = 70004;
	public baseCost: number = this.baseCost + 4;
	public baseRate = 1.2;
	public priority = ATTACK_PRIORITIES.NORMAL;
	public baseRange: number = this.baseRange - 1;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.antimatter]: {
				min: 35,
				max: 50,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.aerial,
		} as UnitAttack.TDirectAttackDefinition,
	];
}

export default AMAACannon;

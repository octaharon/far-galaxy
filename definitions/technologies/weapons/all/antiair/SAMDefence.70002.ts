import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import { GenericAA } from './GenericAA';

export class SAMDefence extends GenericAA {
	public static caption = 'SAM Defense';
	public static description = `Guided rocket anti-aircraft system with a volumetric explosion payload. Though slower than most AA systems, SAM delivers higher amounts of Damage to well protected targets`;
	public static id = 70002;
	public baseCost: number = this.baseCost + 3;
	public baseAccuracy = 1.5;
	public priority = ATTACK_PRIORITIES.LOW;
	public baseRange: number = this.baseRange + 2;
	public baseRate = 0.35;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.thermodynamic]: {
				min: 35,
				max: 55,
				distribution: 'Bell',
			},
			delivery: UnitAttack.deliveryType.aerial,
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.heat]: {
				min: 15,
				max: 30,
				distribution: 'Maximal',
			},
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			magnitude: 0.75,
			radius: 2,
			delivery: UnitAttack.deliveryType.aerial,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default SAMDefence;

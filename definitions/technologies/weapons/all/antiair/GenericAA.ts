import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericAA extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.AntiAir;
	public baseAccuracy = 0.9;
	public baseRate = 1.5;
	public baseCost = 15;
	public priority = ATTACK_PRIORITIES.LOW;
	public baseRange = 7;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.aerial,
		} as UnitAttack.TAttackDefinition,
	];
}

export default GenericAA;

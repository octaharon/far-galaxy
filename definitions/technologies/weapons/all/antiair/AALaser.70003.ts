import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import { GenericAA } from './GenericAA';

export class AALaser extends GenericAA {
	public static caption = 'AALPD';
	public static description = `Anti-Aircraft Laser Point Defense.
    Launches a small capsule which descends on a parachute,
    firing x-ray lasers at surrounding targets`;
	public static id = 70003;
	public baseCost: number = this.baseCost + 8;
	public baseRate: number = this.baseRate / 2;
	public baseRange: number = this.baseRange - 2;
	public priority = ATTACK_PRIORITIES.HIGHER;
	public baseAccuracy: number = this.baseAccuracy + 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			delivery: UnitAttack.deliveryType.aerial,
			[UnitAttack.attackFlags.AoE]: true,
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			magnitude: 4,
			radius: 3,
			[Damage.damageType.heat]: {
				min: 10,
				max: 20,
				distribution: 'Uniform',
			},
			[Damage.damageType.radiation]: {
				min: 10,
				max: 20,
				distribution: 'Uniform',
			},
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default AALaser;

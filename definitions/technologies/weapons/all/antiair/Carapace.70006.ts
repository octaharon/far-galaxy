import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericAA } from './GenericAA';

export class CarapaceWeapon extends GenericAA {
	public static caption = 'CARAPACE ADS';
	public static description = `Air defense system equipped with proximity detonation missiles,
    spreading caustic aerosol on target, which inflicts rapid corruption on most metals and
    also ignites when coming into contact with fuel`;
	public static id = 70006;
	public baseCost: number = this.baseCost + 8;
	public baseRange: number = this.baseRange + 4;
	public baseRate = 0.8;
	public baseAccuracy: number = this.baseAccuracy + 0.4;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.biological]: {
				min: 25,
				max: 60,
				distribution: 'Minimal',
			},
			delivery: UnitAttack.deliveryType.aerial,
		} as UnitAttack.TDirectAttackDefinition,
		{
			[Damage.damageType.heat]: {
				min: 15,
				max: 30,
				distribution: 'Uniform',
			},
			delivery: UnitAttack.deliveryType.immediate,
			[UnitAttack.attackFlags.Deferred]: true,
		} as UnitAttack.TDeferredAttackDefinition,
	];
}

export default CarapaceWeapon;

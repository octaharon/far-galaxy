import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import { GenericAA } from './GenericAA';

export class AAMissile extends GenericAA {
	public static caption = 'Anti-Aircraft Missile';
	public static description = `Air defense launching missiles which detonate at set altitude,
    exploding in burst of burning carbon splinters, hitting aerial targets`;
	public static id = 70001;
	public baseCost: number = this.baseCost + 4;
	public baseAccuracy = 1.8;
	public priority = ATTACK_PRIORITIES.LOWER;
	public baseRange: number = this.baseRange + 3;
	public baseRate: number = this.baseRate - 0.5;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.kinetic]: {
				min: 35,
				max: 55,
				distribution: 'Minimal',
			},
			[Damage.damageType.heat]: {
				min: 20,
				max: 30,
				distribution: 'Bell',
			},
			aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
			magnitude: 0.7,
			radius: 2,
			delivery: UnitAttack.deliveryType.aerial,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default AAMissile;

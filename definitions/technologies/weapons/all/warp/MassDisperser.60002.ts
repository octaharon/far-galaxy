import { ArithmeticPrecision } from '../../../../maths/constants';
import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericWarpWeapon } from './GenericWarp';

export class MassDisperser extends GenericWarpWeapon {
	public static caption = 'Mass Disperser';
	public static description = `Short range warp weapon, which basically collapses
    time-space in front of bearer, dealing immense damage to targets in area, especially
    to the Massive ones`;
	public static id = 60002;
	public baseCost: number = this.baseCost + 7;
	public baseRange = ArithmeticPrecision;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.warp]: {
				min: 120,
				max: 180,
				distribution: 'Bell',
			},
			aoeType: UnitAttack.TAoEDamageSplashTypes.cone90,
			magnitude: 1,
			radius: 4,
			delivery: UnitAttack.deliveryType.immediate,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	];
}

export default MassDisperser;

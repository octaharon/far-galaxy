import { GenericWarpWeapon } from './GenericWarp';

export class WarpGun extends GenericWarpWeapon {
	public static caption = 'Warp projector';
	public static description = `Medium range weapon which creates local irregularities in time-space,
    tearing apart the intestines of any target. The scale of deformations is proportional to density, so
    massive targets are more vulnerable to any warp weapons, while the effect of the weapon
    is immediate, so it never misses`;
	public static id = 60000;
}

export default WarpGun;

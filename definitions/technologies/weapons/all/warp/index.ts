import Weapons from '../../../types/weapons';

export const WarpWeapons = {
	caption: 'Warp Weapons',
	aliases: ['Warps'],
	description: {
		text: `With technology that fractures the time-space itself, it's possible to inflict Damage at any reachable point of space without an option to Dodge it. Those weapons deal WRP Damage, which is most efficient against Massive Units`,
		lineOfSight: false,
		damage: 'high',
		precision: 'veryhigh',
		rate: 'low',
	},
	id: Weapons.TWeaponGroupType.Warp,
	weapons: [],
} as Weapons.IWeaponGroup;
export default WarpWeapons;

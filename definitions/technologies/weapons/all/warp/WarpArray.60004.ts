import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericWarpWeapon } from './GenericWarp';

export class WarpArrayWeapon extends GenericWarpWeapon {
	public static caption = 'Warp array';
	public static description = `Medium range weapon fracturing time-space immediately, rapidly and accurately,
    dealing extreme damage to more dense targets and leaving no chance to dodge the attack to others.`;
	public static id = 60004;
	public baseCost: number = this.baseCost + 5;
	public baseRate: number = this.baseRate + 0.1;
	public baseRange: number = this.baseRange - 3;
	public attacks: UnitAttack.TAttackDefinition[] = new Array(4).fill(0).map(
		(d) =>
			({
				[Damage.damageType.warp]: {
					min: 20,
					max: 35,
					distribution: 'Minimal',
				},
				delivery: UnitAttack.deliveryType.immediate,
			} as UnitAttack.TDirectAttackDefinition),
	);
}

export default WarpArrayWeapon;

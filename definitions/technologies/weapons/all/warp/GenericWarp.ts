import UnitAttack from '../../../../tactical/damage/attack';
import { ATTACK_PRIORITIES } from '../../../../tactical/damage/constants';
import Damage from '../../../../tactical/damage/damage';
import Weapons from '../../../types/weapons';
import GenericWeapon from '../../GenericWeapon';

export class GenericWarpWeapon extends GenericWeapon {
	public static readonly groupType: Weapons.TWeaponGroupType = Weapons.TWeaponGroupType.Warp;
	public baseAccuracy = 1;
	public baseRate = 0.35;
	public baseCost = 19;
	public priority = ATTACK_PRIORITIES.HIGHEST;
	public baseRange = 9.0;
	public attacks: UnitAttack.TAttackDefinition[] = [
		{
			[Damage.damageType.warp]: {
				min: 45,
				max: 75,
				distribution: 'Bell',
			},
			delivery: UnitAttack.deliveryType.immediate,
		} as UnitAttack.TAttackDefinition,
	];
}

export default GenericWarpWeapon;

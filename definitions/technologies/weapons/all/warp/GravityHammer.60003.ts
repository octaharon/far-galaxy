import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericWarpWeapon } from './GenericWarp';

export class GravityHammer extends GenericWarpWeapon {
	public static caption = 'Gravity Hammer';
	public static description = `Long range weapon which fractures the matter in an elliptical area,
    dealing especially severe damage to massive targets.
    Most efficient in  response to artillery squads and big combat vehicles,
    for a price of production cost and reload rate`;
	public static id = 60003;
	public baseCost: number = this.baseCost + 4;
	public baseRange: number = this.baseRange + 3;
	public baseRate = 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.slice(0).concat([
		{
			[Damage.damageType.warp]: {
				min: 40,
				max: 100,
				distribution: 'Bell',
			},
			aoeType: UnitAttack.TAoEDamageSplashTypes.lineAside,
			magnitude: 1,
			radius: 3,
			delivery: UnitAttack.deliveryType.immediate,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	]);
}

export default GravityHammer;

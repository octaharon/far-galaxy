import UnitAttack from '../../../../tactical/damage/attack';
import Damage from '../../../../tactical/damage/damage';
import { GenericWarpWeapon } from './GenericWarp';

export class MassCondenser extends GenericWarpWeapon {
	public static caption = 'Mass Condenser';
	public static description = `Focused gravity waves emitter which instantaneously creates
    microscopic black holes at location, absorbing all materia around and then exploding with a
    burst of rays`;
	public static id = 60001;
	public baseCost: number = this.baseCost + 5;
	public baseRange: number = this.baseRange + 2;
	public baseRate = 0.2;
	public attacks: UnitAttack.TAttackDefinition[] = this.attacks.slice(0).concat([
		{
			[Damage.damageType.magnetic]: {
				min: 20,
				max: 30,
				distribution: 'Parabolic',
			},
			[Damage.damageType.warp]: {
				min: 16,
				max: 24,
				distribution: 'Uniform',
			},
			aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
			magnitude: 1,
			radius: 1.5,
			delivery: UnitAttack.deliveryType.beam,
			[UnitAttack.attackFlags.AoE]: true,
		} as UnitAttack.TAOEAttackDefinition,
	]);
}

export default MassCondenser;

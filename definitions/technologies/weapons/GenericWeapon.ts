import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import BasicMaths from '../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../maths/constants';
import UnitAttack from '../../tactical/damage/attack';
import AttackManager from '../../tactical/damage/AttackManager';
import Damage from '../../tactical/damage/damage';
import DamageManager from '../../tactical/damage/DamageManager';
import UnitPart from '../../tactical/units/UnitPart';
import Weapons, { IWeapon, IWeaponMeta, IWeaponStatic } from '../types/weapons';

export default class GenericWeapon
	extends UnitPart<Weapons.TWeaponProperties, Weapons.TSerializedWeapon, IWeaponMeta, IWeaponStatic>
	implements IWeapon
{
	public static readonly groupType: Weapons.TWeaponGroupType;
	public static readonly id: number = 0;
	public baseAccuracy: number;
	public baseRate: number;
	public priority: number;
	public baseRange: number;
	public attacks: UnitAttack.TAttackDefinition[];
	protected DPT: Damage.TDamageUnit = null;

	public get group() {
		return this.meta ? this.meta.group : null;
	}

	public get compiled(): UnitAttack.IAttackInterface {
		return {
			attacks: this.attacks.slice().map((t) => ({ ...t })),
			baseAccuracy: this.baseAccuracy,
			baseRange: this.baseRange,
			baseRate: this.baseRate,
			priority: this.priority,
			caption: this.caption,
			type: this.static.groupType,
		};
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.weapon];
	}

	public serialize(): Weapons.TSerializedWeapon {
		return {
			...this.__serializeUnitPart(),
			attacks: this.attacks.map((v) => {
				Damage.TDamageTypesArray.forEach((k: Damage.damageType) => {
					if (!v[k]) delete v[k];
				});
				return v;
			}),
			baseAccuracy: this.baseAccuracy,
			baseRange: this.baseRange,
			baseRate: this.baseRate,
			priority: this.priority,
		};
	}

	public getAttacksQuantityMinMax(rate: number = this.baseRate) {
		return AttackManager.getAttacksQuantityMinMax(rate);
	}

	public getDamagePerTurn(force = false): Damage.TDamageUnit {
		if (!force && this.DPT) return this.DPT;
		return (this.DPT = AttackManager.getDamagePerTurn(this));
	}

	applySlotModifier(slot: Weapons.TWeaponSlotType): IWeapon {
		return this.applyModifier(
			...(this.getProvider(DIInjectableCollectibles.weapon).getWeaponSlotModifiers(this.static.id, slot) || []),
		);
	}

	public applyModifier(...m: Weapons.TWeaponModifier[]): IWeapon {
		const t = [].concat(m).filter(Boolean) as Weapons.TWeaponModifier[];
		this.baseAccuracy = BasicMaths.applyModifier(
			this.baseAccuracy,
			...this.pullModifierProperty(t, 'baseAccuracy'),
		);
		if (this.baseRange > ArithmeticPrecision)
			this.baseRange = BasicMaths.applyModifier(this.baseRange, ...this.pullModifierProperty(t, 'baseRange'));
		this.baseRate = BasicMaths.applyModifier(this.baseRate, ...this.pullModifierProperty(t, 'baseRate'));
		this.priority = BasicMaths.applyModifier(this.priority, ...this.pullModifierProperty(t, 'priority'));

		this.baseCost = BasicMaths.applyModifier(this.baseCost, ...this.pullModifierProperty(t, 'baseCost'));

		const aoeModifiers = this.pullModifierProperty(t, 'aoeRadiusModifier');

		const dmgModifiers = this.pullModifierProperty(t, 'damageModifier');
		this.attacks = this.attacks.map((a, ix) => {
			let aCopy = a;
			if (dmgModifiers?.length) aCopy = DamageManager.applyDamageModifierToDamageDefinition(a, ...dmgModifiers);
			if (AttackManager.isAOEAttack(aCopy) && aoeModifiers?.length)
				aCopy.radius = BasicMaths.roundToPrecision(
					BasicMaths.applyModifier((a as UnitAttack.TAOEAttackDefinition).radius, ...aoeModifiers),
					0.01,
				);
			return aCopy;
		});
		return this;
	}

	public applyGroupModifier(...m: Weapons.TWeaponGroupModifier[]): IWeapon {
		const t = [].concat(m).filter(Boolean) as Weapons.TWeaponGroupModifier[];
		const groupId = this.group.id;
		t.forEach((wgm) => this.applyModifier(wgm[groupId] || wgm.default || {}));
		return this;
	}

	/**
	 * Get array of all modifiers to be applied to this weapons' particular property from the Group modifiers
	 * @param m list of group modifiers
	 * @param k key of Weapon Modifier
	 * @param g isWeapon group id
	 */
	protected pullGroupModifierProperty<
		T extends Weapons.TWeaponGroupModifier,
		K extends keyof Weapons.TWeaponModifier,
	>(m: T[], k: K, g: Weapons.TWeaponGroupType = this.meta.group.id) {
		const r = [] as Array<Weapons.TWeaponModifier[K]>;
		m.forEach((mod) => {
			const t = mod[g] || mod.default;
			if (!t || !t[k]) return;
			r[r.length] = t[k];
		});
		return r.filter(Boolean);
	}

	/**
	 * Get array of all modifiers to be applied to this weapons' particular property from Weapon Modifiers
	 * @param m list of modifiers
	 * @param k key of Weapon Modifier
	 */
	protected pullModifierProperty<T extends Weapons.TWeaponModifier, K extends keyof Weapons.TWeaponModifier>(
		m: T[],
		k: K,
	) {
		const r = [] as IModifier[];
		m.forEach((mod) => {
			if (!mod || !mod[k]) return;
			r[r.length] = mod[k];
		});
		return r.filter(Boolean);
	}
}

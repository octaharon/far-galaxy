import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import { isEmptyModifier } from '../../core/helpers';
import $$ from '../../maths/BasicMaths';
import Damage from '../../tactical/damage/damage';
import UnitDefense from '../../tactical/damage/defense';
import { DefenseManager } from '../../tactical/damage/DefenseManager';
import UnitPart from '../../tactical/units/UnitPart';
import { IShield, IShieldMeta, IShieldStatic, TSerializedShield, TShieldProps } from '../types/shield';
import TShield = UnitDefense.TShield;

export class GenericShield
	extends UnitPart<TShieldProps, TSerializedShield, IShieldMeta, IShieldStatic>
	implements IShield
{
	public static readonly type: UnitDefense.TShieldSlotType;
	public static readonly flags?: UnitDefense.TDefenseFlags;
	public shield: UnitDefense.TShieldDefinition;

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.shield];
	}

	public get compiled(): TShield {
		return {
			...this.shield,
			currentAmount: this.shield.shieldAmount,
			caption: this.static.caption,
		};
	}

	public serialize(): TSerializedShield {
		return {
			...this.__serializeUnitPart(),
			shield: DefenseManager.serializeProtection(this.shield),
		};
	}

	public applyModifier(...m: UnitDefense.TShieldsModifier[]): IShield {
		const t = DefenseManager.stackShieldsModifier(...[].concat(m).filter(Boolean));
		if (!t) return this;
		this.baseCost = $$.applyModifier(this.baseCost, t.baseCost);
		this.shield = DefenseManager.serializeProtection(
			DefenseManager.stackDamageProtectionDefinition(this.shield, ...m),
		);
		if (!isEmptyModifier(t.overchargeDecay))
			this.shield.overchargeDecay = $$.applyModifier(this.shield.overchargeDecay || 0, t.overchargeDecay);
		this.shield.shieldAmount = $$.applyModifier(this.shield.shieldAmount, t.shieldAmount);
		return this;
	}
}

export const SmallShield_Bias_Min = -3;
export const SmallShield_Bias_Med = -6;
export const SmallShield_Bias_Max = -9;
export const MediumShield_Bias_Min = -4;
export const MediumShield_Bias_Med = -8;
export const MediumShield_Bias_Max = -12;
export const LargeShield_Bias_Min = -5;
export const LargeShield_Bias_Med = -10;
export const LargeShield_Bias_Max = -15;

export const SmallShield_Threshold_Min = 8;
export const SmallShield_Threshold_Med = 10;
export const SmallShield_Threshold_Max = 12;
export const MediumShield_Threshold_Min = 10;
export const MediumShield_Threshold_Med = 15;
export const MediumShield_Threshold_Max = 20;
export const LargeShield_Threshold_Min = 15;
export const LargeShield_Threshold_Med = 20;
export const LargeShield_Threshold_Max = 25;

export const SmallShield_Factor_Min = 0.8;
export const SmallShield_Factor_Med = 0.7;
export const SmallShield_Factor_Max = 0.6;
export const MediumShield_Factor_Min = 0.75;
export const MediumShield_Factor_Med = 0.65;
export const MediumShield_Factor_Max = 0.55;
export const LargeShield_Factor_Min = 0.7;
export const LargeShield_Factor_Med = 0.6;
export const LargeShield_Factor_Max = 0.5;

export const LargeShield_Capacity_Min = 100;
export const LargeShield_Capacity_Max = 150;
export const MediumShield_Capacity_Min = 50;
export const MediumShield_Capacity_Max = 100;
export const SmallShield_Capacity_Min = 20;
export const SmallShield_Capacity_Max = 60;

export const Shield_Repulsive_DamageTypes: Damage.damageType[] = [
	Damage.damageType.magnetic,
	Damage.damageType.radiation,
	Damage.damageType.heat,
];
export const Shield_Reactive_DamageTypes: Damage.damageType[] = [
	Damage.damageType.kinetic,
	Damage.damageType.thermodynamic,
	Damage.damageType.heat,
];
export const Shield_Neutronium_DamageTypes: Damage.damageType[] = [
	Damage.damageType.biological,
	Damage.damageType.radiation,
	Damage.damageType.antimatter,
];
export const Shield_Warp_DamageTypes: Damage.damageType[] = [
	Damage.damageType.warp,
	Damage.damageType.kinetic,
	Damage.damageType.antimatter,
];
export const Shield_Nano_DamageTypes: Damage.damageType[] = [
	Damage.damageType.magnetic,
	Damage.damageType.kinetic,
	Damage.damageType.radiation,
	Damage.damageType.biological,
];
export const Shield_Plasma_DamageTypes: Damage.damageType[] = [
	Damage.damageType.antimatter,
	Damage.damageType.heat,
	Damage.damageType.biological,
];
export const Shield_SMA_DamageTypes: Damage.damageType[] = [
	Damage.damageType.magnetic,
	Damage.damageType.heat,
	Damage.damageType.warp,
	Damage.damageType.biological,
];

export default GenericShield;

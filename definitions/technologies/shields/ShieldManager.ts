import UnitDefense from '../../tactical/damage/defense';
import { UnitPartFactory } from '../../tactical/units/UnitPart';
import {
	IShield,
	IShieldFactory,
	IShieldMeta,
	IShieldStatic,
	TSerializedShield,
	TShieldItem,
	TShieldProps,
} from '../types/shield';
import ShieldList from './index';

export class ShieldManager
	extends UnitPartFactory<TShieldProps, TSerializedShield, IShield, IShieldMeta, IShieldStatic>
	implements IShieldFactory
{
	protected __lookupBySlotType: EnumProxy<UnitDefense.TShieldSlotType, number[]> = {};

	public doesShieldFitSlot(a: TShieldItem, slot: UnitDefense.TShieldSlotType) {
		return a.type === slot;
	}

	public instantiate(props: Partial<TShieldProps>, id: number): IShield {
		return super.instantiate(props, id);
	}

	public getShieldsBySlotType(slot: UnitDefense.TShieldSlotType): TShieldItem[] {
		return (this.__lookupBySlotType[slot] || []).map((id) => this.get<TShieldItem>(id));
	}

	public fill() {
		this.__fillItems<TShieldItem>(
			ShieldList,
			(shieldType) =>
				({
					slot: shieldType.type,
				} as IShieldMeta),
			(shieldType) => {
				if (!this.__lookupBySlotType[shieldType.type]) this.__lookupBySlotType[shieldType.type] = [];
				this.__lookupBySlotType[shieldType.type].push(shieldType.id);
			},
		);
		return this;
	}
}

export default ShieldManager;

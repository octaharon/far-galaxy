import { TShieldItem } from '../types/shield';
import GenericShield from './GenericShield';

let ShieldList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), ShieldList);
ShieldList = ShieldList.filter(
	(module: any) => module.default.prototype instanceof GenericShield && module.default.id > 0,
).map((module: any) => module.default);
export default ShieldList as TShieldItem[];

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Med,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Med,
	Shield_Neutronium_DamageTypes,
} from '../../GenericShield';

export class NeutroniumShield extends GenericShield {
	public static readonly id = 20004;
	public static readonly caption = 'Neutronium Shield';
	public static readonly description =
		'A bigger version of neutronium shield, exhibiting solid capacity and protection against particle damage';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public baseCost = 35;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Neutronium_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: MediumShield_Bias_Med,
						factor: MediumShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default NeutroniumShield;

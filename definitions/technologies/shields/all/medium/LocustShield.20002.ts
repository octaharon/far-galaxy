import BasicMaths from '../../../../maths/BasicMaths';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Med,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Max,
	MediumShield_Threshold_Med,
} from '../../GenericShield';

const protection = {
	bias: MediumShield_Bias_Med,
	threshold: MediumShield_Threshold_Med,
	factor: MediumShield_Factor_Max,
	min: 0,
};

export class LocustShield extends GenericShield {
	public static readonly id = 20002;
	public static readonly caption = 'Locust Shield';
	public static readonly description =
		'Cybernetic insects create a protective barrier, effective against certain weapons and able to repair itself';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.replenishing]: true,
	};
	public baseCost = 38;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1 / 3,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		overchargeDecay: 5,
		default: {
			min: 0,
			bias: MediumShield_Bias_Med,
		},
		[Damage.damageType.antimatter]: protection,
		[Damage.damageType.kinetic]: protection,
	};
}

export default LocustShield;

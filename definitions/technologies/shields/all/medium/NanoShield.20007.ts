import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Med,
	MediumShield_Threshold_Min,
	Shield_Nano_DamageTypes,
} from '../../GenericShield';

export class NanoShield extends GenericShield {
	public static readonly id = 20007;
	public static readonly caption = 'Nano Shield';
	public static readonly description =
		'A swarm of tiny self-repairing robots, always ready to sacrifice themselves whenever the danger is approaching';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.replenishing]: true,
	};
	public baseCost = 34;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		default: {
			min: 0,
			threshold: MediumShield_Threshold_Min,
		},
		...Shield_Nano_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: MediumShield_Threshold_Min,
						factor: MediumShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default NanoShield;

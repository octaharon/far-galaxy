import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Med,
	MediumShield_Threshold_Max,
	MediumShield_Threshold_Min,
	Shield_Reactive_DamageTypes,
} from '../../GenericShield';

export class CarapaceShield extends GenericShield {
	public static readonly id = 20003;
	public static readonly caption = 'Carapace shield';
	public static readonly description =
		'Top grade military shield with substantial protection against most conventional weapons';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public baseCost = 33;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.4,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Reactive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: MediumShield_Threshold_Max,
						factor: MediumShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
		default: {
			threshold: MediumShield_Threshold_Min,
			min: 0,
		},
	};
}

export default CarapaceShield;

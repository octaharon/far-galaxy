import BasicMaths from '../../../../maths/BasicMaths';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Min,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Min,
	MediumShield_Threshold_Max,
	MediumShield_Threshold_Med,
} from '../../GenericShield';

const protection = {
	factor: MediumShield_Factor_Min,
	max: MediumShield_Threshold_Med,
	bias: MediumShield_Bias_Min,
	min: 0,
};

export class HardenedShield extends GenericShield {
	public static readonly id = 20011;
	public static readonly caption = 'Hardened Shield';
	public static readonly description = `An experimental device which overflows a kinetic shield with energy
        right at the moment of impact, effectively limiting all incoming damage`;
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public baseCost = 39;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1 / 3,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		[Damage.damageType.antimatter]: {
			...protection,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.kinetic]: {
			...protection,
		},
		default: {
			min: 0,
			bias: MediumShield_Bias_Min,
			max: MediumShield_Threshold_Max,
		},
	};
}

export default HardenedShield;

import BasicMaths from '../../../../maths/BasicMaths';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Max,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Max,
	SmallShield_Factor_Min,
} from '../../GenericShield';

export class AntimatterShield extends GenericShield {
	public static readonly id = 20009;
	public static readonly caption = 'Antimatter Shield';
	public static readonly description = `A high power device which reacts to neutrino surges, which often accompany the annihilation,
        and locks down the electron exchange processes in the area of contact`;
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public baseCost = 35;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		[Damage.damageType.antimatter]: {
			factor: MediumShield_Factor_Max,
			bias: MediumShield_Bias_Max,
			min: 0,
		},
		default: {
			min: 0,
			factor: SmallShield_Factor_Min,
		},
	};
}

export default AntimatterShield;

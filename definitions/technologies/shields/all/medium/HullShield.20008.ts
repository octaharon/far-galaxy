import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Min,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Min,
} from '../../GenericShield';

export class HullShield extends GenericShield {
	public static readonly id = 20008;
	public static readonly caption = 'Magnetic Hull';
	public static readonly description = 'A gravity wave emitter which displaces matter thus protecting the bearer';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.hull]: true,
	};
	public baseCost = 37;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 2 / 3,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		default: {
			bias: MediumShield_Bias_Min,
			factor: MediumShield_Factor_Min,
			min: 0,
		},
	};
}

export default HullShield;

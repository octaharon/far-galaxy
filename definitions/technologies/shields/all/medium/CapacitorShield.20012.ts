import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Max,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Med,
	MediumShield_Threshold_Max,
	MediumShield_Threshold_Med,
	Shield_SMA_DamageTypes,
} from '../../GenericShield';

const protection = {
	threshold: MediumShield_Threshold_Med,
	factor: MediumShield_Factor_Med,
	bias: MediumShield_Bias_Max,
	min: 0,
};

export class CapacitorShield extends GenericShield {
	public static readonly id = 20012;
	public static readonly caption = 'SMA Shield';
	public static readonly description = `A repulsive shield implemented with a superconductor magnetic array,
        which is essentialy a capacitor that absorbs the damage and, depending on the damage source,
        can be used to recharge the shield itself and connected to most weapons to increase their muzzle energy,
        when the shield can't be maintained anymore`;
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.sma]: true,
	};
	public baseCost = 37;
	public shield: UnitDefense.TShieldDefinition = {
		overchargeDecay: 10,
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		...Shield_SMA_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: MediumShield_Threshold_Max,
						factor: MediumShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default CapacitorShield;

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Min,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Max,
	MediumShield_Threshold_Max,
	Shield_Plasma_DamageTypes,
} from '../../GenericShield';

export class PlasmaShield extends GenericShield {
	public static readonly id = 20005;
	public static readonly caption = 'Plasma Shield';
	public static readonly description =
		'An ionized gaz locked in a magnetic field, providing best in class protection from certain things, while completely useless against others';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public baseCost = 30;
	public shield: UnitDefense.TShieldDefinition = {
		overchargeDecay: 10,
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 2 / 3,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Plasma_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: MediumShield_Threshold_Max,
						factor: MediumShield_Factor_Max,
						bias: MediumShield_Bias_Min,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default PlasmaShield;

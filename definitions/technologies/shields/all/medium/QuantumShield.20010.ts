import BasicMaths from '../../../../maths/BasicMaths';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Max,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Max,
	MediumShield_Threshold_Min,
} from '../../GenericShield';

export class QuantumShield extends GenericShield {
	public static readonly id = 20010;
	public static readonly caption = 'Quantum Shield';
	public static readonly description = `A black market device of unknown origin, which somehow creates a shaped cloud of quantum entangled photons, which can absorb tremendous amounts of radiation, while yielding minuscule defense against conventional weapons`;
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.tachyon]: true,
	};
	public baseCost = 36;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		overchargeDecay: 5,
		default: {
			threshold: MediumShield_Threshold_Min,
			min: 0,
		},
		[Damage.damageType.radiation]: {
			factor: MediumShield_Factor_Max,
			bias: MediumShield_Bias_Max,
			threshold: MediumShield_Threshold_Min,
			min: 0,
		},
		[Damage.damageType.magnetic]: {
			factor: MediumShield_Factor_Max,
			bias: MediumShield_Bias_Max,
			threshold: MediumShield_Threshold_Min,
			min: 0,
		},
	};
}

export default QuantumShield;

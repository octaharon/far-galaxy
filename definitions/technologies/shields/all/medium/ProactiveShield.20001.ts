import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Med,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Max,
	MediumShield_Factor_Med,
	Shield_Reactive_DamageTypes,
} from '../../GenericShield';

const protection = {
	bias: MediumShield_Bias_Med,
	factor: MediumShield_Factor_Med,
	min: 0,
};

export class ProactiveShield extends GenericShield {
	public static readonly id = 20001;
	public static readonly caption = 'Proactive Shield';
	public static readonly description =
		'Robotized directional shield with substantial protection against most projectile weapons';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public baseCost = 27;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.2,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Reactive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: MediumShield_Bias_Med,
						factor: MediumShield_Factor_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default ProactiveShield;

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Bias_Max,
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Max,
	Shield_Warp_DamageTypes,
} from '../../GenericShield';

export class WarpShield extends GenericShield {
	public static readonly id = 20006;
	public static readonly caption = 'Warp Shield';
	public static readonly description =
		'A targeted shield which creates space-time tears in front of incoming projectiles and counteracts incoming gravity waves';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.warp]: true,
	};
	public baseCost = 34;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.8,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Warp_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						factor: MediumShield_Factor_Max,
						bias: MediumShield_Bias_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default WarpShield;

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	MediumShield_Capacity_Max,
	MediumShield_Capacity_Min,
	MediumShield_Factor_Max,
	MediumShield_Threshold_Max,
	Shield_Repulsive_DamageTypes,
} from '../../GenericShield';

export class BulwarkShield extends GenericShield {
	public static readonly id = 20000;
	public static readonly caption = 'Bulwark Shield';
	public static readonly description = 'Advanced reflective shield with high level protection';
	public static readonly type = UnitDefense.TShieldSlotType.medium;
	public baseCost = 28;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: MediumShield_Capacity_Min,
				max: MediumShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Repulsive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: MediumShield_Threshold_Max,
						factor: MediumShield_Factor_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default BulwarkShield;

import BasicMaths from '../../../../maths/BasicMaths';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Bias_Min,
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Min,
	LargeShield_Threshold_Max,
	LargeShield_Threshold_Med,
} from '../../GenericShield';

const protection = {
	factor: LargeShield_Factor_Min,
	max: LargeShield_Threshold_Med,
	bias: LargeShield_Bias_Min,
	min: 0,
};

export class ImmortalShield extends GenericShield {
	public static readonly id = 30010;
	public static readonly caption = 'The Immortal';
	public static readonly description = `A shielding device manufactured by a private company and sold mostly on the black market,
        using some sort of warp technologies to dissipate most incoming energy before discharging,
        capable of withstanding at least few shots against any weapons, even the heaviest ones`;
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public baseCost = 49;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1 / 3,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		[Damage.damageType.antimatter]: {
			...protection,
		},
		[Damage.damageType.thermodynamic]: {
			...protection,
		},
		[Damage.damageType.kinetic]: {
			...protection,
		},
		default: {
			min: 0,
			bias: LargeShield_Bias_Min,
			max: LargeShield_Threshold_Max,
		},
	};
}

export default ImmortalShield;

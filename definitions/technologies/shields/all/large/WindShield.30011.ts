import BasicMaths from '../../../../maths/BasicMaths';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Max,
	LargeShield_Threshold_Min,
	SmallShield_Factor_Min,
} from '../../GenericShield';

export class WindShield extends GenericShield {
	public static readonly id = 30011;
	public static readonly caption = 'WIND';
	public static readonly description = `Warp induction and negation device. Quite a big device as well. A shield which,
        despite offering some basic protection against most weapons too,is only and ultimately helpful against warp weapons,
        recharging the shield from incoming damage`;
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags = {
		[UnitDefense.defenseFlags.warp]: true,
	} as UnitDefense.TDefenseFlags;
	public baseCost = 48;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 2 / 3,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		[Damage.damageType.warp]: {
			factor: -LargeShield_Factor_Max / 2,
			min: -50,
		},
		default: {
			factor: SmallShield_Factor_Min,
			threshold: LargeShield_Threshold_Min,
			min: 0,
		},
	};
}

export default WindShield;

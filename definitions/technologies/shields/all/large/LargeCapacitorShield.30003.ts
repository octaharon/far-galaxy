import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Med,
	LargeShield_Threshold_Max,
	Shield_SMA_DamageTypes,
} from '../../GenericShield';

export class LargeCapacitorShield extends GenericShield {
	public static readonly id = 30003;
	public static readonly caption = 'Large SMA Shield';
	public static readonly description = `The biggest superconductor magnetic array repulsors in the field, that
         stores some of dissipated energy and supplies it to weapon systems, if any, when the shield is broken.`;
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.sma]: true,
	};
	public baseCost = 48;
	public shield: UnitDefense.TShieldDefinition = {
		overchargeDecay: 15,
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		...Shield_SMA_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: LargeShield_Threshold_Max,
						factor: LargeShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default LargeCapacitorShield;

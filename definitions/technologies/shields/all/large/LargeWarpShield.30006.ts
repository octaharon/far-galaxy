import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Bias_Max,
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Max,
	Shield_Warp_DamageTypes,
} from '../../GenericShield';

export class LargeWarpShield extends GenericShield {
	public static readonly id = 30006;
	public static readonly caption = 'Large Warp Shield';
	public static readonly description =
		'The biggest verson of targeted warp shield, offering amazing protection versus energy weapons';
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.warp]: true,
	};
	public baseCost = 45;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.8,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Warp_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						factor: LargeShield_Factor_Max,
						bias: LargeShield_Bias_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default LargeWarpShield;

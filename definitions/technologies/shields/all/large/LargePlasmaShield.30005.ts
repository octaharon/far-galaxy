import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Bias_Min,
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Max,
	LargeShield_Threshold_Max,
	Shield_Plasma_DamageTypes,
} from '../../GenericShield';

export class LargePlasmaShield extends GenericShield {
	public static readonly id = 30005;
	public static readonly caption = 'Large Plasma Shield';
	public static readonly description =
		'An ionized gaz locked in a magnetic field, providing best in class protection from certain things, while completely useless against others';
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public baseCost = 41;
	public shield: UnitDefense.TShieldDefinition = {
		overchargeDecay: 15,
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 2 / 3,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Plasma_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: LargeShield_Threshold_Max,
						factor: LargeShield_Factor_Max,
						bias: LargeShield_Bias_Min,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default LargePlasmaShield;

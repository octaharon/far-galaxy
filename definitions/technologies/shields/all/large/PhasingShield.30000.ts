import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Max,
	LargeShield_Threshold_Max,
	Shield_Repulsive_DamageTypes,
} from '../../GenericShield';

export class PhasingShield extends GenericShield {
	public static readonly id = 30000;
	public static readonly caption = 'Phasing shield';
	public static readonly description =
		'Top tier reflective magnetic shield with energy supply so huge that it completely ignores smaller damage factors';
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public baseCost = 35;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Repulsive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: LargeShield_Threshold_Max,
						factor: LargeShield_Factor_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default PhasingShield;

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Bias_Max,
	LargeShield_Bias_Med,
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Max,
	LargeShield_Factor_Min,
	Shield_Reactive_DamageTypes,
	Shield_Repulsive_DamageTypes,
} from '../../GenericShield';

export class ClusterShield extends GenericShield {
	public static readonly id = 30013;
	public static readonly caption = 'Cluster Shield';
	public static readonly description = `Large shield battery with energy density modulator, that is extremely efficient against explosives,
        while yielding some defense from energy weapons as well`;
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags = {
		[UnitDefense.defenseFlags.evasion]: true,
	} as UnitDefense.TDefenseFlags;
	public baseCost = 45;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.25,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Repulsive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: LargeShield_Bias_Med,
						factor: LargeShield_Factor_Min,
						min: 0,
					},
				}),
			{},
		),
		...Shield_Reactive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: LargeShield_Bias_Max,
						factor: LargeShield_Factor_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default ClusterShield;

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Bias_Min,
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Min,
} from '../../GenericShield';

export class LargeHullShield extends GenericShield {
	public static readonly id = 30008;
	public static readonly caption = 'Giant Magnetic Hull';
	public static readonly description = 'The biggest version of a gravity displacing shield';
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.hull]: true,
	};
	public baseCost = 46;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 2 / 3,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		default: {
			bias: LargeShield_Bias_Min,
			factor: LargeShield_Factor_Min,
			min: 0,
		},
	};
}

export default LargeHullShield;

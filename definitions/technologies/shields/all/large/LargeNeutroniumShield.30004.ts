import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Bias_Med,
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Med,
	Shield_Neutronium_DamageTypes,
} from '../../GenericShield';

export class LargeNeutroniumShield extends GenericShield {
	public static readonly id = 30004;
	public static readonly caption = 'Large Neutronium Shield';
	public static readonly description =
		'The biggest version of neutronium shield, offering outstanding protection against all damage, and even better defense against energy weapons';
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public baseCost = 46;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Neutronium_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: LargeShield_Bias_Med,
						factor: LargeShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default LargeNeutroniumShield;

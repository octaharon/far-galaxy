import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Med,
	LargeShield_Threshold_Min,
	Shield_Nano_DamageTypes,
} from '../../GenericShield';

export class LargeNanoShield extends GenericShield {
	public static readonly id = 30007;
	public static readonly caption = 'Large Nano Shield';
	public static readonly description =
		'A giant swarm of tiny self-reproducing robots, always ready to sacrifice themselves whenever the danger is approaching';
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.replenishing]: true,
	};
	public baseCost = 46;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		default: {
			min: 0,
			threshold: LargeShield_Threshold_Min,
		},
		...Shield_Nano_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: LargeShield_Threshold_Min,
						factor: LargeShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default LargeNanoShield;

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Threshold_Max,
	MediumShield_Factor_Min,
} from '../../GenericShield';

export class GluonShield extends GenericShield {
	public static readonly id = 30014;
	public static readonly caption = 'Gluon Shield';
	public static readonly description = `Energy-hungry device which generates a thick bubble of quark-gluon plasma
  around the bearer, which barely offers any protection, but is completely impenetrable without bringing in some high energies`;
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags = {
		[UnitDefense.defenseFlags.tachyon]: true,
	} as UnitDefense.TDefenseFlags;
	public baseCost = 50;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		default: {
			threshold: LargeShield_Threshold_Max,
			factor: MediumShield_Factor_Min,
			min: 0,
		},
	};
}

export default GluonShield;

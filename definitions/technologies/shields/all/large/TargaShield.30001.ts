import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Max,
	LargeShield_Threshold_Max,
	Shield_Reactive_DamageTypes,
} from '../../GenericShield';

export class TargaShield extends GenericShield {
	public static readonly id = 30001;
	public static readonly caption = 'TARGE';
	public static readonly description = `Targeted attack repulsion gravity emitter : a top tier robotic technology and bigger generator for good old repulsive shield, offering solid protection vs most conventional weapons, but discharges and energy shots still get through this one pretty easily`;
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags = {
		[UnitDefense.defenseFlags.reactive]: true,
	} as UnitDefense.TDefenseFlags;
	public baseCost = 41;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.4,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Reactive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: LargeShield_Threshold_Max,
						factor: LargeShield_Factor_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default TargaShield;

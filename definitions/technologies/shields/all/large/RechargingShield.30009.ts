import BasicMaths from '../../../../maths/BasicMaths';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Bias_Max,
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
} from '../../GenericShield';

export class RechargingShield extends GenericShield {
	public static readonly id = 30009;
	public static readonly caption = 'Recharging Shield';
	public static readonly description = `Top-tech Shield that recharges itself from directed energy weapons and is able to accumulate some of the lost Capacity that will partially recharge the Shield when it's breached`;
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public static readonly flags = {
		[UnitDefense.defenseFlags.boosters]: true,
	} as UnitDefense.TDefenseFlags;
	public baseCost = 48;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.8,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		[Damage.damageType.antimatter]: {
			bias: LargeShield_Bias_Max * 4,
			min: LargeShield_Bias_Max,
		},
		[Damage.damageType.radiation]: {
			bias: LargeShield_Bias_Max * 3,
			min: LargeShield_Bias_Max,
		},
		[Damage.damageType.magnetic]: {
			bias: LargeShield_Bias_Max * 3,
			min: LargeShield_Bias_Max,
		},
		default: {
			bias: LargeShield_Bias_Max,
			min: 0,
		},
	};
}

export default RechargingShield;

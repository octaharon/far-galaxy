import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	LargeShield_Bias_Med,
	LargeShield_Bias_Min,
	LargeShield_Capacity_Max,
	LargeShield_Capacity_Min,
	LargeShield_Factor_Med,
	LargeShield_Factor_Min,
	LargeShield_Threshold_Max,
	Shield_Repulsive_DamageTypes,
	Shield_Warp_DamageTypes,
} from '../../GenericShield';

const IonCurtainAbsorption: UnitDefense.IDamageProtectionDecorator = {
	bias: -10,
	factor: 0.9,
	min: 0,
};

const IonCurtainAbsorptionHard: UnitDefense.IDamageProtectionDecorator = {
	max: 35,
	bias: -20,
	min: 0,
};

export class IonCurtainShield extends GenericShield {
	public static readonly id = 30002;
	public static readonly caption = 'Ion Curtain';
	public static readonly description = `This device generates a thick ion shield, that reacts very differently to
        various damage sources`;
	public static readonly type = UnitDefense.TShieldSlotType.large;
	public baseCost = 44;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1 / 3,
				min: LargeShield_Capacity_Min,
				max: LargeShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Warp_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						factor: LargeShield_Factor_Min,
						min: 0,
					},
				}),
			{},
		),
		...Shield_Repulsive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: LargeShield_Bias_Min,
						factor: LargeShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
		default: {
			min: 0,
			bias: LargeShield_Bias_Med,
			threshold: LargeShield_Threshold_Max,
		},
	};
}

export default IonCurtainShield;

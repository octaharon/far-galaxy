import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	SmallShield_Bias_Min,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Min,
} from '../../GenericShield';

export class SmallHullShield extends GenericShield {
	public static readonly id = 10008;
	public static readonly caption = 'Small Magnetic Hull';
	public static readonly description = 'A gravity wave emitter which displaces matter thus protecting the bearer';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.hull]: true,
	};
	public baseCost = 28;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 2 / 3,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		default: {
			bias: SmallShield_Bias_Min,
			factor: SmallShield_Factor_Min,
			min: 0,
		},
	};
}

export default SmallHullShield;

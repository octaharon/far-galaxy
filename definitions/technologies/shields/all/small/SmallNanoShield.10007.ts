import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	Shield_Nano_DamageTypes,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Med,
	SmallShield_Threshold_Min,
} from '../../GenericShield';

export class SmallNanoShield extends GenericShield {
	public static readonly id = 10007;
	public static readonly caption = 'Small Nano Shield';
	public static readonly description =
		'A flock of tiny self-repairing robots, always ready to sacrifice themselves whenever the danger is approaching';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.replenishing]: true,
	};
	public baseCost = 23;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		default: {
			min: 0,
			threshold: SmallShield_Threshold_Min,
		},
		...Shield_Nano_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: SmallShield_Threshold_Min,
						factor: SmallShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default SmallNanoShield;

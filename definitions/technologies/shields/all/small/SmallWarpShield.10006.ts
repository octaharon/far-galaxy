import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	Shield_Warp_DamageTypes,
	SmallShield_Bias_Max,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Max,
} from '../../GenericShield';

export class SmallWarpShield extends GenericShield {
	public static readonly id = 10006;
	public static readonly caption = 'Small Warp Shield';
	public static readonly description =
		'A targeted shield which creates space-time tears in front of incoming projectiles, as well as counteracts enemy gravity fields';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.warp]: true,
	};
	public baseCost = 24;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.8,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Warp_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						factor: SmallShield_Factor_Max,
						bias: SmallShield_Bias_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default SmallWarpShield;

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	Shield_Plasma_DamageTypes,
	SmallShield_Bias_Min,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Max,
	SmallShield_Threshold_Max,
} from '../../GenericShield';

export class SmallPlasmaShield extends GenericShield {
	public static readonly id = 10005;
	public static readonly caption = 'Small Plasma Shield';
	public static readonly description =
		'An ionized gaz locked in a magnetic field, providing best in class protection from certain things, while completely useless against others';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public baseCost = 22;
	public shield: UnitDefense.TShieldDefinition = {
		overchargeDecay: 5,
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 2 / 3,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Plasma_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: SmallShield_Threshold_Max,
						factor: SmallShield_Factor_Max,
						bias: SmallShield_Bias_Min,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default SmallPlasmaShield;

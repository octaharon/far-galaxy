import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	Shield_Repulsive_DamageTypes,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Max,
	SmallShield_Threshold_Max,
} from '../../GenericShield';

export class AegisShield extends GenericShield {
	public static readonly id = 10002;
	public static readonly caption = 'AEGIS';
	public static readonly description =
		'Advanced Electronic Guard and Intercept System. Pretty advanced for its size!';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public baseCost = 20;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.5,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Repulsive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: SmallShield_Threshold_Max,
						factor: SmallShield_Factor_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default AegisShield;

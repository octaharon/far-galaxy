import BasicMaths from '../../../../maths/BasicMaths';
import Damage from '../../../../tactical/damage/damage';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Min,
	SmallShield_Threshold_Med,
} from '../../GenericShield';

export class AShield extends GenericShield {
	public static readonly id = 10009;
	public static readonly caption = 'The Shield';
	public static readonly description =
		'Just a solid ballistic shield. Like a SWAT version, but shaped to the Chassis. Not very helpful in most situations, but very cheap.';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public baseCost = 12;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		[Damage.damageType.kinetic]: {
			factor: SmallShield_Factor_Min,
			min: 0,
			threshold: SmallShield_Threshold_Med,
		},
		default: {
			min: 0,
			factor: SmallShield_Factor_Min,
		},
	};
}

export default AShield;

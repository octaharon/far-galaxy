import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	Shield_Neutronium_DamageTypes,
	SmallShield_Bias_Med,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Med,
} from '../../GenericShield';

export class SmallNeutroniumShield extends GenericShield {
	public static readonly id = 10004;
	public static readonly caption = 'Small Neutronium Shield';
	public static readonly description =
		'A layer of neutrons suspended in a warp field offer no defense from conventional weapons, but significantly reduces damage from fundamental particles';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public baseCost = 25;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Neutronium_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: SmallShield_Bias_Med,
						factor: SmallShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default SmallNeutroniumShield;

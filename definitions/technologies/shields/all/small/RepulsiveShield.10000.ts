import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	Shield_Repulsive_DamageTypes,
	SmallShield_Bias_Med,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Med,
} from '../../GenericShield';

export class RepulsiveShield extends GenericShield {
	public static readonly id = 10000;
	public static readonly caption = 'Repulsive Shield';
	public static readonly description =
		'Generic electromagnetic shield, offering basic protection from most energy weapons';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public baseCost = 15;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 1 / 3,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Repulsive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: SmallShield_Bias_Med,
						factor: SmallShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default RepulsiveShield;

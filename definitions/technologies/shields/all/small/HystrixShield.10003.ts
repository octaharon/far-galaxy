import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	Shield_Reactive_DamageTypes,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Med,
	SmallShield_Threshold_Max,
	SmallShield_Threshold_Min,
} from '../../GenericShield';

export class HystrixShield extends GenericShield {
	public static readonly id = 10003;
	public static readonly caption = 'Hystrix Shield';
	public static readonly description =
		'Advanced electronic system that targets discharges at incoming projectiles, yielding the best protection among similar devices';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public baseCost = 22;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.4,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Reactive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						threshold: SmallShield_Threshold_Max,
						factor: SmallShield_Factor_Med,
						min: 0,
					},
				}),
			{},
		),
		default: {
			min: 0,
			threshold: SmallShield_Threshold_Min,
		},
	};
}

export default HystrixShield;

import BasicMaths from '../../../../maths/BasicMaths';
import UnitDefense from '../../../../tactical/damage/defense';
import GenericShield, {
	Shield_Reactive_DamageTypes,
	SmallShield_Bias_Med,
	SmallShield_Capacity_Max,
	SmallShield_Capacity_Min,
	SmallShield_Factor_Max,
} from '../../GenericShield';

export class ReactiveShield extends GenericShield {
	public static readonly id = 10001;
	public static readonly caption = 'Reactive Shield';
	public static readonly description =
		'Electromagnetic directional shield with automated controls, offering some protection from explosives and projectiles';
	public static readonly type = UnitDefense.TShieldSlotType.small;
	public static readonly flags: UnitDefense.TDefenseFlags = {
		[UnitDefense.defenseFlags.reactive]: true,
	};
	public baseCost = 18;
	public shield: UnitDefense.TShieldDefinition = {
		shieldAmount: BasicMaths.roundToPrecision(
			BasicMaths.lerp({
				x: 0.2,
				min: SmallShield_Capacity_Min,
				max: SmallShield_Capacity_Max,
			}),
			5,
		),
		...Shield_Reactive_DamageTypes.reduce(
			(o, dt) =>
				Object.assign(o, {
					[dt]: {
						bias: SmallShield_Bias_Med,
						factor: SmallShield_Factor_Max,
						min: 0,
					},
				}),
			{},
		),
	};
}

export default ReactiveShield;

import DIContainer from './core/DI/DIContainer';

export async function preloadGameLibrary() {
	const preset = await import('./core/DI/DIPresetDefault').then((t) => t.DIPresetDefault);
	DIContainer.inject(preset);
	console.info('Library loaded');
}

// eslint-disable-next-line max-classes-per-file
import Collectible from '../../core/Collectible';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import BasicMaths from '../../maths/BasicMaths';
import { TPlayerId } from '../../player/playerId';
import Players from '../../player/types';
import { stackUnitEffectsModifiers } from '../../prototypes/helpers';
import { IUnitEffectModifier } from '../../prototypes/types';
import { TMapCellProps } from '../map';
import { DEFAULT_EFFECT_POWER } from './constants';
import EffectEvents from './effectEvents';
import StatusEffectMeta, { MAX_EFFECT_DURATION } from './statusEffectMeta';
import { IEffectMeta, IStatusEffect, IStatusEffectStatic, TEffectProps, TSerializedEffect } from './types';

export const defaultEffectProps: TEffectProps<any> = {
	power: DEFAULT_EFFECT_POWER,
	duration: 1,
	priority: -127,
};

export const defaultEffectMeta: IEffectMeta<any> = {
	source: null,
	target: null,
	modifier: null,
	phaseTick: 0,
	turnAdded: 0,
	turnTick: 0,
};

export default class GenericStatusEffect<
		TargetType extends StatusEffectMeta.effectAgentTypes,
		StateType extends object = any,
	>
	extends Collectible<
		TEffectProps<TargetType, StateType>,
		TSerializedEffect<TargetType, StateType>,
		IStatusEffectStatic<TargetType, StateType>
	>
	implements IStatusEffect<TargetType, StateType>
{
	public static readonly baseDuration: number = 1; // -1 means infinite
	public static readonly durationFixed: boolean = false; // if true, duration modifier is not applied
	public static readonly negative: boolean = false;
	public static readonly unremovable: boolean = false;
	public static readonly targetType: StatusEffectMeta.effectAgentTypes;
	public static readonly id: number = 0;

	public priority: number;
	public power: number;
	public duration: number;
	public state: StateType;
	public meta: IEffectMeta<TargetType>;

	public constructor(props: Partial<TEffectProps<TargetType, StateType>>) {
		const p = {
			...defaultEffectProps,
			...(props ?? {}),
		} as TEffectProps<TargetType, StateType>;
		super(p);
		const basePower = props?.meta?.basePower ?? props?.power ?? DEFAULT_EFFECT_POWER;
		const power = props?.power ?? basePower;
		const baseDuration =
			!props?.meta && this.static.durationFixed
				? this.static.baseDuration
				: props?.meta?.baseDuration ??
				  props?.duration ??
				  this.static.baseDuration ??
				  defaultEffectProps.duration;
		const duration =
			!props?.meta && this.static.durationFixed
				? this.static.baseDuration
				: props?.duration ?? this.static.baseDuration ?? defaultEffectProps.duration;
		if (!props?.meta)
			this.meta = {
				...(defaultEffectMeta as IEffectMeta<TargetType>),
				basePower,
				baseDuration,
			};
		this.setProps({
			power,
			duration,
			state: props?.state ?? {},
		});
	}

	public get source() {
		if (!this.meta?.source) return null;
		return this.getProvider(DIInjectableCollectibles.effect)?.getTargetFromSerialized(
			this.meta.source,
		) as StatusEffectMeta.TStatusEffectTarget;
	}

	public get target() {
		if (!this.meta?.target) return null;
		return this.getProvider(DIInjectableCollectibles.effect)?.getTargetFromSerialized(
			this.meta.target,
		) as StatusEffectMeta.TTargetType<TargetType>;
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.effect];
	}

	public static isUnitTarget(
		o: StatusEffectMeta.TTargetType<any>,
	): o is StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.unit> {
		return o?.effectTargetType === StatusEffectMeta.effectAgentTypes.unit;
	}

	public static isPlayerTarget(
		o: StatusEffectMeta.TTargetType<any>,
	): o is StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.player> {
		return o?.effectTargetType === StatusEffectMeta.effectAgentTypes.player;
	}

	public static applyDurationModifier(duration: number, d: IModifier): number {
		duration = duration ?? 0;
		if (Number.isNaN(duration) || duration === 0) return 0;
		if (duration < 0) return -1;
		return BasicMaths.applyModifier(duration, {
			...(d ?? {}),
			min: Math.max(1, d?.min || 0),
			max: Math.min(MAX_EFFECT_DURATION, d?.max || Infinity),
		});
	}

	public static isMapTarget(
		o: StatusEffectMeta.TTargetType<any>,
	): o is StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.map> {
		return o?.effectTargetType === StatusEffectMeta.effectAgentTypes.map;
	}

	doesBelongToPlayer(playerId: TPlayerId): boolean {
		return !!playerId && this.source?.playerId === playerId;
	}

	setSource(source: StatusEffectMeta.TTargetType<any>) {
		this.meta = Object.assign({}, this.meta ?? {}, {
			source: source ? this.getProvider(DIInjectableCollectibles.effect).getSerializedFromTarget(source) : null,
		});
		return this;
	}

	setTarget(target: StatusEffectMeta.TTargetType<TargetType>) {
		this.meta = Object.assign({}, this.meta ?? {}, {
			target:
				target && target.effectTargetType === this.static.targetType
					? this.getProvider(DIInjectableCollectibles.effect).getSerializedFromTarget(target)
					: null,
		});
		return this;
	}

	applyModifier(mod: IUnitEffectModifier, save = false) {
		if (!mod) return this;
		if (mod.statusDurationModifier && !this.static.durationFixed) {
			this.duration = this.static.applyDurationModifier(
				this.meta?.baseDuration ?? this.static.baseDuration,
				BasicMaths.stackModifiers(...mod.statusDurationModifier),
			);
		}
		if (mod.abilityPowerModifier)
			this.power = BasicMaths.applyModifier(
				this.meta?.basePower ?? DEFAULT_EFFECT_POWER,
				...(this.meta?.modifier?.abilityPowerModifier || []),
				...mod.abilityPowerModifier,
			);
		if (save && this.meta)
			// adding modifier to the stack
			this.meta.modifier = stackUnitEffectsModifiers(this.meta.modifier, mod);
		return this;
	}

	public triggerEvent<E extends EffectEvents.TEffectEventList<TargetType>>(
		event: E,
		meta?: EffectEvents.TEffectEventMeta<TargetType, E>,
		target?: StatusEffectMeta.TTargetType<TargetType>,
	): StateType {
		const t = this.target ?? target;
		if (t && this.static.effect[event]) return (this.state = this.static.effect[event](t, this, meta));
		return this.state;
	}

	public serialize() {
		return {
			...this.__serializeCollectible(),
			state: this.state,
			duration: this.duration,
			priority: this.priority,
			power: this.power,
			meta: this.meta,
		} as TSerializedEffect<TargetType, StateType>;
	}
}

export class ConstantStatusEffect<
	TargetType extends StatusEffectMeta.effectAgentTypes,
	StateType extends object = {},
> extends GenericStatusEffect<TargetType, StateType> {
	public static readonly baseDuration: -1 = -1;
	public static readonly durationFixed: false = false;
	public static readonly unremovable: true = true;
}

export class GenericUnitStatusEffect<StateType extends object = any> extends GenericStatusEffect<
	StatusEffectMeta.effectAgentTypes.unit,
	StateType
> {
	public static readonly id: number = -1;
	public static readonly targetType: StatusEffectMeta.effectAgentTypes = StatusEffectMeta.effectAgentTypes.unit;
}

export class GenericMapStatusEffect<StateType extends object = any> extends GenericStatusEffect<
	StatusEffectMeta.effectAgentTypes.map,
	StateType
> {
	public static readonly id: number = -2;
	public static readonly targetType: StatusEffectMeta.effectAgentTypes = StatusEffectMeta.effectAgentTypes.map;
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		radius: 2,
		elevation: 0,
	};
}

export class GenericPlayerStatusEffect<StateType extends object = any> extends GenericStatusEffect<
	StatusEffectMeta.effectAgentTypes.player,
	StateType
> {
	public static readonly id: number = -3;
	public static readonly targetType: StatusEffectMeta.effectAgentTypes = StatusEffectMeta.effectAgentTypes.player;
}

export class ConstantUnitStatusEffect<StateType extends object = any> extends GenericUnitStatusEffect<StateType> {
	public static readonly baseDuration: -1 = -1;
	public static readonly durationFixed: true = true;
	public static id = -10;
}

export class ConstantMapStatusEffect<StateType extends object = any> extends GenericMapStatusEffect<StateType> {
	public static readonly baseDuration: -1 = -1;
	public static readonly durationFixed: true = true;
	public static id = -20;
}

export class ConstantPlayerStatusEffect<StateType extends object = any> extends GenericPlayerStatusEffect<StateType> {
	public static readonly baseDuration: -1 = -1;
	public static readonly durationFixed: true = true;
	public static id = -30;
}

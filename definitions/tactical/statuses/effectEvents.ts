import InterfaceTypes from '../../core/InterfaceTypes';
import { IPlayerBase } from '../../orbital/base/types';
import { TPlayerId } from '../../player/playerId';
import GameStats from '../../stats/types';
import Abilities from '../abilities/types';
import UnitAttack from '../damage/attack';
import Damage from '../damage/damage';
import { UnitDefense } from '../damage/defense';
import { ICombatMap } from '../map';
import Salvage from '../salvage/types';
import UnitActions from '../units/actions/types';
import Units from '../units/types';
import StatusEffectMeta from './statusEffectMeta';
import { IStatusEffect } from './types';

export namespace EffectEvents {
	export type TEffectEventDecorator<T extends StatusEffectMeta.effectAgentTypes> = {
		type: T;
	};

	export type TGlobalEffectMeta = {
		turnIndex: number;
		globalTurnIndex?: number;
		turnTick?: number;
		bases?: Record<TPlayerId, IPlayerBase>;
		turnStats?: GameStats.TTurnStats[];
	};

	export type TMapContextEffectMeta = {
		map?: ICombatMap;
	};

	export type TUnitContextEffectMeta = {
		phase: UnitActions.TUnitActionChoice<UnitActions.unitActionTypes>;
		phaseIndex: number;
		reason?: StatusEffectMeta.TStatusEffectTarget;
	};

	export type TUnitMapContextEffectMeta = TUnitContextEffectMeta & {
		unit: Units.IUnit;
	};

	export type TPlayerContextEffectMeta = {};

	export type TDefaultEffectMeta = Partial<TGlobalEffectMeta> & Partial<TMapContextEffectMeta>;
	export type TInjectableEffectMeta = Partial<TDefaultEffectMeta> &
		Partial<TUnitContextEffectMeta> &
		Partial<TPlayerContextEffectMeta>;

	export enum TMapEffectEvents {
		turnEnd = 'map_effect_event_turn_end',
		turnStart = 'map_effect_event_turn_start',
		apply = 'map_effect_event_apply', // effect instantiated
		expire = 'map_effect_event_expire', // effect expired or dispelled
		unitEntersZone = 'map_effect_unit_enters_zone',
		unitMoves = 'map_effect_unit_moves',
		unitLeavesZone = 'map_effect_unit_leaves_zone',
		unitDeployed = 'map_effect_unit_deployed', // unit is deployed into zone
		unitKilled = 'map_effect_unit_killed', // unit is killed inside zone
		actionPhase = 'map_effect_event_action_phase', // unit enters a new Action Phase inside zone
		salvagePicked = 'map_effect_salvage_picked', // player picks salvage inside zone
		unitAttack = 'map_effect_event_unit_attack', // unit performs an attack
		unitHit = 'map_effect_event_unit_hit', // unit lands an attack
		unitDamaged = 'map_effect_event_unit_damaged', // unit takes damage
		unitInvoke = 'map_effect_event_unit_cast', // unit uses ability
		unitKill = 'map_effect_event_unit_kill', // unit lands a destroy
		unitAttacked = 'map_effect_event_unit_attacked', // unit takes attack
		targetedWithAbility = 'map_effect_event_spell', // unit is a target of an ability
	}

	export type IMapEventMetaData<T extends TMapEffectEvents> = T extends TMapEffectEvents.unitKilled
		? {
				source: StatusEffectMeta.TStatusEffectTarget;
				unit: Units.IUnit;
				salvage: Salvage.ISalvage;
		  }
		: T extends TMapEffectEvents.salvagePicked
		? {
				salvage: Salvage.ISalvage;
		  }
		: T extends TMapEffectEvents.actionPhase
		? TUnitMapContextEffectMeta & {}
		: T extends TMapEffectEvents.turnEnd
		? {}
		: T extends TMapEffectEvents.turnStart
		? {}
		: T extends TMapEffectEvents.apply
		? {}
		: T extends TMapEffectEvents.expire
		? {}
		: T extends TMapEffectEvents.unitEntersZone
		? TUnitMapContextEffectMeta
		: T extends TMapEffectEvents.unitMoves
		? TUnitMapContextEffectMeta & {
				from: InterfaceTypes.IVector;
		  }
		: T extends TMapEffectEvents.unitLeavesZone
		? TUnitMapContextEffectMeta
		: T extends TMapEffectEvents.unitDeployed
		? {
				unit: Units.IUnit;
		  }
		: T extends TMapEffectEvents.unitKilled
		? TUnitMapContextEffectMeta & {
				readonly source: StatusEffectMeta.TStatusEffectTarget;
				salvage: Salvage.ISalvage;
		  }
		: T extends TMapEffectEvents.unitAttacked
		? TUnitMapContextEffectMeta & {
				readonly source: StatusEffectMeta.TStatusEffectTarget;
				readonly attack: UnitAttack.IAttackBatch; //can't be mutated
				result: UnitDefense.IAttackEffectOnDefense; //can be mutated
		  }
		: T extends TMapEffectEvents.unitHit
		? TUnitMapContextEffectMeta & {
				readonly target: Units.IUnit;
				readonly attack: UnitAttack.IAttackBatch; //can't be mutated
				result: UnitDefense.IAttackEffectOnDefense; //can be mutated
		  }
		: T extends TMapEffectEvents.unitDamaged
		? TUnitMapContextEffectMeta & {
				readonly source: StatusEffectMeta.TStatusEffectTarget;
				damage: Damage.TDamageUnit;
		  }
		: T extends TMapEffectEvents.unitKill
		? TUnitMapContextEffectMeta & {
				readonly target: Units.IUnit;
		  }
		: T extends TMapEffectEvents.unitAttack
		? TUnitMapContextEffectMeta & {
				readonly target: Units.IUnit;
				attack: UnitAttack.IAttackBatch; // can be mutated
		  }
		: T extends TMapEffectEvents.unitInvoke
		? TUnitMapContextEffectMeta & Abilities.TAbilityCastMetaOption
		: T extends TMapEffectEvents.targetedWithAbility
		? {
				readonly source: StatusEffectMeta.TStatusEffectTarget;
				readonly ability: Abilities.IAbility<
					| Abilities.abilityTargetTypes.unit
					| Abilities.abilityTargetTypes.self
					| Abilities.abilityTargetTypes.around
				>;
		  }
		: never;

	export type TMapEffectMeta<T extends TMapEffectEvents> = IMapEventMetaData<T> &
		TDefaultEffectMeta &
		TEffectMetaDecorator<StatusEffectMeta.effectAgentTypes.map, T>;

	export enum TPlayerEffectEvents {
		turnEnd = 'player_effect_event_turn_end',
		turnStart = 'player_effect_event_turn_start',
		apply = 'player_effect_event_apply', // effect instantiated
		expire = 'player_effect_event_expire', // effect expires or dispelled
		spell = 'player_effect_event_spell', // player is a target of an ability
		cast = 'player_effect_event_cast', // player invokes an ability
		unitBuilt = 'player_effect_event_unit_built', // player builds a unit
		unitDeployed = 'player_effect_event_unit_deployed', // player deploys a unit
		baseAttacked = 'player_effect_event_base_attacked', // player's Base takes Attack
		baseDamaged = 'player_effect_event_base_damaged', // player's Base takes damage
		xpGained = 'player_effect_event_xp_gained', // player gains XP
		salvagePicked = 'player_effect_salvage_picked', // player picks salvage
		retreat = 'player_effect_retreat', // player Retreats from combat
	}

	export type IPlayerEventMetaData<T extends TPlayerEffectEvents> = T extends TPlayerEffectEvents.apply
		? {}
		: T extends TPlayerEffectEvents.turnEnd
		? {}
		: T extends TPlayerEffectEvents.turnStart
		? {}
		: T extends TPlayerEffectEvents.expire
		? {}
		: T extends TPlayerEffectEvents.cast
		? Abilities.TAbilityCastMetaOption
		: T extends TPlayerEffectEvents.spell
		? {
				source: StatusEffectMeta.TStatusEffectTarget;
				ability: Abilities.IAbility<
					Abilities.abilityTargetTypes.player_self | Abilities.abilityTargetTypes.player
				>;
		  }
		: T extends TPlayerEffectEvents.unitBuilt
		? {
				unit: Units.IUnit;
		  }
		: T extends TPlayerEffectEvents.baseAttacked
		? TUnitContextEffectMeta & {
				attack: UnitAttack.TAttackUnit;
				source: StatusEffectMeta.TStatusEffectTarget;
		  }
		: T extends TPlayerEffectEvents.baseDamaged
		? {
				damage: number;
				source: StatusEffectMeta.TStatusEffectTarget;
		  }
		: T extends TPlayerEffectEvents.unitDeployed
		? {
				unit: Units.IUnit;
		  }
		: T extends TPlayerEffectEvents.salvagePicked
		? {
				salvage: Salvage.ISalvage;
		  }
		: T extends TPlayerEffectEvents.xpGained
		? {
				xp: number;
		  }
		: T extends TPlayerEffectEvents.retreat
		? {}
		: never;

	export type TPlayerEffectMeta<T extends TPlayerEffectEvents> = TDefaultEffectMeta &
		TPlayerContextEffectMeta &
		IPlayerEventMetaData<T> &
		TEffectMetaDecorator<StatusEffectMeta.effectAgentTypes.player, T>;

	export enum TUnitEffectEvents {
		actionPhase = 'unit_effect_event_action_phase', // unit enters a new Action Phase
		turnEnd = 'unit_effect_event_turn_end',
		turnStart = 'unit_effect_event_turn_start',
		apply = 'unit_effect_event_apply', // effect instantiated
		deployed = 'unit_effect_event_deployed', // unit deployed to surface
		expire = 'unit_effect_event_expire', // effect expires or dispelled
		unitKilled = 'unit_effect_event_unit_killed', // unit is destroyed
		xpGained = 'unit_effect_event_xp_gained', // unit gains XP
		unitAttack = 'unit_effect_event_unit_attack', // unit performs an attack
		unitHit = 'unit_effect_event_unit_hit', // unit lands an attack
		unitDamaged = 'unit_effect_event_unit_damaged', // unit takes damage
		unitInvoke = 'unit_effect_event_unit_cast', // unit uses ability
		unitKill = 'unit_effect_event_unit_kill', // unit lands a destroy
		unitAttacked = 'unit_effect_event_unit_attacked', // unit takes attack
		targetedWithAbility = 'unit_effect_event_spell', // unit is a target of an ability
		move = 'unit_effect_event_move', // unit moves
	}

	export type IUnitEventMetaData<T extends TUnitEffectEvents> = T extends TUnitEffectEvents.apply
		? TUnitContextEffectMeta & {}
		: T extends TUnitEffectEvents.actionPhase
		? TUnitContextEffectMeta & {}
		: T extends TUnitEffectEvents.turnEnd
		? {}
		: T extends TUnitEffectEvents.turnStart
		? {}
		: T extends TUnitEffectEvents.deployed
		? {}
		: T extends TUnitEffectEvents.expire
		? TUnitContextEffectMeta & {}
		: T extends TUnitEffectEvents.xpGained
		? TUnitContextEffectMeta & { xp: number }
		: T extends TUnitEffectEvents.unitKilled
		? TUnitContextEffectMeta & {
				readonly source: StatusEffectMeta.TStatusEffectTarget;
				salvage: Salvage.ISalvage;
		  }
		: T extends TUnitEffectEvents.unitAttacked
		? TUnitContextEffectMeta & {
				readonly source: StatusEffectMeta.TStatusEffectTarget;
				readonly attack: UnitAttack.IAttackBatch; //can't be mutated
				result: UnitDefense.IAttackEffectOnDefense; //can be mutated
		  }
		: T extends TUnitEffectEvents.unitHit
		? TUnitContextEffectMeta & {
				readonly target: Units.IUnit;
				readonly attack: UnitAttack.IAttackBatch; //can't be mutated
				result: UnitDefense.IAttackEffectOnDefense; //can be mutated
		  }
		: T extends TUnitEffectEvents.unitDamaged
		? TUnitContextEffectMeta & {
				readonly source: StatusEffectMeta.TStatusEffectTarget;
				damage: Damage.TDamageUnit; // immutable;
		  }
		: T extends TUnitEffectEvents.unitKill
		? TUnitContextEffectMeta & {
				readonly target: Units.IUnit;
		  }
		: T extends TUnitEffectEvents.unitAttack
		? TUnitContextEffectMeta & {
				readonly target: Units.IUnit;
				attack: UnitAttack.IAttackBatch; // can be mutated
		  }
		: T extends TUnitEffectEvents.unitInvoke
		? TUnitContextEffectMeta & Abilities.TAbilityCastMetaOption
		: T extends TUnitEffectEvents.targetedWithAbility
		? {
				readonly source: StatusEffectMeta.TStatusEffectTarget;
				readonly ability: Abilities.IAbility<
					| Abilities.abilityTargetTypes.unit
					| Abilities.abilityTargetTypes.self
					| Abilities.abilityTargetTypes.around
				>;
		  }
		: T extends TUnitEffectEvents.move
		? TUnitContextEffectMeta & {
				readonly from: InterfaceTypes.IVector;
				readonly to: InterfaceTypes.IVector;
		  }
		: never;

	export type TUnitEffectMeta<T extends TUnitEffectEvents> = TDefaultEffectMeta &
		IUnitEventMetaData<T> &
		TEffectMetaDecorator<StatusEffectMeta.effectAgentTypes.unit, T>;

	export type TEffectEvent<T extends StatusEffectMeta.effectAgentTypes> =
		T extends StatusEffectMeta.effectAgentTypes.player
			? ValueOf<TPlayerEffectEvents>
			: T extends StatusEffectMeta.effectAgentTypes.unit
			? ValueOf<TUnitEffectEvents>
			: T extends StatusEffectMeta.effectAgentTypes.map
			? ValueOf<TMapEffectEvents>
			: never;

	export type TEffectEventList<T extends StatusEffectMeta.effectAgentTypes> =
		T extends StatusEffectMeta.effectAgentTypes.player
			? TPlayerEffectEvents
			: T extends StatusEffectMeta.effectAgentTypes.unit
			? TUnitEffectEvents
			: T extends StatusEffectMeta.effectAgentTypes.map
			? TMapEffectEvents
			: never;

	export type TFilteredEventList<T extends StatusEffectMeta.effectAgentTypes, S extends string> = Extract<
		S,
		TEffectEventList<T>
	>;

	export type TEffectMetaDecorator<T extends StatusEffectMeta.effectAgentTypes, E extends TEffectEventList<T>> = {
		event: E;
	} & TEffectEventDecorator<T>;

	export type TEffectEventMeta<
		T extends StatusEffectMeta.effectAgentTypes,
		E extends TEffectEventList<T>,
	> = T extends StatusEffectMeta.effectAgentTypes.unit
		? E extends TUnitEffectEvents
			? TUnitEffectMeta<E>
			: never
		: T extends StatusEffectMeta.effectAgentTypes.player
		? E extends TPlayerEffectEvents
			? TPlayerEffectMeta<E>
			: never
		: T extends StatusEffectMeta.effectAgentTypes.map
		? E extends TMapEffectEvents
			? TMapEffectMeta<E>
			: never
		: never;

	export type TEffectScriptFunction<
		TargetType extends StatusEffectMeta.effectAgentTypes,
		EventType extends TEffectEventList<TargetType>,
		StateType extends object = any,
	> = (
		target: StatusEffectMeta.TTargetType<TargetType>,
		effect: IStatusEffect<TargetType, StateType>,
		meta?: TEffectEventMeta<TargetType, EventType>,
	) => StateType;

	export type TEffectCallback<TargetType extends StatusEffectMeta.effectAgentTypes> = {
		[K in TEffectEventList<TargetType>]?: (meta?: TEffectEventMeta<TargetType, K>) => boolean;
	};

	export type TEffectScriptDefinition<
		TargetType extends StatusEffectMeta.effectAgentTypes,
		StateType extends object = any,
	> = {
		[K in TEffectEventList<TargetType>]?: TEffectScriptFunction<TargetType, K, StateType>;
	};
}
export default EffectEvents;

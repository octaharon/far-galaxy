import GenericAuraEffect from './GenericAura';
import { TAuraItem } from './types';

let AuraList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), AuraList);
AuraList = AuraList.filter(
	(module: any) => module.default.prototype instanceof GenericAuraEffect && module.default.id > 0,
).map((module: any) => module.default);
export default AuraList as TAuraItem[];

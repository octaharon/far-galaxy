import { CollectibleFactory } from '../../../core/Collectible';
import AuraList from './index';
import { IAura, IAuraFactory, TAuraProps, TSerializedAura } from './types';

export class AuraManager extends CollectibleFactory<TAuraProps, TSerializedAura, IAura> implements IAuraFactory {
	public instantiate(p: Partial<TAuraProps>, id: number) {
		const c = this.__instantiateCollectible(p, id);
		if (!p || !Number.isFinite(p.range)) c.range = c.static.baseRange;
		return c;
	}

	public unserialize(t: TSerializedAura) {
		const c = this.__instantiateCollectible(t, t.id);
		c.setInstanceId(t.instanceId);
		return c;
	}

	public fill() {
		this.__fillItems(AuraList);
		return this;
	}
}

export default AuraManager;

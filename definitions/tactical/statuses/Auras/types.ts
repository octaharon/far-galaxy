import {
	ICollectible,
	ICollectibleFactory,
	IStaticCollectible,
	TCollectible,
	TSerializedCollectible,
} from '../../../core/Collectible';
import { TEffectApplianceList } from '../types';

export type TAuraProps = {
	range: number;
};
export type TSerializedAura = TAuraProps & TSerializedCollectible;

export type IAuraStatic = IStaticCollectible &
	ClassConstructor<IAura> & {
		appliedEffects: TEffectApplianceList;
		selective: boolean;
		baseRange: number;
	};

export interface IAura extends TAuraProps, ICollectible<TAuraProps, TSerializedAura, IAuraStatic> {}

export type IAuraFactory = ICollectibleFactory<TAuraProps, TSerializedAura, IAura>;

export type TAuraAppliance = {
	id: number;
	props?: Partial<TAuraProps>;
};

export type TAuraApplianceList = TAuraAppliance[];
export type TAuraItem<O extends IAura = IAura> = TCollectible<O, IAuraStatic>;

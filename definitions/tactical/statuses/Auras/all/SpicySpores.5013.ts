import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../constants';
import SpicySporesStatusEffect, {
	SpicySporesDamage,
	SpicySporesDamageScale,
	SpicySporesXP,
} from '../../Effects/all/Auras/SpicySpores.05013';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';

export class SpicySporesAura extends GenericAuraEffect {
	public static readonly id = 5013;
	public static readonly caption = 'Spicy Spores';
	public static readonly description = `Units in Effect Radius gain extra ${SpicySporesXP} XP for Kills, but at Turn End they take ${numericAPScaling(
		SpicySporesDamageScale,
		AbilityPowerUIToken,
		SpicySporesDamage,
	)} BIO Damage as a Cloud Attack`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: SpicySporesStatusEffect.id,
		},
	];
	public static readonly selective = false;
	public static readonly baseRange = 4;
}

export default SpicySporesAura;

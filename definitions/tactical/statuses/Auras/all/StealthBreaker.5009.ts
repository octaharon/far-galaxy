import StealthBreakerEffect, { StealthBreakerXP } from '../../Effects/all/Auras/StealthBreaker.05009';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';

export class StealthBreakerAura extends GenericAuraEffect {
	public static readonly id = 5009;
	public static readonly caption = 'Stealth Breaker';
	public static readonly description = `A powerful equipment counteracts any radar suppression systems, preventing all Hostile Units in Effect Radius from maintaining Cloak. Breaking a Unit out of Cloak grants ${StealthBreakerXP} XP`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: StealthBreakerEffect.id,
		},
	];
	public static readonly selective = false;
	public static readonly baseRange = 5;
}

export default StealthBreakerAura;

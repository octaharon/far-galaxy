import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../constants';
import SyncreticEvolutionEffect, {
	SyncreticEvolutionShieldBonus,
	SyncreticEvolutionXpRatio,
	SyncreticEvolutionXpScale,
} from '../../Effects/all/Auras/SyncreticEvolution.05010';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';

export class SyncreticEvolutionAura extends GenericAuraEffect {
	public static readonly id = 5010;
	public static readonly caption = 'Syncretic Evolution';
	public static readonly description = `Each affected Unit restores ${SyncreticEvolutionShieldBonus} Shield Capacity both to self and to this Unit at Turn End. This Unit gains ${numericAPScaling(
		SyncreticEvolutionXpScale,
		AbilityPowerUIToken,
		SyncreticEvolutionXpRatio,
		true,
	)} of XP collected by affected Units`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: SyncreticEvolutionEffect.id,
		},
	];
	public static readonly selective = true;
	public static readonly baseRange = 4;
}

export default SyncreticEvolutionAura;

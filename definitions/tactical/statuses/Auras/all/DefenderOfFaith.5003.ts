import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../maths/BasicMaths';
import { AbilityPowerUIToken } from '../../constants';
import DefenderOfFaithStatusEffect, {
	DefenderOfFaithDamageBonus,
	DefenderOfFaithProtectionBonus,
	DefenderOfFaithXP,
} from '../../Effects/all/Auras/DefenderOfFaith.05003';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';
import describeModifier = BasicMaths.describeModifier;

export class DefenderOfFaithAura extends GenericAuraEffect {
	public static readonly id = 5003;
	public static readonly caption = 'Defender Of Faith';
	public static readonly description = `Gain ${describeModifier({
		bias: DefenderOfFaithProtectionBonus,
	})} Armor Protection and ${numericAPScaling(
		DefenderOfFaithDamageBonus,
		AbilityPowerUIToken,
		null,
		true,
	)} Damage for each Unit within Effect Radius. At Turn End receive ${DefenderOfFaithXP} XP for each Hostile Unit in Effect Radius`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DefenderOfFaithStatusEffect.id,
		},
	];
	public static readonly selective = false;
	public static readonly baseRange = 4;
}

export default DefenderOfFaithAura;

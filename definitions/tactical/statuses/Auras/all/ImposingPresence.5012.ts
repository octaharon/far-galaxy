import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../maths/BasicMaths';
import { AbilityPowerUIToken } from '../../constants';
import DemoralizedStatusEffect, {
	ImposingPresenceAttackRangePenalty,
	ImposingPresenceHitChancePenalty,
	ImposingPresenceHitChancePenaltyScale,
} from '../../Effects/all/Auras/Demoralized.05012';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';
import describeModifier = BasicMaths.describeModifier;

export class ImposingPresenceAura extends GenericAuraEffect {
	public static readonly id = 5012;
	public static readonly caption = 'Imposing Presence';
	public static readonly description = `Nearby Hostile Units are Demoralized, taking ${describeModifier({
		bias: -ImposingPresenceAttackRangePenalty,
	})} penalty to Attack Range and ${numericAPScaling(
		-ImposingPresenceHitChancePenaltyScale,
		AbilityPowerUIToken,
		-ImposingPresenceHitChancePenalty,
		true,
	)} to Hit Chance`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DemoralizedStatusEffect.id,
		},
	];
	public static readonly selective = false;
	public static readonly baseRange = 4;
}

export default ImposingPresenceAura;

import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../constants';
import SermonStatusEffect, {
	SermonAccuracyBonus,
	SermonAccuracyBonusScale,
	SermonHealthBonus,
} from '../../Effects/all/Auras/Sermon.05002';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';

export class ChaplainSermonAura extends GenericAuraEffect {
	public static readonly id = 5002;
	public static readonly caption = 'Sermon';
	public static readonly description = `All Allied Units gain ${numericAPScaling(
		SermonAccuracyBonusScale,
		AbilityPowerUIToken,
		SermonAccuracyBonus,
		true,
	)} Hit Chance and additionally restore ${numericAPScaling(
		SermonHealthBonus,
		AbilityPowerUIToken,
		null,
		true,
	)} of Max Hit Points at Turn End if they're Organic`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: SermonStatusEffect.id,
		},
	];
	public static readonly selective = true;
	public static readonly baseRange = 3;
}

export default ChaplainSermonAura;

import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../maths/BasicMaths';
import { AbilityPowerUIToken } from '../../constants';
import BladeCloudEffect, {
	CloudOfBlades_DamagePerDistanceUnit,
	CloudOfBlades_ScanRangePenalty,
	CloudOfBlades_ScanRangePenaltyScale,
} from '../../Effects/all/Auras/BladeCloud.05007';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';
import describePercentileModifier = BasicMaths.describePercentileModifier;

export class CloudOfBladesAura extends GenericAuraEffect {
	public static readonly id = 5007;
	public static readonly caption = 'Cloud Of Blades';
	public static readonly description = `This Unit is surrounded by a cloud of fast-moving blade-like creatures, that drain up to ${numericAPScaling(
		CloudOfBlades_DamagePerDistanceUnit,
		AbilityPowerUIToken,
	)} Shield Capacity from Hover and Airborne Units for every 1 DU they Move inside the Effect Radius, while also granting them ${describePercentileModifier(
		{ bias: 0.5 },
	)} Dodge vs Aerial. Ground and Naval Units instead have their Scan Range reduced by ${numericAPScaling(
		CloudOfBlades_ScanRangePenaltyScale,
		AbilityPowerUIToken,
		CloudOfBlades_ScanRangePenalty,
	)}`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: BladeCloudEffect.id,
		},
	];
	public static readonly selective = false;
	public static readonly baseRange = 3;
}

export default CloudOfBladesAura;

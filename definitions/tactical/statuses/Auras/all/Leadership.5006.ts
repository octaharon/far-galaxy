import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../constants';
import LeadershipEffect, {
	LeadershipDamageBonus,
	LeadershipDamageBonusScale,
} from '../../Effects/all/Auras/Leadership.05006';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';

export class LeadershipAura extends GenericAuraEffect {
	public static readonly id = 5006;
	public static readonly caption = 'Leadership';
	public static readonly description = `Nearby Allied Units have ${numericAPScaling(
		LeadershipDamageBonusScale,
		AbilityPowerUIToken,
		LeadershipDamageBonus,
		true,
	)} Damage on all Weapons`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: LeadershipEffect.id,
		},
	];
	public static readonly selective = true;
	public static readonly baseRange = 3;
}

export default LeadershipAura;

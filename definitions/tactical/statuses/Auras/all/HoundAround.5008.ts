import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../maths/BasicMaths';
import { AbilityPowerUIToken } from '../../constants';
import HoundAroundEffect, {
	HoundEffectPenetrationBonus,
	HoundScanRangeBonus,
	HoundScanRangeScale,
} from '../../Effects/all/Auras/HoundAround.05008';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';
import describeModifier = BasicMaths.describeModifier;

export class HoundAroundAura extends GenericAuraEffect {
	public static readonly id = 5008;
	public static readonly caption = 'Hound Around';
	public static readonly description = `Yield nearby Allied Units ${numericAPScaling(
		HoundScanRangeScale,
		AbilityPowerUIToken,
		HoundScanRangeBonus,
		true,
	)} Scan Range and ${describeModifier({ bias: HoundEffectPenetrationBonus })} Effect Penetration`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: HoundAroundEffect.id,
		},
	];
	public static readonly selective = true;
	public static readonly baseRange = 5;
}

export default HoundAroundAura;

import { biasToPercent, numericAPScaling } from '../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../constants';
import DecoyClusterEffect, {
	DecoyClusterDamageReductionBase,
	DecoyClusterDamageXPScale,
	DecoyClusterRange,
} from '../../Effects/all/Auras/DecoyCluster.05011';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';

export class DecoyClusterAura extends GenericAuraEffect {
	public static readonly id = 5011;
	public static readonly caption = 'Decoy Cluster';
	public static readonly description = `Whenever a nearby Unit takes Damage to Hit Points, ${biasToPercent(
		DecoyClusterDamageReductionBase,
	)} of that Damage is transferred to Sentinel. If the Sentinel mitigates that Damage, it gains XP equal to ${numericAPScaling(
		DecoyClusterDamageXPScale,
		AbilityPowerUIToken,
		null,
		true,
	)} of it`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DecoyClusterEffect.id,
		},
	];
	public static readonly selective = false;
	public static readonly baseRange = DecoyClusterRange;
}

export default DecoyClusterAura;

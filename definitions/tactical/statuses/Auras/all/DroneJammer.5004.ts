import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../constants';
import DroneJammerStatusEffect, {
	DroneJammerDodge,
	DroneJammerDodgeScale,
} from '../../Effects/all/Auras/DroneJammer.05004';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';

export class DroneJammerAura extends GenericAuraEffect {
	public static readonly id = 5004;
	public static readonly caption = 'Drone Jammer';
	public static readonly description = `All Units in Effect Radius gain ${numericAPScaling(
		DroneJammerDodgeScale,
		AbilityPowerUIToken,
		DroneJammerDodge,
		true,
	)} Dodge vs Drone`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DroneJammerStatusEffect.id,
		},
	];
	public static readonly selective = false;
	public static readonly baseRange = 5;
}

export default DroneJammerAura;

import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../constants';
import ConstantRegenerationEffect from '../../Effects/all/Regeneration/ConstantRegeneration.01101';
import { TEffectApplianceList, TStatusEffectStateLookup } from '../../types';
import GenericAuraEffect from '../GenericAura';

export const SVNanoCloudHealAmount = 15;

export class SVNanoCloudAura extends GenericAuraEffect {
	public static readonly id = 5001;
	public static readonly caption = 'Nano Cloud';
	public static readonly description = `All Allied Units within Effect Radius regenerate ${numericAPScaling(
		SVNanoCloudHealAmount,
		AbilityPowerUIToken,
	)} Hit Points in the end of Turn`;
	public static readonly appliedEffects: TEffectApplianceList<
		TStatusEffectStateLookup<typeof ConstantRegenerationEffect>
	> = [
		{
			effectId: ConstantRegenerationEffect.id,
			props: {
				healAmount: 15,
			},
		},
	];
	public static readonly selective = true;
	public static readonly baseRange = 4;
}

export default SVNanoCloudAura;

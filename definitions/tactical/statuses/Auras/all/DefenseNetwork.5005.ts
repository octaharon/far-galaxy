import { biasToPercent, numericAPScaling } from '../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../constants';
import DefenseNetworkEffect, {
	DefenseNetworkShieldBonusBase,
	DefenseNetworkShieldBonusScale,
	DefenseNetworkShieldRestore,
} from '../../Effects/all/Auras/DefenseNetwork.05005';
import { TEffectApplianceList } from '../../types';
import GenericAuraEffect from '../GenericAura';

export class DefenseNetworkAura extends GenericAuraEffect {
	public static readonly id = 5005;
	public static readonly caption = 'Defense Network';
	public static readonly description = `${numericAPScaling(
		DefenseNetworkShieldBonusScale,
		AbilityPowerUIToken,
		DefenseNetworkShieldBonusBase,
		true,
	)} Shield Protection to nearby Allied Units. In the End of Turn restore ${biasToPercent(
		DefenseNetworkShieldRestore,
	)} of Shield Capacity to every Allied Unit in Radius`;
	public static readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DefenseNetworkEffect.id,
		},
	];
	public static readonly selective = true;
	public static readonly baseRange = 4;
}

export default DefenseNetworkAura;

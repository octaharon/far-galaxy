import Collectible from '../../../core/Collectible';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../../../core/DI/injections';
import { TEffectApplianceList } from '../types';
import { IAura, IAuraStatic, TAuraProps, TSerializedAura } from './types';

export class GenericAuraEffect extends Collectible<TAuraProps, TSerializedAura, IAuraStatic> implements IAura {
	public static readonly appliedEffects: TEffectApplianceList = [];
	public static readonly selective: boolean = false;
	public static readonly baseRange: number = 0;
	public static readonly id: number = 0;
	public range: number;

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.aura];
	}

	public serialize(): TSerializedAura {
		return {
			...this.__serializeCollectible(),
			range: this.range,
		};
	}
}

export default GenericAuraEffect;

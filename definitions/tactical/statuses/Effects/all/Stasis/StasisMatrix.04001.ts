import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import Units from '../../../../units/types';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	previousStatus?: Partial<Units.TUnitCombatStatus>;
}

export const STASIS_MATRIX_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.2;

export class StasisMatrixEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 4001;
	public static readonly baseDuration = 2;
	public static readonly negative = false;
	public static readonly unremovable = false;
	public static readonly caption = 'Stasis warp';
	public static readonly description = 'This unit is in Stasis and is out of the timeline';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: StasisMatrixEffect) => {
			const r = {
				...effect.state,
				previousStatus: {
					disabled: unit.status.disabled,
					targetable: unit.status.targetable,
				} as Partial<Units.TUnitCombatStatus>,
			};
			const attacksPrevented = unit.attackFramesLeft.flatMap((x) => x).length;
			if (attacksPrevented > 0 && effect.static.isUnitTarget(effect.source))
				effect.source.addXp(STASIS_MATRIX_XP * attacksPrevented);
			unit.status.disabled = true;
			unit.status.targetable = false;
			return r;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: StasisMatrixEffect) => {
			unit.status.disabled = effect.state.previousStatus.disabled;
			unit.status.targetable = effect.state.previousStatus.targetable;
			return effect.state;
		},
	};
	public priority = -1;
}

export default StasisMatrixEffect;

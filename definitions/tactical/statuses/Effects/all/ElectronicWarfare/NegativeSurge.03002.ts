import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const NEGATIVE_SURGE_DAMAGE = 35;
const NEGATIVE_SURGE_DAMAGE_SCALE = 15;
const NEGATIVE_SURGE_HP = 35;
const NEGATIVE_SURGE_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class NegativeSurgeEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 3002;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Negative Surge';
	public static readonly description = `Restores a Unit ${AbilityPowerUIToken}${NEGATIVE_SURGE_HP} Hit Points per every Non-Negative Status Effect it has, including this one, and deals ${numericAPScaling(
		NEGATIVE_SURGE_DAMAGE_SCALE,
		AbilityPowerUIToken,
		NEGATIVE_SURGE_DAMAGE,
	)} RAD Damage to it per every Negative Status Effect it bears. If there is at least one of each, the invoker gains ${NEGATIVE_SURGE_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: NegativeSurgeEffect) => {
			let positive = false;
			let negative = false;
			unit.getStatusEffects().forEach((e) => {
				if (e.static.negative) {
					negative = true;
					unit.applyAttackUnit(
						{
							delivery: UnitAttack.deliveryType.immediate,
							[Damage.damageType.radiation]:
								NEGATIVE_SURGE_DAMAGE + NEGATIVE_SURGE_DAMAGE_SCALE * effect.power,
						},
						effect.source,
					);
				} else {
					positive = true;
					unit.addHp(NEGATIVE_SURGE_HP * effect.power, effect.source);
				}
			});
			if (negative && positive && effect.static.isUnitTarget(effect.source))
				effect.source.addXp(NEGATIVE_SURGE_XP);
			return {} as IState;
		},
	};
	public priority = -2;
}

export default NegativeSurgeEffect;

import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const SHUTDOWN_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.2;

export class ShutdownStatusEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 3006;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Shutdown';
	public static readonly description = `Disables Unit Abilities and resets their Cooldowns. For every Ability that have its Cooldown shortened the invoker gains ${SHUTDOWN_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: ShutdownStatusEffect) => {
			const src = effect.static.isUnitTarget(effect.source) ? effect.source : null;
			unit.kit.getStableSortedAbilities().forEach((a) => {
				if (src && a.cooldownLeft > 0) src.addXp(SHUTDOWN_XP);
				a.cooldownLeft = 0;
				a.usable = false;
			});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: ShutdownStatusEffect) => {
			unit.kit.getStableSortedAbilities().forEach((a) => {
				a.usable = true;
			});
			return effect.state;
		},
	};
	public priority = -2;
}

export default ShutdownStatusEffect;

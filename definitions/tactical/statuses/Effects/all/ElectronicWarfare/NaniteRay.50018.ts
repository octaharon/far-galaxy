import { describePercentage, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	attackSourcesMap: Record<string, boolean>;
}

const NANITE_RAY_HP = 15;
const NANITE_RAY_HP_SCALE = 5;
const NANITE_RAY_HP_BONUS = 0.25;
const NANITE_RAY_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class NaniteRayEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 50018;
	public static readonly negative = false;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Nanite Ray';
	public static readonly description = `Restores a Unit ${describePercentage(
		NANITE_RAY_HP_BONUS,
	)} of missing Hit Points instantly and additionally ${numericAPScaling(
		NANITE_RAY_HP_SCALE,
		AbilityPowerUIToken,
		NANITE_RAY_HP,
	)} Hit Points every Turn End. Effect Duration is extended by 1 Turn every time a new foe attacks the Unit, and if this happens at least once, the invoker gains ${NANITE_RAY_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: NaniteRayEffect, meta) => {
			return {
				attackSourcesMap: {
					[effect.source?.getInstanceId()]: true,
				},
			};
		},
		[EffectEvents.TUnitEffectEvents.unitAttacked]: (unit, effect: NaniteRayEffect, meta) => {
			if (effect.state.attackSourcesMap[meta.source.getInstanceId()]) return effect.state;
			if (Object.keys(effect.state.attackSourcesMap).length === 1 && effect.static.isUnitTarget(effect.source))
				effect.source.addXp(NANITE_RAY_XP);
			effect.duration += 1;
			return {
				...effect.state,
				attackSourcesMap: {
					...effect.state.attackSourcesMap,
					[meta.source.getInstanceId()]: true,
				},
			};
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: NaniteRayEffect) => {
			unit.addHp(NANITE_RAY_HP_SCALE * effect.power + NANITE_RAY_HP, effect.source);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: NaniteRayEffect) => {
			unit.addHp(NANITE_RAY_HP_BONUS * (unit.maxHitPoints - unit.currentHitPoints), effect.source);
			return {
				attackSourcesMap: {},
			} as IState;
		},
	};
	public priority = -2;
}

export default NaniteRayEffect;

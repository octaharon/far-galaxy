import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import DamageManager from '../../../../damage/DamageManager';
import { getLastActionPhase } from '../../../../units/actions/ActionManager';
import UnitActions from '../../../../units/actions/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	hasBonus?: boolean;
}

export const CAMOUFLAGE_INTERCEPT_DAMAGE_BONUS = 0.2; // 25%
export const CAMOUFLAGE_CLOAK_DAMAGE_BONUS = 0.5; // 50%

export class CamouflageStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 3008;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly unremovable = false;
	public static readonly caption = 'Camouflage';
	public static readonly description = `Unit deals ${numericAPScaling(
		CAMOUFLAGE_INTERCEPT_DAMAGE_BONUS,
		AbilityPowerUIToken,
		null,
		true,
	)} extra Damage until the end of Turn, while performing Intercept or Suppress. Also Attacks, that are performed from Cloak, have extra ${numericAPScaling(
		CAMOUFLAGE_CLOAK_DAMAGE_BONUS,
		AbilityPowerUIToken,
		null,
		true,
	)} Damage and grant an extra Action Phase next Turn (doesn't stack with Delay)`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: () => ({ hasBonus: false }),
		[EffectEvents.TUnitEffectEvents.apply]: () => ({ hasBonus: false }),
		[EffectEvents.TUnitEffectEvents.actionPhase]: (unit, effect, meta) => {
			if (
				[UnitActions.unitActionTypes.suppress, UnitActions.unitActionTypes.intercept].includes(
					meta.phase.action,
				)
			)
				return { hasBonus: true };
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.unitAttack]: (unit, effect: CamouflageStatusEffect, meta?) => {
			if (unit.status.cloaked) {
				unit.actionDelayed = getLastActionPhase(unit.actions);
			} else if (!effect.state.hasBonus) return effect.state; // optimization to prevent iterating with null
			meta.attack.attacks = meta.attack?.attacks.map((a) =>
				DamageManager.applyDamageModifierToDamageUnit(
					a,
					unit.status.cloaked
						? {
								factor: 1 + CAMOUFLAGE_CLOAK_DAMAGE_BONUS * effect.power,
						  }
						: null,
					effect.state.hasBonus
						? {
								factor: 1 + CAMOUFLAGE_INTERCEPT_DAMAGE_BONUS * effect.power,
						  }
						: null,
				),
			);
			return effect.state;
		},
	};
	public priority = ATTACK_PRIORITIES.HIGHEST;
}

export default CamouflageStatusEffect;

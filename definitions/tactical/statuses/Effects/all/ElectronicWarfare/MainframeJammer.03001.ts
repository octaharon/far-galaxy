import BasicMaths from '../../../../../maths/BasicMaths';
import UnitAttack from '../../../../damage/attack';
import { MAX_MAP_RADIUS } from '../../../../map/constants';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	scanRangeReduction: number;
	weaponRangeReduction: number[];
	abilityRangeReduction: number[];
	damageTaken: boolean;
}

const MainframeJammerScanReduction = 3;
const MainframeJammerRangeReduction = 1;

export class MainframeJammerEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 3001;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Mainframe Jamming';
	public static readonly description = `Reduces Scan Range by ${AbilityPowerUIToken}${MainframeJammerScanReduction}, and Attack and Ability Range by ${AbilityPowerUIToken}${MainframeJammerRangeReduction}. If the Unit takes Damage to Hit Points during Effect Duration, all non-Negative Effects from Unit are removed when this Effect expires`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect) => {
			const c: IState = {
				scanRangeReduction: Math.min(unit.scanRange, MainframeJammerScanReduction * effect.power),
				weaponRangeReduction: unit.attacks.map((a) =>
					Math.min(a.baseRange, MainframeJammerRangeReduction * effect.power),
				),
				abilityRangeReduction: unit.kit
					.getStableSortedAbilities()
					.map((a) => Math.min(a.baseRange, MainframeJammerRangeReduction * effect.power)),
				damageTaken: false,
			};
			unit.applyChassisModifier({
				scanRange: {
					bias: -MainframeJammerScanReduction * effect.power,
					maxGuard: MAX_MAP_RADIUS - 1,
					min: 0,
				},
			});
			unit.applyWeaponModifier({
				default: {
					baseRange: {
						bias: -MainframeJammerRangeReduction * effect.power,
						maxGuard: MAX_MAP_RADIUS - 1,
						min: 0,
					},
				},
			});
			unit.applyEffectModifier({
				abilityRangeModifier: [
					{
						bias: -MainframeJammerRangeReduction * effect.power,
						maxGuard: MAX_MAP_RADIUS - 1,
						min: 0,
					},
				],
			});
			return c as IState;
		},
		[EffectEvents.TUnitEffectEvents.unitDamaged]: (unit, effect: MainframeJammerEffect, meta) => {
			return {
				...effect.state,
				damageTaken: true,
			};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: MainframeJammerEffect) => {
			unit.attacks.forEach((t: UnitAttack.IAttackInterface, ix) => {
				if (!effect.state.weaponRangeReduction[ix]) return;
				t.baseRange = BasicMaths.applyModifier(t.baseRange, {
					bias: effect.state.weaponRangeReduction[ix],
				});
			});
			unit.kit.getStableSortedAbilities().forEach((a, ix) => {
				if (!effect.state.abilityRangeReduction[ix]) return;
				a.baseRange = BasicMaths.applyModifier(a.baseRange, {
					bias: effect.state.abilityRangeReduction[ix],
				});
			});
			unit.scanRange = BasicMaths.applyModifier(unit.scanRange, {
				bias: effect.state.scanRangeReduction,
			});
			if (effect.state.damageTaken)
				unit.getStatusEffects()
					.filter((e) => !e.static.negative && !e.static.unremovable)
					.forEach((e) => {
						if (e.getInstanceId() !== effect.getInstanceId()) unit.removeEffect(e.getInstanceId());
					});
			return effect.state;
		},
	};
	public priority = 1;
}

export default MainframeJammerEffect;

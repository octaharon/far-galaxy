import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	multiplier: number;
	startingHP: number;
}

const SubspaceHysteresisScale = 0.01;
const SubspaceHysteresisMultiplier = 0.04;

export class SubspaceHysteresisEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 50016;
	public static readonly negative = true;
	public static readonly baseDuration = 1;
	public static readonly caption = 'Subspace Hysteresis';
	public static readonly description = `At Turn End, all Hit Points gained or lost under Effect Duration are gained or lost, correspondingly, again, increased by ${numericAPScaling(
		SubspaceHysteresisScale,
		AbilityPowerUIToken,
		SubspaceHysteresisMultiplier,
		true,
	)} per every Attack and/or Ability the Unit has performed that Turn`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnStart]: (unit, effect: SubspaceHysteresisEffect) => {
			return {
				...effect.state,
				multiplier: 0,
			};
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SubspaceHysteresisEffect) => {
			return {
				multiplier: effect.source?.getInstanceId() === unit.getInstanceId() ? 1 : 0,
				startingHP: unit.currentHitPoints,
			};
		},
		[EffectEvents.TUnitEffectEvents.unitAttack]: (unit, effect: SubspaceHysteresisEffect) => {
			return {
				...effect.state,
				multiplier: (effect.state.multiplier ?? 0) + 1,
			};
		},
		[EffectEvents.TUnitEffectEvents.unitInvoke]: (unit, effect: SubspaceHysteresisEffect) => {
			return {
				...effect.state,
				multiplier: (effect.state.multiplier ?? 0) + 1,
			};
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: SubspaceHysteresisEffect) => {
			const diff = unit.currentHitPoints - effect.state.startingHP;
			unit.addHp(
				diff *
					(1 +
						(SubspaceHysteresisMultiplier + SubspaceHysteresisScale * effect.power) *
							effect.state.multiplier),
				effect.source,
			);
			return {
				...effect.state,
				multiplier: 0,
			};
		},
	};
	public priority = -2;
}

export default SubspaceHysteresisEffect;

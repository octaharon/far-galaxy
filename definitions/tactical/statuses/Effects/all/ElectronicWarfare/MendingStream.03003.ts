import { ArithmeticPrecision } from '../../../../../maths/constants';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const MENDING_STREAM_HIT_POINTS = 40;
const MENDING_STREAM_DAMAGE = 60;
const MENDING_STREAM_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class MendingStreamEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 3003;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Mending Stream';
	public static readonly description = ` Heals a friendly Unit for ${AbilityPowerUIToken}${MENDING_STREAM_HIT_POINTS} Hit Points, while Hostile Units take ${AbilityPowerUIToken}${MENDING_STREAM_DAMAGE} RAD Damage instead. If the target has full Shield Capacity after that, the invoker gains ${MENDING_STREAM_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: MendingStreamEffect) => {
			if (unit.isFriendlyToPlayer(effect?.source?.playerId)) {
				unit.addHp(MENDING_STREAM_HIT_POINTS * effect.power, effect.source);
			} else {
				unit.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[Damage.damageType.radiation]: MENDING_STREAM_DAMAGE,
					},
					effect.source,
					{
						factor: effect.power,
					},
				);
			}
			if (
				unit.currentShields >= unit.totalShields - ArithmeticPrecision &&
				effect.static.isUnitTarget(effect?.source)
			)
				effect.source.addXp(MENDING_STREAM_XP);
			return {} as IState;
		},
	};
	public priority = -2;
}

export default MendingStreamEffect;

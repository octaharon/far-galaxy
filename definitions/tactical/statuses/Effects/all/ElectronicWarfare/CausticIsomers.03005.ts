import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const CausticIsomersDamage = 45;
const CausticIsomersProtection = 0.05;
const CausticIsomersAPReduction = 0.8;
const CausticIsomersProtectionScale = 0.02;
const CausticIsomersXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class CausticIsomerEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 3005;
	public static readonly negative = true;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Caustic Isomers';
	public static readonly description = ` Unit keep losing ${numericAPScaling(
		-CausticIsomersProtectionScale,
		AbilityPowerUIToken,
		-CausticIsomersProtection,
		true,
	)} Armor Protection permanently every Turn End, and its Ability Power is reduced by ${-CausticIsomersAPReduction} for the Effect Duration. Whenever a Unit invokes an Ability, it takes ${AbilityPowerUIToken}${CausticIsomersDamage} HEA Damage, and the invoker gains extra ${CausticIsomersXP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: CausticIsomerEffect) => {
			unit.applyEffectModifier({ abilityPowerModifier: [{ bias: -CausticIsomersAPReduction }] });
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: CausticIsomerEffect) => {
			unit.applyEffectModifier({ abilityPowerModifier: [{ bias: CausticIsomersAPReduction }] });
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: CausticIsomerEffect) => {
			unit.applyArmorModifier({
				default: {
					factor: 1 - (CausticIsomersProtection + CausticIsomersProtectionScale * effect.power),
				},
			});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.unitInvoke]: (unit, effect: CausticIsomerEffect, meta) => {
			unit.applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.heat]: effect.power * CausticIsomersDamage,
				},
				effect.source,
			);
			if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(CausticIsomersXP);
			return effect.state;
		},
	};
	public priority = -2;
}

export default CausticIsomerEffect;

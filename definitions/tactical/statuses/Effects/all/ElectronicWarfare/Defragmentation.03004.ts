import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const DEFRAGMENTATION_HP_PERCENT = 0.2;
const DEFRAGMENTATION_HP_SCALE = 0.1;
const DEFRAGMENTATION_DAMAGE = 75;
const DEFRAGMENTATION_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class DefragmentationEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 3004;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Defragmentation';
	public static readonly description = `Deals ${numericAPScaling(
		DEFRAGMENTATION_DAMAGE,
		AbilityPowerUIToken,
	)} ANH Damage to Organic and Robotic Units, while others are restored ${numericAPScaling(
		DEFRAGMENTATION_HP_SCALE,
		AbilityPowerUIToken,
		DEFRAGMENTATION_HP_PERCENT,
		true,
	)} of Maximum Hit Points instead. If the target is at full Hit Points after the Effect, the invoker gains ${DEFRAGMENTATION_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DefragmentationEffect) => {
			if (unit.isMechanical) {
				unit.addHp(
					unit.maxHitPoints * (DEFRAGMENTATION_HP_PERCENT + DEFRAGMENTATION_HP_SCALE * effect.power),
					effect.source,
				);
			} else {
				unit.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[Damage.damageType.antimatter]: DEFRAGMENTATION_DAMAGE,
					},
					effect.source,
					{
						factor: effect.power,
					},
				);
			}
			if (
				unit.currentHitPoints >= unit.maxHitPoints - ArithmeticPrecision &&
				effect.static.isUnitTarget(effect.source)
			)
				effect.source.addXp(DEFRAGMENTATION_XP);
			return {} as IState;
		},
	};
	public priority = -2;
}

export default DefragmentationEffect;

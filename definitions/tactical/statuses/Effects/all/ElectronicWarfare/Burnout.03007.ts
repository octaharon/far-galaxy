import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const BURNOUT_EFFECT_HP_REDUCTION = 0.25; // 25%

export class BurnoutStatusEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 3007;
	public static readonly negative = true;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Burnout';
	public static readonly description = `Unit loses ${numericAPScaling(
		BURNOUT_EFFECT_HP_REDUCTION,
		AbilityPowerUIToken,
		null,
		true,
	)} of remaining Hit Points each Turn End`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: BurnoutStatusEffect) => {
			const hp = unit.currentHitPoints * BURNOUT_EFFECT_HP_REDUCTION * effect.power;
			unit.addHp(-hp, effect.source);
			if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(hp);
			return effect.state;
		},
	};
	public priority = ATTACK_PRIORITIES.HIGHEST;
}

export default BurnoutStatusEffect;

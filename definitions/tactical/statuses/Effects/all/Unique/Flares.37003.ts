import { biasToPercent, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const FLARES_DODGE = 0.6;
const FLARES_DODGE_SCALE = 0.2;
const FLARES_DAMAGE = 25;
const FLARES_RANGE = 2;
const FLARES_REDUCTION = 0.15;
const FLARES_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.2;

export class FlaresEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37003;
	public static readonly negative = false;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Flares';
	public static readonly description = `Aircraft Dodge is increased by ${numericAPScaling(
		FLARES_DODGE_SCALE,
		AbilityPowerUIToken,
		FLARES_DODGE,
		true,
	)} against Missile and Aerial, and its Shield Protection is increased by ${biasToPercent(
		FLARES_REDUCTION,
	)}. Airborne Units in ${AbilityPowerUIToken}${FLARES_RANGE} Effect Radius take ${FLARES_DAMAGE} HEA damage, and the invoker gains ${FLARES_XP} XP for each Hostile Unit affected`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: FlaresEffect, meta?) => {
			const dodge = FLARES_DODGE + FLARES_DODGE_SCALE * effect.power;
			unit.applyChassisModifier({
				dodge: {
					[UnitAttack.deliveryType.missile]: {
						bias: dodge,
					},
					[UnitAttack.deliveryType.aerial]: {
						bias: dodge,
					},
				},
			}).applyShieldsModifier({
				default: { factor: 1 - FLARES_REDUCTION },
			});
			if (meta.map) {
				const units = meta.map
					.getUnitsInRange({ ...unit.location, radius: FLARES_RANGE * effect.power })
					.filter((u) => u.getInstanceId() !== unit.getInstanceId())
					.filter((u) => u.isAirborne);
				const xpBonus = units.filter((u) => !u.isFriendlyToPlayer(unit.playerId)).length;
				units.forEach((u) =>
					u.applyAttackUnit(
						{
							delivery: UnitAttack.deliveryType.immediate,
							[Damage.damageType.heat]: FLARES_DAMAGE,
						},
						effect.source,
					),
				);
				if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(FLARES_XP * xpBonus);
			}
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: FlaresEffect, meta?) => {
			const dodge = FLARES_DODGE + FLARES_DODGE_SCALE * effect.power;
			unit.applyChassisModifier({
				dodge: {
					[UnitAttack.deliveryType.missile]: {
						bias: -dodge,
					},
					[UnitAttack.deliveryType.aerial]: {
						bias: -dodge,
					},
				},
			}).applyShieldsModifier({
				default: { factor: 1 / (1 - FLARES_REDUCTION) },
			});
			return {};
		},
	};
	public priority = 0;
}

export default FlaresEffect;

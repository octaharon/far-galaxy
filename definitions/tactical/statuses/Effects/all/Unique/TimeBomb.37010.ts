import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

type IState = {};

const TimeBombDamage = 200;

export class TimeBombEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 37010;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Timebomb Zone';
	public static readonly description = `A zone that retains Effects and Ability Cooldowns of Units inside. When it expires, deal ${AbilityPowerUIToken}${TimeBombDamage} WRP Damage to all Units caught in Effect Radius`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.turnStart]: (cell: IMapCell, effect: TimeBombEffect, meta?) => {
				meta.map.getUnitsInRange(cell).forEach((u) => {
					if (u.kit.total)
						u.kit
							.getStableSortedAbilities()
							.forEach(
								(a) =>
									(a.cooldownLeft =
										a.cooldownLeft > 0 ? Math.min(a.cooldownLeft + 1, a.cooldown) : 0),
							);
					if (u.effects)
						u.effects.forEach((e) => (e.duration = e.duration >= 0 ? e.duration + 1 : e.duration));
				});
				return {};
			},
			[EffectEvents.TMapEffectEvents.expire]: (cell: IMapCell, effect: TimeBombEffect, meta?) => {
				meta.map.applyAoEAttack(
					cell,
					{
						[UnitAttack.attackFlags.AoE]: true,
						delivery: UnitAttack.deliveryType.immediate,
						aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
						magnitude: 1,
						radius: cell.radius,
						[Damage.damageType.warp]: TimeBombDamage * effect.power,
					},
					effect.source,
				);
				return {};
			},
		};
	public priority = -5;
}

export default TimeBombEffect;

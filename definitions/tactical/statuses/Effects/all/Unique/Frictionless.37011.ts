import { numericAPScaling, wellRounded } from '../../../../../../src/utils/wellRounded';
import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import DamageManager from '../../../../damage/DamageManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

type IState = Partial<{
	hasBonus: boolean;
}>;

export const FrictionlessSpeedThreshold = 0.5; //50%
export const FrictionlessDamageBonus = 0.25; //25%

export class FrictionlessEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 37011;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly caption = 'Frictionless';
	public static readonly description = `When a Unit spends ${wellRounded(
		FrictionlessSpeedThreshold * 100,
		0,
	)}% of its Movement Points, it gains ${numericAPScaling(
		FrictionlessDamageBonus,
		AbilityPowerUIToken,
		null,
		true,
	)} Damage bonus until the end of Turn `;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: () => ({ hasBonus: false }),
		[EffectEvents.TUnitEffectEvents.unitAttack]: (unit, effect: FrictionlessEffect, meta) => {
			if (effect.state.hasBonus)
				meta.attack.attacks = meta.attack.attacks.map((a) =>
					DamageManager.applyDamageModifierToDamageUnit(a, {
						factor: 1 + FrictionlessDamageBonus * unit.abilityPower,
					}),
				);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: () => ({ hasBonus: false }),
		[EffectEvents.TUnitEffectEvents.move]: (unit) => ({
			hasBonus: unit.speed && unit.currentMovementPoints / unit.speed <= FrictionlessSpeedThreshold,
		}),
	};
	public priority = ATTACK_PRIORITIES.NORMAL;
}

export default FrictionlessEffect;

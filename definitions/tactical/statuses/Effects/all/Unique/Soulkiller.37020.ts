import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import Series from '../../../../../maths/Series';
import Vectors from '../../../../../maths/Vectors';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import Units from '../../../../units/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	charges: number;
	rangeReduction: number[];
	scanRangeReduction: number;
}

const SoulkillerDamage = 50;
const SoulkillerRangePenalty = 0.25;
const SoulkillerRangePenaltyScale = 0.15;
const SoulkillerMaxCharges = 5;
const SoulkillerXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

const stackEffect = (u: Units.IUnit) => {
	delete u.targetFlags[UnitChassis.targetType.organic];
	u.targetFlags[UnitChassis.targetType.robotic] = true;
	u.addShields(-u.totalShields);
	return u;
};

export class SoulkillerEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37020;
	public static readonly negative = true;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Soulkiller';
	public static readonly description = `Unit is infected with an engram virus, that is slowly taking over onboard digital systems. Unit takes penalty of ${numericAPScaling(
		-SoulkillerRangePenaltyScale,
		AbilityPowerUIToken,
		-SoulkillerRangePenalty,
	)} Ability Range and Scan Range every Turn End, and whenever it invokes an Ability, it is disabled for Effect Duration. Using an Ability or being targeted by an Ability (including this one) grants 1 Infiltration Point, and stacking ${SoulkillerMaxCharges} Infiltration Points under Effect Duration will make the Unit become Robotic and lose all Shield Capacity. When the Effect expires, deal ${AbilityPowerUIToken}${SoulkillerDamage} TRM Damage to the Unit, if it's Robotic, otherwise restore lost Attack Range and Ability Range, while the invoker gains ${SoulkillerXP} XP per every Infiltration Point on Unit`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.targetedWithAbility]: (unit, effect: SoulkillerEffect, meta) => {
			stackEffect(unit);
			return {
				...effect.state,
				charges: (effect.state.charges ?? 1) + 1,
			};
		},
		[EffectEvents.TUnitEffectEvents.unitInvoke]: (unit, effect: SoulkillerEffect, meta) => {
			if (meta.ability) meta.ability.usable = false;
			stackEffect(unit);
			return {
				...effect.state,
				charges: (effect.state.charges ?? 1) + 1,
			};
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: SoulkillerEffect) => {
			const abilities = unit.kit?.getStableSortedAbilities() ?? [];
			const scanRangeReduction = Math.min(
				unit.scanRange,
				SoulkillerRangePenalty + effect.power * SoulkillerRangePenaltyScale,
			);
			const rangeReduction = abilities.map((a) =>
				Math.min(a.baseRange, SoulkillerRangePenalty + effect.power * SoulkillerRangePenaltyScale),
			);
			abilities.forEach((a, ix) => a.applyModifier({ abilityRangeModifier: [{ bias: -rangeReduction[ix] }] }));
			unit.applyChassisModifier({ scanRange: { bias: -scanRangeReduction } });
			return {
				...effect.state,
				rangeReduction: rangeReduction.reduce((a, v, ix) => {
					a[ix] += v;
					return a;
				}, effect.state?.rangeReduction ?? []),
				scanRangeReduction: (effect.state.scanRangeReduction ?? 0) + scanRangeReduction,
			};
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SoulkillerEffect) => {
			const state: IState = {
				charges: 1,
				rangeReduction: (unit.kit?.getStableSortedAbilities() ?? []).map(() => 0),
				scanRangeReduction: 0,
			};
			return state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SoulkillerEffect) => {
			const isRobotic = unit.targetFlags[UnitChassis.targetType.robotic];
			if (isRobotic) {
				unit.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[Damage.damageType.thermodynamic]: effect.power * SoulkillerDamage,
					},
					effect.source,
				);
			} else {
				unit.applyChassisModifier({
					scanRange: {
						bias: effect.state.scanRangeReduction,
					},
				});
			}
			(unit.kit?.getOrderedAbilities() ?? []).forEach((a, ix) => {
				a.usable = true;
				if (!isRobotic) a.baseRange += effect.state?.rangeReduction?.[ix] ?? 0;
			});
			if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(SoulkillerXP * effect.state.charges);
			return effect.state;
		},
	};
	public priority = 0;
}

export default SoulkillerEffect;

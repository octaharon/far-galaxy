import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const QuantumTunnelShieldRate = 0.5;
const QuantumTunnelShieldRateScale = 0.25;
const QuantumTunnelShieldXP = DEFAULT_PLAYER_RESEARCH_XP * 0.2;

export class QuantumTunnelEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37002;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Quantum tunnel';
	public static readonly description = `Discharge Shields of all Hostile Units in Effect Radius and transfer ${numericAPScaling(
		QuantumTunnelShieldRateScale,
		AbilityPowerUIToken,
		QuantumTunnelShieldRate,
		true,
	)} to the invoker, which also gains ${QuantumTunnelShieldXP} XP for every Shield breached`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: QuantumTunnelEffect) => {
			if (GenericStatusEffect.isUnitTarget(effect.source)) {
				if (effect.source.isFriendlyToPlayer(unit.playerId)) return {};
				const shieldsTransferred =
					unit.currentShields * (QuantumTunnelShieldRate + QuantumTunnelShieldRateScale * effect.power);
				unit.addShields(-unit.totalShields);
				effect.source
					.addXp((unit.defense?.shields?.length ?? 0) * QuantumTunnelShieldXP)
					.addShields(shieldsTransferred);
			}
			return {};
		},
	};
	public priority = 0;
}

export default QuantumTunnelEffect;

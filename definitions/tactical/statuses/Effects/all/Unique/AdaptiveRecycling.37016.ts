import { numericAPScaling, wellRounded } from '../../../../../../src/utils/wellRounded';
import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import DefenseManager from '../../../../damage/DefenseManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

type IState = Partial<{
	deployTurn: number;
}>;

export const AdaptiveRecyclingTimeout = 3; // Turns
export const AdaptiveRecyclingDuration = 5; // Turns
export const AdaptiveRecyclingModifier = 0.4; //40%
export const AdaptiveRecyclingShieldValue = 0.2; //20%

const isInEffect = (turnStart: number, currentTurn: number) =>
	currentTurn - turnStart > AdaptiveRecyclingTimeout &&
	currentTurn - turnStart <= AdaptiveRecyclingTimeout + AdaptiveRecyclingDuration;

export class AdaptiveRecyclingEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 37016;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly caption = 'Adaptive Recycling';
	public static readonly description = `${AdaptiveRecyclingTimeout} Turns after Deployment, this Unit starts regenerating ${wellRounded(
		AdaptiveRecyclingShieldValue * 100,
		0,
	)}% of Shield Capacity every Turn for ${AdaptiveRecyclingDuration} Turns. During that period, landing a kill on Hostile Unit grants additional XP equal to ${numericAPScaling(
		AdaptiveRecyclingModifier,
		AbilityPowerUIToken,
		null,
		true,
	)} of that Unit's Hit Points`;

	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect, meta) => ({
			deployTurn: meta?.turnIndex || meta?.globalTurnIndex || 0,
		}),
		[EffectEvents.TUnitEffectEvents.deployed]: (unit, effect, meta) => ({
			deployTurn: meta?.turnIndex || meta?.globalTurnIndex || 0,
		}),
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: AdaptiveRecyclingEffect, meta) => {
			if (isInEffect(effect.state.deployTurn, meta?.turnIndex || meta?.globalTurnIndex || 0))
				unit.addShields(unit.totalShields, AdaptiveRecyclingShieldValue);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.unitKill]: (unit, effect: AdaptiveRecyclingEffect, meta) => {
			if (
				!meta?.map ||
				!unit?.location ||
				!isInEffect(effect.state.deployTurn, meta?.turnIndex || meta?.globalTurnIndex || 0) ||
				unit.doesBelongToPlayer(meta.target?.playerId)
			)
				return effect.state;
			unit.addXp(meta.target.maxHitPoints * AdaptiveRecyclingModifier * effect.power);
			return effect.state;
		},
	};
	public priority = ATTACK_PRIORITIES.HIGH;
}

export default AdaptiveRecyclingEffect;

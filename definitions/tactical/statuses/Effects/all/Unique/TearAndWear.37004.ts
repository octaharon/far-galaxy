import { biasToPercent, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import DamageManager from '../../../../damage/DamageManager';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describeModifier = BasicMaths.describeModifier;

interface IState {
	wasUnstoppable: boolean;
}

const TEAR_AND_WEAR_DAMAGE_BONUS = 0.16;
const TEAR_AND_WEAR_DAMAGE_SCALE = 0.04;
const TEAR_AND_WEAR_SPEED = 2;
const TEAR_AND_WEAR_VULNERABILITY = 0.5;

export class TearAndWearEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37004;
	public static readonly negative = false;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Wear And Tear';
	public static readonly description = `Unit loses 50% of its Shield Capacity but gains a bonus of ${numericAPScaling(
		TEAR_AND_WEAR_DAMAGE_SCALE,
		AbilityPowerUIToken,
		TEAR_AND_WEAR_DAMAGE_BONUS,
		true,
	)} Damage and ${describeModifier({
		bias: TEAR_AND_WEAR_SPEED,
	})} Speed. Mechanical Units also become Unstoppable for the Effect Duration, while others take ${biasToPercent(
		TEAR_AND_WEAR_VULNERABILITY,
	)} extra Damage to Hit Points from Attacks`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: TearAndWearEffect, meta?) => {
			const isMechanical =
				!unit.targetFlags[UnitChassis.targetType.robotic] && !unit.targetFlags[UnitChassis.targetType.organic];
			const state: IState = {
				wasUnstoppable: isMechanical && unit.hasDefensePerks(UnitDefense.defenseFlags.unstoppable),
			};
			unit.addShields(-unit.currentShields / 2)
				.applyChassisModifier({
					speed: {
						bias: TEAR_AND_WEAR_SPEED,
						minGuard: ArithmeticPrecision,
						min: 0,
					},
				})
				.applyWeaponModifier({
					default: {
						damageModifier: {
							factor: 1 + TEAR_AND_WEAR_DAMAGE_BONUS + TEAR_AND_WEAR_DAMAGE_SCALE * effect.power,
						},
					},
				});
			if (isMechanical) unit.addDefensePerks(UnitDefense.defenseFlags.unstoppable);
			return state;
		},
		[EffectEvents.TUnitEffectEvents.unitHit]: (unit, effect: TearAndWearEffect, meta?) => {
			const isMechanical =
				!unit.targetFlags[UnitChassis.targetType.robotic] && !unit.targetFlags[UnitChassis.targetType.organic];
			if (!isMechanical && DamageManager.getTotalDamageValue(meta.result.throughDamage) > 0)
				meta.result.throughDamage = DamageManager.applyDamageModifierToDamageUnit(meta.result.throughDamage, {
					factor: 1 + TEAR_AND_WEAR_VULNERABILITY,
				});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: TearAndWearEffect, meta?) => {
			unit.addDefensePerks(UnitDefense.defenseFlags.unstoppable)
				.applyChassisModifier({
					speed: {
						bias: -TEAR_AND_WEAR_SPEED,
						minGuard: TEAR_AND_WEAR_SPEED,
						min: TEAR_AND_WEAR_SPEED,
					},
				})
				.applyWeaponModifier({
					default: {
						damageModifier: {
							factor: 1 / (1 + TEAR_AND_WEAR_DAMAGE_BONUS + TEAR_AND_WEAR_DAMAGE_SCALE * effect.power),
						},
					},
				});
			if (!effect.state.wasUnstoppable) return effect.state;
			unit.removeDefensePerks(UnitDefense.defenseFlags.unstoppable);
			return effect.state;
		},
	};
	public priority = 0;
}

export default TearAndWearEffect;

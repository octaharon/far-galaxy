import { biasToPercent, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	unitsHit: Record<string, boolean>;
}

const ELECTROCUTION_DAMAGE = 25;
const ELECTROCUTION_RESISTANCE = 0.15;
const ELECTROCUTION_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.2;
const ELECTROCUTION_RADIUS = 3;

export class ElectrocuteEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37000;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Electrocution';
	public static readonly description = `The Unit is imbued with overwhelming electrical field that adds ${biasToPercent(
		ELECTROCUTION_RESISTANCE,
	)} to Shield Protection, and also deals ${numericAPScaling(
		ELECTROCUTION_DAMAGE,
		AbilityPowerUIToken,
	)} EMG Damage as Surface Attack every Action Phase to all other Units in Effect Radius ${ELECTROCUTION_RADIUS}. Every Hostile Unit Damaged grants the invoker of the Ability ${ELECTROCUTION_XP} XP once`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: ElectrocuteEffect, meta?) => {
			unit.applyShieldsModifier({
				default: {
					factor: 1 / (1 - ELECTROCUTION_RESISTANCE),
				},
			});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: ElectrocuteEffect, meta?) => {
			unit.applyShieldsModifier({
				default: { factor: 1 - ELECTROCUTION_RESISTANCE },
			});
			return { unitsHit: {} };
		},
		[EffectEvents.TUnitEffectEvents.actionPhase]: (unit, effect: ElectrocuteEffect, meta?) => {
			if (meta.map && unit.location) {
				const aoeAttack = {
					aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
					magnitude: 1,
					radius: ELECTROCUTION_RADIUS,
					[UnitAttack.attackFlags.AoE]: true as const,
					delivery: UnitAttack.deliveryType.surface,
				};
				const src = effect.static.isUnitTarget(effect.source) ? effect.source : null;
				const units = meta.map
					.getUnitsInAoEZone(
						aoeAttack,
						src?.location ?? unit.location,
						unit.location,
						UnitAttack.deliveryType.surface,
					)
					.filter((u) => u.getInstanceId() !== src?.getInstanceId());
				units.forEach((u) =>
					u.applyAttackUnit(
						{
							...aoeAttack,
							[Damage.damageType.magnetic]: ELECTROCUTION_DAMAGE * effect.power,
						},
						effect.source,
					),
				);
				const newUnitsHit = units
					.filter((u) => !u.isFriendlyToPlayer(src?.playerId) && !effect.state?.unitsHit?.[u.getInstanceId()])
					.map((u) => u.getInstanceId());
				if (src && newUnitsHit.length) src.addXp(ELECTROCUTION_XP * newUnitsHit.length);
				return {
					...(effect.state || {}),
					unitsHit: {
						...(effect.state?.unitsHit || {}),
						...newUnitsHit.reduce((a, id) => ({ ...a, [id]: true }), {}),
					},
				};
			}
		},
	};
	public priority = 0;
}

export default ElectrocuteEffect;

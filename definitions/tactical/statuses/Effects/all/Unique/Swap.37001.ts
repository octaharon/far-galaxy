import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class SwapEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37001;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Swap';
	public static readonly description = 'Swap positions with unit';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SwapEffect, meta?) => {
			if (meta.map && unit.location && GenericStatusEffect.isUnitTarget(effect.source)) {
				const oldLoc = unit.location;
				unit.location = effect.source.location;
				effect.source.location = oldLoc;
			}
			return {};
		},
	};
	public priority = 0;
}

export default SwapEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import { getMaxTotalDefenseShields, getTotalDefenseShieldsLeft } from '../../../../damage/damageModel';
import Units from '../../../../units/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

type IState = Partial<{
	hasBonus: boolean;
	shieldIndex: number;
}>;

const TRAILBLAZER_SPEED_LIMIT = 0.8; //80%
const TRAILBLAZER_SPEED_SCALE = 0.1; //10%
const TRAILBLAZER_SHIELD_MULTIPLIER = 3; //300%
const TRAILBLAZER_SHIELD_SCALE = 1; //100%
const TRAILBLAZER_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5; //100%

const getEffectDefenseId = (unit: Units.IUnit, id: number | string, turnIndex: number) => {
	return `eff-${id}-${unit?.getInstanceId()}-${turnIndex}`;
};

export class TrailblazerStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 37012;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly caption = 'Trailblazer';
	public static readonly description = `If the Unit spends ${numericAPScaling(
		-TRAILBLAZER_SPEED_SCALE,
		AbilityPowerUIToken,
		TRAILBLAZER_SPEED_LIMIT,
		true,
	)} of its Movement Points, they gain a Disposable Shield with Capacity equal to ${numericAPScaling(
		TRAILBLAZER_SHIELD_SCALE,
		AbilityPowerUIToken,
		TRAILBLAZER_SHIELD_MULTIPLIER,
		true,
	)} of Unit's Speed. If that Shield is breached, the Unit is rewarded with bonus XP equal to its maximum Capacity. That additional Shield is removed next Turn`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect, meta) => ({
			hasBonus: false,
			shieldIndex: -1,
		}),
		[EffectEvents.TUnitEffectEvents.turnStart]: (unit, effect: TrailblazerStatusEffect, meta) => {
			const addedDefense = unit.defense.shields[effect.state.shieldIndex];
			if (addedDefense && effect.state.hasBonus) {
				if (addedDefense.currentAmount <= ArithmeticPrecision) unit.addXp(TRAILBLAZER_XP);
				if (addedDefense)
					unit.defense.shields = unit.defense.shields.filter((d, ix) => ix !== effect.state.shieldIndex);
			}
			return {
				hasBonus: false,
				shieldIndex: -1,
			};
		},
		[EffectEvents.TUnitEffectEvents.move]: (unit, effect: TrailblazerStatusEffect) => {
			const result: IState = {
				...effect.state,
				hasBonus:
					unit.speed &&
					unit.currentMovementPoints / unit.speed >=
						TRAILBLAZER_SPEED_LIMIT - TRAILBLAZER_SPEED_SCALE * effect.power,
			};
			if (result.hasBonus && !effect.state.hasBonus) {
				const totalShield =
					unit.speed * (TRAILBLAZER_SHIELD_MULTIPLIER + TRAILBLAZER_SHIELD_SCALE * effect.power);
				unit.defense.shields.push({
					shieldAmount: totalShield,
					currentAmount: totalShield,
				});
				return {
					shieldIndex: unit.defense.shields.length - 1,
					hasBonus: true,
				};
			}
			return result;
		},
	};
	public priority = ATTACK_PRIORITIES.HIGHEST;
}

export default TrailblazerStatusEffect;

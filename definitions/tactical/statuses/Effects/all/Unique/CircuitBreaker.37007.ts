import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	previousSpeed: number;
}

const CIRCUIT_BREAKER_DMG = 100;
const CIRCUIT_BREAKER_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class CircuitBreakerEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37007;
	public static readonly negative = true;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Circuit Breaker';
	public static readonly description = `Robotic Units are immobilized and are stripped of any Shields. Other units take ${AbilityPowerUIToken}${CIRCUIT_BREAKER_DMG} EMG Damage. Every Shield breached grants the invoker ${CIRCUIT_BREAKER_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: CircuitBreakerEffect) => {
			const state: IState = {
				previousSpeed: unit.isRobotic ? unit.speed : null,
			};
			if (!unit.isRobotic) {
				unit.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[Damage.damageType.magnetic]: CIRCUIT_BREAKER_DMG * effect.power,
					},
					effect.source,
				);
				effect.duration = 0;
			} else {
				unit.speed = 0;
				unit.addShields(-unit.totalShields);
			}
			if (effect.static.isUnitTarget(effect.source)) {
				const breachedShields = (unit.defense.shields || []).filter((s) => s.shieldAmount <= 0);
				if (breachedShields.length) effect.source.addXp(CIRCUIT_BREAKER_XP * breachedShields.length);
			}
			return state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: CircuitBreakerEffect) => {
			if (effect.state.previousSpeed) unit.speed = effect.state.previousSpeed;
			return effect.state;
		},
	};
	public priority = 0;
}

export default CircuitBreakerEffect;

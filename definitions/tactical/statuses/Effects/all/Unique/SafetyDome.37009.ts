import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import Damage from '../../../../damage/damage';
import UnitDefense from '../../../../damage/defense';
import { IMapCell } from '../../../../map';
import Units from '../../../../units/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import _ from 'underscore';

type IState = Record<Units.TUnitId, UnitDefense.IDefenseInterface>;

const SafetyDomeThreshold = 15;
const SafetyDomeThresholdScale = 5;
const SafetyDomeThresholdShield = 40;

export class SafetyDomeEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 37009;
	public static readonly negative = false;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Safety Dome';
	public static readonly description = `Units in area have ${numericAPScaling(
		SafetyDomeThresholdScale,
		AbilityPowerUIToken,
		SafetyDomeThreshold,
	)} Threshold on all Shields and Armor and restore ${AbilityPowerUIToken}${SafetyDomeThresholdShield} Shield Capacity, if they finish a turn inside the zone`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.turnEnd]: (cell: IMapCell, effect: SafetyDomeEffect, meta?) => {
				meta.map.getUnitsInRange(cell).forEach((u) => u.addShields(SafetyDomeThresholdShield * effect.power));
				return effect.state;
			},
			[EffectEvents.TMapEffectEvents.unitEntersZone]: (cell: IMapCell, effect: SafetyDomeEffect, meta?) => {
				const state: IState = {
					...effect.state,
					[meta.unit.getInstanceId()]: meta.unit.defense,
				};
				const threshold = SafetyDomeThreshold + SafetyDomeThresholdScale * effect.power;
				meta.unit.applyShieldsModifier({
					default: {
						threshold,
					},
				});
				meta.unit.applyArmorModifier({
					default: {
						threshold,
					},
				});
				return state;
			},
			[EffectEvents.TMapEffectEvents.unitLeavesZone]: (cell: IMapCell, effect: SafetyDomeEffect, meta?) => {
				const state = effect.state;
				const defense = state?.[meta?.unit?.getInstanceId()];
				if (!defense) return state;
				const d = meta.unit.defense;
				d.shields.forEach((s, six) =>
					Damage.TDamageTypesArray.forEach((dt: Damage.damageType) => {
						if (s[dt]) s[dt].threshold = defense.shields[six][dt].threshold || 0;
					}),
				);
				Damage.TDamageTypesArray.forEach((dt: Damage.damageType) => {
					if (d.armor[dt]) d.armor[dt].threshold = defense.armor[dt].threshold || 0;
				});
				return _.omit(state, meta.unit.getInstanceId());
			},
		};
	public priority = 0;
}

export default SafetyDomeEffect;

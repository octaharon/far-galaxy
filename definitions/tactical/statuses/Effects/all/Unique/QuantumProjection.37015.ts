import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

type IState = Partial<{
	hasBonus: boolean;
}>;

export const QuantumProjectionXpBonus = 5;
export const QuantumProjectionDodgeBonus = 0.5; // 50%

export class QuantumProjectionEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 37015;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly caption = 'Quantum Projection';
	public static readonly description = `Dodging an Attack grants this Unit additional ${QuantumProjectionXpBonus} XP. Being a target of a hostile Ability provides additional ${numericAPScaling(
		QuantumProjectionDodgeBonus,
		AbilityPowerUIToken,
		null,
		true,
	)} Dodge until Turn End`;

	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: () => ({ hasBonus: false }),
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: QuantumProjectionEffect) => {
			if (effect.state.hasBonus)
				unit.applyChassisModifier({
					dodge: {
						default: {
							bias: -QuantumProjectionDodgeBonus * unit.abilityPower,
						},
					},
				});
			return { hasBonus: false };
		},
		[EffectEvents.TUnitEffectEvents.unitAttacked]: (unit, effect: QuantumProjectionEffect, meta) => {
			if (!meta.result.hit) unit.addXp(QuantumProjectionXpBonus);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.targetedWithAbility]: (unit, effect: QuantumProjectionEffect, meta) => {
			if (unit.doesBelongToPlayer(meta?.source?.playerId)) return effect.state;
			if (!effect.state.hasBonus)
				unit.applyChassisModifier({
					dodge: {
						default: {
							bias: QuantumProjectionDodgeBonus * unit.abilityPower,
						},
					},
				});
			return {
				hasBonus: true,
			};
		},
	};
	public priority = ATTACK_PRIORITIES.HIGH;
}

export default QuantumProjectionEffect;

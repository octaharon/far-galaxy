import { biasToPercent, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	speedReduction: number;
	bonusMultiplier: number;
}

const TacticalNodeRadius = 5;
const TacticalNodeSpeedReductionScale = 0.15;
const TacticalNodeSpeedReduction = 3;
const TacticalNodeBonusDamage = 0.1;
const TacticalNodeBonusDodge = 0.15;

export class TacticalNodeEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37008;
	public static readonly negative = false;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Tactical Node';
	public static readonly description = `This Unit loses ${TacticalNodeSpeedReduction} Speed (reduced by ${numericAPScaling(
		-TacticalNodeSpeedReductionScale,
		AbilityPowerUIToken,
		null,
		true,
	)}), but gains ${biasToPercent(TacticalNodeBonusDamage)} Damage and ${biasToPercent(
		TacticalNodeBonusDodge,
	)} Dodge per each Robotic Allied Unit within ${AbilityPowerUIToken}${TacticalNodeRadius} Radius`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: TacticalNodeEffect, meta?) => {
			const state: IState = {
				speedReduction: 0,
				bonusMultiplier: 0,
			};
			if (!unit.targetFlags[UnitChassis.targetType.robotic]) {
				effect.duration = -1;
				return state;
			}
			state.speedReduction = Math.min(
				BasicMaths.applyModifier(TacticalNodeSpeedReduction, {
					factor: 1 - TacticalNodeSpeedReductionScale * effect.power,
					min: 0,
				}),
				unit.speed,
			);
			unit.applyChassisModifier({
				speed: {
					bias: -state.speedReduction,
					min: 0,
					minGuard: ArithmeticPrecision,
				},
			});
			if (meta.map) {
				const relevantUnits = meta.map
					.getUnitsInRange({
						...unit.location,
						radius: TacticalNodeRadius * effect.power,
					})
					.filter((u) => u.targetFlags[UnitChassis.targetType.robotic] && u.playerId === unit.playerId);
				state.bonusMultiplier = relevantUnits.length;
			}
			if (state.bonusMultiplier) {
				unit.applyChassisModifier({
					dodge: {
						default: {
							bias: TacticalNodeBonusDodge * state.bonusMultiplier,
						},
					},
				});
				unit.applyWeaponModifier({
					default: {
						damageModifier: {
							factor: 1 + TacticalNodeBonusDamage * state.bonusMultiplier,
						},
					},
				});
			}
			return state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: TacticalNodeEffect, meta?) => {
			if (effect.state.bonusMultiplier) {
				unit.applyChassisModifier({
					dodge: {
						default: {
							bias: -TacticalNodeBonusDodge * effect.state.bonusMultiplier,
						},
					},
				});
				unit.applyWeaponModifier({
					default: {
						damageModifier: {
							factor: 1 / (1 + TacticalNodeBonusDamage * effect.state.bonusMultiplier),
						},
					},
				});
			}
			if (!effect.state.speedReduction) return effect.state;
			unit.speed = BasicMaths.applyModifier(unit.speed, {
				bias: effect.state.speedReduction,
				min: 0,
				minGuard: ArithmeticPrecision,
			});
			return {
				speedReduction: 0,
				bonusMultiplier: 0,
			};
		},
	};
	public priority = 0;
}

export default TacticalNodeEffect;

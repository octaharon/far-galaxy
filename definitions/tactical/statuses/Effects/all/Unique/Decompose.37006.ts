import { describePercentage, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	hp: number;
}

const DECOMPOSE_DMG = 0.75;
const DECOMPOSE_SCALE = 0.5;
const DECOMPOSE_TRANSFER_RATE = 0.4;
const DECOMPOSE_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class DecomposeEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 37006;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Decompose';
	public static readonly description = `Deals ${numericAPScaling(
		DECOMPOSE_SCALE,
		AbilityPowerUIToken,
		DECOMPOSE_DMG,
		true,
	)} of Unit's Max Hit Points as BIO Damage. If the target dies, transfer ${describePercentage(
		DECOMPOSE_TRANSFER_RATE,
	)} of Hit Points Damage done to the invoker, otherwise the latter gains ${DECOMPOSE_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: DecomposeEffect) => {
			if (effect.static.isUnitTarget(effect.source)) {
				if (effect.state?.hp && unit.currentHitPoints <= 0)
					effect.source.addHp(effect.state.hp * DECOMPOSE_TRANSFER_RATE);
				else effect.source.addXp(DECOMPOSE_XP);
			}
			return { hp: 0 };
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DecomposeEffect) => {
			const hp = unit.currentHitPoints;
			unit.applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.biological]:
						unit.maxHitPoints * (DECOMPOSE_SCALE * effect.power + DECOMPOSE_DMG),
				},
				effect.source,
			);
			return { hp };
		},
	};
	public priority = 0;
}

export default DecomposeEffect;

import { numericAPScaling, wellRounded } from '../../../../../../src/utils/wellRounded';
import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import Damage from '../../../../damage/damage';
import DamageManager from '../../../../damage/DamageManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

type IState = Partial<{}>;

export const GammaFresnelDamageMultiplier = 0.1; //10%
export const GammaFresnelRegenMultiplier = 0.15; //15%

export class GammaFresnelEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 37014;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly caption = 'Gamma Fresnel';
	public static readonly description = `If this Unit has charged Shields, ${wellRounded(
		GammaFresnelRegenMultiplier * 100,
		0,
	)}% of RAD Damage they take is restored as Hit Points. This Unit's Attacks have their RAD Damage increased by ${numericAPScaling(
		GammaFresnelDamageMultiplier,
		AbilityPowerUIToken,
		null,
		true,
	)} of Target's Hit Points`;

	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.unitAttacked]: (unit, effect: GammaFresnelEffect, meta) => {
			const radDamage = (meta?.attack?.attacks || []).reduce(
				(sum, att) => (att[Damage.damageType.radiation] ?? 0) + sum,
				0,
			);
			if (unit.currentShields) {
				unit.addHp(radDamage * GammaFresnelRegenMultiplier, effect.source);
			}
			return {};
		},
		[EffectEvents.TUnitEffectEvents.unitAttack]: (unit, effect: GammaFresnelEffect, meta) => {
			if (meta?.target?.maxHitPoints)
				meta.attack = Object.assign(meta.attack, {
					attacks: (meta.attack?.attacks || []).map((a) =>
						DamageManager.applyTypedDamageModifierToDamageUnit(a, Damage.damageType.radiation, {
							bias: GammaFresnelDamageMultiplier * meta.target.currentHitPoints * effect.power,
						}),
					),
				});
			return {};
		},
	};
	public priority = ATTACK_PRIORITIES.HIGH;
}

export default GammaFresnelEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ATTACK_PRIORITIES, COMBAT_TICKS_PER_TURN } from '../../../../damage/constants';
import DefenseManager from '../../../../damage/DefenseManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

type IState = Partial<{
	effectsTick: number;
}>;

export const FortitudeSpeedMultiplier = 0.4; //40%
export const FortitudeXPMultiplier = 5;

const FortitudeDodgePerTick = FortitudeSpeedMultiplier / COMBAT_TICKS_PER_TURN;

export class FortitudeStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 37013;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly caption = 'Fortitude';
	public static readonly description = `A Unit gets additional Dodge per Movement spent, up to ${numericAPScaling(
		FortitudeSpeedMultiplier,
		AbilityPowerUIToken,
		null,
		true,
	)}, which lasts until the Turn End. Whenever the Unit is targeted by an Ability, they receive ${AbilityPowerUIToken}${FortitudeXPMultiplier} bonus XP`;

	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: () => ({
			effectsTick: 0,
		}),
		[EffectEvents.TUnitEffectEvents.targetedWithAbility]: (unit, effect: FortitudeStatusEffect) => {
			unit.addXp(FortitudeXPMultiplier * effect.power);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: FortitudeStatusEffect) => {
			if (effect.state.effectsTick)
				unit.applyChassisModifier({
					dodge: {
						default: { bias: -FortitudeDodgePerTick * effect.state.effectsTick * effect.power },
					},
				});
			return {
				effectsTick: 0,
			};
		},
		[EffectEvents.TUnitEffectEvents.move]: (unit, effect: FortitudeStatusEffect) => {
			unit.applyChassisModifier({
				dodge: {
					default: { bias: FortitudeDodgePerTick * effect.power },
				},
			});
			return {
				effectsTick: 1 + (effect.state.effectsTick || 0),
			};
		},
	};
	public priority = ATTACK_PRIORITIES.HIGHEST;
}

export default FortitudeStatusEffect;

import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

export class AboveTheFightEffect extends ConstantUnitStatusEffect<null> {
	public static readonly id = 29105;
	public static readonly negative = false;
	public static readonly unremovable = false;
	public static readonly caption = 'Above The Fight';
	public static readonly description = `Immune to own Area Attacks`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.unitAttacked]: (unit, effect: AboveTheFightEffect, meta) => {
				if (
					GenericStatusEffect.isUnitTarget(meta.source) &&
					meta.source.getInstanceId() === unit.getInstanceId()
				) {
					meta.result.hit = false;
					meta.result.effectsApplied = [];
					meta.result.breach = false;
					meta.result.removed = false;
					meta.result.throughDamage = {};
					meta.result.shieldReduction = [];
				}
				return null;
			},
		};
	public priority = ATTACK_PRIORITIES.HIGHEST;
}

export default AboveTheFightEffect;

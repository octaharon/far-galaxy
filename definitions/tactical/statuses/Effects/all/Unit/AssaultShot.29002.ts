import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import AttackManager from '../../../../damage/AttackManager';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const AssaultShotDamage = 0.03;
const AssaultShotDamageScale = 0.02;
const AssaultShotXP = DEFAULT_PLAYER_RESEARCH_XP * 0.2;

export class AssaultShotEffect extends GenericUnitStatusEffect<null> {
	public static readonly id = 29002;
	public static readonly baseDuration = 2;
	public static readonly negative = true;
	public static readonly caption = 'Assault Shot';
	public static readonly description = `Deals full Weapons Damage to Unit and marks it, so that it takes extra ${numericAPScaling(
		AssaultShotDamageScale,
		AbilityPowerUIToken,
		AssaultShotDamage,
		true,
	)} of Max Hit Points as EMG Damage with every Attack that hits it and grants the invoker ${AssaultShotXP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.apply]: (unit, effect) => {
				const src = effect.source;
				if (GenericStatusEffect.isUnitTarget(src))
					(src.attacks ?? [])
						.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
						.forEach((a) => unit.applyAttackUnit(a, src));
				return null;
			},
			[EffectEvents.TUnitEffectEvents.unitAttacked]: (unit, effect: AssaultShotEffect, meta) => {
				const percentage = BasicMaths.applyModifier(unit.maxHitPoints, {
					factor: AssaultShotDamage + AssaultShotDamageScale * effect.power,
				});
				if (meta.result.hit && meta.source !== unit) {
					if (effect.static.isUnitTarget(effect.source)) {
						effect.source.addXp(AssaultShotXP);
					}
					unit.applyAttackUnit(
						{
							delivery: UnitAttack.deliveryType.immediate,
							[Damage.damageType.magnetic]: percentage,
						},
						unit,
					);
				}
				return null;
			},
		};
	public priority = 0;
}

export default AssaultShotEffect;

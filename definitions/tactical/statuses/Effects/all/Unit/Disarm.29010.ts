import { biasToPercent, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import AttackManager from '../../../../damage/AttackManager';
import Damage from '../../../../damage/damage';
import { doesActionRequireSeveralWeapons, doesActionRequireWeapon } from '../../../../units/actions/ActionManager';
import Units from '../../../../units/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describeModifier = BasicMaths.describeModifier;
import IUnit = Units.IUnit;

const DisarmDamagePenalty = 0.5;
const DisarmXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;
const DisarmDamageDealt = 25;
const DisarmDamageDealtScale = 15;
type IState = { weapons: IUnit['attacks'] };

export class DisarmEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29010;
	public static readonly baseDuration = 1;
	public static readonly negative = true;
	public static readonly caption = 'Disarm';
	public static readonly description = `Shoot with all Weapons at a Unit, dealing full Damage to it and reducing target's Weapon Damage by ${describeModifier(
		{
			factor: DisarmDamagePenalty,
		},
	)} for the Effect Duration. If target performs Actions with a Weapon during that time, it takes ${numericAPScaling(
		DisarmDamageDealtScale,
		AbilityPowerUIToken,
		DisarmDamageDealt,
	)} EMG Damage and becomes unable to Attack for the remaining Duration, which is extended by 1 Turn once, while the invoker gains ${DisarmXP} XP for every Weapon disabled`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.actionPhase]: (unit, effect: DisarmEffect, meta) => {
			if (doesActionRequireWeapon(meta.phase.action)) {
				unit.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[Damage.damageType.magnetic]: DisarmDamageDealt + effect.power * DisarmDamageDealtScale,
					},
					effect.source,
				);
				const state = { weapons: (unit.attacks ?? []).slice() };
				if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(DisarmXP * state.weapons.length);
				unit.attacks = [];
				return state;
			}
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DisarmEffect) => {
			const src = effect.source;
			if (GenericStatusEffect.isUnitTarget(src))
				(src.attacks ?? [])
					.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
					.forEach((a) => unit.applyAttackUnit(a, src));
			unit.applyWeaponModifier({ default: { damageModifier: { factor: DisarmDamagePenalty } } });
			return { weapons: [] };
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: DisarmEffect) => {
			if (effect.state.weapons.length) {
				unit.attacks = effect.state.weapons;
			}
			return { weapons: [] };
		},
	};
	public priority = 0;
}

export default DisarmEffect;

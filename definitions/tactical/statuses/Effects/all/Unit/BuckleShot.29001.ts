import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import AttackManager from '../../../../damage/AttackManager';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const BUCKLE_SHOT_DAMAGE = 0.75;
const BUCKLE_SHOT_DAMAGE_SCALE = 0.25;
const BUCKLE_SHOT_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class BuckleShotEffect extends GenericUnitStatusEffect<null> {
	public static readonly id = 29001;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Buckle Shot';
	public static readonly description = `Deal full Weapons Damage to Unit, increased by ${numericAPScaling(
		BUCKLE_SHOT_DAMAGE_SCALE,
		AbilityPowerUIToken,
		BUCKLE_SHOT_DAMAGE,
		true,
	)} Damage. Remove Replenishing Defense Perk from target, and when doing so gain additional ${BUCKLE_SHOT_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: BuckleShotEffect) => {
				const src = effect.source;
				if (GenericStatusEffect.isUnitTarget(src))
					(src.attacks ?? [])
						.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
						.forEach((a) =>
							unit.applyAttackUnit(a, src, {
								factor: 1 + BUCKLE_SHOT_DAMAGE + BUCKLE_SHOT_DAMAGE_SCALE * effect.power,
							}),
						);
				if (unit.hasDefensePerks(UnitDefense.defenseFlags.replenishing)) {
					unit.removeDefensePerks(UnitDefense.defenseFlags.replenishing);
					unit.addXp(BUCKLE_SHOT_XP);
				}
				return null;
			},
		};
	public priority = 0;
}

export default BuckleShotEffect;

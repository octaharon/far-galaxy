import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const FieldRepairsHPScale = 0.1;
const FieldRepairsHPBase = 0.15;

type IState = {
	wasDisabled: boolean;
};

export class FieldRepairsEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29103;
	public static readonly baseDuration = 2;
	public static readonly negative = false;
	public static readonly caption = 'Field Repairs';
	public static readonly description = `For the Effect Duration the Unit is Disabled and restores ${numericAPScaling(
		FieldRepairsHPScale,
		AbilityPowerUIToken,
		FieldRepairsHPBase,
		true,
	)} of Max Hit Points every Turn End`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: FieldRepairsEffect) => {
			const state = {
				wasDisabled: unit.isDisabled,
			};
			unit.status.disabled = true;
			return state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: FieldRepairsEffect) => {
			if (effect.state.wasDisabled && !unit.isDisabled) {
				unit.status.disabled = true;
			}
			unit.addHp(unit.maxHitPoints * (FieldRepairsHPScale * effect.power + FieldRepairsHPBase), effect.source);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: FieldRepairsEffect) => {
			unit.status.disabled = false;
			return { wasDisabled: false };
		},
	};
	public priority = 0;
}

export default FieldRepairsEffect;

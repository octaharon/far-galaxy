import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import DamageManager from '../../../../damage/DamageManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const SiegeModeRangeScale = 1;
const SiegeModeRangeBase = 0.5;
const SiegeModeDodgePenalty = 0.7;
const SiegeModeProtectionFactor = 0.85;

type IState = {
	rangeValue: number;
	damageTaken: boolean;
};

export class SiegeModeEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29102;
	public static readonly baseDuration = 1;
	public static readonly negative = false;
	public static readonly caption = 'Siege Mode';
	public static readonly description = `Gain ${numericAPScaling(
		SiegeModeRangeScale,
		AbilityPowerUIToken,
		SiegeModeRangeBase,
	)} Attack Range, but take ${BasicMaths.describePercentileModifier(
		{ bias: -SiegeModeDodgePenalty },
		true,
	)} penalty to Dodge. Taking damage extends the Effect Duration and grants ${BasicMaths.describePercentileModifier({
		bias: 1 - SiegeModeProtectionFactor,
	})} Armor Protection until Turn End`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SiegeModeEffect) => {
			const state: IState = {
				rangeValue: SiegeModeRangeBase + effect.power * SiegeModeRangeScale,
				damageTaken: false,
			};
			unit.applyChassisModifier({
				dodge: {
					default: {
						bias: -SiegeModeDodgePenalty,
					},
				},
			}).applyWeaponModifier({
				default: {
					baseRange: {
						bias: state.rangeValue,
					},
				},
			});
			return state;
		},
		[EffectEvents.TUnitEffectEvents.unitDamaged]: (unit, effect: SiegeModeEffect, meta) => {
			if (!effect.state.damageTaken && DamageManager.getTotalDamageValue(meta?.damage) > ArithmeticPrecision) {
				effect.duration += 1;
				unit.applyArmorModifier({
					default: {
						factor: SiegeModeProtectionFactor,
					},
				});
				return {
					...effect.state,
					damageTaken: true,
				};
			}
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: SiegeModeEffect) => {
			if (effect.state.damageTaken) {
				unit.applyArmorModifier({
					default: {
						factor: 1 / SiegeModeProtectionFactor,
					},
				});
			}
			return {
				...effect.state,
				damageTaken: false,
			};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SiegeModeEffect) => {
			unit.applyChassisModifier({
				dodge: { default: { bias: SiegeModeDodgePenalty } },
			}).applyWeaponModifier({
				default: {
					baseRange: {
						bias: -effect.state.rangeValue,
						min: 0,
						minGuard: ArithmeticPrecision,
					},
				},
			});
			return effect.state;
		},
	};
	public priority = 0;
}

export default SiegeModeEffect;

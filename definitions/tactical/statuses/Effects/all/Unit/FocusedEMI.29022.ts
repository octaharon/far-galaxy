import _ from 'underscore';
import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import DIContainer from '../../../../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../core/DI/injections';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import UnitDefense from '../../../../damage/defense';
import DefenseManager from '../../../../damage/DefenseManager';
import { TSerializedAura } from '../../../Auras/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import TDodgeModifier = UnitDefense.TDodgeModifier;

const FocusedEMIDodgePenalty = 0.35;
const FocusedEMIDodgePenaltyScale = 0.15;
const FocusedEMIXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

type TState = {
	removedAuras: TSerializedAura[];
	removedDodge: TDodgeModifier;
};

export class FocusedEMIEffect extends GenericUnitStatusEffect<TState> {
	public static readonly id = 29022;
	public static readonly baseDuration = 2;
	public static readonly negative = true;
	public static readonly caption = 'Combined EMI';
	public static readonly description = `Temporarily disable all Unit's Auras and nullify Dodge against Drone and Missile Attacks. Permanently remove Shields Overcharge and HULL Module, if any, and, when doing so, additionally drain ${numericAPScaling(
		FocusedEMIDodgePenaltyScale,
		AbilityPowerUIToken,
		FocusedEMIDodgePenalty,
		true,
	)} of total Shield Capacity and gain ${FocusedEMIXP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		TState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: FocusedEMIEffect) => {
			const src = effect.source;
			const state: TState = {
				removedAuras: unit.auras.map((a) => a.serialize()),
				removedDodge: _.mapObject(
					_.pick(unit.defense.dodge, UnitAttack.deliveryType.drone, UnitAttack.deliveryType.missile),
					(v) => ({ bias: v }),
				),
			};
			const removed =
				unit.hasDefensePerks(UnitDefense.defenseFlags.hull) ||
				unit.defense?.shields?.some((t) => t.currentAmount > t.shieldAmount);
			unit.defense = Object.assign(_.omit(unit.defense, UnitDefense.defenseFlags.hull), {
				dodge: _.omit(unit.defense.dodge, UnitAttack.deliveryType.drone, UnitAttack.deliveryType.missile),
				shields: unit.defense.shields.map((s) =>
					Object.assign(s, {
						currentAmount: s.currentAmount > s.shieldAmount ? s.shieldAmount : s.currentAmount,
					}),
				),
			});
			if (removed) {
				const percentage =
					(FocusedEMIDodgePenalty + effect.power * FocusedEMIDodgePenaltyScale) * unit.totalShields;
				unit.addShields(-percentage);
				if (effect.static.isUnitTarget(src)) src.addXp(FocusedEMIXP);
			}
			return state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: FocusedEMIEffect) => {
			if (effect.state.removedAuras.length)
				unit.auras = (unit.auras ?? []).concat(
					...effect.state.removedAuras.map((a) =>
						DIContainer.getProvider(DIInjectableCollectibles.aura).unserialize(a),
					),
				);
			if (Object.keys(effect.state.removedDodge).length)
				unit.applyChassisModifier({
					dodge: effect.state.removedDodge,
				});
			return {
				removedAuras: [],
				removedDodge: {},
			};
		},
	};
	public priority = 0;
}

export default FocusedEMIEffect;

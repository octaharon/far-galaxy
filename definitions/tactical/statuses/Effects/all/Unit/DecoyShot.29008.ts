import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const DecoyShotBonus = 0.15;
const DecoyShotBonusScale = 0.45;
const DecoyShotBonusXp = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class DecoyShotEffect extends GenericUnitStatusEffect<{ dodged: boolean }> {
	public static readonly id = 29008;
	public static readonly baseDuration = 2;
	public static readonly negative = false;
	public static readonly caption = 'Decoy Shot';
	public static readonly description = `Grants a Unit ${numericAPScaling(
		DecoyShotBonusScale,
		AbilityPowerUIToken,
		DecoyShotBonus,
		true,
	)} Dodge versus Missile, Bombardment and Drone Attacks and +1 Effect Resistance. Successfully mitigating any Attacks during Effect Duration grants the invoker ${DecoyShotBonusXp} XP once per Turn.`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		{
			dodged: boolean;
		}
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DecoyShotEffect) => {
			const bonus = { bias: DecoyShotBonus + DecoyShotBonusScale * effect.power };
			unit.applyChassisModifier({
				dodge: {
					[UnitAttack.deliveryType.missile]: bonus,
					[UnitAttack.deliveryType.drone]: bonus,
					[UnitAttack.deliveryType.bombardment]: bonus,
				},
			}).applyEffectModifier({ statusDurationModifier: [{ bias: -1, minGuard: 1, min: 1 }] });
			return { dodged: false };
		},
		[EffectEvents.TUnitEffectEvents.unitAttacked]: (unit, effect: DecoyShotEffect, meta) => {
			if (
				meta?.attack?.attacks.some((v) =>
					[
						UnitAttack.deliveryType.missile,
						UnitAttack.deliveryType.drone,
						UnitAttack.deliveryType.bombardment,
					].includes(v.delivery),
				) &&
				!meta?.result?.hit
			)
				return { dodged: true };
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: DecoyShotEffect) => {
			if (effect.state.dodged && effect.static.isUnitTarget(effect.source)) effect.source.addXp(DecoyShotBonusXp);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: DecoyShotEffect) => {
			const bonus = { bias: -DecoyShotBonus + DecoyShotBonusScale * effect.power };
			unit.applyChassisModifier({
				dodge: {
					[UnitAttack.deliveryType.missile]: bonus,
					[UnitAttack.deliveryType.drone]: bonus,
					[UnitAttack.deliveryType.bombardment]: bonus,
				},
			}).applyEffectModifier({ statusDurationModifier: [{ bias: 1 }] });
			return {
				dodged: false,
			};
		},
	};
	public priority = 0;
}

export default DecoyShotEffect;

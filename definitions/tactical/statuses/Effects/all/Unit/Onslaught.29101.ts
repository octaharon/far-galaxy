import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import AttackManager from '../../../../damage/AttackManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const OnslaughtSpeedScale = 1.5;
const OnslaughtSpeedBase = 0.5;
const OnslaughtAttackRateBase = 0.07;
const OnslaughtAttackRateScale = 0.08;
const OnslaughtHitChancePenalty = 0.3;
const OnslaughtHitChanceRecoverDuration = 3;

type IState = {
	speedDifference: number;
	recover: number;
	damageDone: boolean;
};

export class OnslaughtEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29101;
	public static readonly baseDuration = OnslaughtHitChanceRecoverDuration - 1;
	public static readonly negative = false;
	public static readonly caption = 'Onslaught';
	public static readonly description = `Gain ${numericAPScaling(
		OnslaughtSpeedScale,
		AbilityPowerUIToken,
		OnslaughtSpeedBase,
	)} Speed and ${numericAPScaling(
		OnslaughtAttackRateScale,
		AbilityPowerUIToken,
		OnslaughtAttackRateBase,
	)} Attack Rate, but take ${BasicMaths.describePercentileModifier(
		{ bias: -OnslaughtHitChancePenalty },
		true,
	)} penalty to Hit Chance on all Weapons, recovering by ${BasicMaths.describePercentileModifier(
		{ bias: OnslaughtHitChancePenalty / OnslaughtHitChanceRecoverDuration },
		true,
	)} each Turn Start. Dealing Damage with Weapons extends the Effect Duration`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: OnslaughtEffect) => {
			const state: IState = {
				speedDifference: OnslaughtSpeedBase + effect.power * OnslaughtSpeedScale,
				recover: OnslaughtHitChanceRecoverDuration,
				damageDone: false,
			};
			unit.applyChassisModifier({
				speed: {
					bias: state.speedDifference,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
			}).applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: -OnslaughtHitChancePenalty,
					},
					baseRate: {
						bias: OnslaughtAttackRateBase + effect.power * OnslaughtAttackRateScale,
					},
				},
			});
			return state;
		},
		[EffectEvents.TUnitEffectEvents.turnStart]: (unit, effect: OnslaughtEffect) => {
			if (effect.state.recover) {
				unit.applyWeaponModifier({
					default: {
						baseAccuracy: {
							bias: OnslaughtHitChancePenalty / OnslaughtHitChanceRecoverDuration,
						},
					},
				});
			}
			return {
				...effect.state,
				recover: Math.max(0, effect.state?.recover || 0 - 1),
			};
		},
		[EffectEvents.TUnitEffectEvents.unitHit]: (unit, effect: OnslaughtEffect, meta) => {
			if (!effect?.state?.damageDone && AttackManager.isAttackEfficient(meta?.result)) {
				effect.duration += 1;
				return {
					...effect.state,
					damageDone: true,
				};
			}
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: OnslaughtEffect) => {
			unit.applyChassisModifier({
				speed: {
					bias: -effect.state.speedDifference,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
			}).applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: (OnslaughtHitChancePenalty * effect.state.recover) / OnslaughtHitChanceRecoverDuration,
					},
					baseRate: {
						bias: -1 * (OnslaughtAttackRateBase + effect.power * OnslaughtAttackRateScale),
					},
				},
			});
			return effect.state;
		},
	};
	public priority = 0;
}

export default OnslaughtEffect;

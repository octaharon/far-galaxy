import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import AttackManager from '../../../../damage/AttackManager';
import UnitDefense from '../../../../damage/defense';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const DISABLING_SHOT_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class DisablingShotEffect extends GenericUnitStatusEffect<null> {
	public static readonly id = 29000;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Disabling Shot';
	public static readonly description = `Deal full Weapon Damage and remove a random Non-Negative Effect from target, as well as Resilient Defense Perk. Removing the latter grants additional ${DISABLING_SHOT_XP} XP to the Unit`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DisablingShotEffect) => {
				const src = effect.source;
				if (GenericStatusEffect.isUnitTarget(src)) {
					if (unit.hasDefensePerks(UnitDefense.defenseFlags.resilient)) {
						src.addXp(DISABLING_SHOT_XP);
						unit.removeDefensePerks(UnitDefense.defenseFlags.resilient);
					}
					unit.removeEffect(
						unit.effects.filter((e) => !e.static.negative && !e.static.unremovable)[0]?.getInstanceId(),
					);
					(src.attacks ?? [])
						.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
						.forEach((a) => unit.applyAttackUnit(a, src));
				}
				return null;
			},
		};
	public priority = 0;
}

export default DisablingShotEffect;

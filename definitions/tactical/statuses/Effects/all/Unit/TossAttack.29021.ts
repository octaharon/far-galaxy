import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import Geometry from '../../../../../maths/Geometry';
import UnitAttack from '../../../../damage/attack';
import AttackManager from '../../../../damage/AttackManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const TossAttackBonus = 0.25;
const TossAttackScale = 0.15;

export class TossAttackEffect extends GenericUnitStatusEffect<null> {
	public static readonly id = 29021;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Toss Attack';
	public static readonly description = `Pitch-up barrage that deals full Weapons Damage to the Unit and to all Units caught in the line of fire, increased by ${numericAPScaling(
		TossAttackScale,
		AbilityPowerUIToken,
		TossAttackBonus,
		true,
	)} if a target has full Hit Points. Beam and Supersonic Attacks can't be targeted behind the horizon and thus become Aerial.`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: TossAttackEffect, meta) => {
				const src = effect.source;
				if (!GenericStatusEffect.isUnitTarget(src) || !src.attacks?.length || src.isDisabled || !src.location)
					return effect.state;
				const targets = [unit];
				const lineOfFire = Geometry.getLineFromPoints(src.location, unit.location);
				if (meta.map)
					targets.push(
						...(meta.map
							?.getDeployedUnits()
							?.filter(
								(u) =>
									!u.isFriendlyToPlayer(src.playerId) &&
									Geometry.doesLineIntersectCircle(lineOfFire, u.location),
							) ?? []),
					);
				for (const t of targets)
					src.attacks
						.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
						.map((a) =>
							Object.assign(a, {
								delivery: [UnitAttack.deliveryType.supersonic, UnitAttack.deliveryType.beam].includes(
									a.delivery,
								)
									? UnitAttack.deliveryType.aerial
									: a.delivery,
							} as typeof a),
						)
						.forEach((a) =>
							t.applyAttackUnit(a, src, {
								factor:
									t.currentHitPoints >= t.maxHitPoints
										? effect.power * TossAttackScale + TossAttackBonus + 1
										: 1,
							}),
						);
				return null;
			},
		};
	public priority = 0;
}

export default TossAttackEffect;

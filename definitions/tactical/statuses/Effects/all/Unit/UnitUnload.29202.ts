import { DIInjectableSerializables } from '../../../../../core/DI/injections';
import BasicMaths from '../../../../../maths/BasicMaths';
import Geometry from '../../../../../maths/Geometry';
import Vectors from '../../../../../maths/Vectors';
import { UNIT_MAP_SIZE } from '../../../../units/types';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import UnitCargoEffect, { UnitUnloadRange } from './UnitCargo.29201';

type IState = null;

export class UnitUnloadEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29202;
	public static readonly baseDuration = 0;
	public static readonly negative = false;
	public static readonly caption = 'Unload Unit';
	public static readonly description = `Unload a Unit from cargo bay. If the carried Unit can't be Deployed within ${UnitUnloadRange} Effect Radius from current Location, the Ability has no effect`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: UnitUnloadEffect) => {
			if (
				effect.static.isUnitTarget(effect.source) &&
				effect.source.hasEffect(UnitCargoEffect.id) &&
				effect.source.location?.mapId
			) {
				const map = effect.getProvider(DIInjectableSerializables.map).find(effect.source.location.mapId);
				if (!map) return null;
				let deployed = false;
				effect.source
					.getStatusEffects()
					.filter((e) => e.static.id === UnitCargoEffect.id)
					.forEach((e: UnitCargoEffect) => {
						if (deployed) return;
						const u = effect.getProvider(DIInjectableSerializables.unit).unserialize(e.state.unit);
						// Bruteforce a suitable location on the map
						for (let r = 0; (r += UNIT_MAP_SIZE / 10); !deployed && r <= UnitUnloadRange) {
							const step =
								Math.PI /
								BasicMaths.lerp({
									min: 6,
									max: 60,
									x: r / UnitUnloadRange,
								});
							for (let alpha = 0; (alpha += step); !deployed && alpha < Math.PI * 2 && r > 0) {
								deployed = map.deployUnit(
									u,
									Vectors.add(unit.location, Geometry.polar2vector({ r, alpha })),
								);
							}
						}
						if (deployed) effect.source.removeEffect(e.getInstanceId());
					});
				if (deployed) {
					effect.source.kit.addAbility(UnitCargoEffect.id);
					effect.source.kit.removeAbility(UnitUnloadEffect.id);
				}
			}
			return null;
		},
	};
	public priority = 0;
}

export default UnitUnloadEffect;

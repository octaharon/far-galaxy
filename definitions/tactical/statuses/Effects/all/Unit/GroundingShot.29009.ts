import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import AttackManager from '../../../../damage/AttackManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const GroundingShotBonus = 0.5;
const GroundingShotBonusScale = 1.5;
type IState = { speedReduction: number };

export class GroundingShotEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29009;
	public static readonly baseDuration = 3;
	public static readonly negative = true;
	public static readonly caption = 'Grounding Shot';
	public static readonly description = `Shoot with all Weapons at a Unit, temporarily reducing its Speed by ${numericAPScaling(
		-GroundingShotBonusScale,
		AbilityPowerUIToken,
		-GroundingShotBonus,
	)}`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: GroundingShotEffect) => {
			const src = effect.source;
			if (GenericStatusEffect.isUnitTarget(src))
				(src.attacks ?? [])
					.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
					.forEach((a) => unit.applyAttackUnit(a, src));
			const speedReduction = Math.min(unit.speed, GroundingShotBonus + GroundingShotBonusScale * effect.power);
			unit.applyChassisModifier({ speed: { bias: -speedReduction } });
			return { speedReduction };
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: GroundingShotEffect) => {
			unit.applyChassisModifier({ speed: { bias: effect.state.speedReduction } });
			return { speedReduction: 0 };
		},
	};
	public priority = 0;
}

export default GroundingShotEffect;

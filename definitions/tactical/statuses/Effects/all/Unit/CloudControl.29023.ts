import _ from 'underscore';
import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import AttackManager from '../../../../damage/AttackManager';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericMapStatusEffect, GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const CloudControlDamage = 0.35;
const CloudControlDamageScale = 0.15;
const CloudControlXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;
export const CloudControlAreaRadius = 2.5;

export class CloudControlEffect extends GenericMapStatusEffect<null> {
	public static readonly id = 29023;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Cloud Control';
	public static readonly description = `Deal full Weapons Damage to all Hover and Airborne Units in Effect Radius ${CloudControlAreaRadius}. Units that Move (including Attack Move), interrupt current Action, take additional ${numericAPScaling(
		CloudControlDamageScale,
		AbilityPowerUIToken,
		CloudControlDamage,
		true,
	)} Damage and grant the invoker ${CloudControlXP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, null> = {
		[EffectEvents.TMapEffectEvents.apply]: (mapCell, effect: CloudControlEffect, meta) => {
			const factor = 1 + (CloudControlDamage + CloudControlDamageScale * effect.power);
			const src = effect.source;

			if (GenericStatusEffect.isUnitTarget(src)) {
				const attacks = (src.attacks ?? []).flatMap((a) => AttackManager.getAttackBatch(a, src).attacks);
				meta.map
					.getUnitsInRange(mapCell)
					.filter(
						(u) =>
							u.movement === UnitChassis.movementType.hover ||
							u.movement === UnitChassis.movementType.air,
					)
					.forEach((u) => {
						attacks.forEach((att) => u.applyAttackUnit(att, src, u.status.moving ? { factor } : null));
						if (u.status.moving) {
							src.addXp(CloudControlXP);
							u.status.moving = null;
						}
					});
			}
			return null;
		},
	};
	public priority = 0;
}

export default CloudControlEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import AttackManager from '../../../../damage/AttackManager';
import UnitDefense from '../../../../damage/defense';
import { revokeAction } from '../../../../units/actions/ActionManager';
import UnitActions from '../../../../units/actions/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericMapStatusEffect, GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const DETECTOR_SHOT_DAMAGE = 0.35;
const DETECTOR_SHOT_DAMAGE_SCALE = 0.15;
const DETECTOR_SHOT_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;
export const DetectorShotRadius = 2;

export class DetectorShotEffect extends GenericMapStatusEffect<null> {
	public static readonly id = 29006;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Detector Shot';
	public static readonly description = `Deal ${numericAPScaling(
		DETECTOR_SHOT_DAMAGE_SCALE,
		AbilityPowerUIToken,
		DETECTOR_SHOT_DAMAGE,
		true,
	)} of full Weapons Damage to all Units in ${DetectorShotRadius} Effect Radius. Remove Cloak Action and Evasion Module from all affected Units, and doing so grants additional ${DETECTOR_SHOT_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, null> = {
		[EffectEvents.TMapEffectEvents.apply]: (mapCell, effect: DetectorShotEffect, meta) => {
			if (GenericUnitStatusEffect.isUnitTarget(effect.source) && effect.source.attacks?.length && meta?.map) {
				const factor = 1 + (DETECTOR_SHOT_DAMAGE + DETECTOR_SHOT_DAMAGE_SCALE * effect.power);
				const src = effect.source;

				if (GenericStatusEffect.isUnitTarget(src)) {
					const attacks = (src.attacks ?? []).flatMap((a) => AttackManager.getAttackBatch(a, src).attacks);
					meta.map.getUnitsInRange(mapCell).forEach((u) => {
						let affected = u.hasDefensePerks(UnitDefense.defenseFlags.evasion);
						u.removeDefensePerks(UnitDefense.defenseFlags.evasion);
						if (u.status.cloaked) {
							affected = true;
							u.status.cloaked = false;
						}
						u.actions = revokeAction(u.actions, UnitActions.unitActionTypes.cloak);
						attacks.forEach((att) =>
							u.applyAttackUnit(att, src, {
								factor,
							}),
						);
						if (affected) src.addXp(DETECTOR_SHOT_XP);
					});
				}
			}
			return null;
		},
	};
	public priority = 0;
}

export default DetectorShotEffect;

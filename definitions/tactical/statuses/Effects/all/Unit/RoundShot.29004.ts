import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import AttackManager from '../../../../damage/AttackManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const RoundShotBonus = 0.5;
const RoundShotBonusScale = 0.2;
const RoundShotBonusXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class StrafeShotEffect extends GenericUnitStatusEffect<null> {
	public static readonly id = 29004;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Strafe Shot';
	public static readonly description = `Deal full Weapons Damage to all Hostile targets around with ${numericAPScaling(
		+RoundShotBonusScale,
		AbilityPowerUIToken,
		-RoundShotBonus,
		true,
	)} penalty to Damage. Gain ${RoundShotBonusXP} XP for each breached Shield`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: StrafeShotEffect, meta) => {
				const src = effect.source;
				const factor = 1 - (RoundShotBonus - RoundShotBonusScale * effect.power);
				if (
					!unit.isFriendlyToPlayer(src?.owner?.id) &&
					effect.static.isUnitTarget(src) &&
					src.attacks?.length &&
					meta?.map
				) {
					src.addXp(
						(src.attacks ?? [])
							.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
							.map((a) => unit.applyAttackUnit(a, src, { factor }))
							.filter((v) => v.breach).length * RoundShotBonusXP,
					);
					return null;
				}
			},
		};
	public priority = 0;
}

export default StrafeShotEffect;

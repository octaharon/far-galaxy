import { DIInjectableCollectibles } from '../../../../../core/DI/injections';
import BasicMaths from '../../../../../maths/BasicMaths';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import UnitCargoEffect, { TUnitCargoEffectStateType, UnitLiftSpeedRatio } from './UnitCargo.29201';
import UnitUnloadEffect from './UnitUnload.29202';
import describeModifier = BasicMaths.describeModifier;

type IState = null;

export class UnitLiftEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29200;
	public static readonly baseDuration = 0;
	public static readonly negative = false;
	public static readonly caption = 'Lift Unit';
	public static readonly description = `Lift a non-Massive, non-Airborne Unit and put it into a cargo bay, reducing own Speed by ${describeModifier(
		{ factor: UnitLiftSpeedRatio },
	)}. Lifted Unit is kept in Stasis, not gaining XP and having all Effects and Cooldowns intact by the moment it is Unloaded to a Map`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: UnitLiftEffect) => {
			const u = unit.serialize();
			if (
				effect.static.isUnitTarget(effect.source) &&
				!effect.getProvider(DIInjectableCollectibles.chassis).isMassive(unit.prototype?.chassis?.static?.id) &&
				!effect.getProvider(DIInjectableCollectibles.chassis).isAir(unit.prototype?.chassis?.static?.id)
			) {
				effect.source.applyEffect(
					{
						effectId: UnitCargoEffect.id,
						props: {
							state: {
								unit: u,
							} as TUnitCargoEffectStateType,
						},
					},
					effect.source,
				);
				effect.source.kit.removeAbility(UnitCargoEffect.id);
				effect.source.kit.addAbility(UnitUnloadEffect.id);
			}
			return null;
		},
	};
	public priority = 0;
}

export default UnitLiftEffect;

import BasicMaths from '../../../../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import Units from '../../../../units/types';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describeModifier = BasicMaths.describeModifier;

export type TUnitCargoEffectStateType = {
	unit: Units.TSerializedUnit;
	speedDebuff: number;
};

export const UnitLiftSpeedRatio = 0.8;
export const UnitUnloadRange = 0.5;

export class UnitCargoEffect extends ConstantUnitStatusEffect<TUnitCargoEffectStateType> {
	public static readonly id = 29201;
	public static readonly unremovable = false;
	public static readonly negative = false;
	public static readonly caption = 'Unit Cargo';
	public static readonly description = `This Unit carries another Unit inside and suffers ${describeModifier({
		factor: UnitLiftSpeedRatio,
	})} penalty to Speed while doing so`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		TUnitCargoEffectStateType
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: UnitCargoEffect) => {
			const speedDebuff = unit.speed * (1 - UnitLiftSpeedRatio);
			unit.applyChassisModifier({
				speed: {
					bias: -speedDebuff,
					min: 0,
					minGuard: ArithmeticPrecision,
				},
			});
			return {
				...effect.state,
				speedDebuff,
			};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: UnitCargoEffect, meta) => {
			unit.applyChassisModifier({
				speed: { bias: effect.state.speedDebuff },
			});
			if (
				meta.reason &&
				(meta.reason.effectTargetType !== unit.effectTargetType ||
					meta.reason.getInstanceId() !== unit.getInstanceId())
			) {
				unit.removeEffect(
					unit
						.getStatusEffects()
						.find((t) => t.static.id === UnitCargoEffect.id)
						?.getInstanceId(),
				);
			}
			return effect.state;
		},
	};
	public priority = 0;
}

export default UnitCargoEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import AttackManager from '../../../../damage/AttackManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const ExecutionShotBonus = 0.12;
const ExecutionShotBonusScale = 0.03;

export class ExecutionShotEffect extends GenericUnitStatusEffect<null> {
	public static readonly id = 29003;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Execution Shot';
	public static readonly description = `Attack a Unit with all Weapons, dealing additional ${numericAPScaling(
		ExecutionShotBonusScale,
		AbilityPowerUIToken,
		ExecutionShotBonus,
		true,
	)} Damage per every 10% of target's missing Hit Points. Killing a Unit resets the Ability Cooldown`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: ExecutionShotEffect) => {
				if (GenericUnitStatusEffect.isUnitTarget(effect.source) && effect.source.attacks?.length) {
					const src = effect.source;
					const factor =
						(ExecutionShotBonus + ExecutionShotBonusScale * effect.power) *
						Math.floor((1 - unit.currentHitPoints / unit.maxHitPoints) * 10);
					if (GenericStatusEffect.isUnitTarget(src))
						if (
							(src.attacks ?? [])
								.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
								.map((a) => unit.applyAttackUnit(a, src, { factor }))
								.some((v) => v.killed)
						)
							src.kit.setAbilityCooldown(0, ExecutionShotEffect.id);
				}
				return null;
			},
		};
	public priority = 0;
}

export default ExecutionShotEffect;

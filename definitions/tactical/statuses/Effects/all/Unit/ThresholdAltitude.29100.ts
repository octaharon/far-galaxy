import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import UnitAttack from '../../../../damage/attack';
import DamageManager from '../../../../damage/DamageManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const ThresholdAltitudeDodgeScale = 0.2;
const ThresholdAltitudeDodgeBase = 0.4;
const ThresholdAltitudeSpeedFactor = 0.3;

type IState = {
	dodgeFactor: number;
	speedDifference: number;
	damageTaken: boolean;
};

export class ThresholdAltitudeEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29100;
	public static readonly baseDuration = 1;
	public static readonly negative = false;
	public static readonly caption = 'Threshold Altitude';
	public static readonly description = `Move at minimal altitudes, sacrificing ${BasicMaths.describePercentileModifier(
		{ bias: -ThresholdAltitudeSpeedFactor },
		true,
	)} Speed for ${numericAPScaling(
		ThresholdAltitudeDodgeScale,
		AbilityPowerUIToken,
		ThresholdAltitudeDodgeBase,
		true,
	)} Dodge against Missile, Aerial and Drone Attacks. If the Unit avoids any Damage for the Effect Duration, it goes Cloak`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: ThresholdAltitudeEffect) => {
			const state: IState = {
				dodgeFactor: ThresholdAltitudeDodgeBase + effect.power * ThresholdAltitudeDodgeScale,
				speedDifference: ThresholdAltitudeSpeedFactor * unit.speed,
				damageTaken: false,
			};
			unit.applyChassisModifier({
				speed: {
					bias: -state.speedDifference,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
				dodge: {
					[UnitAttack.deliveryType.missile]: {
						bias: state.dodgeFactor,
					},
					[UnitAttack.deliveryType.aerial]: {
						bias: state.dodgeFactor,
					},
					[UnitAttack.deliveryType.drone]: {
						bias: state.dodgeFactor,
					},
				},
			});
			return state;
		},
		[EffectEvents.TUnitEffectEvents.unitDamaged]: (unit, effect: ThresholdAltitudeEffect, meta) => {
			if (DamageManager.getTotalDamageValue(meta?.damage) > ArithmeticPrecision)
				return {
					...effect.state,
					damageTaken: true,
				};
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: ThresholdAltitudeEffect) => {
			unit.applyChassisModifier({
				speed: {
					bias: effect.state.speedDifference,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
				dodge: {
					[UnitAttack.deliveryType.missile]: {
						bias: -effect.state.dodgeFactor,
					},
					[UnitAttack.deliveryType.ballistic]: {
						bias: -effect.state.dodgeFactor,
					},
					[UnitAttack.deliveryType.drone]: {
						bias: -effect.state.dodgeFactor,
					},
				},
			});
			if (!effect.state.damageTaken) unit.status.cloaked = true;
			return effect.state;
		},
	};
	public priority = 0;
}

export default ThresholdAltitudeEffect;

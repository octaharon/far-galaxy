import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import AttackManager from '../../../../damage/AttackManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericMapStatusEffect, GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const SkyShotBonus = 0.12;
const SkyShotBonusScale = 0.03;
const SkyShotXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;
export const SkyShotRadius = 2;

export class HowitzerShotEffect extends GenericMapStatusEffect<null> {
	public static readonly id = 29005;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Howitzer Shot';
	public static readonly description = `Extra long range attack that deals full Weapons Damage to all Units in Effect Radius ${SkyShotRadius}, increased by ${numericAPScaling(
		SkyShotBonusScale,
		AbilityPowerUIToken,
		SkyShotBonus,
		true,
	)} for each Status Effect on target, including this one. If the Ability hits 2 or more Units, gain ${SkyShotXP} bonus XP for every additional target`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, null> = {
		[EffectEvents.TMapEffectEvents.apply]: (mapCell, effect: HowitzerShotEffect, meta) => {
			if (GenericUnitStatusEffect.isUnitTarget(effect.source) && effect.source.attacks?.length && meta?.map) {
				const src = effect.source;
				const targets = meta.map.getUnitsInRange(mapCell);
				if (GenericStatusEffect.isUnitTarget(src)) {
					(src.attacks ?? [])
						.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
						.forEach((a) =>
							targets.forEach((unit) =>
								unit.applyAttackUnit(a, src, {
									factor:
										1 +
										(unit.getStatusEffects().length + 1) *
											(SkyShotBonus + SkyShotBonusScale * effect.power),
								}),
							),
						);
					if (targets.length >= 2) src.addXp(SkyShotXP * (targets.length - 1));
				}
			}
			return null;
		},
	};
	public priority = 0;
}

export default HowitzerShotEffect;

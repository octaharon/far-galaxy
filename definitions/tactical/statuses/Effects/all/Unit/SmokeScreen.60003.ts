import { numericAPScaling }       from '../../../../../../src/utils/wellRounded';
import { IMapCell }               from '../../../../map';
import Units                      from '../../../../units/types';
import { AbilityPowerUIToken }    from '../../../constants';
import EffectEvents               from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta           from '../../../statusEffectMeta';

interface IState {
	oldUnitStatus: Record<string, Units.TUnitCombatStatus>;
}

const SmokeScreenHitChance = 0.24;
const SmokeScreenScale = 0.16;

export class SmokeScreenEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 60003;
	public static readonly negative = false;
	public static readonly baseDuration = 4;
	public static readonly caption = 'Smoke screen';
	public static readonly description = `All Units in area are Cloaked and get bonus ${numericAPScaling(
		SmokeScreenScale,
		AbilityPowerUIToken,
		SmokeScreenHitChance,
		true,
	)} Dodge, but lose the same amount of Hit Chance`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.unitEntersZone]: (cell: IMapCell, effect: SmokeScreenEffect, meta?) => {
				const chance = SmokeScreenHitChance + SmokeScreenScale * effect.power;
				const r: IState = {
					oldUnitStatus: {},
				};
				const u = meta.unit;
				r.oldUnitStatus[u.getInstanceId()] = u.status;
				u.status.cloaked = true;
				u.applyChassisModifier({
					dodge: {
						default: {
							bias: chance,
						},
					},
				});
				u.applyWeaponModifier({
					default: {
						baseAccuracy: {
							bias: -chance,
						},
					},
				});
				return r;
			},
			[EffectEvents.TMapEffectEvents.unitLeavesZone]: (cell: IMapCell, effect: SmokeScreenEffect, meta?) => {
				const chance = SmokeScreenHitChance + SmokeScreenScale * effect.power;
				const r: IState = {
					oldUnitStatus: {},
				};
				const u = meta.unit;
				u.status.cloaked = !!(
					effect.state.oldUnitStatus[u.getInstanceId()] &&
					effect.state.oldUnitStatus[u.getInstanceId()].cloaked
				);
				u.applyChassisModifier({
					dodge: {
						default: {
							bias: -chance,
						},
					},
				});
				u.applyWeaponModifier({
					default: {
						baseAccuracy: {
							bias: chance,
						},
					},
				});
				return r;
			},
		};
	public priority = 0;
}

export default SmokeScreenEffect;

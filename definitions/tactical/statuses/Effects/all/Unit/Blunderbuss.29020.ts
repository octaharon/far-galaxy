import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import DIContainer from '../../../../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../core/DI/injections';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import AttackManager from '../../../../damage/AttackManager';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const BLUNDERBUSS_DAMAGE = 0.15;
const BLUNDERBUSS_DAMAGE_SCALE = 0.25;
const BLUNDERBUSS_SHOT_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class BlunderbussShotEffect extends GenericUnitStatusEffect<null> {
	public static readonly id = 29020;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Blunderbuss shot';
	public static readonly description = `Deal full Weapon Damage to Unit , with additional ${numericAPScaling(
		BLUNDERBUSS_DAMAGE_SCALE,
		AbilityPowerUIToken,
		BLUNDERBUSS_DAMAGE,
		true,
	)} Damage to Hover Units and Units with Warp Defense. The latter, if any, is removed with this Attack, granting extra ${BLUNDERBUSS_SHOT_XP} XP to the Unit`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: BlunderbussShotEffect) => {
				const src = effect.source;
				const perk = unit.hasDefensePerks(UnitDefense.defenseFlags.warp);
				const factor =
					perk ||
					DIContainer.getProvider(DIInjectableCollectibles.chassis).isHover(
						unit.prototype?.chassis?.static?.id,
					)
						? effect.power * BLUNDERBUSS_DAMAGE_SCALE + BLUNDERBUSS_DAMAGE + 1
						: 1;
				if (GenericStatusEffect.isUnitTarget(src)) {
					if (perk) {
						unit.removeDefensePerks(UnitDefense.defenseFlags.warp);
						src.addXp(BLUNDERBUSS_SHOT_XP);
					}
					(src.attacks ?? [])
						.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
						.forEach((a) => unit.applyAttackUnit(a, src, { factor }));
				}
				return null;
			},
		};
	public priority = 0;
}

export default BlunderbussShotEffect;

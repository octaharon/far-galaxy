import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import AttackManager from '../../../../damage/AttackManager';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

const BLASTING_SHOT_DAMAGE_BONUS = 0.08;
const BLASTING_SHOT_DAMAGE_SCALE = 0.04;
const BLASTING_SHOT_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class BlastingShotEffect extends GenericUnitStatusEffect<null> {
	public static readonly id = 29007;
	public static readonly baseDuration = 0;
	public static readonly negative = true;
	public static readonly caption = 'Blasting Shot';
	public static readonly description = `Shoot with all Weapons at a Unit, dealing ${numericAPScaling(
		BLASTING_SHOT_DAMAGE_SCALE,
		AbilityPowerUIToken,
		BLASTING_SHOT_DAMAGE_BONUS,
		true,
	)} extra Damage per every 10% of target's Shield Capacity. The target loses Reactive Defense, if any, and the Unit gains ${BLASTING_SHOT_XP} XP in that case`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, null> =
		{
			[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: BlastingShotEffect) => {
				if (GenericUnitStatusEffect.isUnitTarget(effect.source) && effect.source.attacks?.length) {
					const src = effect.source;
					const factor =
						1 +
						Math.floor(Math.min(1, unit.currentShields / (unit.totalShields || 1)) * 10) +
						(1 + BLASTING_SHOT_DAMAGE_BONUS + BLASTING_SHOT_DAMAGE_SCALE * effect.power);
					if (GenericStatusEffect.isUnitTarget(src))
						(src.attacks ?? [])
							.flatMap((a) => AttackManager.getAttackBatch(a, src).attacks)
							.forEach((a) => unit.applyAttackUnit(a, src, { factor }));
					if (unit.hasDefensePerks(UnitDefense.defenseFlags.reactive)) {
						unit.addXp(BLASTING_SHOT_XP);
						unit.removeDefensePerks(UnitDefense.defenseFlags.reactive);
					}
				}
				return null;
			},
		};
	public priority = 0;
}

export default BlastingShotEffect;

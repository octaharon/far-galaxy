import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import Geometry from '../../../../../maths/Geometry';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { ILocationOnMap } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import AboveTheFightEffect from './AboveTheFight.29105';
import describeModifier = BasicMaths.describeModifier;

const AirInterdictionSpeedScale = 0.1;
const AirInterdictionSpeedBase = 0.3;
const AirInterdictionAttackRangeBase = 0.75;
const AirInterdictionRateScale = 0.1;
const AirInterdictionRateBonus = 0.15;
const AirInterdictionXP = DEFAULT_PLAYER_RESEARCH_XP;

type IState = {
	rangeDifference: number[];
	speedDifference: number;
	location: ILocationOnMap;
	located: boolean;
};

export class AirInterdictionEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 29104;
	public static readonly baseDuration = 1;
	public static readonly negative = false;
	public static readonly caption = 'Air Interdiction';
	public static readonly description = `Until Turn End lose ${numericAPScaling(
		AirInterdictionSpeedScale,
		AbilityPowerUIToken,
		-(1 - AirInterdictionSpeedBase),
		true,
	)} Speed, but gain ${describeModifier({
		factor: 1 + AirInterdictionAttackRangeBase,
	})} Attack Range and ${numericAPScaling(
		AirInterdictionRateScale,
		AbilityPowerUIToken,
		AirInterdictionRateBonus,
		false,
	)} Attack Rate. If the Unit stays out of Hostile Scan Range for the Effect Duration, it gains ${AirInterdictionXP} XP and, when the Effect expires, returns to the location it was applied. "Above The Fight" Effect is removed for the Effect Duration`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: AirInterdictionEffect) => {
			const state: IState = {
				rangeDifference: unit.attacks.map((a) => AirInterdictionAttackRangeBase * a.baseRange),
				speedDifference: unit.speed * (AirInterdictionSpeedBase + effect.power * AirInterdictionSpeedScale),
				location: { ...unit.location },
				located: false,
			};
			unit.applyChassisModifier({
				speed: {
					bias: state.speedDifference,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
			});
			if (unit.hasEffect(AboveTheFightEffect.id))
				unit.effects
					.filter((e) => e.static.id === AboveTheFightEffect.id)
					.forEach((e) => unit.removeEffect(e.getInstanceId()));
			state.rangeDifference.forEach((rd, ix) =>
				unit.applyWeaponModifier(
					{
						default: {
							baseRate: {
								bias: AirInterdictionRateBonus + effect.power * AirInterdictionRateScale,
								minGuard: ArithmeticPrecision,
							},
							baseRange: {
								bias: rd,
								min: 0,
								minGuard: 0,
							},
						},
					},
					ix,
				),
			);
			return state;
		},
		[EffectEvents.TUnitEffectEvents.move]: (unit, effect: AirInterdictionEffect, meta) => {
			if (!meta?.bases || !effect?.source) return effect.state;
			let located = false;
			for (const playerId of Object.keys(meta.bases).filter((pId) => pId !== effect.source.playerId)) {
				if (meta.map.getScannableArea(playerId).some((s) => Geometry.isPointInRange(unit.location, s))) {
					located = true;
					break;
				}
			}
			return {
				...effect.state,
				located,
			};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: AirInterdictionEffect) => {
			unit.applyChassisModifier({
				speed: {
					bias: effect.state.speedDifference,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
			}).applyEffect(
				{
					effectId: AboveTheFightEffect.id,
				},
				unit,
			);
			effect.state.rangeDifference.forEach((rd, ix) =>
				unit.applyWeaponModifier(
					{
						default: {
							baseRate: {
								bias: -AirInterdictionRateBonus - effect.power * AirInterdictionRateScale,
								minGuard: ArithmeticPrecision,
								min: 0,
							},
							baseRange: {
								bias: -rd,
								min: 0,
								minGuard: 0,
							},
						},
					},
					ix,
				),
			);
			if (effect.state.located) {
				unit.location = effect.state.location;
				unit.addXp(AirInterdictionXP);
			}
			return effect.state;
		},
	};
	public priority = 0;
}

export default AirInterdictionEffect;

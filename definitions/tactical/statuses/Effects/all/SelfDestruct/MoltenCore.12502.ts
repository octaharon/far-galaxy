import { ArithmeticPrecision } from '../../../../../maths/constants';
import MoltenCoreAbility from '../../../../abilities/all/SelfDestruct/MoltenCore.25000';
import UnitAttack from '../../../../damage/attack';
import AttackManager from '../../../../damage/AttackManager';
import Damage from '../../../../damage/damage';
import { ICombatMap } from '../../../../map';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	attack?: UnitAttack.TAttackAOEUnit;
}

export const MOLTEN_CORE_DAMAGE_MIN = 140;
export const MOLTEN_CORE_DAMAGE_MAX = 180;
export const MOLTEN_CORE_DAMAGE_RADIUS = 180;

export class MoltenCoreEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 12502;
	public static readonly negative = true;
	public static readonly unremovable = true;
	public static readonly caption = 'Olympius Reactor';
	public static readonly description = `This unit carries an unstable reactor that explodes in Radius ${MOLTEN_CORE_DAMAGE_RADIUS} for ${MOLTEN_CORE_DAMAGE_MIN} to ${MOLTEN_CORE_DAMAGE_MAX} EMG, HEA and RAD Damage each when destroyed, intentionally or not`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect) => {
			return {
				...effect.state,
				attack: AttackManager.getAttackDamage({
					[UnitAttack.attackFlags.AoE]: true,
					radius: 4,
					delivery: UnitAttack.deliveryType.beam,
					aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
					magnitude: 1,
					[Damage.damageType.magnetic]: {
						min: 150,
						max: 200,
						distribution: 'Uniform',
					},
					[Damage.damageType.heat]: {
						min: 150,
						max: 200,
						distribution: 'Uniform',
					},
					[Damage.damageType.radiation]: {
						min: 150,
						max: 200,
						distribution: 'Uniform',
					},
				}) as UnitAttack.TAttackAOEUnit,
			};
		},
		[EffectEvents.TUnitEffectEvents.unitInvoke]: (unit, effect: MoltenCoreEffect, meta) => {
			if (meta.ability && meta.ability.static.id === MoltenCoreAbility.id) {
				unit.kit.removeAbility(MoltenCoreAbility.id);
				unit.addShields(-unit.totalShields).applyChassisModifier({ speed: { factor: 0 } }).defense.shields = [];
				meta.map.applyAoEAttack(unit.location, effect.state.attack, effect.source);
			}
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.unitKilled]: (unit, effect: MoltenCoreEffect, meta) => {
			(meta.map as ICombatMap).applyAoEAttack(unit.location, effect.state.attack, unit);
			return effect.state;
		},
	};
	public priority = -5;
}

export default MoltenCoreEffect;

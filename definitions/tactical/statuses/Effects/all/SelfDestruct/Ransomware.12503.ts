import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import RansomwareBaseEffect from './RansomwareBase.12504';

interface IState {}

export class RansomwareEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 12503;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Ransomware';
	public static readonly description =
		'This unit onboard circuitry is infected with a virus that persists in Salvage, which, when picked by a Base, yields no Technologies, reduces stored Energy by 50% and deals 50 EMG Damage to it';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.unitKilled]: (unit, effect: RansomwareEffect, meta) => {
			if (meta?.salvage?.payload) {
				meta.salvage.payload.effectsOnPickup = (meta.salvage.payload.effectsOnPickup ?? []).concat({
					effectId: RansomwareBaseEffect.id,
					props: {},
				});
				meta.salvage.payload.technologies = null;
			}
			return null;
		},
	};
	public priority = -5;
}

export default RansomwareEffect;

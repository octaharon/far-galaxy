import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class DestroyUnitEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 12510;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Destroy a unit';
	public static readonly description = 'This unit is destroyed';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect) => {
			unit.destroy(effect.source, false);
			return effect.state;
		},
	};
	public priority = -10;
}

export default DestroyUnitEffect;

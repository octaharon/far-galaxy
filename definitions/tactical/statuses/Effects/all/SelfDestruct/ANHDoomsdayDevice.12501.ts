import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class AnnihilationDoomsdayDeviceEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 12501;
	public static readonly negative = true;
	public static readonly unremovable = true;
	public static readonly caption = 'Hadron Inverter';
	public static readonly description =
		'This unit explodes for 450 ANH Damage in 2.5 Radius when destroyed, and leaves no technological Salvage';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.unitKilled]: (unit, effect: AnnihilationDoomsdayDeviceEffect, meta) => {
			if (meta && meta.salvage) meta.salvage.payload = {};
			if (meta && meta.map)
				meta.map.applyAoEAttack(unit.location, {
					[UnitAttack.attackFlags.AoE]: true,
					radius: 2.5,
					delivery: UnitAttack.deliveryType.beam,
					aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
					magnitude: 1,
					[Damage.damageType.antimatter]: 450,
				});
			return effect.state;
		},
	};
	public priority = -5;
}

export default AnnihilationDoomsdayDeviceEffect;

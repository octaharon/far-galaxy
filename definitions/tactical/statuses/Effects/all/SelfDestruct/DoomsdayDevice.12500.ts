import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { ICombatMap } from '../../../../map';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class DoomsdayDeviceEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 12500;
	public static readonly negative = true;
	public static readonly unremovable = true;
	public static readonly caption = 'Doomsday device';
	public static readonly description =
		'This unit explodes in 2.5 Radius when destroyed and leaves no technology Salvage';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.unitKilled]: (unit, effect: DoomsdayDeviceEffect, meta) => {
			meta.salvage.payload.technologies = {};
			(meta.map as ICombatMap).applyAoEAttack(unit.location, {
				[UnitAttack.attackFlags.AoE]: true,
				radius: 2.5,
				delivery: UnitAttack.deliveryType.supersonic,
				aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
				magnitude: 1,
				[Damage.damageType.thermodynamic]: 200,
				[Damage.damageType.heat]: 200,
			});
			return effect.state;
		},
	};
	public priority = -5;
}

export default DoomsdayDeviceEffect;

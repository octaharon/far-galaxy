import { TPlayerId } from '../../../../../player/playerId';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	sourcePlayerId: TPlayerId;
}

export class RansomwareBaseEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 12504;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Ransomware';
	public static readonly description = `Deal 50 EMG Damage to Base and remove 50% of stored Energy`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base, effect: RansomwareBaseEffect) => {
			if (base && base.playerId !== effect.state.sourcePlayerId) {
				base.applyAttackUnit({
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.magnetic]: 50,
				});
				base.addEnergy(-base.energyCapacity * 0.5);
			}
			return null;
		},
	};
	public priority = 0;
}

export default RansomwareBaseEffect;

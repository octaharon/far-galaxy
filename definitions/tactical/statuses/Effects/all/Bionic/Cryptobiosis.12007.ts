import { describePercentage, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { isOfTargetType } from '../../../../../technologies/chassis/helpers';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	triggered: boolean;
}

const CryptobiosisRegenBase = 15;
const CryptobiosisRegenScale = 15;
const CryptobiosisLastStandHP = 0.3;
const CryptobiosisXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class CryptobiosisEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 12007;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly caption = 'Cryptobiosis';
	public static readonly description = `Unit restores ${numericAPScaling(
		CryptobiosisRegenScale,
		AbilityPowerUIToken,
		CryptobiosisRegenBase,
	)} Hit Points every Turn End, and Organic Units restore twice as much. When the Unit takes Damage to Hit Points, it becomes Immune until Turn End, and then the Effect expires while the invoker gains extra ${CryptobiosisXP} XP. If the Damage was lethal, the Unit also restores ${describePercentage(
		CryptobiosisLastStandHP,
	)} of Max Hit Points and becomes Organic (even it it was Robotic)`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: CryptobiosisEffect, meta?) => {
			if (effect.state.triggered) {
				unit.removeDefensePerks(UnitDefense.defenseFlags.immune);
				effect.duration = 0;
				if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(CryptobiosisXP);
				return { triggered: false };
			} else {
				const mult = unit.isOrganic ? 2 : 1;
				unit.addHp((CryptobiosisRegenBase + CryptobiosisRegenScale * effect.power) * mult, effect.source);
				return effect.state;
			}
		},
		[EffectEvents.TUnitEffectEvents.unitDamaged]: (unit, effect: CryptobiosisEffect, meta?) => {
			if (unit.currentHitPoints <= 0) {
				unit.currentHitPoints = 0;
				unit.addHp(CryptobiosisLastStandHP * unit.maxHitPoints, effect.source);
				delete unit.targetFlags[UnitChassis.targetType.robotic];
				unit.targetFlags[UnitChassis.targetType.organic] = true;
			}
			unit.addDefensePerks(UnitDefense.defenseFlags.immune);
			return { ...effect.state, triggered: true };
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: CryptobiosisEffect, meta?) => {
			return { ...effect.state, triggered: false };
		},
	};
	public priority = 2;
}

export default CryptobiosisEffect;

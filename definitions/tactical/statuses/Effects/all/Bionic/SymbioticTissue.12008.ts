import { biasToPercent, describePercentage, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import DamageManager from '../../../../damage/DamageManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	damageTaken: number;
	damageTakenLastTurn: number;
}

const SymbioticTissueRegenBase = 0.25;
const SymbioticTissueRegenScale = 0.35;
const SymbioticTissueShieldDrain = 25;
const SymbioticTissueDamage = 75;
const SymbioticProtection = 0.12;
const SymbioticTissueXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class SymbioticTissueEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 12008;
	public static readonly negative = true;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Symbiotic Tissue';
	public static readonly description = `At Turn End, the Unit restores ${numericAPScaling(
		SymbioticTissueRegenScale,
		AbilityPowerUIToken,
		SymbioticTissueRegenBase,
		true,
	)} of total Hit Points lost under Effect Duration. If no Damage was taken, drain ${numericAPScaling(
		SymbioticTissueShieldDrain,
		AbilityPowerUIToken,
	)} Shield Capacity instead. If the Effect persists for the full Duration, the Unit gains ${biasToPercent(
		SymbioticProtection,
	)} Armor Protection, and the invoker gains ${SymbioticTissueXP} XP. If the Effect is removed before expiration, the Unit takes ${numericAPScaling(
		SymbioticTissueDamage,
		AbilityPowerUIToken,
	)} BIO Damage which, if it kills the Unit, it grants the invoker a bonus of ${SymbioticTissueXP} XP.`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: SymbioticTissueEffect, meta?) => {
			const state = { ...effect.state };
			if (effect.state.damageTakenLastTurn) {
				state.damageTaken += effect.state.damageTakenLastTurn;
				state.damageTakenLastTurn = 0;
				unit.addHp(
					(SymbioticTissueRegenBase + SymbioticTissueRegenScale * effect.power) * effect.state.damageTaken,
					effect.source,
				);
				return { ...effect.state };
			} else {
				unit.addShields(-effect.power * SymbioticTissueShieldDrain);
			}
			return state;
		},
		[EffectEvents.TUnitEffectEvents.unitDamaged]: (unit, effect: SymbioticTissueEffect, meta?) => {
			const state = { ...effect.state };
			state.damageTakenLastTurn += DamageManager.getTotalDamageValue(meta.damage);
			return state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SymbioticTissueEffect, meta?) => {
			if (effect.duration <= 0 && meta.reason?.getInstanceId() !== unit.getInstanceId()) {
				unit.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[Damage.damageType.biological]: SymbioticTissueDamage * effect.power,
					},
					effect.source,
				);
				if (unit.currentHitPoints <= 0 && effect.static.isUnitTarget(effect.source))
					effect.source.addXp(SymbioticTissueXP);
			} else {
				unit.applyArmorModifier({
					default: {
						factor: 1 - SymbioticProtection,
					},
				});
				if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(SymbioticTissueXP);
			}
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SymbioticTissueEffect, meta?) => {
			return { damageTaken: 0, damageTakenLastTurn: 0 };
		},
	};
	public priority = 2;
}

export default SymbioticTissueEffect;

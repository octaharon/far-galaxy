import _ from 'underscore';
import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import Damage from '../../../../damage/damage';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	shieldIndex: number;
}

const WILD_GROWTH_CAPACITY = 0.75;
const WILD_GROWTH_CAPACITY_SCALE = 0.25;
const WILD_GROWTH_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class WildGrowthEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 12006;
	public static readonly negative = false;
	public static readonly baseDuration = 4;
	public static readonly caption = 'Wild Growth';
	public static readonly description = `Unit gets a temporary Shield with Capacity equal to ${numericAPScaling(
		WILD_GROWTH_CAPACITY_SCALE,
		AbilityPowerUIToken,
		WILD_GROWTH_CAPACITY,
		true,
	)} of its maximum Hit Points, which fully negates BIO and HEA Damage, and it also regenerates 5 Hit Points every time it's damaged. If the Shield is discharged during Effect Duration, the invoker gains ${WILD_GROWTH_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: WildGrowthEffect, meta?) => {
			const grantAmount = unit.maxHitPoints * WILD_GROWTH_CAPACITY + effect.power * WILD_GROWTH_CAPACITY_SCALE;
			const shieldIndex = unit.defense?.shields?.length;
			if (!shieldIndex) return effect.state;
			unit.defense.shields.push({
				shieldAmount: grantAmount,
				currentAmount: grantAmount,
				[Damage.damageType.biological]: {
					factor: 0,
				},
				[Damage.damageType.heat]: {
					factor: 0,
				},
			});
			return { shieldIndex };
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: WildGrowthEffect) => {
			if (Number.isFinite(effect?.state?.shieldIndex) && unit.defense.shields[effect.state.shieldIndex])
				unit.defense.shields = unit.defense.shields.filter((_, i) => i !== effect.state.shieldIndex);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.unitHit]: (unit, effect: WildGrowthEffect, meta?) => {
			const shieldReduction = meta?.result?.shieldReduction[effect.state?.shieldIndex];
			if (shieldReduction > 0) {
				unit.addHp(5, effect.source);
				if (
					shieldReduction >=
						unit.defense.shields[effect.state.shieldIndex].currentAmount - ArithmeticPrecision &&
					effect.static.isUnitTarget(effect.source)
				)
					effect.source.addXp(WILD_GROWTH_XP);
			}
			return effect.state;
		},
	};
	public priority = -1;
}

export default WildGrowthEffect;

import _ from 'underscore';
import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import { DODGE_IMMUNITY_THRESHOLD } from '../../../../damage/constants';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	unitsEntered: Record<string, boolean>;
}

export const GAS_GRENADE_RADIUS = 1.5;
const GAS_GRENADE_SHIELD = 25;
const GAS_GRENADE_DODGE = 0.05;
const GAS_GRENADE_DAMAGE = 45;
const GAS_GRENADE_DAMAGE_SCALE = 25;
const GAS_GRENADE_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.2;

export class GasGrenadeEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 12009;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Gas Grenade';
	public static readonly description = `All Units within Effect Radius ${GAS_GRENADE_RADIUS} take ${numericAPScaling(
		GAS_GRENADE_DAMAGE_SCALE,
		AbilityPowerUIToken,
		GAS_GRENADE_DAMAGE,
	)} BIO Damage whenever they leave the area, but while inside, they lose ${numericAPScaling(
		GAS_GRENADE_DAMAGE_SCALE,
		AbilityPowerUIToken,
	)} Shield Capacity and ${numericAPScaling(
		GAS_GRENADE_DODGE,
		AbilityPowerUIToken,
	)} Dodge each Turn Action Phase. The invoker gains ${GAS_GRENADE_XP} XP for every affected Unit.`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: GasGrenadeEffect, meta?) => {
				if (GenericMapStatusEffect.isUnitTarget(effect?.source)) effect.source.addXp(GAS_GRENADE_XP);
				return effect.state;
			},
			[EffectEvents.TMapEffectEvents.actionPhase]: (cell: IMapCell, effect: GasGrenadeEffect, meta?) => {
				if (meta.unit) {
					meta.unit
						.applyChassisModifier({
							dodge: {
								default: {
									bias: -GAS_GRENADE_DODGE * effect.power,
									maxGuard: DODGE_IMMUNITY_THRESHOLD - ArithmeticPrecision,
								},
							},
						})
						.addShields(-GAS_GRENADE_SHIELD * effect.power);
				}
				return effect.state;
			},
			[EffectEvents.TMapEffectEvents.unitEntersZone]: (cell: IMapCell, effect: GasGrenadeEffect, meta?) => {
				return {
					...effect.state,
					unitsEntered: {
						...(effect.state.unitsEntered || {}),
						...(meta.unit ? { [meta.unit.getInstanceId()]: true } : {}),
					},
				};
			},
			[EffectEvents.TMapEffectEvents.unitLeavesZone]: (cell: IMapCell, effect: GasGrenadeEffect, meta?) => {
				const dmg = GAS_GRENADE_DAMAGE + GAS_GRENADE_DAMAGE_SCALE * effect.power;
				if (meta.unit) {
					meta.unit.applyAttackUnit(
						{
							delivery: UnitAttack.deliveryType.immediate,
							[Damage.damageType.biological]: dmg,
						},
						effect.source,
					);
				}
				return meta.unit
					? {
							...effect.state,
							unitsEntered: _.omit(effect.state.unitsEntered || {}, meta.unit.getInstanceId()),
					  }
					: effect.state;
			},
		};
	public priority = 0;
}

export default GasGrenadeEffect;

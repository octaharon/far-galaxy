import { describePercentage } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describeModifier = BasicMaths.describeModifier;

interface IState {
	xpGained: number;
}

const PREDATOR_SENSE_HP = 40;
const PREDATOR_SENSE_SCAN_RANGE = 5;
const PREDATOR_SENSE_SPEED_BONUS = 3;
const PREDATOR_SENSE_XP_RATE = 0.15;
const PREDATOR_SENSE_DURATION = 2;

export class PredatorSenseEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 12003;
	public static readonly negative = false;
	public static readonly durationFixed: boolean = false;
	public static readonly baseDuration = PREDATOR_SENSE_DURATION;
	public static readonly caption = 'Predator Sense';
	public static readonly description = `At Turn End the invoker receives ${describePercentage(
		PREDATOR_SENSE_XP_RATE,
	)} of XP gained by the Unit that Turn. Organic Units also gain extra ${describeModifier({
		bias: PREDATOR_SENSE_SCAN_RANGE,
	})} Scan Range, ${describeModifier({
		bias: PREDATOR_SENSE_SPEED_BONUS,
	})} Speed and ${AbilityPowerUIToken}${PREDATOR_SENSE_HP} Max Hit Points for Effect Duration`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.xpGained]: (unit, effect: PredatorSenseEffect, meta) => {
			return {
				...(effect.state ?? {}),
				xpGained: (effect?.state?.xpGained ?? 0) + meta.xp,
			};
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: PredatorSenseEffect) => {
			if (effect.static.isUnitTarget(effect.source))
				effect.source.addXp((effect?.state?.xpGained ?? 0) * PREDATOR_SENSE_XP_RATE);
			return { xpGained: 0 };
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: PredatorSenseEffect) => {
			const power = PREDATOR_SENSE_HP * effect.power;
			if (unit.isOrganic) {
				unit.applyChassisModifier({
					scanRange: {
						bias: PREDATOR_SENSE_SCAN_RANGE,
					},
					speed: {
						bias: PREDATOR_SENSE_SPEED_BONUS,
						min: 0,
						minGuard: ArithmeticPrecision,
					},
					hitPoints: {
						bias: power,
					},
				}).addHp(power, effect.source);
			}
			return { xpGained: 0 };
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: PredatorSenseEffect) => {
			const power = PREDATOR_SENSE_HP * effect.power;
			if (unit.isOrganic) {
				unit.applyChassisModifier({
					scanRange: {
						bias: -PREDATOR_SENSE_SCAN_RANGE,
						min: 0,
					},
					speed: {
						bias: -PREDATOR_SENSE_SPEED_BONUS,
						min: 0,
						minGuard: 0,
					},
					hitPoints: {
						bias: -power,
						min: 1,
					},
				});
			}
			return effect.state;
		},
	};
	public priority = 2;
}

export default PredatorSenseEffect;

import { biasToPercent } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { TPlayerId } from '../../../../../player/playerId';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	previousOwner: TPlayerId;
}

const NEURAL_PARASITE_DAMAGE = 50;
const NEURAL_PARASITE_DAMAGE_SCALE = 0.1;
const NEURAL_PARASITE_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.1;

export class NeuralParasiteEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 12005;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Neural Parasite';
	public static readonly description = `Unit takes ${AbilityPowerUIToken}${NEURAL_PARASITE_DAMAGE} HEA Damage every Turn End. For every Hostile (to the invoker) Unit inside this Unit's Scan Range, that Damage is increased by ${biasToPercent(
		NEURAL_PARASITE_DAMAGE_SCALE,
	)}, and the invoker gains ${NEURAL_PARASITE_XP} XP. If the Unit is Organic, it is temporarily controlled by the Invoker`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: NeuralParasiteEffect) => {
			const newState = {
				...effect.state,
				previousOwner: unit.playerId,
			} as IState;
			if (unit.isOrganic && effect.source?.playerId) unit.playerId = effect.source.playerId;
			return newState;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: NeuralParasiteEffect, meta) => {
			if (!unit.location?.mapId) return effect.state;
			const hostileUnitsInScanRange = meta.map
				.getUnitsInRange({
					...unit.location,
					radius: unit.scanRange,
				})
				.filter(
					(u) => !u.isFriendlyToPlayer(effect.source.playerId) && u.getInstanceId() !== unit.getInstanceId(),
				).length;
			if (hostileUnitsInScanRange && effect.static.isUnitTarget(effect.source))
				effect.source.addXp(hostileUnitsInScanRange * NEURAL_PARASITE_XP);
			unit.applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.biological]: NEURAL_PARASITE_DAMAGE * effect.power,
				},
				effect.source,
				{
					factor: 1 + hostileUnitsInScanRange * NEURAL_PARASITE_DAMAGE_SCALE,
				},
			);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: NeuralParasiteEffect, meta) => {
			if (unit.isOrganic) unit.playerId = effect.state.previousOwner;
			return {
				previousOwner: null,
			};
		},
	};
	public priority = 2;
}

export default NeuralParasiteEffect;

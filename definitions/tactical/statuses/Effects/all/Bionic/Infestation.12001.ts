import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describePercentileModifier = BasicMaths.describePercentileModifier;

interface IState {}

const INFESTATION_DODGE = 0.05;
const INGFESTATION_DODGE_MIN = -2;
const INFESTATION_DODGE_SCALE = 0.02;
const INFESTATION_DAMAGE = 15;
const INFESTATION_DAMAGE_SCALE = 25;
const INFESTATION_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;

export class InfestationEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 12001;
	public static readonly negative = true;
	public static readonly caption = 'Infestation';
	public static readonly description = `Unit loses ${numericAPScaling(
		INFESTATION_DODGE_SCALE,
		AbilityPowerUIToken,
		INFESTATION_DODGE,
		true,
	)} Dodge every Turn End, down to a minimum of ${describePercentileModifier({
		bias: INGFESTATION_DODGE_MIN,
	})}. Target also loses Reactive Defense and Resilient Defense. Every Perk removed deals ${numericAPScaling(
		INFESTATION_DAMAGE_SCALE,
		AbilityPowerUIToken,
		INFESTATION_DAMAGE,
	)} BIO Damage to the Unit and grants ${INFESTATION_XP} XP to the Invoker.`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: InfestationEffect, meta?) => {
			unit.applyChassisModifier({
				dodge: {
					default: {
						bias: -INFESTATION_DODGE - INFESTATION_DODGE_SCALE * effect.power,
						min: INGFESTATION_DODGE_MIN,
					},
				},
			});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: InfestationEffect, meta?) => {
			const perksToRemove = [UnitDefense.defenseFlags.resilient, UnitDefense.defenseFlags.reactive];
			const perks = perksToRemove.filter((p) => unit.hasDefensePerks(p));
			if (perks.length) {
				unit.removeDefensePerks(...perksToRemove);
				perks.forEach(() =>
					unit.applyAttackUnit(
						{
							delivery: UnitAttack.deliveryType.immediate,
							[Damage.damageType.biological]:
								INFESTATION_DAMAGE + effect.power * INFESTATION_DAMAGE_SCALE,
						},
						effect.source,
					),
				);
				if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(INFESTATION_XP * perks.length);
			}
			return effect.state;
		},
	};
	public priority = 2;
}

export default InfestationEffect;

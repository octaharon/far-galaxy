import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import Damage from '../../../../damage/damage';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	affected: boolean;
}

const ENZYME_INHIBITORS_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;
const ENZYME_INHIBITORS_PROTECTION_PENALTY = 0.12;
const ENZYME_INHIBITORS_PROTECTION_PENALTY_SCALE = 0.03;

export class EnzymeInhibitorsEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 12004;
	public static readonly negative = true;
	public static readonly baseDuration = 4;
	public static readonly caption = 'Enzyme Inhibitors';
	public static readonly description = `Temporarily Remove Replenishing Defense, and grant the invoker ${ENZYME_INHIBITORS_XP} XP when doing so. For the Duration, Unit permanently loses ${numericAPScaling(
		ENZYME_INHIBITORS_PROTECTION_PENALTY_SCALE,
		AbilityPowerUIToken,
		ENZYME_INHIBITORS_PROTECTION_PENALTY,
		true,
	)} of BIO Armor Protection with every Attack taken`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: EnzymeInhibitorsEffect, meta?) => {
			const newState = {
				...effect.state,
				affected: unit.defense[UnitDefense.defenseFlags.replenishing],
			} as IState;
			unit.removeDefensePerks(UnitDefense.defenseFlags.replenishing);
			return newState;
		},
		[EffectEvents.TUnitEffectEvents.unitHit]: (unit, effect: EnzymeInhibitorsEffect, meta?) => {
			unit.applyArmorModifier({
				[Damage.damageType.biological]: {
					factor:
						1 +
						ENZYME_INHIBITORS_PROTECTION_PENALTY +
						ENZYME_INHIBITORS_PROTECTION_PENALTY_SCALE * effect.power,
				},
			});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: EnzymeInhibitorsEffect, meta?) => {
			if (effect.state.affected) unit.addDefensePerks(UnitDefense.defenseFlags.replenishing);
			return {
				affected: false,
			};
		},
	};
	public priority = 2;
}

export default EnzymeInhibitorsEffect;

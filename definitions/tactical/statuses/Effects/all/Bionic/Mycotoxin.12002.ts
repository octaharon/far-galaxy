import { biasToPercent, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import { ATTACK_PRIORITIES } from '../../../../damage/constants';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	currentHitPoints: number;
	currentDamage: number;
	speedReduction: number;
}

const MYCOTOXIN_DAMAGE = 50;
const MYCOTOXIN_DAMAGE_INCREASE = 0.4;
const MYCOTOXIN_SPEED_DEBUFF = 0.5;
const MYCOTOXIN_SPEED_DEBUFF_SCALE = 0.05;
const MYCOTOXIN_XP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;
const MycotoxinDamage: UnitAttack.TAttackUnit = {
	delivery: UnitAttack.deliveryType.immediate,
	[UnitAttack.attackFlags.AoE]: false,
	[UnitAttack.attackFlags.DoT]: false,
	[UnitAttack.attackFlags.Deferred]: false,
};

export class MycotoxinEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 12002;
	public static readonly negative = true;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Mycotoxin';
	public static readonly description = `Unit's Speed is reduced by ${numericAPScaling(
		-MYCOTOXIN_SPEED_DEBUFF_SCALE,
		AbilityPowerUIToken,
		-MYCOTOXIN_SPEED_DEBUFF,
		true,
	)} for Effect Duration, and it takes ${AbilityPowerUIToken}${MYCOTOXIN_DAMAGE} BIO Damage every Turn End. If the Unit restores any Hit Points since the last time, the Damage done increases by ${biasToPercent(
		MYCOTOXIN_DAMAGE_INCREASE,
	)}, and the invoker gains ${MYCOTOXIN_XP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: MycotoxinEffect) => {
			const speedReduction =
				Math.min(1, MYCOTOXIN_SPEED_DEBUFF + MYCOTOXIN_SPEED_DEBUFF_SCALE * effect.power) * unit.speed;
			const currentHitPoints = unit.currentHitPoints;
			unit.applyChassisModifier({
				speed: {
					bias: -speedReduction,
					minGuard: 0,
					min: 0,
				},
			});
			return {
				speedReduction,
				currentHitPoints,
				currentDamage: MYCOTOXIN_DAMAGE * effect.power,
			};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: MycotoxinEffect) => {
			unit.applyChassisModifier({
				speed: {
					bias: effect.state.speedReduction,
					min: 0,
				},
			});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: MycotoxinEffect) => {
			let { currentDamage } = effect.state;
			if (effect.state.currentHitPoints < unit.currentHitPoints) {
				currentDamage *= 1 + MYCOTOXIN_DAMAGE_INCREASE;
				if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(MYCOTOXIN_XP);
			}
			unit.applyAttackUnit(
				{
					...MycotoxinDamage,
					[Damage.damageType.biological]: currentDamage,
				},
				effect.source,
			);
			return {
				...effect.state,
				currentHitPoints: unit.currentHitPoints,
			};
		},
	};
	public priority = ATTACK_PRIORITIES.HIGHEST;
}

export default MycotoxinEffect;

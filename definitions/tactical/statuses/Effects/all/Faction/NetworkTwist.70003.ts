import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitDefense from '../../../../damage/defense';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const NetworkTwistDodge = 0.15;
const NetworkTwistScale = 0.05;

export class NetworkTwistEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 70003;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Network Twist';
	public static readonly description = `All Hostile Units in area lose HULL Module, Evasion Module and take ${numericAPScaling(
		NetworkTwistScale,
		AbilityPowerUIToken,
		NetworkTwistDodge,
		true,
	)} penalty to Dodge`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (zone: IMapCell, effect: NetworkTwistEffect, meta) => {
				const units = (meta?.map.getUnitsInRange(zone) || []).filter(
					(u) => u.doesBelongToPlayer(effect.source?.playerId) || !u.isImmuneToNegativeEffects,
				);
				units.forEach((u) =>
					u.applyChassisModifier({
						dodge: {
							default: {
								bias: -NetworkTwistDodge - NetworkTwistScale * effect.power,
							},
						},
						defenseFlags: {
							[UnitDefense.defenseFlags.evasion]: false,
							[UnitDefense.defenseFlags.hull]: false,
						},
					}),
				);
				return {};
			},
		};
	public priority = 0;
}

export default NetworkTwistEffect;

import UnitAttack from '../../../../damage/attack';
import AttackManager from '../../../../damage/AttackManager';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const THETA_PROJECTION_DAMAGE: Damage.IDamageFactor = {
	max: 100,
	min: 40,
	distribution: 'Minimal',
};

export const THETA_PROJECTION_ENERGY_SUPPLY = 25;
export const THETA_PROJECTION_RADIUS = 3;

export class ThetaProjectionEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 70010;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Theta Projection';
	public static readonly description = `All Units in ${THETA_PROJECTION_RADIUS} Radius take ${AbilityPowerUIToken}${THETA_PROJECTION_DAMAGE.min} - ${AbilityPowerUIToken}${THETA_PROJECTION_DAMAGE.max} EMG Damage as Beam. For every Kill the Player restores ${THETA_PROJECTION_ENERGY_SUPPLY} Energy at Base Storage`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (zone: IMapCell, effect: ThetaProjectionEffect, meta) => {
				meta?.map?.getUnitsInRange(zone).forEach((u) => {
					const t = u.applyAttackUnit(
						AttackManager.getAttackDamage({
							delivery: UnitAttack.deliveryType.beam,
							[Damage.damageType.magnetic]: THETA_PROJECTION_DAMAGE,
						}),
						effect.source,
						{
							factor: effect.power,
						},
					);
					// unit Killed
					if (!u || u.currentHitPoints <= 0) {
						meta?.bases?.[effect?.source?.playerId]?.addEnergy(THETA_PROJECTION_ENERGY_SUPPLY);
					}
				});
				return null;
			},
		};
	public priority = 0;
}

export default ThetaProjectionEffect;

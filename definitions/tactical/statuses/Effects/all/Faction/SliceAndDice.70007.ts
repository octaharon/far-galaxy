import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const SLICE_AND_DICE_DAMAGE = 30;
export const SLICE_AND_DICE_RADIUS = 6;

export class SliceAndDiceEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 70007;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Slice And Dice';
	public static readonly description = `All Units in Radius ${SLICE_AND_DICE_RADIUS} take ${AbilityPowerUIToken}${SLICE_AND_DICE_DAMAGE} BIO Damage`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (zone: IMapCell, effect, meta) => {
				meta?.map
					?.getUnitsInRange(zone)
					.filter((u) => !u.isImmuneToNegativeEffects)
					.forEach((u) =>
						u.applyAttackUnit(
							{
								delivery: UnitAttack.deliveryType.immediate,
								[Damage.damageType.biological]: SLICE_AND_DICE_DAMAGE,
							},
							effect.source,
							{
								factor: effect.power,
							},
						),
					);
				return null;
			},
		};
	public priority = 0;
}

export default SliceAndDiceEffect;

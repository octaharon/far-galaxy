import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const CLUSTER_WIPE_DAMAGE = 50;

export class ClusterWipeEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 70004;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Cluster Wipe';
	public static readonly description = `A Unit takes ${AbilityPowerUIToken}${CLUSTER_WIPE_DAMAGE} EMG Damage for each Status Effect it bears, including this one`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: ClusterWipeEffect) => {
			unit.applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.magnetic]: CLUSTER_WIPE_DAMAGE * Math.max(1, (unit.effects || []).length),
				},
				effect.source,
				{
					factor: effect.power,
				},
			);
			return null;
		},
	};
	public priority = 0;
}

export default ClusterWipeEffect;

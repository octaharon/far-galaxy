import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	previousScanRange: number;
	damageFactor: number;
}

export const INDOCTRINATION_DAMAGE_DEBUFF = 0.35;
export const INDOCTRINATION_DAMAGE_DEBUFF_SCALE = 0.05;

export class IndoctrinationEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 70002;
	public static readonly negative = true;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Indoctrination';
	public static readonly description = `Temporarily reduces Unit Attack Damage by ${numericAPScaling(
		INDOCTRINATION_DAMAGE_DEBUFF_SCALE,
		AbilityPowerUIToken,
		INDOCTRINATION_DAMAGE_DEBUFF,
		true,
	)} and nullifies its Scan Range`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect) => {
			const state: IState = {
				previousScanRange: unit.scanRange,
				damageFactor: 1 - INDOCTRINATION_DAMAGE_DEBUFF - INDOCTRINATION_DAMAGE_DEBUFF_SCALE * effect.power,
			};
			unit.scanRange = 0;
			unit.applyWeaponModifier({
				default: {
					damageModifier: {
						factor: state.damageFactor,
					},
				},
			});
			return state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: IndoctrinationEffect) => {
			unit.scanRange += effect.state.previousScanRange;
			unit.applyWeaponModifier({
				default: {
					damageModifier: {
						factor: 1 / effect.state.damageFactor,
					},
				},
			});
			return null;
		},
	};
	public priority = 0;
}

export default IndoctrinationEffect;

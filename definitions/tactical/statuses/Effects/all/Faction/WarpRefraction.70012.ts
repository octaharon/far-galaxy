import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import Series from '../../../../../maths/Series';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import DamageManager from '../../../../damage/DamageManager';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const WarpRefractionReturnRate = 0.45;
const WarpRefractionReturnScale = 0.15;

export class WarpRefractionEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 70012;
	public static readonly negative = false;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Warp Refraction';
	public static readonly description = `This Unit returns ${numericAPScaling(
		WarpRefractionReturnScale,
		AbilityPowerUIToken,
		WarpRefractionReturnRate,
		true,
	)} of incoming Attack Damage to the attacker as WRP Damage`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.unitAttacked]: (unit, effect: WarpRefractionEffect, meta) => {
			const damageTaken =
				(DamageManager.getTotalDamageValue(meta?.result?.throughDamage) || 0) +
				(Series.sum(meta?.result?.shieldReduction) || 0);
			if (WarpRefractionEffect.isUnitTarget(meta.source)) {
				meta.source.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[Damage.damageType.warp]:
							damageTaken * (WarpRefractionReturnRate + WarpRefractionReturnScale * effect.power),
					},
					effect.source,
				);
			}
			return null;
		},
	};
	public priority = 0;
}

export default WarpRefractionEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import HighHopesEffect, {
	HIGH_HOPES_HEALTH_RESTORE_PERCENTAGE,
	HIGH_HOPES_HEALTH_RESTORE_PERCENTAGE_SCALE,
} from './HighHopes.70001';

interface IState {}

export class HighHopesMapEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 70101;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'High Hopes';
	public static readonly description = `Restores ${numericAPScaling(
		HIGH_HOPES_HEALTH_RESTORE_PERCENTAGE_SCALE,
		AbilityPowerUIToken,
		HIGH_HOPES_HEALTH_RESTORE_PERCENTAGE,
		true,
	)} of Max Hit Points to all Allied Units in Radius 5 and grant them +${AbilityPowerUIToken}10 Armor Protection temporarily`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (zone: IMapCell, effect: HighHopesMapEffect, meta) => {
				const units = (meta?.map.getUnitsInRange(zone) || []).filter(
					(u) => effect.source.doesBelongToPlayer(u.playerId) && !u.isImmuneToEffects,
				);
				units.forEach((u) =>
					u.applyEffect(
						{
							effectId: HighHopesEffect.id,
						},
						effect.source,
					),
				);
				return null;
			},
		};
	public priority = 0;
}

export default HighHopesMapEffect;

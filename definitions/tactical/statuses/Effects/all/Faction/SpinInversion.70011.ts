import _ from 'underscore';
import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import UnitDefense from '../../../../damage/defense';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const SPIN_INVERSION_DAMAGE = 35;

export class SpinInversionEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 70011;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Spin Inversion';
	public static readonly description = `Every Unit in Effect Radius takes ${numericAPScaling(
		SPIN_INVERSION_DAMAGE,
		AbilityPowerUIToken,
	)} ANH Damage for every of the following Perks it has: Warp Defense, Tachyon Defense, Unstoppable, and one of the Perks is removed randomly`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (zone: IMapCell, effect, meta) => {
				const units = meta?.map.getUnitsInRange(zone) || [];
				units
					.filter((u) => !u.isImmuneToEffects)
					.forEach((u) => {
						const perks = [
							UnitDefense.defenseFlags.unstoppable,
							UnitDefense.defenseFlags.warp,
							UnitDefense.defenseFlags.tachyon,
						].filter((p) => u.hasDefensePerks(p));
						if (perks.length) {
							u.applyAttackUnit(
								{
									delivery: UnitAttack.deliveryType.immediate,
									[Damage.damageType.antimatter]: SPIN_INVERSION_DAMAGE * effect.power * perks.length,
								},
								effect.source,
							);
							u.removeDefensePerks(_.sample(perks));
						}
					});
				return null;
			},
		};
	public priority = 0;
}

export default SpinInversionEffect;

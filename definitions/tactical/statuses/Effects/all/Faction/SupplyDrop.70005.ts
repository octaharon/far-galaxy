import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const SUPPLY_DROP_SHIELD_RECHARGE = 0.35;
export const SUPPLY_DROP_ATTACK_RATE = 0.25;
export const SUPPLY_DROP_ATTACK_SCALE = 0.1;

export class SupplyDropEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 70005;
	public static readonly negative = false;
	public static readonly baseDuration = 1;
	public static readonly caption = 'Supply Drop';
	public static readonly description = `Unit gets a temporary bonus of ${numericAPScaling(
		SUPPLY_DROP_ATTACK_SCALE,
		AbilityPowerUIToken,
		SUPPLY_DROP_ATTACK_RATE,
		true,
	)} Attack Rate and restores ${numericAPScaling(
		SUPPLY_DROP_SHIELD_RECHARGE,
		AbilityPowerUIToken,
	)} of total Shield Capacity immediately`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect) => {
			unit.addShields(unit.totalShields * SUPPLY_DROP_SHIELD_RECHARGE * effect.power);
			unit.applyWeaponModifier({
				default: {
					baseRate: {
						factor: 1 + SUPPLY_DROP_ATTACK_RATE + SUPPLY_DROP_ATTACK_SCALE * effect.power,
					},
				},
			});

			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SupplyDropEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseRate: {
						factor: 1 / (1 + SUPPLY_DROP_ATTACK_RATE + SUPPLY_DROP_ATTACK_SCALE * effect.power),
					},
				},
			});
			return null;
		},
	};
	public priority = 0;
}

export default SupplyDropEffect;

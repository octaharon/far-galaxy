import { describePercentage, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { TBaseBuildingType } from '../../../../../orbital/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const PROVISIONAL_DEFENSE_HP_RESTORATION = 0.5;
export const PROVISIONAL_DEFENSE_HP_RESTORATION_SCALE = 0.1;
export const PROVISIONAL_DEFENSE_PRODUCTION_POINTS = 10;
export const PROVISIONAL_DEFENSE_PRODUCTION_POINTS_SCALE = 5;
export const PROVISIONAL_DEFENSE_XP_RATE = 0.1;

export class ProvisionalDefenseEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 70009;
	public static readonly negative = false;
	public static readonly baseDuration = 1;
	public static readonly caption = 'Provisional Defense';
	public static readonly description = `Unit restores ${numericAPScaling(
		PROVISIONAL_DEFENSE_HP_RESTORATION_SCALE,
		AbilityPowerUIToken,
		PROVISIONAL_DEFENSE_HP_RESTORATION,
		true,
	)} of its Max Hit Points at Turn End, with ${describePercentage(
		PROVISIONAL_DEFENSE_XP_RATE,
	)} of excess, if any, being converted to XP. If the Unit is destroyed this Turn, the owning Player gets ${numericAPScaling(
		PROVISIONAL_DEFENSE_PRODUCTION_POINTS_SCALE,
		AbilityPowerUIToken,
		PROVISIONAL_DEFENSE_PRODUCTION_POINTS,
	)} Production Points at every working Factory`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.unitKilled]: (unit, effect: ProvisionalDefenseEffect, meta) => {
			const base = meta?.bases?.[effect?.source?.playerId];
			if (!base) return null;
			base.buildings[TBaseBuildingType.factory]?.forEach((b) => {
				if (b && b.currentProductionSlot && b.productionPrototypes[b.currentProductionSlot])
					b.currentProductionStock +=
						PROVISIONAL_DEFENSE_PRODUCTION_POINTS +
						PROVISIONAL_DEFENSE_PRODUCTION_POINTS_SCALE * effect.power;
			});
			return null;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: ProvisionalDefenseEffect) => {
			const bonusHP =
				unit.maxHitPoints *
				(PROVISIONAL_DEFENSE_HP_RESTORATION + PROVISIONAL_DEFENSE_HP_RESTORATION_SCALE * effect.power);
			const excess = unit.currentHitPoints - unit.maxHitPoints + bonusHP;
			unit.addHp(bonusHP, effect.source);
			if (excess > 0) unit.addXp(excess * PROVISIONAL_DEFENSE_XP_RATE);
			return null;
		},
	};
	public priority = 0;
}

export default ProvisionalDefenseEffect;

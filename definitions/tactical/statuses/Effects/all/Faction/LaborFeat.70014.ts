import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const LaborFeatPercentage = 0.45;
const LaborFeatPercentageScale = 0.15;

export class LaborFeatEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 70014;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Labor Feat';
	public static readonly description = `Produces Repair Points equal to ${numericAPScaling(
		LaborFeatPercentageScale,
		AbilityPowerUIToken,
		LaborFeatPercentage,
		true,
	)} of Base Hit Points. Units at Bay gain +1 Shield Protection permanently`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base, effect: LaborFeatEffect) => {
			base?.addRepair(
				base?.currentEconomy.maxHitPoints * (LaborFeatPercentage + LaborFeatPercentageScale * effect.power),
			);
			(base?.unitsAtBay || []).forEach((u) => {
				u.applyShieldsModifier({
					default: {
						bias: -1,
					},
				});
			});
			return null;
		},
	};
	public priority = 0;
}

export default LaborFeatEffect;

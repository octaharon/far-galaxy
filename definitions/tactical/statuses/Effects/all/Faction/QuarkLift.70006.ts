import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const QUARK_LIFT_MATERIAL_THRESHOLD = 50;

export class QuarkLiftEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 70006;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Quark Lift';
	public static readonly description = `Steal ${AbilityPowerUIToken}${QUARK_LIFT_MATERIAL_THRESHOLD} Raw Materials from Base, dealing it 2 WRP Damage per every 1 missing piece`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base, effect: QuarkLiftEffect, meta) => {
			const amount = QUARK_LIFT_MATERIAL_THRESHOLD * effect.power;
			const transferredAmount = Math.min(amount, base?.rawMaterials);
			base.addRawMaterials(-transferredAmount);
			base?.addRawMaterials(transferredAmount);
			if (transferredAmount < amount)
				base.applyAttackUnit({
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.warp]: (amount - transferredAmount) * 2,
				});
			return null;
		},
	};
	public priority = 0;
}

export default QuarkLiftEffect;

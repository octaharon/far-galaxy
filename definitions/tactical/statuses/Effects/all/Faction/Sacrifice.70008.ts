import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const SACRIFICE_DAMAGE_BUFF = 0.3;
export const SACRIFICE_DAMAGE_SCALE = 0.1;
export const SACRIFICE_SPEED_BUFF = 3;

export class SacrificeEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 70008;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Sacrifice';
	public static readonly description = `This Unit temporarily has its Damage increased by ${numericAPScaling(
		SACRIFICE_DAMAGE_SCALE,
		AbilityPowerUIToken,
		SACRIFICE_DAMAGE_BUFF,
		true,
	)} and Speed by ${AbilityPowerUIToken}${SACRIFICE_SPEED_BUFF}. When the effect expires, Unit takes ${AbilityPowerUIToken}100% of its Max Hit Points as ANH Damage, which is halved if the Unit is Massive`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect) => {
			unit.applyChassisModifier({
				speed: {
					bias: SACRIFICE_SPEED_BUFF * effect.power,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
			});
			unit.applyWeaponModifier({
				default: {
					damageModifier: {
						factor: 1 + SACRIFICE_DAMAGE_BUFF + SACRIFICE_DAMAGE_SCALE * effect.power,
					},
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SacrificeEffect) => {
			unit.applyChassisModifier({
				speed: {
					bias: -SACRIFICE_SPEED_BUFF * effect.power,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
			});
			unit.applyWeaponModifier({
				default: {
					damageModifier: {
						factor: 1 / (1 + SACRIFICE_DAMAGE_BUFF + SACRIFICE_DAMAGE_SCALE * effect.power),
					},
				},
			});
			unit.applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.antimatter]: unit.maxHitPoints * effect.power * (unit.isMassive ? 0.5 : 1),
				},
				effect.source,
			);
			return null;
		},
	};
	public priority = 0;
}

export default SacrificeEffect;

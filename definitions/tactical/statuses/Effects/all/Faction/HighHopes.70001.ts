import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	bonusHp?: number;
	bonusArmor?: number;
}

export const HIGH_HOPES_HEALTH_RESTORE_PERCENTAGE = 0.25;
export const HIGH_HOPES_HEALTH_RESTORE_PERCENTAGE_SCALE = 0.1;
export const HIGH_HOPES_ARMOR = 10;

export class HighHopesEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 70001;
	public static readonly negative = false;
	public static readonly baseDuration = 2;
	public static readonly caption = 'High Hopes';
	public static readonly description = `Temporarily has extra ${AbilityPowerUIToken}${HIGH_HOPES_ARMOR} Armor Protection`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: HighHopesEffect) => {
			const state: IState = {
				bonusHp:
					unit.maxHitPoints *
					(HIGH_HOPES_HEALTH_RESTORE_PERCENTAGE + effect.power * HIGH_HOPES_HEALTH_RESTORE_PERCENTAGE_SCALE),
				bonusArmor: 10 * effect.power,
			};
			unit.addHp(state.bonusHp, effect.source);
			unit.applyArmorModifier({
				default: {
					bias: -state.bonusArmor,
					minGuard: 0,
					min: 0,
				},
			});
			return state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: HighHopesEffect) => {
			unit.applyArmorModifier({
				default: {
					bias: effect.state.bonusArmor ?? 10 * effect.power,
					minGuard: 0,
					min: 0,
				},
			});
			return {};
		},
	};
	public priority = -2;
}

export default HighHopesEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import SupplyDropEffect, {
	SUPPLY_DROP_ATTACK_RATE,
	SUPPLY_DROP_ATTACK_SCALE,
	SUPPLY_DROP_SHIELD_RECHARGE,
} from './SupplyDrop.70005';

interface IState {}

export const SUPPLY_DROP_RADIUS = 3;

export class SupplyDropMapEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 70105;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Supply Drop';
	public static readonly description = `Allied Units in ${SUPPLY_DROP_RADIUS} Radius get a temporary bonus of ${numericAPScaling(
		SUPPLY_DROP_ATTACK_SCALE,
		AbilityPowerUIToken,
		SUPPLY_DROP_ATTACK_RATE,
		true,
	)} Attack Rate and immediately restore ${numericAPScaling(
		SUPPLY_DROP_SHIELD_RECHARGE,
		AbilityPowerUIToken,
		null,
		true,
	)} of total Shield Capacity`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (zone: IMapCell, effect: SupplyDropMapEffect, meta) => {
				const units = (meta?.map.getUnitsInRange(zone) || []).filter(
					(u) => u.doesBelongToPlayer(effect.source?.playerId) && !u.isImmuneToEffects,
				);
				units.forEach((u) =>
					u.applyEffect(
						{
							effectId: SupplyDropEffect.id,
						},
						effect.source,
					),
				);
				return null;
			},
		};
	public priority = 0;
}

export default SupplyDropMapEffect;

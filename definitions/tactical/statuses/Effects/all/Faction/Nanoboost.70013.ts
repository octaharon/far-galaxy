import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { BASE_PRODUCTION_SPEED } from '../../../../../orbital/factories/types';
import { TBaseBuildingType } from '../../../../../orbital/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	bonus?: number;
}

export const NANOBOOST_PRODUCTION_BONUS = 0.2;
export const NANOBOOST_PRODUCTION_BONUS_SCALE = 0.05;

export class NanoboostEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 70013;
	public static readonly negative = false;
	public static readonly baseDuration = 3;
	public static readonly description = `All Factories have a temporary bonus of ${numericAPScaling(
		NANOBOOST_PRODUCTION_BONUS_SCALE,
		AbilityPowerUIToken,
		NANOBOOST_PRODUCTION_BONUS,
		true,
	)} to Production Speed`;
	public static readonly caption = 'Nanoboost';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base, effect) => {
			const state = {
				bonus:
					BASE_PRODUCTION_SPEED *
					(NANOBOOST_PRODUCTION_BONUS + NANOBOOST_PRODUCTION_BONUS_SCALE * effect.power),
			} as IState;
			base?.buildings[TBaseBuildingType.factory].forEach((f) => (f.productionSpeed += state.bonus));
			return state;
		},
		[EffectEvents.TPlayerEffectEvents.expire]: (base, effect: NanoboostEffect) => {
			base?.buildings[TBaseBuildingType.factory].forEach((f) => (f.productionSpeed -= effect.state.bonus));
			return { bonus: 0 };
		},
	};
	public priority = 0;
}

export default NanoboostEffect;

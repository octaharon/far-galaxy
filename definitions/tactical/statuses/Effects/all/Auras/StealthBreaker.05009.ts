import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import Units from '../../../../units/types';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const StealthBreakerXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;
const handler = (unit: Units.IUnit, effect: StealthBreakerEffect) => {
	if (unit.status.cloaked && !unit.isFriendlyToPlayer(effect.source.playerId)) {
		if (effect.static.isUnitTarget(effect.source)) effect.source.addXp(StealthBreakerXP);
		unit.status.cloaked = false;
	}
	return {};
};

export class StealthBreakerEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5009;
	public static readonly negative = true;
	public static readonly unremovable = true;
	public static readonly caption = 'Stealth Breaker';
	public static readonly description =
		'Nearby Higgs Field Generator breaks Cloak, if its bearer is Hostile to this Unit';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: handler,
		[EffectEvents.TUnitEffectEvents.move]: handler,
		[EffectEvents.TUnitEffectEvents.turnEnd]: handler,
		[EffectEvents.TUnitEffectEvents.turnStart]: handler,
		[EffectEvents.TUnitEffectEvents.actionPhase]: handler,
	};
	public priority = 0;
}

export default StealthBreakerEffect;

import UnitAttack from '../../../../damage/attack';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const DroneJammerDodge = 0.75;
export const DroneJammerDodgeScale = 0.25;

export class DroneJammerStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5003;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Drone Jammer';
	public static readonly description = 'This Unit has increased Dodge vs Drone';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DroneJammerStatusEffect, meta) => {
			unit.applyChassisModifier({
				dodge: {
					[UnitAttack.deliveryType.drone]: { bias: DroneJammerDodge + DroneJammerDodgeScale * effect.power },
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: DroneJammerStatusEffect) => {
			unit.applyChassisModifier({
				dodge: {
					[UnitAttack.deliveryType.drone]: {
						bias: -(DroneJammerDodge + DroneJammerDodgeScale * effect.power),
					},
				},
			});
			return {};
		},
	};
	public priority = 0;
}

export default DroneJammerStatusEffect;

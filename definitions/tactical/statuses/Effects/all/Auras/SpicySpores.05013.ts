import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const SpicySporesXP = DEFAULT_PLAYER_RESEARCH_XP * 0.5;
export const SpicySporesDamage = 10;
export const SpicySporesDamageScale = 5;

export class SpicySporesStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5013;
	public static readonly negative = true;
	public static readonly unremovable = true;
	public static readonly caption = 'Spicy Spores';
	public static readonly description = `This Unit gains extra ${SpicySporesXP} XP for Kills, and at Turn End they take ${numericAPScaling(
		SpicySporesDamageScale,
		AbilityPowerUIToken,
		SpicySporesDamage,
	)} BIO Damage as a Cloud Attack`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: SpicySporesStatusEffect, meta) => {
			unit.applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.cloud,
					[Damage.damageType.biological]: SpicySporesDamage + SpicySporesDamageScale * effect.power,
				},
				effect.source,
			);
			return {};
		},
		[EffectEvents.TUnitEffectEvents.unitKill]: (unit, effect: SpicySporesStatusEffect, meta) => {
			unit.addXp(SpicySporesXP);
			return {};
		},
	};
	public priority = 0;
}

export default SpicySporesStatusEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const LeadershipDamageBonus = 0.1;
export const LeadershipDamageBonusScale = 0.05;

export class LeadershipEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5006;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Leader In Sight';
	public static readonly description = `This Unit is led by an exemplar combatant and deals ${numericAPScaling(
		LeadershipDamageBonusScale,
		AbilityPowerUIToken,
		LeadershipDamageBonus,
		true,
	)} Damage with Weapons while it's around`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: LeadershipEffect) => {
			unit.applyWeaponModifier({
				default: {
					damageModifier: {
						factor: 1 + LeadershipDamageBonus + LeadershipDamageBonusScale * effect.power,
					},
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: LeadershipEffect) => {
			unit.applyWeaponModifier({
				default: {
					damageModifier: {
						factor: 1 / (1 + LeadershipDamageBonus + LeadershipDamageBonusScale * effect.power),
					},
				},
			});
			return {};
		},
	};
	public priority = 0;
}

export default LeadershipEffect;

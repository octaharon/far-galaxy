import { biasToPercent } from '../../../../../../src/utils/wellRounded';
import Series from '../../../../../maths/Series';
import UnitAttack from '../../../../damage/attack';
import DamageManager from '../../../../damage/DamageManager';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const DecoyClusterDamageReductionBase = 0.25;
export const DecoyClusterDamageXPScale = 0.05;
export const DecoyClusterRange = 5;

export class DecoyClusterEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5011;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Decoy Cluster';
	public static readonly description = `${biasToPercent(
		DecoyClusterDamageReductionBase,
	)} of Hit Point Damage this Unit takes is transferred to the nearest Sentinel`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.unitDamaged]: (unit, effect: DecoyClusterEffect, meta) => {
			const dmg = DamageManager.getTotalDamageValue(meta.damage);
			unit.addHp(dmg * 0.25, effect.source);
			if (DecoyClusterEffect.isUnitTarget(effect.source)) {
				const result = effect.source.applyAttackUnit(
					{ ...meta.damage, delivery: UnitAttack.deliveryType.immediate },
					meta.source,
					{
						factor: 0.25,
					},
				);
				if (Series.sum(result.shieldReduction) + DamageManager.getTotalDamageValue(result.throughDamage) <= 0) {
					effect.source.addXp(dmg * DecoyClusterDamageXPScale * effect.power);
				}
			}
			return {};
		},
	};
	public priority = 0;
}

export default DecoyClusterEffect;

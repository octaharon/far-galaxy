import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitChassis from '../../../../../technologies/types/chassis';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const SermonAccuracyBonus = 0.2;
export const SermonAccuracyBonusScale = 0.1;
export const SermonHealthBonus = 0.25;

export class SermonStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5002;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Sermon';
	public static readonly description = `Unit has ${numericAPScaling(
		SermonAccuracyBonusScale,
		AbilityPowerUIToken,
		SermonAccuracyBonus,
		true,
	)} Hit Chance. Also, it restores ${numericAPScaling(
		SermonHealthBonus,
		AbilityPowerUIToken,
		null,
		true,
	)} of Max Hit Points at Turn End, if it's Organic`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SermonStatusEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: SermonAccuracyBonus + SermonAccuracyBonusScale * effect.power,
					},
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SermonStatusEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: -(SermonAccuracyBonus + SermonAccuracyBonusScale * effect.power),
					},
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect) => {
			if (unit.isOrganic) unit.addHp(unit.maxHitPoints * (effect.power * SermonHealthBonus), effect.source);
			return {};
		},
	};
	public priority = 0;
}

export default SermonStatusEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import Vectors from '../../../../../maths/Vectors';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Units from '../../../../units/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describePercentileModifier = BasicMaths.describePercentileModifier;

interface IState {
	scanRangeReduction: number;
}

const condition = (t: Units.IUnit) =>
	[UnitChassis.movementType.hover, UnitChassis.movementType.air].includes(t.movement);

export const CloudOfBlades_DamagePerDistanceUnit = 40;
export const CloudOfBlades_DodgeBonus = 0.5;
export const CloudOfBlades_ScanRangePenalty = 1;
export const CloudOfBlades_ScanRangePenaltyScale = 0.5;

export class BladeCloudEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5007;
	public static readonly negative = true;
	public static readonly unremovable = true;
	public static readonly caption = 'Cloud Of Blades';
	public static readonly description = `Moving inside the cloud drains up to ${numericAPScaling(
		CloudOfBlades_DamagePerDistanceUnit,
		AbilityPowerUIToken,
	)} Shield Capacity from Hover and Airborne Units for every 1 DU they travel, but they gain ${describePercentileModifier(
		{ bias: 0.5 },
	)} Dodge vs Aerial. Ground and Naval Units have their Scan Range reduced by ${numericAPScaling(
		CloudOfBlades_ScanRangePenaltyScale,
		AbilityPowerUIToken,
		CloudOfBlades_ScanRangePenalty,
	)}`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: BladeCloudEffect) => {
			let scanRangeReduction = 0;
			if (condition(unit))
				unit.applyChassisModifier({
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: CloudOfBlades_DodgeBonus,
						},
					},
				});
			else {
				scanRangeReduction = Math.min(
					unit.scanRange,
					CloudOfBlades_ScanRangePenalty + CloudOfBlades_ScanRangePenaltyScale * effect.power,
				);
				unit.applyChassisModifier({
					scanRange: {
						bias: -scanRangeReduction,
					},
				});
			}
			return { scanRangeReduction };
		},
		[EffectEvents.TUnitEffectEvents.move]: (unit, effect: BladeCloudEffect, meta) => {
			const distance = Vectors.module()(Vectors.add(meta.from, Vectors.inverse(meta.to)));
			const damage = CloudOfBlades_DamagePerDistanceUnit * distance * effect.power;
			if (condition(unit)) unit.addShields(-damage, -1);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: BladeCloudEffect) => {
			if (condition(unit))
				unit.applyChassisModifier({
					dodge: {
						[UnitAttack.deliveryType.aerial]: {
							bias: -CloudOfBlades_DodgeBonus,
						},
					},
				});
			else if (effect.state.scanRangeReduction)
				unit.applyChassisModifier({
					scanRange: {
						bias: effect.state.scanRangeReduction,
					},
				});
			return effect.state;
		},
	};
	public priority = 0;
}

export default BladeCloudEffect;

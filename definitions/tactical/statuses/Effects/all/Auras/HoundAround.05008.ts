import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describeModifier = BasicMaths.describeModifier;

interface IState {
	scanRangeBonus: number;
}

export const HoundScanRangeBonus = 0.15;
export const HoundScanRangeScale = 0.05;
export const HoundEffectPenetrationBonus = 2;

export class HoundAroundEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5008;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Hound Around';
	public static readonly description = `A communication with a highly proficient, well-trained predator yields additional ${numericAPScaling(
		HoundScanRangeScale,
		AbilityPowerUIToken,
		HoundScanRangeBonus,
		true,
	)} Scan Range and ${describeModifier({ bias: HoundEffectPenetrationBonus })} Effect Penetration`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: HoundAroundEffect, meta) => {
			const scanRangeBonus = unit.scanRange * (HoundScanRangeBonus + HoundScanRangeScale * effect.power);
			unit.applyEffectModifier({
				abilityDurationModifier: [
					{
						bias: HoundEffectPenetrationBonus,
					},
				],
			}).applyChassisModifier({
				scanRange: { bias: scanRangeBonus },
			});
			return { scanRangeBonus };
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: HoundAroundEffect) => {
			unit.applyEffectModifier({
				abilityDurationModifier: [
					{
						bias: -HoundEffectPenetrationBonus,
					},
				],
			}).applyChassisModifier({
				scanRange: { bias: -effect.state.scanRangeBonus, min: 0 },
			});
			return { scanRangeBonus: 0 };
		},
	};
	public priority = 0;
}

export default HoundAroundEffect;

import { biasToPercent, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}
export const DefenseNetworkShieldBonusBase = 0.1;
export const DefenseNetworkShieldBonusScale = 0.05;
export const DefenseNetworkShieldRestore = 0.2;

export class DefenseNetworkEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5005;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Shield Network';
	public static readonly description = `This Unit gains ${numericAPScaling(
		DefenseNetworkShieldBonusScale,
		AbilityPowerUIToken,
		DefenseNetworkShieldBonusBase,
		true,
	)} Shield Protection and restores ${biasToPercent(
		DefenseNetworkShieldRestore,
	)} of Shield Capacity at the end of Turn`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DefenseNetworkEffect) => {
			unit.applyShieldsModifier({
				default: {
					factor: 1 - (DefenseNetworkShieldBonusBase + DefenseNetworkShieldBonusScale * effect.power),
					maxGuard: 0,
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: DefenseNetworkEffect) => {
			unit.applyShieldsModifier({
				default: {
					factor: 1 / (1 - (DefenseNetworkShieldBonusBase + DefenseNetworkShieldBonusScale * effect.power)),
					maxGuard: 0,
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit) => {
			unit.addShields(unit.totalShields * DefenseNetworkShieldRestore);
			return {};
		},
	};
	public priority = 0;
}

export default DefenseNetworkEffect;

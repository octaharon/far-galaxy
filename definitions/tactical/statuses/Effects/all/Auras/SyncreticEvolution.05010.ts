import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const SyncreticEvolutionShieldBonus = 5;
export const SyncreticEvolutionXpRatio = 0.25;
export const SyncreticEvolutionXpScale = 0.05;

export class SyncreticEvolutionEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5010;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Syncretic Evolution';
	public static readonly description = `Transfers ${numericAPScaling(
		SyncreticEvolutionXpScale,
		AbilityPowerUIToken,
		SyncreticEvolutionXpRatio,
		true,
	)} of received XP to the Aura source. Every Turn End restores ${SyncreticEvolutionShieldBonus} Shield Capacity to both Units`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect) => {
			if (effect.static.isUnitTarget(effect.source)) {
				effect.source.addShields(SyncreticEvolutionShieldBonus);
			}
			unit.addShields(SyncreticEvolutionShieldBonus);
			return {};
		},
		[EffectEvents.TUnitEffectEvents.xpGained]: (unit, effect, meta) => {
			if (effect.static.isUnitTarget(effect.source)) {
				effect.source.addXp(meta.xp * (SyncreticEvolutionXpRatio + SyncreticEvolutionXpScale * effect.power));
			}
			return {};
		},
	};
	public priority = 0;
}

export default SyncreticEvolutionEffect;

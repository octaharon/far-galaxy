import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describeModifier = BasicMaths.describeModifier;

interface IState {}

export const DefenderOfFaithProtectionBonus = 2;
export const DefenderOfFaithDamageBonus = 0.04;
export const DefenderOfFaithXP = DEFAULT_PLAYER_RESEARCH_XP * 0.1;

export class DefenderOfFaithStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5004;
	public static readonly negative = false;
	public static readonly unremovable = true;
	public static readonly caption = 'Defender Of Faith';
	public static readonly description = `This Unit provides ${describeModifier({
		bias: DefenderOfFaithProtectionBonus,
	})} Armor Protection and ${numericAPScaling(
		DefenderOfFaithDamageBonus,
		AbilityPowerUIToken,
		null,
		true,
	)} Damage to the Defender Of Faith`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: DefenderOfFaithStatusEffect) => {
			if (effect.static.isUnitTarget(effect.source) && !effect.source.isFriendlyToPlayer(unit.playerId))
				effect.source.addXp(DefenderOfFaithXP);
			return {};
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DefenderOfFaithStatusEffect) => {
			if (effect.static.isUnitTarget(effect.source)) {
				effect.source.applyArmorModifier({
					default: {
						bias: -DefenderOfFaithProtectionBonus,
						maxGuard: 0,
					},
				});
				effect.source.applyWeaponModifier({
					default: {
						damageModifier: {
							factor: 1 + effect.power * DefenderOfFaithDamageBonus,
						},
					},
				});
			}
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: DefenderOfFaithStatusEffect) => {
			if (effect.static.isUnitTarget(effect.source)) {
				effect.source.applyArmorModifier({
					default: {
						bias: DefenderOfFaithProtectionBonus,
						maxGuard: 0,
						max: 0,
					},
				});
				effect.source.applyWeaponModifier({
					default: {
						damageModifier: {
							factor: 1 / (1 + effect.power * DefenderOfFaithDamageBonus),
						},
					},
				});
			}
			return {};
		},
	};
	public priority = 0;
}

export default DefenderOfFaithStatusEffect;

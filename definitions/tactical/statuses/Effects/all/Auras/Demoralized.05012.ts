import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describeModifier = BasicMaths.describeModifier;

interface IState {
	rangeReduction?: number[];
}

export const ImposingPresenceHitChancePenalty = 0.25;
export const ImposingPresenceHitChancePenaltyScale = 0.15;
export const ImposingPresenceAttackRangePenalty = 1;

export class DemoralizedStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 5012;
	public static readonly negative = true;
	public static readonly unremovable = true;
	public static readonly caption = 'Demoralized';
	public static readonly description = `This Unit is Demoralized and has its Hit Chance reduced by ${numericAPScaling(
		ImposingPresenceHitChancePenaltyScale,
		AbilityPowerUIToken,
		ImposingPresenceHitChancePenalty,
		true,
	)} and Attack Range by ${describeModifier({ bias: -ImposingPresenceAttackRangePenalty })}`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DemoralizedStatusEffect) => {
			if (unit.doesBelongToPlayer(effect.source?.playerId)) {
				unit.removeEffect(effect.getInstanceId());
				return {};
			}
			const state: IState = {
				rangeReduction: unit.attacks.map((a) => Math.min(a.baseRange, ImposingPresenceAttackRangePenalty)),
			};
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: ImposingPresenceHitChancePenaltyScale * effect.power + ImposingPresenceHitChancePenalty,
					},
				},
			});
			unit.attacks = unit.attacks.map((a, ix) => ({
				...a,
				baseRange: a.baseRange - state.rangeReduction[ix],
			}));
			return state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: DemoralizedStatusEffect) => {
			if (effect.state.rangeReduction?.length) {
				unit.attacks = unit.attacks.map((a, ix) => ({
					...a,
					baseRange: a.baseRange + effect.state.rangeReduction?.[ix] || 0,
				}));
			}
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: ImposingPresenceHitChancePenaltyScale * effect.power + ImposingPresenceHitChancePenalty,
					},
				},
			});
			return {};
		},
	};
	public priority = 0;
}

export default DemoralizedStatusEffect;

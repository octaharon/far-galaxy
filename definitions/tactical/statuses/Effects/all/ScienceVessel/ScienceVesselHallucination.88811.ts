import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const HallucinationDamage = 25;
const HallucinationHitChance = 0.35;
const HallucinationScale = 0.05;

export class SVHallucinationEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 88811;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Hallucination';
	public static readonly description = `Deal ${AbilityPowerUIToken}${HallucinationDamage} HEA Damage to affected Units as Disruptive Attack, and temporarily reduce their Dodge and Hit Chance by ${numericAPScaling(
		HallucinationScale,
		AbilityPowerUIToken,
		HallucinationHitChance,
		true,
	)}. When expires, deals the same Disruptive Attack again`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SVHallucinationEffect, meta?) => {
			if (unit.doesBelongToPlayer(effect?.source?.playerId)) return {};
			const bonus = HallucinationHitChance + HallucinationScale * effect.power;
			unit.applyChassisModifier({
				dodge: {
					default: {
						bias: -bonus,
					},
				},
			})
				.applyWeaponModifier({
					default: {
						baseAccuracy: {
							bias: -bonus,
						},
					},
				})
				.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[UnitAttack.attackFlags.Disruptive]: true,
						[Damage.damageType.heat]: HallucinationDamage * effect.power,
					},
					effect.source,
				);
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SVHallucinationEffect) => {
			if (unit.doesBelongToPlayer(effect?.source?.playerId)) return {};
			const bonus = HallucinationHitChance + HallucinationScale * effect.power;
			unit.applyChassisModifier({
				dodge: {
					default: {
						bias: bonus,
					},
				},
			})
				.applyWeaponModifier({
					default: {
						baseAccuracy: {
							bias: bonus,
						},
					},
				})
				.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[UnitAttack.attackFlags.Disruptive]: true,
						[Damage.damageType.heat]: HallucinationDamage * effect.power,
					},
					effect.source,
				);
			return {};
		},
	};
	public priority = 0;
}

export default SVHallucinationEffect;

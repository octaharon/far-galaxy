import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class SVNullifyEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 88804;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Nullify';
	public static readonly description = 'Remove all potentially removable Status Effects on a target Unit';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit) => {
			unit.effects.forEach((e) => unit.removeEffect(e.getInstanceId()));
			return {} as IState;
		},
	};
	public priority = -2;
}

export default SVNullifyEffect;

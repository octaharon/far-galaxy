import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class SVSurveillanceEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 88801;
	public static readonly negative = false;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Surveillance Assistance';
	public static readonly description = 'Player gains +3 Intelligence level';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base) => {
			base.intelligenceLevel += 3;
			return {} as IState;
		},
		[EffectEvents.TPlayerEffectEvents.expire]: (base) => {
			base.intelligenceLevel -= 3;
			return {} as IState;
		},
	};
	public priority = -3;
}

export default SVSurveillanceEffect;

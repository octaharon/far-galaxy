import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	abilityCooldowns: number[];
	hitPoints: number;
}

export class SVTimeTwistEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 88809;
	public static readonly negative = false;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Time twist';
	public static readonly description =
		"Restore this unit's Hit Points and recharge all abilities's cooldowns, but revert the changes after effect expires";
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SVTimeTwistEffect, meta?) => {
			const newState = {
				hitPoints: unit.currentHitPoints,
				abilityCooldowns: unit.kit.getOrderedAbilities().map((a) => a.cooldownLeft),
			} as IState;
			unit.currentHitPoints = unit.maxHitPoints;
			unit.kit.getStableSortedAbilities().forEach((a) => {
				a.cooldownLeft = 0;
			});
			return newState;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SVTimeTwistEffect, meta?) => {
			const newState = {} as IState;
			unit.currentHitPoints = effect.state.hitPoints;
			unit.kit.getStableSortedAbilities().forEach((a, ix) => {
				a.cooldownLeft = effect.state.abilityCooldowns[ix] || 0;
			});
			return newState;
		},
	};
	public priority = -2;
}

export default SVTimeTwistEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const CleansePower = 0.3;
const CleansePowerScale = 0.1;

export class SVCleanseEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 88805;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Cleanse';
	public static readonly description = `Remove all negative non-constant effects on a target unit and restore it ${numericAPScaling(
		CleansePowerScale,
		AbilityPowerUIToken,
		CleansePower,
		true,
	)} of maximum Hit Points`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, eff: SVCleanseEffect) => {
			unit.getStatusEffects().forEach((effect) => {
				if (effect.static.negative && !effect.static.unremovable) unit.removeEffect(effect.getInstanceId());
			});
			unit.addHp(unit.maxHitPoints * (CleansePower + CleansePowerScale * eff.power), eff.source);
			return {} as IState;
		},
	};
	public priority = -2;
}

export default SVCleanseEffect;

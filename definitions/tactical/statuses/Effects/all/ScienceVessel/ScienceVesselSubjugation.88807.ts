import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class SVSubjugateEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 88807;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Subjugation';
	public static readonly description = 'Target unit is Disabled for 2 Turns';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit) => {
			unit.status.disabled = true;
			return {} as IState;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit) => {
			unit.status.disabled = false;
			return {} as IState;
		},
	};
	public priority = -2;
}

export default SVSubjugateEffect;

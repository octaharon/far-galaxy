import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const NanoswarmPower = 50;

export class SVNanoSwarmEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 88803;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Nano Swarm';
	public static readonly description = `Deal ${AbilityPowerUIToken}${NanoswarmPower} HEA Damage as Disruptive Attack against a Hostile Mechanical Unit or an Allied Organic or Robotic Unit. When applied to an Allied Mechanical Unit or to a Hostile Organic or Robotic Unit, restore ${AbilityPowerUIToken}${NanoswarmPower} Hit Points instead`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SVNanoSwarmEffect) => {
			const friendlyUnit = unit.doesBelongToPlayer(effect.source?.playerId);
			const mechanicalUnit =
				!unit.targetFlags[UnitChassis.targetType.robotic] && !unit.targetFlags[UnitChassis.targetType.organic];
			if ((friendlyUnit && mechanicalUnit) || (!friendlyUnit && !mechanicalUnit)) {
				unit.addHp(NanoswarmPower * effect.power, effect.source);
			} else {
				unit.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[UnitAttack.attackFlags.Disruptive]: true,
						[Damage.damageType.heat]: NanoswarmPower * effect.power,
					},
					effect.source,
				);
			}
			return {} as IState;
		},
	};
	public priority = -2;
}

export default SVNanoSwarmEffect;

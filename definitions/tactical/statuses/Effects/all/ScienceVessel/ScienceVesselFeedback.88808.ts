import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const FEEDBACK_MULTIPLIER = 0.35;
const FEEDBACK_SCALE = 0.15;

export class SVFeedbackEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 88808;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Feedback';
	public static readonly description = `Discharge target's shields and deal EMG damage to it equal to ${numericAPScaling(
		FEEDBACK_SCALE,
		AbilityPowerUIToken,
		FEEDBACK_MULTIPLIER,
		true,
	)} the amount of lost Shield Capacity`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SVFeedbackEffect, meta?) => {
			const shieldCapacity = unit.totalShields;
			unit.addShields(-shieldCapacity + ArithmeticPrecision).applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.magnetic]: shieldCapacity,
				},
				effect.source,
				{ factor: FEEDBACK_MULTIPLIER + effect.power * FEEDBACK_SCALE },
			);
			return {} as IState;
		},
	};
	public priority = -2;
}

export default SVFeedbackEffect;

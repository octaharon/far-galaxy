import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const BlindHitChance = -0.9;
const BlindHitChanceScale = -0.2;

export class SVBlindEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 88806;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Blind';
	public static readonly description = `A Unit loses ${numericAPScaling(
		BlindHitChanceScale,
		AbilityPowerUIToken,
		BlindHitChance,
		true,
	)} Hit Chance on all Weapons for Effect Duration`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: SVBlindEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: BlindHitChance + BlindHitChanceScale * effect.power,
					},
				},
			});
			return {} as IState;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SVBlindEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: -BlindHitChance - BlindHitChanceScale * effect.power,
					},
				},
			});
			return {} as IState;
		},
	};
	public priority = -2;
}

export default SVBlindEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const SurveyBaseRadius = 2.5;
export const SurveyRadiusScale = 0.5;
export const SurveyXP = DEFAULT_PLAYER_RESEARCH_XP * 0.2;

export class SVSurveyEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 88810;
	public static readonly negative = false;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Survey';
	public static readonly description = `Player gains a vision over a zone with Radius ${numericAPScaling(
		SurveyRadiusScale,
		AbilityPowerUIToken,
		SurveyBaseRadius,
	)}, as if it were inside their Scan Range. When invoking, break Cloak of Hostile Units inside the zone and gain ${SurveyXP} XP per Unit, when doing so `;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: SVSurveyEffect, meta?) => {
				cell.radius = SurveyBaseRadius + SurveyRadiusScale * effect.power;
				const src = effect?.source;
				if (meta.map) {
					meta.map.getUnitsInRange(cell).forEach((u) => {
						if (effect.static.isUnitTarget(src) && !u.isFriendlyToPlayer(src.playerId)) {
							u.status.cloaked = false;
							src.addXp(SurveyXP);
						}
					});
				}
				return {} as IState;
			},
		};
	public priority = -3;
}

export default SVSurveyEffect;

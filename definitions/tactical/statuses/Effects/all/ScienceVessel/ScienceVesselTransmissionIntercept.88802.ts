import { IPlayerBase } from '../../../../../orbital/base/types';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class SVTransmissionInterceptEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 88802;
	public static readonly negative = false;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Transmission Intercept';
	public static readonly description = 'Player gains +3 Counter-Intelligence';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base: IPlayerBase) => {
			base.counterIntelligenceLevel += 3;
			return {} as IState;
		},
		[EffectEvents.TPlayerEffectEvents.expire]: (base: IPlayerBase) => {
			base.counterIntelligenceLevel -= 3;
			return {} as IState;
		},
	};
	public priority = -3;
}

export default SVTransmissionInterceptEffect;

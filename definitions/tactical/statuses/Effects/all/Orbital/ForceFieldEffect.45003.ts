import { IMapCell } from '../../../../map';
import Units from '../../../../units/types';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	oldStatus: Record<string, Partial<Units.TUnitCombatStatus>>;
}

export class ForceFieldEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 45003;
	public static readonly negative = false;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Force Field';
	public static readonly description =
		'Impassable and impenetrable sphere of time-space turbulence, which disables captured units';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: ForceFieldEffect, meta?) => {
				const c: IState = {
					oldStatus: {},
				};
				if (meta.map) {
					const units = meta.map.getUnitsInRange(cell).filter((u) => !u.isImmuneToEffects);
					for (const unit of units) {
						c.oldStatus[unit.getInstanceId()] = unit.status;
						unit.status.disabled = true;
						unit.status.targetable = false;
						unit.status.moving = null;
					}
				}
				return c;
			},
			[EffectEvents.TMapEffectEvents.expire]: (cell: IMapCell, effect: ForceFieldEffect, meta?) => {
				if (meta.map) {
					const units = meta.map.getUnitsInRange(cell).filter((u) => !u.isImmuneToEffects);
					for (const unit of units) {
						if (effect.state.oldStatus[unit.getInstanceId()])
							unit.status = {
								...unit.status,
								...effect.state.oldStatus[unit.getInstanceId()],
							};
					}
				}
				return {
					oldStatus: {},
				};
			},
		};
	public priority = 0;
}

export default ForceFieldEffect;

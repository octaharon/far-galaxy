import DIContainer from '../../../../../core/DI/DIContainer';
import { DIInjectableSerializables } from '../../../../../core/DI/injections';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class AbductionEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 45002;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Abduction';
	public static readonly description =
		"Steal a Unit and put it into Orbital Bay (if there's free space), taking control over it and receiving some Engineering Points towards its technologies, otherwise Salvage it completely";
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: AbductionEffect, meta?) => {
			const base = meta?.bases?.[effect?.source?.playerId];
			if (base) {
				unit.playerId = effect.source.playerId;
				const salvage = DIContainer.getProvider(DIInjectableSerializables.salvage).createFromDestroyedUnit(
					unit.prototype,
					unit,
					true,
					true,
					true,
				);
				if (base.unitsAtBay.length >= base.currentEconomy.bayCapacity) {
					unit.destroy(effect.source);
					// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/7
					base.claimSalvage(salvage);
				} else {
					base.putUnitToBay(unit);
					base.owner?.addTechnologies(salvage.payload.technologies);
				}
			}
			return {} as IState;
		},
	};
	public priority = 0;
}

export default AbductionEffect;

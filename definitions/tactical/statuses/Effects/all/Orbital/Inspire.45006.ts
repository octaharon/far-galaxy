import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const InspirePercent = 0.1;
const InspireScale = 0.02;
export const INSPIRE_RADIUS = 4;

export class InspireEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 45006;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Inspire';
	public static readonly description = `Grant all Allied Units in ${INSPIRE_RADIUS} Radius ${numericAPScaling(
		InspireScale,
		AbilityPowerUIToken,
		InspirePercent,
		true,
	)} bonus HP and Damage permanently`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: InspireEffect, meta?) => {
				const strength = effect.power * InspireScale + InspirePercent;
				if (meta.map) {
					const units = meta.map
						.getUnitsInRange(cell)
						.filter((u) => u.doesBelongToPlayer(effect.source?.playerId));
					for (const unit of units) {
						unit.applyChassisModifier({
							hitPoints: {
								factor: 1 + strength,
							},
						});
						unit.currentHitPoints = BasicMaths.applyModifier(unit.currentHitPoints, {
							factor: 1 + strength,
						});
						unit.applyWeaponModifier({
							default: {
								damageModifier: {
									factor: 1 + strength,
								},
							},
						});
					}
				}
				return {} as IState;
			},
		};
	public priority = 0;
}

export default InspireEffect;

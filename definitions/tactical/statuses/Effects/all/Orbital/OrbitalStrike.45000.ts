import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../damage/attack';
import AttackManager from '../../../../damage/AttackManager';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const OrbitalStrikeBaseDamage = 20;
const OrbitalStrikeScatterChance = 1.65;
const OrbitalStrikeBaseRadius = 1.5;
const OrbitalStrikeScaleRadius = 0.5;
export const OrbitalStrikeAttack: (power: number) => UnitAttack.TAOEAttackDefinition = (power) => ({
	[UnitAttack.attackFlags.AoE]: true,
	delivery: UnitAttack.deliveryType.bombardment,
	magnitude: OrbitalStrikeScatterChance * power,
	aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
	radius: OrbitalStrikeScaleRadius * power + OrbitalStrikeBaseRadius,
	[Damage.damageType.heat]: {
		min: OrbitalStrikeBaseDamage * 2,
		max: OrbitalStrikeBaseDamage * 4,
		distribution: 'Parabolic',
	},
	[Damage.damageType.magnetic]: {
		min: OrbitalStrikeBaseDamage * 1.5,
		max: OrbitalStrikeBaseDamage * 3,
		distribution: 'Parabolic',
	},
	[Damage.damageType.thermodynamic]: {
		min: OrbitalStrikeBaseDamage * 2.5,
		max: OrbitalStrikeBaseDamage * 5,
		distribution: 'Parabolic',
	},
});

export class OrbitalStrikeEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 45000;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Orbital Strike';
	public static readonly description = `Issue a Bombardment Scatter Attack with AoE Radius of ${numericAPScaling(
		OrbitalStrikeScaleRadius,
		AbilityPowerUIToken,
		OrbitalStrikeScaleRadius,
	)} and ${AbilityPowerUIToken}${OrbitalStrikeScatterChance} Hits per Unit. Each Hit deals a total of ${AbilityPowerUIToken}${
		6 * OrbitalStrikeBaseDamage
	}+${AbilityPowerUIToken}${12 * OrbitalStrikeBaseDamage} HEA, EMG and TRM Damage`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: OrbitalStrikeEffect, meta?) => {
				if (meta.map) {
					meta.map.applyAoEAttack(
						cell,
						{
							...(AttackManager.getAttackDamage(
								OrbitalStrikeAttack(effect.power),
							) as UnitAttack.TAttackAOEUnit),
							radius: cell.radius,
						},
						effect.source,
					);
				}
				return {} as IState;
			},
		};
	public priority = 0;
}

export default OrbitalStrikeEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const MytochondrialHitPoints = 0.15;
const MytochondrialScale = 0.05;

export class MytochondrialMutationEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 37005;
	public static readonly negative = false;
	public static readonly baseDuration = -1;
	public static readonly caption = 'Mytochondrial Mutation';
	public static readonly unremovable = false;
	public static readonly description = `Organic Unit permanently gains ${numericAPScaling(
		MytochondrialScale,
		AbilityPowerUIToken,
		MytochondrialHitPoints,
		true,
	)} Hit Points, +0.5 Ability Power and becomes immune to RAD damage to Hit Points. Doesn't stack with itself, but is removable`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: MytochondrialMutationEffect, meta?) => {
			if (unit.hasEffect(MytochondrialMutationEffect.id)) return effect.state;
			unit.applyEffectModifier({
				abilityPowerModifier: [{ bias: 0.5, min: 0, minGuard: ArithmeticPrecision }],
			});
			unit.applyChassisModifier({
				hitPoints: {
					factor: 1 + MytochondrialScale * effect.power + MytochondrialHitPoints,
				},
			});
			unit.applyArmorModifier({
				[Damage.damageType.radiation]: {
					factor: 0,
					bias: -Infinity,
					min: 0,
					max: ArithmeticPrecision,
					minGuard: 0,
				},
			});
			return {};
		},
	};
	public priority = 0;
}

export default MytochondrialMutationEffect;

import { IMapCell } from '../../../../map';
import EffectEvents from '../../../effectEvents';
import { ConstantMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class DeployBeaconEffect extends ConstantMapStatusEffect<IState> {
	public static readonly id = 45004;
	public static readonly negative = false;
	public static readonly caption = 'Deploy beacon';
	public static readonly description = 'A zone where units can be deployed';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: DeployBeaconEffect, meta?) => {
				return {};
			},
		};
	public priority = 0;
}

export default DeployBeaconEffect;

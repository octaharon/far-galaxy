import BasicMaths from '../../../../../maths/BasicMaths';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	damageValue: number;
}

const IRRADIATE_DAMAGE = 100;

export class IrradiateEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 45008;
	public static readonly negative = false;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Irradiate';
	public static readonly description = `At Turn Start, all Robotic Units in the Zone are dealt ${AbilityPowerUIToken}${IRRADIATE_DAMAGE} RAD Damage, while all Organic Units heal fo the same amount instead. Per every Unit affected, the Damage/Hit Points value is increased by 20% and the Effect Radius is increased by 0.5`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: IrradiateEffect, meta?) => {
				const baseDamage = IRRADIATE_DAMAGE * effect.power;
				const units = meta.map.getUnitsInRange(cell);
				units.forEach((u) => {
					if (u.isOrganic) u.addHp(baseDamage, effect.source);
					if (u.isRobotic)
						u.applyAttackUnit(
							{
								delivery: UnitAttack.deliveryType.immediate,
								[Damage.damageType.radiation]: baseDamage,
							},
							effect.source,
						);
				});
				cell.radius = BasicMaths.applyModifier(cell.radius, {
					bias: 0.5 * units.length,
				});
				return {
					damageValue: BasicMaths.applyModifier(baseDamage, {
						factor: 1 + 0.2 * units.length,
					}),
				} as IState;
			},
			[EffectEvents.TMapEffectEvents.turnStart]: (cell: IMapCell, effect: IrradiateEffect, meta?) => {
				const units = meta.map.getUnitsInRange(cell);
				units.forEach((u) => {
					if (u.isOrganic) u.addHp(effect.state.damageValue, effect.source);
					if (u.isRobotic)
						u.applyAttackUnit(
							{
								delivery: UnitAttack.deliveryType.immediate,
								[Damage.damageType.radiation]: effect.state.damageValue,
							},
							effect.source,
						);
				});
				cell.radius = BasicMaths.applyModifier(cell.radius, {
					bias: 0.5 * units.length,
				});
				return {
					damageValue: BasicMaths.applyModifier(effect.state.damageValue, {
						factor: 1 + 0.2 * units.length,
					}),
				} as IState;
			},
		};
	public priority = 0;
}

export default IrradiateEffect;

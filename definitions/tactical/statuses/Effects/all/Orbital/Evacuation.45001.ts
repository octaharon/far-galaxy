import _ from 'underscore';
import Vectors from '../../../../../maths/Vectors';
import { IMapCell } from '../../../../map';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class EvacuationEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 45001;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Evacuation';
	public static readonly description =
		'Put all Allied Units in Effect Radius into your Unit Bay (as long as space is available)';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: EvacuationEffect, meta?) => {
				const base = meta.bases?.[effect?.source?.playerId];
				if (meta.map && base) {
					const units = _.sortBy(
						meta.map
							.getUnitsInRange(cell)
							.filter((u) => u.doesBelongToPlayer(effect.source.playerId) && !u.isImmuneToEffects),
						(u) => Vectors.module()(Vectors.create(u.location, cell)),
					);
					for (const unit of units) {
						if (base.unitsAtBay.length >= base.currentEconomy.bayCapacity) break;
						meta.map.removeUnit(unit);
						base.putUnitToBay(unit);
					}
				}
				return {};
			},
		};
	public priority = 0;
}

export default EvacuationEffect;

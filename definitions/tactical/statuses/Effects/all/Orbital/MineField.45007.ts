import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const MinefieldDamage = 25;

export class MinefieldEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 45007;
	public static readonly negative = true;
	public static readonly baseDuration = 4;
	public static readonly caption = 'Mine Field';
	public static readonly description = 'Detonate the area when any unit moves inside it';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.unitMoves]: (cell: IMapCell, effect: MinefieldEffect, meta?) => {
				if (meta.map) {
					meta.map.applyAoEAttack(
						cell,
						{
							[UnitAttack.attackFlags.AoE]: true,
							delivery: UnitAttack.deliveryType.cloud,
							aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
							radius: cell.radius,
							magnitude: 1,
							[Damage.damageType.heat]: MinefieldDamage * 2 * effect.power,
							[Damage.damageType.thermodynamic]: MinefieldDamage * 3 * effect.power,
							[Damage.damageType.biological]: MinefieldDamage * 4 * effect.power,
						},
						effect.source,
					);
					meta.map.cells = meta.map.cells.filter((m) => m.getInstanceId() === cell.getInstanceId());
				}
				return {} as IState;
			},
		};
	public priority = 0;
}

export default MinefieldEffect;

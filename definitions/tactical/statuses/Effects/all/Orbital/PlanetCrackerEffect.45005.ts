import Geometry from '../../../../../maths/Geometry';
import Vectors from '../../../../../maths/Vectors';
import UnitChassis from '../../../../../technologies/types/chassis';
import { IMapCell } from '../../../../map';
import GenericCombatMap from '../../../../map/GenericCombatMap';
import MapGenerator from '../../../../map/MapGenerator';
import { isCaveEntity, isHillEntity, isLakeEntity } from '../../../../map/MapObstacles';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class PlanetCrackerEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 45005;
	public static readonly negative = false;
	public static readonly duration = 0;
	public static readonly caption = 'Planet Cracker';
	public static readonly description =
		'Destroy hills at location, or create a rift in plain land. Has no such effect in water or rifts. Destroy doodads and all ground and naval units in area';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: PlanetCrackerEffect, meta?) => {
				if (meta.map) {
					let inWater = false;
					let inPlainLand = true;
					const crossedEntities = Object.values(meta.map.terrainEntities).filter(
						(e) => !isCaveEntity(e) && GenericCombatMap.isPointWithinEntityBounds(cell, e, cell.radius),
					);
					for (const entity of crossedEntities) {
						inPlainLand = false;
						if (isHillEntity(entity)) {
							if (Geometry.isPointInRange(entity.shape.center, cell)) {
								delete meta.map.terrainEntities[entity.entityId];
							} else {
								entity.shape.center = Vectors.create(cell, entity.shape.center);
								const croppedEntity = MapGenerator.i(meta.map.parameters).clipEntityToCentralRing(
									entity,
									cell.radius,
								);
								croppedEntity.shape.center = Vectors.add(entity.shape.center, cell);
								meta.map.terrainEntities[croppedEntity.entityId] = croppedEntity;
							}
						}
						if (isLakeEntity(entity)) {
							inWater = true;
						}
					}
					if (!inWater && inPlainLand) {
						const rift = MapGenerator.i(meta.map.parameters).generateCave(0, 0, cell.radius);
						rift.shape.center = {
							x: cell.x,
							y: cell.y,
						};
						meta.map.terrainEntities[rift.entityId] = rift;
					}
					const doodadsInArea: string[] = [];
					meta.map.doodads.forEach((d) => {
						if (GenericCombatMap.isPointWithinMapCell(d, cell)) doodadsInArea.push(d.doodadId);
					});
					if (doodadsInArea.length)
						meta.map.doodads = meta.map.doodads.filter((d) => !doodadsInArea.includes(d.doodadId));
					meta.map.getUnitsInRange(cell).forEach((u) => {
						if ([UnitChassis.movementType.ground, UnitChassis.movementType.naval].includes(u.movement))
							u.destroy(effect.source, false);
					});
				}
				return {};
			},
		};
	public priority = 0;
}

export default PlanetCrackerEffect;

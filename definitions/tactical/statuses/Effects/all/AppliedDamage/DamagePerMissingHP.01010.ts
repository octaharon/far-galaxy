import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitDefense from '../../../../damage/defense';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import { DOTEffectProps } from './DamageOverTime.01002';

interface IState extends DOTEffectProps {}

export class DOTPerMissingHPEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 1010;
	public static readonly baseDuration = 2;
	public static readonly negative = true;
	public static readonly caption = 'Damage Over Time';
	public static readonly description = `This unit takes damage every turn, increased by ${numericAPScaling(
		0.01,
		AbilityPowerUIToken,
		null,
		true,
	)} per every 1% of Unit's missing Hit Points`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DOTPerMissingHPEffect) => {
			if (unit.isImmuneToNegativeEffects || unit.hasPerks(UnitDefense.defenseFlags.warp)) {
				effect.duration = 0;
				return {
					...effect.state,
					damageSeries: [],
				};
			}
			effect.duration = Math.min(effect.state.damageSeries?.length || 0, effect.duration);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: DOTPerMissingHPEffect) => {
			if (!effect.state?.damageSeries?.length) return effect.state;
			effect.state.damageSeries = [...effect.state.damageSeries.slice(1), effect.state.damageSeries[0]];
			const aUnit = effect.state.damageSeries[0];
			unit.applyAttackUnit(aUnit, effect.source, {
				factor: 1 + (1 - unit.currentHitPoints / unit.maxHitPoints) * effect.power,
			});
			return effect.state;
		},
	};
	public priority = 5;
}

export default DOTPerMissingHPEffect;

import UnitAttack from '../../../../damage/attack';
import UnitDefense from '../../../../damage/defense';
import { DOTStatusEffectId } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import _ from 'underscore';

export interface DOTEffectProps extends UnitAttack.IDamageOverTimeEffect {}

export class DamageOverTimeStatusEffect extends GenericUnitStatusEffect<DOTEffectProps> {
	public static readonly id = DOTStatusEffectId;
	public static readonly baseDuration = 3;
	public static readonly negative = true;
	public static readonly caption = 'Damage over time';
	public static readonly description = '%s this unit takes damage every turn';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		DOTEffectProps
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DamageOverTimeStatusEffect) => {
			if (unit.hasPerks(UnitDefense.defenseFlags.warp)) {
				effect.duration = 0;
				return {
					...effect.state,
					damageSeries: [],
				};
			}
			effect.duration = Math.min(effect.duration, effect.state.damageSeries?.length || 0);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: DamageOverTimeStatusEffect) => {
			if (!effect.state?.damageSeries?.length) return effect.state;
			effect.state.damageSeries = [...effect.state.damageSeries.slice(1), effect.state.damageSeries[0]];
			const aUnit = effect.state.damageSeries[0];
			unit.applyAttackUnit(aUnit, effect.source, {
				factor: effect.power,
			});
			return effect.state;
		},
	};
	public priority = 5;
}

export default DamageOverTimeStatusEffect;

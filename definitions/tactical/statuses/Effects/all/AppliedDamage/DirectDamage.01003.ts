import UnitAttack from '../../../../damage/attack';
import { EffectEvents } from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	attackUnit: UnitAttack.TAttackUnit;
}

export class DirectDamageStatusEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 1003;
	public static readonly baseDuration: number = 0;
	public static readonly caption = 'Immediate Damage, scaled with Ability Power';
	public static readonly negative = true;
	public static readonly durationFixed: boolean = true;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DirectDamageStatusEffect) => {
			unit.applyAttackUnit(effect.state.attackUnit, effect.source, {
				factor: effect.power,
			});
			return effect.state;
		},
	};

	public priority = 0;
}

export default DirectDamageStatusEffect;

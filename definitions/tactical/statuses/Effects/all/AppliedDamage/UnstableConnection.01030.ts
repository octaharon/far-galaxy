import Vectors from '../../../../../maths/Vectors';
import UnitAttack from '../../../../damage/attack';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

export interface TUnstableConnectionProps {
	damage: UnitAttack.TAttackUnit;
	capacity: number;
	range: number;
}

export class UnstableConnectionEffect extends GenericUnitStatusEffect<TUnstableConnectionProps> {
	public static readonly id = 1030;
	public static readonly baseDuration = 2;
	public static readonly negative = true;
	public static readonly caption = 'Unstable Connection';
	public static readonly description = `Unit will take Damage if it gets too far away from the invoker, while the invoker gains Shield Capacity every Turn End if this Unit stays bound`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		TUnstableConnectionProps
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: UnstableConnectionEffect) => {
			if (effect.static.isUnitTarget(effect.source)) {
				effect.source.addShields(effect.state.capacity * effect.power);
			}
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.move]: (unit, effect: UnstableConnectionEffect) => {
			if (effect.static.isUnitTarget(effect.source)) {
				if (
					Vectors.normal()(
						Vectors.create(effect.source.location || { x: 0, y: 0 }, unit.location || { x: 0, y: 0 }),
					) > effect.state.range
				) {
					unit.applyAttackUnit(effect.state.damage, effect.source, {
						factor: effect.power,
					});
					unit.removeEffect(effect.getInstanceId());
				}
			}
			return effect.state;
		},
	};
	public priority = 4;
}

export default UnstableConnectionEffect;

import Vectors from '../../../../../maths/Vectors';
import UnitAttack from '../../../../damage/attack';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

export interface TLeavingRangeDamageProps {
	damage: UnitAttack.TAttackUnit;
	range: number;
}

export class DamageWhenLeavingRangeEffect extends GenericUnitStatusEffect<TLeavingRangeDamageProps> {
	public static readonly id = 1020;
	public static readonly baseDuration = 2;
	public static readonly negative = true;
	public static readonly caption = 'Bound';
	public static readonly description = `This Unit takes Damage if it gets too far away from the invoker`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		TLeavingRangeDamageProps
	> = {
		[EffectEvents.TUnitEffectEvents.move]: (unit, effect: DamageWhenLeavingRangeEffect) => {
			if (effect.static.isUnitTarget(effect.source)) {
				if (
					Vectors.normal()(
						Vectors.create(effect.source.location || { x: 0, y: 0 }, unit.location || { x: 0, y: 0 }),
					) > effect.state.range
				) {
					unit.applyAttackUnit(effect.state.damage, effect.source, {
						factor: effect.power,
					});
					unit.removeEffect(effect.getInstanceId());
				}
			}
			return effect.state;
		},
	};
	public priority = 4;
}

export default DamageWhenLeavingRangeEffect;

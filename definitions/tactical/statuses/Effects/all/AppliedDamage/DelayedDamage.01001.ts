import UnitAttack from '../../../../damage/attack';
import { DeferredStatusEffectId } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	attackUnit: UnitAttack.TAttackUnit;
}

export class DeferredDamageEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = DeferredStatusEffectId;
	public static readonly caption = 'Deferred damage';
	public static readonly description =
		'Unit will take damage either 1 Turn before the Effect Duration expires or right then, whatever happens first';
	public static readonly negative = true;
	public static baseDuration = 2;
	public static durationFixed = true;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnStart]: (unit, effect: DeferredDamageEffect) => {
			if (effect.duration > 1) return effect.state;
			unit.applyAttackUnit(effect.state.attackUnit, effect.source, {
				factor: effect.power,
			});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: DeferredDamageEffect) => {
			if (effect.duration > 1) return effect.state;
			unit.applyAttackUnit(effect.state.attackUnit, effect.source, {
				factor: effect.power,
			});
			return effect.state;
		},
	};
	public priority = 0;
}

export default DeferredDamageEffect;

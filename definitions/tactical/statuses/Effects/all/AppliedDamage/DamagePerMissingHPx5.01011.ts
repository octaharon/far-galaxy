import DamageManager from '../../../../damage/DamageManager';
import UnitDefense from '../../../../damage/defense';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import { DOTEffectProps } from './DamageOverTime.01002';

interface IState extends DOTEffectProps {}

export class DamagePerMissingHPx5Effect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 1011;
	public static readonly baseDuration = 2;
	public static readonly negative = true;
	public static readonly caption = 'Damage Over Time';
	public static readonly description = `This unit takes damage every turn, increased by 5% per every 1% of Unit's missing Hit Points`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: DamagePerMissingHPx5Effect) => {
			if (unit.hasPerks(UnitDefense.defenseFlags.warp)) {
				effect.duration = 0;
				return {
					...effect.state,
					damageSeries: [],
				};
			}
			effect.duration = effect.state.damageSeries?.length || 0;
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnStart]: (unit, effect: DamagePerMissingHPx5Effect) => {
			const aUnit = effect.state.damageSeries?.shift();
			if (aUnit)
				unit.applyAttackUnit(
					DamageManager.applyDamageModifierToDamageUnit(aUnit, {
						factor: (1 - unit.currentHitPoints / unit.maxHitPoints) * 5 + 1,
					}),
					effect.source,
					{
						factor: effect.power,
					},
				);
			return effect.state;
		},
	};
	public priority = 5;
}

export default DamagePerMissingHPx5Effect;

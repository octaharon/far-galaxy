import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const GreatEqualizerEffectRadius = 5;

export class GreatEqualizerEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 60011;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Great Equalizer';
	public static readonly description = `Set Hit Points of all Units in Effect Radius ${GreatEqualizerEffectRadius} from Target equal to their combined average percentage`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect, meta) => {
			let sum = 0;
			let total = 0;
			meta.map
				?.getUnitsInRange({
					...(unit?.location || { x: 0, y: 0 }),
					radius: GreatEqualizerEffectRadius,
				})
				?.forEach((unit) => {
					total++;
					sum += unit.currentHitPoints / unit.maxHitPoints;
				});
			if (!total) return effect.state;
			const avg = sum / total;
			meta.map
				?.getUnitsInRange({
					...(unit?.location || { x: 0, y: 0 }),
					radius: GreatEqualizerEffectRadius,
				})
				?.forEach((unit) => {
					unit.currentHitPoints = unit.maxHitPoints * avg;
				});
		},
	};
	public priority = 0;
}

export default GreatEqualizerEffect;

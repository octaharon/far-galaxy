import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const ShieldFunnelStrength = 0.01;

export class ShieldFunnelEFfect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 60007;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Shield Funnel';
	public static readonly description = `Increase an Allied Unit's Shield Capacity by ${numericAPScaling(
		ShieldFunnelStrength,
		AbilityPowerUIToken,
	)} per each point of Base Energy Output and fully restore them. Hostile unit's Shields lose the same amount of Shield Capacity instead`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: ShieldFunnelEFfect, meta?) => {
			const base = meta?.bases?.[effect?.source?.playerId];
			if (!base) return {};
			const percentage = base.currentEconomy.energyOutput * effect.power * ShieldFunnelStrength;
			const extraShield = unit.totalShields * percentage;
			if (unit.doesBelongToPlayer(effect.source.playerId)) {
				unit.applyShieldsModifier({
					shieldAmount: {
						factor: 1 + percentage,
					},
				});
				unit.addShields(Math.max(0, unit.totalShields - unit.currentShields));
			} else {
				unit.addShields(-extraShield);
			}
			return {};
		},
	};
	public priority = 0;
}

export default ShieldFunnelEFfect;

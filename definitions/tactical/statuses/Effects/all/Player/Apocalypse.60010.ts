import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import InterfaceTypes from '../../../../../core/InterfaceTypes';
import BasicMaths from '../../../../../maths/BasicMaths';
import { applyDistribution } from '../../../../../maths/Distributions';
import Geometry from '../../../../../maths/Geometry';
import UnitChassis from '../../../../../technologies/types/chassis';
import UnitAttack from '../../../../damage/attack';
import AttackManager from '../../../../damage/AttackManager';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import { MAX_MAP_RADIUS } from '../../../../map/constants';
import { isLakeEntity } from '../../../../map/MapObstacles';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const ApocalypseEffectRadius = 4.75;
export const ApocalypseEffectRadiusScale = 0.75;
export const ApocalypseEffectQuantity = 7;
export const ApocalypseDamage = 30;
export const ApocalypseEffectCoastlineOffsetPercent = 0.1;
export const ApocalypseEffectAttack: (power: number) => UnitAttack.TAOEAttackDefinition = (power) => ({
	[UnitAttack.attackFlags.AoE]: true,
	delivery: UnitAttack.deliveryType.immediate,
	magnitude: 1,
	aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
	radius: ApocalypseEffectRadius + ApocalypseEffectRadiusScale * power,
	[Damage.damageType.warp]: {
		min: ApocalypseDamage * 3 * power,
		max: ApocalypseDamage * 4 * power,
		distribution: 'Bell',
	},
	[Damage.damageType.magnetic]: {
		min: ApocalypseDamage * 6 * power,
		max: ApocalypseDamage * 8 * power,
		distribution: 'Minimal',
	},
	[Damage.damageType.radiation]: {
		min: ApocalypseDamage * power,
		max: ApocalypseDamage * 3 * power,
		distribution: 'Maximal',
	},
});

export class ApocalypseEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 60010;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Apocalypse';
	public static readonly description = `Drop ${ApocalypseEffectQuantity} planetary bombs to the map randomly. Each planetary bomb explodes in ${numericAPScaling(
		ApocalypseEffectRadiusScale,
		AbilityPowerUIToken,
		ApocalypseEffectRadius,
	)} Effect Radius, dealing ${AbilityPowerUIToken}${10 * ApocalypseDamage} - ${AbilityPowerUIToken}${
		15 * ApocalypseDamage
	} Damage of EMG, WRP and RAD to all Units, destroying doodads and reshaping coastlines`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: ApocalypseEffect, meta?) => {
				if (meta.map) {
					const locations: InterfaceTypes.ISpot[] = [cell];
					for (let i = 0; i < ApocalypseEffectQuantity - 1; i++) {
						locations.push({
							radius: ApocalypseEffectRadius,
							...Geometry.polar2vector({
								alpha: Math.PI * 2 * Math.random(),
								r: applyDistribution({
									min: 0,
									max: meta.map.parameters.size * MAX_MAP_RADIUS - ApocalypseEffectRadius / 2,
									distribution: 'Maximal',
								}),
							}),
						});
					}
					locations.map((l) => {
						// removing doodads
						meta.map.doodads = meta.map.doodads.filter((d) => Geometry.isPointInRange(d, l));
						// reshaping lakes
						Object.values(meta.map.terrainEntities).filter((e) => {
							if (isLakeEntity(e)) {
								for (let i = 0; i < e.shape.radiusArray.length; i++) {
									const p = Geometry.polarShapePoint(e.shape, i);
									if (Geometry.isPointInRange(p, l))
										e.shape.radiusArray[i] = BasicMaths.applyModifier(e.shape.radiusArray[i], {
											factor:
												1 -
												ApocalypseEffectCoastlineOffsetPercent +
												Math.random() * 2 * ApocalypseEffectCoastlineOffsetPercent,
										});
								}
							}
						});
						// destroying water units caught off the water
						meta.map.unitsOnMap
							.filter(
								(u) =>
									u.movement === UnitChassis.movementType.naval &&
									!meta.map.isPointPassable(u.location, u.movement),
							)
							.map((u) => u.destroy(effect.source, false));
						// bombing locations
						meta.map.applyAoEAttack(
							l,
							{
								...(AttackManager.getAttackDamage(
									ApocalypseEffectAttack(effect.power),
								) as UnitAttack.TAttackAOEUnit),
								radius: l.radius,
							},
							effect.source,
						);
					});
				}
				return {} as IState;
			},
		};
	public priority = 0;
}

export default ApocalypseEffect;

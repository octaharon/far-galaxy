import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import { IStatusEffect } from '../../../types';

interface IState {}

const appliance = (
	base: StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.player>,
	effect: IStatusEffect<StatusEffectMeta.effectAgentTypes.player>,
) => {
	base.addEnergy(-base.energyCapacity);
	base.addRawMaterials(-base.rawMaterials);
	(base.unitsAtBay ?? []).map((u) => u.addHp(u.maxHitPoints - u.currentHitPoints, effect.source));
	base.currentHitPoints = base.currentEconomy.maxHitPoints;
	return null as IState;
};

export class MobilizationEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 60025;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Mobilization';
	public static readonly description = `The Base loses all Energy and Raw Materials Storage, but it and its Docked Units are fully and instantly repaired`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: appliance,
	};
	public priority = 0;
}

export default MobilizationEffect;

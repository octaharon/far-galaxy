import { TBaseBuildingType } from '../../../../../orbital/types';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const AlterationLength = 2;
const AlterationScale = 12;

export class AlterationEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 60030;
	public static readonly negative = true;
	public static readonly baseDuration = AlterationLength;
	public static readonly caption = 'Alteration';
	public static readonly description = `The next Base Ability you use within ${AlterationLength} Turns has its Cooldown reset, and, when you do, every working Factory gains ${AbilityPowerUIToken}${AlterationScale} Production Points`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.spell]: (base, effect, meta) => {
			meta.ability.cooldownLeft = 0;
			base.buildings[TBaseBuildingType.factory].filter(Boolean).map((f) => {
				if (f.currentProductionSlot && f.isWorking() && f.productionPrototypes[f.currentProductionSlot])
					f.currentProductionStock += AlterationScale * effect.power;
			});
			return null;
		},
	};
	public priority = 0;
}

export default AlterationEffect;

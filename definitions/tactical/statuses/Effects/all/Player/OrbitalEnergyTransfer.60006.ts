import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitChassis from '../../../../../technologies/types/chassis';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const ORBITAL_ENERGY_TRANSFER_CAPACITY = 0.3;
const ORBITAL_ENERGY_TRANSFER_SCALE = 0.2;

export class OrbitalEnergyTransferEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 60006;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Orbital Energy Transfer';
	public static readonly description = `All units inside the zone restore ${numericAPScaling(
		ORBITAL_ENERGY_TRANSFER_SCALE,
		AbilityPowerUIToken,
		ORBITAL_ENERGY_TRANSFER_CAPACITY,
		true,
	)} of their total Shield Capacity and get Overcharged if possible. Robotic Units restore all Hit Points as well`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.turnStart]: (cell: IMapCell, effect: OrbitalEnergyTransferEffect, meta?) => {
				if (meta.map) {
					meta.map.getUnitsInRange(cell).forEach((u) => {
						if (u.isRobotic) u.addHp(u.maxHitPoints, effect.source);
						u.addShields(
							u.totalShields *
								(ORBITAL_ENERGY_TRANSFER_CAPACITY + ORBITAL_ENERGY_TRANSFER_SCALE * effect.power),
						);
					});
				}
				return {};
			},
		};
	public priority = 0;
}

export default OrbitalEnergyTransferEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const EquinoxBarrageDamage = 20;
const EquinoxBarrageDamageScale = 10;

export class EquinoxBarrageEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 60009;
	public static readonly negative = true;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Equinox Barrage';
	public static readonly description = `Deal ${numericAPScaling(
		EquinoxBarrageDamageScale * 8,
		AbilityPowerUIToken,
		EquinoxBarrageDamage * 8,
	)} Damage to enemy's Base, comprised equally of ANH, KIN, TRM and HEA.`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base, effect) => {
			if (base) {
				const dmg = EquinoxBarrageDamage + EquinoxBarrageDamageScale * effect.power;
				for (let i = 0; i < 3; i++)
					base.applyAttackUnit({
						delivery: UnitAttack.deliveryType.missile,
						[Damage.damageType.antimatter]: dmg * 2,
						[Damage.damageType.kinetic]: dmg * 2,
						[Damage.damageType.thermodynamic]: dmg * 2,
						[Damage.damageType.heat]: dmg * 2,
					});
			}
			return {} as IState;
		},
	};
	public priority = 4;
}

export default EquinoxBarrageEffect;

import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const WeatherStormDamage = 120;

export class WeatherStormEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 60004;
	public static readonly negative = true;
	public static readonly baseDuration = 5;
	public static readonly caption = 'Weather storm';
	public static readonly description = `All Units caught in the storm by the Turn End take ${AbilityPowerUIToken}${WeatherStormDamage} EMG Damage as a Cloud Attack`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.turnEnd]: (cell: IMapCell, effect: WeatherStormEffect, meta?) => {
				if (meta.map) {
					meta.map.getUnitsInRange(cell).forEach((u) =>
						u.applyAttackUnit(
							{
								delivery: UnitAttack.deliveryType.cloud,
								[Damage.damageType.magnetic]: WeatherStormDamage * effect.power,
							},
							effect.source,
						),
					);
				}
				return {};
			},
		};
	public priority = 0;
}

export default WeatherStormEffect;

import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class SegmentationFaultEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 60000;
	public static readonly negative = false;
	public static readonly baseDuration = 0;
	public static readonly caption = 'Segmentation Fault';
	public static readonly description = 'Get an extra deployment next turn';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base) => {
			if (base) base.addDeploy();
			return {} as IState;
		},
	};
	public priority = 0;
}

export default SegmentationFaultEffect;

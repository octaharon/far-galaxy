import { TBaseBuildingType } from '../../../../../orbital/types';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class JeopardyEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 60002;
	public static readonly negative = true;
	public static readonly baseDuration = 2;
	public static readonly caption = 'Jeopardy';
	public static readonly description = 'The production is disabled';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.apply]: (base, effect: JeopardyEffect, meta?) => {
			if (base) {
				base.buildings[TBaseBuildingType.factory].forEach((b) => {
					b.disabled = true;
				});
			}
			return {} as IState;
		},
		[EffectEvents.TPlayerEffectEvents.expire]: (base, effect: JeopardyEffect, meta?) => {
			if (base) {
				base.buildings[TBaseBuildingType.factory].forEach((b) => {
					b.disabled = false;
				});
			}
			return {} as IState;
		},
	};
	public priority = 0;
}

export default JeopardyEffect;

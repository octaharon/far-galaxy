import Geometry from '../../../../../maths/Geometry';
import UnitChassis from '../../../../../technologies/types/chassis';
import { IMapCell } from '../../../../map';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class CrumbleEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 60008;
	public static readonly negative = true;
	public static readonly duration = 0;
	public static readonly caption = 'Crumble';
	public static readonly description = `Destroy all Doodads and Airborne units in area. Turn down all cushions for Hover units, making them Ground. If a unit can't land properly, it's destroyed`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.apply]: (cell: IMapCell, effect: CrumbleEffect, meta?) => {
				if (meta.map) {
					meta.map.doodads = meta.map.doodads.filter((d) => !Geometry.isPointInRange(d, cell));
					meta.map.getUnitsInRange(cell).forEach((u) => {
						if (u.movement === UnitChassis.movementType.air) u.destroy(effect.source, false);
						if (u.movement === UnitChassis.movementType.hover) u.movement = UnitChassis.movementType.ground;
						if (!meta.map.isPointPassable(u.location, u.movement)) u.destroy(effect.source, false);
					});
				}
				return {};
			},
		};
	public priority = -5;
}

export default CrumbleEffect;

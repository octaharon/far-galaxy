import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { IMapCell } from '../../../../map';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericMapStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const CelestialFlowHPStrength = 0.24;
export const CelestialFlowHPScale = 0.06;
export const CelestialFlowRadius = 4;

export class CelestialFlowEffect extends GenericMapStatusEffect<IState> {
	public static readonly id = 60005;
	public static readonly negative = false;
	public static readonly baseDuration = 3;
	public static readonly caption = 'Celestial Flow';
	public static readonly description = `For the Duration, each Turn End restores ${numericAPScaling(
		CelestialFlowHPScale,
		AbilityPowerUIToken,
		CelestialFlowHPStrength,
		true,
	)} of max Hit Points to all Units inside an area of Radius ${CelestialFlowRadius}`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.map, IState> =
		{
			[EffectEvents.TMapEffectEvents.turnStart]: (cell: IMapCell, effect: CelestialFlowEffect, meta?) => {
				if (meta.map) {
					meta.map.getUnitsInRange(cell).forEach((u) => u.addHp(u.maxHitPoints, effect.source));
				}
				return {};
			},
		};
	public priority = 0;
}

export default CelestialFlowEffect;

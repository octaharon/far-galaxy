import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericPlayerStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import { IStatusEffect } from '../../../types';

interface IState {}

const VentilationStrength = 25;
const VentilationScale = 50;

const appliance = (
	base: StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.player>,
	effect: IStatusEffect<StatusEffectMeta.effectAgentTypes.player>,
) => {
	const energyDamage = Math.min(base.energyCapacity, VentilationStrength + VentilationScale * effect.power);
	base.addEnergy(-energyDamage);
	const unitDamage = base?.unitsAtBay?.length ? energyDamage / base.unitsAtBay.length : 0;
	if (unitDamage)
		base.unitsAtBay.map((u) => {
			u.applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.magnetic]: unitDamage,
				},
				effect.source,
			);
		});
	return null as IState;
};

export class VentilationEffect extends GenericPlayerStatusEffect<IState> {
	public static readonly id = 60020;
	public static readonly negative = true;
	public static readonly baseDuration = 4;
	public static readonly caption = 'Ventilation';
	public static readonly description = `The Base loses up to ${numericAPScaling(
		VentilationScale,
		AbilityPowerUIToken,
		VentilationStrength,
	)} Energy Storage now and every time a Unit is produced. Lost amount is spread equally between Units in Bay and dealt as EMG Damage`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.player,
		IState
	> = {
		[EffectEvents.TPlayerEffectEvents.unitBuilt]: appliance,
		[EffectEvents.TPlayerEffectEvents.apply]: appliance,
	};
	public priority = 0;
}

export default VentilationEffect;

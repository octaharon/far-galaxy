import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	healAmount: number;
}

export class RegenerationEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 1100;
	public static readonly baseDuration = 3;
	public static readonly caption = 'HP Regeneration';
	public static readonly description = 'Restore Hit Points, more with greater Ability Power';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: RegenerationEffect, meta?) => {
			unit.addHp(effect.state.healAmount * unit.abilityPower, effect.source);
			return effect.state;
		},
	};
	public priority = 1;
}

export default RegenerationEffect;

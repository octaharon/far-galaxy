import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	healAmount: number;
}

export class ConstantRegenerationEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 1101;
	public static readonly caption = 'HP Regeneration';
	public static readonly description = 'Restore Hit Points, more with greater Ability Power';
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: ConstantRegenerationEffect) => {
			unit.addHp(effect.state.healAmount * effect.power, effect.source);
			return effect.state;
		},
	};
	public priority = 1;
}

export default ConstantRegenerationEffect;

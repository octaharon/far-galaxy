import { biasToPercent, describePercentage, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';
import describePercentileModifier = BasicMaths.describePercentileModifier;

interface IState {}

const OrbitalGuidanceHitChance = 0.5;
const OrbitalGuidanceHitChanceScale = 0.1;
const OrbitalGuidanceRange = 1.5;
const OrbitalGuidanceXPRate = 0.15;

export class OrbitalGuidanceStatusEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 2003;
	public static readonly baseDuration = 3;
	public static readonly negative = false;
	public static readonly caption = 'Orbital Guidance Firmware';
	public static readonly description = `Temporarily grants a Unit ${numericAPScaling(
		OrbitalGuidanceHitChanceScale,
		AbilityPowerUIToken,
		OrbitalGuidanceHitChance,
		true,
	)} Hit Chance and ${numericAPScaling(
		OrbitalGuidanceRange,
		AbilityPowerUIToken,
	)} Attack Range on all Weapons. Dealing Damage to Shields under Effect Duration grants the Unit XP equal to ${describePercentage(
		OrbitalGuidanceXPRate,
	)} of drained Capacity`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: OrbitalGuidanceStatusEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: OrbitalGuidanceHitChance + OrbitalGuidanceHitChanceScale * effect.power,
					},
					baseRange: {
						bias: OrbitalGuidanceRange * effect.power,
					},
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: OrbitalGuidanceStatusEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: -OrbitalGuidanceHitChance - OrbitalGuidanceHitChanceScale * effect.power,
					},
					baseRange: {
						bias: -OrbitalGuidanceRange * effect.power,
						min: OrbitalGuidanceRange,
						minGuard: OrbitalGuidanceRange,
					},
				},
			});
			return {};
		},
	};
	public priority = -1;
}

export default OrbitalGuidanceStatusEffect;

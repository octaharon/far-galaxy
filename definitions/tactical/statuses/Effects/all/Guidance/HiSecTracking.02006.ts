import { biasToPercent } from '../../../../../../src/utils/wellRounded';
import BasicMaths from '../../../../../maths/BasicMaths';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export const HiSecTrackingHitChance = 0.03; // 3%

export class HiSecTrackingStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 2006;
	public static readonly negative = false;
	public static readonly caption = 'Hi-Sec Firmware';
	public static readonly description = `This Unit's Weapons get ${biasToPercent(
		HiSecTrackingHitChance,
	)} Hit Chance with every successful Attack`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.unitHit]: (unit, effect: HiSecTrackingStatusEffect, meta?) => {
			if (meta?.result.hit)
				unit.attacks = unit.attacks.map((a) =>
					Object.assign(a, {
						baseAccuracy: BasicMaths.applyModifier(a.baseAccuracy, {
							bias: HiSecTrackingHitChance,
						}),
					}),
				);
			return {};
		},
	};
	public priority = -1;
}

export default HiSecTrackingStatusEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const AssistedTrainingXP = 0.2;
const AssistedTrainingXPScale = 0.04;

export class AssistedTrainingEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 2008;
	public static readonly baseDuration = -1;
	public static readonly unremovable = true;
	public static readonly negative = false;
	public static readonly caption = 'Assisted Training';
	public static readonly description = `Unit gains extra ${numericAPScaling(
		AssistedTrainingXPScale,
		AbilityPowerUIToken,
		AssistedTrainingXP,
		true,
	)} XP for Kills`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, {}> = {
		[EffectEvents.TUnitEffectEvents.unitKill]: (unit, effect: AssistedTrainingEffect, meta?) => {
			if (meta && meta.target)
				unit.addXp(meta.target.maxHitPoints * (AssistedTrainingXP + AssistedTrainingXPScale * effect.power));
			return {};
		},
	};
	public priority = -1;
}

export default AssistedTrainingEffect;

import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { ArithmeticPrecision } from '../../../../../maths/constants';
import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../../../player/constants';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const ComputerGuidanceDodge = 0.3;
const ComputerGuidanceDodgeScale = 0.15;
const ComputerGuidanceSpeed = 1;
const ComputerGuidanceXP = DEFAULT_PLAYER_RESEARCH_XP;

export class ComputerGuidanceStatusEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 2004;
	public static readonly baseDuration = 3;
	public static readonly negative = false;
	public static readonly caption = 'Computer Guidance';
	public static readonly description = `Affected Units temporarily gain extra ${AbilityPowerUIToken}${ComputerGuidanceSpeed} Speed and ${numericAPScaling(
		ComputerGuidanceDodgeScale,
		AbilityPowerUIToken,
		ComputerGuidanceDodge,
		true,
	)} Dodge. Expending full Movement by the Turn End rewards the Unit with extra ${ComputerGuidanceXP} XP`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: ComputerGuidanceStatusEffect) => {
			if (unit.speed > 0 && unit.currentMovementPoints <= 0) {
				unit.addXp(ComputerGuidanceXP);
			}
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: ComputerGuidanceStatusEffect) => {
			if (!unit.doesBelongToPlayer(effect?.source?.playerId)) return {};
			unit.applyChassisModifier({
				dodge: {
					default: { bias: ComputerGuidanceDodge + ComputerGuidanceDodgeScale * effect.power },
				},
				speed: {
					bias: ComputerGuidanceSpeed * effect.power,
					minGuard: ArithmeticPrecision,
					min: 0,
				},
			});

			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect) => {
			unit.applyChassisModifier({
				dodge: {
					default: { bias: -ComputerGuidanceDodge - ComputerGuidanceDodgeScale * effect.power },
				},
				speed: {
					bias: -ComputerGuidanceSpeed * effect.power,
					min: 0,
					minGuard: 0,
				},
			});
			return {};
		},
	};
	public priority = -1;
}

export default ComputerGuidanceStatusEffect;

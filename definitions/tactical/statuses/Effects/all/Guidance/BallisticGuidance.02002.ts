import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	hasMoved: boolean;
	accumulatedAccuracy: number;
	accumulatedRange: number;
}

const RANGE_INCREMENT = 0.05;
const RANGE_SCALE = 0.15;
const HIT_INCREMENT = 0.08;

export class BallisticGuidanceStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 2002;
	public static readonly baseDuration = -1;
	public static readonly negative = false;
	public static readonly caption = 'Ballistic Guidance';
	public static readonly description = `Unit continuously stacks ${numericAPScaling(
		HIT_INCREMENT,
		AbilityPowerUIToken,
		null,
		true,
	)} Hit Chance and ${numericAPScaling(
		RANGE_SCALE,
		AbilityPowerUIToken,
		RANGE_INCREMENT,
	)} Range every Turn End, whenever it hadn't spent any Movement. If the Effect is removed, lose half of gained bonuses but gain 50% of XP remaining to the next Rank`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect: BallisticGuidanceStatusEffect) => {
			if (!effect.state?.accumulatedAccuracy || !effect.state?.accumulatedRange)
				return {
					accumulatedAccuracy: 0,
					accumulatedRange: 0,
					hasMoved: false,
				};
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.move]: (unit, effect: BallisticGuidanceStatusEffect, meta) => {
			if (!meta?.map && !meta?.to) return effect.state;
			return {
				...effect.state,
				hasMoved: true,
			};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: BallisticGuidanceStatusEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseRange: {
						bias: -effect.state.accumulatedRange * 0.5,
						min: 0,
						minGuard: 0,
					},
					baseAccuracy: {
						bias: -effect.state.accumulatedAccuracy * 0.5,
					},
				},
			});
			unit.addXp((unit.nextLevelXp - unit.xp) * 0.5);
			return null;
		},
		[EffectEvents.TUnitEffectEvents.turnStart]: (unit, effect: BallisticGuidanceStatusEffect) => {
			return {
				...effect.state,
				hasMoved: false,
			};
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect: BallisticGuidanceStatusEffect) => {
			if (!effect.state?.hasMoved) return effect.state;
			const bonusRange = RANGE_INCREMENT + RANGE_SCALE * effect.power;
			const bonusHit = HIT_INCREMENT * effect.power;
			unit.applyWeaponModifier({
				default: {
					baseRange: {
						bias: bonusRange,
					},
					baseAccuracy: {
						bias: bonusHit,
					},
				},
			});
			return {
				...effect.state,
				accumulatedAccuracy: (effect.state.accumulatedAccuracy || 0) + bonusHit,
				accumulatedRange: (effect.state.accumulatedRange || 0) + bonusRange,
			};
		},
	};
	public priority = -1;
}

export default BallisticGuidanceStatusEffect;

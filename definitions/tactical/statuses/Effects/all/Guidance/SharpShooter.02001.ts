import { numericAPScaling } from '../../../../../../src/utils/wellRounded';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {
	accumulatedAccuracy: number;
}

const SharpshooterBonus = 0.05;

export class SharpshooterStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 2001;
	public static readonly negative = false;
	public static readonly unremovable = false;
	public static readonly caption = 'Sharpshooter firmware';
	public static readonly description = `This unit stacks ${numericAPScaling(
		SharpshooterBonus,
		AbilityPowerUIToken,
	)} Hit Chance every Turn, which is lost if the Effect is removed`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.turnStart]: (unit, effect: SharpshooterStatusEffect) => {
			const v = SharpshooterBonus * effect.power;
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: v,
					},
				},
			});
			return {
				...effect.state,
				accumulatedAccuracy: (effect.state.accumulatedAccuracy || 0) + v,
			};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect: SharpshooterStatusEffect) => {
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: -effect.state.accumulatedAccuracy,
					},
				},
			});
			return {
				...effect.state,
				accumulatedAccuracy: 0,
			};
		},
	};
	public priority = -1;
}

export default SharpshooterStatusEffect;

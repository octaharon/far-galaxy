import { describePercentage, numericAPScaling } from '../../../../../../src/utils/wellRounded';
import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const MainframeGuidanceDodge = 0.3;
const MainframeGuidanceHitChance = 0.4;
const MainframeGuidanceAttackRate = 0.5;
const MainframeGuidanceXP = 15;
const MainframeGuidanceDamageRate = 0.15;

export class MainframeGuidanceStatusEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 2005;
	public static readonly baseDuration = 2;
	public static readonly negative = false;
	public static readonly caption = 'Mainframe Guidance';
	public static readonly description = `Unit gains extra ${numericAPScaling(
		MainframeGuidanceDodge,
		AbilityPowerUIToken,
		null,
		true,
	)} Dodge, ${numericAPScaling(
		MainframeGuidanceHitChance,
		AbilityPowerUIToken,
		null,
		true,
	)} Hit Chance and ${numericAPScaling(
		MainframeGuidanceAttackRate,
		AbilityPowerUIToken,
	)} Attack Rate for 2 Turns. Landing a Kill under Effect Duration grants additional ${AbilityPowerUIToken}${MainframeGuidanceXP} XP. However, overstretching makes the Unit take ${describePercentage(
		MainframeGuidanceDamageRate,
	)} of its Max Hit Points as EMG Damage at Turn End`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.apply]: (unit, effect) => {
			unit.applyChassisModifier({
				dodge: {
					default: {
						bias: MainframeGuidanceDodge * effect.power,
					},
				},
			});
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: MainframeGuidanceHitChance,
					},
					baseRate: {
						bias: MainframeGuidanceAttackRate,
					},
				},
			});
			return {};
		},
		[EffectEvents.TUnitEffectEvents.expire]: (unit, effect) => {
			unit.applyChassisModifier({
				dodge: {
					default: {
						bias: -MainframeGuidanceDodge * effect.power,
					},
				},
			});
			unit.applyWeaponModifier({
				default: {
					baseAccuracy: {
						bias: -MainframeGuidanceHitChance,
					},
					baseRate: {
						bias: -MainframeGuidanceAttackRate,
						min: MainframeGuidanceAttackRate,
						minGuard: MainframeGuidanceAttackRate,
					},
				},
			});
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.turnEnd]: (unit, effect) => {
			unit.applyAttackUnit(
				{
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.magnetic]: unit.maxHitPoints * MainframeGuidanceDamageRate,
				},
				effect.source,
			);
			return effect.state;
		},
		[EffectEvents.TUnitEffectEvents.unitKill]: (unit, effect) => {
			unit.addXp(MainframeGuidanceXP * effect.power);
			return effect.state;
		},
	};
	public priority = -1;
}

export default MainframeGuidanceStatusEffect;

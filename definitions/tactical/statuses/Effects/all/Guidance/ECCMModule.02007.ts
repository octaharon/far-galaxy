import UnitAttack from '../../../../damage/attack';
import Damage from '../../../../damage/damage';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import GenericStatusEffect, { ConstantUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

const ECCMDamage = 50;

export class ECCMModuleStatusEffect extends ConstantUnitStatusEffect<IState> {
	public static readonly id = 2007;
	public static readonly negative = false;
	public static readonly caption = 'ECCM Module';
	public static readonly unremovable = true;
	public static readonly description = `Whenever the Unit is targeted by a Hostile Ability, the invoking Unit immediately takes ${AbilityPowerUIToken}${ECCMDamage} EMG Damage`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<
		StatusEffectMeta.effectAgentTypes.unit,
		IState
	> = {
		[EffectEvents.TUnitEffectEvents.targetedWithAbility]: (unit, effect: ECCMModuleStatusEffect, meta?) => {
			const source = meta?.source;
			if (source && GenericStatusEffect.isUnitTarget(source) && source.playerId !== unit.playerId) {
				source.applyAttackUnit(
					{
						delivery: UnitAttack.deliveryType.immediate,
						[Damage.damageType.magnetic]: ECCMDamage,
					},
					unit,
					{
						factor: unit.abilityPower,
					},
				);
			}
			return {};
		},
	};
	public priority = -1;
}

export default ECCMModuleStatusEffect;

import BasicMaths from '../../../../../maths/BasicMaths';
import { AbilityPowerUIToken } from '../../../constants';
import EffectEvents from '../../../effectEvents';
import { GenericUnitStatusEffect } from '../../../GenericStatusEffect';
import StatusEffectMeta from '../../../statusEffectMeta';

interface IState {}

export class GroundingStatusEffect extends GenericUnitStatusEffect<IState> {
	public static readonly id = 2009;
	public static readonly baseDuration = -1;
	public static readonly unremovable = true;
	public static readonly negative = false;
	public static readonly caption = 'Grounding Device';
	public static readonly description = `Every time the Unit is a target of an Ability, its own Abilities have their Cooldown decreased by ${AbilityPowerUIToken}1 Turn, if applicable`;
	public static readonly effect: EffectEvents.TEffectScriptDefinition<StatusEffectMeta.effectAgentTypes.unit, {}> = {
		[EffectEvents.TUnitEffectEvents.targetedWithAbility]: (unit, effect: GroundingStatusEffect) => {
			(unit.kit.getOrderedAbilities() || []).forEach((a) => {
				a.cooldownLeft = BasicMaths.applyModifier(a.cooldownLeft, {
					bias: -1 * effect.power,
					min: 0,
				});
			});
			return {};
		},
	};
	public priority = -1;
}

export default GroundingStatusEffect;

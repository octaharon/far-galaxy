import GenericStatusEffect from '../GenericStatusEffect';
import { TStatusEffect } from '../types';

let EffectList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), EffectList);
EffectList = EffectList.filter(
	(module: any) => module.default.prototype instanceof GenericStatusEffect && module.default.id > 0,
).map((module: any) => module.default);
export default EffectList as Array<TStatusEffect<any>>;

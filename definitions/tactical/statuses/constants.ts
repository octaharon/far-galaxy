export const DOTStatusEffectId = 1002;
export const DeferredStatusEffectId = 1001;
export const DEFAULT_EFFECT_POWER = 1.0;

export const AbilityPowerUIToken = `AP × `;
export const ArrowUPUIToken = '🠕';
export const ArrowDoubleUPUIToken = '⮅';
export const ArrowDownUIToken = '🠗';
export const ArrowDoubleDownUIToken = '⮇';

export const MinBoundaryUIToken = '↑';
export const MaxBoundaryUIToken = '↓';

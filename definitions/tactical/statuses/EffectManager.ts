import { CollectibleFactory, TCollectible } from '../../core/Collectible';
import { DIInjectableSerializables } from '../../core/DI/injections';
import { IPlayerBase } from '../../orbital/base/types';
import Units from '../units/types';
import { DEFAULT_EFFECT_POWER } from './constants';
import EffectEvents from './effectEvents';
import EffectList from './Effects/index';
import GenericStatusEffect from './GenericStatusEffect';
import StatusEffectMeta from './statusEffectMeta';
import { IStatusEffect, IStatusEffectFactory, IStatusEffectStatic, TEffectProps, TSerializedEffect } from './types';

const effects = EffectList;

export default class EffectManager
	extends CollectibleFactory<TEffectProps<any>, TSerializedEffect<any>, IStatusEffect<any>>
	implements IStatusEffectFactory
{
	public globalDurationModifier: IModifier = {
		min: -1,
	};
	#initializeTurn = 0;
	#globalTurn = 0;
	#tick = 0;

	public constructor(turnIndex = 0) {
		super();
		this.#initializeTurn = turnIndex;
	}

	public get currentTurn() {
		return this.#globalTurn - this.#initializeTurn;
	}

	public get globalTurn() {
		return this.#globalTurn;
	}

	public clone<T extends StatusEffectMeta.effectAgentTypes>(e: IStatusEffect<T>): IStatusEffect<T> {
		return super.clone(e).setTarget(e.target).setSource(e.source);
	}

	/**
	 * Set local timelime turn
	 * @param global - the global turn id
	 * @param localStart - local start of timelime, defaults to 0
	 */
	public setTurn(global: number, localStart?: number): void {
		this.#globalTurn = global;
		this.#initializeTurn = localStart || this.#initializeTurn || 0;
	}

	/**
	 * Set a tick inside turn
	 * @param tick
	 */
	setTick(tick: number): void {
		this.#tick = tick || 0;
	}

	getTurnMeta(): EffectEvents.TGlobalEffectMeta {
		return {
			turnIndex: this.currentTurn,
			globalTurnIndex: this.globalTurn,
			turnTick: this.#tick,
		};
	}

	public getSerializedFromTarget<
		T extends StatusEffectMeta.effectAgentTypes,
		O extends StatusEffectMeta.TTargetType<T>,
	>(t: O): StatusEffectMeta.TSerializedStatusEffectTarget<T> {
		if (!t) return null;
		if (GenericStatusEffect.isUnitTarget(t))
			return {
				type: StatusEffectMeta.effectAgentTypes.unit,
				unitId: t.getInstanceId(),
			} as StatusEffectMeta.TSerializedStatusEffectTarget<T>;
		if (GenericStatusEffect.isPlayerTarget(t))
			return {
				type: StatusEffectMeta.effectAgentTypes.player,
				baseId: t.getInstanceId(),
			} as StatusEffectMeta.TSerializedStatusEffectTarget<T>;
		if (GenericStatusEffect.isMapTarget(t)) {
			return {
				type: StatusEffectMeta.effectAgentTypes.map,
				mapCellId: t.getInstanceId(),
			} as StatusEffectMeta.TSerializedStatusEffectTarget<T>;
		}
		return null;
	}

	public getTargetFromSerialized(
		t: StatusEffectMeta.TSerializedStatusEffectTarget<StatusEffectMeta.effectAgentTypes.unit>,
	): StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.unit>;

	public getTargetFromSerialized(
		t: StatusEffectMeta.TSerializedStatusEffectTarget<StatusEffectMeta.effectAgentTypes.player>,
	): StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.player>;

	public getTargetFromSerialized(
		t: StatusEffectMeta.TSerializedStatusEffectTarget<StatusEffectMeta.effectAgentTypes.map>,
	): StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.map>;
	public getTargetFromSerialized(
		t: StatusEffectMeta.TSerializedStatusEffectTarget<any>,
	): StatusEffectMeta.TTargetType<any> {
		if (!t) return null;
		if (this.isSerializedPlayer(t)) {
			return this.getProvider(DIInjectableSerializables.base).find<IPlayerBase>(t.baseId);
		}
		if (this.isSerializedUnit(t)) {
			return this.getProvider(DIInjectableSerializables.unit).find<Units.IUnit>(t.unitId);
		}
		if (this.isSerializedMapCell(t)) {
			return this.getProvider(DIInjectableSerializables.mapcell).find(t.mapCellId);
		}
	}

	public instantiate<T extends StatusEffectMeta.effectAgentTypes, O extends IStatusEffect<T> = IStatusEffect<T>>(
		props: Partial<TEffectProps<T>>,
		id: number,
		source?: StatusEffectMeta.TTargetType<any>,
		target?: StatusEffectMeta.TTargetType<T>,
	): O {
		props = {
			power: Number.isFinite(props?.power) ? props.power : DEFAULT_EFFECT_POWER,
			...(props ?? {}),
		};
		const effectStatic = this.get<TCollectible<IStatusEffect<any>, IStatusEffectStatic<any>>>(id);
		props.meta = {
			source: this.getSerializedFromTarget(source),
			target: this.getSerializedFromTarget(target),
			turnAdded: this.getTurnMeta().globalTurnIndex,
			turnTick: this.getTurnMeta().turnIndex - this.getTurnMeta().globalTurnIndex,
			phaseTick: this.getTurnMeta().turnTick,
			basePower: Math.max(0, props?.meta?.basePower ?? props?.power ?? DEFAULT_EFFECT_POWER),
			baseDuration: effectStatic.durationFixed
				? effectStatic.baseDuration
				: props.duration ?? effectStatic.baseDuration ?? 1,
			modifier: effectStatic.isUnitTarget(source)
				? source.effectModifiers
				: effectStatic.isPlayerTarget(source)
				? {
						abilityPowerModifier: [{ factor: source.currentEconomy.abilityPower }],
				  }
				: effectStatic.isMapTarget(source)
				? {} //@TODO planet modifiers
				: null,
			...(props?.meta ?? {}),
		};
		const e = this.__instantiateCollectible(props, id);
		if (source) e.setSource(source);
		if (target) e.setTarget(target);
		return e as O;
	}

	public unserialize(t: TSerializedEffect<any>) {
		return this.instantiate(t, t.id);
	}

	public find<T extends StatusEffectMeta.effectAgentTypes, O extends IStatusEffect<T> = GenericStatusEffect<T>>(
		instanceId: string,
	): O {
		return (this.__objectCache[instanceId] as O) || null;
	}

	public fill() {
		const invalidEffects = effects.filter(
			(e) => !e.targetType || !Object.values(StatusEffectMeta.effectAgentTypes).includes(e.targetType),
		);
		if (invalidEffects.length)
			throw new Error(`Invalid target type for Effects ${invalidEffects.map((e) => e.id).join(', ')}`);
		this.__fillItems(effects);
		return this;
	}

	protected isSerializedUnit(
		t: StatusEffectMeta.TSerializedStatusEffectTarget<any>,
	): t is StatusEffectMeta.TSerializedStatusEffectTarget<StatusEffectMeta.effectAgentTypes.unit> {
		return t.type === StatusEffectMeta.effectAgentTypes.unit;
	}

	protected isSerializedPlayer(
		t: StatusEffectMeta.TSerializedStatusEffectTarget<any>,
	): t is StatusEffectMeta.TSerializedStatusEffectTarget<StatusEffectMeta.effectAgentTypes.player> {
		return t.type === StatusEffectMeta.effectAgentTypes.player;
	}

	protected isSerializedMapCell(
		t: StatusEffectMeta.TSerializedStatusEffectTarget<any>,
	): t is StatusEffectMeta.TSerializedStatusEffectTarget<StatusEffectMeta.effectAgentTypes.map> {
		return t.type === StatusEffectMeta.effectAgentTypes.map;
	}
}

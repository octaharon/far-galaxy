import {
	ICollectible,
	ICollectibleFactory,
	IStaticCollectible,
	TCollectible,
	TSerializedCollectible,
} from '../../core/Collectible';
import { TPlayerId } from '../../player/playerId';
import { IUnitEffectModifier } from '../../prototypes/types';
import EffectEvents from './effectEvents';
import StatusEffectMeta from './statusEffectMeta';

export type TSerializedEffect<
	TargetType extends StatusEffectMeta.effectAgentTypes,
	StateType extends object = {},
> = TEffectProps<TargetType, StateType> & TSerializedCollectible;
export type IEffectMeta<TargetType extends StatusEffectMeta.effectAgentTypes> = Partial<{
	source: StatusEffectMeta.TSerializedStatusEffectTarget<any>; // Map, Base or Unit
	target: StatusEffectMeta.TSerializedStatusEffectTarget<TargetType>;
	turnAdded: number; // Turn it was added
	turnTick: number; // number of Turns it exists on target
	phaseTick: number; // number of Ticks during current turn it exists on target
	basePower: number; // power at initialization
	baseDuration: number; // base at initialization
	modifier: IUnitEffectModifier; // inherited from Map and (Base or Unit) @TODO
}>;
export type TEffectProps<
	TargetType extends StatusEffectMeta.effectAgentTypes,
	StateType extends object = any,
> = Partial<{
	state: Partial<StateType>; // every effect declares its state type
	priority: number;
	power: number;
	duration: number;
	meta?: Partial<IEffectMeta<TargetType>>;
}>;
export type TEffectApplianceItem<EffectPropType extends {} = {}> = {
	effectId: number;
	props?: EffectPropType;
};
export type TEffectApplianceList<EffectPropType extends {} = {}> = Array<TEffectApplianceItem<EffectPropType>>;

export type IStatusEffectTypedDecorator<T extends StatusEffectMeta.effectAgentTypes> = {
	readonly targetType: T;
};

export type IStatusEffectStatic<
	T extends StatusEffectMeta.effectAgentTypes,
	StateType extends object = {},
> = IStaticCollectible &
	IStatusEffectTypedDecorator<T> & {
		readonly baseDuration: number; // -1 means infinite
		readonly durationFixed: boolean; // if true, duration modifier is not applied
		readonly negative: boolean;
		readonly unremovable: boolean;
		readonly effect: EffectEvents.TEffectScriptDefinition<T, StateType>;

		applyDurationModifier(duration: number, d: IModifier): number;

		isPlayerTarget(
			o: StatusEffectMeta.TTargetType<any>,
		): o is StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.player>;

		isUnitTarget(
			o: StatusEffectMeta.TTargetType<any>,
		): o is StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.unit>;

		isMapTarget(
			o: StatusEffectMeta.TTargetType<any>,
		): o is StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.map>;
	};

export interface IStatusEffect<T extends StatusEffectMeta.effectAgentTypes, StateType extends object = any>
	extends TEffectProps<T>,
		ICollectible<TEffectProps<T>, TSerializedEffect<T>, IStatusEffectStatic<T>> {
	source: StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes>;
	target: StatusEffectMeta.TTargetType<T>;

	setSource(source: StatusEffectMeta.TTargetType<any>): this;

	setTarget(source: StatusEffectMeta.TTargetType<T>): this;

	triggerEvent<E extends EffectEvents.TEffectEventList<T>>(
		event: E,
		meta?: Partial<EffectEvents.TEffectEventMeta<T, E>>,
		target?: StatusEffectMeta.TTargetType<T>,
	): StateType;

	applyModifier(mod: IUnitEffectModifier, save?: boolean): this;

	doesBelongToPlayer(playerId: TPlayerId): boolean;
}

export interface IStatusEffectFactory
	extends ICollectibleFactory<
			TEffectProps<any>,
			TSerializedEffect<any>,
			IStatusEffect<any>,
			IStatusEffectStatic<any>
		>,
		StatusEffectMeta.IStatusEffectSerializerProvider {
	currentTurn: number;
	globalTurn: number;
	globalDurationModifier: IModifier;

	clone<T extends StatusEffectMeta.effectAgentTypes>(e: IStatusEffect<T>): IStatusEffect<T>;

	setTurn(global: number, localStart?: number): void;

	setTick(tick: number): void;

	getTurnMeta(): EffectEvents.TGlobalEffectMeta;

	instantiate<T extends StatusEffectMeta.effectAgentTypes, O extends IStatusEffect<T> = IStatusEffect<T>>(
		props: Partial<TEffectProps<T>>,
		id: number,
		source?: StatusEffectMeta.TTargetType<any>,
		target?: StatusEffectMeta.TTargetType<T>,
	): O;
}

export type TStatusEffectStateLookup<T extends IStatusEffectStatic<any, any>> = T extends IStatusEffectStatic<
	any,
	infer X
>
	? X
	: any;

export type TStatusEffect<
	T extends StatusEffectMeta.effectAgentTypes,
	StateType extends object = {},
	ObjectType extends IStatusEffect<T, StateType> = IStatusEffect<T, StateType>,
> = TCollectible<ObjectType, IStatusEffectStatic<T, StateType>>;
export type TStatusEffectList<T extends StatusEffectMeta.effectAgentTypes> = Array<TStatusEffect<T>>;

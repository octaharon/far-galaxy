import { ISerializable } from '../../core/Serializable';
import { IPlayerBase } from '../../orbital/base/types';
import { IMapCell } from '../map';
import Units from '../units/types';
import EffectEvents from './effectEvents';
import { IStatusEffect, TEffectApplianceItem, TSerializedEffect } from './types';

export const MAX_EFFECT_DURATION = 12;

namespace StatusEffectMeta {
	export enum effectAgentTypes {
		unit = 'eff_unit', // effect applies to a unit
		player = 'eff_player', // effect applies to a player base
		map = 'eff_map', // effect applies to a map point
	}

	interface TTargetMapping {
		[effectAgentTypes.unit]: Units.IUnit;
		[effectAgentTypes.player]: IPlayerBase;
		[effectAgentTypes.map]: IMapCell;
	}

	export type TTargetType<T extends effectAgentTypes> = TTargetMapping[T];
	export type TTargetTypeOption = ValueOf<{
		[T in effectAgentTypes]: TTargetType<T>;
	}>;

	export type TStatusEffectTarget = TTargetTypeOption;

	export type TSerializedStatusEffectTarget<T extends effectAgentTypes> = T extends effectAgentTypes.player
		? { type: effectAgentTypes.player; baseId: string }
		: T extends effectAgentTypes.unit
		? { type: effectAgentTypes.unit; unitId: Units.TUnitId }
		: T extends effectAgentTypes.map
		? { type: effectAgentTypes.map; mapCellId: string }
		: never;

	export type TSerializedEffectOwner<T extends effectAgentTypes> = IWithInstanceID & {
		effects: Array<TSerializedEffect<T>>;
	};

	export type TEffectOwner<T extends effectAgentTypes> = {
		effects: Array<IStatusEffect<T>>;
	};

	export interface IStatusEffectSerializerProvider {
		getSerializedFromTarget<T extends effectAgentTypes, O extends TTargetType<T>>(
			t: O,
		): TSerializedStatusEffectTarget<T>;

		getTargetFromSerialized<T extends effectAgentTypes>(t: TSerializedStatusEffectTarget<T>): TTargetType<T>;
	}

	export interface IStatusEffectTarget<
		PropType extends {},
		SerializedType extends TSerializedEffectOwner<TargetType>,
		TargetType extends effectAgentTypes = any,
	> extends ISerializable<PropType, SerializedType>,
			TEffectOwner<TargetType> {
		readonly effectTargetType: TargetType;

		applyEffect(eff: TEffectApplianceItem, source?: TStatusEffectTarget): this;

		getStatusEffects(): Array<IStatusEffect<TargetType>>;

		hasEffect(effectId: number): boolean;

		addEffect(effect: IStatusEffect<TargetType>): this;

		removeEffect(effectId: string): this;

		callEffectEvent<T extends EffectEvents.TEffectEventList<TargetType>>(
			payload?: EffectEvents.TEffectEventMeta<TargetType, T>,
		): this;
	}

	export type TEffectTargetFlag<T extends TStatusEffectTarget> = T extends TTargetType<infer X> ? X : never;
	export type TSerializedTargetMapping<T extends TSerializedStatusEffectTarget<any>> =
		T extends TSerializedStatusEffectTarget<infer X> ? TTargetType<X> : never;

	export type TEffectAgents = EnumProxy<effectAgentTypes, boolean>;

	export type TEffectDefinitionId = number;
}

export default StatusEffectMeta;

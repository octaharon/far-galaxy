import { ISerializable, ISerializableFactory } from '../../core/Serializable';
import { TPlayerId } from '../../player/playerId';
import Players from '../../player/types';
import { IPrototype } from '../../prototypes/types';
import Resources from '../../world/resources/types';
import { ILocationOnMap } from '../map';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import { TEffectApplianceItem } from '../statuses/types';

export namespace Salvage {
	export type TSalvagePayload = {
		technologies?: Players.TPlayerTechnologyPayload;
		resources?: Resources.TResourcePayload;
		rawMaterials?: number;
		effectsOnPickup?: TEffectApplianceItem[];
	};

	export type TSalvageProps = {
		payload: TSalvagePayload;
		playerId: TPlayerId;
		location: ILocationOnMap;
	};

	export type TSerializedSalvage = TSalvageProps & IWithInstanceID;

	export interface ISalvage extends TSalvageProps, ISerializable<TSalvageProps, TSerializedSalvage> {}

	export interface ISalvageFactory extends ISerializableFactory<TSalvageProps, TSerializedSalvage, ISalvage> {
		createFromDestroyedUnit(
			destroyedUnitPrototype: IPrototype,
			source: StatusEffectMeta.TStatusEffectTarget,
			withTechnology?: boolean,
			withResources?: boolean,
			withRawMaterials?: boolean,
		): ISalvage;
	}
}

export default Salvage;

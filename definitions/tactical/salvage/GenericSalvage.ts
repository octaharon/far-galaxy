import { DIEntityDescriptors, DIInjectableSerializables } from '../../core/DI/injections';
import Serializable from '../../core/Serializable';
import { TPlayerId } from '../../player/playerId';
import { ILocationOnMap } from '../map';
import Salvage from './types';

export default class GenericSalvage
	extends Serializable<Salvage.TSalvageProps, Salvage.TSerializedSalvage>
	implements Salvage.ISalvage
{
	public payload: Salvage.TSalvagePayload = {};
	public playerId: TPlayerId;
	public location: ILocationOnMap;

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableSerializables.salvage];
	}

	public serialize() {
		return {
			...this.__serializeSerializable(),
			playerId: this.playerId,
			payload: this.payload,
			location: this.location,
		} as Salvage.TSerializedSalvage;
	}
}

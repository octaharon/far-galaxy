import { SerializableFactory } from '../../core/Serializable';
import { applyDistribution } from '../../maths/Distributions';
import { SALVAGE_RAW_MATERIALS_FACTOR } from '../../orbital/base/constants';
import Players from '../../player/types';
import { IPrototype } from '../../prototypes/types';
import GenericArmor from '../../technologies/armor/GenericArmor';
import GenericChassis from '../../technologies/chassis/genericChassis';
import GenericEquipment from '../../technologies/equipment/GenericEquipment';
import GenericShield from '../../technologies/shields/GenericShield';
import UnitChassis from '../../technologies/types/chassis';
import GenericWeapon from '../../technologies/weapons/GenericWeapon';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import { UNIT_MAP_SIZE, UNIT_MAP_SIZE_MASSIVE } from '../units/types';
import { IUnitPart } from '../units/UnitPart';
import GenericSalvage from './GenericSalvage';
import Salvage from './types';

let __instance: SalvageManager = null;

export default class SalvageManager
	extends SerializableFactory<Salvage.TSalvageProps, Salvage.TSerializedSalvage, Salvage.ISalvage>
	implements Salvage.ISalvageFactory
{
	public static i(): SalvageManager {
		if (!__instance) {
			__instance = new SalvageManager();
		}
		return __instance;
	}

	public instantiate(props: Partial<Salvage.TSalvageProps>) {
		const c = this.__instantiateSerializable(props, GenericSalvage);
		return c;
	}

	public unserialize(t: Salvage.TSerializedSalvage) {
		const c = this.instantiate(t);
		c.setInstanceId(t.instanceId);
		return c;
	}

	public createFromDestroyedUnit(
		destroyedUnitPrototype: IPrototype,
		source: StatusEffectMeta.TStatusEffectTarget,
		withTechnology = true,
		withResources = false,
		withRawMaterials = true,
	): Salvage.ISalvage {
		const payload = {} as Salvage.TSalvagePayload;
		const techPayload = {} as Players.TPlayerTechnologyPayload;
		let techScramble: Array<IUnitPart<any, any, any>> = [];
		techScramble.push(destroyedUnitPrototype.chassis);
		techScramble.concat(Object.values(destroyedUnitPrototype.weapons));
		techScramble.concat(destroyedUnitPrototype.armor);
		techScramble.concat(destroyedUnitPrototype.shields);
		techScramble.concat(destroyedUnitPrototype.equipment);
		techScramble.sort(() => Math.random() - 0.5);
		techScramble = techScramble.filter((unitPart) => !!unitPart.static.salvageable);
		const pickNumber = Math.min(
			techScramble.length,
			Math.round(
				applyDistribution({
					min: 1,
					max: 2 + Math.log(destroyedUnitPrototype.cost) / Math.log(10),
					distribution: 'Bell',
				}),
			),
		); // amount of technology items salvaged, depends on unit cost and composition

		// more expensive units grant more percentile
		const techCostModifier = destroyedUnitPrototype.cost / 1000;

		/**
		 * Every technology grants:
		 *  2% - 16% of availability for units with cost = 10
		 *  6% - 20% of availability for units with cost = 50
		 *  11% - 20% of availability for units with cost = 100
		 *  21% - 25% of availability for units with cost = 200
		 *  29% of availability for units with cost >= 280
		 */
		techScramble.slice(0, pickNumber).forEach((t: any) => {
			const techValue = applyDistribution({
				min: 0.01 + techCostModifier,
				max: Math.max(0.01 + techCostModifier, Math.min(1, 0.15 + techCostModifier / 2)),
				distribution: 'MostlyMinimal',
			});
			switch (true) {
				case t instanceof GenericChassis:
					techPayload.chassis = {
						[t.static.id]: techValue,
					};
					return;
				case t instanceof GenericWeapon:
					techPayload.weapons = Object.assign(techPayload.weapons || {}, {
						[t.static.id]: techValue,
					});
					return;
				case t instanceof GenericArmor:
					techPayload.armor = Object.assign(techPayload.armor || {}, {
						[t.static.id]: techValue,
					});
					return;
				case t instanceof GenericShield:
					techPayload.shield = Object.assign(techPayload.shield || {}, {
						[t.static.id]: techValue,
					});
					return;
				case t instanceof GenericEquipment:
					techPayload.equipment = Object.assign(techPayload.equipment || {}, {
						[t.static.id]: techValue,
					});
					return;
				default:
					return;
			}
		});
		if (withTechnology) payload.technologies = techPayload;
		/** @TODO resources and effects on applyEffects */
		if (withRawMaterials)
			payload.rawMaterials = applyDistribution({
				min: 1,
				max: destroyedUnitPrototype.cost * SALVAGE_RAW_MATERIALS_FACTOR,
				distribution: 'MostlyMaximal',
			});
		return this.instantiate({
			payload,
			playerId: source.playerId,
			location: {
				mapId: null,
				x: 0,
				y: 0,
				radius:
					(Math.random() * UNIT_MAP_SIZE) / 2 +
					(destroyedUnitPrototype.chassis.static.flags[UnitChassis.targetType.massive]
						? UNIT_MAP_SIZE_MASSIVE
						: UNIT_MAP_SIZE),
			},
		});
	}
}

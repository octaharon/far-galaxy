import InterfaceTypes from '../../core/InterfaceTypes';
import { ISerializableFactory } from '../../core/Serializable';
import { IPlayerProperty, IPlayerPropertyProps } from '../../player/types';
import { IPrototype, IPrototypeModifier, IUnitEffectModifier } from '../../prototypes/types';
import GameStats from '../../stats/types';
import UnitChassis from '../../technologies/types/chassis';
import Weapons from '../../technologies/types/weapons';
import Abilities from '../abilities/types';
import UnitAttack from '../damage/attack';
import UnitDefense from '../damage/defense';
import { ILocationOnMap } from '../map';
import Salvage from '../salvage/types';
import { IAura, TSerializedAura } from '../statuses/Auras/types';
import EffectEvents from '../statuses/effectEvents';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import UnitActions from './actions/types';
import UnitTalents from './levels/unitTalents';

export const UNIT_MAP_SIZE = 0.1;
export const UNIT_MAP_SIZE_MASSIVE = 0.25;

export namespace Units {
	import IWithAbilityKit = Abilities.IWithAbilityKit;
	export type TUnitId = string;

	export type TUnitCombatStatus = {
		targetable: boolean; // can be targeted and damaged by direct attacks
		disabled: boolean; // can't act, and effects don't tick
		cloaked: boolean; // can't be targeted with direct attacks and invisible to orbital scanner
		moving: InterfaceTypes.IVector; // unit is moving
		currentTurnTick: number;
		currentTurnAttackFrames: IBounds[][];
		commandChain: UnitActions.TUnitActionCommandOption[];
		currentActionIndex: number;
	};

	export type TUnitState = {
		prototypeId: string;
		currentHitPoints: number;
		maxHitPoints: number;
		currentMovementPoints: number;
		speed: number;
		scanRange: number;
		cost: number;
		status: TUnitCombatStatus;
		chassisType: UnitChassis.chassisClass;
		targetFlags: UnitChassis.TTargetFlags;
		movement: UnitChassis.movementType;
		attacks: UnitAttack.IAttackInterface[];
		defense: UnitDefense.IDefenseInterface;
		actions: UnitActions.TUnitActionOptions[];
		actionDelayed: UnitActions.TUnitActionOptions | null;
		xp: number;
		level: number;
		location: ILocationOnMap;
		abilitiesUsed: boolean[];
		orbitalAttacksPerformed: boolean[];
		talentPoints: number;
		talentsLearned: UnitTalents.TUnitTalentId[];
		effectModifiers: IUnitEffectModifier;
		stats: GameStats.TUnitStats;
	};

	export type TUnitSerializedState = Abilities.ISerializedWithAbilityKit & {
		auras: TSerializedAura[];
	};
	export type TUnitDynamicState = IWithAbilityKit & {
		auras: IAura[];
	};

	export type TUnitPropsStatic = TUnitState & IPlayerPropertyProps;
	export type TUnitProps = TUnitPropsStatic &
		TUnitDynamicState &
		StatusEffectMeta.TEffectOwner<StatusEffectMeta.effectAgentTypes.unit>;

	export type TSerializedUnit = TUnitSerializedState &
		TUnitPropsStatic &
		StatusEffectMeta.TSerializedEffectOwner<StatusEffectMeta.effectAgentTypes.unit>;

	export type TUnitDestroyedCallback = (
		source: StatusEffectMeta.TStatusEffectTarget,
		unit: IUnit,
		salvage: Salvage.ISalvage,
	) => any;

	export type IUnit = TUnitProps &
		IPlayerProperty &
		Abilities.IWithAbilityKit &
		StatusEffectMeta.IStatusEffectTarget<
			TUnitProps,
			Units.TSerializedUnit,
			StatusEffectMeta.effectAgentTypes.unit
		> & {
			isRobotic: boolean;
			isMechanical: boolean;
			isOrganic: boolean;
			isMassive: boolean;
			isUnique: boolean;
			isGround: boolean;
			isHover: boolean;
			isNaval: boolean;
			isAirborne: boolean;
			attackFramesLeft: IBounds[][];
			getEffectiveActions(): UnitActions.TUnitActionOptions[];
			canPerformOrbitalAttack(): boolean;
			// @TODO antipattern, refactor through event chain
			setDestroyedCallback(callback: TUnitDestroyedCallback): IUnit;
			fillAttackFrames(): IUnit;
			applyArmorModifier(m: UnitDefense.TArmorModifier): IUnit;
			applyShieldsModifier(m: UnitDefense.TShieldsModifier): IUnit;
			applyChassisModifier(m: UnitChassis.TUnitChassisModifier): IUnit;
			applyWeaponModifier(m: Weapons.TWeaponGroupModifier, weaponIndex?: number): IUnit;
			applyPrototypeModifier(m: IPrototypeModifier): IUnit;
			applyEffectModifier(...m: IUnitEffectModifier[]): IUnit;
			addShields(amount: number, shieldIndex?: number): IUnit;
			addXp(xp: number): Units.IUnit;
			hasDefensePerks(...perks: UnitDefense.defenseFlags[]): boolean;
			addDefensePerks(...perks: UnitDefense.defenseFlags[]): Units.IUnit;
			removeDefensePerks(...perks: UnitDefense.defenseFlags[]): Units.IUnit;
			isTalentAvailable(talentId: UnitTalents.TUnitTalentId): boolean;
			learnTalent(talentId: UnitTalents.TUnitTalentId): Units.IUnit;
			addHp(health: number, source?: StatusEffectMeta.TStatusEffectTarget): Units.IUnit;
			destroy(source: StatusEffectMeta.TStatusEffectTarget, withSalvage?: boolean): Units.IUnit;
			applyAttackBatch(a: UnitAttack.IAttackBatch, source: StatusEffectMeta.TStatusEffectTarget): boolean;
			applyAttackUnit(
				a: UnitAttack.TAttackUnit,
				source: StatusEffectMeta.TStatusEffectTarget,
				damageModifier?: IModifier,
			): UnitDefense.IAttackEffectOnUnit;
			hasPerks(...flags: UnitDefense.defenseFlags[]): boolean;
			hasEnoughMovementToPerform(
				action: UnitActions.IUnitAction<
					| UnitActions.unitActionTypes.move
					| UnitActions.unitActionTypes.attack_move
					| UnitActions.unitActionTypes.blink
				>,
			): boolean;
			moveTo(
				location: ILocationOnMap | null,
				action?: UnitActions.IUnitAction<
					| UnitActions.unitActionTypes.move
					| UnitActions.unitActionTypes.attack_move
					| UnitActions.unitActionTypes.blink
				>,
			): Units.IUnit;
			getEventContextMeta(unit?: IUnit): EffectEvents.TUnitContextEffectMeta;

			// Calculated properties
			readonly prototype: IPrototype;
			readonly isTargetable: boolean;
			readonly isDisabled: boolean;
			readonly isMovable: boolean;
			readonly isImmuneToEffects: boolean;
			readonly isImmuneToNegativeEffects: boolean;
			readonly totalShields: number;
			readonly currentShields: number;
			readonly abilityPower: number;
			readonly nextLevelXp: number;
		};

	export interface IUnitFactory extends ISerializableFactory<TUnitProps, TSerializedUnit, IUnit> {
		createFromPrototype(t: IPrototype): Units.IUnit;
	}
}

export default Units;

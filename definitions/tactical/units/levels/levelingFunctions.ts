import UnitTalents from './unitTalents';

export enum TLevelingFunction {
	Linear50 = 'leveling_l50', // Recruit
	Linear100 = 'leveling_l100', // Squaddie
	Linear150 = 'leveling_l150', // Conscript
	Fast50 = 'leveling_f50', // Savvy
	Fast100 = 'leveling_f100', // Smart
	Fast150 = 'leveling_f150', // Expert
	Prodigy = 'leveling_prodigy', // Specialist
}

export const LevelingFunctions: EnumProxy<TLevelingFunction, UnitTalents.TUnitLevelingSignature> = {
	[TLevelingFunction.Linear50]: (level: number) => Math.max(0, 30 + 60 * (level - 1)), // 30, 90, 150, 210, 270
	// ...
	[TLevelingFunction.Linear100]: (level: number) => Math.max(0, 50 + 80 * (level - 1)), // 50, 130, 210, 290, 370
	// ...
	[TLevelingFunction.Linear150]: (level: number) => Math.max(0, 70 + 100 * (level - 1)), // 70, 170, 270, 370, 470
	// ...
	[TLevelingFunction.Fast50]: (level: number) => level * 80, // 80, 160, 240, 320, 400
	[TLevelingFunction.Fast100]: (level: number) => level * 90, // 90, 180, 270, 360, 450
	[TLevelingFunction.Fast150]: (level: number) => level * 100, // 100, 200, 300, 400, 500
	[TLevelingFunction.Prodigy]: (level: number) => Math.max(0, (level * (64 + (level - 1) * 24)) / 2),
};

export function getLevelingFunction(id: TLevelingFunction): UnitTalents.TUnitLevelingSignature {
	return LevelingFunctions[id];
}

export default getLevelingFunction;

import Units from '../types';

export namespace UnitTalents {
	export type TUnitTalentId = string;
	export type TUnitTalentSignature = (target: Units.IUnit) => Units.IUnit;
	export type TUnitTalentLevelingSignature = (level: number, target: Units.IUnit) => Units.IUnit;
	export type TUnitLevelingSignature = (level: number) => number;

	export interface IUnitTalent {
		talentId: TUnitTalentId;
		minimumLevel: number;
		caption?: string;
		description?: string;
		effect: TUnitTalentSignature;
	}

	export interface IUnitTalentTreeItem {
		talent: IUnitTalent;
		unlocks?: IUnitTalentTreeItem[];
	}

	export interface ITalentTree {
		caption?: string;
		description?: string;
		levelingFunction: TUnitLevelingSignature;
		levelingEffectFunction: TUnitTalentLevelingSignature;
		talentOptions: IUnitTalentTreeItem[];
	}
}

export default UnitTalents;

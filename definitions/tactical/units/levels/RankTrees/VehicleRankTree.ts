import BasicMaths from '../../../../maths/BasicMaths';
import ComputerGuidanceAbility from '../../../abilities/all/GuidanceAbilities/ComputerGuidanceAbility.10002';
import UnitAttack from '../../../damage/attack';
import { ATTACK_PRIORITIES } from '../../../damage/constants';
import DefenseManager from '../../../damage/DefenseManager';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import { grantAction } from '../../actions/ActionManager';
import { UnitActions } from '../../actions/types';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';

export const VehicleRankTree: UnitTalents.ITalentTree = {
	caption: 'Vehicle pilot training',
	description:
		'Be it manned or unmanned vehicle, the crew gets experience which allows them to excel at certain combat tactics. Gains +5% Dodge per Rank',
	levelingFunction: getLevelingFunction(TLevelingFunction.Linear100),
	levelingEffectFunction: (level, unit) => {
		return unit.applyChassisModifier({ dodge: { default: { bias: 0.05 } } });
	},
	talentOptions: [
		{
			talent: {
				talentId: 'vehicle_assault_training_1',
				caption: 'Assault Training',
				description: 'Gain +0.5 Scan Range and +0.5 Speed',
				minimumLevel: 1,
				effect: (unit) =>
					unit.applyChassisModifier({
						scanRange: { bias: 0.5 },
						speed: { bias: 0.5 },
					}),
			},
			unlocks: [
				{
					talent: {
						talentId: 'vehicle_assault_training_2',
						caption: 'Advanced Assault Training',
						description: 'Gain +0.5 Speed and +10% Dodge',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyChassisModifier({
								speed: { bias: 0.5 },
								dodge: { default: { bias: 0.1 } },
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'vehicle_assault_training_3_a',
								caption: 'Targeting Practice',
								description: 'Gain +15% Hit Chance',
								minimumLevel: 5,
								effect: (unit) =>
									unit.applyWeaponModifier({ default: { baseAccuracy: { bias: 0.15 } } }),
							},
						},
						{
							talent: {
								talentId: 'vehicle_assault_training_3_b',
								caption: 'Predictive Engagement',
								description: 'Gain +1 Range and higher Priority on all Weapons',
								minimumLevel: 5,
								effect: (unit) => {
									unit.attacks.forEach((attack) => {
										attack.priority = BasicMaths.applyModifier(attack.priority, {
											bias: ATTACK_PRIORITIES.HIGH,
											min: ATTACK_PRIORITIES.HIGHEST,
										});
										attack.baseRange += 1;
									});
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'vehicle_support_training_1',
				caption: 'Support Training',
				description: `Gain +1 Ability Range and +0.5${AbilityPowerUIToken}`,
				minimumLevel: 1,
				effect: (unit) =>
					unit
						.applyEffectModifier({
							abilityPowerModifier: [{ bias: 1 }],
							abilityRangeModifier: [{ bias: 1 }],
						})
						.applyChassisModifier({ hitPoints: { bias: 10 } }),
			},
			unlocks: [
				{
					talent: {
						talentId: 'vehicle_support_training_2',
						caption: 'Advanced Support Training',
						description: `Gain +1 Scan Range and reduce incoming Effect Duration by 1`,
						minimumLevel: 3,
						effect: (unit) =>
							unit
								.applyChassisModifier({ scanRange: { bias: 1 } })
								.applyEffectModifier({ statusDurationModifier: [{ bias: -1, min: 1, minGuard: 1 }] }),
					},
					unlocks: [
						{
							talent: {
								talentId: 'vehicle_support_training_3_a',
								caption: 'High Maintenance',
								description: 'Restore Hit Points and gain Computer Guidance Ability',
								minimumLevel: 5,
								effect: (unit) => {
									unit.currentHitPoints = unit.maxHitPoints;
									unit.addAbility(ComputerGuidanceAbility.id);
									return unit;
								},
							},
						},
						{
							talent: {
								talentId: 'vehicle_support_training_3_b',
								caption: 'Adaptive Rewiring',
								description:
									'All Abilities are re-enabled and have their Cooldowns reset, while Ability Power is increased by 25%',
								minimumLevel: 5,
								effect: (unit) => {
									unit.kit.getStableSortedAbilities().forEach((ability) => {
										ability.cooldownLeft = 0;
										ability.usable = true;
									});
									unit.applyEffectModifier({
										abilityPowerModifier: [{ factor: 1.25 }],
									});
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'vehicle_defense_training_1',
				caption: 'Defensive training',
				description: 'Gain +5 HP and +25% Dodge against Projectile and Bombardment Attacks',
				minimumLevel: 1,
				effect: (unit) =>
					unit.applyChassisModifier({
						hitPoints: { bias: 5 },
						dodge: {
							[UnitAttack.deliveryType.ballistic]: { bias: 0.25 },
							[UnitAttack.deliveryType.bombardment]: { bias: 0.25 },
						},
					}),
			},
			unlocks: [
				{
					talent: {
						talentId: 'vehicle_defense_training_2',
						caption: 'Advanced defensive training',
						description: 'Gain +15 Hit Points and 50% Dodge against Bombardment and Missile Attacks',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyChassisModifier({
								hitPoints: { bias: 15 },
								dodge: {
									[UnitAttack.deliveryType.missile]: { bias: 0.5 },
									[UnitAttack.deliveryType.bombardment]: { bias: 0.5 },
								},
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'vehicle_defense_training_3_a',
								caption: 'Cover Practice',
								description:
									'Gain 15% Dodge and enable Suppress, Defend and Protect at all Action Phases',
								minimumLevel: 5,
								effect: (unit) => {
									unit.applyChassisModifier({
										dodge: {
											default: { bias: 0.15 },
										},
									});
									unit.actions = grantAction(
										unit.actions,
										UnitActions.unitActionTypes.suppress,
										UnitActions.unitActionTypes.defend,
										UnitActions.unitActionTypes.protect,
									);
									return unit;
								},
							},
						},
						{
							talent: {
								talentId: 'vehicle_defense_training_3_b',
								caption: 'Battle Endurance',
								description: 'Increase Armor Damage Reduction by 5 and increase Shield Capacity by 20%',
								minimumLevel: 5,
								effect: (unit) =>
									unit
										.applyArmorModifier({ default: { bias: -5 } })
										.applyShieldsModifier({ shieldAmount: { factor: 1.2 } }),
							},
						},
					],
				},
			],
		},
	],
};
export default VehicleRankTree;

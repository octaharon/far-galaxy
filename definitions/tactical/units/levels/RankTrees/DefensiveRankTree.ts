import { ArithmeticPrecision } from '../../../../maths/constants';
import UnitAttack from '../../../damage/attack';
import UnitDefense from '../../../damage/defense';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import UnitActions from '../../actions/types';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';
import TShield = UnitDefense.TShield;

export const DefensiveRankTree: UnitTalents.ITalentTree = {
	caption: 'Protector Combat Suite',
	description:
		'Battle AI tailored for stationary defenses and heavily armed units. Gains +3 Damage Reduction on Armor per Rank',
	levelingFunction: getLevelingFunction(TLevelingFunction.Linear150),
	levelingEffectFunction: (level, target) =>
		target.applyArmorModifier({
			default: {
				bias: -3,
			},
		}),
	talentOptions: [
		{
			talent: {
				talentId: 'protector_range_upgrade_1',
				caption: 'Target Practice',
				description: 'Gain 1 Attack Range on all Weapons',
				minimumLevel: 1,
				effect: (unit) => unit.applyWeaponModifier({ default: { baseRange: { bias: 1 } } }),
			},
			unlocks: [
				{
					talent: {
						talentId: 'protector_range_upgrade_2',
						caption: 'Elevation Exercise',
						description: 'Gain +0.5 Attack Range and +10% Attack Rate on all Weapons',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyWeaponModifier({
								default: { baseRange: { bias: 0.5 }, baseRate: { factor: 1.1 } },
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'protector_rate_upgrade',
								caption: 'Holotargeting',
								description: `Gain +0.15 Attack Rate on all Weapons and +1 ${AbilityPowerUIToken}`,
								minimumLevel: 5,
								effect: (unit) =>
									unit
										.applyWeaponModifier({ default: { baseRate: { bias: 0.15 } } })
										.applyEffectModifier({ abilityPowerModifier: [{ bias: 1 }] }),
							},
						},
						{
							talent: {
								talentId: 'protector_zone_control',
								caption: 'Zone Control',
								description:
									'Gain +1 Aura Range and Ability Range. Add Suppress and Delay to all Action Phases',
								minimumLevel: 4,
								effect: (unit) => {
									unit.auras = unit.auras.map((a) => Object.assign({}, a, { range: a.range + 1 }));
									unit.kit.applyModifier({
										abilityRangeModifier: [
											{
												bias: 1,
												minGuard: ArithmeticPrecision,
											},
										],
									});
									unit.actions = unit.actions.slice(0).map((a) => {
										if (!a.includes(UnitActions.unitActionTypes.suppress))
											a.push(UnitActions.unitActionTypes.suppress);
										if (!a.includes(UnitActions.unitActionTypes.delay))
											a.push(UnitActions.unitActionTypes.delay);
										return a;
									});
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'protector_radio_interference',
				caption: 'Radio Interference',
				description: 'Increase Dodge by 100% versus Drones',
				minimumLevel: 1,
				effect: (unit) =>
					unit.applyChassisModifier({ dodge: { [UnitAttack.deliveryType.drone]: { bias: 1 } } }),
			},
			unlocks: [
				{
					talent: {
						talentId: 'protector_data_uplink',
						caption: 'Data Uplink',
						description: 'Gain +2 Scan Range',
						minimumLevel: 3,
						effect: (unit) => unit.applyChassisModifier({ scanRange: { bias: 2 } }),
					},
					unlocks: [
						{
							talent: {
								talentId: 'protector_repairs',
								caption: 'Nanoid Recovery',
								description: `Restore all Hit Points and gain +1${AbilityPowerUIToken}`,
								minimumLevel: 4,
								effect: (unit) =>
									unit
										.applyEffectModifier({ abilityPowerModifier: [{ bias: 1 }] })
										.addHp(unit.maxHitPoints, unit),
							},
						},
						{
							talent: {
								talentId: 'protector_strafe',
								caption: 'Strafe',
								description: 'All weapons gain +5% Damage and +10% Hit Chance',
								minimumLevel: 5,
								effect: (unit) =>
									unit.applyWeaponModifier({
										default: { baseAccuracy: { bias: 0.1 }, damageModifier: { factor: 1.05 } },
									}),
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'protector_shield_upgrade',
				caption: 'Energy Recirculation',
				description: "Gain +20 to all Shields' capacity",
				minimumLevel: 1,
				effect: (unit) => unit.applyShieldsModifier({ shieldAmount: { bias: 20 } }),
			},
			unlocks: [
				{
					talent: {
						talentId: 'protector_hull_upgrade',
						caption: 'Fortification',
						description: 'Gain +15% to Max Hit Points',
						minimumLevel: 3,
						effect: (unit) => unit.applyChassisModifier({ hitPoints: { factor: 1.15 } }),
					},
					unlocks: [
						{
							talent: {
								talentId: 'protector_tachyon_condenser',
								caption: 'Tachyon Condenser',
								description: 'Add Tachyon Defense perk and grant +30% Dodge',
								minimumLevel: 4,
								effect: (unit) => {
									unit.applyChassisModifier({ dodge: { default: { bias: 0.3 } } }).addDefensePerks(
										UnitDefense.defenseFlags.tachyon,
									);
									return unit;
								},
							},
						},
						{
							talent: {
								talentId: 'protector_shield_restoration',
								caption: 'Shield Overload',
								description:
									'Overcharge the outermost Shield by 100% and provide it with 15 Overchange Decay,if not present. All Shields Protection is increased by 10%',
								minimumLevel: 5,
								effect: (unit) => {
									unit.applyShieldsModifier({ default: { factor: 0.9 } });
									const lastShieldIndex = (unit.defense.shields || []).length - 1;
									if (lastShieldIndex < 0) return unit;
									const shield = { ...unit.defense.shields[lastShieldIndex] } as TShield;
									shield.currentAmount = shield.shieldAmount * 2;
									shield.overchargeDecay = !Number.isFinite(shield.overchargeDecay) ? 15 : 0;
									unit.defense.shields[lastShieldIndex] = shield;
									return unit;
								},
							},
						},
					],
				},
			],
		},
	],
};
export default DefensiveRankTree;

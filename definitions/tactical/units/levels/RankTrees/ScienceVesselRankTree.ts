import DIContainer from '../../../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../core/DI/injections';
import { ArithmeticPrecision } from '../../../../maths/constants';
import SVBlindAbility from '../../../abilities/all/ScienceVessel/SVBlindAbility.99906';
import SVCleanseAbility from '../../../abilities/all/ScienceVessel/SVCleanseAbility.99905';
import SVFeedbackAbility from '../../../abilities/all/ScienceVessel/SVFeedbackAbility.99908';
import SVNanoSwarmAbility from '../../../abilities/all/ScienceVessel/SVNanoSwarmAbility.99903';
import SVNullifyAbility from '../../../abilities/all/ScienceVessel/SVNullifyAbility.99904';
import SVSubjugateAbility from '../../../abilities/all/ScienceVessel/SVSubjugationAbility.99907';
import SVSurveillanceAbility from '../../../abilities/all/ScienceVessel/SVSurveillanceAbility.99901';
import SVTimeTwistAbility from '../../../abilities/all/ScienceVessel/SVTimeTwistAbility.99909';
import SVTransmissionInterceptAbility from '../../../abilities/all/ScienceVessel/SVTransmissionInterceptAbility.99902';
import UnitDefense from '../../../damage/defense';
import DefenseManager from '../../../damage/DefenseManager';
import SVNanoCloudAura from '../../../statuses/Auras/all/SVNanoCloud.5002';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';

export const ScienceVesselRankTree: UnitTalents.ITalentTree = {
	caption: 'Empirical method',
	description: `Science Vessel can learn new combat Abilities with training. Gain +0.4${AbilityPowerUIToken} with each Rank`,
	levelingFunction: getLevelingFunction(TLevelingFunction.Prodigy),
	levelingEffectFunction: (level, target) => {
		target.applyEffectModifier({
			abilityPowerModifier: [
				{
					bias: 0.4,
				},
			],
		});
		return target;
	},
	talentOptions: [
		{
			talent: {
				talentId: 'sv_offensive_talent_1',
				caption: 'Intelligent Assistance',
				description: "Ability to improve Player's Intelligence Level. Passively gain +2 Scan Range.",
				minimumLevel: 1,
				effect: (unit) => {
					unit.scanRange += 2;
					unit.addAbility(SVSurveillanceAbility.id);
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'sv_offensive_talent_2',
						caption: 'Nullify',
						description: 'Ability to remove effects from Units. Passively gain +2 Scan Range.',
						minimumLevel: 2,
						effect: (unit) => {
							unit.maxHitPoints += 15;
							unit.currentHitPoints += 15;
							unit.addAbility(SVNullifyAbility.id);
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'sv_offensive_talent_2_1',
								caption: 'Elusive Shell',
								description: 'Vessel gets +3 Speed and Evasion Module',
								minimumLevel: 3,
								effect: (unit) => {
									return unit
										.applyChassisModifier({
											speed: { bias: 3 },
										})
										.addDefensePerks(UnitDefense.defenseFlags.evasion);
								},
							},
						},
						{
							talent: {
								talentId: 'sv_offensive_talent_2_2',
								caption: 'Probability Matrix',
								description:
									"Grants the Player additional XP, equal to this Unit's XP. The Unit gains +0.5 Ability Power",
								minimumLevel: 4,
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 0.5,
											},
										],
									});
									unit.owner?.addXp(unit.xp);
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'sv_offensive_talent_3',
						caption: 'Noise Generator',
						description: "Gain an ability to reduce Hostile Hit Chance. Restore this Unit's Shields.",
						minimumLevel: 2,
						effect: (unit) => {
							unit.addShields(Math.max(0, unit.totalShields - unit.currentShields)).addAbility(
								SVBlindAbility.id,
							);
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'sv_offensive_talent_3_1',
								caption: 'Subjugate',
								description: 'Gain an Ability to Disable Hostile Units.',
								minimumLevel: 4,
								effect: (unit) => {
									unit.addAbility(SVSubjugateAbility.id);
									return unit;
								},
							},
							unlocks: [],
						},
						{
							talent: {
								talentId: 'sv_offensive_talent_3_1',
								caption: 'Feedback',
								description: 'Ability to deal EMG Damage to a Unit based on its Shield Capacity',
								minimumLevel: 5,
								effect: (unit) => {
									unit.addAbility(SVFeedbackAbility.id);
									return unit;
								},
							},
							unlocks: [],
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'sv_defensive_talent_1',
				caption: 'Transmission Intercept',
				description: "Ability to improve Player's Counter-Intelligence Level. Scan Range is increased by 2.",
				minimumLevel: 1,
				effect: (unit) => {
					unit.scanRange += 2;
					unit.addAbility(SVTransmissionInterceptAbility.id);
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'sv_defensive_talent_2',
						caption: 'Nano Swarm',
						description:
							'An Ability to repair Mechanical units and to deal Damage to the others. Gain +1 Ability Range',
						minimumLevel: 2,
						effect: (unit) => {
							unit.addAbility(SVNanoSwarmAbility.id);
							unit.applyEffectModifier({
								abilityRangeModifier: [{ bias: 1 }],
							});
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'sv_defensive_talent_2_1',
								caption: 'Cleanse',
								description:
									"Ability to restore Unit's Hit Points and remove Negative Effects from them.",
								minimumLevel: 3,
								effect: (unit) => {
									unit.applyEffectModifier({
										statusDurationModifier: [
											{
												bias: -1,
												min: 0,
												minGuard: ArithmeticPrecision,
											},
										],
									});
									unit.addAbility(SVCleanseAbility.id);
									return unit;
								},
							},
							unlocks: [],
						},
						{
							talent: {
								talentId: 'sv_defensive_talent_2_2',
								caption: 'Hardened Veteran',
								description: 'Fully restore Hit Points of the Vessel and grant it +50% Dodge',
								minimumLevel: 4,
								effect: (unit) => {
									unit.currentHitPoints = unit.maxHitPoints;
									unit.applyChassisModifier({
										dodge: {
											default: { bias: 0.5 },
										},
									});
									return unit;
								},
							},
							unlocks: [],
						},
					],
				},
				{
					talent: {
						talentId: 'sv_defensive_talent_3',
						caption: 'Nano Cloud',
						description: "Passively restore surrounding Units' Hit Points",
						minimumLevel: 3,
						effect: (unit) => {
							unit.auras.push(
								DIContainer.getProvider(DIInjectableCollectibles.aura).instantiate(
									null,
									SVNanoCloudAura.id,
								),
							);
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'sv_defensive_talent_3_1',
								caption: 'Time Twist',
								description:
									'Ability that temporarily restores HP and resets Cooldowns of other Abilities',
								minimumLevel: 5,
								effect: (unit) => {
									unit.addAbility(SVTimeTwistAbility.id);
									return unit;
								},
							},
							unlocks: [],
						},
						{
							talent: {
								talentId: 'sv_defensive_talent_3_2',
								caption: 'Power Overwhelming',
								description: 'Restore 200 Shield Capacity and add +1 Ability Power',
								minimumLevel: 4,
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 1,
											},
										],
									});
									unit.addShields(200);
									return unit;
								},
							},
							unlocks: [],
						},
					],
				},
			],
		},
	],
};
export default ScienceVesselRankTree;

import { ArithmeticPrecision } from '../../../../maths/constants';
import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import DamageManager from '../../../damage/DamageManager';
import UnitDefense from '../../../damage/defense';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import { UnitActions } from '../../actions/types';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';

export const ArtilleryRankTree: UnitTalents.ITalentTree = {
	caption: 'Artillery AI',
	description: 'Long range shooting AI uses combat data to continuously improve. Gains +5% Hit Chance per Rank',
	levelingFunction: getLevelingFunction(TLevelingFunction.Fast150),
	levelingEffectFunction: (level, target) => {
		target.applyWeaponModifier({
			default: {
				baseAccuracy: {
					bias: 0.05,
				},
			},
		});
		target.attacks.forEach((v) => {
			v.baseAccuracy += 0.1;
		});
		return target;
	},
	talentOptions: [
		{
			talent: {
				talentId: 'artillery_rate_boost_1',
				caption: 'Swift Aiming',
				description: 'Gain +0.15 Attack Rate',
				minimumLevel: 1,
				effect: (unit) =>
					unit.applyWeaponModifier({
						default: {
							baseRate: {
								bias: 0.15,
							},
						},
					}),
			},
			unlocks: [
				{
					talent: {
						talentId: 'artillery_rate_boost_2',
						caption: 'Reactive aiming',
						description: 'Allows to Retaliate and Watch at every Action Phase ',
						minimumLevel: 3,
						effect: (unit) => {
							unit.actions.forEach((v) =>
								v.push(UnitActions.unitActionTypes.watch, UnitActions.unitActionTypes.retaliate),
							);
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'artillery_rate_boost_3a',
								caption: 'Rapid reload',
								description: 'Gain +0.1 Attack Rate and +10% Dodge',
								minimumLevel: 4,
								effect: (unit) =>
									unit
										.applyWeaponModifier({
											default: {
												baseRate: {
													bias: 0.15,
												},
											},
										})
										.applyChassisModifier({
											dodge: {
												default: {
													bias: 0.1,
												},
											},
										}),
							},
						},
						{
							talent: {
								talentId: 'artillery_rate_boost_3b',
								caption: 'Enhanced ballistics',
								description: 'Gain +15% Damage',
								minimumLevel: 5,
								effect: (unit) => {
									unit.attacks.forEach(
										(v) =>
											(v.attacks = v.attacks.map((t) =>
												DamageManager.applyDamageModifierToDamageDefinition(t, {
													factor: 1.15,
												}),
											)),
									);
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'artillery_accuracy_boost_1',
				caption: 'Ballistic tracking',
				description: `Gain +20% Hit Chance and +0.5${AbilityPowerUIToken}`,
				minimumLevel: 1,
				effect: (unit) => {
					unit.applyEffectModifier({
						abilityPowerModifier: [
							{
								bias: 0.5,
							},
						],
					});
					unit.applyWeaponModifier({ default: { baseAccuracy: { bias: 0.2 } } });
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'artillery_accuracy_boost_2',
						caption: 'Optimistic prediction',
						description: 'Gain +2 Attack Range and +2 Scan Range',
						minimumLevel: 3,
						effect: (unit) =>
							unit
								.applyChassisModifier({
									scanRange: {
										bias: 2,
									},
								})
								.applyWeaponModifier({
									default: {
										baseRange: {
											bias: 2,
										},
									},
								}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'artillery_accuracy_boost_3a',
								caption: 'Target simulation',
								description: 'Gain +10% Hit Chance and +25% AoE Radius',
								minimumLevel: 4,
								effect: (unit) =>
									unit.applyWeaponModifier({
										default: {
											baseAccuracy: {
												bias: 0.1,
											},
											aoeRadiusModifier: {
												factor: 1.25,
											},
										},
									}),
							},
						},
						{
							talent: {
								talentId: 'artillery_accuracy_boost_3b',
								caption: 'Critical precision',
								description: `All Weapons' Direct Attacks and AOE Damage distribution is Maximized. Gain +0.5${AbilityPowerUIToken}`,
								minimumLevel: 5,
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 0.5,
											},
										],
									});
									unit.attacks = unit.attacks.map((v) =>
										Object.assign({}, v, {
											attacks: v.attacks
												.filter(
													(a) =>
														AttackManager.isAOEAttack(a) || AttackManager.isDirectAttack(a),
												)
												.map((t) =>
													Damage.TDamageTypesArray.reduce(
														(obj, k: Damage.damageType) => {
															if (t[k])
																obj[k] = { ...t[k], distribution: 'MostlyMaximal' };
															return obj;
														},
														{ ...t },
													),
												),
										}),
									);
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'artillery_mobility_boost_1',
				caption: 'Rapid Deployment',
				description: 'Gain +20% Dodge and +0.5 Speed',
				minimumLevel: 1,
				effect: (unit) => {
					unit.applyChassisModifier({
						speed: {
							bias: 0.5,
							minGuard: ArithmeticPrecision,
						},
						dodge: {
							default: { bias: 0.2 },
						},
					});
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'artillery_mobility_boost_2',
						caption: 'Pathfinding Algorithms',
						description: 'Gain +1.5 Speed',
						minimumLevel: 3,
						effect: (unit) => {
							unit.applyChassisModifier({
								speed: {
									bias: 1.5,
								},
							});
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'artillery_mobility_boost_3a',
								caption: 'Ambush',
								description: `Gain +80% Dodge versus bombing, drones and cloud attacks and +0.5${AbilityPowerUIToken}`,
								minimumLevel: 4,
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 0.5,
											},
										],
									});
									unit.applyChassisModifier({
										dodge: {
											[UnitAttack.deliveryType.bombardment]: { bias: 0.8 },
											[UnitAttack.deliveryType.cloud]: { bias: 0.8 },
											[UnitAttack.deliveryType.drone]: { bias: 0.8 },
										},
									});
									return unit;
								},
							},
						},
						{
							talent: {
								talentId: 'artillery_mobility_boost_3b',
								caption: 'Dozer',
								description: 'Gain Unstoppable perk and restore 50% of shields',
								minimumLevel: 5,
								effect: (unit) => {
									unit.addShields(unit.totalShields * 0.5).addDefensePerks(
										UnitDefense.defenseFlags.unstoppable,
									);
									return unit;
								},
							},
						},
					],
				},
			],
		},
	],
};
export default ArtilleryRankTree;

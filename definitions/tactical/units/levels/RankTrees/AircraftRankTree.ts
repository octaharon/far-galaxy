import ComputerGuidanceAbility from '../../../abilities/all/GuidanceAbilities/ComputerGuidanceAbility.10002';
import UnitAttack from '../../../damage/attack';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import { addActionPhase, grantAction } from '../../actions/ActionManager';
import { UnitActions } from '../../actions/types';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';

export const AircraftRankTree: UnitTalents.ITalentTree = {
	caption: 'Aircraft Combat AI',
	description: 'The most advanced autopilot equipment with self-enhancing capabilities. Gains +.5 Speed per Rank',
	levelingFunction: getLevelingFunction(TLevelingFunction.Fast100),
	levelingEffectFunction: (level, target) => {
		target.speed += 1;
		target.currentMovementPoints = Math.max(target.speed, target.currentMovementPoints + 1);
		return target;
	},
	talentOptions: [
		{
			talent: {
				talentId: 'aircraft_assault_training_1',
				caption: 'Cunning Stunts',
				description: 'Gain +20% Dodge versus Aerial Attacks',
				minimumLevel: 1,
				effect: (unit) =>
					unit.applyChassisModifier({
						dodge: {
							[UnitAttack.deliveryType.aerial]: { bias: 0.2 },
						},
					}),
			},
			unlocks: [
				{
					talent: {
						talentId: 'aircraft_assault_training_2',
						caption: 'Bait Maneuver',
						description: 'Gain +0.5 weapon Range',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyWeaponModifier({
								default: {
									baseRange: {
										bias: 0.5,
									},
								},
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'aircraft_assault_training_3_a',
								caption: 'Precision Strike',
								description: 'Gain +15% Hit Chance',
								minimumLevel: 4,
								effect: (unit) =>
									unit.applyWeaponModifier({
										default: {
											baseAccuracy: {
												bias: 0.15,
											},
										},
									}),
							},
						},
						{
							talent: {
								talentId: 'aircraft_assault_training_3_b',
								caption: 'Combat Felicity',
								description: 'Gain +10% damage',
								minimumLevel: 5,
								effect: (unit) =>
									unit.applyWeaponModifier({
										default: {
											damageModifier: {
												factor: 1.1,
											},
										},
									}),
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'aircraft_support_training_1',
				caption: 'Optimized Circuitry',
				description: 'Restore 20% of Shield Capacity and gain 1 Effect Penetration',
				minimumLevel: 1,
				effect: (unit) => {
					unit.addShields(unit.totalShields * 0.2).applyEffectModifier({
						abilityDurationModifier: [
							{
								bias: 1,
							},
						],
					});
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'aircraft_support_training_2',
						caption: 'Surveillance Craft',
						description: 'Gain +3 Scan Range and a Computer Guidance ability',
						minimumLevel: 3,
						effect: (unit) => {
							unit.applyChassisModifier({
								scanRange: {
									bias: 3,
								},
							});
							unit.addAbility(ComputerGuidanceAbility.id);
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'aircraft_support_training_3_a',
								caption: 'Combat Endurance',
								description: `Gain +15 Maximum Hit Points and +1${AbilityPowerUIToken}`,
								minimumLevel: 4,
								effect: (unit) => {
									unit.applyChassisModifier({
										hitPoints: {
											bias: 15,
										},
									});
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 1,
											},
										],
									});
									return unit;
								},
							},
						},
						{
							talent: {
								talentId: 'aircraft_support_training_3_b',
								caption: 'Multifold Tracking',
								description:
									'Range of all Abilities and Auras is increased by 1. Effect Duration is reduced by 1 Turn',
								minimumLevel: 5,
								effect: (unit) => {
									unit.auras.forEach((a) => (a.range += 1));
									unit.applyEffectModifier({
										abilityRangeModifier: [
											{
												bias: 1,
											},
										],
										statusDurationModifier: [
											{
												bias: -1,
												min: 1,
												minGuard: 1,
											},
										],
									});
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'aircraft_defense_training_1',
				caption: 'Aerial Supremacy',
				description: 'Gain +2 Speed',
				minimumLevel: 1,
				effect: (unit) => {
					unit.applyChassisModifier({
						speed: {
							bias: 2,
						},
					});
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'aircraft_defense_training_2',
						caption: 'Elusive Yawing',
						description: 'Gain +15% Dodge',
						minimumLevel: 3,
						effect: (unit) => {
							unit.applyChassisModifier({
								dodge: {
									default: {
										bias: 0.15,
									},
								},
							});
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'aircraft_defense_training_3_a',
								caption: 'Extended Chandelle',
								description:
									'Allow unit to Suppress, Defend and Delay on all actions, and add extra action phase',
								minimumLevel: 5,
								effect: (unit) => {
									unit.actions = addActionPhase(
										grantAction(
											unit.actions,
											UnitActions.unitActionTypes.suppress,
											UnitActions.unitActionTypes.defend,
											UnitActions.unitActionTypes.delay,
										),
									);
									return unit;
								},
							},
						},
						{
							talent: {
								talentId: 'aircraft_defense_training_3_b',
								caption: 'Decoy Flares',
								description: 'Increase Shield Absorption by 10% and restore the most damaged Shield',
								minimumLevel: 4,
								effect: (unit) => {
									let leastDamagedShield = 0;
									let leastDamagedShieldCapacity = 1;
									unit.defense.shields.forEach((s, ix) => {
										if (s.currentAmount / s.shieldAmount < leastDamagedShieldCapacity) {
											leastDamagedShield = ix;
											leastDamagedShieldCapacity = s.currentAmount / s.shieldAmount;
										}
									});
									unit.defense.shields[leastDamagedShield].currentAmount =
										unit.defense.shields[leastDamagedShield].shieldAmount;
									unit.applyShieldsModifier({
										default: {
											factor: 0.9,
										},
									});
									return unit;
								},
							},
						},
					],
				},
			],
		},
	],
};
export default AircraftRankTree;

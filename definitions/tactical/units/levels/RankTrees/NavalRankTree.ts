import _ from 'underscore';
import DefragmentationAbility from '../../../abilities/all/ElectronicWarfare/Defragmentation.50004';
import ShutdownAbility from '../../../abilities/all/ElectronicWarfare/Shutdown.50006';
import UnitAttack from '../../../damage/attack';
import UnitDefense from '../../../damage/defense';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import UnitActions from '../../actions/types';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';
import TShield = UnitDefense.TShield;

export const NavalRankTree: UnitTalents.ITalentTree = {
	caption: 'Naval Combat Suite',
	description:
		'Naval units combat capabilities are assisted by a vast computational complex and first-class communication system, being excellent at long range combat and utility. Gains +0.5 Scan Range per Rank',
	levelingFunction: getLevelingFunction(TLevelingFunction.Linear100),
	levelingEffectFunction: (level, target) => {
		target.scanRange += 1;
		return target;
	},
	talentOptions: [
		{
			talent: {
				talentId: 'naval_offensive_1',
				caption: 'Navigation Matrix',
				description: 'Gain +1 Speed',
				minimumLevel: 1,
				effect: (unit) => unit.applyChassisModifier({ speed: { bias: 1 } }),
			},
			unlocks: [
				{
					talent: {
						talentId: 'naval_offensive_2a',
						caption: 'Naval Artillery',
						description: 'Gain +1 Attack Range',
						minimumLevel: 3,
						effect: (unit) => unit.applyWeaponModifier({ default: { baseRange: { bias: 1 } } }),
					},
					unlocks: [
						{
							talent: {
								talentId: 'naval_offensive_3a',
								caption: 'Precise Delivery',
								description: 'Gain +15% Damage on all Weapons',
								minimumLevel: 5,
								effect: (unit) =>
									unit.applyWeaponModifier({ default: { damageModifier: { factor: 1.15 } } }),
							},
						},
					],
				},
				{
					talent: {
						talentId: 'naval_offensive_2b',
						caption: 'Timely Reload',
						description: 'Gain +0.12 fire rate',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyWeaponModifier({
								default: {
									baseRate: {
										bias: 0.12,
									},
								},
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'naval_offensive_3b',
								caption: 'Interactive Targeting',
								description: 'Gain +25% Hit Chance and a Barrage action',
								minimumLevel: 5,
								effect: (unit) => {
									unit.attacks.forEach((d) => (d.baseAccuracy += 0.25));
									unit.actions.forEach((v) => v.push(UnitActions.unitActionTypes.barrage));
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'naval_defensive_1',
				caption: 'Proactive Defense',
				description: 'Gain +15% Dodge',
				minimumLevel: 1,
				effect: (unit) => unit.applyChassisModifier({ dodge: { default: { bias: 0.15 } } }),
			},
			unlocks: [
				{
					talent: {
						talentId: 'naval_defensive_2a',
						caption: 'Close Range Interception',
						description: 'Gain +50% Dodge versus Drones and Bombardment and an Intercept Action',
						minimumLevel: 3,
						effect: (unit) => {
							unit.applyChassisModifier({
								dodge: {
									[UnitAttack.deliveryType.bombardment]: { bias: 0.15 },
									[UnitAttack.deliveryType.drone]: { bias: 0.15 },
								},
							});
							unit.actions = unit.actions.map((v) =>
								_.uniq(v.concat(UnitActions.unitActionTypes.intercept)),
							);
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'naval_defensive_3a',
								caption: 'Shield Isolation',
								description: 'Become Immune to Cloud Attacks. Gain Unstoppable perk',
								minimumLevel: 5,
								effect: (unit) => {
									unit.applyChassisModifier({
										dodge: {
											[UnitAttack.deliveryType.cloud]: { bias: Infinity },
										},
									}).addDefensePerks(UnitDefense.defenseFlags.unstoppable);
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'naval_defensive_2b',
						caption: 'Ray Fencing',
						description: 'Gain +50% Dodge versus Beam and Supersonic weapons',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyChassisModifier({
								dodge: {
									[UnitAttack.deliveryType.beam]: { bias: 0.5 },
									[UnitAttack.deliveryType.supersonic]: { bias: 0.5 },
								},
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'naval_defensive_3b',
								caption: 'Energy transient',
								description:
									'All Shields are restored by 25% and gain Overcharge, which Decays at 5/sec',
								minimumLevel: 5,
								effect: (unit) => {
									unit.defense.shields = unit.defense.shields.map((s) => {
										const t = { ...s } as TShield;
										if ((t.overchargeDecay ?? 0) <= 0) t.overchargeDecay = 5;
										t.currentAmount += t.shieldAmount * 0.25;
										return t;
									});
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'naval_defensive_2c',
						caption: 'Long range Interception',
						description: `Gain +50% Dodge versus Ballistic and Missile weapons. +1${AbilityPowerUIToken}`,
						minimumLevel: 3,
						effect: (unit) =>
							unit
								.applyEffectModifier({
									abilityPowerModifier: [{ bias: 1 }],
								})
								.applyChassisModifier({
									dodge: {
										[UnitAttack.deliveryType.ballistic]: { bias: 0.5 },
										[UnitAttack.deliveryType.missile]: { bias: 0.5 },
									},
								}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'naval_defensive_3c',
								caption: 'Quick Fixes',
								description:
									'Gain 4 Damage Reduction on Armor, 25% Dodge against Beam and restore 50% Hit Points',
								minimumLevel: 5,
								effect: (unit) =>
									unit
										.applyChassisModifier({
											dodge: { [UnitAttack.deliveryType.beam]: { bias: 0.25 } },
										})
										.applyArmorModifier({ default: { bias: -4 } })
										.addHp(unit.maxHitPoints * 0.5),
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'naval_support_1',
				caption: 'Radioelectronic Warfare',
				description: 'Increase Shield Capacity by 10% and gain +1 Scan Range',
				minimumLevel: 1,
				effect: (unit) =>
					unit
						.applyShieldsModifier({ shieldAmount: { factor: 1.1 } })
						.applyChassisModifier({ scanRange: { bias: 1 } }),
			},
			unlocks: [
				{
					talent: {
						talentId: 'naval_support_2a',
						caption: 'Defensive Maneuvers',
						description:
							'Gain +3 Damage Reduction on both Shields and Armor. Gain a Defragmentation ability',
						minimumLevel: 3,
						effect: (unit) => {
							return unit
								.addAbility(DefragmentationAbility.id)
								.applyShieldsModifier({
									default: {
										bias: -3,
									},
								})
								.applyArmorModifier({
									default: {
										bias: -3,
									},
								});
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'naval_support_3a',
								caption: 'Carcass Upgrade',
								description: 'Gain +15% Max Hit Points and restore all of them',
								minimumLevel: 5,
								effect: (unit) => {
									unit.currentHitPoints = unit.maxHitPoints = Math.round(unit.maxHitPoints * 1.15);
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'naval_support_2b',
						caption: 'Cybernetic Warfare',
						description: 'All Auras get +1 Aura Range, and Ability Power is increased by 1.5',
						minimumLevel: 3,
						effect: (unit) => {
							unit.auras.forEach((a) => (a.range += 1));
							unit.applyEffectModifier({
								abilityPowerModifier: [
									{
										bias: 1.5,
									},
								],
							});
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'naval_support_3b',
								caption: 'Offensive Support',
								description: 'Gain +1 Speed and a Shutdown Ability',
								minimumLevel: 5,
								effect: (unit) => {
									unit.actions.forEach((v) => v.push(UnitActions.unitActionTypes.cloak));
									unit.speed += 1;
									unit.addAbility(ShutdownAbility.id);
									return unit;
								},
							},
						},
					],
				},
			],
		},
	],
};
export default NavalRankTree;

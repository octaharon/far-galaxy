import BasicMaths from '../../../../maths/BasicMaths';
import PredatorSenseAbility from '../../../abilities/all/Bionic/PredatorSense.12003';
import UnitAttack from '../../../damage/attack';
import Damage from '../../../damage/damage';
import DamageManager from '../../../damage/DamageManager';
import DefenseManager from '../../../damage/DefenseManager';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import ConstantRegenerationEffect from '../../../statuses/Effects/all/Regeneration/ConstantRegeneration.01101';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';

export const BiotechRankTree: UnitTalents.ITalentTree = {
	caption: 'Forced training',
	description:
		'Biotechnological units can quickly adapt and evolve new features in combat environment. Restores 30% of Max Hit Points every Rank',
	levelingFunction: getLevelingFunction(TLevelingFunction.Fast100),
	levelingEffectFunction: (level, target) => {
		target.addHp(target.maxHitPoints * 0.3);
		return target;
	},
	talentOptions: [
		{
			talent: {
				talentId: 'biotech_hp_upgrade_1',
				caption: 'Battle Awareness',
				description: 'Gain 10% Dodge',
				minimumLevel: 1,
				effect: (unit) => {
					unit.applyChassisModifier({
						dodge: {
							default: {
								bias: 0.1,
							},
						},
					});
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'biotech_hp_upgrade_2',
						caption: 'Hardened Skin',
						description: 'Gain +3 Damage Reduction on Armor',
						minimumLevel: 3,
						effect: (unit) => {
							return unit.applyArmorModifier({
								default: {
									bias: -3,
								},
							});
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'biotech_heal',
								caption: 'Cell Inhibitors',
								minimumLevel: 5,
								description: 'Regenerate 15 Hit Points a turn',
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 1,
											},
										],
									});
									const state = {
										healAmount: 15,
									};
									unit.applyEffect({
										effectId: ConstantRegenerationEffect.id,
										props: {
											state,
										},
									});
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'biotech_speed_upgrade',
						caption: 'Adrenaline Glands',
						description: 'Gain +1 to Speed and +10% to Attack Rate',
						minimumLevel: 3,
						effect: (unit) => {
							unit.speed += 1;
							unit.currentMovementPoints += 1;
							unit.attacks.forEach((a) => {
								a.baseRate = BasicMaths.applyModifier(a.baseRate, {
									factor: 1.1,
								});
							});
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'biotech_predator_sense',
								caption: 'Predator Sense',
								minimumLevel: 5,
								description:
									'Gain +2 to Scan Range, +100% Dodge versus Clouds and Surfaces and a unique Predator Sense Ability',
								effect: (unit) => {
									return unit
										.applyChassisModifier({
											scanRange: { bias: 2 },
											dodge: {
												[UnitAttack.deliveryType.cloud]: { bias: 1 },
												[UnitAttack.deliveryType.surface]: { bias: 1 },
											},
										})
										.addAbility(PredatorSenseAbility.id);
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'biotech_acc_upgrade_1',
				caption: 'Cybernetic Integration',
				description: 'Gain +15% Hit Chance',
				minimumLevel: 1,
				effect: (unit) => {
					unit.attacks.forEach((attack) => (attack.baseAccuracy += 0.15));
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'biotech_metabolism',
						caption: 'Metabolism Suppression',
						description: 'Gain +25% Damage Reduction on Armor versus HEA and BIO damage',
						minimumLevel: 3,
						effect: (unit) => {
							return unit.applyArmorModifier({
								[Damage.damageType.heat]: {
									factor: 0.75,
								},
								[Damage.damageType.biological]: {
									factor: 0.75,
								},
							});
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'biotech_rage',
								caption: 'Primal Rage',
								minimumLevel: 5,
								description: 'Gain +30 Max Hit Points, +1 Speed and remove all Negative Effects',
								effect: (unit) => {
									unit.maxHitPoints += 30;
									unit.currentHitPoints += 30;
									unit.getStatusEffects().forEach((v) => {
										if (!v.static.unremovable && v.static.negative)
											unit.removeEffect(v.getInstanceId());
									});
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'biotech_ferocity',
						caption: 'Unrestricted Ferocity',
						description: 'Gain +10% Damage on all Weapons',
						minimumLevel: 3,
						effect: (unit) => {
							unit.attacks.forEach((v) =>
								v.attacks.forEach((t) => {
									DamageManager.applyDamageModifierToDamageDefinition(t, {
										factor: 1.1,
									});
								}),
							);
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'biotech_alpha_protocol',
								caption: 'Alpha Protocol',
								minimumLevel: 5,
								description: `Gain one additional Action Phase, identical to the last one, and +1${AbilityPowerUIToken}`,
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 1,
											},
										],
									});
									unit.actions.push(unit.actions[unit.actions.length - 1]);
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'biotech_def_upgrade_1',
				caption: 'Mutagen Abstinence',
				description: 'Gain +1 Effect Resistance',
				minimumLevel: 1,
				effect: (unit) =>
					unit.applyEffectModifier({
						statusDurationModifier: [
							{
								bias: -1,
								min: 1,
								minGuard: 1,
							},
						],
					}),
			},
			unlocks: [
				{
					talent: {
						talentId: 'biotech_dodge_1',
						caption: 'Lightning Reflexes',
						description: 'Gain +15% Dodge against Projectile and Missile Attacks',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyChassisModifier({
								dodge: {
									[UnitAttack.deliveryType.missile]: {
										bias: 0.15,
									},
									[UnitAttack.deliveryType.ballistic]: {
										bias: 0.15,
									},
								},
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'biotech_subdermal',
								caption: 'Subdermal Induction',
								minimumLevel: 5,
								description: 'Gain +25% Dodge against Drones and Beam, +10% Shield Capacity',
								effect: (unit) =>
									unit
										.applyChassisModifier({
											dodge: {
												[UnitAttack.deliveryType.drone]: {
													bias: 0.25,
												},
												[UnitAttack.deliveryType.beam]: {
													bias: 0.25,
												},
											},
										})
										.applyShieldsModifier({
											shieldAmount: {
												factor: 1.1,
											},
										}),
							},
						},
					],
				},
				{
					talent: {
						talentId: 'biotech_dodge_2',
						caption: 'Extrasensory Tissue',
						description: 'Gain +40% Dodge against Supersonic',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyChassisModifier({
								dodge: {
									[UnitAttack.deliveryType.supersonic]: {
										bias: 0.4,
									},
								},
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'biotech_circuits',
								caption: 'Brain Circuits',
								minimumLevel: 5,
								description: `+15% Dodge and +1${AbilityPowerUIToken}`,
								effect: (unit) =>
									unit
										.applyEffectModifier({
											abilityPowerModifier: [
												{
													bias: 1,
												},
											],
										})
										.applyChassisModifier({
											dodge: {
												default: {
													bias: 0.15,
												},
											},
										}),
							},
						},
					],
				},
			],
		},
	],
};
export default BiotechRankTree;

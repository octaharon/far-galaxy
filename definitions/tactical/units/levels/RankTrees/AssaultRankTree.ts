import UnitAttack from '../../../damage/attack';
import DamageManager from '../../../damage/DamageManager';
import UnitDefense from '../../../damage/defense';
import DefenseManager from '../../../damage/DefenseManager';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import { grantAction } from '../../actions/ActionManager';
import UnitActions from '../../actions/types';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';
import TShield = UnitDefense.TShield;

export const AssaultRankTree: UnitTalents.ITalentTree = {
	caption: 'Evolving tactical core',
	description:
		'A self-improving intelligent system capable of introducing structural changes to the armament. Gains +3% Damage per Rank',
	levelingFunction: getLevelingFunction(TLevelingFunction.Linear150),
	levelingEffectFunction: (level, target) => {
		target.applyWeaponModifier({
			default: {
				damageModifier: {
					factor: 1.03,
				},
			},
		});
		return target;
	},
	talentOptions: [
		{
			talent: {
				talentId: 'assault_damage_1',
				caption: 'Deadly precision',
				description: 'Gain +5% Damage',
				minimumLevel: 1,
				effect: (unit) =>
					unit.applyWeaponModifier({
						default: {
							damageModifier: {
								factor: 1.05,
							},
						},
					}),
			},
			unlocks: [
				{
					talent: {
						talentId: 'assault_damage_2a',
						caption: 'Assault tactics',
						description: 'Gain +1 Speed',
						minimumLevel: 2,
						effect: (unit) =>
							unit.applyChassisModifier({
								speed: {
									bias: 1,
								},
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'assault_damage_3a',
								caption: 'Veteran training',
								description: 'Gain +1 Attack Range on all Weapons',
								minimumLevel: 4,
								effect: (unit) =>
									unit.applyWeaponModifier({
										default: {
											baseRange: {
												bias: 1,
											},
										},
									}),
							},
						},
					],
				},
				{
					talent: {
						talentId: 'assault_damage_2b',
						caption: 'Lookout',
						description: 'Gain +1 Scan Range and +25% Dodge against Missile and Bombardment',
						minimumLevel: 2,
						effect: (unit) =>
							unit.applyChassisModifier({
								scanRange: {
									bias: 1,
								},
								dodge: {
									[UnitAttack.deliveryType.missile]: {
										bias: 0.25,
									},
									[UnitAttack.deliveryType.bombardment]: {
										bias: 0.25,
									},
								},
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'assault_damage_3b',
								caption: 'Field repairs',
								description: 'Increase Hit Points by 15 and fully restore them',
								minimumLevel: 3,
								effect: (unit) => {
									unit.applyChassisModifier({
										hitPoints: {
											bias: 15,
										},
									});
									unit.currentHitPoints = unit.maxHitPoints;
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'assault_evasion_1',
				caption: 'Impeccable evasion',
				description: 'Gain +10% Dodge',
				minimumLevel: 1,
				effect: (unit) => {
					return unit.applyChassisModifier({ dodge: { default: { bias: 0.1 } } });
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'assault_evasion_2a',
						caption: 'Enhanced protection',
						description:
							'Gain +2 Damage Reduction on Armor and +15% Dodge against Projectile and Supersonic Attacks',
						minimumLevel: 2,
						effect: (unit) =>
							unit
								.applyArmorModifier({
									default: {
										bias: -2,
									},
								})
								.applyChassisModifier({
									dodge: {
										[UnitAttack.deliveryType.ballistic]: {
											bias: 0.15,
										},
										[UnitAttack.deliveryType.supersonic]: {
											bias: 0.15,
										},
									},
								}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'assault_evasion_3a',
								caption: 'Last stand',
								description: 'Fully recharge all Shields and increase their Capacity by 20',
								minimumLevel: 3,
								effect: (unit) => {
									return unit
										.addShields(Math.max(0, unit.totalShields - unit.currentShields))
										.applyShieldsModifier({ shieldAmount: { bias: 20 } });
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'assault_evasion_2b',
						caption: 'Energy overflow',
						description:
							'Innermost Shield can be Overcharged with Decay of 10 and is boosted by 15% of Max Capacity',
						minimumLevel: 2,
						effect: (unit) => {
							if (!unit.defense.shields?.length) return unit;
							const s = { ...(unit.defense.shields?.[0] || {}) } as TShield;
							s.overchargeDecay = 10;
							s.currentAmount += s.shieldAmount * 0.15;
							unit.defense.shields[0] = s;
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'assault_evasion_3b',
								caption: 'Tactical genius',
								description:
									'Add one extra Action Phase to Unit, identical to the last one available, and allow for Delay during any Action Phase',
								minimumLevel: 4,
								effect: (unit) => {
									unit.actions.push(unit.actions[unit.actions.length - 1]);
									unit.actions = unit.actions.map((v) =>
										v.includes(UnitActions.unitActionTypes.delay)
											? v
											: v.concat(UnitActions.unitActionTypes.delay),
									);
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'assault_evolution_1',
				caption: 'Combat Evolution',
				description: 'Gain +0.2 Attack Rate',
				minimumLevel: 1,
				effect: (unit) => {
					unit.applyWeaponModifier({
						default: {
							baseRate: {
								bias: 0.2,
							},
						},
					});
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'assault_evolution_2a',
						caption: 'Advanced Accuracy',
						description: 'Gain +15% Hit Chance on all Weapons',
						minimumLevel: 2,
						effect: (unit) => {
							unit.attacks.forEach((v) => (v.baseAccuracy += 0.15));
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'assault_evolution_3a',
								caption: 'Wake of Destruction',
								description: `All Area Attacks have their AoE Radius increased by 1. Gain +1${AbilityPowerUIToken}`,
								minimumLevel: 4,
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 1,
											},
										],
									});
									unit.applyWeaponModifier({ default: { aoeRadiusModifier: { bias: 1 } } });
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'assault_evolution_2b',
						caption: 'Screened Framework',
						description:
							'Remove all Negative Status Effects, gain +1 Effect Resistance and +20% Dodge against Beam and Drone',
						minimumLevel: 2,
						effect: (unit) => {
							unit.getStatusEffects().forEach((v) => {
								if (!v.static.unremovable && v.static.negative) unit.removeEffect(v.getInstanceId());
							});
							unit.applyEffectModifier({
								statusDurationModifier: [
									{
										bias: -1,
										min: 1,
										minGuard: 1,
									},
								],
							});
							unit.applyChassisModifier({
								dodge: {
									[UnitAttack.deliveryType.drone]: {
										bias: 0.2,
									},
									[UnitAttack.deliveryType.beam]: {
										bias: 0.2,
									},
								},
							});
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'assault_evolution_3b',
								caption: 'Jack of all trades',
								description: `Allow for Watch, Suppress, Protect, Retaliate and Intercept during every Action Phase. Gain +0.5${AbilityPowerUIToken}`,
								minimumLevel: 3,
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 0.5,
											},
										],
									});
									unit.actions = grantAction(
										unit.actions,
										UnitActions.unitActionTypes.suppress,
										UnitActions.unitActionTypes.protect,
										UnitActions.unitActionTypes.retaliate,
										UnitActions.unitActionTypes.watch,
										UnitActions.unitActionTypes.intercept,
									);
									return unit;
								},
							},
						},
					],
				},
			],
		},
	],
};
export default AssaultRankTree;

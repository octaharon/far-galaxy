import _ from 'underscore';
import { fastMerge } from '../../../../core/dataTransformTools';
import { ArithmeticPrecision } from '../../../../maths/constants';
import SVNullifyAbility from '../../../abilities/all/ScienceVessel/SVNullifyAbility.99904';
import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import { ATTACK_PRIORITIES } from '../../../damage/constants';
import Damage from '../../../damage/damage';
import DefenseManager from '../../../damage/DefenseManager';
import { AbilityPowerUIToken, DeferredStatusEffectId, DOTStatusEffectId } from '../../../statuses/constants';
import ConstantRegenerationEffect from '../../../statuses/Effects/all/Regeneration/ConstantRegeneration.01101';
import { TEffectApplianceItem, TStatusEffectStateLookup } from '../../../statuses/types';
import { grantAction } from '../../actions/ActionManager';
import { UnitActions } from '../../actions/types';
import { getLevelingFunction, TLevelingFunction } from '../levelingFunctions';
import UnitTalents from '../unitTalents';

export const InfantryRankTree: UnitTalents.ITalentTree = {
	caption: 'Ground force training',
	description:
		'With combat experience soldiers gain valuable skills, mostly focusing on agility and combat durability. Gains 6% Attack Rate per Rank',
	levelingFunction: getLevelingFunction(TLevelingFunction.Linear50),
	levelingEffectFunction: (level, target) =>
		target.applyWeaponModifier({
			default: {
				baseRate: {
					factor: 1.06,
				},
			},
		}),
	talentOptions: [
		{
			talent: {
				talentId: 'infantry_hp_upgrade_1',
				caption: 'Endurance Training',
				description: 'Gain +10 maximum Hit Points and restore all Hit Points',
				minimumLevel: 1,
				effect: (unit) => {
					unit.currentHitPoints += 10;
					unit.maxHitPoints = unit.currentHitPoints;
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'infantry_hp_upgrade_2',
						caption: 'Sufferance',
						description: 'Gain +10 additional maximum Hit Points and +10% Armor Damage Reduction',
						minimumLevel: 3,
						effect: (unit) =>
							unit
								.applyChassisModifier({
									hitPoints: {
										bias: 10,
									},
								})
								.applyArmorModifier({
									default: {
										factor: 0.9,
									},
								}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'infantry_heal',
								caption: 'First aid',
								minimumLevel: 5,
								description:
									'Remove all DoT and Deferred Damage effects and grant constant +10 HP/Turn Regeneration',
								effect: (unit) => {
									unit.getStatusEffects().forEach((e) => {
										if (e.static.id === DeferredStatusEffectId || e.static.id === DOTStatusEffectId)
											unit.removeEffect(e.getInstanceId());
									});
									unit.applyEffect(
										{
											effectId: ConstantRegenerationEffect.id,
											props: { healAmount: 10 },
										} as TEffectApplianceItem<
											TStatusEffectStateLookup<typeof ConstantRegenerationEffect>
										>,
										unit,
									);
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'infantry_speed_upgrade',
						caption: 'Fitness Training',
						description: 'Gain +1 to speed',
						minimumLevel: 3,
						effect: (unit) => unit.applyChassisModifier({ speed: { bias: 1 } }),
					},
					unlocks: [
						{
							talent: {
								talentId: 'infantry_tactic_delay',
								caption: 'Advanced Tactics',
								minimumLevel: 5,
								description:
									'Allows to Delay turn at any phase. +50% Dodge versus Bombardment and Surface',
								effect: (unit) => {
									unit.actions = unit.actions.map((a) => a.concat(UnitActions.unitActionTypes.delay));
									unit.applyChassisModifier({
										dodge: {
											[UnitAttack.deliveryType.bombardment]: { bias: 0.25 },
											[UnitAttack.deliveryType.surface]: { bias: 0.25 },
										},
									});
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'infantry_acc_upgrade_1',
				caption: 'Marksman training',
				description: 'Gain +15% Hit Chance',
				minimumLevel: 1,
				effect: (unit) => unit.applyWeaponModifier({ default: { baseAccuracy: { bias: 0.15 } } }),
			},
			unlocks: [
				{
					talent: {
						talentId: 'infantry_acc_upgrade_2',
						caption: 'Sniper Elite',
						description: 'Direct Attacks gain +8 Maximum Damage. Weapons now have Highest Priority',
						minimumLevel: 3,
						effect: (unit) => {
							unit.attacks = (unit.attacks || []).map((a) => ({
								...a,
								priority: ATTACK_PRIORITIES.HIGHEST,
								attacks: a.attacks?.map((att) => {
									if (AttackManager.isDirectAttack(att)) {
										Object.keys(Damage.DamageTemplate).forEach((dt: Damage.damageType) => {
											if (att[dt]) att[dt] = fastMerge(att[dt], { max: att[dt].max + 8 });
										});
									}
									return att;
								}),
							}));
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'infantry_tactic_suppress',
								caption: 'Barrage Fire',
								minimumLevel: 5,
								description: 'Allows to Suppress enemies. Grants +25% Fire Rate',
								effect: (unit) => {
									unit.actions = grantAction(unit.actions, UnitActions.unitActionTypes.suppress);
									unit.applyWeaponModifier({ default: { baseRate: { factor: 1.25 } } });
									return unit;
								},
							},
						},
					],
				},
				{
					talent: {
						talentId: 'infantry_scan_upgrade',
						caption: 'Scout Training',
						description: 'Gain +1 to Scan Range and 30% Dodge vs Projectile',
						minimumLevel: 3,
						effect: (unit) =>
							unit.applyChassisModifier({
								scanRange: { bias: 1 },
								dodge: { [UnitAttack.deliveryType.ballistic]: { bias: 0.3 } },
							}),
					},
					unlocks: [
						{
							talent: {
								talentId: 'infantry_tactic_intercept',
								caption: 'Cloak of Shadows',
								minimumLevel: 5,
								description:
									'Allows to Cloak and Intercept enemies. Removes all negative status effects',
								effect: (unit) => {
									unit.getStatusEffects().forEach((v) => {
										if (!v.static.unremovable && v.static.negative)
											unit.removeEffect(v.getInstanceId());
									});
									unit.actions = unit.actions.map((a) =>
										_.uniq(
											a.concat(
												UnitActions.unitActionTypes.intercept,
												UnitActions.unitActionTypes.cloak,
											),
										),
									);
									return unit;
								},
							},
						},
					],
				},
			],
		},
		{
			talent: {
				talentId: 'infantry_tch_upgrade_1',
				caption: 'Engineering Training',
				description: `Gain +1 ${AbilityPowerUIToken}`,
				minimumLevel: 1,
				effect: (unit) => {
					unit.applyEffectModifier({ abilityPowerModifier: [{ bias: 1 }] });
					return unit;
				},
			},
			unlocks: [
				{
					talent: {
						talentId: 'infantry_tch_upgrade_2',
						caption: 'Field Technician',
						description: `Gain +1 Ability Range and +0.5${AbilityPowerUIToken}`,
						minimumLevel: 3,
						effect: (unit) => {
							unit.applyEffectModifier({
								abilityPowerModifier: [{ bias: 0.5 }],
								abilityRangeModifier: [{ bias: 1 }],
							});
							return unit;
						},
					},
					unlocks: [
						{
							talent: {
								talentId: 'infantry_netrunner',
								caption: 'Netrunner',
								minimumLevel: 5,
								description: `Adds +1 Effect Resistance and grants +1 Effect Duration on all Abilities`,
								effect: (unit) => {
									unit.applyEffectModifier({
										abilityDurationModifier: [{ bias: 1 }],
										statusDurationModifier: [{ bias: -1, minGuard: ArithmeticPrecision, min: 0 }],
									});
									return unit;
								},
							},
						},
						{
							talent: {
								talentId: 'infantry_archon',
								caption: 'Archon',
								minimumLevel: 5,
								description: `Adds +2 Ability Power`,
								effect: (unit) =>
									unit.applyEffectModifier({
										abilityPowerModifier: [
											{
												bias: 2,
											},
										],
									}),
							},
						},
						{
							talent: {
								talentId: 'infantry_legate',
								caption: 'Legate',
								minimumLevel: 5,
								description: `Grants Nullify Ability and 25% Dodge`,
								effect: (unit) =>
									unit.addAbility(SVNullifyAbility.id).applyChassisModifier({
										dodge: { default: { bias: 0.25 } },
									}),
							},
						},
					],
				},
			],
		},
	],
};
export default InfantryRankTree;

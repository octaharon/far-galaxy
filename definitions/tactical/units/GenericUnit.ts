import _ from 'underscore';
import { DIEntityDescriptors, DIInjectableCollectibles, DIInjectableSerializables } from '../../core/DI/injections';
import { DIGlobalContainer } from '../../core/DI/types';
import BasicMaths from '../../maths/BasicMaths';
import { ArithmeticPrecision } from '../../maths/constants';
import Vectors from '../../maths/Vectors';
import PlayerProperty from '../../player/PlayerProperty';
import { stackUnitEffectsModifiers } from '../../prototypes/helpers';
import { IPrototypeModifier, IUnitEffectModifier } from '../../prototypes/types';
import GameStats from '../../stats/types';
import UnitChassis from '../../technologies/types/chassis';
import Weapons from '../../technologies/types/weapons';
import { AbilityKit } from '../abilities/AbilityKit';
import Abilities from '../abilities/types';
import UnitAttack from '../damage/attack';
import AttackManager from '../damage/AttackManager';
import { COMBAT_TICKS_PER_TURN } from '../damage/constants';
import Damage from '../damage/damage';
import DamageManager from '../damage/DamageManager';
import {
	applyAttackBatchToDefense,
	applyAttackEffectToDefense,
	applyAttackUnitToDefense,
	getAttackFrames,
} from '../damage/damageModel';
import UnitDefense, { isDefenseFlag } from '../damage/defense';
import { DefenseManager } from '../damage/DefenseManager';
import { ICombatMap, ILocationOnMap } from '../map';
import { IAura } from '../statuses/Auras/types';
import { DEFAULT_EFFECT_POWER, DOTStatusEffectId } from '../statuses/constants';
import EffectEvents from '../statuses/effectEvents';
import GenericStatusEffect from '../statuses/GenericStatusEffect';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import { IStatusEffect, TEffectApplianceItem } from '../statuses/types';
import {
	addActionPhase,
	doesActionRequireSeveralWeapons,
	doesActionRequireSpeed,
	doesActionRequireWeapon,
	doesActionUseAbility,
	grantAction,
	isOrbitalAttackAction,
} from './actions/ActionManager';
import { ATTACK_MOVE_SPEED_REDUCTION, ORBITAL_ATTACK_RANGE } from './actions/constants';
import UnitActions from './actions/types';
import UnitTalents from './levels/unitTalents';
import Units from './types';

type TTalentTreeInternal = {
	talent: UnitTalents.IUnitTalent;
	prev?: UnitTalents.TUnitTalentId;
	next?: UnitTalents.TUnitTalentId[];
};

type TTalentsInternal = {
	[key: string]: TTalentTreeInternal;
};

export const defaultUnitProps: Partial<Units.TUnitProps> = {
	stats: {
		orbitalAttacksPerformed: 0,
		orbitalDamageDone: 0,
		turnsDisabled: 0,
		spellsTaken: 0,
		offensiveAbilitiesCast: 0,
		defensiveAbilitiesCast: 0,
		accuracyByDeliveryType: {},
		accuracyByWeaponType: {},
		attacksDodgedByType: {},
		breachAttacksDone: 0,
		damageDoneByAttackType: {},
		damageDoneByDeliveryType: {},
		damageDoneByWeaponTypes: {},
		damageDoneToArmor: {},
		damageDoneToClass: {},
		damageDoneToShields: {},
		damageTakenByType: Damage.DamageTemplate(0),
		distanceCoveredByMovementType: {},
		doodadsDestroyed: 0,
		healthDamageDoneByType: Damage.DamageTemplate(0),
		shieldsLost: 0,
		turnsRowWithoutTakingDamage: 0,
		turnsSurvived: 0,
		unitKilledByAttackType: {},
		unitsKilledByDeliveryType: {},
		unitsKilledByWeaponTypes: {},
	},
	status: {
		cloaked: false,
		targetable: true,
		disabled: false,
		commandChain: [],
		currentActionIndex: -1,
		moving: null,
		currentTurnTick: 0,
		currentTurnAttackFrames: [],
	},
};

export class GenericUnit extends PlayerProperty<Units.TUnitProps, Units.TSerializedUnit> implements Units.IUnit {
	public currentHitPoints: number;
	public currentMovementPoints: number;
	public orbitalAttacksPerformed: boolean[];
	public abilitiesUsed: boolean[];
	public actionDelayed: UnitActions.TUnitActionOptions | null;
	public location: ILocationOnMap;
	public prototypeId: string;
	public xp: number;
	public level: number;
	public talentsLearned: UnitTalents.TUnitTalentId[];
	public talentPoints: number;
	public status: Units.TUnitCombatStatus;
	public maxHitPoints: number;
	public cost: number;
	public actions: UnitActions.TUnitActionOptions[];
	public speed: number;
	public chassisType: UnitChassis.chassisClass;
	public targetFlags: UnitChassis.TTargetFlags;
	public scanRange: number;
	public movement: UnitChassis.movementType;
	public attacks: UnitAttack.IAttackInterface[];
	public defense: UnitDefense.IDefenseInterface;
	public auras: IAura[];
	public stats: GameStats.TUnitStats;
	public effects: Array<IStatusEffect<StatusEffectMeta.effectAgentTypes.unit>>;
	public effectModifiers: IUnitEffectModifier;
	public readonly effectTargetType = StatusEffectMeta.effectAgentTypes.unit;
	public kit: Abilities.IAbilityKit;

	protected destroyCallback: Units.TUnitDestroyedCallback[];
	protected effectCallback: EffectEvents.TEffectCallback<StatusEffectMeta.effectAgentTypes.unit> = {
		[EffectEvents.TUnitEffectEvents.turnStart]: () => {
			this._init();
			if (this.location) return true;
			// @TODO https://gitlab.com/octaharon/far-galaxy/-/issues/13
			return false;
		},
	};
	protected _talents: TTalentsInternal;

	public constructor(props: Partial<Units.TUnitProps>) {
		super(props);
		if (!props?.currentHitPoints) this.currentHitPoints = this.maxHitPoints;
		if (!Number.isFinite(props?.currentMovementPoints)) this.currentMovementPoints = COMBAT_TICKS_PER_TURN;
		this.effects = props?.effects || [];
		this.auras = props?.auras || [];
		this.kit = new AbilityKit(this, {
			order: props?.kit?.order ?? [],
			abilities: props?.kit?.abilities ?? [],
			modifiers: props?.kit?.modifiers ?? this.effectModifiers,
		});
		if (!props?.kit?.total && this.prototype?.getBaseAbilities().length)
			this.prototype.getBaseAbilities().forEach((abilityId) => this.kit.addAbility(abilityId));
		this.kit.save();
		if (!props?.status)
			this.status = {
				disabled: false,
				cloaked: false,
				commandChain: [],
				currentActionIndex: -1,
				currentTurnAttackFrames: [],
				currentTurnTick: -1,
				moving: null,
				targetable: true,
			};
		this._setTalents();
	}

	public get isRobotic() {
		return this.targetFlags[UnitChassis.targetType.robotic];
	}

	public get isOrganic() {
		return this.targetFlags[UnitChassis.targetType.organic];
	}

	public get isMechanical() {
		return !this.isRobotic && !this.isOrganic;
	}

	public get isMassive() {
		return this.targetFlags[UnitChassis.targetType.massive];
	}

	public get isUnique() {
		return this.targetFlags[UnitChassis.targetType.unique];
	}

	public get attackFramesLeft(): IBounds[][] {
		return this.status.currentTurnAttackFrames.map((attackInterfaceFrames) =>
			attackInterfaceFrames.filter((frame) => frame.min >= this.status.currentTurnTick),
		);
	}

	public get isGround(): boolean {
		return this.chassis.static.movement === UnitChassis.movementType.ground;
	}

	public get isHover(): boolean {
		return this.chassis.static.movement === UnitChassis.movementType.hover;
	}

	public get isNaval(): boolean {
		return this.chassis.static.movement === UnitChassis.movementType.naval;
	}

	public get isAirborne(): boolean {
		return this.chassis.static.movement === UnitChassis.movementType.air;
	}

	public get abilityPower() {
		return BasicMaths.applyModifier(
			this.owner?.abilityPower ?? DEFAULT_EFFECT_POWER,
			{ min: 0, minGuard: 0 },
			...[]
				.concat(
					this.effectModifiers?.abilityPowerModifier,
					this.owner?.getUnitModifiers(this.chassisType).map((v) => v?.unitModifier?.abilityPowerModifier),
				)
				.filter(Boolean),
		);
	}

	public get player() {
		return this.getProvider(DIInjectableSerializables.player).getByPlayerId(this.playerId);
	}

	public get isImmuneToEffects() {
		return !this.status.targetable || this.hasDefensePerks(UnitDefense.defenseFlags.immune);
	}

	public get isImmuneToNegativeEffects() {
		return this.isImmuneToEffects || this.hasDefensePerks(UnitDefense.defenseFlags.unstoppable);
	}

	public get isDisabled() {
		return this.status.disabled || this.currentAction.action === UnitActions.unitActionTypes.warp;
	}

	public get isTargetable() {
		return (
			this.status.targetable &&
			![UnitActions.unitActionTypes.warp, UnitActions.unitActionTypes.cloak].includes(this.currentAction.action)
		);
	}

	public get isMovable() {
		return this.hostingMap && !this.isDisabled && this.currentMovementPoints > 0 && this.speed > 0;
	}

	public get totalShields() {
		return (this.defense?.shields ?? []).filter(Boolean).reduce((sum, d) => sum + d.shieldAmount, 0);
	}

	public get nextLevelXp() {
		let nextLevel = 0;
		while (this.xp >= this.chassis?.static?.levelTree?.levelingFunction(nextLevel) || 0) {
			nextLevel++;
		}
		return this.chassis?.static?.levelTree?.levelingFunction(nextLevel) || 0;
	}

	public get currentShields() {
		return (this.defense?.shields ?? []).filter(Boolean).reduce((sum, d) => sum + d.currentAmount, 0);
	}

	public get prototype() {
		const provider = this.getProvider(DIInjectableSerializables.prototype);
		if (!provider) return null;
		return provider.find(this.prototypeId) || null;
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableSerializables.unit];
	}

	protected get hostingMap(): ICombatMap {
		return this.location && this.location.mapId
			? this.getProvider(DIInjectableSerializables.map).find(this.location.mapId)
			: null;
	}

	protected get chassis() {
		return this.prototype?.chassis || null;
	}

	protected get currentAction(): UnitActions.TUnitActionCommandOption {
		return (
			this.status.commandChain[this.status.currentActionIndex] || {
				action: UnitActions.unitActionTypes.skip,
				meta: {},
			}
		);
	}

	injectDependencies(container: DIGlobalContainer) {
		super.injectDependencies(container);
		if (this.kit) this.kit.injectDependencies(this.DIContainer);
		return this;
	}

	public clearAbilities() {
		if (this.kit) this.kit.clearAbilities();
		return this;
	}

	public addAbility(abilityId: number) {
		this.kit.addAbility(abilityId);
		return this;
	}

	public removeAbility(abilityId: number) {
		this.kit.removeAbility(abilityId);
		return this;
	}

	public hasEnoughMovementToPerform(
		action: UnitActions.IUnitAction<
			| UnitActions.unitActionTypes.move
			| UnitActions.unitActionTypes.attack_move
			| UnitActions.unitActionTypes.blink
		>,
	) {
		if (!this.hostingMap || !this.isMovable) return false;
		const to = action?.meta?.to;
		const from = this.location;
		if (from && to) {
			switch (action.action) {
				case UnitActions.unitActionTypes.move:
					return (
						this.hostingMap.isPathTraversible(from, to, this.movement) &&
						Vectors.distance(from, to) <= this.currentMovementPoints
					);
				case UnitActions.unitActionTypes.attack_move:
					return (
						this.hostingMap.isPathTraversible(from, to, this.movement) &&
						Vectors.distance(from, to) <= this.currentMovementPoints * ATTACK_MOVE_SPEED_REDUCTION
					);
				case UnitActions.unitActionTypes.blink:
					return this.isMovable;
				default:
					return false;
			}
		}
		throw new Error(`Invalid action: ${action?.serialize() || action}`);
	}

	public moveTo(
		location: ILocationOnMap,
		action:
			| UnitActions.IUnitAction<
					| UnitActions.unitActionTypes.move
					| UnitActions.unitActionTypes.attack_move
					| UnitActions.unitActionTypes.blink
			  >
			| undefined,
	) {
		if (this.isMovable) {
			if (action) {
				if (this.hasEnoughMovementToPerform(action)) {
					const to = action.meta.to;
					const distance =
						Vectors.distance(this.location, to) *
						(action.action === UnitActions.unitActionTypes.attack_move
							? 1 / ATTACK_MOVE_SPEED_REDUCTION
							: action.action === UnitActions.unitActionTypes.blink
							? 0
							: 1);
					this.location = Object.assign(this.location, to);
					this.currentMovementPoints = BasicMaths.applyModifier(this.currentMovementPoints, {
						min: 0,
						bias: -distance,
					});
				} else throw new Error(`impossible action: ${action}`);
			} else if (location) {
				if (this.hostingMap) {
					if (this.hostingMap.getInstanceId() === location.mapId) {
						this.location = location;
					} else throw new Error(`Map Mismatch: ${location.mapId}`);
				} else this.location = location;
			} else throw new Error(`either Location or Action must be specified`);
		}
		return this;
	}

	/**
	 * used by external storages to wipe their local states
	 * @param callback
	 */
	public setDestroyedCallback(callback: Units.TUnitDestroyedCallback) {
		if (callback) this.destroyCallback = (this.destroyCallback || []).concat(callback);
		return this;
	}

	public getStatusEffects(): Array<IStatusEffect<StatusEffectMeta.effectAgentTypes.unit>> {
		return this.effects || [];
	}

	public fillAttackFrames() {
		this.status.currentTurnAttackFrames = (this.attacks ?? []).map((v) =>
			getAttackFrames(AttackManager.getNumberOfAttacks(v.baseRate)),
		);
		return this;
	}

	public applyArmorModifier(m: UnitDefense.TArmorModifier) {
		this.defense = Object.assign({}, this.defense, {
			armor: DefenseManager.serializeProtection(
				DefenseManager.stackDamageProtectionDefinition(this.defense.armor || {}, m),
			),
		});
		return this;
	}

	public applyChassisModifier(m: UnitChassis.TUnitChassisModifier) {
		if (!m) return this;
		if (m.hitPoints) {
			this.maxHitPoints = BasicMaths.applyModifier(this.maxHitPoints, m.hitPoints);
			this.currentHitPoints = BasicMaths.applyModifier(this.currentHitPoints, { min: 1 }, m.hitPoints);
			if (this.currentHitPoints > this.maxHitPoints) this.currentHitPoints = this.maxHitPoints;
		}
		if (m.speed) {
			this.speed = BasicMaths.applyModifier(this.speed, m.speed);
			this.currentMovementPoints = BasicMaths.applyModifier(this.currentMovementPoints, { min: 0 }, m.speed);
		}
		this.defense = Object.assign({}, this.defense, {
			dodge: DefenseManager.stackDodge(this.defense.dodge || {}, m.dodge),
			...(m.defenseFlags ?? {}),
		});
		this.scanRange = BasicMaths.applyModifier(this.scanRange, m.scanRange);
		const actionOptions = _.uniq(_.flatten(m.actions?.filter(Boolean)));
		const actionMod = m.actionCount;
		if (actionOptions && actionOptions.length) this.actions = grantAction(this.actions, ...actionOptions);
		if (actionMod !== 0) this.actions = addActionPhase(this.actions, actionMod);
		return this;
	}

	public applyWeaponModifier(m: Weapons.TWeaponGroupModifier, weaponIndex = -1) {
		this.attacks = (this.attacks ?? []).map((w, ix) => {
			if (Number.isFinite(weaponIndex) && ix !== weaponIndex) return w;
			const mod = m[w.type] ?? m.default ?? {};
			w.baseRange = BasicMaths.applyModifier(w.baseRange, mod?.baseRange);
			w.baseAccuracy = BasicMaths.applyModifier(w.baseAccuracy, mod?.baseAccuracy);
			w.baseRate = BasicMaths.applyModifier(w.baseRate, mod?.baseRate);
			w.priority = BasicMaths.applyModifier(w.priority, mod?.priority);

			w.attacks = w.attacks.map((a, ix) => {
				let aCopy = a;
				if (mod.damageModifier)
					aCopy = DamageManager.applyDamageModifierToDamageDefinition(a, mod.damageModifier);
				if (AttackManager.isAOEAttack(aCopy) && mod.aoeRadiusModifier)
					aCopy.radius = BasicMaths.roundToPrecision(
						BasicMaths.applyModifier((a as UnitAttack.TAOEAttackDefinition).radius, mod.aoeRadiusModifier),
						0.01,
					);
				return aCopy;
			});
		});
		return this;
	}

	/**
	 *
	 * @param amount
	 * @param shieldIndex negative for outermost shields, positive for innermost
	 */
	public addShields(amount: number, shieldIndex: number = null) {
		if (!Number.isFinite(amount) || amount === 0) return this;
		const currentIndex = -1;
		let amountLeft = amount;
		const inc = amount > 0;
		const allShields = this.defense.shields || [];
		const total = allShields.length;
		const startIndex = BasicMaths.applyModifier(
			Number.isFinite(shieldIndex) && shieldIndex >= 0
				? shieldIndex
				: Number.isFinite(shieldIndex) && shieldIndex < 0
				? total + shieldIndex
				: amount < 0
				? total - 1
				: 0,
			{
				min: 0,
				max: total - 1,
			},
		);
		for (
			let ix = startIndex;
			Math.abs(amountLeft) >= ArithmeticPrecision && (inc ? ix < total : ix >= 0);
			ix += inc ? 1 : -1
		) {
			const s = allShields[ix];
			const currentAmount = s.currentAmount;
			const shield = DefenseManager.addShields(s, amountLeft);
			const delta = s.currentAmount - currentAmount;
			// amount>0: adding 30 to 120/125 Shield will yield delta=5
			// amount<0: adding -30 to 5/125 Shield will yield delta=-5
			if (delta / amountLeft < 1) amountLeft -= delta;
			Object.assign(s, shield);
		}
		return this;
	}

	public applyShieldsModifier(m: UnitDefense.TShieldsModifier) {
		this.defense = Object.assign({}, this.defense, {
			shields: (this.defense.shields || []).map((s) =>
				Object.assign(
					s,
					DefenseManager.serializeProtection(DefenseManager.stackDamageProtectionDefinition(s, m)),
					{
						shieldAmount: BasicMaths.applyModifier(s.shieldAmount, m.shieldAmount),
						currentAmount: BasicMaths.applyModifier(s.currentAmount, m.shieldAmount),
						overchargeDecay: BasicMaths.applyModifier(s.overchargeDecay ?? 0, m.overchargeDecay),
					},
				),
			),
		});
		return this;
	}

	public applyEffectModifier(...m: IUnitEffectModifier[]) {
		this.effectModifiers = stackUnitEffectsModifiers(this.effectModifiers || {}, ...m);
		this.getStatusEffects().forEach((eff) => m.forEach((mod) => eff.applyModifier(mod)));
		this.kit?.applyModifier(...m);
		return this;
	}

	public applyPrototypeModifier(m: IPrototypeModifier) {
		if (!m) return this;
		m.weapon?.forEach((wm) => this.applyWeaponModifier(wm));
		m.armor?.forEach((am) => this.applyArmorModifier(am));
		m.shield?.forEach((sm) => this.applyShieldsModifier(sm));
		m.chassis?.forEach((cm) => this.applyChassisModifier(cm));
		this.applyEffectModifier(m.unitModifier);
		return this;
	}

	public getEventContextMeta(unit: Units.IUnit = this): EffectEvents.TUnitContextEffectMeta {
		return {
			phaseIndex: unit.status?.currentActionIndex || 0,
			phase: unit.status?.commandChain?.[unit.status?.currentActionIndex] || null,
		};
	}

	/**
	 *
	 *
	 * @param {UnitAttack.IAttackBatch} a
	 * @param source
	 * @returns {boolean} true if unit is killed
	 * @memberof GenericUnit
	 */
	public applyAttackBatch(a: UnitAttack.IAttackBatch, source?: StatusEffectMeta.TStatusEffectTarget): boolean {
		const activeDefense = this.defense;
		if (GenericStatusEffect.isUnitTarget(source)) {
			if (this.status.moving)
				// reduce hit chance for a moving target
				a.accuracy = AttackManager.adjustAccuracyToMovement(
					a.accuracy,
					a.priority,
					this.status.moving,
					this.location,
					source.location,
				);
		}
		a.attacks = a.attacks.filter((at) => AttackManager.isReachableByAttack(at.delivery, this.movement));
		const attackResult = applyAttackBatchToDefense(a, activeDefense, this);
		// Attack Result can be mutated here
		this.callEffectEvent({
			event: EffectEvents.TUnitEffectEvents.unitAttacked,
			type: StatusEffectMeta.effectAgentTypes.unit,
			source,
			attack: a,
			result: attackResult,
			...this.getEventContextMeta(),
		});
		if (attackResult.hit && GenericStatusEffect.isUnitTarget(source))
			source.callEffectEvent({
				event: EffectEvents.TUnitEffectEvents.unitHit,
				type: StatusEffectMeta.effectAgentTypes.unit,
				target: this,
				attack: a,
				result: attackResult,
				...this.getEventContextMeta(source),
			});
		// Cascading AoE Attacks
		if (this.hostingMap)
			(a.attacks || [])
				.filter((attack) => AttackManager.isAOEAttack(attack))
				.forEach((aoeAttack: UnitAttack.TAttackAOEUnit) => {
					this.hostingMap.applyAoEAttack(this.location, aoeAttack, source, this);
				});
		// Not the final Attack Result is actually applied
		this._applyAttackResultToDefense(attackResult, activeDefense, source);

		return false;
	}

	public applyEffect(eff: TEffectApplianceItem, source: StatusEffectMeta.TStatusEffectTarget = this): this {
		if (this.getProvider(DIInjectableCollectibles.effect).get(eff.effectId).targetType !== this.effectTargetType)
			return null;
		const newEffect = this.getProvider(
			DIInjectableCollectibles.effect,
		).instantiate<StatusEffectMeta.effectAgentTypes.unit>(eff.props, eff.effectId, source, this);
		if (source === this) newEffect.power = this.kit?.abilityPower ?? this.abilityPower;
		return this.addEffect(newEffect);
	}

	public hasDefensePerks(...perks: UnitDefense.defenseFlags[]): boolean {
		return perks.filter((v) => isDefenseFlag()(v)).every((flag) => !!this.defense[flag]);
	}

	public addDefensePerks(...perks: UnitDefense.defenseFlags[]) {
		perks.filter((v) => isDefenseFlag()(v)).forEach((flag) => (this.defense[flag] = true));
		return this;
	}

	public removeDefensePerks(...perks: UnitDefense.defenseFlags[]) {
		this.defense = _.omit(this.defense, ...perks.filter((v) => isDefenseFlag()(v)));
		return this;
	}

	public applyAttackUnit(
		a: UnitAttack.TAttackUnit,
		source?: StatusEffectMeta.TStatusEffectTarget,
		damageModifier?: IModifier,
	) {
		const r: UnitDefense.IAttackEffectOnUnit = {
			hit: false,
			breach: false,
			shieldReduction: [],
			removed: false,
			effectsApplied: [],
			throughDamage: {},
			killed: false,
		};
		if (!AttackManager.isReachableByAttack(a.delivery, this.movement)) return r;
		if (AttackManager.isSelectiveAttack(a) && this.doesBelongToPlayer(source.playerId)) return r;
		const activeDefense = this.defense;
		Object.assign(r, applyAttackUnitToDefense(a, activeDefense, this, damageModifier));
		this._applyAttackResultToDefense(r, activeDefense, source);
		return {
			...r,
			killed: this.currentHitPoints <= 0,
		};
	}

	public destroy(source: StatusEffectMeta.TStatusEffectTarget, withSalvage = true) {
		this._unitKilled(source, !withSalvage);
		return this;
	}

	public hasEffect(effectId: number): boolean {
		return this.effects.filter((ef) => ef.static.id === effectId).length > 0;
	}

	public addEffect(effect: IStatusEffect<StatusEffectMeta.effectAgentTypes.unit>) {
		if (
			this.isImmuneToEffects ||
			(effect.static.negative && this.isImmuneToNegativeEffects) ||
			effect.static.targetType !== StatusEffectMeta.effectAgentTypes.unit
		)
			return this;
		effect.duration = GenericStatusEffect.applyDurationModifier(
			effect.duration,
			BasicMaths.stackModifiers(...(this.effectModifiers?.statusDurationModifier || [])),
		);
		if (effect.static.id === DOTStatusEffectId) {
			// keep 1 DoT per Source on Unit
			const existingDOT = this.effects.find(
				(e) => e.static.id === effect.static.id && effect.source.getInstanceId() === e.source.getInstanceId(),
			);
			if (existingDOT) {
				existingDOT.duration = Math.max(existingDOT.duration, effect.duration);
				return this;
			}
		}
		this.effects.push(effect);
		effect.triggerEvent(EffectEvents.TUnitEffectEvents.apply);
		return this;
	}

	public removeEffect(effectId: string) {
		const removedEffect = this.effects.find((v) => v.getInstanceId() === effectId);
		if (removedEffect && removedEffect.static.unremovable) return this;
		removedEffect.triggerEvent(EffectEvents.TUnitEffectEvents.expire);
		this.effects = this.effects.filter((v) => v.getInstanceId() === effectId);
		return this;
	}

	public addXp(xp: number): Units.IUnit {
		if (xp <= 0) return this;
		this.xp += xp;
		let nextLevel = 0;
		while (this.xp >= this.chassis?.static?.levelTree?.levelingFunction(nextLevel) || 0) {
			nextLevel++;
		}
		while (this.level < nextLevel) {
			this._levelUp();
		}
		return this;
	}

	public addHp(health: number, source?: StatusEffectMeta.TStatusEffectTarget): Units.IUnit {
		if (health < 0)
			this._damageToHp(
				{
					[Damage.damageType.warp]: health,
				},
				source,
			);
		this.currentHitPoints = Math.min(this.maxHitPoints, this.currentHitPoints + health || 0);
		return this;
	}

	public isTalentAvailable(talentId: UnitTalents.TUnitTalentId): boolean {
		let t = this._talents[talentId] as TTalentTreeInternal;
		if (!t) return false;
		if (t.talent.minimumLevel > this.level) return false;
		while (t.prev) {
			t = this._talents[t.prev];
			if (!this.talentsLearned.includes(t.talent.talentId)) return false;
		}
		return true;
	}

	public learnTalent(talentId: UnitTalents.TUnitTalentId): Units.IUnit {
		if (!this.isTalentAvailable(talentId)) return this;
		this._talents[talentId].talent.effect(this);
		this.talentsLearned.push(talentId);
		return this;
	}

	public hasPerks(...flags: UnitDefense.defenseFlags[]): boolean {
		for (let i = 0; i < (flags?.length || 0); i++) {
			if (!this.defense[flags[i]]) return false;
		}
		return true;
	}

	public serialize(): Units.TSerializedUnit {
		return {
			...this.__serializePlayerProperty(),
			stats: this.stats,
			cost: this.cost,
			prototypeId: this.prototypeId,
			currentHitPoints: this.currentHitPoints,
			currentMovementPoints: this.currentMovementPoints,
			movement: this.movement,
			orbitalAttacksPerformed: this.orbitalAttacksPerformed,
			scanRange: this.scanRange,
			abilitiesUsed: this.abilitiesUsed,
			actionDelayed: this.actionDelayed,
			location: this.location,
			maxHitPoints: this.maxHitPoints,
			speed: this.speed,
			chassisType: this.chassisType,
			targetFlags: this.targetFlags,
			attacks: this.attacks,
			defense: this.defense,
			actions: this.actions,
			status: this.status,
			xp: this.xp,
			level: this.level,
			talentsLearned: this.talentsLearned,
			talentPoints: this.talentPoints,
			effectModifiers: this.effectModifiers,
			effects: this.effects?.map((e) => e.serialize()),
			auras: this.auras?.map((v) => v.serialize()),
			kit: this.kit.save(),
		};
	}

	public callEffectEvent<T extends EffectEvents.TUnitEffectEvents>(payload?: EffectEvents.TUnitEffectMeta<T>) {
		if (!this.status.targetable)
			// Non-targetable units do not have effect ticks
			return this;
		if (this.effectCallback && !this.effectCallback[payload.event]) return this;
		if (!this.status.disabled)
			this.effects
				.sort((a, b) => a.priority - b.priority)
				.forEach((e) => {
					e.triggerEvent<EffectEvents.TUnitEffectEvents>(payload.event, {
						...this.getProvider(DIInjectableCollectibles.effect).getTurnMeta(),
						map: this.hostingMap,
						...payload,
					});
				});
		return this;
	}

	public getEffectiveActions(): UnitActions.TUnitActionOptions[] {
		return (this.chassis?.actions || []).map((actions) =>
			[
				...actions.filter((v) => v !== UnitActions.unitActionTypes.orbital_attack),
				...(this.canPerformOrbitalAttack() && actions.some(isOrbitalAttackAction)
					? [UnitActions.unitActionTypes.orbital_attack]
					: []),
			].filter(
				(action) =>
					!this.isDisabled &&
					(this.isMovable || !doesActionRequireSpeed(action)) &&
					(this.attacks?.filter(Boolean)?.length >= 2 || !doesActionRequireSeveralWeapons(action)) &&
					(!!this.attacks?.filter(Boolean)?.length ||
						!doesActionRequireWeapon(action) ||
						(!!this.kit?.total && doesActionUseAbility(action))),
			),
		);
	}

	public canPerformOrbitalAttack(): boolean {
		return this.attacks?.filter(Boolean)?.some((w) => w.baseRange >= ORBITAL_ATTACK_RANGE);
	}

	/**
	 *  What happens at the beginning of a turn
	 */
	protected _init() {
		this.abilitiesUsed = new Array(this.kit.total).fill(false);
		this.currentMovementPoints = this.speed;
		this.actionDelayed = null;
		this.orbitalAttacksPerformed = this.attacks.map((v) => false);
		this.status.currentTurnTick = 0;
		this.status.moving = null;
		this.status.commandChain = null;
		this.fillAttackFrames();
	}

	protected _unitKilled(source?: StatusEffectMeta.TStatusEffectTarget, overkill = false) {
		this.currentHitPoints = 0;
		const salvage = !overkill
			? this.getProvider(DIInjectableSerializables.salvage).createFromDestroyedUnit(
					this.prototype,
					source,
					true,
					false,
					true,
			  )
			: null;
		this.callEffectEvent({
			event: EffectEvents.TUnitEffectEvents.unitKilled,
			type: StatusEffectMeta.effectAgentTypes.unit,
			source,
			salvage,
			...this.getEventContextMeta(),
		});
		if (this.destroyCallback) this.destroyCallback.map((c) => c(source, this, salvage));
		return this;
	}

	protected _levelUp() {
		this.talentPoints++;
		this.level++;
		this.chassis?.static?.levelTree?.levelingEffectFunction(this.level, this);
	}

	protected _damageToHp(value: Damage.TDamageUnit, source: StatusEffectMeta.TStatusEffectTarget) {
		const damageToHp = DamageManager.getTotalDamageValue(value);
		if (!Number.isFinite(damageToHp)) return false;
		const deadlyDamage = this.currentHitPoints;
		this.currentHitPoints = Math.min(deadlyDamage - damageToHp, this.maxHitPoints);
		this.callEffectEvent({
			type: StatusEffectMeta.effectAgentTypes.unit,
			event: EffectEvents.TUnitEffectEvents.unitDamaged,
			source,
			damage: value,
			...this.getEventContextMeta(),
		});
		if (this.currentHitPoints <= 0) {
			this._unitKilled(source, damageToHp - deadlyDamage >= this.maxHitPoints);
		}
		return this;
	}

	protected _applyAttackResultToDefense(
		r: UnitDefense.IAttackEffectOnDefense,
		activeDefense: UnitDefense.IDefenseInterface,
		source: StatusEffectMeta.TStatusEffectTarget,
	) {
		this.defense = applyAttackEffectToDefense(r, activeDefense);
		if (r.effectsApplied && r.effectsApplied.length) {
			r.effectsApplied.forEach((eff) => {
				this.applyEffect(eff, source);
			});
		}
		if (r.breach) {
			return this._damageToHp(r.throughDamage, source);
		}
		return this;
	}

	protected _setTalents() {
		const mapTalent = (t: UnitTalents.IUnitTalentTreeItem, parent?: UnitTalents.IUnitTalentTreeItem) => {
			this._talents = {
				...this._talents,
				[t.talent.talentId]: {
					talent: t.talent,
					prev: parent ? parent.talent.talentId : null,
					next: t.unlocks.map((a) => mapTalent(a, t)),
				} as TTalentTreeInternal,
			};
			return t.talent.talentId;
		};
		(this.chassis?.static?.levelTree?.talentOptions || []).forEach((t) => mapTalent(t));
		return this;
	}
}

export default GenericUnit;
export type TUnit = GenericUnit;

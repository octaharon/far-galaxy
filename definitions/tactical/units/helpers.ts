import Units from './types';
import IUnit = Units.IUnit;

export const getMaxUnitLevel = (units: IUnit[]) =>
	Math.max(...[0].concat((units ?? []).filter(Boolean).map((v) => v.level)));

export const getMinUnitLevel = (units: IUnit[]) =>
	Math.min(...[0].concat((units ?? []).filter(Boolean).map((v) => v.level)));

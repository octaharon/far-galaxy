import { DEFAULT_PLAYER_RESEARCH_XP } from '../../../player/constants';
import UnitAttack from '../../damage/attack';
import UnitDefense from '../../damage/defense';
import UnitActions from './types';

export const ActionCaptions: UnitActions.TActionDictionary = {
	[UnitActions.unitActionTypes.skip]: 'Skip',
	[UnitActions.unitActionTypes.delay]: 'Delay',
	[UnitActions.unitActionTypes.move]: 'Move',
	[UnitActions.unitActionTypes.attack_move]: 'Attack Move',
	[UnitActions.unitActionTypes.attack]: 'Attack/Use',
	[UnitActions.unitActionTypes.barrage]: 'Barrage',
	[UnitActions.unitActionTypes.orbital_attack]: 'Orbital Attack',
	[UnitActions.unitActionTypes.defend]: 'Defend',
	[UnitActions.unitActionTypes.watch]: 'Watch',
	[UnitActions.unitActionTypes.retaliate]: 'Retaliate',
	[UnitActions.unitActionTypes.protect]: 'Protect',
	[UnitActions.unitActionTypes.suppress]: 'Suppress',
	[UnitActions.unitActionTypes.intercept]: 'Intercept',
	[UnitActions.unitActionTypes.cloak]: 'Cloak',
	[UnitActions.unitActionTypes.warp]: 'Warp',
	[UnitActions.unitActionTypes.blink]: 'Blink',
};

// Actions effects
export const DEFEND_MAX_DODGE_BONUS = 0.5; // 100%
export const DEFEND_MAX_DAMAGE_REDUCTION = 0.1; // 10%
export const ORBITAL_ATTACK_RANGE = 16;
export const BARRAGE_MAX_DAMAGE_REDUCTION = 0.15; // 15%
export const SUPPRESS_ACCURACY_REDUCTION = 0.5; // 50%
export const ATTACK_MOVE_SPEED_REDUCTION = 0.7;
export const SUPPRESS_RANGE_REDUCTION = 0.2; // 20%
export const INTERCEPT_DAMAGE_BONUS = 0.1; // 10%
export const INTERCEPT_HIT_CHANCE_BONUS = 0.5; // 50%
export const PROTECT_BASE_DODGE: UnitDefense.TDodgeModifier = {
	[UnitAttack.deliveryType.missile]: {
		bias: 0.5,
	},
	[UnitAttack.deliveryType.ballistic]: {
		bias: 0.5,
	},
	[UnitAttack.deliveryType.bombardment]: {
		bias: 1,
	},
	[UnitAttack.deliveryType.drone]: {
		bias: 1,
	},
};
export const PROTECT_BONUS_DODGE: UnitDefense.TDodgeModifier = {
	default: {
		bias: 0.5,
	},
};

// Actions XP

export const SUPPRESS_XP_BONUS = DEFAULT_PLAYER_RESEARCH_XP * 0.2;
export const PROTECT_XP_BONUS = DEFAULT_PLAYER_RESEARCH_XP * 0.2;
export const INTERCEPT_XP_BONUS = DEFAULT_PLAYER_RESEARCH_XP * 0.2;
export const DEFEND_XP_BONUS = DEFAULT_PLAYER_RESEARCH_XP * 0.2;
export const RETALIATE_XP_BONUS = DEFAULT_PLAYER_RESEARCH_XP * 0.1;
export const ORBITAL_ATTACK_XP_BONUS = DEFAULT_PLAYER_RESEARCH_XP * 5;
export const WATCH_XP_BONUS = DEFAULT_PLAYER_RESEARCH_XP * 0.1;

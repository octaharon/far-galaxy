import { DIEntityDescriptors, DIInjectableSerializables } from '../../../core/DI/injections';
import Serializable from '../../../core/Serializable';
import Units from '../types';
import UnitActions from './types';

export default class UnitAction<T extends UnitActions.unitActionTypes>
	extends Serializable<UnitActions.TUnitActionProps<T>, UnitActions.TSerializedUnitAction<T>>
	implements UnitActions.IUnitAction<T>
{
	public readonly sourceUnitId: Units.TUnitId;
	public readonly action: T;
	public meta: UnitActions.TUnitActionMeta<T>;
	public hasTarget: boolean;
	public status: UnitActions.unitActionStatus;

	get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableSerializables.action];
	}

	public get executor(): Units.IUnit {
		if (!this.sourceUnitId) return null;
		return this.getProvider(DIInjectableSerializables.unit)?.find(this.sourceUnitId);
	}

	public serialize(): UnitActions.TSerializedUnitAction<T> {
		return {
			...this.__serializeSerializable(),
			action: this.action,
			meta: this.meta,
			sourceUnitId: this.sourceUnitId,
			hasTarget: this.hasTarget,
			status: this.status,
		};
	}
}

export type TUnitCommandChain = Array<UnitAction<any>>;

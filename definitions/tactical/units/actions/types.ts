import InterfaceTypes from '../../../core/InterfaceTypes';
import { ISerializable } from '../../../core/Serializable';
import Units from '../types';

export namespace UnitActions {
	export enum unitActionTypes {
		/* skip phase */
		'skip' = 'ac_skip',
		/* move to destination */
		'move' = 'ac_move',
		/* move to destination, attacking closest enemies on the way wit a given isWeapon interface */
		'attack_move' = 'ac_attack_move',
		/* weapon a given target with one of Weapon Interfaces or Devices */
		'attack' = 'ac_attack',
		/* weapon with all weapon interfaces simultaneously, with minimum range */
		'barrage' = 'ac_barrage',
		/* go invisible for global scan and untargettable, still vulnerable to AoE effects. Can protect, move, defend and blink without breaking stealth (if not firing)*/
		'cloak' = 'ac_cloak',
		/* create a tear in time-space and go into Stasis till the end of the turn, removing all active Effects and losing all Shields */
		'warp' = 'ac_warp',
		/* teleport to a target location within movement range, once per turn */
		'blink' = 'ac_blink',
		/* unit will suppress fire of given Hostile at range, reducing that unit's range
         by 20% of this isWeapon's range, and additionally reduce their Hit Chance by 50% if selected weapon is Drone weapon */
		'suppress' = 'ac_suppress',
		/* unit will weapon first enemy, moving inside weapon radius, prioritizing closest ones */
		'watch' = 'ac_watch',
		/* unit will defend a target unit for this phase, giving him +25% dodge versus
         Missiles, Projectiles, +50% dodge versus Drones and Bombs, and extra 15% dodge,
         if the source unit is equipped with an Beam/Instant/Supersonic isWeapon */
		'protect' = 'ac_protect',
		/* Unit gains +50% dodge and -10% Incoming Damage for this phase */
		'defend' = 'ac_defend',
		/* unit will have this action choice after all units have moved. Can be done once per turn */
		'delay' = 'ac_delay',
		/* unit will weapon first enemy,     entering the given area (45 deg cone), having +10% damage and +50% accuracy in that area */
		'intercept' = 'ac_intercept',
		/* unit will shoot back at targets that attacks it */
		'retaliate' = 'ac_backfire',
		/* unit will perform an weapon on another player's Base, requires a range of ORBITAL_ATTACK_RANGE */
		'orbital_attack' = 'ac_orbital',
	}

	export enum unitActionStatus {
		plan = 'act_status_plan',
		command = 'act_status_command',
		running = 'act_status_running',
		complete = 'act_status_complete',
	}

	export type TUseActionMeta = Partial<{
		weaponSlots: number[];
		abilitySlots: number[];
	}>;
	export type TUnitActionMeta<T extends unitActionTypes> = T extends unitActionTypes.skip
		? {}
		: T extends unitActionTypes.move
		? {
				to: InterfaceTypes.IVector;
		  }
		: T extends unitActionTypes.attack_move
		? {
				to: InterfaceTypes.IVector;
		  } & TUseActionMeta
		: T extends unitActionTypes.attack
		? TUseActionMeta &
				(
					| {
							targetUnitId?: Units.TUnitId;
					  }
					| {
							targetMapCell?: InterfaceTypes.IVector;
					  }
				)
		: T extends unitActionTypes.barrage
		? TUseActionMeta &
				(
					| {
							targetUnitId?: Units.TUnitId;
					  }
					| {
							targetMapCell?: InterfaceTypes.IVector;
					  }
				)
		: T extends unitActionTypes.cloak
		? {}
		: T extends unitActionTypes.warp
		? {}
		: T extends unitActionTypes.blink
		? {
				to: InterfaceTypes.IVector;
		  }
		: T extends unitActionTypes.suppress
		? {
				targetUnitId?: Units.TUnitId;
		  } & TUseActionMeta
		: T extends unitActionTypes.watch
		? {} & TUseActionMeta
		: T extends unitActionTypes.protect
		? {
				targetUnitId?: Units.TUnitId;
		  } & TUseActionMeta
		: T extends unitActionTypes.defend
		? {}
		: T extends unitActionTypes.delay
		? {}
		: T extends unitActionTypes.intercept
		? {
				azimuth: number;
		  } & TUseActionMeta
		: T extends unitActionTypes.retaliate
		? TUseActionMeta
		: never;

	export const defaultActionOrder: unitActionTypes[] = [
		unitActionTypes.delay,
		unitActionTypes.blink,
		unitActionTypes.warp,
		unitActionTypes.cloak,
		unitActionTypes.attack_move,
		unitActionTypes.attack,
		unitActionTypes.barrage,
		unitActionTypes.orbital_attack,
		unitActionTypes.move,
		unitActionTypes.watch,
		unitActionTypes.retaliate,
		unitActionTypes.protect,
		unitActionTypes.intercept,
		unitActionTypes.suppress,
		unitActionTypes.defend,
		unitActionTypes.skip,
	];

	export type TUnitActionDefinition<T> = EnumProxy<unitActionTypes, T>;
	export type TUnitActionOptions = unitActionTypes[];

	export type TUnitActionChoice<T extends unitActionTypes> = {
		action: T;
		meta: TUnitActionMeta<T>;
	};

	type TMappedActions = {
		[T in unitActionTypes]: TUnitActionChoice<T>;
	};

	export type TUnitActionCommandOption = ValueOf<TMappedActions>;

	export type TUnitActionProps<T extends unitActionTypes> = {
		action: unitActionTypes;
		meta: TUnitActionMeta<T>;
		status: unitActionStatus;
		hasTarget: boolean;
		sourceUnitId: Units.TUnitId;
	};

	export type TSerializedUnitAction<T extends unitActionTypes> = TUnitActionProps<T> & IWithInstanceID;

	export type IUnitAction<T extends unitActionTypes> = TUnitActionProps<T> &
		ISerializable<TUnitActionProps<T>, TSerializedUnitAction<T>>;

	export type TActionDictionary = Required<EnumProxy<UnitActions.unitActionTypes, string>>;
}

export default UnitActions;

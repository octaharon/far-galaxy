import _ from 'underscore';
import Weapons from '../../../technologies/types/weapons';
import UnitAttack from '../../damage/attack';
import AttackManager from '../../damage/AttackManager';
import Damage from '../../damage/damage';
import UnitDefense from '../../damage/defense';
import Units from '../types';
import {
	BARRAGE_MAX_DAMAGE_REDUCTION,
	DEFEND_MAX_DAMAGE_REDUCTION,
	DEFEND_MAX_DODGE_BONUS,
	INTERCEPT_DAMAGE_BONUS,
	INTERCEPT_HIT_CHANCE_BONUS,
	PROTECT_BASE_DODGE,
	PROTECT_BONUS_DODGE,
	SUPPRESS_ACCURACY_REDUCTION,
	SUPPRESS_RANGE_REDUCTION,
} from './constants';
import UnitActions from './types';
import TUnitActionOptions = UnitActions.TUnitActionOptions;

export const grantActionOnPhase = <K extends UnitActions.TUnitActionOptions[]>(
	list: K,
	action: UnitActions.unitActionTypes,
	phase = 0,
) => list.map((item, ix) => (phase === ix ? _.uniq(item.concat(action)) : item));

export const revokeActionOnPhase = <K extends UnitActions.TUnitActionOptions[]>(
	list: K,
	action: UnitActions.unitActionTypes,
	phase = 0,
) => list.map((item, ix) => (phase === ix ? _.omit(item, action) : item));

export const grantAction = <K extends UnitActions.TUnitActionOptions[]>(
	list: K,
	...action: UnitActions.unitActionTypes[]
) => list.map((item) => _.uniq(item.concat(action)));

export const revokeAction = <K extends UnitActions.TUnitActionOptions[]>(
	list: K,
	...action: UnitActions.unitActionTypes[]
) => list.map((item) => _.without(item, ...action));

export const getLastActionPhase = <K extends UnitActions.TUnitActionOptions[]>(list: K): TUnitActionOptions =>
	(list || []).slice(-1)[0] || null;

export const addActionPhase = <K extends UnitActions.TUnitActionOptions[]>(list: K, amount = 1): K =>
	amount > 1 ? addActionPhase(list, amount - 1) : ([...list, getLastActionPhase(list)] as K);

export const removeActionPhase = <K extends UnitActions.TUnitActionOptions[]>(list: K) =>
	list?.length ? list.slice(0, list.length - 1) : list;

export const createActionOptions = (
	options: UnitActions.TUnitActionOptions,
	amount: number,
): UnitActions.TUnitActionOptions[] => new Array(amount).fill(null).map(() => options.slice());

export const getDefendDodgeModifier = (unit?: Units.IUnit): UnitDefense.TDodgeModifier => {
	return {
		default: {
			bias: (unit?.speed ? unit.currentMovementPoints / unit.speed : 1) * DEFEND_MAX_DODGE_BONUS,
		},
	};
};

export const getDefendDamageModifier = (): Damage.IDamageModifier => {
	return {
		factor: 1 - DEFEND_MAX_DAMAGE_REDUCTION,
	};
};

export const getBarrageDamageModifier = (): Damage.IDamageModifier => {
	return {
		factor: 1 - BARRAGE_MAX_DAMAGE_REDUCTION,
	};
};

export const getSuppressAttackModifier = (attackInterface: UnitAttack.IAttackInterface): Weapons.TWeaponModifier => {
	const directAttacks = AttackManager.getInterfaceDirectAttacks(attackInterface, true);
	if (!directAttacks.length) return {};
	const r: Weapons.TWeaponModifier = {
		baseRange: {
			bias: -attackInterface.baseRange * SUPPRESS_RANGE_REDUCTION,
			min: 0,
			minGuard: 0,
		},
	};
	if (
		directAttacks.some((attack) =>
			[
				UnitAttack.deliveryType.beam,
				UnitAttack.deliveryType.supersonic,
				UnitAttack.deliveryType.immediate,
			].includes(attack.delivery),
		)
	)
		r.baseAccuracy = {
			bias: -SUPPRESS_ACCURACY_REDUCTION,
		};
	return r;
};

export const getInterceptAttackModifier = (attackInterface?: UnitAttack.IAttackInterface): Weapons.TWeaponModifier => {
	if (attackInterface) {
		const directAttacks = AttackManager.getInterfaceDirectAttacks(attackInterface, true);
		if (!directAttacks.length) return {};
	}
	return {
		baseAccuracy: {
			bias: INTERCEPT_HIT_CHANCE_BONUS,
		},
		damageModifier: {
			factor: 1 + INTERCEPT_DAMAGE_BONUS,
		},
	};
};

export const getProtectDodgeModifiers = (
	attackInterface?: UnitAttack.IAttackInterface,
): UnitDefense.TDodgeModifier[] => {
	if (attackInterface) {
		const directAttacks = AttackManager.getInterfaceDirectAttacks(attackInterface, true);
		if (!directAttacks.length) return [];
		return [
			PROTECT_BASE_DODGE,
			directAttacks.some((attack) =>
				[
					UnitAttack.deliveryType.beam,
					UnitAttack.deliveryType.supersonic,
					UnitAttack.deliveryType.immediate,
				].includes(attack.delivery),
			)
				? PROTECT_BONUS_DODGE
				: undefined,
		].filter(Boolean);
	}
	return [PROTECT_BASE_DODGE];
};

export const isFinalAction = (action: UnitActions.unitActionTypes) =>
	[
		UnitActions.unitActionTypes.skip,
		UnitActions.unitActionTypes.delay,
		UnitActions.unitActionTypes.blink,
		UnitActions.unitActionTypes.cloak,
		UnitActions.unitActionTypes.warp,
	].includes(action);

export const doesActionRequireWeapon = (action: UnitActions.unitActionTypes) =>
	[
		UnitActions.unitActionTypes.attack,
		UnitActions.unitActionTypes.retaliate,
		UnitActions.unitActionTypes.barrage,
		UnitActions.unitActionTypes.protect,
		UnitActions.unitActionTypes.attack_move,
		UnitActions.unitActionTypes.intercept,
		UnitActions.unitActionTypes.suppress,
		UnitActions.unitActionTypes.watch,
		UnitActions.unitActionTypes.orbital_attack,
	].includes(action);

export const doesActionRequireSeveralWeapons = (action: UnitActions.unitActionTypes) =>
	[UnitActions.unitActionTypes.barrage].includes(action);

export const isOrbitalAttackAction = (action: UnitActions.unitActionTypes) =>
	[UnitActions.unitActionTypes.barrage, UnitActions.unitActionTypes.attack].includes(action);

export const doesActionRequireSpeed = (action: UnitActions.unitActionTypes) =>
	[
		UnitActions.unitActionTypes.blink,
		UnitActions.unitActionTypes.attack_move,
		UnitActions.unitActionTypes.move,
	].includes(action);

export const doesActionUseAbility = (action: UnitActions.unitActionTypes) =>
	[UnitActions.unitActionTypes.attack].includes(action);

export default {
	grantActionOnPhase,
	revokeActionOnPhase,
	grantAction,
	revokeAction,
	addActionPhase,
	removeActionPhase,
	createActionOptions,
	getDefendDodgeModifier,
	getDefendDamageModifier,
	getBarrageDamageModifier,
	getSuppressAttackModifier,
	getInterceptAttackModifier,
	getProtectDodgeModifiers,
	isFinalAction,
	getLastActionPhase,
	doesActionRequireWeapon,
	canActionUseAbility: doesActionUseAbility,
};

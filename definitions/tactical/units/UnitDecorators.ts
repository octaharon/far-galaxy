import Weapons from '../../technologies/types/weapons';
import UnitDefense from '../damage/defense';

export namespace UnitDecorators {
	export interface IUnitWeaponModifier {
		weaponModifier?: Weapons.TWeaponModifier;
	}

	export interface IUnitArmorModifier {
		armorModifier?: UnitDefense.TArmorModifier;
	}

	export interface IUnitShieldsModifier {
		shieldModifier?: UnitDefense.TShieldsModifier;
	}

	export interface IUnitDodgeModifier {
		dodge?: UnitDefense.TDodgeModifier;
	}

	export interface IUnitDodgeDecorator {
		dodge?: UnitDefense.TDodge;
	}

	export interface ICostModifier {
		baseCost?: IModifier;
	}

	export interface IPriorityModifier {
		priority?: IModifier;
	}

	export interface IUnitRangeModifier {
		range?: IModifier;
	}

	export interface IUnitScanRangeModifier {
		scanRange?: IModifier;
	}

	export interface IUnitHitPointsModifier {
		hitPoints?: IModifier;
	}

	export interface IUnitSpeedModifier {
		speed?: IModifier;
	}

	export type TUnitModifier = IUnitWeaponModifier &
		IUnitArmorModifier &
		IUnitShieldsModifier &
		IUnitScanRangeModifier &
		IUnitDodgeModifier &
		ICostModifier &
		IUnitRangeModifier &
		IUnitSpeedModifier &
		IUnitHitPointsModifier;
}

export default UnitDecorators;

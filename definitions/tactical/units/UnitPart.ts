import Collectible, {
	CollectibleFactory,
	ICollectible,
	ICollectibleFactory,
	IStaticCollectible,
	TCollectible,
	TSerializedCollectible,
} from '../../core/Collectible';
import InterfaceTypes from '../../core/InterfaceTypes';

export interface IUnitPartDecorator {
	baseCost: number;
}

export type IUnitPartModifier = Proxy<IUnitPartDecorator, IModifier>;

export interface IStaticUnitPart extends IStaticCollectible {
	salvageable?: boolean;
}

export interface IUnitPart<
	PropType extends IUnitPartDecorator,
	SerializedType extends TSerializedCollectible & PropType,
	MetaType extends {} = {},
	StaticType extends IStaticUnitPart = IStaticUnitPart,
> extends IUnitPartDecorator,
		ICollectible<PropType, SerializedType, StaticType> {
	baseCost: number;
	meta: MetaType;
	static: TCollectible<IUnitPart<PropType, SerializedType, MetaType, StaticType>, StaticType>;
}

export abstract class UnitPart<
		PropType extends IUnitPartDecorator,
		SerializedType extends TSerializedCollectible & PropType,
		MetaType extends {} = {},
		StaticType extends IStaticCollectible = IStaticCollectible,
	>
	extends Collectible<PropType, SerializedType, StaticType>
	implements IUnitPart<PropType, SerializedType, MetaType, StaticType>
{
	public static readonly id: number = 0;
	public static readonly salvageable: boolean = true;
	public baseCost: number;
	public meta: MetaType;

	public constructor(p: Partial<PropType>, meta: MetaType = null) {
		super(p);
		if (meta) this.meta = meta;
	}

	protected __serializeUnitPart() {
		return {
			...this.__serializeCollectible(),
			baseCost: this.baseCost,
		};
	}
}

export default UnitPart;

export type TSerializedUnitPart = IUnitPartDecorator & TSerializedCollectible;

export interface IUnitPartFactory<
	PropType extends IUnitPartDecorator,
	SerializedType extends PropType & TSerializedUnitPart,
	ObjectType extends IUnitPart<PropType, SerializedType, MetaType, StaticType>,
	MetaType extends {} = {},
	StaticType extends IStaticUnitPart = IStaticUnitPart,
> extends ICollectibleFactory<PropType, SerializedType, ObjectType, StaticType> {
	getItemMeta(id: number): MetaType;
}

export abstract class UnitPartFactory<
		PropType extends IUnitPartDecorator,
		SerializedType extends PropType & TSerializedCollectible,
		ObjectType extends IUnitPart<PropType, SerializedType, MetaType, StaticType>,
		MetaType extends {} = {},
		StaticType extends IStaticUnitPart = IStaticUnitPart,
	>
	extends CollectibleFactory<PropType, SerializedType, ObjectType, StaticType>
	implements IUnitPartFactory<PropType, SerializedType, ObjectType, MetaType, StaticType>
{
	protected __lookupById: InterfaceTypes.TIdMap<MetaType> = {};

	public unserialize(t: SerializedType): ObjectType {
		if (!t) return null;
		const f = this.instantiate(t, t.id);
		if (!f) return null;
		f.meta = this.getItemMeta(t.id);
		f.setProps(t);
		f.setInstanceId(t.instanceId);
		return f;
	}

	public getItemMeta(id: number): MetaType {
		return this.__lookupById[id];
	}

	public instantiate(props: Partial<PropType>, id: number) {
		return this.__instantiateUnitPart(props, id);
	}

	protected __fillItems<C extends TCollectible<ObjectType, StaticType>>(
		objects: C[],
		metaCallback: (a: C) => MetaType,
		cacheCallback?: (a: C) => any,
	): UnitPartFactory<PropType, SerializedType, ObjectType, MetaType, StaticType> {
		super.__fillItems(objects, (o: C) => {
			this.__lookupById[o.id] = metaCallback(o);
			if (cacheCallback) cacheCallback(o);
		});
		return this;
	}

	protected __instantiateUnitPart(props: Partial<PropType>, id: number) {
		const c = this.__instantiateCollectible(props, id, this.getItemMeta(id));
		if (!c) throw new Error(`Can't instantiate UnitPart ${id} with props ${JSON.stringify(props)}`);
		return c;
	}
}

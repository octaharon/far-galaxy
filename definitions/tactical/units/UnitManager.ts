import _ from 'underscore';
import { DIInjectableCollectibles, DIInjectableSerializables } from '../../core/DI/injections';
import { SerializableFactory } from '../../core/Serializable';
import { IPrototype } from '../../prototypes/types';
import { AbilityKit } from '../abilities/AbilityKit';
import UnitAttack from '../damage/attack';
import AttackManager from '../damage/AttackManager';
import UnitDefense from '../damage/defense';
import DefenseManager from '../damage/DefenseManager';
import GenericUnit, { defaultUnitProps } from './GenericUnit';
import Units from './types';

class UnitManager
	extends SerializableFactory<Units.TUnitProps, Units.TSerializedUnit, Units.IUnit>
	implements Units.IUnitFactory
{
	public instantiate(props: Partial<Units.TUnitProps>) {
		const c = this.__instantiateSerializable(
			{
				...defaultUnitProps,
				...props,
			},
			GenericUnit,
		);
		c.setDestroyedCallback((source, unit, salvage) => {
			this.destroy(unit.getInstanceId());
		});
		return this.inject(c);
	}

	public unserialize(t: Units.TSerializedUnit) {
		const c = this.instantiate({
			...t,
			kit: null,
			auras: t.auras.map((a) => this.getProvider(DIInjectableCollectibles.aura).unserialize(a)),
			effects: t.effects.map((e) => this.getProvider(DIInjectableCollectibles.effect).unserialize(e)),
		});
		c.kit = new AbilityKit(c, t.kit);
		c.setInstanceId(t.instanceId);
		return c;
	}

	public createFromPrototype(t: IPrototype): Units.IUnit {
		if (!t?.getChassis()) return null;
		const attackList = Object.values(t.weapons)
			.map((w, ix) => {
				const i = w?.compiled;
				if (i)
					i.caption =
						i.caption +
						this.getProvider(DIInjectableCollectibles.weapon).getSlotSuffix(
							t.chassis.static.weaponSlots[ix],
						);
				return i;
			})
			.filter(Boolean);
		attackList.sort((a, b) => (a.priority || 0) - (b.priority || 0));

		const defenseInterface = {
			...({
				armor: DefenseManager.getTotalArmor(...t.armor.map((v) => v?.compiled).filter(Boolean)),
				dodge: DefenseManager.stackDodge(
					t.getChassis()?.dodge,
					...t.armor
						.filter(Boolean)
						.map((a) => a.static.dodgeModifier)
						.filter((v) => !!v),
				),
				caption: t.armor
					.filter(Boolean)
					.map((a) => a.static.caption)
					.join(' & '),
				shields: t.shields.map((s) => s?.compiled).filter(Boolean),
				priority: 5,
				id: `native`,
			} as UnitDefense.IDefenseInterface),
			...DefenseManager.getTotalFlags(
				...[]
					.concat(
						t.armor.filter(Boolean).map((v) => v.static.flags),
						t.shields.filter(Boolean).map((v) => v.static.flags),
						t.getChassis()?.defenseFlags,
					)
					.filter((v) => !!v),
			),
		} as UnitDefense.IDefenseInterface;

		const abilities = [
			...t.equipment.filter(Boolean).reduce((list, eq) => list.concat(eq.static.abilities), [] as number[]),
			...t.getChassis().static.abilities,
		].filter(Boolean);

		const auras = t.equipment
			.filter(Boolean)
			.reduce((au, e) => au.concat(...(e.static.auras || [])), (t.getChassis()?.static.auras || []).slice())
			.filter((v) => !!v);

		const actions = t.equipment.filter(Boolean).reduce((ac, eq) => {
			if (eq.static.actions) {
				ac = ac.map((a) => a.concat(eq.static.actions));
			}
			return ac;
		}, t.getChassis()?.actions || []);

		this.getProvider(DIInjectableSerializables.prototype).store(t);

		const unit = this.instantiate({
			playerId: t.playerId,
			prototypeId: t.getInstanceId(),
			currentHitPoints: t.getChassis()?.hitPoints,
			maxHitPoints: t.getChassis()?.hitPoints,
			speed: t.getChassis()?.speed,
			cost: t.cost,
			chassisType: t.getChassis()?.static.type,
			targetFlags: t.getChassis()?.static.flags,
			scanRange: t.getChassis()?.scanRange,
			movement: t.getChassis()?.static.movement,
			attacks: attackList,
			defense: defenseInterface,
			auras: auras.map((a) => this.getProvider(DIInjectableCollectibles.aura).instantiate(a.props, a.id)),
			actions,
		});

		// Creating abilities
		abilities.forEach((abilityId) => unit.addAbility(abilityId));

		// embedding permanent Status Effects
		[...t.getChassis().static.effects, ...t.equipment.reduce((list, eq) => list.concat(eq.static.effects), [])]
			.filter(Boolean)
			.forEach((e) => unit.applyEffect(e, unit));

		unit.applyEffectModifier(t.modifiers.unitModifier);

		return unit;
	}
}

export default UnitManager;

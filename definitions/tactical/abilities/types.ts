import {
	ICollectible,
	ICollectibleFactory,
	IStaticCollectible,
	TCollectible,
	TSerializedCollectible,
} from '../../core/Collectible';
import { DIContainerDependency, IDIContainerDependency } from '../../core/DI/types';
import { IPlayerBase } from '../../orbital/base/types';
import { IUnitEffectModifier } from '../../prototypes/types';
import { IMapCell, TMapCellProps } from '../map';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import { IStatusEffect, TEffectApplianceList } from '../statuses/types';
import Units from '../units/types';

export namespace Abilities {
	export enum abilityTargetTypes {
		none = 'abl_none',
		unit = 'abl_unit', // ability can target any unit
		self = 'abl_self', // ability is cast on oneself
		map = 'abl_map', // ability is cast on a map point
		player = 'abl_player', // ability is cast on a player
		player_self = 'abl_player_self', // ability is cast on an invoking player
		around = 'abl_circle', // ability is cast on all units around the caster
	}

	export type TAbilityTargetType<T extends abilityTargetTypes> = T extends abilityTargetTypes.player
		? IPlayerBase
		: T extends abilityTargetTypes.player_self
		? IPlayerBase
		: T extends abilityTargetTypes.unit
		? Units.IUnit
		: T extends abilityTargetTypes.self
		? Units.IUnit
		: T extends abilityTargetTypes.around
		? Units.IUnit
		: T extends abilityTargetTypes.map
		? IMapCell
		: never;

	export type TAbilityEffectTargetType<T extends abilityTargetTypes> = T extends abilityTargetTypes.map
		? StatusEffectMeta.effectAgentTypes.map
		: T extends abilityTargetTypes.player
		? StatusEffectMeta.effectAgentTypes.player
		: T extends abilityTargetTypes.player_self
		? StatusEffectMeta.effectAgentTypes.player
		: StatusEffectMeta.effectAgentTypes.unit;

	export type TAbilityAgents = EnumProxy<abilityTargetTypes, boolean>;

	export type TAbilityProps = {
		cooldownLeft: number;
		usable: boolean;
		cooldown: number;
		baseRange: number;
	};

	export type TAbilityCastMeta<K extends Abilities.abilityTargetTypes> = {
		target: K;
		ability: Abilities.IAbility<K>;
	};

	export type TAbilityCastMetaOption = ValueOf<{
		[T in Abilities.abilityTargetTypes]: TAbilityCastMeta<T>;
	}>;

	export type IStaticAbility<T extends abilityTargetTypes> = IStaticCollectible &
		TAbilityTypedDecorator<T> & {
			readonly appliedEffects: number[];
		};

	export type TStaticAbilityOption = ValueOf<{
		[T in abilityTargetTypes]: IStaticAbility<T>;
	}>;

	export type TSerializedAbility = TAbilityProps & TSerializedCollectible;

	export type TAbilityTypedDecorator<T extends abilityTargetTypes> = T extends abilityTargetTypes.map
		? { mapCellProps: Partial<TMapCellProps> }
		: {};

	export interface IAbility<T extends abilityTargetTypes>
		extends ICollectible<TAbilityProps, TSerializedAbility, IStaticAbility<T>>,
			TAbilityProps {
		readonly targetType: abilityTargetTypes;

		init(): this;

		applyModifier(m: IUnitEffectModifier): this;

		onTurnStart(): this;

		onTurnEnd(): this;
	}

	export type TAbilityOption = ValueOf<{
		[T in abilityTargetTypes]: IAbility<T>;
	}>;

	export type TAbilityCollectionItem = {
		abilityId: number;
		props?: Partial<TAbilityProps>;
	};

	export type TAbilitySet = Record<number, TSerializedAbility>;

	export type TAbilityKitProps = {
		abilities: TAbilitySet;
		modifiers: IUnitEffectModifier;
		order: number[];
	};

	export interface IAbilityKit extends TAbilityKitProps, IDIContainerDependency {
		abilityPower: number;
		total: number;
		owner:
			| StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.unit>
			| StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.player>;

		load(props: TAbilityKitProps): this;

		save(): TAbilityKitProps;

		addAbility(abilityId: number): this;

		setAbilityOrder(abilityId: number, order: number): this;

		removeAbility(abilityId: number): this;

		clearAbilities(): this;

		getAbility(abilityId: number): IAbility<any>;

		getOrderedAbilities(): Array<IAbility<any>>;

		getStableSortedAbilities(): Array<IAbility<any>>;

		addAbilityCooldown(cooldown?: number, abilityId?: number): this;

		setAbilityCooldown(cooldown?: number, abilityId?: number): this;

		setAbilityEnabled(enabled?: boolean, abilityId?: number): this;

		applyModifier(...modifiers: IUnitEffectModifier[]): this;

		getEffectAppliance<T extends abilityTargetTypes>(abilityId: number): TEffectApplianceList;

		instantiateEffects<T extends abilityTargetTypes>(
			abilityId: number,
			target: Abilities.TAbilityTargetType<T>,
		): Array<IStatusEffect<TAbilityEffectTargetType<T>>>;
	}

	export interface IWithAbilityKit extends DIContainerDependency {
		kit: IAbilityKit;

		addAbility(abilityId: number): this;

		removeAbility(abilityId: number): this;

		clearAbilities(): this;
	}

	export interface ISerializedWithAbilityKit {
		kit: TAbilityKitProps;
	}

	export interface IAbilityFactory
		extends ICollectibleFactory<TAbilityProps, TSerializedAbility, TAbilityOption, TStaticAbilityOption> {
		instantiateEffects<T extends abilityTargetTypes>(
			ability: IAbility<T>,
			source: StatusEffectMeta.TStatusEffectTarget,
			target: Abilities.TAbilityTargetType<T>,
		): Array<IStatusEffect<TAbilityEffectTargetType<T>>>;
	}

	export type TAbilityCollection = TAbilityCollectionItem[];
}

export default Abilities;
export type TAbility<
	T extends Abilities.abilityTargetTypes,
	O extends Abilities.IAbility<T> = Abilities.IAbility<T>,
> = Abilities.IStaticAbility<T> & TCollectible<O, Abilities.IStaticAbility<T>>;
export type TAbilityList<T extends Abilities.abilityTargetTypes> = Array<TAbility<T>>;

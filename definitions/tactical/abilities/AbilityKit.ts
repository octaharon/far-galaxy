import _ from 'underscore';
import { DIInjectableCollectibles } from '../../core/DI/injections';
import { DIContainerDependency } from '../../core/DI/types';
import BasicMaths from '../../maths/BasicMaths';
import { IUnitEffectModifier } from '../../prototypes/types';
import { DEFAULT_EFFECT_POWER } from '../statuses/constants';
import GenericStatusEffect from '../statuses/GenericStatusEffect';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import { IStatusEffect, TEffectApplianceList, TEffectProps } from '../statuses/types';
import Abilities from './types';
import IAbility = Abilities.IAbility;
import IAbilityKit = Abilities.IAbilityKit;

/**
 * This class represents a collection of Abilities for a Unit or a Base,
 * storing their continuous state, like cooldown and stuff
 *
 * This object is created from scratch every time a Unit or Player is added to the world,
 * and then stored within corresponding consumer's serialized body
 *
 * This collection is what should be used by consumers and game engine
 * for operating Abilities during Combat
 */
export class AbilityKit extends DIContainerDependency implements IAbilityKit {
	abilities: Abilities.TAbilitySet;
	order: number[];
	owner:
		| StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.unit>
		| StatusEffectMeta.TTargetType<StatusEffectMeta.effectAgentTypes.player>;
	modifiers: IUnitEffectModifier;
	#instances: Record<number, IAbility<any>>;

	constructor(owner: IAbilityKit['owner'], props?: Abilities.TAbilityKitProps) {
		super();
		this.owner = owner;
		if (owner) this.injectDependencies(owner.DIContainer);
		this.load(props);
	}

	public get total() {
		return Object.keys(this.abilities ?? {}).length;
	}

	public get abilityPower() {
		const basePower = GenericStatusEffect.isUnitTarget(this.owner)
			? this.owner.abilityPower ?? DEFAULT_EFFECT_POWER
			: GenericStatusEffect.isPlayerTarget(this.owner)
			? this.owner.currentEconomy.abilityPower ?? DEFAULT_EFFECT_POWER
			: DEFAULT_EFFECT_POWER;
		return BasicMaths.applyModifier(basePower, ...(this.modifiers?.abilityPowerModifier || []));
	}

	setAbilityOrder(abilityId: number, order: number) {
		if (!this.abilities?.[abilityId] || !Number.isFinite(order) || order < 0 || order >= this.total) return this;
		const newOrder = this.order.filter((v) => v !== abilityId);
		newOrder.splice(order, 0, abilityId);
		this.order = newOrder;
		return this;
	}

	getOrderedAbilities() {
		return (this.order ?? []).map((id) => this.getAbility(id));
	}

	getStableSortedAbilities(): Array<Abilities.IAbility<any>> {
		return Object.keys(this.abilities).map((abilityId) => this.#instances[abilityId]);
	}

	/**
	 * Without params, reduce all active abilities cooldown by 1
	 * With 1 param adds a given value to all abilities' cooldowns
	 * With 2 params adds a set cooldown to a specific ability, if it exists and available, otherwise do nothing
	 *
	 * @param cooldown which value to add to ability(-es) cooldown, default: -1
	 * @param abilityId ID of an ability to modify, otherwise the cooldown applied to all abilities
	 */
	addAbilityCooldown(cooldown = -1, abilityId?: number) {
		if (Number.isFinite(abilityId))
			if (this.#instances?.[abilityId]?.usable) {
				const ability = this.#instances[abilityId];
				ability.cooldownLeft = Math.max(0, ability.cooldownLeft + cooldown);
				this.save();
				return this;
			} else return this;
		//without abilityId it is applied to all abilities
		this.getStableSortedAbilities().forEach((ability) => {
			if (!ability.usable) return;
			ability.cooldownLeft = Math.max(0, ability.cooldownLeft + cooldown);
		});
		this.save();
		return this;
	}

	/**
	 * Without params, set all cooldowns to 0
	 * With 1 param adds a given value to all abilities' cooldowns
	 * With 2 params adds a set cooldown to a specific ability, if it exists and available, otherwise do nothing
	 *
	 * @param cooldown which value to set to ability(-es) current cooldown, default: 0
	 * @param abilityId ID of an ability to modify, otherwise the cooldown applied to all abilities
	 */
	setAbilityCooldown(cooldown = 0, abilityId?: number) {
		if (Number.isFinite(abilityId))
			if (this.#instances?.[abilityId]?.usable) {
				const ability = this.#instances[abilityId];
				ability.cooldownLeft = Math.max(0, cooldown);
				this.save();
				return this;
			} else return this;
		//without abilityId it is applied to all abilities
		this.getStableSortedAbilities().forEach((ability) => {
			if (!ability.usable) return;
			ability.cooldownLeft = Math.max(0, cooldown);
		});
		this.save();
		return this;
	}

	/**
	 * Without params, enable all abilities
	 * With 1 param either enable or disable all abilities
	 * With 2 params enables or disabled a specific ability, if it exists and available, otherwise do nothing
	 *
	 * @param enabled enables or disables an ability(-ies)
	 * @param abilityId ID of an ability to modify, otherwise the usability status is applied to all abilities
	 */
	setAbilityEnabled(enabled = true, abilityId?: number) {
		if (Number.isFinite(abilityId))
			if (this.#instances?.[abilityId]) {
				const ability = this.#instances[abilityId];
				ability.usable = enabled ?? true;
				this.save();
				return this;
			} else return this;
		this.getStableSortedAbilities().forEach((ability) => {
			ability.usable = enabled ?? true;
		});
		this.save();
		return this;
	}

	applyModifier(...m: IUnitEffectModifier[]) {
		if (!this.modifiers) this.modifiers = {};
		[]
			.concat(m)
			.filter(Boolean)
			.forEach((mod) => {
				//saving relevant modifier properties to local queue
				if (mod.abilityCooldownModifier) {
					this.modifiers.abilityCooldownModifier = (this.modifiers.abilityCooldownModifier || []).concat(
						mod.abilityCooldownModifier,
					);
				}
				if (mod.abilityRangeModifier) {
					this.modifiers = {
						...(this.modifiers || {}),
						abilityRangeModifier: (this.modifiers?.abilityRangeModifier || []).concat(
							mod.abilityRangeModifier,
						),
					};
				}
				//applying a modifier to the ability
				this.getOrderedAbilities().forEach((ab) => ab.applyModifier(mod));
			});
		return this;
	}

	getAbility(abilityId: number): Abilities.IAbility<any> {
		return Number.isFinite(abilityId) ? this.#instances[abilityId] || null : null;
	}

	getEffectAppliance<T extends Abilities.abilityTargetTypes>(abilityId: number): TEffectApplianceList {
		const ability = this.getAbility(abilityId);
		if (!ability) return [];
		return ability.static.appliedEffects.map((effectId) => ({
			effectId,
			props: {
				basePower: this.abilityPower,
			} as TEffectProps<StatusEffectMeta.effectAgentTypes.unit>,
		}));
	}

	instantiateEffects<T extends Abilities.abilityTargetTypes>(
		abilityId: number,
		target: Abilities.TAbilityTargetType<T>,
	) {
		const ability = this.getAbility(abilityId);
		if (!ability) return [];
		return this.getProvider(DIInjectableCollectibles.ability).instantiateEffects(
			ability,
			this.owner,
			target,
		) as Array<IStatusEffect<Abilities.TAbilityEffectTargetType<T>>>;
	}

	load(props: Abilities.TAbilityKitProps) {
		this.abilities = props?.abilities || {};
		this.modifiers = props?.modifiers || {};
		this.order = props?.order || [];
		this.instantiateAbilities();
		return this;
	}

	save(): Abilities.TAbilityKitProps {
		this.abilities = _.mapObject(this.#instances ?? {}, (ability) => ability.serialize());
		this.order = this.order.filter((v) => !Object.keys(this.abilities).includes(v));

		return {
			abilities: this.abilities,
			order: this.order,
			modifiers: this.modifiers,
		};
	}

	addAbility(abilityId: number) {
		if (Number.isFinite(abilityId) && !this.abilities[abilityId]) {
			const instance = this.getProvider(DIInjectableCollectibles.ability).instantiate(null, abilityId);
			instance.applyModifier(this.modifiers).init();
			this.abilities[abilityId] = instance.serialize();
			this.#instances[abilityId] = instance;
			this.order = [...(this.order ?? []), abilityId];
		}
		return this;
	}

	clearAbilities() {
		this.abilities = {};
		this.#instances = {};
		return this;
	}

	removeAbility(abilityId: number) {
		if (Number.isFinite(abilityId) && this.abilities[abilityId]) {
			delete this.abilities[abilityId];
			delete this.#instances[abilityId];
			this.order = (this.order ?? []).filter((v) => v !== abilityId);
		}
		return this;
	}

	protected instantiateAbilities() {
		this.#instances = _.mapObject(this.abilities, (serializedAbility) =>
			this.getProvider(DIInjectableCollectibles.ability).unserialize(serializedAbility),
		);
	}
}

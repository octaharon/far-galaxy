import GenericAbility from './GenericAbility';
import { TAbility } from './types';

let AbilityList = [] as any;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: any[]) {
	return r.keys().forEach((key) => cache.push(r(key)));
}

requireAll(require.context('./all/', true, /\.ts$/), AbilityList);
AbilityList = AbilityList.filter(
	(module: any) => module.default.prototype instanceof GenericAbility && module.default.id > 0,
).map((module: any) => module.default);
export default AbilityList as Array<TAbility<any>>;

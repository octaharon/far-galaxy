import OrbitalGuidanceStatusEffect from '../../../statuses/Effects/all/Guidance/OrbitalGuidance.02003';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class OrbitalTargetingAbility extends GenericAbility<TargetType> {
	public static readonly id = 10003;
	public static readonly caption = 'Orbital Targeting';
	public static readonly description = OrbitalGuidanceStatusEffect.description;
	public static readonly appliedEffects = [OrbitalGuidanceStatusEffect.id];
	public cooldown = 3;
	public baseRange = 6;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default OrbitalTargetingAbility;

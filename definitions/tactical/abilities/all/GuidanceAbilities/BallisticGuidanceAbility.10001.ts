import BallisticGuidanceStatusEffect from '../../../statuses/Effects/all/Guidance/BallisticGuidance.02002';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class BallisticGuidanceAbility extends GenericAbility<TargetType> {
	public static readonly id = 10001;
	public static readonly caption = 'Ballistic Support';
	public static readonly description = BallisticGuidanceStatusEffect.description;
	public static readonly appliedEffects = [BallisticGuidanceStatusEffect.id];
	public cooldown = 3;
	public baseRange = 7;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default BallisticGuidanceAbility;

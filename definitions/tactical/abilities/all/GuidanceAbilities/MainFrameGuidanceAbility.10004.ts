import MainframeGuidanceStatusEffect from '../../../statuses/Effects/all/Guidance/MainframeGuidance.02005';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class MainframeGuidanceAbility extends GenericAbility<TargetType> {
	public static readonly id = 10004;
	public static readonly caption = 'Mainframe support';
	public static readonly description = MainframeGuidanceStatusEffect.description;
	public static readonly appliedEffects = [MainframeGuidanceStatusEffect.id];
	public cooldown = 3;
	public baseRange = 9;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default MainframeGuidanceAbility;

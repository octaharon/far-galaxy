import ComputerGuidanceStatusEffect from '../../../statuses/Effects/all/Guidance/ComputerGuidance.02004';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.around;

export class ComputerGuidanceAbility extends GenericAbility<TargetType> {
	public static readonly id = 10002;
	public static readonly caption = 'Computer Guidance';
	public static readonly description = ComputerGuidanceStatusEffect.description;
	public static readonly appliedEffects = [ComputerGuidanceStatusEffect.id];
	public cooldown = 5;
	public baseRange = 3;
	public readonly targetType = Abilities.abilityTargetTypes.around;
}

export default ComputerGuidanceAbility;

import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import OrbitalStrikeEffect from '../../../statuses/Effects/all/Orbital/OrbitalStrike.45000';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class OrbitalStrikeAbility extends GenericAbility<TargetType> {
	public static readonly id = 45000;
	public static readonly caption = 'Orbital Strike';
	public static readonly description = OrbitalStrikeEffect.description;
	public static readonly appliedEffects = [OrbitalStrikeEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: true,
		water: false,
		deploy: false,
		radius: 3,
	};
	public cooldown = 4;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default OrbitalStrikeAbility;

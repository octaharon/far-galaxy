import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import IrradiateEffect from '../../../statuses/Effects/all/Orbital/Irradiate.45008';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class IrradiateAbility extends GenericAbility<TargetType> {
	public static readonly id = 45008;
	public static readonly caption = 'Irradiate';
	public static readonly description = IrradiateEffect.description;
	public static readonly appliedEffects = [IrradiateEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 2.5,
	};
	public cooldown = 6;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default IrradiateAbility;

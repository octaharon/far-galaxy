import { DEPLOY_ZONE_RADIUS } from '../../../../orbital/base/constants';
import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import DeployBeaconEffect from '../../../statuses/Effects/all/Orbital/DeployBeacon.45004';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class DeployBeaconAbility extends GenericAbility<TargetType> {
	public static readonly id = 45004;
	public static readonly caption = 'Deploy Beacon';
	public static readonly description = 'Create additional area where units can be deployed to';
	public static readonly appliedEffects = [DeployBeaconEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: true,
		water: false,
		deploy: true,
		radius: DEPLOY_ZONE_RADIUS,
	};
	public cooldown = 6;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default DeployBeaconAbility;

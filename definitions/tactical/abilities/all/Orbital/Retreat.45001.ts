import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import EvacuationEffect from '../../../statuses/Effects/all/Orbital/Evacuation.45001';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class RetreatAbility extends GenericAbility<TargetType> {
	public static readonly id = 45001;
	public static readonly caption = 'Retreat';
	public static readonly description = 'Put all Allied units in radius 3 into your Bay, as long as space is provided';
	public static readonly appliedEffects = [EvacuationEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 3,
	};
	public cooldown = 6;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default RetreatAbility;

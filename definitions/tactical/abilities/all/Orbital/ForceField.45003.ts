import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import ForceFieldEffect from '../../../statuses/Effects/all/Orbital/ForceFieldEffect.45003';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class ForceFieldAbility extends GenericAbility<TargetType> {
	public static readonly id = 45003;
	public static readonly caption = 'Force Field';
	public static readonly description =
		"Create an Force Field on map, which can't be traversed or shot through. All units captured inside the Force Field are disabled for duration";
	public static readonly appliedEffects = [ForceFieldEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: false,
		passable: false,
		scan: false,
		water: false,
		deploy: false,
		radius: 2.5,
	};
	public cooldown = 4;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default ForceFieldAbility;

import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import MinefieldEffect from '../../../statuses/Effects/all/Orbital/MineField.45007';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class MinefieldAbility extends GenericAbility<TargetType> {
	public static readonly id = 45007;
	public static readonly caption = 'Mine Field';
	public static readonly description =
		'Fill an area with proximity mines, which all detonate simultaneously whenever a unit moves inside the area';
	public static readonly appliedEffects = [MinefieldEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 2,
	};
	public cooldown = 3;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default MinefieldAbility;

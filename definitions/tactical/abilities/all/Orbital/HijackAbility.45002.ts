import { MAX_MAP_RADIUS } from '../../../map/constants';
import AbductionEffect from '../../../statuses/Effects/all/Orbital/Abduction.45002';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class HijackAbility extends GenericAbility<TargetType> {
	public static readonly id = 45002;
	public static readonly caption = 'Hijack';
	public static readonly description = AbductionEffect.description;
	public static readonly appliedEffects = [AbductionEffect.id];
	public cooldown = 10;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default HijackAbility;

import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import PlanetCrackerEffect from '../../../statuses/Effects/all/Orbital/PlanetCrackerEffect.45005';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class PlanetCrackerAbility extends GenericAbility<TargetType> {
	public static readonly id = 45005;
	public static readonly caption = 'Planet cracker';
	public static readonly description =
		"Destroy ground and naval units in 4 Radius, flatten Hills, if any, otherwise create a Rift on surface, unless it's in Pond";
	public static readonly appliedEffects = [PlanetCrackerEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: false,
		scan: false,
		water: false,
		deploy: false,
		radius: 4,
	};
	public cooldown = 10;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default PlanetCrackerAbility;

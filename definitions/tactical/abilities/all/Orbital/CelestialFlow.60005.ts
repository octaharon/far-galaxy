import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import CelestialFlowEffect from '../../../statuses/Effects/all/Player/CelestialFlow.60005';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class CelestialFlowAbility extends GenericAbility<TargetType> {
	public static readonly id = 60005;
	public static readonly caption = 'Celestial Flow';
	public static readonly description = CelestialFlowEffect.description;
	public static readonly appliedEffects = [CelestialFlowEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 4,
	};
	public cooldown = 7;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default CelestialFlowAbility;

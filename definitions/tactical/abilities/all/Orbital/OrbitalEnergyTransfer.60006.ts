import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import OrbitalEnergyTransferEffect from '../../../statuses/Effects/all/Player/OrbitalEnergyTransfer.60006';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class OrbitalEnergyTransferAbility extends GenericAbility<TargetType> {
	public static readonly id = 60006;
	public static readonly caption = 'Orbital energy transfer';
	public static readonly description = OrbitalEnergyTransferEffect.description;
	public static readonly appliedEffects = [OrbitalEnergyTransferEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 3,
	};
	public cooldown = 7;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default OrbitalEnergyTransferAbility;

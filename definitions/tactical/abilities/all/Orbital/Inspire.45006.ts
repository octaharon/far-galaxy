import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import InspireEffect, { INSPIRE_RADIUS } from '../../../statuses/Effects/all/Orbital/Inspire.45006';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class InspireAbility extends GenericAbility<TargetType> {
	public static readonly id = 45006;
	public static readonly caption = 'Inspire';
	public static readonly description = InspireEffect.description;
	public static readonly appliedEffects = [InspireEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: true,
		water: false,
		deploy: false,
		radius: INSPIRE_RADIUS,
	};
	public cooldown = 7;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default InspireAbility;

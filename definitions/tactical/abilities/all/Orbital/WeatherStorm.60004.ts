import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import WeatherStormEffect from '../../../statuses/Effects/all/Player/WeatherStorm.60004';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class WeatherStormAbility extends GenericAbility<TargetType> {
	public static readonly id = 60004;
	public static readonly caption = 'Weather Storm';
	public static readonly description = WeatherStormEffect.description;
	public static readonly appliedEffects = [WeatherStormEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 4,
	};
	public cooldown = 10;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default WeatherStormAbility;

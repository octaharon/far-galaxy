import { MAX_MAP_RADIUS } from '../../../map/constants';
import ShieldFunnelEFfect from '../../../statuses/Effects/all/Player/ShieldFunnel.60007';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class ShieldFunnelAbility extends GenericAbility<TargetType> {
	public static readonly id = 60007;
	public static readonly caption = 'Shield funnel';
	public static readonly description = ShieldFunnelEFfect.description;
	public static readonly appliedEffects = [ShieldFunnelEFfect.id];
	public cooldown = 5;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default ShieldFunnelAbility;

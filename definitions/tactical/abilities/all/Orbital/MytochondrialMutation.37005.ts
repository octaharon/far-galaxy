import MytochondrialMutationEffect from '../../../statuses/Effects/all/Orbital/MytochondrialMutation.37005';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class MytochondrialMutationAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 37005;
	public static readonly caption = 'Mytochondrial Mutation';
	public static readonly description = MytochondrialMutationEffect.description;
	public cooldown = 5;
	public baseRange = Infinity;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: MytochondrialMutationEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default MytochondrialMutationAbility;

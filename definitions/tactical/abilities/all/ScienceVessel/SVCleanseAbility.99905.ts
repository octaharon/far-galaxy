import SVCleanseEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselCleanse.88805';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class SVCleanseAbility extends GenericAbility<TargetType> {
	public static readonly id = 99905;
	public static readonly caption = 'Cleanse';
	public static readonly description = SVCleanseEffect.description;
	public static readonly appliedEffects = [SVCleanseEffect.id];
	public cooldown = 4;
	public baseRange = 10;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SVCleanseAbility;

import SVTransmissionInterceptEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselTransmissionIntercept.88802';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.player_self;

export class SVTransmissionInterceptAbility extends GenericAbility<TargetType> {
	public static readonly id = 99902;
	public static readonly caption = 'Transmission intercept';
	public static readonly description = 'Player gains +3 Counter-Reconaissance level for 3 Turns';
	public static readonly appliedEffects = [SVTransmissionInterceptEffect.id];
	public cooldown = 5;
	public baseRange = 0;
	public readonly targetType = Abilities.abilityTargetTypes.player_self;
}

export default SVTransmissionInterceptAbility;

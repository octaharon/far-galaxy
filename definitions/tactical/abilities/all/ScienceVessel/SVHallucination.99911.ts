import SVHallucinationEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselHallucination.88811';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.around;

export class SVHallucinationAbility extends GenericAbility<TargetType> {
	public static readonly id = 99911;
	public static readonly caption = 'Hallucination';
	public static readonly description = SVHallucinationEffect.description;
	public static readonly appliedEffects = [SVHallucinationEffect.id];
	public cooldown = 4;
	public baseRange = 2.5;
	public readonly targetType = Abilities.abilityTargetTypes.around;
}

export default SVHallucinationAbility;

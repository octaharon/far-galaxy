import SVSurveillanceEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselSurveillance.88801';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.player_self;

export class SVSurveillanceAbility extends GenericAbility<TargetType> {
	public static readonly id = 99901;
	public static readonly caption = 'Surveillance assistance';
	public static readonly description = 'Player gains +3 Reconaissance level for 3 Turns';
	public static readonly appliedEffects = [SVSurveillanceEffect.id];
	public cooldown = 4;
	public baseRange = 0;
	public readonly targetType = Abilities.abilityTargetTypes.player_self;
}

export default SVSurveillanceAbility;

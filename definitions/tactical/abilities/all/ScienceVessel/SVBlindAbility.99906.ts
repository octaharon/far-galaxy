import SVBlindEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselBlind.88806';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class SVBlindAbility extends GenericAbility<TargetType> {
	public static readonly id = 99906;
	public static readonly caption = 'Blind';
	public static readonly description = SVBlindEffect.description;
	public static readonly appliedEffects = [SVBlindEffect.id];
	public cooldown = 2;
	public baseRange = 15;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SVBlindAbility;

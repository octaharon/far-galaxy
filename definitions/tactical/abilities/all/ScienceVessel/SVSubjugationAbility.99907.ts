import SVSubjugateEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselSubjugation.88807';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class SVSubjugateAbility extends GenericAbility<TargetType> {
	public static readonly id = 99907;
	public static readonly caption = 'Subjugate';
	public static readonly description =
		"Disable a unit for 2 Turns. Disabled units do not take any Actions and can't move";
	public static readonly appliedEffects = [SVSubjugateEffect.id];
	public cooldown = 4;
	public baseRange = 13;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SVSubjugateAbility;

import SVNanoSwarmEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselNanoSwarm.88803';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class SVNanoSwarmAbility extends GenericAbility<TargetType> {
	public static readonly id = 99903;
	public static readonly caption = 'Nano swarm';
	public static readonly description = SVNanoSwarmEffect.description;
	public static readonly appliedEffects = [SVNanoSwarmEffect.id];
	public cooldown = 2;
	public baseRange = 10;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SVNanoSwarmAbility;

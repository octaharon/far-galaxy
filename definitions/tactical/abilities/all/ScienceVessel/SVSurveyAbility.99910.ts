import { TMapCellProps } from '../../../map';
import SVSurveyEffect, {
	SurveyBaseRadius,
} from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselSurvey.88810';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class SVSurveyAbility extends GenericAbility<TargetType> {
	public static readonly id = 99910;
	public static readonly caption = 'Survey';
	public static readonly description = SVSurveyEffect.description;
	public static readonly appliedEffects = [SVSurveyEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: true,
		water: false,
		deploy: false,
		radius: SurveyBaseRadius,
	};
	public cooldown = 3;
	public baseRange = 15;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default SVSurveyAbility;

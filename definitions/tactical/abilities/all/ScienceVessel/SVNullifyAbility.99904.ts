import SVNullifyEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselNullify.86004';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class SVNullifyAbility extends GenericAbility<TargetType> {
	public static readonly id = 99904;
	public static readonly caption = 'Nullify';
	public static readonly description = 'Remove all Status Effects from target';
	public static readonly appliedEffects = [SVNullifyEffect.id];
	public cooldown = 3;
	public baseRange = 12;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SVNullifyAbility;

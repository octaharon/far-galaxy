import SVTimeTwistEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselTimeTwist.88809';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class SVTimeTwistAbility extends GenericAbility<TargetType> {
	public static readonly id = 99909;
	public static readonly caption = 'Time Twist';
	public static readonly description =
		"Restore this unit's Hit Points and recharge all abilities's cooldowns, but revert all that after 2 Turns";
	public static readonly appliedEffects = [SVTimeTwistEffect.id];
	public cooldown = 6;
	public baseRange = 10;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SVTimeTwistAbility;

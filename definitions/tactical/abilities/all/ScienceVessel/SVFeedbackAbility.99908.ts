import SVFeedbackEffect from '../../../statuses/Effects/all/ScienceVessel/ScienceVesselFeedback.88808';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class SVFeedbackAbility extends GenericAbility<TargetType> {
	public static readonly id = 99908;
	public static readonly caption = 'Feedback';
	public static readonly description = SVFeedbackEffect.description;
	public static readonly appliedEffects = [SVFeedbackEffect.id];
	public cooldown = 2;
	public baseRange = 13;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SVFeedbackAbility;

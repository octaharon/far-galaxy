import DestroyUnitEffect from '../../../statuses/Effects/all/SelfDestruct/DestroyUnit.12510';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.self;

export class DoomsdayDeviceAbility extends GenericAbility<TargetType> {
	public static readonly id = 25001;
	public static readonly caption = 'Self Destruct';
	public static readonly description = 'Trigger Doomsday device, exploding this unit and dealing damage around';
	public cooldown = 0;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DestroyUnitEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default DoomsdayDeviceAbility;

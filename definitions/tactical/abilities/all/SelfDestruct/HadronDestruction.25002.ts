import DestroyUnitEffect from '../../../statuses/Effects/all/SelfDestruct/DestroyUnit.12510';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.self;

export class HadronDestructionAbility extends GenericAbility<TargetType> {
	public static readonly id = 25002;
	public static readonly caption = 'Self Destruct';
	public static readonly description =
		'Overload Hadron Inverter, destroying this Unit without any Salvage and producing a devastating explosion of 450 ANH Damage in 2.5 Area of Effect';
	public cooldown = 0;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DestroyUnitEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default HadronDestructionAbility;

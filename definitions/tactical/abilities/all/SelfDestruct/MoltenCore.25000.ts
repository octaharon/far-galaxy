import {
	MOLTEN_CORE_DAMAGE_MAX,
	MOLTEN_CORE_DAMAGE_MIN,
	MOLTEN_CORE_DAMAGE_RADIUS,
} from '../../../statuses/Effects/all/SelfDestruct/MoltenCore.12502';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.self;

export class MoltenCoreAbility extends GenericAbility<TargetType> {
	public static readonly id = 25000;
	public static readonly caption = 'Molten Core';
	public static readonly description = `Melt the core of Olympius Reactor, dealing ${MOLTEN_CORE_DAMAGE_MIN} to ${MOLTEN_CORE_DAMAGE_MAX} EMG, HEA and RAD Damage each in Radius ${MOLTEN_CORE_DAMAGE_RADIUS}. If the unit survives the explosion, it loses all Shields and is unable to Move`;
	public cooldown = 0;
	public baseRange = 0;
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default MoltenCoreAbility;

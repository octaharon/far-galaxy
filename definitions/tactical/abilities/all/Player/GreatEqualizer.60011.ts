import { MAX_MAP_RADIUS } from '../../../map/constants';
import GreatEqualizerEffect from '../../../statuses/Effects/all/Player/GreatEqualizer.60011';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class EqualizerAbility extends GenericAbility<TargetType> {
	public static readonly id = 60011;
	public static readonly caption = 'Great Equalizer';
	public static readonly description = GreatEqualizerEffect.description;
	public static readonly appliedEffects = [GreatEqualizerEffect.id];
	public cooldown = 7;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default EqualizerAbility;

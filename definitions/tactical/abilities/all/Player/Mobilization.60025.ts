import { MAX_MAP_RADIUS } from '../../../map/constants';
import MobilizationEffect from '../../../statuses/Effects/all/Player/Mobilization.60025';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.player_self;

export class MobilizationAbility extends GenericAbility<TargetType> {
	public static readonly id = 60025;
	public static readonly caption = 'Mobilization';
	public static readonly description = MobilizationEffect.description;
	public static readonly appliedEffects = [MobilizationEffect.id];
	public cooldown = 10;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.player_self;
}

export default MobilizationAbility;

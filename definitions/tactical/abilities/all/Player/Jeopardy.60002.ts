import { MAX_MAP_RADIUS } from '../../../map/constants';
import JeopardyEffect from '../../../statuses/Effects/all/Player/JeopardyEffect.60002';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.player;

export class JeopardyAbility extends GenericAbility<TargetType> {
	public static readonly id = 60002;
	public static readonly caption = 'Jeopardy';
	public static readonly description = "Disable Player's Factories for 2 Turns";
	public static readonly appliedEffects = [JeopardyEffect.id];
	public cooldown = 6;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.player;
}

export default JeopardyAbility;

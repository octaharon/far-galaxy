import { MAX_MAP_RADIUS } from '../../../map/constants';
import VentilationEffect from '../../../statuses/Effects/all/Player/Ventilation.60020';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.player;

export class VentilationAbility extends GenericAbility<TargetType> {
	public static readonly id = 60020;
	public static readonly caption = 'Ventilation';
	public static readonly description = VentilationEffect.description;
	public static readonly appliedEffects = [VentilationEffect.id];
	public cooldown = 7;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.player;
}

export default VentilationAbility;

import SegmentationFaultEffect from '../../../statuses/Effects/all/Player/SegmentationFault.60000';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.player_self;

export class SegmentationFaultAbility extends GenericAbility<TargetType> {
	public static readonly id = 60000;
	public static readonly caption = 'Segmentation fault';
	public static readonly description = 'Gain an extra Deployment next turn';
	public static readonly appliedEffects = [SegmentationFaultEffect.id];
	public cooldown = 3;
	public baseRange = 0;
	public readonly targetType = Abilities.abilityTargetTypes.player_self;
}

export default SegmentationFaultAbility;

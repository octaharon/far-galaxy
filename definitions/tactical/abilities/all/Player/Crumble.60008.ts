import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import CrumbleEffect from '../../../statuses/Effects/all/Player/Crumble.60008';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class CrumbleAbility extends GenericAbility<TargetType> {
	public static readonly id = 60008;
	public static readonly caption = 'Crumble';
	public static readonly description =
		"Destroy all Doodads and Airborne units in area. Turn down all cushions for Hover units, making them move on Ground. If a unit can't land properly, it's destroyed";
	public static readonly appliedEffects = [CrumbleEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 3,
	};
	public cooldown = 8;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default CrumbleAbility;

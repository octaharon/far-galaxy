import { MAX_MAP_RADIUS } from '../../../map/constants';
import AlterationEffect from '../../../statuses/Effects/all/Player/Alteration.60030';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.player_self;

export class AlterationAbility extends GenericAbility<TargetType> {
	public static readonly id = 60030;
	public static readonly caption = 'Alteration';
	public static readonly description = AlterationEffect.description;
	public static readonly appliedEffects = [AlterationEffect.id];
	public cooldown = 10;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.player_self;
}

export default AlterationAbility;

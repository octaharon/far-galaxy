import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import ApocalypseEffect, { ApocalypseEffectRadius } from '../../../statuses/Effects/all/Player/Apocalypse.60010';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class ApocalypseAbility extends GenericAbility<TargetType> {
	public static readonly id = 60010;
	public static readonly caption = 'Apocalypse';
	public static readonly description = ApocalypseEffect.description;
	public static readonly appliedEffects = [ApocalypseEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: ApocalypseEffectRadius,
	};
	public cooldown = 12;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default ApocalypseAbility;

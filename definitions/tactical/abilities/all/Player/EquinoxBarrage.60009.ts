import { MAX_MAP_RADIUS } from '../../../map/constants';
import EquinoxBarrageEffect from '../../../statuses/Effects/all/Player/EquinoxBarrage.60009';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.player;

export class EquinoxBarrage extends GenericAbility<TargetType> {
	public static readonly id = 60009;
	public static readonly caption = 'Equinox Barrage';
	public static readonly description = EquinoxBarrageEffect.description;
	public static readonly appliedEffects = [EquinoxBarrageEffect.id];
	public cooldown = 9;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.player;
}

export default EquinoxBarrage;

import SymbioticTissueEffect from '../../../statuses/Effects/all/Bionic/SymbioticTissue.12008';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class SymbioticTissueAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = SymbioticTissueEffect.id;
	public static readonly caption = SymbioticTissueEffect.caption;
	public static readonly description = SymbioticTissueEffect.description;
	public cooldown = 5;
	public baseRange = 7;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: SymbioticTissueEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SymbioticTissueAbility;

import ElectrocuteEffect        from '../../../statuses/Effects/all/Unique/Electrocute.37000';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility           from '../../GenericAbility';
import Abilities                from '../../types';

export class ElectrocutionAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 37000;
	public static readonly caption = 'Electrocute';
	public static readonly description = ElectrocuteEffect.description;
	public cooldown = 5;
	public baseRange = 9;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: ElectrocuteEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default ElectrocutionAbility;

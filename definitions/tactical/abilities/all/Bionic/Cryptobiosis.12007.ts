import CryptobiosisEffect from '../../../statuses/Effects/all/Bionic/Cryptobiosis.12007';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class CryptobiosisAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 12007;
	public static readonly caption = CryptobiosisEffect.caption;
	public static readonly description = CryptobiosisEffect.description;
	public cooldown = 5;
	public baseRange = 6;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: CryptobiosisEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default CryptobiosisAbility;

import MycotoxinEffect from '../../../statuses/Effects/all/Bionic/Mycotoxin.12002';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class MycotoxinAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 12002;
	public static readonly caption = MycotoxinEffect.caption;
	public static readonly description = MycotoxinEffect.description;
	public cooldown = 5;
	public baseRange = 13;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: MycotoxinEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default MycotoxinAbility;

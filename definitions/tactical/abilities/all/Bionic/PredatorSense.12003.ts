import PredatorSenseEffect from '../../../statuses/Effects/all/Bionic/PredatorSense.12003';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class PredatorSenseAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 12003;
	public static readonly caption = PredatorSenseEffect.caption;
	public static readonly description = PredatorSenseEffect.description;
	public cooldown = 2;
	public baseRange = 9;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: PredatorSenseEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default PredatorSenseAbility;

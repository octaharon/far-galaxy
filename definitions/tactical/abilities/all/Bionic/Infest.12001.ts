import InfestationEffect from '../../../statuses/Effects/all/Bionic/Infestation.12001';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class InfestAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 12001;
	public static readonly caption = 'Infest';
	public static readonly description = InfestationEffect.description;
	public cooldown = 6;
	public baseRange = 14;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: InfestationEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default InfestAbility;

import WildGrowthEffect from '../../../statuses/Effects/all/Bionic/WildGrowth.12006';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class WildGrowthAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 12006;
	public static readonly caption = 'Wild Growth';
	public static readonly description = WildGrowthEffect.description;
	public cooldown = 5;
	public baseRange = 12;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: WildGrowthEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default WildGrowthAbility;

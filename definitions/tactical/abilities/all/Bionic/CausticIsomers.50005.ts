import CausticIsomerEffect from '../../../statuses/Effects/all/ElectronicWarfare/CausticIsomers.03005';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class CausticIsomersAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50005;
	public static readonly caption = 'Caustic Isomers';
	public static readonly description = CausticIsomerEffect.description;
	public cooldown = 4;
	public baseRange = 7;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: CausticIsomerEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default CausticIsomersAbility;

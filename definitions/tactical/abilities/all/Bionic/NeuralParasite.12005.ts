import NeuralParasiteEffect from '../../../statuses/Effects/all/Bionic/NeuralParasite.12005';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class NeuralParasiteAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 12005;
	public static readonly caption = 'Neural Parasite';
	public static readonly description = NeuralParasiteEffect.description;
	public cooldown = 5;
	public baseRange = 10;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: NeuralParasiteEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default NeuralParasiteAbility;

import EnzymeInhibitorsEffect from '../../../statuses/Effects/all/Bionic/EnzymeInhibitors.12004';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class EnzymeInhibitorsAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 12004;
	public static readonly caption = EnzymeInhibitorsEffect.caption;
	public static readonly description = EnzymeInhibitorsEffect.description;
	public cooldown = 4;
	public baseRange = 9;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: EnzymeInhibitorsEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default EnzymeInhibitorsAbility;

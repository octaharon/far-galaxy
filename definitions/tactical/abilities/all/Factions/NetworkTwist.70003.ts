import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import NetworkTwistEffect from '../../../statuses/Effects/all/Faction/NetworkTwist.70003';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class NetworkTwistAbility extends GenericAbility<TargetType> {
	public static readonly id = 70003;
	public static readonly caption = 'Network Twist';
	public static readonly description = NetworkTwistEffect.description;
	public static readonly appliedEffects = [NetworkTwistEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 4,
	};
	public cooldown = 7;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default NetworkTwistAbility;

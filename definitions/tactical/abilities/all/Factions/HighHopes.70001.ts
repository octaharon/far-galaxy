import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import HighHopesMapEffect from '../../../statuses/Effects/all/Faction/HighHopesZoneEffect.70101';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class HighHopesAbility extends GenericAbility<TargetType> {
	public static readonly id = 70001;
	public static readonly caption = 'High Hopes';
	public static readonly description = HighHopesMapEffect.description;
	public static readonly appliedEffects = [HighHopesMapEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 5,
	};
	public cooldown = 5;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default HighHopesAbility;

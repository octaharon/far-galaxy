import { MAX_MAP_RADIUS } from '../../../map/constants';
import ProvisionalDefenseEffect from '../../../statuses/Effects/all/Faction/ProvisionalDefense.70009';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class ProvisionalDefenseAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 70009;
	public static readonly caption = 'Provisional Defense';
	public static readonly description = ProvisionalDefenseEffect.description;
	public static readonly appliedEffects = [ProvisionalDefenseEffect.id];
	public cooldown = 4;
	public baseRange = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default ProvisionalDefenseAbility;

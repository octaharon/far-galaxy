import LaborFeatEffect from '../../../statuses/Effects/all/Faction/LaborFeat.70014';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class LaborFeatAbility extends GenericAbility<Abilities.abilityTargetTypes.player_self> {
	public static readonly id = 70014;
	public static readonly caption = 'Labor Feat';
	public static readonly description = LaborFeatEffect.description;
	public static readonly appliedEffects = [LaborFeatEffect.id];
	public cooldown = 6;
	public baseRange = 0;
	public readonly targetType = Abilities.abilityTargetTypes.player_self;
}

export default LaborFeatAbility;

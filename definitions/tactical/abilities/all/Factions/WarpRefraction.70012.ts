import { MAX_MAP_RADIUS } from '../../../map/constants';
import WarpRefractionEffect from '../../../statuses/Effects/all/Faction/WarpRefraction.70012';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class WarpRefractionAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 70012;
	public static readonly caption = 'Warp Refraction';
	public static readonly description = WarpRefractionEffect.description;
	public static readonly appliedEffects = [WarpRefractionEffect.id];
	public cooldown = 4;
	public baseRange = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default WarpRefractionAbility;

import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import SupplyDropMapEffect, {
	SUPPLY_DROP_RADIUS,
} from '../../../statuses/Effects/all/Faction/SupplyDropZoneEffect.70105';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class SupplyDropAbility extends GenericAbility<TargetType> {
	public static readonly id = 70005;
	public static readonly caption = 'Supply Drop';
	public static readonly description = SupplyDropMapEffect.description;
	public static readonly appliedEffects = [SupplyDropMapEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: SUPPLY_DROP_RADIUS,
	};
	public cooldown = 5;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default SupplyDropAbility;

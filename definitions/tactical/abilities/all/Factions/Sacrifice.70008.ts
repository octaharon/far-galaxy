import { MAX_MAP_RADIUS } from '../../../map/constants';
import SacrificeEffect from '../../../statuses/Effects/all/Faction/Sacrifice.70008';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class SacrificeAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 70008;
	public static readonly caption = 'Sacrifice';
	public static readonly description = SacrificeEffect.description;
	public static readonly appliedEffects = [SacrificeEffect.id];
	public cooldown = 6;
	public baseRange = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SacrificeAbility;

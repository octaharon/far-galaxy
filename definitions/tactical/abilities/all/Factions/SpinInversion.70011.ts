import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import SpinInversionEffect from '../../../statuses/Effects/all/Faction/SpinInversion.70011';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export const SPIN_INVERSION_RADIUS = 3;

export class SpinInversionAbility extends GenericAbility<Abilities.abilityTargetTypes.map> {
	public static readonly id = 70011;
	public static readonly caption = 'Spin Inversion';
	public static readonly description = `All Units in Radius ${SPIN_INVERSION_RADIUS} permanently lose Warp Defense, Tachyon Defense and Unstoppable Perks along with ANH and WRP Armor Absorption`;
	public static readonly appliedEffects = [SpinInversionEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: SPIN_INVERSION_RADIUS,
	};
	public cooldown = 7;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default SpinInversionAbility;

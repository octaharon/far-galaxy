import { MAX_MAP_RADIUS } from '../../../map/constants';
import ClusterWipeEffect from '../../../statuses/Effects/all/Faction/ClusterWipe.70004';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class ClusterWipeAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 70004;
	public static readonly caption = 'Cluster Wipe';
	public static readonly description = ClusterWipeEffect.description;
	public static readonly appliedEffects = [ClusterWipeEffect.id];
	public cooldown = 4;
	public baseRange = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default ClusterWipeAbility;

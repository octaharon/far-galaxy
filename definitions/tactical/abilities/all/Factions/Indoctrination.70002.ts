import { MAX_MAP_RADIUS } from '../../../map/constants';
import IndoctrinationEffect from '../../../statuses/Effects/all/Faction/Indoctrination.70002';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class IndoctrinationAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 70002;
	public static readonly caption = 'Indoctrination';
	public static readonly description = IndoctrinationEffect.description;
	public static readonly appliedEffects = [IndoctrinationEffect.id];
	public cooldown = 5;
	public baseRange = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default IndoctrinationAbility;

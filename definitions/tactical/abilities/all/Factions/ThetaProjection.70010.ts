import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import ThetaProjectionEffect, {
	THETA_PROJECTION_RADIUS,
} from '../../../statuses/Effects/all/Faction/ThetaProjection.70010';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class ThetaProjectionAbility extends GenericAbility<Abilities.abilityTargetTypes.map> {
	public static readonly id = 70010;
	public static readonly caption = 'Theta Projection';
	public static readonly description = ThetaProjectionEffect.description;
	public static readonly appliedEffects = [ThetaProjectionEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: THETA_PROJECTION_RADIUS,
	};
	public cooldown = 7;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default ThetaProjectionAbility;

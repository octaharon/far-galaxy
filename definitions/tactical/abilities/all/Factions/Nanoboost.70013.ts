import NanoboostEffect from '../../../statuses/Effects/all/Faction/Nanoboost.70013';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class NanoboostAbility extends GenericAbility<Abilities.abilityTargetTypes.player_self> {
	public static readonly id = 70013;
	public static readonly caption = 'Nanoboost';
	public static readonly description = NanoboostEffect.description;
	public static readonly appliedEffects = [NanoboostEffect.id];
	public cooldown = 7;
	public baseRange = 0;
	public readonly targetType = Abilities.abilityTargetTypes.player_self;
}

export default NanoboostAbility;

import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import SliceAndDiceEffect, { SLICE_AND_DICE_RADIUS } from '../../../statuses/Effects/all/Faction/SliceAndDice.70007';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class SliceAndDiceAbility extends GenericAbility<Abilities.abilityTargetTypes.map> {
	public static readonly id = 70007;
	public static readonly caption = 'Slice And Dice';
	public static readonly description = SliceAndDiceEffect.description;
	public static readonly appliedEffects = [SliceAndDiceEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: SLICE_AND_DICE_RADIUS,
	};
	public cooldown = 5;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default SliceAndDiceAbility;

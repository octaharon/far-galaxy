import { MAX_MAP_RADIUS } from '../../../map/constants';
import QuarkLiftEffect from '../../../statuses/Effects/all/Faction/QuarkLift.70006';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class QuarkLiftAbility extends GenericAbility<Abilities.abilityTargetTypes.player> {
	public static readonly id = 70006;
	public static readonly caption = 'Quark Lift';
	public static readonly description = QuarkLiftEffect.description;
	public static readonly appliedEffects = [QuarkLiftEffect.id];
	public cooldown = 6;
	public baseRange: number = MAX_MAP_RADIUS;
	public readonly targetType = Abilities.abilityTargetTypes.player;
}

export default QuarkLiftAbility;

import MendingStreamEffect from '../../../statuses/Effects/all/ElectronicWarfare/MendingStream.03003';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class MendingStreamAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50003;
	public static readonly caption = 'Mending Stream';
	public static readonly description = MendingStreamEffect.description;
	public cooldown = 2;
	public baseRange = 9;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: MendingStreamEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default MendingStreamAbility;

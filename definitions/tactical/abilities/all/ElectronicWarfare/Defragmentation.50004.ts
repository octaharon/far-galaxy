import DefragmentationEffect from '../../../statuses/Effects/all/ElectronicWarfare/Defragmentation.03004';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class DefragmentationAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50004;
	public static readonly caption = 'Defragmentation';
	public static readonly description = DefragmentationEffect.description;
	public cooldown = 3;
	public baseRange = 12;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DefragmentationEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default DefragmentationAbility;

import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import DirectDamageStatusEffect from '../../../statuses/Effects/all/AppliedDamage/DirectDamage.01003';
import { TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const SNIPE_SHOT_DMG_MIN = 35;
const SNIPE_SHOT_DMG_MAX = 50;
const SNIPE_SHOT_RADIUS = 12;

export class SnipeShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50012;
	public static readonly caption = 'Snipe Shot';
	public static readonly description = `Deal ${AbilityPowerUIToken}${SNIPE_SHOT_DMG_MIN} to ${AbilityPowerUIToken}${SNIPE_SHOT_DMG_MAX} KIN Damage as Projectile Attack to a target Unit`;
	public cooldown = 0;
	public baseRange = SNIPE_SHOT_RADIUS;
	public readonly appliedEffects: TEffectApplianceList<TStatusEffectStateLookup<typeof DirectDamageStatusEffect>> = [
		{
			effectId: DirectDamageStatusEffect.id,
			props: {
				attackUnit: AttackManager.getAttackDamage({
					delivery: UnitAttack.deliveryType.ballistic,
					[Damage.damageType.kinetic]: {
						min: SNIPE_SHOT_DMG_MIN,
						max: SNIPE_SHOT_DMG_MAX,
						distribution: 'Uniform',
					},
				}),
			},
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SnipeShotAbility;

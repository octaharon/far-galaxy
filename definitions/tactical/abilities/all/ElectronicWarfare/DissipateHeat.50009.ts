import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import DamageOverTimeStatusEffect from '../../../statuses/Effects/all/AppliedDamage/DamageOverTime.01002';
import { TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const DISSIPATE_HEAT_DMG_MIN = 25;
const DISSPIATE_HEAT_DMG_MAX = 35;
const DISSIPATE_HEAT_RANGE = 9;
const DISSIPATE_HEAT_DURATION = 10;

export class DissipateHeatAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50009;
	public static readonly caption = 'Dissipate Heat';
	public static readonly description = `Place a DoT on a unit, that deals ${AbilityPowerUIToken}${DISSIPATE_HEAT_DMG_MIN} to ${AbilityPowerUIToken}${DISSPIATE_HEAT_DMG_MAX} HEA Damage every Turn End for the effect Duration`;
	public cooldown = 1;
	public baseRange = DISSIPATE_HEAT_RANGE;
	public readonly appliedEffects: TEffectApplianceList<TStatusEffectStateLookup<typeof DamageOverTimeStatusEffect>> =
		[
			{
				effectId: DamageOverTimeStatusEffect.id,
				props: AttackManager.getDoTEffectFromAttack({
					delivery: UnitAttack.deliveryType.immediate,
					duration: DISSIPATE_HEAT_DURATION,
					[UnitAttack.attackFlags.DoT]: true,
					[Damage.damageType.heat]: {
						min: DISSIPATE_HEAT_DMG_MIN,
						max: DISSPIATE_HEAT_DMG_MAX,
						distribution: 'Uniform',
					},
				}),
			},
		];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default DissipateHeatAbility;

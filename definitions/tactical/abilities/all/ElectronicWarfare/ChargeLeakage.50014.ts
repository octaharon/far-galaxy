import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import { DOTEffectProps } from '../../../statuses/Effects/all/AppliedDamage/DamageOverTime.01002';
import DOTPerMissingHPEffect from '../../../statuses/Effects/all/AppliedDamage/DamagePerMissingHP.01010';
import { TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const CHARGE_LEAK_DMG_MIN = 15;
const CHARGE_LEAK_DMG_MAX = 30;
const CHARGE_DMG_RADIUS = 6;
const CHARGE_DMG_DURATION = 10;

export class ChargeLeakageAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50014;
	public static readonly caption = 'Charge Leak';
	public static readonly description = `Apply a DOT effect to a Unit, that deals ${AbilityPowerUIToken}${CHARGE_LEAK_DMG_MIN} to ${AbilityPowerUIToken}${CHARGE_LEAK_DMG_MAX} EMG Damage, increased by 1% for every 1% of target's missing Hit Points`;
	public cooldown = 1;
	public baseRange = CHARGE_DMG_RADIUS;
	public readonly appliedEffects: TEffectApplianceList<TStatusEffectStateLookup<typeof DOTPerMissingHPEffect>> = [
		{
			effectId: DOTPerMissingHPEffect.id,
			props: AttackManager.getDoTEffectFromAttack({
				delivery: UnitAttack.deliveryType.immediate,
				duration: CHARGE_DMG_DURATION,
				[UnitAttack.attackFlags.DoT]: true,
				[Damage.damageType.magnetic]: {
					min: CHARGE_LEAK_DMG_MIN,
					max: CHARGE_LEAK_DMG_MAX,
					distribution: 'Gaussian',
				},
			}) as DOTEffectProps,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default ChargeLeakageAbility;

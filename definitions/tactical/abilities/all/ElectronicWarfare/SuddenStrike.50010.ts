import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import DirectDamageStatusEffect from '../../../statuses/Effects/all/AppliedDamage/DirectDamage.01003';
import { TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const SuddenStrikeDamageMin = 50;
const SuddenStrikeDamageMax = 70;
const SuddenStrikeRadius = 7;

export class SuddenStrikeAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50010;
	public static readonly caption = 'Sudden Strike';
	public static readonly description = `Deal ${AbilityPowerUIToken}${SuddenStrikeDamageMin} to ${AbilityPowerUIToken}${SuddenStrikeDamageMax} TRM Damage to a target Unit with a Bombardment Precision Attack`;
	public cooldown = 2;
	public baseRange = SuddenStrikeRadius;
	public readonly appliedEffects: TEffectApplianceList<TStatusEffectStateLookup<typeof DirectDamageStatusEffect>> = [
		{
			effectId: DirectDamageStatusEffect.id,
			props: {
				attackUnit: AttackManager.getAttackDamage({
					delivery: UnitAttack.deliveryType.bombardment,
					[UnitAttack.attackFlags.Disruptive]: true,
					[Damage.damageType.thermodynamic]: {
						min: SuddenStrikeDamageMin,
						max: SuddenStrikeDamageMax,
						distribution: 'Bell',
					},
				}),
			},
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SuddenStrikeAbility;

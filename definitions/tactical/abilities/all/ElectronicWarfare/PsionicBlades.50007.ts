import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import DirectDamageStatusEffect from '../../../statuses/Effects/all/AppliedDamage/DirectDamage.01003';
import { TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const PSIONIC_BLADES_DMG_MIN = 15;
const PSIONIC_BLADES_DMG_MAX = 25;
const PSIONIC_BLADES_RANGE = 3.5;

export class PsionicBladesAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50007;
	public static readonly caption = 'Psionic Blades';
	public static readonly description = `Strike a Unit with two Precision Attacks. One is Supersonic with TRM Damage, another one - Beam with RAD Damage, and each does ${AbilityPowerUIToken}${PSIONIC_BLADES_DMG_MIN} to ${AbilityPowerUIToken}${PSIONIC_BLADES_DMG_MAX} Damage. Line of Sight rule is not required for this Ability`;
	public cooldown = 0;
	public baseRange = PSIONIC_BLADES_RANGE;
	public readonly appliedEffects: TEffectApplianceList<TStatusEffectStateLookup<typeof DirectDamageStatusEffect>> = [
		{
			effectId: DirectDamageStatusEffect.id,
			props: {
				attackUnit: AttackManager.getAttackDamage({
					delivery: UnitAttack.deliveryType.supersonic,
					[UnitAttack.attackFlags.Disruptive]: true,
					[Damage.damageType.thermodynamic]: {
						min: PSIONIC_BLADES_DMG_MIN,
						max: PSIONIC_BLADES_DMG_MAX,
						distribution: 'Bell',
					},
				}),
			},
		},
		{
			effectId: DirectDamageStatusEffect.id,
			props: {
				attackUnit: AttackManager.getAttackDamage({
					delivery: UnitAttack.deliveryType.beam,
					[UnitAttack.attackFlags.Disruptive]: true,
					[Damage.damageType.radiation]: {
						min: PSIONIC_BLADES_DMG_MIN,
						max: PSIONIC_BLADES_DMG_MAX,
						distribution: 'Bell',
					},
				}),
			},
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default PsionicBladesAbility;

import NaniteRayEffect from '../../../statuses/Effects/all/ElectronicWarfare/NaniteRay.50018';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class NaniteRayAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = NaniteRayEffect.id;
	public static readonly caption = NaniteRayEffect.caption;
	public static readonly description = NaniteRayEffect.description;
	public cooldown = 3;
	public baseRange = 8;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: NaniteRayEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default NaniteRayAbility;

import SubspaceHysteresisEffect from '../../../statuses/Effects/all/ElectronicWarfare/SubspaceHysteresis.50016';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const SubspaceHysteresisDistance = 12;

export class SubspaceHysteresisAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50016;
	public static readonly caption = 'Subspace Hysteresis';
	public static readonly description = SubspaceHysteresisEffect.description;
	public cooldown = 3;
	public baseRange = SubspaceHysteresisDistance;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: SubspaceHysteresisEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SubspaceHysteresisAbility;

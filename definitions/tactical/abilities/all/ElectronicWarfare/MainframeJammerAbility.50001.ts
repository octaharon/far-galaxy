import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import DamageOverTimeStatusEffect from '../../../statuses/Effects/all/AppliedDamage/DamageOverTime.01002';
import MainframeJammerEffect from '../../../statuses/Effects/all/ElectronicWarfare/MainframeJammer.03001';
import { TEffectApplianceItem, TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class MainframeJammerAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50001;
	public static readonly caption = 'Mainframe Jammer';
	public static readonly description = MainframeJammerEffect.description;
	public cooldown = 3;
	public baseRange = MAX_MAP_RADIUS;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: MainframeJammerEffect.id,
		},
		{
			effectId: DamageOverTimeStatusEffect.id,
			props: AttackManager.getDoTEffectFromAttack({
				delivery: UnitAttack.deliveryType.immediate,
				[UnitAttack.attackFlags.DoT]: true,
				[Damage.damageType.magnetic]: {
					min: 25,
					max: 25,
					distribution: 'Uniform',
				},
			}),
		} as TEffectApplianceItem<TStatusEffectStateLookup<typeof DamageOverTimeStatusEffect>>,
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default MainframeJammerAbility;

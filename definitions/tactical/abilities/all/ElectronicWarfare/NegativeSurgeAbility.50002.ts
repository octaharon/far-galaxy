import NegativeSurgeEffect from '../../../statuses/Effects/all/ElectronicWarfare/NegativeSurge.03002';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class NegativeSurgeAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50002;
	public static readonly caption = 'Negative surge';
	public static readonly description = NegativeSurgeEffect.description;
	public cooldown = 2;
	public baseRange = 9;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: NegativeSurgeEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default NegativeSurgeAbility;

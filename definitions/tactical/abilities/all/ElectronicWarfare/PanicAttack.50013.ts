import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import DOTPerMissingHPEffect from '../../../statuses/Effects/all/AppliedDamage/DamagePerMissingHP.01010';
import { TEffectApplianceItem, TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const PANIC_ATTACK_DMG_MIN = 35;
const PANIC_ATTACK_DMG_MAX = 50;
const PANIC_ATTACK_RANGE = 8;
const PANIC_ATTACK_DURATION = 10;

export class PanicAttackAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50013;
	public static readonly caption = 'Panic Attack';
	public static readonly description = `Apply a DOT effect to a Unit, that deals ${AbilityPowerUIToken}${PANIC_ATTACK_DMG_MIN} to ${AbilityPowerUIToken}${PANIC_ATTACK_DMG_MAX} TRM Damage, increased by the percentage of Target's missing Hit Points, for ${PANIC_ATTACK_DURATION} Turns`;
	public cooldown = 3;
	public baseRange = PANIC_ATTACK_RANGE;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DOTPerMissingHPEffect.id,
			props: AttackManager.getDoTEffectFromAttack({
				delivery: UnitAttack.deliveryType.immediate,
				duration: PANIC_ATTACK_DURATION,
				[UnitAttack.attackFlags.DoT]: true,
				[Damage.damageType.thermodynamic]: {
					min: PANIC_ATTACK_DMG_MIN,
					max: PANIC_ATTACK_DMG_MAX,
					distribution: 'Uniform',
				},
			}),
		} as TEffectApplianceItem<TStatusEffectStateLookup<typeof DOTPerMissingHPEffect>>,
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default PanicAttackAbility;

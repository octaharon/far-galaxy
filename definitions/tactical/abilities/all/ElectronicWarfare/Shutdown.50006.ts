import ShutdownStatusEffect, { SHUTDOWN_XP } from '../../../statuses/Effects/all/ElectronicWarfare/Shutdown.03006';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class ShutdownAbility extends GenericAbility<Abilities.abilityTargetTypes> {
	public static readonly id = 50006;
	public static readonly caption = 'Shutdown';
	public static readonly description = `Resets Cooldown of all Abilities that the Unit has, but disables them for Effect Duration. Gain ${SHUTDOWN_XP} XP for every Ability that was on Cooldown`;
	public cooldown = 4;
	public baseRange = 10;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: ShutdownStatusEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default ShutdownAbility;

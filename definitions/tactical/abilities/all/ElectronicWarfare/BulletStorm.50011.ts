import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import DirectDamageStatusEffect from '../../../statuses/Effects/all/AppliedDamage/DirectDamage.01003';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const BulletStormDamageMin = 60;
const BulletStormDamageMax = 80;
const BulletStormRadius = 6;

export class BulletStormAbility extends GenericAbility<Abilities.abilityTargetTypes.around> {
	public static readonly id = 50011;
	public static readonly caption = 'Bullet Storm';
	public static readonly description = `Deal ${AbilityPowerUIToken}${BulletStormDamageMin} to ${AbilityPowerUIToken}${BulletStormDamageMax} KIN Damage as Projectile to all Hostile Units within AoE Radius ${BulletStormRadius}`;
	public cooldown = 3;
	public baseRange = BulletStormRadius;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DirectDamageStatusEffect.id,
			props: {
				attackUnit: AttackManager.getAttackDamage({
					delivery: UnitAttack.deliveryType.ballistic,
					[UnitAttack.attackFlags.Selective]: true,
					[Damage.damageType.kinetic]: {
						min: BulletStormDamageMin,
						max: BulletStormDamageMax,
						distribution: 'Uniform',
					},
				}),
			},
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.around;
}

export default BulletStormAbility;

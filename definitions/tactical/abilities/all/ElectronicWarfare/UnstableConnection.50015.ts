import { numericAPScaling } from '../../../../../src/utils/wellRounded';
import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import UnstableConnectionEffect, {
	TUnstableConnectionProps,
} from '../../../statuses/Effects/all/AppliedDamage/UnstableConnection.01030';
import { TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const UNSTABLE_CONNECTION_DAMAGE_MIN = 70;
const UNSTABLE_CONNECTION_DAMAGE_MAX = 100;
const UNSTABLE_CONNECTIO_RADIUS = 3;
const UNSTABLE_CONNECTION_DISTANCE = 8;
const UNSTABLE_CONNECTION_CAPACITY = 10;

export class UnstableConnectionAbility extends GenericAbility<Abilities.abilityTargetTypes.around> {
	public static readonly id = 50015;
	public static readonly caption = 'Unstable Connection';
	public static readonly description = `Bind all Units in AOE Radius ${UNSTABLE_CONNECTIO_RADIUS} to the invoker for Effect Duration. If the bound Target gets as far away as ${UNSTABLE_CONNECTION_DISTANCE} DU, it takes ${AbilityPowerUIToken}${UNSTABLE_CONNECTION_DAMAGE_MIN} to ${AbilityPowerUIToken}${UNSTABLE_CONNECTION_DAMAGE_MAX} WRP Damage. The invoker gains ${numericAPScaling(
		UNSTABLE_CONNECTION_CAPACITY,
		AbilityPowerUIToken,
	)} Shield Capacity for every bound Hostile Unit.`;
	public cooldown = 4;
	public baseRange = UNSTABLE_CONNECTIO_RADIUS;
	public readonly appliedEffects: TEffectApplianceList<TStatusEffectStateLookup<typeof UnstableConnectionEffect>> = [
		{
			effectId: UnstableConnectionEffect.id,
			props: {
				range: UNSTABLE_CONNECTION_DISTANCE,
				capacity: UNSTABLE_CONNECTION_CAPACITY,
				damage: AttackManager.getAttackDamage({
					delivery: UnitAttack.deliveryType.immediate,
					[Damage.damageType.warp]: {
						min: UNSTABLE_CONNECTION_DAMAGE_MIN,
						max: UNSTABLE_CONNECTION_DAMAGE_MAX,
						distribution: 'Uniform',
					},
				}),
			} as TUnstableConnectionProps,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.around;
}

export default UnstableConnectionAbility;

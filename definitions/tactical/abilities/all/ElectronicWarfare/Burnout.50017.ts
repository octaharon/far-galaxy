import BurnoutStatusEffect, {
	BURNOUT_EFFECT_HP_REDUCTION,
} from '../../../statuses/Effects/all/ElectronicWarfare/Burnout.03007';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const BURNOUT_DISTANCE = 6;

export class BurnoutAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 50017;
	public static readonly caption = 'Burnout';
	public static readonly description = `Unit loses ${Math.round(
		BURNOUT_EFFECT_HP_REDUCTION * 100,
	)}% of remaining Hit Points in the end of each Turn`;
	public cooldown = 4;
	public baseRange = BURNOUT_DISTANCE;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: BurnoutStatusEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default BurnoutAbility;

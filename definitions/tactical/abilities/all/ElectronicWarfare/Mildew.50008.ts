import UnitAttack from '../../../damage/attack';
import AttackManager from '../../../damage/AttackManager';
import Damage from '../../../damage/damage';
import { AbilityPowerUIToken } from '../../../statuses/constants';
import DirectDamageStatusEffect from '../../../statuses/Effects/all/AppliedDamage/DirectDamage.01003';
import { TEffectApplianceList, TStatusEffectStateLookup } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

const MILDEW_DAMAGE_MIN = 25;
const MILDEW_DAMAGE_MAX = 35;
const MILDEW_RADIUS = 2.5;

export class MildewAbility extends GenericAbility<Abilities.abilityTargetTypes.around> {
	public static readonly id = 50008;
	public static readonly caption = 'Mildew';
	public static readonly description = `All Hostile Units within AoE Radius ${MILDEW_RADIUS} are dealt ${AbilityPowerUIToken}${MILDEW_DAMAGE_MIN} to ${AbilityPowerUIToken}${MILDEW_DAMAGE_MAX} BIO and HEA Damage`;
	public cooldown = 0;
	public baseRange = MILDEW_RADIUS;
	public readonly appliedEffects: TEffectApplianceList<TStatusEffectStateLookup<typeof DirectDamageStatusEffect>> = [
		{
			effectId: DirectDamageStatusEffect.id,
			props: {
				attackUnit: AttackManager.getAttackDamage({
					delivery: UnitAttack.deliveryType.immediate,
					[UnitAttack.attackFlags.Selective]: true,
					[Damage.damageType.biological]: {
						min: MILDEW_DAMAGE_MIN,
						max: MILDEW_DAMAGE_MAX,
						distribution: 'Bell',
					},
					[Damage.damageType.heat]: {
						min: MILDEW_DAMAGE_MIN,
						max: MILDEW_DAMAGE_MAX,
						distribution: 'Parabolic',
					},
				}),
			},
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.around;
}

export default MildewAbility;

import FocusedEMIEffect from '../../../statuses/Effects/all/Unit/FocusedEMI.29022';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class FocusedEMIAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = FocusedEMIEffect.id;
	public static readonly caption = 'Combined EMI';
	public static readonly description = FocusedEMIEffect.description;
	public cooldown = 5;
	public baseRange = 8;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: FocusedEMIEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default FocusedEMIAbility;

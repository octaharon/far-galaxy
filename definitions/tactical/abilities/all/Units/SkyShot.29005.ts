import { TMapCellProps } from '../../../map';
import HowitzerShotEffect, { SkyShotRadius } from '../../../statuses/Effects/all/Unit/HowitzerShot.29005';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class HowitzerShotAbility extends GenericAbility<Abilities.abilityTargetTypes.map> {
	public static readonly id = HowitzerShotEffect.id;
	public static readonly caption = 'Howitzer Shot';
	public static readonly description = HowitzerShotEffect.description;
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: SkyShotRadius,
	};
	public cooldown = 6;
	public baseRange = 13;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: HowitzerShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default HowitzerShotAbility;

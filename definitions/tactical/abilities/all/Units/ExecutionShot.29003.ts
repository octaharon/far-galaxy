import ExecutionShotEffect from '../../../statuses/Effects/all/Unit/ExecutionShot.29003';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class ExecutionShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = ExecutionShotEffect.id;
	public static readonly caption = 'Execution Shot';
	public static readonly description = ExecutionShotEffect.description;
	public cooldown = 5;
	public baseRange = 6;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: ExecutionShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default ExecutionShotAbility;

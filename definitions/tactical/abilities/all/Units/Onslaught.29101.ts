import OnslaughtEffect from '../../../statuses/Effects/all/Unit/Onslaught.29101';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class OnslaughtAbility extends GenericAbility<Abilities.abilityTargetTypes.self> {
	public static readonly id = OnslaughtEffect.id;
	public static readonly caption = 'Onslaught';
	public static readonly description = OnslaughtEffect.description;
	public cooldown = 5;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: OnslaughtEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default OnslaughtAbility;

import { TMapCellProps } from '../../../map';
import { MAX_MAP_RADIUS } from '../../../map/constants';
import SmokeScreenEffect from '../../../statuses/Effects/all/Unit/SmokeScreen.60003';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class SmokeScreenAbility extends GenericAbility<TargetType> {
	public static readonly id = 60003;
	public static readonly caption = 'Smoke Screen';
	public static readonly description = SmokeScreenEffect.description;
	public static readonly appliedEffects = [SmokeScreenEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: false,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 4,
	};
	public cooldown = 5;
	public baseRange = 6;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default SmokeScreenAbility;

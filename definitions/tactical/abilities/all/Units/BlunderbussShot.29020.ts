import BlunderbussShotEffect from '../../../statuses/Effects/all/Unit/Blunderbuss.29020';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class BlunderbussShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = BlunderbussShotEffect.id;
	public static readonly caption = 'Blunderbuss Shot';
	public static readonly description = BlunderbussShotEffect.description;
	public cooldown = 3;
	public baseRange = 4;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: BlunderbussShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default BlunderbussShotAbility;

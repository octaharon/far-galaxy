import SiegeModeEffect from '../../../statuses/Effects/all/Unit/SiegeMode.29102';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class SiegeModeAbility extends GenericAbility<Abilities.abilityTargetTypes.self> {
	public static readonly id = SiegeModeEffect.id;
	public static readonly caption = 'Siege Mode';
	public static readonly description = SiegeModeEffect.description;
	public cooldown = 5;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: SiegeModeEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default SiegeModeAbility;

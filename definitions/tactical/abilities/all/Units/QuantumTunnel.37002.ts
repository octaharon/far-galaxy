import QuantumTunnelEffect      from '../../../statuses/Effects/all/Unique/QuantumTunnel.37002';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility           from '../../GenericAbility';
import Abilities                from '../../types';

export class QuantumTunnelAbility extends GenericAbility<Abilities.abilityTargetTypes.around> {
	public static readonly id = 37002;
	public static readonly caption = QuantumTunnelEffect.caption;
	public static readonly description = QuantumTunnelEffect.description;
	public cooldown = 5;
	public baseRange = 4;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: QuantumTunnelEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.around;
}

export default QuantumTunnelAbility;

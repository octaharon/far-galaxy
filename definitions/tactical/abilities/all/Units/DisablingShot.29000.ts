import DisablingShotEffect from '../../../statuses/Effects/all/Unit/DisablingShot.29000';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class DisablingShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = DisablingShotEffect.id;
	public static readonly caption = 'Disabling Shot';
	public static readonly description = DisablingShotEffect.description;
	public cooldown = 5;
	public baseRange = 7;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DisablingShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default DisablingShotAbility;

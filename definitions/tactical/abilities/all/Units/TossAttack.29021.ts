import TossAttackEffect from '../../../statuses/Effects/all/Unit/TossAttack.29021';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class TossAttackAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = TossAttackEffect.id;
	public static readonly caption = 'Toss Attack';
	public static readonly description = TossAttackEffect.description;
	public cooldown = 4;
	public baseRange = 6;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: TossAttackEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default TossAttackAbility;

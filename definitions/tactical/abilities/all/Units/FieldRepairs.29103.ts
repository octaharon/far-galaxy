import FieldRepairsEffect from '../../../statuses/Effects/all/Unit/FieldRepairs.29103';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class FieldRepairsAbility extends GenericAbility<Abilities.abilityTargetTypes.self> {
	public static readonly id = FieldRepairsEffect.id;
	public static readonly caption = 'Field Repairs';
	public static readonly description = FieldRepairsEffect.description;
	public cooldown = 6;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: FieldRepairsEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default FieldRepairsAbility;

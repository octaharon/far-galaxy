import { UnitCargoEffect, UnitUnloadRange } from '../../../statuses/Effects/all/Unit/UnitCargo.29201';
import UnitLiftEffect from '../../../statuses/Effects/all/Unit/UnitLift.29200';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class UnitLiftAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = UnitCargoEffect.id;
	public static readonly caption = UnitLiftEffect.caption;
	public static readonly description = UnitLiftEffect.description;
	public cooldown = 0;
	public baseRange = UnitUnloadRange;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: UnitLiftEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default UnitLiftAbility;

import GroundingShotEffect from '../../../statuses/Effects/all/Unit/GroundingShot.29009';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class GroundingShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = GroundingShotEffect.id;
	public static readonly caption = 'Grounding Shot';
	public static readonly description = GroundingShotEffect.description;
	public cooldown = 4;
	public baseRange = 6;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: GroundingShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default GroundingShotAbility;

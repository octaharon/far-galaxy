import AirInterdictionEffect from '../../../statuses/Effects/all/Unit/AirInterdiction.29104';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class AirInterdictionAbility extends GenericAbility<Abilities.abilityTargetTypes.self> {
	public static readonly id = AirInterdictionEffect.id;
	public static readonly caption = 'Air Interdiction';
	public static readonly description = AirInterdictionEffect.description;
	public cooldown = 4;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: AirInterdictionEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default AirInterdictionAbility;

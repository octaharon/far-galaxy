import StrafeShotEffect from '../../../statuses/Effects/all/Unit/RoundShot.29004';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class StrafeShotAbility extends GenericAbility<Abilities.abilityTargetTypes.around> {
	public static readonly id = StrafeShotEffect.id;
	public static readonly caption = 'Strafe Shot';
	public static readonly description = StrafeShotEffect.description;
	public cooldown = 1;
	public baseRange = 3;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: StrafeShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.around;
}

export default StrafeShotAbility;

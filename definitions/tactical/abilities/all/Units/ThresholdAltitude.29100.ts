import StrafeShotEffect from '../../../statuses/Effects/all/Unit/RoundShot.29004';
import ThresholdAltitudeEffect from '../../../statuses/Effects/all/Unit/ThresholdAltitude.29100';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class ThresholdAltitudeAbility extends GenericAbility<Abilities.abilityTargetTypes.self> {
	public static readonly id = ThresholdAltitudeEffect.id;
	public static readonly caption = 'Threshold Altitude';
	public static readonly description = ThresholdAltitudeEffect.description;
	public cooldown = 5;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: ThresholdAltitudeEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default ThresholdAltitudeAbility;

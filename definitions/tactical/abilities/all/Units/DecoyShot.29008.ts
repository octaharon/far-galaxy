import DecoyShotEffect from '../../../statuses/Effects/all/Unit/DecoyShot.29008';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class DecoyShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = DecoyShotEffect.id;
	public static readonly caption = 'Decoy Shot';
	public static readonly description = DecoyShotEffect.description;
	public cooldown = 4;
	public baseRange = 8;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DecoyShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default DecoyShotAbility;

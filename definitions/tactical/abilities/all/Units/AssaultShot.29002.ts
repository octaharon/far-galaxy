import AssaultShotEffect from '../../../statuses/Effects/all/Unit/AssaultShot.29002';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class AssaultShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = AssaultShotEffect.id;
	public static readonly caption = 'Assault Shot';
	public static readonly description = AssaultShotEffect.description;
	public cooldown = 4;
	public baseRange = 9;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: AssaultShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default AssaultShotAbility;

import { TMapCellProps } from '../../../map';
import CloudControlEffect, { CloudControlAreaRadius } from '../../../statuses/Effects/all/Unit/CloudControl.29023';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class CloudControlAbility extends GenericAbility<Abilities.abilityTargetTypes.map> {
	public static readonly id = CloudControlEffect.id;
	public static readonly caption = 'Cloud Control';
	public static readonly description = CloudControlEffect.description;
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: CloudControlAreaRadius,
	};
	public cooldown = 5;
	public baseRange = 8;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: CloudControlEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default CloudControlAbility;

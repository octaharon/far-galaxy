import DisarmEffect from '../../../statuses/Effects/all/Unit/Disarm.29010';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class DisarmAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = DisarmEffect.id;
	public static readonly caption = 'Disarm';
	public static readonly description = DisarmEffect.description;
	public cooldown = 5;
	public baseRange = 8;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DisarmEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default DisarmAbility;

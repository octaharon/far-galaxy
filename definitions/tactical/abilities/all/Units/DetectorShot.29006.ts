import { TMapCellProps } from '../../../map';
import DetectorShotEffect, { DetectorShotRadius } from '../../../statuses/Effects/all/Unit/DetectorShot.29006';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class DetectorShotAbility extends GenericAbility<Abilities.abilityTargetTypes.map> {
	public static readonly id = DetectorShotEffect.id;
	public static readonly caption = 'Detector Shot';
	public static readonly description = DetectorShotEffect.description;
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: DetectorShotRadius,
	};
	public cooldown = 4;
	public baseRange = 7;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DetectorShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default DetectorShotAbility;

import BuckleShotEffect from '../../../statuses/Effects/all/Unit/BuckleShot.29001';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class BuckleShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = BuckleShotEffect.id;
	public static readonly caption = 'Buckle Shot';
	public static readonly description = BuckleShotEffect.description;
	public cooldown = 4;
	public baseRange = 9;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: BuckleShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default BuckleShotAbility;

import UnitUnloadEffect from '../../../statuses/Effects/all/Unit/UnitUnload.29202';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class UnitUnloadAbility extends GenericAbility<Abilities.abilityTargetTypes.self> {
	public static readonly id = UnitUnloadEffect.id;
	public static readonly caption = UnitUnloadEffect.caption;
	public static readonly description = UnitUnloadEffect.description;
	public cooldown = 0;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: UnitUnloadEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default UnitUnloadAbility;

import { BlastingShotEffect } from '../../../statuses/Effects/all/Unit/BlastingShot.29007';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class BlastingShotAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = BlastingShotEffect.id;
	public static readonly caption = 'Blasting Shot';
	public static readonly description = BlastingShotEffect.description;
	public cooldown = 4;
	public baseRange = 6;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: BlastingShotEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default BlastingShotAbility;

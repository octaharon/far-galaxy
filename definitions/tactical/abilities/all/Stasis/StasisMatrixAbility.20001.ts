import StasisMatrixEffect, { STASIS_MATRIX_XP } from '../../../statuses/Effects/all/Stasis/StasisMatrix.04001';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.unit;

export class StasisMatrixAbility extends GenericAbility<TargetType> {
	public static readonly id = 20001;
	public static readonly caption = 'Stasis Matrix';
	public static readonly description = `Put a unit into stasis, rendering it Disabled and Non-Targetable for 2 Turns. For every possible Attack prevented, gain ${STASIS_MATRIX_XP} XP`;
	public static readonly appliedEffects = [StasisMatrixEffect.id];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
	public cooldown = 4;
	public baseRange = 16;
}

export default StasisMatrixAbility;

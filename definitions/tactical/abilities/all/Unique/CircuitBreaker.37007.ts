import CircuitBreakerEffect from '../../../statuses/Effects/all/Unique/CircuitBreaker.37007';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class CircuitBreakerAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 37007;
	public static readonly caption = 'Circuit Breaker';
	public static readonly description = CircuitBreakerEffect.description;
	public cooldown = 5;
	public baseRange = 3;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: CircuitBreakerEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default CircuitBreakerAbility;

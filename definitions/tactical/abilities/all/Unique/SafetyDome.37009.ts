import { TMapCellProps } from '../../../map';
import SafetyDomeEffect from '../../../statuses/Effects/all/Unique/SafetyDome.37009';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class SafetyDomeAbility extends GenericAbility<TargetType> {
	public static readonly id = 37009;
	public static readonly caption = 'Safety Dome';
	public static readonly description = SafetyDomeEffect.description;
	public static readonly appliedEffects = [SafetyDomeEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: true,
		water: false,
		deploy: false,
		radius: 3,
	};
	public cooldown = 6;
	public baseRange = 12;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default SafetyDomeAbility;

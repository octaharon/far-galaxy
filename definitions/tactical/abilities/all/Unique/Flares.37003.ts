import FlaresEffect from '../../../statuses/Effects/all/Unique/Flares.37003';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class FlaresAbility extends GenericAbility<Abilities.abilityTargetTypes.self> {
	public static readonly id = 37003;
	public static readonly caption = 'Flares';
	public static readonly description = FlaresEffect.description;
	public cooldown = 4;
	public baseRange = 0;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: FlaresEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default FlaresAbility;

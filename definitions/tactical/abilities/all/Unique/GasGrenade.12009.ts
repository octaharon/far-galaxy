import { TMapCellProps } from '../../../map';
import { GAS_GRENADE_RADIUS, GasGrenadeEffect } from '../../../statuses/Effects/all/Bionic/GasGrenade.12009';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class GasGrenadeAbility extends GenericAbility<TargetType> {
	public static readonly id = GasGrenadeEffect.id;
	public static readonly caption = 'Gas Grenade';
	public static readonly description = GasGrenadeEffect.description;
	public static readonly appliedEffects = [GasGrenadeEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: GAS_GRENADE_RADIUS,
	};
	public cooldown = 5;
	public baseRange = 9;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default GasGrenadeAbility;

import TearAndWearEffect from '../../../statuses/Effects/all/Unique/TearAndWear.37004';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class WearAndTearAbility extends GenericAbility<Abilities.abilityTargetTypes.around> {
	public static readonly id = 37004;
	public static readonly caption = 'Wear And Tear';
	public static readonly description = TearAndWearEffect.description;
	public cooldown = 4;
	public baseRange = 4;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: WearAndTearAbility.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.around;
}

export default WearAndTearAbility;

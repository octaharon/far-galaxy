import TacticalNodeEffect from '../../../statuses/Effects/all/Unique/TacticalNode.37008';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class TacticalNodeAbility extends GenericAbility<Abilities.abilityTargetTypes.self> {
	public static readonly id = 37008;
	public static readonly caption = 'Tactical Node';
	public static readonly description = TacticalNodeEffect.description;
	public cooldown = 4;
	public baseRange = 10;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: TacticalNodeEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.self;
}

export default TacticalNodeAbility;

import SoulkillerEffect from '../../../statuses/Effects/all/Unique/Soulkiller.37020';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class SoulkillerAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = SoulkillerEffect.id;
	public static readonly caption = SoulkillerEffect.caption;
	public static readonly description = SoulkillerEffect.description;
	public cooldown = 6;
	public baseRange = 7;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: SoulkillerEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SoulkillerAbility;

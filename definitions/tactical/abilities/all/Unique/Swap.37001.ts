import SwapEffect from '../../../statuses/Effects/all/Unique/Swap.37001';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class SwapAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 37001;
	public static readonly caption = 'Swap';
	public static readonly description = 'Swap positions with a Unit';
	public cooldown = 3;
	public baseRange = 12;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: SwapEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default SwapAbility;

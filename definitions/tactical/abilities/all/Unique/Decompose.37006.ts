import DecomposeEffect from '../../../statuses/Effects/all/Unique/Decompose.37006';
import { TEffectApplianceList } from '../../../statuses/types';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

export class DecomposeAbility extends GenericAbility<Abilities.abilityTargetTypes.unit> {
	public static readonly id = 37006;
	public static readonly caption = 'Decompose';
	public static readonly description = DecomposeEffect.description;
	public cooldown = 5;
	public baseRange = 7;
	public readonly appliedEffects: TEffectApplianceList = [
		{
			effectId: DecomposeEffect.id,
		},
	];
	public readonly targetType = Abilities.abilityTargetTypes.unit;
}

export default DecomposeAbility;

import { TMapCellProps } from '../../../map';
import TimeBombEffect from '../../../statuses/Effects/all/Unique/TimeBomb.37010';
import GenericAbility from '../../GenericAbility';
import Abilities from '../../types';

type TargetType = Abilities.abilityTargetTypes.map;

export class TimeBombAbility extends GenericAbility<TargetType> {
	public static readonly id = 37010;
	public static readonly caption = 'Timebomb Zone';
	public static readonly description = TimeBombEffect.description;
	public static readonly appliedEffects = [TimeBombEffect.id];
	public static readonly mapCellProps: Partial<TMapCellProps> = {
		lineOfSight: true,
		passable: true,
		scan: false,
		water: false,
		deploy: false,
		radius: 4,
	};
	public cooldown = 5;
	public baseRange = 11;
	public readonly targetType = Abilities.abilityTargetTypes.map;
}

export default TimeBombAbility;

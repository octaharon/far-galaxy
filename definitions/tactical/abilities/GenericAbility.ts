import Collectible, { TCollectible } from '../../core/Collectible';
import { DIEntityDescriptors, DIInjectableCollectibles } from '../../core/DI/injections';
import BasicMaths from '../../maths/BasicMaths';
import { IUnitEffectModifier } from '../../prototypes/types';
import Abilities from './types';

export class GenericAbility<TargetType extends Abilities.abilityTargetTypes>
	extends Collectible<Abilities.TAbilityProps, Abilities.TSerializedAbility, Abilities.IStaticAbility<TargetType>>
	implements Abilities.IAbility<TargetType>
{
	public static readonly appliedEffects: number[] = [];
	public static readonly id: number = 0;
	public readonly targetType: TargetType;
	public cooldown: number;
	public baseRange: number;
	public cooldownLeft: number;
	public usable: boolean;

	public get static() {
		return this.constructor as TCollectible<this, Abilities.IStaticAbility<TargetType>>;
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableCollectibles.ability];
	}

	public init() {
		if (isNaN(this.cooldownLeft) || this.cooldownLeft < 0) this.cooldownLeft = 0;
		this.usable = true;
		return this;
	}

	onTurnEnd() {
		if (this.usable) this.cooldownLeft = Math.max(this.cooldownLeft - 1, 0);
		return this;
	}

	onTurnStart() {
		return this;
	}

	applyModifier(m: IUnitEffectModifier) {
		if (m?.abilityRangeModifier)
			this.baseRange = BasicMaths.applyModifier(this.baseRange, ...m.abilityRangeModifier);
		if (m?.abilityCooldownModifier) {
			const relativeCd = this.cooldownLeft / this.cooldown;
			this.cooldown = BasicMaths.applyModifier(this.cooldown, ...m.abilityCooldownModifier);
			this.cooldownLeft = this.cooldown * relativeCd;
		}
		return this;
	}

	public serialize(): Abilities.TSerializedAbility {
		return {
			...this.__serializeCollectible(),
			cooldown: this.cooldown,
			baseRange: this.baseRange,
			cooldownLeft: this.cooldownLeft,
			usable: this.usable,
		};
	}
}

export default GenericAbility;

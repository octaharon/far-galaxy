import { CollectibleFactory } from '../../core/Collectible';

import { DIInjectableCollectibles } from '../../core/DI/injections';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import { IStatusEffect } from '../statuses/types';
import AbilityList from './index';
import Abilities from './types';

export class AbilityManager
	extends CollectibleFactory<Abilities.TAbilityProps, Abilities.TSerializedAbility, Abilities.TAbilityOption>
	implements Abilities.IAbilityFactory
{
	public instantiate(p: Abilities.TAbilityProps, id: number) {
		return this.__instantiateCollectible(
			{
				usable: true,
				cooldownLeft: 0,
				...p,
			},
			id,
		);
	}

	public instantiateEffects<T extends Abilities.abilityTargetTypes>(
		ability: Abilities.IAbility<T>,
		source: StatusEffectMeta.TStatusEffectTarget,
		target: Abilities.TAbilityTargetType<T>,
	): Array<IStatusEffect<Abilities.TAbilityEffectTargetType<T>>> {
		return ability.static.appliedEffects.map((effectId) => {
			return this.getProvider(DIInjectableCollectibles.effect).instantiate(null, effectId, source, target);
		});
	}

	public unserialize(t: Abilities.TSerializedAbility) {
		const c = this.__instantiateCollectible(t, t.id);
		c.setInstanceId(t.instanceId);
		return c;
	}

	public fill() {
		this.__fillItems(AbilityList);
		return this;
	}
}

export default AbilityManager;

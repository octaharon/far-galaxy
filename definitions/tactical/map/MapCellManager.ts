import { DIInjectableCollectibles } from '../../core/DI/injections';
import { SerializableFactory } from '../../core/Serializable';
import { IMapCell, IMapCellFactory, TMapCellProps, TSerializedMapCell } from './index';
import GenericMapCell from './MapCell';

class MapCellManager
	extends SerializableFactory<TMapCellProps, TSerializedMapCell, IMapCell>
	implements IMapCellFactory
{
	public instantiate(props: Partial<TMapCellProps>) {
		const c = this.__instantiateSerializable(props, GenericMapCell);
		return c;
	}

	public unserialize(t: TSerializedMapCell) {
		const c = this.instantiate({
			...t,
			effects: (t.effects || []).map((e) => this.getProvider(DIInjectableCollectibles.effect).unserialize(e)),
		});
		c.setInstanceId(t.instanceId);
		return c;
	}
}

export default MapCellManager;

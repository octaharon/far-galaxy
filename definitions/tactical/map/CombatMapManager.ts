import { DIInjectableSerializables } from '../../core/DI/injections';
import { SerializableFactory } from '../../core/Serializable';
import GenericCombatMap from './GenericCombatMap';
import { ICombatMap, ICombatMapFactory, TCombatMapProps, TSerializedCombatMap } from './index';

class CombatMapManager
	extends SerializableFactory<TCombatMapProps, TSerializedCombatMap, ICombatMap>
	implements ICombatMapFactory
{
	public instantiate(props: Partial<TCombatMapProps>) {
		return this.__instantiateSerializable(props, GenericCombatMap);
	}

	public unserialize(t: TSerializedCombatMap) {
		const c = this.instantiate({
			...t,
			salvages: (t.salvages || []).map((c) => this.getProvider(DIInjectableSerializables.salvage).unserialize(c)),
			unitsOnMap: (t.unitsOnMap || []).map((c) =>
				this.getProvider(DIInjectableSerializables.unit).unserialize(c),
			),
			cells: (t.cells || []).map((c) => this.getProvider(DIInjectableSerializables.mapcell).unserialize(c)),
		});
		c.setInstanceId(t.instanceId);
		return c;
	}
}

export default CombatMapManager;

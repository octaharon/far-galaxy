import memoize from 'memoize-one';
import { DIEntityDescriptors, DIInjectableSerializables } from '../../core/DI/injections';
import InterfaceTypes from '../../core/InterfaceTypes';
import Serializable from '../../core/Serializable';
import { GeometricPrecision } from '../../maths/constants';
import Geometry from '../../maths/Geometry';
import Vectors from '../../maths/Vectors';
import { IPlayerBase } from '../../orbital/base/types';
import { TPlayerId } from '../../player/playerId';
import UnitChassis from '../../technologies/types/chassis';
import UnitAttack from '../damage/attack';
import AttackManager from '../damage/AttackManager';
import Salvage from '../salvage/types';
import GenericStatusEffect from '../statuses/GenericStatusEffect';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import Units, { UNIT_MAP_SIZE, UNIT_MAP_SIZE_MASSIVE } from '../units/types';
import {
	ICombatMap,
	ICombatMapStatic,
	IMapCell,
	TCombatMapProps,
	TMapCellStaticProps,
	TMapEntities,
	TMapParameters,
	TSerializedCombatMap,
} from './index';
import { IMapLake, ITerrainEntityDecorator, TMapDoodad, TMapTerrainEntity } from './MapObstacles';

export type TCombatMap<ObjectType extends GenericCombatMap = GenericCombatMap> = ClassConstructor<GenericCombatMap> &
	typeof GenericCombatMap;

export default class GenericCombatMap
	extends Serializable<TCombatMapProps, TSerializedCombatMap>
	implements ICombatMap
{
	public radius: number;
	public sectors: number;
	public spinAngle: number;
	public terrainEntities: TMapEntities;
	public doodads: TMapDoodad[];
	public parameters: TMapParameters;
	public cells: IMapCell[];
	public unitsOnMap: Units.IUnit[];
	public salvages: Salvage.ISalvage[];

	public get static() {
		return this.constructor as ICombatMapStatic;
	}

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableSerializables.map];
	}

	public static isEntityPassable<T extends ITerrainEntityDecorator>(t: T, m: UnitChassis.movementType): boolean {
		switch (m) {
			case UnitChassis.movementType.air:
				return true;
			case UnitChassis.movementType.hover:
				return t.passable;
			case UnitChassis.movementType.ground:
				return t.passable && !t.water;
			case UnitChassis.movementType.naval:
				return t.water && t.passable;
			default:
				return false;
		}
	}

	public static whichEntityContainsPoint<T extends TMapTerrainEntity>(
		x: InterfaceTypes.IVector,
		entities: T[],
		withinRange: number = GeometricPrecision,
	): T {
		const findEntity = memoize(
			(t: InterfaceTypes.IVector, e: T[]) => {
				for (const entity of e)
					if (GenericCombatMap.isPointWithinEntityBounds(t, entity, withinRange)) return entity;
				return null;
			},
			(newArgs: [InterfaceTypes.IVector, T[]], oldArgs: [InterfaceTypes.IVector, T[]]) => {
				const n1 = newArgs[1] ? newArgs[1].map((e) => e.entityId) : null;
				const o1 = oldArgs[1] ? oldArgs[1].map((e) => e.entityId) : null;
				return JSON.stringify([newArgs[0], n1]) === JSON.stringify([oldArgs[0]], o1);
			},
		);
		return null;
	}

	public static isPointWithinEntityBounds(x: InterfaceTypes.IVector, t: TMapTerrainEntity, within = 0): boolean {
		return Geometry.isPointInsideShape(x, t.shape, within);
	}

	public static isPointWithinTerrainEntities(
		x: InterfaceTypes.IVector,
		entities: TMapTerrainEntity[],
		entityFlags: Partial<ITerrainEntityDecorator> = {},
		within = 0,
	): boolean {
		return entities.reduce((r, e) => {
			if (r) return r;
			if (
				(entityFlags.water !== undefined && entityFlags.water !== e.water) ||
				(entityFlags.passable !== undefined && entityFlags.passable !== e.passable) ||
				(entityFlags.lineOfSight !== undefined && entityFlags.lineOfSight !== e.lineOfSight)
			)
				return r;
			return r || GenericCombatMap.isPointWithinEntityBounds(x, e, within);
		}, false);
	}

	public static isPointWithinDoodad(
		x: InterfaceTypes.IVector,
		d: TMapDoodad,
		within: number = GeometricPrecision,
	): boolean {
		return Geometry.isPointInRange(x, {
			...d,
			radius: within + d.collisionRadius,
		});
	}

	public static isPointWithinMapCell(
		x: InterfaceTypes.IVector,
		d: IMapCell,
		filter: Partial<TMapCellStaticProps> = {},
		within: number = GeometricPrecision,
	): boolean {
		return Geometry.isPointInRange(x, {
			...d,
			radius: within + d.radius,
		});
	}

	public static doesWaterPathExists<T extends IMapLake>(
		p1: InterfaceTypes.IVector,
		p2: InterfaceTypes.IVector,
		waterEntities: T[],
		waterZones: IMapCell[] = [],
	): boolean {
		// Look for points where a path intersects water edges
		const intersections: Record<string, InterfaceTypes.IVector[]> = waterEntities.reduce((obj, e) => {
			const i = Geometry.doesRayIntersectShape(e.shape, p1, p2);
			if (i) Object.assign(obj, { [e.entityId]: i });
			return obj;
		}, {});
		if (waterZones.length)
			waterZones.forEach((z) => {
				const i = Geometry.doesRayIntersectCircle(z, p1, p2);
				if (i) intersections[z.getInstanceId()] = i;
			});

		// Preparing those entities which are crossed by path
		const waterEntitiesMap = waterEntities.reduce<Record<string, T>>(
			(acc, v) => (intersections[v.entityId] ? { ...acc, [v.entityId]: v } : acc),
			{},
		);
		const waterZonesMap = waterZones.reduce<Record<string, IMapCell>>(
			(acc, v) => (intersections[v.getInstanceId()] ? { ...acc, [v.getInstanceId()]: v } : acc),
			{},
		);

		const intersectionsMap = {
			...waterEntitiesMap,
			...waterZonesMap,
		};

		const isEntity = (c: IMapCell | T): c is T => 'entityId' in c;
		const isMapCell = (c: IMapCell | T): c is IMapCell => 'playerId' in c;
		const insideEntityOrMapCell = (c: IMapCell | T, point: InterfaceTypes.IVector) => {
			if (isMapCell(c)) return GenericCombatMap.isPointWithinMapCell(point, c);
			if (isEntity(c)) return GenericCombatMap.isPointWithinEntityBounds(point, c);
			return false;
		};

		let start = false;
		let end = false;
		const crossedEntitiesIds = Object.keys(intersectionsMap);
		for (const e of Object.values(intersectionsMap)) {
			if (insideEntityOrMapCell(e, p1)) start = true;
			if (insideEntityOrMapCell(e, p2)) end = true;
			if (start && end) break;
		}
		if (!(start && end)) return false;

		for (const entityId of crossedEntitiesIds) {
			for (const point of intersections[entityId]) {
				let cross = false; // every point where the path crosses with a bound should be inside another water
				// entity for line to be inside the water zone
				for (const anotherEntity of crossedEntitiesIds) {
					if (entityId === anotherEntity) continue;
					if (insideEntityOrMapCell(intersectionsMap[anotherEntity], point)) {
						cross = true;
						break;
					}
				}
				if (!cross) return false;
			}
		}

		return true;
	}

	public isShotPossible(
		attack: UnitAttack.IAttackInterface,
		source: Units.IUnit,
		target: Units.IUnit | InterfaceTypes.IVector,
	): boolean {
		const directAttack = attack.attacks.find((a) => AttackManager.isDirectAttack(a));
		const aoeAttack = attack.attacks.find((a) => AttackManager.isAOEAttack(a));
		const isUnit = (t: Units.IUnit | InterfaceTypes.IVector): t is Units.IUnit => 'effectTargetType' in t;
		const isVector = (t: Units.IUnit | InterfaceTypes.IVector): t is InterfaceTypes.IVector => !isUnit(t);
		if (isVector(target)) {
			if (!aoeAttack) return false;
			return (
				source.movement === UnitChassis.movementType.air ||
				!AttackManager.isLineOfSightAttack(aoeAttack.delivery) ||
				this.isLineOfSightAvailable(
					source.location,
					target,
					!(directAttack && AttackManager.isPenetrativeAttack(directAttack)),
				)
			);
		}
		if (isUnit(target)) {
			const priorityAttack = directAttack || aoeAttack;
			if (!priorityAttack) return false;
			return (
				AttackManager.isReachableByAttack(priorityAttack.delivery, target.movement) &&
				(source.movement === UnitChassis.movementType.air ||
					!AttackManager.isLineOfSightAttack(aoeAttack.delivery) ||
					this.isLineOfSightAvailable(
						source.location,
						target.location,
						!(directAttack && AttackManager.isPenetrativeAttack(directAttack)),
					))
			);
		}
		return false;
	}

	public deployUnit(unit: Units.IUnit, location: InterfaceTypes.IVector) {
		if (!unit || !this.isPointPassable(location, unit.movement)) return false;
		unit.location = {
			...location,
			elevation:
				unit.movement === UnitChassis.movementType.air
					? 1
					: unit.movement === UnitChassis.movementType.hover
					? 0.5
					: 0,
			radius: unit.targetFlags[UnitChassis.targetType.massive] ? UNIT_MAP_SIZE_MASSIVE : UNIT_MAP_SIZE,
			mapId: this.getInstanceId(),
		};
		unit.setDestroyedCallback((destructionSource, destroyedUnit, salvage) => {
			if (destroyedUnit.location) {
				this.removeUnit(destroyedUnit);
				this.putSalvage(salvage, destroyedUnit.location);
			}
		});
		this.unitsOnMap.push(unit);
		return true;
	}

	public deployUnitFromBase(base: IPlayerBase, unitBayIndex: number, location: InterfaceTypes.IVector): boolean {
		const unit = base?.unitsAtBay?.[unitBayIndex] ?? null;
		if (!unit || !base.canUnitBeDeployedToPlanet(unit, this.parameters)) return false;
		const deployZones = this.getMapCells({
			deploy: true,
		})
			.filter((c) => c.playerId && c.playerId === unit.playerId)
			.filter((c) => GenericCombatMap.isPointWithinMapCell(location, c));
		if (!deployZones.length) return false;
		const deployedUnit = base.deployUnit(unit);
		return this.deployUnit(deployedUnit, location);
	}

	public getScannableArea(player: TPlayerId): InterfaceTypes.ISpot[] {
		return this.getDeployedUnits()
			.filter((u) => u.playerId === player)
			.map((u) => ({
				x: u.location.x,
				y: u.location.y,
				radius: u.scanRange,
			}))
			.concat(...this.getMapCells({ playerId: player, scan: true }));
	}

	public removeUnit(unit: Units.IUnit) {
		this.unitsOnMap = this.unitsOnMap.filter((u) => u.getInstanceId() === unit.getInstanceId());
		return this;
	}

	public putSalvage(salvage: Salvage.ISalvage, location: InterfaceTypes.IVector) {
		if (!Object.keys(salvage.payload).length) return this;
		if (!this.isPointPassable(location, UnitChassis.movementType.hover)) return this;
		this.salvages.push(salvage);
		salvage.location = {
			...(salvage.location || { radius: UNIT_MAP_SIZE }),
			...location,
			elevation: 0,
			mapId: this.getInstanceId(),
		};
		return this;
	}

	public applyAoEAttack(
		location: InterfaceTypes.IVector,
		attack: UnitAttack.TAttackAOEUnit,
		source?: StatusEffectMeta.TStatusEffectTarget,
		excludeUnit?: Units.IUnit,
	): this {
		const defaultSourceLocation = { x: 0, y: 0 };
		const sourceLocation = GenericStatusEffect.isUnitTarget(source)
			? source.location || defaultSourceLocation
			: defaultSourceLocation;
		let targets = this.getUnitsInAoEZone(
			attack,
			sourceLocation,
			location,
			attack.delivery,
			AttackManager.isSelectiveAttack(attack) && source ? (t) => t.doesBelongToPlayer(source.playerId) : Boolean,
		);
		if (excludeUnit) targets = targets.filter((u) => u.getInstanceId() !== excludeUnit.getInstanceId());
		targets.forEach((u) => {
			const modifier: IModifier = {};
			if (
				[
					UnitAttack.TAoEDamageSplashTypes.circle,
					UnitAttack.TAoEDamageSplashTypes.cone90,
					UnitAttack.TAoEDamageSplashTypes.cone180,
					UnitAttack.TAoEDamageSplashTypes.lineAside,
					UnitAttack.TAoEDamageSplashTypes.lineBehind,
				].includes(attack.aoeType)
			)
				// Accounting for AoE Magnitude as a distance factor
				modifier.factor = Vectors.module()(Vectors.add(location, Vectors.inverse(u.location))) / attack.radius;
			u.applyAttackUnit(attack, source, modifier);
		});
		return this;
	}

	public getUnitClosestToPoint(
		point: InterfaceTypes.IVector,
		units: Units.IUnit[] = this.unitsOnMap,
		within: number = this.radius,
	) {
		let closestUnit: Units.IUnit = null;
		let minDistance = Infinity;
		for (const u of units)
			if (u.location) {
				const distance = Vectors.module()(Vectors.add(u.location, Vectors.inverse(point)));
				if (distance < within && distance < minDistance) {
					minDistance = distance;
					closestUnit = u;
				}
			}
		return closestUnit;
	}

	public getUnitsInAoEZone(
		zone: UnitAttack.IAreaOfEffectAttackDecorator,
		source: InterfaceTypes.IVector,
		target: InterfaceTypes.IVector,
		delivery: UnitAttack.deliveryType,
		selectionFn: (u: Units.IUnit) => boolean = Boolean,
	): Units.IUnit[] {
		switch (zone.aoeType) {
			case UnitAttack.TAoEDamageSplashTypes.circle:
				return this.getUnitsInRange({
					...target,
					radius: zone.radius,
				}).filter(
					(u) =>
						(!AttackManager.isLineOfSightAttack(delivery) ||
							this.isLineOfSightAvailable(target, u.location)) &&
						AttackManager.isReachableByAttack(delivery, u.movement) &&
						selectionFn(u),
				);
			case UnitAttack.TAoEDamageSplashTypes.scatter:
				return this.getUnitsInRange({
					...target,
					radius: zone.radius,
				})
					.filter(
						(u) =>
							(!AttackManager.isLineOfSightAttack(delivery) ||
								this.isLineOfSightAvailable(target, u.location)) &&
							AttackManager.isReachableByAttack(delivery, u.movement) &&
							selectionFn(u),
					)
					.reduce((selectedUnits, unit, ix, arr) => {
						if (ix >= arr.length * zone.magnitude) return selectedUnits;
						return selectedUnits.concat(arr[Math.floor(Math.random() * arr.length)]);
					}, []);
			case UnitAttack.TAoEDamageSplashTypes.chain:
				const r: Units.IUnit[] = [];
				let impactPoint: InterfaceTypes.IVector = target;
				let unitsCopy = this.unitsOnMap.filter(
					(u) => AttackManager.isReachableByAttack(delivery, u.movement) && selectionFn(u),
				);
				for (let i = 0; i < zone.magnitude; i++) {
					const nextUnit = this.getUnitClosestToPoint(impactPoint, unitsCopy, zone.radius);
					if (!nextUnit) {
						return r;
					}
					unitsCopy = unitsCopy.filter((u) => u.getInstanceId() !== nextUnit.getInstanceId());
					if (
						AttackManager.isLineOfSightAttack(delivery) &&
						!this.isLineOfSightAvailable(impactPoint, nextUnit.location)
					) {
						i--;
						continue;
					}
					impactPoint = nextUnit.location;
					r.push(nextUnit);
				}
				return r;
			case UnitAttack.TAoEDamageSplashTypes.cone90:
				const hitSector = Geometry.generateImpactSector(source, target, zone.radius, 90, true);
				return this.getUnitsInRange({
					...target,
					radius: zone.radius,
				})
					.filter(
						(u) =>
							(!AttackManager.isLineOfSightAttack(delivery) ||
								this.isLineOfSightAvailable(target, u.location)) &&
							AttackManager.isReachableByAttack(delivery, u.movement) &&
							selectionFn(u),
					)
					.filter((u) =>
						Geometry.isPointInsideSector(Vectors.create(target, u.location), hitSector[0], hitSector[1]),
					);
			case UnitAttack.TAoEDamageSplashTypes.cone180:
				const semiCircle = Geometry.generateImpactSector(source, target, zone.radius, 180, true);
				return this.getUnitsInRange({
					...target,
					radius: zone.radius,
				})
					.filter(
						(u) =>
							(!AttackManager.isLineOfSightAttack(delivery) ||
								this.isLineOfSightAvailable(target, u.location)) &&
							AttackManager.isReachableByAttack(delivery, u.movement) &&
							selectionFn(u),
					)
					.filter((u) =>
						Geometry.isPointInsideSector(Vectors.create(target, u.location), semiCircle[0], semiCircle[1]),
					);
			case UnitAttack.TAoEDamageSplashTypes.lineBehind:
				const area = Geometry.generateImpactRectangle(
					source,
					target,
					zone.radius,
					UNIT_MAP_SIZE_MASSIVE,
					true,
					true,
				);
				return this.unitsOnMap
					.filter(
						(u) =>
							(!AttackManager.isLineOfSightAttack(delivery) ||
								this.isLineOfSightAvailable(target, u.location)) &&
							AttackManager.isReachableByAttack(delivery, u.movement) &&
							selectionFn(u),
					)
					.filter((u) => Geometry.isPointInsidePolygon(u.location, area));
			case UnitAttack.TAoEDamageSplashTypes.lineAside:
				const line = Geometry.generateImpactRectangle(
					source,
					target,
					UNIT_MAP_SIZE_MASSIVE,
					zone.radius,
					false,
					true,
				);
				return this.unitsOnMap
					.filter(
						(u) =>
							(!AttackManager.isLineOfSightAttack(delivery) ||
								this.isLineOfSightAvailable(target, u.location)) &&
							AttackManager.isReachableByAttack(delivery, u.movement) &&
							selectionFn(u),
					)
					.filter((u) => Geometry.isPointInsidePolygon(u.location, line));
			default:
				return [];
		}
	}

	public isPointPassable(p: InterfaceTypes.IVector, m: UnitChassis.movementType): boolean {
		if (m === UnitChassis.movementType.air) return true;
		const c = GenericCombatMap.whichEntityContainsPoint(p, Object.values(this.terrainEntities));
		if (c) {
			const r = GenericCombatMap.isEntityPassable(c, m);
			if (!r) return false;
		}
		for (const c of this.getMapCells())
			if (!GenericCombatMap.isEntityPassable(c, m) && GenericCombatMap.isPointWithinMapCell(p, c)) return false;
		if (
			this.doodads
				.filter((d) => !d.passable)
				.filter((d) =>
					Geometry.isPointInRange(p, {
						x: d.x,
						y: d.y,
						radius: d.collisionRadius,
					}),
				).length > 0
		)
			return false;
		return m !== UnitChassis.movementType.naval;
	}

	public isLineOfSightAvailable(p1: InterfaceTypes.IVector, p2: InterfaceTypes.IVector, withDoodads = true): boolean {
		const losEntities = Object.values(this.terrainEntities).filter((t) => !t.lineOfSight);
		const losDoodads = withDoodads ? this.doodads.filter((t) => !t.lineOfSight) : [];
		const losZones = this.getMapCells({
			lineOfSight: false,
		});
		for (const c of losZones) if (Geometry.doesRayIntersectCircle(c, p1, p2)) return false;
		for (const d of losDoodads)
			if (
				Geometry.doesRayIntersectCircle(
					{
						x: d.x,
						y: d.y,
						radius: d.collisionRadius,
					},
					p1,
					p2,
				)
			)
				return false;
		for (const e of losEntities) if (Geometry.doesRayIntersectShape(e.shape, p1, p2)) return false;
		return true;
	}

	public isPathTraversible(
		p1: InterfaceTypes.IVector,
		p2: InterfaceTypes.IVector,
		m: UnitChassis.movementType,
	): boolean {
		if (m === UnitChassis.movementType.air) return true;
		const stopEntitites = Object.values(this.terrainEntities).filter(
			(t) => !GenericCombatMap.isEntityPassable(t, m),
		);
		const stopDoodads = this.doodads.filter((t) => !GenericCombatMap.isEntityPassable(t, m));
		const stopZones = this.getMapCells().filter((c) => !GenericCombatMap.isEntityPassable(c, m));
		for (const c of stopZones) if (Geometry.doesRayIntersectCircle(c, p1, p2)) return false;
		for (const d of stopDoodads)
			if (
				Geometry.doesRayIntersectCircle(
					{
						x: d.x,
						y: d.y,
						radius: d.collisionRadius,
					},
					p1,
					p2,
				)
			)
				return false;
		for (const e of stopEntitites) if (Geometry.doesRayIntersectShape(e.shape, p1, p2)) return false;
		if (m === UnitChassis.movementType.naval)
			return GenericCombatMap.doesWaterPathExists(
				p1,
				p2,
				Object.values(this.terrainEntities).filter((e) => !!e.water) as IMapLake[],
				this.getMapCells({ water: true }),
			); // naval units only move on water
		return true;
	}

	public getDeployedUnits(): Units.IUnit[] {
		return this.unitsOnMap || [];
	}

	public getMapCells(filter: Partial<TMapCellStaticProps> = {}): IMapCell[] {
		if (Object.keys(filter).length)
			return this.cells.filter((c) => {
				for (const k of Object.keys(filter) as Array<keyof TMapCellStaticProps>)
					if (filter[k] !== c[k]) return false;
				return true;
			});
		return this.cells;
	}

	public getUnitsInRange(area: InterfaceTypes.ISpot): Units.IUnit[] {
		return this.getDeployedUnits().filter((u) => Geometry.isPointInRange(u.location, area));
	}

	public serialize(): TSerializedCombatMap {
		return {
			...this.__serializeSerializable(),
			radius: this.radius,
			sectors: this.sectors,
			terrainEntities: this.terrainEntities,
			parameters: this.parameters,
			doodads: this.doodads,
			spinAngle: this.spinAngle,
			unitsOnMap: this.unitsOnMap.map((c) => c.serialize()),
			salvages: this.salvages.map((c) => c.serialize()),
			cells: this.cells.map((c) => c.serialize()),
		};
	}
}

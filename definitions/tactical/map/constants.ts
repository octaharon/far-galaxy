export const MAX_MAP_RADIUS = 90;
export const MIN_MAP_RADIUS = 30;
export const MAX_MAP_DOODADS = 40;
export const MIN_DOODAD_SIZE = 0.5;
export const MAX_DOODAD_SIZE = 1;
export const MAP_CENTER_RADIUS = 15;
export const MAP_ENTITY_SHAPE_DENSITY = 256;
export const DEPLOY_BEACON_RADIUS = 3;

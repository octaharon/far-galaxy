import digest from '../../../src/utils/digest';
import createRandomArray from '../../../src/utils/randomArray';
import InterfaceTypes from '../../core/InterfaceTypes';
import BasicMaths from '../../maths/BasicMaths';
import { GeometricPrecision } from '../../maths/constants';
import ContiniousNoise from '../../maths/ContiniousNoise';
import Distributions, { applyDistribution } from '../../maths/Distributions';
import Geometry from '../../maths/Geometry';
import Vectors from '../../maths/Vectors';
import {
	MAP_CENTER_RADIUS,
	MAP_ENTITY_SHAPE_DENSITY,
	MAX_DOODAD_SIZE,
	MAX_MAP_DOODADS,
	MAX_MAP_RADIUS,
	MIN_DOODAD_SIZE,
	MIN_MAP_RADIUS,
} from './constants';
import GenericCombatMap from './GenericCombatMap';
import { defaultMapParameters, TCombatMapProps, TMapEntities, TMapParameters } from './index';
import {
	GEODESIC_PARABOLIC,
	GEODESIC_RIDGE,
	GeodesicElevationFunctions,
	IMapCave,
	IMapHill,
	IMapLake,
	TMapDoodad,
	TMapElevationParameters,
	TMapTerrainEntity,
} from './MapObstacles';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const noise3d = require('noise3d');
let _instance: MapGenerator = null;

function shuffle(a: number[]) {
	let j;
	let x;
	let i;
	for (i = a.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		x = a[i];
		a[i] = a[j];
		a[j] = x;
	}
	return a;
}

export default class MapGenerator {
	protected constructor(public parameters: TMapParameters) {}

	public get radius() {
		return BasicMaths.lerp({
			min: MIN_MAP_RADIUS,
			max: MAX_MAP_RADIUS,
			x: this.parameters.size,
		});
	}

	public get sectors() {
		switch (true) {
			case this.parameters.size === 0: // 2 players
				return 4;
			case this.parameters.size <= 0.2: // 3 players
				return 5;
			case this.parameters.size <= 0.4: // 4 players
				return 6;
			case this.parameters.size <= 0.8: // 5 players
				return 7;
			default:
				// 6 or more players
				return 8;
		}
	}

	public get static() {
		return this.constructor as typeof MapGenerator;
	}

	protected get lakeDistanceBounds(): IBounds {
		const c =
			((this.parameters.centralZone ? MAP_CENTER_RADIUS / MAX_MAP_RADIUS : 0.1) + this.parameters.waterPresence) *
			this.radius;
		return {
			min: c,
			max: c + Math.random() * (0.4 + this.parameters.waterPresence) * this.radius,
		};
	}

	protected get caveDistanceBounds(): IBounds {
		return {
			min: this.parameters.centralZone ? MAP_CENTER_RADIUS + 10 : 0,
			max: this.radius,
		};
	}

	protected get hillDistanceBounds(): IBounds {
		const minR = this.parameters.centralZone ? MAP_CENTER_RADIUS : 5;
		const maxR = this.radius;
		return {
			min: minR,
			max: maxR,
		};
	}

	public static generate(p: TMapParameters = defaultMapParameters) {
		const gen = MapGenerator.i(p);
		return gen.generateMap();
	}

	public static i(p: TMapParameters) {
		if (!_instance) _instance = new MapGenerator(p);
		else _instance.setParameters(p);
		return _instance;
	}

	public generateHill(
		azimuth = 0,
		distance = 10,
		size = 5,
		density: number = MAP_ENTITY_SHAPE_DENSITY / 2,
	): IMapHill {
		const randomParameters = {
			skew: Distributions.DistributionOptions.Bell(Math.random()) * 1.75 + 0.25,
			elevation: Distributions.DistributionOptions.Parabolic(Math.random()) * 0.6 + 0.15, // 0.1 to 0.7
			noiseScale: Math.random() * 45 + 15,
		};

		const maxR = size;
		const minR = maxR / (1.1 + Math.sqrt(randomParameters.skew));
		const center = Geometry.polar2vector({
			r: distance,
			alpha: azimuth,
		});
		// console.log(randomParameters, { minR, maxR });
		const noiseScaleFactor = 1 / 2000;
		const noiseGen = new ContiniousNoise(1, noiseScaleFactor * randomParameters.noiseScale);
		const curvatureFunction = (x: number) => noiseGen.getVal(x);
		const smoothingDistance = Math.round(density * 0.1);
		let curvatureArray = new Float64Array(density + smoothingDistance)
			.fill(0)
			.map((v, ix) => curvatureFunction(ix));
		const maxCurvature = Math.max(...curvatureArray);
		const minCurvature = Math.min(...curvatureArray);
		curvatureArray = curvatureArray.map((x) =>
			BasicMaths.normalizeValue(
				{
					x,
					min: minCurvature,
					max: maxCurvature,
				},
				2,
			),
		); // 0 .. 1

		/* Blending curvature to make it smooth */
		curvatureArray
			.slice(-smoothingDistance)
			.forEach(
				(v, ix) =>
					(curvatureArray[ix] =
						(curvatureArray[ix] * ix) / (smoothingDistance - 1) + v * (1 - ix / (smoothingDistance - 1))),
			);
		curvatureArray = curvatureArray.slice(0, density);

		/* Rolling elevation */
		const elevationFn =
			Object.keys(GeodesicElevationFunctions)[
				Math.floor(Math.random() * Object.values(GeodesicElevationFunctions).length)
			];
		const elevationParameters = {
			fn: elevationFn,
			maxElevation: randomParameters.elevation,
			parameters: createRandomArray(5),
		};

		/* Building geodesic lines */
		const radiuses = new Float64Array(curvatureArray.length).fill(0).map((v, t) => {
			return curvatureArray[t] * (maxR - minR) + minR;
		});
		// rotating array
		let radiusArray = Array.from(radiuses);
		const rotationOffset = Math.round(Math.random() * (radiusArray.length - 1));
		radiusArray = radiusArray.slice(rotationOffset).concat(radiusArray.slice(0, rotationOffset));
		return {
			entityId: `hill-${digest({ ...radiuses, ...center })}`,
			passable: false,
			lineOfSight: false,
			water: false,
			shape: {
				radiusArray,
				center,
			},
			elevation: elevationParameters as TMapElevationParameters,
		};
	}

	public generateLake(azimuth = 0, density: number = MAP_ENTITY_SHAPE_DENSITY): IMapLake {
		const randomParameters = {
			skew: applyDistribution({
				x: Math.random(),
				min: 1 - this.parameters.terrainDensity / 2,
				max: 1 + (1 - this.parameters.waterPresence) * 0.7,
				distribution: 'Gaussian',
			}),
			noiseScale: Math.random() * 4.5 * (1 + Math.pow(this.parameters.terrainDensity, 0.5)) + 3.5,
			aspect: applyDistribution({
				x: Math.random(),
				min: 0.5,
				max: 1.5,
				distribution: 'Bell',
			}),
		};

		const distance = applyDistribution({
			distribution: 'Bell',
			...this.lakeDistanceBounds,
			x: Math.random(),
		});

		const center = Geometry.polar2vector({
			r: distance,
			alpha: azimuth,
		});

		const lakeRadius =
			distance -
			this.radius +
			this.radius *
				(0.5 +
					BasicMaths.skewedNormalRandom(
						0.25,
						0.5 + Math.pow(1 - this.parameters.waterPresence, 2) / 5,
						randomParameters.skew,
					));

		const maxR = lakeRadius;
		const minR = lakeRadius * 0.65 + 0.12 * (2 - this.parameters.terrainDensity);

		const perlin = noise3d.createPerlin({
			interpolation: noise3d.interpolation.cosine,
			permutation: noise3d.array.shuffle(noise3d.array.range(0, 255), Math.random),
		});

		const brownian = noise3d.createBrownianMotion({
			octaves: 2,
			persistence: 0.4,
			noise: perlin,
		});
		const curvatureFunction = (x: number) =>
			brownian((x / density) * randomParameters.noiseScale, randomParameters.skew, 1) / 2 + 0.5;

		// const curvatureFunction = (x: number) => noiseGen.getVal(x);
		const smoothingDistance = Math.round(density * 0.25);
		let curvatureArray = new Float64Array(density + smoothingDistance)
			.fill(0)
			.map((v, ix) => curvatureFunction(ix));
		const maxCurvature = Math.max(...curvatureArray);
		const minCurvature = Math.min(...curvatureArray);
		curvatureArray = curvatureArray.map((x) =>
			BasicMaths.normalizeValue(
				{
					x,
					min: minCurvature,
					max: maxCurvature,
				},
				0.5,
			),
		); // 0 .. 1

		/* Blending curvature to make it smooth */
		curvatureArray.slice(-smoothingDistance).forEach(
			(v, ix) =>
				(curvatureArray[ix] = Vectors.module(2)({
					x: (curvatureArray[ix] * ix) / (smoothingDistance - 1),
					y: v * (1 - ix / (smoothingDistance - 1)),
				})),
		);
		curvatureArray = curvatureArray.slice(0, density);

		/* Rolling elevation */
		const elevationParameters = {
			fn: GEODESIC_RIDGE,
			maxElevation: this.parameters.terrainDensity * 0.8 + 0.2,
			parameters: createRandomArray(5),
		} as TMapElevationParameters;

		const toEllipse = (t: number) =>
			randomParameters.aspect /
			Math.sqrt(Math.pow(Math.sin(t), 2) + Math.pow(randomParameters.aspect * Math.cos(t), 2));

		/* Building geodesic lines */
		const radiuses = new Float64Array(curvatureArray.length).fill(0).map((v, t) => {
			return curvatureArray[t] * (maxR - minR) * toEllipse((t / curvatureArray.length) * 2 * Math.PI) + minR;
		});
		// rotating array
		let radiusArray = Array.from(radiuses);
		const rotationOffset = Math.round(Math.random() * (radiusArray.length - 1));
		radiusArray = radiusArray.slice(rotationOffset).concat(radiusArray.slice(0, rotationOffset));
		return {
			entityId: `lake-${digest({ ...radiuses, ...center })}`,
			passable: true,
			lineOfSight: true,
			water: true,
			shape: {
				radiusArray,
				center,
			},
			elevation: elevationParameters,
		};
	}

	public generateCave(azimuth = 0, distance = 10, size = 5, density: number = MAP_ENTITY_SHAPE_DENSITY): IMapCave {
		const randomParameters = {
			aspect: Distributions.DistributionOptions.Parabolic(Math.random()) * 0.5 + 0.35,
			elevation: applyDistribution({
				x: Math.random(),
				min: 0.4,
				max: 1,
				distribution: 'Uniform',
			}), // 0.1 to 0.7
			noiseScale: applyDistribution({
				x: Math.random(),
				min: 16,
				max: 64,
				distribution: 'Gaussian',
			}),
		};

		const maxR = size;
		const minR = size * (0.5 - Math.random() * 0.3);
		const center = Geometry.polar2vector({
			r: distance,
			alpha: azimuth,
		});
		// console.log(randomParameters, { minR, maxR });
		const perlin = noise3d.createPerlin({
			interpolation: noise3d.interpolation.linear,
			permutation: noise3d.array.shuffle(noise3d.array.range(0, 255), Math.random),
		});

		const brownian = noise3d.createBrownianMotion({
			octaves: 4,
			persistence: 0.5,
			noise: perlin,
		});
		const curvatureFunction = (x: number) =>
			brownian(x / randomParameters.noiseScale, randomParameters.elevation, 1) / 2 + 0.5;
		const smoothingDistance = Math.round(density * 0.2);
		let curvatureArray = new Float64Array(density + smoothingDistance)
			.fill(0)
			.map((v, ix) => curvatureFunction(ix));
		const maxCurvature = Math.max(...curvatureArray);
		const minCurvature = Math.min(...curvatureArray);
		curvatureArray = curvatureArray.map((x) =>
			BasicMaths.normalizeValue(
				{
					x,
					min: minCurvature,
					max: maxCurvature,
				},
				1,
			),
		); // 0 .. 1

		/* Blending curvature to make it smooth */
		curvatureArray.slice(-smoothingDistance).forEach(
			(v, ix) =>
				(curvatureArray[ix] = Vectors.module(1)({
					x: (curvatureArray[ix] * ix) / (smoothingDistance - 1),
					y: v * (1 - ix / (smoothingDistance - 1)),
				})),
		);
		curvatureArray = curvatureArray.slice(0, density);

		/* Rolling elevation */
		const elevationParameters = {
			fn: GEODESIC_PARABOLIC,
			maxElevation: randomParameters.elevation,
			parameters: createRandomArray(5),
		} as TMapElevationParameters;

		/* Building geodesic lines */
		const radiuses = new Float64Array(curvatureArray.length).fill(0).map((v, x) => {
			return (
				((t) =>
					randomParameters.aspect /
					Math.sqrt(Math.pow(Math.sin(t), 2) + Math.pow(randomParameters.aspect * Math.cos(t), 2)))(
					((2 * Math.PI) / curvatureArray.length) * x,
				) *
					curvatureArray[x] *
					(maxR - minR) +
				minR
			);
		});
		// rotating array
		let radiusArray = Array.from(radiuses);
		const rotationOffset = Math.round(Math.random() * (radiusArray.length - 1));
		radiusArray = radiusArray.slice(rotationOffset).concat(radiusArray.slice(0, rotationOffset));
		return {
			entityId: `cave-${digest({ ...radiuses, ...center })}`,
			passable: false,
			lineOfSight: true,
			water: false,
			shape: {
				radiusArray,
				center,
			},
			elevation: elevationParameters,
		};
	}

	public clipEntityToCentralRing(
		e: TMapTerrainEntity,
		radius: number = this.parameters.centralZone ? 15 : 0,
	): TMapTerrainEntity {
		e.shape.radiusArray.forEach((r, ix) => {
			const p = Vectors.add(
				e.shape.center,
				Geometry.polar2vector({
					r,
					alpha: BasicMaths.lerp({
						x: ix / (e.shape.radiusArray.length - 1),
						min: 0,
						max: Math.PI * 2,
					}),
				}),
			);
			const boundaryPoint = Geometry.vector2polar(p);
			const crossingPoints = Geometry.getRayUnitCircleIntersection(radius, p, e.shape.center);
			if (boundaryPoint.r < radius && crossingPoints.length) {
				const r1 = Vectors.module()(Vectors.add(Vectors.inverse(crossingPoints[0]), e.shape.center));
				const r2 =
					crossingPoints.length > 1
						? Vectors.module()(Vectors.add(Vectors.inverse(crossingPoints[1]), e.shape.center))
						: r1;
				e.shape.radiusArray[ix] = Math.min(r1, r2);
			}
		});
		return e;
	}

	protected setParameters(p: TMapParameters) {
		this.parameters = p;
	}

	protected generateLakes(): IMapLake[] {
		const amount = Math.floor(
			2 * this.parameters.waterPresence + 4 * this.parameters.waterPresence * this.parameters.terrainDensity,
		);
		if (!amount) return [];

		const angleStep = (Math.PI * 2) / Math.max(amount, 3);
		const angles = new Array(amount).fill(0).map((v, ix) => ix * angleStep + Math.random() * angleStep);
		shuffle(angles);
		return angles.map((v) => this.generateLake(v));
	}

	protected getCaveDistanceArray(amount: number): number[] {
		return createRandomArray(amount).map((x) =>
			applyDistribution({
				distribution: 'Uniform',
				...this.caveDistanceBounds,
				x,
			}),
		);
	}

	protected generateCaves(): IMapCave[] {
		const amount = Math.round(
			3 *
				(1 - this.parameters.flatness) *
				(Math.pow(this.parameters.terrainDensity, 0.5) + Math.pow(1 - this.parameters.flatness, 0.5)),
		);
		if (!amount) return [];
		const rs = this.getCaveDistanceArray(amount);
		const sizes = new Array(amount)
			.fill(0)
			.map(() =>
				BasicMaths.skewedNormalRandom(
					0.5 + 3 * this.parameters.terrainDensity,
					7 + 5 * this.parameters.terrainDensity,
					0.8 + Math.sqrt(this.parameters.flatness) / 2,
				),
			);

		const angleStep = (Math.PI * 2) / rs.length;
		const angles = rs.map((v, ix) => (ix - 2) * angleStep + Math.random() * angleStep * 4);
		shuffle(angles);
		shuffle(sizes);
		return rs.map((v, ix) => {
			const c = this.generateCave(angles[ix], v + Math.random() * 2 - 1, sizes[ix]);
			c.elevation.maxElevation *= 1 - this.parameters.flatness / 2;
			return c;
		});
	}

	protected getHillDistanceArray(amount: number): number[] {
		return new Array(amount).fill(null).map((x) =>
			applyDistribution({
				distribution: 'Bell',
				...this.hillDistanceBounds,
			}),
		);
	}

	protected generateHills(): IMapHill[] {
		const amount = Math.round(
			16 *
				Math.sqrt(1 - Math.pow(this.parameters.flatness, 2)) *
				Math.sqrt(
					Math.pow(this.parameters.terrainDensity, 2) +
						Math.sqrt(1 + Math.pow(Math.sin(this.parameters.flatness * Math.PI + Math.PI / 2), 2)) / 4,
				),
		);
		if (!amount) return [];

		const rs = this.getHillDistanceArray(amount);
		const sizes = new Array(amount)
			.fill(0)
			.map(() => BasicMaths.skewedNormalRandom(1, this.radius / 4, 0.7 + this.parameters.flatness));

		const angleStep = (Math.PI * 2) / rs.length;
		const angles = rs.map((v, ix) => (ix - 1) * angleStep + Math.random() * angleStep * 2);
		shuffle(angles);
		shuffle(sizes);
		return rs.map((v, ix) => {
			const c = this.generateHill(angles[ix], v + Math.random() * 2 - 1, sizes[ix]);
			c.elevation.maxElevation *= 1 - this.parameters.flatness;
			return c;
		});
	}

	protected clipEntityToMapBounds(e: TMapTerrainEntity): TMapTerrainEntity {
		e.shape.radiusArray.forEach((r, ix) => {
			const p = Vectors.add(
				e.shape.center,
				Geometry.polar2vector({
					r,
					alpha: BasicMaths.lerp({
						x: ix / (e.shape.radiusArray.length - 1),
						min: 0,
						max: Math.PI * 2,
					}),
				}),
			);
			const boundaryPoint = Geometry.vector2polar(p);
			const crossingPoints = Geometry.getRayUnitCircleIntersection(this.radius, p, e.shape.center);
			if (boundaryPoint.r > this.radius && crossingPoints.length) {
				const r1 = Vectors.module()(Vectors.add(crossingPoints[0], Vectors.inverse(e.shape.center)));
				const r2 =
					crossingPoints.length > 1
						? Vectors.module()(Vectors.add(crossingPoints[1], Vectors.inverse(e.shape.center)))
						: r1;
				if (r1 <= r2) e.shape.radiusArray[ix] = Geometry.vector2polar(crossingPoints[0]).r;
				else e.shape.radiusArray[ix] = Geometry.vector2polar(crossingPoints[1]).r;
			}
		});
		return e;
	}

	protected generateDoodads(
		terrainEntities: TMapTerrainEntity[],
		amount: number = Math.round(MAX_MAP_DOODADS * this.parameters.terrainDensity),
	): TMapDoodad[] {
		const r = [] as TMapDoodad[];
		for (let i = 0; i < amount; i++) {
			const c = {} as InterfaceTypes.IPolar;
			let cv = {} as InterfaceTypes.IVector;
			let ix = 10;
			let collision = false;
			do {
				c.alpha = 2 * Math.PI * Math.random();
				c.r = applyDistribution({
					x: Math.random(),
					min: this.parameters.centralZone ? MAP_CENTER_RADIUS + GeometricPrecision : 2,
					max: this.radius - 1,
					distribution: this.parameters.centralZone ? 'Minimal' : 'Uniform',
				});
				cv = Geometry.polar2vector(c);
			} while (
				ix-- &&
				(collision =
					GenericCombatMap.isPointWithinTerrainEntities(cv, terrainEntities, {}, 0.1) ||
					r.reduce((a, doodad) => a || GenericCombatMap.isPointWithinDoodad(cv, doodad, 0.5), false))
			);
			if (!collision)
				r.push({
					...Geometry.polar2vector(c),
					doodadId: digest(c, true),
					passable: false,
					water: false,
					lineOfSight: Math.random() > 0.75,
					collisionRadius: applyDistribution({
						min: MIN_DOODAD_SIZE,
						max: MAX_DOODAD_SIZE,
						x: Math.random(),
						distribution: 'Parabolic',
					}),
				} as TMapDoodad);
		}
		return r;
	}

	protected generateMap() {
		const entities = {} as TMapEntities;
		this.generateLakes().forEach(
			(lake) =>
				(entities[lake.entityId] = this.parameters.centralZone ? this.clipEntityToCentralRing(lake) : lake),
		);
		this.generateCaves().forEach((cave) => {
			let ix = 10;
			while (
				ix-- &&
				Object.values(entities).reduce(
					(r, e) =>
						r ||
						(e.water &&
							GenericCombatMap.isPointWithinEntityBounds(
								cave.shape.center,
								e,
								Math.max(...cave.shape.radiusArray),
							)) ||
						(!e.passable && e.lineOfSight && Geometry.isPointInsideShape(cave.shape.center, e.shape)),
					false,
				)
			) {
				// resample cave so it's not in a lake
				cave.shape.center = Geometry.polar2vector({
					r: this.getCaveDistanceArray(5)[0],
					alpha: Math.PI * 2 * Math.random(),
				});
			}
			if (
				Object.values(entities)
					.filter((v) => v.water)
					.reduce(
						(r, e) =>
							r ||
							GenericCombatMap.isPointWithinEntityBounds(
								cave.shape.center,
								e,
								Math.max(...cave.shape.radiusArray),
							) ||
							(!e.passable &&
								e.lineOfSight &&
								GenericCombatMap.isPointWithinEntityBounds(cave.shape.center, e)),
						false,
					)
			)
				return;
			entities[cave.entityId] = cave;
		});
		this.generateHills().forEach((hill) => {
			let ix = 10;
			while (
				ix-- &&
				Object.values(entities)
					.filter((v) => !v.water)
					.reduce(
						(r, e) =>
							r ||
							(e.lineOfSight &&
								GenericCombatMap.isPointWithinEntityBounds(
									hill.shape.center,
									e,
									Math.max(...hill.shape.radiusArray),
								)) ||
							(!e.lineOfSight && GenericCombatMap.isPointWithinEntityBounds(hill.shape.center, e)),
						false,
					)
			) {
				// resample hill location so it does not cross a cave
				hill.shape.center = Geometry.polar2vector({
					r: this.getHillDistanceArray(5)[0],
					alpha: Math.PI * 2 * Math.random(),
				});
			}
			if (
				Object.values(entities)
					.filter((v) => !v.water)
					.reduce(
						(r, e) =>
							r ||
							(e.lineOfSight &&
								GenericCombatMap.isPointWithinEntityBounds(
									hill.shape.center,
									e,
									Math.max(...hill.shape.radiusArray),
								)) ||
							(!e.lineOfSight && GenericCombatMap.isPointWithinEntityBounds(hill.shape.center, e)),
						false,
					)
			)
				return;
			entities[hill.entityId] = hill;
		});
		const doodads = this.generateDoodads(Object.values(entities));
		return new GenericCombatMap({
			radius: this.radius,
			sectors: this.sectors,
			terrainEntities: entities,
			doodads,
			parameters: this.parameters,
		} as TCombatMapProps);
	}
}

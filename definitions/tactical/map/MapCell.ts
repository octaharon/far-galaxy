import { DIEntityDescriptors, DIInjectableCollectibles, DIInjectableSerializables } from '../../core/DI/injections';
import PlayerProperty from '../../player/PlayerProperty';
import EffectEvents from '../statuses/effectEvents';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import { IStatusEffect, TEffectApplianceItem } from '../statuses/types';
import { IMapCell, TMapCellProps, TSerializedMapCell } from './index';

export class GenericMapCell extends PlayerProperty<TMapCellProps, TSerializedMapCell> implements IMapCell {
	public x: number;
	public y: number;
	public elevation: number;
	public radius: number;
	public effects: Array<IStatusEffect<StatusEffectMeta.effectAgentTypes.map>>;
	public passable: boolean;
	public deploy: boolean;
	public water: boolean;
	public lineOfSight: boolean;
	public scan: boolean;
	public readonly effectTargetType = StatusEffectMeta.effectAgentTypes.map;

	protected get typeDescriptor() {
		return DIEntityDescriptors[DIInjectableSerializables.mapcell];
	}

	public applyEffect(eff: TEffectApplianceItem, source: StatusEffectMeta.TStatusEffectTarget = this): this {
		if (this.getProvider(DIInjectableCollectibles.effect).get(eff.effectId).targetType !== this.effectTargetType)
			return null;
		const newEffect = this.getProvider(
			DIInjectableCollectibles.effect,
		).instantiate<StatusEffectMeta.effectAgentTypes.map>(eff.props, eff.effectId, source, this);
		return this.addEffect(newEffect);
	}

	public addEffect(effect: IStatusEffect<StatusEffectMeta.effectAgentTypes.map>) {
		if (effect.static.targetType !== StatusEffectMeta.effectAgentTypes.map) return this;
		this.effects.push(effect);
		effect.triggerEvent(EffectEvents.TMapEffectEvents.apply);
		return this;
	}

	public hasEffect(effectId: number): boolean {
		return this.effects.filter((ef) => ef.static.id === effectId).length > 0;
	}

	public removeEffect(effectId: string) {
		const removedEffect = this.effects.find((v) => v.getInstanceId() === effectId);
		if (removedEffect && removedEffect.static.unremovable) return this;
		removedEffect.triggerEvent(EffectEvents.TMapEffectEvents.expire);
		this.effects = this.effects.filter((v) => v.getInstanceId() === effectId);
		return this;
	}

	public getStatusEffects(): Array<IStatusEffect<StatusEffectMeta.effectAgentTypes.map>> {
		return this.effects;
	}

	public callEffectEvent<T extends EffectEvents.TMapEffectEvents>(
		payload?: EffectEvents.TEffectEventMeta<StatusEffectMeta.effectAgentTypes.map, T>,
	) {
		this.getStatusEffects()
			.sort((a, b) => a.priority - b.priority)
			.forEach((eff) => {
				eff.triggerEvent(payload.event, payload);
			});
		return this;
	}

	public serialize(): TSerializedMapCell {
		return {
			...this.__serializePlayerProperty(),
			x: this.x,
			y: this.y,
			radius: this.radius,
			elevation: this.elevation,
			effects: this.effects.map((e) => e.serialize()),
			passable: this.passable,
			deploy: this.deploy,
			lineOfSight: this.lineOfSight,
			scan: this.scan,
			water: this.water,
		};
	}
}

export type TMapCell = GenericMapCell;

export default GenericMapCell;

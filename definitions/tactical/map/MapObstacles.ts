import InterfaceTypes from '../../core/InterfaceTypes';

const CurvatureRootsRangeMin = -2;
const CurvatureRootsRangeMax = 2;

export type TMapElevationFunction = (p: InterfaceTypes.IPolar, maxElevation: number, parameters: number[]) => number;

export const GEODESIC_PARABOLIC = 'parabolic';
export const GEODESIC_VOLCANO = 'volcano';
export const GEODESIC_PIKE = 'pike';
export const GEODESIC_RIDGE = 'ridge';
export const GEODESIC_PLATEAU = 'plateau';
export const GeodesicElevationFunctions = {
	[GEODESIC_PARABOLIC]: (f: number) => f,
	[GEODESIC_VOLCANO]: (f: number) => f,
	[GEODESIC_PIKE]: (f: number) => f,
	[GEODESIC_RIDGE]: (f: number) => f,
	[GEODESIC_PLATEAU]: (f: number) => f,
};
export type TMapGeodesicElevationFunctions = keyof typeof GeodesicElevationFunctions;

export type TMapElevationParameters = {
	fn: TMapGeodesicElevationFunctions;
	maxElevation: number;
	parameters: number[];
};

export interface IEntityCollisionDecorator {
	passable: boolean; // true means traversable
	lineOfSight: boolean; // true means no line of sight detection
}

export interface IEntityWaterDecorator {
	water: boolean;
}

export type ITerrainEntityDecorator = IEntityCollisionDecorator & IEntityWaterDecorator;

export type TMapTerrainEntity = ITerrainEntityDecorator & {
	shape: InterfaceTypes.TPolarShape<InterfaceTypes.IVector>;
	elevation: TMapElevationParameters;
	entityId: string;
};

export interface IMapHill extends TMapTerrainEntity {
	passable: false;
	water: false;
	lineOfSight: false;
}

export interface IMapCave extends TMapTerrainEntity {
	passable: false;
	water: false;
	lineOfSight: true;
}

export interface IMapLake extends TMapTerrainEntity {
	passable: true;
	water: true;
	lineOfSight: true;
}

export type TMapDoodad = InterfaceTypes.IVector &
	IEntityCollisionDecorator & {
		water: false;
		collisionRadius: number;
		doodadId: string;
	};

export function isLakeEntity(e: TMapTerrainEntity): e is IMapLake {
	return !!e.passable && !!e.water && !!e.lineOfSight;
}

export function isHillEntity(e: TMapTerrainEntity): e is IMapHill {
	return !e.passable && !e.water && !e.lineOfSight;
}

export function isCaveEntity(e: TMapTerrainEntity): e is IMapCave {
	return !e.passable && !e.water && !e.lineOfSight;
}

/**
 * Map generator parameters, all values 0..1
 */
import InterfaceTypes from '../../core/InterfaceTypes';
import { ISerializable, ISerializableFactory, TSerializable } from '../../core/Serializable';
import { IPlayerBase } from '../../orbital/base/types';
import { TPlayerId } from '../../player/playerId';
import { IPlayerProperty, IPlayerPropertyProps } from '../../player/types';
import { TPlanetSurfaceProps } from '../../space/Planets';
import UnitChassis from '../../technologies/types/chassis';
import UnitAttack from '../damage/attack';
import Salvage from '../salvage/types';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import Units from '../units/types';
import { IMapLake, ITerrainEntityDecorator, TMapDoodad, TMapTerrainEntity } from './MapObstacles';
import IUnit = Units.IUnit;

export type TCombatMapId = string;
export type TMapParameters = TPlanetSurfaceProps & {
	flatness: number; // elevation and amount of hills
	terrainDensity: number; // 0 - 1, amount of entities
	size: number; // 0=40, 1=100
	resourceFields: number; // 2-5
	centralZone: boolean;
};
export const defaultMapParameters: TMapParameters = {
	warpFactor: 0.5,
	spinFactor: 0.5,
	gravity: 0.5,
	atmosphericDensity: 0.5,
	radiationLevel: 0,
	size: 0,
	magneticField: 0.5,
	terrainDensity: 0.5,
	resourceFields: 4,
	centralZone: true,
	flatness: 0.5,
	waterPresence: 0.3,
	temperature: 0.5,
	atmosphericColorHex: 0xfffcda,
	surfaceColorHex: 0x554422,
};
export type TCombatMapProps = TCombatMapStaticProps & {
	cells: IMapCell[];
	unitsOnMap: Units.IUnit[];
	salvages: Salvage.ISalvage[];
};
export type TSerializedCombatMap = TCombatMapStaticProps &
	IWithInstanceID & {
		cells: TSerializedMapCell[];
		unitsOnMap: Units.TSerializedUnit[];
		salvages: Salvage.TSerializedSalvage[];
	};
export type TMapEntities = InterfaceTypes.TDictionary<TMapTerrainEntity>;
export type TCombatMapStaticProps = {
	radius: number;
	sectors: number;
	spinAngle: number;
	parameters: TMapParameters;
	terrainEntities: TMapEntities;
	doodads: TMapDoodad[];
};

export interface ICombatMapStatic extends TSerializable<ICombatMap> {
	isEntityPassable<T extends ITerrainEntityDecorator>(t: T, m: UnitChassis.movementType): boolean;

	isPointWithinEntityBounds(x: InterfaceTypes.IVector, t: TMapTerrainEntity, within: number): boolean;

	isPointWithinDoodad(x: InterfaceTypes.IVector, d: TMapDoodad, within: 0): boolean;

	isPointWithinTerrainEntities(
		x: InterfaceTypes.IVector,
		entities: TMapTerrainEntity[],
		entityFlags?: Partial<ITerrainEntityDecorator>,
		within?: number,
	): boolean;

	whichEntityContainsPoint<T extends TMapTerrainEntity>(x: InterfaceTypes.IVector, entities: T[]): T;

	doesWaterPathExists<T extends TMapTerrainEntity & IMapLake>(
		p1: InterfaceTypes.IVector,
		p2: InterfaceTypes.IVector,
		waterEntities: T[],
	): boolean;
}

export interface ICombatMap extends TCombatMapProps, ISerializable<TCombatMapProps, TSerializedCombatMap> {
	static: ICombatMapStatic;

	getDeployedUnits(): Units.IUnit[];

	getUnitsInRange(area: InterfaceTypes.ISpot): Units.IUnit[];

	getScannableArea(player: TPlayerId): InterfaceTypes.ISpot[];

	getMapCells(filter?: Partial<TMapCellStaticProps>): IMapCell[];

	isPointPassable(p: InterfaceTypes.IVector, m: UnitChassis.movementType): boolean;

	isLineOfSightAvailable(p1: InterfaceTypes.IVector, p2: InterfaceTypes.IVector, withDoodads?: boolean): boolean;

	isPathTraversible(p1: InterfaceTypes.IVector, p2: InterfaceTypes.IVector, m: UnitChassis.movementType): boolean;

	isShotPossible(
		attack: UnitAttack.IAttackInterface,
		source: Units.IUnit,
		target: Units.IUnit | InterfaceTypes.IVector,
	): boolean;

	deployUnit(unit: IUnit, location: InterfaceTypes.IVector): boolean;

	deployUnitFromBase(base: IPlayerBase, unitBayIndex: number, location: InterfaceTypes.IVector): boolean;

	removeUnit(unit: Units.IUnit): this;

	putSalvage(salvage: Salvage.ISalvage, location: InterfaceTypes.IVector): this;

	applyAoEAttack(
		location: InterfaceTypes.IVector,
		attack: UnitAttack.TAttackAOEUnit,
		source?: StatusEffectMeta.TStatusEffectTarget,
		excludeUnit?: Units.IUnit,
	): this;

	getUnitsInAoEZone(
		zone: UnitAttack.IAreaOfEffectAttackDecorator,
		source: InterfaceTypes.IVector,
		target: InterfaceTypes.IVector,
		delivery: UnitAttack.deliveryType,
	): Units.IUnit[];

	getUnitClosestToPoint(point: InterfaceTypes.IVector, units?: Units.IUnit[], within?: number): Units.IUnit;
}

export type TCellId = string;
export type TMapCellStaticProps = IPlayerPropertyProps &
	InterfaceTypes.ISpot &
	ITerrainEntityDecorator & {
		scan: boolean;
		deploy: boolean;
		elevation: number;
	};

export type ILocationOnMap = InterfaceTypes.ISpot & {
	elevation?: number;
	mapId: string;
};

export type TMapCellDynamicProps = StatusEffectMeta.TEffectOwner<StatusEffectMeta.effectAgentTypes.map> & {};

export type TMapCellProps = TMapCellStaticProps & TMapCellDynamicProps;

export type TSerializedMapCell = TMapCellStaticProps &
	StatusEffectMeta.TSerializedEffectOwner<StatusEffectMeta.effectAgentTypes.map>;

export interface IMapCell
	extends TMapCellProps,
		IPlayerProperty,
		StatusEffectMeta.IStatusEffectTarget<
			TMapCellProps,
			TSerializedMapCell,
			StatusEffectMeta.effectAgentTypes.map
		> {}

export type IMapCellFactory = ISerializableFactory<TMapCellProps, TSerializedMapCell, IMapCell>;

export type ICombatMapFactory = ISerializableFactory<TCombatMapProps, TSerializedCombatMap, ICombatMap>;

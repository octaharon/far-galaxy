// Ticks per turn
export const COMBAT_MIN_ATTACKS_PER_TURN = 1;
export const COMBAT_MAX_ATTACKS_PER_TURN = 18;
export const COMBAT_TICKS_PER_TURN = 120;

// Dodge stacking
export const DODGE_IMMUNITY_THRESHOLD = 4;
export const MOVEMENT_DODGE_MODIFIER = 0.25; // moving at 1 speed perpendicular to firing range of 1
export const MOVEMENT_DODGE_BONUS_CAP = 1;

// Attack priorities
export const ATTACK_PRIORITIES = {
	HIGHEST: -3,
	HIGHER: -2,
	HIGH: -1,
	NORMAL: 0,
	LOW: 1,
	LOWER: 2,
	LOWEST: 3,
};

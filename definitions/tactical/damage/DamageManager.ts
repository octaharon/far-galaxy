import _ from 'underscore';
import { cloneObject } from '../../core/dataTransformTools';
import $ from '../../core/InterfaceTypes';
import BasicMaths from '../../maths/BasicMaths';
import Distributions, { applyDistribution } from '../../maths/Distributions';
import Series from '../../maths/Series';
import Damage from './damage';

class DamageMethods {
	public applyDamageModifierToDamageDefinition<K extends Damage.TDamageDefinition>(
		dd: K,
		...m: Damage.IDamageModifier[]
	): K {
		const t = [].concat(m) as Damage.IDamageModifier[];
		const newD = cloneObject(dd) as K;
		Damage.TDamageTypesArray.forEach((k: Damage.damageType) => {
			if (!newD[k]) return false;
			newD[k] = this.applyDamageModifierToDamageFactor(newD[k], ...t);
		});
		return newD;
	}

	public applyDamageModifierToDamageUnit<K extends Damage.TDamageUnit>(dd: K, ...m: Damage.IDamageModifier[]): K {
		return {
			...dd,
			..._.mapObject(this.extractDamageGeneric<number>(dd), (dmg) => BasicMaths.applyModifier(dmg, ...m)),
		};
	}

	public applyTypedDamageModifierToDamageUnit<K extends Damage.TDamageUnit>(
		dd: K,
		type: Damage.damageType,
		...m: Damage.IDamageModifier[]
	): K {
		return {
			...dd,
			..._.mapObject(this.extractDamageGeneric<number>(dd), (dmg, dt) =>
				type === dt ? BasicMaths.applyModifier(dmg, ...m) : dmg,
			),
		};
	}

	public applyDamageModifierToDamageFactor<K extends Damage.IDamageFactor>(d: K, ...m: Damage.IDamageModifier[]): K {
		const t = [].concat(m) as Damage.IDamageModifier[];
		const newD = cloneObject(d) as K;
		newD.min = BasicMaths.applyModifier(newD.min, ...t);
		newD.max = BasicMaths.applyModifier(newD.max, ...t);
		const dist = _.pluck(t, 'distribution').filter((v) => !!v);
		if (dist.length) newD.distribution = dist[dist.length - 1];
		return newD;
	}

	public getDamageValue(d: Damage.IDamageFactor): number {
		if (!d.distribution || !Object.keys(Distributions.DistributionOptions).includes(d.distribution)) {
			throw new Error('Invalid distribution: ' + d.distribution);
		}
		return applyDistribution({
			max: d.max,
			min: d.min,
			distribution: d.distribution,
		});
	}

	public extractDamageGeneric<K, T extends Damage.TDamageGeneric<K> = Damage.TDamageGeneric<K>>(t: T) {
		return Damage.TDamageTypesArray.reduce(
			(obj, k: Damage.damageType) => (t[k] ? { ...obj, [k]: t[k] } : obj),
			{},
		) as Damage.TDamageGeneric<K>;
	}

	public getTotalDamageValue(d: Damage.TDamageUnit): number {
		return Object.keys(d ?? {}).reduce((sum: number, dt: Damage.damageType) => {
			const v = d[dt];
			return sum + v;
		}, 0);
	}

	public addDamageUnits(...d: $.TList<Damage.TDamageUnit>) {
		return Damage.TDamageTypesArray.reduce(
			(dObj, damageType) => ({
				...dObj,
				[damageType]: Series.sum(_.pluck(d, damageType)),
			}),
			{},
		) as Damage.TDamageUnit;
	}

	public substractDamageUnits(d1: Damage.TDamageUnit, d2: Damage.TDamageUnit, min = 0) {
		return Damage.TDamageTypesArray.reduce(
			(dObj, damageType: Damage.damageType) => ({
				...dObj,
				[damageType]: Math.max(min, d1[damageType] - d2[damageType]),
			}),
			{},
		) as Damage.TDamageUnit;
	}
}

export const DamageManager = new DamageMethods();
export default DamageManager;

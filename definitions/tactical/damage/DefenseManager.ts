import _ from 'underscore';
import { cloneObject } from '../../core/dataTransformTools';
import {
	isDefaultProtection,
	isEmptyDodgeObject,
	isEmptyModifier,
	isEmptyModifierProperty,
	isEmptyProtection,
	isEmptyProtectionContainer,
	isValidDodgeValue,
	trimNumbers,
} from '../../core/helpers';
import $$ from '../../maths/BasicMaths';
import { ArrowUPUIToken, MaxBoundaryUIToken, MinBoundaryUIToken } from '../statuses/constants';
import UnitAttack from './attack';
import Damage from './damage';
import DamageManager from './DamageManager';
import UnitDefense from './defense';
import { DefenseFlagCaptions, deliveryCaptions } from './strings';

class DefenseMethods {
	/**
	 * Creates a single protection decorator from a collection of them
	 *
	 * @param {UnitDefense.IDamageProtectionDecorator} args[] any number of protection decorators
	 */
	public stackDamageProtection(
		...args: UnitDefense.IDamageProtectionDecorator[]
	): UnitDefense.IDamageProtectionDecorator {
		args = args.filter((v) => !isEmptyProtection(v));
		if (!args.length) return {};
		const r = $$.stackAbsorptionModifiers(...args);
		const t = Math.max(..._.pluck(args, 'threshold').filter((v) => !isNaN(v)));
		if (t > 0) r.threshold = t;
		return this.serializeProtectionDecorator(r);
	}

	/**
	 * Rearranges protection collection to minify its notation
	 *
	 * @param {UnitDefense.TArmor} protection a Protection Collection
	 * @return {UnitDefense.TArmor} an identical minified Protection Collection
	 */
	public optimizeProtection<K extends UnitDefense.TArmor>(protection: K): K {
		const r = {} as K;
		if (!protection) return r;
		if (isEmptyProtectionContainer(protection)) return protection;

		const objectCache: Array<{
			protection: UnitDefense.IDamageProtectionDecorator;
			damageTypes: Damage.damageType[];
		}> = [];
		const protectionKeys: EnumProxy<Damage.damageType, boolean> = {};

		// gathering keys to merge
		for (const damageType of Damage.TDamageTypesArray) {
			const p = this.getDamageProtectorDecorator(protection, damageType);
			if (isEmptyProtection(p)) continue;
			protectionKeys[damageType] = true;
			const memo = objectCache.find((v) => _.isEqual(v.protection, p));
			if (memo) memo.damageTypes.push(damageType);
			else
				objectCache.push({
					protection: p,
					damageTypes: [damageType],
				});
		}
		// finding the most used protection decorator
		let defaultProtectionIndex = 0;
		for (let i = 0; i < objectCache.length; i++)
			if (
				(objectCache[i].damageTypes?.length || 0) >
				(objectCache[defaultProtectionIndex].damageTypes?.length || 0)
			)
				defaultProtectionIndex = i;
		const defaultProtection = objectCache[defaultProtectionIndex];

		// if there are only empty protection decorators, return the same protection
		if (isEmptyProtection(defaultProtection.protection)) return protection;

		// if the most used protection decorator is used more than once, and all damage types are covered, use it as default
		if (
			defaultProtection.damageTypes.length > 1 &&
			Object.keys(protectionKeys).length === Damage.TDamageTypesArray.length
		)
			r.default = defaultProtection.protection;
		// else just copy it
		else defaultProtection.damageTypes.forEach((dt) => (r[dt] = defaultProtection.protection));
		// copy other damage types
		objectCache
			.filter((obj, index) => index !== defaultProtectionIndex && !isEmptyProtection(obj.protection))
			.forEach(({ protection, damageTypes }) => damageTypes.forEach((dt) => (r[dt] = protection)));

		// copy extraneous fields
		return Object.assign({}, r, _.omit(protection, ...[...Damage.TDamageTypesArray, 'default']));
	}

	/**
	 * Rearranges Dodge object to minify its notation
	 *
	 * @param {UnitDefense.TDodge} baseDodge a Dodge object
	 * @return {UnitDefense.TDodge} an identical minified Dodge object
	 */
	public optimizeDodge(baseDodge: UnitDefense.TDodge): UnitDefense.TDodge {
		const r = {} as UnitDefense.TDodge;
		if (isEmptyDodgeObject(baseDodge)) return r;

		const objectCache: Array<{
			dodge: number;
			deliveryTypes: UnitAttack.deliveryType[];
		}> = [];

		const precision = 1e-3;

		// gathering keys to merge
		for (const deliveryType of UnitAttack.TDeliveryTypeArray) {
			const p = $$.roundToPrecision(baseDodge[deliveryType] ?? baseDodge.default ?? 0, precision);
			const memo = objectCache.find((v) => Math.abs(p - v.dodge) < precision);
			if (memo) memo.deliveryTypes.push(deliveryType);
			else
				objectCache.push({
					dodge: p,
					deliveryTypes: [deliveryType],
				});
		}

		// finding the most used value
		let defaultDeliveryIndex = -1;
		for (let i = 0; i < objectCache.length; i++)
			if (
				(objectCache[i].deliveryTypes?.length ?? 0) >
				(objectCache[defaultDeliveryIndex]?.deliveryTypes?.length ?? -1)
			)
				defaultDeliveryIndex = i;

		if (defaultDeliveryIndex < 0) return r;
		const defaultDodgeValue = objectCache[defaultDeliveryIndex];
		if (defaultDodgeValue.deliveryTypes.length >= 1) r.default = defaultDodgeValue.dodge;
		// copy other delivery values
		objectCache
			.filter((obj, index) => index !== defaultDeliveryIndex && isValidDodgeValue(obj.dodge))
			.forEach(({ dodge, deliveryTypes }) => deliveryTypes.forEach((dt) => (r[dt] = dodge)));

		if (!r.default || Object.keys(r).length === 1) return _.omit(r, (k) => k === 0);
		return r;
	}

	/**
	 * Normalize Protection collection, removing useless decorators and properties and expanding `default`
	 *
	 * @param {UnitDefense.TArmor} protection
	 */
	public serializeProtection<K extends UnitDefense.TArmor>(protection: K): K {
		if (!protection) return {} as K;
		return this.optimizeProtection(
			_.mapObject(
				_.omit(
					Damage.TDamageTypesArray.reduce((o, k) => {
						const d = this.getDamageProtectorDecorator(protection, k);
						if (!isEmptyProtection(d)) o[k] = d;
						return o;
					}, cloneObject(protection)),
					'default',
				),
				(v, k) =>
					Damage.TDamageTypesArray.includes(k as Damage.damageType)
						? this.serializeProtectionDecorator(v)
						: v,
			) as K,
		);
	}

	/**
	 * Get appropriate modifier from Protection collection for a given Damage Type, with respect to default prop
	 *
	 * @param {UnitDefense.TArmor} protection
	 * @param {Damage.damageType} damageType
	 */
	public getDamageProtectorDecorator<T extends UnitDefense.TArmor>(
		protection: T,
		damageType: Damage.damageType,
	): UnitDefense.IDamageProtectionDecorator {
		return protection?.[damageType] || protection?.default || {};
	}

	/**
	 * Apply a set of Protection Modifiers to a given Protection, returning a new Protection
	 *
	 * @param protection
	 * @param mods
	 */
	public stackDamageProtectionDefinition<T extends UnitDefense.TArmor>(
		protection: T,
		...mods: UnitDefense.TArmorModifier[]
	): T {
		mods = mods.filter((v) => v !== null && Object.keys(v).length);
		if (!mods.length) return protection;
		const modifiers = this.stackArmorModifiers(...mods);
		Damage.TDamageTypesArray.forEach((dT: Damage.damageType) => {
			protection[dT] = this.stackDamageProtection(
				this.getDamageProtectorDecorator(protection, dT),
				modifiers[dT] || modifiers.default,
			);
		});
		protection.default = this.stackDamageProtection(protection.default, modifiers.default);
		return this.serializeProtection(protection);
	}

	/**
	 * Adds 1% of Damage Protection Factor per every 2% of Missing Hit Points, down to 20%, unless Factor is negative
	 *
	 * @param {UnitDefense.TArmor} protection - Protection Structure
	 * @param {number} healthPercentage - health percentage
	 */
	public addResilientProtection<T extends UnitDefense.TArmor>(protection: T, healthPercentage = 1): T {
		const absorptionMargin = 0.2;
		return this.serializeProtection(
			Damage.TDamageTypesArray.reduce((obj, key) => {
				const mod = this.getDamageProtectorDecorator(obj, key);
				if (Number.isFinite(mod.factor) && mod.factor < absorptionMargin)
					return Object.assign(obj, {
						[key]: mod,
					});
				return Object.assign(obj, {
					[key]: Object.assign({}, mod, {
						factor: $$.clipValue({
							min: absorptionMargin,
							max: mod.factor || 1,
							x: (mod.factor || 1) - (1 - $$.clipTo1(healthPercentage)) / 2,
						}),
					}),
				});
			}, cloneObject(protection || {}) as T),
		);
	}

	/**
	 * Stack an arbitrary number of Armor Modifiers, creating a new one that provides identical cumulative protection
	 *
	 * @param {UnitDefense.TArmorModifier} armorMods[]
	 */
	public stackArmorModifiers(...armorMods: UnitDefense.TArmorModifier[]): UnitDefense.TArmorModifier {
		const p = [].concat(armorMods).filter(Boolean);
		if (!p.length) return {};
		const allKeys = Damage.TDamageTypesArray.filter((damageType) =>
			p.some((mod) => !isEmptyProtection(mod[damageType])),
		);
		const r = allKeys.reduce((newA: UnitDefense.TArmorModifier, dT: Damage.damageType) => {
			return {
				...newA,
				[dT]: this.stackDamageProtection(...p.map((v) => this.getDamageProtectorDecorator(v, dT))),
			};
		}, {});
		r.default = this.stackDamageProtection(...p.map((x) => x.default).filter((v) => !isEmptyProtection(v)));
		r.baseCost = $$.stackModifiers(...p.map((x) => x.baseCost).filter((v) => !isEmptyModifier(v)));
		return _.omit(r, (mod) => isEmptyModifier(mod));
	}

	public getTotalArmor(...armors: UnitDefense.TArmor[]) {
		const k = {} as UnitDefense.TArmor;
		Damage.TDamageTypesArray.forEach((dT: Damage.damageType) => {
			const t = this.stackDamageProtection(...armors.map((a) => this.getDamageProtectorDecorator(a, dT)));
			if (!isEmptyProtection(t)) k[dT] = t;
		});
		k.default = this.stackDamageProtection(..._.pluck(armors, 'default'));
		return this.serializeProtection(k);
	}

	/**
	 * Stack an arbitrary number of Defense Flags into a new object, containing all available flags
	 *
	 * @param {UnitDefense.TDefenseFlags }defenseFlage
	 */
	public getTotalFlags(...defenseFlage: UnitDefense.TDefenseFlags[]): UnitDefense.TDefenseFlags {
		const k = {} as UnitDefense.TDefenseFlags;
		Object.values(UnitDefense.defenseFlags).forEach((v: UnitDefense.defenseFlags) => {
			if ((defenseFlage || []).some((d) => !!d?.[v])) k[v] = true;
		});
		return k;
	}

	/**
	 * Stack an arbitrary number of Dodge Modifiers, creating a new one Dodge collection with identical effects
	 *
	 * @param {UnitDefense.TDodge} dodge
	 * @param {UnitDefense.TDodge} dodgeMods[]
	 */
	public stackDodge(dodge: UnitDefense.TDodge, ...dodgeMods: UnitDefense.TDodgeModifier[]): UnitDefense.TDodge {
		const c = {} as UnitDefense.TDodge;
		UnitAttack.TDeliveryTypeArray.forEach((t) => {
			const d = $$.stackModifiers(
				...dodgeMods.map((v) => v?.[t] || v?.default).filter((v) => !isEmptyModifier(v)),
			);
			const base = dodge?.[t] ?? dodge?.default ?? 0;
			if ((!isValidDodgeValue(base) || base === 0) && isEmptyModifier(d)) return;
			c[t] = $$.applyModifier(base, d);
		});
		c.default = $$.applyModifier(
			dodge?.default ?? 0,
			...dodgeMods.map((v) => v?.default).filter((v) => !isEmptyModifier(v)),
		);
		return this.optimizeDodge(c);
	}

	/**
     * Stack an arbitrary number of Shield Modifiers, creating a new one that provides identical cumulative protection and other props

     * @param {UnitDefense.TShieldsModifier} shieldMods[]
     */
	public stackShieldsModifier(...shieldMods: UnitDefense.TShieldsModifier[]): UnitDefense.TShieldsModifier {
		shieldMods = [].concat(shieldMods).filter(Boolean);
		if (!shieldMods.length) return {};
		const r: UnitDefense.TShieldsModifier = this.stackArmorModifiers(...shieldMods);
		const shieldAmountModifiers: IModifier[] = _.pluck(shieldMods, 'shieldAmount').filter(
			(v) => !isEmptyModifier(v),
		);
		if (shieldAmountModifiers.length) r.shieldAmount = $$.stackModifiers(...shieldAmountModifiers);
		const shieldOverchargeModifiers: IModifier[] = _.pluck(shieldMods, 'overchargeDecay').filter(
			(v) => !isEmptyModifier(v),
		);
		if (shieldOverchargeModifiers.length) r.overchargeDecay = $$.stackModifiers(...shieldOverchargeModifiers);
		return r;
	}

	/**
	 * Sanitizes a Protection Decorator, removing useless fields and applying min=0 if not present
	 *
	 * @param {UnitDefense.IDamageProtectionDecorator} protection
	 */
	public serializeProtectionDecorator(protection: UnitDefense.IDamageProtectionDecorator) {
		if (!protection || !Object.keys(protection).length) return null;
		const t = _.omit(
			protection || {},
			(val, key) =>
				val === null ||
				(key === 'factor' && val === 1) ||
				(key === 'bias' && val === 0) ||
				(key === 'min' && val === -Infinity) ||
				(key === 'threshold' && val <= 0) ||
				(key === 'minGuard' && val === -Infinity) ||
				(key === 'max' && val === Infinity) ||
				(key === 'maxGuard' && val === Infinity),
		);
		if (isEmptyModifierProperty(t.min)) t.min = 0;
		return trimNumbers(t);
	}

	public addShields<T extends UnitDefense.TShield = UnitDefense.TShield>(shield: T, quantity: number): T {
		if (!shield) return null;
		if (!Number.isFinite(quantity) || quantity === 0) return this.serializeProtection(shield);
		const r = cloneObject(shield);
		r.currentAmount = Math.min(
			r.currentAmount + quantity ?? 0,
			Number.isFinite(shield.overchargeDecay) && shield.overchargeDecay > 0 ? Infinity : r.shieldAmount,
		);
		if (r.currentAmount < 0) r.currentAmount = 0;
		return this.serializeProtection(r);
	}

	public describeShield(s: UnitDefense.TShield): string {
		return `${s.caption || 'Shield'} [${Math.round(s.currentAmount)}/${Math.round(s.shieldAmount)}]${
			s.overchargeDecay > 0 ? `(${s.overchargeDecay}/Turn)` : ''
		} : ${this.describeProtection(s)} Damage`;
	}

	public describeArmor(a: UnitDefense.TArmor): string {
		return `Armor: ${this.describeProtection(a)} Damage`;
	}

	public describeDodge(d: UnitDefense.TDodge): string {
		if (!d || !Object.keys(d).length) return '';
		d = this.optimizeDodge(d);
		const r: string[] = [];
		const getPercentile = (v: number) => `${v > 0 ? '+' : ''}${Math.round(v * 100)}%`;
		if (d?.default && Object.keys(d).length === 1) {
			r.push(`${getPercentile(d.default)} All Attacks`);
		} else {
			Object.keys(d)
				.filter((v) => v !== 'default')
				.forEach((delivery) => r.push(`${getPercentile(d[delivery])} ${deliveryCaptions[delivery]}`));
			if (d.default) r.push(`${getPercentile(d.default)} Other Attacks`);
		}
		return r.join(', ');
	}

	public describeDodgeModifier(d: UnitDefense.TDodgeModifier): string {
		if (!d || !Object.keys(d).length) return '';
		const r: string[] = [];

		if (d?.default && Object.keys(d).length === 1) {
			r.push(`${$$.describePercentileModifier(d.default, true)} All Attacks`);
		} else {
			Object.keys(d)
				.filter((v) => v !== 'default')
				.forEach((delivery) =>
					r.push(`${$$.describePercentileModifier(d[delivery], true)} ${deliveryCaptions[delivery]}`),
				);
			if (d.default) r.push(`${$$.describePercentileModifier(d.default, true)} Other Attacks`);
		}
		return r.join(', ');
	}

	public describeDefenseFlags(d: UnitDefense.TDefenseFlags) {
		return Object.keys(d)
			.filter((k) => !!d[k])
			.map((k) => DefenseFlagCaptions[k])
			.join(', ');
	}

	public describeProtectionDecorator(
		p: UnitDefense.IDamageProtectionDecorator,
		damageType: Damage.damageType = null,
		customCaption: string = null,
	): string {
		return `${$$.describeModifier(p, true, false, true)}${p.threshold ? `/ ≥${p.threshold} ` : ''}${
			p.max > 0 ? `/ ≤${p.max}` : ''
		}${p.min < 0 ? ` [0-${-p.min}]${ArrowUPUIToken}` : ''} ${customCaption || Damage.getShortCaption(damageType)}`;
	}

	public describeProtection(p: UnitDefense.TArmor): string {
		const r: string[] = [];
		p = this.serializeProtection(p);
		const protection = DamageManager.extractDamageGeneric<
			UnitDefense.IDamageProtectionDecorator,
			UnitDefense.TArmor
		>(p);
		const hasDefault = !isEmptyProtection(p.default) && !isDefaultProtection(p.default);
		if (Object.keys(protection).length === 0)
			if (hasDefault) r.push(this.describeProtectionDecorator(p.default, null));
			else return '';
		else {
			Object.keys(protection).forEach((damageType: Damage.damageType) => {
				r.push(this.describeProtectionDecorator(p[damageType], damageType));
			});
			if (hasDefault) r.push(this.describeProtectionDecorator(p.default, null, ' Other'));
		}
		return r
			.map((v) =>
				v
					.replace(/\s*\/\s*/g, '/')
					.replace(/[\s\t]+/g, ' ')
					.replace(/^\//, '')
					.trim(),
			)
			.filter(Boolean)
			.join(' | ');
	}

	public describeDefense(d: UnitDefense.IDefenseInterface): string {
		return [
			d.caption || 'Defense',
			d.shields.map(this.describeShield.bind(this)).join('\n'),
			this.describeArmor(d.armor),
			...Object.values(UnitDefense.defenseFlags).reduce((obj, flag) => (d[flag] ? obj.concat(flag) : obj), []),
		].join('\n');
	}
}

export const DefenseManager = new DefenseMethods();

export default DefenseManager;

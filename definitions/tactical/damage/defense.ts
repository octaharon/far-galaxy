import { TEffectApplianceList } from '../statuses/types';
import { IUnitPartModifier } from '../units/UnitPart';
import UnitAttack from './attack';
import Damage from './damage';

export namespace UnitDefense {
	export interface IDamageProtectionDecorator extends IModifier {
		threshold?: number;
	}

	export type TDamageProtectionType = keyof IDamageProtectionDecorator;
	export type TDamageProtectionKey = Damage.damageType | 'default';
	export type TProtectionKind = 'armor' | 'shield';

	export type TArmor = Partial<Damage.TDamageGenericWithDefault<IDamageProtectionDecorator>>;

	export enum defenseFlags {
		none = 'def_none',
		reactive = 'def_rea',
		/* Reactive Defense applies reduction twice against 'drone', 'ballistic' and 'missile' attacks while shields are active and evades Delayed damage*/
		hull = 'def_hul',
		/* Magnetic hull doesn't pass any damage to body when the last shield is destroyed, and evades AOE attacks of "Surface", "Aerial" and "Cloud" type */
		evasion = 'def_eva',
		/* Evasion Module doubles the chance to miss with 'ballistic', 'missile', 'aerial' and 'surface' */
		sma = 'def_sma',
		/* SMA increases damage up to +50% depending on missing Hit Points */
		resilient = 'def_res',
		/* Resilient defense increase Armor absorption up to +50% based on missing Hit Points */
		boosters = 'def_bst',
		/* Shield boosters restore 25% of the innermost shield when the defense is breached (still applying body damage) */
		warp = 'def_wrp',
		/* Warp defense loses half of shields from 'beam', 'supersonic' and 'immediate' attacks, and evades DamageOverTime attacks */
		tachyon = 'def_tch',
		/* Tachyon defense doubles the chance to miss with everything but "immediate" */
		disposable = 'def_dsp',
		/* disposable defence  will dissipate after breached */
		replenishing = 'def_rpl' /* @TODO */,
		/* Replenishing defense will regenerate the least damaged shield if no damage has been taken last turn  */
		immune = 'def_immune',
		/* immune targets are immune to Hit Points damage and status effects */
		unstoppable = 'def_unstoppable',
		/* unstoppable targets are immune to Negative status effects */
	}

	export type TDodge = UnitAttack.TDeliveryTypeGenericWithDefault<number>;

	export const DodgeTemplate = (value = 0, withDefault?: boolean): TDodge => {
		return withDefault
			? { default: value }
			: (Object.values(UnitAttack.deliveryType).reduce((o, k) => ({ ...o, [k]: value }), {}) as TDodge);
	};

	export type TDefenseFlags = EnumProxy<defenseFlags, boolean>;
	export type TDefenseFlagsDecorator = {
		defenseFlags: TDefenseFlags;
	};

	export interface IShieldDecorator {
		shieldAmount: number;
		overchargeDecay?: number; // if positive, shield can be overcharged, and a parameter describes decay rate
	}

	export interface IDamageShield extends TArmor {
		caption?: string;
		currentAmount: number;
	}

	export type TShield = IShieldDecorator & IDamageShield;
	export type TShieldDefinition = IShieldDecorator & TArmor;

	export interface IDefenseInterface extends TDefenseFlags {
		id?: string;
		caption?: string;
		shields?: TShield[];
		armor?: TArmor;
		dodge?: TDodge;
		priority?: number;
	}

	export type TDodgeModifier = Proxy<TDodge, IModifier>; // modifier describes transform of value, as value is number
	// if value is modifier itself, then this modifier describes same structure, which is added field-wise
	export type TArmorModifier = Proxy<TArmor, IDamageProtectionDecorator> & IUnitPartModifier;
	export type TShieldsModifier = TArmorModifier & Proxy<IShieldDecorator, IModifier>;

	export interface IAttackEffectOnDefense {
		shieldReduction: number[]; // amount to deduce from shields, index-wise
		breach: boolean; // defence was breached
		removed?: boolean; // when true, defense is to be removed
		throughDamage?: Damage.TDamageUnit; // damage passing the defense
		hit: boolean; // defence was hit directly
		effectsApplied?: TEffectApplianceList;
	}

	export interface IAttackEffectOnUnit extends IAttackEffectOnDefense {
		killed: boolean;
	}

	export interface IAttackBatchEffectOnDefense extends IAttackEffectOnDefense {
		aoeHit: boolean; // AoE weapon hit
		directHit: boolean; // Direct attacks hit
	}

	export enum TArmorSlotType {
		none = 'armor_none',
		small = 'armor_small',
		medium = 'armor_medium',
		large = 'armor_large',
	}

	export enum TShieldSlotType {
		none = 'shield_none',
		small = 'shield_small',
		medium = 'shield_medium',
		large = 'shield_large',
	}
}

export const isDefenseFlag =
	<T extends UnitDefense.defenseFlags = UnitDefense.defenseFlags>(type?: T) =>
	(t: any): t is T =>
		Object.keys(UnitDefense.defenseFlags).includes(t) && (!type || type === t);

export const createProtectionTemplate = (factor = 0, bias = 0, threshold = 0) => {
	return Object.values(Damage.damageType).reduce(
		(o, k) => ({
			...o,
			[k]: {
				factor,
				bias,
				threshold,
				min: 0,
			},
		}),
		{},
	) as UnitDefense.TArmor;
};

export default UnitDefense;

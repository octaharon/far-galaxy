import UnitAttack from './attack';
import AttackManager from './AttackManager';
import UnitDefense from './defense';

export const DefenseFlagCaptions: EnumProxyWithDefault<UnitDefense.defenseFlags, string> = {
	[UnitDefense.defenseFlags.sma]: 'SMA Module',
	[UnitDefense.defenseFlags.reactive]: 'Reactive Defense',
	[UnitDefense.defenseFlags.evasion]: 'Evasion Module',
	[UnitDefense.defenseFlags.resilient]: 'Resilient Defense',
	[UnitDefense.defenseFlags.replenishing]: 'Replenishing Defense',
	[UnitDefense.defenseFlags.unstoppable]: 'Unstoppable',
	[UnitDefense.defenseFlags.immune]: 'Invulnerable',
	[UnitDefense.defenseFlags.warp]: 'Warp Defense',
	[UnitDefense.defenseFlags.boosters]: 'Shield Booster',
	[UnitDefense.defenseFlags.tachyon]: 'Tachyon Defense',
	[UnitDefense.defenseFlags.hull]: 'HULL Module',
	[UnitDefense.defenseFlags.disposable]: 'Disposable Defense',
	default: 'Unknown Perk',
};
export const deliveryCaptions: EnumProxyWithDefault<UnitAttack.deliveryType, string> = {
	[UnitAttack.deliveryType.aerial]: 'Aerial',
	[UnitAttack.deliveryType.ballistic]: 'Projectile',
	[UnitAttack.deliveryType.missile]: 'Missile',
	[UnitAttack.deliveryType.immediate]: 'Immediate',
	[UnitAttack.deliveryType.cloud]: 'Cloud',
	[UnitAttack.deliveryType.surface]: 'Surface',
	[UnitAttack.deliveryType.bombardment]: 'Bombardment',
	[UnitAttack.deliveryType.beam]: 'Beam',
	[UnitAttack.deliveryType.drone]: 'Drone',
	[UnitAttack.deliveryType.supersonic]: 'Supersonic',
};

export const getAOETypeCaption = <
	T extends UnitAttack.IAreaOfEffectAttackDecorator = UnitAttack.IAreaOfEffectAttackDecorator,
>(
	attack: T,
	plural = false,
) => {
	const captions: EnumProxyWithDefault<UnitAttack.TAoEDamageSplashTypes, string> = {
		[UnitAttack.TAoEDamageSplashTypes.chain]: plural ? 'Chain Attacks' : 'Chain Attack',
		[UnitAttack.TAoEDamageSplashTypes.scatter]: plural ? 'Scatter Attacks' : 'Scatter Attack',
		[UnitAttack.TAoEDamageSplashTypes.circle]: plural ? 'Circle Splash Attacks' : 'Circle Splash',
		[UnitAttack.TAoEDamageSplashTypes.cone90]: plural ? 'Sector Splash Attacks' : 'Sector Splash',
		[UnitAttack.TAoEDamageSplashTypes.cone180]: plural ? 'Semicircle Splash Attacks' : 'Semicircle Splash',
		[UnitAttack.TAoEDamageSplashTypes.lineAside]: plural ? 'Splash Aside Attacks' : 'Splash Aside',
		[UnitAttack.TAoEDamageSplashTypes.lineBehind]: plural ? 'Splash Behind Attacks' : 'Splash Behind',
		default: plural ? 'Area Attacks' : 'Area Attack',
	};
	return captions[attack?.aoeType] || captions.default;
};

export const getTypeCaption = (
	attack: UnitAttack.TAttackTypedDecorator,
	attackType?: UnitAttack.attackFlags,
	plural = false,
): string => {
	const captions: EnumProxyWithDefault<UnitAttack.attackFlags, string> = {
		[UnitAttack.attackFlags.DoT]: 'Damage Over Time',
		[UnitAttack.attackFlags.Deferred]: !plural ? 'Deferred Damage' : 'Deferred Attacks',
		[UnitAttack.attackFlags.Selective]: !plural ? 'Selective Attack' : 'Selective Attacks',
		[UnitAttack.attackFlags.Disruptive]: !plural ? 'Disruptive Attack' : 'Disruptive Attacks',
		[UnitAttack.attackFlags.Penetrative]: !plural ? 'Penetrative Attack' : 'Penetrative Attacks',
		[UnitAttack.attackFlags.AoE]: getAOETypeCaption(attack as UnitAttack.IAreaOfEffectAttackDecorator, plural),
		default: !plural ? 'Direct Attack' : 'Direct Attacks',
	};
	switch (true) {
		case !!attackType:
			return captions[attackType];
		case AttackManager.isDOTAttack(attack):
			return captions[UnitAttack.attackFlags.DoT];
		case AttackManager.isDeferredAttack(attack):
			return captions[UnitAttack.attackFlags.Deferred];
		case AttackManager.isDisruptiveAttack(attack):
			return captions[UnitAttack.attackFlags.Disruptive];
		case AttackManager.isSelectiveAttack(attack):
			return captions[UnitAttack.attackFlags.Selective];
		case AttackManager.isPenetrativeAttack(attack):
			return captions[UnitAttack.attackFlags.Penetrative];
		case AttackManager.isAOEAttack(attack):
			return captions[UnitAttack.attackFlags.AoE];
		default:
			return 'Direct Attack';
	}
};

export function describeAttackDelivery(a: UnitAttack.IAttackInterface) {
	return a.attacks?.map((atk) => {
		return deliveryCaptions[atk.delivery];
	});
}

export function describeAttackTypes(a: UnitAttack.IAttackInterface) {
	return a.attacks?.map((atk) => {
		return getTypeCaption(atk);
	});
}

import predicateComparator from '../../../src/utils/predicateComparator';
import Distributions from '../../maths/Distributions';

namespace Damage {
	export enum damageType {
		kinetic = 'dmg_t_kin', // does 1.25 versus shields
		heat = 'dmg_t_hea', // does 1.25 versus targets below 50% hp, if shields are breached
		thermodynamic = 'dmg_t_trm', // does 1.25 to Organic and Massive targets if shields are breached
		magnetic = 'dmg_t_emg', // does 1.25 damage to Robotic and Massive targets if shields are breached
		radiation = 'dmg_t_rad', // does 1.25 damage to Organic and Robotic targets if shields are breached
		biological = 'dmg_t_bio', // does 1.5 damage to Organic targets if shields are breached
		antimatter = 'dmg_t_antimatter', // does 1.5 versus shields
		warp = 'dmg_t_warp', // does 1.5 damage to Massive targets when shields are breached
	}

	export type IDamageModifier = IModifier & {
		distribution?: Distributions.TDistribution;
	};

	export interface IDamageModifierProperty {
		damageModifier: IDamageModifier;
	}

	export type IDamageFactor = Distributions.ISeedValue;

	export type TDamageGeneric<T = any> = EnumProxy<damageType, T>;
	export type TDamageGenericWithDefault<T = any> = EnumProxyWithDefault<damageType, T>;
	export type TDamageDefinition = TDamageGeneric<IDamageFactor>;
	export type TDamageUnit = TDamageGeneric<number>;
	export const TDamageTypesArray = Object.values(damageType).sort(
		predicateComparator(
			Object.values(Damage.damageType).map((k) => (t) => t === k),
			String,
		),
	);

	export function DamageTemplate(value: number) {
		return TDamageTypesArray.reduce((o, t) => ({ ...o, [t]: value }), {}) as TDamageUnit;
	}

	export const shortCaptions: EnumProxy<Damage.damageType, string> = {
		[Damage.damageType.warp]: 'WRP',
		[Damage.damageType.radiation]: 'RAD',
		[Damage.damageType.magnetic]: 'EMG',
		[Damage.damageType.heat]: 'HEA',
		[Damage.damageType.biological]: 'BIO',
		[Damage.damageType.antimatter]: 'ANH',
		[Damage.damageType.thermodynamic]: 'TRM',
		[Damage.damageType.kinetic]: 'KIN',
	};

	export function getShortCaption(t: Damage.damageType): string {
		return shortCaptions[t] || 'All';
	}
}

export default Damage;

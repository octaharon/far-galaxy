import _ from 'underscore';
import DIContainer from '../../core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../core/DI/injections';
import $$ from '../../maths/BasicMaths';
import UnitChassis from '../../technologies/types/chassis';
import Units from '../units/types';
import UnitAttack from './attack';
import AttackManager from './AttackManager';
import { COMBAT_TICKS_PER_TURN } from './constants';
import Damage from './damage';
import DamageManager from './DamageManager';
import UnitDefense from './defense';
import DefenseManager from './DefenseManager';

const DELIVERY = UnitAttack.deliveryType;

/**
 * Return the amount of damage passing through a particular protection
 *
 * @export
 * @param {number} dmg
 * @param {IDamageProtectionDecorator} [protection] defense factor for the current damage type
 * @returns Damage done through Protection
 */
export function applyDamageProtection(dmg: number, protection: UnitDefense.IDamageProtectionDecorator): number {
	if (!protection) return dmg;
	const p = DefenseManager.serializeProtectionDecorator(protection);
	if (!isNaN(p?.threshold) && dmg < p?.threshold) return 0;
	return $$.applyModifier(dmg, p);
}

/**
 * Return the amount of damage passing through a particular Armor
 *
 * @export
 * @param {Damage.TDamageUnit} dmg Damage Unit
 * @param {UnitDefense.TArmor} def Armor/Shield
 * @param {IModifier} modifier applied to damage reduction
 * @returns {Damage.TDamageUnit} Damage done to hitpoints per type
 */
export function applyDamageUnitToArmor(
	dmg: Damage.TDamageUnit,
	def: UnitDefense.TArmor,
	modifier?: IModifier,
): Damage.TDamageUnit {
	const reducedDamage = _.mapObject(dmg, (v: number, k: Damage.damageType) =>
		applyDamageProtection(v, DefenseManager.getDamageProtectorDecorator(def, k)),
	);

	if (!modifier) return reducedDamage;

	const damageReduction = _.mapObject(DamageManager.substractDamageUnits(dmg, reducedDamage), (v: number) =>
		$$.applyModifier(v, modifier),
	);

	return DamageManager.substractDamageUnits(dmg, damageReduction);
}

/**
 * Apply damage modifiers per type of damage and target status
 * @param d {Damage.TDamageUnit} incoming damage
 * @param targetHealthPercentage {number} fraction of target Hit Points at the moment of weapon
 * @param targetShieldPercentage {number} fraction of target shield being attacked
 * @param targetModifier {UnitChassis.TTargetFlags} target type
 */
export function applyDamageTypeModifiers(
	d: Damage.TDamageUnit,
	targetHealthPercentage: number | null = 1,
	targetShieldPercentage = 0,
	targetModifier: UnitChassis.TTargetFlags = {},
): Damage.TDamageUnit {
	const r = {} as Damage.TDamageUnit;
	Object.keys(d).forEach((dt: Damage.damageType) => {
		let v = d[dt];
		switch (true) {
			case dt === Damage.damageType.kinetic:
				if (targetShieldPercentage) v *= 1.5; // Projectile attacks do +50% damage versus shields
				break;
			case dt === Damage.damageType.antimatter:
				if (targetShieldPercentage) v *= 1.25;
				// Antimatter attacks do +25% damage versus shields
				else if (targetModifier[UnitChassis.targetType.unique])
					// and +50% to Unique Units without Shields
					v *= 1.5;
				break;
			case dt === Damage.damageType.heat:
				if (!isNaN(targetHealthPercentage) && targetHealthPercentage < 0.5 && !targetShieldPercentage)
					v *= 1.25; // Heat attacks do +25% damage versus targets with less than 50% hp
				break;
			case dt === Damage.damageType.thermodynamic && !targetShieldPercentage:
				if (targetModifier[UnitChassis.targetType.organic] || targetModifier[UnitChassis.targetType.massive])
					v *= 1.25; // Pressure attacks do +25% damage to Bio and Massive
				break;
			case dt === Damage.damageType.biological && !targetShieldPercentage:
				if (targetModifier[UnitChassis.targetType.organic]) v *= 1.5; // Bio attacks do +50% damage to Bio
				break;
			case dt === Damage.damageType.warp && !targetShieldPercentage:
				if (targetModifier[UnitChassis.targetType.massive]) v *= 1.5; // Warp attacks do +50% damage to Massive
				break;
			case dt === Damage.damageType.magnetic && !targetShieldPercentage:
				if (targetModifier[UnitChassis.targetType.robotic] || targetModifier[UnitChassis.targetType.massive])
					v *= 1.25; // Magnetic attacks do +25% damage to Robotic and Massive
				break;
			case dt === Damage.damageType.radiation && !targetShieldPercentage:
				if (targetModifier[UnitChassis.targetType.organic] || targetModifier[UnitChassis.targetType.robotic])
					v *= 1.25; // Radiation attacks do +25% damage to Bio and Robotic
				break;
			default:
		}
		r[dt] = v;
	});
	return r;
}

/**
 * Return amount of damage passing through all the Armors of a DefenseInterface
 *
 * @export
 * @param {UnitAttack.TAttackUnit} attack
 * @param {UnitDefense.IDefenseInterface} defense
 * @param {Units.IUnit} target in case hitpoints are considered
 * @param {IModifier} damageModifier additional damage modifier
 * @returns Damage done through all the defenses
 */
export function applyAttackUnitToDefense(
	attack: UnitAttack.TAttackUnit,
	defense: UnitDefense.IDefenseInterface,
	target?: Units.IUnit,
	damageModifier?: IModifier,
): UnitDefense.IAttackEffectOnDefense {
	const r = {
		shieldReduction: new Array(defense.shields.length).fill(0),
		effectsApplied: [],
		breach: false,
		hit: false,
	} as UnitDefense.IAttackEffectOnDefense;
	if (
		(AttackManager.isAOEAttack(attack) &&
			defense[UnitDefense.defenseFlags.hull] &&
			[
				// Some AoE are not applied to Hull
				DELIVERY.surface,
				DELIVERY.cloud,
				DELIVERY.aerial,
			].includes(attack.delivery)) ||
		(AttackManager.isDeferredAttack(attack) && // Deferred is not applied to Reactive defense
			defense[UnitDefense.defenseFlags.reactive]) ||
		(AttackManager.isDOTAttack(attack) && // DoT is not applied to Warp defense
			defense[UnitDefense.defenseFlags.warp])
	)
		return r;
	r.hit = true;
	if (attack.effects && !defense[UnitDefense.defenseFlags.immune]) {
		r.effectsApplied = attack.effects;
		if (defense[UnitDefense.defenseFlags.unstoppable])
			r.effectsApplied = r.effectsApplied.filter(
				(eff) => !DIContainer.getProvider(DIInjectableCollectibles.effect).get(eff.effectId).negative,
			);
	}
	let throughDamage = DamageManager.extractDamageGeneric<number, UnitAttack.TAttackUnit>(attack);
	let dmg = DamageManager.getTotalDamageValue(throughDamage);
	let shieldDamage = {} as Damage.TDamageUnit;

	const previousShieldValue = getTotalDefenseShieldsLeft(defense);
	const totalShields = getMaxTotalDefenseShields(defense);
	const targetHealthPercentage: number = target ? target.currentHitPoints / target.maxHitPoints : null; // a base being Attacked
	const targetType = target ? target.targetFlags : undefined;

	if (damageModifier) throughDamage = DamageManager.applyDamageModifierToDamageUnit(throughDamage, damageModifier);
	if (previousShieldValue)
		throughDamage = applyDamageTypeModifiers(throughDamage, null, previousShieldValue / totalShields, targetType);

	/* Applying damage to Shields */
	if (!attack[UnitAttack.attackFlags.Disruptive] && previousShieldValue) {
		for (let i = defense.shields.length - 1; i >= 0; i--) {
			const shld = defense.shields[i];
			if (shld.currentAmount > 0) {
				let newDmg = applyDamageUnitToArmor(throughDamage, shld);
				if (
					defense[UnitDefense.defenseFlags.reactive] &&
					[DELIVERY.drone, DELIVERY.ballistic, DELIVERY.missile].includes(attack.delivery)
				)
					newDmg = applyDamageUnitToArmor(newDmg, defense.armor); // apply armor protection as well
				shieldDamage = DamageManager.addDamageUnits(
					// calculating damage done to shields
					shieldDamage,
					newDmg,
				);
				dmg = DamageManager.getTotalDamageValue(newDmg);
				if (!dmg) break;
				r.shieldReduction[i] = dmg;
				if (shld.currentAmount >= dmg) {
					// shield absorbs all the damage
					dmg = 0;
					throughDamage = _.mapObject(throughDamage, () => 0);
					break;
				} else {
					const cf = shld.currentAmount / dmg; // percentage absorbed by shield
					throughDamage = _.mapObject(throughDamage, (v) => v * (1 - cf));
					dmg -= shld.currentAmount;
				}
			}
		}

		if (
			Math.max(...r.shieldReduction) &&
			defense[UnitDefense.defenseFlags.warp] &&
			[DELIVERY.beam, DELIVERY.immediate, DELIVERY.supersonic].includes(attack.delivery)
		) {
			// applying Warp modifier that halves shield lost
			r.shieldReduction = r.shieldReduction.map((v) => v / 2);
		}
		// can't reduce shields to negative
		r.shieldReduction = r.shieldReduction.map((v, i) => Math.min(v, defense.shields[i].currentAmount || 0));
	}
	/* if some damage skipped the shields */
	if (dmg) {
		throughDamage = applyDamageTypeModifiers(throughDamage, targetHealthPercentage, 0, targetType);
		if (defense[UnitDefense.defenseFlags.disposable]) r.removed = true;
		if (
			defense[UnitDefense.defenseFlags.immune] || // Immune targets don't take damage to HP
			(previousShieldValue > 0 && defense[UnitDefense.defenseFlags.hull]) // Hull units ignore damage if shield
			// is just breached
		)
			return r;
		throughDamage = applyDamageUnitToArmor(
			throughDamage,
			defense[UnitDefense.defenseFlags.resilient]
				? DefenseManager.addResilientProtection(defense.armor, targetHealthPercentage)
				: defense.armor,
		);
		dmg = DamageManager.getTotalDamageValue(throughDamage);
	}
	if (dmg) {
		r.breach = true;
		r.throughDamage = _.mapObject(throughDamage, (v: number) => $$.roundToPrecision(v));
	}
	return r;
}

export function getTotalDefenseShieldsLeft(defense: UnitDefense.IDefenseInterface, shieldReduction?: number[]) {
	if (!shieldReduction) shieldReduction = new Array(defense.shields.length).fill(0);
	return (defense?.shields || [])?.reduce(
		(acc, shield, ix) => acc + shield.currentAmount - (shieldReduction[ix] || 0),
		0,
	);
}

export function getMaxTotalDefenseShields(defense: UnitDefense.IDefenseInterface) {
	return (defense?.shields || [])?.reduce((acc, shield) => acc + shield.shieldAmount, 0);
}

/**
 * Get Attack frames for given weapon numbers
 *
 * @param nAttacks
 * @param ticks
 */
export function getAttackFrames(nAttacks: number, ticks = COMBAT_TICKS_PER_TURN): IBounds[] {
	return new Array(nAttacks).fill(0).map((v, ix) => ({
		min: Math.floor((ticks / nAttacks) * ix),
		max: Math.min(ticks - 1, Math.floor((ticks / nAttacks) * (ix + 1))),
	}));
}

export function getMissChance(
	accuracy: number,
	delivery: UnitAttack.deliveryType,
	d: UnitDefense.IDefenseInterface,
): number {
	if (
		d[UnitDefense.defenseFlags.evasion] &&
		[DELIVERY.ballistic, DELIVERY.missile, DELIVERY.aerial, DELIVERY.surface].includes(delivery)
	)
		accuracy /= 2; // Evasive flag applies to certain attacks
	let immediateMissChance =
		delivery === DELIVERY.immediate
			? 0 // immediate and secondary targets (except for Hull, which is applied
			: // above)
			  1 - accuracy + (d.dodge[delivery] || d.dodge.default || 0); // default dodge applies
	if (immediateMissChance > 0 && d[UnitDefense.defenseFlags.tachyon]) immediateMissChance *= 2; // Tachyon perk apply if hit is not secured
	return immediateMissChance;
}

/**
 * Calculate weapon batch effect on a defense interface
 *
 * @export
 * @param {UnitAttack.IAttackBatch} attack
 * @param {UnitDefense.IDefenseInterface} defense
 * @param {Units.IUnit} [target]
 * @returns {UnitDefense.IAttackBatchEffectOnDefense}
 */
export function applyAttackBatchToDefense(
	attack: UnitAttack.IAttackBatch,
	defense: UnitDefense.IDefenseInterface,
	target?: Units.IUnit,
): UnitDefense.IAttackBatchEffectOnDefense {
	const r = {
		hit: false,
		aoeHit: false,
		directHit: false,
		breach: false,
		effectsApplied: [],
		shieldReduction: new Array(defense.shields.length).fill(0),
	} as UnitDefense.IAttackBatchEffectOnDefense;
	const d = JSON.parse(JSON.stringify(defense)) as UnitDefense.IDefenseInterface;
	// Attacks are iterated starting with Direct ones
	const attacksSorted: UnitAttack.TAttackUnit[] = _.sortBy(attack.attacks, (a) =>
		AttackManager.isDirectAttack(a) ? -1 : 1,
	);

	for (const a of attacksSorted) {
		let attackEffect: UnitDefense.IAttackEffectOnDefense = null;
		let hit = false;
		// Calculating Hit Chance
		const immediateMissChance = getMissChance(attack.accuracy, a.delivery, d);

		/* Rolling hit */
		if (Math.random() >= immediateMissChance)
			/* Contact! */
			hit = true;
		switch (true) {
			case AttackManager.isAOEAttack(a):
				r.aoeHit = true;
				attackEffect = applyAttackUnitToDefense(a, d, target);
				break;
			case AttackManager.isDOTAttack(a):
				if (!r.directHit) a.effects = [];
				// Without Direct Hit DoT effects are not applied
				else hit = true;
				if (!hit) continue; // Without a direct hit and local hit Damage Over Time effect is ignored
				attackEffect = applyAttackUnitToDefense(a, d, target);
				break;
			case AttackManager.isDeferredAttack(a):
				if (!r.directHit) a.effects = [];
				else hit = true;
				if (!hit) continue;
				attackEffect = applyAttackUnitToDefense(a, d, target);
				break;
			default:
				if (!hit) continue;
				r.directHit = true;
				r.hit = true;
				attackEffect = applyAttackUnitToDefense(a, d, target);
				break;
		}
		// Applying results of attackEffect
		/* Applying shield reduction */
		(attackEffect.shieldReduction || []).forEach((v, ix) => {
			d.shields[ix].currentAmount -= v;
			r.shieldReduction[ix] += v;
		});
		/* defense should be removed */
		if (attackEffect.removed) r.removed = true;
		/* all shields gone means defense breached, for the first time */
		if (!r.breach && attackEffect.breach) {
			// defense breached for the first time
			r.breach = true;
			r.throughDamage = Damage.DamageTemplate(0);
		}
		if (r.breach) {
			r.throughDamage = DamageManager.addDamageUnits(r.throughDamage, attackEffect.throughDamage);
		}
		if (
			// miss or no effects applied due to flags
			attackEffect.hit &&
			!d[UnitDefense.defenseFlags.immune]
		)
			r.effectsApplied = r.effectsApplied.concat(attackEffect.effectsApplied);
	}

	if (d[UnitDefense.defenseFlags.unstoppable])
		r.effectsApplied = r.effectsApplied.filter(
			(eff) => !DIContainer.getProvider(DIInjectableCollectibles.effect).get(eff.effectId).negative,
		);

	return r;
}

/**
 * Apply a result of weapon to a given Defense Interface and return a new Defense Interface
 * @param {UnitDefense.IAttackEffectOnDefense} r Attack result
 * @param {UnitDefense.IDefenseInterface} d Current state of defense
 * @return {UnitDefense.IDefenseInterface}
 */
export function applyAttackEffectToDefense(
	r: UnitDefense.IAttackEffectOnDefense,
	d: UnitDefense.IDefenseInterface,
): UnitDefense.IDefenseInterface {
	d = JSON.parse(JSON.stringify(d));
	if (r.shieldReduction && r.shieldReduction.length) {
		r.shieldReduction.forEach((v: number, ix: number) => {
			// reducing defense shields
			if (d.shields && d.shields[ix])
				d.shields[ix].currentAmount = Math.max(0, $$.roundToPrecision(d.shields[ix].currentAmount - v, 0.1));
		});
	}
	// if defense has Boosters and defense is breached for the first time, restore 25% of shield
	if (d.shields && d.shields.length && d[UnitDefense.defenseFlags.boosters] && r.breach) {
		d.shields[0] = DefenseManager.addShields(d.shields[0], d.shields[0].shieldAmount / 4);
		delete d[UnitDefense.defenseFlags.boosters];
	}
	return d;
}

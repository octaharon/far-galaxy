import $ from '../../core/InterfaceTypes';
import Weapons from '../../technologies/types/weapons';
import { TEffectApplianceList } from '../statuses/types';
import Damage from './damage';

namespace UnitAttack {
	export enum attackFlags {
		DoT = 'att_dot', // weapon is damage over time effect
		Selective = 'att_sel', // selective attacks can not be applied to Allied units
		AoE = 'att_aoe', // weapon splashes
		Disruptive = 'att_dis', // weapon ignores shields
		Deferred = 'att_def', // the damage is applied in the end of turn,
		Penetrative = 'att_pen', // hits all targets on trajectory, passes through doodads and destroys high doodads
	}

	export type TAttackFlags = EnumProxy<attackFlags, boolean>;

	export const defaultAttackFlagOrder = [
		attackFlags.Selective,
		attackFlags.AoE,
		attackFlags.DoT,
		attackFlags.Deferred,
		attackFlags.Disruptive,
		attackFlags.Penetrative,
	];

	export enum deliveryType {
		immediate = 'dmg_del_immediate',
		ballistic = 'dmg_del_ballistic', // ballistic trajectory (in gravity field), subsonic speed
		missile = 'dmg_del_missile', // a self-propelled charge
		beam = 'dmg_del_beam', // directed beam which travels at speed of light
		supersonic = 'dmg_del_supersonic', // travelling straightforward at high speed
		drone = 'dmg_del_drone', // actively dodging self-propelled chassis, can target anything
		bombardment = 'dmg_del_bombardment', // falling on the target from above
		surface = 'dmg_del_surface', // spreading on the ground
		cloud = 'dmg_del_cloud', // aoe which affects both ground and aerial targets
		aerial = 'dmg_del_aerial', // affects only aerial targets
	}

	export const TDeliveryTypeArray = Object.values(deliveryType);

	export type TDeliveryTypeGeneric<T> = EnumProxy<deliveryType, T>;
	export type TDeliveryTypeGenericWithDefault<T> = EnumProxyWithDefault<deliveryType, T>;

	export interface IAttackDecorator extends TAttackFlags {
		delivery: deliveryType;
	}

	export enum TAoEDamageSplashTypes {
		lineBehind = 'dmg_splash_behind', // damage in a line behind the target
		lineAside = 'dmg_splash_aside', // damage in a line perpendicular to trajectory at the point of contact
		circle = 'dmg_splash_circle', // damage in a circular area around target
		cone180 = 'dmg_splash_180', // damage in a semicircle behind the target
		cone90 = 'dmg_splash_90', // damage in a 90 deg  area behind the target
		scatter = 'dmg_splash_scatter', // damage random targets around the primary one, magnitude is chance of
		// hitting
		chain = 'dmg_splash_chain', // damage closest target in a given radius, magnitude is number of targets
	}

	export interface IDirectDamageAttackDecorator extends IAttackDecorator {
		[attackFlags.DoT]?: false;
		[attackFlags.Deferred]?: false;
	}

	export interface IDeferredDamageAttackDecorator extends IAttackDecorator {
		[attackFlags.Deferred]: true;
		[attackFlags.DoT]?: false;
		[attackFlags.AoE]?: false;
	}

	export interface IDamageOverTimeAttackDecorator extends IAttackDecorator, $.IDurationDecorator {
		[attackFlags.DoT]: true;
		[attackFlags.Deferred]?: false;
	}

	export interface IAreaOfEffectAttackDecorator extends IAttackDecorator {
		/* scatter / splash / chain reach radius */
		radius: number;
		/* percentage of damage splashed, or random threshold for Scatter, or number of targets per Chain */
		magnitude: number;
		aoeType: TAoEDamageSplashTypes;
		[attackFlags.AoE]: true;
	}

	export type TAttackTypedDecorator =
		| IDirectDamageAttackDecorator
		| IDamageOverTimeAttackDecorator
		| IAreaOfEffectAttackDecorator
		| IDeferredDamageAttackDecorator;

	export type TAttackGeneric<T, D extends TAttackTypedDecorator = IDirectDamageAttackDecorator> = T & D;
	export type TAttackDefinitionGeneric<D extends TAttackTypedDecorator = IDirectDamageAttackDecorator> =
		TAttackGeneric<Damage.TDamageDefinition, D>;
	export type TAttackUnitGeneric<D extends TAttackTypedDecorator = IDirectDamageAttackDecorator> = TAttackGeneric<
		Damage.TDamageUnit,
		D
	>;
	export type TDOTAttackDefinition = TAttackDefinitionGeneric<IDamageOverTimeAttackDecorator>;
	export type TAOEAttackDefinition = TAttackDefinitionGeneric<IAreaOfEffectAttackDecorator>;
	export type TDeferredAttackDefinition = TAttackDefinitionGeneric<IDeferredDamageAttackDecorator>;
	export type TDirectAttackDefinition = TAttackDefinitionGeneric;
	export type TAttackDefinition =
		| TDirectAttackDefinition
		| TDeferredAttackDefinition
		| TAOEAttackDefinition
		| TDOTAttackDefinition;
	export type TAttackDOTUnit = TAttackUnitGeneric<IDamageOverTimeAttackDecorator>;
	export type TAttackAOEUnit = TAttackUnitGeneric<IAreaOfEffectAttackDecorator>;
	export type TAttackDelayedUnit = TAttackUnitGeneric<IDeferredDamageAttackDecorator>;
	export type TAttackDamageUnit = TAttackUnitGeneric;
	export type TAttackUnit = {
		effects?: TEffectApplianceList;
	} & (TAttackAOEUnit | TAttackDOTUnit | TAttackDelayedUnit | TAttackDamageUnit);

	export interface IDamageOverTimeEffect extends IDamageOverTimeAttackDecorator {
		damageSeries: $.TList<TAttackDOTUnit>;
	}

	export interface IAttackInterfaceGeneric<T extends IAttackDecorator> {
		attacks: $.TList<T>;
		priority?: number;
	}

	export interface IAttackInterface extends IAttackInterfaceGeneric<TAttackDefinition> {
		type?: Weapons.TWeaponGroupType;
		caption?: string;
		baseRate: number;
		baseRange: number;
		baseAccuracy: number;
	}

	export interface IAttackBatch extends IAttackInterfaceGeneric<TAttackUnit> {
		range: number;
		accuracy: number;
	}
}

export default UnitAttack;

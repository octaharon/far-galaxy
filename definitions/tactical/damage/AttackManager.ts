import memoize from 'memoize-one';
import _ from 'underscore';
import digest from '../../../src/utils/digest';
import { wellRounded } from '../../../src/utils/wellRounded';
import InterfaceTypes from '../../core/InterfaceTypes';
import $$ from '../../maths/BasicMaths';
import { ArithmeticPrecision, GeometricPrecision } from '../../maths/constants';
import { getDistributionAverage } from '../../maths/Distributions';
import Series from '../../maths/Series';
import Vectors from '../../maths/Vectors';
import UnitChassis from '../../technologies/types/chassis';
import { DEFAULT_EFFECT_POWER, DeferredStatusEffectId, DOTStatusEffectId } from '../statuses/constants';
import StatusEffectMeta from '../statuses/statusEffectMeta';
import { TEffectProps } from '../statuses/types';
import Units from '../units/types';
import UnitAttack from './attack';
import {
	ATTACK_PRIORITIES,
	COMBAT_MAX_ATTACKS_PER_TURN,
	COMBAT_MIN_ATTACKS_PER_TURN,
	COMBAT_TICKS_PER_TURN,
	MOVEMENT_DODGE_BONUS_CAP,
	MOVEMENT_DODGE_MODIFIER,
} from './constants';
import Damage from './damage';
import DamageManager from './DamageManager';
import UnitDefense from './defense';

const dpsSamples = 500;

export default class AttackManager {
	public static getDamagePerTurn = memoize(
		(attack: UnitAttack.IAttackInterface): Damage.TDamageUnit => {
			const nAttacks: Damage.TDamageUnit[] = [];
			for (let i = 0; i < dpsSamples; i++) {
				const attackSamples = new Array(AttackManager.getNumberOfAttacks(attack.baseRate))
					.fill(0)
					.map(() =>
						attack.attacks.reduce(
							(totalDmg, a) =>
								DamageManager.addDamageUnits(
									totalDmg,
									AttackManager.getAvgDamage(a),
									...(AttackManager.isDeferredAttack(a) ? [AttackManager.getAvgDamage(a)] : []),
									...(AttackManager.isDOTAttack(a)
										? new Array(a.duration).fill(AttackManager.getAvgDamage(a))
										: []),
								),
							{},
						),
					);
				nAttacks.push(...attackSamples);
			}
			return Damage.TDamageTypesArray.reduce(
				(dmg, dt) =>
					Object.assign(dmg, {
						[dt]: Series.sum(_.pluck(nAttacks, dt)) / dpsSamples,
					}),
				{} as Damage.TDamageUnit,
			);
		},
		(newArgs: any[], oldArgs: any[]) => {
			const a1 = newArgs[0] as UnitAttack.IAttackInterface;
			const a2 = oldArgs[0] as UnitAttack.IAttackInterface;
			return (
				Math.abs(a1.baseRate - a2.baseRate) < ArithmeticPrecision && digest(a1.attacks) === digest(a2.attacks)
			);
		},
	);
	/**
	 * Calculate a minimal and maximal amount of attacks per given attack rate
	 *
	 * @param {number} weaponRate an attack rate of a Weapon or Attack Interface
	 * @returns {[number,number]} a tuple of minimal and maximal amount of attacks for that attack rate
	 */
	public static getAttacksQuantityMinMax = memoize(
		(weaponRate: number): [number, number] => {
			if (!Number.isFinite(weaponRate) || weaponRate < 0)
				return [COMBAT_MIN_ATTACKS_PER_TURN, COMBAT_MIN_ATTACKS_PER_TURN];
			const transferFunction = (x: number) => Math.log(x + 1) / Math.log(2);
			const t = transferFunction(weaponRate);
			const factor = Math.pow(Math.sin((Math.PI / 7) * Math.min(3.5, t)), 0.9);
			const maxAttacks =
				COMBAT_MIN_ATTACKS_PER_TURN + (COMBAT_MAX_ATTACKS_PER_TURN - COMBAT_MIN_ATTACKS_PER_TURN) * factor;
			const minAttacks = Math.min(
				maxAttacks,
				Math.max(COMBAT_MIN_ATTACKS_PER_TURN, (factor * COMBAT_MAX_ATTACKS_PER_TURN + t) / 2),
			);
			return [Math.round(minAttacks), Math.round(maxAttacks)];
		},
		(args1, args2) => Math.abs(args1[0] - args2[0]) < GeometricPrecision,
	);

	/**
	 * Get number of attacks per turn for given Attack Rate
	 *
	 * @param weaponRate
	 */
	public static getNumberOfAttacks(weaponRate: number): number {
		if (!Number.isFinite(weaponRate) || weaponRate < 0) return 1;
		const [minAttacks, maxAttacks] = AttackManager.getAttacksQuantityMinMax(weaponRate);
		return Math.round(
			$$.clipValue({
				x: $$.skewedNormalRandom(
					minAttacks,
					maxAttacks,
					Math.pow(1 / (weaponRate + ArithmeticPrecision), 0.5),
					GeometricPrecision,
				),
				min: minAttacks,
				max: maxAttacks,
			}),
		);
	}

	public static adjustAccuracyToMovement(
		hitChance: number,
		priority: number,
		targetDirection: InterfaceTypes.IVector,
		targetLocation: InterfaceTypes.IVector,
		fromPoint: InterfaceTypes.IVector,
	): number {
		if (Vectors.isNullVector(targetDirection)) return hitChance;
		const lineOfSight = Vectors.create(fromPoint, targetLocation);
		const attackAngleSine = Math.sqrt(1 - Math.pow(Vectors.angleCosine(targetDirection, lineOfSight), 2));
		const angularProjectedSpeed =
			(Vectors.module()(targetDirection) / Vectors.module()(lineOfSight)) *
			attackAngleSine *
			COMBAT_TICKS_PER_TURN;
		const priorityModifier = $$.clipValue({
			x: 1 + priority / 5,
			max: 2,
			min: 0.2,
		});
		// perpendicular movement at speed 1 distance 1 yields 0.25 dodge against normal priority weapon
		return (
			hitChance -
			Math.min(MOVEMENT_DODGE_BONUS_CAP, MOVEMENT_DODGE_MODIFIER * priorityModifier * angularProjectedSpeed)
		);
	}

	public static isLineOfSightAttack(attackDeliveryType: UnitAttack.deliveryType): boolean {
		return [
			UnitAttack.deliveryType.supersonic,
			UnitAttack.deliveryType.beam,
			UnitAttack.deliveryType.ballistic,
		].includes(attackDeliveryType);
	}

	public static isReachableByAttack(
		attackDeliveryType: UnitAttack.deliveryType,
		targetMovementType: UnitChassis.movementType,
	): boolean {
		switch (targetMovementType) {
			case UnitChassis.movementType.air:
				return ![UnitAttack.deliveryType.surface, UnitAttack.deliveryType.bombardment].includes(
					attackDeliveryType,
				);
			case UnitChassis.movementType.ground:
				return ![UnitAttack.deliveryType.aerial].includes(attackDeliveryType);
			default:
				return ![UnitAttack.deliveryType.surface, UnitAttack.deliveryType.aerial].includes(attackDeliveryType);
		}
	}

	public static getDoTEffectFromAttack(
		attackDefinition: UnitAttack.TDOTAttackDefinition,
	): UnitAttack.IDamageOverTimeEffect {
		return {
			...attackDefinition,
			[UnitAttack.attackFlags.DoT]: true,
			damageSeries: new Array(attackDefinition.duration)
				.fill(0)
				.map(() =>
					AttackManager.getAttackDamage<UnitAttack.TDOTAttackDefinition, UnitAttack.TAttackDOTUnit>(
						attackDefinition,
					),
				),
		};
	}

	public static getAttackBatch(attackInterface: UnitAttack.IAttackInterface, source?: Units.IUnit) {
		const r = {
			accuracy: attackInterface.baseAccuracy,
			range: attackInterface.baseRange,
			priority: attackInterface.priority,
			attacks: [],
		} as UnitAttack.IAttackBatch;
		if (!attackInterface) return null;
		r.attacks = attackInterface.attacks
			.filter((a) => this.isDirectAttack(a) || this.isAOEAttack(a))
			.map((a) => this.getAttackDamage(a));

		const DoTAttacks = attackInterface.attacks.filter(
			AttackManager.isDOTAttack,
		) as UnitAttack.TDOTAttackDefinition[];
		for (const a of DoTAttacks)
			r.attacks.push({
				...this.getAttackDamage(a),
				effects: [
					{
						effectId: DOTStatusEffectId,
						props: {
							state: {
								dotAttacks: AttackManager.getDoTEffectFromAttack(a),
							},
							meta: {
								source: {
									type: StatusEffectMeta.effectAgentTypes.unit,
									unitId: source?.getInstanceId(),
								},
								modifier: {
									abilityDurationModifier: [source?.effectModifiers?.abilityDurationModifier].filter(
										Boolean,
									),
									abilityPowerModifier: [source?.effectModifiers?.abilityPowerModifier].filter(
										Boolean,
									),
								},
							},
							power: source?.abilityPower ?? DEFAULT_EFFECT_POWER,
						} as TEffectProps<StatusEffectMeta.effectAgentTypes.unit>,
					},
				],
			} as UnitAttack.TAttackUnit);
		const DelayedAttacks = attackInterface.attacks.filter(this.isDeferredAttack);
		for (const a of DelayedAttacks)
			r.attacks.push({
				...this.getAttackDamage(a),
				effects: [
					{
						effectId: DeferredStatusEffectId,
						props: {
							state: {
								attackUnit: AttackManager.getAttackDamage(a),
							},
							meta: {
								source: {
									type: StatusEffectMeta.effectAgentTypes.unit,
									unitId: source.getInstanceId(),
								},
								modifier: {
									abilityDurationModifier: [source?.effectModifiers?.abilityDurationModifier].filter(
										Boolean,
									),
									abilityPowerModifier: [source?.effectModifiers?.abilityPowerModifier].filter(
										Boolean,
									),
								},
							},
							power: source?.abilityPower ?? DEFAULT_EFFECT_POWER,
						} as TEffectProps<StatusEffectMeta.effectAgentTypes.unit>,
					},
				],
			});

		return r;
	}

	public static getAttackDamage<
		AttackDefinitionSubtype extends UnitAttack.TAttackDefinition = UnitAttack.TDirectAttackDefinition,
		AttackUnitSubtype extends UnitAttack.TAttackUnit = UnitAttack.TAttackUnit,
	>(attackDefinition: AttackDefinitionSubtype): AttackUnitSubtype {
		let k: Damage.damageType;
		const dmg = Damage.DamageTemplate(0);
		for (k of Damage.TDamageTypesArray)
			if (attackDefinition[k]) dmg[k] = DamageManager.getDamageValue(attackDefinition[k]);
		return Object.assign({} as AttackUnitSubtype, attackDefinition, {
			..._.omit(dmg, (v) => Math.abs(v) < ArithmeticPrecision),
		});
	}

	public static getAvgDamage<T extends UnitAttack.TAttackDefinition = UnitAttack.TDirectAttackDefinition>(
		attackDefinition: T,
	) {
		const dmg = { ...Damage.DamageTemplate(0) };
		Damage.TDamageTypesArray.forEach((damageType) => {
			if (attackDefinition[damageType]) dmg[damageType] = getDistributionAverage(attackDefinition[damageType]);
		});
		return {
			...attackDefinition,
			..._.omit(dmg, (v) => Math.abs(v) < ArithmeticPrecision),
		} as UnitAttack.TAttackUnit;
	}

	public static getAttackFlagsGenericFromAttack<T extends UnitAttack.TAttackTypedDecorator>(
		attackLikeObject: T,
	): UnitAttack.TAttackFlags {
		const r = {} as UnitAttack.TAttackFlags;
		if (!attackLikeObject) return r;
		Object.values(UnitAttack.attackFlags).forEach((k: UnitAttack.attackFlags) => {
			if (attackLikeObject[k]) r[k] = true;
		});
		return r;
	}

	public static isDirectAttack<T extends UnitAttack.IDirectDamageAttackDecorator>(
		attackLikeObject: UnitAttack.TAttackTypedDecorator,
		includeAreaAttacks = false,
	): attackLikeObject is T {
		return (
			!!attackLikeObject &&
			!attackLikeObject[UnitAttack.attackFlags.Deferred] &&
			!attackLikeObject[UnitAttack.attackFlags.DoT] &&
			(includeAreaAttacks || !attackLikeObject[UnitAttack.attackFlags.AoE])
		);
	}

	public static getInterfaceDirectAttacks(attackInterface: UnitAttack.IAttackInterface, includeAreaAttacks = false) {
		return (attackInterface.attacks || []).filter((attack) =>
			AttackManager.isDirectAttack(attack, includeAreaAttacks),
		) as UnitAttack.IDirectDamageAttackDecorator[];
	}

	public static isDOTAttack<
		T extends UnitAttack.IDamageOverTimeAttackDecorator = UnitAttack.IDamageOverTimeAttackDecorator,
	>(attackLikeObject: UnitAttack.TAttackTypedDecorator): attackLikeObject is T {
		return !!attackLikeObject && !!attackLikeObject[UnitAttack.attackFlags.DoT];
	}

	public static isAOEAttack<
		C extends UnitAttack.IAreaOfEffectAttackDecorator = UnitAttack.IAreaOfEffectAttackDecorator,
	>(attackLikeObject: UnitAttack.TAttackTypedDecorator): attackLikeObject is C {
		return attackLikeObject && !!attackLikeObject[UnitAttack.attackFlags.AoE];
	}

	public static isDeferredAttack<T extends UnitAttack.IDeferredDamageAttackDecorator>(
		attackLikeObject: UnitAttack.TAttackTypedDecorator,
	): attackLikeObject is T {
		return !!attackLikeObject && !!attackLikeObject[UnitAttack.attackFlags.Deferred];
	}

	public static isPenetrativeAttack<T extends UnitAttack.IDirectDamageAttackDecorator>(
		attackLikeObject: UnitAttack.TAttackTypedDecorator,
	): attackLikeObject is T {
		return !!attackLikeObject && !!attackLikeObject[UnitAttack.attackFlags.Penetrative];
	}

	public static isDisruptiveAttack(attackLikeObject: UnitAttack.TAttackTypedDecorator): boolean {
		return !!attackLikeObject && !!attackLikeObject[UnitAttack.attackFlags.Disruptive];
	}

	public static isSelectiveAttack(attackLikeObject: UnitAttack.TAttackTypedDecorator): boolean {
		return !!attackLikeObject && !!attackLikeObject[UnitAttack.attackFlags.Selective];
	}

	public static describeAttack(a: UnitAttack.TAttackDefinition) {
		const d = DamageManager.extractDamageGeneric<Damage.IDamageFactor, UnitAttack.TAttackDefinition>(a);
		return (
			`    ${a.delivery}:\n` +
			Object.keys(d).reduce(
				(s, t: Damage.damageType) =>
					s + `    \t${t} ${d[t].min}-${d[t].max} (${getDistributionAverage(d[t])})\n`,
				'',
			)
		);
	}

	public static isAttackEfficient(a: UnitDefense.IAttackEffectOnDefense) {
		return (
			Series.sum(a?.shieldReduction ?? []) + DamageManager.getTotalDamageValue(a?.throughDamage) >
			ArithmeticPrecision
		);
	}

	public static describeDamagePerTurn(a: UnitAttack.IAttackInterface, perAttack = false) {
		const attacksNumber = AttackManager.getAttacksQuantityMinMax(a.baseRate);
		const attackFactor = (atk: UnitAttack.TAttackDefinition) =>
			AttackManager.isAOEAttack(atk) &&
			[UnitAttack.TAoEDamageSplashTypes.scatter, UnitAttack.TAoEDamageSplashTypes.chain].includes(atk.aoeType)
				? atk.magnitude
				: 1;
		const avgDamage = DamageManager.applyDamageModifierToDamageUnit(
			DamageManager.addDamageUnits(
				...a.attacks.map((atk) =>
					DamageManager.applyDamageModifierToDamageUnit(AttackManager.getAvgDamage(atk), {
						factor: attackFactor(atk),
					}),
				),
			),
			{
				factor: perAttack ? 1 : (attacksNumber[0] + attacksNumber[1]) / 2,
			},
		);
		const labels = Damage.TDamageTypesArray.map((dt) => {
			const baseDamage = a.attacks
				.filter((atk) => !!atk[dt])
				.map((atk) =>
					AttackManager.isDOTAttack(atk)
						? [atk[dt].min * atk.duration, atk[dt].max * atk.duration]
						: [
								atk[dt].min * attackFactor(atk) * attacksNumber[0],
								atk[dt].max * attackFactor(atk) * attacksNumber[1],
						  ],
				)
				.reduce((arr: [number, number], atk) => [arr[0] + atk[0], arr[1] + atk[1]], [0, 0] as [number, number]);
			if (baseDamage[0] + baseDamage[1] < ArithmeticPrecision) return null;
			return `${wellRounded(baseDamage[0], 0)}-${wellRounded(baseDamage[1])} ${Damage.getShortCaption(dt)}`;
		}).filter(Boolean);
		return {
			labels,
			value: $$.roundToPrecision(DamageManager.getTotalDamageValue(avgDamage), 1),
		};
	}

	public static describePriority(priority: number) {
		switch (true) {
			case priority <= ATTACK_PRIORITIES.HIGHEST:
				return 'Highest';
			case priority <= ATTACK_PRIORITIES.HIGHER:
				return 'Higher';
			case priority <= ATTACK_PRIORITIES.HIGH:
				return 'High';
			case priority <= ATTACK_PRIORITIES.NORMAL:
				return 'Normal';
			case priority <= ATTACK_PRIORITIES.LOW:
				return 'Low';
			case priority <= ATTACK_PRIORITIES.LOWER:
				return 'Lower';
			default:
				return 'Lowest';
		}
	}
}

# Contribution Guidelines
## Philosophy
We are in no rush. This is more of a waterfall project rather than of an Agile one. A samurai has no goal but the path, and so do we. The game is ready when it's ready. Maybe it's never, that's OK. What is really important is what we learn in the process, while keeping it entertaining, and how do we extend the boundary of our knowledge. If you join, that means you are willing to improve yourself, the community and then the game. Feel free to propose any ideas or help with any issues which spark the joy in you and let you move forward, and don't hesitate to ask for an advise or help with issues.
## How to help
Depending on your skills and commitment you might be able to bring a lot of value to the project. If you have any concerns or ideas you think are of value, create an issue, or text @octaharon directly, if you wish to keep it private
### Developers
If you happen to know Typescript, React or Node, familiar with modern web development stack, you are welcome to look through the issues and see where you might be of use. Any code improvements, extra tests, infrastructure optimizations and other chore have-to-dos are always welcome as PR.

There are some specific things to do which require more intricate knowledge:
- Optimizing and improving GLSL shaders
- Rebuilding few parts of code to WASM and introducing WASM build infrastructure
- Building Combat AI using modern ML technologies and introducing Python infrastructure
  If you are into one of these activities, please discuss the options with @octaharon

### Artists
Any artwork might help. The project needs some UI designs, a lot of illustrations and more icons, textures and cliparts. Most of the art are in the SVG format, and we use it for some animations as well, so if you are an expert on vector graphics, there are most definitely things you could help with. All the digital media you provide to the project should be licensed under CC-BY-SA, and all the authors will be credited. To propose your artwork, please make an Issue with a PR for discussion.

### Sound Designers
The game needs a lot of sound effects, and more soundtracks would never hurt. All authors are credited, and all the media is to be licensed under CC-BY-SA.

Sound operational workflow is currently absent in the game, and we will be happy to onboard a person that could set that up and introduce modern sound engineering techniques.

Before you start working on some soundwork, make sure to plan your options with @octaharon, as he is being personally responsible, among other things, for audio engineering in the project.

### Editors and copyrighters
The story part of the project is the least worked through, at the moment. Meanwhile, text is the most informant media in the game's design. The overall concept and some pieces of scenario are imagined but are not yet structured and published. Apart from that, there are a lot of descriptions for items, game mechanics, technologies etc, to be improved and checked for mistakes. Don't hesitate to contribute a PR or two or join the discussion in issues.

### Publishers and investors
Right now, there is no need for external production, but we could use some resources to hire a few professionals to commit to specific tasks. If you want to donate, please consider it a charity or rather don't.

## How to contribute

### Respect the Docs
No, really. Check out [the docs](https://fargalaxy.notion.site/) before you try. Most of Issues have references to the respective sections. When creating a new Issus, consider leaving a reference yourself

### Use git flow
When contributing, fork from master for a new branch, and merge it again before you push. Submit all your Pull Requests to a master branch.

### Use Issues
If you want to propose any idea, open an Issue or join an existing discussion. Be relevant and concise. If you want to roll into the development, consider taking a 1-Weight existing Issue as a challenge.

### Respect folder structure
Don't move files between folders unless it's  agreed with maintainers explicitly.

When creating new stuff, look for an appropriate directory and try to copy existing structure; most likely it's already there. If not, contact maintainers for an advice

### Save often
Do sync your repo with master frequently. Regularly update the dependencies. However, when doing a lot of changes or updates, consider creating extra intermediary commits instead of a big one, or even fork some incomplete changes to another branch. Some experiments take longer than others, so when working on few branches concurrently, pull master to all of them often.

### Be patient
The games is still in early stage of development, and sometimes there are big changes to the codebase, including renaming the commonly used types and constants. Don't panic, merging is good to train attention.

### Be friendly
Nothing in this world is worth a fight. And this is merely a open source game. Please respect all the contributors and the community, try channeling your anger elsewhere.

### Optimize and refactor
If you are one that enjoys better code for the sake of better code - we appreciate that, go ahead, create a PR. The main principiles to justify is the code is "better", are some of the following:
* less module dependencies
* better performance
* less code
* faster builds
* better looking visuals
* less typos and better grammar
* more test coverage
* more code reusing

### Use linters
The project has [prettier](https://www.npmjs.com/package/prettier) and [eslint](https://www.npmjs.com/package/eslint) set up for most environments, we strongly suggest you import the settings into your IDE. Don't hesitate to make an extra `make lint` before commit. Regardless, husky will run a precommit hook that lints staged files

### Use tests
Run `make test` when making big changes to the codebase, and every time after module updates. Spend some extra time to write some unit tests in `tests` directory, even for the simplest things. PRs with just tests are extremely welcome.

### Use doubt
When in doubt, communicate. Ask questions. There's a lot of information in the game, and many connections within it. Those take time to comprehend, and some things might yet be too complex, - speak up, if you feel so. Point out mistakes. It's normal to disagree. 

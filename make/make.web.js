const {spawn} = require("child_process");
const path = require("path");
const {entries} = require("./sources.web.js");
const {ROOT_DIR} = require("./tools.js");


let ENV = process.argv[2] || process.env.NODE_ENV || 'dev';
if (ENV === 'development')
    ENV = 'dev';
const isDev = ENV === 'dev';
const configName = path.resolve(ROOT_DIR, 'make', 'configs', 'webpack.config.web.js');

const cmd = `npx cross-env  ${isDev ? 'webpack-dev-server' : 'webpack'} --config ${configName} ${isDev ? '--progress --colors -d' : '--display-error-details --display-optimization-bailout -p'} ` + entries.join(' ');
const args = [
    'cross-env',
    `NODE_ENV=${ENV}`,
    isDev ? 'webpack-dev-server' : 'webpack',
    '--config',
    configName,
    isDev ? '--progress --colors -d' : '--display-error-details --display-optimization-bailout -p',
    ...entries
]

console.warn(`Building in ${ENV} mode\n`, entries);

spawn('npx', args, {shell: true, stdio: 'inherit'});
const webpack = require('webpack');
const glob = require('glob');
const nodeExternals = require('webpack-node-externals');
const path = require('path');
const fs = require('fs');

const { DEST_DIR, entries } = require('../sources.srv.js');
const { ROOT_DIR } = require('../tools.js');

const SOURCES = {};
entries.forEach((arg) => {
	if (arg.match(/^\w+=[^=]+$/)) {
		const entry = arg.split('=');
		SOURCES[entry[0]] = entry[1];
	}
});

const Package = require(path.resolve(ROOT_DIR, 'package.json'));

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const isDebug = (env) => ['production', 'prod', 'staging'].indexOf(env) < 0;
const isDev = (env) => ['development', 'dev'].indexOf(env) > -1;

const getPlugins = (env) => {
	let plugs = [];
	if (!isDev(env)) {
		plugs.push(
			new CleanWebpackPlugin({
				root: ROOT_DIR,
				verbose: true,
				dry: false,
			}),
		);
	} else {
		plugs.push(new webpack.NamedChunksPlugin());
	}

	plugs.push(
		new webpack.DefinePlugin({
			__PRODUCTION: JSON.stringify(!isDebug(env)),
		}),
	);
	return plugs;
};

const getDefaultConfig = (env) => ({
	target: 'node',
	stats: 'minimal',
	entry: SOURCES,
	output: {
		path: path.resolve(ROOT_DIR, DEST_DIR),
		publicPath: '/',
		filename: '[name].js',
		globalObject: 'this',
		hashFunction: 'sha256',
	},
	devtool: 'source-map',
	externals: [nodeExternals()],
	module: {
		rules: [
			{
				test: /\.(ts)$/,
				exclude: /(node_modules)/,
				use: [
					{
						loader: 'ts-loader',
					},
				],
			},
			{
				test: /\.js$/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							sourceMap: isDebug(env),
							cacheDirectory: false,
						},
					},
				],
				exclude: /(node_modules)/,
			},
		],
	},
	optimization: {},
	plugins: getPlugins(env),
	resolve: {
		alias: {
			'lodash-es': 'lodash',
			assets: path.resolve(ROOT_DIR, 'assets'),
			'bn.js': path.resolve(ROOT_DIR, 'node_modules', 'bn.js'),
		},
		extensions: ['.ts', '.tsx', '.js', '.jsx'],
	},
});

let config = {},
	env = process.env.NODE_ENV || 'dev';
if (env === 'development') env = 'dev';

try {
	config = require(`./webpack.config.${env}`);
} catch (e) {
	throw new Error('Environment is not defined: ' + env);
	process.exit();
}

module.exports = Object.assign(getDefaultConfig(env), config);

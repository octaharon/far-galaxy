require('@babel/register');

const nodeExternals = require('webpack-node-externals');
const babelOptions = {
	babelrc: true,
	plugins: [
		[
			'@babel/plugin-transform-runtime',
			{
				helpers: true,
				regenerator: true,
			},
		],
		[
			'@babel/plugin-proposal-decorators',
			{
				legacy: true,
			},
		],
		[
			'@babel/plugin-proposal-class-properties',
			{
				loose: true,
			},
		],
		'lodash',
		'@babel/transform-react-constant-elements',
		'@babel/transform-react-inline-elements',
		'transform-react-remove-prop-types',
	],
};

module.exports = {
	target: 'node',
	stats: 'none',
	mode: 'production',
	output: {
		hashFunction: 'sha256',
	},
	externals: [nodeExternals()],
	resolve: {
		alias: {
			'lodash-es': 'lodash',
		},
		extensions: ['.ts', '.tsx', '.js', '.jsx'],
	},
	optimization: {
		concatenateModules: false,
		minimize: false,
	},
	module: {
		rules: [
			{
				test: /\.(tsx)$/,
				use: [
					{
						loader: 'babel-loader',
						options: babelOptions,
					},
					{
						loader: 'ts-loader',
					},
				],
			},
			{
				exclude: /(node_modules)/,
				test: /\.(ts)$/,
				use: [
					{
						loader: 'ts-loader',
					},
				],
			},
			{
				test: /\.jsx?$/,
				use: [
					{
						loader: 'babel-loader',
						options: babelOptions,
					},
				],
				exclude: /(node_modules)/,
			},
		],
	},
};

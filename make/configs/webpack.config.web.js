const webpack = require('webpack');
const glob = require('glob');
const path = require('path');
const fs = require('fs');

const cssModuleName = (env) => (isDev(env) ? '[folder]-[local]' : '[md5:contenthash:hex:8]');

const { DEST_DIR, entries } = require('../sources.web.js');
const { ROOT_DIR } = require('../tools.js');
const SRC_DIR = path.resolve(ROOT_DIR, 'src');

const Package = require(path.resolve(ROOT_DIR, 'package.json'));

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ResourceHintWebpackPlugin = require('resource-hints-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const HtmlWebpackInjector = require('html-webpack-injector');
const SriPlugin = require('webpack-subresource-integrity');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const SOURCES = {};
process.argv.forEach((arg) => {
	if (arg.match(/^\w+=[^=]+$/)) {
		const entry = arg.split('=');
		SOURCES[entry[0]] = entry[1];
	}
});

const isDebug = (env) => ['production', 'prod', 'staging'].indexOf(env) < 0;
const isDev = (env) => ['development', 'dev'].indexOf(env) > -1;

const babelOptions = (env) => ({
	babelrc: true,
	/*"presets": [
        [
            "@babel/preset-env",
            {
                "useBuiltIns": false,
                "spec": false,
                "forceAllTransforms": true,
                "targets": {
                    "browsers": [
                        "last 2 versions",
                    ],
                    "node": "current"
                }
            }
        ],
        "@babel/preset-react"
    ],*/
	plugins: babelPlugins(env),
	sourceMap: isDebug(env),
});

const getPlugins = (env) => {
	let plugs = [
		!isDev(env)
			? new CleanWebpackPlugin({
					root: ROOT_DIR,
					verbose: true,
					dry: false,
			  })
			: null,
		new webpack.DefinePlugin({
			__PRODUCTION: JSON.stringify(!isDebug(env)),
		}),
		new CopyPlugin({
			patterns: [{ from: path.resolve(ROOT_DIR, 'assets', 'root') }],
		}),
	];

	/*if (isDev(env))
        plugs.push(new CircularDependencyPlugin({
            // exclude detection of files based on a RegExp
            exclude: /node_modules/,
            // add errors to webpack instead of warnings
            failOnError: true,
            // set the current working directory for displaying module paths
            cwd: process.cwd(),
        }));*/

	const tplDefault = path.join(SRC_DIR, 'templates', 'default.ejs');
	Object.keys(SOURCES).forEach((fname) => {
		const tplFile = path.join(SRC_DIR, 'templates', `${fname}.ejs`);
		const template = fs.existsSync(tplFile) ? tplFile : tplDefault;
		plugs.push(
			new HtmlWebpackPlugin({
				inject: 'body',
				alwaysWriteToDisk: true,
				scriptLoading: 'defer',
				cache: false,
				chunks: [fname],
				chunksSortMode: 'auto',
				minify: process.env.NODE_ENV === 'production',
				title: fname,
				filename: fname + '.html',
				xhtml: true,
				template,
			}),
		);
	});

	if (!isDev(env))
		plugs.push(
			new MiniCssExtractPlugin({
				filename: '[name].[contenthash].css',
				linkType: 'text/css',
				chunkFilename: '[id].[contenthash].css',
			}),
		);

	plugs = plugs.concat(
		new HtmlWebpackInjector(),
		new HtmlWebpackHarddiskPlugin(),
		new SriPlugin({
			hashFuncNames: ['sha256', 'sha384'],
			enabled: process.env.NODE_ENV === 'production',
		}),
	);

	if (!isDev(env))
		plugs.push(
			new BundleAnalyzerPlugin({
				analyzerMode: 'static',
				openAnalyzer: false,
				generateStatsFile: true,
			}),
		);
	return plugs.filter(Boolean);
};

const cssChain = (env) => [
	{
		loader: 'css-loader',
		options: {
			importLoaders: 1,
			modules: {
				localIdentName: cssModuleName(env),
			},
			url: true,
			sourceMap: isDebug(env),
		},
	},
	/*{
    loader: 'postcss-loader',
    options: {
      modules: true,
      ident: 'postcss',
      sourceMap: isDebug(env),
    },
  },*/
	{
		loader: 'sass-loader',
		options: {
			sourceMap: isDebug(env),
			implementation: require('sass'),
		},
	},
];

const styleConfig = (env) => {
	if (isDev(env)) return ['style-loader'].concat(cssChain(env));
	else return [MiniCssExtractPlugin.loader].concat(cssChain(env));
};

const babelPlugins = (env) => {
	let plugs = [
		[
			'@babel/plugin-transform-runtime',
			{
				helpers: true,
				regenerator: true,
			},
		],
		[
			'@babel/plugin-proposal-decorators',
			{
				legacy: true,
			},
		],
		[
			'@babel/plugin-proposal-class-properties',
			{
				loose: true,
			},
		],
		[
			'@babel/plugin-proposal-private-methods',
			{
				loose: true,
			},
		],
		[
			'@babel/plugin-proposal-private-property-in-object',
			{
				loose: true,
			},
		],
		'@babel/plugin-proposal-optional-chaining',
		'@babel/plugin-proposal-nullish-coalescing-operator',
		'lodash',
	];
	if (isDev(env)) plugs.push('react-hot-loader/babel');
	if (!isDebug(env))
		plugs = plugs.concat([
			'@babel/transform-react-constant-elements',
			'@babel/transform-react-inline-elements',
			'transform-react-remove-prop-types',
		]);
	return plugs;
};

const getFileLoaders = (env) => {
	const getAssetPath = (type) =>
		`${type}/${isDebug(env) ? '[name].' : ''}[sha512:hash:hex:${isDebug(env) ? 8 : 12}].[ext]`;
	return [
		{
			test: /\.(mov|mp4|avif|webp)$/,
			use: [
				{
					loader: 'file-loader',
					options: {
						name: getAssetPath('video'),
						hash: 'sha512',
						digest: 'hex',
					},
				},
			],
		},
		{
			test: /\.(mp3)$/,
			use: [
				{
					loader: 'file-loader',
					options: {
						name: getAssetPath('music'),
						hash: 'sha512',
						digest: 'hex',
					},
				},
			],
		},
		{
			test: /\.(wav|ogg)$/,
			use: [
				{
					loader: 'file-loader',
					options: {
						name: getAssetPath('sfx'),
						hash: 'sha512',
						digest: 'hex',
					},
				},
			],
		},
		{
			test: /\.(glb|gltf|dae)$/,
			use: [
				{
					loader: 'file-loader',
					options: {
						name: getAssetPath('models'),
					},
				},
			],
		},
		{
			test: /\.(ttf|eot|woff)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
			use: {
				loader: 'file-loader',
				options: {
					name: getAssetPath('fonts'),
					hash: 'sha512',
					digest: 'hex',
				},
			},
		},
	];
};

const getDefaultConfig = (env) => ({
	target: 'web',
	stats: 'minimal',
	context: SRC_DIR,
	entry: SOURCES,
	output: {
		path: path.resolve(ROOT_DIR, DEST_DIR),
		publicPath: '',
		filename: isDebug(env) ? '[id].js' : '[contenthash].js',
		hashFunction: 'sha256',
	},
	module: {
		rules: [
			{
				test: /\.ejs$/,
				use: {
					loader: 'ejs-compiled-loader',
					options: {
						htmlmin: true,
						htmlminOptions: {
							removeComments: true,
						},
					},
				},
			},
			{
				test: /\.(sa|sc|c)ss$/,
				use: styleConfig(env),
			},
			...getFileLoaders(env),
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'img/[name].[hash].[ext]',
							hash: 'sha512',
							esModule: false,
							digest: 'hex',
						},
					},
					{
						loader: 'image-webpack-loader',
						options: {
							optipng: {
								enabled: !isDev(env),
								optimizationLevel: 7,
							},
							gifsicle: {
								interlaced: false,
							},
							pngquant: {
								quality: [0.65, 0.9],
								speed: 4,
							},
							mozjpeg: {
								progressive: true,
								quality: 65,
							},
							webp: {
								quality: 65,
							},
							svgo: {
								full: true,
								plugins: [
									'removeComments',
									'removeMetadata',
									'removeTitle',
									'removeDesc',
									'removeEditorsNSData',
									'cleanupEnableBackground',
									'removeRasterImages',
									'removeDoctype',
									'removeXMLProcInst',
									'convertStyleToAttrs',
									'cleanupIDs',
									'removeUselessStrokeAndFill',
									'convertPathData',
									'removeEmptyText',
									'collapseGroups',
									'convertColors',
									'sortAttrs',
								],
							},
						},
					},
				],
			},
			{
				exclude: /(node_modules)/,
				test: /\.(tsx)$/,
				use: [
					{
						loader: 'babel-loader',
						options: babelOptions(env),
					},
					{
						loader: 'ts-loader',
						options: {
							transpileOnly: isDev(env),
						},
					},
				],
			},
			{
				exclude: /(node_modules)/,
				test: /\.(ts)$/,
				use: [
					{
						loader: 'ts-loader',
						options: {
							transpileOnly: isDev(env),
						},
					},
				],
			},
			{
				test: /\.jsx?$/,
				use: [
					{
						loader: 'babel-loader',
						options: babelOptions(env),
					},
				],
				exclude: /(node_modules)/,
			},
			{
				test: /\.mjs$/,
				use: [
					{
						loader: 'babel-loader',
						options: babelOptions(env),
					},
				],
			},
		],
	},
	optimization: {
		splitChunks: {
			chunks: 'all',
			minSize: 1024 * 1024,
			automaticNameDelimiter: '-',
			maxInitialRequests: 2,
			name: true,
			cacheGroups: {
				vendors: {
					test: /node_modules/,
					enforce: true,
					minChunks: 1,
					priority: 10,
					reuseExistingChunk: true,
				},
				default: {
					minChunks: 1,
					maxSize: 4 * 1024 * 1024,
					reuseExistingChunk: true,
				},
			},
		},
	},
	plugins: getPlugins(env),
	resolve: {
		alias: {
			'lodash-es': 'lodash',
			assets: path.resolve(ROOT_DIR, 'assets'),
			'bn.js': path.resolve(ROOT_DIR, 'node_modules', 'bn.js'),
		},
		extensions: ['.ts', '.tsx', '.js', '.jsx'],
	},
});

let config = {},
	env = process.env.NODE_ENV || 'dev';
if (env === 'development') env = 'dev';

try {
	config = require(`./webpack.config.${env}`);
} catch (e) {
	throw new Error('Environment is not defined: ' + env);
	process.exit();
}

module.exports = Object.assign(getDefaultConfig(env), config);

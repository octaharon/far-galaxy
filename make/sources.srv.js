const glob = require("glob");
const path = require("path");
const {prepareEntries, ROOT_DIR} = require("./tools.js");

const sources = process.argv.slice(3);

const serverSources = glob.sync(path.join(ROOT_DIR, 'server', '*.ts')).reduce((names, file) => ({
    ...names,
    [path.basename(file, '.ts')]: file
}), {});

const toolsSources = glob.sync(path.join(ROOT_DIR, 'tools', '*.ts')).reduce((names, file) => ({
    ...names,
    [path.basename(file, '.ts')]: file
}), {});

const allSources = {
    ...serverSources,
    ...toolsSources
};

module.exports = {
    entries: prepareEntries(allSources, sources),
    DEST_DIR: path.resolve(ROOT_DIR, 'build/server')
}
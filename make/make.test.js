const { spawn } = require('child_process');
const path = require('path');
const { entries } = require('./sources.tests.js');
const { ROOT_DIR } = require('./tools.js');

const args = [
	`mochapack`,
	'--webpack-config ./make/configs/webpack.config.tests.js',
	'--check-leaks',
	'--reporter mochawesome',
	'--include isomorphic-fetch',
	'--parallel',
	'--quiet',
	'--reporter-options reportDir=./build/tests,reportFilename=testReport',
	'',
	...entries.map((filename) => path.relative('.', filename)),
];

spawn('clear && npx', args, { shell: true, stdio: 'inherit' });

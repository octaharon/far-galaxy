const { ROOT_DIR } = require('./tools.js');
const { DEST_DIR, entries } = require('./sources.srv.js');
const path = require('path');
const { spawn } = require('child_process');

let ENV = process.argv[2] || process.env.NODE_ENV || 'dev';
if (ENV === 'development') ENV = 'dev';
const isDev = ENV === 'dev';
const configName = path.resolve(ROOT_DIR, 'make', 'configs', 'webpack.config.js');
const isSingleSource = isDev && entries.length === 1;
const args = [
	'cross-env',
	`NODE_ENV=${ENV}`,
	`webpack`,
	`--config ${configName}`,
	isSingleSource ? '' : ' --progress --colors --display-error-details --display-optimization-bailout',
	isDev ? ' -d' : ' -p',
	...entries,
	isSingleSource
		? ` && node -r dotenv/config ${path.resolve(DEST_DIR, entries[0].split('=')[0].trim() + '.js')}`
		: '',
];

console.warn(`Building in ${ENV} mode\n`, entries);

spawn('npx', args, { shell: true, stdio: 'inherit' });

const path = require('path');
const url = require('url');

module.exports = {
    ROOT_DIR: path.resolve(
        path.dirname(
            __filename//url.fileURLToPath(__filename),
        ),
        '..',
    ),
    prepareEntries: (allSources, requestedSources) => {
        const entries = [];
        Object.keys(allSources).forEach(src => {
            if (requestedSources.length && !requestedSources.includes(src))
                delete allSources[src];
            else
                entries.push(`${src}=${allSources[src]}`)
        });
        return entries;
    }
};
const glob = require("glob");
const path = require("path");
const {prepareEntries, ROOT_DIR} = require("./tools.js");

const sources = process.argv.slice(3);

const allSources = glob.sync(path.resolve(ROOT_DIR, 'src', 'pages', '**/*.tsx')).reduce((names, file) => ({
    ...names,
    [path.basename(file, '.tsx')]: file
}), {});

module.exports = {
    entries: prepareEntries(allSources, sources),
    DEST_DIR: path.resolve(ROOT_DIR, 'build/web')
};

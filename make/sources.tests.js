const glob = require("glob");
const path = require("path");
const _ = require("underscore");
const {ROOT_DIR} = require("./tools.js");

const sources = process.argv.slice(2).map(v => v.trim()).filter(Boolean);

const allSources = glob
    .sync(path.join(ROOT_DIR, 'tests', '**/*.test.ts'))
    .reduce((names, file) => ({
        ...names,
        [path.basename(file, '.ts').replace(/\.?test\.?/, '')]: file
    }), {});
module.exports = {
    entries: Object
        .values(
            _.filter(
                allSources,
                (filename, filekey) => sources.length ?
                    sources.includes(filekey) :
                    true
            )
        ),
    DEST_DIR: 'build/tests'
}

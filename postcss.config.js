module.exports = {
	map: true,
	parser: 'sugarss',
	plugins: ['postcss-preset-env', 'postcss-cssnext', 'cssnano'],
};

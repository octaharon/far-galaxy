# Far Galaxy

A wannabe turn based tactics game made with [Typescript](https://www.typescriptlang.org/), [React](https://reactjs.org/), [SVG](https://developer.mozilla.org/en-US/docs/Web/SVG) and [Three.js](https://threejs.org/), currently an educational project.

[[_TOC_]]

## Documentation
See 📚[Rulebook](https://fargalaxy.notion.site/)

## Contributing
Read the [Contribution Guidelines](CONTRIBUTING.md) to better understand the philosophy of the project.

All contributions are welcome, but especially those which are listed in
[Issues](https://gitlab.com/octaharon/far-galaxy/-/issues)

## Installation
```shell
make install
```
To maintain dependencies, we highly recommend to install [npm-check](https://www.npmjs.com/package/npm-check) globally:
```shell 
npm i -g npm-check
npm-check -u 
```
If you want to install different versions, use `npm install --force`, since some currently used versions are formally incompatible.

## Testing
```shell
make test [suites...]
```
* *suites* - one or many files from `./tests`, without `.test.ts`. If not specified, all tests are performed.

After **installation**, [husky](https://www.npmjs.com/package/husky) hooks ensure that tests are run automatically on commit. If you want to skip them, use `gti commit --no-verify`, but only to a dedicated branch, which should never be merged to `master`. 

More tests are always welcome. Particularly, any new code added to `/definitions` **must** have tests included in MR to be reviewed.

## Building tools and server scripts
```shell
make srv [env] [scripts...]
```

* *env* - **dev** or **prod**. When running a single script in **dev**, it's also executed after build. Default is **dev**.
* *scripts* - one or many files from `./tools` or `./server`, without extension. If not specified, all files are built.

For instance:
```shell
# Run sample map generator
make srv dev mapGeneratorTest

# Build all scripts to ./build/server 
make srv prod 
```

### Working tools
Use with `make srv dev <name>`:
* Prepare sprites and vectors - **necessary part of a build process**
    * _convertChassisImages_
    * _createBuildingSprites_
* Upload collectible [Technologies](https://fargalaxy.notion.site/Technologies-1aa2dadc39d2434480aa5dad78625c1f), [Effects](https://fargalaxy.notion.site/Status-Effects-b2483da1e64149d9a521eef94508baba) and [Abilities](https://fargalaxy.notion.site/Abilities-224a77e71b75485ba47719316c8a224d) to Notion:
  * _syncWeapons_
  * _syncArmor_
  * _syncShields_
  * _syncEquipment_
  * _syncFactories_
  * _syncEstablishments_
  * _syncFacilities_
  * _syncAuras_
  * _syncEffects_
  * _syncAbilities_
* Export above to CSV:
  * _exportAbilities_
  * _exportArmor_
  * _exportShields_
  * _exportChassis_
  * _exportEstablishments_
  * _exportFacilities_
  * _exportFactories_
  * _exportEstablishments_
* Export [Talent](https://fargalaxy.notion.site/Talents-3ce31fa3ae2e445494e2d625acc9e0db) and [Research](https://fargalaxy.notion.site/Research-42dcd0435fdd40929c39952257468d39) graphs (as [mermaid](https://mermaid-js.github.io/) Markdown, for use in GitLab)
  * _exportTalents_
  * _exportResearch_
* Calculate availability of Technologies:
  * _scanTalents_
  * _scanResearch_
* Calculating the total possible number of [Prototype](https://fargalaxy.notion.site/Prototypes-7b5362a9b00a4a4281669011ebd79a8d) variants
  * _scanTechnologies_
* Build a HTML with sample randomly generated [Maps](https://fargalaxy.notion.site/Map-f65d2d7ae2fe483b880da155c9149d24) (saved to `/mapTest.html`):
  * _mapGeneratorTest_
* Sample effect of a [Weapon](https://fargalaxy.notion.site/Weapons-bd4ea6175b1b4af2b183ab6791ceaba0) against a preset [Protection](https://fargalaxy.notion.site/Damage-Reduction-ccdfd0ef4bd441d497bc78e3e86d69f6):
  * _sampleDamageModel_
* Print a Markdown table with sampled [Random](https://fargalaxy.notion.site/Random-Values-0d517f08a8fc4abfa8dc66d2df03ca2a) values:
  * _printDistributions_
  * _printAttackRates_
* Print a table with connection between [Chassis](https://fargalaxy.notion.site/Chassis-dd17fb42b9fa4e5a9f06df4d8736a681) and [Factories](https://fargalaxy.notion.site/Factories-927a9b69194e4794a28f02bceef5d98e)
  * _printFactoryChassisMapping_
* Generate a sample galaxy with [Planets](https://fargalaxy.notion.site/Planets-a76dc49eccee487a901862a17446b16c) and [Resources](https://fargalaxy.notion.site/Resources-a1783583bd894f41b794b4b3e7050615) distribution:
  * _generatePlanets_
* Generate a random [Pirate](https://fargalaxy.notion.site/Pirates-271166f5218248e3b0ef7f828d96efec) Unit and test its serialization
  * _genericUnitTest_


## Building web pages
```shell
make web [env] [pages...]
```

* *env* - **dev** or **prod**. When running in **dev**, *webpack-dev-server* is started to serve built pages on `127.0.0.1:8080`. Default is **dev**.
* *pages* - one or many files from `./src/pages`, without extensions. If not specified, all files are built.

For instance:
```shell
# Build and serve the page at http://127.0.0.1:8080/BaseLayout.html
make web dev BaseLayout

# Build all pages to ./build/web 
make web prod 
```

### Working Pages
Use with `make web dev <PageId>`

| PageId              | Description                                                                   | Progress         |
|:--------------------|:------------------------------------------------------------------------------|:-----------------|
| _AbilityShowcase_   | A list of all Abilities                                                       | Done (+++)       |
| _AuraShowcase_      | A list of all Auras                                                           | Done (+++)       |
| _ArmorShowcase_     | A list of all Armor                                                           | Done (+++)       |
| _ShieldShowcase_    | A list of all Shields                                                         | Done (+++)       |
| _ChassisShowcase_   | A list of all Chassis                                                         | Done (+++)       |
| _EquipmentShowcase_ | A list of all Equipment                                                       | Done (+++)       |
| _WeaponShowcase_    | A list of all Shields                                                         | Done (+++)       |
| _PlanetTest_        | A 3D render of a Planet with configurable Parameters                          | Mostly Done (++) |
| _BaseLayout_        | **Player GUI**: an interface for a Player to control an Orbital Base          | WIP     (+)      |
| _Settings_          | a sample interface of a popup to control graphics and sound settings          | WIP        (+)   |
| _PrototypeEditor_   | **Player GUI**: an interface for a Player to edit a Prototype                 | WIP        (+)   |
| _MainMenu_          | **Player GUI**: a starting page of the game, used as a gateway to other pages | Mostly Done (++) |
| _UIKitTest_         | A test page, containing almost all common UI elements                         | Mostly Done (++) |
| _MapTest_           | **Player GUI**: a sample Combat Map interface                                 | Almost None      |

import { Client } from '@notionhq/client';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../definitions/core/DI/injections';
import { DICollectibleType } from '../../definitions/core/DI/types';
import {
	AbilityDatabaseId,
	ArmorDatabaseId,
	AuraDatabaseId,
	EffectDatabaseId,
	EquipmentDatabaseId,
	EstablishmentDatabaseId,
	FacilitiesDatabaseId,
	FactoriesDatabaseId,
	ShieldDatabaseId,
	WeaponDatabaseId,
} from './constants';
import { getDatabaseCollectionIdMap } from './dataHelpers';

function createCollectibleArray<T extends DIInjectableCollectibles, O extends DICollectibleType<T>>(
	type: T,
	afterInstantiate?: (instance: O) => O,
): O[] {
	return DIContainer.getProvider(type)
		.getIds()
		.map((collectibleId) => {
			const instance = DIContainer.getProvider(type).instantiate(null, collectibleId) as O;
			if (afterInstantiate) return afterInstantiate(instance);
			return instance;
		});
}

export const createAbilityArray = () => createCollectibleArray(DIInjectableCollectibles.ability, (ab) => ab.init());
export const createFactoryArray = () => createCollectibleArray(DIInjectableCollectibles.factory);
export const createEstablishmentArray = () => createCollectibleArray(DIInjectableCollectibles.establishment);
export const createFacilityArray = () => createCollectibleArray(DIInjectableCollectibles.facility);
export const createEquipmentArray = () => createCollectibleArray(DIInjectableCollectibles.equipment);
export const createEffectArray = () => createCollectibleArray(DIInjectableCollectibles.effect);
export const createAuraArray = () => createCollectibleArray(DIInjectableCollectibles.aura);
export const createArmorArray = () => createCollectibleArray(DIInjectableCollectibles.armor);
export const createShieldArray = () => createCollectibleArray(DIInjectableCollectibles.shield);
export const createWeaponArray = () => createCollectibleArray(DIInjectableCollectibles.weapon);

export async function createAuraMapping(notion: Client) {
	console.log('Creating a Collection Map for Auras...');
	const abilityPageMap = await getDatabaseCollectionIdMap(notion, AuraDatabaseId);
	console.log('Built Collection Map for Auras:', abilityPageMap);
	return abilityPageMap;
}

export async function createAbilityMapping(notion: Client) {
	console.log('Creating a Collection Map for Abilities...');
	const abilityPageMap = await getDatabaseCollectionIdMap(notion, AbilityDatabaseId);
	console.log('Built Collection Map for Abilities:', abilityPageMap);
	return abilityPageMap;
}

export async function createFactoryMapping(notion: Client) {
	console.log('Creating a Collection Map for Factory...');
	const factoryPageMap = await getDatabaseCollectionIdMap(notion, FactoriesDatabaseId);
	console.log('Built Collection Map for Factory:', factoryPageMap);
	return factoryPageMap;
}

export async function creatFacilityMapping(notion: Client) {
	console.log('Creating a Collection Map for Facility...');
	const facilityPageMap = await getDatabaseCollectionIdMap(notion, FacilitiesDatabaseId);
	console.log('Built Collection Map for Facility:', facilityPageMap);
	return facilityPageMap;
}

export async function createEstablishmentMapping(notion: Client) {
	console.log('Creating a Collection Map for Establishment...');
	const establishmentPageMap = await getDatabaseCollectionIdMap(notion, EstablishmentDatabaseId);
	console.log('Built Collection Map for Establishment:', establishmentPageMap);
	return establishmentPageMap;
}

export async function createEquipmentMapping(notion: Client) {
	console.log('Creating a Collection Map for Equipment...');
	const equipmentPageMap = await getDatabaseCollectionIdMap(notion, EquipmentDatabaseId);
	console.log('Built Collection Map for Equipment:', equipmentPageMap);
	return equipmentPageMap;
}

export async function createEffectMapping(notion: Client) {
	console.log('Creating a Collection Map for Effects...');
	const equipmentPageMap = await getDatabaseCollectionIdMap(notion, EffectDatabaseId);
	console.log('Built Collection Map for Effects:', equipmentPageMap);
	return equipmentPageMap;
}

export async function createArmorMapping(notion: Client) {
	console.log('Creating a Collection Map for Armor...');
	const armorPageMap = await getDatabaseCollectionIdMap(notion, ArmorDatabaseId);
	console.log('Built Collection Map for Armor:', armorPageMap);
	return armorPageMap;
}

export async function createShieldMapping(notion: Client) {
	console.log('Creating a Collection Map for Shield...');
	const armorPageMap = await getDatabaseCollectionIdMap(notion, ShieldDatabaseId);
	console.log('Built Collection Map for Shield:', armorPageMap);
	return armorPageMap;
}

export async function createWeaponMapping(notion: Client) {
	console.log('Creating a Collection Map for Weapons...');
	const weaponPageMap = await getDatabaseCollectionIdMap(notion, WeaponDatabaseId);
	console.log('Built Collection Map for Weapons:', weaponPageMap);
	return weaponPageMap;
}

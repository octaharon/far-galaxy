import { describeBuildingModifier } from '../../definitions/orbital/base/helpers';
import { TTypedBaseBuilding } from '../../definitions/orbital/types';
import AttackManager from '../../definitions/tactical/damage/AttackManager';
import Damage from '../../definitions/tactical/damage/damage';
import UnitDefense from '../../definitions/tactical/damage/defense';
import DefenseManager from '../../definitions/tactical/damage/DefenseManager';
import {
	DefenseFlagCaptions,
	describeAttackDelivery,
	describeAttackTypes,
} from '../../definitions/tactical/damage/strings';
import { IWeapon } from '../../definitions/technologies/types/weapons';
import { describeResourcePayload } from '../../definitions/world/resources/helpers';
import Resources from '../../definitions/world/resources/types';
import { predicateSelection } from '../utils/predicateChoices';

export type TNotionTextColors =
	| 'default'
	| 'gray'
	| 'brown'
	| 'orange'
	| 'yellow'
	| 'green'
	| 'blue'
	| 'purple'
	| 'pink'
	| 'red';

export type TNotionLabel = { name: string; color?: TNotionTextColors };

export function mapDamageTypeToNotionColor(dt: Damage.damageType): TNotionTextColors {
	const mapping: Damage.TDamageGeneric<TNotionTextColors> = {
		[Damage.damageType.thermodynamic]: 'pink',
		[Damage.damageType.biological]: 'green',
		[Damage.damageType.heat]: 'orange',
		[Damage.damageType.antimatter]: 'red',
		[Damage.damageType.warp]: 'default',
		[Damage.damageType.radiation]: 'purple',
		[Damage.damageType.magnetic]: 'blue',
		[Damage.damageType.kinetic]: 'yellow',
	};
	return mapping?.[dt] || 'gray';
}

export function sanitizeCollectibleName(name: string) {
	return name
		.split(' ')
		.map((e) => e.slice(0, 1).toUpperCase() + e.slice(1))
		.join(' ');
}

export function sanitizeProtection(protection: UnitDefense.TArmor): TNotionLabel[] {
	return DefenseManager.describeProtection(protection)
		.split('|')
		.map((s) => s.trim())
		.map((v) => ({
			name: v,
			color: mapDamageTypeToNotionColor(
				Damage.TDamageTypesArray.find((dt) => v.includes(Damage.shortCaptions[dt])),
			),
		}));
}

export function sanitizeDPT(weapon: IWeapon): TNotionLabel[] {
	return AttackManager.describeDamagePerTurn(weapon)
		.labels.map((s) => s.trim())
		.map((v) => ({
			name: v,
			color: mapDamageTypeToNotionColor(
				Damage.TDamageTypesArray.find((dt) => v.includes(Damage.shortCaptions[dt])),
			),
		}));
}

export function sanitizeDPSTotal(weapon: IWeapon): number {
	return AttackManager.describeDamagePerTurn(weapon, true).value;
}

export function sanitizeAttackTypes(dmg: IWeapon): TNotionLabel[] {
	return describeAttackTypes(dmg)
		.map((s) => s.trim())
		.map((v) => ({
			name: v,
		}));
}

export function sanitizeAttackTransfer(dmg: IWeapon): TNotionLabel[] {
	return describeAttackDelivery(dmg)
		.map((s) => s.trim())
		.map((v) => ({
			name: v,
		}));
}

export function sanitizePerks(protection: UnitDefense.TDefenseFlags) {
	return Object.keys(protection)
		.filter((k) => !!protection[k])
		.map((k) => DefenseFlagCaptions[k] ?? DefenseFlagCaptions.default);
}

export function sanitizeCollectibleDescription(desc: string) {
	return desc?.replace(/\s*×\s*/giu, '×').replace(/\s+/gim, ' ');
}

export function sanitizeResourcePayload(r: Resources.TResourcePayload) {
	return describeResourcePayload(r)
		.split(',')
		.map((s) => s.trim())
		.filter(Boolean)
		.map((text) => ({
			name: text.trim(),
			color: predicateSelection<TNotionTextColors, string>([
				['blue', (t) => t.toLowerCase().includes('nitrogen')],
				['purple', (t) => t.toLowerCase().includes('hydrogen')],
				['yellow', (t) => t.toLowerCase().includes('metals')],
				['pink', (t) => t.toLowerCase().includes('halogens')],
				['red', (t) => t.toLowerCase().includes('isotopes')],
				['brown', (t) => t.toLowerCase().includes('carbon')],
				['green', (t) => t.toLowerCase().includes('organics')],
				['orange', (t) => t.toLowerCase().includes('oxygen')],
				['gray', (t) => t.toLowerCase().includes('helium')],
				['default', (t) => t.toLowerCase().includes('minerals')],
				['default', Boolean],
			])(text.trim()),
		})) as TNotionLabel[];
}

export function sanitizeBuildingModifier(building: TTypedBaseBuilding) {
	return describeBuildingModifier(building).map((text) => ({
		name: text,
		color: predicateSelection<TNotionTextColors, string>([
			['blue', (t) => t.toLowerCase().includes('energy')],
			['purple', (t) => t.toLowerCase().includes('raw materials')],
			['purple', (t) => t.toLowerCase().includes('ability power')],
			['yellow', (t) => t.toLowerCase().includes('recon')],
			['yellow', (t) => t.toLowerCase().includes('bay')],
			['pink', (t) => t.toLowerCase().includes('repair')],
			['red', (t) => t.toLowerCase().includes('hit points')],
			['brown', (t) => t.toLowerCase().includes('armor')],
			['green', (t) => t.toLowerCase().includes('engineering')],
			['orange', (t) => t.toLowerCase().includes('research')],
			['gray', (t) => t.toLowerCase().includes('production capacity')],
			['red', (t) => t.toLowerCase().includes('consumption')],
			['green', (t) => t.toLowerCase().includes('output')],
			['default', Boolean],
		])(text),
	})) as TNotionLabel[];
}

import { Client } from '@notionhq/client';
import { QueryDatabaseResponse } from '@notionhq/client/build/src/api-endpoints';
import { delayMS } from '../utils/delayMS';
import _ from 'underscore';
import { splitIntoChunks } from '../utils/splitIntoChunks';
import { TNotionLabel } from './valueHelpers';

async function getRemotePagesWithOffset(notion: Client, dBID: string, cursor: string) {
	return notion.databases.query({ database_id: dBID, start_cursor: cursor });
}

export async function getRemotePagePropertyById(notion: Client, pageId: string, propertyId: string) {
	if (!propertyId) return null;
	return notion.pages.properties.retrieve({ page_id: pageId, property_id: propertyId });
}

export async function updateDatabaseSelectOptions(
	notion: Client,
	dbId: string,
	propertyName: string,
	options: TNotionLabel[],
) {
	console.info(`Updating ${propertyName} property with ${options.length} options`);
	return notion.databases.update({
		database_id: dbId,
		properties: {
			[propertyName]: {
				multi_select: {
					options,
				},
			},
		},
	});
}

export async function getRemotePagesByID(notion: Client, dBID: string) {
	let compl: QueryDatabaseResponse['results'] = [];
	let newCurs;

	do {
		const r: QueryDatabaseResponse = await getRemotePagesWithOffset(notion, dBID, newCurs);
		compl = compl.concat(r.results);
		newCurs = r.next_cursor;
	} while (newCurs);
	return compl;
}

export async function deleteRemotePage(notion: Client, pageId: string) {
	console.log('Removing page :', pageId);
	return notion.pages.update({
		page_id: pageId,
		archived: true,
	});
}

export async function clearRemotePageset(notion: Client, dBID: string) {
	const pageset = await getRemotePagesByID(notion, dBID);
	await Promise.all(pageset.map((e) => deleteRemotePage(notion, e.id)));
}

export const throttledPromises = <T, K>(
	asyncFunction: (i: T, ix?: number, arr?: T[]) => Promise<K>,
	items: T[] = [],
	keyingFunction: (value: T) => string = String,
	batchSize = 2,
	delay = 400,
) => {
	async function asyncForEach<P>(array: P[], callback: (i: P, ix?: number, arr?: P[]) => any) {
		for (let index = 0; index < array.length; index++) {
			await callback(array[index], index, array);
		}
	}

	return new Promise<Record<string, K>>((resolve, reject) => {
		const output: Record<string, K> = {};
		const batches = splitIntoChunks(items, batchSize);
		let counter = 0;
		asyncForEach(batches, async (batch, index) => {
			console.log(
				`Requesting batch ${index + 1} of ${batches.length}, entries ${counter + 1} to ${(counter +=
					batch.length)}...`,
			);
			const promises = batch.map(asyncFunction);
			const results = await Promise.all(promises);
			batch.forEach((item, ix) => {
				output[keyingFunction(item)] = results[ix];
			});
			await delayMS(delay);
		})
			.then(() => resolve(output))
			.catch(reject);
	});
};

export async function getDatabaseCollectionIdMap(notion: Client, dbID: string) {
	console.log(`Retrieving Collection page list from DB ${dbID}...`);
	const notionBD = await getRemotePagesByID(notion, dbID);
	console.log(`Total of ${notionBD.length} pages, retrieving Collection IDs...`);
	const collectionIds = await throttledPromises(
		(page) =>
			//@ts-ignore
			getRemotePagePropertyById(notion, page.id, page.properties?.ID?.id).then((r) =>
				//@ts-ignore
				String(r?.['number'] ?? r?.id ?? '0'),
			),
		notionBD,
		(item) => item.id,
	);
	return _.mapObject(collectionIds, Number);
}

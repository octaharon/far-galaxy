import UnitChassis from '../../definitions/technologies/types/chassis';
import Weapons from '../../definitions/technologies/types/weapons';

export const AbilityDatabaseId = '4c510a0181ad40a6be313c47e0b8c2dd';
export const EquipmentDatabaseId = 'ef2042ce838c41f0af6bd2b5da75b8b2';
export const AuraDatabaseId = 'e4c275c82061455f8a9fa68b2bf12f3a';
export const EffectDatabaseId = 'b332f2dbd9c74e60a1d792a246341eff';
export const ArmorDatabaseId = 'fc16dbc98fda411db6739d2f11054822';
export const ShieldDatabaseId = 'c4d158ee1e5e43a9860c36cdd8004f9b';
export const WeaponGroupsDatabaseId = '35b2551df60043c0a3ef780ffdbd2f29';
export const WeaponSlotsDatabaseId = '987f60f2fc6643d8a1446d79bce151f2';
export const WeaponDatabaseId = '26b7ebbbff1d4dc9a85326057203c93c';
export const FactoriesDatabaseId = '59f5d263985c469499856c1e3ea45fb2';
export const EstablishmentDatabaseId = 'a9659af6740a4d88b9b5016bfc2c1327';
export const FacilitiesDatabaseId = '96787f2e997d42fdab09bd4a43d695e8';

export const WeaponGroupsPagesId: Record<Weapons.TWeaponGroupType, string> = {
	[Weapons.TWeaponGroupType.Warp]: '83fee69a-9429-482c-b1ca-55c5440155be',
	[Weapons.TWeaponGroupType.Drone]: 'c883f28c-0693-4496-b93f-9ceb06a04b71',
	[Weapons.TWeaponGroupType.AntiAir]: '34bc2679-df6d-4504-9754-e212661f6a3a',
	[Weapons.TWeaponGroupType.Beam]: '62981943-ad98-4d77-8fdb-a84d6f33cf9e',
	[Weapons.TWeaponGroupType.Lasers]: '55a22006-5af5-4f9d-a2c9-34bb62f2eaa7',
	[Weapons.TWeaponGroupType.Plasma]: 'cd1062b6-1172-4b08-8ab3-180588458cbe',
	[Weapons.TWeaponGroupType.Rockets]: '581dfb50-5585-49e7-9382-bb127e86d371',
	[Weapons.TWeaponGroupType.Cannons]: '3e9aa3d9-3129-4e0a-9763-fe28ad640e91',
	[Weapons.TWeaponGroupType.Railguns]: '94f8f8a9-9e93-461c-9893-54de8f9d419f',
	[Weapons.TWeaponGroupType.Launchers]: 'c2a46066-741d-414f-99b9-ab7d2f4ae1fd',
	[Weapons.TWeaponGroupType.Cruise]: '826de087-f48d-41c4-8857-773865eea96b',
	[Weapons.TWeaponGroupType.Missiles]: '2d677cb4-5fa2-46b9-89e7-9cd89c8ae84d',
	[Weapons.TWeaponGroupType.Bombs]: '9d11cc09-1866-4662-a82b-c8a58a18d085',
	[Weapons.TWeaponGroupType.Guns]: 'fc1deb81-bb86-463d-a64d-08b294832e4b',
};

export const WeaponGroupsIconsId: Record<Weapons.TWeaponGroupType, string> = {
	[Weapons.TWeaponGroupType.Warp]: 'https://octaharon.github.io/icons/weapons_warp.png',
	[Weapons.TWeaponGroupType.Drone]: 'https://octaharon.github.io/icons/weapon_drones.png',
	[Weapons.TWeaponGroupType.AntiAir]: 'https://octaharon.github.io/icons/weapon_antiair.png',
	[Weapons.TWeaponGroupType.Beam]: 'https://octaharon.github.io/icons/weapon_energy.png',
	[Weapons.TWeaponGroupType.Bombs]: 'https://octaharon.github.io/icons/weapon_bomb.png',
	[Weapons.TWeaponGroupType.Cannons]: 'https://octaharon.github.io/icons/weapon_cannons.png',
	[Weapons.TWeaponGroupType.Cruise]: 'https://octaharon.github.io/icons/weapons_cruise.png',
	[Weapons.TWeaponGroupType.Missiles]: 'https://octaharon.github.io/icons/weapon_homing.png',
	[Weapons.TWeaponGroupType.Rockets]: 'https://octaharon.github.io/icons/weapon_rockets.png',
	[Weapons.TWeaponGroupType.Railguns]: 'https://octaharon.github.io/icons/weapons_railguns.png',
	[Weapons.TWeaponGroupType.Plasma]: 'https://octaharon.github.io/icons/weapon_plasma.png',
	[Weapons.TWeaponGroupType.Launchers]: 'https://octaharon.github.io/icons/weapon_launchers.png',
	[Weapons.TWeaponGroupType.Lasers]: 'https://octaharon.github.io/icons/weapon_lasers.png',
	[Weapons.TWeaponGroupType.Guns]: 'https://octaharon.github.io/icons/weapon_guns.png',
};

export const WeaponSlotsPagesId: Record<Weapons.TWeaponSlotType, string> = {
	[Weapons.TWeaponSlotType.hangar]: '4306f05e-2649-47c8-95c4-e738e8f5983f',
	[Weapons.TWeaponSlotType.naval]: '78c62b2f-9007-41bb-b584-b63ec2e4a965',
	[Weapons.TWeaponSlotType.air]: '1b01ead4-c539-4275-b526-95d36b215aeb',
	[Weapons.TWeaponSlotType.extra]: '79de7d57-7b34-4ca2-bb51-ac9e2a3f6298',
	[Weapons.TWeaponSlotType.medium]: '5dfd62f4-8455-4840-b929-03ae4bcb38d6',
	[Weapons.TWeaponSlotType.small]: '6c5763b5-2ff3-4c86-b722-78e176b70d3c',
	[Weapons.TWeaponSlotType.large]: '821f17d2-08ea-4c86-8e74-127564543a94',
};

export const ChassisClassPagesId: Record<UnitChassis.chassisClass, string> = {
	[UnitChassis.chassisClass.none]: '',
	[UnitChassis.chassisClass.sup_aircraft]: 'a532b1d3-642e-42f7-8727-282c31aed15c',
	[UnitChassis.chassisClass.sup_ship]: '16cef657-e257-423d-8775-89dfe571e1d5',
	[UnitChassis.chassisClass.sup_vehicle]: '0f9841f9-029c-4fbe-a121-cdb91c9aea01',
	[UnitChassis.chassisClass.tank]: 'd5a16b55-a198-4e16-87fb-af24dac2b67a',
	[UnitChassis.chassisClass.swarm]: '70f58fb8-51c7-4802-a668-26e7b4724d0d',
	[UnitChassis.chassisClass.submarine]: '7df43147-b3e6-47a7-a572-17f88748673b',
	[UnitChassis.chassisClass.strike_aircraft]: '94169f94-fc35-4a53-b348-f597431d4f84',
	[UnitChassis.chassisClass.scout]: '76d66731-561a-49fe-b546-e2c8b06ea02a',
	[UnitChassis.chassisClass.science_vessel]: 'e65edddf-e95f-42ea-bcfc-ed6ce84766d0',
	[UnitChassis.chassisClass.rover]: '4aba8661-681f-44e3-a5e5-abc6adb09df7',
	[UnitChassis.chassisClass.pointdefense]: '87592f99-0665-4f2c-b561-1c340d6e242f',
	[UnitChassis.chassisClass.mothership]: '85d60062-38e7-4e80-9c6a-28ea7b65a198',
	[UnitChassis.chassisClass.lav]: '67023e2a-13fe-4091-9343-8ce3bdb8d5f2',
	[UnitChassis.chassisClass.launcherpad]: 'b53faed2-6a6b-4fb3-997f-18fdbfc97320',
	[UnitChassis.chassisClass.infantry]: '20344e3f-d4a1-436f-a982-f4a84881f28c',
	[UnitChassis.chassisClass.hydra]: 'f1a01d27-3905-481b-84e0-380139b8f222',
	[UnitChassis.chassisClass.hovertank]: '997af2ce-3fba-4f21-bff7-425751543b08',
	[UnitChassis.chassisClass.helipad]: 'edf3d051-94a3-43cb-9806-ae12a4e74370',
	[UnitChassis.chassisClass.tiltjet]: '719e9bc2-57b6-4320-94c4-f80bd36a3ff3',
	[UnitChassis.chassisClass.hav]: '457d2e02-bc45-40e5-a6fa-e9920154129d',
	[UnitChassis.chassisClass.fortress]: 'eff601d4-561a-4a11-ba24-0f07a5d474b3',
	[UnitChassis.chassisClass.fighter]: '74665098-44ac-4d46-b0d5-a82076fc17ff',
	[UnitChassis.chassisClass.exosuit]: '0e79003c-dd3a-441f-9d92-e89b403657c6',
	[UnitChassis.chassisClass.droid]: '5098babf-fe72-4ca1-940d-2451c736db50',
	[UnitChassis.chassisClass.destroyer]: '7eefc62d-85cc-41de-9e87-9ca5f6b3d348',
	[UnitChassis.chassisClass.cruiser]: '084b8890-d4bf-433a-a800-55e84913ca95',
	[UnitChassis.chassisClass.corvette]: 'c0c6f167-4fb4-40c4-8fa9-2b6dec95dd14',
	[UnitChassis.chassisClass.mecha]: '3b350934-f046-40cc-bff5-e87c5ff5baec',
	[UnitChassis.chassisClass.combatdrone]: 'c9029d47-c833-4a18-b866-7599ed895cd1',
	[UnitChassis.chassisClass.carrier]: 'b0437ec2-fc8b-4426-8342-c8517f71c9c8',
	[UnitChassis.chassisClass.manofwar]: 'ca779d9b-f18f-49ef-912d-5d3f8679b6ad',
	[UnitChassis.chassisClass.bomber]: '8d5fb11d-82b8-41c9-a181-e5c4b9a3ba6b',
	[UnitChassis.chassisClass.biotech]: '7fc5e36a-90eb-4047-8c6c-aa933a5d4096',
	[UnitChassis.chassisClass.artillery]: '71c6078a-d6f0-455e-9438-8772bbda703e',
	[UnitChassis.chassisClass.battleship]: 'ba147b6f-bc0c-4091-8c29-54ebcc63d427',
	[UnitChassis.chassisClass.quadrapod]: 'e04830b9-e80b-4572-ac62-7889f49c362d',
};

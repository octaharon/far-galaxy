import InterfaceTypes from '../../definitions/core/InterfaceTypes';
import BasicMaths from '../../definitions/maths/BasicMaths';
import Vectors from '../../definitions/maths/Vectors';

const numjs = require('numericjs');

export const transformByPoints = (source: InterfaceTypes.TQuadrigon, target: InterfaceTypes.TQuadrigon): string => {
	const width = Vectors.module()(Vectors.create(source[0], source[1]));
	const height = Vectors.module()(Vectors.create(source[2], source[1]));
	const sourcePoints = [
		[0, 0],
		[width, 0],
		[width, height],
		[0, height],
	];
	const targetPoints = target.map((p) => Vectors.create(source[0], p)).map((p) => [p.x, p.y]);
	const a = [];
	const b = [];
	for (let i = 0, n = sourcePoints.length; i < n; ++i) {
		const s = sourcePoints[i];
		const t = targetPoints[i];
		a.push([s[0], s[1], 1, 0, 0, 0, -s[0] * t[0], -s[1] * t[0]]);
		a.push([0, 0, 0, s[0], s[1], 1, -s[0] * t[1], -s[1] * t[1]]);
		b.push(t[0]);
		b.push(t[1]);
	}

	const X = numjs.solve(a, b, true);
	const matrix = [X[0], X[3], 0, X[6], X[1], X[4], 0, X[7], 0, 0, 1, 0, X[2], X[5], 0, 1].map((x) => BasicMaths.roundToPrecision(x, 10e-6));
	return matrix.toString();
};

export function splitIntoChunks<P>(arr: P[], n: number) {
	const res = [];
	while (arr.length) {
		res.push(arr.splice(0, n));
	}
	return res;
}

export default function createRandomArray(length: number) {
	return new Array(length).fill(null).map(Math.random);
}

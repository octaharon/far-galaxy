export function predicateComparator<T>(predicates: Array<(t: T) => boolean>, map: (t: T) => any = JSON.stringify) {
	return (a: T, b: T) => {
		for (const p of predicates) {
			if (p(a) && !p(b)) return -1;
			if (p(b) && !p(a)) return 1;
		}
		// @ts-ignore
		return map(a) - map(b);
	};
}
export default predicateComparator;

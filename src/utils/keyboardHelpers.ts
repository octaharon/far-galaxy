export const notCombinedKey = (event: KeyboardEvent) => !(event.altKey || event.shiftKey || event.ctrlKey);

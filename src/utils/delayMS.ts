export const delayMS = (t = 200) => {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(t);
		}, t);
	});
};

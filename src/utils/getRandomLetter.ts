const Letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZΨΦΣΘΔΩ';
export const getLetters = () => Letters.split('');
export const getRandomLetter = (l = getLetters()) => l[Math.floor(Math.random() * l.length)];
export const getLettersWithoutRomans = () => Letters.split('').filter((v) => !'XVILM'.includes(v));
export const getRandomLetterWithoutRomans = () => getRandomLetter(getLettersWithoutRomans());

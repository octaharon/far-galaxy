import crypto from 'crypto';

/**
 * Creates a unique or stable hash based on incoming data
 *
 * @param {Object|string} salt the data to extract hash from
 * @param {boolean} unique make the hash globally unique, thus losing the option to validate it against data.
 * @returns {string} hash of incoming data, which is idempotent unless Unique is set to true
 */
export default function digest(salt: any, unique = false): string {
	return crypto
		.createHash('sha1')
		.update(JSON.stringify(salt) + (unique ? (Math.random() * 100000).toString() + Date.now().toString() : ''))
		.digest('hex');
}

/**
 * A simple numeric hashing function, yielding idempotent numeric value for any given string, like CRC
 *
 * @param {string} value text to hash
 * @param {number} min optional minimum value, defaults to 0
 * @param {number} max optional maximum value, defaults to 65535
 * @returns {number} numeric stable hash of the given text
 */
export function numericHash(value: string, min?: number, max?: number): number {
	if (!Number.isFinite(min)) min = 0;
	if (!Number.isFinite(max)) max = 65536;
	let sum = 0;
	if (!value) return sum;
	for (let i = 0; i < value.length; i++) sum += value[i].charCodeAt(0);
	return min + (sum % (max - min));
}

/* eslint-disable @typescript-eslint/ban-ts-comment,max-lines,@typescript-eslint/no-this-alias */
// @ts-nocheck

// eslint-disable-next-line max-classes-per-file
import { FrontSide, ShaderLib, ShaderMaterial, UniformsUtils } from 'three';

class BaseAnimationMaterial extends ShaderMaterial {
	constructor(parameters, uniforms) {
		super();

		if (parameters.uniformValues) {
			console.warn('THREE.BAS - `uniformValues` is deprecated. Put their values directly into the parameters.');

			Object.keys(parameters.uniformValues).forEach((key) => {
				parameters[key] = parameters.uniformValues[key];
			});

			delete parameters.uniformValues;
		}

		// copy parameters to (1) make use of internal #define generation
		// and (2) prevent 'x is not a property of this material' warnings.
		Object.keys(parameters).forEach((key) => {
			this[key] = parameters[key];
		});

		// override default parameter values
		this.setValues(parameters);

		// override uniforms
		this.uniforms = UniformsUtils.merge([uniforms, parameters.uniforms || {}]);

		// set uniform values from parameters that affect uniforms
		this.setUniformValues(parameters);
	}

	setUniformValues(values) {
		if (!values) return;

		const keys = Object.keys(values);

		keys.forEach((key) => {
			if (key in this.uniforms) this.uniforms[key].value = values[key];
		});
	}

	stringifyChunk(name) {
		let value;

		if (!this[name]) {
			value = '';
		} else if (typeof this[name] === 'string') {
			value = this[name];
		} else {
			value = this[name].join('\n');
		}

		return value;
	}
}

class PhongAnimationMaterial extends BaseAnimationMaterial {
	/**
	 * Extends THREE.MeshPhongMaterial with custom shader chunks.
	 *
	 * @see http://three-bas-examples.surge.sh/examples/materials_phong/
	 *
	 * @param {Object} parameters Object containing material properties and custom shader chunks.
	 * @constructor
	 */
	constructor(parameters) {
		super(parameters, ShaderLib['phong'].uniforms);

		this.lights = true;
		this.vertexShader = this.concatVertexShader();
		this.fragmentShader = this.concatFragmentShader();
	}

	concatVertexShader() {
		return ShaderLib.phong.vertexShader
			.replace(
				'void main() {',
				`
        ${this.stringifyChunk('vertexParameters')}
        ${this.stringifyChunk('varyingParameters')}
        ${this.stringifyChunk('vertexFunctions')}

        void main() {
          ${this.stringifyChunk('vertexInit')}
        `,
			)
			.replace(
				'#include <beginnormal_vertex>',
				`
        #include <beginnormal_vertex>

        ${this.stringifyChunk('vertexNormal')}
        `,
			)
			.replace(
				'#include <begin_vertex>',
				`
        #include <begin_vertex>

        ${this.stringifyChunk('vertexPosition')}
        ${this.stringifyChunk('vertexColor')}
        `,
			)
			.replace(
				'#include <morphtarget_vertex>',
				`
        #include <morphtarget_vertex>

        ${this.stringifyChunk('vertexPostMorph')}
        `,
			)
			.replace(
				'#include <skinning_vertex>',
				`
        #include <skinning_vertex>

        ${this.stringifyChunk('vertexPostSkinning')}
        `,
			);
	}

	concatFragmentShader() {
		return ShaderLib.phong.fragmentShader
			.replace(
				'void main() {',
				`
        ${this.stringifyChunk('fragmentParameters')}
        ${this.stringifyChunk('varyingParameters')}
        ${this.stringifyChunk('fragmentFunctions')}

        void main() {
          ${this.stringifyChunk('fragmentInit')}
        `,
			)
			.replace(
				'#include <map_fragment>',
				`
        ${this.stringifyChunk('fragmentDiffuse')}
        ${this.stringifyChunk('fragmentMap') || '#include <map_fragment>'}

        `,
			)
			.replace(
				'#include <emissivemap_fragment>',
				`
        ${this.stringifyChunk('fragmentEmissive')}

        #include <emissivemap_fragment>
        `,
			)
			.replace(
				'#include <lights_phong_fragment>',
				`
        #include <lights_phong_fragment>
        ${this.stringifyChunk('fragmentSpecular')}
        `,
			);
	}
}

class EnhancedMaterial extends PhongAnimationMaterial {
	constructor({
		flatShading = true,
		side = FrontSide,
		uniforms = {},
		uniformValues = {},
		vertexParameters = [],
		vertexColor = [],
		varyingParameters = [],
		vertexFunctions = [],
		vertexInit = [],
		vertexNormal = [],
		vertexPosition = [],
		fragmentParameters = [],
		fragmentFunctions = [],
		fragmentInit = [],
		fragmentDiffuse = [],
		fragmentMap = [],
		fragmentEmissive = [],
		fragmentSpecular = [],
		...rest
	}) {
		super({
			flatShading,
			side,
			uniforms,
			uniformValues,
			vertexParameters: ['// vertexParameters', ...vertexParameters],
			vertexColor: ['// vertexColor', ...vertexColor],
			varyingParameters: ['// varyingParameters', ...varyingParameters],
			vertexFunctions: ['// vertexFunctions', ...vertexFunctions],
			vertexInit: ['// vertexInit', ...vertexInit],
			vertexNormal: ['// vertexNormal', ...vertexNormal],
			vertexPosition: ['// vertexPosition', ...vertexPosition],
			fragmentParameters: ['// fragmentParameters', ...fragmentParameters],
			fragmentFunctions: ['// fragmentFunctions', ...fragmentFunctions],
			fragmentInit: ['// fragmentInit', ...fragmentInit],
			fragmentDiffuse: ['// fragmentDiffuse', ...fragmentDiffuse],
			fragmentMap: ['// fragmentMap', ...fragmentMap],
			fragmentEmissive: ['// fragmentEmissive', ...fragmentEmissive],
			fragmentSpecular: ['// fragmentSpecular', ...fragmentSpecular],
			...rest,
		});
	}
}

export default EnhancedMaterial;

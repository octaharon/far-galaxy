import _ from 'underscore';

export const weightedRandom = <K>(weightMap: Array<[K, number]> = []) => {
	const options = weightMap.reduce(
		(acc, [item, weight]) => acc.concat(_.times(weight, () => _.clone(item))),
		[] as K[],
	);
	return _.sample(options);
};

export const weightedSample = <K extends string>(weightMap: Partial<Record<K, number>>): K =>
	weightedRandom(Object.entries(weightMap).map(([k, v]) => [k as K, Math.round(Math.max(0, v as number))]));

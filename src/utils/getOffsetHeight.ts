// @ts-ignore
export const getOffsetHeight: (el: HTMLElement) => number = (el: HTMLElement) => {
    let v: number;
    try {
        v = parseInt(window.getComputedStyle(el, null)
            .getPropertyValue('height'), 10);
    } catch (e) {
        // @ts-ignore
        v = el.currentStyle.height;
    }
    return v;
};

export default getOffsetHeight;
export const INFINITY_SYMBOL = '∞';
export const PLUS_SYMBOL = '+';
export const MINUS_SYMBOL = '-';
export const PLUSMINUS_REGEX = new RegExp(`[${MINUS_SYMBOL}${PLUS_SYMBOL}]+`, 'gi');

/**
 * Represent a value with a given number of fractional digits, or as infinity symbol
 *
 * @param {number} n a value
 * @param {number} digits number  of fractional digits, defaults to 2
 * @param {number} bailout infinity threshold, equals 10^9 by default. Greater values are represented with Infinity symbol
 * @return {string|null} a string representation of number, or null if provided value is not numeric
 */
export function wellRounded(n: number, digits = 2, bailout = 10e9): string | null {
	if (n >= Math.min(Math.abs(bailout), Infinity)) return INFINITY_SYMBOL;
	if (n <= Math.max(-Math.abs(bailout), -Infinity)) return '-' + INFINITY_SYMBOL;
	if (!Number.isFinite(n)) return null;
	if (!Number.isFinite(digits) || digits < 0) digits = 1;
	return n
		.toFixed(digits)
		.replace(/(?<=\.\d*)0+$/, '')
		.replace(/\.$/, '');
}

/**
 * Represent a bias property of IModifier as percentage increment or decrement
 *
 * @param {number} n a bias value
 * @returns {string|null} a string with percentage modifier, or null, if provided value is not numeric
 */
export function biasToPercent(n: number): string | null {
	if (!Number.isFinite(n)) return null;
	return (n > 0 ? '+' : '') + describePercentage(n);
}

/**
 * Represent a bias property of IModifier as percentage increment or decrement
 *
 * @param {number} n a bias value
 * @returns {string|null} a string with percentage modifier, or null, if provided value is not numeric
 */
export function describePercentage(n: number): string | null {
	if (!Number.isFinite(n)) return null;
	return Math.round(n * 100) + '%';
}

export function numericAPScaling(scale: number, APToken: string, base?: number, percentage = false) {
	let r = '';
	if (Number.isFinite(base)) {
		r += base > 0 ? '' : '-';
		r += (percentage ? biasToPercent(base) : wellRounded(base)).replace(PLUSMINUS_REGEX, '');
	}
	if (Number.isFinite(scale)) {
		r += Number.isFinite(base)
			? scale > 0
				? PLUS_SYMBOL
				: MINUS_SYMBOL
			: percentage
			? scale > 0
				? PLUS_SYMBOL
				: MINUS_SYMBOL
			: '';
		r += APToken;
		r += (percentage ? biasToPercent(scale) : wellRounded(scale)).replace(PLUSMINUS_REGEX, '');
	}
	return r;
}

import InterfaceTypes from '../../definitions/core/InterfaceTypes';
import { wellRounded } from './wellRounded';

export const polygonToSvgPath = (polygon: InterfaceTypes.TPolygon) =>
	`M ${polygon[0].x},${polygon[0].y} ${polygon
		.slice(1)
		.map((p) => `L ${wellRounded(p.x, 3)},${wellRounded(p.y, 3)}`)
		.join(' ')} z`;

export const polygonToCss = (polygon: InterfaceTypes.TPolygon, boundaries?: InterfaceTypes.IVector) =>
	'polygon(' +
	polygon
		.map(
			(point) =>
				`${(boundaries?.x ? (point.x / boundaries.x) * 100 : point.x).toFixed(3)}% ${(boundaries?.y
					? (point.y / boundaries.y) * 100
					: point.y
				).toFixed(3)}%`,
		)
		.join(', ') +
	')';

type TPredicateChoices<ValueType, ParameterType = any> = Array<[ValueType, (p: ParameterType) => boolean]>;

export function predicateSelection<ValueType, ParameterType = any>(
	predicates: TPredicateChoices<ValueType, ParameterType>,
) {
	return (predicateArg: ParameterType = null) => {
		if (!predicates.length) return null;
		const choice = (predicates || []).find(([, predicate]) => predicate(predicateArg));
		if (!choice) return predicates[0][0];
		return choice[0];
	};
}

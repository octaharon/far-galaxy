import _ from 'underscore';

export type TShaderChunks = Partial<{
	overwriteChunk: string;
	definitionsChunks: string[] | string;
	varyingChunks: string[] | string;
	uniformChunks: string[] | string;
	postProcessChunks: string[] | string;
	exportChunk: (chunkName: string) => string;
	replacementChunks: Record<string, string>;
}>;
/**
 * Injects custom chunks into threejs fragment shaders
 *
 * @param origin complete original shader
 * @param chunks categorized chunk partials
 * @param chunkName the export function name, which can be reused in combination with other chunks
 * @return string
 */
export const injectShaderChunks = (origin: string, chunks: TShaderChunks = {}, chunkName?: string) => {
	const uniformPostfix = '#include <common>';
	const {
		varyingChunks = [],
		uniformChunks = [],
		postProcessChunks = [],
		definitionsChunks = [],
		overwriteChunk,
		exportChunk,
		replacementChunks,
	} = chunks;
	const serializeChunks = (s: string | string[], ...others: string[]) =>
		[]
			.concat(s, others ?? [])
			.flatMap((s: string) => s.trim().replace('\t', ''))
			.filter(Boolean)
			.join('\n');
	const dedupeChunks = (s: string | string[]) =>
		_.uniq([].concat(s).flatMap((s) => s.split('\n')))
			.map((v: string) => v.trim())
			.join('\n');
	const newShader = Object.keys(replacementChunks || {}).reduce(
		(str, lookupString) => str.replace(lookupString, replacementChunks[lookupString]),
		overwriteChunk || origin,
	);
	return `${newShader.replace(
		uniformPostfix,
		`${dedupeChunks(serializeChunks(uniformChunks))}
		${dedupeChunks(varyingChunks)}
		${serializeChunks(definitionsChunks)}
		${!!chunkName && exportChunk ? exportChunk(chunkName) : ''}
		${uniformPostfix}\n`,
	)}
	${serializeChunks(postProcessChunks)}`
		.replace(/^[\s\t]+/im, '')
		.trim();
};

/**
 * Inject a set of named shader chunks into a complete threejs fragment shader
 *
 * @param origin complete original shader
 * @param collections a set of named shader chunks, key being the exported function name
 */
export const injectShaderChunksCollection = (origin: string, collections: Record<string, TShaderChunks>) => {
	const allKeys = ['definitionsChunks', 'uniformChunks', 'varyingChunks', 'postProcessChunks'];
	const shaderChunk = _.uniq(allKeys).reduce(
		(result, shaderChunkKey) =>
			Object.assign(result, {
				[shaderChunkKey]: _.pluck(collections, shaderChunkKey).filter(Boolean),
			}),
		{} as TShaderChunks,
	);
	shaderChunk.overwriteChunk = _.pluck(collections, 'overwriteChunk').filter(Boolean)[0];
	shaderChunk.exportChunk = _.pluck(collections, 'exportChunk').filter(Boolean)[0];
	shaderChunk.replacementChunks = _.pluck(collections, 'replacementChunks')
		.filter(Boolean)
		.reduce((chunks, item) => Object.assign(chunks, item), {});
	//console.log(shaderChunk);
	return injectShaderChunks(origin, shaderChunk);
};

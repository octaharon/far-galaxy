// eslint-disable-next-line @typescript-eslint/no-var-requires
const glsl = require('glslify') as (...a: any[]) => string;

export const CloudFMBShaders = {
	vertexShader: glsl`
	uniform vec2 uvScale;
	varying vec2 vUv;
	varying vec3	vVertexWorldPosition;
  varying vec3	vVertexNormal;

	void main()
	{

		vUv = uvScale * uv;
		vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
		vVertexNormal	= normalize(normalMatrix * normal);
		vVertexWorldPosition	= (modelMatrix * vec4(position, 1.0)).xyz;
		gl_Position = projectionMatrix * mvPosition;

	}
	`,
	fragmentShader: glsl`

uniform vec2 uResolution;
uniform float uTime;
uniform float	cf;
uniform float	power;
uniform float transparency;
uniform vec3	glowColor;


varying vec2 vUv;
varying vec3	vVertexNormal;
varying vec3	vVertexWorldPosition;


float random (in vec2 _st) { 
    return fract(sin(dot(_st.xy,
                         vec2(12.9898,78.233)))* 
        43758.5453123);
}

float noise (in vec2 _st) {
    vec2 i = floor(_st);
    vec2 f = fract(_st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    vec2 u = f * f * (3.0 - 2.0 * f);

    return mix(a, b, u.x) + 
            (c - a)* u.y * (1.0 - u.x) + 
            (d - b) * u.x * u.y;
}

float blendScreen(float base, float blend) {
	return 1.0-((1.0-base)*(1.0-blend));
}

vec3 blendScreen(vec3 base, vec3 blend) {
	return vec3(blendScreen(base.r,blend.r),blendScreen(base.g,blend.g),blendScreen(base.b,blend.b));
}


vec3 blendScreen(vec3 base, vec3 blend, float opacity) {
	return (blendScreen(base, blend) * opacity + base * (1.0 - opacity));
}


float fbm ( in vec2 _st) {
    int numOctaves = int(min(10.0, log2(uResolution.x))) - 3;
    
    float v = 0.0;
    float a = 0.5;
    vec2 shift = vec2(100.0);
    // Rotate to reduce axial bias
    mat2 rot = mat2(cos(0.5), sin(0.5), 
                    -sin(0.5), cos(0.50));
    
    // Unrolled loop; because GL won't let me compare against a non-constant.
    if (numOctaves >= 1) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 2) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 3) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 4) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 5) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 6) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 7) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 8) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 9) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    if (numOctaves >= 10) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    return v;
}

void main() {
    vec2 st = gl_FragCoord.xy/uResolution.xy*4.;
    //vec2 st=vUv*3.;
    vec3 color = vec3(0.0);

    vec2 q = vec2(0.);
    q.x = fbm( st + 0.00*uTime);
    q.y = fbm( st + vec2(1.0));

    vec2 r = vec2(0.);
    r.x = fbm( st + 1.0*q + vec2(1.7,9.2)+ 0.15*uTime );
    r.y = fbm( st + 1.0*q + vec2(8.3,2.8)+ 0.126*uTime);

    float f = fbm(st+r);

    color = mix(vec3(0.15,0.15,0.9),
                vec3(0.9,0.9,0.15),
                clamp((f*f)*4.0,0.0,1.0));

    color = mix(color,
                vec3(0.15,0.5,0.15),
                clamp(length(q),0.0,1.0));

    color = mix(color,
                vec3(.9,.9,.9),
                clamp(length(r.x),0.0,1.0));
                
		vec3 worldCameraToVertex = vVertexWorldPosition - cameraPosition;
		vec3 viewCameraToVertex	= (viewMatrix * vec4(worldCameraToVertex, 0.0)).xyz;
		viewCameraToVertex = normalize(viewCameraToVertex);
		float viewAngle=dot(normalize(vVertexNormal), normalize(viewCameraToVertex));
		float intensity	= clamp(pow(cf + dot(vVertexNormal, viewCameraToVertex), power), 0.0, 1.0);
		float edgeTransparencyThreshold=-0.25;
		float edgeAlpha	= viewAngle>=edgeTransparencyThreshold?clamp(pow(viewAngle/edgeTransparencyThreshold,3.0),0.0,transparency):transparency;
		edgeAlpha=viewAngle>0.0?0.0:edgeAlpha;
		vec3 cloudTextureColor=vec3((f*f*f+.6*f*f+.5*f)*color);
		vec3 luma = vec3(0.33, 0.33, 0.33);
		float v = pow(dot( cloudTextureColor, luma ),0.25);
    gl_FragColor = vec4(glowColor,blendScreen(v,intensity)*edgeAlpha);
}
`,
};

import { Color, ShaderMaterial } from 'three';
import { ColorifyShader } from 'three/examples/jsm/shaders/ColorifyShader';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const glsl = require('glslify');

export const colorizeShaderMaterial = (baseColor: Color) =>
	new ShaderMaterial({
		vertexShader: ColorifyShader.vertexShader,
		fragmentShader: glsl`

		uniform vec3 color;
		uniform sampler2D tDiffuse;

		varying vec2 vUv;

		void main() {

			vec4 texel = texture2D( tDiffuse, vUv );

			vec3 luma = vec3(0.21, 0.71, 0.07);
			float v = dot( texel.rgb, luma );
			gl_FragColor = vec4( v * color, texel.a );
		}
		`,
		transparent: true,
		uniforms: {
			color: { value: baseColor },
		},
	});

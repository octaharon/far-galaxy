import { doesTextureResolutionIncludes } from '../../contexts/GraphicSettings/helpers';
import { TGraphicTextureSize } from '../../contexts/GraphicSettings/slice';
import { TShaderChunks } from '../injectShaderChunks';

const fmt = (n: number) => n.toFixed(3);

const fbmDisplacement: (
	params?: Partial<{
		speed: number;
		offset: number;
		amplitude: number;
		resolutionX: number;
		resolutionY: number;
		scaleX: number;
		scaleY: number;
		power: number;
		spin: number;
		quality: TGraphicTextureSize;
	}>,
) => TShaderChunks = (params) => ({
	uniformChunks: `uniform float uTime;
	uniform sampler2D texOffsetMap;
`,
	definitionsChunks: `const float distort_iterations = 3.0;
	float hash( float n ) { return fract(sin(n)*123.456789); }

vec2 rotate( in vec2 uv, float a)
{
    float c = cos( a );
    float s = sin( a );
    return vec2( c * uv.x - s * uv.y, s * uv.x + c * uv.y );
}

float noise_fbm( in vec3 p )
{
    vec3 fl = floor( p );
    vec3 fr = fract( p );
    fr = fr * fr * ( 3.0 - 2.0 * fr );

    float n = fl.x + fl.y * 157.0 + 113.0 * fl.z;
    return mix( mix( mix( hash( n +   0.0), hash( n +   1.0 ), fr.x ),
                     mix( hash( n + 157.0), hash( n + 158.0 ), fr.x ), fr.y ),
                mix( mix( hash( n + 113.0), hash( n + 114.0 ), fr.x ),
                     mix( hash( n + 270.0), hash( n + 271.0 ), fr.x ), fr.y ), fr.z );
}

float fbm( in vec2 p, float t )
{
    float f;
    f  = 0.5000 * noise_fbm( vec3( p, t ) ); p *= 2.1;
    ${
		doesTextureResolutionIncludes(params?.quality ?? 'HD', 'HD')
			? `f += 0.2500 * noise_fbm( vec3( p, t ) ); p *= 2.2;`
			: ''
	}
	  ${
			doesTextureResolutionIncludes(params?.quality ?? 'HD', 'UHD')
				? `f += 0.1250 * noise_fbm( vec3( p, t ) ); p *= 2.3;`
				: ''
		}
    ${doesTextureResolutionIncludes(params?.quality ?? 'HD', 'UHD') ? `f += 0.0625 * noise_fbm( vec3( p, t ) );` : ''}
    return f;
}

vec2 field(vec2 p)
{
    float t = ${fmt(params?.speed ?? 0.001)} * uTime;

    p.x += t;
    p.y += t;

    float n = fbm( p, t );

    float e = ${fmt(params.amplitude ?? 0.25)};
    float nx = fbm( p + vec2( e, 0.0 ), t );
    float ny = fbm( p + vec2( 0.0, e ), t );

    return vec2( n - ny, nx - n ) / e;
}

vec3 distort( in vec2 p, sampler2D tex )
{
 		p.x *= ${fmt(params?.scaleX ?? 3.0)};
 		//p.x = ${fmt(params?.spin ?? 0.001)} * uTime + p.x;
 		p.y += ${fmt(params?.offset ?? 0)};
    p.y *= ${fmt(params?.scaleY ?? 7.0)};
    for( float i = 0.0; i < distort_iterations; ++i )
    {
        p += field( p ) / distort_iterations;
    }
    vec3 s = ${fmt(params?.power ?? 2.5)} * texture( tex, vec2( p.x * ${fmt(params?.resolutionX ?? 0.55)}, p.y * ${fmt(
		params?.resolutionY ?? 0.55,
	)} ) ).xyz;

    return fbm( p, 0.0 ) * s;
}
`,
	replacementChunks: {
		'#include <map_fragment>': `
    vec4 texelColor = vec4(distort(vUv.xy,map),1);
		diffuseColor *= texelColor;
`,
	},
});
export default fbmDisplacement;

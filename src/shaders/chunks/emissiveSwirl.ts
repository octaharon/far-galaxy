import { getTextureGeneratedResolution } from '../../contexts/GraphicSettings/helpers';
import { TGraphicTextureSize } from '../../contexts/GraphicSettings/slice';
import { TShaderChunks } from '../injectShaderChunks';

const fmt = (n: number) => n.toFixed(3);

const emissiveSwirl: (
	params?: Partial<{
		noiseResolution?: TGraphicTextureSize;
		animSpeed?: number;
		amplitude?: number;
		scale?: number;
		quality: TGraphicTextureSize;
	}>,
) => TShaderChunks = (params) => ({
	uniformChunks: `uniform float uTime;
	uniform sampler2D emissiveOffsetMap;
`,
	definitionsChunks: `
	float warp_noise(in vec3 x, sampler2D tex)
	{
			vec3 p = floor(x);
			vec3 f = fract(x);
			f = smoothstep(0.0, 1.0, f);
			
			vec2 uv = (p.xy + vec2(27.0, 16.0) * p.z) + f.xy;
			vec2 rg = texture(tex, (uv + 0.5) / 512.0, -0.5).yx;
			return mix(rg.x, rg.y, f.z) * 2.0 - 1.0;
	}

		vec2 swirl(in vec2 p, sampler2D tex)
		{
			return vec2(warp_noise(vec3(p.xy, .33), tex), warp_noise(vec3(p.yx, .66), tex));
		}
`,
	replacementChunks: {
		'#include <emissivemap_fragment>': `
		
		vec2 p = vUv.xy * ${fmt(params?.scale ?? 2)}* vec2(2, -2);
		//vec2 p = gl_FragCoord.xy/${fmt(getTextureGeneratedResolution(params?.quality) ?? 512)} * vec2(2, -2);
    vec4 col = vec4(0.87, 0.8, 0.87, 1.0);

    for(int i = 0; i < 3; i++) 
		col += texture(emissiveMap, p + 0.01 * swirl(12.15 * p + uTime * ${fmt(
			params?.animSpeed ?? 0.17,
		)}, emissiveOffsetMap)) * col * col;

    vec4 emissiveColor = col * ${fmt(params?.amplitude ?? 0.06)};
    
		totalEmissiveRadiance *= emissiveColor.rgb;
`,
	},
});
export default emissiveSwirl;

import { TShaderChunks } from '../injectShaderChunks';

const fmt = (n: number) => n.toFixed(5);

const emissiveMapWarp: (
	params?: Partial<{
		speedX: number;
		speedY: number;
		distanceX: number;
		distanceY: number;
		resolution: number;
	}>,
) => TShaderChunks = (params) => ({
	uniformChunks: `uniform float uTime;
	uniform sampler2D emissiveOffsetMap;
`,
	replacementChunks: {
		'#include <emissivemap_fragment>': `
		
    float warpSpeedX = ${fmt(params?.speedX ?? 0.3)};
    float warpSpeedY = ${fmt(params?.speedY ?? 0.3)};
    float warpDistanceX = ${fmt(params?.distanceX ?? 0.02)};
    float warpDistanceY = ${fmt(params?.distanceY ?? 0.02)};
    
		vec2 uv = vUv.xy;
    uv.y *= -1.0;
    
    vec3 offTexX = texture2D(emissiveOffsetMap, uv).rgb;
    vec3 luma = vec3(0.299, 0.587, 0.114);
    float power = dot(offTexX, luma);
    
    float powerX = sin(3.1415927*2.0 * mod(power + uTime * warpSpeedX, 1.0));
    float powerY = cos(3.1415927*2.0 * mod(power + uTime * warpSpeedY, 1.0));
    
    vec4 texelColor = texture2D( emissiveMap, fract((uv+vec2(powerX*warpDistanceX, powerY*warpDistanceY))*${fmt(
		params?.resolution ?? 1,
	)}));
		
		totalEmissiveRadiance *= texelColor.rgb;
`,
	},
});
export default emissiveMapWarp;

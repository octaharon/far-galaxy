import { TShaderChunks } from '../injectShaderChunks';
import Three from 'three';

const fmt = (n: number) => n.toFixed(3);

const animatedNoise: (
	params: Partial<{
		startAmp: number;
		startFrequency: number;
		frequency: number;
		amp: number;
		avgColor: Three.Vector3;
		oscColor: Three.Vector3;
		colorFrequency: Three.Vector3;
		colorOffset: Three.Vector3;
		octaves: number;
		animSpeedX: number;
		animSpeedY: number;
	}>,
) => TShaderChunks = (params) => ({
	uniformChunks: `uniform vec2 uResolution;
uniform float uTime;

`,
	varyingChunks: `varying vec2 vUv;
varying vec3 vVertexNormal;
varying vec3 vVertexWorldPosition;`,
	definitionsChunks: `float hash(vec2 p) { return fract(1e4 * sin(17.0 * p.x + p.y * 0.1) * (0.1 + abs(sin(p.y * 13.0 + p.x)))); }
float noise(vec2 x) {
  vec2 i = floor(x);
  vec2 f = fract(x);
  float a = hash(i);
  float b = hash(i + vec2(1.0, 0.0));
  float c = hash(i + vec2(0.0, 1.0));
  float d = hash(i + vec2(1.0, 1.0));
  vec2 u = f * f * (3.0 - 2.0 * f);
  return mix(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}

float fbm (in vec2 p) {

    float value = 0.0;
    float freq = ${fmt(params.startFrequency ?? 2.0)};
    float amp = ${fmt(params.startAmp ?? 0.55)};    

    for (int i = 0; i < ${fmt(params.octaves ?? 5)}; i++) {
        value += amp * (noise((p - vec2(1.0)) * freq));
        freq *= ${fmt(params.frequency ?? 2.23)};
        amp *= ${fmt(params.amp ?? 0.6)};
    }
    return value;
}

float pattern(in vec2 p) {
    vec2 offset = vec2(-0.5);

    vec2 aPos = vec2(sin(uTime * ${fmt(0.005 * (params.animSpeedX ?? 1))}), sin(uTime * ${fmt(
		0.01 * (params.animSpeedY ?? 1),
	)})) * 6.;
    vec2 aScale = vec2(3.0);
    float a = fbm(p * aScale + aPos);

    vec2 bPos = vec2(sin(uTime * ${fmt(0.01 * (params.animSpeedX ?? 1))}), sin(uTime * ${fmt(
		0.01 * (params.animSpeedX ?? 1),
	)})) * 1.;
	
    vec2 bScale = vec2(0.6);
    float b = fbm((p + a) * bScale + bPos);

    vec2 cPos = vec2(-0.6, -0.5) + vec2(sin(-uTime * 0.001), sin(uTime * 0.01)) * 2.;
    vec2 cScale = vec2(2.6);
    float c = fbm((p + b) * cScale + cPos);
    return c;
}

vec3 palette(in float t) {
    vec3 a = vec3(${fmt(params.avgColor?.x ?? 0.9)}, ${fmt(params.avgColor?.y ?? 0.3)}, ${fmt(
		params.avgColor?.z ?? 0.2,
	)});
    vec3 b = vec3(${fmt(params.avgColor?.x ?? 0.1)}, ${fmt(params.avgColor?.y ?? 0.2)}, ${fmt(
		params.avgColor?.z ?? 0.05,
	)});
    vec3 c = vec3(${fmt(params.avgColor?.x ?? 0.2)}, ${fmt(params.avgColor?.y ?? 0.8)}, ${fmt(
		params.avgColor?.z ?? 0.1,
	)});
    vec3 d = vec3(${fmt(params.avgColor?.x ?? 0.2)}, ${fmt(params.avgColor?.y ?? 0.5)}, ${fmt(
		params.avgColor?.z ?? 0.1,
	)});
    return a + b * cos(6.28318 * (c * t + d));
}
`,
	exportChunk: (chunkName) => `vec4 ${chunkName} () {
    vec2 p = gl_fragCoord.xy / uResolution.xy;
    p.x *= uResolution.x / uResolution.y;
    float value = pow(pattern(p), 2.); // more "islands"
    vec3 color = palette(value);
    return vec4(color, 1.0);
}`,
});
export default animatedNoise;

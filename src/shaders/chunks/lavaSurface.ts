import { TShaderChunks } from '../injectShaderChunks';

const fmt = (n: number) => n.toFixed(3);

const lavaSurfaceDisplacement: (
	params?: Partial<{
		speed: number;
		resolution: number;
		fogColor?: [number, number, number];
		fogDensity?: number;
	}>,
) => TShaderChunks = (params) => ({
	uniformChunks: `uniform float uTime;
	uniform sampler2D emissiveOffsetMap;
`,
	replacementChunks: {
		'#include <emissivemap_fragment>': `

   vec2 position = - 1.0 + ${fmt(params?.resolution ?? 2)} * vUv;

vec4 noise = texture2D (emissiveOffsetMap, vUv);
vec2 T1 = vUv + vec2( 1.5, - 1.5 ) * uTime * 2.0 * ${fmt(params?.speed ?? 0.01)};
vec2 T2 = vUv + vec2( - 0.5, 2.0 ) * uTime * ${fmt(params?.speed ?? 0.01)};

T1.x += noise.x * 2.0;
T1.y += noise.y * 2.0;
T2.x -= noise.y * 0.2;
T2.y += noise.z * 0.2;

float p = texture2D( emissiveOffsetMap, T1 * 2.0 ).a;

vec4 color = texture2D( emissiveMap, T2 * 2.0 );
vec4 temp = color * ( vec4( p, p, p, p ) * 2.0 ) + ( color * color - 0.1 );

if( temp.r > 1.0 ) { temp.bg += clamp( temp.r - 2.0, 0.0, 100.0 ); }
if( temp.g > 1.0 ) { temp.rb += temp.g - 1.0; }
if( temp.b > 1.0 ) { temp.rg += temp.b - 1.0; }

vec4 tmpColor = temp;

float depth = gl_FragCoord.z / gl_FragCoord.w;
const float LOG2 = 1.442695;
float fogFactor = exp2( -${fmt((params?.fogDensity ?? 0.45) ** 2)} * depth * depth * LOG2 );
fogFactor = 1.0 - clamp( fogFactor, 0.0, 1.0 );

totalEmissiveRadiance = mix( tmpColor, vec4( ${(params?.fogColor ?? [0, 0, 0])
			.map((v) => fmt(v))
			.join(', ')}, tmpColor.w ), fogFactor ).rgb;
`,
	},
});

export default lavaSurfaceDisplacement;

import { TShaderChunks } from '../injectShaderChunks';

const fmt = (n: number) => n.toFixed(5);

const textureMapDisplacement: (
	params?: Partial<{
		speed: number;
		resolution: number;
	}>,
) => TShaderChunks = (params) => ({
	uniformChunks: `uniform float uTime;
	uniform sampler2D texOffsetMap;
`,
	replacementChunks: {
		'#include <map_fragment>': `
		float animScale=${fmt(params?.resolution ?? 0.55)};
    vec2 uv = vUv.xy * animScale; 
  	vec4 texCol = vec4( texture2D(texOffsetMap, uv ) );
    mat3 tfm;
    tfm[0] = vec3(dot(texCol.xyz,vec3(0.2,0.7,0.1)),-0.2,0);
    tfm[1] = vec3(0.0,texCol.y,0.45);
    tfm[2] = vec3(0,0,1.0);    
    vec2 muv = (vec3(uv,1.0)*tfm).xy + uTime * ${fmt(params?.speed ?? 0.01)};
    vec4 texelColor = texture2D( map, muv );
		diffuseColor *= texelColor;
`,
	},
});

export default textureMapDisplacement;

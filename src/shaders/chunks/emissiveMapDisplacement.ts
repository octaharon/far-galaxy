import { TShaderChunks } from '../injectShaderChunks';

const fmt = (n: number) => n.toFixed(5);

const emissiveMapDisplacement: (
	params?: Partial<{
		speed: number;
		resolution: number;
	}>,
) => TShaderChunks = (params) => ({
	uniformChunks: `uniform float uTime;
	uniform sampler2D emissiveOffsetMap;
`,
	replacementChunks: {
		'#include <emissivemap_fragment>': `
		
		vec2 uv = vec2(fract(vUv.x * ${fmt(params?.resolution ?? 0.55)}), fract(vUv.y * ${fmt(params?.resolution ?? 0.55)})); 
  	vec4 texCol = vec4( texture2D(emissiveOffsetMap, uv ) );
  	vec3 luma = vec3(0.3, 0.4, 0.3);
    mat3 tfm;
    tfm[0] = vec3(dot(texCol.xyz,vec3(0.2,0.7,0.1)),-0.2,0);
    tfm[1] = vec3(0.0,texCol.y,0.45);
    tfm[2] = vec3(0,0,1.0);    
    vec2 muv = (vec3(uv,1.0)*tfm).xy + uTime * ${fmt(params?.speed ?? 0.01)};
    vec4 texelColor = texture2D( emissiveMap, muv );
		totalEmissiveRadiance *= texelColor.rgb;
`,
	},
});
export default emissiveMapDisplacement;

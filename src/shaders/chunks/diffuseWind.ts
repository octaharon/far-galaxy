import { TShaderChunks } from '../injectShaderChunks';

const fmt = (n: number) => n.toFixed(5);

const diffuseMapWind: (
	params?: Partial<{
		maxDepth: number;
		dampening: number;
		speed: number;
		direction: number;
	}>,
) => TShaderChunks = (params) => ({
	uniformChunks: `uniform float uTime;
	uniform sampler2D emissiveOffsetMap;
`,
	definitionsChunks: `
	float blendScreen(float base, float blend) {
					 // return base<0.5?2.0*base*blend:1.0-2.0*(1.0-base)*(1.0-blend);
					return base*blend;
					//return 1.0-(1.0-blend)/base;
		}
		
		vec3 blendScreen(vec3 base, vec3 blend) {
			return vec3(blendScreen(base.r,blend.r),blendScreen(base.g,blend.g),blendScreen(base.b,blend.b));
		}
		
		vec3 blendScreen(vec3 base, vec3 blend, float opacity) {
			return (blendScreen(base, blend) * opacity + base * (1.0 - opacity));
		}
		
		vec3 IBN (vec2 uv, float speedDamping, int depth, sampler2D map, sampler2D emissiveMap ) {
				vec3 color = vec3 (0.0);
				float sumW = 0.0;
				int numIter = min (depth, 64);
				for (int i = 1; i <= numIter; ++i) {
						float time = ${fmt(params?.speed ?? 0.8)}*uTime/(float (i));
						float w = 1.0-float (i)/float (depth);
						vec2 duv = vec2(dot(texture2D(emissiveOffsetMap, uv+vec2 (time)).rgb,vec3(0.33,0.33,0.33))/speedDamping);
						duv.y*=${fmt(params?.direction ?? -1)};
						vec3 rgb = texture2D(map, uv+duv).rgb;
						color += w * rgb;
						sumW += w;
				}
				return color/sumW;
		}
		`,
	replacementChunks: {
		'#include <map_fragment>': `
		
		vec2 windUv = vUv.xy;
    int depth = int (${fmt(params?.maxDepth ?? 50)});
    float speedDamping = ${fmt(params?.dampening ?? 10)};
    vec4 windColor = vec4 (IBN (windUv, speedDamping, depth, map, emissiveMap), 1.0);
		diffuseColor.rgb = blendScreen(diffuseColor.rgb, windColor.rgb);
`,
	},
});
export default diffuseMapWind;

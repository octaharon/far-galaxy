import { AdditiveBlending, Color, ShaderMaterial } from 'three';
import { ColorifyShader } from 'three/examples/jsm/shaders/ColorifyShader';
import BasicMaths from '../../definitions/maths/BasicMaths';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const glsl = require('glslify');

export const additiveTransparencyMaterial = (transparency = 1) =>
	new ShaderMaterial({
		vertexShader: ColorifyShader.vertexShader,
		fragmentShader: glsl`

		uniform float transparency;
		uniform sampler2D tDiffuse;

		varying vec2 vUv;

		void main() {

			vec4 texel = texture2D( tDiffuse, vUv );

			vec3 luma = vec3(0.33, 0.33, 0.33);
			float v = pow(dot( texel.rgb, luma ),0.05);
			gl_FragColor = vec4( texel.rgb, texel.a*v*transparency );
		}
		`,
		transparent: true,
		blending: AdditiveBlending,
		uniforms: {
			transparency: { value: BasicMaths.clipTo1(transparency) },
		},
	});

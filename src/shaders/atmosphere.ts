// eslint-disable-next-line @typescript-eslint/no-var-requires
const glsl = require('glslify');
export const AtmosphericShaders = {
	vertexShader: glsl`
		varying vec3	vVertexWorldPosition;
		varying vec3	vVertexNormal;
		void main(){
			vVertexNormal	= normalize(normalMatrix * normal);
			vVertexWorldPosition	= (modelMatrix * vec4(position, 1.0)).xyz;
			gl_Position	= projectionMatrix * modelViewMatrix * vec4(position, 1.0);
		}
		`,
	fragmentShader: glsl`
		uniform vec3	glowColor;
		uniform float	cf;
		uniform float	power;
		uniform float transparency;

		varying vec3	vVertexNormal;
		varying vec3	vVertexWorldPosition;

		void main(){
			vec3 worldCameraToVertex = vVertexWorldPosition - cameraPosition;
			vec3 viewCameraToVertex	= (viewMatrix * vec4(worldCameraToVertex, 0.0)).xyz;
			viewCameraToVertex = normalize(viewCameraToVertex);
			float intensity	= clamp(pow(cf + dot(vVertexNormal, viewCameraToVertex), power), 0.0, 1.0) * transparency;
			vec4 atmosGlowColor = vec4(glowColor, intensity);
			gl_FragColor = atmosGlowColor;
		}`,
};

import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import React from 'react';
import {
	connect,
	ReactReduxContextValue,
	TypedUseSelectorHook,
	createStoreHook,
	createDispatchHook,
	createSelectorHook,
	useSelector,
} from 'react-redux';
import {
	TIconGlobalStorage,
	TSVGIconComputedContext,
	TSVGIconContextInstance,
	TSVGIconContextSetters,
	TSVGIconContextVersion,
	TSVGIconTypeKey,
} from '../components/UI/SVG/IconContainer/types';
import { IconContainerDispatcher, IconContainerSelector, TWithIconContainerProps } from '../contexts/IconContainer';
import SVGIconContainerSlice, { defaultSVGIconContext } from '../contexts/IconContainer/slice';

export const SVGIconContainerStore = configureStore({
	reducer: {
		container: SVGIconContainerSlice.reducer,
	},
	preloadedState: {
		container: defaultSVGIconContext,
	},
});

export const SVGIconContainerContext = React.createContext<ReactReduxContextValue>(undefined);

export type TIconContainerDispatch = typeof SVGIconContainerStore.dispatch;
export type TIconContainerState = ReturnType<typeof SVGIconContainerStore.getState>;
export type TIconContainerThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	TIconContainerState,
	unknown,
	Action<string>
>;

export const useIconContainerDispatch = createDispatchHook<TIconContainerState>(SVGIconContainerContext);
export const useIconContainerSelector: TypedUseSelectorHook<TIconContainerState> =
	createSelectorHook(SVGIconContainerContext);

export const useIconContainer = (storage: TIconGlobalStorage) => {
	const dispatch = useIconContainerDispatch();
	const setters = IconContainerDispatcher(storage)(dispatch);
	return [useIconContainerSelector(IconContainerSelector(storage)), setters] as [
		TSVGIconContextVersion & TSVGIconComputedContext,
		TSVGIconContextSetters,
	];
};

export const withIconContainer = (storage: TIconGlobalStorage) =>
	connect(
		(state: TIconContainerState) =>
			({ container: IconContainerSelector(storage)(state) } as TWithIconContainerProps),
		null,
		null,
		{ context: SVGIconContainerContext },
	);

import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { connect, TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { GraphicSettingsSlice } from '../contexts/GraphicSettings/slice';
import SoundSettingsSlice from '../contexts/SoundSettings/slice';
import WindowStateSlice from '../contexts/WindowState/slice';

const configStorageKey = 'configuration';
const loadConfigFromStorage = () => {
	try {
		const initialState = DIContainer.getLocalStorage()?.getItem(configStorageKey);
		if (!initialState) return undefined;
		return JSON.parse(initialState);
	} catch (e) {
		console.error(e);
		return undefined;
	}
};

export const PersistentConfiguration = configureStore({
	reducer: {
		graphicSettings: GraphicSettingsSlice.reducer,
		soundSettings: SoundSettingsSlice.reducer,
		windowState: WindowStateSlice.reducer,
	},
	preloadedState: loadConfigFromStorage(),
});

const saveConfigToStorage = () => {
	DIContainer.getLocalStorage()?.setItem(configStorageKey, JSON.stringify(PersistentConfiguration.getState()));
};

export const unbindPersistentConfiguration = PersistentConfiguration.subscribe(saveConfigToStorage);
saveConfigToStorage();

export type TConfigDispatch = typeof PersistentConfiguration.dispatch;
export type TConfigState = ReturnType<typeof PersistentConfiguration.getState>;
export type TConfigThunk<ReturnType = void> = ThunkAction<ReturnType, TConfigState, unknown, Action<string>>;

export const useConfigDispatch = () => useDispatch<TConfigDispatch>();
export const useConfigSelector: TypedUseSelectorHook<TConfigState> = useSelector;

export const withPersistentConfiguration = connect((state: TConfigState) => state as TConfigState);

import React from 'react';
import { Provider } from 'react-redux';
import { PersistentConfiguration } from './persistentConfiguration';

export const PersistentConfigurationProvider: React.FC<React.PropsWithChildren<any>> = ({ children }) => (
	<Provider store={PersistentConfiguration}>{children}</Provider>
);

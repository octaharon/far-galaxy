import {
	GraphicSettingsQualityLevels,
	TEXTURE_GENERATED_RESOLUTIONS,
	TEXTURE_VIDEO_RESOLUTIONS,
	TGraphicQualityLevel,
	TGraphicTextureSize,
} from './slice';

export const getTextureVideoResolution = (t: TGraphicTextureSize) => TEXTURE_VIDEO_RESOLUTIONS[t ?? 'SD'];
export const getTextureGeneratedResolution = (t: TGraphicTextureSize) => TEXTURE_GENERATED_RESOLUTIONS[t ?? 'SD'];
export const doesTextureResolutionIncludes = (
	currentSetting: TGraphicTextureSize = 'QHD',
	targetSetting: TGraphicTextureSize = 'SD',
) => getTextureGeneratedResolution(currentSetting) >= getTextureGeneratedResolution(targetSetting);
export const doesQualitySettingIncludes = (
	currentSetting: TGraphicQualityLevel = 'Max',
	targetSetting: TGraphicQualityLevel = 'Min',
) =>
	(GraphicSettingsQualityLevels[currentSetting] ?? Infinity) >=
	(GraphicSettingsQualityLevels[targetSetting] ?? -Infinity);

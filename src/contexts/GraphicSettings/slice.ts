import { createAsyncThunk, createSlice, PayloadAction, ValidateSliceCaseReducers } from '@reduxjs/toolkit';

export const GraphicSettingsQualityLevels = {
	Min: 0,
	Med: 1,
	Max: 2,
};

export const TEXTURE_VIDEO_RESOLUTIONS = {
	SD: 480,
	HD: 720,
	UHD: 1080,
	QHD: 1440,
};
export type TGraphicTextureSize = keyof typeof TEXTURE_VIDEO_RESOLUTIONS;

export const TEXTURE_GENERATED_RESOLUTIONS: Record<TGraphicTextureSize, number> = {
	SD: 512,
	HD: 1024,
	UHD: 1536,
	QHD: 2048,
};

export type TGraphicQualityLevel = keyof typeof GraphicSettingsQualityLevels;

export const GraphicSettings_QualityLevels: TGraphicQualityLevel[] = Object.keys(GraphicSettingsQualityLevels);
export const GraphicSettings_TextureSizes: TGraphicTextureSize[] = Object.keys(TEXTURE_VIDEO_RESOLUTIONS);

export type TGraphicSettings = Partial<{
	qualityLevel: TGraphicQualityLevel;
	textureSize: TGraphicTextureSize;
	showTooltips: boolean;
	showDetails: boolean;
}>;

export type TGraphicSettingsSetters = {
	setQuality: (q: TGraphicQualityLevel) => any;
	setTextureSize: (q: TGraphicTextureSize) => any;
	toggleTooltips: () => any;
	toggleDetails: () => any;
};

export type TGraphicSettingsContext = TGraphicSettings & TGraphicSettingsSetters;

export const defaultGraphicSettings: TGraphicSettings = {
	qualityLevel: 'Max',
	textureSize: 'QHD',
	showDetails: true,
	showTooltips: true,
};

export const GraphicSettingsSlice = createSlice({
	name: 'GraphicSettings',
	initialState: defaultGraphicSettings,
	reducers: {
		toggleTooltips: (state) => {
			state.showTooltips = !state.showTooltips;
		},
		toggleDetails: (state) => {
			state.showDetails = !state.showDetails;
		},
		setQuality: (state, action: PayloadAction<TGraphicQualityLevel>) => {
			state.qualityLevel = action.payload || defaultGraphicSettings.qualityLevel;
		},
		setTextureSize: (state, action: PayloadAction<TGraphicTextureSize>) => {
			state.textureSize = action.payload || defaultGraphicSettings.textureSize;
		},
	},
});

export default GraphicSettingsSlice;

/* eslint-disable max-classes-per-file */
import memoize from 'memoize-one';
import React, { PropsWithChildren } from 'react';
import { connect } from 'react-redux';
import {
	TConfigDispatch,
	TConfigState,
	useConfigDispatch,
	useConfigSelector,
} from '../../stores/persistentConfiguration';
import { doesQualitySettingIncludes } from './helpers';
import GraphicSettingsSlice, {
	TGraphicQualityLevel,
	TGraphicSettings,
	TGraphicSettingsSetters,
	TGraphicTextureSize,
} from './slice';
import _ from 'underscore';

export const withGraphicQualityThreshold = <T,>(
	currentQuality: TGraphicQualityLevel,
	minQuality: TGraphicQualityLevel,
	content: T,
	fallback: T | null = null,
) => (doesQualitySettingIncludes(currentQuality, minQuality) ? content : fallback);

export const GraphicSettingsConfigSelector = (state: TConfigState) => state.graphicSettings;

export type TWithGraphicSettingsProps = {
	graphicSettings?: TGraphicSettings;
};
export const withGraphicSettings = connect(
	(state: TConfigState) => ({ graphicSettings: state.graphicSettings } as TWithGraphicSettingsProps),
);

export const GraphicSettingsConfigDispatchers = memoize(
	(dispatch: TConfigDispatch) =>
		({
			toggleDetails: () => dispatch(GraphicSettingsSlice.actions.toggleDetails()),
			toggleTooltips: () => dispatch(GraphicSettingsSlice.actions.toggleTooltips()),
			setQuality: (qualityLevel: TGraphicQualityLevel) =>
				dispatch(GraphicSettingsSlice.actions.setQuality(qualityLevel)),
			setTextureSize: (textureSize: TGraphicTextureSize) =>
				dispatch(GraphicSettingsSlice.actions.setTextureSize(textureSize)),
		} as TGraphicSettingsSetters),
	_.isEqual,
);

export const useGraphicSettings = () => {
	const dispatch = useConfigDispatch();
	const setters = GraphicSettingsConfigDispatchers(dispatch);
	return [useConfigSelector(GraphicSettingsConfigSelector), setters] as [TGraphicSettings, TGraphicSettingsSetters];
};

class GraphicQualityRequiredComponent extends React.PureComponent<
	TWithGraphicSettingsProps & PropsWithChildren<{ minQuality: TGraphicQualityLevel }>,
	never
> {
	render() {
		return doesQualitySettingIncludes(this.props.graphicSettings.qualityLevel, this.props.minQuality)
			? this.props.children
			: null;
	}
}

export const GraphicQualityRequired = withGraphicSettings(GraphicQualityRequiredComponent);

class MedGraphicQualityRequired extends React.PureComponent<PropsWithChildren<any>> {
	render() {
		return <GraphicQualityRequired minQuality={'Med'}>{this.props.children}</GraphicQualityRequired>;
	}
}
export const MedQualityRequired = withGraphicSettings(MedGraphicQualityRequired);

class MaxGraphicQualityRequired extends React.PureComponent<PropsWithChildren<any>> {
	render() {
		return <GraphicQualityRequired minQuality={'Max'}>{this.props.children}</GraphicQualityRequired>;
	}
}
export const MaxQualityRequired = withGraphicSettings(MaxGraphicQualityRequired);

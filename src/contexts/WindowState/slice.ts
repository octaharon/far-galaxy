import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import BasicMaths from '../../../definitions/maths/BasicMaths';

export const TWindowPages = {
	main: 'MainMenu.html',
	base: 'BaseLayout.html',
	prototypes: 'PrototypeEditor.html',
};

export type TWindowState = Partial<{
	currentWindow: keyof typeof TWindowPages;
	displaySettings: boolean;
}>;

export const defaultWindowState: TWindowState = {
	currentWindow: 'main',
	displaySettings: false,
};

export type TWindowStateSetters = {
	setPage: (page: TWindowState['currentWindow']) => any;
	setSettingsVisibility: (visible?: boolean) => any;
};

export const WindowStateSlice = createSlice({
	name: 'WindowState',
	initialState: defaultWindowState,
	reducers: {
		setPage: (state, action: PayloadAction<TWindowState['currentWindow']>) => {
			state.currentWindow = action.payload ?? defaultWindowState.currentWindow;
		},
		setSettingsVisibility: (state, action: PayloadAction<boolean>) => {
			state.displaySettings = action.payload ?? !state.displaySettings;
		},
	},
});

export default WindowStateSlice;

import { connect } from 'react-redux';
import {
	TConfigDispatch,
	TConfigState,
	useConfigDispatch,
	useConfigSelector,
} from '../../stores/persistentConfiguration';
import WindowStateSlice, { TWindowState, TWindowStateSetters } from './slice';

export const WindowStateConfigSelector = (state: TConfigState) => state.windowState;

export const WindowStateConfigDispatcher = (dispatch: TConfigDispatch) =>
	({
		setPage: (page) => dispatch(WindowStateSlice.actions.setPage(page)),
		setSettingsVisibility: (visible) => dispatch(WindowStateSlice.actions.setSettingsVisibility(visible)),
	} as TWindowStateSetters);

export const withWindowState = connect(WindowStateConfigSelector);

export const withWindowStateControls = connect(WindowStateConfigSelector, WindowStateConfigDispatcher);

export const useWindowState = () => {
	const dispatch = useConfigDispatch();
	const setters = WindowStateConfigDispatcher(dispatch);
	return [useConfigSelector(WindowStateConfigSelector), setters] as [TWindowState, TWindowStateSetters];
};

import React from 'react';
import { Provider } from 'react-redux';
import _ from 'underscore';
import {
	SVGIconContainerElementOrder,
	TIconGlobalStorage,
	TSVGIconComputedContext,
	TSVGIconContextInstance,
	TSVGIconContextSetters,
	TSVGIconContextVersion,
	TSVGIconTypeKey,
} from '../../components/UI/SVG/IconContainer/types';
import {
	SVGIconContainerContext,
	SVGIconContainerStore,
	TIconContainerDispatch,
	TIconContainerState,
} from '../../stores/iconContainer';
import { SVGIconContainerSlice } from './slice';

export const IconContainerSelector =
	(storage: TIconGlobalStorage) =>
	(state: TIconContainerState): TSVGIconContextVersion & TSVGIconComputedContext => {
		return {
			version: state.container.version,
			...SVGIconContainerElementOrder.reduce((obj, type) => {
				const items = state.container[type];
				return Object.assign(obj, {
					[type]: Object.keys(items)
						.filter((id) => storage[id])
						.reduce(
							(acc, id) => ({
								...acc,
								[id]: storage[id],
							}),
							{} as TSVGIconComputedContext[typeof type],
						),
				});
			}, {} as TSVGIconComputedContext),
		};
	};

export type TWithIconContainerProps = {
	container?: TSVGIconContextVersion & TSVGIconComputedContext;
};

export const IconContainerDispatcher =
	(storage: TIconGlobalStorage) =>
	(dispatch: TIconContainerDispatch): TSVGIconContextSetters => ({
		registerIcons(...props: Array<TSVGIconContextInstance<TSVGIconTypeKey>>) {
			for (const prop of props) {
				const { icon, id, type } = prop;
				if (storage[id]) continue;
				storage[prop.id] = React.cloneElement(icon as JSX.Element, {
					id,
					key: id,
				});
				dispatch(
					SVGIconContainerSlice.actions.register({
						id,
						type,
						icon: _.uniqueId(type),
					}),
				);
			}
		},
	});

export const IconContainerProvider: React.FC = ({ children }) => (
	<Provider store={SVGIconContainerStore} context={SVGIconContainerContext}>
		{children}
	</Provider>
);

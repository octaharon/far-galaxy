import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
	SVGIconContextKeys,
	TIconStorage,
	TSVGIconTypeKey,
	TSVGIconContext,
	TSVGIconTypeDiscriminator,
	TSVGIconContextInstance,
	TSVGIconPayload,
} from '../../components/UI/SVG/IconContainer/types';

export const defaultSVGIconContext = {
	ids: {},
	version: 0,
	...Object.values(SVGIconContextKeys).reduce(
		(acc, k) => ({
			...acc,
			[k]: {},
		}),
		{} as TIconStorage,
	),
} as TSVGIconContext;

export const isSVGIconContextInstanceOfType =
	<K extends TSVGIconTypeKey>(type: K) =>
	(p: TSVGIconTypeDiscriminator<any>): p is TSVGIconContextInstance<K> =>
		p?.type === type;

export const SVGIconContainerSlice = createSlice({
	name: 'SVGIcons',
	initialState: defaultSVGIconContext,
	reducers: {
		updateVersion(state) {
			state.version++;
		},
		register(state, action: PayloadAction<TSVGIconPayload<TSVGIconTypeKey>>) {
			const { id, icon, type } = action.payload;
			if (state.ids[id]) {
				console.error(`Duplicate ${type} #${id} found`);
			} else {
				state.ids[id] = type;
				state[type][id] = { id, type };
				state.version++;
			}
		},
	},
});

export default SVGIconContainerSlice;

import { connect } from 'react-redux';
import {
	TConfigDispatch,
	TConfigState,
	useConfigDispatch,
	useConfigSelector,
} from '../../stores/persistentConfiguration';
import SoundSettingsSlice, { TSoundSettings, TSoundSettingsSetters } from './slice';

export const SoundSettingsConfigSelector = (state: TConfigState) => state.soundSettings;

export type TWithSoundSettingsProps = {
	soundSettings?: TSoundSettings;
};
export const withSoundSettings = connect(
	(state: TConfigState) => ({ soundSettings: state.soundSettings } as TWithSoundSettingsProps),
);

export const SoundSettingsConfigDispatcher = (dispatch: TConfigDispatch) =>
	({
		setMasterVolume: (volume: number) => dispatch(SoundSettingsSlice.actions.setMasterVolume(volume)),
		setMusicVolume: (volume: number) => dispatch(SoundSettingsSlice.actions.setMusicVolume(volume)),
		setSfxVolume: (volume: number) => dispatch(SoundSettingsSlice.actions.setSfxVolume(volume)),
		toggleMute: () => dispatch(SoundSettingsSlice.actions.toggleMute()),
	} as TSoundSettingsSetters);

export const useSoundSettings = () => {
	const dispatch = useConfigDispatch();
	const setters = SoundSettingsConfigDispatcher(dispatch);
	return [useConfigSelector(SoundSettingsConfigSelector), setters] as [TSoundSettings, TSoundSettingsSetters];
};

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import BasicMaths from '../../../definitions/maths/BasicMaths';

export type TSoundSettings = Partial<{
	masterVolume: number;
	sfxVolume: number;
	musicVolume: number;
	muted: boolean;
}>;

export const defaultSoundSettings: TSoundSettings = {
	masterVolume: 0.5,
	sfxVolume: 1,
	musicVolume: 1,
	muted: false,
};

export type TSoundSettingsSetters = {
	setMasterVolume: (volume: number) => any;
	setMusicVolume: (volume: number) => any;
	setSfxVolume: (volume: number) => any;
	toggleMute: () => any;
};

export const SoundSettingsSlice = createSlice({
	name: 'SoundSettings',
	initialState: defaultSoundSettings,
	reducers: {
		setMasterVolume: (state, action: PayloadAction<number>) => {
			state.masterVolume = BasicMaths.clipTo1(action.payload ?? defaultSoundSettings.masterVolume);
			state.muted = state.masterVolume <= Number.EPSILON;
		},
		setMusicVolume: (state, action: PayloadAction<number>) => {
			state.musicVolume = BasicMaths.clipTo1(action.payload ?? defaultSoundSettings.musicVolume);
		},
		setSfxVolume: (state, action: PayloadAction<number>) => {
			state.sfxVolume = BasicMaths.clipTo1(action.payload ?? defaultSoundSettings.sfxVolume);
		},
		toggleMute: (state) => {
			state.muted = !state.muted;
		},
	},
});

export default SoundSettingsSlice;

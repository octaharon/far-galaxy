import BasicMaths from '../../../definitions/maths/BasicMaths';

export const normalizeVolume = (localVolume: number, masterVolume = 1) =>
	BasicMaths.clipTo1(localVolume * (Number.isFinite(masterVolume) ? masterVolume : 1));

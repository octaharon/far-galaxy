const Environment = {
	NOTION_API_KEY: process?.env?.NOTION_API_KEY ?? null,
};
const undefinedKeys = Object.keys(Environment).filter((k: keyof typeof Environment) => !Environment[k]);
if (undefinedKeys.length) throw new Error(`Environment Vars not set: ${undefinedKeys.join(', ')}`);
export default Environment;

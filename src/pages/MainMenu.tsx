/* eslint-disable @typescript-eslint/no-var-requires */
import * as React from 'react';
import _ from 'underscore';
import MenuSoundtrack from '../../assets/audio/ost/Far Galaxy.mp3';
import MenuBackgroundImage from '../../assets/img/bg/menu.avif';
import MenuBGVideoUHD from '../../assets/video/bg/menu1080.mp4';
import MenuBGVideoQHD from '../../assets/video/bg/menu1440.mp4';
import MenuBGVideoSD from '../../assets/video/bg/menu480.mp4';
import MenuBGVideoHD from '../../assets/video/bg/menu720.mp4';
import { AdaptiveVideoSource } from '../components/Media/AdaptiveVideoSource';
import BackgroundMusicPlayer from '../components/Media/MusicPlayer';
import { MenuSprites, UIMenuCaptionClass, UIMenuSprite } from '../components/Symbols/MenuAnimatedSprite';
import { preloadResources } from '../components/UI/preloading';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import onLoad, { hideLoader, setLoaderTitle, TRootComponentProps } from '../components/UI/tools';
import { TConfigState, withPersistentConfiguration } from '../stores/persistentConfiguration';

const preloadedImages: string[] = [
	MenuBackgroundImage,
	..._.reduce(MenuSprites, (arr, sprite) => arr.concat(sprite.bloom, sprite.base), []),
];

class App extends React.PureComponent<
	TRootComponentProps & TConfigState,
	{
		loaded: boolean;
	}
> {
	public constructor(props: any) {
		super(props);
		this.state = {
			loaded: false,
		};
	}

	componentDidMount() {
		requestAnimationFrame(() => {
			setLoaderTitle('Loading assets...');
			preloadResources([preloadedImages]).then((resources: any[]) => {
				this.setState({
					loaded: true,
				});
				this.props.hideLoader({
					withBg: false,
				});
			});
		});

		setTimeout(() => hideLoader(), 30000);
	}

	public render() {
		return (
			<React.Fragment>
				<BackgroundMusicPlayer src={MenuSoundtrack} />
				{this.state.loaded && (
					<SVGIconContainer>
						<div id="cinemaScreen" style={{ backgroundImage: `url${MenuBackgroundImage}` }}>
							<video
								autoPlay
								muted
								loop
								className="__showAfterLoad"
								preload="metadata"
								poster={MenuBackgroundImage}>
								<AdaptiveVideoSource
									sources={{
										1921: MenuBGVideoQHD,
										1920: MenuBGVideoUHD,
										1280: MenuBGVideoHD,
										860: MenuBGVideoSD,
									}}
								/>
							</video>
						</div>
						<div id="cinemaContent">
							<UIMenuSprite type={'conquest'} top={915} left={147} caption={'Conquest'} />
							<UIMenuSprite type={'skirmish'} top={677} left={1194} caption={'Skirmish'} />
							<UIMenuSprite type={'orbital'} top={177} left={245} caption={'Construction'} />
							<UIMenuSprite type={'prototype'} top={180} left={1777} caption={'Technology'} />
							<UIMenuSprite type={'galaxy'} top={920} left={1959} caption={'Navigation'} />
							<div
								style={{
									position: 'absolute',
									right: '1vmax',
									bottom: '1vmax',
									lineHeight: 1.2,
									textAlign: 'right',
								}}>
								<a className={UIMenuCaptionClass} href="#">
									Credits
								</a>
								<br />
								<a className={UIMenuCaptionClass} href="#">
									Exit
								</a>
							</div>
						</div>
					</SVGIconContainer>
				)}
			</React.Fragment>
		);
	}
}

onLoad(withPersistentConfiguration(App), true);

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { preloadGameLibrary } from '../../definitions/load';
import { defaultMapParameters } from '../../definitions/tactical/map';
import { UNIT_MAP_SIZE } from '../../definitions/tactical/units/types';
import PoliticalFactions from '../../definitions/world/factions';
import CMapContainer from '../components/CombatMap/MapContainer';
import ChassisImageProvider from '../components/Symbols/ChassisImageProvider';
import UIIndex, { classnames } from '../components/UI';
import SVGIconContainer from '../components/UI/SVG/IconContainer';

const hideLoader = () => {
	document.getElementById('pageContainer').className = classnames(UIIndex.GeneralStyles.container);
	document.querySelectorAll('.__showAfterLoad').forEach((el: HTMLElement) => (el.style.visibility = 'visible'));
	document.getElementById('preloader').remove();
	console.info('Loading done');
};

const App = () => {
	React.useEffect(() => hideLoader());
	return (
		<SVGIconContainer>
			<CMapContainer
				parameters={{
					...defaultMapParameters,
					temperature: 0.99,
					waterPresence: 0.95,
				}}
			>
				<ChassisImageProvider
					width={UNIT_MAP_SIZE}
					height={UNIT_MAP_SIZE}
					type={'topdown'}
					chassisId={89501}
					faction={PoliticalFactions.TFactionID.stellarians}
				/>
			</CMapContainer>
		</SVGIconContainer>
	);
};

window.addEventListener('load', (ev) => {
	preloadGameLibrary();
	requestAnimationFrame(() => {
		ReactDOM.render(<App />, document.getElementById('pageContainer'));
	});
});

import { useCallback, useMemo, useState } from 'react';
import * as React from 'react';
import { PlanetTypeCaptions, TPlanetProps, TPlanetType } from '../../definitions/space/Planets';
import { MAX_PLANET_ZONES } from '../../definitions/space/Planets/constants';
import { generatePlanetByType, getRandomPlanetType } from '../../definitions/space/Planets/generators';
import { Stats } from '@react-three/drei';
import { useControls, Leva } from 'leva';
import {
	colorIntToHex,
	getPlanetHash,
	getPlanetWithHexColors,
	getPlanetWithIntColors,
} from '../../definitions/space/Planets/helpers';
import { generateSystem } from '../../definitions/space/Systems/tools';
import UIPlanetImage from '../components/Symbols/PlanetImage';
import {
	SurfaceHeightTextureCache,
	SurfaceNormalTextureCache,
	SurfaceTextureCache,
} from '../components/Symbols/PlanetImage/textures';
import TextureContainer from '../components/Symbols/TextureContainer';
import { preloadResources } from '../components/UI/preloading';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import onLoad, { hideLoader, setLoaderTitle, TRootComponentProps } from '../components/UI/tools';
import { PersistentConfigurationProvider } from '../stores';
import { TConfigState } from '../stores/persistentConfiguration';
import _ from 'underscore';

require('../components/Symbols/PlanetImage/prepare');

const preloadedImages = Object.values(SurfaceTextureCache).concat(
	Object.values(SurfaceNormalTextureCache),
	Object.values(SurfaceHeightTextureCache),
);

const getPlanet = (type: TPlanetType = null) => {
	const system = generateSystem(true);
	const planet = _.sample(system.planets);
	return Object.assign(
		planet,
		generatePlanetByType(
			type || getRandomPlanetType(),
			_.sample(_.range(0, system.planets.length)),
			Math.random(),
			system,
		),
	) as TPlanetProps;
};

type TRootPropType = TRootComponentProps & TConfigState;

let planetCache = getPlanet();

const PlanetDebug = () => {
	const [planet, setPlanet] = useState(planetCache);
	const resetCtrl = useControls({
		// @ts-ignore
		Random: {
			type: 'BUTTON',
			label: 'New planet',
			onClick: () => {
				setPlanet(getPlanet());
			},
			settings: {
				disabled: false,
			},
		},
	});
	const [planetProps, setPlanetProps] = useControls(() => ({
		type: {
			value: planet.type,
			onChange: (v: TPlanetType) => {
				setPlanet(getPlanet(v));
			},
			transient: false,
			options: Object.keys(PlanetTypeCaptions)
				.filter((k) => k !== 'default')
				.reduce(
					(acc, k) =>
						Object.assign(acc, {
							[PlanetTypeCaptions[k]]: k,
						}),
					{},
				) as Record<string, TPlanetType>,
		},
		surfaceColorHex: {
			label: 'surface color',
			value: colorIntToHex(planet.surfaceColorHex),
		},
		atmosphericColorHex: {
			label: 'atmos color',
			value: colorIntToHex(planet.atmosphericColorHex),
		},
		starlightColorHex: {
			label: 'star color',
			value: colorIntToHex(planet.starlightColorHex),
		},
		age: {
			value: planet.age,
			min: 0,
			max: 1,
		},
		mass: {
			value: planet.mass,
			min: 0,
			max: 1,
		},
		size: {
			value: planet.size,
			min: 0,
			max: 1,
		},
		gravity: {
			value: planet.gravity,
			min: 0,
			max: 1,
		},
		atmosphericDensity: {
			label: 'atmos density',
			value: planet.atmosphericDensity,
			min: 0,
			max: 1,
		},
		waterPresence: {
			label: 'water',
			value: planet.waterPresence,
			min: 0,
			max: 1,
		},
		temperature: {
			value: planet.temperature,
			min: 0,
			max: 1,
		},
		radiationLevel: {
			label: 'radiation',
			value: planet.radiationLevel,
			min: 0,
			max: 1,
		},
		magneticField: {
			label: 'magnetism',
			value: planet.magneticField,
			min: 0,
			max: 1,
		},
		warpFactor: {
			label: 'warp',
			value: planet.warpFactor,
			min: 0,
			max: 1,
		},
		spinFactor: {
			label: 'spin',
			value: planet.spinFactor,
			min: 0,
			max: 1,
		},
		distanceFromStar: {
			label: 'distance',
			value: planet.distanceFromStar,
			min: 0,
			max: 1,
		},
	}));
	planetCache = Object.assign(planetCache, planet, getPlanetWithIntColors(planetProps));
	const rerollCtrl = useControls({
		// @ts-ignore
		Reroll: {
			type: 'BUTTON',
			onClick: () => {
				setPlanet(getPlanet(planetCache.type));
			},
			settings: {
				disabled: false,
			},
		},
	});
	React.useEffect(() => {
		if (planetProps.type !== planet.type) setPlanet(getPlanet(planetProps.type));
	}, [planetProps, planet]);
	React.useEffect(() => {
		setPlanetProps(getPlanetWithHexColors(_.omit(planet, 'name', 'regions')));
	}, [planet, setPlanetProps]);

	return (
		<>
			<UIPlanetImage key={getPlanetHash({ planet: planetCache })} planet={planetCache} />
			<Stats />
		</>
	);
};

class App extends React.PureComponent<TRootPropType, { loaded: boolean }> {
	constructor(props: TRootPropType) {
		super(props);
		this.state = { loaded: false };
	}

	componentDidMount() {
		requestAnimationFrame(() => {
			setLoaderTitle('Loading assets...');
			preloadResources([preloadedImages]).then((resources: any[]) => {
				this.setState({
					loaded: true,
				});
				this.props.hideLoader({
					withBg: false,
				});
			});
		});

		setTimeout(() => hideLoader(), 30000);
		this.props.hideLoader({
			withColumns: false,
			withBg: false,
			withScroll: true,
		});
	}

	render() {
		return (
			this.state.loaded && (
				<PersistentConfigurationProvider>
					<SVGIconContainer>
						<TextureContainer>
							<PlanetDebug />
						</TextureContainer>
					</SVGIconContainer>
				</PersistentConfigurationProvider>
			)
		);
	}
}

onLoad(App);

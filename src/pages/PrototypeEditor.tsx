import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableSerializables } from '../../definitions/core/DI/injections';
import { TPlayerId } from '../../definitions/player/playerId';
import Players from '../../definitions/player/types';
import { IPrototype } from '../../definitions/prototypes/types';
import { getAllFactionIds } from '../../definitions/world/factionIndex';
import PoliticalFactions from '../../definitions/world/factions';
import { UIPrototypeChassisSelector } from '../components/PrototypeEditor/PrototypeChassisSelector';
import UIPrototypePreview from '../components/PrototypeEditor/PrototypePreview';
import UIFactionCaption from '../components/UI/Political/FactionCaption';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import { TSVGSmallTexturedIconStyles } from '../components/UI/SVG/SmallTexturedIcon';
import UIListSelector from '../components/UI/Templates/ListSelector';
import onLoad, { TRootComponentProps } from '../components/UI/tools';
import { TWithGraphicSettingsProps } from '../contexts/GraphicSettings';
import prettyPrint from '../utils/prettyPrintJSON';

class App extends React.PureComponent<
	TRootComponentProps & TWithGraphicSettingsProps,
	{
		prototype: IPrototype;
		faction: PoliticalFactions.TFactionID;
		player: Players.IPlayer;
	}
> {
	protected get factions() {
		return getAllFactionIds();
	}

	public constructor(props: any) {
		super(props);
		const prototypeProvider = DIContainer.getProvider(DIInjectableSerializables.prototype);
		const defaultPrototypeProps = (playerId: TPlayerId) => ({
			playerId,
		});
		const defaultPrototype = (playerId: TPlayerId) => {
			const hash = window.location.hash.slice(1);
			if (hash) return prototypeProvider.createFromHash(hash, defaultPrototypeProps(playerId));
			return prototypeProvider.instantiate(defaultPrototypeProps(playerId));
		};

		const faction = this.factions[Math.floor(Math.random() * this.factions.length)];
		const player = DIContainer.getProvider(DIInjectableSerializables.player).getTestPlayer(faction);

		this.state = {
			faction,
			player,
			prototype: defaultPrototype(player.id),
		};
	}

	updatePrototype = (p: IPrototype, faction: PoliticalFactions.TFactionID = this.state.faction) => {
		if (!p) return;
		const t = p.clone();
		const player = DIContainer.getProvider(DIInjectableSerializables.player).getTestPlayer(faction);
		t.setOwner(player.id);
		t.update(true);
		this.setState(
			{
				...this.state,
				player,
				faction,
				prototype: t,
			},
			() => {
				const hash = '#' + DIContainer.getProvider(DIInjectableSerializables.prototype).getHash(t);
				if (history.pushState) {
					history.pushState(null, null, hash);
				} else {
					location.hash = hash;
				}
			},
		);
	};

	changeFaction = (t: PoliticalFactions.TFactionID) => this.updatePrototype(this.state.prototype, t);

	componentDidMount() {
		this.props.hideLoader({
			withColumns: false,
			withBg: false,
			withScroll: true,
		});
	}

	public render() {
		const serializedPrototype = this.state.prototype?.serialize();
		const text = prettyPrint(serializedPrototype, 2);

		/*const sampleUnit = DIContainer.getProvider(DIInjectableSerializables.unit).createFromPrototype(prototype.final);
    console.log(sampleUnit);*/
		console.log(this.state.prototype.final);
		return (
			<SVGIconContainer>
				<div
					style={{
						display: 'flex',
						flexDirection: 'row',
						flexWrap: 'wrap',
						alignContent: 'flex-start',
						alignItems: 'flex-start',
					}}
				>
					<div
						style={{
							position: 'relative',
							overflow: 'visible',
							maxWidth: '100vmin',
							flex: '0 0 calc(100vmin - 6rem)',
						}}
					>
						<UIListSelector
							selectedItem={this.state.faction}
							options={getAllFactionIds()}
							optionRender={(f) => <UIFactionCaption faction={f} tooltip={true} />}
							onSelect={this.changeFaction}
							buttonColor={TSVGSmallTexturedIconStyles.gray}
						/>
						<UIPrototypeChassisSelector
							player={this.state.player}
							prototype={this.state.prototype}
							updatePrototype={this.updatePrototype}
						/>
					</div>
					<div
						id="debugView"
						style={{
							flex: '1 1 auto',
							minWidth: '20rem',
							display: 'flex',
							flexDirection: 'row',
							flexWrap: 'wrap',
							color: '#FFF',
							fontSize: '1rem',
						}}
					>
						<UIPrototypePreview prototype={this.state.prototype} />
					</div>
				</div>
			</SVGIconContainer>
		);
	}
}

onLoad(App);

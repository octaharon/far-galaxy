import * as React from 'react';
import Weapons from '../../definitions/technologies/types/weapons';
import UIBuildingCatalogue from '../components/UI/Base/Buildings/BuildingCatalogue';
import UIIndex from '../components/UI/index';
import UIMenuBar from '../components/UI/Menu/MenuBar';
import UIArmorCatalogue from '../components/UI/Parts/Armor/ArmorCatalogue';
import UIChassisCatalogue from '../components/UI/Parts/Chassis/ChassisCatalogue';
import UIEquipmentCatalogue from '../components/UI/Parts/Equipment/EquipmentCatalogue';
import UIShieldCatalogue from '../components/UI/Parts/Shield/ShieldCatalogue';
import UIWeaponCatalogue from '../components/UI/Parts/Weapon/WeaponCatalogue';
import onLoad, { TRootComponentProps } from '../components/UI/tools';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import { withGraphicSettings } from '../contexts/GraphicSettings';
import { PersistentConfigurationProvider } from '../stores';

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	React.useEffect(
		() =>
			hideLoader({
				withColumns: true,
				withBg: true,
				withScroll: true,
				isShowcase: true,
			}),
		[hideLoader],
	);
	return (
		<PersistentConfigurationProvider>
			<SVGIconContainer>
				{/*<UIMenuBar floating={true} />*/}
				<UIBuildingCatalogue onBuildingSelected={null} className={UIIndex.panelClass} />
				<UIArmorCatalogue defaultSelectedArmor={20000} className={UIIndex.panelClass} />
				<UIArmorCatalogue defaultSelectedArmor={10000} short={true} className={UIIndex.panelClass} />
				<UIEquipmentCatalogue defaultSelectedEquipment={10000} short={true} className={UIIndex.panelClass} />
				<UIEquipmentCatalogue defaultSelectedEquipment={20000} className={UIIndex.panelClass} />
				<UIShieldCatalogue defaultSelectedShield={20000} className={UIIndex.panelClass} />
				<UIShieldCatalogue defaultSelectedShield={10000} short={true} className={UIIndex.panelClass} />
				<UIWeaponCatalogue defaultSelectedWeapon={20000} className={UIIndex.panelClass} />
				<UIWeaponCatalogue
					defaultSelectedWeapon={10000}
					short={true}
					className={UIIndex.panelClass}
					forSlot={Weapons.TWeaponSlotType.large}
				/>
				<UIChassisCatalogue defaultSelectedChassis={10000} className={UIIndex.panelClass} />
			</SVGIconContainer>
		</PersistentConfigurationProvider>
	);
};

onLoad(withGraphicSettings(App));

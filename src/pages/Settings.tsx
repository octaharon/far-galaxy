import * as React from 'react';
import UISettingsContainer from '../components/UI/Settings/SettingsContainer';
import onLoad, { TRootComponentProps } from '../components/UI/tools';
import { withWindowStateControls } from '../contexts/WindowState';
import { TWindowState, TWindowStateSetters } from '../contexts/WindowState/slice';

const App: React.FC<TRootComponentProps & TWindowState & TWindowStateSetters> = ({
	hideLoader,
	setSettingsVisibility,
}) => {
	React.useEffect(() => {
		setSettingsVisibility(true);
		hideLoader({
			withColumns: false,
			withBg: true,
			withScroll: true,
			isShowcase: false,
		});
	}, [hideLoader]);
	return <UISettingsContainer />;
};

onLoad(withWindowStateControls(App), true);

import elk, { ElkNode, ElkPrimitiveEdge } from 'elkjs/lib/elk.bundled';
import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableSerializables } from '../../definitions/core/DI/injections';
import TalentRegistry from '../../definitions/player/Talents/TalentRegistry';
import PoliticalFactions from '../../definitions/world/factions';
import { UITalentDescriptor } from '../components/UI/Player/TalentDescriptor';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import onLoad, { TRootComponentProps } from '../components/UI/tools';

const graph = {
	id: 'talents',
	layoutOptions: { 'elk.algorithm': 'layered' },
	children: [] as ElkNode[],
	edges: [] as ElkPrimitiveEdge[],
};

TalentRegistry.getAll().forEach((talent) => {
	graph.children.push({
		id: String(talent.id),
		width: 360,
		height: 180,
	});
	if (talent.prerequisiteTalentIds?.length)
		talent.prerequisiteTalentIds.forEach((connectionId) => {
			graph.edges.push({
				id: `gr-${connectionId}-${talent.id}`,
				source: `${connectionId}`,
				target: `${talent.id}`,
			});
		});
});

const e = new elk();
e.layout(graph, {}).then(console.log);

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	const player = DIContainer.getProvider(DIInjectableSerializables.player).getTestPlayer(
		PoliticalFactions.TFactionID.stellarians,
	);
	Object.keys(player.arsenal).forEach((arKey) => {
		Object.keys(player.arsenal[arKey]).forEach((techId) => {
			if (Math.random() > 0.2) player.arsenal[arKey][techId] = Math.random();
			if (Math.random() > 0.85) player.arsenal[arKey][techId] = 0;
		});
	});
	player.addXp(1);
	Object.keys(player.talents).forEach((talentId) => {
		if (player.talents[talentId].learned) player.addTechnologies(player.talents[talentId].technologyReward);
	});
	console.log(player);
	React.useEffect(() => {
		hideLoader({
			withColumns: false,
			withBg: false,
			withScroll: true,
			isShowcase: false,
		});
	}, [hideLoader]);
	return (
		<SVGIconContainer>
			<div className="masonry-layout">
				{!!player &&
					graph.children.map((t) => (
						<UITalentDescriptor
							key={t.id}
							player={player}
							talent={TalentRegistry.get(parseInt(t.id))}
							x={t.x}
							y={t.y}
						/>
					))}
			</div>
		</SVGIconContainer>
	);
};

onLoad(App);

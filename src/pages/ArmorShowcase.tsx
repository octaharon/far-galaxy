import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../definitions/core/DI/injections';
import UIArmorDescriptor from '../components/UI/Parts/Armor/ArmorDescriptor';
import onLoad, { TRootComponentProps } from '../components/UI/tools';
import SVGIconContainer from '../components/UI/SVG/IconContainer';

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	React.useEffect(
		() =>
			hideLoader({
				withColumns: true,
				withBg: true,
				withScroll: true,
				isShowcase: true,
			}),
		[hideLoader],
	);
	return (
		<SVGIconContainer>
			<div className="masonry-layout">
				{Object.values(DIContainer.getProvider(DIInjectableCollectibles.armor).getAll()).map((factory) => (
					<UIArmorDescriptor
						key={factory.id}
						className="masonry-layout__panel"
						details={false}
						armorId={factory.id}
					/>
				))}
			</div>
		</SVGIconContainer>
	);
};

onLoad(App);

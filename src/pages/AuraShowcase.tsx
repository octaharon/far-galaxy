import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../definitions/core/DI/injections';
import UIAuraDescriptor from '../components/UI/Parts/Ability/AuraDescriptor';
import onLoad, { TRootComponentProps } from '../components/UI/tools';
import SVGIconContainer from '../components/UI/SVG/IconContainer';

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	React.useEffect(
		() =>
			hideLoader({
				withColumns: true,
				withBg: true,
				withScroll: true,
				isShowcase: true,
			}),
		[hideLoader],
	);
	return (
		<>
			<SVGIconContainer>
				{Object.keys(DIContainer.getProvider(DIInjectableCollectibles.aura).getAll()).map((auraId) => {
					const aura = DIContainer.getProvider(DIInjectableCollectibles.aura).instantiate(null, auraId);
					return <UIAuraDescriptor aura={aura} key={auraId} disabled={Math.random() < 0.1} tooltip={true} />;
				})}
			</SVGIconContainer>
		</>
	);
};

onLoad(App);

import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../definitions/core/DI/injections';
import UIEquipmentDescriptor from '../components/UI/Parts/Equipment/EquipmentDescriptor';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import onLoad, { TRootComponentProps } from '../components/UI/tools';

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	React.useEffect(
		() =>
			hideLoader({
				withColumns: true,
				withBg: true,
				withScroll: true,
				isShowcase: true,
			}),
		[hideLoader],
	);
	const eqProvider = DIContainer.getProvider(DIInjectableCollectibles.equipment);
	return (
		<SVGIconContainer>
			<div className="masonry-layout">
				{Object.values(eqProvider.getAll()).map((equipment) => (
					<UIEquipmentDescriptor
						key={equipment.id}
						className="masonry-layout__panel"
						details={true}
						equipmentId={equipment.id}
					/>
				))}
			</div>
		</SVGIconContainer>
	);
};

onLoad(App);

import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../definitions/core/DI/injections';
import UIAbilityDescriptor from '../components/UI/Parts/Ability/AbilityDescriptor';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import onLoad, { TRootComponentProps } from '../components/UI/tools';

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	React.useEffect(
		() =>
			hideLoader({
				withColumns: true,
				withBg: true,
				withScroll: true,
				isShowcase: true,
			}),
		[hideLoader],
	);
	return (
		<>
			<SVGIconContainer>
				{Object.keys(DIContainer.getProvider(DIInjectableCollectibles.ability).getAll()).map((abilityId) => {
					const ability = DIContainer.getProvider(DIInjectableCollectibles.ability)
						.instantiate(null, abilityId)
						.init();
					ability.cooldownLeft = Math.random() >= 0 ? 0 : 1 /*Math.round(
									BasicMaths.lerp({
										min: 0,
										max: 1,
										x: Math.random(),
									}),
							  )*/;
					if (Math.random() >= 1.9) ability.usable = false;
					return <UIAbilityDescriptor ability={ability} key={abilityId} usable={true} />;
				})}
			</SVGIconContainer>
		</>
	);
};

onLoad(App);

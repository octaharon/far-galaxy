import * as React from 'react';
import { TWeapon } from '../../definitions/technologies/types/weapons';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../definitions/core/DI/injections';
import UIWeaponDescriptor from '../components/UI/Parts/Weapon/WeaponDescriptor';
import onLoad, { TRootComponentProps } from '../components/UI/tools';
import SVGIconContainer from '../components/UI/SVG/IconContainer';

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	React.useEffect(
		() =>
			hideLoader({
				withColumns: true,
				withBg: true,
				withScroll: true,
				isShowcase: true,
			}),
		[hideLoader],
	);
	return (
		<SVGIconContainer>
			<div className="masonry-layout">
				{Object.values(DIContainer.getProvider(DIInjectableCollectibles.weapon).getAll<TWeapon>()).map(
					(constructor) => (
						<UIWeaponDescriptor
							className="masonry-layout__panel"
							details={true}
							key={constructor.id}
							weaponId={constructor.id}
						/>
					),
				)}
			</div>
		</SVGIconContainer>
	);
};

onLoad(App);

import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../definitions/core/DI/injections';
import UIChassisDescriptor from '../components/UI/Parts/Chassis/ChassisDescriptor';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import onLoad, { TRootComponentProps } from '../components/UI/tools';

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	React.useEffect(
		() =>
			hideLoader({
				withColumns: false,
				withBg: true,
				withScroll: true,
				isShowcase: true,
			}),
		[hideLoader],
	);
	const eqProvider = DIContainer.getProvider(DIInjectableCollectibles.chassis);
	return (
		<SVGIconContainer>
			<div className="masonry-layout">
				{Object.values(eqProvider.getAll()).map((chassis) => (
					<UIChassisDescriptor
						key={chassis.id}
						className="masonry-layout__panel"
						details={true}
						chassisId={chassis.id}
					/>
				))}
			</div>
		</SVGIconContainer>
	);
};

onLoad(App);

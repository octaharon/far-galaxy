import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../definitions/core/DI/injections';
import UIShieldDescriptor from '../components/UI/Parts/Shield/ShieldDescriptor';
import onLoad, { TRootComponentProps } from '../components/UI/tools';
import SVGIconContainer from '../components/UI/SVG/IconContainer';

const App: React.FC<TRootComponentProps> = ({ hideLoader }) => {
	React.useEffect(
		() =>
			hideLoader({
				withColumns: true,
				withBg: true,
				withScroll: true,
				isShowcase: true,
			}),
		[hideLoader],
	);
	return (
		<SVGIconContainer>
			<div className="masonry-layout">
				{Object.values(DIContainer.getProvider(DIInjectableCollectibles.shield).getAll()).map((factory) => (
					<UIShieldDescriptor
						key={factory.id}
						className="masonry-layout__panel"
						details={false}
						shieldId={factory.id}
					/>
				))}
			</div>
		</SVGIconContainer>
	);
};

onLoad(App);

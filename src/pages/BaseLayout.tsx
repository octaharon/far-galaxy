/* eslint-disable @typescript-eslint/no-var-requires */
import * as React from 'react';
import DIContainer from '../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles, DIInjectableSerializables } from '../../definitions/core/DI/injections';
import { TBaseBuildingArray } from '../../definitions/orbital/base/types';
import AscendancySummitEstablishment from '../../definitions/orbital/establishments/all/AscendancySummit.503';
import FrequencyArrayEstablishment from '../../definitions/orbital/establishments/all/FrequencyArray.203';
import NeutrinoHullEstablishment from '../../definitions/orbital/establishments/all/NeutrinoHull.302';
import VertebralArmorEstablishment from '../../definitions/orbital/establishments/all/VertebralArmor.303';
import VRNetworkEstablishment from '../../definitions/orbital/establishments/all/VRNetwork.102';
import ColdFusionReactorFacility from '../../definitions/orbital/facilities/all/ColdFusionReactor.303';
import LivingShellFacility from '../../definitions/orbital/facilities/all/LivingShell.405';
import MassRelayFacility from '../../definitions/orbital/facilities/all/MassRelay.304';
import PlanetCrackerFacility from '../../definitions/orbital/facilities/all/PlanetCracker.408';
import QuarkStreamlineFacility from '../../definitions/orbital/facilities/all/QuarkStreamline.406';
import TemperingCauldronFacility from '../../definitions/orbital/facilities/all/TemperingCauldron.204';
import WormholeProjectorFacility from '../../definitions/orbital/facilities/all/WormholeProjector.103';
import MassiveIndustryComplex from '../../definitions/orbital/factories/all/MassiveIndustryComplex.600';
import NexusFoundryFactory from '../../definitions/orbital/factories/all/NexusFoundry.302';
import TitanForgeFactory from '../../definitions/orbital/factories/all/Titanforge.601';

import { TBaseBuildingType } from '../../definitions/orbital/types';
import PoliticalFactions from '../../definitions/world/factions';
import { BuildingPlaceholders, UIBuildingsContainer } from '../components/BaseLayout/BuildingsContainer';
import BuildingSpriteCollection from '../components/Symbols/buildingSprites';
import UIMenuBar from '../components/UI/Menu/MenuBar';
import { preloadResources } from '../components/UI/preloading';
import UISettingsContainer from '../components/UI/Settings/SettingsContainer';
import SVGIconContainer from '../components/UI/SVG/IconContainer';
import onLoad, { hideLoader, setLoaderTitle, TRootComponentProps } from '../components/UI/tools';
import { TConfigState, withPersistentConfiguration } from '../stores/persistentConfiguration';

const buildingImages = Object.keys(BuildingSpriteCollection).reduce(
	(arr, buildingType) =>
		arr.concat(
			Object.values(BuildingSpriteCollection[buildingType]).reduce(
				(a, buildingSprite) =>
					a.concat(
						Object.values(buildingSprite)
							.flatMap((v) => v)
							.filter((v) => /(avif|png|jpg|jpeg)$/i.test(String(v))),
					),
				[],
			),
		),
	[],
);

const preloadedImages: string[] = [...buildingImages, ...Object.keys(BuildingPlaceholders)];
let buildingSlots: EnumProxy<TBaseBuildingType, number>;
let buildings: TBaseBuildingArray;

class App extends React.PureComponent<
	TRootComponentProps & TConfigState,
	{
		loaded: boolean;
	}
> {
	public constructor(props: any) {
		super(props);
		this.state = {
			loaded: false,
		};
	}

	componentDidMount() {
		requestAnimationFrame(() => {
			setLoaderTitle('Loading assets');
			preloadResources([preloadedImages]).then((resources: any[]) => {
				buildingSlots = {
					[TBaseBuildingType.factory]: 5,
					[TBaseBuildingType.establishment]: 7,
					[TBaseBuildingType.facility]: 7,
				};
				buildings = {
					[TBaseBuildingType.factory]: [
						DIContainer.getProvider(DIInjectableCollectibles.factory).instantiate(
							null,
							NexusFoundryFactory.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.factory).instantiate(
							null,
							TitanForgeFactory.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.factory).instantiate(
							null,
							MassiveIndustryComplex.id,
						),
					],
					[TBaseBuildingType.establishment]: [
						DIContainer.getProvider(DIInjectableCollectibles.establishment).instantiate(
							null,
							FrequencyArrayEstablishment.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.establishment).instantiate(
							null,
							AscendancySummitEstablishment.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.establishment).instantiate(
							null,
							VertebralArmorEstablishment.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.establishment).instantiate(
							null,
							NeutrinoHullEstablishment.id,
						),
						null,
						DIContainer.getProvider(DIInjectableCollectibles.establishment).instantiate(
							null,
							VRNetworkEstablishment.id,
						),
					],
					[TBaseBuildingType.facility]: [
						DIContainer.getProvider(DIInjectableCollectibles.facility).instantiate(
							null,
							LivingShellFacility.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.facility).instantiate(
							null,
							MassRelayFacility.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.facility).instantiate(
							null,
							PlanetCrackerFacility.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.facility).instantiate(
							null,
							WormholeProjectorFacility.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.facility).instantiate(
							null,
							ColdFusionReactorFacility.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.facility).instantiate(
							null,
							TemperingCauldronFacility.id,
						),
						DIContainer.getProvider(DIInjectableCollectibles.facility).instantiate(
							null,
							QuarkStreamlineFacility.id,
						),
					],
				};
				this.setState({
					loaded: true,
				});
				hideLoader({
					withScroll: true,
				});
			});
		});

		setTimeout(() => hideLoader({ withScroll: true }), 30000);
	}

	public render() {
		// console.log(BuildingSpriteCollection);
		return (
			this.state.loaded && (
				<SVGIconContainer>
					<UIMenuBar />
					{this.props.windowState.displaySettings && <UISettingsContainer />}
					<UIBuildingsContainer
						player={DIContainer.getProvider(DIInjectableSerializables.player).getTestPlayer(
							PoliticalFactions.TFactionID.venaticians,
						)}
						buildings={buildings}
						buildingSlots={buildingSlots}
					/>
				</SVGIconContainer>
			)
		);
	}
}

onLoad(withPersistentConfiguration(App));

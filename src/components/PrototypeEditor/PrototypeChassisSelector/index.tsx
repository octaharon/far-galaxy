import React from 'react';
import Players from '../../../../definitions/player/types';
import { IPrototype } from '../../../../definitions/prototypes/types';
import { classnames } from '../../UI';
import UIChassisListSelector from '../../UI/Parts/Chassis/ChassisListSelector';
import { UIPrototypeChassisImage } from '../PrototypeChassisImage';
import UIPrototypeSlotsOverlay from '../PrototypeSlotsOverlay';
import styles from './index.scss';

type UIPrototypeChassisSelectorProps = {
	className?: string;
	player?: Players.IPlayer;
	prototype?: IPrototype;
	updatePrototype?: (p: IPrototype) => any;
};

export const UIPrototypeChassisSelector: React.FC<UIPrototypeChassisSelectorProps> = ({
	className,
	prototype,
	player,
	updatePrototype,
}) => {
	const chId = prototype.getChassis()?.static?.id || 10000;
	const [selectedChassis, setChassis] = React.useState<number>(chId);
	const updateChassis = (chId: number) => {
		setChassis(chId);
		if (prototype && updatePrototype) updatePrototype(prototype.setChassis(chId));
	};
	if (prototype && !prototype.getChassis()) prototype.setChassis(selectedChassis);

	return (
		<div className={classnames(styles.prototypeChassisSelector, className)}>
			<UIChassisListSelector
				className={styles.selector}
				onChassisSelected={updateChassis}
				selectedChassis={selectedChassis}
				player={prototype?.owner || player}
			/>
			<UIPrototypeChassisImage
				chassisId={selectedChassis}
				faction={(prototype?.owner || player)?.faction ?? null}
				className={styles.img}
			>
				<UIPrototypeSlotsOverlay prototype={prototype} updatePrototype={updatePrototype} />
			</UIPrototypeChassisImage>
		</div>
	);
};

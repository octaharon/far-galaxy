import React from 'react';
import DIContainer from '../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../definitions/core/DI/injections';
import { IPrototype } from '../../../../definitions/prototypes/types';
import { doesActionRequireWeapon } from '../../../../definitions/tactical/units/actions/ActionManager';
import UnitActions from '../../../../definitions/tactical/units/actions/types';
import { stackWeaponGroupModifiers } from '../../../../definitions/technologies/weapons/WeaponManager';
import UIContainer, { classnames } from '../../UI';
import UIArmorCatalogue from '../../UI/Parts/Armor/ArmorCatalogue';
import UIArmorDescriptor from '../../UI/Parts/Armor/ArmorDescriptor';
import UIArmorSlotDescriptor from '../../UI/Parts/Armor/ArmorSlotDescriptor';
import UIEquipmentCatalogue from '../../UI/Parts/Equipment/EquipmentCatalogue';
import UIEquipmentDescriptor from '../../UI/Parts/Equipment/EquipmentDescriptor';
import UIEquipmentSlotDescriptor from '../../UI/Parts/Equipment/EquipmentSlotDescriptor';
import UIShieldCatalogue from '../../UI/Parts/Shield/ShieldCatalogue';
import UIShieldDescriptor from '../../UI/Parts/Shield/ShieldDescriptor';
import UIShieldSlotDescriptor from '../../UI/Parts/Shield/ShieldSlotDescriptor';
import UIWeaponCatalogue from '../../UI/Parts/Weapon/WeaponCatalogue';
import UIWeaponDescriptor from '../../UI/Parts/Weapon/WeaponDescriptor';
import UIWeaponSlotDescriptor from '../../UI/Parts/Weapon/WeaponSlotDescriptor';
import UITooltip from '../../UI/Templates/Tooltip';
import styles from './index.scss';

type UIPrototypeSlotsOverlayProps = {
	className?: string;
	prototype: IPrototype;
	updatePrototype?: (p: IPrototype) => any;
};

type UIPrototypeSlotOverlayType = 'Armor' | 'Shield' | 'Weapon' | 'Equipment';

type UIPrototypeSlotsOverlayState = {
	selectingState: UIPrototypeSlotOverlayType;
	forSlot: number;
	selectedId: number;
	prototypeId: string;
};

export const UIPrototypeSlotSelector: React.FC<
	React.PropsWithChildren<{
		kind: UIPrototypeSlotOverlayType;
		selectedId?: number;
		originalId?: number;
		onSelect: (id: number) => any;
		onCancel: () => any;
	}>
> = ({ onCancel, onSelect, originalId, selectedId, children }) => {
	return (
		<div className={styles.selector}>
			<div className={styles.buttons}>
				<div className={classnames(styles.button, styles.cancel)} onClick={onCancel}>
					⮪ Cancel
				</div>
				{!!originalId && (
					<div className={classnames(styles.button, styles.reset)} onClick={() => onSelect(null)}>
						&times; Clear Slot
					</div>
				)}
			</div>
			{children}
			<div className={styles.buttons}>
				<div className={classnames(styles.button, styles.select)} onClick={() => onSelect(selectedId)}>
					&#10003; Select
				</div>
			</div>
		</div>
	);
};

export class UIPrototypeSlotsOverlay extends React.PureComponent<
	UIPrototypeSlotsOverlayProps,
	UIPrototypeSlotsOverlayState
> {
	public constructor(props: UIPrototypeSlotsOverlayProps) {
		super(props);
		this.reset = this.reset.bind(this);
		this.onSelect = this.onSelect.bind(this);
		this.onUpdate = this.onUpdate.bind(this);
		this.whatIsInSlot = this.whatIsInSlot.bind(this);
		this.switchSelectingState = this.switchSelectingState.bind(this);
		this.state = {
			selectingState: null,
			forSlot: null,
			selectedId: null,
			prototypeId: props.prototype?.getInstanceId(),
		};
	}

	protected get armorMods() {
		return this.props.prototype?.modifiers?.armor || [];
	}

	protected get weaponMods() {
		return this.props.prototype?.modifiers?.weapon || [];
	}

	protected get shieldMods() {
		return this.props.prototype?.modifiers?.shield || [];
	}

	protected get chassisMods() {
		return this.props.prototype?.modifiers?.chassis || [];
	}

	public static getDerivedStateFromProps(
		props: UIPrototypeSlotsOverlayProps,
		state: UIPrototypeSlotsOverlayState,
	): UIPrototypeSlotsOverlayState {
		if (props.prototype?.getInstanceId() !== state.prototypeId)
			return {
				selectingState: null,
				forSlot: null,
				selectedId: null,
				prototypeId: props.prototype?.getInstanceId(),
			};
		return state;
	}

	public reset() {
		this.setState({
			...this.state,
			forSlot: null,
			selectedId: null,
			selectingState: null,
		});
	}

	public onSelect() {
		if (this.props.updatePrototype) this.props.updatePrototype(this.props.prototype);
	}

	public switchSelectingState(selectingState: UIPrototypeSlotOverlayType, slotId: number, selectedId: number) {
		this.setState({
			...this.state,
			selectingState,
			forSlot: slotId,
			selectedId,
		});
	}

	public whatIsInSlot() {
		switch (this.state.selectingState) {
			case 'Armor':
				return this.props.prototype?.getArmor(this.state.forSlot)?.static?.id || null;
			case 'Weapon':
				return this.props.prototype?.getWeapon(this.state.forSlot)?.static?.id || null;
			case 'Shield':
				return this.props.prototype?.getShield(this.state.forSlot)?.static?.id || null;
			case 'Equipment':
				return this.props?.prototype.getEquipment(this.state.forSlot)?.static?.id || null;
			default:
				return null;
		}
	}

	public onUpdate(id: number) {
		switch (this.state.selectingState) {
			case 'Armor':
				this.props?.prototype?.setArmor(this.state.forSlot, id);
				break;
			case 'Weapon':
				this.props?.prototype?.setWeapon(this.state.forSlot, id);
				break;
			case 'Shield':
				this.props?.prototype?.setShield(this.state.forSlot, id);
				break;
			case 'Equipment':
				this.props?.prototype?.setEquipment(this.state.forSlot, id);
				break;
			default:
		}
		this.onSelect();
	}

	public render() {
		const { className, prototype } = this.props;
		if (!prototype.getChassis()) return null;
		const chassis = DIContainer.getProvider(DIInjectableCollectibles.chassis).get(
			prototype.getChassis()?.static?.id,
		);
		return (
			<div
				className={classnames(
					styles.prototypeSlotsOverlay,
					this.state.selectingState ? styles.locked : null,
					className,
				)}>
				{this.state.selectingState === null ? (
					<React.Fragment>
						{!!chassis.weaponSlots?.length && (
							<div className={styles.weaponSlots}>
								{chassis.weaponSlots.map((ws, ix) => (
									<div
										className={classnames(
											styles.slot,
											prototype.hasWeaponAtSlot(ix) ? UIContainer.hasTooltipClass : null,
										)}
										key={ix}>
										<UIWeaponSlotDescriptor
											slotType={prototype.hasWeaponAtSlot(ix) ? null : ws}
											tooltip={false}
											highlight={prototype.hasWeaponAtSlot(ix)}
											weaponGroup={
												prototype.hasWeaponAtSlot(ix)
													? prototype.getWeapon(ix)?.meta?.group?.id
													: null
											}
											onClick={() =>
												this.switchSelectingState(
													'Weapon',
													ix,
													prototype.getWeapon(ix)?.static?.id || null,
												)
											}
											className={styles.slotIcon}
										/>
										{prototype.hasWeaponAtSlot(ix) && (
											<UITooltip className={styles.hoverDescriptor}>
												<UIWeaponDescriptor
													weaponId={prototype.getWeapon(ix)?.static?.id}
													phases={
														prototype
															.getChassis()
															?.actions?.filter((v) =>
																[
																	UnitActions.unitActionTypes.attack,
																	UnitActions.unitActionTypes.watch,
																	UnitActions.unitActionTypes.barrage,
																].some((t) => v.includes(t)),
															)?.length || 0
													}
													modifiers={[
														stackWeaponGroupModifiers(...this.weaponMods)?.[
															prototype.getWeapon(ix)?.meta?.group?.id
														] || stackWeaponGroupModifiers(...this.weaponMods)?.default,
													]}
													tooltip={false}
													details={true}
													currentSlot={ws}
												/>
											</UITooltip>
										)}
									</div>
								))}
							</div>
						)}
						{!!chassis.armorSlots?.length && (
							<div className={styles.armorSlots}>
								{chassis.armorSlots.map((ws, ix) => (
									<div
										className={classnames(
											styles.slot,
											prototype.hasArmorAtSlot(ix) ? UIContainer.hasTooltipClass : null,
										)}
										key={ix}>
										<UIArmorSlotDescriptor
											slotType={prototype.hasArmorAtSlot(ix) ? null : ws}
											onClick={() =>
												this.switchSelectingState(
													'Armor',
													ix,
													prototype.getArmor(ix)?.static?.id || null,
												)
											}
											highlight={prototype.hasArmorAtSlot(ix)}
											className={styles.slotIcon}
											tooltip={false}
										/>
										{prototype.hasArmorAtSlot(ix) && (
											<UITooltip className={styles.hoverDescriptor}>
												<UIArmorDescriptor
													armorId={prototype.getArmor(ix)?.static?.id}
													modifiers={this.armorMods}
													tooltip={false}
													details={true}
												/>
											</UITooltip>
										)}
									</div>
								))}
							</div>
						)}
						{!!chassis.shieldSlots?.length && (
							<div className={styles.shieldSlots}>
								{chassis.shieldSlots.map((ws, ix) => (
									<div
										className={classnames(
											styles.slot,
											prototype.hasShieldAtSlot(ix) ? UIContainer.hasTooltipClass : null,
										)}
										key={ix}>
										<UIShieldSlotDescriptor
											slotType={prototype.hasShieldAtSlot(ix) ? null : ws}
											onClick={() =>
												this.switchSelectingState(
													'Shield',
													ix,
													prototype.getShield(ix)?.static?.id || null,
												)
											}
											highlight={prototype.hasShieldAtSlot(ix)}
											className={styles.slotIcon}
										/>
										{prototype.hasShieldAtSlot(ix) && (
											<UITooltip className={styles.hoverDescriptor}>
												<UIShieldDescriptor
													shieldId={prototype.getShield(ix)?.static?.id}
													tooltip={false}
													modifiers={this.shieldMods}
													details={true}
												/>
											</UITooltip>
										)}
									</div>
								))}
							</div>
						)}
						{!!prototype.getChassis()?.equipmentSlots && (
							<div className={styles.equipmentSlots}>
								{new Array(prototype.getChassis()?.equipmentSlots).fill(0).map((ws, ix) => (
									<div
										className={classnames(
											styles.slot,
											prototype.hasEquipmentAtSlot(ix) ? UIContainer.hasTooltipClass : null,
										)}
										key={ix}>
										<UIEquipmentSlotDescriptor
											onClick={() =>
												this.switchSelectingState(
													'Equipment',
													ix,
													prototype.getEquipment(ix)?.static?.id || null,
												)
											}
											highlight={prototype.hasEquipmentAtSlot(ix)}
											group={DIContainer.getProvider(
												DIInjectableCollectibles.equipment,
											).getGroupById(prototype.getEquipment(ix)?.static?.id)}
											className={styles.slotIcon}
										/>
										{prototype.hasEquipmentAtSlot(ix) && (
											<UITooltip className={styles.hoverDescriptor}>
												<UIEquipmentDescriptor
													equipmentId={prototype.getEquipment(ix)?.static?.id}
													tooltip={false}
													details={true}
												/>
											</UITooltip>
										)}
									</div>
								))}
							</div>
						)}
					</React.Fragment>
				) : (
					<UIPrototypeSlotSelector
						kind={this.state.selectingState}
						onCancel={this.reset}
						selectedId={this.state.selectedId}
						originalId={this.whatIsInSlot()}
						onSelect={this.onUpdate}>
						{this.state.selectingState === 'Weapon' && (
							<UIWeaponCatalogue
								className={styles.catalogue}
								onWeaponSelected={(id) => this.switchSelectingState('Weapon', this.state.forSlot, id)}
								player={this.props.prototype?.owner}
								phases={
									prototype
										.getChassis()
										?.actions?.filter((v) => v.some((t) => doesActionRequireWeapon(t)))?.length || 0
								}
								short={true}
								tooltip={true}
								defaultSelectedWeapon={this.whatIsInSlot() || this.state.selectedId}
								forSlot={chassis.weaponSlots[this.state.forSlot]}
								modifiers={this.weaponMods}
							/>
						)}
						{this.state.selectingState === 'Armor' && (
							<UIArmorCatalogue
								className={styles.catalogue}
								onArmorSelected={(id) => this.switchSelectingState('Armor', this.state.forSlot, id)}
								player={this.props.prototype?.owner}
								short={true}
								tooltip={true}
								defaultSelectedArmor={this.whatIsInSlot() || this.state.selectedId}
								forSlot={chassis.armorSlots[this.state.forSlot]}
								modifiers={this.armorMods}
							/>
						)}
						{this.state.selectingState === 'Shield' && (
							<UIShieldCatalogue
								className={styles.catalogue}
								onShieldSelected={(id) => this.switchSelectingState('Shield', this.state.forSlot, id)}
								player={this.props.prototype?.owner}
								short={true}
								tooltip={true}
								defaultSelectedShield={this.whatIsInSlot() || this.state.selectedId}
								forSlot={chassis.shieldSlots[this.state.forSlot]}
								modifiers={this.shieldMods}
							/>
						)}
						{this.state.selectingState === 'Equipment' && (
							<UIEquipmentCatalogue
								className={styles.catalogue}
								onEquipmentSelected={(id) =>
									this.switchSelectingState('Equipment', this.state.forSlot, id)
								}
								player={this.props.prototype?.owner}
								short={true}
								defaultSelectedEquipment={this.whatIsInSlot() || this.state.selectedId}
							/>
						)}
					</UIPrototypeSlotSelector>
				)}
			</div>
		);
	}
}

export default UIPrototypeSlotsOverlay;

import * as React from 'react';
import DIContainer from '../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles, DIInjectableSerializables } from '../../../../definitions/core/DI/injections';
import { IPrototype } from '../../../../definitions/prototypes/types';
import {
	UIBackdropPatternID,
	UIPrototypeBackdropPatternID,
	UIPrototypeBackgroundGradientID,
	UITalentBackdropPatternID,
	UITalentBackgroundGradientID,
	UIUnitBackdropPatternID,
	UIUnitBackgroundGradientID,
} from '../../Symbols/backdrops';
import { classnames, IUICommonProps } from '../../UI';
import UIRawMaterialsDescriptor from '../../UI/Economy/RawMaterialsDescriptor';
import UIAbilityDescriptor from '../../UI/Parts/Ability/AbilityDescriptor';
import UIAuraDescriptor from '../../UI/Parts/Ability/AuraDescriptor';
import UIChassisClassDescriptor from '../../UI/Parts/Chassis/ChassisClassDescriptor';
import UIChassisDescriptor from '../../UI/Parts/Chassis/ChassisDescriptor';
import UIHitPointsDescriptor from '../../UI/Parts/Chassis/HitPointsDescriptor';
import UILearningCurveDescriptor from '../../UI/Parts/Chassis/LearningCurveDescriptor';
import UILevelingDescriptor from '../../UI/Parts/Chassis/LevelingDescriptor';
import UIPromotionClassDescriptor from '../../UI/Parts/Chassis/PromotionClassDescriptor';
import UIScanRangeDescriptor from '../../UI/Parts/Chassis/ScanRangeDescriptor';
import UISpeedDescriptor from '../../UI/Parts/Chassis/SpeedDescriptor';
import UITargetTypeDescriptor from '../../UI/Parts/Chassis/TargetTypeDescriptor';
import UIDodgeDescriptor from '../../UI/Parts/Defense/DodgeDescriptor';
import UIActionCounter from '../../UI/Prototype/ActionCount';
import UIActionTable from '../../UI/Prototype/ActionTable';
import UIPrototypeModifier from '../../UI/Prototype/PrototypeModifier';
import UIRankCollection from '../../UI/Prototype/RankCollection';
import { UIStatusEffectDescriptor } from '../../UI/Prototype/StatusEffectDescriptor';
import UIBackdrop, { UIBackdropGradientID } from '../../UI/SVG/Backdrop';

import styles from './index.scss';

type UIPrototypePreviewProps = IUICommonProps & {
	prototype: IPrototype;
};

export default class UIPrototypePreview extends React.PureComponent<UIPrototypePreviewProps, {}> {
	public static getClass(): string {
		return styles.prototypePreviewBlock;
	}

	public render() {
		if (!this.props.prototype || !this.props.prototype.getChassis()) return null;
		const prototype = this.props.prototype;
		const fPrototype = prototype.final;
		const unit = DIContainer.getProvider(DIInjectableSerializables.unit).createFromPrototype(fPrototype);
		// console.log(unit);
		return (
			<>
				<div className={classnames(UIPrototypePreview.getClass(), this.props.className || '')}>
					<UIBackdrop
						className={styles.frame}
						gradientId={UITalentBackgroundGradientID}
						patternId={UITalentBackdropPatternID}
					/>
					<div className={styles.content}>
						<dl className={styles.meta}>
							<dt className={styles.group}>
								<UIChassisClassDescriptor
									chassisType={prototype.getChassis()?.static?.type}
									tooltip={true}
								/>
							</dt>
							<dd className={styles.cost}>
								<UIRawMaterialsDescriptor
									short={true}
									value={fPrototype.cost}
									className={styles.costDescriptor}
								/>
							</dd>
							<dt className={styles.title}>{fPrototype.caption}</dt>
						</dl>
						<div className={styles.description}>
							{UIChassisDescriptor.getIcon(
								prototype.getChassis()?.static?.id,
								prototype.owner ? prototype.owner.faction : null,
							)}
							{prototype.getChassis().static.description}
						</div>
						<UITargetTypeDescriptor targetFlags={prototype.chassis.static.flags} tooltip={true} />
						<UIHitPointsDescriptor maxHitPoints={fPrototype.getChassis().hitPoints} tooltip={true} />
						<UISpeedDescriptor
							movement={fPrototype.getChassis().static.movement}
							speed={fPrototype.getChassis().speed}
							tooltip={true}
						/>
						<UIScanRangeDescriptor scanRange={fPrototype.getChassis().scanRange} tooltip={true} />
						{fPrototype.getBaseEffects().map((eff) => (
							<UIStatusEffectDescriptor
								className={styles.effectDescriptor}
								key={eff.effectId}
								tooltip={true}
								effect={DIContainer.getProvider(DIInjectableCollectibles.effect).instantiate(
									eff?.props || null,
									eff?.effectId,
								)}
							/>
						))}
						{fPrototype.getBaseAuras().map((auraAppliance) => (
							<UIAuraDescriptor
								className={styles.abilityDescriptor}
								key={auraAppliance.id}
								tooltip={true}
								aura={DIContainer.getProvider(DIInjectableCollectibles.aura).instantiate(
									auraAppliance.props || null,
									auraAppliance.id,
								)}
							/>
						))}
						<UIDodgeDescriptor
							dodge={fPrototype.getChassis().dodge}
							tooltip={true}
							movement={fPrototype.getChassis().static.movement}
						/>
					</div>
				</div>
				<div className={classnames(UIPrototypePreview.getClass(), this.props.className || '')}>
					<UIBackdrop
						className={styles.frame}
						gradientId={UIPrototypeBackgroundGradientID}
						patternId={UIPrototypeBackdropPatternID}
					/>
					<div className={styles.content}>
						<UIActionCounter actions={fPrototype.getEffectiveActions()} tooltip={true} />
						<UIActionTable actions={fPrototype.getEffectiveActions()} tooltip={true} />
						{fPrototype.getBaseAbilities().map((abilityID) => (
							<UIAbilityDescriptor
								className={styles.abilityDescriptor}
								key={abilityID}
								ability={DIContainer.getProvider(DIInjectableCollectibles.ability).instantiate(
									null,
									abilityID,
								)}
							/>
						))}
					</div>
				</div>
				<div className={classnames(UIPrototypePreview.getClass(), this.props.className || '')}>
					<UIBackdrop
						className={styles.frame}
						gradientId={UIUnitBackgroundGradientID}
						patternId={UIUnitBackdropPatternID}
					/>
					<div className={styles.content}>
						<UIPrototypeModifier modifier={prototype.modifiers} />
					</div>
				</div>
				<div className={classnames(UIPrototypePreview.getClass(), this.props.className || '')}>
					<UIBackdrop
						className={styles.frame}
						gradientId={UIBackdropGradientID}
						patternId={UIBackdropPatternID}
					/>
					<div className={styles.content}>
						<UILearningCurveDescriptor
							levelingFunction={fPrototype.getChassis().static.levelTree.levelingFunction}
							tooltip={true}
						/>
						<UILevelingDescriptor
							maxLevel={3}
							levelingFunction={fPrototype.getChassis().static.levelTree.levelingFunction}
							className={styles.levelingDescriptor}
						/>
						<UIPromotionClassDescriptor
							promotionClass={fPrototype.getChassis().static.levelTree}
							tooltip={true}
						/>
						<UIRankCollection
							tooltip={true}
							className={styles.abilityDescriptor}
							talents={fPrototype.getChassis().static.levelTree}
							filter={(e) => e.talent.minimumLevel <= 1}
						/>
					</div>
				</div>
			</>
		);
	}
}

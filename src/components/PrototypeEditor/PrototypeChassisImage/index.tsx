import React from 'react';
import DIContainer from '../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../definitions/core/DI/injections';
import BasicMaths from '../../../../definitions/maths/BasicMaths';
import UnitChassis, { IChassisStatic } from '../../../../definitions/technologies/types/chassis';
import PoliticalFactions from '../../../../definitions/world/factions';
import {
	MaxQualityRequired,
	MedQualityRequired,
	useGraphicSettings,
	withGraphicQualityThreshold,
} from '../../../contexts/GraphicSettings';
import ChassisImageProvider from '../../Symbols/ChassisImageProvider';
import { getChassisDepthMask, getChassisWaterMask } from '../../Symbols/ChassisImageProvider/depthMasks';
import {
	chassisCushions,
	defaultDepthCurve,
	depthCurves,
	transformByClass,
} from '../../Symbols/ChassisImageProvider/extras';
import { classnames, getTemplateColor, IUICommonProps } from '../../UI';
import SVGIconContainer, {
	SVGBasicNoiseFilterID,
	SVGFractalNoiseFilterID,
	SVGMaskFilterID,
} from '../../UI/SVG/IconContainer';
import styles from './index.scss';

type UIPrototypeChassisImageProps = IUICommonProps & {
	chassisId: number;
	faction?: PoliticalFactions.TFactionID;
};

export const SVGUniqueGlowGradientId = 'unique-chassis-glow-gradient';
export const SVGUniqueGlowFilterId = 'unique-chassis-glow-filter';
export const SVGRefractFilterId = 'basic-refract-filter';

const SVGDepthMaskId = 'chassis-depth-mask';
const SVGDepthMaskTransparencyFilter = 'chassis-depth-filter';
const SVGCloudTextureId = 'chassis-cloud-texture';
const SVGCloudFilterId = 'chassis-cloud-filter';
const SVGWaterUnitFilter = 'chassis-water-unit-filter';
const SVGOutlineMaskId = 'chassis-outline-mask';
const SVGSymbolId = 'chassis-sprite-symbol';
const SVGGroundMaskId = 'chassis-sprite-ground-mask';
const SVGBackgroundGradientId = 'chassis-bg-gradient';
const SVGWaterGradientId = 'chassis-water-gradient';
const SVGWaterMaskId = 'chassis-water-mask';
const SVGCushionGradientId = 'chassis-cushion-gradient';
const SVGCushionFilterId = 'chassis-cushion-filter';
const SVGCushionSymbolId = 'chassis-cushion';

const chassisViewboxSize = 1920;
const waterViewboxLevel = 597;

const cushionTranslateSpeed = 0.75;
const cushionInterval = 0.2;
const cushionScaleSpeed = 0.5;
const cushionRingRadius = 210;
const cushionRingEccentricity = 3 / 7;
const cushionHeight = 350;

const SVGChassisCushionSymbol = (
	<symbol
		viewBox={`-${chassisViewboxSize / 2} -${chassisViewboxSize / 2} ${chassisViewboxSize} ${chassisViewboxSize}`}
		id={SVGCushionSymbolId}>
		{new Array(8).fill(null).map((d, ix) => (
			<g key={ix}>
				<MedQualityRequired>
					<animateTransform
						attributeType="XML"
						attributeName="transform"
						type="scale"
						values="0.6;0.7;1"
						dur={`${cushionTranslateSpeed}s`}
						repeatCount="indefinite"
						fill="freeze"
						additive="sum"
						begin={`${cushionInterval * ix}s`}
					/>
				</MedQualityRequired>
				<ellipse
					key={ix}
					cx="0"
					cy="0"
					stroke={SVGIconContainer.getUrlRef(SVGCushionGradientId)}
					strokeWidth="10"
					fill="none"
					rx={cushionRingRadius.toPrecision(3)}
					ry={(cushionRingRadius * cushionRingEccentricity).toPrecision(3)}
					className={styles.additive}
					filter={SVGIconContainer.getUrlRef(SVGCushionFilterId)}
					opacity=".7">
					<MaxQualityRequired>
						<animateTransform
							attributeType="XML"
							attributeName="transform"
							type="scale"
							values="1;0.85;1"
							dur={`${BasicMaths.lerp({
								x: Math.random(),
								min: cushionScaleSpeed * 0.9,
								max: cushionScaleSpeed * 1.1,
							}).toPrecision(2)}s`}
							repeatCount="indefinite"
							fill="freeze"
							additive="sum"
							begin={`${(Math.random() * 0.5).toPrecision(2)}s`}
						/>
						<animateTransform
							attributeType="XML"
							attributeName="transform"
							type="translate"
							from="0 0"
							to={`0 ${cushionHeight}`}
							additive="sum"
							dur={`${cushionTranslateSpeed}s`}
							repeatCount="indefinite"
							fill="freeze"
							begin={`${cushionInterval * ix}s`}
						/>
						<animate
							attributeName="opacity"
							attributeType="XML"
							values="0;.7;1;1;0"
							begin={`${cushionInterval * ix}s`}
							fill="freeze"
							repeatCount="indefinite"
							additive="sum"
							dur={`${cushionTranslateSpeed}s`}
						/>
					</MaxQualityRequired>
				</ellipse>
			</g>
		))}
	</symbol>
);

const SVGExtraDefs = (
	<>
		<linearGradient gradientUnits="objectBoundingBox" x1="0" x2="0" y1="0" y2="100%" id={SVGCushionGradientId}>
			<stop stopColor="#07F" stopOpacity="0.1" offset="0" />
			<stop stopColor="#CFF" stopOpacity="0.6" offset="1" />
		</linearGradient>
		<linearGradient id={SVGGroundMaskId} x1="0" y1="0" x2="0" y2="100%" gradientUnits="objectBoundingBox">
			<stop stopColor="#FFF" offset={0} />
			<stop stopColor="#777" offset={0.4} />
			<stop stopColor="#000" offset={0.8} />
			<stop stopColor="#000" offset={1} />
		</linearGradient>
		<linearGradient x1="35%" x2="55%" y1="0%" y2="100%" id={SVGWaterGradientId} gradientUnits="objectBoundingBox">
			<stop stopColor="#143" offset="0" />
			<stop stopColor="#333" offset="0.3" />
			<stop stopColor="#356" offset="0.65" />
			<stop stopColor={getTemplateColor('navy_blue')} offset="1" />
		</linearGradient>
		<linearGradient id={SVGBackgroundGradientId} x1="0" x2="0" y1="0" y2="100%" gradientUnits="objectBoundingBox">
			<stop stopColor="#000" offset="0" stopOpacity={1} />
			<stop stopColor={getTemplateColor('tooltip_bg')} offset="0.1" stopOpacity={0.75} />
			<stop stopColor={getTemplateColor('secondary_bg')} offset="0.4" stopOpacity={0.85} />
			<stop stopColor={getTemplateColor('text_dark')} offset="0.65" stopOpacity={1} />
			<stop stopColor="#000" offset="1" stopOpacity={1} />
		</linearGradient>
		<filter
			id={SVGCloudFilterId}
			colorInterpolationFilters="sRGB"
			filterUnits="objectBoundingBox"
			height="200%"
			primitiveUnits="userSpaceOnUse"
			width="200%"
			x="-50%"
			y="-50%">
			<feTurbulence
				type="turbulence"
				baseFrequency="0.02 0.011"
				numOctaves="2"
				seed="16"
				stitchTiles="stitch"
				result="turbulence">
				<MaxQualityRequired>
					<animate
						attributeType="XML"
						attributeName="seed"
						dur="4s"
						fill="freeze"
						from="0"
						repeatCount="indefinite"
						to="100"
					/>
				</MaxQualityRequired>
			</feTurbulence>
			<feMorphology operator="dilate" radius="2 6" in="SourceGraphic" result="morphology">
				<MaxQualityRequired>
					<animate
						attributeName="radius"
						attributeType="XML"
						dur="10s"
						fill="freeze"
						repeatCount="indefinite"
						values="2 6; 4 4; 6 2; 4 4; 2 6"
						keyTimes="0; 0.25; 0.5; 0.75; 1"
					/>
				</MaxQualityRequired>
			</feMorphology>
			<feDisplacementMap
				in2="turbulence"
				in="morphology"
				scale="60"
				xChannelSelector="G"
				yChannelSelector="B"
				result="displacementMap"
			/>
			<feGaussianBlur stdDeviation="15 15" in="displacementMap" edgeMode="wrap" result="blur" />
		</filter>
		<filter
			colorInterpolationFilters="linearRGB"
			filterUnits="objectBoundingBox"
			id={SVGCloudTextureId}
			primitiveUnits="userSpaceOnUse"
			width="150%"
			height="150%"
			x="-25%"
			y="-25%">
			<feTurbulence
				baseFrequency="0.002 0.0017"
				numOctaves="4"
				stitchTiles="stitch"
				type="fractalNoise"
				result="turbulence"
				seed="2"
			/>
			<feColorMatrix in="turbulence" result="colormatrix" type="saturate" values="0" />
			<feColorMatrix in="colormatrix" result="colormatrix1" type="luminanceToAlpha" />
			<feMorphology in="colormatrix1" operator="dilate" radius="2" result="morphology" />
			<feColorMatrix
				in="morphology"
				result="colormatrix2"
				type="matrix"
				values="-1 0 0 0 1
                                                             0 -1 0 0 1
                                                             0 0 -1 0 1
                                                             0 0 0 1 0"
			/>
			<feGaussianBlur stdDeviation="5 5" in="colormatrix2" edgeMode="wrap" result="blur" />
		</filter>
		<filter
			id={SVGCushionFilterId}
			x="-100%"
			y="-100%"
			width="300%"
			height="300%"
			filterUnits="objectBoundingBox"
			colorInterpolationFilters="sRGB">
			<feMorphology operator="dilate" radius="10" in="morphology" result="morphology1" />
			<feTurbulence
				type="turbulence"
				baseFrequency="0.01 0.05"
				numOctaves="2"
				seed="20"
				stitchTiles="stitch"
				result="turbulence">
				<MaxQualityRequired>
					<animate
						attributeName="seed"
						attributeType="XML"
						fill="freeze"
						from="20"
						to="100"
						repeatCount="indefinite"
						dur="3s"
					/>
				</MaxQualityRequired>
			</feTurbulence>
			<feGaussianBlur stdDeviation="15" in="morphology1" result="blur" />
			<feDisplacementMap
				in="blur"
				in2="turbulence"
				scale="68"
				xChannelSelector="R"
				yChannelSelector="G"
				result="displacementMap"
			/>
			<feColorMatrix type="hueRotate" values="-35" in="SourceGraphic" result="colormatrix" />
			<feMorphology operator="erode" radius="4" in="displacementMap" result="morphology" />
			<feOffset dx="-15" dy="-15" in="morphology" result="offset" />
			<feBlend mode="hard-light" in="offset" in2="colormatrix" result="blend" />
			<feGaussianBlur stdDeviation="3" in="blend" result="blur1" />
			<feDropShadow
				stdDeviation="10 10"
				in="blur1"
				dx="0"
				dy="15"
				floodColor="#bf9ef0"
				floodOpacity="0.5"
				result="dropShadow"
			/>
		</filter>
	</>
);

export const UIPrototypeChassisImage: React.FC<UIPrototypeChassisImageProps> = ({
	children,
	className,
	chassisId,
	faction,
}) => {
	if (!chassisId) return null;
	const [{ showTooltips, showDetails, qualityLevel }] = useGraphicSettings();
	const provider = DIContainer.getProvider(DIInjectableCollectibles.chassis);
	let chassis: IChassisStatic = null;
	try {
		chassis = provider?.get(chassisId);
	} catch (e) {
		console.error(e);
		return null;
	}
	const depthMaskCurve = depthCurves[chassis.type] || defaultDepthCurve;
	const aboveClouds = !provider.isMassive(chassisId) && !provider.isNaval(chassisId);
	const transformOrigin = { transformOrigin: `${chassisViewboxSize / 2}px ${chassisViewboxSize / 2}px` };
	const transformStyle = {
		...transformOrigin,
		transform: provider.isMassive(chassisId)
			? null
			: `scale(${transformByClass[chassis.type]?.scale || 1}) translate(${
					chassisViewboxSize * (transformByClass[chassis.type]?.offsetX || 0)
			  }px, ${chassisViewboxSize * (transformByClass[chassis.type]?.offsetY || 0)}px)`,
	};
	const spriteSymbol = (
		<React.Fragment>
			{!!chassisCushions[chassis.type] &&
				provider.isHover(chassisId) &&
				chassisCushions[chassis.type].map((transforms, ix) => (
					<g
						key={ix}
						style={{
							transform: `scale(${transforms.scale || 1}, 1) translate(${
								(transforms.offsetX || 0) - chassisViewboxSize / 2
							}px, ${(transforms.offsetY || 0) - chassisViewboxSize / 2}px)`,
						}}>
						<use {...SVGIconContainer.getXLinkProps(SVGCushionSymbolId)} />
					</g>
				))}
			<use {...SVGIconContainer.getXLinkProps(SVGSymbolId)} />
		</React.Fragment>
	);
	return (
		<div className={classnames(styles.prototypeChassisImage, className)}>
			<svg className={styles.svgWrapper} viewBox={`0 0 ${chassisViewboxSize} ${chassisViewboxSize}`}>
				<defs>
					{SVGChassisCushionSymbol}
					<symbol id={SVGSymbolId}>
						<g style={transformStyle}>
							<ChassisImageProvider
								type={'full'}
								chassisId={chassisId}
								faction={faction}
								className={styles.svgImage}
							/>
						</g>
					</symbol>
					{SVGExtraDefs}
					<mask id={SVGWaterMaskId}>
						<rect
							x={0}
							y={0}
							width={chassisViewboxSize}
							height={waterViewboxLevel}
							fill="#000"
							opacity={1}
						/>
						<rect
							x={0}
							y={waterViewboxLevel}
							width={chassisViewboxSize}
							height={chassisViewboxSize - waterViewboxLevel}
							fill="#FFF"
							opacity={1}
						/>
						<image height="100%" width="100%" x="0" y="0" xlinkHref={getChassisWaterMask(chassisId)} />
					</mask>
					<mask id={SVGDepthMaskId}>
						<rect
							x={0}
							y={0}
							width={chassisViewboxSize}
							height={chassisViewboxSize}
							fill={provider.isAir(chassisId) ? '#FFF' : SVGIconContainer.getUrlRef(SVGGroundMaskId)}
							opacity={1}
						/>
						<g style={transformStyle} filter={SVGIconContainer.getUrlRef(SVGDepthMaskTransparencyFilter)}>
							<image height="100%" width="100%" x="0" y="0" xlinkHref={getChassisDepthMask(chassisId)} />
						</g>
					</mask>
					<mask id={SVGOutlineMaskId}>
						<rect fill="#000" height={chassisViewboxSize} width={chassisViewboxSize} x="0" y="0" />
						<g filter={SVGIconContainer.getUrlRef(SVGMaskFilterID)}>
							<use {...SVGIconContainer.getXLinkProps(SVGSymbolId)} />
						</g>
					</mask>
					<filter
						id={SVGDepthMaskTransparencyFilter}
						x="0%"
						y="0%"
						width="100%"
						height="100%"
						filterUnits="objectBoundingBox"
						primitiveUnits="objectBoundingBox"
						colorInterpolationFilters="sRGB">
						<feComponentTransfer in="SourceGraphic" result="componentTransfer">
							<feFuncR type="table" tableValues={depthMaskCurve.join(' ')} />
							<feFuncG type="table" tableValues={depthMaskCurve.join(' ')} />
							<feFuncB type="table" tableValues={depthMaskCurve.join(' ')} />
							<feFuncA type="table" tableValues="0 1" />
						</feComponentTransfer>
					</filter>
					<filter
						id={SVGWaterUnitFilter}
						x="-20%"
						y="-20%"
						width="140%"
						height="140%"
						filterUnits="objectBoundingBox"
						primitiveUnits="userSpaceOnUse">
						{/* Preparing Mask Images: maskImage, totalMask*/}
						<feImage
							xlinkHref={getChassisWaterMask(chassisId)}
							result="maskImage"
							x={0}
							y={0}
							width="100%"
							height="100%"
							preserveAspectRatio="xMidYMid slice"
						/>
						{/* Preparing Noise Generators: turbulence, noise, ripple*/}
						<feTurbulence
							type="turbulence"
							baseFrequency="0.001 0.005"
							numOctaves="2"
							seed="43"
							stitchTiles="stitch"
							result="turbulence"
						/>
						<feTurbulence
							type="fractalNoise"
							baseFrequency="0.003 0.013"
							numOctaves="4"
							seed="16"
							stitchTiles="stitch"
							result="noise"
						/>
						<feColorMatrix type="hueRotate" result="noise_rotate" in="noise">
							<MedQualityRequired>
								<animate attributeName="values" values="0;360" dur="3.5s" repeatCount="indefinite" />
							</MedQualityRequired>
						</feColorMatrix>
						<feMorphology
							operator="erode"
							radius="2"
							x="0%"
							y="0%"
							width="100%"
							height="100%"
							in="noise_rotate"
							result="morphology1"
						/>
						<feDisplacementMap
							in="morphology1"
							in2="turbulence"
							scale="11"
							xChannelSelector="R"
							yChannelSelector="B"
							x="0%"
							y="0%"
							width="100%"
							height="100%"
							result="noiseDisplacementMap"
						/>
						<feMorphology
							operator="dilate"
							radius="2 0"
							x="0%"
							y="0%"
							width="100%"
							height="100%"
							in="noiseDisplacementMap"
							result="ripple"
						/>
						<feDisplacementMap
							in="ripple"
							in2="turbulence"
							scale="15"
							xChannelSelector="R"
							yChannelSelector="G"
							x="0%"
							y="0%"
							width="100%"
							height="100%"
							result="wavesDisplacement"
						/>
						<feBlend
							mode="hard-light"
							x="0%"
							y="0%"
							width="100%"
							height="100%"
							in="wavesDisplacement"
							in2="SourceGraphic"
							result="waves"
						/>
						<feDiffuseLighting
							surfaceScale="25"
							diffuseConstant="0.75"
							lightingColor="#054034"
							x="0%"
							azimuth="1"
							y="0%"
							width="100%"
							height="100%"
							in="waves"
							result="shadowed">
							<feDistantLight azimuth="150" elevation="150">
								<MaxQualityRequired>
									<animate
										attributeType="XML"
										attributeName="azimuth"
										from="1"
										to="361"
										fill="freeze"
										repeatCount="indefinite"
										dur="9.5s"
									/>
								</MaxQualityRequired>
							</feDistantLight>
						</feDiffuseLighting>
						<feGaussianBlur stdDeviation="3" in="shadowed" result="blurredLight" />
						<feBlend
							mode="overlay"
							x="0%"
							y="0%"
							width="100%"
							height="100%"
							in="waves"
							in2="blurredLight"
							result="texturedWater"
						/>
						{/* Stacking Layers: masking textured water to "finalWater"*/}
						<feComposite
							in="texturedWater"
							in2="maskImage"
							operator="arithmetic"
							k1={0.3}
							k2={1}
							k3={0}
							k4={0}
							result="waterMask"
						/>
					</filter>
				</defs>
				<rect
					id="chassis_bg_gradient"
					x={0}
					y={0}
					width={chassisViewboxSize}
					height={chassisViewboxSize}
					fill={SVGIconContainer.getUrlRef(SVGBackgroundGradientId)}
				/>
				{!aboveClouds && spriteSymbol}
				<g
					id="chassis_water"
					//mask={SVGIconContainer.getUrlRef(SVGWaterMaskId)}
					filter={withGraphicQualityThreshold(
						qualityLevel,
						'Med',
						SVGIconContainer.getUrlRef(SVGRefractFilterId),
					)}
					mask={SVGIconContainer.getUrlRef(SVGWaterMaskId)}
					opacity={provider.isNaval(chassisId) ? 1 : 0}>
					<g filter={SVGIconContainer.getUrlRef(SVGWaterUnitFilter)}>
						<rect
							fill={SVGIconContainer.getUrlRef(SVGWaterGradientId)}
							width={chassisViewboxSize}
							height={chassisViewboxSize}
							x="0"
							y="0"
						/>
					</g>
				</g>

				{!provider.isAir(chassisId) && (
					<g
						id="chassis_ground_fog"
						mask={SVGIconContainer.getUrlRef(SVGDepthMaskId)}
						className={styles.additive}
						filter={withGraphicQualityThreshold(
							qualityLevel,
							'Max',
							SVGIconContainer.getUrlRef(SVGRefractFilterId),
						)}>
						<g
							filter={withGraphicQualityThreshold(
								qualityLevel,
								'Med',
								SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID),
							)}>
							<circle
								fill="#999"
								r={1.5 * chassisViewboxSize}
								cx={0}
								cy={chassisViewboxSize * 0.5}
								filter={withGraphicQualityThreshold(
									qualityLevel,
									'Med',
									SVGIconContainer.getUrlRef(SVGCloudTextureId),
								)}>
								<MaxQualityRequired>
									<animate
										attributeName="opacity"
										attributeType="XML"
										dur="5s"
										fill="freeze"
										repeatCount="indefinite"
										values="1; 0.75; 1"
										keyTimes="0; 0.35; 1"
									/>
									<animateTransform
										additive="sum"
										attributeName="transform"
										attributeType="XML"
										dur="41s"
										fill="freeze"
										from={`360 ${-chassisViewboxSize * 0} ${chassisViewboxSize * 0.5}`}
										repeatCount="indefinite"
										to={`0 ${-chassisViewboxSize * 0} ${chassisViewboxSize * 0.5}`}
										type="rotate"
									/>
								</MaxQualityRequired>
							</circle>
							<circle
								fill="#999"
								opacity={0.75}
								r={1.5 * chassisViewboxSize}
								cx={chassisViewboxSize * 1}
								cy={chassisViewboxSize * 0.5}
								filter={withGraphicQualityThreshold(
									qualityLevel,
									'Med',
									SVGIconContainer.getUrlRef(SVGCloudTextureId),
								)}>
								<MaxQualityRequired>
									<animate
										attributeName="opacity"
										attributeType="XML"
										dur="5s"
										fill="freeze"
										repeatCount="indefinite"
										values="0.75; 1; 0.75"
										keyTimes="0; 0.65; 1"
									/>
									<animateTransform
										additive="sum"
										attributeName="transform"
										attributeType="XML"
										dur="39s"
										fill="freeze"
										from={`0 ${chassisViewboxSize * 1} ${chassisViewboxSize * 0.5}`}
										repeatCount="indefinite"
										to={`360 ${chassisViewboxSize * 1} ${chassisViewboxSize * 0.5}`}
										type="rotate"
									/>
								</MaxQualityRequired>
							</circle>
							<g
								filter={withGraphicQualityThreshold(
									qualityLevel,
									'Med',
									SVGIconContainer.getUrlRef(SVGFractalNoiseFilterID),
								)}
								style={{ mixBlendMode: 'hard-light' }}>
								<rect
									x={0}
									y={0}
									width={chassisViewboxSize}
									height={chassisViewboxSize}
									style={{
										...transformOrigin,
										transform: 'rotate(0deg)',
									}}
									filter={SVGIconContainer.getUrlRef(SVGCloudTextureId)}>
									<animate
										attributeName="opacity"
										attributeType="XML"
										dur="6s"
										fill="freeze"
										repeatCount="indefinite"
										values="0.75; 0.9; 0.4; 0.75"
										keyTimes="0; 0.35; 0.65; 1"
									/>
								</rect>
							</g>
						</g>
					</g>
				)}
				{provider.isAir(chassisId) && (
					<g id="chassis_view_clouds" mask={SVGIconContainer.getUrlRef(SVGDepthMaskId)}>
						<g
							filter={withGraphicQualityThreshold(
								qualityLevel,
								'Med',
								SVGIconContainer.getUrlRef(SVGRefractFilterId),
							)}
							style={{
								transformOrigin: '960px 960px' /*
								 transform: 'scale(2) rotate(30deg)',*/,
							}}>
							<circle
								fill="#CCC"
								r={2 * chassisViewboxSize}
								cx={-chassisViewboxSize * 0.5}
								cy={chassisViewboxSize * 1.5}
								filter={SVGIconContainer.getUrlRef(SVGCloudTextureId)}>
								<MaxQualityRequired>
									<animate
										attributeName="opacity"
										attributeType="XML"
										dur="9s"
										fill="freeze"
										repeatCount="indefinite"
										values="1; 0.7; 1"
										keyTimes="0; 0.35; 1"
									/>
									<animateTransform
										additive="sum"
										attributeName="transform"
										attributeType="XML"
										dur="24s"
										fill="freeze"
										from={`360 ${-chassisViewboxSize * 0.5} ${chassisViewboxSize * 1.5}`}
										repeatCount="indefinite"
										to={`0 ${-chassisViewboxSize * 0.5} ${chassisViewboxSize * 1.5}`}
										type="rotate"
									/>
								</MaxQualityRequired>
							</circle>
							<circle
								fill="#CCC"
								r={2 * chassisViewboxSize}
								cx={chassisViewboxSize * 1.5}
								cy={-chassisViewboxSize * 0.5}
								filter={withGraphicQualityThreshold(
									qualityLevel,
									'Med',
									SVGIconContainer.getUrlRef(SVGCloudTextureId),
								)}>
								<MaxQualityRequired>
									<animate
										attributeName="opacity"
										attributeType="XML"
										dur="10.5s"
										fill="freeze"
										repeatCount="indefinite"
										values="0.65; 1; 0.65"
										keyTimes="0; 0.65; 1"
									/>
									<animateTransform
										additive="sum"
										attributeName="transform"
										attributeType="XML"
										dur="19s"
										fill="freeze"
										from={`0 ${chassisViewboxSize * 1.5} ${-chassisViewboxSize * 0.5}`}
										repeatCount="indefinite"
										to={`360 ${chassisViewboxSize * 1.5} ${-chassisViewboxSize * 0.5}`}
										type="rotate"
									/>
								</MaxQualityRequired>
							</circle>
						</g>
					</g>
				)}
				{aboveClouds && spriteSymbol}
				{!!chassis.flags[UnitChassis.targetType.unique] && (
					<g
						style={{ mixBlendMode: 'hard-light' }}
						filter={SVGIconContainer.getUrlRef(SVGUniqueGlowFilterId)}>
						<g mask={SVGIconContainer.getUrlRef(SVGOutlineMaskId)}>
							<circle
								fill={SVGIconContainer.getUrlRef(SVGUniqueGlowGradientId)}
								r={chassisViewboxSize * 1.3}
								cx={chassisViewboxSize / 2}
								y={chassisViewboxSize / 2}>
								<MaxQualityRequired>
									<animateTransform
										additive="sum"
										attributeName="transform"
										attributeType="XML"
										dur="4s"
										fill="freeze"
										from="0 960 960"
										repeatCount="indefinite"
										to="360 960 960"
										type="rotate"
									/>
								</MaxQualityRequired>
							</circle>
						</g>
					</g>
				)}
			</svg>
			<div
				className={classnames(
					styles.border,
					provider.isAir(chassisId) ? styles.air : null,
					provider.isNaval(chassisId) ? styles.water : null,
					provider.isHover(chassisId) ? styles.magnetic : null,
				)}
			/>
			{children}
		</div>
	);
};

SVGIconContainer.registerGradient(
	SVGUniqueGlowGradientId,
	<linearGradient gradientUnits="userSpaceOnUse">
		<stop offset="0.1" stopColor={getTemplateColor('light_purple')} stopOpacity="0.8" />
		<stop offset="0.35" stopColor={getTemplateColor('light_blue')} stopOpacity="0.75" />
		<stop offset="0.62" stopColor={getTemplateColor('light_pink')} stopOpacity="0.9" />
		<stop offset="0.9" stopColor={getTemplateColor('light_golden')} stopOpacity="0.75" />
	</linearGradient>,
)
	.registerFilter(
		SVGRefractFilterId,
		<filter
			colorInterpolationFilters="sRGB"
			filterUnits="objectBoundingBox"
			height="200%"
			primitiveUnits="userSpaceOnUse"
			width="200%"
			x="-50%"
			y="-50%">
			<feTurbulence baseFrequency="0.005" numOctaves="2" stitchTiles="stitch" type="fractalNoise" />
			<feColorMatrix type="hueRotate">
				<animate attributeName="values" values="0;360" dur="6.5s" repeatCount="indefinite" />
			</feColorMatrix>
			<feDisplacementMap in="SourceGraphic" scale="90" xChannelSelector="R" yChannelSelector="G" />
		</filter>,
	)
	.registerFilter(
		SVGUniqueGlowFilterId,
		<filter
			colorInterpolationFilters="sRGB"
			filterUnits="userSpaceOnUse"
			height="300%"
			primitiveUnits="userSpaceOnUse"
			width="300%"
			x="-100%"
			y="-100%">
			<feMorphology
				height="200%"
				in="SourceGraphic"
				operator="dilate"
				radius="10"
				result="morphology"
				width="200%"
				x="-50%"
				y="-50%"
			/>
			<feTurbulence
				baseFrequency="0.45 0.01"
				height="100%"
				numOctaves="3"
				result="turbulence1"
				seed="2"
				stitchTiles="stitch"
				type="fractalNoise"
				width="100%"
				x="0%"
				y="0%">
				<MaxQualityRequired>
					<animate
						attributeName="seed"
						attributeType="XML"
						dur="10s"
						fill="freeze"
						keyTimes="0;0.5;1"
						repeatCount="indefinite"
						values="100;200;100"
					/>
				</MaxQualityRequired>
			</feTurbulence>
			<feDisplacementMap
				height="100%"
				in="morphology"
				in2="turbulence1"
				result="displacementMap"
				scale="120"
				width="100%"
				x="0%"
				xChannelSelector="R"
				y="0%"
				yChannelSelector="B"
			/>
			<feGaussianBlur
				edgeMode="none"
				height="100%"
				in="displacementMap"
				result="blur"
				stdDeviation="5"
				width="100%"
				x="0%"
				y="0%"
			/>
			<feComposite
				height="100%"
				in="blur"
				in2="SourceGraphic"
				operator="out"
				result="composite1"
				width="100%"
				x="0%"
				y="0%"
			/>
		</filter>,
	);

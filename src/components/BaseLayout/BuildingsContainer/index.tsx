import React from 'react';
import BuildingsFloorBackground from '../../../../assets/img/buildings/floor.jpg';
import FoundationMask from '../../../../assets/img/buildings/masks/foundation.json';
import PlaceholderMask_1 from '../../../../assets/img/buildings/masks/mask_p1.json';
import PlaceholderMask_2 from '../../../../assets/img/buildings/masks/mask_p2.json';
import PlaceholderMask_3 from '../../../../assets/img/buildings/masks/mask_p3.json';
import BuildingPlaceholder_1 from '../../../../assets/img/buildings/placeholders/p1.png';
import BuildingPlaceholder_2 from '../../../../assets/img/buildings/placeholders/p2.png';
import BuildingPlaceholder_3 from '../../../../assets/img/buildings/placeholders/p3.png';
import InterfaceTypes from '../../../../definitions/core/InterfaceTypes';
import BasicMaths from '../../../../definitions/maths/BasicMaths';
import IntegerNoise from '../../../../definitions/maths/IntegerNoise';
import Vectors from '../../../../definitions/maths/Vectors';
import { PLAYER_BASE_MAX_BUILDING_SLOTS } from '../../../../definitions/orbital/base/constants';
import { IPlayerBase, TBaseTypedBuilding } from '../../../../definitions/orbital/base/types';
import { BaseBuildingOrder, TBaseBuildingType } from '../../../../definitions/orbital/types';
import Players from '../../../../definitions/player/types';
import { numericHash } from '../../../utils/digest';
import { polygonToCss } from '../../../utils/polygonTools';
import { wellRounded } from '../../../utils/wellRounded';
import UIBuildingSprite from '../../Symbols/BuildingSprite';
import {
	BUILDING_SPRITE_STATE_ACTIVE,
	BUILDING_SPRITE_STATE_BASE,
	BUILDING_SPRITE_STATE_OUTPUT,
	BUILDING_SPRITE_STATE_RESOURCE,
} from '../../Symbols/BuildingSprite/types';
import { classnames, IUICommonProps } from '../../UI';
import styles from './index.scss';
import IVector = InterfaceTypes.IVector;

export type TBuildingsContainerProps = IUICommonProps &
	Pick<IPlayerBase, 'buildings' | 'buildingSlots'> & {
		player?: Players.IPlayer;
		backgroundImage?: string;
	};

export const BuildingPlaceholders = {
	[BuildingPlaceholder_1]: PlaceholderMask_1,
	[BuildingPlaceholder_2]: PlaceholderMask_2,
	[BuildingPlaceholder_3]: PlaceholderMask_3,
};

const baseSize = parseFloat(styles.baseSize);
const buildingSize = parseFloat(styles.buildingSize);
const scaleFactor = parseFloat(styles.scaleFactor);

const horizontalOffsetVector: IVector = {
	x: BasicMaths.roundToPrecision((parseFloat(styles.hOffsetX) / baseSize) * scaleFactor, 1e-3),
	y: BasicMaths.roundToPrecision((parseFloat(styles.hOffsetY) / baseSize) * scaleFactor, 1e-3),
};

const verticalOffsetVector: IVector = {
	x: -BasicMaths.roundToPrecision((parseFloat(styles.vOffsetX) / baseSize) * scaleFactor, 1e-3),
	y: BasicMaths.roundToPrecision((parseFloat(styles.vOffsetY) / baseSize) * scaleFactor, 1e-3),
};

const offsetVector = (buildingCoords: IVector) =>
	Vectors.add(
		Vectors.multiply(horizontalOffsetVector, buildingCoords.x, 1.15),
		Vectors.multiply(verticalOffsetVector, buildingCoords.y, 1.1),
	);

const UIBuildingPlaceholder: React.FC<{
	placeholderId: number;
}> = ({ placeholderId }) => (
	<div
		className={styles.placeholder}
		style={{
			backgroundImage: `url(${
				Object.keys(BuildingPlaceholders)[placeholderId] || Object.keys(BuildingPlaceholders)[0]
			})`,
		}}
	/>
);

const UIBuildingSlot: React.FC<
	IUICommonProps & {
		hoverable?: boolean;
		placeholderId?: number;
		pos: IVector;
		label?: string;
	}
> = ({ placeholderId, pos, className, hoverable = false, label, children }) => {
	const clipPath =
		placeholderId >= 0
			? polygonToCss(Object.values(BuildingPlaceholders)[placeholderId], {
					x: buildingSize,
					y: buildingSize,
			  })
			: polygonToCss(FoundationMask, {
					x: buildingSize,
					y: buildingSize,
			  });
	const vector = offsetVector(pos);
	return (
		<div
			className={classnames(styles.buildingSprite, className, !hoverable ? styles.dummy : '')}
			style={{
				left: `${wellRounded(vector.x * 100, 4)}vmax`,
				top: `${wellRounded(vector.y * 100, 4)}vmax`,
			}}
		>
			{!!hoverable && <div className={styles.cursor} />}
			{placeholderId >= 0 && <UIBuildingPlaceholder placeholderId={placeholderId} />}
			{children}
			{!!hoverable && (
				<div
					className={styles.svgMask}
					style={{
						clipPath,
					}}
				/>
			)}
			{!!label && <span className={styles.label}>{label}</span>}
		</div>
	);
};

export const UIBuildingsContainer: React.FC<TBuildingsContainerProps> = ({
	buildings,
	buildingSlots,
	className = '',
	player,
	backgroundImage = BuildingsFloorBackground,
}) => {
	if (!buildings || !buildingSlots) return null;
	const playerUniqueSeed = player
		? numericHash(player.id, 0, 255)
		: numericHash(
				JSON.stringify(Object.values(buildings).flatMap((bt) => bt.map((b) => b?.serialize()).filter(Boolean))),
				0,
				255,
		  );
	const noiseGen = IntegerNoise.i(playerUniqueSeed + 1e4 + 1e4);
	const slots: JSX.Element[] = [];
	for (let y = -4; y <= 1; y++)
		for (
			let x = -PLAYER_BASE_MAX_BUILDING_SLOTS + Math.abs(y);
			x <= PLAYER_BASE_MAX_BUILDING_SLOTS - Math.abs(y) - 2;
			x++
		) {
			if (
				y >= -1 &&
				y <= 1 &&
				x >= -Math.floor(PLAYER_BASE_MAX_BUILDING_SLOTS / 2) &&
				x <= Math.floor(PLAYER_BASE_MAX_BUILDING_SLOTS / 2)
			) {
				const buildingType = BaseBuildingOrder[y + 1];
				const buildingIndex = x + Math.floor(PLAYER_BASE_MAX_BUILDING_SLOTS / 2);
				const entry = buildings[buildingType][buildingIndex];
				switch (true) {
					case buildingIndex >= buildingSlots[buildingType]:
						slots.push(
							<UIBuildingSlot
								key={`${x}:${y}`}
								pos={{ x, y }}
								hoverable={true}
								placeholderId={
									Math.round(
										(playerUniqueSeed + buildingSlots[buildingType]) * 2 + buildingIndex + y + 1,
									) % Object.keys(BuildingPlaceholders).length
								}
								label={'Clear'}
							/>,
						);
						break;
					case !!buildings?.[buildingType]?.[buildingIndex]:
						slots.push(
							<UIBuildingSlot key={`${x}:${y}`} pos={{ x, y }} hoverable={false}>
								<UIBuildingSprite
									transparent={true}
									buildingType={buildingType}
									state={
										buildingType !== TBaseBuildingType.facility
											? entry.powered
												? BUILDING_SPRITE_STATE_ACTIVE
												: BUILDING_SPRITE_STATE_BASE
											: entry.powered
											? (entry as TBaseTypedBuilding<TBaseBuildingType.facility>).outputMode
												? BUILDING_SPRITE_STATE_OUTPUT
												: BUILDING_SPRITE_STATE_RESOURCE
											: BUILDING_SPRITE_STATE_BASE
									}
									buildingId={entry.static.id}
								/>
							</UIBuildingSlot>,
						);
						break;
					default:
						slots.push(
							<UIBuildingSlot
								key={`${x}:${y}`}
								pos={{ x, y }}
								hoverable={true}
								placeholderId={-1}
								label={'Build'}
							/>,
						);
				}
			} else {
				const rnd = noiseGen.noise();
				slots.push(
					<UIBuildingSlot
						key={`${x}:${y}`}
						pos={{ x, y }}
						placeholderId={rnd > 0 ? Math.floor(rnd * Object.values(BuildingPlaceholders).length) : -1}
					/>,
				);
			}
		}
	return (
		<div
			className={classnames(styles.buildingsContainer, className)}
			style={{
				backgroundImage: `url(${backgroundImage})`,
			}}
		>
			<div className={styles.buildingAnchor}>{slots}</div>
			<div className={styles.borderShadow} />
		</div>
	);
};

import memoize from 'memoize-one';
import * as React from 'react';
import Distributions from '../../../../../definitions/maths/Distributions';
import { IUICommonProps } from '../../index';
import UIDistributionIcon from '../../SVG/DistributionIcon';
import UICircleIconWithValue from '../../Templates/CircleIconWithValue';
import Style from './index.scss';

export const distributionDescription: Proxy<typeof Distributions.DistributionOptions, string> = {
	Uniform: 'Uniform Distribution',
	Bell: 'Bell Distribution',
	Gaussian: 'Gaussian Distribution',
	Parabolic: 'Parabolic Distribution',
	Minimal: 'Low Skewed Distribution',
	Maximal: 'High Skewed Distribution',
	MostlyMaximal: 'Maximized Distribution',
	MostlyMinimal: 'Minimized Distribution',
};

export const distributionText: Proxy<typeof Distributions.DistributionOptions, string> = {
	Uniform: 'All values within range occur equally often',
	Bell: 'Values around the middle of the range occure twice as often as values around its ends',
	Gaussian: 'Values around the middle of the range are 5 times more probable than values around its ends',
	Parabolic: 'Values on both ends of the range are twice as often to occur than the values around its middle',
	Maximal: 'The greater the value is, the higher is the probability of its occurrence',
	Minimal: 'The lower the value is, the higher is the probability of its occurrence',
	MostlyMaximal: 'Values at the higher end occur much more often than lower values',
	MostlyMinimal: 'Values at the lower end occur much more often than higher values',
};

export type UIDistributionDescriptorProps = IUICommonProps & {
	colorClass?: string;
	fillColor?: string;
	distribution: Distributions.TDistribution;
};

const getIcon = memoize((d: Distributions.TDistribution) => (c: string) => (
	<UIDistributionIcon distribution={d} fillColor={c} stroke={true} />
));

export default class UIDistributionDescriptor extends React.PureComponent<UIDistributionDescriptorProps, {}> {
	public static getClass(): string {
		return Style.distribution;
	}

	public static getCaption(t: Distributions.TDistribution): string {
		return distributionDescription[t];
	}

	public static getDescription(t: Distributions.TDistribution): string {
		return distributionText[t];
	}

	public render() {
		const props = {
			tooltip: UIDistributionDescriptor.getCaption(this.props.distribution),
			className: UIDistributionDescriptor.getClass() + ' ' + (this.props.className || ''),
			circleClassName: this.props.colorClass || '',
			iconContent: null as JSX.Element,
		};
		if (this.props.distribution)
			props.iconContent = getIcon(this.props.distribution)(this.props.fillColor || '#FFF');
		return <UICircleIconWithValue {...props}>{this.props.children}</UICircleIconWithValue>;
	}
}

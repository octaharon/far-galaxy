import * as React from 'react';
import { getFaction } from '../../../../../definitions/world/factionIndex';
import PoliticalFactions from '../../../../../definitions/world/factions';
import { getFactionIconId, getFactionIconStyle } from '../../../Symbols/factionIcons';
import UIContainer, { classnames, getFactionColor, getTemplateColor, IUICommonProps } from '../../index';
import SVGHexIcon from '../../SVG/HexIcon';
import SVGIconContainer from '../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../SVG/SmallTexturedIcon';
import UITooltip from '../../Templates/Tooltip';
import UIFactionBanner from '../FactionBanner';
import styles from './index.scss';

type UIFactionCaptionProps = IUICommonProps & {
	faction: PoliticalFactions.TFactionID;
};

const getStyle = (f: PoliticalFactions.TFactionID): React.CSSProperties => ({
	textShadow: `1px 1px 1px ${getFactionColor(f, 'secondary')}, -1px -1px 1px ${getFactionColor(
		f,
		'secondary',
	)}, 1px -1px 1px ${getFactionColor(f, 'primary')}, -1px 1px 1px ${getFactionColor(f, 'primary')}`,
});

export default class UIFactionCaption extends React.PureComponent<UIFactionCaptionProps, {}> {
	public static getTooltip(faction?: PoliticalFactions.TFactionID) {
		let f = getFaction(faction);
		if (!f)
			f = {
				id: null,
				baseColor: getTemplateColor('text_main'),
				secondaryColor: getTemplateColor('text_highlight'),
				captionShort: 'Factions',
				captionLong: 'Factions',
				description: 'Factions are political powers contesting for the Far Galaxy',
			};
		return (
			<React.Fragment>
				<UIFactionBanner faction={faction} />
				<b
					className={UIFactionCaption.getCaptionClass()}
					style={{
						...UIFactionCaption.getCaptionStyle(faction),
					}}>
					{f.captionLong}
				</b>
				{UIFactionCaption.getIcon(faction, UIContainer.tooltipIconClass)}
				<p>{f.description.split('\n')[0]}</p>
			</React.Fragment>
		);
	}

	public static getCaptionStyle(faction?: PoliticalFactions.TFactionID) {
		return getStyle(faction);
	}

	public static getClass() {
		return styles.factionCaptionDescriptor;
	}

	public static getCaptionClass() {
		return styles.factionCaption;
	}

	public static getIcon(faction: PoliticalFactions.TFactionID = null, iconClass = UIContainer.iconClass) {
		const reverseGradient = [
			PoliticalFactions.TFactionID.reticulans,
			PoliticalFactions.TFactionID.aquarians,
			PoliticalFactions.TFactionID.pirates,
			PoliticalFactions.TFactionID.anders,
		].includes(faction);
		return (
			<SVGHexIcon
				className={classnames(iconClass, styles.factionIcon)}
				highlight={true}
				colorTop={getFactionColor(faction, reverseGradient ? 'primary' : 'secondary')}
				colorBottom={getFactionColor(faction, reverseGradient ? 'secondary' : 'primary')}>
				<SVGSmallTexturedIcon style={getFactionIconStyle(faction)} asSvg={true} tooltip={false}>
					<use {...SVGIconContainer.getXLinkProps(getFactionIconId(faction))} />
				</SVGSmallTexturedIcon>
			</SVGHexIcon>
		);
	}

	public static getCaption(faction: PoliticalFactions.TFactionID = null) {
		return getFaction(faction)?.captionShort || 'Factions';
	}

	public render() {
		return (
			<span
				className={classnames(
					UIFactionCaption.getClass(),
					this.props.tooltip ? UIContainer.hasTooltipClass : null,
					this.props.className,
				)}>
				{UIFactionCaption.getIcon(this.props.faction, styles.factionIcon)}
				<span
					className={UIFactionCaption.getCaptionClass()}
					style={UIFactionCaption.getCaptionStyle(this.props.faction)}>
					{UIFactionCaption.getCaption(this.props.faction)}
				</span>
				{this.props.tooltip && <UITooltip>{UIFactionCaption.getTooltip(this.props.faction)}</UITooltip>}
			</span>
		);
	}
}

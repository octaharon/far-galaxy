import React from 'react';
import PoliticalFactions from '../../../../../definitions/world/factions';
import { classnames, IUICommonProps } from '../../index';
import styles from './index.scss';

type UIFactionBannerProps = IUICommonProps & {
	faction: PoliticalFactions.TFactionID;
};

export const getFactionImage = (t: PoliticalFactions.TFactionID) => {
	switch (t) {
		case PoliticalFactions.TFactionID.anders:
			return require('../../../../../assets/img/factions/anders.jpg');
		case PoliticalFactions.TFactionID.pirates:
			return require('../../../../../assets/img/factions/pirates.jpg');
		case PoliticalFactions.TFactionID.aquarians:
			return require('../../../../../assets/img/factions/aquarius.jpg');
		case PoliticalFactions.TFactionID.reticulans:
			return require('../../../../../assets/img/factions/reticulans.jpg');
		case PoliticalFactions.TFactionID.venaticians:
			return require('../../../../../assets/img/factions/venaticians.jpg');
		case PoliticalFactions.TFactionID.stellarians:
			return require('../../../../../assets/img/factions/stellarians.jpg');
		case PoliticalFactions.TFactionID.draconians:
			return require('../../../../../assets/img/factions/draconians.jpg');
		default:
			return require('../../../../../assets/img/bg/menu.avif');
	}
};

export const UIFactionBanner: React.FC<UIFactionBannerProps> = ({ className, faction, children }) => {
	if (!faction) return null;
	return (
		<div
			className={classnames(styles.factionBanner, className)}
			style={{ backgroundImage: `url(${getFactionImage(faction)})` }}
		>
			{children}
		</div>
	);
};

export default UIFactionBanner;

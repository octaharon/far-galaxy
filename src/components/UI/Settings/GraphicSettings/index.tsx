import React from 'react';
import { connect } from 'react-redux';
import { classnames } from '../..';
import { GraphicSettingsConfigDispatchers, GraphicSettingsConfigSelector } from '../../../../contexts/GraphicSettings';
import {
	GraphicSettingsQualityLevels,
	TEXTURE_VIDEO_RESOLUTIONS,
	TGraphicQualityLevel,
	TGraphicSettingsContext,
	TGraphicTextureSize,
} from '../../../../contexts/GraphicSettings/slice';
import Style from './index.scss';

const stateToggleCaptions = {
	On: true,
	Off: false,
};

const qualityCaptions: Record<TGraphicQualityLevel, string> = {
	Min: 'Minimal',
	Med: 'Normal',
	Max: 'Extra',
};

const textureCaptions: Record<TGraphicTextureSize, string> = {
	SD: '480p',
	HD: '720p',
	UHD: '1080p',
	QHD: '1440p',
};

class UIGraphicSettings extends React.PureComponent<TGraphicSettingsContext> {
	render() {
		const {
			qualityLevel,
			textureSize,
			setQuality,
			setTextureSize,
			showTooltips = true,
			toggleTooltips,
			showDetails = false,
			toggleDetails,
		} = this.props;
		return (
			<div className={Style.graphicSettingsForm}>
				<section className={Style.settingSection}>
					<p className={Style.settingsTitle}>Graphics and VFX</p>
					<p className={Style.settingsCaption}>
						{showTooltips ? 'Lower this if you are facing performance issues' : <>&nbsp;</>}
					</p>
					<ul className={Style.settingList}>
						{Object.keys(GraphicSettingsQualityLevels).map((e, i) => {
							return (
								<li
									key={i}
									onClick={qualityLevel !== e ? () => setQuality(e) : null}
									className={classnames(
										Style.settingListItem,
										qualityLevel === e ? Style.selected : undefined,
									)}
								>
									{qualityCaptions[e]}
								</li>
							);
						})}
					</ul>
				</section>
				<section className={Style.settingSection}>
					<p className={Style.settingsTitle}>Textures and video resolution</p>
					<p className={Style.settingsCaption}>
						{showTooltips ? 'Reduce network traffic if you are playing on mobile' : <>&nbsp;</>}
					</p>
					<ul className={Style.settingList}>
						{Object.keys(TEXTURE_VIDEO_RESOLUTIONS).map((e, i) => {
							return (
								<li
									key={i}
									onClick={textureSize !== e ? () => setTextureSize(e) : null}
									className={classnames(
										Style.settingListItem,
										textureSize === e ? Style.selected : undefined,
									)}
								>
									{textureCaptions[e]}
								</li>
							);
						})}
					</ul>
				</section>
				<section className={Style.settingSection}>
					<p className={Style.settingsTitle}>Hints and tooltips</p>
					<p className={Style.settingsCaption}>
						{showTooltips ? (
							'Hover almost anything to learn about its mechanics, if you are new to the game'
						) : (
							<>&nbsp;</>
						)}
					</p>
					<ul className={Style.settingList}>
						{Object.keys(stateToggleCaptions).map((e, i) => {
							return (
								<li
									key={i}
									onClick={showTooltips !== stateToggleCaptions[e] ? () => toggleTooltips() : null}
									className={classnames(
										Style.settingListItem,
										showTooltips === stateToggleCaptions[e] ? Style.selected : undefined,
									)}
								>
									{e}
								</li>
							);
						})}
					</ul>
				</section>
				<section className={Style.settingSection}>
					<p className={Style.settingsTitle}>Explicit descriptions</p>
					<p className={Style.settingsCaption}>
						{showTooltips ? (
							'Get detailed numeric information about entities, if you want to go deeper'
						) : (
							<>&nbsp;</>
						)}
					</p>
					<ul className={Style.settingList}>
						{Object.keys(stateToggleCaptions).map((e, i) => {
							return (
								<li
									key={i}
									onClick={showDetails !== stateToggleCaptions[e] ? () => toggleDetails() : null}
									className={classnames(
										Style.settingListItem,
										showDetails === stateToggleCaptions[e] ? Style.selected : undefined,
									)}
								>
									{e}
								</li>
							);
						})}
					</ul>
				</section>
			</div>
		);
	}
}

export default connect(GraphicSettingsConfigSelector, (dispatch) => GraphicSettingsConfigDispatchers(dispatch))(
	UIGraphicSettings,
);

import React from 'react';
import { withWindowStateControls } from '../../../../contexts/WindowState';
import { TWindowState, TWindowStateSetters } from '../../../../contexts/WindowState/slice';
import { classnames, IUICommonProps } from '../../index';
import UIGraphicSettings from '../GraphicSettings';
import styles from './index.scss';

type UISettingsContainerProps = IUICommonProps & TWindowStateSetters & TWindowState & {};
const hideSettings = (cb: TWindowStateSetters['setSettingsVisibility']) => cb(false);

class UISettingsContainer extends React.PureComponent<UISettingsContainerProps, never> {
	cb: TWindowStateSetters['setSettingsVisibility'] = null;
	eventHandler = (event: KeyboardEvent) =>
		this.props.displaySettings && event.key === 'Escape' ? hideSettings(this.cb) : null;

	componentDidMount() {
		this.cb = this.props.setSettingsVisibility;
		document.addEventListener('keydown', this.eventHandler);
	}

	componentWillUnmount() {
		document.removeEventListener('keydown', this.eventHandler);
	}

	render() {
		return this.props.displaySettings ? (
			<div
				className={classnames(styles.settingsWrapper, this.props.className)}
				onClick={(event) => {
					if (
						event?.target instanceof HTMLDivElement &&
						event.target.className?.includes(styles.settingsWrapper)
					)
						hideSettings(this.props.setSettingsVisibility);
				}}
			>
				<div className={styles.settingsContainer}>
					<UIGraphicSettings />
					<div className={styles.saveButton} onClick={() => this.props.setSettingsVisibility(false)}>
						Save & Close
					</div>
				</div>
			</div>
		) : null;
	}
}

export default withWindowStateControls(UISettingsContainer);

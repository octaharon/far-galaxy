import React from 'react';
import ReactDOM from 'react-dom';
import { preloadGameLibrary } from '../../../definitions/load';
import { PersistentConfigurationProvider } from '../../stores';
import UIIndex, { classnames } from './index';

export const hideLoader = ({ withColumns = false, withBg = false, withScroll = false, isShowcase = false } = {}) => {
	document.getElementById('pageContainer').className = classnames(
		isShowcase ? UIIndex.GeneralStyles.container : '',
		withBg ? UIIndex.GeneralStyles['with-background'] : '',
		withColumns ? UIIndex.GeneralStyles['with-columns'] : '',
	);
	document.querySelectorAll('.__showAfterLoad').forEach((el: HTMLElement) => (el.style.visibility = 'visible'));
	if (withScroll) document.body.style.overflow = 'scroll';
	setLoaderTitle('Loading done');
	setLoaderClassname('fadeOut');
	return true;
};

export type TRootComponentProps = {
	hideLoader: (props?: {
		withColumns?: boolean;
		withBg?: boolean;
		withScroll?: boolean;
		isShowcase?: boolean;
	}) => any;
};

export const onLoad = async (App: React.ComponentType<TRootComponentProps>, skipLibrary = false) => {
	window.addEventListener('load', async (ev) => {
		document.querySelector('#preloader span').innerHTML = 'Loading game data...';
		if (!skipLibrary) await preloadGameLibrary();
		return requestAnimationFrame(() => {
			ReactDOM.render(
				<PersistentConfigurationProvider>
					<App hideLoader={hideLoader} />
				</PersistentConfigurationProvider>,
				document.getElementById('pageContainer'),
			);
		});
	});
};

export const setLoaderTitle = (p: string) => {
	if (document.querySelector('#preloader span')) document.querySelector('#preloader span').innerHTML = p;
};

export const setLoaderClassname = (p: string) => {
	if (document.querySelector('#preloader span')) document.querySelector('#preloader').className = p;
};

export default onLoad;

import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import { basicTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import { getAOETypeIconID, getAttackIcon } from '../AttackIcon';
import Style from './index.scss';

type UIAOERadiusDescriptorProps = IUICommonProps & {
	short?: boolean;
	value?: number;
	modifier?: IModifier;
	caption?: string | JSX.Element;
	isAura?: boolean;
};

export default class UIAoeRadiusDescriptor extends React.PureComponent<UIAOERadiusDescriptorProps, {}> {
	public static getIcon(isAura = false, className: string = UIContainer.iconClass) {
		return getAttackIcon(
			getAOETypeIconID(UnitAttack.TAoEDamageSplashTypes.scatter),
			className,
			null,
			isAura ? TSVGSmallTexturedIconStyles.blue : TSVGSmallTexturedIconStyles.light,
		);
	}

	public static getCaption(isAura = false) {
		return isAura ? 'Effect Radius' : 'AoE Radius';
	}

	public static getTooltip(isAura = false) {
		return basicTooltipTemplate(
			UIAoeRadiusDescriptor.getIcon(isAura, UITooltip.getIconClass()),
			UIAoeRadiusDescriptor.getCaption(isAura),
			`is a characteristic size of an ${isAura ? 'Aura or Ability' : 'Area Attack'} impact zone,${
				isAura
					? ' describing the radius of a circle in which the related Effects are applied.'
					: ' having specific meanings for different types of Area Attacks.'
			}\n\n The value is measured in Distance Units, same as Attack Range and Speed`,
		);
	}

	public static getEntityCaption(isAura = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }} className={Style.caption}>
				{UIAoeRadiusDescriptor.getIcon(isAura)}
				<span className={isAura ? Style.auraCaption : Style.radiusCaption}>
					{UIAoeRadiusDescriptor.getCaption(isAura)}
				</span>
			</em>
		);
	}

	public static getValue({ value, modifier }: { value?: number; modifier?: IModifier }) {
		const isPositive = 100 < BasicMaths.applyModifier(100, modifier);
		const modOnly = !Number.isFinite(value);
		if (modOnly && !modifier) return '';
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, true).trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(value) && <span className={Style.value}>{value}</span>}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	public render() {
		const { modifier, value, caption, isAura } = this.props;
		const valueText = UIAoeRadiusDescriptor.getValue({ value, modifier });
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						Style.radiusDescriptor,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIAoeRadiusDescriptor.getIcon(isAura)}
					<span className={isAura ? Style.auraRadiusValue : Style.weaponRadiusValue}>{valueText}</span>
					{!!this.props.tooltip && <UITooltip>{UIAoeRadiusDescriptor.getTooltip(isAura)}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(this.props.className || '', Style.radiusDescriptor)}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIAoeRadiusDescriptor.getIcon(isAura)}
					<span className={isAura ? Style.auraCaption : Style.radiusCaption}>
						{caption ?? UIAoeRadiusDescriptor.getCaption(isAura)}
					</span>
					{!!this.props.tooltip && <UITooltip>{UIAoeRadiusDescriptor.getTooltip(isAura)}</UITooltip>}
				</dt>
				<dd
					className={classnames(
						UIContainer.descriptorClass,
						isAura ? Style.auraRadiusValue : Style.weaponRadiusValue,
					)}>
					{valueText}
				</dd>
			</dl>
		);
	}
}

import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import { MAX_MAP_RADIUS } from '../../../../../../definitions/tactical/map/constants';
import { ORBITAL_ATTACK_RANGE } from '../../../../../../definitions/tactical/units/actions/constants';
import UnitActions from '../../../../../../definitions/tactical/units/actions/types';
import { UNIT_MAP_SIZE } from '../../../../../../definitions/tactical/units/types';
import { INFINITY_SYMBOL, wellRounded } from '../../../../../utils/wellRounded';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import UIActionDescriptor from '../../Chassis/ActionDescriptor';
import UIWeaponDescriptor from '../../Weapon/WeaponDescriptor';
import UIAoeRadiusDescriptor from '../AOERadiusDescriptor';
import styles from './index.scss';

const UIAttackRangeDescriptorClass = styles.rangeDescriptor;

export type UIAttackRangeDescriptorProps = IUICommonProps & {
	range?: number;
	modifier?: IModifier;
	isWeapon?: boolean;
	isRadius?: boolean;
	precision?: number;
	short?: boolean;
};

export const SVGRangeIconID = 'weapon-range-icon';

SVGIconContainer.registerSymbol(
	SVGRangeIconID,
	<symbol viewBox="0 0 561 561">
		{/* <g
         style={{
         transformOrigin: '50% 50%',
         transform:       'scale(1.0,1.2)',
         }}>
         <path
         d="M331.5,127.5L234.6,255l73.95,96.9l-40.8,30.6C224.4,326.4,153,229.5,153,229.5L0,433.5h561L331.5,127.5z"/>
         </g>*/}
		<path
			d="M437.387,74.543C349.146-13.714,208.1-24.545,107.471,47.097c-30.538-17.119-67.27,3.171-67.27,38.055
			c0,8.235,2.35,16.065,6.467,22.96C-24.405,208.71-13.41,349.297,74.612,437.318C124.072,486.778,190.522,512,256.235,512
			c51.245,0,103.558-15.562,147.635-46.717c6.882,4.102,14.694,6.445,22.907,6.445c24.88,0,45.02-20.123,45.02-45.02
			c0-8.238-2.345-16.065-6.465-22.962C536.405,303.149,525.408,162.564,437.387,74.543z M240.993,481.458
			c-52.767-3.552-105.34-25.53-145.161-65.359c-39.821-39.822-61.795-92.395-65.348-145.161h60.499
			c2.033,22.772,9.103,46.09,20.729,66.763c-11.918,17.414-10.361,41.477,5.34,57.178c15.771,15.769,39.805,17.159,57.048,5.446
			c20.698,11.658,44.096,18.467,66.893,20.506V481.458z M240.993,390.722c-16.616-1.827-33.87-6.866-49.124-14.958
			c5.052-17.154-0.084-33.492-11.141-44.548c-11.557-11.566-28.852-14.618-44.174-10.449c-8.313-15.456-13.479-32.956-15.335-49.831
			h92.524c4.534,12.714,14.535,22.715,27.249,27.249V390.722z M240.993,213.675c-12.714,4.534-22.715,14.535-27.249,27.249h-92.735
			c6.807-60.745,53.991-112.21,119.984-119.546V213.675z M240.993,91.366c-82.878,7.529-143.037,72.262-150.126,149.559H30.633
			c2.588-39.482,15.552-79.424,39.174-113.604c4.889,1.783,10.055,2.852,15.415,2.852c24.838,0,45.02-20.112,45.02-45.02
			c0-5.36-1.07-10.528-2.852-15.417c34.181-23.627,74.123-36.593,113.603-39.181V91.366z M271.008,121.379
			c24.662,2.742,49.423,12.252,69.814,28.512l-65.533,65.533c-1.391-0.665-2.815-1.226-4.281-1.748V121.379z M271.008,91.364v-60.96
			c48.031,3.234,95.968,21.902,134.057,55.241l-43.021,43.021C335.67,106.734,303.371,94.304,271.008,91.364z M271.007,298.187
			c12.714-4.534,22.715-14.635,27.249-27.349h92.297c-7.357,66.172-59.003,113.302-119.546,120.091V298.187z M441.905,384.949
			c-30.618-10.413-60.148,11.094-60.148,41.76c0,5.36,1.069,10.528,2.854,15.417c-34.181,23.628-74.125,36.593-113.605,39.181
			v-60.889c80.939-7.474,142.252-69.281,149.548-149.58h60.813C478.769,310.475,465.707,350.668,441.905,384.949z"
		/>
	</symbol>,
);

export default class UIAttackRangeDescriptor extends React.PureComponent<UIAttackRangeDescriptorProps, {}> {
	public static getClass(): string {
		return UIAttackRangeDescriptorClass;
	}

	public static getCaption(isWeapon = false, isRadius = false) {
		return isRadius
			? isWeapon
				? UIAoeRadiusDescriptor.getCaption()
				: 'Ability Radius'
			: isWeapon
			? 'Attack Range'
			: 'Ability Range';
	}

	public static getTooltip(isWeapon = false, isRadius = false) {
		if (isWeapon && isRadius) return UIAoeRadiusDescriptor.getTooltip();
		return (
			<span>
				{UIAttackRangeDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UIAttackRangeDescriptor.getCaption(isWeapon)}</b> is the maximum distance at which a target can be
				engaged with {isWeapon ? UIWeaponDescriptor.getEntityCaption() : <em>Ability</em>}.&nbsp;
				{isWeapon ? (
					<>
						Weapons with range of <em className={styles.orbitalRange}>{ORBITAL_ATTACK_RANGE}</em> and more
						can perform {UIActionDescriptor.getEntityCaption(UnitActions.unitActionTypes.orbital_attack)} on{' '}
						<em>Player</em>'s base
					</>
				) : isRadius ? (
					<>This Ability affects all eligible targets inside a given range of the Unit.</>
				) : (
					<>
						Ability Range of {INFINITY_SYMBOL}, which most Player Abilities have, means they can be used
						anywhere on the Map.
					</>
				)}
				<br />
				<br />
				<em>1 Distance Unit</em> equals <em>10x unit size</em> or <em>5%</em> of the smallest map radius.
			</span>
		);
	}

	public static getEntityCaption(isWeapon = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }} className={styles.rangeCaption}>
				{UIAttackRangeDescriptor.getIcon()}
				{UIAttackRangeDescriptor.getCaption(isWeapon)}
			</em>
		);
	}

	public static getIcon(className?: string) {
		return (
			<SVGSmallTexturedIcon
				style={TSVGSmallTexturedIconStyles.gray}
				className={[UIContainer.iconClass, className || ''].join(' ')}>
				<use {...SVGIconContainer.getXLinkProps(SVGRangeIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getValueCaption({
		value,
		modifier,
		isWeapon = false,
		precision = 3,
	}: {
		value: number;
		modifier?: IModifier;
		isWeapon: boolean;
		precision?: number;
	}) {
		const isPositive = BasicMaths.applyModifier(100, modifier) >= 100;
		const mod = (
			<span className={classnames(styles.modifier, isPositive ? styles.positive : styles.negative)}>
				{BasicMaths.describeModifier(modifier, true)}
			</span>
		);
		const v = wellRounded(value, precision, MAX_MAP_RADIUS - UNIT_MAP_SIZE);
		return (
			<>
				{!Number.isNaN(value) && (
					<span
						className={classnames(
							styles.value,
							!Number.isNaN(value) && value >= ORBITAL_ATTACK_RANGE && !!isWeapon
								? styles.orbitalRange
								: '',
						)}>
						{v}
					</span>
				)}
				{!!modifier && (Number.isFinite(value) ? <>({mod})</> : mod)}
			</>
		);
	}

	public render() {
		const classNames = [UIAttackRangeDescriptor.getClass(), this.props.className || ''];
		const itemClassName = classnames(
			!!this.props.tooltip && UIContainer.hasTooltipClass,
			UIContainer.captionClass,
			UIContainer.descriptorClass,
			styles.rangeCaption,
		);
		const value = UIAttackRangeDescriptor.getValueCaption({
			value: this.props.range,
			precision: this.props.precision,
			isWeapon: !!this.props.isWeapon,
			modifier: this.props.modifier,
		});
		return this.props.short ? (
			<span className={classnames(...classNames, this.props.tooltip ? UIContainer.hasTooltipClass : '')}>
				{UIAttackRangeDescriptor.getIcon(UIContainer.iconClass)}
				<span className={styles.value}>{value}</span>
				{!!this.props.tooltip && (
					<UITooltip>
						{UIAttackRangeDescriptor.getTooltip(this.props.isWeapon, this.props.isRadius)}
					</UITooltip>
				)}
			</span>
		) : (
			<dl className={classnames(...classNames)}>
				<dt className={itemClassName}>
					{UIAttackRangeDescriptor.getIcon(UIContainer.iconClass)}
					{UIAttackRangeDescriptor.getCaption(!!this.props.isWeapon, !!this.props.isRadius)}
					{!!this.props.tooltip && (
						<UITooltip>
							{UIAttackRangeDescriptor.getTooltip(this.props.isWeapon, this.props.isRadius)}
						</UITooltip>
					)}
				</dt>
				<dd>{value}</dd>
			</dl>
		);
	}
}

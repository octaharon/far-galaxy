import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import DamageManager from '../../../../../../definitions/tactical/damage/DamageManager';
import { IWeapon } from '../../../../../../definitions/technologies/types/weapons';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGSegmentedBar, { ISVGSegmentedBarSegment } from '../../../SVG/SegmentedBar';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import UIDamageTypeDescriptor from '../../Damage/DamageTypeDescriptor';
import UIDamageValueDescriptor from '../../Damage/DamageValue';
import styles from './index.scss';

const UIDamagePerTurnDescriptorClass = styles.damagePerTurnDescriptor;

export interface UIDamagePerTurnDescriptorProps extends IUICommonProps {
	weapon: IWeapon;
	phases?: number;
}

export default class UIDamagePerTurnBar extends React.PureComponent<UIDamagePerTurnDescriptorProps, {}> {
	public static getClass(): string {
		return UIDamagePerTurnDescriptorClass;
	}

	public render() {
		const phases = this.props.phases > 0 ? this.props.phases : 1;
		const dmg = DamageManager.applyDamageModifierToDamageUnit(this.props.weapon.getDamagePerTurn(), {
			factor: 1 / Math.min(phases, this.props.weapon.getAttacksQuantityMinMax()[0]),
		});
		const totalDmg = DamageManager.getTotalDamageValue(dmg);
		const keys = Object.keys(dmg).filter((k: Damage.damageType) => dmg[k] > 0);
		keys.sort((k1: Damage.damageType, k2: Damage.damageType) => -dmg[k1] + dmg[k2]);
		const maxDamageType = Object.keys(dmg).reduce((a: Damage.damageType, b: Damage.damageType) =>
			dmg[a] > dmg[b] ? a : b,
		);
		return (
			<span
				className={classnames(
					UIDamagePerTurnBar.getClass(),
					UIDamageTypeDescriptor.getClass(maxDamageType as Damage.damageType),
					this.props.tooltip ? UIContainer.hasTooltipClass : null,
					this.props.className,
				)}
			>
				<SVGSegmentedBar
					className={styles.bar}
					segments={keys.map((k: Damage.damageType) => {
						return {
							value: dmg[k],
							gradientId: UIDamageTypeDescriptor.getSVGGradientId(k),
						};
					})}
				/>
				{BasicMaths.roundToPrecision(totalDmg, 0.2, 1)}
				{this.props.tooltip && (
					<UITooltip>
						<p>
							<b>Mean Damage</b>
							<Tokenizer
								description={` dealt per typical ${
									phases > 1 ? 'Action Phase' : 'Turn'
								}, given every Attack connects`}
							/>
						</p>
						{keys.map((k: Damage.damageType) => (
							<p key={k}>
								<UIDamageValueDescriptor damageType={k}>
									{BasicMaths.roundToPrecision(dmg[k], 0.2, 1)}&nbsp;
									{UIDamageTypeDescriptor.getAbbr(k)} Damage
								</UIDamageValueDescriptor>
								<em> ({((dmg[k] / totalDmg) * 100).toFixed(0)}%)</em>
							</p>
						))}
					</UITooltip>
				)}
			</span>
		);
	}
}

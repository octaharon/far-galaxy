import * as React from 'react';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIAttacksQuantityDescriptor from '../AttacksQuantityDescriptor';
import styles from './index.scss';

const UIAPTDescriptorClass = styles.aptDescriptor;

export type UIAPTDescriptorProps = IUICommonProps & {
	attackRate: number;
	short?: boolean;
	phases?: number;
};

export default class UIAPTDescriptor extends React.PureComponent<UIAPTDescriptorProps, {}> {
	public static getClass(): string {
		return UIAPTDescriptorClass;
	}

	public static getCaption() {
		return 'Attacks Per Turn';
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIAttacksQuantityDescriptor.getIcon(UIContainer.iconClass)}
				<span className={styles.aptCaption}>{UIAPTDescriptor.getCaption()}</span>
			</em>
		);
	}

	public render() {
		const classNames = [UIAPTDescriptor.getClass(), this.props.className || ''];
		const itemClassName = classnames(
			!!this.props.tooltip && UIContainer.hasTooltipClass,
			UIContainer.captionClass,
			UIContainer.descriptorClass,
			styles.aptCaption,
		);
		const value = <UIAttacksQuantityDescriptor attackRate={this.props.attackRate} phases={this.props.phases} />;
		return this.props.short ? (
			<span className={classnames(...classNames, this.props.tooltip ? UIContainer.hasTooltipClass : '')}>
				{value}
				{!!this.props.tooltip && (
					<UITooltip>{UIAttacksQuantityDescriptor.getTooltip(this.props.phases)}</UITooltip>
				)}
			</span>
		) : (
			<dl className={classnames(...classNames)}>
				<dt className={itemClassName}>
					{UIAttacksQuantityDescriptor.getIcon(UIContainer.iconClass)}
					{UIAttacksQuantityDescriptor.getCaption(this.props.phases)}
					{!!this.props.tooltip && (
						<UITooltip>{UIAttacksQuantityDescriptor.getTooltip(this.props.phases)}</UITooltip>
					)}
				</dt>
				<dd>{value}</dd>
			</dl>
		);
	}
}

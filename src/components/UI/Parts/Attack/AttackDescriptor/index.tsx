import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import AttackManager from '../../../../../../definitions/tactical/damage/AttackManager';
import { describeAttackTypes, getTypeCaption } from '../../../../../../definitions/tactical/damage/strings';
import DamageOverTimeStatusEffect from '../../../../../../definitions/tactical/statuses/Effects/all/AppliedDamage/DamageOverTime.01002';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UIModifierDescriptor from '../../../Templates/ModifierDescriptor';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import UIDurationDescriptor from '../../Ability/DurationDescriptor';
import UIDamageModifierInlineDescriptor from '../../Damage/DamageModifierInlineDescriptor';
import UIDamageTypeIcons from '../../Damage/DamageTypeIcons';
import UIAoeRadiusDescriptor from '../AOERadiusDescriptor';
import { UIDamageCollectionDescriptor } from '../../Damage/DamageCollectionDescriptor';
import { getAttackTypeIcon, getTooltip } from '../AttackIcon';
import UIAttackDeliveryDescriptor from '../DeliveryDescriptor';
import styles from './index.scss';

const attackDescriptorClass = styles.attackDescriptor;

export type UIAttackDescriptorProps = IUICommonProps & {
	attack: UnitAttack.TAttackDefinition;
	details?: boolean;
	withCaption?: string;
};

const getAoEAttackExtraMeta = (attack: UnitAttack.IAreaOfEffectAttackDecorator, tooltip?: boolean) => {
	switch (attack.aoeType) {
		case UnitAttack.TAoEDamageSplashTypes.chain:
			return (
				<React.Fragment>
					<dt className={classnames(styles.subtype, tooltip ? UIContainer.hasTooltipClass : null)}>
						Additional Targets
						{tooltip && (
							<UITooltip>
								<Tokenizer
									description={`With this Area Attack its Damage is replicated to the nearest target within AoE Radius ${attack.radius} from current Unit, until at most ${attack.magnitude} targets are hit`}
								/>
							</UITooltip>
						)}
					</dt>
					<dd className={classnames(styles.value)}>{attack.magnitude}</dd>
				</React.Fragment>
			);
		case UnitAttack.TAoEDamageSplashTypes.scatter:
			return (
				<React.Fragment>
					<dt className={classnames(styles.subtype, tooltip ? UIContainer.hasTooltipClass : null)}>
						Hits per Unit
						{tooltip && (
							<UITooltip>
								<Tokenizer
									description={`This Area Attack targets all appropriate Units within AoE Radius ${attack.radius} invoking a total of ${attack.magnitude} Attacks per every Unit, choosing targets randomly`}
								/>
							</UITooltip>
						)}
					</dt>
					<dd className={classnames(styles.value)}>{attack.magnitude}</dd>
				</React.Fragment>
			);
		default:
			if (attack.magnitude === 1) return null;
			return (
				<React.Fragment>
					<dt
						className={classnames(
							UIContainer.descriptorClass,
							UIContainer.captionClass,
							tooltip ? UIContainer.hasTooltipClass : null,
						)}
					>
						<UIDamageModifierInlineDescriptor modifier={null} caption={'Damage Falloff'} />
						{tooltip && (
							<UITooltip>
								<Tokenizer
									description={`The Damage done by this Area Attack is reduced the further away from the impact point a target is, reaching a maximum penalty of ${BasicMaths.describeModifier(
										{ factor: attack.magnitude },
									)} at AoE Radius ${attack.radius}`}
								/>
							</UITooltip>
						)}
					</dt>
					<dd>
						<UIModifierDescriptor
							className={classnames(styles.modifier, styles.negative)}
							modifier={{
								factor: attack.magnitude,
							}}
						/>
					</dd>
				</React.Fragment>
			);
	}
};

export const UIAttackTypeDescriptorPart: React.FC<
	IUICommonProps & { attackType: UnitAttack.attackFlags; attack: UnitAttack.TAttackTypedDecorator }
> = ({ className, tooltip, attackType, attack }) => {
	const protoAttack = {
		[attackType]: true,
		delivery: UnitAttack.deliveryType.immediate,
	};
	const isAoE = AttackManager.isAOEAttack(attack) && attackType === UnitAttack.attackFlags.AoE;
	const isDoT = attackType === UnitAttack.attackFlags.DoT;
	const withExtra = isAoE || isDoT;
	if (isAoE && AttackManager.isAOEAttack(attack))
		Object.assign(protoAttack, {
			aoeType: attack.aoeType,
			magnitude: attack.magnitude,
			radius: attack.radius,
		});
	return (
		<React.Fragment>
			<dt
				className={classnames(
					UIContainer.descriptorClass,
					tooltip ? UIContainer.hasTooltipClass : null,
					withExtra ? null : styles.extended,
					className,
				)}
			>
				{getAttackTypeIcon(protoAttack, styles.icon, false)}
				<span className={styles.aoeCaption}>
					{getTypeCaption(withExtra ? attack : protoAttack, attackType)}
				</span>
				{!!tooltip && <UITooltip>{getTooltip(withExtra ? attack : protoAttack, attackType)}</UITooltip>}
			</dt>
			{isAoE && (
				<dd>
					<UIAoeRadiusDescriptor
						short={true}
						tooltip={tooltip}
						value={(attack as UnitAttack.IAreaOfEffectAttackDecorator).radius}
						isAura={false}
					/>
				</dd>
			)}
			{isAoE && getAoEAttackExtraMeta(attack as UnitAttack.IAreaOfEffectAttackDecorator, !!tooltip)}
			{isDoT && (
				<dd>
					<UIDurationDescriptor
						duration={
							(attack as UnitAttack.IDamageOverTimeAttackDecorator).duration ||
							DamageOverTimeStatusEffect.baseDuration
						}
						type={'duration'}
						short={true}
						tooltip={!!tooltip}
					/>
				</dd>
			)}
		</React.Fragment>
	);
};

export default class UIAttackDescriptor extends React.PureComponent<UIAttackDescriptorProps, {}> {
	public static getClass(): string {
		return attackDescriptorClass;
	}

	public static getEntityCaption<T extends UnitAttack.TAttackTypedDecorator = UnitAttack.TAttackTypedDecorator>(
		a?: T,
		plural = false,
	) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{getAttackTypeIcon(a, UIContainer.iconClass, false)}
				<span className={styles.caption}>
					{a ? getTypeCaption(a, null, plural) : plural ? 'Attacks' : 'Attack'}
				</span>
			</em>
		);
	}

	public static getAttackTitle(a: UnitAttack.TAttackTypedDecorator) {
		switch (true) {
			case AttackManager.isDeferredAttack(a):
				return 'Deferred Damage';
			case AttackManager.isDOTAttack(a):
				return 'Damage Over Time';
			case AttackManager.isAOEAttack(a):
				return 'Area Attack';
			default:
				return 'Targeted Attack';
		}
	}

	public render() {
		return (
			<div
				className={[
					UIAttackDescriptor.getClass(),
					this.props.details ? '' : styles.short,
					this.props.className || '',
				].join(' ')}
			>
				{!this.props.details && (
					<dl>
						<dt className={[styles.type, UIContainer.descriptorClass].join(' ')}>
							{getAttackTypeIcon(this.props.attack, styles.icon, true)}
							<UIAttackDeliveryDescriptor delivery={this.props.attack.delivery} tooltip={true} />
						</dt>
						<dd>
							<UIDamageTypeIcons
								damage={AttackManager.getAttackDamage(this.props.attack)}
								className={styles.damageTypeIcons}
							/>
						</dd>
					</dl>
				)}

				{this.props.details && (
					<React.Fragment>
						<dl>
							<dt
								className={classnames(
									UIContainer.descriptorClass,
									styles.type,
									this.props.tooltip ? UIContainer.hasTooltipClass : null,
								)}
							>
								<span className={classnames(styles.attackTypeCaption)}>
									{UIAttackDescriptor.getAttackTitle(this.props.attack)}
									{!!this.props.tooltip && (
										<UITooltip>
											{getTooltip(
												this.props.attack,
												AttackManager.isAOEAttack(this.props.attack)
													? UnitAttack.attackFlags.AoE
													: null,
												null,
											)}
										</UITooltip>
									)}
								</span>
							</dt>
							<dd>
								<UIAttackDeliveryDescriptor
									delivery={this.props.attack.delivery}
									tooltip={this.props.tooltip}
								/>
							</dd>
							{UnitAttack.defaultAttackFlagOrder
								.filter((k) => !!this.props.attack?.[k])
								.map((k) => (
									<UIAttackTypeDescriptorPart
										key={k}
										tooltip={!!this.props.tooltip}
										attackType={k}
										attack={this.props.attack}
									/>
								))}
						</dl>
						<UIDamageCollectionDescriptor attack={this.props.attack} tooltip={!!this.props.tooltip} />
					</React.Fragment>
				)}
			</div>
		);
	}
}

import * as React from 'react';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import AttackManager from '../../../../../../definitions/tactical/damage/AttackManager';
import { deliveryCaptions } from '../../../../../../definitions/tactical/damage/strings';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import UIContainer, { IUICommonProps } from '../../../index';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import UISpeedDescriptor from '../../Chassis/SpeedDescriptor';
import styles from './index.scss';

const deliveryDescriptorClass = styles.deliveryDescriptor;

export type UIAttackDeliveryDescriptorProps = IUICommonProps & {
	delivery: UnitAttack.deliveryType;
};

export default class UIAttackDeliveryDescriptor extends React.PureComponent<UIAttackDeliveryDescriptorProps, {}> {
	public static getClass(): string {
		return deliveryDescriptorClass;
	}

	public static getDeliveryTooltip(d: UnitAttack.deliveryType): string {
		const Caption = UIAttackDeliveryDescriptor.getCaption(d);
		switch (d) {
			case UnitAttack.deliveryType.immediate:
				return `${Caption} attack is delivered instantly, bypassing the time-space, and can't be dodged`;
			case UnitAttack.deliveryType.drone:
				return `${Caption} attack is performed by a swarm of flying unmanned vehicles which are advanced enough to make it hard to Dodge. However, there is a special equipment that electronically suppresses Drones, and they can also be downed before they reach the target by a Protect Action from another friendly Unit`;
			case UnitAttack.deliveryType.bombardment:
				return `${Caption} attack is delivered by a guided projectile, coming from altitude at steep attack angle, making it hard to Dodge by most units, especially Naval. Nevertheless, those projectiles can occasionally be intercepted by Protect Action`;
			case UnitAttack.deliveryType.aerial:
				return `${Caption} attack includes various mid-air damaging factors, coming mostly from Anti-Air Weapons, and provides the ultimate threat to airborne Units`;
			case UnitAttack.deliveryType.cloud:
				return `${Caption} attack inflicts Damage in a volume, usually with some gas or pressure impact, reaching targets on any altitudes and reaching through the tightest gaps in landscape`;
			case UnitAttack.deliveryType.surface:
				return `${Caption} attack is transferred either slightly above or straight over the ground, incapable of reaching elevated targets or traversing liquid substances`;
			case UnitAttack.deliveryType.beam:
				return `${Caption} attack is a projected discharge of some energy form, delivering Damage blazing fast and providing additional bonuses for Protect and Suppress Actions`;
			case UnitAttack.deliveryType.supersonic:
				return `${Caption} attack delivers Damage with a projectile that is accelerated to a speed of few Machs, reaching targets faster than most conventional weapons and is capable of penetrating through Units and smaller obstacles`;
			case UnitAttack.deliveryType.missile:
				return `${Caption} is a self-propelled projectile with a payload, highly maneuverable and capable
						of navigating the landscape and hitting targets with great precision`;
			case UnitAttack.deliveryType.ballistic:
				return `${Caption} attack is performed by shooting solid ammunition at the target in a straight line, and is usually the easiest type of Attacks to Dodge`;
			default:
				return `Affects any kind of Attacks, except for Immediate`;
		}
	}

	public static getCaption(d: UnitAttack.deliveryType): string {
		return deliveryCaptions[d] ?? 'All weapons';
	}

	public render() {
		return (
			<span
				className={[
					UIAttackDeliveryDescriptor.getClass(),
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
					this.props.className || '',
				].join(' ')}
			>
				{!!this.props.tooltip && (
					<UITooltip>
						<Tokenizer description={UIAttackDeliveryDescriptor.getDeliveryTooltip(this.props.delivery)} />
						<hr />
						<p>
							<b>Targets</b>:&nbsp;
							{Object.values(UnitChassis.movementType)
								.filter((t) => AttackManager.isReachableByAttack(this.props.delivery, t))
								.map((t, ix, arr) => (
									<React.Fragment key={ix}>
										{UISpeedDescriptor.getMovementCaption(t)}
										{ix === arr.length - 1 ? '' : ', '}
									</React.Fragment>
								))}
							<br />
						</p>
						<p>
							<b>Line of sight</b>:&nbsp;
							{AttackManager.isLineOfSightAttack(this.props.delivery) ? (
								<>
									Yes, unless fired{' '}
									{UISpeedDescriptor.getMovementCaption(UnitChassis.movementType.air)}
								</>
							) : (
								'No'
							)}
						</p>
					</UITooltip>
				)}
				<span className={styles.deliveryCaption}>
					{UIAttackDeliveryDescriptor.getCaption(this.props.delivery)}
				</span>
			</span>
		);
	}
}

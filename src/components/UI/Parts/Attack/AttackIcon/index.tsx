import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import AttackManager from '../../../../../../definitions/tactical/damage/AttackManager';
import { getAOETypeCaption, getTypeCaption } from '../../../../../../definitions/tactical/damage/strings';
import { UIDurationIcon, UISplashIcon, UITargetIcon } from '../../../../Symbols/commonIcons';
import UIContainer from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import { basicTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import styles from '../AttackDescriptor/index.scss';

export const UIAttackIconAOE_Aside_ID = 'weapon-type-icon-aoe-aside';
export const UIAttackIconAOE_Chain_ID = 'weapon-type-icon-aoe-chain';
export const UIAttackIconAOE_Behind_ID = 'weapon-type-icon-aoe-behind';
export const UIAttackIconAOE_Scatter_ID = 'weapon-type-icon-aoe-scatter';
export const UIAttackIconAOE_Cone90_ID = 'weapon-type-icon-aoe-cone90';
export const UIAttackIconAOE_Cone180_ID = 'weapon-type-icon-aoe-cone180';
export const UIAttackIconDelayed_ID = 'weapon-type-icon-delayed';
export const UIAttackIconPenetrative_ID = 'weapon-type-icon-piercing';
export const UIAttackIconPure_ID = 'weapon-type-icon-pure';
export const UIAttackIconSelective_ID = 'weapon-type-icon-selective';

SVGIconContainer.registerSymbol(
	UIAttackIconAOE_Cone180_ID,
	<symbol viewBox="200 0 200 200">
		<circle cx="300" cy="130" r="15" />
		<path d="M 254.997 52.06 C 314.995 17.416 389.997 60.714 390 129.996 C 390 129.997 390 129.999 390 130 L 375 130 C 375 129.999 375 129.998 375 129.997 C 374.999 103.202 360.703 78.443 337.498 65.047 C 287.497 36.183 225.001 72.267 225 130 L 210 130 C 210 97.848 227.153 68.137 254.997 52.06 Z M 267.498 73.71 C 310.83 48.69 364.998 79.96 365 129.998 C 365 129.999 365 129.999 365 130 L 350 130 C 350 129.999 350 129.999 350 129.998 C 349.999 112.135 340.469 95.629 324.998 86.698 C 291.665 67.454 250 91.512 250 130 L 235 130 C 235 106.779 247.388 85.321 267.498 73.71 Z M 279.998 95.36 C 306.664 79.962 339.998 99.206 340 129.998 C 340 129.999 340 129.999 340 130 L 325 130 C 325 130 325 129.999 325 129.999 C 324.999 121.067 320.234 112.814 312.499 108.349 C 295.832 98.727 275 110.756 275 130 L 260 130 C 259.999 115.71 267.622 102.505 279.998 95.36 Z" />
	</symbol>,
)
	.registerSymbol(
		UIAttackIconAOE_Behind_ID,
		<symbol viewBox="200 0 200 200">
			<circle cx="300" cy="160" r="25" />
			<path
				d="M 296.023 83.523 H 363.636 L 356.67 45.273 L 416.67 127.273 L 356.67 209.273 L 363.636 171.023 H 296.023 V 83.523 Z"
				transform="matrix(0, -1, 1, 0, 172.72699, 423.295471)"
			/>
		</symbol>,
	)
	.registerSymbol(
		UIAttackIconSelective_ID,
		<symbol viewBox="-15 0 512 512">
			<path d="m135.59375 256.613281h-102.667969c13.09375 35.820313 29.601563 74.785157 48.523438 106.175781 29.230469 48.488282 66.398437 86.667969 85.953125 105.113282 14.339844 13.527344 28.289062 25.058594 40.332031 33.347656 7.578125 5.214844 13.597656 8.582031 18.4375 10.75v-164.808594c-46.738281-6.777344-83.800781-43.839844-90.578125-90.578125zm0 0" />
			<path d="m257.058594 347.191406v164.46875c4.707031-2.175781 10.5-5.464844 17.6875-10.414062 12.042968-8.289063 25.988281-19.820313 40.332031-33.347656 19.554687-18.441407 56.722656-56.625 85.953125-105.109376 18.925781-31.390624 35.429688-70.355468 48.519531-106.175781h-101.914062c-6.777344 46.738281-43.839844 83.800781-90.578125 90.578125zm0 0" />
			<path d="m225.804688 135.199219v-135.199219h-91.527344c-7.339844 0-13.664063 5.164062-15.132813 12.351562-.128906.644532-14.28125 65.050782-111.640625 123.40625-5.847656 3.503907-8.703125 10.429688-7.03125 17.039063.652344 2.566406 8.472656 33.140625 21.695313 72.929687h113.425781c6.757812-46.617187 43.644531-83.609374 90.210938-90.527343zm0 0" />
			<path d="m347.636719 225.726562h112.675781c13.222656-39.785156 21.042969-70.363281 21.691406-72.929687 1.675782-6.609375-1.179687-13.535156-7.027344-17.039063-50.441406-30.230468-78.128906-62.253906-92.46875-83.796874-15.035156-22.574219-18.949218-38.652344-19.183593-39.667969-1.40625-7.253907-7.722657-12.292969-15.121094-12.292969h-91.511719v135.097656c46.914063 6.636719 84.148438 43.765625 90.945313 90.628906zm0 0" />
			<path d="m241.617188 164.914062c-42.046876 0-76.253907 34.210938-76.253907 76.253907 0 42.046875 34.207031 76.253906 76.253907 76.253906 42.042968 0 76.25-34.207031 76.25-76.253906 0-42.042969-34.207032-76.253907-76.25-76.253907zm42.21875 69.660157-46.082032 46.082031c-2.902344 2.902344-6.832031 4.523438-10.917968 4.523438-.246094 0-.496094-.003907-.75-.019532-4.347657-.210937-8.40625-2.246094-11.175782-5.609375l-19.707031-23.953125c-5.417969-6.585937-4.472656-16.316406 2.113281-21.734375 6.585938-5.421875 16.316406-4.476562 21.734375 2.109375l8.898438 10.8125 34.050781-34.046875c6.03125-6.03125 15.808594-6.03125 21.835938 0 6.035156 6.027344 6.035156 15.804688 0 21.835938zm0 0" />
		</symbol>,
	)
	.registerSymbol(
		UIAttackIconPure_ID,
		<symbol viewBox="0 0 194.291 194.291">
			<path
				d="M157.208,119.194l-37.213,13.072l-6.869-15.424c-0.681-1.528-1.969-2.703-3.553-3.241s-3.321-0.39-4.792,0.409
		l-55.472,30.136c-2.842,1.544-3.948,5.065-2.501,7.958c0.206,0.412,0.458,0.798,0.753,1.152
		c12.811,15.411,28.426,28.912,46.41,40.126c0.972,0.606,2.073,0.909,3.175,0.909s2.203-0.303,3.175-0.909
		c27.299-17.022,48.858-39.072,64.079-65.535c1.352-2.351,0.958-5.318-0.96-7.236C161.808,118.98,159.386,118.43,157.208,119.194z
		 M97.146,181.178c-13.399-8.714-25.318-18.794-35.543-30.054l43.202-23.47l6.478,14.547c1.271,2.855,4.52,4.255,7.47,3.22
		l25.919-9.105C132.103,153.766,116.181,168.801,97.146,181.178z"
			/>
			<path
				d="M188.493,32.878c-1.133-1.19-2.704-1.863-4.346-1.863c-0.046,0.001-0.093,0-0.139,0c-6.422,0-12.409-2.564-16.869-7.229
		c-4.528-4.735-6.844-10.947-6.521-17.492c0.081-1.641-0.514-3.243-1.647-4.432C157.837,0.673,156.267,0,154.624,0H39.667
		c-1.643,0-3.213,0.673-4.346,1.863c-1.133,1.189-1.728,2.792-1.647,4.432c0.323,6.544-1.993,12.756-6.521,17.492
		c-4.46,4.664-10.447,7.229-16.869,7.229c-0.037,0-0.074,0-0.111,0c-1.699-0.011-3.235,0.667-4.374,1.863s-1.734,2.811-1.645,4.46
		c1.329,24.612,7.598,61.318,29.918,96.415c1.138,1.79,3.077,2.781,5.067,2.781c0.97,0,1.952-0.235,2.86-0.729l73.936-40.167
		l8.085,19.692c1.208,2.943,4.504,4.431,7.508,3.393l41.602-14.362c1.672-0.577,3.005-1.863,3.641-3.514
		c7.67-19.908,12.167-41.276,13.368-63.51C190.227,35.688,189.631,34.075,188.493,32.878z M166.55,93.938l-33.582,11.594
		l-8.473-20.635c-0.647-1.576-1.934-2.803-3.54-3.374c-1.604-0.572-3.377-0.433-4.875,0.38l-74.808,40.64
		C24.276,93.75,18.327,64.19,16.531,42.471c7.321-1.296,14.017-4.872,19.294-10.39c5.268-5.509,8.609-12.597,9.582-20.081h103.478
		c0.973,7.484,4.313,14.572,9.582,20.081c5.278,5.52,11.974,9.095,19.297,10.39C176.295,60.411,172.533,77.684,166.55,93.938z"
			/>
		</symbol>,
	)
	.registerSymbol(
		UIAttackIconPenetrative_ID,
		<symbol viewBox="0 0 438 438">
			<path
				d="M310.655,131.095l-61.862-24.436v326.47c0,2.73-2.223,4.953-4.959,4.953h-49.58c-2.734,0-4.951-2.223-4.951-4.953V106.66
		l-61.868,24.436c-2.078,0.815-4.439,0.15-5.781-1.635c-1.345-1.773-1.339-4.232,0.029-5.999L215.119,1.93
		c0.937-1.221,2.397-1.93,3.928-1.93c1.543,0,2.991,0.709,3.928,1.93l93.438,121.531c0.686,0.889,1.028,1.956,1.028,3.02
		c0,1.052-0.331,2.101-1.005,2.979C315.1,131.251,312.729,131.911,310.655,131.095z M397.431,173.227H266.951v105.921h130.479
		c6.686,0,12.105-5.421,12.105-12.105v-81.71C409.536,178.65,404.116,173.227,397.431,173.227z M28.546,185.332v81.71
		c0,6.685,5.423,12.105,12.105,12.105H171.14V173.227H40.651C33.969,173.227,28.546,178.65,28.546,185.332z"
			/>
		</symbol>,
	)
	.registerSymbol(
		UIAttackIconAOE_Chain_ID,
		<symbol viewBox="0 0 612 612">
			<path
				d="M537.009,52.103c-41.348,0-74.991,33.641-74.991,74.991c0,22.798,10.238,43.237,26.347,57.003l-70.466,133.881
				c-6.214-1.672-12.736-2.578-19.47-2.578c-16.092,0-31.006,5.113-43.233,13.774l-53.625-48.369
				c6.13-10.862,9.648-23.388,9.648-36.723c0-41.351-33.643-74.991-74.994-74.991c-41.348,0-74.991,33.641-74.991,74.991
				c0,19.961,7.854,38.109,20.616,51.56l-79.882,119.313c-8.378-3.242-17.469-5.04-26.978-5.04C33.64,409.915,0,443.556,0,484.906
				s33.64,74.991,74.991,74.991c41.348,0,74.991-33.641,74.991-74.991c0-19.961-7.854-38.111-20.616-51.562l79.882-119.313
				c8.378,3.242,17.469,5.04,26.978,5.04c16.095,0,31.006-5.113,43.236-13.775l53.622,48.369
				c-6.127,10.862-9.646,23.387-9.646,36.723c0,41.35,33.643,74.991,74.991,74.991c41.351,0,74.994-33.641,74.994-74.991
				c0-22.798-10.241-43.237-26.347-57.002l70.464-133.88c6.214,1.672,12.736,2.579,19.47,2.579
				c41.351,0,74.991-33.641,74.991-74.991C612,85.744,578.36,52.103,537.009,52.103z M74.991,526.902
				c-23.157,0-41.996-18.839-41.996-41.996c0-23.157,18.839-41.996,41.996-41.996s41.996,18.839,41.996,41.996
				C116.987,508.063,98.148,526.902,74.991,526.902z M236.225,286.077c-23.157,0-41.996-18.839-41.996-41.994
				c0-23.157,18.839-41.996,41.996-41.996s41.999,18.839,41.999,41.996C278.224,267.238,259.382,286.077,236.225,286.077z
				 M356.434,390.389c0-23.154,18.839-41.993,41.996-41.993s41.999,18.839,41.999,41.993c0,23.157-18.842,41.996-41.999,41.996
				C375.273,432.385,356.434,413.546,356.434,390.389z M537.009,169.09c-23.157,0-41.996-18.839-41.996-41.996
				s18.839-41.996,41.996-41.996c23.157,0,41.996,18.839,41.996,41.996C579.005,150.252,560.166,169.09,537.009,169.09z"
			/>
		</symbol>,
	)
	.registerSymbol(
		UIAttackIconAOE_Aside_ID,
		<symbol viewBox="0 0 200 200">
			<circle cx="100" cy="100" r="15" />
			<path d="M 118.75 80 H 161.922 L 152.317 50 L 200.343 100 L 152.317 150 L 161.922 120 H 118.75 V 80 Z" />
			<path d="M -81.933 -120.568 H -38.761 L -48.366 -150.568 L -0.34 -100.568 L -48.366 -50.568 L -38.761 -80.568 H -81.933 V -120.568 Z" />
			<path
				d="M -81.933 -120.568 H -38.761 L -48.366 -150.568 L -0.34 -100.568 L -48.366 -50.568 L -38.761 -80.568 H -81.933 V -120.568 Z"
				transform="matrix(-1, 0, 0, -1, 0, 0)"
			/>
		</symbol>,
	)
	.registerSymbol(
		UIAttackIconAOE_Scatter_ID,
		<symbol viewBox="0 0 490.4 490.4">
			<g
				style={{
					transformOrigin: '50%, 50%',
					transform: 'scale(0.9) translate(0, -1%)',
				}}>
				<path
					d="M454.4,209.85c-15.9,0-29.5,10.9-33.4,25.6h-71.1c-1.9-22.1-10.9-42.3-24.5-58.6l50.5-50.5c13.2,7.8,30.3,5.8,41.9-5.4
		c13.6-13.6,13.6-35.7,0-49.3s-35.7-13.6-49.3,0c-11.3,11.3-13.2,28.4-5.4,41.9l-50.5,50.5c-16.3-13.6-36.5-22.5-58.6-24.5v-71.1
		c14.8-3.9,25.6-17.5,25.6-33.4c0-19-15.5-35-35-35c-19,0-35,15.5-35,35c0,15.9,10.9,29.5,25.6,33.4v71.5
		c-22.1,1.9-42.3,10.9-58.6,24.5l-50.5-50.5c7.8-13.2,5.8-30.7-5.4-41.9c-13.6-13.6-35.7-13.6-49.3,0s-13.6,35.7,0,49.3
		c11.3,11.3,28.4,13.2,41.9,5.4l50.5,50.5c-13.6,16.3-22.5,36.5-24.5,58.6H68.4c-3.9-14.8-17.5-25.7-33.4-25.7c-19,0-35,15.5-35,35
		c0,19,15.5,35,35,35c15.9,0,29.5-10.9,33.4-25.6h71.5c1.9,22.1,10.9,42.3,24.5,58.6l-50.5,50.5c-13.2-7.8-30.7-5.8-41.9,5.4
		c-13.6,13.6-13.6,35.7,0,49.3c13.6,13.6,35.7,13.6,49.3,0c11.3-11.3,13.2-28.4,5.4-41.9l50.5-50.5c16.3,13.6,36.5,22.5,58.6,24.5
		v71.5c-14.8,3.9-25.6,17.5-25.6,33.4c0,19,15.5,35,35,35c19,0,35-15.5,35-35c0-15.9-10.9-29.5-25.6-33.4v-71.9
		c22.1-1.9,42.3-10.9,58.6-24.5l50.5,50.5c-7.8,13.2-5.8,30.7,5.4,41.9c13.6,13.6,35.7,13.6,49.3,0c13.6-13.6,13.6-35.7,0-49.3
		c-11.3-11.3-28.4-13.2-41.9-5.4l-50.5-50.5c13.6-16.3,22.5-36.5,24.5-58.6H422c3.9,14.8,17.5,25.6,33.4,25.6c19,0,35-15.5,35-35
		S473.9,209.85,454.4,209.85z"
				/>
			</g>
		</symbol>,
	)
	.registerSymbol(
		UIAttackIconAOE_Cone90_ID,
		<symbol viewBox="200 0 200 200">
			<g
				style={{
					transformOrigin: '50% 50%',
					transform: 'translate(0,-5%)',
				}}>
				<circle cx="300" cy="157.488" r="20" />
				<path d="M 241.819 55.779 C 290.347 27.757 346.397 38.81 382.003 72.791 L 368.117 86.679 C 362.55 81.367 356.311 76.671 349.481 72.727 C 309.17 49.456 262.635 58.535 232.952 86.639 L 219.142 72.827 C 225.9 66.327 233.494 60.586 241.819 55.779 Z M 258.133 84.031 C 292.971 63.915 333.177 71.683 358.914 95.88 L 345.037 109.757 C 341.468 106.432 337.496 103.478 333.168 100.98 C 306.548 85.611 275.855 91.408 256.044 109.73 L 242.216 95.902 C 246.981 91.389 252.308 87.395 258.133 84.031 Z M 274.445 112.284 C 295.595 100.071 319.959 104.557 335.817 118.977 L 321.939 132.855 C 320.381 131.503 318.684 130.287 316.858 129.235 C 303.927 121.768 289.074 124.291 279.156 132.842 L 265.3 118.985 C 268.063 116.455 271.123 114.202 274.445 112.284 Z" />
			</g>
		</symbol>,
	)
	.registerSymbol(
		UIAttackIconDelayed_ID,
		<symbol viewBox="0 0 512 512">
			<g
				style={{
					transformOrigin: '50%, 50%',
					transform: 'scale(0.9) translate(0, 5%)',
				}}>
				<path d="m256 90.054688c24.8125 0 45-20.199219 45-45.027344s-20.1875-45.027344-45-45.027344-45 20.199219-45 45.027344 20.1875 45.027344 45 45.027344zm0 0" />
				<path d="m437.023438 74.613281c-24.957032-24.980469-54.0625-44.179687-86.503907-57.066406-4.988281-1.984375-10.660156-1.144531-14.867187 2.195313-4.207032 3.34375-6.300782 8.679687-5.5 13.988281.5625 3.71875.847656 7.519531.847656 11.296875 0 14.636718-4.234375 28.835937-12.246094 41.058594-2.382812 3.636718-3.070312 8.125-1.886718 12.308593 1.183593 4.183594 4.125 7.648438 8.0625 9.492188 27.707031 12.996093 51.191406 33.480469 67.914062 59.238281 17.148438 26.421875 26.21875 57.078125 26.21875 88.65625 0 90-73.148438 163.222656-163.0625 163.222656-89.910156 0-163.0625-73.222656-163.0625-163.222656 0-20.804688 3.832031-41.039062 11.382812-60.136719 1.941407-4.910156 1.160157-10.476562-2.058593-14.660156-17.214844-22.390625-32.730469-42.648437-42.710938-55.699219-3.039062-3.96875-7.855469-6.167968-12.847656-5.855468s-9.5 3.089843-12.019531 7.410156c-22.691406 38.871094-34.683594 83.457031-34.683594 128.941406 0 68.4375 26.628906 132.777344 74.976562 181.167969 48.351563 48.398437 112.640626 75.050781 181.023438 75.050781s132.671875-26.652344 181.023438-75.050781c48.347656-48.390625 74.976562-112.730469 74.976562-181.167969 0-68.433594-26.628906-132.773438-74.976562-181.167969zm0 0" />
				<path d="m224.175781 287.617188c8.5 8.507812 19.800781 13.191406 31.824219 13.191406s23.324219-4.6875 31.824219-13.191406c17.539062-17.554688 17.539062-46.113282 0-63.667969-12.070313-12.085938-174.628907-136.5-193.125-150.644531-5.96875-4.570313-14.398438-4.007813-19.714844 1.300781-5.316406 5.3125-5.878906 13.742187-1.320313 19.714843 14.136719 18.511719 138.4375 181.214844 150.511719 193.296876zm0 0" />
			</g>
		</symbol>,
	);

export const iconAttackMapping: EnumProxy<UnitAttack.attackFlags, string> = {
	[UnitAttack.attackFlags.Deferred]: UIAttackIconDelayed_ID,
	[UnitAttack.attackFlags.DoT]: UIDurationIcon,
	[UnitAttack.attackFlags.Penetrative]: UIAttackIconPenetrative_ID,
	[UnitAttack.attackFlags.Selective]: UIAttackIconSelective_ID,
	[UnitAttack.attackFlags.Disruptive]: UIAttackIconPure_ID,
	[UnitAttack.attackFlags.AoE]: UISplashIcon,
};

export const getAttackIcon = (
	symbol: string,
	className: string = UIContainer.iconClass,
	tooltip: string | JSX.Element = null,
	color: TSVGSmallTexturedIconStyles = TSVGSmallTexturedIconStyles.red,
) => {
	return (
		<SVGSmallTexturedIcon
			key={JSON.stringify({ symbol, color })}
			style={color}
			tooltip={tooltip}
			className={className}>
			<use {...SVGIconContainer.getXLinkProps(symbol)} />
		</SVGSmallTexturedIcon>
	);
};

export const getAOETypeIconID = (a: UnitAttack.TAoEDamageSplashTypes) => {
	switch (a) {
		case UnitAttack.TAoEDamageSplashTypes.chain:
			return UIAttackIconAOE_Chain_ID;
		case UnitAttack.TAoEDamageSplashTypes.scatter:
			return UIAttackIconAOE_Scatter_ID;
		case UnitAttack.TAoEDamageSplashTypes.cone90:
			return UIAttackIconAOE_Cone90_ID;
		case UnitAttack.TAoEDamageSplashTypes.cone180:
			return UIAttackIconAOE_Cone180_ID;
		case UnitAttack.TAoEDamageSplashTypes.lineAside:
			return UIAttackIconAOE_Aside_ID;
		case UnitAttack.TAoEDamageSplashTypes.lineBehind:
			return UIAttackIconAOE_Behind_ID;
		default:
			return UISplashIcon;
	}
};

export const getTooltip = <T extends UnitAttack.TAttackTypedDecorator = UnitAttack.TAttackTypedDecorator>(
	a: T,
	type: UnitAttack.attackFlags,
	aoeType: UnitAttack.TAoEDamageSplashTypes = (a as UnitAttack.IAreaOfEffectAttackDecorator)?.aoeType,
) => {
	if (AttackManager.isAOEAttack(a)) {
		const icon = getAttackIcon(getAOETypeIconID(aoeType), UITooltip.getIconClass());
		const damageMod =
			a.magnitude < 1
				? `as little as ${BasicMaths.describeModifier({ factor: a.magnitude }, true)}`
				: a.magnitude > 1
				? `as much as ${BasicMaths.describeModifier({ factor: a.magnitude }, true)}`
				: 'full';
		const title = getAOETypeCaption(a);
		return basicTooltipTemplate(
			icon,
			title,
			aoeType === UnitAttack.TAoEDamageSplashTypes.chain
				? `is a kind of ${getAOETypeCaption(null)} that bounces between appropriate targets in ${
						a.radius
				  } AoE Radius for ${
						a.magnitude
				  } times, prioritizing closest Units and dealing full Attack Damage to them, bypassing Dodge checks.`
				: aoeType === UnitAttack.TAoEDamageSplashTypes.scatter
				? `is an ${getAOETypeCaption(null)} that deals Damage ${
						a.magnitude
				  } times, multiplied by number of appropriate targets in ${
						a.radius
				  } AoE Radius (rounded up), picking a random Unit as a target each time. Dodge checks are bypassed.`
				: aoeType === UnitAttack.TAoEDamageSplashTypes.cone90
				? `is an ${getAOETypeCaption(
						null,
				  )} that splashes in 90 degree sector behind the point of impact, aligned with the velocity of the shot, dealing ${damageMod} Damage to targets up to ${
						a.radius
				  } AoE Radius away.`
				: aoeType === UnitAttack.TAoEDamageSplashTypes.cone180
				? `is an ${getAOETypeCaption(
						null,
				  )} that splashes in 180 degree sector behind the point of impact, aligned with the velocity of the shot, dealing ${damageMod} Damage to targets up to ${
						a.radius
				  } AoE Radius away.`
				: aoeType === UnitAttack.TAoEDamageSplashTypes.lineAside
				? `is an ${getAOETypeCaption(
						null,
				  )} that is applied to all appropriate Units sideways from impact point, dealing ${damageMod} Damage to targets up to ${
						a.radius
				  } AoE Radius away.`
				: aoeType === UnitAttack.TAoEDamageSplashTypes.lineBehind
				? `is an ${getAOETypeCaption(
						null,
				  )} that is applied to all appropriate Units behind impact point, dealing ${damageMod} Damage to targets up to ${
						a.radius
				  } AoE Radius away.`
				: aoeType === UnitAttack.TAoEDamageSplashTypes.circle
				? `is an ${getAOETypeCaption(
						null,
				  )} that hits all appropriate Units near impact point, dealing ${damageMod} Damage to targets up to ${
						a.radius
				  } AoE Radius away from it.`
				: `hits all reachable Units in an area, depending on its type. It can be targeted at any point at Map within Attack Range or batched with Targeted Attack. ${getAOETypeCaption(
						a,
						true,
				  )} always connect, cannot be mitigated by Dodge (unless Immune) and break Clock`,
		);
	}
	const icon = getAttackIcon(iconAttackMapping[type] ?? UITargetIcon, UITooltip.getIconClass());
	if (AttackManager.isDOTAttack(a)) {
		return basicTooltipTemplate(
			icon,
			getTypeCaption(a),
			`deals Damage every Turn Start, lasting for ${a.duration} Turns. However, if batched Targeted Attack and/or Area Attack misses, the Damage is only applied once`,
		);
	}
	if (AttackManager.isDeferredAttack(a)) {
		return basicTooltipTemplate(
			icon,
			getTypeCaption(a),
			`is a damaging factor that takes time to settle. The Damage is dealt at the Turn
					End, but only if the batched Targeted Attack and/or Area Attack connected`,
		);
	}
	if (AttackManager.isDisruptiveAttack(a)) {
		return basicTooltipTemplate(
			icon,
			getTypeCaption(a),
			`ignores any Shields present on target Unit and always does Damage to Hit Points, mitigated only by Armor`,
		);
	}
	if (AttackManager.isSelectiveAttack(a)) {
		return basicTooltipTemplate(
			icon,
			getTypeCaption(a),
			`employs friend-or-foe recognition and communication feedback to avoid friendly fire and collateral casualties. Does not deal Damage to Allied Units and Doodads`,
		);
	}
	if (AttackManager.isPenetrativeAttack(a)) {
		return basicTooltipTemplate(
			icon,
			getTypeCaption(a),
			`has extreme velocity and is capable of penetrating through anything, dealing Damage to all Units on the line of fire, as well as to tall Doodads, which can occasionally be severely reduced`,
		);
	}
	return basicTooltipTemplate(
		icon,
		getTypeCaption({ delivery: UnitAttack.deliveryType.immediate }),
		`is an aimed shot from a chosen Weapon at Unit. Those attacks have to pass Dodge checks to deal Damage and/or to apply any related Effects`,
	);
};

export const getAttackTypeIcon = <T extends UnitAttack.TAttackTypedDecorator = UnitAttack.TAttackTypedDecorator>(
	a: T,
	className: string,
	tooltip = false,
) => {
	switch (true) {
		case AttackManager.isDeferredAttack(a):
			return getAttackIcon(
				iconAttackMapping[UnitAttack.attackFlags.Deferred],
				styles.icon,
				tooltip ? getTooltip(a, UnitAttack.attackFlags.Deferred) : null,
			);
		case AttackManager.isDOTAttack(a):
			return getAttackIcon(
				iconAttackMapping[UnitAttack.attackFlags.DoT],
				styles.icon,
				tooltip ? getTooltip(a, UnitAttack.attackFlags.DoT) : null,
			);
		case AttackManager.isAOEAttack(a):
			return getAttackIcon(
				getAOETypeIconID((a as UnitAttack.IAreaOfEffectAttackDecorator).aoeType),
				className,
				tooltip ? getTooltip(a, UnitAttack.attackFlags.AoE) : null,
			);
		case AttackManager.isSelectiveAttack(a):
			return getAttackIcon(
				iconAttackMapping[UnitAttack.attackFlags.Selective],
				styles.icon,
				tooltip ? getTooltip(a, UnitAttack.attackFlags.Selective) : null,
			);
		case AttackManager.isDisruptiveAttack(a):
			return getAttackIcon(
				iconAttackMapping[UnitAttack.attackFlags.Disruptive],
				styles.icon,
				tooltip ? getTooltip(a, UnitAttack.attackFlags.Disruptive) : null,
			);
		case AttackManager.isPenetrativeAttack(a):
			return getAttackIcon(
				iconAttackMapping[UnitAttack.attackFlags.Penetrative],
				styles.icon,
				tooltip ? getTooltip(a, UnitAttack.attackFlags.Penetrative) : null,
			);
		default:
			return getAttackIcon(UITargetIcon, styles.icon, tooltip ? getTooltip(a, null) : null);
	}
};

import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import { wellRounded } from '../../../../../utils/wellRounded';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import { SVGAttackQuantityIconID } from '../AttacksQuantityDescriptor';
import styles from './index.scss';

const UIAttackRateDescriptorClass = styles.rateDescriptor;

export interface UIAttackRateDescriptorProps extends IUICommonProps {
	attackRate?: number;
	modifier?: IModifier;
	precision?: number;
	short?: boolean;
}

export const SVGRateIconID = 'weapon-rate-icon';

SVGIconContainer.registerSymbol(
	SVGRateIconID,
	<symbol viewBox="0 0 503.067 503.06">
		<g
			style={{
				transformOrigin: '50% 50%',
				transform: 'scale(-1.2, 1) translate(-3%, 0%)',
			}}>
			<rect x="83.933" y="235.016" width="100.721" height="184.656" />
			<polygon points="167.868,191.067 167.868,151.08 100.721,151.08 100.721,191.067 87.14,218.228 181.449,218.228 			" />
			<path
				d="M153.048,41.682l-10.71-35.697C141.272,2.434,137.999,0,134.297,0c-3.71,0-6.975,2.434-8.041,5.985l-10.718,35.697
				c-9.031,30.107-13.765,61.23-14.512,92.613h66.535C166.813,102.912,162.079,71.789,153.048,41.682z"
			/>
			<path
				d="M177.196,458.165c0.26-0.688,0.529-1.368,0.713-2.09l4.902-19.615H85.775l4.893,19.615
				c0.185,0.722,0.453,1.402,0.722,2.09c-4.516,3.794-7.453,9.417-7.453,15.763v8.998c0,11.407,9.275,20.681,20.681,20.681h59.358
				c11.398,0,20.681-9.275,20.681-20.681v-8.998C184.658,467.583,181.72,461.959,177.196,458.165z"
			/>
			<polygon points="402.885,258.214 402.885,218.228 335.737,218.228 335.737,258.214 322.157,285.375 416.465,285.375 			" />
			<rect x="318.95" y="302.164" width="100.721" height="117.508" />
			<path
				d="M388.065,108.829l-10.71-35.697c-1.066-3.55-4.339-5.984-8.041-5.984c-3.71,0-6.975,2.434-8.041,5.984l-10.718,35.697
				c-9.031,30.107-13.765,61.23-14.512,92.613h66.535C401.83,170.06,397.096,138.937,388.065,108.829z"
			/>
			<path
				d="M412.212,458.165c0.26-0.688,0.529-1.368,0.713-2.09l4.902-19.615h-97.037l4.893,19.615
				c0.185,0.722,0.453,1.402,0.722,2.09c-4.516,3.794-7.453,9.417-7.453,15.763v8.998c0,11.407,9.275,20.681,20.681,20.681h59.358
				c11.398,0,20.681-9.275,20.681-20.681v-8.998C419.674,467.583,416.737,461.959,412.212,458.165z"
			/>
			<rect x="201.441" y="268.59" width="100.721" height="151.082" />
			<polygon points="285.377,224.64 285.377,184.654 218.229,184.654 218.229,224.64 204.648,251.802 298.957,251.802 			" />
			<path
				d="M270.556,75.256l-10.71-35.697c-1.066-3.55-4.339-5.984-8.041-5.984c-3.71,0-6.975,2.434-8.041,5.984l-10.718,35.697
				c-9.031,30.107-13.765,61.23-14.512,92.613h66.535C284.322,136.486,279.588,105.363,270.556,75.256z"
			/>
			<path
				d="M294.704,458.165c0.26-0.688,0.529-1.368,0.713-2.09l4.902-19.615h-97.037l4.893,19.615
				c0.185,0.722,0.453,1.402,0.722,2.09c-4.516,3.794-7.453,9.417-7.453,15.763v8.998c0,11.407,9.275,20.681,20.681,20.681h59.358
				c11.398,0,20.681-9.275,20.681-20.681v-8.998C302.166,467.583,299.228,461.959,294.704,458.165z"
			/>
		</g>
	</symbol>,
);

export default class UIAttackRateDescriptor extends React.PureComponent<UIAttackRateDescriptorProps, {}> {
	public static getClass(): string {
		return UIAttackRateDescriptorClass;
	}

	public static getCaption() {
		return 'Attack Rate';
	}

	public static getTooltip() {
		return (
			<span>
				{UIAttackRateDescriptor.getIcon(classnames(UITooltip.getIconClass(), styles.tooltipIcon))}
				<b>{UIAttackRateDescriptor.getCaption()}</b> describes the frequency of attacks. The bigger it is, the
				more attacks a unit is able to perform per turn, but the exact amount of attacks is variable in a
				certain range
			</span>
		);
	}

	public static getIcon(className?: string) {
		return (
			<SVGSmallTexturedIcon
				style={TSVGSmallTexturedIconStyles.gray}
				className={classnames(UIContainer.iconClass, className || '')}>
				<use {...SVGIconContainer.getXLinkProps(SVGAttackQuantityIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIAttackRateDescriptor.getIcon()}
				<span className={styles.rateCaption}>{UIAttackRateDescriptor.getCaption()}</span>
			</em>
		);
	}

	public static getValue({
		value,
		modifier,
		precision = 2,
	}: {
		value: number;
		modifier?: IModifier;
		precision?: number;
	}) {
		const isPositive = BasicMaths.applyModifier(100, modifier) >= 100;
		const mod = (
			<span className={classnames(styles.modifier, isPositive ? styles.positive : styles.negative)}>
				{BasicMaths.describeModifier(modifier, true)}
			</span>
		);
		const v = wellRounded(value, precision);
		return (
			<>
				{Number.isFinite(value) && <span className={classnames(styles.value)}>{v}</span>}
				{!!modifier && (Number.isFinite(value) ? <>({mod})</> : mod)}
			</>
		);
	}

	public render() {
		const classNames = [UIAttackRateDescriptor.getClass(), this.props.className || ''];
		const itemClassName = classnames(
			!!this.props.tooltip && UIContainer.hasTooltipClass,
			UIContainer.captionClass,
			UIContainer.descriptorClass,
			styles.rateCaption,
		);
		const value = UIAttackRateDescriptor.getValue({
			value: this.props.attackRate,
			precision: this.props.precision,
			modifier: this.props.modifier,
		});
		return this.props.short ? (
			<span className={classnames(...classNames, this.props.tooltip ? UIContainer.hasTooltipClass : '')}>
				{UIAttackRateDescriptor.getIcon(UIContainer.iconClass)}
				<span className={styles.value}>{value}</span>
				{!!this.props.tooltip && <UITooltip>{UIAttackRateDescriptor.getTooltip()}</UITooltip>}
			</span>
		) : (
			<dl className={classnames(...classNames)}>
				<dt className={itemClassName}>
					{UIAttackRateDescriptor.getIcon(UIContainer.iconClass)}
					{UIAttackRateDescriptor.getCaption()}
					{!!this.props.tooltip && <UITooltip>{UIAttackRateDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>{value}</dd>
			</dl>
		);
	}
}

import * as React from 'react';
import Distributions from '../../../../../../definitions/maths/Distributions';
import AttackManager from '../../../../../../definitions/tactical/damage/AttackManager';
import { wellRounded } from '../../../../../utils/wellRounded';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../../index';
import UIDistributionIcon from '../../../SVG/DistributionIcon';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import styles from './index.scss';

const UIAttacksQuantityDescriptorClass = styles.attacksQuantityDescriptor;

export type UIAttacksQuantityDescriptorProps = IUICommonProps & {
	attackRate: number;
	phases?: number;
};

export const SVGAttackQuantityIconID = 'weapon-quantity-icon';
export const getAttackQuantityDistribution: (weaponRate: number) => Distributions.TDistribution = (weaponRate) => {
	switch (true) {
		case weaponRate >= 25:
			return 'MostlyMaximal';
		case weaponRate >= 7.5:
			return 'Maximal';
		case weaponRate >= 0.8:
			return 'Bell';
		case weaponRate >= 0.25:
			return 'Gaussian';
		case weaponRate >= 0.15:
			return 'Minimal';
		default:
			return 'MostlyMinimal';
	}
};

SVGIconContainer.registerSymbol(
	SVGAttackQuantityIconID,
	<symbol viewBox="0 0 512 512">
		<g
			style={{
				transform: 'translateY(-10%)',
			}}>
			<path
				d="M66.519,50.46L52.617,19.104L38.713,50.46c-0.883,1.991-19.51,44.695-23.469,107.216h74.744
			C86.029,95.155,67.402,52.451,66.519,50.46z"
			/>
			<polygon points="8.922,188.094 8.922,218.51 16.196,218.51 94.406,218.51 96.31,218.51 96.31,188.094 		" />
			<polygon points="101.021,248.927 9.581,248.927 0,248.927 0,492.896 105.232,492.896 105.232,248.927 		" />
			<path
				d="M202.138,50.46l-13.903-31.356L174.333,50.46c-0.883,1.991-19.51,44.695-23.469,107.217h74.744
			C221.649,95.155,203.021,52.451,202.138,50.46z"
			/>
			<polygon points="144.542,188.094 144.542,218.51 158.104,218.51 231.929,218.51 231.929,188.094 		" />
			<polygon points="151.49,248.927 135.649,248.927 135.649,492.896 240.822,492.896 240.822,248.927 		" />
			<path
				d="M337.728,50.46l-13.903-31.356L309.923,50.46c-0.883,1.991-19.51,44.695-23.469,107.217h74.744
			C357.237,95.155,338.611,52.451,337.728,50.46z"
			/>
			<polygon points="280.131,188.094 280.131,218.51 299.981,218.51 367.518,218.51 367.518,188.094 		" />
			<polygon points="293.366,248.927 271.239,248.927 271.239,492.896 376.41,492.896 376.41,248.927 		" />
			<path
				d="M473.316,50.46l-13.903-31.356L445.51,50.46c-0.883,1.991-19.51,44.695-23.469,107.217h74.744
			C492.827,95.155,474.2,52.451,473.316,50.46z"
			/>
			<polygon points="415.721,188.094 415.721,218.51 441.859,218.51 503.107,218.51 503.107,188.094 		" />
			<polygon points="435.244,248.927 406.828,248.927 406.828,492.896 512,492.896 512,248.927 		" />
		</g>
	</symbol>,
);

export default class UIAttacksQuantityDescriptor extends React.PureComponent<UIAttacksQuantityDescriptorProps, {}> {
	public static getClass(): string {
		return UIAttacksQuantityDescriptorClass;
	}

	public static getCaption(phases?: number) {
		return phases > 0 ? 'Attacks Per Phase' : 'Attacks Per Turn';
	}

	public static getTooltip(phases?: number) {
		return (
			<span>
				{UIAttacksQuantityDescriptor.getIcon(classnames(UITooltip.getIconClass(), styles.tooltipIcon))}
				<b>{UIAttacksQuantityDescriptor.getCaption(phases)}</b>{' '}
				<Tokenizer
					description={`is a total amount of times a Weapon can perform Attack/Use Action per one ${
						phases > 0 ? 'Action Phase' : 'Turn'
					}. That is equivalent to amount of Attacks the Weapon would perform at one given ${
						phases > 0 ? 'Action Phase' : 'Turn'
					} when it's spent on any other Actions that require a Weapon, incl. Watch, Protect, Intercept etc.`}
				/>
			</span>
		);
	}

	public static getIcon(className?: string) {
		return (
			<SVGSmallTexturedIcon
				style={TSVGSmallTexturedIconStyles.gold}
				className={[UIContainer.iconClass, className || ''].join(' ')}>
				<use {...SVGIconContainer.getXLinkProps(SVGAttackQuantityIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public render() {
		const phases = this.props.phases || 1;
		const attacks = AttackManager.getAttacksQuantityMinMax(this.props.attackRate).map((v) => v / phases);

		const avgAttacks = (attacks[0] + attacks[1]) / 2;
		return (
			<span className={UIAttacksQuantityDescriptor.getClass()}>
				{Number.isFinite(this.props.phases) ? (
					wellRounded(avgAttacks, 3)
				) : (
					<>
						{wellRounded(attacks[0], 2)}
						<UIDistributionIcon
							distribution={getAttackQuantityDistribution(this.props.attackRate)}
							fillColor={getTemplateColor('golden')}
						/>
						{wellRounded(attacks[1], 2)}
					</>
				)}
			</span>
		);
	}
}

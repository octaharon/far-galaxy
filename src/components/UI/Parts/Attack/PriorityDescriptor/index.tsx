import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import AttackManager from '../../../../../../definitions/tactical/damage/AttackManager';
import { ATTACK_PRIORITIES } from '../../../../../../definitions/tactical/damage/constants';
import { ArrowDownUIToken, ArrowUPUIToken } from '../../../../../../definitions/tactical/statuses/constants';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import UIWeaponDescriptor from '../../Weapon/WeaponDescriptor';
import styles from './index.scss';

const UIAttackPriorityClass = styles.priorityDescriptor;

export interface UIAttackPriorityDescriptorProps extends IUICommonProps {
	priority?: number;
	modifier?: IModifier;
	short?: boolean;
}

export const SVGPriorityIconID = 'priority-icon';

SVGIconContainer.registerSymbol(
	SVGPriorityIconID,
	<symbol viewBox="0 0 382.982 382.982">
		<g
			style={{
				transformOrigin: '50% 50%',
				transform: 'scale(0.9) translateY(-5%)',
			}}
		>
			<polygon points="191.491,118.328 257.336,87.897 191.49,0 125.645,87.896" />
			<polygon points="322.867,185.372 270.236,115.116 191.491,151.509 112.745,115.116 60.115,185.372 191.491,251.631" />
			<polygon points="336.252,213.24 191.491,286.249 46.73,213.24 1.491,273.629 191.49,382.982 381.491,273.629" />
		</g>
	</symbol>,
);

export default class UIAttackPriorityDescriptor extends React.PureComponent<UIAttackPriorityDescriptorProps, {}> {
	public static getClass(): string {
		return UIAttackPriorityClass;
	}

	public static getCaption() {
		return 'Attack Priority';
	}

	public static getEntityCaption(content: any = UIAttackPriorityDescriptor.getCaption()) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIAttackPriorityDescriptor.getIcon(UIContainer.iconClass)}
				<span className={styles.priorityCaption}>{content}</span>
			</em>
		);
	}

	public static getTooltip() {
		return (
			<span>
				{UIAttackPriorityDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UIAttackPriorityDescriptor.getCaption()}</b> is a representation of how fast a{' '}
				{UIWeaponDescriptor.getEntityCaption()} can aim and shoot compared to others. Attack with higher
				priority occur slightly earlier, which could be crucial in duels
			</span>
		);
	}

	public static getIcon(className?: string) {
		return (
			<SVGSmallTexturedIcon
				style={TSVGSmallTexturedIconStyles.gray}
				className={[UIContainer.iconClass, className || ''].join(' ')}
			>
				<use {...SVGIconContainer.getXLinkProps(SVGPriorityIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getValue(p: number, modifier?: IModifier) {
		const isPositive =
			!!modifier && BasicMaths.applyModifier(ATTACK_PRIORITIES.NORMAL, modifier) <= ATTACK_PRIORITIES.NORMAL;
		const value = `${
			modifier ? (isPositive ? ArrowUPUIToken : ArrowDownUIToken) : ''
		}${AttackManager.describePriority(p)}`;
		return (
			<span
				className={classnames(styles.value, modifier ? (isPositive ? styles.positive : styles.negative) : '')}
			>
				{value}
			</span>
		);
	}

	public render() {
		const cn = [UIAttackPriorityDescriptor.getClass(), this.props.className || ''];
		const value = UIAttackPriorityDescriptor.getValue(this.props.priority, this.props.modifier);
		const itemClassName = classnames(
			!!this.props.tooltip && UIContainer.hasTooltipClass,
			UIContainer.captionClass,
			UIContainer.descriptorClass,
			styles.priorityCaption,
		);

		return this.props.short ? (
			<span className={classnames(...cn, this.props.tooltip ? UIContainer.hasTooltipClass : null)}>
				{UIAttackPriorityDescriptor.getIcon(UIContainer.iconClass)}
				<span className={styles.value}>{value}</span>
				{!!this.props.tooltip && <UITooltip>{UIAttackPriorityDescriptor.getTooltip()}</UITooltip>}
			</span>
		) : (
			<dl className={classnames(...cn)}>
				<dt className={itemClassName}>
					{UIAttackPriorityDescriptor.getIcon(UIContainer.iconClass)}
					{UIAttackPriorityDescriptor.getCaption()}
					{!!this.props.tooltip && <UITooltip>{UIAttackPriorityDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>{value}</dd>
			</dl>
		);
	}
}

import * as React from 'react';
import _ from 'underscore';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { UIUnitBackdropPatternID, UIUnitBackgroundGradientID } from '../../../../Symbols/backdrops';
import UIRawMaterialsDescriptor from '../../../Economy/RawMaterialsDescriptor';
import UIContainer, { IUICommonProps } from '../../../index';
import UIPrototypeModifier from '../../../Prototype/PrototypeModifier';
import { UIStatusEffectDescriptor } from '../../../Prototype/StatusEffectDescriptor';
import UIBackdrop from '../../../SVG/Backdrop';
import Tokenizer from '../../../Templates/Tokenizer';
import UIAbilityDescriptor from '../../Ability/AbilityDescriptor';
import UIAuraDescriptor from '../../Ability/AuraDescriptor';
import UIEquipmentGroupDescriptor from '../EquipmentGroupDescriptor';
import UIEquipmentSlotDescriptor from '../EquipmentSlotDescriptor';
import styles from './index.scss';

const equipmentDescriptor = styles.equipmentDescriptor;

export type UIEquipmentDescriptorProps = IUICommonProps & {
	equipmentId: number;
	details?: boolean;
};

class UIEquipmentDescriptor extends React.PureComponent<UIEquipmentDescriptorProps, {}> {
	public static getClass(): string {
		return equipmentDescriptor;
	}

	public static getCaption() {
		return UIEquipmentSlotDescriptor.getCaption();
	}

	public render() {
		const provider = DIContainer.getProvider(DIInjectableCollectibles.equipment);
		const equipment = provider.instantiate(null, this.props.equipmentId);
		return (
			<div className={[UIEquipmentDescriptor.getClass(), this.props.className || ''].join(' ')}>
				<UIBackdrop
					className={styles.frame}
					gradientId={UIUnitBackgroundGradientID}
					patternId={UIUnitBackdropPatternID}
				/>
				<div className={[this.props.details ? '' : styles.short, styles.content].join(' ')}>
					<dl className={styles.meta}>
						{this.props.details && (
							<>
								<dt className={UIContainer.descriptorClass}>
									<UIEquipmentGroupDescriptor
										group={provider.getGroupById(this.props.equipmentId)}
										tooltip={true}
									/>
								</dt>
								<dd className={styles.equipmentSlots}>
									<UIEquipmentSlotDescriptor className={styles.equipmentSlotItem} tooltip={true} />
								</dd>
							</>
						)}
						<dt className={styles.title}>
							{equipment.static.caption}{' '}
							{!this.props.details && <UIEquipmentSlotDescriptor className={styles.equipmentSlotItem} />}
						</dt>
						<dd className={styles.cost}>
							<UIRawMaterialsDescriptor
								short={true}
								value={equipment.baseCost}
								className={styles.costDescriptor}
							/>
						</dd>
					</dl>
					{this.props.details && (
						<div className={styles.description}>
							<Tokenizer description={equipment.static.description} />
						</div>
					)}
					{!_.isEmpty(equipment.static.prototypeModifier) && (
						<UIPrototypeModifier modifier={equipment.static.prototypeModifier} />
					)}
					{equipment.static.effects &&
						equipment.static.effects.map((eff) => (
							<UIStatusEffectDescriptor
								className={styles.effectDescriptor}
								key={eff.effectId}
								tooltip={true}
								effect={DIContainer.getProvider(DIInjectableCollectibles.effect).instantiate(
									eff?.props || null,
									eff?.effectId,
								)}
							/>
						))}
					{equipment.static.auras &&
						!!equipment.static.auras.length &&
						equipment.static.auras.map((auraAppliance) => (
							<UIAuraDescriptor
								className={styles.abilityDescriptor}
								tooltip={true}
								key={auraAppliance.id}
								aura={DIContainer.getProvider(DIInjectableCollectibles.aura).instantiate(
									auraAppliance.props || null,
									auraAppliance.id,
								)}
							/>
						))}
					{equipment.static.abilities &&
						!!equipment.static.abilities.length &&
						equipment.static.abilities.map((abilityId) => (
							<UIAbilityDescriptor
								className={styles.abilityDescriptor}
								key={abilityId}
								ability={DIContainer.getProvider(DIInjectableCollectibles.ability).instantiate(
									null,
									abilityId,
								)}
							/>
						))}
				</div>
			</div>
		);
	}
}

export default UIEquipmentDescriptor;

import * as React from 'react';
import { TEquipmentGroup } from '../../../../../../definitions/technologies/equipment/groups';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import styles from './index.scss';
import UIEquipmentSlotDescriptor from '../EquipmentSlotDescriptor';

type UIEquipmentGroupDescriptorProps = IUICommonProps & {
	group: TEquipmentGroup;
};

export const UIEquipmentGroupDescriptor: React.FC<UIEquipmentGroupDescriptorProps> = ({
	className,
	group,
	tooltip = false,
}) => (
	<span
		className={classnames(styles.equipmentGroupDescriptor, tooltip ? UIContainer.hasTooltipClass : null, className)}
	>
		<UIEquipmentSlotDescriptor className={styles.equipmentIcon} tooltip={false} group={group} highlight={true} />
		<span className={UIEquipmentSlotDescriptor.getCaptionClass()}>
			{DIContainer.getProvider(DIInjectableCollectibles.equipment).getGroupMeta(group).caption}
		</span>
		{!!tooltip && <UITooltip>{UIEquipmentSlotDescriptor.getTooltip(group)}</UITooltip>}
	</span>
);

export default UIEquipmentGroupDescriptor;

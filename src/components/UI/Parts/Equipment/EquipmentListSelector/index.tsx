import * as React from 'react';
import {
	doesCollectibleSetIncludeItem,
	findCollectibleItemInSet,
} from '../../../../../../definitions/core/Collectible';
import Players from '../../../../../../definitions/player/types';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import { TEquipmentGroup } from '../../../../../../definitions/technologies/equipment/groups';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UIListSelector from '../../../Templates/ListSelector';
import UITooltip from '../../../Templates/Tooltip';
import UIEquipmentGroupDescriptor from '../EquipmentGroupDescriptor';
import UIEquipmentSlotDescriptor from '../EquipmentSlotDescriptor';
import styles from './index.scss';
import _ from 'underscore';

const equipmentListSelectorClass = styles.equipmentListSelector;

type UIEquipmentListSelectorProps = IUICommonProps & {
	onEquipmentSelected: (equipmentId: number) => any;
	selectedEquipment?: number;
	player?: Players.IPlayer;
};

type IEquipmentHash = Record<TEquipmentGroup, number[]>;

interface UIEquipmentListSelectorState {
	equipmentAvailable: IEquipmentHash;
	equipmentGroup: TEquipmentGroup;
}

export default class UIEquipmentListSelector extends React.PureComponent<
	UIEquipmentListSelectorProps,
	UIEquipmentListSelectorState
> {
	public static getClass(): string {
		return equipmentListSelectorClass;
	}

	protected getEquipmentGroup(selectedEquipment: number = this.props.selectedEquipment): TEquipmentGroup {
		return selectedEquipment
			? Object.keys(this.state.equipmentAvailable).find((k) =>
					doesCollectibleSetIncludeItem(this.props.selectedEquipment, this.state.equipmentAvailable[k]),
			  )
			: Object.keys(this.state.equipmentAvailable)[0] || null;
	}

	protected getSelectedEquipment(): number {
		return doesCollectibleSetIncludeItem(
			this.props.selectedEquipment,
			Object.keys(this.state.equipmentAvailable).reduce(
				(acc, gr) => acc.concat(this.state.equipmentAvailable[gr]),
				[],
			),
		)
			? this.props.selectedEquipment
			: this.state.equipmentAvailable[this.getEquipmentGroup()][0];
	}

	public static getDerivedStateFromProps(props: UIEquipmentListSelectorProps, state: UIEquipmentListSelectorState) {
		return {
			...(state || {}),
			equipmentAvailable: UIEquipmentListSelector.buildEquipmentHash(props),
			equipmentGroup:
				DIContainer.getProvider(DIInjectableCollectibles.equipment).getGroupById(
					props.selectedEquipment || null,
				) || state.equipmentGroup,
		};
	}

	protected static buildEquipmentHash(props: UIEquipmentListSelectorProps) {
		const provider = DIContainer.getProvider(DIInjectableCollectibles.equipment);
		const r = {} as IEquipmentHash;
		Object.keys(provider.getAll()).forEach((equipmentId) => {
			if (!props.player || props.player.isEquipmentAvailable(equipmentId)) {
				if (!r[provider.getGroupById(equipmentId)]) r[provider.getGroupById(equipmentId)] = [];
				r[provider.getGroupById(equipmentId)].push(equipmentId);
			}
		});
		return r;
	}

	public constructor(props: UIEquipmentListSelectorProps) {
		super(props);
		const equipmentAvailable = UIEquipmentListSelector.buildEquipmentHash(props);
		this.state = {
			equipmentAvailable,
			equipmentGroup: DIContainer.getProvider(DIInjectableCollectibles.equipment).getGroupById(
				props.selectedEquipment || null,
			),
		};
	}

	public render() {
		const equipmentGroupCaption = (equipmentGroup: TEquipmentGroup) => (
			<UIEquipmentGroupDescriptor group={equipmentGroup} tooltip={true} />
		);
		const equipmentCaption = (equipmentId: number) => (
			<span className={styles.equipmentCaption}>
				{DIContainer.getProvider(DIInjectableCollectibles.equipment).get(equipmentId).caption}
			</span>
		);
		const onEquipmentSelect = (equipmentId: number) => this.props.onEquipmentSelected(equipmentId);
		return (
			<div className={[UIEquipmentListSelector.getClass(), this.props.className].join(' ')}>
				<UIListSelector
					options={DIContainer.getProvider(DIInjectableCollectibles.equipment).getGroups()}
					selectedItem={this.state.equipmentGroup}
					buttonColor={TSVGSmallTexturedIconStyles.green}
					optionRender={equipmentGroupCaption}
					onSelect={(eqGroup) =>
						this.setState(
							{
								...this.state,
								equipmentGroup: eqGroup,
							},
							() => {
								onEquipmentSelect(this.state.equipmentAvailable[eqGroup][0]);
							},
						)
					}
				/>
				<UIListSelector
					options={this.state.equipmentAvailable[this.getEquipmentGroup()]}
					selectedItem={this.getSelectedEquipment()}
					selectedIndex={findCollectibleItemInSet(
						this.getSelectedEquipment(),
						this.state.equipmentAvailable[this.getEquipmentGroup()],
					)}
					buttonColor={TSVGSmallTexturedIconStyles.green}
					optionRender={equipmentCaption}
					onSelect={onEquipmentSelect}
				/>
			</div>
		);
	}
}

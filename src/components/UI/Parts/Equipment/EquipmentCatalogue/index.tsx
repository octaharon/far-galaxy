import * as React from 'react';
import { doesCollectibleSetIncludeItem } from '../../../../../../definitions/core/Collectible';
import Players from '../../../../../../definitions/player/types';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import DefenseManager from '../../../../../../definitions/tactical/damage/DefenseManager';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { getFactionUnitModifiers } from '../../../../../../definitions/world/factionIndex';
import { classnames, IUICommonProps } from '../../../index';
import UIEquipmentDescriptor from '../EquipmentDescriptor';
import UIEquipmentListSelector from '../EquipmentListSelector';
import styles from './index.scss';
import IPlayer = Players.IPlayer;

const equipmentCatalogueClass = styles.equipmentCatalogue;

type UIEquipmentCatalogueProps = IUICommonProps & {
	player?: IPlayer;
	defaultSelectedEquipment?: number;
	short?: boolean;
	onEquipmentSelected?: (equipmentId: number) => any;
};

export interface UIEquipmentCatalogueState {
	selectedEquipment: number;
}

const getEquipmentId = (props: UIEquipmentCatalogueProps, equipmentId?: number) => {
	const provider = DIContainer.getProvider(DIInjectableCollectibles.equipment);
	let eId = equipmentId ?? props.defaultSelectedEquipment;
	const possibleEquipmentOptions = props.player
		? Object.keys(provider.getAll()).filter((id) => props.player.isEquipmentAvailable(id))
		: Object.keys(provider.getAll());
	if (!doesCollectibleSetIncludeItem(eId, possibleEquipmentOptions)) eId = possibleEquipmentOptions[0];
	return eId;
};

export default class UIEquipmentCatalogue extends React.PureComponent<
	UIEquipmentCatalogueProps,
	UIEquipmentCatalogueState
> {
	public static getClass(): string {
		return equipmentCatalogueClass;
	}

	public static getDerivedStateFromProps(props: UIEquipmentCatalogueProps, state?: UIEquipmentCatalogueState) {
		const equipmentId = getEquipmentId(props, state?.selectedEquipment);
		return {
			...(state || {}),
			selectedEquipment: equipmentId || 10000,
		};
	}

	public constructor(props: UIEquipmentCatalogueProps) {
		super(props);
		this.state = UIEquipmentCatalogue.getDerivedStateFromProps(props);
		this.onEquipmentSelected = this.onEquipmentSelected.bind(this);
	}

	public componentDidMount() {
		if (!this.props.defaultSelectedEquipment && this.props.onEquipmentSelected)
			this.props.onEquipmentSelected(this.state.selectedEquipment);
	}

	public render() {
		return (
			<div className={classnames(UIEquipmentCatalogue.getClass(), this.props.className)}>
				<UIEquipmentListSelector
					selectedEquipment={this.state.selectedEquipment}
					onEquipmentSelected={this.onEquipmentSelected}
					player={this.props.player}
					className={styles.selector}
				/>
				<UIEquipmentDescriptor
					equipmentId={this.state.selectedEquipment}
					className={styles.descriptor}
					details={!this.props.short}
				/>
			</div>
		);
	}

	protected onEquipmentSelected(equipmentId: number) {
		this.setState(
			{
				...this.state,
				selectedEquipment: getEquipmentId(this.props, equipmentId),
			},
			() => {
				if (this.props.onEquipmentSelected) this.props.onEquipmentSelected(equipmentId);
			},
		);
	}
}

import * as React from 'react';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import { IUICommonProps } from '../../../index';
import SVGHexIcon from '../../../SVG/HexIcon/index';
import SVGIconContainer from '../../../SVG/IconContainer';
import UIDamageTypeDescriptor, { DamageWildcardGradientId } from '../DamageTypeDescriptor/index';
import styles from './index.scss';

const UIDamageTypeIconsClass = styles.damageTypeIcons;

export type UIDamageTypeIconsProps = IUICommonProps & {
	damage: Damage.TDamageUnit;
	tooltip?: boolean;
	wildcard?: boolean;
};

export default class UIDamageTypeIcons extends React.PureComponent<UIDamageTypeIconsProps, {}> {
	public static getClass(): string {
		return UIDamageTypeIconsClass;
	}

	public render() {
		const dmg = this.props.damage;
		const keys = Object.keys(dmg).filter(
			(k: Damage.damageType) => Damage.TDamageTypesArray.includes(k) && dmg[k] > 0,
		);
		keys.sort((k1: Damage.damageType, k2: Damage.damageType) => dmg[k1] - dmg[k2]);
		return (
			<span
				className={[
					UIDamageTypeIcons.getClass(),
					UIDamageTypeDescriptor.getClass(keys[keys.length - 1] as Damage.damageType),
					this.props.className || '',
				].join(' ')}
			>
				{this.props.wildcard ? (
					<SVGHexIcon
						className={styles.icon}
						fill={SVGIconContainer.getUrlRef(DamageWildcardGradientId)}
						tooltip={UIDamageTypeDescriptor.getTooltip(null)}
					/>
				) : (
					keys.map((k: Damage.damageType) =>
						dmg[k] > 0 ? UIDamageTypeDescriptor.getIcon(k, styles.icon, true) : null,
					)
				)}
			</span>
		);
	}
}

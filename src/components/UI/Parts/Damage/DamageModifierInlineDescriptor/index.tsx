import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UIDamageDefinitionDescriptor from '../DamageDefinitionDescriptor';
import UIDamageTypeDescriptor from '../DamageTypeDescriptor';
import Styles from './index.scss';

export type UIDamageDefinitionDescriptorProps = IUICommonProps & {
	modifier: IModifier;
	caption?: string;
	damageType?: Damage.damageType;
};

export const damageModifierInlineDescriptorClass = Styles.dmgModifier;

export default class UIDamageModifierInlineDescriptor extends React.Component<UIDamageDefinitionDescriptorProps, {}> {
	public static getCaption() {
		return 'Damage';
	}

	public render() {
		const d = this.props.modifier;
		const description = BasicMaths.describeModifier(d, true);
		const cn = classnames(
			damageModifierInlineDescriptorClass,
			UIDamageTypeDescriptor.getClass(this.props.damageType),
			this.props.tooltip ? UIContainer.hasTooltipClass : '',
			this.props.className || '',
		);
		const WrappingTag: React.FC<React.PropsWithChildren<any>> = ({ children }) =>
			this.props.caption ? <span className={cn}>{children}</span> : <em className={cn}>{children}</em>;
		return (
			<WrappingTag>
				{this.props.modifier && `${description} `}
				{this.props.damageType ? (
					<UIDamageTypeDescriptor damageType={this.props.damageType} tooltip={false} />
				) : (
					UIDamageDefinitionDescriptor.getIcon()
				)}
				<span
					className={
						this.props.damageType
							? UIDamageTypeDescriptor.getClass(this.props.damageType)
							: Styles.damageCaption
					}
				>
					{this.props.caption || UIDamageModifierInlineDescriptor.getCaption()}
				</span>
			</WrappingTag>
		);
	}
}

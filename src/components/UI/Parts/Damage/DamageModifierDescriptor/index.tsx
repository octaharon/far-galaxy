import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIDamageDefinitionDescriptor from '../DamageDefinitionDescriptor';
import UIDamageTypeDescriptor from '../DamageTypeDescriptor';
import Style from './index.scss';

type UIDamageModifierPureProps = {
	modifier?: IModifier;
	type?: Damage.damageType;
};
type UIDamageModifierProps = IUICommonProps &
	UIDamageModifierPureProps & {
		short?: boolean;
		caption?: string;
	};

export default class UIDamageModifierDescriptor extends React.PureComponent<UIDamageModifierProps, {}> {
	public static getIcon(className: string = UIContainer.iconClass) {
		return UIDamageDefinitionDescriptor.getIcon(className);
	}

	public static getCaption() {
		return 'Weapon Damage';
	}

	public static getTooltip() {
		return UIDamageDefinitionDescriptor.getTooltip();
	}

	public render() {
		const { modifier, caption, type } = this.props;
		const isPositive = BasicMaths.applyModifier(100, modifier) >= 100;
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, true).trim()}
			</span>
		);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						Style.damageModifier,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}
				>
					{UIDamageModifierDescriptor.getIcon()}
					{mod}
					{!!this.props.tooltip && <UITooltip>{UIDamageModifierDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(this.props.className || '', Style.damageModifier)}
				onClick={this.props.onClick || null}
			>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}
				>
					{UIDamageModifierDescriptor.getIcon()}
					<span className={Style.caption}>{caption ?? UIDamageModifierDescriptor.getCaption()}</span>
					{!!this.props.tooltip && <UITooltip>{UIDamageModifierDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					{!!this.props.type && <UIDamageTypeDescriptor damageType={this.props.type} full={false} />}
					{mod}
				</dd>
			</dl>
		);
	}
}

import * as React from 'react';
import Distributions from '../../../../../../definitions/maths/Distributions';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import UIDistributionDescriptor from '../../../Distributions/DistributionDescriptor';
import UIDamageTypeDescriptor from '../DamageTypeDescriptor';
import UIDamageValueDescriptor from '../DamageValue';

export type UIDamageItemPlaqueProps = React.PropsWithChildren<{
	damageType?: Damage.damageType;
	distribution?: Distributions.TDistribution;
}>;

export default class UIDamageItemPlaque extends React.PureComponent<UIDamageItemPlaqueProps, {}> {
	public static getClass(t: Damage.damageType = null): string {
		return UIDamageValueDescriptor.getClass();
	}

	public render() {
		return (
			<UIDistributionDescriptor
				distribution={this.props.distribution || null}
				colorClass={`${UIDamageTypeDescriptor.getBgGradientClass(
					this.props.damageType || Damage.damageType.warp,
				)} ${UIDamageTypeDescriptor.getBorderClass(this.props.damageType || Damage.damageType.warp)}`}
				className={UIDamageTypeDescriptor.getClass(this.props.damageType)}>
				<span>{this.props.children}</span>
			</UIDistributionDescriptor>
		);
	}
}

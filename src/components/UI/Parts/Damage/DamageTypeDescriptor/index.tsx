import * as React from 'react';

import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../../index';
import SVGHexIcon from '../../../SVG/HexIcon';
import SVGIconContainer from '../../../SVG/IconContainer';
import UITooltip from '../../../Templates/Tooltip';
import UIHitPointsInlineDescriptor from '../../Chassis/HitPointsInlineDescriptor';
import UITargetTypeDescriptor from '../../Chassis/TargetTypeDescriptor';
import UIShieldDescriptor from '../../Shield/ShieldDescriptor';
import UIDamageModifierInlineDescriptor from '../DamageModifierInlineDescriptor';
import styles from './index.scss';

const damageColorMapping: EnumProxy<Damage.damageType, string> = {
	[Damage.damageType.warp]: 'warp',
	[Damage.damageType.radiation]: 'radiation',
	[Damage.damageType.magnetic]: 'magnetic',
	[Damage.damageType.heat]: 'heat',
	[Damage.damageType.biological]: 'molecular',
	[Damage.damageType.antimatter]: 'antimatter',
	[Damage.damageType.thermodynamic]: 'pressure',
	[Damage.damageType.kinetic]: 'projectile',
};

const damageWebColors: EnumProxy<Damage.damageType, string> = Object.keys(damageColorMapping).reduce(
	(a, k: Damage.damageType) => ({
		...a,
		[k]: styles[`color-${damageColorMapping[k]}`],
	}),
	{},
);

const damageWebGradients: EnumProxy<Damage.damageType, { top: string; bottom: string }> = Object.keys(
	damageColorMapping,
).reduce(
	(a, k: Damage.damageType) => ({
		...a,
		[k]: {
			top: styles[`color-${damageColorMapping[k]}-top`],
			bottom: styles[`color-${damageColorMapping[k]}-bottom`],
		},
	}),
	{},
);

export type UIDamageTypeDescriptorProps = IUICommonProps & {
	damageType: Damage.damageType;
	full?: boolean;
};

export const DamageWildcardGradientId = 'damage-wildcard-gradient';

const getDamageHighlightedHeader = (t: Damage.damageType) => (
	<b className={UIDamageTypeDescriptor.getClass(t)}>
		{UIDamageTypeDescriptor.getCaption(t)} ({UIDamageTypeDescriptor.getAbbr(t)})
	</b>
);

export default class UIDamageTypeDescriptor extends React.PureComponent<UIDamageTypeDescriptorProps, {}> {
	public static getClass(t: Damage.damageType): string {
		if (t === null) return styles.dmg_wildcard;
		return styles[`dmg-fg-${damageColorMapping[t]}`];
	}

	public static getSVGGradientId(t: Damage.damageType): string {
		return 'damage-gradient-' + t;
	}

	public static getColor(t: Damage.damageType): string {
		return damageWebColors[t];
	}

	public static getBgClass(t: Damage.damageType): string {
		return styles[`dmg-bg-${damageColorMapping[t]}`];
	}

	public static getBorderClass(t: Damage.damageType): string {
		return styles[`dmg-border-${damageColorMapping[t]}`];
	}

	public static getBgGradientClass(t: Damage.damageType): string {
		return styles[`dmg-bg-gradient-${damageColorMapping[t]}`];
	}

	public static getCaption(t: Damage.damageType): string {
		if (t === null) return 'Any Damage';
		const dictionary = {
			[Damage.damageType.warp]: 'Warp',
			[Damage.damageType.radiation]: 'Radiation',
			[Damage.damageType.magnetic]: 'Electromagnetic',
			[Damage.damageType.heat]: 'Heat',
			[Damage.damageType.biological]: 'Biological',
			[Damage.damageType.antimatter]: 'Annihilation',
			[Damage.damageType.thermodynamic]: 'Thermodynamic',
			[Damage.damageType.kinetic]: 'Kinetic',
		};
		return dictionary[t];
	}

	public static getAbbr(t: Damage.damageType): string {
		return Damage.getShortCaption(t);
	}

	public static getIcon(t: Damage.damageType, className: string = UIContainer.iconClass, tooltip = false) {
		if (t === null)
			return (
				<SVGHexIcon
					className={className}
					fill={SVGIconContainer.getUrlRef(DamageWildcardGradientId)}
					tooltip={tooltip ? UIDamageTypeDescriptor.getTooltip(null) : ''}
				/>
			);
		return (
			<SVGHexIcon
				className={className}
				colorTop={UIDamageTypeDescriptor.getGradientColorTop(t)}
				colorBottom={UIDamageTypeDescriptor.getGradientColorBottom(t)}
				tooltip={tooltip ? UIDamageTypeDescriptor.getTooltip(t) : ''}
				key={t}
			>
				<circle fill={UIDamageTypeDescriptor.getColor(t)} cx="50%" cy="50%" r={5} />
			</SVGHexIcon>
		);
	}

	public static getTooltip(t: Damage.damageType) {
		switch (t) {
			case Damage.damageType.radiation:
				return (
					<span>
						{getDamageHighlightedHeader(t)} damage includes alpha-, beta- and gamma rays, dealing
						<UIDamageModifierInlineDescriptor modifier={{ factor: 1.25 }} /> to{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.organic)} and{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.robotic)} targets'{' '}
						<UIHitPointsInlineDescriptor />
					</span>
				);
			case Damage.damageType.magnetic:
				return (
					<span>
						{getDamageHighlightedHeader(t)} damage derives from high energy photon streams causing intense
						electrical discharges in conducting materials, dealing{' '}
						<UIDamageModifierInlineDescriptor modifier={{ factor: 1.25 }} /> to{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.massive)} and{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.robotic)} targets'{' '}
						<UIHitPointsInlineDescriptor />
					</span>
				);
			case Damage.damageType.thermodynamic:
				return (
					<span>
						{getDamageHighlightedHeader(t)} damage comes from pressure effects and impact waves, dealing{' '}
						<UIDamageModifierInlineDescriptor modifier={{ factor: 1.25 }} /> to{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.massive)} and{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.organic)} targets'{' '}
						<UIHitPointsInlineDescriptor />
					</span>
				);
			case Damage.damageType.biological:
				return (
					<span>
						{getDamageHighlightedHeader(t)} damage is chemical by nature, involving acids, solvents and
						other aggressive substances, dealing{' '}
						<UIDamageModifierInlineDescriptor modifier={{ factor: 1.5 }} /> to{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.organic)} targets'{' '}
						<UIHitPointsInlineDescriptor />
					</span>
				);
			case Damage.damageType.warp:
				return (
					<span>
						{getDamageHighlightedHeader(t)} damage occurs whenever gravity noise waves or negative mass
						concentration is created, dealing{' '}
						<UIDamageModifierInlineDescriptor modifier={{ factor: 1.5 }} /> to{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.massive)} targets'{' '}
						<UIHitPointsInlineDescriptor />
					</span>
				);
			case Damage.damageType.antimatter:
				return (
					<span>
						{getDamageHighlightedHeader(t)} damage derives from intense meson streams, carrying humongous
						energy dissipated by mutual conversion of matter and antimatter. It deals{' '}
						<UIDamageModifierInlineDescriptor modifier={{ factor: 1.25 }} /> to target's{' '}
						{UIShieldDescriptor.getEntityCaption()} and additionally{' '}
						<UIDamageModifierInlineDescriptor modifier={{ factor: 1.5 }} /> to{' '}
						<UIHitPointsInlineDescriptor />, if the target is{' '}
						{UITargetTypeDescriptor.getEntityCaption(UnitChassis.targetType.unique)}
					</span>
				);
			case Damage.damageType.heat:
				return (
					<span>
						{getDamageHighlightedHeader(t)} damage happens when something burns, melts or gets warmed up by
						particle rays, and deals <UIDamageModifierInlineDescriptor modifier={{ factor: 1.25 }} /> to
						targets that are below <UIHitPointsInlineDescriptor value={'50%'} />
					</span>
				);
			case Damage.damageType.kinetic:
				return (
					<span>
						{getDamageHighlightedHeader(t)} damage emerges from mechanical collision with the target that
						leads to major lesions in the structure. Kinetic projectiles do{' '}
						<UIDamageModifierInlineDescriptor modifier={{ factor: 1.5 }} /> to target's{' '}
						{UIShieldDescriptor.getEntityCaption()}
					</span>
				);
			default:
				return (
					<span>
						<b>Any damage</b> is relevant to all <UIDamageModifierInlineDescriptor modifier={null} />{' '}
						sources
					</span>
				);
		}
	}

	public static getGradientColorTop(t: Damage.damageType): string {
		return damageWebGradients[t]?.top || '#777';
	}

	public static getGradientColorBottom(t: Damage.damageType): string {
		return damageWebGradients[t]?.bottom || '#FFF';
	}

	public render() {
		return (
			<span
				className={[
					styles.damageTypeDescriptor,
					this.props.className || '',
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
					UIContainer.descriptorClass,
				].join(' ')}
			>
				{this.props.full && UIDamageTypeDescriptor.getIcon(this.props.damageType)}
				<span className={classnames(styles.dmgType, UIDamageTypeDescriptor.getClass(this.props.damageType))}>
					{this.props.full ? (
						UIDamageTypeDescriptor.getCaption(this.props.damageType)
					) : (
						<small>{UIDamageTypeDescriptor.getAbbr(this.props.damageType)}</small>
					)}
				</span>
				{!!this.props.tooltip && (
					<UITooltip>{UIDamageTypeDescriptor.getTooltip(this.props.damageType)}</UITooltip>
				)}
			</span>
		);
	}
}

Object.keys(damageColorMapping).forEach((t: Damage.damageType) => {
	SVGIconContainer.registerGradient(
		UIDamageTypeDescriptor.getSVGGradientId(t),
		<linearGradient x1="50%" y1="0" x2="50%" y2="100%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={UIDamageTypeDescriptor.getGradientColorTop(t)} />
			<stop offset="1" stopColor={UIDamageTypeDescriptor.getGradientColorBottom(t)} />
		</linearGradient>,
	);
});

SVGIconContainer.registerGradient(
	DamageWildcardGradientId,
	<radialGradient cx="50%" cy="50%" r="47%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor={getTemplateColor('text_highlight')} />
		<stop offset="0.6" stopColor={getTemplateColor('light_blue')} />
		<stop offset="1" stopColor={getTemplateColor('light_green')} />
	</radialGradient>,
);

import * as React from 'react';
import { IWeapon } from '../../../../../../definitions/technologies/types/weapons';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIDamageDefinitionDescriptor from '../DamageDefinitionDescriptor';
import UIDamagePerTurnBar from '../../Attack/DamagePerTurnBar';
import styles from './index.scss';

const UIDPTDescriptorClass = styles.dptDescriptor;

export type UIDPTDescriptorProps = IUICommonProps & {
	weapon: IWeapon;
	phases?: number;
	short?: boolean;
};

export default class UIDPTDescriptor extends React.PureComponent<UIDPTDescriptorProps, {}> {
	public static getClass(): string {
		return UIDPTDescriptorClass;
	}

	public static getCaption(phases?: number) {
		return phases > 0 ? 'Max Damage Per Phase' : 'Damage Per Turn';
	}

	public render() {
		const classNames = [UIDPTDescriptor.getClass(), this.props.className || ''];
		const itemClassName = classnames(
			!!this.props.tooltip && UIContainer.hasTooltipClass,
			UIContainer.captionClass,
			UIContainer.descriptorClass,
			styles.dptCaption,
		);
		const value = (
			<UIDamagePerTurnBar weapon={this.props.weapon} phases={this.props.phases} tooltip={!!this.props.tooltip} />
		);
		return this.props.short ? (
			<span className={classnames(...classNames, this.props.tooltip ? UIContainer.hasTooltipClass : '')}>
				{value}
				{!!this.props.tooltip && <UITooltip>{UIDamageDefinitionDescriptor.getTooltip()}</UITooltip>}
			</span>
		) : (
			<dl className={classnames(...classNames)}>
				<dt className={itemClassName}>
					{UIDamageDefinitionDescriptor.getIcon(UIContainer.iconClass)}
					{UIDPTDescriptor.getCaption(this.props.phases)}
					{!!this.props.tooltip && <UITooltip>{UIDamageDefinitionDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>{value}</dd>
			</dl>
		);
	}
}

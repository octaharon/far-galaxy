import * as React from 'react';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import predicateComparator from '../../../../../utils/predicateComparator';
import { classnames, IUICommonProps } from '../../../index';
import UIDamageDefinitionDescriptor from '../DamageDefinitionDescriptor';
import UIDamageTypeDescriptor from '../DamageTypeDescriptor';
import styles from './index.scss';

export const UIDamageCollectionDescriptor: React.FC<IUICommonProps & { attack: Damage.TDamageDefinition }> = ({
	className,
	attack,
	tooltip,
}) => (
	<dl className={classnames(styles.attackDefinitionDescriptor, className)}>
		{Object.keys(attack)
			.filter((t) => Damage.TDamageTypesArray.includes(t))
			.sort(
				predicateComparator(
					Object.values(Damage.damageType).map((k) => (t) => t === k),
					String,
				),
			)
			.map((t) => (
				<React.Fragment key={t}>
					<dt>
						<UIDamageTypeDescriptor damageType={t} full={true} tooltip={tooltip} />
					</dt>
					<dd>
						<UIDamageDefinitionDescriptor
							damageFactor={attack[t]}
							damageType={t}
							tooltip={tooltip}
							withRange={true}
							withType={false}
						/>
					</dd>
				</React.Fragment>
			))}
	</dl>
);

export default UIDamageDefinitionDescriptor;

import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import { getDistributionAverage, getDistributionSpectrum } from '../../../../../../definitions/maths/Distributions';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import UIDistributionDescriptor from '../../../Distributions/DistributionDescriptor';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../../index';
import UIDistributionIcon from '../../../SVG/DistributionIcon';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import UIArmorDescriptor from '../../Armor/ArmorDescriptor';
import UIShieldDescriptor from '../../Shield/ShieldDescriptor';
import { SVGWeaponIconID } from '../../Weapon/WeaponSlotDescriptor';
import UIDamageModifierInlineDescriptor from '../DamageModifierInlineDescriptor';
import UIDamageTypeDescriptor from '../DamageTypeDescriptor';
import UIDamageValueDescriptor from '../DamageValue';
import Styles from './index.scss';

export type UIDamageDefinitionDescriptorProps = IUICommonProps & {
	damageFactor: Damage.IDamageFactor;
	damageType: Damage.damageType;
	withRange?: boolean;
	withType?: boolean;
};

export const damageDescriptorClass = Styles.dmgDescription;

export const SVGDamageIconID = 'damage-descriptor-icon';

SVGIconContainer.registerSymbol(
	SVGDamageIconID,
	<symbol viewBox="0 0 512 512">
		<g
			style={{
				transformOrigin: '50% 50%',
				transform: 'scale(1.05, 1) translate(10%, 0)',
			}}
		>
			<path d="m246.546875 0h-87.117187c-87.910157 0-159.429688 71.519531-159.429688 159.429688v157.074218c0 17.175782 9.503906 32.6875 24.804688 40.484375l29.414062 15c5.199219 2.648438 8.429688 7.921875 8.429688 13.757813v111.253906c0 8.285156 6.714843 15 15 15h50.667968v-72.5625c0-8.285156 6.714844-15 15-15s15 6.714844 15 15v72.5625h29.671875v-72.5625c0-8.285156 6.714844-15 15-15 8.28125 0 15 6.714844 15 15v72.5625h29.667969v-72.5625c0-8.285156 6.71875-15 15-15 8.285156 0 15 6.714844 15 15v72.5625h50.667969c8.285156 0 15-6.714844 15-15v-111.253906c0-5.835938 3.230469-11.105469 8.433593-13.757813l29.414063-15c15.300781-7.796875 24.804687-23.3125 24.804687-40.484375v-157.074218c-.003906-87.910157-71.523437-159.429688-159.429687-159.429688zm-136.957031 262.464844h-28.796875c-8.285157 0-15-6.71875-15-15v-45.429688c0-8.28125 6.714843-15 15-15h73.796875c8.285156 0 15 6.71875 15 15v.429688c0 33.136718-26.863282 60-60 60zm93.398437 107.546875h-23.402343c-11.769532 0-19.125-12.746094-13.242188-22.9375l11.703125-20.265625 11.699219-20.265625c5.882812-10.191407 20.597656-10.191407 26.484375 0l23.398437 40.53125c5.886719 10.195312-1.46875 22.9375-13.242187 22.9375zm33.394531-167.546875v-.429688c0-8.28125 6.714844-15 15-15h73.800782c8.28125 0 15 6.71875 15 15v45.429688c0 8.28125-6.71875 15-15 15h-28.800782c-33.136718 0-60-26.863282-60-60zm0 0" />
		</g>
	</symbol>,
);

const damageRounded = (v: number) => BasicMaths.roundToPrecision(v, 0.2, 1);

export default class UIDamageDefinitionDescriptor extends React.Component<UIDamageDefinitionDescriptorProps, {}> {
	public static getCaption() {
		return 'Damage';
	}

	public static getTooltip() {
		return (
			<span>
				{UIDamageDefinitionDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UIDamageDefinitionDescriptor.getCaption()}</b> is a core concept of the combat. <em>Damage</em>{' '}
				eventually brings down the <em>Hit Points</em> of a unit until it is destroyed, but it can be absorbed
				by {UIShieldDescriptor.getEntityCaption()} and reduced by {UIArmorDescriptor.getEntityCaption()}
			</span>
		);
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				<UIDamageModifierInlineDescriptor modifier={null} />
			</em>
		);
	}

	public static getDetailedTooltip(type: Damage.damageType, damageFactor: Damage.IDamageFactor) {
		return (
			<span>
				<UIDistributionIcon
					distribution={damageFactor.distribution}
					className={UIContainer.tooltipIconClass}
					stroke={true}
					fillColor={getTemplateColor('text_main')}
				/>
				<b>{UIDistributionDescriptor.getCaption(damageFactor.distribution)}</b>
				<p>{UIDistributionDescriptor.getDescription(damageFactor.distribution)}</p>
				<hr />
				{getDistributionSpectrum(damageFactor).map((v, ix) => (
					<section key={ix} style={{ whiteSpace: 'nowrap' }}>
						<em>{Math.round(v.x * 100)}%</em> chance:{' '}
						<UIDamageValueDescriptor damageType={type}>
							{damageRounded(v.min)} &mdash; {damageRounded(v.max)} {UIDamageTypeDescriptor.getAbbr(type)}{' '}
							Damage
						</UIDamageValueDescriptor>{' '}
					</section>
				))}
			</span>
		);
	}

	public static getIcon(className?: string) {
		return (
			<SVGSmallTexturedIcon
				asSvg={true}
				style={TSVGSmallTexturedIconStyles.gold}
				className={[UIContainer.iconClass, className || ''].join(' ')}
			>
				<use {...SVGIconContainer.getXLinkProps(SVGWeaponIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public render() {
		const d = this.props.damageFactor;
		const damageAvg = BasicMaths.roundToPrecision(getDistributionAverage(d), 0.1);

		return (
			<div
				className={classnames(
					damageDescriptorClass,
					UIDamageTypeDescriptor.getClass(this.props.damageType),
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
					this.props.className || '',
				)}
			>
				{(this.props.withType || false) && (
					<UIDamageTypeDescriptor
						damageType={this.props.damageType}
						tooltip={false}
						className={Styles.dmgType}
					/>
				)}
				<UIDamageValueDescriptor damageType={this.props.damageType}>
					{(this.props.withRange || false) && (
						<span className={Styles.range}>
							{damageRounded(this.props.damageFactor.min)}
							<UIDistributionIcon
								distribution={d.distribution}
								fillColor={UIDamageTypeDescriptor.getColor(this.props.damageType)}
							/>
							{damageRounded(this.props.damageFactor.max)}
						</span>
					)}
					{this.props.children}
				</UIDamageValueDescriptor>
				{!!this.props.tooltip && (
					<UITooltip>
						{UIDamageDefinitionDescriptor.getDetailedTooltip(
							this.props.damageType,
							this.props.damageFactor,
						)}
					</UITooltip>
				)}
			</div>
		);
	}
}

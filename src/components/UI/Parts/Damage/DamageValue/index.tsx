import * as React from 'react';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import { classnames } from '../../../index';
import UIDamageTypeDescriptor from '../DamageTypeDescriptor';
import styles from './index.scss';

const damageValueClass = styles.dmgValue;

export type UIDamageValueDescriptorProps = React.PropsWithChildren<{
	damageType?: Damage.damageType;
}>;

export default class UIDamageValueDescriptor extends React.PureComponent<UIDamageValueDescriptorProps, {}> {
	public static getClass(t: Damage.damageType = null): string {
		return classnames(damageValueClass, UIDamageTypeDescriptor.getClass(t));
	}

	public render() {
		return <span className={UIDamageValueDescriptor.getClass(this.props.damageType)}>{this.props.children}</span>;
	}
}

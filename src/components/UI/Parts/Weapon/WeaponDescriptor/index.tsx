import * as React from 'react';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import AttackManager from '../../../../../../definitions/tactical/damage/AttackManager';
import Weapons, { IWeapon } from '../../../../../../definitions/technologies/types/weapons';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import predicateComparator from '../../../../../utils/predicateComparator';
import {
	UIPrototypeBackdropPatternID,
	UIPrototypeBackgroundGradientID,
	UIUnitBackdropPatternID,
	UIUnitBackgroundGradientID,
} from '../../../../Symbols/backdrops';
import UIContainer, { IUICommonProps } from '../../../index';
import UIRawMaterialsDescriptor from '../../../Economy/RawMaterialsDescriptor';
import UIBackdrop from '../../../SVG/Backdrop';
import UIAttackDescriptor from '../../Attack/AttackDescriptor';
import UIWeaponGroupDescriptor from '../WeaponGroupDescriptor';
import UIAttackMetaDescriptor from '../WeaponMetaDescriptor';
import UIWeaponSlotDescriptor from '../WeaponSlotDescriptor';
import styles from './index.scss';

const weaponDescriptorClass = styles.weaponDescriptor;

export type UIWeaponDescriptorProps = IUICommonProps & {
	weaponId: number;
	currentSlot?: Weapons.TWeaponSlotType;
	phases?: number;
	modifiers?: Weapons.TWeaponModifier[];
	details?: boolean;
};

export interface UIWeaponDescriptorState {
	weapon?: IWeapon;
	currentSlot?: Weapons.TWeaponSlotType;
}

const attackOrderComparator = predicateComparator<UnitAttack.TAttackDefinition>(
	[
		(t) => AttackManager.isDirectAttack(t) && !AttackManager.isDeferredAttack(t) && !AttackManager.isDOTAttack(t),
		(t) => AttackManager.isAOEAttack(t) && !AttackManager.isDeferredAttack(t) && !AttackManager.isDOTAttack(t),
		AttackManager.isDOTAttack,
		AttackManager.isDeferredAttack,
	],
	(t) => t.delivery !== UnitAttack.deliveryType.immediate,
);

export default class UIWeaponDescriptor extends React.PureComponent<UIWeaponDescriptorProps, UIWeaponDescriptorState> {
	public constructor(props: UIWeaponDescriptorProps) {
		const { modifiers = [], currentSlot = null, weaponId = null } = props;
		super(props);
		const w = DIContainer.getProvider(DIInjectableCollectibles.weapon).instantiate(null, weaponId);
		if (this.props.currentSlot) {
			this.state = {
				currentSlot,
				weapon: UIWeaponDescriptor.applyModifiers(w, currentSlot, ...modifiers),
			};
		} else {
			const cSlot = DIContainer.getProvider(DIInjectableCollectibles.weapon).getItemMeta(weaponId).slots[0];
			this.state = {
				weapon: UIWeaponDescriptor.applyModifiers(w, cSlot, ...modifiers),
				currentSlot: cSlot,
			};
		}
	}

	public static getClass(): string {
		return weaponDescriptorClass;
	}

	public static getCaption(plural = false): string {
		return plural ? `Weapons` : 'Weapon';
	}

	public static getEntityCaption(plural = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				<UIWeaponSlotDescriptor className={styles.weaponSlotIcon} highlight={true} slotType={null} />
				<span className={styles.weaponCaption}>{UIWeaponDescriptor.getCaption(plural)}</span>
			</em>
		);
	}

	public static getDerivedStateFromProps(
		newProps: UIWeaponDescriptorProps,
		oldState: UIWeaponDescriptorState,
	): UIWeaponDescriptorState {
		const newState: UIWeaponDescriptorState = Object.assign({}, oldState || {});
		if (
			newProps.weaponId !== oldState.weapon.static.id ||
			(newProps.currentSlot && newProps.currentSlot !== oldState.currentSlot)
		) {
			const w = DIContainer.getProvider(DIInjectableCollectibles.weapon).instantiate(null, newProps.weaponId);
			if (newProps.currentSlot) {
				newState.currentSlot = newProps.currentSlot;
				newState.weapon = UIWeaponDescriptor.applyModifiers(w, newProps.currentSlot, ...newProps.modifiers);
			} else {
				const wMeta = DIContainer.getProvider(DIInjectableCollectibles.weapon).getItemMeta(newProps.weaponId);
				newState.currentSlot = wMeta.slots.includes(oldState.currentSlot)
					? oldState.currentSlot
					: wMeta.slots[0];
				newState.weapon = UIWeaponDescriptor.applyModifiers(w, newState.currentSlot, ...newProps.modifiers);
			}
		}
		return newState;
	}

	protected static applyModifiers(w: IWeapon, slot: Weapons.TWeaponSlotType, ...m: Weapons.TWeaponModifier[]) {
		return w.applySlotModifier(slot).applyModifier(...m);
	}

	public render() {
		return (
			<div className={[UIWeaponDescriptor.getClass(), this.props.className || ''].join(' ')}>
				<UIBackdrop
					className={styles.frame}
					gradientId={UIPrototypeBackgroundGradientID}
					patternId={UIPrototypeBackdropPatternID}
				/>
				<div className={[this.props.details ? '' : styles.short, styles.content].join(' ')}>
					<dl className={styles.meta}>
						{this.props.details && (
							<>
								<dt className={styles.group}>
									<UIWeaponGroupDescriptor
										group={this.state.weapon.meta.group}
										tooltip={!!this.props.tooltip}
										className={UIContainer.descriptorClass}
									/>
								</dt>
								<dd className={styles.weaponSlots}>
									{this.props.currentSlot ? (
										<UIWeaponSlotDescriptor
											slotType={this.props.currentSlot}
											key={this.props.currentSlot}
											highlight={true}
											tooltip={!!this.props.tooltip}
											className={styles.weaponSlotItem}
										/>
									) : (
										DIContainer.getProvider(DIInjectableCollectibles.weapon)
											.getItemMeta(this.state.weapon.static.id)
											.slots.map((slotType) => (
												<UIWeaponSlotDescriptor
													slotType={slotType}
													onClick={
														this.state.currentSlot === slotType
															? null
															: this.getWeaponSlotClickHandler(slotType)
													}
													key={slotType}
													highlight={this.state.currentSlot === slotType}
													tooltip={!!this.props.tooltip}
													className={styles.weaponSlotItem}
												/>
											))
									)}
								</dd>
							</>
						)}
						<dt className={styles.title}>
							{this.props.details
								? ''
								: UIWeaponGroupDescriptor.getIcon(
										this.state.weapon.meta.group.id,
										styles.groupIcon,
										UIWeaponGroupDescriptor.getTooltip(this.state.weapon.meta.group.id),
								  )}
							{this.state.weapon.caption}
							<UIWeaponSlotDescriptor
								slotType={this.props.currentSlot || this.state.currentSlot}
								highlight={true}
								tooltip={!!this.props.tooltip}
								className={styles.weaponSlotItem}
							/>
						</dt>
						<dd className={styles.cost}>
							<UIRawMaterialsDescriptor
								short={true}
								tooltip={!!this.props.tooltip}
								value={this.state.weapon.baseCost}
								className={styles.costDescriptor}
							/>
						</dd>
					</dl>
					{this.props.details && <div className={styles.description}>{this.state.weapon.description}</div>}
					<div>
						<UIAttackMetaDescriptor
							weapon={this.state.weapon}
							details={this.props.details || false}
							phases={this.props.phases}
						/>
						<div>
							{this.state.weapon.attacks.sort(attackOrderComparator).map((a, ix) => (
								<UIAttackDescriptor
									key={ix}
									tooltip={!!this.props.tooltip}
									attack={a}
									details={this.props.details || false}
								/>
							))}
						</div>
					</div>
				</div>
			</div>
		);
	}

	protected switchWeaponSlot(slot: Weapons.TWeaponSlotType) {
		this.setState({
			...this.state,
			weapon: UIWeaponDescriptor.applyModifiers(
				DIContainer.getProvider(DIInjectableCollectibles.weapon).instantiate(null, this.props.weaponId),
				slot,
				...this.props.modifiers,
			),
			currentSlot: slot,
		});
	}

	protected getWeaponSlotClickHandler(slot: Weapons.TWeaponSlotType) {
		return (e: React.MouseEvent) => this.switchWeaponSlot(slot);
	}
}

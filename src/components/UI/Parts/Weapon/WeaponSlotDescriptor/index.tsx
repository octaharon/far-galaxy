import * as React from 'react';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import Weapons from '../../../../../../definitions/technologies/types/weapons';
import WeaponSlots from '../../../../../../definitions/technologies/weapons/weaponSlots';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../../index';
import SVGHexIcon from '../../../SVG/HexIcon';
import SVGIconContainer, { SVGBasicNoiseFilterID } from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import UIAttackDescriptor from '../../Attack/AttackDescriptor';
import UIWeaponDescriptor from '../WeaponDescriptor';
import UIWeaponGroupDescriptor, { UIWeaponGroupIconMapping } from '../WeaponGroupDescriptor';
import Style from './index.scss';

export type UIWeaponSlotDescriptorProps = IUICommonProps & {
	slotType: Weapons.TWeaponSlotType;
	highlight?: boolean;
	weaponGroup?: Weapons.TWeaponGroupType;
};

export const SVGWeaponIconID = 'weapon-icon';

SVGIconContainer.registerSymbol(
	SVGWeaponIconID,
	<symbol viewBox="0 0 612 612">
		<path
			d="M597.813,317.4l-123.576,9.042v-107.62c0-10.507-11.686-16.795-20.454-11.008l-76.698,50.621l-36.728-190.03
			c-2.449-12.671-19.76-14.647-25.002-2.853l-76.227,171.509l-119.781-83.532c-9.877-6.887-23.059,1.974-20.411,13.719
			l37.211,165.022L35.566,353.935c-10.612,2.286-14.074,15.696-5.894,22.832l71.499,62.372l-64.12,50.519h120.388l34.12-26.882
			l-48.706-42.488l67.359-14.508l-23.834-105.702l78.758,54.924l49.742-111.92l23.834,123.319l51.815-34.198v70.468l84.977-6.218
			l-71.505,70.468l36.173,22.737h127.332l-68.007-42.747l108.536-106.964C616.772,331.337,610.049,316.505,597.813,317.4z"
		/>
		<path
			d="M16.486,554.284h573.642c9.105,0,16.486-7.381,16.486-16.487v-6.595c0-9.105-7.381-16.486-16.486-16.486H16.486
			C7.381,514.717,0,522.098,0,531.203v6.595C0,546.903,7.381,554.284,16.486,554.284z"
		/>
	</symbol>,
);

export default class UIWeaponSlotDescriptor extends React.PureComponent<UIWeaponSlotDescriptorProps, {}> {
	public static getSlotDescription(slot: Weapons.TWeaponSlotType) {
		const getTextPart = (t: Weapons.TWeaponSlotType) => {
			switch (t) {
				case Weapons.TWeaponSlotType.small:
					return (
						<>
							{' '}
							possesses minimal range and damage and is carried by infantry or mounted to light vehicles.
							Some heavy units can also mount it for a supplementary firepower.{' '}
							{UIAttackDescriptor.getEntityCaption({
								[UnitAttack.attackFlags.AoE]: true,
								aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
								magnitude: 1,
								radius: 1,
								delivery: UnitAttack.deliveryType.immediate,
							})}{' '}
							attacks have their effect radius decreased by <em>50%</em>
						</>
					);
				case Weapons.TWeaponSlotType.large:
					return (
						<>
							{' '}
							requires some engineering work that makes a weapon shoot further and hit harder, despite
							with less precision.{' '}
							{UIAttackDescriptor.getEntityCaption({
								[UnitAttack.attackFlags.AoE]: true,
								aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
								magnitude: 1,
								radius: 1,
								delivery: UnitAttack.deliveryType.immediate,
							})}{' '}
							attacks have their effect radius increased by <em>50%</em>
						</>
					);
				case Weapons.TWeaponSlotType.extra:
					return (
						<>
							{' '}
							fits only the most powerful weapons and offers tremendous range and damage for a heavy
							price, as well as increasing the radius of{' '}
							{UIAttackDescriptor.getEntityCaption({
								[UnitAttack.attackFlags.AoE]: true,
								aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
								magnitude: 1,
								radius: 1,
								delivery: UnitAttack.deliveryType.immediate,
							})}{' '}
							attacks by <em>100%</em>. The reload time reduces the rate of fire drastically
						</>
					);
				case Weapons.TWeaponSlotType.air:
					return (
						<>
							{' '}
							can custom-fit selected weapons, limiting their range but slightly increasing the damage,
							and this slot is required to fit{' '}
							<UIWeaponGroupDescriptor
								iconClassName={UIContainer.iconClass}
								group={DIContainer.getProvider(DIInjectableCollectibles.weapon).getGroupMeta(
									Weapons.TWeaponGroupType.Bombs,
								)}
							/>
							. Attacks from air ignore line of sight rules.
						</>
					);
				case Weapons.TWeaponSlotType.naval:
					return (
						<>
							{' '}
							improve weapons' range and damage, while making it fast to aim but slow to reload.{' '}
							{UIAttackDescriptor.getEntityCaption({
								[UnitAttack.attackFlags.AoE]: true,
								aoeType: UnitAttack.TAoEDamageSplashTypes.circle,
								magnitude: 1,
								radius: 1,
								delivery: UnitAttack.deliveryType.immediate,
							})}{' '}
							attacks also have their radius increased by <em>1</em>
						</>
					);
				case Weapons.TWeaponSlotType.hangar:
					return (
						<>
							{' '}
							is a special module that can't fit conventional weapons but can assembly{' '}
							<UIWeaponGroupDescriptor
								iconClassName={UIContainer.iconClass}
								group={DIContainer.getProvider(DIInjectableCollectibles.weapon).getGroupMeta(
									Weapons.TWeaponGroupType.Drone,
								)}
							/>
							. Also can be modified to accommodate bigger versions of{' '}
							<UIWeaponGroupDescriptor
								iconClassName={UIContainer.iconClass}
								group={DIContainer.getProvider(DIInjectableCollectibles.weapon).getGroupMeta(
									Weapons.TWeaponGroupType.Bombs,
								)}
							/>{' '}
							and deliver them with bomber drones
						</>
					);
				case Weapons.TWeaponSlotType.medium:
					return (
						<>
							{' '}
							is a typical weapon mount for all kinds of vehicles. Supports most weapons, except for{' '}
							<UIWeaponGroupDescriptor
								iconClassName={UIContainer.iconClass}
								group={DIContainer.getProvider(DIInjectableCollectibles.weapon).getGroupMeta(
									Weapons.TWeaponGroupType.Bombs,
								)}
							/>{' '}
							and{' '}
							<UIWeaponGroupDescriptor
								iconClassName={UIContainer.iconClass}
								group={DIContainer.getProvider(DIInjectableCollectibles.weapon).getGroupMeta(
									Weapons.TWeaponGroupType.Drone,
								)}
							/>
						</>
					);
				default:
					return (
						<>
							{' '}
							is used to perform <em>Attacks</em> on other Units and Orbital Bases during Combat. A Unit
							may equip several <em>Weapons</em>, picking a right one for the job or firing all of them at
							once with <em>Barrage Action</em>
						</>
					);
			}
		};

		return (
			<>
				<SVGHexIcon
					className={[UITooltip.getIconClass()].join(' ')}
					colorTop={getTemplateColor('dark_red')}
					colorBottom={getTemplateColor('text_dark')}>
					{!slot ? (
						UIWeaponSlotDescriptor.getIcon()
					) : (
						<text
							x="50%"
							y="50%"
							textAnchor="middle"
							dominantBaseline="middle"
							filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)}
							stroke="none"
							fill={getTemplateColor('text_highlight')}>
							{WeaponSlots[slot].abbr.substr(0, 1).toLocaleUpperCase()}
						</text>
					)}
				</SVGHexIcon>
				<span>
					<b>{slot ? WeaponSlots[slot].name : UIWeaponDescriptor.getCaption()}</b>
					{getTextPart(slot)}
				</span>
			</>
		);
	}

	public static getIcon() {
		return (
			<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.gray} asSvg={true}>
				<g
					style={{
						transformOrigin: '50% 50%',
						transform: 'scale(0.75) translateY(-5%)',
					}}>
					<use {...SVGIconContainer.getXLinkProps(SVGWeaponIconID)} />
				</g>
			</SVGSmallTexturedIcon>
		);
	}

	public render() {
		return (
			<SVGHexIcon
				className={classnames(Style.weaponSlotDescriptor, UIContainer.iconClass, this.props.className || '')}
				onClick={this.props.onClick}
				highlight={!!this.props.highlight}
				colorTop={getTemplateColor(this.props.highlight ? 'dark_red' : 'text_dark')}
				colorBottom={getTemplateColor(this.props.highlight ? 'text_dark' : 'light_gray')}
				tooltip={!!this.props.tooltip && UIWeaponSlotDescriptor.getSlotDescription(this.props.slotType)}>
				{!this.props.slotType ? (
					this.props.weaponGroup ? (
						<SVGSmallTexturedIcon
							style={
								this.props.highlight
									? TSVGSmallTexturedIconStyles.gray
									: TSVGSmallTexturedIconStyles.gray
							}
							asSvg={true}
							tooltip={false}>
							<use
								{...SVGIconContainer.getXLinkProps(UIWeaponGroupIconMapping(this.props.weaponGroup))}
							/>
						</SVGSmallTexturedIcon>
					) : (
						UIWeaponSlotDescriptor.getIcon()
					)
				) : (
					<text
						x="50%"
						y="50%"
						textAnchor="middle"
						dominantBaseline="middle"
						filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)}
						stroke="none"
						fill={getTemplateColor('text_highlight')}>
						{WeaponSlots[this.props.slotType].abbr.substr(0, 1).toLocaleUpperCase()}
					</text>
				)}
			</SVGHexIcon>
		);
	}
}

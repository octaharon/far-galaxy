import * as React from 'react';
import Weapons from '../../../../../../definitions/technologies/types/weapons';
import WeaponGroups from '../../../../../../definitions/technologies/weapons/Groups';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UIUnitCostDescriptor from '../../../Prototype/UnitCostDescriptor';
import UITooltip from '../../../Templates/Tooltip';
import UIAoeRadiusDescriptor from '../../Attack/AOERadiusDescriptor';
import UIHitChanceDescriptor from '../../Attack/HitChanceDescriptor';
import UIAttackPriorityDescriptor from '../../Attack/PriorityDescriptor';
import UIAttackRangeDescriptor from '../../Attack/RangeDescriptor';
import UIAttackRateDescriptor from '../../Attack/RateDescriptor';
import UIDamageModifierDescriptor from '../../Damage/DamageModifierDescriptor';
import UIWeaponDescriptor from '../WeaponDescriptor';
import UIWeaponGroupDescriptor from '../WeaponGroupDescriptor';
import UIWeaponSlotDescriptor from '../WeaponSlotDescriptor';
import styles from './index.scss';

const weaponModifierDescriptorClass = styles.weaponModifierDescriptor;

export type UIWeaponModifierDescriptorProps = IUICommonProps & {
	modifier: Weapons.TWeaponGroupModifier;
	groupClassName?: string;
	hideCost?: boolean;
};

const ModifierDescriptions: Record<
	Weapons.TWeaponModifierKey,
	(v: IModifier, short: boolean) => JSX.Element | 'string'
> = {
	baseRange: (v, short) => (
		<UIAttackRangeDescriptor
			tooltip={true}
			isWeapon={true}
			short={!short}
			modifier={v}
			className={short ? UIContainer.captionWithoutIconClass : ''}
		/>
	),
	baseRate: (v, short) => (
		<UIAttackRateDescriptor
			tooltip={true}
			short={!short}
			modifier={v}
			className={short ? UIContainer.captionWithoutIconClass : ''}
		/>
	),
	baseAccuracy: (v, short) => (
		<UIHitChanceDescriptor
			tooltip={true}
			short={!short}
			modifier={v}
			className={short ? UIContainer.captionWithoutIconClass : ''}
		/>
	),
	priority: (v, short) => (
		<UIAttackPriorityDescriptor
			tooltip={true}
			short={!short}
			modifier={v}
			className={short ? UIContainer.captionWithoutIconClass : ''}
		/>
	),
	damageModifier: (v, short) => (
		<UIDamageModifierDescriptor
			tooltip={true}
			short={!short}
			modifier={v}
			className={short ? UIContainer.captionWithoutIconClass : ''}
		/>
	),
	aoeRadiusModifier: (v, short) => (
		<UIAoeRadiusDescriptor
			tooltip={true}
			short={!short}
			modifier={v}
			className={short ? UIContainer.captionWithoutIconClass : ''}
		/>
	),
	baseCost: (v, short) => (
		<UIUnitCostDescriptor
			tooltip={true}
			className={short ? UIContainer.captionWithoutIconClass : ''}
			short={!short}
			caption={'Production Cost'}
			totalCost={false}
			modifier={v}
		/>
	),
};

export default class UIWeaponModifierDescriptor extends React.PureComponent<UIWeaponModifierDescriptorProps> {
	public static getClass(): string {
		return weaponModifierDescriptorClass;
	}

	public static getCaption(isDefault = false): string {
		return `${isDefault ? 'All' : 'Other'} ${UIWeaponDescriptor.getCaption(true)}`;
	}

	public static getGroupCaption = (weaponGroup: keyof Weapons.TWeaponGroupModifier, isDefault = false) => {
		switch (true) {
			case isDefault:
				return (
					<span className={UIContainer.hasTooltipClass}>
						<UIWeaponSlotDescriptor slotType={null} className={styles.weaponEntityIcon} highlight={true} />
						<span className={UIWeaponGroupDescriptor.getCaptionClass()}>
							{UIWeaponModifierDescriptor.getCaption(isDefault)}
						</span>
						<UITooltip>{UIWeaponSlotDescriptor.getSlotDescription(null)}</UITooltip>
					</span>
				);
			case weaponGroup === 'default':
				return (
					<span className={UIContainer.hasTooltipClass}>
						<UIWeaponSlotDescriptor slotType={null} className={styles.weaponEntityIcon} highlight={true} />
						<span className={UIWeaponGroupDescriptor.getCaptionClass()}>
							{UIWeaponModifierDescriptor.getCaption()}
						</span>
						<UITooltip>{UIWeaponSlotDescriptor.getSlotDescription(null)}</UITooltip>
					</span>
				);
			default:
				return (
					<UIWeaponGroupDescriptor
						group={WeaponGroups.find((w) => w.id === weaponGroup)}
						iconClassName={styles.weaponGroupIcon}
						tooltip={true}
					/>
				);
		}
	};

	public render() {
		const itemClass = classnames(
			UIContainer.hasTooltipClass,
			UIContainer.captionClass,
			UIContainer.descriptorClass,
		);
		const mod = this.props.modifier;

		return Object.keys(ModifierDescriptions).map((prop) => {
			if (prop === 'baseCost' && !!this.props.hideCost) return null;
			const groups: Array<{ group: Weapons.TWeaponGroupType | 'default'; object: IModifier }> = Object.keys(
				mod,
			).reduce(
				(a, weaponGroup) =>
					mod[weaponGroup][prop]
						? a.concat({
								group: weaponGroup,
								object: mod[weaponGroup][prop],
						  })
						: a,
				[],
			);
			if (!groups.length) return null;
			return (
				<div key={`group-${prop}`} className={this.props.groupClassName}>
					{ModifierDescriptions[prop](null, true)}
					{groups
						.sort((a, b) => (a.group === 'default' ? 1 : -1))
						.map(({ group, object }) => (
							<React.Fragment key={group}>
								<dl className={classnames(UIWeaponModifierDescriptor.getClass())}>
									<dt className={itemClass}>
										{UIWeaponModifierDescriptor.getGroupCaption(
											group,
											groups.length === 1 && group === 'default',
										)}
									</dt>
									<dd className={UIContainer.descriptorClass}>
										{/*<span
										className={classnames(
											styles.modifier,
											BasicMaths.applyModifier(100, object) > 100
												? styles.positive
												: styles.negative,
										)}>
										{BasicMaths.describeModifier(object, true).trim()}
									</span>*/}
										{ModifierDescriptions[prop](object, false)}
									</dd>
								</dl>
							</React.Fragment>
						))}
				</div>
			);
		});
	}
}

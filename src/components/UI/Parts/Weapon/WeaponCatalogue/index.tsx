import * as React from 'react';
import { doesCollectibleSetIncludeItem } from '../../../../../../definitions/core/Collectible';
import Players from '../../../../../../definitions/player/types';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import FuzeShells from '../../../../../../definitions/technologies/weapons/all/cannons/FuzeShells.10000';
import Weapons from '../../../../../../definitions/technologies/types/weapons';
import { stackWeaponGroupModifiers } from '../../../../../../definitions/technologies/weapons/WeaponManager';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { classnames, IUICommonProps } from '../../../index';
import UIWeaponDescriptor from '../WeaponDescriptor';
import UIWeaponListSelector from '../WeaponListSelector';
import styles from './index.scss';
import IPlayer = Players.IPlayer;

const weaponCatalogueClass = styles.weaponCatalogue;

type UIWeaponCatalogueProps = IUICommonProps & {
	player?: IPlayer;
	defaultSelectedWeapon?: number;
	short?: boolean;
	modifiers?: Weapons.TWeaponGroupModifier[];
	forChassis?: UnitChassis.chassisClass;
	forSlot?: Weapons.TWeaponSlotType;
	onWeaponSelected?: (weaponId: number) => any;
	phases?: number;
};

export interface UIWeaponCatalogueState {
	selectedWeapon: number;
}

const defaultWeaponId = FuzeShells.id;

const stackWeaponMods = (props: UIWeaponCatalogueProps) => stackWeaponGroupModifiers(...(props.modifiers || []));

const getWeaponId = (props: UIWeaponCatalogueProps, weaponId?: number) => {
	const provider = DIContainer.getProvider(DIInjectableCollectibles.weapon);
	let wId = (weaponId ?? props.defaultSelectedWeapon) || defaultWeaponId;
	const possibleWeaponList = props.player
		? Object.keys(provider.getAll() || []).filter((id) => props.player.isWeaponAvailable(id))
		: Object.keys(provider.getAll());
	if (!doesCollectibleSetIncludeItem(wId, possibleWeaponList)) wId = possibleWeaponList[0];
	if (props.forSlot) {
		if (!provider.doesWeaponFitSlot(wId, props.forSlot))
			wId = possibleWeaponList.filter((id) => provider.doesWeaponFitSlot(id, props.forSlot))[0];
	}
	return wId;
};

export default class UIWeaponCatalogue extends React.PureComponent<UIWeaponCatalogueProps, UIWeaponCatalogueState> {
	public static getClass(): string {
		return weaponCatalogueClass;
	}

	public static getDerivedStateFromProps(props: UIWeaponCatalogueProps, state?: UIWeaponCatalogueState) {
		const wId = getWeaponId(props, state?.selectedWeapon);
		return {
			selectedWeapon: wId || defaultWeaponId,
		};
	}

	public get selectedGroup() {
		return DIContainer.getProvider(DIInjectableCollectibles.weapon).getItemMeta(this.state.selectedWeapon).group.id;
	}

	public constructor(props: UIWeaponCatalogueProps) {
		super(props);
		this.state = UIWeaponCatalogue.getDerivedStateFromProps(props);
		this.onWeaponSelected = this.onWeaponSelected.bind(this);
	}

	public componentDidMount() {
		if (!this.props.defaultSelectedWeapon && this.props.onWeaponSelected)
			this.props.onWeaponSelected(this.state.selectedWeapon);
	}

	public render() {
		const mods = stackWeaponMods(this.props);
		return (
			<div className={classnames(UIWeaponCatalogue.getClass(), this.props.className)}>
				<UIWeaponListSelector
					selectedGroup={this.selectedGroup}
					selectedWeapon={this.state.selectedWeapon}
					weaponSlot={this.props.forSlot}
					onWeaponSelected={this.onWeaponSelected}
					player={this.props.player}
					className={styles.selector}
				/>
				<UIWeaponDescriptor
					weaponId={this.state.selectedWeapon}
					phases={this.props.phases}
					className={styles.descriptor}
					currentSlot={this.props.forSlot}
					modifiers={[mods?.[this.selectedGroup] || mods?.default]}
					details={!this.props.short}
					tooltip={true}
				/>
			</div>
		);
	}

	protected onWeaponSelected(weaponId: number) {
		const selectedWeapon = getWeaponId(this.props, weaponId);
		this.setState(
			{
				...this.state,
				selectedWeapon,
			},
			() => {
				if (this.props.onWeaponSelected) this.props.onWeaponSelected(selectedWeapon);
			},
		);
	}
}

import * as React from 'react';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import { IWeapon } from '../../../../../../definitions/technologies/types/weapons';
import UIContainer from '../../../index';
import UIAPTDescriptor from '../../Attack/APTDescriptor';
import UIHitChanceDescriptor from '../../Attack/HitChanceDescriptor';
import UIAttackPriorityDescriptor from '../../Attack/PriorityDescriptor';
import UIAttackRangeDescriptor from '../../Attack/RangeDescriptor';
import UIAttackRateDescriptor from '../../Attack/RateDescriptor';
import UIDPTDescriptor from '../../Damage/DPTDescriptor';
import styles from './index.scss';

const weaponMetaDescriptorClass = styles.weaponMeta;

export interface UIAttackMetaDescriptorProps<T extends UnitAttack.IAttackInterface = IWeapon> {
	weapon: T;
	details?: boolean;
	phases?: number;
}

export default class UIAttackMetaDescriptor extends React.PureComponent<UIAttackMetaDescriptorProps, {}> {
	public static getClass(): string {
		return weaponMetaDescriptorClass;
	}

	public render() {
		return (
			<>
				{this.props.details ? (
					<div className={UIAttackMetaDescriptor.getClass()}>
						<UIAttackRangeDescriptor range={this.props.weapon.baseRange} isWeapon={true} tooltip={true} />
						<UIHitChanceDescriptor accuracy={this.props.weapon.baseAccuracy} tooltip={true} />
						<UIAttackPriorityDescriptor priority={this.props.weapon.priority} tooltip={true} />
						<UIAttackRateDescriptor tooltip={true} attackRate={this.props.weapon.baseRate} />
					</div>
				) : (
					<dl className={UIAttackMetaDescriptor.getClass()}>
						<dt className={UIContainer.descriptorClass}>
							<UIAttackRangeDescriptor
								range={this.props.weapon.baseRange}
								isWeapon={true}
								short={true}
								tooltip={true}
							/>
						</dt>
						<dd className={''}>
							<UIHitChanceDescriptor
								accuracy={this.props.weapon.baseAccuracy}
								short={true}
								tooltip={true}
							/>
						</dd>
						<dt className={UIContainer.descriptorClass}>
							<UIAttackRateDescriptor
								tooltip={true}
								short={true}
								attackRate={this.props.weapon.baseRate}
							/>
						</dt>
						<dd className={''}>
							<UIAttackPriorityDescriptor
								priority={this.props.weapon.priority}
								tooltip={true}
								short={true}
							/>
						</dd>
					</dl>
				)}
				<UIAPTDescriptor attackRate={this.props.weapon.baseRate} tooltip={true} />
				{!!this.props.phases && (
					<UIAPTDescriptor
						attackRate={this.props.weapon.baseRate}
						tooltip={true}
						phases={this.props.phases}
					/>
				)}
				<UIDPTDescriptor weapon={this.props.weapon} tooltip={true} />
				{!!this.props.phases && (
					<UIDPTDescriptor weapon={this.props.weapon} tooltip={true} phases={this.props.phases} />
				)}
			</>
		);
	}
}

import * as React from 'react';
import {
	doesCollectibleSetIncludeItem,
	findCollectibleItemInSet,
} from '../../../../../../definitions/core/Collectible';
import Players from '../../../../../../definitions/player/types';
import Weapons from '../../../../../../definitions/technologies/types/weapons';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { IUICommonProps } from '../../../index';
import { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UIListSelector from '../../../Templates/ListSelector';
import UIWeaponGroupDescriptor from '../WeaponGroupDescriptor';
import styles from './index.scss';

const weaponListSelectorClass = styles.weaponListSelector;

export type UIWeaponListSelectorProps = IUICommonProps & {
	onWeaponSelected: (weaponId: number) => any;
	selectedGroup?: Weapons.TWeaponGroupType;
	selectedWeapon?: number;
	weaponSlot?: Weapons.TWeaponSlotType;
	player?: Players.IPlayer;
};

type IWeaponHash = Weapons.TWeaponGroupGeneric<number[]>;

export interface UIWeaponListSelectorState {
	weaponsAvailable: IWeaponHash;
}

export default class UIWeaponListSelector extends React.PureComponent<
	UIWeaponListSelectorProps,
	UIWeaponListSelectorState
> {
	public static getClass(): string {
		return weaponListSelectorClass;
	}

	protected get weaponOptions() {
		return this.state.weaponsAvailable[this.selectedGroup];
	}

	protected get selectedGroup(): Weapons.TWeaponGroupType {
		return this.props.selectedGroup
			? this.state.weaponsAvailable[this.props.selectedGroup]
				? this.props.selectedGroup
				: Object.keys(this.state.weaponsAvailable)[0]
			: this.props.selectedWeapon
			? Object.keys(this.state.weaponsAvailable).find((k) =>
					doesCollectibleSetIncludeItem(this.props.selectedWeapon, this.state.weaponsAvailable[k]),
			  )
			: Object.keys(this.state.weaponsAvailable)[0] || null;
	}

	protected getSelectedWeapon(group = this.selectedGroup): number {
		if (!group) return null;
		return doesCollectibleSetIncludeItem(this.props.selectedWeapon, this.state.weaponsAvailable[group])
			? this.props.selectedWeapon
			: this.state.weaponsAvailable[group][0];
	}

	public static getDerivedStateFromProps(props: UIWeaponListSelectorProps, state: UIWeaponListSelectorState) {
		return {
			...(state || {}),
			weaponsAvailable: UIWeaponListSelector.buildWeaponsHash(props),
		};
	}

	protected static buildWeaponsHash(props: UIWeaponListSelectorProps) {
		const provider = DIContainer.getProvider(DIInjectableCollectibles.weapon);
		const r = {} as IWeaponHash;
		Object.keys(provider.getAll()).forEach((weaponId) => {
			if (props.player && !props.player.isWeaponAvailable(weaponId)) return;
			if (props.weaponSlot && !provider.doesWeaponFitSlot(weaponId, props.weaponSlot)) return;
			const key = provider.getItemMeta(weaponId).group.id;
			r[key] = (r[key] || []).concat(weaponId);
		});
		return r;
	}

	public constructor(props: UIWeaponListSelectorProps) {
		super(props);
		const weaponsAvailable = UIWeaponListSelector.buildWeaponsHash(props);
		this.state = {
			weaponsAvailable,
		};
	}

	public render() {
		const weaponGroupCaption = (wg: Weapons.TWeaponGroupType) => (
			<UIWeaponGroupDescriptor
				group={DIContainer.getProvider(DIInjectableCollectibles.weapon).getGroupMeta(wg)}
				tooltip={true}
				className={styles.weaponGroup}
			/>
		);
		const onGroupSelect = (wg: Weapons.TWeaponGroupType) => this.props.onWeaponSelected(this.getSelectedWeapon(wg));
		const weaponCaption = (weaponId: number) => (
			<span className={styles.weaponCaption}>
				{DIContainer.getProvider(DIInjectableCollectibles.weapon).get(weaponId).caption}
			</span>
		);
		const onWeaponSelect = (weaponId: number) => this.props.onWeaponSelected(weaponId);
		return (
			<div className={[UIWeaponListSelector.getClass(), this.props.className].join(' ')}>
				<UIListSelector
					options={Object.keys(this.state.weaponsAvailable)}
					selectedItem={this.selectedGroup}
					buttonColor={TSVGSmallTexturedIconStyles.pink}
					optionRender={weaponGroupCaption}
					onSelect={onGroupSelect}
				/>
				<UIListSelector
					options={this.weaponOptions}
					selectedItem={this.getSelectedWeapon()}
					selectedIndex={findCollectibleItemInSet(this.getSelectedWeapon(), this.weaponOptions)}
					buttonColor={TSVGSmallTexturedIconStyles.pink}
					optionRender={weaponCaption}
					onSelect={onWeaponSelect}
				/>
			</div>
		);
	}
}

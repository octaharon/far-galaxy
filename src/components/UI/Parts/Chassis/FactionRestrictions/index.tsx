import React from 'react';
import { FactionList } from '../../../../../../definitions/world/factionIndex';
import PoliticalFactions from '../../../../../../definitions/world/factions';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import { UIWarningIcon } from '../../../../Symbols/commonIcons';
import UIFactionCaption from '../../../Political/FactionCaption';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import styles from './index.scss';

type UIFactionRestrictionProps = IUICommonProps & {
	restrictedFactions?: PoliticalFactions.TFactionID[];
	exclusiveFactions?: PoliticalFactions.TFactionID[];
};

const RestrictionIcon = ({ exclusive = false }: { exclusive: boolean }) => (
	<SVGSmallTexturedIcon style={exclusive ? TSVGSmallTexturedIconStyles.gold : TSVGSmallTexturedIconStyles.red}>
		<use {...SVGIconContainer.getXLinkProps(UIWarningIcon)} />
	</SVGSmallTexturedIcon>
);

const ExclusiveCaption = (props: { factionId: PoliticalFactions.TFactionID }) => (
	<span className={styles.exclusiveCaption}>
		{`Only available to `}
		<span className={UIFactionCaption.getCaptionClass()} style={UIFactionCaption.getCaptionStyle(props.factionId)}>
			{UIFactionCaption.getCaption(props.factionId)}
		</span>
	</span>
);

const RestrictedCaption = (props: { factionId: PoliticalFactions.TFactionID }) => (
	<span className={styles.restrictionCaption}>
		{`Not available to `}
		<span className={UIFactionCaption.getCaptionClass()} style={UIFactionCaption.getCaptionStyle(props.factionId)}>
			{UIFactionCaption.getCaption(props.factionId)}
		</span>
	</span>
);

const UIFactionRestrictionDescriptor: React.FC<UIFactionRestrictionProps> = (props) =>
	!props.exclusiveFactions?.length && !props.restrictedFactions?.length ? null : (
		<dl
			className={classnames(
				styles.factionRestriction,
				props.tooltip ? UIContainer.hasTooltipClass : null,
				props.className,
			)}>
			{(props.restrictedFactions || []).map((factionId) => (
				<dt key={factionId}>
					{UIFactionCaption.getIcon(factionId)}
					<RestrictedCaption factionId={factionId} />
					{props.tooltip && <UITooltip>{UIFactionCaption.getTooltip(factionId)}</UITooltip>}
				</dt>
			))}
			{(props.exclusiveFactions || []).map((factionId) => (
				<dt key={factionId}>
					{UIFactionCaption.getIcon(factionId)}
					<ExclusiveCaption factionId={factionId} />
					{props.tooltip && <UITooltip>{UIFactionCaption.getTooltip(factionId)}</UITooltip>}
				</dt>
			))}
		</dl>
	);

export default UIFactionRestrictionDescriptor;

import * as React from 'react';
import UIContainer, { IUICommonProps } from '../../../index';
import UIHitPointsDescriptor from '../HitPointsDescriptor';
import Styles from './index.scss';

export type UIHitPointsInlineDescriptorProps = IUICommonProps & {
	value?: number | string;
};

export const hitPointsInlineDescriptorClass = Styles.hitPointsInlineDescriptor;

export default class UIHitPointsInlineDescriptor extends React.Component<UIHitPointsInlineDescriptorProps, {}> {
	public static getCaption() {
		return UIHitPointsDescriptor.getCaption();
	}

	public render() {
		return (
			<em className={[hitPointsInlineDescriptorClass, this.props.className || ''].join(' ')}>
				{Number.isFinite(this.props.value) || this.props.value ? this.props.value : null}
				{UIHitPointsDescriptor.getIcon()}
				{UIHitPointsInlineDescriptor.getCaption()}
			</em>
		);
	}
}

import * as React from 'react';
import Players from '../../../../../../definitions/player/types';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import DefenseManager from '../../../../../../definitions/tactical/damage/DefenseManager';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { getFactionUnitModifiers } from '../../../../../../definitions/world/factionIndex';
import { IUICommonProps } from '../../../index';
import UIChassisDescriptor from '../ChassisDescriptor';
import UIChassisListSelector from '../ChassisListSelector';
import styles from './index.scss';
import IPlayer = Players.IPlayer;

const chassisCatalogueClass = styles.chassisCatalogue;

type UIChassisCatalogueProps = IUICommonProps & {
	player?: IPlayer;
	defaultSelectedChassis?: number;
	short?: boolean;
	modifiers?: UnitChassis.TUnitChassisModifier[];
	onChassisSelected?: (chassisId: number) => any;
};

export interface UIChassisCatalogueState {
	selectedChassis: number;
}

const stackChassisMods = (
	props: UIChassisCatalogueProps,
	selectedChassis: number = props.defaultSelectedChassis,
): UnitChassis.TUnitChassisModifier[] => {
	const chassis = DIContainer.getProvider(DIInjectableCollectibles.chassis).get(selectedChassis);
	return [
		...(props.modifiers || []),
		...(props.player?.unitModifier?.[chassis?.type || 'default']?.chassis || []),
		...(getFactionUnitModifiers(props.player?.faction)
			.map((v) => v[chassis?.type || 'default'] || v.default)
			.flatMap((v) => v.shield) || []),
	];
};

const getChassisId = (props: UIChassisCatalogueProps, chassisId?: number) => {
	const provider = DIContainer.getProvider(DIInjectableCollectibles.chassis);
	let sId = chassisId ?? props.defaultSelectedChassis;
	const possibleChassisOptions = props.player
		? Object.keys(provider.getAll()).filter((id) => props.player.isChassisAvailable(id))
		: Object.keys(provider.getAll());
	if (!possibleChassisOptions.map((v) => parseInt(String(v))).includes(sId)) sId = possibleChassisOptions[0];
	return sId;
};

export default class UIChassisCatalogue extends React.PureComponent<UIChassisCatalogueProps, UIChassisCatalogueState> {
	public static getClass(): string {
		return chassisCatalogueClass;
	}

	public static getDerivedStateFromProps(props: UIChassisCatalogueProps, state?: UIChassisCatalogueState) {
		const chassisId = getChassisId(props, state?.selectedChassis);
		return {
			selectedChassis: chassisId || 10000,
		};
	}

	public get selectedSlot() {
		return DIContainer.getProvider(DIInjectableCollectibles.chassis).get(this.state.selectedChassis)?.type || null;
	}

	public constructor(props: UIChassisCatalogueProps) {
		super(props);
		this.state = UIChassisCatalogue.getDerivedStateFromProps(props);
		this.onChassisSelected = this.onChassisSelected.bind(this);
	}

	public render() {
		const mods = stackChassisMods(this.props);
		return (
			<div className={UIChassisCatalogue.getClass()}>
				<UIChassisListSelector
					selectedChassis={this.state.selectedChassis}
					onChassisSelected={this.onChassisSelected}
					player={this.props.player}
					className={styles.selector}
				/>
				<UIChassisDescriptor
					chassisId={this.state.selectedChassis}
					className={styles.descriptor}
					modifiers={mods}
					details={!this.props.short}
					player={this.props.player}
				/>
			</div>
		);
	}

	protected onChassisSelected(chassisId: number) {
		this.setState(
			{
				...this.state,
				selectedChassis: getChassisId(this.props, chassisId),
			},
			() => {
				if (this.props.onChassisSelected) this.props.onChassisSelected(chassisId);
			},
		);
	}
}

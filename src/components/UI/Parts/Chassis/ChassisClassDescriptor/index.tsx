import * as React from 'react';
import { ChassisClassTitles } from '../../../../../../definitions/technologies/chassis';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import { getChassisClassIconId } from '../../../../Symbols/chassisClassIcons';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import { termTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import styles from './index.scss';

export const ChassisClassDescriptions: EnumProxyWithDefault<UnitChassis.chassisClass, string> = {
	[UnitChassis.chassisClass.infantry]:
		'A multipurpose Organic Ground Unit, fragile but the quickest to replenish. A wide range of human options available, possessing different unique qualities and specializations',
	[UnitChassis.chassisClass.biotech]:
		'A bioengineered Organic Ground Unit: quick, ferocious and expendable. Emits offensive Aura, offers a great firepower and restores Hit Points with Ranks',
	[UnitChassis.chassisClass.exosuit]:
		'Durable Ground Unit, Organic in nature, but augmented by a Large Armor frame. Has good survivability and considerable Damage output even against bigger targets',
	[UnitChassis.chassisClass.droid]:
		'A cheap and flimsy Robotic Ground Unit, compatible with almost full variety of Weapons and having various upgrade options for maximum versatility',
	[UnitChassis.chassisClass.mecha]:
		'An Organic Ground Unit, covered with a Massive heavily armored shell. Provides outstanding Damage output and long time to live in combat, and can be specialized for various defensive tasks',
	[UnitChassis.chassisClass.scout]:
		'A fast and light Ground Unit, providing little firepower, but is a worthy source of an intel on opponent movements. Can be manufactured in numbers and becomes exceptionally good after a few Ranks',
	[UnitChassis.chassisClass.quadrapod]:
		'An Massive Robotic Unit, that slowly traverses the Ground, equipped with larger Weapons and Equipment. Purposed for sniping out targets, while relying on Shields rather than on Armor for survival',
	[UnitChassis.chassisClass.artillery]:
		'A specialized Ground Unit, that moves slow but delivers overwhelming Damage at superior Attack Range. Has good vision and after several Ranks can take out even the toughest opponents',
	[UnitChassis.chassisClass.tank]:
		'A Ground Unit with heavy Armor and outstanding durability in combat, but quite limited with its tactical options. Good for distraction and sustaining against bigger enemies',
	[UnitChassis.chassisClass.lav]:
		'A mobile Ground Unit with little sustainability but very valuable Ranks. It can build up a lot of Damage and keep delivering it while changing positions',
	[UnitChassis.chassisClass.hav]:
		'An onslaught Ground Unit, refactored for use with Large Weapons and quite durable, but still capable of firing at full throttle. Can be a worthy opponent on its own, but works best in composition with other vehicles',
	[UnitChassis.chassisClass.swarm]:
		'A Organic Airborne Unit with extreme survivability, but very slow. Provides ground support for Units around it with a mighty offensive Aura',
	[UnitChassis.chassisClass
		.mothership]: `A queen of the fleet, a Unique Airborne Unit with tremendous Damage output, a hangar for Drones and an Equipment slot. Even though it's pretty slow and too big to miss, it can absorb a hefty lot of enemy firepower before going down`,
	[UnitChassis.chassisClass
		.sup_aircraft]: `A tactical Airborne Unit that provides vision and a protective Aura for other friendly Units around. Can't use Weapons, but has unique set of Abilities and double Equipment slot`,
	[UnitChassis.chassisClass
		.sup_vehicle]: `A tactical Ground Unit that provides vision and protective Aura for other friendly Units around. Can't use Weapons, but has an Equipment slot and a unique set of Abilities`,
	[UnitChassis.chassisClass.carrier]:
		'A Massive Airborne Unit that carries Drones and covers large areas with formidable Damage output. However, it can be an easy target if left unprotected',
	[UnitChassis.chassisClass.helipad]:
		'An Massive flying counterpart of Artillery that brings together the supremacy of Airborne Units with menacing Damage and outstanding Range of big guns',
	[UnitChassis.chassisClass
		.fighter]: `Light, dodgy, blazing fast aircraft, dominating other Airborne Units and lines of Anti-Air defense. Favors high-precision Weapons, but can't efficiently operate Bombs`,
	[UnitChassis.chassisClass
		.bomber]: `An Airborne vehicle that is purposed to wipe out large areas of the map with Bombs. Very vulnerable to Anti-Air attacks and is not good at air combat either`,
	[UnitChassis.chassisClass.strike_aircraft]:
		'Highly maneuverable, versatile Airborne Unit that excels in taking out ground targets. Nevertheless, can be refitted for almost any combat mission with extra Equipment',
	[UnitChassis.chassisClass.tiltjet]:
		'Well-protected mid-range Airborne Unit with great leveling options and covering vast areas with considerate Damage, but too small to efficiently operate Bombs',
	[UnitChassis.chassisClass.corvette]:
		'Fast and light Naval Unit for scouting navy combat and hunting Submarines. Has great vision and Cloak breaking Aura',
	[UnitChassis.chassisClass.destroyer]:
		'Versatile Naval Unit, that is maneuverable and resilient. It has limited vision but compensates for it with good firepower and movement Speed',
	[UnitChassis.chassisClass.submarine]:
		'A tactical Naval Unit, that Cloak underwater and issue long range Missile strikes out of the blue, but, when detected, is not so durable. Requires special means of production',
	[UnitChassis.chassisClass.battleship]:
		'A heavy Naval Unit, that has only one goal: to deliver immense Damage at isolated targets. Quite vulnerable to Airborne Units and Submarines, so works best at long range combat, duels or as a part of composition with other vessels',
	[UnitChassis.chassisClass.hydra]:
		'A Massive Naval Unit, bioengineered from Organic matter and a Titan Weapon platform. To make it even better, it is immune to Effects, has an Equipment slot and emits an offensive Aura, all together allowing to exercise zone control in melee and dominate targets at range',
	[UnitChassis.chassisClass.rover]:
		'A scouting Hover Unit, slightly more powerful than a basic Scout. Can fit most conventional Weapons and protection and has advanced dodging capabilities',
	[UnitChassis.chassisClass.combatdrone]:
		'A Robotic Hover Unit, mobile and well-armed. While not so bulky, it possesses an outstanding combination of Speed and firepower, that allows it both to dig into enemy lines and draw back when it becomes too dangerous',
	[UnitChassis.chassisClass
		.pointdefense]: `A stationary Robotic Hover Unit, made for zone control and protection. It's can barely Dodge attacks but has formidable Damage output and wide Weapon compatibility, and can stack multiple Shields to achieve significant protection from various damaging factors`,
	[UnitChassis.chassisClass
		.hovertank]: `An onslaught Hover Unit, that is just good at everything. It has formidable protection, several Weapons, many tactical options and competitive Speed and maneuverability, that makes it a valuable combat asset both in composition or on its own`,
	[UnitChassis.chassisClass
		.launcherpad]: `A slowly moving Hover Unit with a Titan Weapon mount: a pinnacle of Artillery evolution, providing for longest Attack Range and one-shot takedowns. Nevertheless, is not as vulnerable to faster Units, boasting best Armor and Shields`,
	[UnitChassis.chassisClass
		.science_vessel]: `A top-tier Robotic tactical Unit, that can't use Weapons, but gains unique Abilities with Ranks. Provides passive regeneration of Hit Points and Shields to nearby friendly Units, and achieves best mobibility with Hover`,
	[UnitChassis.chassisClass
		.fortress]: `A Massive stationary Robotic Unit, that holds the position on Hover cushions and snipes out the most remote targets with its Titan Weapon. Has plenty of Hit Points and can accumulate a lot of Armor`,
	[UnitChassis.chassisClass.cruiser]:
		'A universal Naval Unit, that can be purposed for any mission due to Equipment slot. Has good survivability versus almost any Weapons and has several very powerful tactical varieties',
	[UnitChassis.chassisClass.manofwar]:
		'A Massive Naval Unit, the head of the fleet. It is very complex and Unique due to its combination of many protection measures, a powerful Weapon onboard and a hangar for Drones',
	[UnitChassis.chassisClass.sup_ship]:
		"A tactical Naval Unit that provides vision and protective Aura for other friendly Units around. Can't use Weapons, but has double Equipment slot and a unique set of Abilities",
	default:
		'is a foundation for Prototype, which is a blueprint for Unit Production at Factories. Chassis directly defines many Unit qualities, notably Hit Points, Movement, Leveling and Actions, but most importantly it determines, which Weapons, Shields, Armor and Equipment can be forged together for a Unit. Chassis are divided into classes, with every Chassis inside a class having similar composition and recognizable looks',
};

type UIChassisClassDescriptorProps = IUICommonProps & {
	chassisType: UnitChassis.chassisClass;
};

export default class UIChassisClassDescriptor extends React.PureComponent<UIChassisClassDescriptorProps, {}> {
	public static getTooltip(chassisType?: UnitChassis.chassisClass) {
		return termTooltipTemplate(
			UIChassisClassDescriptor.getIcon(chassisType, UIContainer.tooltipIconClass),
			UIChassisClassDescriptor.getCaption(chassisType),
			ChassisClassDescriptions[chassisType] || ChassisClassDescriptions.default,
		); /*(
			<span>
				<b>Chassis</b>
				<Tokenizer
					description={(()=>{})()
						' is a foundation for creating a Prototype, which is a blueprint for Unit Production at Factories. Chassis influences many Unit qualities, notably Hit Points, Movement, Leveling and Actions, but most importantly it determines, which Weapons, Shields, Armor and Equipment can be brought together in a Prototype with that Chassis. Chassis are divided into classes, with every Chassis inside a class having similar Slots and recognizable looks'
					}
				/>
			</span>
		);*/
	}

	public static getClass() {
		return styles.chassisClassDescriptor;
	}

	public static getCaptionClass() {
		return styles.chassisCaption;
	}

	public static getIcon(chassisClass: UnitChassis.chassisClass = null, iconClass = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon className={iconClass} style={TSVGSmallTexturedIconStyles.gold}>
				<use {...SVGIconContainer.getXLinkProps(getChassisClassIconId(chassisClass))} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getEntityCaption(chassisClass: UnitChassis.chassisClass = null) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIChassisClassDescriptor.getIcon(chassisClass, UIContainer.iconClass)}
				<span className={UIChassisClassDescriptor.getCaptionClass()}>
					{UIChassisClassDescriptor.getCaption(chassisClass)}
				</span>
			</em>
		);
	}

	public static getCaption(chassisType?: UnitChassis.chassisClass) {
		return ChassisClassTitles[chassisType || 'default'] || 'Unknown Chassis';
	}

	public render() {
		return (
			<span
				className={classnames(
					UIChassisClassDescriptor.getClass(),
					UIContainer.captionClass,
					this.props.tooltip ? UIContainer.hasTooltipClass : null,
					this.props.className,
				)}>
				{UIChassisClassDescriptor.getIcon(this.props.chassisType, styles.chassisIcon)}
				<span className={UIChassisClassDescriptor.getCaptionClass()}>
					{UIChassisClassDescriptor.getCaption(this.props.chassisType)}
				</span>
				{this.props.tooltip && (
					<UITooltip>{UIChassisClassDescriptor.getTooltip(this.props.chassisType)}</UITooltip>
				)}
			</span>
		);
	}
}

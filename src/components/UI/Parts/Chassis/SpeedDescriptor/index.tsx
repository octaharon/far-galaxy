import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer/index';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon/index';
import UITooltip from '../../../Templates/Tooltip';
import Style from './index.scss';

type UISpeedDescriptorPureProps = {
	speed?: number;
	modifier?: IModifier;
	movement?: UnitChassis.movementType;
};
type UISpeedDescriptorProps = IUICommonProps &
	UISpeedDescriptorPureProps & {
		short?: boolean;
	};

const MovementCaption: Partial<Record<UnitChassis.movementType, string>> = {
	[UnitChassis.movementType.air]: 'Airborne',
	[UnitChassis.movementType.hover]: 'Hover',
	[UnitChassis.movementType.naval]: 'Naval',
	[UnitChassis.movementType.ground]: 'Ground',
};

const MovementClass: Partial<Record<UnitChassis.movementType, string>> = {
	[UnitChassis.movementType.air]: Style.air,
	[UnitChassis.movementType.hover]: Style.hover,
	[UnitChassis.movementType.naval]: Style.naval,
	[UnitChassis.movementType.ground]: Style.ground,
};
export const SVGSpeedIconID = 'speed-icon';

SVGIconContainer.registerSymbol(
	SVGSpeedIconID,
	<symbol viewBox="0 0 512 512">
		<path d="m511.666 255.833-135.052-86.404v71.404h-46.275c-6.009-29.895-29.611-53.497-59.506-59.506v-46.275h71.404l-86.404-135.052-86.404 135.051h71.404v46.275c-29.895 6.009-53.497 29.611-59.506 59.506h-46.275v-71.404l-135.052 86.405 135.052 86.404v-71.404h46.275c6.009 29.895 29.611 53.497 59.506 59.506v46.275h-71.404l86.404 135.051 86.404-135.051h-71.404v-46.275c29.895-6.009 53.497-29.611 59.506-59.506h46.275v71.404z" />
	</symbol>,
);

export default class UISpeedDescriptor extends React.PureComponent<UISpeedDescriptorProps, {}> {
	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.green} className={className}>
				<use {...SVGIconContainer.getXLinkProps(SVGSpeedIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption(value: string = null) {
		return value || 'Speed';
	}

	public static getTooltip() {
		return (
			<span>
				{UISpeedDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UISpeedDescriptor.getCaption()}</b> describes how far a unit can go on the battle map. Speed is
				expressed in the same <em>Distance Units</em> as <em>Range</em> and is shared equally between every{' '}
				<em>Action Phase</em> a Unit has that turn
			</span>
		);
	}
	public static getMovementCaption(m: UnitChassis.movementType, raw: true): string;
	public static getMovementCaption(m: UnitChassis.movementType, raw?: false): JSX.Element;
	public static getMovementCaption(m: UnitChassis.movementType, raw = false) {
		return raw ? (
			MovementCaption[m]
		) : (
			<span className={classnames(Style.movement, MovementClass[m])}>
				{MovementCaption[m] || 'Unknown Movement'}
			</span>
		);
	}

	public static getEntityCaption(caption: string = null) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UISpeedDescriptor.getIcon()}
				<span className={Style.speedCaption}>{UISpeedDescriptor.getCaption(caption)}</span>
			</em>
		);
	}

	public static getSpeedValue(props: UISpeedDescriptorProps) {
		const { speed, modifier, movement = null } = props;
		const modOnly = !Number.isFinite(speed);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = BasicMaths.applyModifier(100, modifier) >= 100;
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, true).trim()}
			</span>
		);
		return (
			<>
				{!!movement && <React.Fragment>{UISpeedDescriptor.getMovementCaption(movement)}, </React.Fragment>}
				{Number.isFinite(speed) && <span className={speed > 0 ? Style.max : Style.zero}>{speed}</span>}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	public render() {
		const value = UISpeedDescriptor.getSpeedValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						Style.speedDescriptor,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}
				>
					{UISpeedDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && <UITooltip>{UISpeedDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(this.props.className || '', Style.speedDescriptor)}
				onClick={this.props.onClick || null}
			>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}
				>
					{UISpeedDescriptor.getIcon()}
					<span className={Style.speedCaption}>{UISpeedDescriptor.getCaption()}</span>
					{!!this.props.tooltip && <UITooltip>{UISpeedDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

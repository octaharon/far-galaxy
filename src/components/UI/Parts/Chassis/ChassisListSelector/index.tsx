import * as React from 'react';
import {
	doesCollectibleSetIncludeItem,
	findCollectibleItemInSet,
} from '../../../../../../definitions/core/Collectible';
import Players from '../../../../../../definitions/player/types';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import predicateComparator from '../../../../../utils/predicateComparator';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UIListSelector from '../../../Templates/ListSelector';
import UIChassisClassDescriptor from '../ChassisClassDescriptor';
import UITargetTypeDescriptor from '../TargetTypeDescriptor';
import styles from './index.scss';

const chassisListSelectorClass = styles.chassisListSelector;

type UIChassisListSelectorProps = IUICommonProps & {
	onChassisSelected: (chassisId: number) => any;
	selectedChassis?: number;
	chassisClass?: UnitChassis.chassisClass;
	player?: Players.IPlayer;
};

type IChassisHash = EnumProxy<UnitChassis.chassisClass, number[]>;

interface UIShiedListSelectorState {
	chassisAvailable: IChassisHash;
}

export default class UIChassisListSelector extends React.PureComponent<
	UIChassisListSelectorProps,
	UIShiedListSelectorState
> {
	public static getClass(): string {
		return chassisListSelectorClass;
	}

	protected get chassisOptions() {
		return this.state.chassisAvailable?.[this.chassisClass] || [];
	}

	protected get chassisClass(): UnitChassis.chassisClass {
		if (this.props.chassisClass)
			return this.state.chassisAvailable[this.props.chassisClass]?.length
				? this.props.chassisClass
				: Object.keys(this.state.chassisAvailable)[0];
		if (this.props.selectedChassis) {
			try {
				const c = DIContainer.getProvider(DIInjectableCollectibles.chassis).get(
					this.props.selectedChassis,
				)?.type;
				return this.state.chassisAvailable[c]?.length ? c : Object.keys(this.state.chassisAvailable)[0];
			} catch (e) {
				return Object.keys(this.state.chassisAvailable)[0];
			}
		}
		return Object.keys(this.state.chassisAvailable)[0] || null;
	}

	protected getSelectedChassis(group = this.chassisClass): number {
		if (!group) return null;
		if (doesCollectibleSetIncludeItem(this.props.selectedChassis, this.state.chassisAvailable[group]))
			return this.props.selectedChassis;
		return this.state.chassisAvailable[group][0];
	}

	public static getDerivedStateFromProps(props: UIChassisListSelectorProps, state: UIShiedListSelectorState) {
		return {
			...(state || {}),
			chassisAvailable: UIChassisListSelector.buildChassisHash(props),
		};
	}

	public componentDidUpdate() {
		if (this.getSelectedChassis() !== this.props.selectedChassis && this.props.onChassisSelected)
			this.props.onChassisSelected(this.state.chassisAvailable[this.chassisClass][0]);
	}

	public componentDidMount() {
		if (this.getSelectedChassis() !== this.props.selectedChassis && this.props.onChassisSelected)
			this.props.onChassisSelected(this.state.chassisAvailable[this.chassisClass][0]);
	}

	protected static buildChassisHash(props: UIChassisListSelectorProps) {
		const provider = DIContainer.getProvider(DIInjectableCollectibles.chassis);
		const r = {} as IChassisHash;
		Object.keys(provider.getAll()).forEach((chassisId) => {
			if (!props.player || props.player.isChassisAvailable(chassisId)) {
				const key = provider.get(chassisId).type;
				r[key] = (r[key] || []).concat(parseInt(String(chassisId)));
			}
		});
		return r;
	}

	public constructor(props: UIChassisListSelectorProps) {
		super(props);
		const chassisAvailable = UIChassisListSelector.buildChassisHash(props);
		this.state = {
			chassisAvailable,
		};
	}

	public render() {
		const provider = DIContainer.getProvider(DIInjectableCollectibles.chassis);
		const chassisGroupCaption = (chassisType: UnitChassis.chassisClass) => (
			<UIChassisClassDescriptor chassisType={chassisType} tooltip={true} />
		);
		const onGroupSelect = (chassisType: UnitChassis.chassisClass) =>
			this.props.onChassisSelected(this.getSelectedChassis(chassisType));
		const chassisCaption = (chassisId: number) =>
			chassisId ? (
				<span className={classnames(styles.chassisCaption)}>
					{provider.isUnique(chassisId) && UITargetTypeDescriptor.getIcon(UnitChassis.targetType.unique)}
					{provider.get(chassisId).caption}
				</span>
			) : null;
		const onChassisSelect = (chassisId: number) => this.props.onChassisSelected(chassisId);
		return (
			<div className={[UIChassisListSelector.getClass(), this.props.className].join(' ')}>
				<UIListSelector
					options={
						this.props.chassisClass
							? [this.props.chassisClass]
							: Object.keys(this.state.chassisAvailable).sort(
									predicateComparator<UnitChassis.chassisClass>(
										[
											(t) => provider.isGround(this.state.chassisAvailable[t][0]),
											(t) => provider.isNaval(this.state.chassisAvailable[t][0]),
											(t) => provider.isHover(this.state.chassisAvailable[t][0]),
											(t) => provider.isAir(this.state.chassisAvailable[t][0]),
										],
										(t) => provider.instantiate(null, this.state.chassisAvailable[t][0]).baseCost,
									),
							  )
					}
					selectedItem={this.chassisClass}
					buttonColor={TSVGSmallTexturedIconStyles.gold}
					optionRender={chassisGroupCaption}
					onSelect={onGroupSelect}
				/>
				<UIListSelector
					options={this.chassisOptions}
					selectedItem={this.getSelectedChassis()}
					selectedIndex={findCollectibleItemInSet(this.getSelectedChassis(), this.chassisOptions)}
					buttonColor={TSVGSmallTexturedIconStyles.gold}
					optionRender={chassisCaption}
					onSelect={onChassisSelect}
				/>
			</div>
		);
	}
}

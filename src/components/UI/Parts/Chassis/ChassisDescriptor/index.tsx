import * as React from 'react';
import Players from '../../../../../../definitions/player/types';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import PoliticalFactions from '../../../../../../definitions/world/factions';
import {
	UIBackdropPatternID,
	UIPrototypeBackdropPatternID,
	UIPrototypeBackgroundGradientID,
	UITalentBackdropPatternID,
	UITalentBackgroundGradientID,
	UIUnitBackdropPatternID,
	UIUnitBackgroundGradientID,
} from '../../../../Symbols/backdrops';
import ChassisImageProvider from '../../../../Symbols/ChassisImageProvider';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UIActionCounter from '../../../Prototype/ActionCount';
import UIActionTable from '../../../Prototype/ActionTable';
import UIPrototypeModifier from '../../../Prototype/PrototypeModifier';
import UIRankCollection from '../../../Prototype/RankCollection';
import UIRawMaterialsDescriptor from '../../../Economy/RawMaterialsDescriptor';
import UIBackdrop, { UIBackdropGradientID } from '../../../SVG/Backdrop';
import UITabsWrapper, { UITabsList } from '../../../Templates/Tabs/TabsWrapper';
import UIAbilityDescriptor from '../../Ability/AbilityDescriptor';
import UIAuraDescriptor from '../../Ability/AuraDescriptor';
import UIDefenseFlagsDescriptor from '../../Defense/DefenseFlagsDescriptor';
import UIDodgeDescriptor from '../../Defense/DodgeDescriptor';
import UIChassisClassDescriptor from '../ChassisClassDescriptor';
import UIFactionRestrictionDescriptor from '../FactionRestrictions';
import UIHitPointsDescriptor from '../HitPointsDescriptor';
import UILearningCurveDescriptor from '../LearningCurveDescriptor';
import UILevelingDescriptor from '../LevelingDescriptor';
import UIPromotionClassDescriptor from '../PromotionClassDescriptor';
import UIScanRangeDescriptor from '../ScanRangeDescriptor';
import UIChassisSlotsDescriptor from '../SlotsDescriptor';
import UISpeedDescriptor from '../SpeedDescriptor';
import UITargetTypeDescriptor from '../TargetTypeDescriptor';

import styles from './index.scss';

type UIChassisDescriptorProps = IUICommonProps & {
	chassisId: number;
	player?: Players.IPlayer;
	details?: boolean;
	modifiers?: UnitChassis.TUnitChassisModifier[];
};

type TChassisTabs = 'index' | 'act' | 'mod' | 'lvl';

const ChassisBgPattern: Record<TChassisTabs, string> = {
	index: UIBackdropPatternID,
	act: UIPrototypeBackdropPatternID,
	lvl: UITalentBackdropPatternID,
	mod: UIUnitBackdropPatternID,
};

const ChassisBgGradient: Record<TChassisTabs, string> = {
	index: UIBackdropGradientID,
	act: UIPrototypeBackgroundGradientID,
	lvl: UITalentBackgroundGradientID,
	mod: UIUnitBackgroundGradientID,
};

export default class UIChassisDescriptor extends React.PureComponent<
	UIChassisDescriptorProps,
	{
		selectedTab: TChassisTabs;
	}
> {
	public constructor(props: UIChassisDescriptorProps) {
		super(props);
		this.state = {
			selectedTab: 'index',
		};
	}

	public static getClass(): string {
		return styles.chassisDescriptor;
	}

	public static getCaption() {
		return UIChassisClassDescriptor.getCaption(null);
	}

	public static getIcon(chassisId: number, faction: PoliticalFactions.TFactionID, className = styles.chassisIcon) {
		return (
			<ChassisImageProvider
				type={'topdown'}
				chassisId={chassisId}
				className={className}
				faction={faction || PoliticalFactions.TFactionID.none}
			/>
		);
	}

	public static getEntityCaption() {
		return (
			<span style={{ whiteSpace: 'nowrap' }}>
				{UIChassisClassDescriptor.getIcon()}
				{UIChassisDescriptor.getCaption()}
			</span>
		);
	}

	public render() {
		const chassis = DIContainer.getProvider(DIInjectableCollectibles.chassis).instantiate(
			null,
			this.props.chassisId,
		);
		chassis.applyModifier(...(this.props.modifiers || []));
		return (
			<div className={classnames(UIChassisDescriptor.getClass(), this.props.className || '')}>
				<UIBackdrop
					className={styles.frame}
					gradientId={ChassisBgGradient[this.state.selectedTab]}
					patternId={ChassisBgPattern[this.state.selectedTab]}
				/>
				<div className={classnames(this.props.details ? '' : styles.short, styles.content)}>
					<dl className={styles.meta}>
						<dt className={classnames(UIContainer.descriptorClass, styles.chassisCaption)}>
							<UIChassisClassDescriptor chassisType={chassis.static.type} tooltip={true} />
						</dt>
						<dd className={styles.chassisSlots}>
							<UIChassisSlotsDescriptor chassis={chassis} tooltip={true} short={true} />
						</dd>
					</dl>
					<dl className={styles.meta}>
						<dt
							className={
								chassis.static.flags[UnitChassis.targetType.unique] ? styles.title_unique : styles.title
							}
						>
							{chassis.static.caption}{' '}
						</dt>
						<dd className={styles.cost}>
							<UIRawMaterialsDescriptor
								short={true}
								value={chassis.baseCost}
								className={styles.costDescriptor}
							/>
						</dd>
					</dl>
					<UIFactionRestrictionDescriptor
						{...chassis.static}
						className={styles.restrictions}
						tooltip={true}
					/>
					<UITabsWrapper
						onSelect={(id: TChassisTabs) =>
							this.setState({
								selectedTab: id,
							})
						}
						className={styles.tabs}
						tabs={
							[
								{
									caption: 'Stats',
									id: 'index',
									selected: 'index' === this.state.selectedTab,
									content: (
										<>
											{this.props.details && (
												<div className={styles.description}>
													{UIChassisDescriptor.getIcon(
														this.props.chassisId,
														this.props.player ? this.props.player.faction : null,
														styles.chassisIcon,
													)}
													{chassis.static.description}
												</div>
											)}
											<UITargetTypeDescriptor targetFlags={chassis.static.flags} tooltip={true} />
											<UIHitPointsDescriptor maxHitPoints={chassis.hitPoints} tooltip={true} />
											<UISpeedDescriptor
												movement={chassis.static.movement}
												speed={chassis.speed}
												tooltip={true}
											/>
											<UIScanRangeDescriptor scanRange={chassis.scanRange} tooltip={true} />
											<UIDodgeDescriptor
												dodge={chassis.dodge}
												tooltip={true}
												movement={chassis.static.movement}
											/>
										</>
									),
								},
								{
									caption: 'Actions',
									id: 'act',
									selected: 'act' === this.state.selectedTab,
									content: (
										<>
											<UIActionCounter actions={chassis.actions} tooltip={true} />
											<UIActionTable actions={chassis.actions} tooltip={true} />
											{chassis.static.auras &&
												!!chassis.static.auras.length &&
												chassis.static.auras.map((auraAppliance) => (
													<UIAuraDescriptor
														className={styles.abilityDescriptor}
														key={auraAppliance.id}
														tooltip={true}
														aura={DIContainer.getProvider(
															DIInjectableCollectibles.aura,
														).instantiate(auraAppliance.props || null, auraAppliance.id)}
													/>
												))}
											{chassis.static.abilities &&
												!!chassis.static.abilities.length &&
												chassis.static.abilities.map((abilityId) => (
													<UIAbilityDescriptor
														className={styles.abilityDescriptor}
														key={abilityId}
														ability={DIContainer.getProvider(
															DIInjectableCollectibles.ability,
														).instantiate(null, abilityId)}
													/>
												))}
										</>
									),
								},
								{
									caption: 'Ranks',
									id: 'lvl',
									selected: 'lvl' === this.state.selectedTab,
									content: (
										<>
											<UILearningCurveDescriptor
												levelingFunction={chassis.static.levelTree.levelingFunction}
												tooltip={true}
											/>
											<UILevelingDescriptor
												maxLevel={3}
												levelingFunction={chassis.static.levelTree.levelingFunction}
												className={styles.levelingDescriptor}
											/>
											<UIPromotionClassDescriptor
												promotionClass={chassis.static.levelTree}
												tooltip={true}
											/>
											<UIRankCollection
												tooltip={true}
												className={styles.talentDescriptor}
												talents={chassis.static.levelTree}
												filter={(e) => e.talent.minimumLevel <= 1}
											/>
										</>
									),
								},
								{
									caption: 'Modifiers',
									id: 'mod',
									selected: 'mod' === this.state.selectedTab,
									content: (
										<>
											<UIDefenseFlagsDescriptor
												flags={chassis.defenseFlags}
												short={false}
												tooltip={true}
												className={styles.metaCaption}
											/>
											<UIPrototypeModifier
												modifier={{
													armor: [chassis.static.armorModifier],
													shield: [chassis.static.shieldModifier],
													weapon: [chassis.static.weaponModifier],
													unitModifier: chassis.static.unitEffectModifier,
												}}
											/>
											{(this.props.player?.getUnitModifiers() || []).map((mod, key) => (
												<UIPrototypeModifier modifier={mod} key={key} />
											))}
										</>
									),
								},
							] as UITabsList<TChassisTabs>
						}
					/>
				</div>
			</div>
		);
	}
}

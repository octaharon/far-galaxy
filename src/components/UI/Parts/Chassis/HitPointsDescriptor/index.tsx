import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import { wellRounded } from '../../../../../utils/wellRounded';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer/index';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon/index';
import UITooltip from '../../../Templates/Tooltip';
import UIDamageModifierInlineDescriptor from '../../Damage/DamageModifierInlineDescriptor';
import UIArmorDescriptor from '../../Armor/ArmorDescriptor';
import UIShieldDescriptor from '../../Shield/ShieldDescriptor';
import Style from './index.scss';

type UIHitPointsPureProps = {
	hitPoints?: number;
	maxHitPoints?: number;
	modifier?: IModifier;
	caption?: JSX.Element | string;
};
export type UIHitPointsDescriptorProps = IUICommonProps &
	UIHitPointsPureProps & {
		short?: boolean;
	};

export const SVGHitPointsIconId = 'health-icon';

SVGIconContainer.registerSymbol(
	SVGHitPointsIconId,
	<symbol viewBox="0 0 329.928 329.928">
		<path
			d="M115.604,278.4c5.95,5.224,12.092,10.614,18.41,16.202c0.043,0.038,0.087,0.077,0.131,0.115l21.018,18.155
		c2.816,2.433,6.311,3.649,9.806,3.649s6.991-1.217,9.807-3.649l21.014-18.155c0.044-0.038,0.087-0.076,0.13-0.114
		c41.607-36.796,72.802-64.966,95.371-92.44c26.36-32.088,38.638-61.101,38.638-91.305c0-54.646-42.805-97.451-97.449-97.451
		c-24.56,0-48.827,9.248-67.511,25.279c-18.689-16.032-42.956-25.279-67.517-25.279C42.806,13.406,0,56.212,0,110.857
		C0,176.912,45.99,217.286,115.604,278.4z M104.964,134.965h39.999V94.964h40.001v40.001h40v40h-40v39.999h-40.001v-39.999h-39.999
		V134.965z"
		/>
	</symbol>,
);

export default class UIHitPointsDescriptor extends React.PureComponent<UIHitPointsDescriptorProps, {}> {
	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.red} className={className}>
				<use {...SVGIconContainer.getXLinkProps(SVGHitPointsIconId)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption() {
		return 'Hit Points';
	}

	public static getTooltip() {
		return (
			<span>
				{UIHitPointsDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UIHitPointsDescriptor.getCaption()}</b> is a measure of Unit's integrity. When taking{' '}
				<UIDamageModifierInlineDescriptor modifier={null} />, it's first absorbed by{' '}
				{UIShieldDescriptor.getEntityCaption()} and reduced by {UIArmorDescriptor.getEntityCaption()}, and then
				is subtracted from <em>Hit Points</em>, and when none are left, the Unit is destroyed
			</span>
		);
	}

	public static getHitPointsValue(props: UIHitPointsPureProps) {
		const { maxHitPoints, hitPoints, modifier } = props;
		const modOnly = !Number.isFinite(hitPoints) && !Number.isFinite(maxHitPoints);
		if (modOnly && !modifier) return 'Unknown';
		const mod = <span className={Style.modifier}>{BasicMaths.describeModifier(modifier).join('/').trim()}</span>;
		return (
			<>
				{Number.isFinite(hitPoints) && (
					<>
						<span className={hitPoints > 0 ? Style.max : Style.zero}>{wellRounded(hitPoints)}</span>
						{maxHitPoints ? '/' : ''}
					</>
				)}
				{Number.isFinite(maxHitPoints) && <span className={Style.max}>{wellRounded(maxHitPoints)}</span>}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	public render() {
		const value = UIHitPointsDescriptor.getHitPointsValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						UIContainer.captionClass,
						Style.hitPointsDescriptor,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
						this.props.className || '',
					)}>
					{UIHitPointsDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && <UITooltip>{UIHitPointsDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(this.props.className || '', Style.hitPointsDescriptor)}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIHitPointsDescriptor.getIcon()}
					<span className={Style.caption}>{this.props.caption || UIHitPointsDescriptor.getCaption()}</span>
					{!!this.props.tooltip && <UITooltip>{UIHitPointsDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

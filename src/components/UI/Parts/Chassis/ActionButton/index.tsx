import * as React from 'react';
import UnitActions from '../../../../../../definitions/tactical/units/actions/types';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIIconValuePlaque from '../../../Templates/ValuePlaque';
import UIActionDescriptor from '../ActionDescriptor';
import Style from './index.scss';

const UIActionButton = ({
	actionType,
	tooltip = false,
	className,
}: { actionType: UnitActions.unitActionTypes } & IUICommonProps) => (
	<span
		key={actionType}
		className={classnames(Style.actionLabel, className, tooltip ? UIContainer.hasTooltipClass : '')}
	>
		<UIIconValuePlaque
			withFrame={true}
			className={classnames(Style.actionPlaque, className)}
			icon={UIActionDescriptor.getIcon(actionType, true)}
		>
			{UIActionDescriptor.getCaption(actionType)}
		</UIIconValuePlaque>
		{tooltip && <UITooltip>{UIActionDescriptor.getTooltip(actionType)}</UITooltip>}
	</span>
);

export default UIActionButton;

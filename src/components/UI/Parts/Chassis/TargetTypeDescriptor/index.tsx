import * as React from 'react';
import { TargetTypeOrderWithDefault } from '../../../../../../definitions/technologies/chassis/constants';
import { isOfTargetType } from '../../../../../../definitions/technologies/chassis/helpers';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import { termTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import styles from './index.scss';

const targetTypeDescriptorClass = styles.targetTypeDescriptor;

export interface UITargetTypeDescriptorProps extends IUICommonProps {
	targetFlags: UnitChassis.TTargetFlags;
	short?: boolean;
	tooltip?: boolean;
}

export const TargetTypeIcon_Bio_ID = 'target-type-bio';
export const TargetTypeIcon_Massive_ID = 'target-type-massive';
export const TargetTypeIcon_Robotic_ID = 'target-type-robotic';
export const TargetTypeIcon_Unique_ID = 'target-type-unique';
export const TargetTypeIcon_Mechanical_ID = 'target-type-mechanical';

const iconMapping: EnumProxyWithDefault<UnitChassis.targetType, string> = {
	[UnitChassis.targetType.robotic]: TargetTypeIcon_Robotic_ID,
	[UnitChassis.targetType.organic]: TargetTypeIcon_Bio_ID,
	[UnitChassis.targetType.massive]: TargetTypeIcon_Massive_ID,
	[UnitChassis.targetType.unique]: TargetTypeIcon_Unique_ID,
	default: TargetTypeIcon_Mechanical_ID,
};

SVGIconContainer.registerSymbol(
	TargetTypeIcon_Mechanical_ID,
	<symbol viewBox="0 0 64 64">
		<path d="m29.68981 15.615c-7.6606.24525-7.66791 11.37007.00012 11.61005 7.68207-.24505 7.67112-11.36205-.00012-11.61005z" />
		<path d="m25.09978 41.415-1.56995-2.71a1.00263 1.00263 0 0 0 -1.1-.47l-1.82.44a9.194 9.194 0 0 0 -2.94-1.7l-.53-1.8a1.01743 1.01743 0 0 0 -.96-.72h-3.13a1.0173 1.0173 0 0 0 -.96.72l-.53 1.8a9.41374 9.41374 0 0 0 -2.94 1.7l-1.82-.44a1.00861 1.00861 0 0 0 -1.1.47l-1.57 2.71a.999.999 0 0 0 .15 1.19l1.29 1.36a8.595 8.595 0 0 0 0 3.39l-1.29 1.36a.999.999 0 0 0 -.15 1.19l1.57 2.71a.99462.99462 0 0 0 1.1.47l1.82-.44a9.22305 9.22305 0 0 0 2.94 1.7l.53 1.8a1.01731 1.01731 0 0 0 .96.72h3.13a1.01744 1.01744 0 0 0 .96-.72l.53-1.8a9.0123 9.0123 0 0 0 2.94-1.7l1.82.44a1.00263 1.00263 0 0 0 1.1-.47l1.56995-2.71a1.01238 1.01238 0 0 0 -.13995-1.19l-1.3-1.36a8.53308 8.53308 0 0 0 0-3.39l1.3-1.36a1.01238 1.01238 0 0 0 .13995-1.19zm-4.91 4.25c-.23564 7.36185-10.912 7.35906-11.14-.00012.22622-7.37468 10.90836-7.37404 11.14003.00012z" />
		<path d="m14.6198 42.085a3.57527 3.57527 0 0 0 .00011 7.15 3.57527 3.57527 0 0 0 -.00011-7.15z" />
		<path d="m32.01982 37.605a1.01637 1.01637 0 0 0 .96-.71l.86005-2.92a13.36946 13.36946 0 0 0 4.65-2.68l2.95.71a1.00239 1.00239 0 0 0 1.1-.47l1.7-2.94c.27643-.58275 1.14125-1.51184.58-2.17-.03-.04-2.19-2.32-2.19-2.32a12.57893 12.57893 0 0 0 0-5.36l2.1-2.21a.99546.99546 0 0 0 .14-1.19l-2.33-4.03a.97547.97547 0 0 0 -1.1-.47l-2.95.71a13.21489 13.21489 0 0 0 -4.65-2.68l-.86-2.92a1.005 1.005 0 0 0 -.96-.72h-4.66a1.005 1.005 0 0 0 -.96.72l-.86005 2.92a13.08547 13.08547 0 0 0 -4.63995 2.68l-2.96-.71a.97548.97548 0 0 0 -1.1.47l-2.33 4.03a.99545.99545 0 0 0 .14 1.19l2.1 2.21a13.00415 13.00415 0 0 0 -.29 2.68 13.15278 13.15278 0 0 0 .29 2.68l-2.1 2.21a.98239.98239 0 0 0 -.14 1.18l2.33 4.04a.99459.99459 0 0 0 1.1.47l2.96-.71a13.23733 13.23733 0 0 0 4.63995 2.68l.86005 2.92a1.00825 1.00825 0 0 0 .96.71zm-2.33-8.38a7.81 7.81 0 0 1 -7.8-7.8c.39022-10.35145 15.21737-10.3391 15.61.0001a7.81862 7.81862 0 0 1 -7.81001 7.7999z" />
		<path d="m59.96978 40.555-1.21-4.5a.98422.98422 0 0 0 -.94-.74l-3.05-.08a12.57983 12.57983 0 0 0 -3.79-3.78l-.07-3.05a.99323.99323 0 0 0 -.74-.94l-3.18-.85a3.08244 3.08244 0 0 1 -.43 1.95l-2.28 3.97a2.99784 2.99784 0 0 1 -3.31 1.41l-1.98-.47a15.38357 15.38357 0 0 1 -3.52 2.02l-.56995 1.96a2.99646 2.99646 0 0 1 -1.96 2 7.28912 7.28912 0 0 1 -2.1.15 11.76585 11.76585 0 0 0 -.30994 2.13l-2.6 1.58a.98885.98885 0 0 0 -.44 1.11l1.2 4.51a.99323.99323 0 0 0 .94.74l3.05.08a12.75776 12.75776 0 0 0 3.79 3.78l.07 3.05a.99329.99329 0 0 0 .74.94l4.51 1.2a.74686.74686 0 0 0 .26.04 1.01091 1.01091 0 0 0 .85-.48l1.58-2.59a13.2755 13.2755 0 0 0 5.19-1.39l2.67 1.45a.97015.97015 0 0 0 1.18-.17l3.3-3.29a.98659.98659 0 0 0 .18-1.18005l-1.46-2.68a13.17035 13.17035 0 0 0 1.38-5.18l2.6-1.59a1.01428 1.01428 0 0 0 .44989-1.10995zm-21.76-3.57995c4.749-4.89919 13.43889-1.3234 13.31991 5.52014.09773 6.8476-8.53422 10.41939-13.32 5.51966a7.82185 7.82185 0 0 1 .00014-11.03985z" />
		<path d="m47.82982 46.595c5.26693-5.58322-2.62514-13.46194-8.21011-8.19992-5.2612 5.58186 2.62912 13.46397 8.21011 8.19992z" />
	</symbol>,
)
	.registerSymbol(
		TargetTypeIcon_Bio_ID,
		<symbol viewBox="0 0 512 512">
			<g
				style={{
					transformOrigin: '50% 50%',
					transform: 'translate(-2%, 0) scale(1.03, 1)',
				}}>
				<path
					d="M510.792,151.326L424.012,4.27C422.45,1.631,419.612,0,416.54,0H251.659c-3.072,0-5.91,1.631-7.472,4.27l-86.78,147.057
				c-1.649,2.786-1.605,6.248,0.104,8.999l2.846,4.556h26.433c9.068,0,17.33,4.591,22.111,12.288l64.85,104.118
				c4.261,6.821,4.877,15.039,2.23,22.441H416.54c2.994,0,5.78-1.545,7.368-4.096l86.78-139.307
				C512.397,157.575,512.441,154.112,510.792,151.326z M390.507,52.068H282.544l-66.881,108.683
				c-1.64,2.664-4.487,4.131-7.402,4.131c-1.553,0-3.124-0.417-4.539-1.284c-4.087-2.508-5.354-7.862-2.846-11.941l69.424-112.814
				c1.588-2.569,4.382-4.131,7.394-4.131h112.814c4.79,0,8.678,3.879,8.678,8.678S395.297,52.068,390.507,52.068z"
				/>
				<path
					d="M198.411,403.56l60.746-104.136c1.623-2.777,1.571-6.231-0.13-8.956l-64.868-104.136
				c-1.588-2.551-4.365-4.096-7.368-4.096H69.422c-3.081,0-5.944,1.631-7.498,4.304L1.178,290.677c-1.571,2.708-1.571,6.04,0,8.747
				L61.924,403.56c1.553,2.673,4.417,4.304,7.498,4.304h121.492C193.994,407.864,196.858,406.233,198.411,403.56z M164.879,234.305
				h-64.616l-40.839,65.362c-1.649,2.629-4.469,4.079-7.368,4.079c-1.571,0-3.159-0.425-4.591-1.319
				c-4.061-2.534-5.302-7.897-2.76-11.958l43.39-69.441c1.579-2.534,4.365-4.079,7.359-4.079h69.424c4.79,0,8.678,3.879,8.678,8.678
				C173.557,230.426,169.67,234.305,164.879,234.305z"
				/>
				<path
					d="M400.006,321.085H266.617l-53.213,91.223c-3.827,6.561-10.388,10.891-17.72,12.279l48.484,83.109
				c1.553,2.673,4.408,4.304,7.489,4.304h121.491c3.194,0,6.127-1.753,7.645-4.565l60.746-112.814
				c1.493-2.768,1.362-6.118-0.321-8.773L400.006,321.085z"
				/>
			</g>
		</symbol>,
	)
	.registerSymbol(
		TargetTypeIcon_Robotic_ID,
		<symbol viewBox="0 0 512 512">
			<path d="m201.774 302.69c9.558 0 17.333-7.775 17.333-17.333s-7.776-17.333-17.333-17.333-17.333 7.776-17.333 17.333 7.776 17.333 17.333 17.333z" />
			<path d="m256 114c-13.602 0-26.762 1.927-39.226 5.514v120.96c18.767 6.289 32.333 24.026 32.333 44.883 0 26.1-21.234 47.333-47.333 47.333s-47.333-21.233-47.333-47.333c0-20.857 13.566-38.594 32.333-44.883v-108.417c-43.384 24.327-72.774 70.764-72.774 123.943 0 78.299 63.701 142 142 142 13.602 0 26.762-1.927 39.226-5.514v-120.96c-18.767-6.289-32.333-24.026-32.333-44.883 0-26.1 21.234-47.333 47.333-47.333s47.333 21.233 47.333 47.333c0 20.857-13.566 38.594-32.333 44.883v108.417c43.384-24.327 72.774-70.764 72.774-123.943 0-78.299-63.701-142-142-142z" />
			<path d="m310.226 209.31c-9.558 0-17.333 7.775-17.333 17.333s7.776 17.333 17.333 17.333 17.333-7.776 17.333-17.333-7.776-17.333-17.333-17.333z" />
			<path d="m480.742 202.435c-5.661-23.907-15.033-46.522-27.957-67.464l18.7-25.523-68.933-68.934-25.523 18.701c-20.941-12.924-43.557-22.295-67.464-27.957l-4.822-31.258h-97.486l-4.822 31.258c-23.907 5.662-46.522 15.033-67.464 27.957l-25.523-18.701-68.933 68.934 18.7 25.523c-12.924 20.941-22.295 43.557-27.957 67.464l-31.258 4.822v97.486l31.258 4.822c5.661 23.907 15.033 46.522 27.957 67.464l-18.7 25.524 68.932 68.932 25.524-18.7c20.941 12.924 43.557 22.295 67.464 27.957l4.822 31.258h97.486l4.822-31.258c23.907-5.661 46.522-15.033 67.464-27.957l25.524 18.7 68.932-68.932-18.7-25.524c12.924-20.942 22.296-43.558 27.957-67.464l31.258-4.822v-97.486zm-224.742 225.565c-94.841 0-172-77.159-172-172s77.159-172 172-172 172 77.159 172 172-77.159 172-172 172z" />
		</symbol>,
	)
	.registerSymbol(
		TargetTypeIcon_Massive_ID,
		<symbol viewBox="0 0 512 512">
			<polygon points="316.349,122 195.651,122 161,213.248 161,232 351,232 351,213.248 		" />
			<polygon
				points="396.849,262 276.151,262 256,315.065 235.849,262 115.151,262 80.5,353.248 80.5,372 431.5,372 431.5,353.248
			"
			/>
			<polygon
				points="477.349,402 356.651,402 336.5,455.065 316.349,402 195.651,402 175.5,455.065 155.349,402 34.651,402 0,493.248
			0,512 512,512 512,493.248 		"
			/>
			<rect x="241" width="30" height="70" />
			<rect
				x="158.231"
				y="14.595"
				transform="matrix(0.9397 -0.342 0.342 0.9397 -6.5158 62.2356)"
				width="30"
				height="70"
			/>
			<rect
				x="85.45"
				y="56.619"
				transform="matrix(0.766 -0.6428 0.6428 0.766 -35.3904 86.0071)"
				width="29.998"
				height="69.998"
			/>
			<rect
				x="31.421"
				y="120.997"
				transform="matrix(0.5 -0.866 0.866 0.5 -111.8853 118.1977)"
				width="29.999"
				height="69.997"
			/>
			<rect
				x="430.589"
				y="141.004"
				transform="matrix(0.866 -0.5 0.5 0.866 -15.6236 253.7008)"
				width="69.997"
				height="29.999"
			/>
			<rect
				x="376.544"
				y="76.62"
				transform="matrix(0.6428 -0.766 0.766 0.6428 76.8144 347.9752)"
				width="69.998"
				height="29.998"
			/>
			<rect
				x="303.771"
				y="34.593"
				transform="matrix(0.342 -0.9397 0.9397 0.342 176.3089 350.9752)"
				width="70"
				height="30"
			/>
		</symbol>,
	)
	.registerSymbol(
		TargetTypeIcon_Unique_ID,
		<symbol viewBox="0 0 49.655 49.655">
			<path
				d="M24.827,0C11.138,0,0,11.137,0,24.826c0,13.688,11.137,24.828,24.826,24.828c13.688,0,24.827-11.14,24.827-24.828
			C49.653,11.137,38.516,0,24.827,0z M38.543,23.011L38.543,23.011l-7.188,5.201l2.769,8.46c0.044,0.133,0.067,0.273,0.067,0.417
			c0,0.739-0.6,1.339-1.339,1.339c-0.293,0-0.562-0.094-0.782-0.252l-7.244-5.24l-7.243,5.24c-0.22,0.158-0.49,0.252-0.783,0.252
			c-0.74,0-1.339-0.599-1.339-1.339c0-0.146,0.024-0.284,0.068-0.417l2.769-8.46l-7.187-5.2v-0.001
			c-0.336-0.242-0.554-0.637-0.554-1.083c0-0.739,0.598-1.337,1.338-1.337h8.897l2.755-8.421c0.168-0.547,0.677-0.945,1.28-0.945
			c0.603,0,1.112,0.398,1.28,0.945l2.754,8.421h8.896c0.739,0,1.338,0.598,1.338,1.337C39.096,22.374,38.878,22.769,38.543,23.011z"
			/>
		</symbol>,
	);

export default class UITargetTypeDescriptor extends React.PureComponent<UITargetTypeDescriptorProps, {}> {
	public static getClass(): string {
		return targetTypeDescriptorClass;
	}

	public static getTargetTooltip(t: UnitChassis.targetType) {
		const extras: EnumProxyWithDefault<UnitChassis.targetType, string> = {
			[UnitChassis.targetType.robotic]:
				'Robotic Units take extra +25% EMG and RAD Damage, but gain +5% XP for every other Robotic Unit in 3 Effect Radius',
			[UnitChassis.targetType.organic]:
				'Organic Units take extra +25% TRM and RAD Damage, and +50% BIO Damage. However, they deal +5% Weapons Damage for every other Organic Unit in 3 Effect Radius',
			[UnitChassis.targetType.unique]:
				'Unique Units take +50% ANH Damage, and each Player can support only 1 Unique Unit on the battlefield',
			[UnitChassis.targetType.massive]: 'Unique Units are twice as big as regular, and take +50% WRP Damage',
			default: 'Mechanical Units have no special properties',
		};
		const descriptions: EnumProxyWithDefault<UnitChassis.targetType, string> = {
			[UnitChassis.targetType.robotic]:
				'These Units are totally autonomous self-aware machines, programmed to perform orders, but still possessing a somewhat of a personality. The destruction of Robotic Units is considered a murder in some Factions.',
			[UnitChassis.targetType
				.organic]: `Those Units are actual living beings on the battleground. Killing them is considered a murder in some Factions.`,
			[UnitChassis.targetType.unique]:
				'Such Units require a special operational group, additional communication protocols and a huge orbital power supply, and generally have much more value than any other single Unit.',
			[UnitChassis.targetType.massive]:
				'Those Units are of exceptional size. They occupy more space on the battlefield and usually are pretty slow, but they have exceptional firepower and combat capabilities',
			default:
				'Mechanical Units have neither life nor conciousness and are basically machines that are operated by remote operators. Destroying them is routine and is only a matter of cost',
		};
		return termTooltipTemplate(
			UITargetTypeDescriptor.getIcon(t, UITooltip.getIconClass()),
			UITargetTypeDescriptor.getCaption(t) + ' Unit',
			descriptions[t] ?? descriptions.default,
			<>
				<br />
				<br />
				<Tokenizer description={extras[t] ?? extras.default} />
			</>,
		);
	}

	public static getCaption(t: UnitChassis.targetType | 'default'): string {
		switch (t) {
			case UnitChassis.targetType.massive:
				return 'Massive';
			case UnitChassis.targetType.robotic:
				return 'Robotic';
			case UnitChassis.targetType.organic:
				return 'Organic';
			case UnitChassis.targetType.unique:
				return 'Unique';
			default:
				return 'Mechanical';
		}
	}

	public static getEntityCaption(t: UnitChassis.targetType | 'default') {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UITargetTypeDescriptor.getIcon(t)}
				<span className={UITargetTypeDescriptor.getCaptionClass(t)}>
					{UITargetTypeDescriptor.getCaption(t)}
				</span>
			</em>
		);
	}

	public static getCaptionClass(t: UnitChassis.targetType | 'default') {
		return t === UnitChassis.targetType.unique ? styles.targetTypeUnique : styles.targetTypeCaption;
	}

	public static getIcon(type: UnitChassis.targetType | 'default', className?: string) {
		return (
			<SVGSmallTexturedIcon
				className={[className || '', UIContainer.iconClass].join(' ')}
				style={
					type === UnitChassis.targetType.unique
						? TSVGSmallTexturedIconStyles.gold
						: TSVGSmallTexturedIconStyles.blue
				}
				asSvg={true}>
				<use {...SVGIconContainer.getXLinkProps(iconMapping[type])} />
			</SVGSmallTexturedIcon>
		);
	}

	public render() {
		const topClass = classnames(UITargetTypeDescriptor.getClass(), this.props.className);
		const itemClass = classnames(styles.item, this.props.tooltip ? UIContainer.hasTooltipClass : '');
		return this.props.short ? (
			<span className={topClass}>
				{TargetTypeOrderWithDefault.map((t: UnitChassis.targetType) =>
					isOfTargetType(t)(this.props.targetFlags) ? (
						<span className={itemClass} key={t}>
							{UITargetTypeDescriptor.getIcon(t)}
							{!!this.props.tooltip && (
								<UITooltip>{UITargetTypeDescriptor.getTargetTooltip(t)}</UITooltip>
							)}
						</span>
					) : null,
				)}
			</span>
		) : (
			<dl className={topClass}>
				{TargetTypeOrderWithDefault.map((t: UnitChassis.targetType) =>
					isOfTargetType(t)(this.props.targetFlags) ? (
						<dt className={classnames(UIContainer.descriptorClass, itemClass)} key={t}>
							{UITargetTypeDescriptor.getIcon(t)}
							<span className={UITargetTypeDescriptor.getCaptionClass(t)}>
								{UITargetTypeDescriptor.getCaption(t)}
							</span>
							{!!this.props.tooltip && (
								<UITooltip>{UITargetTypeDescriptor.getTargetTooltip(t)}</UITooltip>
							)}
						</dt>
					) : null,
				)}
			</dl>
		);
	}
}

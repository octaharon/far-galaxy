import * as React from 'react';
import UnitTalents from '../../../../../../definitions/tactical/units/levels/unitTalents';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UIRankCaption from '../../../Prototype/Rank';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import UILearningCurveDescriptor from '../LearningCurveDescriptor';
import Style from './index.scss';

type UIPromotionClassProps = IUICommonProps & {
	promotionClass: UnitTalents.ITalentTree;
};

export default class UIPromotionClassDescriptor extends React.PureComponent<UIPromotionClassProps, {}> {
	public static getIcon(className: string = UIContainer.iconClass) {
		return UIRankCaption.getIcon(true, className);
	}

	public static getCaption() {
		return 'Promotion Class';
	}

	public static getTooltip() {
		return (
			<span>
				{UIPromotionClassDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UIPromotionClassDescriptor.getCaption()}</b> describes a <em>{UIRankCaption.getEntityCaption()}</em>{' '}
				progression available to a <em>Unit</em>, defining how it evolves along its{' '}
				<em>{UILearningCurveDescriptor.getEntityCaption()}</em>
			</span>
		);
	}

	public static getEntityCaption() {
		return (
			<span style={{ whiteSpace: 'nowrap' }} className={Style.caption}>
				{UIPromotionClassDescriptor.getIcon()}
				{UIPromotionClassDescriptor.getCaption()}
			</span>
		);
	}

	public render() {
		return (
			<dl
				className={classnames(
					this.props.className || '',
					Style.promotionClassDescriptor,
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				)}
				onClick={this.props.onClick || null}
			>
				<dt className={classnames(UIContainer.captionClass, UIContainer.descriptorClass)}>
					{UIPromotionClassDescriptor.getIcon(Style.classIcon)}
					<span className={Style.promotionClassTitle}>{this.props.promotionClass.caption}</span>
				</dt>
				<dt className={Style.description}>
					<Tokenizer description={this.props.promotionClass.description} />
				</dt>
				{!!this.props.tooltip && <UITooltip>{UIPromotionClassDescriptor.getTooltip()}</UITooltip>}
			</dl>
		);
	}
}

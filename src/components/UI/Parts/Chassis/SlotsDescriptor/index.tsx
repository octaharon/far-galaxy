import * as React from 'react';
import _ from 'underscore';
import { IChassis } from '../../../../../../definitions/technologies/types/chassis';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UIArmorSlotDescriptor from '../../Armor/ArmorSlotDescriptor';
import UIShieldSlotDescriptor from '../../Shield/ShieldSlotDescriptor';
import UIEquipmentSlotDescriptor from '../../Equipment/EquipmentSlotDescriptor';
import UIWeaponSlotDescriptor from '../../Weapon/WeaponSlotDescriptor';
import Style from './index.scss';

type UIChassisSlotsDescriptorProps = IUICommonProps & {
	chassis: IChassis;
	tooltip?: boolean;
	short?: boolean;
};

export default class UIChassisSlotsDescriptor extends React.PureComponent<UIChassisSlotsDescriptorProps, {}> {
	public static getCaption(): string {
		return 'Slots';
	}

	public static getClass(): string {
		return Style.slotsOptions;
	}

	public render() {
		const self = this.constructor as typeof UIChassisSlotsDescriptor;
		const { short = false, chassis, tooltip = false, className = '' } = this.props;
		if (!chassis) return null;
		const itemClass = classnames(self.getClass(), className);
		const slots = (
			<>
				{chassis.static.weaponSlots.map((ws, ix) => (
					<UIWeaponSlotDescriptor
						className={Style.slotIcon}
						highlight={true}
						key={`ws-${ix}`}
						slotType={ws}
						tooltip={tooltip}
					/>
				))}
				{chassis.static.armorSlots.map((ws, ix) => (
					<UIArmorSlotDescriptor
						className={Style.slotIcon}
						key={`ar-${ix}`}
						slotType={ws}
						tooltip={tooltip}
					/>
				))}
				{chassis.static.shieldSlots.map((ws, ix) => (
					<UIShieldSlotDescriptor
						className={Style.slotIcon}
						key={`sh-${ix}`}
						slotType={ws}
						tooltip={tooltip}
					/>
				))}
				{_.range(chassis.equipmentSlots).map((ix) => (
					<UIEquipmentSlotDescriptor key={`eq-${ix}`} className={Style.slotIcon} tooltip={true} />
				))}
			</>
		);
		return short ? (
			<span className={itemClass}>{slots}</span>
		) : (
			<dl className={itemClass}>
				<dt className={classnames(UIContainer.descriptorClass, Style.slotsCaption)}>{self.getCaption()}</dt>
				<dd>{slots}</dd>
			</dl>
		);
	}
}

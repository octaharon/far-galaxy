import React from 'react';
import _ from 'underscore';
import UnitTalents from '../../../../../../definitions/tactical/units/levels/unitTalents';
import romanize from '../../../../../utils/romanize';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../../index';
import UIRankCaption from '../../../Prototype/Rank';
import UIXPCaption from '../../../Prototype/XP';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSegmentedBar from '../../../SVG/SegmentedBar';
import styles from './index.scss';

type UILevelingDescriptorProps = IUICommonProps & {
	levelingFunction: UnitTalents.TUnitLevelingSignature;
	currentXp?: number;
	maxLevel?: number;
};

export const UILevelBarGradientID = 'level-gradient';
export const UIXPBarGradientID = 'xp-gradient';

const SVGWidth = 320;

SVGIconContainer.registerGradient(
	UILevelBarGradientID,
	<linearGradient x1="0%" x2="0%" y1="0%" y2="100%" gradientUnits="objectBoundingBox">
		<stop stopColor={getTemplateColor('text_dark')} offset={0} />
		<stop stopColor={getTemplateColor('dark_green')} offset={1} />
	</linearGradient>,
).registerGradient(
	UIXPBarGradientID,
	<linearGradient x1="0%" x2="0%" y1="0%" y2="100%" gradientUnits="objectBoundingBox">
		<stop stopColor={getTemplateColor('golden')} offset={0} />
		<stop stopColor={getTemplateColor('light_red')} offset={1} />
	</linearGradient>,
);

const UILevelingDescriptor: React.FC<UILevelingDescriptorProps> = ({
	className,
	levelingFunction,
	currentXp,
	tooltip,
	maxLevel,
}) => {
	let currentLevel = 0;
	let totalXp = 0;
	while (currentXp >= levelingFunction(currentLevel + 1)) currentLevel += 1;
	maxLevel = Math.max(maxLevel ?? currentLevel + 3, currentLevel + 1);
	const segments = _.range(currentLevel, maxLevel).map((ix) => {
		const lXp = levelingFunction(ix + 1) - levelingFunction(ix);
		totalXp += lXp;
		return {
			value: lXp,
			gradientId: UILevelBarGradientID,
			isXp: false,
			level: ix + 1,
			totalOffset: totalXp,
		};
	});
	if (currentXp) {
		const cxp = currentXp - levelingFunction(currentLevel);
		segments[0].value -= cxp;
		segments.unshift({
			gradientId: UIXPBarGradientID,
			value: cxp,
			isXp: true,
			level: segments[0].level,
			totalOffset: 0,
		});
	}
	return (
		<dl className={classnames(styles.levelingDescriptor, className)}>
			<dt className={styles.xpCaptions}>
				<span className={classnames(styles.txtLabel)} style={{ left: 0 }}>
					<UIXPCaption value={currentXp} tooltip={true} />
				</span>
				{segments
					.filter((v) => !v.isXp && v.totalOffset && v.level < maxLevel)
					.map((lvl) => (
						<span
							className={styles.txtLabel}
							key={lvl.totalOffset}
							style={{ left: ((100 * lvl.totalOffset) / totalXp).toFixed(1) + '%' }}>
							<b>{levelingFunction(lvl.level)}</b>
						</span>
					))}
				<span className={styles.txtLabel} style={{ right: 0 }}>
					<b>{levelingFunction(maxLevel)}</b>
				</span>
			</dt>
			<dt className={classnames(UIContainer.descriptorClass)}>
				<SVGSegmentedBar
					width={0}
					className={styles.levelBar}
					strokeColor={getTemplateColor('light_orange')}
					segments={segments}
				/>
			</dt>
			<dt className={styles.levelCaptions}>
				<span className={styles.txtLabel} style={{ left: 0 }}>
					<UIRankCaption value={currentLevel} tooltip={true} highlight={true} />
				</span>
				{segments
					.filter((v) => !v.isXp && v.totalOffset && v.level < maxLevel)
					.map((lvl) => (
						<span
							className={styles.txtLabel}
							key={lvl.totalOffset}
							style={{ left: ((100 * lvl.totalOffset) / totalXp).toFixed(1) + '%' }}>
							<b>{romanize(lvl.level)}</b>
						</span>
					))}
				<span className={styles.txtLabel} style={{ right: 0 }}>
					<b>{romanize(maxLevel)}</b>
				</span>
			</dt>
		</dl>
	);
};

export default UILevelingDescriptor;

import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import { wellRounded } from '../../../../../utils/wellRounded';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer/index';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon/index';
import UITooltip from '../../../Templates/Tooltip';
import Style from './index.scss';

type UIScanDescriptorPureProps = {
	scanRange?: number;
	modifier?: IModifier;
};
export type UIScanRangeDescriptorProps = IUICommonProps &
	UIScanDescriptorPureProps & {
		short?: boolean;
	};

export const SVGScanRangeIconID = 'ScanRange-icon';

SVGIconContainer.registerSymbol(
	SVGScanRangeIconID,
	<symbol viewBox="0 0 426.667 426.667">
		<path
			d="M364.16,62.507l-30.08,30.08C364.8,123.52,384,166.187,384,213.333C384,307.627,307.627,384,213.333,384
			S42.667,307.627,42.667,213.333c0-87.04,65.067-158.72,149.333-169.173v43.093c-60.587,10.24-106.667,62.72-106.667,126.08
			c0,70.613,57.387,128,128,128s128-57.387,128-128c0-35.413-14.293-67.413-37.547-90.453l-30.08,30.08
			c15.36,15.573,24.96,36.907,24.96,60.373c0,47.147-38.187,85.333-85.333,85.333S128,260.48,128,213.333
			c0-39.68,27.307-72.747,64-82.347v45.653c-12.8,7.467-21.333,20.907-21.333,36.693c0,23.467,19.2,42.667,42.667,42.667
			S256,236.8,256,213.333c0-15.787-8.533-29.44-21.333-36.693V0h-21.333C95.573,0,0,95.573,0,213.333
			s95.573,213.333,213.333,213.333s213.333-95.573,213.333-213.333C426.667,154.453,402.773,101.12,364.16,62.507z"
		/>
	</symbol>,
);

export default class UIScanRangeDescriptor extends React.PureComponent<UIScanRangeDescriptorProps, {}> {
	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.blue} className={className}>
				<use {...SVGIconContainer.getXLinkProps(SVGScanRangeIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption() {
		return 'Scan Range';
	}

	public static getTooltip() {
		return (
			<span>
				{UIScanRangeDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UIScanRangeDescriptor.getCaption()}</b> is a distance at which a Unit provides additional{' '}
				<em>Reconnaissance</em>. Scan Range is expressed in the same <em>Distance Units</em> as <em>Range</em>{' '}
				and <em>Speed</em>
			</span>
		);
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIScanRangeDescriptor.getIcon()}
				<span className={Style.scanCaption}>{UIScanRangeDescriptor.getCaption()}</span>
			</em>
		);
	}

	public static getScanRangeValue(props: UIScanRangeDescriptorProps) {
		const { scanRange, modifier } = props;
		const modOnly = !Number.isFinite(scanRange);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = BasicMaths.applyModifier(100, modifier) >= 100;
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, true).trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(scanRange) && (
					<span className={scanRange > 0 ? Style.max : Style.zero}>{wellRounded(scanRange)}</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	public render() {
		const value = UIScanRangeDescriptor.getScanRangeValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						Style.scanRangeDescriptor,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIScanRangeDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && <UITooltip>{UIScanRangeDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(this.props.className || '', Style.scanRangeDescriptor)}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIScanRangeDescriptor.getIcon()}
					<span className={Style.scanCaption}>{UIScanRangeDescriptor.getCaption()}</span>
					{!!this.props.tooltip && <UITooltip>{UIScanRangeDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

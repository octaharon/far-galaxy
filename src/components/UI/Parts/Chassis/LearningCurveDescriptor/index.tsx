import * as React from 'react';
import {
	LevelingFunctions,
	TLevelingFunction,
} from '../../../../../../definitions/tactical/units/levels/levelingFunctions';
import UnitTalents from '../../../../../../definitions/tactical/units/levels/unitTalents';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UIXPCaption from '../../../Prototype/XP';
import UITooltip from '../../../Templates/Tooltip';
import Style from './index.scss';

type UILearningCurveProps = IUICommonProps & {
	levelingFunction: UnitTalents.TUnitLevelingSignature;
};

const LevelingCurveCaptions: Partial<Record<TLevelingFunction, string>> = {
	[TLevelingFunction.Linear50]: 'Recruit',
	[TLevelingFunction.Linear100]: 'Squaddie',
	[TLevelingFunction.Linear150]: 'Conscript',
	[TLevelingFunction.Fast50]: 'Savvy',
	[TLevelingFunction.Fast100]: 'Smart',
	[TLevelingFunction.Fast150]: 'Expert',
	[TLevelingFunction.Prodigy]: 'Specialist',
};

const getLevelingCaption = (lf: UnitTalents.TUnitLevelingSignature): string =>
	LevelingCurveCaptions[Object.keys(LevelingFunctions).find((v) => LevelingFunctions[v] === lf)] || 'Other';

export default class UILearningCurveDescriptor extends React.PureComponent<UILearningCurveProps, {}> {
	public static getIcon(className: string = UIContainer.iconClass) {
		return UIXPCaption.getIcon(false, className);
	}

	public static getCaption() {
		return 'Learning Curve';
	}

	public static getTooltip() {
		return (
			<span>
				{UILearningCurveDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UILearningCurveDescriptor.getCaption()}</b> describes how fast a <em>Unit</em> gains levels compared
				to others and an amount of {UIXPCaption.getEntityCaption()} required for each level
			</span>
		);
	}

	public static getEntityCaption() {
		return (
			<span style={{ whiteSpace: 'nowrap' }} className={Style.caption}>
				{UILearningCurveDescriptor.getIcon()}
				{UILearningCurveDescriptor.getCaption()}
			</span>
		);
	}

	public render() {
		return (
			<dl
				className={classnames(this.props.className || '', Style.learningCurveDescriptor)}
				onClick={this.props.onClick || null}
			>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}
				>
					<span className={Style.caption}>{UILearningCurveDescriptor.getCaption()}</span>
					{!!this.props.tooltip && <UITooltip>{UILearningCurveDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.learningCurveCaption}>
						{getLevelingCaption(this.props.levelingFunction)}
					</span>
				</dd>
			</dl>
		);
	}
}

import * as React from 'react';
import { IAura } from '../../../../../../definitions/tactical/statuses/Auras/types';
import ImageList from '../../../../Symbols/abilityIcons';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import { UISplashIcon } from '../../../../Symbols/commonIcons';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import RibbonFactory from '../../../Templates/Ribbons';
import Tokenizer from '../../../Templates/Tokenizer';
import UIAoeRadiusDescriptor from '../../Attack/AOERadiusDescriptor';
import SVGAuraIcon from '../AuraIcon';
import Style from './index.scss';

export type UIAuraDescriptorProps = IUICommonProps & {
	aura: NonNullable<IAura>;
	disabled?: boolean;
};

const UIAuraDescriptor: React.FC<UIAuraDescriptorProps> = ({ className, aura, tooltip, disabled = false }) => {
	if (!aura) throw new Error(`Invalid aura ${aura}`);
	const bgRef = React.useRef(null);
	React.useEffect(() => {
		const objectCache = {} as Record<string, any>;
		if (!disabled)
			objectCache.ribbons = new RibbonFactory(bgRef.current, {
				colorAlpha: 0.15,
				ribbonCount: 9,
				loopTime: 3500,
				colorLoopTime: 3500,
				delayTime: 2000,
				minHue: 50,
				maxHue: 160,
				minHStep: 0.05,
				maxHStep: 0.25,
				minVStep: -0.2,
				maxVStep: 0.2,
				strokeSize: 1,
			});
		return () => {
			objectCache.ribbons?.destroy();
			delete objectCache.ribbons;
		};
	}, [disabled]);
	return (
		<div className={classnames(Style.auraWrapper, className || '')}>
			<div className={Style.auraBg} ref={bgRef} />
			<div className={classnames(Style.auraCaption)}>
				<div className={Style.text}>
					<SVGAuraIcon className={Style.icon} aura={aura} disabled={disabled}>
						<image xlinkHref={ImageList[aura.static.id]} width="100%" height="100%" />
					</SVGAuraIcon>
					<span className={classnames(Style.caption, !disabled ? '' : Style.disabled)}>
						{aura.static.caption}
					</span>
					{!disabled && (
						<>
							<div className={Style.rangeCaption}>
								<UIAoeRadiusDescriptor isAura={true} value={aura.range} tooltip={tooltip} />
							</div>
							<div className={Style.selectiveCaption}>
								{aura.static.selective ? (
									<span className={Style.selective}>Affects friendly Units only</span>
								) : (
									<span className={Style.nonSelective}>Affects all Units</span>
								)}
							</div>
						</>
					)}
					<span className={Style.target}>
						<span
							className={classnames(
								Style.target_caption,
								UIContainer.captionClass,
								UIContainer.descriptorClass,
							)}>
							<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.blue}>
								<use {...SVGIconContainer.getXLinkProps(UISplashIcon)} />
							</SVGSmallTexturedIcon>
							<span>Aura</span>
						</span>
					</span>
					{disabled && <span className={Style.ifDisabled}>Disabled</span>}
					{disabled ? (
						<span className={classnames(Style.description, Style.disabled)}>{aura.static.description}</span>
					) : (
						<span className={Style.description}>
							<Tokenizer description={aura.static.description} />
						</span>
					)}
				</div>
			</div>
		</div>
	);
};

export default UIAuraDescriptor;

import * as React from 'react';
import _ from 'underscore';
import SVGTools from '../../../../../../definitions/maths/SVGTools';
import Abilities from '../../../../../../definitions/tactical/abilities/types';
import { IAura } from '../../../../../../definitions/tactical/statuses/Auras/types';
import digest from '../../../../../utils/digest';
import UIContainer, { getTemplateColor, IUICommonProps } from '../../../index';
import { SVGHexIconShapeID } from '../../../SVG/HexIcon/index';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGBasicEmbossFilterID,
	SVGBasicNoiseFilterID,
	SVGBasicShadowFilterID,
	SVGFilterSaturate20ID,
} from '../../../SVG/IconContainer/index';
import UITooltip from '../../../Templates/Tooltip/index';
import Style from './index.scss';

type SVGAuraIconProps = IUICommonProps & {
	transparent?: boolean;
	aura: IAura;
	disabled?: boolean;
};

interface IStateType {
	id: string;
}

export default class SVGAuraIcon extends React.PureComponent<SVGAuraIconProps, IStateType> {
	public static getClass(): string {
		return Style.abilityIcon;
	}

	public static getDerivedStateFromProps(props: SVGAuraIconProps, prevState?: IStateType) {
		return {
			id: digest({
				disabled: props.disabled ?? false,
				aura: props.aura.static.id,
			}),
		};
	}

	constructor(props: SVGAuraIconProps) {
		super(props);
		this.state = SVGAuraIcon.getDerivedStateFromProps(props);
	}

	public render() {
		return (
			<span
				className={[
					this.props.className || '',
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
					this.props.disabled ? Style.disabled : '',
					Style.auraIcon,
				].join(' ')}
			>
				{this.props.tooltip && <UITooltip className={Style.tooltip}>{this.props.tooltip}</UITooltip>}
				<svg
					className={Style.svg}
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 120 100"
					overflow="visible"
					onClick={this.props.onClick || null}
				>
					<defs>
						<clipPath id={this.getClipPathId()}>
							<use {...SVGIconContainer.getXLinkProps(SVGHexIconShapeID)} />
						</clipPath>
						<linearGradient
							id={this.getBorderGradientId()}
							x1="100%"
							y1="0"
							x2="25%"
							y2="100%"
							gradientUnits="objectBoundingBox"
						>
							<stop offset="0" stopColor={getTemplateColor('light_green')} />
							<stop offset="1" stopColor={getTemplateColor('golden')} />
						</linearGradient>
					</defs>
					<g
						filter={SVGIconContainer.getUrlRef(SVGBasicShadowFilterID)}
						style={{
							transformOrigin: '50% 50%',
						}}
						clipPath={SVGIconContainer.getUrlRef(this.getClipPathId())}
					>
						{!this.props.transparent && (
							<g>
								<g className="__hexOutline" filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}>
									<use
										{...SVGIconContainer.getXLinkProps(SVGHexIconShapeID)}
										fill={SVGIconContainer.getUrlRef(this.getBorderGradientId())}
									/>
								</g>
								<g
									filter={SVGIconContainer.getUrlRef(SVGBasicEmbossFilterID)}
									className="__hexPlate"
									style={{
										transform: 'scale(0.92)',
										transformOrigin: '50% 50%',
									}}
								>
									<use
										{...SVGIconContainer.getXLinkProps(SVGHexIconShapeID)}
										fill={getTemplateColor('text_dark')}
									/>
								</g>
							</g>
						)}
						<g
							className="__hexIcon"
							filter={SVGIconContainer.getUrlRef(
								!this.props.disabled ? SVGBasicNoiseFilterID : SVGFilterSaturate20ID,
							)}
							style={{
								transform: 'translate(0,0) scale(0.73)',
								transformOrigin: '50% 50%',
							}}
						>
							{this.props.children}
						</g>
					</g>
				</svg>
			</span>
		);
	}

	protected getBorderGradientId() {
		return `${this.constructor.name}-gradient-a-${this.state.id}`;
	}

	protected getClipPathId() {
		return `${this.constructor.name}-clip-${this.state.id}`;
	}
}

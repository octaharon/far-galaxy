import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import Style from './index.scss';

type IUICooldownDescriptorPureProps = {
	cooldown?: number;
	cooldownLeft?: number;
	modifier?: IModifier;
};

export type UICooldownDescriptorProps = IUICommonProps &
	IUICooldownDescriptorPureProps & {
		short?: boolean;
	};

export const SVGCooldownIcon = 'cooldown-icon-shape';

SVGIconContainer.registerSymbol(
	SVGCooldownIcon,
	<symbol viewBox="0 0 532.745 532.745">
		<g
			style={{
				transformOrigin: '50% 50%',
				transform: 'scale(1, 0.95) translate(0.1rem, -0.1rem)',
			}}>
			<path
				d="M96.863,532.745h339.02v-169.51l-96.863-96.863l96.863-96.863V0H96.863v169.51l96.863,96.863l-96.863,96.863V532.745z
			 M121.078,373.261l89.768-89.768l17.12-17.12l-17.12-17.12l-89.768-89.768V24.216h290.588v135.269l-89.768,89.768l-17.12,17.12
			l17.12,17.12l89.768,89.768v135.269H121.078V373.261z"
			/>
			<polygon points="145.294,387.451 145.294,484.314 387.451,484.314 387.451,387.451 266.373,266.373 		" />
		</g>
	</symbol>,
);

export default class UICooldownDescriptor extends React.PureComponent<UICooldownDescriptorProps, {}> {
	public static getClass(): string {
		return Style.cooldownDescriptor;
	}

	public static getCaptionClass(): string {
		return Style.cooldownCaption;
	}

	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon className={className} style={TSVGSmallTexturedIconStyles.blue}>
				<use {...SVGIconContainer.getXLinkProps(SVGCooldownIcon)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption(short = true) {
		return short ? 'Cooldown' : 'Ability Cooldown';
	}

	public static getEntityCaption(short = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UICooldownDescriptor.getIcon()}
				<span className={Style.cooldownCaption}>{UICooldownDescriptor.getCaption(short)}</span>
			</em>
		);
	}

	public static getTooltip() {
		return (
			<span>
				{UICooldownDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UICooldownDescriptor.getCaption(false)}</b> is the amount of turns required for an <em>Ability</em>{' '}
				to be used again. It is affected by other abilities and environmental factors. If an <em>Ability</em> is{' '}
				<em>Disabled</em>, the Cooldown is not reduced
			</span>
		);
	}

	public static getValue(props: IUICooldownDescriptorPureProps) {
		const { cooldown, modifier } = props;
		let cooldownLeft = props.cooldownLeft;
		const modOnly = !Number.isFinite(cooldownLeft) && !Number.isFinite(cooldown);
		if (!Number.isFinite(cooldownLeft) && Number.isFinite(cooldown)) cooldownLeft = 0;
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = 100 > BasicMaths.applyModifier(100, modifier);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false).join('/').trim()}
			</span>
		);
		return (
			<>
				{cooldownLeft > 0 && (
					<>
						<span className={Style.current}>{cooldownLeft}</span>
						{cooldownLeft ? '/' : ''}
					</>
				)}
				{Number.isFinite(cooldown) && <span className={Style.base}>{cooldown === 0 ? '-' : cooldown}</span>}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	constructor(props: UICooldownDescriptorProps) {
		super(props);
	}

	public render() {
		const value = UICooldownDescriptor.getValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						UICooldownDescriptor.getClass(),
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UICooldownDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && <UITooltip>{UICooldownDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(UICooldownDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						Style.cooldownCaption,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UICooldownDescriptor.getIcon()}
					<span className={Style.caption}>{UICooldownDescriptor.getCaption()}</span>
					{!!this.props.tooltip && <UITooltip>{UICooldownDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

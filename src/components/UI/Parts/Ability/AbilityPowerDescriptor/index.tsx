import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import { AbilityPowerUIToken } from '../../../../../../definitions/tactical/statuses/constants';
import { wellRounded } from '../../../../../utils/wellRounded';
import UIContainer, { classnames, IUICommonProps, UI_SYMBOLS } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import { termTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import styles from './index.scss';

const UIAbilityPowerDescriptorClass = styles.abilityPowerDescriptor;

export type UIAbilityPowerDescriptorProps = IUICommonProps & {
	power?: number;
	modifier?: IModifier;
	isPlayer?: boolean;
	precision?: number;
	short?: boolean;
};

export const SVGAPIconID = 'ability-power-icon';

SVGIconContainer.registerSymbol(
	SVGAPIconID,
	<symbol viewBox="0 0 855 855">
		{/* <g
         style={{
         transformOrigin: '50% 50%',
         transform:       'scale(1.0,1.2)',
         }}>
         <path
         d="M331.5,127.5L234.6,255l73.95,96.9l-40.8,30.6C224.4,326.4,153,229.5,153,229.5L0,433.5h561L331.5,127.5z"/>
         </g>*/}
		<path
			d="M270.436,853.938c8.801,5.399,19.101-4.301,14-13.301c-13.399-23.8-21-51.199-21-80.5c0-62,34.301-111.6,84.801-144.6
		c44.699-29.2,32.5-66.9,39.899-111.8c4.4-26.601,21-50.8,34.9-66.9c5.6-6.5,16.1-3.399,17.5,5c8.2,50.4,73.6,136.2,111.3,192.2
		c43.4,64.6,40.6,121.5,40.3,125.8c0,0.2,0,0.4,0,0.601c-0.1,29.1-7.7,56.399-21,80c-5.1,9,5.2,18.8,14,13.3
		c69.4-42.9,119.4-113.7,136.101-193.601c13.3-63.6,5.8-129.3-15.7-190.1c-12.7-35.9-30.2-70-51.5-101.6
		c-68.9-102.5-188.8-259.601-203.2-351.5c-2.7-16.9-24.1-22.9-35.2-9.9c-0.2,0.2-20.6,22.7-33.399,47.6
		c-10.9,21.3-19.801,43.6-24.9,66.9c-9.6,43.9-7.9,90.9,3.1,134.4c3.601,14.2,8.2,28.1,13.801,41.6
		c5.399,13,11.199,26.1,12.199,40.3c1.601,26.5-22.399,49.4-48.399,49.4c-23.3,0-42.601-14-47.601-40.4
		c-1.3-6.8-8.899-10.399-14.899-6.899c-88.5,52.1-147.9,148.399-147.9,258.5C127.836,706.438,184.836,801.137,270.436,853.938z"
		/>
	</symbol>,
);

export default class UIAbilityPowerDescriptor extends React.PureComponent<UIAbilityPowerDescriptorProps, {}> {
	public static getClass(): string {
		return UIAbilityPowerDescriptorClass;
	}

	public static getCaption(isPlayer = false, short = false) {
		return (isPlayer ? 'Base ' : '') + (short ? 'AP' : 'Ability Power');
	}

	public static getTooltip(isPlayer = false) {
		return termTooltipTemplate(
			UIAbilityPowerDescriptor.getIcon(UITooltip.getIconClass()),
			UIAbilityPowerDescriptor.getCaption(isPlayer),
			`Also known as ${AbilityPowerUIToken}, this is a relative attribute that increases the
				Damage, or efficiency, of most Abilities used by the ${
					isPlayer ? 'Base' : 'Unit, as well as Damage Over Time and Deferred Damage of its Weapons'
				}. 
				
				While usually multiplicative, the details can vary for different Abilities, some of which are not
				affected by Ability Power at all. Without any bonuses, every ${
					isPlayer ? 'Base' : 'Unit'
				} starts with an Ability Power of 1.0`,
		);
	}

	public static getEntityCaption(isPlayer = false, short = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }} className={styles.abilityPowerCaption}>
				{UIAbilityPowerDescriptor.getIcon()}
				{UIAbilityPowerDescriptor.getCaption(isPlayer, short)}
			</em>
		);
	}

	public static getIcon(className?: string) {
		return (
			<SVGSmallTexturedIcon
				style={TSVGSmallTexturedIconStyles.gold}
				className={[UIContainer.iconClass, className || ''].join(' ')}
			>
				<use {...SVGIconContainer.getXLinkProps(SVGAPIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getValueCaption({
		value,
		modifier,
		isPlayer = false,
		precision = 3,
	}: {
		value: number;
		modifier?: IModifier;
		isPlayer: boolean;
		precision?: number;
	}) {
		const isPositive = BasicMaths.applyModifier(100, modifier) >= 100;
		const mod = (
			<span className={classnames(styles.modifier, isPositive ? styles.positive : styles.negative)}>
				{BasicMaths.describeModifier(modifier, true)}
			</span>
		);
		const v = wellRounded(value, precision);
		return (
			<>
				{!Number.isNaN(value) && <span className={classnames(styles.value)}>{v}</span>}
				{!!modifier && (Number.isFinite(value) ? <>({mod})</> : mod)}
			</>
		);
	}

	public render() {
		const classNames = [UIAbilityPowerDescriptor.getClass(), this.props.className || ''];
		const itemClassName = classnames(
			!!this.props.tooltip && UIContainer.hasTooltipClass,
			UIContainer.captionClass,
			UIContainer.descriptorClass,
			styles.abilityPowerCaption,
		);
		const value = UIAbilityPowerDescriptor.getValueCaption({
			value: this.props.power,
			precision: this.props.precision,
			isPlayer: !!this.props.isPlayer,
			modifier: this.props.modifier,
		});
		return this.props.short ? (
			<span className={classnames(...classNames, this.props.tooltip ? UIContainer.hasTooltipClass : '')}>
				{UIAbilityPowerDescriptor.getIcon(UIContainer.iconClass)}
				<span className={styles.value}>{value}</span>
				{!!this.props.tooltip && (
					<UITooltip>{UIAbilityPowerDescriptor.getTooltip(this.props.isPlayer)}</UITooltip>
				)}
			</span>
		) : (
			<dl className={classnames(...classNames)}>
				<dt className={itemClassName}>
					{UIAbilityPowerDescriptor.getIcon(UIContainer.iconClass)}
					{UIAbilityPowerDescriptor.getCaption(!!this.props.isPlayer)}
					{!!this.props.tooltip && (
						<UITooltip>{UIAbilityPowerDescriptor.getTooltip(this.props.isPlayer)}</UITooltip>
					)}
				</dt>
				<dd>{value}</dd>
			</dl>
		);
	}
}

import * as React from 'react';
import _ from 'underscore';
import SVGTools from '../../../../../../definitions/maths/SVGTools';
import Abilities from '../../../../../../definitions/tactical/abilities/types';
import digest from '../../../../../utils/digest';
import UIContainer, { getTemplateColor, IUICommonProps } from '../../../index';
import { SVGHexIconShapeID } from '../../../SVG/HexIcon';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGBasicEmbossFilterID,
	SVGBasicNoiseFilterID,
	SVGBasicShadowFilterID,
	SVGDarkenFilterID,
} from '../../../SVG/IconContainer/index';
import UITooltip from '../../../Templates/Tooltip/index';
import Style from './index.scss';

export type SVGAbilityIconProps = IUICommonProps & {
	transparent?: boolean;
	ability: Abilities.IAbility<any>;
};

interface IStateType {
	id: string;
}

export default class SVGAbilityIcon extends React.PureComponent<SVGAbilityIconProps, IStateType> {
	public static getClass(): string {
		return Style.abilityIcon;
	}

	public static getDerivedStateFromProps(nextProps: SVGAbilityIconProps, prevState: IStateType) {
		return {
			id: digest({
				..._.pick(nextProps, 'transparent'),
				tooltip: (nextProps.tooltip || '--').toString(),
				now: Date.now(),
				seed: Math.random(),
			}),
		};
	}

	constructor(props: SVGAbilityIconProps) {
		super(props);
		this.state = {
			id: digest({
				..._.pick(props, 'transparent'),
				tooltip: (props.tooltip || '--').toString(),
				now: Date.now(),
				seed: Math.random(),
			}),
		};
	}

	public render() {
		const isClickable = !!this.props.onClick && this.props.ability.cooldownLeft <= 0;
		const cd = Math.ceil(this.props.ability.cooldownLeft);
		const fillRadius = 80;
		return (
			<span
				className={[
					this.props.className || '',
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
					!this.props.ability.usable ? Style.disabled : '',
					Style.abilityIcon,
					isClickable ? Style.hoverable : '',
				].join(' ')}
			>
				{this.props.tooltip && <UITooltip className={Style.tooltip}>{this.props.tooltip}</UITooltip>}
				<svg
					className={Style.svg}
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 120 100"
					overflow="visible"
					onClick={this.props.onClick || null}
				>
					<defs>
						<clipPath id={this.getClipPathId()}>
							<use {...SVGIconContainer.getXLinkProps(SVGHexIconShapeID)} />
						</clipPath>
						<radialGradient id={this.getPlateGradientId()} gradientUnits="objectBoundingBox">
							<stop offset="0" stopColor={getTemplateColor('tooltip_bg')} />
							<stop offset="1" stopColor={getTemplateColor('light_blue')} />
						</radialGradient>
						<linearGradient
							id={this.getBorderGradientId()}
							x1="100%"
							y1="0"
							x2="25%"
							y2="100%"
							gradientUnits="objectBoundingBox"
						>
							<stop offset="0" stopColor={getTemplateColor('dark_red')} />
							<stop offset="1" stopColor={getTemplateColor('light_purple')} />
						</linearGradient>
					</defs>
					<g
						filter={SVGIconContainer.getUrlRef(SVGBasicShadowFilterID)}
						style={{
							transformOrigin: '50% 50%',
						}}
						clipPath={SVGIconContainer.getUrlRef(this.getClipPathId())}
					>
						{!this.props.transparent && (
							<g>
								<g className="__hexOutline" filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}>
									<use
										{...SVGIconContainer.getXLinkProps(SVGHexIconShapeID)}
										fill={SVGIconContainer.getUrlRef(this.getBorderGradientId())}
									/>
								</g>
								<g
									filter={SVGIconContainer.getUrlRef(SVGBasicEmbossFilterID)}
									className="__hexPlate"
									style={{
										transform: 'scale(0.92)',
										transformOrigin: '50% 50%',
									}}
								>
									<use
										{...SVGIconContainer.getXLinkProps(SVGHexIconShapeID)}
										fill={getTemplateColor('text_dark')}
									/>
								</g>
							</g>
						)}
						<g
							className="__hexIcon"
							filter={SVGIconContainer.getUrlRef(
								!!this.props.ability.usable && this.props.ability.cooldownLeft <= 0
									? SVGBasicNoiseFilterID
									: SVGDarkenFilterID,
							)}
							style={{
								transform: 'translate(0,0.1rem) scale(0.73)',
								transformOrigin: '50% 50%',
							}}
						>
							{this.props.children}
						</g>
						{cd > 0 && (
							<g
								style={{
									opacity: 0.9,
									transform: 'rotate(30deg)',
									transformOrigin: '50% 50%',
								}}
							>
								{this.props.ability.cooldown > 1 ? (
									new Array(cd).fill(null).map((n, ix) => {
										const width = (Math.PI * 2) / Math.ceil(this.props.ability.cooldown);
										const startAngle = width * ix + (Math.PI * 3) / 2;
										return (
											<g key={ix} filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)}>
												<path
													d={SVGTools.describeSector(
														60,
														50,
														fillRadius,
														startAngle,
														startAngle + width,
													)}
													strokeWidth="0.15rem"
													stroke={getTemplateColor('dark_green')}
													fill={SVGIconContainer.getUrlRef(this.getPlateGradientId())}
												/>
											</g>
										);
									})
								) : (
									<g filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)}>
										<circle
											cx={60}
											cy={50}
											r={fillRadius}
											fill={SVGIconContainer.getUrlRef(this.getPlateGradientId())}
										/>
									</g>
								)}
							</g>
						)}
					</g>
				</svg>
			</span>
		);
	}

	protected getBorderGradientId() {
		return `${this.constructor.name}-gradient-a-${this.state.id}`;
	}

	protected getPlateGradientId() {
		return `${this.constructor.name}-gradient-b-${this.state.id}`;
	}

	protected getClipPathId() {
		return `${this.constructor.name}-clip-${this.state.id}`;
	}
}

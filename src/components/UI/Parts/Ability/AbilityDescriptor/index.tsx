import * as React from 'react';
import Abilities from '../../../../../../definitions/tactical/abilities/types';
import { MAX_MAP_RADIUS } from '../../../../../../definitions/tactical/map/constants';
import AbilityIconList from '../../../../Symbols/abilityIcons';
import { UISelfCastIcon, UITargetIcon } from '../../../../Symbols/commonIcons';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import RibbonFactory from '../../../Templates/Ribbons';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import { UIAttackIconAOE_Scatter_ID } from '../../Attack/AttackIcon';
import UIAttackRangeDescriptor from '../../Attack/RangeDescriptor';
import SVGAbilityIcon from '../AbilityIcon';
import UICooldownDescriptor from '../CooldownDescriptor';
import Style from './index.scss';

export type UIAbilityDescriptorProps = IUICommonProps & {
	ability: NonNullable<Abilities.IAbility<any>>;
	usable?: boolean;
};

const getAbilityTargetCaption = (a: Abilities.abilityTargetTypes) => {
	const className = [Style.target_caption, UIContainer.captionClass, UIContainer.descriptorClass].join(' ');
	const DirectIcon = (
		<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.light}>
			<use {...SVGIconContainer.getXLinkProps(UITargetIcon)} />
		</SVGSmallTexturedIcon>
	);
	const AreaIcon = (
		<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.light}>
			<use {...SVGIconContainer.getXLinkProps(UIAttackIconAOE_Scatter_ID)} />
		</SVGSmallTexturedIcon>
	);
	const SelfIcon = (
		<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.light}>
			<use {...SVGIconContainer.getXLinkProps(UISelfCastIcon)} />
		</SVGSmallTexturedIcon>
	);
	switch (a) {
		case Abilities.abilityTargetTypes.unit:
			return <span className={className}>{DirectIcon}Any unit</span>;
		case Abilities.abilityTargetTypes.player:
			return <span className={className}>{DirectIcon}Opponent</span>;
		case Abilities.abilityTargetTypes.self:
			return <span className={className}>{SelfIcon}Unit</span>;
		case Abilities.abilityTargetTypes.player_self:
			return <span className={className}>{SelfIcon}Player</span>;
		case Abilities.abilityTargetTypes.around:
			return <span className={className}>{AreaIcon}Range</span>;
		case Abilities.abilityTargetTypes.map:
			return <span className={className}>{AreaIcon}Location</span>;
		default:
			return <span className={className}>Unknown</span>;
	}
};

const UIAbilityDescriptor: React.FC<UIAbilityDescriptorProps> = ({ className, ability, tooltip, usable = false }) => {
	if (!ability) throw new Error(`Invalid ability ${ability}`);
	const bgRef = React.useRef(null);
	const isUsableByState = !!ability.usable && ability.cooldownLeft <= 0;
	React.useEffect(() => {
		const objectCache = {} as Record<string, any>;
		if (isUsableByState)
			objectCache.ribbons = new RibbonFactory(bgRef.current, {
				colorAlpha: 0.2,
				ribbonCount: 9,
				loopTime: 3500,
				vertical: true,
				colorLoopTime: 2500,
				delayTime: 1500,
				minHue: -30,
				maxHue: 30,
				minHStep: -0.15,
				maxHStep: 0.15,
				minVStep: 0.05,
				maxVStep: 0.25,
				strokeSize: 2,
			});
		return () => {
			objectCache.ribbons?.destroy();
			delete objectCache.ribbons;
		};
	}, [isUsableByState]);
	return (
		<div
			className={classnames(
				Style.abilityWrapper,
				usable && isUsableByState ? Style.usable : '',
				className || '',
			)}>
			<div className={Style.abilityBg} ref={bgRef} />
			<div className={classnames(tooltip ? UIContainer.hasTooltipClass : '', Style.abilityPlaque)}>
				<div className={Style.text}>
					<SVGAbilityIcon tooltip={tooltip} className={Style.icon} ability={ability} transparent={false}>
						<image xlinkHref={AbilityIconList[ability.static.id]} width="100%" height="100%" />
					</SVGAbilityIcon>
					<span className={[Style.caption, isUsableByState ? '' : Style.disabled].join(' ')}>
						{ability.static.caption}
					</span>
					{!!ability.usable && (
						<>
							<div className={Style.cooldownCaption}>
								<UICooldownDescriptor
									cooldown={ability.cooldown}
									cooldownLeft={ability.cooldownLeft}
									tooltip={!usable || !isUsableByState}
									short={false}
								/>
							</div>
							{ability.baseRange > 0 && (
								<div className={Style.rangeCaption}>
									<UIAttackRangeDescriptor
										range={ability.baseRange < MAX_MAP_RADIUS ? ability.baseRange : Infinity}
										precision={2}
										tooltip={!usable || !isUsableByState}
										isRadius={[Abilities.abilityTargetTypes.around].includes(ability.targetType)}
									/>
								</div>
							)}
							<span className={Style.target}>{getAbilityTargetCaption(ability.targetType)}</span>
						</>
					)}
					{!ability.usable && <span className={Style.ifDisabled}>Disabled</span>}
					{!isUsableByState ? (
						<span className={classnames(Style.description, Style.disabled)}>
							{ability.static.description}
						</span>
					) : (
						<span className={Style.description}>
							<Tokenizer description={ability.static.description} />
						</span>
					)}
				</div>
				{usable && !!tooltip && (
					<UITooltip>
						<Tokenizer description={ability.static.description} />
					</UITooltip>
				)}
			</div>
		</div>
	);
};

export default UIAbilityDescriptor;

import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, IUICommonProps, UI_SYMBOLS } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import Style from './index.scss';

export const DURATION_TYPE_EFFECT = 'duration';
export const DURATION_TYPE_ABILITY = 'ability';
export const DURATION_TYPE_RESISTANCE = 'resistance';

const DurationTypes = {
	[DURATION_TYPE_RESISTANCE]: 'Effect Resistance',
	[DURATION_TYPE_EFFECT]: 'Effect Duration',
	[DURATION_TYPE_ABILITY]: 'Effect Penetration',
};

type IUIDurationDescriptorType = keyof typeof DurationTypes;

type IUIDurationDescriptorPureProps = {
	modifier?: IModifier;
	duration?: number;
	type: IUIDurationDescriptorType;
	caption?: string | JSX.Element;
};

export type IUIDurationDescriptorProps = IUICommonProps &
	IUIDurationDescriptorPureProps & {
		short?: boolean;
	};

export const SVGDurationIcon = 'duration-icon-shape';

SVGIconContainer.registerSymbol(
	SVGDurationIcon,
	<symbol viewBox="0 0 96 96">
		<g
			style={{
				transformOrigin: '50% 50%',
				transform: 'scale(1,0.95) translateY(-5%)',
			}}>
			<path d="m77.08 28.577 5.748-5.748-5.656-5.658-6.149 6.149a39.747 39.747 0 0 0 -19.023-7.12v-8.2h8v-8h-24v8h8v8.2a39.747 39.747 0 0 0 -19.023 7.12l-6.149-6.149-5.656 5.658 5.748 5.748a40 40 0 1 0 58.16 0zm-29.08 59.423a32 32 0 1 1 32-32 32.036 32.036 0 0 1 -32 32z" />
			<path d="m48 32v24h-24a24 24 0 1 0 24-24z" />
		</g>
	</symbol>,
);

export default class UIDurationDescriptor extends React.PureComponent<IUIDurationDescriptorProps, {}> {
	public static getClass(): string {
		return Style.durationDescriptor;
	}

	public static getCaptionClass(type: IUIDurationDescriptorType = DURATION_TYPE_EFFECT): string {
		return type === DURATION_TYPE_RESISTANCE ? Style.resistanceCaption : Style.durationCaption;
	}

	public static getIcon(
		className: string = UIContainer.iconClass,
		type: IUIDurationDescriptorType = DURATION_TYPE_EFFECT,
	) {
		const iconTypes = {
			[DURATION_TYPE_RESISTANCE]: TSVGSmallTexturedIconStyles.pink,
			[DURATION_TYPE_EFFECT]: TSVGSmallTexturedIconStyles.gray,
			[DURATION_TYPE_ABILITY]: TSVGSmallTexturedIconStyles.light,
		};
		return (
			<SVGSmallTexturedIcon className={className} style={iconTypes[type]}>
				<use {...SVGIconContainer.getXLinkProps(SVGDurationIcon)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption(type: IUIDurationDescriptorType = DURATION_TYPE_EFFECT) {
		return DurationTypes[type];
	}

	public static getEntityCaption(type: IUIDurationDescriptorType = DURATION_TYPE_EFFECT) {
		return (
			<span style={{ whiteSpace: 'nowrap' }} className={Style.durationCaption}>
				{UIDurationDescriptor.getIcon(Style.durationIcon, type)}
				<span className={UIDurationDescriptor.getCaptionClass(type)}>
					{UIDurationDescriptor.getCaption(type)}
				</span>
			</span>
		);
	}

	public static getTooltip(type: IUIDurationDescriptorType = DURATION_TYPE_EFFECT) {
		const verb = {
			[DURATION_TYPE_EFFECT]: 'is',
			[DURATION_TYPE_RESISTANCE]: 'reduces',
			[DURATION_TYPE_ABILITY]: 'increases',
		};
		const adjective = {
			[DURATION_TYPE_EFFECT]: 'Status Effect',
			[DURATION_TYPE_RESISTANCE]: 'Acquired Status Effects',
			[DURATION_TYPE_ABILITY]: 'Inflicted Status Effects',
		};
		return (
			<span>
				{UIDurationDescriptor.getIcon(UITooltip.getIconClass(), type)}
				<b>{UIDurationDescriptor.getCaption(type)}</b> {verb[type]} the amount of turns required for{' '}
				<em>{adjective[type]}</em> to expire and wear off. If the affected <em>Unit</em> is <em>Disabled</em>,
				the <em>{DurationTypes[DURATION_TYPE_EFFECT]}</em> is not reduced that <em>Turn</em>
			</span>
		);
	}

	public static getValue(props: IUIDurationDescriptorPureProps) {
		const { duration, modifier, type } = props;
		const modOnly = !Number.isFinite(duration);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = !(type === DURATION_TYPE_RESISTANCE) || !(BasicMaths.applyModifier(100, modifier) >= 100);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, true, false, type === DURATION_TYPE_RESISTANCE).trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(duration) && (
					<span className={type === DURATION_TYPE_RESISTANCE ? Style.resistanceValue : Style.durationValue}>
						{duration >= 0 ? duration : UI_SYMBOLS.Infinity}
					</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	constructor(props: IUIDurationDescriptorProps) {
		super(props);
	}

	public render() {
		const value = UIDurationDescriptor.getValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						UIDurationDescriptor.getClass(),
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIDurationDescriptor.getIcon(UIContainer.iconClass, this.props.type)}
					{value}
					{!!this.props.tooltip && <UITooltip>{UIDurationDescriptor.getTooltip(this.props.type)}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(UIDurationDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIDurationDescriptor.getIcon(UIContainer.iconClass, this.props.type)}
					<span
						className={
							this.props.type === DURATION_TYPE_RESISTANCE
								? Style.resistanceCaption
								: Style.durationCaption
						}>
						{this.props.caption || UIDurationDescriptor.getCaption(this.props.type)}
					</span>
					{!!this.props.tooltip && <UITooltip>{UIDurationDescriptor.getTooltip(this.props.type)}</UITooltip>}
				</dt>
				<dd>{value}</dd>
			</dl>
		);
	}
}

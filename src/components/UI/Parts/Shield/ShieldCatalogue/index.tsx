import * as React from 'react';
import { doesCollectibleSetIncludeItem } from '../../../../../../definitions/core/Collectible';
import { extractPlayerUnitModifier } from '../../../../../../definitions/player/helpers';
import Players from '../../../../../../definitions/player/types';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import DefenseManager from '../../../../../../definitions/tactical/damage/DefenseManager';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { getFactionUnitModifiers } from '../../../../../../definitions/world/factionIndex';
import { classnames, IUICommonProps } from '../../../index';
import UIShieldDescriptor from '../ShieldDescriptor';
import UIShieldListSelector from '../ShieldListSelector';
import styles from './index.scss';
import IPlayer = Players.IPlayer;

const shieldCatalogueClass = styles.shieldCatalogue;

type UIShieldCatalogueProps = IUICommonProps & {
	player?: IPlayer;
	defaultSelectedShield?: number;
	short?: boolean;
	modifiers?: UnitDefense.TShieldsModifier[];
	forChassis?: UnitChassis.chassisClass;
	forSlot?: UnitDefense.TShieldSlotType;
	onShieldSelected?: (shieldId: number) => any;
};

export interface UIShieldCatalogueState {
	selectedShield: number;
}

const stackShieldMods = (props: UIShieldCatalogueProps) =>
	DefenseManager.stackShieldsModifier(
		...(props.modifiers || []),
		...extractPlayerUnitModifier(getFactionUnitModifiers(props.player?.faction), 'shield', props.forChassis),
	);

const getShieldId = (props: UIShieldCatalogueProps, shieldId?: number) => {
	const provider = DIContainer.getProvider(DIInjectableCollectibles.shield);
	let sId = shieldId || props.defaultSelectedShield;
	const possibleShieldOptions = props.player
		? Object.keys(provider.getAll()).filter((id) => props.player.isShieldAvailable(id))
		: Object.keys(provider.getAll());
	if (!doesCollectibleSetIncludeItem(sId, possibleShieldOptions)) sId = possibleShieldOptions[0];
	if (props.forSlot) {
		if (!provider.doesShieldFitSlot(provider.get(sId), props.forSlot))
			sId = possibleShieldOptions.filter((id) => provider.doesShieldFitSlot(provider.get(id), props.forSlot))[0];
	}
	return sId;
};

export default class UIShieldCatalogue extends React.PureComponent<UIShieldCatalogueProps, UIShieldCatalogueState> {
	public static getClass(): string {
		return shieldCatalogueClass;
	}

	public static getDerivedStateFromProps(props: UIShieldCatalogueProps, state?: UIShieldCatalogueState) {
		const shieldId = getShieldId(props, state?.selectedShield);
		return {
			selectedShield: shieldId || 10000,
		};
	}

	public componentDidMount() {
		if (!this.props.defaultSelectedShield && this.props.onShieldSelected)
			this.props.onShieldSelected(this.state.selectedShield);
	}

	public get selectedSlot() {
		return DIContainer.getProvider(DIInjectableCollectibles.shield).get(this.state.selectedShield)?.type || null;
	}

	public constructor(props: UIShieldCatalogueProps) {
		super(props);
		this.state = UIShieldCatalogue.getDerivedStateFromProps(props);
		this.onShieldSelected = this.onShieldSelected.bind(this);
	}

	public render() {
		const mods = stackShieldMods(this.props);
		return (
			<div className={classnames(UIShieldCatalogue.getClass(), this.props.className || '')}>
				<UIShieldListSelector
					shieldSlot={this.props.forSlot}
					selectedShield={this.state.selectedShield}
					onShieldSelected={this.onShieldSelected}
					player={this.props.player}
					className={styles.selector}
				/>
				<UIShieldDescriptor
					shieldId={this.state.selectedShield}
					className={styles.descriptor}
					modifiers={[mods]}
					details={!this.props.short}
				/>
			</div>
		);
	}

	protected onShieldSelected(shieldId: number) {
		this.setState(
			{
				...this.state,
				selectedShield: getShieldId(this.props, shieldId),
			},
			() => {
				if (this.props.onShieldSelected) this.props.onShieldSelected(shieldId);
			},
		);
	}
}

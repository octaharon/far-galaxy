import * as React from 'react';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import UIContainer, { IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIShieldSlotDescriptor from '../ShieldSlotDescriptor';
import styles from './index.scss';

export const shieldGroupDescriptorClass = styles.shieldGroup;

export type UIShieldGroupDescriptorProps = IUICommonProps & {
	group: UnitDefense.TShieldSlotType;
	highlight?: boolean;
	tooltip?: boolean;
};

export default class UIShieldGroupDescriptor extends React.PureComponent<UIShieldGroupDescriptorProps, {}> {
	public static getClass(): string {
		return shieldGroupDescriptorClass;
	}

	public static getCaptionClass(): string {
		return styles.shieldGroupCaption;
	}

	public render() {
		return (
			<span
				className={[
					UIShieldGroupDescriptor.getClass(),
					this.props.className || '',
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				].join(' ')}
			>
				<UIShieldSlotDescriptor
					slotType={this.props.group}
					className={styles.slotIcon}
					tooltip={false}
					highlight={!!this.props.highlight}
				/>
				<span className={UIShieldGroupDescriptor.getCaptionClass()}>
					{UIShieldSlotDescriptor.getShieldSlotCaption(this.props.group)}
				</span>
				{this.props.tooltip && (
					<UITooltip>{UIShieldSlotDescriptor.getShieldSlotDescription(this.props.group)}</UITooltip>
				)}
			</span>
		);
	}
}

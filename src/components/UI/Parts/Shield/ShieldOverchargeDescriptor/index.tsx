import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import UIShieldDescriptor from '../ShieldDescriptor';
import Style from './index.scss';

export type UIShieldOverchargeDescriptorProps = IUICommonProps & {
	overchargeDecay?: number;
	modifier?: IModifier;
	short?: boolean;
};

export const SVGShieldOverchargeIconID = 'shield-overcharge-icon';

SVGIconContainer.registerSymbol(
	SVGShieldOverchargeIconID,
	<symbol viewBox="0 0 512 512" width="100%" height="100%">
		<g
			style={{
				transformOrigin: '50% 50%',
			}}>
			<path d="m457.057 94.697c-43.75 0-79.344-35.593-79.344-79.343v-15.354h-106.36v130.814h34.416v59.564h59.564v99.538h-59.564v59.564h-34.416v162.52l18.875-12.852c114.077-77.674 182.182-206.441 182.182-344.451v-60z" />
			<path d="m275.062 318.773v-59.564h59.564v-38.124h-59.564v-59.564h-38.124v59.564h-59.564v38.124h59.564v59.564z" />
			<path d="m206.231 349.48v-59.564h-59.564v-99.538h59.564v-59.564h34.416v-130.814h-106.36v15.353c0 43.75-35.594 79.343-79.344 79.343h-15.354v60c0 138.01 68.105 266.777 182.182 344.451l18.876 12.853v-162.52z" />
		</g>
	</symbol>,
);

export default class UIShieldOverchargeDescriptor extends React.PureComponent<UIShieldOverchargeDescriptorProps, {}> {
	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.blue} className={className}>
				<use {...SVGIconContainer.getXLinkProps(SVGShieldOverchargeIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption() {
		return 'Overcharge Decay';
	}

	public static getTooltip() {
		return (
			<span>
				{UIShieldOverchargeDescriptor.getIcon(UITooltip.getIconClass())}
				<b>{UIShieldOverchargeDescriptor.getCaption()}</b> implies that {UIShieldDescriptor.getEntityCaption()}{' '}
				can accumulate energy above the <em>capacity</em> but, if so, will lose it eventually for a given value
				per turn, until it's below the <em>maximal capacity</em>
			</span>
		);
	}

	public static getEntityCaption() {
		return (
			<span style={{ whiteSpace: 'nowrap' }}>
				{UIShieldOverchargeDescriptor.getIcon()}
				<span className={Style.decayCaption}>{UIShieldOverchargeDescriptor.getCaption()}</span>
			</span>
		);
	}

	public static getOverchargeValue(props: { overchargeDecay?: number; modifier?: IModifier }) {
		const { overchargeDecay, modifier } = props;
		if (!Number.isFinite(overchargeDecay) && !modifier) return 'Unknown';
		return (
			<>
				{Number.isFinite(overchargeDecay) && <span className={Style.value}>{overchargeDecay}</span>}
				{!!modifier && Number.isFinite(overchargeDecay) && (
					<>
						(<span className={Style.mod}>{BasicMaths.describeModifier(modifier).join('/').trim()}</span>)
					</>
				)}
				{!!modifier && !Number.isFinite(overchargeDecay) && (
					<span className={Style.mod}>{BasicMaths.describeModifier(modifier).join('/').trim()}</span>
				)}
			</>
		);
	}

	public render() {
		const self = this.constructor as typeof UIShieldOverchargeDescriptor;
		if (this.props.short)
			return (
				<span
					className={classnames(
						UIContainer.captionClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : null,
						Style.shieldOverchargeDescriptor,
						this.props.className || '',
					)}>
					{UIShieldOverchargeDescriptor.getIcon()}
					<span>{UIShieldOverchargeDescriptor.getOverchargeValue(this.props)}</span>
					{!!this.props.tooltip && <UITooltip>{UIShieldOverchargeDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={[this.props.className || '', Style.shieldOverchargeDescriptor].join(' ')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : null,
					)}>
					{UIShieldOverchargeDescriptor.getIcon()}
					<span className={Style.decayCaption}>{UIShieldOverchargeDescriptor.getCaption()}</span>
					{!!this.props.tooltip && <UITooltip>{UIShieldOverchargeDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>{UIShieldOverchargeDescriptor.getOverchargeValue(this.props)}</dd>
			</dl>
		);
	}
}

import * as React from 'react';
import _ from 'underscore';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import { IShield } from '../../../../../../definitions/technologies/types/shield';
import { UITalentBackdropPatternID, UIUnitBackgroundGradientID } from '../../../../Symbols/backdrops';
import UIRawMaterialsDescriptor from '../../../Economy/RawMaterialsDescriptor';
import UIContainer, { IUICommonProps } from '../../../index';
import UIBackdrop from '../../../SVG/Backdrop';
import UITooltip from '../../../Templates/Tooltip';
import UIDamageProtectionDescriptor from '../../Defense/DamageProtectionDescriptor';
import UIDefenseFlagsDescriptor from '../../Defense/DefenseFlagsDescriptor';
import UIShieldCapacityDescriptor from '../ShieldCapacityDescriptor';
import UIShieldGroupDescriptor from '../ShieldGroupDescriptor';
import UIShieldOverchargeDescriptor from '../ShieldOverchargeDescriptor';
import UIShieldSlotDescriptor from '../ShieldSlotDescriptor';
import styles from './index.scss';

const shieldDescriptorClass = styles.shieldDescriptor;

export type UIShieldDescriptorProps = IUICommonProps & {
	shieldId: number;
	modifiers?: UnitDefense.TShieldsModifier[];
	details?: boolean;
};

export interface UIShieldDescriptorState {
	shield: IShield;
}

export const ShieldMetaDescriptor: React.FC<{ shield: IShield; short?: boolean }> = ({ shield, short }) => {
	const hasOvercharge = shield.shield.overchargeDecay > 0;
	const hasFlags =
		!_.isEmpty(shield.static.flags) &&
		Object.keys(shield.static.flags).filter((v) => !!shield.static.flags[v]).length > 0;
	switch (true) {
		case !hasOvercharge && !hasFlags:
			return null;
		case !short:
			return (
				<>
					<UIDefenseFlagsDescriptor
						flags={shield.static.flags || {}}
						short={false}
						tooltip={true}
						className={styles.metaCaption}
					/>
					{hasOvercharge && (
						<UIShieldOverchargeDescriptor overchargeDecay={shield.shield.overchargeDecay} tooltip={true} />
					)}
				</>
			);
		case !hasOvercharge && hasFlags:
			return (
				<UIDefenseFlagsDescriptor
					flags={shield.static.flags || {}}
					short={false}
					tooltip={true}
					className={styles.metaCaption}
				/>
			);
		case hasOvercharge && !hasFlags:
			return <UIShieldOverchargeDescriptor overchargeDecay={shield.shield.overchargeDecay} tooltip={true} />;
		default:
			return (
				<dl className={styles.meta}>
					<dt className={UIContainer.descriptorClass}>
						<UIDefenseFlagsDescriptor
							flags={shield.static.flags || {}}
							short={true}
							forceCaption={true}
							tooltip={true}
							className={styles.metaCaption}
						/>
					</dt>
					<dd className={''}>
						<UIShieldOverchargeDescriptor
							short={true}
							overchargeDecay={shield.shield.overchargeDecay}
							tooltip={true}
						/>
					</dd>
				</dl>
			);
	}
};

export default class UIShieldDescriptor extends React.PureComponent<UIShieldDescriptorProps, UIShieldDescriptorState> {
	public constructor(props: UIShieldDescriptorProps) {
		super(props);
		this.state = UIShieldDescriptor.getDerivedStateFromProps(props);
	}

	public static getClass(): string {
		return shieldDescriptorClass;
	}

	public static getDerivedStateFromProps(
		newProps: UIShieldDescriptorProps,
		oldState: UIShieldDescriptorState = {
			shield: null,
		},
	): UIShieldDescriptorState {
		const newState = {
			...oldState,
		};
		if (newProps.shieldId !== oldState?.shield?.static?.id) {
			const w = DIContainer.getProvider(DIInjectableCollectibles.shield).instantiate(null, newProps.shieldId);
			newState.shield = w.applyModifier(...(newProps.modifiers || []));
		}
		return newState;
	}

	public static getCaption() {
		return UIShieldSlotDescriptor.getShieldSlotCaption(UnitDefense.TShieldSlotType.none);
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{
					<UIShieldSlotDescriptor
						slotType={UnitDefense.TShieldSlotType.none}
						highlight={true}
						className={styles.shieldSlotIcon}
					/>
				}
				<span className={UIShieldGroupDescriptor.getCaptionClass()}>{UIShieldDescriptor.getCaption()}</span>
			</em>
		);
	}

	public static getDescriptor() {
		return (
			<>
				{
					<UIShieldSlotDescriptor
						slotType={UnitDefense.TShieldSlotType.none}
						className={UIContainer.iconClass}
					/>
				}
				<span className={styles.shieldCaption}>{UIShieldDescriptor.getCaption()}</span>
			</>
		);
	}

	public render() {
		const protectionScale = {
			[UnitDefense.TShieldSlotType.small]: 1,
			[UnitDefense.TShieldSlotType.medium]: 2,
			[UnitDefense.TShieldSlotType.large]: 3,
			[UnitDefense.TShieldSlotType.none]: 10,
		};
		return (
			<div className={[UIShieldDescriptor.getClass(), this.props.className || ''].join(' ')}>
				<UIBackdrop
					className={styles.frame}
					gradientId={UIUnitBackgroundGradientID}
					patternId={UITalentBackdropPatternID}
				/>
				<div className={[this.props.details ? '' : styles.short, styles.content].join(' ')}>
					<dl className={styles.meta}>
						{this.props.details && (
							<>
								<dt className={styles.group}>
									<UIShieldGroupDescriptor
										highlight={true}
										group={this.state.shield.static.type}
										tooltip={true}
									/>
								</dt>
								<dd className={styles.shieldSlots}>
									<UIShieldSlotDescriptor
										slotType={this.state.shield.static.type}
										tooltip={true}
										className={styles.shieldSlotItem}
									/>
								</dd>
							</>
						)}
						<dt className={styles.title}>
							{this.state.shield.caption}{' '}
							{!this.props.details && (
								<UIShieldSlotDescriptor
									slotType={this.state.shield.static.type}
									className={styles.shieldSlotItem}
								/>
							)}
						</dt>
						<dd className={styles.cost}>
							<UIRawMaterialsDescriptor
								short={true}
								value={this.state.shield.baseCost}
								className={styles.costDescriptor}
							/>
						</dd>
					</dl>
					{this.props.details && <div className={styles.description}>{this.state.shield.description}</div>}
					<ShieldMetaDescriptor shield={this.state.shield} short={!this.props.details} />
					<dl className={styles.meta}>
						<dt
							className={[
								UIContainer.hasTooltipClass,
								UIContainer.captionClass,
								UIContainer.descriptorClass,
								styles.shieldCapacity,
							].join(' ')}>
							{UIShieldCapacityDescriptor.getIcon()}
							{UIShieldCapacityDescriptor.getCaption()}
							<UITooltip>{UIShieldCapacityDescriptor.getTooltip()}</UITooltip>
						</dt>
						<dd>
							<UIShieldCapacityDescriptor
								totalAmount={this.state.shield.shield.shieldAmount}
								overcharge={this.state.shield.shield.overchargeDecay > 0}
							/>
						</dd>
					</dl>
					<div className={styles.protection}>
						<UIDamageProtectionDescriptor
							scale={protectionScale[this.state.shield.static.type]}
							protection={this.state.shield.shield}
							short={!this.props.details}
							kind={'shield'}
						/>
					</div>
				</div>
			</div>
		);
	}
}

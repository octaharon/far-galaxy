import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UIModifierDescriptor from '../../../Templates/ModifierDescriptor';
import UITooltip from '../../../Templates/Tooltip';
import UIShieldCapacityDescriptor from '../ShieldCapacityDescriptor';
import styles from './index.scss';

const UIShieldCapacityModifierDescriptorClass = styles.shieldCapacityModifierDescriptor;

export type UIShieldCapacityDescriptorProps = IUICommonProps & {
	modifier: IModifier;
};

export default class UIShieldCapacityModifierDescriptor extends React.PureComponent<
	UIShieldCapacityDescriptorProps,
	{}
> {
	public static getClass(): string {
		return UIShieldCapacityModifierDescriptorClass;
	}

	public render() {
		const isPositive = BasicMaths.applyModifier(100, this.props.modifier) >= 100;
		return (
			<dl
				className={classnames(this.props.className || '', UIShieldCapacityModifierDescriptor.getClass())}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIShieldCapacityDescriptor.getIcon()}
					<span className={styles.caption}>{UIShieldCapacityDescriptor.getCaption()}</span>
					{this.props.tooltip && <UITooltip>{UIShieldCapacityDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<UIModifierDescriptor
						precision={4}
						className={isPositive ? styles.value_positive : styles.value_negative}
						modifier={this.props.modifier || {}}
					/>
				</dd>
			</dl>
		);
	}
}

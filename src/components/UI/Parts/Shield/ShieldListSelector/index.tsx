import * as React from 'react';
import {
	doesCollectibleSetIncludeItem,
	findCollectibleItemInSet,
} from '../../../../../../definitions/core/Collectible';
import Players from '../../../../../../definitions/player/types';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import Weapons from '../../../../../../definitions/technologies/types/weapons';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { IUICommonProps } from '../../../index';
import { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UIListSelector from '../../../Templates/ListSelector';
import UIShieldGroupDescriptor from '../ShieldGroupDescriptor';
import UIShieldSlotDescriptor from '../ShieldSlotDescriptor';
import styles from './index.scss';

const weaponListSelectorClass = styles.shieldListSelector;

type UIShieldListSelectorProps = IUICommonProps & {
	onShieldSelected: (shieldId: number) => any;
	shieldSlot?: UnitDefense.TShieldSlotType;
	selectedShield?: number;
	player?: Players.IPlayer;
};

type IShieldHash = EnumProxy<UnitDefense.TShieldSlotType, number[]>;

interface UIShiedListSelectorState {
	shieldsAvailable: IShieldHash;
}

export default class UIShieldListSelector extends React.PureComponent<
	UIShieldListSelectorProps,
	UIShiedListSelectorState
> {
	public static getClass(): string {
		return weaponListSelectorClass;
	}

	protected get shieldOptions() {
		return this.state.shieldsAvailable[this.shieldSlot];
	}

	protected get shieldSlot(): UnitDefense.TShieldSlotType {
		return this.props.shieldSlot
			? this.state.shieldsAvailable[this.props.shieldSlot]
				? this.props.shieldSlot
				: Object.keys(this.state.shieldsAvailable)[0]
			: this.props.selectedShield
			? Object.keys(this.state.shieldsAvailable).find((k) =>
					doesCollectibleSetIncludeItem(this.props.selectedShield, this.state.shieldsAvailable[k]),
			  )
			: Object.keys(this.state.shieldsAvailable)[0] || null;
	}

	protected getSelectedShield(group = this.shieldSlot): number {
		if (!group) return null;
		return doesCollectibleSetIncludeItem(this.props.selectedShield, this.state.shieldsAvailable[group])
			? this.props.selectedShield
			: this.state.shieldsAvailable[group][0];
	}

	public static getDerivedStateFromProps(props: UIShieldListSelectorProps, state: UIShiedListSelectorState) {
		return {
			...(state || {}),
			shieldsAvailable: UIShieldListSelector.buildShieldHash(props),
		};
	}

	protected static buildShieldHash(props: UIShieldListSelectorProps) {
		const provider = DIContainer.getProvider(DIInjectableCollectibles.shield);
		const r = {} as IShieldHash;
		Object.keys(provider.getAll()).forEach((shieldId) => {
			if (!props.player || props.player.isShieldAvailable(shieldId)) {
				const key = provider.get(shieldId).type;
				r[key] = (r[key] || []).concat(shieldId);
			}
		});
		return r;
	}

	public constructor(props: UIShieldListSelectorProps) {
		super(props);
		const shieldsAvailable = UIShieldListSelector.buildShieldHash(props);
		this.state = {
			shieldsAvailable,
		};
	}

	public render() {
		const shieldGroupCaption = (slot: UnitDefense.TShieldSlotType) => (
			<span className={styles.shieldSlot}>
				<UIShieldGroupDescriptor group={slot} tooltip={true} />
			</span>
		);
		const onGroupSelect = (slot: UnitDefense.TShieldSlotType) =>
			this.props.onShieldSelected(this.getSelectedShield(slot));
		const shieldCaption = (shieldId: number) => (
			<span className={styles.shieldCaption}>
				{DIContainer.getProvider(DIInjectableCollectibles.shield).get(shieldId).caption}
			</span>
		);
		const onShieldSelect = (shieldId: number) => this.props.onShieldSelected(shieldId);
		return (
			<div className={[UIShieldListSelector.getClass(), this.props.className].join(' ')}>
				<UIListSelector
					options={this.props.shieldSlot ? [this.props.shieldSlot] : Object.keys(this.state.shieldsAvailable)}
					selectedItem={this.shieldSlot}
					buttonColor={TSVGSmallTexturedIconStyles.blue}
					optionRender={shieldGroupCaption}
					onSelect={onGroupSelect}
				/>
				<UIListSelector
					options={this.shieldOptions}
					selectedItem={this.getSelectedShield()}
					selectedIndex={findCollectibleItemInSet(this.getSelectedShield(), this.shieldOptions)}
					buttonColor={TSVGSmallTexturedIconStyles.blue}
					optionRender={shieldCaption}
					onSelect={onShieldSelect}
				/>
			</div>
		);
	}
}

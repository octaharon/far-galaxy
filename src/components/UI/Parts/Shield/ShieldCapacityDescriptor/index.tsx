import * as React from 'react';
import UIContainer, { getTemplateColor, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSegmentedBar, { ISVGSegmentedBarSegment } from '../../../SVG/SegmentedBar';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import UIHitPointsInlineDescriptor from '../../Chassis/HitPointsInlineDescriptor';
import UIArmorDescriptor from '../../Armor/ArmorDescriptor';
import { SVGShieldEntityIconID } from '../ShieldSlotDescriptor';
import styles from './index.scss';

const UIShieldCapacityDescriptorClass = styles.shieldCapacityDescriptor;

export type UIShieldCapacityDescriptorProps = IUICommonProps & {
	totalAmount: number;
	overcharge?: boolean;
	currentAmount?: number;
};

export default class UIShieldCapacityDescriptor extends React.PureComponent<UIShieldCapacityDescriptorProps, {}> {
	public static getClass(): string {
		return UIShieldCapacityDescriptorClass;
	}

	public static getIcon(className = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.blue} className={className}>
				<use {...SVGIconContainer.getXLinkProps(SVGShieldEntityIconID)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption() {
		return 'Shield Capacity';
	}

	public static getTooltip() {
		return (
			<span>
				{UIShieldCapacityDescriptor.getIcon(UITooltip.getIconClass())}
				<em>{UIShieldCapacityDescriptor.getCaption()}</em> describes how much damage a shield can absorb. When
				the <em>capacity</em> drops to zero, the shield is discharged and the damage is dealt to target's{' '}
				<UIHitPointsInlineDescriptor /> after being reduced by {UIArmorDescriptor.getEntityCaption()}
			</span>
		);
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIShieldCapacityDescriptor.getIcon()}
				<span className={styles.capacityCaption}>{UIShieldCapacityDescriptor.getCaption()}</span>
			</em>
		);
	}

	public render() {
		const currentAmount = isNaN(this.props.currentAmount) ? this.props.totalAmount : this.props.currentAmount;
		const valueClass =
			this.props.overcharge && currentAmount >= this.props.totalAmount
				? styles.max_decay
				: currentAmount > 0
				? styles.current
				: styles.zero;
		return (
			<span className={[UIShieldCapacityDescriptor.getClass()].join(' ')}>
				{currentAmount !== this.props.totalAmount && (
					<SVGSegmentedBar
						className={styles.bar}
						segments={[
							{
								value: currentAmount,
								colorTop: getTemplateColor('dark_blue'),
								colorBottom: getTemplateColor('light_green'),
							} as ISVGSegmentedBarSegment,
						].concat(
							isNaN(this.props.currentAmount)
								? []
								: [
										{
											value: this.props.totalAmount - this.props.currentAmount,
											colorTop: getTemplateColor('text_dark'),
											colorBottom: '#000',
										} as ISVGSegmentedBarSegment,
								  ],
						)}
					/>
				)}
				<span className={valueClass}>{Math.round(currentAmount)}</span>/
				<span className={this.props.overcharge ? styles.max_decay : styles.max}>
					{Math.round(this.props.totalAmount)}
				</span>
			</span>
		);
	}
}

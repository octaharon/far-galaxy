import * as React from 'react';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import { getTemplateColor, IUICommonProps } from '../../../index';
import SVGHexIcon from '../../../SVG/HexIcon';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import Style from './index.scss';

export type UIShieldSlotDescriptorProps = IUICommonProps & {
	slotType: UnitDefense.TShieldSlotType;
	highlight?: boolean;
};

export const SVGShieldEntityIconID = 'shield-entity-icon';

SVGIconContainer.registerSymbol(
	SVGShieldEntityIconID,
	<symbol viewBox="0 0 502.239 502.239">
		<path d="m341.835 334.166c24.194-33.632 39.068-72.278 43.711-113.07l-134.426-38.225-134.427 38.225c4.642 40.791 19.516 79.438 43.711 113.07 23.557 32.744 54.82 59.087 90.716 76.512 35.895-17.425 67.159-43.768 90.715-76.512z" />
		<path d="m251.12 151.682 136 38.673v-62.379l-136-38.673-136 38.673v62.379z" />
		<path d="m266.12 0v62.379l151 42.939v88.02c0 57.276-17.611 112.032-50.931 158.349-26.048 36.205-60.501 65.447-100.069 85.043v65.51c59.749-23.283 111.028-63.044 148.774-115.514 40.708-56.586 62.226-123.459 62.226-193.388v-133.338z" />
		<path d="m236.12 436.729c-39.568-19.596-74.022-48.838-100.068-85.043-33.32-46.316-50.932-101.072-50.932-158.349v-88.02l151-42.939v-62.378l-211 60v133.337c0 69.929 21.518 136.802 62.226 193.388 37.747 52.47 89.026 92.231 148.774 115.514z" />
	</symbol>,
);

export default class UIShieldSlotDescriptor extends React.PureComponent<UIShieldSlotDescriptorProps, {}> {
	public static getSVGText(slot: UnitDefense.TShieldSlotType, highlight?: boolean) {
		if (highlight || slot === UnitDefense.TShieldSlotType.none || !slot)
			return (
				<SVGSmallTexturedIcon
					style={highlight ? TSVGSmallTexturedIconStyles.gray : TSVGSmallTexturedIconStyles.gray}
					asSvg={true}>
					<g
						style={{
							transformOrigin: '50% 50%',
							transform: 'scale(0.7) translateY(5%)',
						}}>
						<use {...SVGIconContainer.getXLinkProps(SVGShieldEntityIconID)} />
					</g>
				</SVGSmallTexturedIcon>
			);
		return (
			<text
				x="50%"
				y="50%"
				textAnchor="middle"
				dominantBaseline="middle"
				className={Style.text}
				stroke={getTemplateColor('dark_blue')}
				strokeWidth="0.5"
				fill={getTemplateColor('text_highlight')}>
				{UIShieldSlotDescriptor.getShieldSlotAbbr(slot)}
			</text>
		);
	}

	public static getShieldSlotDescription(slot: UnitDefense.TShieldSlotType) {
		const getIcon = (s: UnitDefense.TShieldSlotType) => {
			return (
				<SVGHexIcon
					className={[UITooltip.getIconClass()].join(' ')}
					colorTop={getTemplateColor('dark_blue')}
					colorBottom={getTemplateColor('dark_green')}>
					{UIShieldSlotDescriptor.getSVGText(slot, !slot)}
				</SVGHexIcon>
			);
		};
		switch (slot) {
			case UnitDefense.TShieldSlotType.small:
				return (
					<span>
						{getIcon(slot)}
						<b>{UIShieldSlotDescriptor.getShieldSlotCaption(slot)}</b> can fit a portable shield generator,
						which can absorb some incoming energy enhancing the surviving capabilities of the bearer
					</span>
				);
			case UnitDefense.TShieldSlotType.medium:
				return (
					<span>
						{getIcon(slot)}
						<b>{UIShieldSlotDescriptor.getShieldSlotCaption(slot)}</b> is the most common form factor of
						shield generators, suitable for the majority of vehicles and offering substantial damage
						absorption
					</span>
				);
			case UnitDefense.TShieldSlotType.large:
				return (
					<span>
						{getIcon(slot)}
						<b>{UIShieldSlotDescriptor.getShieldSlotCaption(slot)}</b> fits only on larger vehicles and
						specially designated combat units and exhibits solid capacity, great resistance against heavy
						weapons and often some unique properties
					</span>
				);
			default:
				return (
					<span>
						{getIcon(null)}
						<b>{UIShieldSlotDescriptor.getShieldSlotCaption(slot)}</b>{' '}
						<Tokenizer
							description={
								'offers additional protection to a Unit by absorbing incoming Attack Damage, which is mitigated by Shield Protection while Shield Capacity is above 0. When added to a Unit, Shields can provide additional Perks to it'
							}
						/>
					</span>
				);
		}
	}

	public static getShieldSlotCaption(slot: UnitDefense.TShieldSlotType) {
		switch (slot) {
			case UnitDefense.TShieldSlotType.large:
				return 'Large Shield';
			case UnitDefense.TShieldSlotType.medium:
				return 'Medium Shield';
			case UnitDefense.TShieldSlotType.small:
				return 'Small Shield';
			default:
				return 'Shields';
		}
	}

	public static getShieldSlotAbbr(slot: UnitDefense.TShieldSlotType) {
		switch (slot) {
			case UnitDefense.TShieldSlotType.small:
				return 'S';
			case UnitDefense.TShieldSlotType.medium:
				return 'M';
			case UnitDefense.TShieldSlotType.large:
				return 'L';
			default:
				return 'A';
		}
	}

	public render() {
		const self = this.constructor as typeof UIShieldSlotDescriptor;
		return (
			<SVGHexIcon
				className={[this.props.className || '', Style.armorSlotDescriptor].join(' ')}
				onClick={this.props.onClick}
				highlight={!!this.props.highlight}
				colorTop={getTemplateColor(this.props.highlight ? 'dark_blue' : 'navy_blue')}
				colorBottom={getTemplateColor(this.props.highlight ? 'dark_green' : 'dark_blue')}
				tooltip={this.props.tooltip ? self.getShieldSlotDescription(this.props.slotType) : null}>
				{UIShieldSlotDescriptor.getSVGText(this.props.slotType, this.props.highlight)}
			</SVGHexIcon>
		);
	}
}

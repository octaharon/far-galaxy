import * as React from 'react';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import { DODGE_IMMUNITY_THRESHOLD } from '../../../../../../definitions/tactical/damage/constants';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import UIAttackDescriptor from '../../Attack/AttackDescriptor';
import UIAttackDeliveryDescriptor from '../../Attack/DeliveryDescriptor';
import UIHitChanceDescriptor from '../../Attack/HitChanceDescriptor';
import styles from '../../Attack/RateDescriptor/index.scss';
import Style from './index.scss';

export type UIDodgeDescriptorProps = IUICommonProps & {
	tooltip?: boolean;
};

export const SVGDodgeIconID = 'dodge-icon-shape';

SVGIconContainer.registerSymbol(
	SVGDodgeIconID,
	<symbol viewBox="0 0 512 512">
		<g>
			<polygon points="312.5685424949238,312.5685424949238 199.4314575050762,312.5685424949238 199.4314575050762,199.4314575050762 312.5685424949238,199.4314575050762" />
			<path d="m144.356 257.141c0-56.426 42.08-103.202 96.505-110.613v-146.528c-24.671 1.06-49.245 4.703-73.249 10.948l-44.128 11.479v11.705c0 36.696-24.966 68.118-60.713 76.412l-11.717 2.719v155.06c0 106.24 67.414 201.082 167.752 236.001l22.055 7.676v-144.245c-54.425-7.412-96.505-54.188-96.505-110.614z" />
			<path d="m343.321 326.623h110.727c4.215-17.54 6.558-35.709 6.858-54.244h-94.306c-2.787 20.324-11.06 38.921-23.279 54.244z" />
			<path d="m343.481 187.858c12.171 15.335 20.399 33.931 23.147 54.244h94.319v-54.244z" />
			<path d="m271.139 367.755v43.39h144.898c11.694-16.781 21.338-34.993 28.651-54.244h-138.604c-10.772 5.43-22.537 9.164-34.945 10.854z" />
			<path d="m271.139 146.528c12.56 1.71 24.464 5.513 35.343 11.052h154.464v-44.317l-11.718-2.719c-6.858-1.591-13.315-4.041-19.279-7.208h-158.81z" />
			<path d="m398.77 73.058c-6.561-11.526-10.255-24.833-10.255-38.926v-11.705l-44.127-11.479c-24.004-6.245-48.579-9.888-73.249-10.948v73.058z" />
			<path d="m271.139 512 22.056-7.676c38.033-13.236 71.33-35.088 98.024-62.902h-120.08z" />
		</g>
	</symbol>,
);

export default class UIDodgeCaption extends React.PureComponent<UIDodgeDescriptorProps, {}> {
	public static getClass(): string {
		return Style.dodgeDescriptor;
	}

	public static getCaptionClass(): string {
		return Style.dodgeCaption;
	}

	public static getIcon(className?: string) {
		return (
			<SVGSmallTexturedIcon
				className={classnames(className || '', UIContainer.iconClass)}
				style={TSVGSmallTexturedIconStyles.pink}
			>
				<use {...SVGIconContainer.getXLinkProps(SVGDodgeIconID)} />1
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption() {
		return 'Dodge';
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIDodgeCaption.getIcon()}
				<span className={Style.dodgeCaption}>{UIDodgeCaption.getCaption()}</span>
			</em>
		);
	}

	public static getTooltip() {
		return (
			<span>
				{UIDodgeCaption.getIcon(UITooltip.getIconClass())}
				<b>{UIDodgeCaption.getCaption()}</b> is an ability to evade or suppress incoming attacks, reducing{' '}
				<em>{UIHitChanceDescriptor.getEntityCaption()}</em> of{' '}
				{UIAttackDescriptor.getEntityCaption({
					delivery: UnitAttack.deliveryType.immediate,
				} as UnitAttack.IDirectDamageAttackDecorator)}{' '}
				of a particular kind or all of them but{' '}
				<UIAttackDeliveryDescriptor delivery={UnitAttack.deliveryType.immediate} />.{' '}
				{UIDodgeCaption.getEntityCaption()} above <em>{(DODGE_IMMUNITY_THRESHOLD * 100).toFixed(0)}%</em>{' '}
				renders the bearer immune to that kind of <em>Attacks</em>, including{' '}
				{UIAttackDescriptor.getEntityCaption({
					delivery: UnitAttack.deliveryType.immediate,
					[UnitAttack.attackFlags.AoE]: true,
				})}
				.
			</span>
		);
	}

	constructor(props: UIDodgeDescriptorProps) {
		super(props);
	}

	public render() {
		return (
			<span
				className={[
					this.props.className || '',
					UIDodgeCaption.getClass(),
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				].join(' ')}
			>
				{UIDodgeCaption.getIcon()}
				<span className={Style.dodgeCaption}> {this.props.children}</span>
				{this.props.tooltip && <UITooltip>{UIDodgeCaption.getTooltip()}</UITooltip>}
			</span>
		);
	}
}

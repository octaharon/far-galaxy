import * as React from 'react';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import { DefenseFlagCaptions } from '../../../../../../definitions/tactical/damage/strings';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import { termTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import UIAttackDescriptor from '../../Attack/AttackDescriptor';
import UIAttackDeliveryDescriptor from '../../Attack/DeliveryDescriptor';
import UIHitChanceDescriptor from '../../Attack/HitChanceDescriptor';
import UIHitPointsInlineDescriptor from '../../Chassis/HitPointsInlineDescriptor';
import UIDamageDefinitionDescriptor from '../../Damage/DamageDefinitionDescriptor';
import UIDamageModifierInlineDescriptor from '../../Damage/DamageModifierInlineDescriptor';
import UIArmorDescriptor from '../../Armor/ArmorDescriptor';
import UIDamageProtectionDescriptor from '../DamageProtectionDescriptor';
import UIShieldDescriptor from '../../Shield/ShieldDescriptor';
import Style from './index.scss';

export type UIDefenseFlagsDescriptorProps = IUICommonProps & {
	flags: UnitDefense.TDefenseFlags;
	tooltip?: boolean;
	short?: boolean;
	forceCaption?: boolean;
};

export const UIDefenseFlagIconSMA = 'defense-flag-sma-icon';
export const UIDefenseFlagIconReactive = 'defense-flag-reactive-icon';
export const UIDefenseFlagIconEvasion = 'defense-flag-evasion-icon';
export const UIDefenseFlagIconHull = 'defense-flag-hull-icon';
export const UIDefenseFlagIconTachyon = 'defense-flag-tachyon-icon';
export const UIDefenseFlagIconDisposable = 'defense-flag-disposable-icon';
export const UIDefenseFlagIconResilient = 'defense-flag-resilient-icon';
export const UIDefenseFlagIconBoosters = 'defense-flag-boosters-icon';
export const UIDefenseFlagIconReplenishing = 'defense-flag-replenishing-icon';
export const UIDefenseFlagIconImmune = 'defense-flag-immune-icon';
export const UIDefenseFlagIconUnstoppable = 'defense-flag-unstoppable-icon';
export const UIDefenseFlagIconWarp = 'defense-flag-warp-icon';

const symbolMapping: EnumProxy<UnitDefense.defenseFlags, string> = {
	[UnitDefense.defenseFlags.sma]: UIDefenseFlagIconSMA,
	[UnitDefense.defenseFlags.reactive]: UIDefenseFlagIconReactive,
	[UnitDefense.defenseFlags.evasion]: UIDefenseFlagIconEvasion,
	[UnitDefense.defenseFlags.hull]: UIDefenseFlagIconHull,
	[UnitDefense.defenseFlags.tachyon]: UIDefenseFlagIconTachyon,
	[UnitDefense.defenseFlags.disposable]: UIDefenseFlagIconDisposable,
	[UnitDefense.defenseFlags.resilient]: UIDefenseFlagIconResilient,
	[UnitDefense.defenseFlags.boosters]: UIDefenseFlagIconBoosters,
	[UnitDefense.defenseFlags.replenishing]: UIDefenseFlagIconReplenishing,
	[UnitDefense.defenseFlags.warp]: UIDefenseFlagIconWarp,
	[UnitDefense.defenseFlags.immune]: UIDefenseFlagIconImmune,
	[UnitDefense.defenseFlags.unstoppable]: UIDefenseFlagIconUnstoppable,
};

SVGIconContainer.registerSymbol(
	UIDefenseFlagIconWarp,
	<symbol viewBox="0 0 512 512">
		<g
			style={{
				transformOrigin: '50% 50%',
				transform: 'scale(-1.1, 1)',
			}}>
			<circle cx="271" cy="156" r="15" />
			<circle cx="241" cy="356" r="15" />
			<path d="m362.679 183.519c6.893-4.595 16.206-2.733 20.801 4.16l15.547 23.321h51.973v-116c0-8.284-6.716-15-15-15-35.841 0-65-29.159-65-65 0-8.284-6.716-15-15-15h-200c-8.284 0-15 6.716-15 15 0 35.841-29.159 65-65 65-8.284 0-15 6.716-15 15v56h83.787l25.607-25.607c5.858-5.858 15.355-5.858 21.213 0 5.858 5.858 5.858 15.355 0 21.213l-30 30c-2.813 2.814-6.629 4.394-10.607 4.394h-90v30h124.364l43.839-38.359c-2.059-5.152-3.203-10.764-3.203-16.641 0-24.813 20.187-45 45-45s45 20.187 45 45-20.187 45-45 45c-8.003 0-15.518-2.109-22.036-5.787l-48.086 42.076c-2.735 2.392-6.244 3.711-9.878 3.711h-130v30h60c5.015 0 9.699 2.506 12.481 6.679l20 30c4.595 6.893 2.733 16.206-4.16 20.801-6.893 4.595-16.206 2.733-20.801-4.16l-15.548-23.32h-50.291c5.347 45.439 23.399 88.951 52.179 124.828 34.732 43.298 83.701 73.774 137.886 85.815 2.143.477 4.365.477 6.508 0 54.186-12.041 103.154-42.517 137.886-85.815 15.75-19.633 28.278-41.557 37.282-64.828h-67.208l-25.606 25.607c-5.858 5.858-15.355 5.858-21.213 0-5.858-5.858-5.858-15.355 0-21.213l30-30c2.812-2.814 6.627-4.394 10.605-4.394h82.879c2.429-9.863 4.249-19.883 5.44-30h-122.683l-43.839 38.359c2.059 5.152 3.203 10.764 3.203 16.641 0 24.813-20.187 45-45 45s-45-20.187-45-45 20.187-45 45-45c8.003 0 15.518 2.109 22.036 5.787l48.086-42.076c2.734-2.393 6.244-3.711 9.877-3.711h130v-30h-60c-5.015 0-9.698-2.506-12.48-6.679l-20-30c-4.595-6.894-2.732-16.206 4.16-20.802z" />
		</g>
	</symbol>,
)
	.registerSymbol(
		UIDefenseFlagIconUnstoppable,
		<symbol viewBox="0 0 512 512">
			<g
				style={
					{
						//	transformOrigin: '50% 50%',
						//	transform: 'scale(0.9 1)',
					}
				}>
				<path d="m122.857 245.954c0-68.37 51.803-124.859 118.218-132.302v-113.652c-58.515 1.597-116.826 12.58-172.532 32.965l-9.796 3.585v292.815c0 21.469 5.535 42.723 16.006 61.464s25.67 34.596 43.953 45.85l122.369 75.321v-133.744c-66.415-7.444-118.218-63.932-118.218-132.302z" />
				<path d="m443.457 32.965c-55.706-20.385-114.017-31.368-172.532-32.965v113.652c66.415 7.443 118.218 63.932 118.218 132.302s-51.803 124.858-118.218 132.302v133.744l122.369-75.322c18.283-11.254 33.481-27.108 43.953-45.85 10.471-18.741 16.006-39.996 16.006-61.464v-292.815z" />
				<path d="m359.293 245.954c0-56.956-46.337-103.293-103.293-103.293s-103.293 46.337-103.293 103.293 46.337 103.293 103.293 103.293 103.293-46.337 103.293-103.293zm-173.864 41.1 23.062-41.1-23.062-41.1h46.829l23.742-40.431 23.742 40.43h46.829l-23.062 41.1 23.062 41.1h-46.829l-23.742 40.432-23.742-40.43h-46.829z" />
			</g>
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconImmune,
		<symbol viewBox="0 0 512.001 512.001">
			<g
				style={{
					transformOrigin: '50% 50%',
					transform: 'scale(0.95) translateX(9%)',
				}}>
				<path
					d="M333.713,289.128c-3.839-1.555-8.211,0.3-9.765,4.14c-25.733,63.613-64.916,116.432-116.561,157.223v-218.54
			c0-4.142-3.358-7.5-7.5-7.5c-4.142,0-7.5,3.358-7.5,7.5v218.512C87.401,367.293,58.106,251.724,50.04,198.986l135.358-48.146
			c3.903-1.388,5.941-5.677,4.553-9.58c-1.388-3.902-5.678-5.94-9.58-4.553L39.226,186.912c-3.344,1.189-5.396,4.559-4.918,8.075
			c7.211,53.125,37.353,186.43,161.168,276.678c1.316,0.959,2.867,1.439,4.417,1.439c1.549,0,3.099-0.479,4.414-1.437
			c59.87-43.585,104.802-101.715,133.546-172.774C339.406,295.053,337.553,290.681,333.713,289.128z"
				/>
				<path
					d="M355.168,321.335c-3.763-1.735-8.217-0.092-9.952,3.669c-32.641,70.763-81.518,128.109-145.327,170.523
			c-40.3-26.799-74.988-59.934-103.163-98.553c-23.333-31.982-42.289-67.792-56.342-106.434
			c-17.527-48.195-23.887-92.256-25.155-113.206l184.659-65.682l29.517,10.499c3.903,1.387,8.192-0.65,9.58-4.553
			c1.388-3.903-0.65-8.192-4.553-9.58l-32.03-11.393c-1.626-0.578-3.401-0.578-5.027,0L4.987,165.058
			c-3.042,1.082-5.05,3.99-4.985,7.218c0.044,2.198,1.37,54.621,26.086,122.847c14.511,40.056,34.127,77.199,58.303,110.397
			c30.243,41.529,67.729,76.949,111.416,105.274c1.241,0.805,2.661,1.207,4.08,1.207s2.839-0.402,4.08-1.207
			c68.223-44.233,120.328-104.627,154.868-179.505C360.571,327.526,358.929,323.071,355.168,321.335z"
				/>
				<path
					d="M506.271,144.502l-82.261-19.974l21.001-47.653c1.248-2.832,0.628-6.14-1.56-8.328s-5.496-2.809-8.328-1.56l-47.65,21.002
			L367.499,5.731C366.682,2.368,363.671,0,360.211,0s-6.472,2.368-7.288,5.73L332.95,87.991l-47.655-21.003
			c-2.832-1.249-6.141-0.628-8.328,1.56c-2.188,2.188-2.808,5.497-1.56,8.328l21.004,47.653l-82.26,19.974
			c-3.363,0.816-5.73,3.828-5.73,7.288c0,3.46,2.368,6.472,5.73,7.288l82.26,19.973l-21.004,47.653
			c-1.249,2.832-0.629,6.14,1.56,8.328c2.188,2.188,5.495,2.808,8.328,1.56l47.655-21.003l19.973,82.26
			c0.816,3.363,3.828,5.73,7.288,5.73c3.46,0,6.472-2.368,7.288-5.73l19.974-82.26l47.65,21.003c2.833,1.249,6.14,0.628,8.328-1.56
			c2.188-2.188,2.808-5.496,1.56-8.328l-21.001-47.654l82.261-19.973c3.363-0.816,5.73-3.828,5.73-7.288
			C512.001,148.33,509.633,145.319,506.271,144.502z M411.764,166.589c-2.157,0.523-3.972,1.977-4.955,3.967
			c-0.983,1.991-1.034,4.314-0.139,6.346l16.823,38.172l-38.17-16.824c-2.031-0.896-4.355-0.845-6.346,0.138
			c-1.991,0.983-3.444,2.797-3.967,4.955l-14.799,60.95l-14.799-60.95c-0.523-2.157-1.977-3.972-3.967-4.955
			c-1.045-0.516-2.183-0.775-3.321-0.775c-1.029,0-2.06,0.212-3.024,0.637l-38.174,16.824l16.825-38.172
			c0.896-2.031,0.845-4.355-0.138-6.346c-0.983-1.991-2.797-3.443-4.955-3.967l-60.95-14.799l60.95-14.799
			c2.158-0.523,3.972-1.977,4.955-3.967c0.983-1.99,1.034-4.314,0.138-6.346l-16.825-38.172l38.173,16.825
			c2.031,0.894,4.355,0.845,6.346-0.139c1.99-0.983,3.443-2.798,3.967-4.955l14.799-60.951l14.799,60.951
			c0.523,2.158,1.977,3.972,3.967,4.955c1.992,0.983,4.315,1.034,6.346,0.138l38.17-16.824l-16.823,38.172
			c-0.895,2.031-0.844,4.355,0.139,6.346c0.983,1.99,2.798,3.443,4.955,3.967l60.951,14.799L411.764,166.589z"
				/>
				<path
					d="M199.888,168.339c-4.142,0-7.5,3.358-7.5,7.5v24.046c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-24.046
			C207.388,171.697,204.03,168.339,199.888,168.339z"
				/>
			</g>
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconReplenishing,
		<symbol viewBox="-33 0 512 512.001">
			<path d="m167.394531 192.480469c3.074219-3.808594 6.53125-7.296875 10.316407-10.398438v-96.066406h-16.226563c-18.707031 30.007813-37.148437 50.304687-74.089844 69.804687v33.019532c.070313 1.214844.152344 2.429687.238281 3.640625zm0 0" />
			<path d="m263.539062 236.941406c0-22.605468-18.386718-40.996094-40.992187-40.996094s-40.996094 18.390626-40.996094 40.996094c0 22.601563 18.390625 40.992188 40.996094 40.992188s40.992187-18.390625 40.992187-40.992188zm0 0" />
			<path d="m267.382812 182.082031c3.785157 3.101563 7.238282 6.589844 10.316407 10.398438h79.761719c.085937-1.210938.167968-2.425781.238281-3.640625v-33.019532c-36.941407-19.5-55.382813-39.796874-74.089844-69.804687h-16.226563zm0 0" />
			<path d="m177.710938 291.796875c-3.539063-2.898437-6.796876-6.128906-9.714844-9.648437h-58.921875c15.597656 36.574218 38.960937 68.847656 68.636719 94.417968zm0 0" />
			<path d="m277.097656 282.148438c-2.921875 3.519531-6.175781 6.75-9.714844 9.648437v84.769531c29.671876-25.570312 53.039063-57.84375 68.632813-94.417968zm0 0" />
			<path d="m434.566406 98.242188c-42.074218-14.671876-75.328125-46.972657-91.226562-88.628907l-3.671875-9.613281h-234.242188l-3.671875 9.613281c-15.898437 41.65625-49.152344 73.957031-91.230468 88.628907l-10.023438 3.492187v69.421875c0 68.128906 18.726562 134.898438 54.152344 193.089844 35.425781 58.195312 86.136718 105.488281 146.65625 136.777344l21.238281 10.976562 21.234375-10.980469c60.519531-31.285156 111.234375-78.578125 146.660156-136.773437 35.425782-58.191406 54.152344-124.960938 54.152344-193.089844v-69.421875zm-47 91.875c-2.8125 49.570312-17.144531 97.492187-41.4375 138.585937-25.484375 43.097656-62.105468 78.742187-105.90625 103.078125l-.398437.214844-17.277344 8.933594-17.675781-9.148438c-43.804688-24.335938-80.425782-59.980469-105.90625-103.082031-24.296875-41.089844-38.625-89.011719-41.4375-138.585938l-.023438-.84375v-51.949219l8.378906-4.097656c39.5-19.316406 54.871094-37.148437 74.375-69.816406l4.351563-7.28125h155.875l4.347656 7.28125c19.507813 32.667969 34.875 50.5 74.378907 69.816406l8.378906 4.097656zm0 0" />
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconBoosters,
		<symbol viewBox="0 0 512 512">
			<path
				d="M91.146,26.819c-6.063,2.1-10.134,7.856-10.134,14.328v55.164l-40.567,16.388c-5.592,2.258-9.304,7.676-9.426,13.757
c-0.061,3.009-1.158,72.557,22.399,154.259l74.883-29.573c-5.983-22.062-10.547-45.277-13.553-69.24
c-0.858-6.842,2.969-13.406,9.31-15.967l36.954-14.929v-47.781c0-7.28,5.128-13.53,12.211-14.883
c22.213-4.244,44.948-6.744,67.777-7.507V0C189.27,1.497,137.76,10.675,91.146,26.819z"
			/>
			<path
				d="M480.982,126.456c-0.122-6.081-3.834-11.499-9.426-13.757l-40.567-16.388V41.147c0-6.472-4.071-12.229-10.134-14.328
C374.24,10.675,322.73,1.497,271,0v80.834c22.829,0.762,45.564,3.262,67.777,7.507c7.083,1.353,12.211,7.603,12.211,14.883v47.781
l36.928,14.918c6.34,2.561,10.167,9.123,9.311,15.964c-3.009,24.034-7.546,47.25-13.51,69.261l74.865,29.567
C482.139,199.014,481.042,129.465,480.982,126.456z"
			/>
			<path
				d="M137.216,280.151l-74.528,29.433C95.134,401.152,152.31,475.271,241,512v-90.273
C189.679,389.925,157.101,337.347,137.216,280.151z"
			/>
			<path
				d="M374.77,280.145c-20.002,57.52-52.612,109.882-103.77,141.583V512c88.692-36.73,145.867-110.85,178.312-202.417
L374.77,280.145z"
			/>
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconResilient,
		<symbol viewBox="0 0 512 512">
			<g
				style={{
					transformOrigin: '50% 50%',
					transform: 'scale(1, 1)',
				}}>
				<path
					d="M471.7,61c-11,0-20.6,0.4-28-1.7C407.7,48.2,410,15.6,410,0.2L102,0c0,0,6.5,37.4-24.6,55.8c-8.8,5.2-21.1,5.5-37.2,5.5
	c-0.3,12.4-0.2,44.8-0.2,58.5s0.8,55.8,13.6,114.6c27-0.3,89.8-0.5,97.8-0.5s23.3-1,47.2-67l17.7,68.7l21.8-58.5l35.2,79.2
	l35.5-102.5c0,0,13.5,40.7,17.3,47.8c2.9,5.3,7.8,21.8,19.8,29.4c4.1,2.6,9.5,3.4,15.3,3.4c22.8,0,97.5,0,97.5,0s7.1-28.6,10.8-75
	C471.9,131.5,471.7,95,471.7,61z M405.3,209.1h-28.8c0,0,12.4-23.1,12.4-46.8S367.1,99,326.3,99c0,0-53-3.3-70.5,68.6
	c0,0-14.5-68.5-73.3-68.5c-18.1,0-31.9,8.6-42,17.9c-22.7,20.8-26.1,57.6-9.5,92c-0.5,0-27.3,0-27.3,0c-8.7-26-8.3-31.3-8.3-45.3
	c0-4-0.2-13.6,2.8-24.7c7.5-27.2,29.5-65.3,85.2-65.3c31.8,0,43.3,10.7,53.4,18.5c14.7,11.3,19,21.2,19,22.5
	c0,2.1,10.5-41.4,72.9-41.4s87,64.4,87,91.3S405.3,209.1,405.3,209.1z"
				/>
				<path
					d="M358.7,258.6c-36.3,0-48.7-52.7-48.7-52.7l-34.3,87.3L239,223l-24.7,54.3L194,220c-14,39.3-42.1,39.1-42.1,39.1
	S61,260,59.7,260c0,0,19.2,71.5,67.5,138.7C175.5,465.8,239,511.8,239,511.8h4V420h26c0,38.1,0,90.4,0,91.7c1.1-0.1,1.8,0,3,0
	c88.6-66.9,133.7-139.6,157.1-190.1c17.3-37.3,23.3-62.9,23.3-62.9S395,258.6,358.7,258.6z M305.5,328.3c-19,14.2-49.7,32-49.7,32
	s-8.9-4.1-19.8-10.8c-11.3-7-25-16.7-33.3-23.4c-16.5-13.2-48.2-42.3-48.2-42.3h38.7c0,0,16.5,15.2,30.3,26.2s32.3,21.2,32.3,21.2
	s16-10,29.5-20s33-27,33-27h37.2C355.5,284.2,326.7,312.5,305.5,328.3z"
				/>
			</g>
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconTachyon,
		<symbol viewBox="-34 0 512 512">
			<path d="m222.046875 301.210938c8.324219 0 15.097656-6.773438 15.097656-15.097657s-6.773437-15.097656-15.097656-15.097656c-8.328125 0-15.101563 6.773437-15.101563 15.097656s6.773438 15.097657 15.101563 15.097657zm0 0" />
			<path d="m148.871094 220.097656c8.328125 0 15.101562-6.773437 15.101562-15.097656s-6.773437-15.097656-15.101562-15.097656c-8.324219 0-15.097656 6.773437-15.097656 15.097656s6.773437 15.097656 15.097656 15.097656zm0 0" />
			<path d="m294.3125 220.097656c8.324219 0 15.097656-6.773437 15.097656-15.097656s-6.773437-15.097656-15.097656-15.097656c-8.328125 0-15.097656 6.773437-15.097656 15.097656s6.769531 15.097656 15.097656 15.097656zm0 0" />
			<path d="m309.257812 162.574219c17.476563 6.175781 30.042969 22.855469 30.042969 42.425781 0 24.804688-20.183593 44.988281-44.988281 44.988281-24.808594 0-44.988281-20.183593-44.988281-44.988281 0-19.570312 12.5625-36.25 30.042969-42.425781v-86.851563h-42.378907v167.964844c17.480469 6.175781 30.042969 22.859375 30.042969 42.425781 0 24.808594-20.179688 44.988281-44.984375 44.988281-24.808594 0-44.988281-20.179687-44.988281-44.988281 0-19.566406 12.5625-36.25 30.042968-42.425781v-167.964844h-43.285156v86.851563c17.480469 6.175781 30.042969 22.855469 30.042969 42.425781 0 24.804688-20.179687 44.988281-44.988281 44.988281-24.804688 0-44.984375-20.183593-44.984375-44.988281 0-19.570312 12.558593-36.25 30.042969-42.425781v-70.734375l-50.917969 48.980468v42.5625c5.546875 98.609376 56.203125 184.636719 135.585937 230.246094l3.449219 1.839844 3.445313-1.839844c79.382812-45.609375 130.039062-131.636718 135.585937-230.246094v-42.621093l-51.820313-50.527344zm0 0" />
			<path d="m334.9375 0h-225.785156l-109.152344 106.035156v65.121094c0 68.128906 18.726562 134.898438 54.148438 193.089844 35.425781 58.195312 86.140624 105.488281 146.660156 136.773437l21.238281 10.980469 21.234375-10.980469c60.519531-31.285156 111.234375-78.578125 146.660156-136.773437 35.421875-58.191406 54.148438-124.960938 54.148438-193.089844v-65.121094zm56.007812 184.617188c-2.875 52.28125-17.519531 102.824218-42.355468 146.167968-26.074219 45.515625-63.570313 83.164063-108.425782 108.886719l-.398437.21875-17.71875 9.457031-18.121094-9.675781c-44.859375-25.722656-82.351562-63.371094-108.425781-108.886719-24.835938-43.34375-39.480469-93.886718-42.355469-146.167968l-.023437-.820313v-55.699219l85.519531-82.265625h167.886719l84.4375 82.328125zm0 0" />
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconHull,
		<symbol viewBox="0 0 512 512">
			<path
				d="M429,191c-0.2-58.4,0-102,0-102C304,84,269,17,255,7c-6,6-59,81-174,81c0,0,0,179,0,204s24,143,161,201c0,0,7,2,13,12
	c0,0,5-8,12-12c2.5-1.4,75-25,126-101c19.9-29.7,33-67,36-102C429,259,429.1,221.4,429,191z M334,350c-21.5,17.2-46.4,27-79,27
	c-74,0-120-66-120-116c0-24.4,5-65,38-95c35-33,81-33,81-33c17.9,0,35.7,2.8,52,10c41.9,18.5,74,59.9,74,111
	C380,293.7,364,326,334,350z"
			/>
			<path d="M219,155c-28,10-69,45-69,99s36,87,65,99c-14-16-32-52-32-96C185,191,219,155,219,155z" />
			<path d="M249,151v209c0,0-48-19-51-103c0,0-2-37,19-72S249,151,249,151z" />
			<path d="M294,155c28,10,69,45,69,99s-36,87-65,99c14-16,32-52,32-96C328,191,294,155,294,155z" />
			<path d="M264,151v209c0,0,48-19,51-103c0,0,2-37-19-72S264,151,264,151z" />
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconEvasion,
		<symbol viewBox="0 0 512 512">
			<path
				d="M451.1,203.8V100h-15c-46.9,0-85-38.1-85-85V0h-80v122.6c47,17.4,35.7,86.5-15,87.5c-50.7-1-62-70.1-15-87.5V0h-80v15
	c0,46.9-38.1,85-85,85h-15v103.8C58.7,358.5,169.5,474.9,256,512C343.1,474.7,453.2,358.1,451.1,203.8z M281.6,248.1l21.2-21.2
	c17.6,17.6,17.6,46.1,0,63.7l-21.2-21.2C287.5,263.5,287.5,254,281.6,248.1z M245.7,283.8c0.8,0.8,2,1.8,3.6,2.7
	c2,1,4.5,1.8,7.3,1.7c3.1,0,6.7-1,10.3-4.4l21.2,21.2c0,0-4,4.5-11.2,8.2c-6.8,3.4-16.1,6.1-26.6,4.7c-6.4-0.9-13.1-3-20-7.9
	c-1.9-1.4-4-3.2-5.9-5C226,303.6,245.7,283.8,245.7,283.8z M209.2,226.9l21.2,21.2c-5.9,5.9-5.9,15.4,0,21.2l-21.2,21.2
	C191.6,273,191.6,244.5,209.2,226.9z M166.7,184.5l21.2,21.2c-29.3,29.3-29.3,76.9,0,106.1L166.7,333
	C125.8,292.1,125.8,225.4,166.7,184.5z M306,365.7c-12.3,6.5-28.4,12.5-50,12.5c-3.6,0-10.3-0.3-18-1.7c-5.8-1-12.1-2.6-18.4-5
	c-7.3-2.8-14.5-6.5-20.6-10.4c-7.4-4.7-13.3-9.7-16.9-13.7c0.8-0.9,20.3-20.4,21.3-21.3c0,0,8.9,10.3,26.2,17
	c5.3,2,10.9,3.8,19.5,4.6c3,0.3,6.4,0.3,10.2,0.3c4,0,10.1-0.8,16.2-2.4c5.8-1.6,11.7-3.9,17.2-6.8c9.5-4.9,16.9-12.7,16.9-12.7
	c3,2.9,19.6,19.5,21.2,21.2C327.2,350.8,318.3,359.2,306,365.7z M345.3,333l-21.2-21.2c29.3-29.3,29.3-76.9,0-106.1l21.2-21.2
	C386.2,225.4,386.2,292.1,345.3,333z"
			/>
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconDisposable,
		<symbol viewBox="-23 0 512 512.001">
			<path d="m232.5 345.6875c-4.925781 0-8.933594 4.003906-8.933594 8.929688v12.773437c0 4.925781 4.007813 8.933594 8.933594 8.933594s8.933594-4.007813 8.933594-8.933594v-12.773437c0-4.925782-4.007813-8.929688-8.933594-8.929688zm0 0" />
			<path d="m232.5 168.691406c-4.925781 0-8.933594 4.003906-8.933594 8.929688v84.171875c0 4.925781 4.007813 8.933593 8.933594 8.933593s8.933594-4.007812 8.933594-8.933593v-84.171875c0-4.925782-4.007813-8.929688-8.933594-8.929688zm0 0" />
			<path d="m439.53125 83.5c-27.417969-8.714844-48.96875-30.371094-57.648438-57.929688-4.816406-15.292968-18.792968-25.570312-34.777343-25.570312h-229.207031c-15.984376 0-29.964844 10.277344-34.78125 25.570312-8.679688 27.558594-30.230469 49.214844-57.648438 57.929688-15.234375 4.839844-25.46875 18.847656-25.46875 34.859375v59.394531c0 73.675782 22.097656 144.617188 63.902344 205.160156 38.671875 56.011719 92.066406 99.652344 154.40625 126.207032 4.511718 1.917968 9.351562 2.878906 14.191406 2.878906s9.679688-.960938 14.1875-2.878906c62.339844-26.550782 115.734375-70.191406 154.410156-126.207032 41.804688-60.542968 63.902344-131.484374 63.902344-205.160156v-59.394531c0-16.011719-10.234375-30.019531-25.46875-34.859375zm-168.097656 283.890625c0 21.46875-17.464844 38.933594-38.933594 38.933594s-38.933594-17.464844-38.933594-38.933594v-12.773437c0-21.464844 17.464844-38.933594 38.933594-38.933594s38.933594 17.464844 38.933594 38.933594zm0-105.597656c0 21.46875-17.464844 38.933593-38.933594 38.933593s-38.933594-17.464843-38.933594-38.933593v-84.171875c0-21.464844 17.464844-38.933594 38.933594-38.933594s38.933594 17.464844 38.933594 38.933594zm54.980468-181.71875h-187.828124c-8.285157 0-15-6.714844-15-15 0-8.285157 6.714843-15 15-15h187.828124c8.285157 0 15 6.714843 15 15 0 8.285156-6.714843 15-15 15zm0 0" />
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconSMA,
		<symbol viewBox={'0 0 512 512'}>
			<g
				style={
					{
						//	transformOrigin: '50% 50%',
						//	transform: 'scale(0.95) translateX(5%)',
					}
				}>
				<path d="m189.983 172.72-30.272-17.465c-4.672 11.74-7.244 24.534-7.244 37.919 0 13.392 2.575 26.193 7.251 37.937l30.265-17.48z" />
				<path d="m255.335 60.383c44.78 0 84.451 22.283 108.516 56.341h114.174v-53.887h-14.962c-26.398 0-47.875-21.477-47.875-47.876v-14.961h-318.376v14.962c0 26.399-21.477 47.876-47.875 47.876h-14.962v53.887h112.844c24.065-34.059 63.735-56.342 108.516-56.342z" />
				<path d="m335.949 256.999-30.222-17.452-35.425 20.453v34.95c26.487-3.879 49.726-17.885 65.647-37.951z" />
				<path d="m270.3 126.347 35.429 20.455 30.226-17.444c-15.921-20.071-39.164-34.081-65.655-37.959z" />
				<path d="m204.942 146.804 35.43-20.455v-34.951c-26.494 3.878-49.74 17.89-65.662 37.964z" />
				<path d="m240.374 294.95v-34.952l-35.426-20.453-30.225 17.457c15.923 20.067 39.164 34.072 65.651 37.948z" />
				<path d="m122.544 193.174c0-16.363 2.983-32.04 8.42-46.527h-96.989v64.864c0 104.978 52.445 201.146 137.377 258.29v-173.845c-29.772-24.372-48.808-61.394-48.808-102.782z" />
				<path d="m255.338 234.086 35.425-20.452v-40.918l-35.427-20.454-35.43 20.455v40.912z" />
				<path d="m388.126 193.174c0 41.388-19.036 78.41-48.808 102.781v174.74c85.716-57.032 138.707-153.658 138.707-259.185v-64.864h-98.319c5.437 14.488 8.42 30.165 8.42 46.528z" />
				<path d="m320.693 213.634 30.26 17.474c4.675-11.744 7.25-24.543 7.25-37.933 0-13.387-2.573-26.183-7.246-37.924l-30.263 17.466v40.917z" />
				<path d="m255.335 325.965c-19.241 0-37.537-4.119-54.059-11.513v173.14c7.048 3.675 54.724 24.408 54.724 24.408s46.767-20.304 53.394-23.719v-173.829c-16.522 7.394-34.818 11.513-54.059 11.513z" />
			</g>
		</symbol>,
	)
	.registerSymbol(
		UIDefenseFlagIconReactive,
		<symbol viewBox="0 0 512 512">
			<path d="m268.188 277.24c-5.713-7.91-18.662-7.939-24.375 0-7.412 10.283-16.362 19.233-26.572 26.572-3.955 2.856-6.24 7.31-6.24 12.188 0 8.276 6.724 15 15 15 4.395 0 14.092-1.758 24.976-5.64l5.024-1.787 5.024 1.787c10.883 3.882 20.58 5.64 24.975 5.64 8.276 0 15-6.724 15-15 0-4.878-2.271-9.331-6.24-12.188-10.283-7.426-19.234-16.362-26.572-26.572z" />
			<path d="m256 0-135 34.281v11.719c0 24.814-20.186 45-45 45h-15v192.524c0 161.426 182.49 224.316 190.254 226.894l4.746 1.582 4.746-1.582c7.764-2.578 190.254-65.468 190.254-226.894v-192.524h-15c-24.814 0-45-20.186-45-45v-11.719zm0 121c27.056 0 45 36.108 45 60 0 17.794-9.659 30-37.471 30h-15.474c-21.751-.493-37.055-7.123-37.055-30 0-23.892 17.944-60 45-60zm-127.295 131.367c-16.89-16.89-29.736-55.122-10.605-74.253 18.818-18.759 57.25-6.352 74.238 10.605v.015s0 0 .015 0c17.164 17.203 9.181 33.519-7.881 50.317l-5.42 5.42c-16.901 17.196-33.364 24.879-50.347 7.896zm157.295 108.633c-7.676 0-18.823-2.139-30-5.684-11.177 3.545-22.324 5.684-30 5.684-24.814 0-45-20.186-45-45 0-14.429 6.987-28.081 18.706-36.533 7.485-5.376 14.326-12.231 19.775-19.79 16.934-23.408 56.177-23.394 73.052.029 5.376 7.485 12.231 14.326 19.79 19.775l.015.015c11.689 8.438 18.662 22.09 18.662 36.504 0 24.814-20.186 45-45 45zm107.9-182.9c19.131 19.146 6.284 57.378-10.605 74.268-16.983 16.983-33.446 9.3-50.347-7.896l-5.42-5.42c-17.13-16.864-25.005-33.193-7.866-50.332 17.117-17.117 55.454-29.286 74.238-10.62z" />
		</symbol>,
	);

export default class UIDefenseFlagsDescriptor extends React.PureComponent<UIDefenseFlagsDescriptorProps, {}> {
	public static getDefenseFlagIcon(flag: UnitDefense.defenseFlags, className: string = Style.icon) {
		return (
			<SVGSmallTexturedIcon key={flag} style={TSVGSmallTexturedIconStyles.light} className={className}>
				<use {...SVGIconContainer.getXLinkProps(symbolMapping[flag])} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getDefenseFlagCaption(f: UnitDefense.defenseFlags): string {
		return DefenseFlagCaptions[f || 'default'];
	}

	public static getDefenseFlagDescription(f: UnitDefense.defenseFlags) {
		const icon = UIDefenseFlagsDescriptor.getDefenseFlagIcon(f, UIContainer.tooltipIconClass);
		const fullTitles: Partial<Record<UnitDefense.defenseFlags, string>> = {
			[UnitDefense.defenseFlags.sma]: 'Superconductor Magnetic Array',
			[UnitDefense.defenseFlags.hull]: 'Electromagnetic Hull',
		};
		const descriptions: Partial<Record<UnitDefense.defenseFlags, string>> = {
			[UnitDefense.defenseFlags.sma]:
				'relays the incoming energy to the weapon system of the owner, yielding +1% Weapon Damage per each 2% Hit Points missing',
			[UnitDefense.defenseFlags.reactive]:
				'negates any Deferred Damage and proactively suppresses incoming Projectile, Missile and Drone Attacks, reducing their Damage to Shields by both Shield Protection and Armor Protection.',
			[UnitDefense.defenseFlags.evasion]:
				'allows for better defensive maneuvers to be performed against incoming Projectile, Missile, Surface and Aerial Attacks, reducing their Hit Chance by half',
			[UnitDefense.defenseFlags.resilient]:
				'system transfers the energy absorbed by chassis to reinforce defensive circuits, granting +1% Armor Protection (limited by 80%) per each 2% of Hit Points missing',
			[UnitDefense.defenseFlags.replenishing]:
				"fully restores the least reduced Shield Capacity, if there's any, unless any Hit Points Damage were taken last Turn",
			[UnitDefense.defenseFlags.warp]:
				'renders the Unit immune to DOT, and Beam, Supersonic and Immediate Attacks drain -50% less Shield Capacity',
			[UnitDefense.defenseFlags.boosters]:
				'is a one-off battery that recharges 25% of Shield Capacity on the innermost Shield, when the Unit takes Damage to Hit Points',
			[UnitDefense.defenseFlags.tachyon]:
				"is a matter displacement device that counteracts all possibly incoming Attacks with insufficient Hit Chance, doubling the probability they will miss. Doesn't affect Immediate Damage sources",
			[UnitDefense.defenseFlags.hull]:
				'safely negates any Damage to Hit Points from Attacks that hit the Shields, and makes the owner immune to' +
				' Surface, Cloud and Aerial Area Attacks',
			[UnitDefense.defenseFlags.disposable]:
				'provides additional layer of Armor and Shields, which is removed whenever any Hit Points Damage is taken',
			[UnitDefense.defenseFlags.unstoppable]: `This Unit is ${UIDefenseFlagsDescriptor.getDefenseFlagCaption(
				f,
			)}, being invincible to most Negative Status Effects, including Deferred and DOT`,
			[UnitDefense.defenseFlags.immune]: `This Unit is ${UIDefenseFlagsDescriptor.getDefenseFlagCaption(
				f,
			)} to all Damage and most Negative Status Effects, both present and incoming`,
		};

		if (!descriptions[f]) return 'Unknown Perk';
		return termTooltipTemplate(
			icon,
			fullTitles[f] || UIDefenseFlagsDescriptor.getDefenseFlagCaption(f),
			descriptions[f].includes(UIDefenseFlagsDescriptor.getDefenseFlagCaption(f))
				? descriptions[f]
				: UIDefenseFlagsDescriptor.getDefenseFlagCaption(f) + ' ' + descriptions[f],
		);
	}

	public static getEntityCaption(flag: UnitDefense.defenseFlags) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIDefenseFlagsDescriptor.getDefenseFlagIcon(flag, UIContainer.iconClass)}
				<span className={Style.defensePerkCaption}>{UIDefenseFlagsDescriptor.getDefenseFlagCaption(flag)}</span>
			</em>
		);
	}

	public render() {
		const self = this.constructor as typeof UIDefenseFlagsDescriptor;
		const cls = classnames(
			Style.defenseFlagDescriptor,
			this.props.short ? UIContainer.descriptorClass : '',
			this.props.className || '',
		);
		const withCaption = !this.props.short || !!this.props.forceCaption;
		const WrappingTag = (props: React.PropsWithChildren<{ key?: React.Key; className?: string }>) =>
			this.props.short ? <span {...props} /> : <dt {...props} />;
		const Flags = Object.values(UnitDefense.defenseFlags)
			.map((f: UnitDefense.defenseFlags) =>
				this.props.flags[f] ? (
					<WrappingTag
						key={f}
						className={classnames(
							Style.item,
							!withCaption || this.props.short ? Style.short : UIContainer.descriptorClass,
							this.props.tooltip ? UIContainer.hasTooltipClass : '',
						)}>
						{self.getDefenseFlagIcon(f)}
						{withCaption && (
							<span className={Style.defensePerkCaption}>{self.getDefenseFlagCaption(f)}</span>
						)}
						{!!this.props.tooltip && <UITooltip>{self.getDefenseFlagDescription(f)}</UITooltip>}
					</WrappingTag>
				) : null,
			)
			.filter((v) => !!v);
		if (!Flags.length) return this.props.short ? <>&nbsp;</> : null;
		return this.props.short ? <span className={cls}>{Flags}</span> : <dl className={cls}>{Flags}</dl>;
	}
}

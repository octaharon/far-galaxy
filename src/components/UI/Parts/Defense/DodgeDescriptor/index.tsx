import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import AttackManager from '../../../../../../definitions/tactical/damage/AttackManager';
import { DODGE_IMMUNITY_THRESHOLD } from '../../../../../../definitions/tactical/damage/constants';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIAttackDeliveryDescriptor from '../../Attack/DeliveryDescriptor';
import UIHitChanceDescriptor from '../../Attack/HitChanceDescriptor';
import UIDodgeCaption from '../DodgeCaption';
import Style from './index.scss';

type UIDodgeDescriptorProps = IUICommonProps & {
	dodge: UnitDefense.TDodge;
	movement?: UnitChassis.movementType;
};

export default class UIDodgeDescriptor extends React.PureComponent<UIDodgeDescriptorProps, {}> {
	public render() {
		const getModifierClass = (m = 0) => UIHitChanceDescriptor.getHitChanceClass(m + 1);
		const getValue = (m = 0) => (m === 0 ? '-' : `${m > 0 ? '' : ''}${Math.round(m * 100)}%`);
		const getDodge = (t: UnitDefense.TDodge, k: UnitAttack.deliveryType) =>
			AttackManager.isReachableByAttack(k, this.props.movement ?? null) ? t[k] ?? t.default ?? NaN : Infinity;
		const dodge = this.props.dodge;
		const extraKeys = (
			Object.keys(UnitDefense.DodgeTemplate(0)).filter(
				(dt) => dt !== 'default' && dt !== UnitAttack.deliveryType.immediate,
			) as UnitAttack.deliveryType[]
		).filter((dt) => Boolean(getDodge(dodge, dt)));
		return (
			<dl
				className={classnames(
					Style.dodgeDescriptor,
					extraKeys.length ? Style.descriptorGroup : null,
					this.props.className,
				)}
			>
				<dt className={classnames(!!this.props.tooltip && UIContainer.hasTooltipClass, Style.caption)}>
					<UIDodgeCaption className={UIContainer.descriptorClass}>
						{UIDodgeCaption.getCaption()}
					</UIDodgeCaption>
					{!!this.props.tooltip && (
						<UITooltip className={Style.tooltip}>{UIDodgeCaption.getTooltip()}</UITooltip>
					)}
				</dt>
				<dd />
				{extraKeys.map((dt) => (
					<React.Fragment key={dt}>
						<dt key={dt + '-dt'} className={[Style.subtype].join(' ')}>
							<UIAttackDeliveryDescriptor
								tooltip={!!this.props.tooltip}
								delivery={dt as UnitAttack.deliveryType}
							/>
						</dt>
						<dd className={getModifierClass(getDodge(dodge, dt))} key={dt + '-dd'}>
							{getDodge(dodge, dt) > DODGE_IMMUNITY_THRESHOLD ? 'Immune' : getValue(getDodge(dodge, dt))}
						</dd>
					</React.Fragment>
				))}
			</dl>
		);
	}
}

import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import DefenseManager from '../../../../../../definitions/tactical/damage/DefenseManager';
import { biasToPercent } from '../../../../../utils/wellRounded';
import { classnames, getTemplateColor, IUICommonProps, UI_SYMBOLS } from '../../../index';
import UISVGPolarChart from '../../../SVG/PolarChart';
import SVGProtectionIcon, { SVGProtectionSymbolIDs } from '../../../SVG/ProtectionIcon';
import UIDamageTypeDescriptor from '../../Damage/DamageTypeDescriptor';
import styles from './index.scss';
import describeModifier = BasicMaths.describeModifier;

export interface UIAbsorptionChartProps extends IUICommonProps {
	protection: UnitDefense.TArmor;
	thresholdScale?: number;
	biasScale?: number;
	factorScale?: number;
	kind: UnitDefense.TProtectionKind;
}

export const AbsorptionChartScale = {
	bias: 10,
	threshold: 10,
	factor: 0.1,
};

export default class UIAbsorptionChart extends React.PureComponent<UIAbsorptionChartProps, {}> {
	public static getClass(): string {
		return styles.absorptionChart;
	}

	public render() {
		const { protection, thresholdScale = 3, biasScale = 5, factorScale = 4, kind } = this.props;
		const iconOutlineColor = getTemplateColor(kind === 'shield' ? 'light_blue' : 'light_orange');
		const thresholdValues = Damage.TDamageTypesArray.map((dt) => ({
			damageType: dt,
			value: DefenseManager.getDamageProtectorDecorator(protection, dt)?.threshold || 0,
			colorInner: UIDamageTypeDescriptor.getGradientColorBottom(dt),
			colorOuter: UIDamageTypeDescriptor.getGradientColorTop(dt),
		}));
		const biasValues = Damage.TDamageTypesArray.map((dt) => ({
			damageType: dt,
			value: -DefenseManager.getDamageProtectorDecorator(protection, dt)?.bias || 0,
			colorInner: UIDamageTypeDescriptor.getGradientColorBottom(dt),
			colorOuter: UIDamageTypeDescriptor.getGradientColorTop(dt),
		}));
		const factorValues = Damage.TDamageTypesArray.map((dt) => ({
			damageType: dt,
			value: 1 - (DefenseManager.getDamageProtectorDecorator(protection, dt)?.factor || 1),
			colorInner: UIDamageTypeDescriptor.getGradientColorBottom(dt),
			colorOuter: UIDamageTypeDescriptor.getGradientColorTop(dt),
		}));
		const tooltipThresholdValues = thresholdValues.filter((v) => v.value > 0).sort((a, b) => b.value - a.value);
		const tooltipBiasValues = biasValues
			.filter((v) => Number.isFinite(v.value) && v.value !== 0)
			.sort((a, b) => b.value - a.value);
		const tooltipFactorsValues = factorValues
			.filter(
				(v) =>
					(Number.isFinite(v.value) && v.value !== 1 && v.value !== 0) ||
					DefenseManager.getDamageProtectorDecorator(this.props.protection, v.damageType).max > 0,
			)
			.sort((a, b) => b.value - a.value);
		return (
			<div className={classnames(UIAbsorptionChart.getClass(), this.props.className)}>
				<UISVGPolarChart
					className={styles.chart}
					values={thresholdValues}
					ringValue={AbsorptionChartScale.threshold}
					maxScale={thresholdScale * AbsorptionChartScale.threshold}
					iconId={SVGProtectionSymbolIDs[kind].threshold}
					iconOutline={iconOutlineColor}
					segmentOutline={iconOutlineColor}
					tooltip={
						<span>
							{SVGProtectionIcon.getReductionTypeTooltip('threshold', kind)}. Each degree is{' '}
							<em>{AbsorptionChartScale.threshold}</em> Threshold
							{!!tooltipThresholdValues.length && <hr />}
							{tooltipThresholdValues.map((v) => (
								<section key={v.damageType}>
									<em>{v.value}</em> <UIDamageTypeDescriptor damageType={v.damageType} full={false} />{' '}
									Damage Threshold
								</section>
							))}
						</span>
					}
				/>
				<UISVGPolarChart
					className={styles.chart}
					values={biasValues}
					maxScale={biasScale * AbsorptionChartScale.bias}
					ringValue={AbsorptionChartScale.bias}
					iconId={SVGProtectionSymbolIDs[kind].bias}
					iconOutline={iconOutlineColor}
					segmentOutline={iconOutlineColor}
					tooltip={
						<span>
							{SVGProtectionIcon.getReductionTypeTooltip('bias', kind)}. Each degree is{' '}
							<em>{AbsorptionChartScale.bias}</em> Reduction
							{!!tooltipBiasValues.length && <hr />}
							{tooltipBiasValues.map((v) => (
								<section key={v.damageType}>
									<em>{describeModifier({ bias: -v.value })}</em>{' '}
									<UIDamageTypeDescriptor damageType={v.damageType} full={false} />{' '}
									{this.props.protection[v.damageType]?.min < 0 ? (
										<>
											Damage, up to <em>{-this.props.protection[v.damageType].min}</em>{' '}
											{kind === 'armor' ? 'Hit Points' : 'Capacity'} Restored
										</>
									) : DefenseManager.getDamageProtectorDecorator(this.props.protection, v.damageType)
											.max > 0 ? (
										<>
											Damage, Maximum Damage:{' '}
											<em>
												{
													DefenseManager.getDamageProtectorDecorator(
														this.props.protection,
														v.damageType,
													).max
												}
											</em>
										</>
									) : (
										<>Damage</>
									)}
								</section>
							))}
						</span>
					}
				/>
				<UISVGPolarChart
					className={styles.chart}
					values={factorValues.map((v) => ({ ...v }))}
					maxScale={factorScale * AbsorptionChartScale.factor}
					ringValue={AbsorptionChartScale.factor}
					iconId={SVGProtectionSymbolIDs[kind].factor}
					iconOutline={iconOutlineColor}
					segmentOutline={iconOutlineColor}
					tooltip={
						<span>
							{SVGProtectionIcon.getReductionTypeTooltip('factor', kind)}. Each degree is{' '}
							<em>{biasToPercent(AbsorptionChartScale.factor)}</em> Reduction
							{!!tooltipFactorsValues.length && <hr />}
							{tooltipFactorsValues.map((v) => (
								<p key={v.damageType}>
									{v.value ? (
										<React.Fragment>
											<em>
												{(v.value < 0 ? '+' : '') +
													BasicMaths.roundToPrecision(
														v.value < 1 ? -v.value * 100 : (v.value - 1) * 100,
														1,
													)}
												%
											</em>{' '}
											<UIDamageTypeDescriptor damageType={v.damageType} full={false} />{' '}
											{v.value > 1 ? (
												<>
													Damage restores up to{' '}
													<em>
														{this.props.protection[v.damageType]?.min !== 0
															? -this.props.protection[v.damageType].min
															: UI_SYMBOLS.Infinity}
													</em>{' '}
													{kind === 'armor' ? 'Hit Points' : 'Capacity'}
												</>
											) : DefenseManager.getDamageProtectorDecorator(
													this.props.protection,
													v.damageType,
											  ).max > 0 ? (
												<>
													Damage, Maximum Damage:{' '}
													<em>
														{
															DefenseManager.getDamageProtectorDecorator(
																this.props.protection,
																v.damageType,
															).max
														}
													</em>
												</>
											) : (
												<>Damage</>
											)}
										</React.Fragment>
									) : (
										<React.Fragment>
											Maximum <UIDamageTypeDescriptor damageType={v.damageType} full={false} />{' '}
											Damage:{' '}
											<em>
												{
													DefenseManager.getDamageProtectorDecorator(
														this.props.protection,
														v.damageType,
													).max
												}
											</em>
										</React.Fragment>
									)}
								</p>
							))}
						</span>
					}
				/>
			</div>
		);
	}
}

import * as React from 'react';
import { isEmptyModifierProperty, isEmptyProtection } from '../../../../../../definitions/core/helpers';
import Damage from '../../../../../../definitions/tactical/damage/damage';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import DefenseManager from '../../../../../../definitions/tactical/damage/DefenseManager';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGProtectionIcon from '../../../SVG/ProtectionIcon';
import UITooltip from '../../../Templates/Tooltip';
import UIDamageTypeDescriptor from '../../Damage/DamageTypeDescriptor';
import UIAbsorptionChart from '../AbsorptionChart';
import Style from './index.scss';

export type UIDamageProtectionDescriptorProps = IUICommonProps & {
	protection: UnitDefense.TArmor;
	caption?: boolean | string | JSX.Element;
	short?: boolean;
	kind?: UnitDefense.TProtectionKind;
	scale?: number; // One scale equals 10 Threshold, 10% Reduction and 10 Bias
};

const getGridRow = (
	rowCount: number,
	protection: UnitDefense.IDamageProtectionDecorator,
	t: Damage.damageType,
	isDefault = false,
) => {
	if (isEmptyProtection(protection)) return null;
	const nClass = classnames(Style['grid-value'], UIDamageTypeDescriptor.getClass(t));
	return (
		<React.Fragment key={'row' + t}>
			<div
				className={Style['grid-damage-type']}
				style={{
					gridColumn: '1/span 1',
					gridRow: `${rowCount + 1}/span 1`,
				}}>
				<UIDamageTypeDescriptor
					damageType={isDefault ? null : t}
					full={true}
					tooltip={true}
					className={Style.type}
				/>
			</div>
			<div
				className={classnames(nClass, protection.bias > 0 ? Style.negative : '')}
				style={{
					gridColumn: '3/span 1',
					gridRow: `${rowCount + 1}/span 1`,
				}}>
				{-protection.bias || ''}
			</div>
			<div
				className={classnames(nClass, protection.factor > 1 ? Style.negative : '')}
				style={{
					gridColumn: '4/span 1',
					gridRow: `${rowCount + 1}/span 1}`,
				}}>
				{isEmptyModifierProperty(protection.factor) || protection.factor === 1
					? ''
					: (protection.factor > 1 ? '-' : '') + Math.round(Math.abs(1 - protection.factor) * 100) + '%'}
			</div>
			<div
				className={nClass}
				style={{
					gridColumn: '2/span 1',
					gridRow: `${rowCount + 1}/span 1`,
				}}>
				{protection.threshold ? `${protection.threshold}` : ''}
			</div>
			<div
				className={classnames(nClass)}
				style={{
					gridColumn: '5/span 1',
					gridRow: `${rowCount + 1}/span 1`,
				}}>
				{[
					(!isEmptyModifierProperty(protection.min) &&
						protection.min > -Infinity &&
						protection.min !== 0 &&
						`${protection.min}↑`) ||
						null,
					(!isEmptyModifierProperty(protection.max) && protection.max < Infinity && `${protection.max}↓`) ||
						null,
				]
					.filter((v) => !!v)
					.join(',')}
			</div>
		</React.Fragment>
	);
};

export default class UIDamageProtectionDescriptor extends React.PureComponent<UIDamageProtectionDescriptorProps, {}> {
	public static getClass(): string {
		return Style.damageProtectionDescriptor;
	}

	public static getCaption(kind: UnitDefense.TProtectionKind): string {
		switch (kind) {
			case 'armor':
				return 'Armor Protection';
			case 'shield':
				return 'Shields Protection';
			default:
				return 'Protection';
		}
	}

	public static getCaptionClass(kind: UnitDefense.TProtectionKind): string {
		return classnames(
			Style.damageProtectionCaption,
			kind === 'shield' ? Style.shield : kind === 'armor' ? Style.armor : null,
		);
	}

	public static getEntityCaption(
		type: UnitDefense.TDamageProtectionType = 'threshold',
		kind: UnitDefense.TProtectionKind = 'armor',
	) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{SVGProtectionIcon.getIcon(type, kind)}
				<span className={UIDamageProtectionDescriptor.getCaptionClass(kind)}>
					{UIDamageProtectionDescriptor.getCaption(kind)}
				</span>
			</em>
		);
	}

	public render() {
		let rowCount = 0;
		// check if the protection only has default modifier
		let isDefault = true;
		Damage.TDamageTypesArray.forEach((d: Damage.damageType) => {
			if (!isEmptyProtection(this.props.protection[d])) {
				isDefault = false;
			}
		});
		const defaults = this.props.protection.default || {};
		return (
			<div className={[UIDamageProtectionDescriptor.getClass(), this.props.className || ''].join(' ')}>
				{this.props.short ? (
					<div className={Style.damageProtectionDescriptor}>
						<UIAbsorptionChart
							protection={this.props.protection}
							kind={this.props.kind || 'armor'}
							biasScale={this.props.scale}
							thresholdScale={this.props.scale}
							factorScale={this.props.scale}
						/>
					</div>
				) : (
					<div className={Style['grid-container']}>
						<div
							className={classnames(
								Style['grid-header-empty'],
								UIDamageProtectionDescriptor.getCaptionClass(this.props.kind),
							)}>
							{this.props.caption ?? UIDamageProtectionDescriptor.getCaption(this.props.kind)}
						</div>
						<div className={Style['grid-header-bias'] + ' ' + UIContainer.hasTooltipClass}>
							<UITooltip>{SVGProtectionIcon.getReductionTypeTooltip('bias', this.props.kind)}</UITooltip>
							<SVGProtectionIcon type={'bias'} kind={this.props.kind} className={Style.icon_header} />
						</div>
						<div className={Style['grid-header-factor'] + ' ' + UIContainer.hasTooltipClass}>
							<UITooltip>
								{SVGProtectionIcon.getReductionTypeTooltip('factor', this.props.kind)}
							</UITooltip>
							<SVGProtectionIcon type={'factor'} kind={this.props.kind} className={Style.icon_header} />
						</div>
						<div className={Style['grid-header-threshold'] + ' ' + UIContainer.hasTooltipClass}>
							<UITooltip>
								{SVGProtectionIcon.getReductionTypeTooltip('threshold', this.props.kind)}
							</UITooltip>
							<SVGProtectionIcon
								type={'threshold'}
								kind={this.props.kind}
								className={Style.icon_header}
							/>
						</div>
						<div className={Style['grid-header-rest'] + ' ' + UIContainer.hasTooltipClass}>
							<UITooltip>
								{SVGProtectionIcon.getReductionTypeTooltip('minGuard', this.props.kind)}
							</UITooltip>
							<SVGProtectionIcon type={'minGuard'} kind={this.props.kind} className={Style.icon_header} />
						</div>
						{isDefault
							? getGridRow(1, defaults, null, true)
							: Damage.TDamageTypesArray.map((t: Damage.damageType) => {
									const d = DefenseManager.getDamageProtectorDecorator(this.props.protection, t);
									if (isEmptyProtection(d)) return null;
									// console.log(d);
									rowCount++;
									return getGridRow(rowCount, d, t, false);
							  })}
					</div>
				)}
			</div>
		);
	}
}

import * as React from 'react';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import UnitAttack from '../../../../../../definitions/tactical/damage/attack';
import { DODGE_IMMUNITY_THRESHOLD } from '../../../../../../definitions/tactical/damage/constants';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIAttackDeliveryDescriptor from '../../Attack/DeliveryDescriptor';
import UIHitChanceDescriptor from '../../Attack/HitChanceDescriptor';
import UIDodgeCaption from '../DodgeCaption';
import Style from './index.scss';

export type UIDodgeModifierDescriptorProps = IUICommonProps & {
	dodgeModifier: UnitDefense.TDodgeModifier;
};

export default class UIDodgeModifierDescriptor extends React.PureComponent<UIDodgeModifierDescriptorProps, {}> {
	public render() {
		const self = this.constructor as typeof UIDodgeModifierDescriptor;
		const extraKeys = Object.keys(this.props.dodgeModifier)
			.filter((v) => v !== 'default')
			.filter((k) => !!this.props.dodgeModifier[k]);
		const getModifierClass = (m: IModifier) =>
			UIHitChanceDescriptor.getHitChanceClass(BasicMaths.applyModifier(1, m));
		return (
			<div className={this.props.className || ''}>
				<dl
					className={classnames(
						Style.dodgeModifierDescriptor,
						extraKeys.length ? Style.descriptorGroup : null,
					)}
				>
					<dt className={classnames(Style.caption, !!this.props.tooltip && UIContainer.hasTooltipClass)}>
						<UIDodgeCaption className={UIContainer.descriptorClass}>
							{UIDodgeCaption.getCaption()}
						</UIDodgeCaption>
						{!!this.props.tooltip && (
							<UITooltip className={Style.tooltip}>{UIDodgeCaption.getTooltip()}</UITooltip>
						)}
					</dt>
					<dd className={getModifierClass(this.props.dodgeModifier.default || {})}>
						{BasicMaths.describePercentileModifier(this.props.dodgeModifier.default, true)}
					</dd>
					{extraKeys.length
						? extraKeys.map((dt) => (
								<React.Fragment key={dt}>
									<dt key={dt + '-dt'} className={classnames(Style.subtype)}>
										<UIAttackDeliveryDescriptor
											tooltip={true}
											delivery={dt as UnitAttack.deliveryType}
										/>
									</dt>
									<dd
										className={getModifierClass(this.props.dodgeModifier[dt] || {})}
										key={dt + '-dd'}
									>
										{this.props.dodgeModifier[dt]?.bias > DODGE_IMMUNITY_THRESHOLD
											? 'Immune'
											: BasicMaths.describePercentileModifier(this.props.dodgeModifier[dt], true)}
									</dd>
								</React.Fragment>
						  ))
						: null}
				</dl>
			</div>
		);
	}
}

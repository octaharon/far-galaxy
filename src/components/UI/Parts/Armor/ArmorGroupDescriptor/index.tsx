import * as React from 'react';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import UIContainer, { IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIArmorSlotDescriptor from '../ArmorSlotDescriptor';
import styles from './index.scss';

export const armorGroupDescriptorClass = styles.armorGroup;

export type UIArmorGroupDescriptorProps = IUICommonProps & {
	group: UnitDefense.TArmorSlotType;
	highlight?: boolean;
	tooltip?: boolean;
};

export default class UIArmorGroupDescriptor extends React.PureComponent<UIArmorGroupDescriptorProps, {}> {
	public static getClass(): string {
		return armorGroupDescriptorClass;
	}

	public static getCaptionClass(): string {
		return styles.armorGroupCaption;
	}

	public render() {
		return (
			<span
				className={[
					UIArmorGroupDescriptor.getClass(),
					this.props.className || '',
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				].join(' ')}
			>
				<UIArmorSlotDescriptor
					slotType={this.props.group}
					className={styles.slotIcon}
					tooltip={false}
					highlight={this.props.highlight}
				/>
				<span className={UIArmorGroupDescriptor.getCaptionClass()}>
					{UIArmorSlotDescriptor.getArmorSlotCaption(this.props.group)}
				</span>
				{this.props.tooltip && (
					<UITooltip>{UIArmorSlotDescriptor.getArmorSlotDescription(this.props.group)}</UITooltip>
				)}
			</span>
		);
	}
}

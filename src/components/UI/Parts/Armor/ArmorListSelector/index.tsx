import * as React from 'react';
import {
	doesCollectibleSetIncludeItem,
	findCollectibleItemInSet,
} from '../../../../../../definitions/core/Collectible';
import Players from '../../../../../../definitions/player/types';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { IUICommonProps } from '../../../index';
import { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UIListSelector from '../../../Templates/ListSelector';
import UIArmorGroupDescriptor from '../ArmorGroupDescriptor';
import styles from './index.scss';

const weaponListSelectorClass = styles.armorListSelector;

type UIArmorListSelectorProps = IUICommonProps & {
	onArmorSelected: (armorId: number) => any;
	armorSlot?: UnitDefense.TArmorSlotType;
	selectedArmor?: number;
	player?: Players.IPlayer;
};

type IArmorHash = EnumProxy<UnitDefense.TArmorSlotType, number[]>;

interface UIArmorListSelectorState {
	armorAvailable: IArmorHash;
}

export default class UIArmorListSelector extends React.PureComponent<
	UIArmorListSelectorProps,
	UIArmorListSelectorState
> {
	public static getClass(): string {
		return weaponListSelectorClass;
	}

	protected get armorOptions() {
		return this.state.armorAvailable[this.armorSlot];
	}

	protected get armorSlot(): UnitDefense.TArmorSlotType {
		return this.props.armorSlot
			? this.state.armorAvailable[this.props.armorSlot]
				? this.props.armorSlot
				: Object.keys(this.state.armorAvailable)[0]
			: this.props.selectedArmor
			? Object.keys(this.state.armorAvailable).find((k) =>
					doesCollectibleSetIncludeItem(this.props.selectedArmor, this.state.armorAvailable[k]),
			  )
			: Object.keys(this.state.armorAvailable)[0] || null;
	}

	protected getSelectedArmor(group = this.armorSlot): number {
		if (!group) return null;
		return doesCollectibleSetIncludeItem(this.props.selectedArmor, this.state.armorAvailable[group])
			? this.props.selectedArmor
			: this.state.armorAvailable[group][0];
	}

	public static getDerivedStateFromProps(props: UIArmorListSelectorProps, state: UIArmorListSelectorState) {
		return {
			...(state || {}),
			armorAvailable: UIArmorListSelector.buildArmorHash(props),
		};
	}

	protected static buildArmorHash(props: UIArmorListSelectorProps) {
		const provider = DIContainer.getProvider(DIInjectableCollectibles.armor);
		const r = {} as IArmorHash;
		Object.keys(provider.getAll()).forEach((armorId) => {
			if (!props.player || props.player.isArmorAvailable(armorId)) {
				const key = provider.get(armorId).type;
				r[key] = (r[key] || []).concat(armorId);
			}
		});
		return r;
	}

	public constructor(props: UIArmorListSelectorProps) {
		super(props);
		const armorAvailable = UIArmorListSelector.buildArmorHash(props);
		this.state = {
			armorAvailable,
		};
	}

	public render() {
		const armorGroupCaption = (slot: UnitDefense.TArmorSlotType) => (
			<span className={styles.armorSlot}>
				<UIArmorGroupDescriptor group={slot} tooltip={true} />
			</span>
		);
		const onGroupSelect = (slot: UnitDefense.TArmorSlotType) =>
			this.props.onArmorSelected(this.getSelectedArmor(slot));
		const armorCaption = (armorId: number) => (
			<span className={styles.armorCaption}>
				{DIContainer.getProvider(DIInjectableCollectibles.armor).get(armorId).caption}
			</span>
		);
		const onArmorSelect = (armorId: number) => this.props.onArmorSelected(armorId);
		return (
			<div className={[UIArmorListSelector.getClass(), this.props.className].join(' ')}>
				<UIListSelector
					options={this.props.armorSlot ? [this.props.armorSlot] : Object.keys(this.state.armorAvailable)}
					selectedItem={this.armorSlot}
					buttonColor={TSVGSmallTexturedIconStyles.light}
					optionRender={armorGroupCaption}
					onSelect={onGroupSelect}
				/>
				<UIListSelector
					options={this.armorOptions}
					selectedItem={this.getSelectedArmor()}
					selectedIndex={findCollectibleItemInSet(this.getSelectedArmor(), this.armorOptions)}
					buttonColor={TSVGSmallTexturedIconStyles.light}
					optionRender={armorCaption}
					onSelect={onArmorSelect}
				/>
			</div>
		);
	}
}

import * as React from 'react';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import { getTemplateColor, IUICommonProps } from '../../../index';
import SVGHexIcon from '../../../SVG/HexIcon';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGProtectionIcon from '../../../SVG/ProtectionIcon';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import Style from './index.scss';

export type UIArmorSlotDescriptorProps = IUICommonProps & {
	slotType: UnitDefense.TArmorSlotType;
	highlight?: boolean;
};

export const SVGArmorEntityIconID = 'armor-entity-icon';

SVGIconContainer.registerSymbol(
	SVGArmorEntityIconID,
	<symbol viewBox="0 0 31.694 31.694">
		<path
			d="M27.125,3.842c-9.266,0-10.835-3.518-10.849-3.551C16.208,0.119,16.042,0.004,15.85,0
		c0,0-0.002,0-0.006,0c-0.185,0-0.354,0.115-0.426,0.283C15.405,0.32,13.802,3.842,4.569,3.842c-0.257,0-0.462,0.209-0.462,0.461
		v15.764c0,6.453,11.085,11.383,11.553,11.59c0.062,0.027,0.123,0.037,0.188,0.037c0.061,0,0.126-0.01,0.184-0.037
		c0.473-0.207,11.556-5.137,11.556-11.59V4.303C27.587,4.051,27.382,3.842,27.125,3.842z M25.04,19.15
		c0,5.049-8.678,8.912-9.048,9.072c-0.045,0.023-0.097,0.031-0.144,0.031c-0.051,0-0.098-0.008-0.148-0.031
		c-0.365-0.16-9.043-4.023-9.043-9.072V6.811c0-0.199,0.161-0.363,0.361-0.363c7.23,0,8.481-2.756,8.495-2.783
		c0.057-0.133,0.188-0.223,0.334-0.223c0.002,0,0.004,0,0.004,0C16,3.444,16.13,3.534,16.184,3.671
		c0.011,0.025,1.239,2.777,8.494,2.777c0.201,0,0.361,0.164,0.361,0.363C25.039,6.811,25.039,19.15,25.04,19.15z M23.309,19.016
		c0,4.102-7.045,7.234-7.346,7.363c-0.036,0.021-0.079,0.027-0.117,0.027c-0.041,0-0.117-20.141,0-20.141h0.002
		c0.121,0.002,0.229,0.074,0.271,0.186c0.008,0.02,1.006,2.254,6.895,2.254c0.164,0,0.295,0.133,0.295,0.293V19.016z"
		/>
	</symbol>,
);

export default class UIArmorSlotDescriptor extends React.PureComponent<UIArmorSlotDescriptorProps, {}> {
	public static getSVGText(slot: UnitDefense.TArmorSlotType, highlight?: boolean) {
		if (highlight || slot === UnitDefense.TArmorSlotType.none || !slot)
			return (
				<SVGSmallTexturedIcon
					style={highlight ? TSVGSmallTexturedIconStyles.gray : TSVGSmallTexturedIconStyles.gray}
					asSvg={true}>
					<g
						style={{
							transformOrigin: '50% 50%',
							transform: 'scale(0.75) translateY(0%)',
						}}>
						<use {...SVGIconContainer.getXLinkProps(SVGArmorEntityIconID)} />
					</g>
				</SVGSmallTexturedIcon>
			);
		return (
			<text
				x="50%"
				y="48%"
				textAnchor="middle"
				dominantBaseline="middle"
				className={Style.text}
				stroke={getTemplateColor('dark_orange')}
				strokeWidth="0.5"
				fill={getTemplateColor('text_highlight')}>
				{UIArmorSlotDescriptor.getArmorSlotAbbr(slot)}
			</text>
		);
	}

	public static getArmorSlotDescription(slot: UnitDefense.TArmorSlotType) {
		const getIcon = (s: UnitDefense.TArmorSlotType) => {
			return (
				<SVGHexIcon
					className={[UITooltip.getIconClass()].join(' ')}
					colorTop={getTemplateColor('dark_orange')}
					colorBottom={getTemplateColor('text_dark')}>
					{UIArmorSlotDescriptor.getSVGText(slot)}
				</SVGHexIcon>
			);
		};
		switch (slot) {
			case UnitDefense.TArmorSlotType.small:
				return (
					<span>
						{getIcon(slot)}
						<b>{UIArmorSlotDescriptor.getArmorSlotCaption(slot)}</b>{' '}
						<Tokenizer
							description={
								' includes personal defense and armor frames for lesser vehicles, including most Airborne Chassis, It can be quite efficient versus light Weapons, but for heavy Damage this kind of protection is next to non-existent'
							}
						/>
					</span>
				);
			case UnitDefense.TArmorSlotType.medium:
				return (
					<span>
						{getIcon(slot)}
						<b>{UIArmorSlotDescriptor.getArmorSlotCaption(slot)}</b>
						<Tokenizer
							description={
								' is the most common form factor of armor frames, suitable for the majority of Chassis and offering somewhat protection from most Weapons'
							}
						/>
					</span>
				);
			case UnitDefense.TArmorSlotType.large:
				return (
					<span>
						{getIcon(slot)}
						<b>{UIArmorSlotDescriptor.getArmorSlotCaption(slot)}</b>{' '}
						<Tokenizer
							description={
								' fits only bigger vehicles and organisms and exhibits great resistance against the most dangerous Weapons, while for smaller ones, like Guns, some Cannons and Lasers, it might be totally impenetrable'
							}
						/>
					</span>
				);
			default:
				return (
					<span>
						{getIcon(slot)}
						<b>{UIArmorSlotDescriptor.getArmorSlotCaption(slot)}</b>{' '}
						<Tokenizer
							description={
								"mitigates any Attack Damage dealt to Hit Points, when installed on Unit. Armor technologies can affect Unit's Dodge capabilities or provide other valuable Perks"
							}
						/>
					</span>
				);
		}
	}

	public static getArmorSlotCaption(slot: UnitDefense.TArmorSlotType) {
		switch (slot) {
			case UnitDefense.TArmorSlotType.large:
				return 'Heavy Armor';
			case UnitDefense.TArmorSlotType.medium:
				return 'Medium Armor';
			case UnitDefense.TArmorSlotType.small:
				return 'Small Armor';
			default:
				return 'Armor';
		}
	}

	public static getArmorSlotAbbr(slot: UnitDefense.TArmorSlotType) {
		switch (slot) {
			case UnitDefense.TArmorSlotType.small:
				return 'S';
			case UnitDefense.TArmorSlotType.medium:
				return 'M';
			case UnitDefense.TArmorSlotType.large:
				return 'L';
			default:
				return 'A';
		}
	}

	public render() {
		const self = this.constructor as typeof UIArmorSlotDescriptor;
		return (
			<SVGHexIcon
				className={[Style.armorSlotDescriptor, this.props.className || ''].join(' ')}
				onClick={this.props.onClick}
				highlight={!!this.props.highlight}
				colorTop={getTemplateColor(this.props.highlight ? 'dark_orange' : 'text_dark')}
				colorBottom={getTemplateColor(this.props.highlight ? 'dark_purple' : 'dark_orange')}
				tooltip={this.props.tooltip ? self.getArmorSlotDescription(this.props.slotType) : null}>
				{UIArmorSlotDescriptor.getSVGText(this.props.slotType, this.props.highlight)}
			</SVGHexIcon>
		);
	}
}

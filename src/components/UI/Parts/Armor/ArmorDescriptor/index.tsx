import * as React from 'react';
import _ from 'underscore';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import { IArmor } from '../../../../../../definitions/technologies/types/armor';
import { UIPrototypeBackgroundGradientID, UITalentBackdropPatternID } from '../../../../Symbols/backdrops';
import UIRawMaterialsDescriptor from '../../../Economy/RawMaterialsDescriptor';
import UIContainer, { IUICommonProps } from '../../../index';
import UIBackdrop from '../../../SVG/Backdrop';
import UIHitPointsDescriptor from '../../Chassis/HitPointsDescriptor';
import UIDamageProtectionDescriptor from '../../Defense/DamageProtectionDescriptor';
import UIDefenseFlagsDescriptor from '../../Defense/DefenseFlagsDescriptor';
import UIDodgeModifierDescriptor from '../../Defense/DodgeModifierDescriptor';
import UIArmorGroupDescriptor from '../ArmorGroupDescriptor';
import UIArmorSlotDescriptor from '../ArmorSlotDescriptor';
import styles from './index.scss';

const armorDescriptorClass = styles.armorDescriptor;

type UIArmorDescriptorProps = IUICommonProps & {
	armorId: number;
	modifiers?: UnitDefense.TArmorModifier[];
	details?: boolean;
};

interface UIArmorDescriptorState {
	armor: IArmor;
}

export const ArmorMetaDescriptor: React.FC<{ armor: IArmor; short?: boolean }> = ({ armor, short }) => {
	const hasHPMod = !_.isEmpty(armor.static.healthModifier);
	const hasFlags =
		!_.isEmpty(armor.static.flags) &&
		Object.keys(armor.static.flags).filter((v) => !!armor.static.flags[v]).length > 0;
	switch (true) {
		case !hasHPMod && !hasFlags:
			return null;
		case !short:
			return (
				<>
					<UIDefenseFlagsDescriptor
						flags={armor.static.flags || {}}
						short={false}
						tooltip={true}
						className={styles.metaCaption}
					/>
					{hasHPMod && <UIHitPointsDescriptor modifier={armor.static.healthModifier} tooltip={true} />}
				</>
			);
		case !hasHPMod && hasFlags:
			return (
				<UIDefenseFlagsDescriptor
					flags={armor.static.flags || {}}
					short={false}
					tooltip={true}
					className={styles.metaCaption}
				/>
			);
		case hasHPMod && !hasFlags:
			return <UIHitPointsDescriptor modifier={armor.static.healthModifier} tooltip={true} />;
		default:
			return (
				<dl className={styles.meta}>
					<dt className={UIContainer.descriptorClass}>
						<UIDefenseFlagsDescriptor
							flags={armor.static.flags || {}}
							short={true}
							forceCaption={true}
							tooltip={true}
							className={styles.metaCaption}
						/>
					</dt>
					<dd className={''}>
						<UIHitPointsDescriptor short={true} tooltip={true} modifier={armor.static.healthModifier} />
					</dd>
				</dl>
			);
	}
};

export default class UIArmorDescriptor extends React.PureComponent<UIArmorDescriptorProps, UIArmorDescriptorState> {
	public static getClass(): string {
		return armorDescriptorClass;
	}

	public static getDerivedStateFromProps(
		newProps: UIArmorDescriptorProps,
		oldState: UIArmorDescriptorState = {
			armor: null,
		},
	): UIArmorDescriptorState {
		const newState = {
			...oldState,
		};
		if (newProps.armorId !== oldState?.armor?.static?.id) {
			const w = DIContainer.getProvider(DIInjectableCollectibles.armor).instantiate(null, newProps.armorId);
			newState.armor = w.applyModifier(...(newProps.modifiers || []));
		}
		return newState;
	}

	public static getCaption() {
		return UIArmorSlotDescriptor.getArmorSlotCaption(UnitDefense.TArmorSlotType.none);
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{
					<UIArmorSlotDescriptor
						tooltip={false}
						slotType={UnitDefense.TArmorSlotType.none}
						highlight={true}
						className={styles.armorSlotIcon}
					/>
				}
				<span className={UIArmorGroupDescriptor.getCaptionClass()}>{UIArmorDescriptor.getCaption()}</span>
			</em>
		);
	}

	public static getDescriptor() {
		return (
			<>
				{<UIArmorSlotDescriptor slotType={UnitDefense.TArmorSlotType.none} className={UIContainer.iconClass} />}
				<span className={UIArmorGroupDescriptor.getCaptionClass()}>{UIArmorDescriptor.getCaption()}</span>
			</>
		);
	}

	public constructor(props: UIArmorDescriptorProps) {
		super(props);
		this.state = UIArmorDescriptor.getDerivedStateFromProps(props);
	}

	public render() {
		const protectionScale = {
			[UnitDefense.TArmorSlotType.small]: 1,
			[UnitDefense.TArmorSlotType.medium]: 2,
			[UnitDefense.TArmorSlotType.large]: 3,
			[UnitDefense.TArmorSlotType.none]: 10,
		};
		return (
			<div className={[UIArmorDescriptor.getClass(), this.props.className || ''].join(' ')}>
				<UIBackdrop
					className={styles.frame}
					gradientId={UIPrototypeBackgroundGradientID}
					patternId={UITalentBackdropPatternID}
				/>
				<div className={[this.props.details ? '' : styles.short, styles.content].join(' ')}>
					<dl className={styles.meta}>
						{this.props.details && (
							<>
								<dt className={styles.group}>
									<UIArmorGroupDescriptor
										group={this.state.armor.static.type}
										tooltip={true}
										highlight={true}
									/>
								</dt>
								<dd className={styles.armorSlots}>
									<UIArmorSlotDescriptor
										slotType={this.state.armor.static.type}
										className={styles.armorSlotItem}
									/>
								</dd>
							</>
						)}
						<dt className={styles.title}>
							{this.state.armor.caption}{' '}
							{!this.props.details && (
								<UIArmorSlotDescriptor
									tooltip={true}
									slotType={this.state.armor.static.type}
									className={styles.armorSlotItem}
								/>
							)}
						</dt>
						<dd className={styles.cost}>
							<UIRawMaterialsDescriptor
								short={true}
								value={this.state.armor.baseCost}
								className={styles.costDescriptor}
							/>
						</dd>
					</dl>
					{this.props.details && <div className={styles.description}>{this.state.armor.description}</div>}
					<ArmorMetaDescriptor armor={this.state.armor} short={!this.props.details} />
					{!_.isEmpty(this.state.armor.static.dodgeModifier) && (
						<UIDodgeModifierDescriptor
							dodgeModifier={this.state.armor.static.dodgeModifier}
							tooltip={true}
						/>
					)}
					<div className={styles.protection}>
						<UIDamageProtectionDescriptor
							protection={this.state.armor.armor}
							short={!this.props.details}
							scale={protectionScale[this.state.armor.static.type]}
						/>
					</div>
				</div>
			</div>
		);
	}
}

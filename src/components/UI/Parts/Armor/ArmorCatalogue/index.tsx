import * as React from 'react';
import { doesCollectibleSetIncludeItem } from '../../../../../../definitions/core/Collectible';
import { extractPlayerUnitModifier } from '../../../../../../definitions/player/helpers';
import Players from '../../../../../../definitions/player/types';
import UnitChassis from '../../../../../../definitions/technologies/types/chassis';
import UnitDefense from '../../../../../../definitions/tactical/damage/defense';
import DefenseManager from '../../../../../../definitions/tactical/damage/DefenseManager';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { getFactionUnitModifiers } from '../../../../../../definitions/world/factionIndex';
import { classnames, IUICommonProps } from '../../../index';
import UIArmorDescriptor from '../ArmorDescriptor';
import UIArmorListSelector from '../ArmorListSelector';
import styles from './index.scss';
import IPlayer = Players.IPlayer;

const armorCatalogueClass = styles.armorCatalogue;

type UIArmorCatalogueProps = IUICommonProps & {
	player?: IPlayer;
	defaultSelectedArmor?: number;
	short?: boolean;
	modifiers?: UnitDefense.TArmorModifier[];
	forChassis?: UnitChassis.chassisClass;
	forSlot?: UnitDefense.TArmorSlotType;
	onArmorSelected?: (armorId: number) => any;
};

export interface UIArmorCatalogueState {
	selectedArmor: number;
}

const stackArmorMods = (props: UIArmorCatalogueProps) =>
	DefenseManager.stackArmorModifiers(
		...(props.modifiers || []),
		...extractPlayerUnitModifier(getFactionUnitModifiers(props.player?.faction), 'armor', props.forChassis),
	);

const getArmorId = (props: UIArmorCatalogueProps, armorId?: number) => {
	const provider = DIContainer.getProvider(DIInjectableCollectibles.armor);
	let aId = armorId || props.defaultSelectedArmor;
	const possibleArmorOptions = props.player
		? Object.keys(provider.getAll()).filter((id) => props.player.isArmorAvailable(id))
		: Object.keys(provider.getAll());
	if (!doesCollectibleSetIncludeItem(aId, possibleArmorOptions)) aId = possibleArmorOptions[0];
	if (aId && props.forSlot) {
		if (!provider.doesArmorFitSlot(provider.get(aId), props.forSlot))
			aId = possibleArmorOptions.filter((id) => provider.doesArmorFitSlot(provider.get(id), props.forSlot))[0];
	}
	return aId;
};

export default class UIArmorCatalogue extends React.PureComponent<UIArmorCatalogueProps, UIArmorCatalogueState> {
	public static getClass(): string {
		return armorCatalogueClass;
	}

	public static getDerivedStateFromProps(props: UIArmorCatalogueProps, state?: UIArmorCatalogueState) {
		const armorId = getArmorId(props, state?.selectedArmor);
		return {
			selectedArmor: armorId,
		};
	}

	public get selectedSlot() {
		return DIContainer.getProvider(DIInjectableCollectibles.armor).get(this.state.selectedArmor)?.type || null;
	}

	public componentDidMount() {
		if (!this.props.defaultSelectedArmor && this.props.onArmorSelected)
			this.props.onArmorSelected(this.state.selectedArmor);
	}

	public constructor(props: UIArmorCatalogueProps) {
		super(props);
		this.onArmorSelected = this.onArmorSelected.bind(this);
		this.state = UIArmorCatalogue.getDerivedStateFromProps(props);
	}

	public render() {
		const mods = stackArmorMods(this.props);
		return (
			<div className={classnames(UIArmorCatalogue.getClass(), this.props.className)}>
				<UIArmorListSelector
					armorSlot={this.props.forSlot}
					selectedArmor={this.state.selectedArmor}
					onArmorSelected={this.onArmorSelected}
					player={this.props.player}
					className={styles.selector}
				/>
				<UIArmorDescriptor
					armorId={this.state.selectedArmor}
					className={styles.descriptor}
					modifiers={[mods]}
					details={!this.props.short}
				/>
			</div>
		);
	}

	protected onArmorSelected(armorId: number) {
		const selectedArmor = getArmorId(this.props, armorId);
		this.setState(
			{
				...this.state,
				selectedArmor,
			},
			() => {
				if (this.props.onArmorSelected) this.props.onArmorSelected(selectedArmor);
			},
		);
	}
}

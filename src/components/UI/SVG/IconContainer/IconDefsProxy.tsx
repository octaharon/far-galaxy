import React from 'react';
import _ from 'underscore';
import { IconContainerDispatcher } from '../../../../contexts/IconContainer';
import { useIconContainerDispatch } from '../../../../stores/iconContainer';
import SVGIconContainer from './index';
import { TSVGIconContextInstance, TSVGIconTypeKey } from './types';

const idEquality = <T extends { id: string }>(a: T, b: T) => a.id === b.id;

export const SVGIconDefsProxyOnce = React.memo(<T extends TSVGIconTypeKey>(prop: TSVGIconContextInstance<T>) => {
	const dispatch = useIconContainerDispatch();
	const setters = IconContainerDispatcher(SVGIconContainer.getStorage())(dispatch);
	const id = prop.id;
	React.useEffect(() => {
		console.debug(`portalling ${id}`, prop.icon);
		setters.registerIcons(prop);
	}, [id]);
	return null;
}, idEquality);

export const SVGIconDefsProxyContainer = React.memo(
	({ icons, children }: React.PropsWithChildren<{ icons: Array<TSVGIconContextInstance<TSVGIconTypeKey>> }>) => {
		const storage = SVGIconContainer.getStorage();
		const existingObjects = icons.map((icon) => document.getElementById(icon.id));
		const existingCollection = icons.map((icon) => storage[icon.id]);
		const [domReady, setDomReady] = React.useState(existingObjects.every(Boolean));
		const dispatch = useIconContainerDispatch();
		const setters = IconContainerDispatcher(storage)(dispatch);
		const missingIcons = icons.filter((icon, index) => !existingObjects[index] && !existingCollection[index]);
		const iconHash = missingIcons.map((v) => v.id).join('|');
		React.useEffect(() => {
			if (missingIcons.length) {
				setters.registerIcons(...missingIcons);
			} else if (!domReady) {
				setDomReady(true);
			}
		}, [icons, dispatch, iconHash]);
		if (domReady) return <>{children}</>;
		return null;
	},
	(p1, p2) => _.without(_.pluck(p2.icons, 'id'), ..._.pluck(p1.icons, 'id')).length === 0,
);

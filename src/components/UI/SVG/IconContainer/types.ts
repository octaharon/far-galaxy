import type React from 'react';

export const SVGIconContextKeys = {
	filters: 'filters',
	gradients: 'gradients',
	symbols: 'symbols',
	paths: 'paths',
	patterns: 'patterns',
} as const;
export type TSVGIconTypeKey = ValueOf<typeof SVGIconContextKeys>;
export const SVGIconTypeKeysArray = Object.values(SVGIconContextKeys) as TSVGIconTypeKey[];
export const SVGIconContainerElementID: Record<TSVGIconTypeKey, string> = {
	filters: 'svg-icon-defs-filters',
	paths: 'svg-icon-defs-paths',
	symbols: 'svg-icon-defs-symbols',
	patterns: 'svg-icon-defs-patterns',
	gradients: 'svg-icon-defs-gradients',
};

export const SVGIconContainerElementOrder: TSVGIconTypeKey[] = ['gradients', 'paths', 'patterns', 'filters', 'symbols'];
export type TFilter = React.ReactElement<SVGFilterElement>;
export type TGradient = React.ReactElement<SVGGradientElement>;
export type TSymbol = React.ReactElement<SVGSymbolElement>;
export type TPath = React.ReactElement<SVGGraphicsElement>;
export type TPattern = React.ReactElement<SVGPatternElement>;

type TSVGContextObjectTypes = {
	filters: TFilter;
	gradients: TGradient;
	symbols: TSymbol;
	paths: TPath;
	patterns: TPattern;
};

export type TSVGContextObjectByType<K extends TSVGIconTypeKey> = TSVGContextObjectTypes[K];
export type TSVGContextTypeByObject<K extends ValueOf<TSVGContextObjectTypes>> = K extends TSVGContextObjectByType<
	infer X
>
	? X
	: never;
export type TSVGIconFactory<K extends TSVGIconTypeKey> = () => TSVGContextObjectByType<K>;

export type TRegisteredIcon<K extends TSVGIconTypeKey> = {
	id: string;
	type: K;
};

export type TIconStorage = {
	[K in TSVGIconTypeKey]: Dictionary<TRegisteredIcon<K>>;
};
export type TIconGlobalStorage = Record<string, TSVGContextObjectByType<any>>;
export type TIconGlobalComponentStorage = Record<string, JSX.Element>;
export type TSVGIconPayload<K extends TSVGIconTypeKey> = {
	id: string;
	icon: string;
} & TSVGIconTypeDiscriminator<K>;
export type TSVGIconTypeDiscriminator<K extends TSVGIconTypeKey = TSVGIconTypeKey> = {
	type: K;
};
export type TSVGIconContextInstance<K extends TSVGIconTypeKey> = {
	id: string;
	icon: TSVGContextObjectByType<K>;
} & TSVGIconTypeDiscriminator<K>;

export type TSVGIconContextVersion = {
	version: number;
};
export type TSVGIconComputedContext = {
	[K in TSVGIconTypeKey]: Dictionary<TSVGContextObjectByType<K>>;
};
export type TSVGIconContext = TSVGIconContextVersion & TIconStorage & { ids: Record<string, TSVGIconTypeKey> };
export type TSVGIconContextSetters = {
	registerIcons: (...props: Array<TSVGIconContextInstance<TSVGIconTypeKey>>) => void;
};

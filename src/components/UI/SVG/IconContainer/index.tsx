import * as React from 'react';
import {
	IconContainerDispatcher,
	IconContainerProvider,
	TWithIconContainerProps,
} from '../../../../contexts/IconContainer';
import { SVGIconContainerStore, withIconContainer } from '../../../../stores/iconContainer';
import { classnames, IUICommonProps } from '../../index';
import { SVGIconDefsProxyOnce } from './IconDefsProxy';
import Style from './index.scss';
import {
	SVGIconContainerElementOrder,
	SVGIconTypeKeysArray,
	TFilter,
	TGradient,
	TIconGlobalComponentStorage,
	TIconGlobalStorage,
	TPath,
	TPattern,
	TSVGIconTypeKey,
	TSymbol,
} from './types';

export const UISVGIconViewbox = [1920, 1920];

export interface SVGIconContainerProps extends IUICommonProps {
	viewBox?: number;
}

type TContainer = typeof SVGIconContainer;

type TFilterWrapperProps = IUICommonProps & {
	svgProps?: React.SVGProps<SVGSVGElement>;
};

const Storage: TIconGlobalStorage = {};

const ComponentStorage: TIconGlobalComponentStorage = {};

export const SVGIconStandartViewbox = `0 0 ${UISVGIconViewbox[0]} ${UISVGIconViewbox[1]}`;

function assignElementId(id: string, el: JSX.Element) {
	return React.cloneElement(el, {
		key: id,
		id,
	});
}

export const SVGFilterWrapper: React.FC<TFilterWrapperProps> = (props) => (
	<svg
		className={classnames(props.className || Style.filterWrapper)}
		overflow="visible"
		xmlns="http://www.w3.org/2000/svg"
		viewBox={SVGIconStandartViewbox}
		{...(props.svgProps || {})}>
		{props.children}
	</svg>
);

class SVGIconContextDefs extends React.Component<TWithIconContainerProps> {
	shouldComponentUpdate(nextProps: Readonly<TWithIconContainerProps>): boolean {
		return nextProps.container?.version !== this.props.container?.version;
	}

	render() {
		const { container } = this.props;
		return (
			<>
				<defs id={`defs-container-${this.props.container.version}`}>
					{SVGIconContainerElementOrder.flatMap((type) => Object.values(container[type]).filter(Boolean))}
				</defs>
			</>
		);
	}
}

const SVGIconConnectedContextDefs = withIconContainer(Storage)(SVGIconContextDefs);

const reduxConnector = IconContainerDispatcher(Storage)(SVGIconContainerStore.dispatch);
const IconContainerRef = React.createRef<SVGSVGElement>();
const IconContainersRefs = SVGIconTypeKeysArray.reduce(
	(acc, type) => ({
		...acc,
		[type]: React.createRef<SVGDefsElement>(),
	}),
	{} as Record<TSVGIconTypeKey, React.RefObject<SVGDefsElement>>,
);

export class SVGIconContainer extends React.PureComponent<SVGIconContainerProps & TWithIconContainerProps> {
	public static getClass(): string {
		return Style.iconContainer;
	}

	public static getRef(type: TSVGIconTypeKey): React.RefObject<SVGDefsElement> {
		return IconContainersRefs[type];
	}

	public static getStorage() {
		return Storage;
	}

	public static getComponentStorage() {
		return ComponentStorage;
	}

	constructor(props: SVGIconContainerProps & TWithIconContainerProps) {
		super(props);
	}

	public static registerFilter(id: string, filter: TFilter, useOwnSize = false): TContainer {
		reduxConnector.registerIcons({
			icon: React.cloneElement(
				assignElementId(id, filter),
				useOwnSize
					? {}
					: {
							x: '-50%',
							y: '-50%',
							width: '200%',
							height: '200%',
					  },
			),
			id,
			type: 'filters',
		});
		return SVGIconContainer;
	}

	public static registerSymbol(id: string, symbol: TSymbol): TContainer {
		reduxConnector.registerIcons({
			icon: assignElementId(id, symbol),
			id,
			type: 'symbols',
		});
		return SVGIconContainer;
	}

	public static registerGradient(id: string, gradient: TGradient): TContainer {
		reduxConnector.registerIcons({
			icon: assignElementId(id, gradient),
			id,
			type: 'gradients',
		});
		return SVGIconContainer;
	}

	public static registerPattern(id: string, pattern: TPattern): TContainer {
		reduxConnector.registerIcons({
			icon: assignElementId(id, pattern),
			id,
			type: 'patterns',
		});
		return SVGIconContainer;
	}

	public static registerPath(id: string, path: TPath): TContainer {
		reduxConnector.registerIcons({
			icon: assignElementId(id, path),
			id,
			type: 'paths',
		});
		return SVGIconContainer;
	}

	public static setIsUsed(id: string) {
		// @TODO
		return SVGIconContainer;
	}

	public static getUrlRef(id: string): string {
		SVGIconContainer.setIsUsed(id);
		return `url(#${id})`;
	}

	public static useFilter(id: string) {
		SVGIconContainer.setIsUsed(id);
		return {
			filter: SVGIconContainer.getUrlRef(id),
		};
	}

	public static getXLinkProps(id: string): Partial<React.SVGProps<SVGUseElement>> {
		if (!id) return {};
		SVGIconContainer.setIsUsed(id);
		return {
			xlinkHref: '#' + id,
		};
	}

	public render() {
		return (
			<div className={classnames(Style.iconContainer, this.props.className)}>
				<IconContainerProvider>
					<svg
						className={SVGIconContainer.getClass()}
						style={{}}
						xmlns="http://www.w3.org/2000/svg"
						ref={IconContainerRef}
						viewBox={`0 0 ${this.props.viewBox || UISVGIconViewbox[0]} ${
							this.props.viewBox || UISVGIconViewbox[1]
						}`}>
						<SVGIconConnectedContextDefs />
					</svg>
					<SVGBasicFilters />
					<SVGBasicSymbols />
					{this.props.children}
				</IconContainerProvider>
			</div>
		);
	}
}

export const SVGBasicNoiseFilterID = '__filter-basic-noise';
export const SVGAnimatedNoiseFilterID = '__filter-animated-noise';
export const SVGBasicShadowFilterID = '__filter-basic-shadow';
export const SVGBasicGlowFilterID = '__filter-basic-glow';
export const SVGContrastGlowFilterID = '__filter-contrast-glow';
export const SVGLargeGlowFilterID = '__filter-large-glow';
export const SVGBasicBevelFilterID = '__filter-basic-bevel';
export const SVGFractalNoiseFilterID = '__filter-noise-fractal';
export const SVGBasicEmbossFilterID = '__filter-basic-emboss';
export const SVGBasicBlurFilterID = '__filter-basic-blur';
export const SVGDarkenFilterID = '__filter-basic-darken';
export const SVGMaskFilterID = '__filter-mask';
export const SVGMaskDilateFilterID = '__filter-mask-dilate';
export const SVGFilterDesaturateID = '__filter-desaturate';
export const SVGFilterSaturate20ID = '__filter-desaturate-20';
export const SVGFilterSaturate40ID = '__filter-desaturate-40';
export const SVGFilterSaturate60ID = '__filter-desaturate-60';
export const SVGFilterSaturate80ID = '__filter-desaturate-80';

export const SVGIconArrowLeftID = '__icon_arrow_left';
export const SVGIconArrowRightID = '__icon_arrow_right';
export const SVGIconArrowUpID = '__icon_arrow_up';
export const SVGIconArrowDownID = '__icon_arrow_down';

export const SVGBasicFilters = () => (
	<>
		<SVGIconDefsProxyOnce
			id={SVGBasicBlurFilterID}
			icon={
				<filter
					filterUnits="objectBoundingBox"
					primitiveUnits="objectBoundingBox"
					colorInterpolationFilters="linearRGB"
					x="0"
					y="0"
					width="100%"
					height="100%"
					filterRes="100">
					<feGaussianBlur stdDeviation="8" in="SourceGraphic" edgeMode="duplicate" />
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGFilterDesaturateID}
			icon={
				<filter colorInterpolationFilters="sRGB">
					<feColorMatrix in="SourceGraphic" type="saturate" values="0" />
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGFilterSaturate20ID}
			icon={
				<filter colorInterpolationFilters="sRGB">
					<feColorMatrix in="SourceGraphic" type="saturate" values="0.2" />
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGFilterSaturate40ID}
			icon={
				<filter colorInterpolationFilters="sRGB">
					<feColorMatrix in="SourceGraphic" type="saturate" values="0.4" />
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGFilterSaturate60ID}
			icon={
				<filter colorInterpolationFilters="sRGB">
					<feColorMatrix in="SourceGraphic" type="saturate" values="0.6" />
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGFilterSaturate80ID}
			icon={
				<filter colorInterpolationFilters="sRGB">
					<feColorMatrix in="SourceGraphic" type="saturate" values="0.8" />
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGDarkenFilterID}
			icon={
				<filter colorInterpolationFilters="sRGB">
					<feColorMatrix in="SourceGraphic" result="D" type="saturate" values="0.5" />
					<feComponentTransfer in="D">
						<feFuncR type="linear" slope="0.7" intercept="-0.2" />
						<feFuncG type="linear" slope="0.7" intercept="-0.2" />
						<feFuncB type="linear" slope="0.7" intercept="-0.2" />
						<feFuncA type="identity" />
					</feComponentTransfer>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGBasicGlowFilterID}
			icon={
				<filter
					height="300%"
					width="300%"
					x="-100%"
					y="-100%"
					colorInterpolationFilters="sRGB"
					filterUnits="objectBoundingBox">
					<feMorphology operator="dilate" radius="1" in="SourceAlpha" result="thicken" />
					<feGaussianBlur in="thicken" stdDeviation="2" result="blurred" />
					<feFlood floodColor="rgba(255,255,255,0.8)" result="glowColor" />
					<feComposite in="glowColor" in2="blurred" operator="in" result="softGlow_colored" />
					<feMerge>
						<feMergeNode in="softGlow_colored" />
						<feMergeNode in="SourceGraphic" />
					</feMerge>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGContrastGlowFilterID}
			icon={
				<filter
					height="300%"
					width="300%"
					x="-75%"
					y="-75%"
					colorInterpolationFilters="sRGB"
					filterUnits="objectBoundingBox">
					<feMorphology operator="dilate" radius="2" in="SourceAlpha" result="thicken" />
					<feGaussianBlur in="thicken" stdDeviation="1" result="blurred" />
					<feFlood floodColor="rgba(0,50,100,0.7)" result="glowColor" />
					<feComposite in="glowColor" in2="blurred" operator="in" result="softGlow_colored" />
					<feMerge>
						<feMergeNode in="softGlow_colored" />
						<feMergeNode in="SourceGraphic" />
					</feMerge>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGBasicShadowFilterID}
			icon={
				<filter
					height="300%"
					width="300%"
					x="-75%"
					y="-75%"
					colorInterpolationFilters="sRGB"
					filterUnits="objectBoundingBox">
					<feMorphology operator="dilate" radius="1" in="SourceAlpha" result="thicken" />
					<feGaussianBlur in="thicken" stdDeviation="4" result="blurred" />
					<feFlood floodColor="rgba(0,0,0,0.85)" result="glowColor" />
					<feComposite in="glowColor" in2="blurred" operator="in" result="softGlow_colored" />
					<feMerge>
						<feMergeNode in="softGlow_colored" />
						<feMergeNode in="SourceGraphic" />
					</feMerge>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGLargeGlowFilterID}
			icon={
				<filter
					height="300%"
					width="300%"
					x="-75%"
					y="-75%"
					colorInterpolationFilters="sRGB"
					filterUnits="objectBoundingBox">
					<feMorphology operator="dilate" radius="1" in="SourceAlpha" result="thicken" />
					<feGaussianBlur in="thicken" stdDeviation="2" result="blurred" />
					<feFlood floodColor="rgba(100,20,58,0.7)" result="glowColor" />
					<feComposite in="glowColor" in2="blurred" operator="in" result="softGlow_colored" />
					<feMerge>
						<feMergeNode in="softGlow_colored" />
						<feMergeNode in="SourceGraphic" />
					</feMerge>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGBasicBevelFilterID}
			icon={
				<filter
					filterUnits="objectBoundingBox"
					x="-25%"
					y="-25%"
					width="150%"
					height="150%"
					primitiveUnits="objectBoundingBox"
					colorInterpolationFilters="sRGB">
					<feGaussianBlur in="SourceAlpha" stdDeviation="1" result="blur" />
					<feSpecularLighting
						in="blur"
						surfaceScale="3"
						specularConstant="0.5"
						specularExponent="10"
						result="specOut"
						lightingColor="#CCC">
						<fePointLight x="5000" y="-2500" z="2500" />
					</feSpecularLighting>
					<feComposite in="specOut" in2="SourceAlpha" operator="in" result="specOut2" />
					<feComposite
						in="SourceGraphic"
						in2="specOut2"
						operator="arithmetic"
						k1="0"
						k2="1"
						k3="1"
						k4="0"
						result="litPaint"
					/>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGBasicNoiseFilterID}
			icon={
				<filter filterUnits="objectBoundingBox" x="-25%" y="-25%" width="150%" height="150%">
					<feTurbulence baseFrequency="1" result="noisy" />
					<feColorMatrix type="saturate" values="0" />
					<feBlend in="SourceGraphic" in2="noisy" result="blend" mode="hard-light" />
					<feComposite in="blend" in2="SourceAlpha" operator="in" />
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGAnimatedNoiseFilterID}
			icon={
				<filter filterUnits="objectBoundingBox" x="-25%" y="-25%" width="150%" height="150%">
					<feTurbulence type="fractalNoise" baseFrequency="1" result="noisy">
						<animate
							attributeType="XML"
							attributeName="seed"
							dur="3s"
							fill="freeze"
							repeatCount="indefinite"
							values="1;15;1"
						/>
					</feTurbulence>
					<feColorMatrix type="saturate" values="0" />
					<feBlend in="SourceGraphic" in2="noisy" result="blend" mode="multiply" />
					<feComposite in="blend" in2="SourceAlpha" operator="in" />
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGFractalNoiseFilterID}
			icon={
				<filter
					colorInterpolationFilters="sRGB"
					filterUnits="userSpaceOnUse"
					primitiveUnits="userSpaceOnUse"
					x="-25%"
					y="-25%"
					width="150%"
					height="150%">
					<feTurbulence
						type="fractalNoise"
						baseFrequency="0.055 0.075"
						numOctaves="2"
						seed="6"
						stitchTiles="stitch"
						x="0%"
						y="0%"
						width="100%"
						height="100%"
						result="turbulence"
					/>
					<feBlend
						mode="luminosity"
						x="0%"
						y="0%"
						width="100%"
						height="100%"
						in="turbulence"
						in2="SourceGraphic"
						result="blend"
					/>
					<feComposite
						in="blend"
						in2="SourceAlpha"
						operator="in"
						x="0%"
						y="0%"
						width="100%"
						height="100%"
						result="composite1"
					/>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGBasicEmbossFilterID}
			icon={
				<filter
					colorInterpolationFilters="linearRGB"
					primitiveUnits="userSpaceOnUse"
					filterUnits="objectBoundingBox"
					x="-25%"
					y="-25%"
					width="150%"
					height="150%">
					<feFlood floodColor="#FFFFFF" floodOpacity="0.9" result="whiteOutline" />
					<feFlood floodColor="#000000" floodOpacity="0.9" result="blackOutline" />
					<feFlood floodColor="#777777" floodOpacity="0.8" result="grayOutline" />
					<feComposite in="whiteOutline" in2="SourceAlpha" operator="in" result="whiteOutlineMasked" />
					<feComposite in="blackOutline" in2="SourceAlpha" operator="in" result="blackOutlineMasked" />
					<feComposite in="grayOutline" in2="SourceAlpha" operator="in" result="grayOutlineMasked" />
					<feOffset dx="0" dy="-1" in="blackOutlineMasked" result="blackOutlineOffset" />
					<feOffset dx="0" dy="1" in="whiteOutlineMasked" result="whiteOutlineOffset" />
					<feOffset dx="-0.5" dy="-0.5" in="grayOutlineMasked" result="grayOutlineOffset" />
					<feGaussianBlur
						stdDeviation="1 1"
						in="whiteOutlineOffset"
						edgeMode="none"
						result="whiteOutlineBlur"
					/>
					<feComposite in="blackOutline" in2="SourceAlpha" operator="out" result="blackInnerMask" />
					<feGaussianBlur stdDeviation="4" in="blackInnerMask" edgeMode="none" result="innerShadow" />
					<feOffset dx="1" dy="2" in="innerShadow" result="innerShadowOffset" />
					<feComposite in="innerShadowOffset" in2="SourceAlpha" operator="in" result="innerShadowMasked" />
					<feBlend mode="screen" in="whiteOutlineBlur" in2="SourceGraphic" result="whiteEdgeBlended" />
					<feBlend
						mode="multiply"
						in="grayOutlineOffset"
						in2="whiteEdgeBlended"
						result="grayAndWhiteEdgeBlended"
					/>
					<feBlend
						mode="multiply"
						in="blackOutlineOffset"
						in2="grayAndWhiteEdgeBlended"
						result="edgesBlended"
					/>
					<feBlend mode="normal" in="SourceGraphic" in2="edgesBlended" result="originalWithEdges" />
					<feMerge>
						<feMergeNode in="originalWithEdges" />
						<feMergeNode in="innerShadowMasked" />
					</feMerge>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGMaskFilterID}
			icon={
				<filter colorInterpolationFilters="linearRGB" filterUnits="objectBoundingBox">
					<feFlood
						floodColor="#ffffff"
						floodOpacity="1"
						height="100%"
						result="flood"
						width="100%"
						x="0%"
						y="0%"
					/>
					<feComposite
						height="100%"
						in="flood"
						in2="SourceAlpha"
						operator="in"
						result="composite"
						width="100%"
						x="0%"
						y="0%"
					/>
				</filter>
			}
			type={'filters'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGMaskDilateFilterID}
			icon={
				<filter colorInterpolationFilters="linearRGB" filterUnits="userSpaceOnUse">
					<feFlood
						floodColor="#ffffff"
						floodOpacity="1"
						height="100%"
						result="flood"
						width="100%"
						x="0%"
						y="0%"
					/>
					<feComposite
						height="100%"
						in="flood"
						in2="SourceAlpha"
						operator="in"
						result="composite"
						width="100%"
						x="0%"
						y="0%"
					/>
					<feMorphology
						operator="dilate"
						radius=".25"
						x="0%"
						y="0%"
						width="100%"
						height="100%"
						in="composite"
						result="morphology"
					/>
				</filter>
			}
			type={'filters'}
		/>
	</>
);

export const SVGBasicSymbols = React.memo(() => (
	<>
		<SVGIconDefsProxyOnce
			id={SVGIconArrowDownID}
			icon={
				<symbol viewBox="0 0 292.359 292.359">
					<path
						d="M286.935,69.377c-3.614-3.617-7.898-5.424-12.848-5.424H18.274c-4.952,0-9.233,1.807-12.85,5.424
		C1.807,72.998,0,77.279,0,82.228c0,4.948,1.807,9.229,5.424,12.847l127.907,127.907c3.621,3.617,7.902,5.428,12.85,5.428
		s9.233-1.811,12.847-5.428L286.935,95.074c3.613-3.617,5.427-7.898,5.427-12.847C292.362,77.279,290.548,72.998,286.935,69.377z"
					/>
				</symbol>
			}
			type={'symbols'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGIconArrowUpID}
			icon={
				<symbol viewBox="0 0 292.359 292.359">
					<path
						d="M286.935,197.287L159.028,69.381c-3.613-3.617-7.895-5.424-12.847-5.424s-9.233,1.807-12.85,5.424L5.424,197.287
		C1.807,200.904,0,205.186,0,210.134s1.807,9.233,5.424,12.847c3.621,3.617,7.902,5.425,12.85,5.425h255.813
		c4.949,0,9.233-1.808,12.848-5.425c3.613-3.613,5.427-7.898,5.427-12.847S290.548,200.904,286.935,197.287z"
					/>
				</symbol>
			}
			type={'symbols'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGIconArrowLeftID}
			icon={
				<symbol viewBox="0 0 292.359 292.359">
					<path
						d="M222.979,5.424C219.364,1.807,215.08,0,210.132,0c-4.949,0-9.233,1.807-12.848,5.424L69.378,133.331
		c-3.615,3.617-5.424,7.898-5.424,12.847c0,4.949,1.809,9.233,5.424,12.847l127.906,127.907c3.614,3.617,7.898,5.428,12.848,5.428
		c4.948,0,9.232-1.811,12.847-5.428c3.617-3.614,5.427-7.898,5.427-12.847V18.271C228.405,13.322,226.596,9.042,222.979,5.424z"
					/>
				</symbol>
			}
			type={'symbols'}
		/>
		<SVGIconDefsProxyOnce
			id={SVGIconArrowRightID}
			icon={
				<symbol viewBox="0 0 292.359 292.359">
					<path
						d="M222.979,133.331L95.073,5.424C91.456,1.807,87.178,0,82.226,0c-4.952,0-9.233,1.807-12.85,5.424
		c-3.617,3.617-5.424,7.898-5.424,12.847v255.813c0,4.948,1.807,9.232,5.424,12.847c3.621,3.617,7.902,5.428,12.85,5.428
		c4.949,0,9.23-1.811,12.847-5.428l127.906-127.907c3.614-3.613,5.428-7.897,5.428-12.847
		C228.407,141.229,226.594,136.948,222.979,133.331z"
					/>
				</symbol>
			}
			type={'symbols'}
		/>
	</>
));

/*
SVGIconContainer.registerFilter(
	SVGMaskFilterID,
	<filter colorInterpolationFilters="linearRGB" filterUnits="objectBoundingBox">
		<feFlood floodColor="#ffffff" floodOpacity="1" height="100%" result="flood" width="100%" x="0%" y="0%" />
		<feComposite
			height="100%"
			in="flood"
			in2="SourceAlpha"
			operator="in"
			result="composite"
			width="100%"
			x="0%"
			y="0%"
		/>
	</filter>,
)
	.registerFilter(
		SVGMaskDilateFilterID,
		<filter colorInterpolationFilters="linearRGB" filterUnits="userSpaceOnUse">
			<feFlood floodColor="#ffffff" floodOpacity="1" height="100%" result="flood" width="100%" x="0%" y="0%" />
			<feComposite
				height="100%"
				in="flood"
				in2="SourceAlpha"
				operator="in"
				result="composite"
				width="100%"
				x="0%"
				y="0%"
			/>
			<feMorphology
				operator="dilate"
				radius=".25"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				in="composite"
				result="morphology"
			/>
		</filter>,
	)
	.registerFilter(
		SVGBasicEmbossFilterID,
		<filter
			colorInterpolationFilters="linearRGB"
			primitiveUnits="userSpaceOnUse"
			filterUnits="objectBoundingBox"
			x="-25%"
			y="-25%"
			width="150%"
			height="150%">
			<feFlood floodColor="#FFFFFF" floodOpacity="0.9" result="whiteOutline" />
			<feFlood floodColor="#000000" floodOpacity="0.9" result="blackOutline" />
			<feFlood floodColor="#777777" floodOpacity="0.8" result="grayOutline" />
			<feComposite in="whiteOutline" in2="SourceAlpha" operator="in" result="whiteOutlineMasked" />
			<feComposite in="blackOutline" in2="SourceAlpha" operator="in" result="blackOutlineMasked" />
			<feComposite in="grayOutline" in2="SourceAlpha" operator="in" result="grayOutlineMasked" />
			<feOffset dx="0" dy="-1" in="blackOutlineMasked" result="blackOutlineOffset" />
			<feOffset dx="0" dy="1" in="whiteOutlineMasked" result="whiteOutlineOffset" />
			<feOffset dx="-0.5" dy="-0.5" in="grayOutlineMasked" result="grayOutlineOffset" />
			<feGaussianBlur stdDeviation="1 1" in="whiteOutlineOffset" edgeMode="none" result="whiteOutlineBlur" />
			<feComposite in="blackOutline" in2="SourceAlpha" operator="out" result="blackInnerMask" />
			<feGaussianBlur stdDeviation="4" in="blackInnerMask" edgeMode="none" result="innerShadow" />
			<feOffset dx="1" dy="2" in="innerShadow" result="innerShadowOffset" />
			<feComposite in="innerShadowOffset" in2="SourceAlpha" operator="in" result="innerShadowMasked" />
			<feBlend mode="screen" in="whiteOutlineBlur" in2="SourceGraphic" result="whiteEdgeBlended" />
			<feBlend mode="multiply" in="grayOutlineOffset" in2="whiteEdgeBlended" result="grayAndWhiteEdgeBlended" />
			<feBlend mode="multiply" in="blackOutlineOffset" in2="grayAndWhiteEdgeBlended" result="edgesBlended" />
			<feBlend mode="normal" in="SourceGraphic" in2="edgesBlended" result="originalWithEdges" />
			<feMerge>
				<feMergeNode in="originalWithEdges" />
				<feMergeNode in="innerShadowMasked" />
			</feMerge>
		</filter>,
	)
	.registerFilter(
		SVGFractalNoiseFilterID,
		<filter
			colorInterpolationFilters="sRGB"
			filterUnits="userSpaceOnUse"
			primitiveUnits="userSpaceOnUse"
			x="-25%"
			y="-25%"
			width="150%"
			height="150%">
			<feTurbulence
				type="fractalNoise"
				baseFrequency="0.055 0.075"
				numOctaves="2"
				seed="6"
				stitchTiles="stitch"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				result="turbulence"
			/>
			<feBlend
				mode="luminosity"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				in="turbulence"
				in2="SourceGraphic"
				result="blend"
			/>
			<feComposite
				in="blend"
				in2="SourceAlpha"
				operator="in"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				result="composite1"
			/>
		</filter>,
	)
	.registerFilter(
		SVGBasicNoiseFilterID,
		<filter filterUnits="objectBoundingBox" x="-25%" y="-25%" width="150%" height="150%">
			<feTurbulence baseFrequency="1" result="noisy" />
			<feColorMatrix type="saturate" values="0" />
			<feBlend in="SourceGraphic" in2="noisy" result="blend" mode="hard-light" />
			<feComposite in="blend" in2="SourceAlpha" operator="in" />
		</filter>,
	)
	.registerFilter(
		SVGAnimatedNoiseFilterID,
		<filter filterUnits="objectBoundingBox" x="-25%" y="-25%" width="150%" height="150%">
			<feTurbulence type="fractalNoise" baseFrequency="1" result="noisy">
				<animate
					attributeType="XML"
					attributeName="seed"
					dur="3s"
					fill="freeze"
					repeatCount="indefinite"
					values="1;15;1"
				/>
			</feTurbulence>
			<feColorMatrix type="saturate" values="0" />
			<feBlend in="SourceGraphic" in2="noisy" result="blend" mode="multiply" />
			<feComposite in="blend" in2="SourceAlpha" operator="in" />
		</filter>,
	)
	.registerFilter(
		SVGContrastGlowFilterID,
		<filter
			height="300%"
			width="300%"
			x="-75%"
			y="-75%"
			colorInterpolationFilters="sRGB"
			filterUnits="objectBoundingBox">
			<feMorphology operator="dilate" radius="2" in="SourceAlpha" result="thicken" />
			<feGaussianBlur in="thicken" stdDeviation="1" result="blurred" />
			<feFlood floodColor="rgba(0,50,100,0.7)" result="glowColor" />
			<feComposite in="glowColor" in2="blurred" operator="in" result="softGlow_colored" />
			<feMerge>
				<feMergeNode in="softGlow_colored" />
				<feMergeNode in="SourceGraphic" />
			</feMerge>
		</filter>,
	)
	.registerFilter(
		SVGBasicShadowFilterID,
		<filter
			height="300%"
			width="300%"
			x="-75%"
			y="-75%"
			colorInterpolationFilters="sRGB"
			filterUnits="objectBoundingBox">
			<feMorphology operator="dilate" radius="1" in="SourceAlpha" result="thicken" />
			<feGaussianBlur in="thicken" stdDeviation="4" result="blurred" />
			<feFlood floodColor="rgba(0,0,0,0.85)" result="glowColor" />
			<feComposite in="glowColor" in2="blurred" operator="in" result="softGlow_colored" />
			<feMerge>
				<feMergeNode in="softGlow_colored" />
				<feMergeNode in="SourceGraphic" />
			</feMerge>
		</filter>,
	)
	.registerFilter(
		SVGLargeGlowFilterID,
		<filter
			height="300%"
			width="300%"
			x="-75%"
			y="-75%"
			colorInterpolationFilters="sRGB"
			filterUnits="objectBoundingBox">
			<feMorphology operator="dilate" radius="1" in="SourceAlpha" result="thicken" />
			<feGaussianBlur in="thicken" stdDeviation="2" result="blurred" />
			<feFlood floodColor="rgba(100,20,58,0.7)" result="glowColor" />
			<feComposite in="glowColor" in2="blurred" operator="in" result="softGlow_colored" />
			<feMerge>
				<feMergeNode in="softGlow_colored" />
				<feMergeNode in="SourceGraphic" />
			</feMerge>
		</filter>,
	)
	.registerFilter(
		SVGBasicBevelFilterID,
		<filter
			filterUnits="objectBoundingBox"
			x="-25%"
			y="-25%"
			width="150%"
			height="150%"
			primitiveUnits="objectBoundingBox"
			colorInterpolationFilters="sRGB">
			<feGaussianBlur in="SourceAlpha" stdDeviation="1" result="blur" />
			<feSpecularLighting
				in="blur"
				surfaceScale="3"
				specularConstant="0.5"
				specularExponent="10"
				result="specOut"
				lightingColor="#CCC">
				<fePointLight x="5000" y="-2500" z="2500" />
			</feSpecularLighting>
			<feComposite in="specOut" in2="SourceAlpha" operator="in" result="specOut2" />
			<feComposite
				in="SourceGraphic"
				in2="specOut2"
				operator="arithmetic"
				k1="0"
				k2="1"
				k3="1"
				k4="0"
				result="litPaint"
			/>
		</filter>,
	)
	.registerFilter(
		SVGBasicBlurFilterID,
		<filter
			filterUnits="objectBoundingBox"
			primitiveUnits="objectBoundingBox"
			colorInterpolationFilters="linearRGB"
			x="0"
			y="0"
			width="100%"
			height="100%"
			filterRes="100">
			<feGaussianBlur stdDeviation="8" in="SourceGraphic" edgeMode="duplicate" />
		</filter>,
	)
	.registerFilter(
		SVGFilterDesaturateID,
		<filter colorInterpolationFilters="sRGB">
			<feColorMatrix in="SourceGraphic" type="saturate" values="0" />
		</filter>,
	)
	.registerFilter(
		SVGFilterSaturate20ID,
		<filter colorInterpolationFilters="sRGB">
			<feColorMatrix in="SourceGraphic" type="saturate" values="0.2" />
		</filter>,
	)
	.registerFilter(
		SVGFilterSaturate40ID,
		<filter colorInterpolationFilters="sRGB">
			<feColorMatrix in="SourceGraphic" type="saturate" values="0.4" />
		</filter>,
	)
	.registerFilter(
		SVGDarkenFilterID,
		<filter colorInterpolationFilters="sRGB">
			<feColorMatrix in="SourceGraphic" result="D" type="saturate" values="0.5" />
			<feComponentTransfer in="D">
				<feFuncR type="linear" slope="0.7" intercept="-0.2" />
				<feFuncG type="linear" slope="0.7" intercept="-0.2" />
				<feFuncB type="linear" slope="0.7" intercept="-0.2" />
				<feFuncA type="identity" />
			</feComponentTransfer>
		</filter>,
	)
	.registerFilter(
		SVGFilterSaturate60ID,
		<filter colorInterpolationFilters="sRGB">
			<feColorMatrix in="SourceGraphic" type="saturate" values="0.6" />
		</filter>,
	)
	.registerFilter(
		SVGFilterSaturate80ID,
		<filter colorInterpolationFilters="sRGB">
			<feColorMatrix in="SourceGraphic" type="saturate" values="0.8" />
		</filter>,
	)
	.registerFilter(
		SVGBasicGlowFilterID,
		<filter
			height="300%"
			width="300%"
			x="-100%"
			y="-100%"
			colorInterpolationFilters="sRGB"
			filterUnits="objectBoundingBox">
			<feMorphology operator="dilate" radius="1" in="SourceAlpha" result="thicken" />
			<feGaussianBlur in="thicken" stdDeviation="2" result="blurred" />
			<feFlood floodColor="rgba(255,255,255,0.8)" result="glowColor" />
			<feComposite in="glowColor" in2="blurred" operator="in" result="softGlow_colored" />
			<feMerge>
				<feMergeNode in="softGlow_colored" />
				<feMergeNode in="SourceGraphic" />
			</feMerge>
		</filter>,
	)
	.registerSymbol(
		SVGIconArrowDownID,
		<symbol viewBox="0 0 292.359 292.359">
			<path
				d="M286.935,69.377c-3.614-3.617-7.898-5.424-12.848-5.424H18.274c-4.952,0-9.233,1.807-12.85,5.424
		C1.807,72.998,0,77.279,0,82.228c0,4.948,1.807,9.229,5.424,12.847l127.907,127.907c3.621,3.617,7.902,5.428,12.85,5.428
		s9.233-1.811,12.847-5.428L286.935,95.074c3.613-3.617,5.427-7.898,5.427-12.847C292.362,77.279,290.548,72.998,286.935,69.377z"
			/>
		</symbol>,
	)
	.registerSymbol(
		SVGIconArrowUpID,
		<symbol viewBox="0 0 292.359 292.359">
			<path
				d="M286.935,197.287L159.028,69.381c-3.613-3.617-7.895-5.424-12.847-5.424s-9.233,1.807-12.85,5.424L5.424,197.287
		C1.807,200.904,0,205.186,0,210.134s1.807,9.233,5.424,12.847c3.621,3.617,7.902,5.425,12.85,5.425h255.813
		c4.949,0,9.233-1.808,12.848-5.425c3.613-3.613,5.427-7.898,5.427-12.847S290.548,200.904,286.935,197.287z"
			/>
		</symbol>,
	)
	.registerSymbol(
		SVGIconArrowLeftID,
		<symbol viewBox="0 0 292.359 292.359">
			<path
				d="M222.979,5.424C219.364,1.807,215.08,0,210.132,0c-4.949,0-9.233,1.807-12.848,5.424L69.378,133.331
		c-3.615,3.617-5.424,7.898-5.424,12.847c0,4.949,1.809,9.233,5.424,12.847l127.906,127.907c3.614,3.617,7.898,5.428,12.848,5.428
		c4.948,0,9.232-1.811,12.847-5.428c3.617-3.614,5.427-7.898,5.427-12.847V18.271C228.405,13.322,226.596,9.042,222.979,5.424z"
			/>
		</symbol>,
	)
	.registerSymbol(
		SVGIconArrowRightID,
		<symbol viewBox="0 0 292.359 292.359">
			<path
				d="M222.979,133.331L95.073,5.424C91.456,1.807,87.178,0,82.226,0c-4.952,0-9.233,1.807-12.85,5.424
		c-3.617,3.617-5.424,7.898-5.424,12.847v255.813c0,4.948,1.807,9.232,5.424,12.847c3.621,3.617,7.902,5.428,12.85,5.428
		c4.949,0,9.23-1.811,12.847-5.428l127.906-127.907c3.614-3.613,5.428-7.897,5.428-12.847
		C228.407,141.229,226.594,136.948,222.979,133.331z"
			/>
		</symbol>,
	);
*/

export default SVGIconContainer;

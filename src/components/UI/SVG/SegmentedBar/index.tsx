import * as React from 'react';
import _ from 'underscore';
import digest from '../../../../utils/digest';
import { getTemplateColor, IUICommonProps } from '../../index';
import SVGIconContainer, { SVGBasicBevelFilterID } from '../IconContainer';
import Style from './index.scss';

export type ISVGSegmentedBarSegment = Omit<IUICommonProps, 'tooltip'> & {
	value: number;
	colorTop?: string;
	colorBottom?: string;
	gradientId?: string;
	blink?: boolean;
};

export type SVGSegmentedBarProps = IUICommonProps & {
	segments: ISVGSegmentedBarSegment[];
	strokeColor?: string;
	width?: number;
};

interface IStateType {
	id: string;
}

export default class SVGSegmentedBar extends React.PureComponent<SVGSegmentedBarProps, IStateType> {
	public static getClass(): string {
		return Style.segmentedBar;
	}

	public static getDerivedStateFromProps(nextProps: SVGSegmentedBarProps, prevState: IStateType) {
		return {
			id: digest(nextProps),
		};
	}

	constructor(props: SVGSegmentedBarProps) {
		super(props);
		this.state = {
			id: digest(props),
		};
	}

	public render() {
		const width = this.props.width || 198;
		const total = this.props.segments.reduce((a, v) => a + v.value, 0);
		const segments = this.props.segments.map((v) => ({
			...v,
			gradientId: v.gradientId || digest(v),
		}));
		let current = 0;
		return (
			<svg
				className={[this.props.className || '', Style.segmentedBar].join(' ')}
				xmlns="http://www.w3.org/2000/svg"
				preserveAspectRatio="none"
				style={{ width: this.props.width > 0 ? width + 'px' : '' }}
				viewBox={`0 0 ${width + 2} 20`}>
				<defs>
					{_.uniq(
						segments.filter((v) => v.colorBottom && v.colorTop),
						true,
						(v) => v.gradientId,
					).map((segment) => (
						<linearGradient
							id={segment.gradientId}
							key={segment.gradientId}
							x1="50%"
							y1="0"
							x2="75%"
							y2="100%"
							gradientUnits="objectBoundingBox">
							<stop offset="0" stopColor={segment.colorBottom} />
							<stop offset="1" stopColor={segment.colorTop} />
						</linearGradient>
					))}
					<clipPath id={this.getMaskId()}>
						<rect x="1" width={width} height="18" y="1" stroke="none" rx={10} ry={20} />
					</clipPath>
				</defs>
				<g filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}>
					<g clipPath={SVGIconContainer.getUrlRef(this.getMaskId())}>
						{segments.map((segment, i) => {
							const x = 1 + (current / total) * width;
							const w = ((total - current) / total) * width;
							const t = (
								<g key={i} className={segment.blink ? Style.blink : ''}>
									<rect
										x={x}
										y={1}
										height={18}
										width={w}
										fill={SVGIconContainer.getUrlRef(segment.gradientId)}
									/>
									{i > 0 && (
										<line
											strokeWidth={2}
											x1={x}
											x2={x}
											y1={1}
											y2={19}
											stroke={this.props.strokeColor || getTemplateColor('dark_gray')}
										/>
									)}
								</g>
							);
							current += segment.value;
							return t;
						})}
					</g>
					<rect
						x="1"
						width={width}
						height={18}
						y="1"
						stroke={getTemplateColor('dark_gray')}
						rx={10}
						ry={20}
						fill="none"
					/>
				</g>
			</svg>
		);
	}

	protected getMaskId() {
		return `${this.state.id}-mask`;
	}
}

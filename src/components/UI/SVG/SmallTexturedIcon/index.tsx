import * as React from 'react';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import UITooltip from '../../Templates/Tooltip';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGBasicNoiseFilterID,
	SVGFilterWrapper,
	SVGLargeGlowFilterID,
} from '../IconContainer';
import Style from './index.scss';

export enum TSVGSmallTexturedIconStyles {
	light = 'light',
	dark = 'dark',
	blue = 'blue',
	pink = 'pink',
	red = 'red',
	gray = 'gray',
	green = 'green',
	gold = 'gold',
	navy = 'navy',
	teal = 'teal',
}

export type SVGSmallTexturedIconProps = IUICommonProps & {
	style?: TSVGSmallTexturedIconStyles;
	asSvg?: boolean;
	viewBox?: number;
};

const SVGSmallIconGradientDarkID = 'icon-gradient-dark';
const SVGSmallIconGradientLightID = 'icon-gradient-light';
const SVGSmallIconGradientRedID = 'icon-gradient-red';
const SVGSmallIconGradientBlueID = 'icon-gradient-blue';
const SVGSmallIconGradientPinkID = 'icon-gradient-pink';
const SVGSmallIconGradientGrayID = 'icon-gradient-gray';
const SVGSmallIconGradientGreenID = 'icon-gradient-green';
const SVGSmallIconGradientGoldID = 'icon-gradient-gold';
const SVGSmallIconGradientNavyID = 'icon-gradient-navy';
const SVGSmallIconGradientTealID = 'icon-gradient-teal';

SVGIconContainer.registerGradient(
	SVGSmallIconGradientLightID,
	<linearGradient x1="75%" y1="0" x2="0%" y2="50%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor={getTemplateColor('light_gray')} />
		<stop offset="0.25" stopColor={getTemplateColor('text_highlight')} />
		<stop offset="0.55" stopColor={getTemplateColor('light_orange')} />
		<stop offset="0.77" stopColor={getTemplateColor('light_golden')} />
		<stop offset="1" stopColor={getTemplateColor('light_orange')} />
	</linearGradient>,
)
	.registerGradient(
		SVGSmallIconGradientDarkID,
		<linearGradient x1="60%" y1="20%" x2="40%" y2="80%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('tooltip_bg')} />
			<stop offset="0.25" stopColor={getTemplateColor('dark_purple')} />
			<stop offset="0.55" stopColor={getTemplateColor('text_dark')} />
			<stop offset="1" stopColor={getTemplateColor('dark_brown')} />
		</linearGradient>,
	)
	.registerGradient(
		SVGSmallIconGradientNavyID,
		<linearGradient x1="0%" y1="30%" x2="100%" y2="60%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('tooltip_bg')} />
			<stop offset="0.51" stopColor={getTemplateColor('dark_blue')} />
			<stop offset="0.86" stopColor={getTemplateColor('navy_blue')} />
			<stop offset="1" stopColor={getTemplateColor('text_dark')} />
		</linearGradient>,
	)
	.registerGradient(
		SVGSmallIconGradientPinkID,
		<linearGradient x1="10%" y1="0" x2="90%" y2="100%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('dark_purple')} />
			<stop offset="0.3" stopColor={getTemplateColor('light_pink')} />
			<stop offset="0.8" stopColor={getTemplateColor('light_purple')} />
			<stop offset="1" stopColor={getTemplateColor('dark_pink')} />
		</linearGradient>,
	)
	.registerGradient(
		SVGSmallIconGradientGoldID,
		<linearGradient x1="10%" y1="20%" x2="90%" y2="80%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('light_orange')} />
			<stop offset="0.25" stopColor={getTemplateColor('light_golden')} />
			<stop offset="0.65" stopColor={getTemplateColor('golden')} />
			<stop offset="0.8" stopColor={getTemplateColor('light_pink')} />
			<stop offset="1" stopColor={getTemplateColor('light_orange')} />
		</linearGradient>,
	)
	.registerGradient(
		SVGSmallIconGradientGreenID,
		<linearGradient x1="55%" y1="0" x2="40%" y2="100%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('text_main')} />
			<stop offset="0.25" stopColor={getTemplateColor('light_green')} />
			<stop offset="0.7" stopColor={getTemplateColor('grass_green')} />
			<stop offset="1" stopColor={getTemplateColor('dark_green')} />
		</linearGradient>,
	)
	.registerGradient(
		SVGSmallIconGradientBlueID,
		<linearGradient x1="0" y1="0" x2="35%" y2="100%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('dark_blue')} />
			<stop offset="0.35" stopColor={getTemplateColor('light_blue')} />
			<stop offset="0.78" stopColor={getTemplateColor('text_highlight')} />
			<stop offset="0.82" stopColor={getTemplateColor('light_blue')} />
			<stop offset="1" stopColor={getTemplateColor('light_purple')} />
		</linearGradient>,
	)
	.registerGradient(
		SVGSmallIconGradientRedID,
		<linearGradient x1="20%" y1="0%" x2="60%" y2="100%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('light_orange')} />
			<stop offset="0.45" stopColor={getTemplateColor('dark_red')} />
			<stop offset="0.85" stopColor={getTemplateColor('light_red')} />
			<stop offset="1" stopColor={getTemplateColor('light_orange')} />
		</linearGradient>,
	)
	.registerGradient(
		SVGSmallIconGradientGrayID,
		<linearGradient x1="0" y1="0" x2="70%" y2="100%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('gray_bg')} />
			<stop offset="0.3" stopColor={getTemplateColor('light_gray')} />
			<stop offset="0.65" stopColor={getTemplateColor('text_highlight')} />
			<stop offset="0.9" stopColor={getTemplateColor('light_gray')} />
			<stop offset="1" stopColor={getTemplateColor('dark_gray')} />
		</linearGradient>,
	)
	.registerGradient(
		SVGSmallIconGradientTealID,
		<linearGradient x1="0" y1="0" x2="30%" y2="100%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={getTemplateColor('tealish')} />
			<stop offset="0.3" stopColor={getTemplateColor('text_highlight')} />
			<stop offset="0.65" stopColor={getTemplateColor('light_teal')} />
			<stop offset="0.9" stopColor={getTemplateColor('tealish')} />
			<stop offset="1" stopColor={getTemplateColor('dark_green')} />
		</linearGradient>,
	);

export default class SVGSmallTexturedIcon extends React.PureComponent<SVGSmallTexturedIconProps, {}> {
	public static getClass(): string {
		return Style.small_icon;
	}

	public static getGradientId(type: TSVGSmallTexturedIconStyles): string {
		switch (type) {
			case TSVGSmallTexturedIconStyles.dark:
				return SVGSmallIconGradientDarkID;
			case TSVGSmallTexturedIconStyles.blue:
				return SVGSmallIconGradientBlueID;
			case TSVGSmallTexturedIconStyles.pink:
				return SVGSmallIconGradientPinkID;
			case TSVGSmallTexturedIconStyles.gray:
				return SVGSmallIconGradientGrayID;
			case TSVGSmallTexturedIconStyles.red:
				return SVGSmallIconGradientRedID;
			case TSVGSmallTexturedIconStyles.green:
				return SVGSmallIconGradientGreenID;
			case TSVGSmallTexturedIconStyles.gold:
				return SVGSmallIconGradientGoldID;
			case TSVGSmallTexturedIconStyles.teal:
				return SVGSmallIconGradientTealID;
			case TSVGSmallTexturedIconStyles.navy:
				return SVGSmallIconGradientNavyID;
			default:
				return SVGSmallIconGradientLightID;
		}
	}

	public render() {
		const Icon = (className: string = null) => (
			<SVGFilterWrapper className={className}>
				<g filter={SVGIconContainer.getUrlRef(SVGLargeGlowFilterID)}>
					<g filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)}>
						<g
							filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}
							fill={SVGIconContainer.getUrlRef(SVGSmallTexturedIcon.getGradientId(this.props.style))}
						>
							{this.props.children}
						</g>
					</g>
				</g>
			</SVGFilterWrapper>
		);
		if (!this.props.asSvg)
			return (
				<span
					className={classnames(
						UIContainer.iconClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
						this.props.className || '',
					)}
				>
					{Icon()}
					{this.props.tooltip && <UITooltip>{this.props.tooltip}</UITooltip>}
				</span>
			);
		return Icon(classnames(this.props.className || '', UIContainer.iconClass));
	}
}

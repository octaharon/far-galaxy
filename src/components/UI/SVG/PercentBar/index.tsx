import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import digest from '../../../../utils/digest';
import { classnames, getTemplateColor, IUICommonProps } from '../../index';
import SVGIconContainer, { SVGBasicBevelFilterID } from '../IconContainer';
import Style from './index.scss';

export type SVGPercentBarProps = IUICommonProps & {
	value: number;
	colorTop?: string;
	colorBottom?: string;
	gradientId?: string;
	strokeColor?: string;
	width?: number;
};

interface IStateType {
	id: string;
}

export default class SVGPercentBar extends React.PureComponent<SVGPercentBarProps, IStateType> {
	public static getClass(): string {
		return Style.percentedBar;
	}

	public static getDerivedStateFromProps(nextProps: SVGPercentBarProps, prevState: IStateType) {
		return {
			id: digest(nextProps),
		};
	}

	constructor(props: SVGPercentBarProps) {
		super(props);
		this.state = {
			id: digest(props),
		};
	}

	public render() {
		const width = this.props.width || 98;
		const value = BasicMaths.clipTo1(this.props.value) * width;
		const gradientId = this.props.gradientId || digest(this.props);
		return (
			<svg
				className={classnames(this.props.className || '', Style.percentedBar)}
				xmlns="http://www.w3.org/2000/svg"
				preserveAspectRatio="none"
				viewBox={`0 0 ${width + 2} 16`}
			>
				<defs>
					<linearGradient
						id={gradientId}
						key={gradientId}
						x1="50%"
						y1="0"
						x2="75%"
						y2="100%"
						gradientUnits="objectBoundingBox"
					>
						<stop offset="0" stopColor={this.props.colorBottom} />
						<stop offset="1" stopColor={this.props.colorTop} />
					</linearGradient>
					<clipPath id={this.getMaskId()}>
						<rect x="1" width={width} height="14" y="1" stroke="none" rx={3} ry={7} />
					</clipPath>
				</defs>
				<g filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}>
					<g clipPath={SVGIconContainer.getUrlRef(this.getMaskId())}>
						<rect x={0} y={1} height={14} width={value} fill={SVGIconContainer.getUrlRef(gradientId)} />
						{this.props.value < 1 && (
							<line
								strokeWidth={2}
								x1={value}
								x2={value}
								y1={1}
								y2={15}
								stroke={this.props.strokeColor || getTemplateColor('dark_green')}
							/>
						)}
					</g>
					<rect
						x="1"
						width={width}
						height={14}
						y="1"
						stroke={this.props.strokeColor || getTemplateColor('dark_green')}
						rx={3}
						ry={7}
						fill="none"
					/>
				</g>
			</svg>
		);
	}

	protected getMaskId() {
		return `${this.state.id}-mask`;
	}
}

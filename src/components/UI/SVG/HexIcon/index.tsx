import * as React from 'react';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGBasicNoiseFilterID,
	SVGContrastGlowFilterID,
	SVGLargeGlowFilterID,
} from '../IconContainer';
import { SVGIconDefsProxyContainer, SVGIconDefsProxyOnce } from '../IconContainer/IconDefsProxy';
import Style from './index.scss';

export type SVGHexIconProps = IUICommonProps & {
	transparent?: boolean;
	colorTop?: string;
	colorBottom?: string;
	fill?: string;
	highlight?: boolean;
};

export const SVGHexIconShapeID = 'hex-icon-shape';

SVGIconContainer.registerPath(
	SVGHexIconShapeID,
	<path
		stroke="none"
		d={`M38,2
           L82,2
           A12,12 0 0,1 94,10
           L112,44
           A12,12 0 0,1 112,56
           L94,90
           A12,12 0 0,1 82,98
           L38,98
           A12,12 0 0,1 26,90
           L8,56
           A12,12 0 0,1 8,44
           L26,10
           A12,12 0 0,1 38,2`}
	/>,
);

const HexIconGradientCache: Record<string, string> = {};

export default class SVGHexIcon extends React.PureComponent<SVGHexIconProps> {
	public static getClass(): string {
		return Style.svg;
	}

	public static getBorderGradientId(gradientId: string) {
		return `hex-grd-a-${gradientId}`;
	}

	public static getPlateGradientId(gradientId: string) {
		return `hex-grd-b-${gradientId}`;
	}

	public static getUniqueGradientId(props: SVGHexIconProps) {
		if (!props.colorTop && !props.fill) console.warn('empty props', props);
		if (props.fill) return props.fill.toLowerCase().replace('url(#', '').replace(')', '');
		return (props.colorTop + '' + props.colorBottom)
			.replace(/#/g, '')
			.replace(/[^0-9a-fA-F]+/g, '_')
			.toLowerCase();
	}

	public render() {
		const gradientId = SVGHexIcon.getUniqueGradientId(this.props);
		const hasDefinedFillProperty = !!this.props.fill;
		const outerGradientUrl = SVGIconContainer.getUrlRef(SVGHexIcon.getBorderGradientId(gradientId));
		const innerGradientUrl = SVGIconContainer.getUrlRef(SVGHexIcon.getPlateGradientId(gradientId));
		const HexIconDefs = [
			<SVGIconDefsProxyOnce
				key={'outer'}
				type={'gradients'}
				id={SVGHexIcon.getBorderGradientId(SVGHexIcon.getUniqueGradientId(this.props))}
				icon={
					<linearGradient x1="50%" y1="0" x2="25%" y2="100%" gradientUnits="objectBoundingBox">
						<stop offset="0" stopColor={this.props.colorBottom} />
						<stop offset="1" stopColor={this.props.colorTop} />
					</linearGradient>
				}
			/>,
			<SVGIconDefsProxyOnce
				type={'gradients'}
				key={'inner'}
				id={SVGHexIcon.getPlateGradientId(SVGHexIcon.getUniqueGradientId(this.props))}
				icon={
					<linearGradient x1="50%" y1="0" x2="75%" y2="100%" gradientUnits="objectBoundingBox">
						<stop offset="0" stopColor={this.props.colorTop} />
						<stop offset="1" stopColor={this.props.colorBottom} />
					</linearGradient>
				}
			/>,
		];
		const HexIconContent = (
			<svg
				className={Style.svg}
				xmlns="http://www.w3.org/2000/svg"
				viewBox="0 0 120 100"
				overflow="visible"
				onClick={this.props.onClick || null}>
				<g
					filter={SVGIconContainer.getUrlRef(
						!this.props.highlight ? SVGLargeGlowFilterID : SVGContrastGlowFilterID,
					)}
					style={{
						transformOrigin: '50% 50%',
						transform: 'scale(1.08)',
					}}>
					{!this.props.transparent && (
						<g filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}>
							<g className="__hexOutline">
								<use
									{...SVGIconContainer.getXLinkProps(SVGHexIconShapeID)}
									fill={hasDefinedFillProperty ? this.props.fill : outerGradientUrl}
								/>
							</g>
							<g filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)} className="__hexPlate">
								<use
									{...SVGIconContainer.getXLinkProps(SVGHexIconShapeID)}
									fill={hasDefinedFillProperty ? this.props.fill : innerGradientUrl}
								/>
							</g>
						</g>
					)}
					<g
						className="__hexIcon"
						style={{
							transformOrigin: '50% 50%',
						}}>
						{this.props.children}
					</g>
				</g>
			</svg>
		);

		return (
			<span
				className={classnames(
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
					Style.hexIcon,
					this.props.highlight ? Style.highlight : '',
					this.props.onClick ? Style.hoverable : '',
					this.props.className || '',
				)}>
				{!hasDefinedFillProperty && HexIconDefs}
				{HexIconContent}
			</span>
		);
	}
}

import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import Distributions, { applyDistribution } from '../../../../../definitions/maths/Distributions';
import { IUICommonProps } from '../../index';
import Style from './index.scss';

const minimalOffset = 0.25;
const mostlyMinimalOffset = 0.1;
const BellOffset = 0.3;
const GaussOffset = 0.15;
const rows = 5;
const sampleRate = 10000;
const minimalArray = new Array(rows).fill(0).map((v, ix) =>
	BasicMaths.lerp({
		min: minimalOffset,
		max: 1 - minimalOffset,
		x: ix / (rows - 1),
	}),
);
const mostlyMinimalArray = new Array(rows).fill(0).map((v, ix) =>
	BasicMaths.lerp({
		min: 0,
		max: 1 - mostlyMinimalOffset,
		x: Math.pow(ix / (rows - 1), 0.5),
	}),
);

let BellArray = new Array(rows).fill(0);
let GaussArray = new Array(rows).fill(0);
for (let i = 0; i < sampleRate; i++) {
	BellArray[
		Math.round(
			applyDistribution({
				distribution: 'Bell',
				min: 0,
				max: rows - 1,
			}),
		)
	]++;
	GaussArray[
		Math.round(
			applyDistribution({
				distribution: 'Gaussian',
				min: 0,
				max: rows - 1,
			}),
		)
	]++;
}

const bellMax = Math.max(...BellArray);
const gaussMax = Math.max(...GaussArray);
const bellMin = Math.min(...BellArray);
const gaussMin = Math.min(...GaussArray);
BellArray = BellArray.map((v) =>
	BasicMaths.lerp({
		x:
			1 -
			BasicMaths.normalizeValue({
				x: v,
				min: bellMin,
				max: bellMax,
			}),
		min: BellOffset,
		max: 1 - BellOffset,
	}),
);

GaussArray = GaussArray.map((v) =>
	BasicMaths.lerp({
		x:
			1 -
			BasicMaths.normalizeValue({
				x: v,
				min: gaussMin,
				max: gaussMax,
			}),
		min: GaussOffset,
		max: 1 - GaussOffset,
	}),
);

export type UIDistributionIconProps = IUICommonProps & {
	stroke?: boolean;
	fillColor?: string;
	distribution: Distributions.TDistribution;
};

export default class UIDistributionIcon extends React.PureComponent<UIDistributionIconProps, {}> {
	public static getClass(): string {
		return Style.distribution_icon;
	}

	public static getValues(t: Distributions.TDistribution): number[] {
		switch (t) {
			case 'MostlyMinimal':
				return mostlyMinimalArray;
			case 'Minimal':
				return minimalArray;
			case 'Uniform':
				return minimalArray.map((v) => 0.5);
			case 'Bell':
				return BellArray;
			case 'Parabolic':
				return BellArray.map((v) => 1 - v);
			case 'Gaussian':
				return GaussArray;
			case 'Maximal':
				return minimalArray.slice().reverse();
			case 'MostlyMaximal':
				return mostlyMinimalArray.slice().reverse();
			default:
				return [];
		}
	}

	public static getSVGContent(t: Distributions.TDistribution, color = '#FFF', stroke = false) {
		const lineWidth = 1 / (rows * 1.25);
		return (
			<g>
				{UIDistributionIcon.getValues(t).map((v, ix) => (
					<rect
						key={ix}
						className={stroke ? Style.stroked : ''}
						x={ix / rows}
						y={v}
						width={lineWidth}
						height={1 - v}
						fill={color}
					/>
				))}
			</g>
		);
	}

	public render() {
		return (
			<svg
				className={[this.props.className || '', Style.distribution_icon].join(' ')}
				xmlns="http://www.w3.org/2000/svg"
				viewBox={`0 0 1.01 1.01`}
			>
				{UIDistributionIcon.getSVGContent(this.props.distribution, this.props.fillColor, this.props.stroke)}
			</svg>
		);
	}
}

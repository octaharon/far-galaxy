import * as React from 'react';
import UnitDefense from '../../../../../definitions/tactical/damage/defense';
import UIContainer, { getTemplateColor, IUICommonProps } from '../../index';
import UIHitPointsInlineDescriptor from '../../Parts/Chassis/HitPointsInlineDescriptor';
import { SVGShieldEntityIconID } from '../../Parts/Shield/ShieldSlotDescriptor';
import Tokenizer from '../../Templates/Tokenizer';
import UITooltip from '../../Templates/Tooltip';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGBasicShadowFilterID,
	SVGFilterWrapper,
	SVGIconStandartViewbox,
	UISVGIconViewbox,
} from '../IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../SmallTexturedIcon';
import Style from './index.scss';

export type SVGProtectionIconProps = IUICommonProps & {
	color?: string;
	asSvg?: boolean;
	type: UnitDefense.TDamageProtectionType;
	kind?: UnitDefense.TProtectionKind;
};

export const SVGProtectionIconID = 'protection-icon-shape';

export const SVGProtectionSymbolIDs = {
	shield: {
		bias: 'shield-protection-bias',
		factor: 'shield-protection-factor',
		threshold: 'shield-protection-threshold',
		min: 'shield-protection-min',
		max: 'shield-protection-max',
		default: 'shield-protection-default',
	},
	armor: {
		bias: 'armor-protection-bias',
		factor: 'armor-protection-factor',
		threshold: 'armor-protection-threshold',
		min: 'armor-protection-min',
		max: 'armor-protection-max',
		default: 'armor-protection-default',
	},
};

export default class SVGProtectionIcon extends React.PureComponent<SVGProtectionIconProps, {}> {
	public static getClass(): string {
		return Style.svg;
	}

	public static getIcon(
		t: UnitDefense.TDamageProtectionType,
		kind: UnitDefense.TProtectionKind = 'armor',
		className: string = UIContainer.iconClass,
	) {
		return (
			<SVGFilterWrapper className={className}>
				<use
					{...SVGIconContainer.getXLinkProps(
						SVGProtectionSymbolIDs[kind][t as keyof typeof SVGProtectionSymbolIDs.armor] ||
							SVGProtectionSymbolIDs[kind].default,
					)}
				/>
			</SVGFilterWrapper>
		);
	}

	public static getReductionTypeCaption(type: UnitDefense.TDamageProtectionType) {
		switch (type) {
			case 'bias':
				return 'Flat Absorption';
			case 'factor':
				return 'Percentage Absorption';
			case 'threshold':
				return 'Damage Threshold';
			default:
				return 'Damage Limit';
		}
	}

	public static getReductionTypeTooltip(
		type: UnitDefense.TDamageProtectionType,
		kind: UnitDefense.TProtectionKind = 'armor',
	) {
		const caption = SVGProtectionIcon.getReductionTypeCaption(type);
		const icon = SVGProtectionIcon.getIcon(type, kind, UIContainer.tooltipIconClass);
		const typedDamageDescription = (
			<Tokenizer description={`Damage to ${kind === 'armor' ? 'Hit Points' : 'Shields'}`} />
		);
		switch (type) {
			case 'bias':
				return (
					<span>
						{icon}
						<b>{caption}</b> reduces {typedDamageDescription} by the specified value, and is applied before{' '}
						<em>{SVGProtectionIcon.getReductionTypeCaption('factor')}</em>
					</span>
				);
			case 'factor':
				return (
					<span>
						{icon}
						<b>{caption}</b> reduces {typedDamageDescription} by specified fraction. Values more than{' '}
						<em>100%</em> restore{' '}
						<Tokenizer description={`${kind === 'armor' ? 'Hit Points' : 'Shield Capacity'}`} /> instead
					</span>
				);
			case 'threshold':
				return (
					<span>
						{icon}
						<b>{caption}</b> ignores any incoming {typedDamageDescription} below the specified value,
						<Tokenizer
							description={` regardless of other ${kind === 'armor' ? 'Armor' : 'Shield'} properties`}
						/>
					</span>
				);
			default:
				return (
					<span>
						{icon}
						<b>{caption}</b> restricts final {typedDamageDescription} to the specified value from above
						and/or beyond. If <em>Minimal Limit</em> is negative,{' '}
						<Tokenizer
							description={`absorbed Damage can restore ${
								kind === 'armor' ? 'Hit Points' : 'Shield Capacity'
							}`}
						/>
					</span>
				);
		}
	}

	public static getReductionTypeSymbol(
		type: UnitDefense.TDamageProtectionType | 'default',
		color: string = getTemplateColor('light_blue'),
	) {
		switch (type) {
			case 'bias':
				return (
					<text
						x="70%"
						y="80%"
						textAnchor="middle"
						alignmentBaseline="middle"
						className={Style.text}
						transform="scale(1.1)"
						filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}
						fill={color}
					>
						+
					</text>
				);
			case 'factor':
				return (
					<text
						x="80%"
						y="85%"
						textAnchor="middle"
						alignmentBaseline="middle"
						className={Style.small}
						filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}
						fill={color}
					>
						%
					</text>
				);
			case 'threshold':
				return (
					<g
						style={{
							transformOrigin: '50% 50%',
							transform: 'scale(0.9,0.7)',
						}}
					>
						<text
							className={Style.small}
							x="80%"
							y="95%"
							textAnchor="middle"
							alignmentBaseline="middle"
							filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}
							fill={color}
						>
							⟨⟩
						</text>
					</g>
				);
			case 'min':
				return (
					<g
						style={{
							transformOrigin: '50% 50%',
							transform: 'scale(0.9,0.9)',
						}}
					>
						<text
							className={Style.text}
							x="90%"
							y="90%"
							textAnchor="middle"
							alignmentBaseline="middle"
							filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}
							fill={color}
						>
							↑
						</text>
					</g>
				);
			case 'max':
				return (
					<g
						style={{
							transformOrigin: '50% 50%',
							transform: 'scale(0.9,0.9)',
						}}
					>
						<text
							className={Style.text}
							x="90%"
							y="90%"
							textAnchor="middle"
							alignmentBaseline="middle"
							filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}
							fill={color}
						>
							↓
						</text>
					</g>
				);
			default:
				return (
					<text
						className={Style.arrows}
						x="80%"
						y="70%"
						textAnchor="middle"
						alignmentBaseline="middle"
						filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}
						style={{
							transformOrigin: '50% 50%',
							transform: 'scale(0.8,1)',
						}}
						fill={color}
					>
						↓↑
					</text>
				);
		}
	}

	constructor(props: SVGProtectionIconProps) {
		super(props);
	}

	public render() {
		return (
			<span
				className={[
					this.props.className || '',
					Style.protectionIcon,
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				].join(' ')}
				onClick={this.props.onClick || null}
			>
				{SVGProtectionIcon.getIcon(this.props.type, this.props.kind)}
				{this.props.tooltip && <UITooltip>{this.props.tooltip}</UITooltip>}
			</span>
		);
	}
}

SVGIconContainer.registerSymbol(
	SVGProtectionIconID,
	<symbol viewBox="0 0 31.694 31.694">
		<path
			d="M27.125,3.842c-9.266,0-10.835-3.518-10.849-3.551C16.208,0.119,16.042,0.004,15.85,0
		c0,0-0.002,0-0.006,0c-0.185,0-0.354,0.115-0.426,0.283C15.405,0.32,13.802,3.842,4.569,3.842c-0.257,0-0.462,0.209-0.462,0.461
		v15.764c0,6.453,11.085,11.383,11.553,11.59c0.062,0.027,0.123,0.037,0.188,0.037c0.061,0,0.126-0.01,0.184-0.037
		c0.473-0.207,11.556-5.137,11.556-11.59V4.303C27.587,4.051,27.382,3.842,27.125,3.842z M25.04,19.15
		c0,5.049-8.678,8.912-9.048,9.072c-0.045,0.023-0.097,0.031-0.144,0.031c-0.051,0-0.098-0.008-0.148-0.031
		c-0.365-0.16-9.043-4.023-9.043-9.072V6.811c0-0.199,0.161-0.363,0.361-0.363c7.23,0,8.481-2.756,8.495-2.783
		c0.057-0.133,0.188-0.223,0.334-0.223c0.002,0,0.004,0,0.004,0C16,3.444,16.13,3.534,16.184,3.671
		c0.011,0.025,1.239,2.777,8.494,2.777c0.201,0,0.361,0.164,0.361,0.363C25.039,6.811,25.039,19.15,25.04,19.15z M23.309,19.016
		c0,4.102-7.045,7.234-7.346,7.363c-0.036,0.021-0.079,0.027-0.117,0.027c-0.041,0-0.117-20.141,0-20.141h0.002
		c0.121,0.002,0.229,0.074,0.271,0.186c0.008,0.02,1.006,2.254,6.895,2.254c0.164,0,0.295,0.133,0.295,0.293V19.016z"
		/>
	</symbol>,
);

Object.keys(SVGProtectionSymbolIDs).forEach((kind) =>
	Object.keys(SVGProtectionSymbolIDs[kind]).forEach((type) => {
		SVGIconContainer.registerSymbol(
			SVGProtectionSymbolIDs[kind][type],
			<symbol viewBox={`0 0 140 140`}>
				<SVGSmallTexturedIcon
					style={kind === 'shield' ? TSVGSmallTexturedIconStyles.blue : TSVGSmallTexturedIconStyles.light}
					asSvg={true}
				>
					{kind === 'shield' ? (
						<g style={{ transform: 'scale(0.9) translate(0.3rem, 0.2rem)' }}>
							<use {...SVGIconContainer.getXLinkProps(SVGShieldEntityIconID)} />
						</g>
					) : (
						<use {...SVGIconContainer.getXLinkProps(SVGProtectionIconID)} />
					)}
				</SVGSmallTexturedIcon>
				<g filter={SVGIconContainer.getUrlRef(SVGBasicShadowFilterID)}>
					{SVGProtectionIcon.getReductionTypeSymbol(
						type,
						getTemplateColor(kind === 'shield' ? 'light_golden' : 'light_blue'),
					)}
				</g>
			</symbol>,
		);
	}),
);

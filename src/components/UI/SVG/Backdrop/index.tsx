import * as React from 'react';
import { UIBackdropPatternID, UIUnitBackdropPatternID } from '../../../Symbols/backdrops';
import { getTemplateColor } from '../../index';
import SVGIconContainer, { SVGBasicBevelFilterID } from '../../SVG/IconContainer';
import Style from './index.scss';

export const UIBackdropGradientID = 'ui-backdrop-gradient';
const defaultBackdropOpacity = 0.5;

export interface UIBackdropProps {
	frame?: boolean;
	className?: string;
	opacity?: number;
	gridOpacity?: number;
	gradientId?: string;
	patternId?: string;
}

export default class UIBackdrop extends React.PureComponent<UIBackdropProps, {}> {
	public static getClass(): string {
		return Style.ui_backdrop;
	}

	public render() {
		return (
			<svg
				className={[this.props.className || '', Style.ui_backdrop].join(' ')}
				xmlns="http://www.w3.org/2000/svg">
				<rect
					style={{
						opacity: 0.6,
					}}
					x={0}
					y={0}
					width="100%"
					height="100%"
					fill="#000"
				/>
				<rect
					style={{
						opacity: this.props.opacity || defaultBackdropOpacity,
					}}
					x={0}
					y={0}
					width="100%"
					height="100%"
					fill={SVGIconContainer.getUrlRef(this.props.gradientId || UIBackdropGradientID)}
				/>

				<rect
					style={{
						opacity: Math.max(0.05, this.props.gridOpacity || 0.35),
						mixBlendMode: this.props.patternId === UIUnitBackdropPatternID ? 'soft-light' : 'normal',
					}}
					x={0}
					y={0}
					width="100%"
					height="100%"
					fill={SVGIconContainer.getUrlRef(this.props.patternId || UIBackdropPatternID)}
				/>

				{!this.props.frame && (
					<rect
						x={0}
						y={0}
						width="100%"
						height="100%"
						strokeWidth={2}
						fill="none"
						stroke={getTemplateColor('outline')}
						filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}
					/>
				)}
			</svg>
		);
	}
}

SVGIconContainer.registerGradient(
	UIBackdropGradientID,
	<linearGradient x1="50%" y1="0" x2="75%" y2="100%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor={getTemplateColor('text_dark')} />
		<stop offset="0.3" stopColor={getTemplateColor('secondary_bg')}>
			<animate
				attributeType="XML"
				attributeName="stop-color"
				fill="freeze"
				repeatCount="indefinite"
				dur="7.5s"
				values={[
					getTemplateColor('secondary_bg'),
					getTemplateColor('navy_blue'),
					'#113',
					getTemplateColor('secondary_bg'),
				].join(';')}
			/>
		</stop>
		<stop offset="1" stopColor={getTemplateColor('tooltip_bg')} />
	</linearGradient>,
);

import * as React from 'react';
import { FactionList } from '../../../../definitions/world/factionIndex';
import PoliticalFactions from '../../../../definitions/world/factions';
import { getTemplateColor } from '../index';
import SVGIconContainer from './IconContainer';
import _ from 'underscore';

export const UIFactionGradients: Record<PoliticalFactions.TFactionID, string> = _.mapObject(
	FactionList,
	(faction, factionId) => `ui-${factionId}-grd`,
);
Object.keys(FactionList).forEach((factionId) =>
	SVGIconContainer.registerGradient(
		UIFactionGradients[factionId],
		<linearGradient x1="25%" y1="0" x2="75%" y2="100%" gradientUnits="objectBoundingBox">
			<stop offset="0" stopColor={FactionList[factionId]?.baseColor || getTemplateColor('text_dark')} />
			<stop offset="1" stopColor={FactionList[factionId]?.secondaryColor || getTemplateColor('gray_bg')} />
		</linearGradient>,
	),
);

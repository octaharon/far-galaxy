import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import { ArithmeticPrecision } from '../../../../../definitions/maths/constants';
import SVGTools from '../../../../../definitions/maths/SVGTools';
import digest from '../../../../utils/digest';
import { SVGRefractFilterId } from '../../../PrototypeEditor/PrototypeChassisImage';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import UITooltip from '../../Templates/Tooltip';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGBasicEmbossFilterID,
	SVGBasicShadowFilterID,
	SVGContrastGlowFilterID,
	SVGDarkenFilterID,
	SVGFilterSaturate40ID,
	SVGFractalNoiseFilterID,
	SVGIconStandartViewbox,
} from '../IconContainer';
import styles from './index.scss';

export type IPolarChartSegment = {
	value: number;
	colorInner?: string;
	colorOuter?: string;
	gradientId?: string;
};

export type UISVGPolarChartProps = IUICommonProps & {
	values: IPolarChartSegment[];
	maxScale?: number;
	logarithmic?: number;
	ringValue?: number;
	segmentOutline?: string;
	iconOutline?: string;
	iconId?: string;
};

export const SVGPolarChart_ScaleRadius = 100;
export const SVGPolarChart_IconRadius = 35;
export const SVGPolarChart_RingGradientID = 'svg-polar-ring-gradient';
const SVGPolarChart_RingColor = getTemplateColor('gray_transparent');

export const UISVGPolarChart: React.FC<UISVGPolarChartProps> = ({
	values = [],
	maxScale = 0,
	ringValue = 0,
	iconId = null,
	logarithmic,
	tooltip,
	className,
	iconOutline = getTemplateColor('text_highlight'),
	segmentOutline = getTemplateColor('text_dark'),
}) => {
	const iconGradientId = 'polar-chart-icon-' + digest(iconOutline);
	maxScale = Math.max(
		ringValue > 0
			? Math.ceil(BasicMaths.roundToPrecision(maxScale, ArithmeticPrecision, 3) / ringValue) * ringValue
			: maxScale,
		...(values || []).map((v) => v?.value ?? 0),
	);
	const numRings = ringValue > 0 ? Math.ceil(maxScale / ringValue) : 1;
	const transform = (v: number) =>
		Number.isFinite(logarithmic) && logarithmic > 1
			? BasicMaths.logInterp(logarithmic)({
					x: BasicMaths.normalizeValue({
						x: v,
						min: 0,
						max: maxScale,
					}),
					min: iconId ? SVGPolarChart_IconRadius : 0,
					max: SVGPolarChart_ScaleRadius,
			  })
			: BasicMaths.lerp({
					x: BasicMaths.normalizeValue({
						x: v,
						min: 0,
						max: maxScale,
					}),
					min: iconId ? SVGPolarChart_IconRadius : 0,
					max: SVGPolarChart_ScaleRadius,
			  });
	const segments = values.map((v) => ({
		...v,
		gradientId: v.gradientId || digest(v),
		originalValue: v.value,
		value: transform(Math.abs(v.value)),
	}));
	const rings = numRings ? new Array(numRings).fill(0).map((v, ix, a) => transform((ix + 1) * ringValue)) : [];
	//console.log(ringValue, maxScale, segments, rings);
	const angleStep = (Math.PI * 2) / segments.length;
	const iconSize = 1.2;
	const iconProps = {
		x: -SVGPolarChart_IconRadius * (iconSize / 2 + 0.05),
		y: -SVGPolarChart_IconRadius * (iconSize / 2 + 0.05),
		width: SVGPolarChart_IconRadius * iconSize,
		height: SVGPolarChart_IconRadius * iconSize,
	};
	return (
		<div className={classnames(styles.polarChart, tooltip ? UIContainer.hasTooltipClass : null, className)}>
			<svg
				className={styles.svg}
				viewBox={`-${SVGPolarChart_ScaleRadius} -${SVGPolarChart_ScaleRadius} ${
					SVGPolarChart_ScaleRadius * 2
				} ${SVGPolarChart_ScaleRadius * 2}`}>
				<defs>
					<radialGradient
						id={iconGradientId}
						gradientUnits="userSpaceOnUse"
						cx={0}
						cy={0}
						r={SVGPolarChart_IconRadius}>
						<stop offset="0" stopColor={getTemplateColor('text_dark')} />
						<stop offset="0.8" stopOpacity={0.8} stopColor={getTemplateColor('text_dark')} />
						<stop offset="1" stopColor={iconOutline} stopOpacity={0.8} />
					</radialGradient>
					{segments
						.filter(
							(segment) =>
								!!segment.colorInner && !!segment.colorOuter && Number.isFinite(segment.originalValue),
						)
						.map((segment) => (
							<radialGradient
								id={segment.gradientId}
								key={segment.gradientId}
								gradientUnits="userSpaceOnUse"
								cx="0"
								cy="0"
								r={SVGPolarChart_ScaleRadius}>
								<stop offset="0" stopColor={segment.colorOuter} />
								<stop offset="1" stopColor={segment.colorInner} />
							</radialGradient>
						))}
				</defs>
				<g>
					{rings.reverse().map((r, ix) => (
						<circle
							key={ix}
							cx={0}
							cy={0}
							r={r}
							strokeWidth={2}
							strokeOpacity={0.5}
							strokeDasharray="3 3"
							stroke={SVGPolarChart_RingColor}
							fill={SVGIconContainer.getUrlRef(SVGPolarChart_RingGradientID)}
						/>
					))}
				</g>
				<g {...SVGIconContainer.useFilter(SVGBasicEmbossFilterID)}>
					{segments.map((v, ix) =>
						Number.isFinite(v.originalValue) ? (
							<path
								{...SVGIconContainer.useFilter(
									v.originalValue >= 0 ? SVGBasicBevelFilterID : SVGDarkenFilterID,
								)}
								key={ix}
								d={
									iconId
										? SVGTools.describeRing(
												0,
												0,
												SVGPolarChart_IconRadius,
												v.value,
												ix * angleStep,
												(ix + 1) * angleStep,
										  )
										: SVGTools.describeSector(0, 0, v.value, ix * angleStep, (ix + 1) * angleStep)
								}
								strokeWidth={0.5}
								fill={SVGIconContainer.getUrlRef(v.gradientId)}
								stroke={segmentOutline}
							/>
						) : null,
					)}
				</g>
				<g {...SVGIconContainer.useFilter(SVGBasicShadowFilterID)} style={{ mixBlendMode: 'hard-light' }}>
					{rings.reverse().map((r, ix) => (
						<circle
							key={ix}
							cx={0}
							cy={0}
							r={r}
							strokeWidth={1}
							strokeOpacity={0.3 + 0.4 * (ix / Math.max(1, rings.length - 1))}
							stroke={SVGPolarChart_RingColor}
							fill="transparent"
						/>
					))}
				</g>
				{iconId && (
					<g className={styles.centerIcon}>
						<circle
							cx={0}
							cy={0}
							r={SVGPolarChart_IconRadius}
							fill={SVGIconContainer.getUrlRef(iconGradientId)}
							strokeWidth={2}
							strokeOpacity={0.5}
							stroke={iconOutline}
						/>
						<svg viewBox={SVGIconStandartViewbox} {...iconProps}>
							<use {...SVGIconContainer.getXLinkProps(iconId)} />
						</svg>
					</g>
				)}
			</svg>
			{tooltip && <UITooltip>{tooltip}</UITooltip>}
		</div>
	);
};

SVGIconContainer.registerGradient(
	SVGPolarChart_RingGradientID,
	<radialGradient cx="50%" cy="50%" r="50%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor="black" stopOpacity="0" />
		<stop offset="0.7" stopColor="black" stopOpacity="0" />
		<stop offset="1" stopColor={SVGPolarChart_RingColor} stopOpacity="1" />
	</radialGradient>,
);

export default UISVGPolarChart;

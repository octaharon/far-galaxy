import * as React from 'react';
import DIContainer from '../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../definitions/core/DI/injections';
import { ArithmeticPrecision } from '../../../../../definitions/maths/constants';
import Players from '../../../../../definitions/player/types';
import { wellRounded } from '../../../../utils/wellRounded';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import SVGSegmentedBar, { ISVGSegmentedBarSegment } from '../../SVG/SegmentedBar';
import UITechnologyCaption from '../TechnologyCaption';
import Style from './index.scss';
import IPlayer = Players.IPlayer;
import TPlayerTechnologyPayload = Players.TPlayerTechnologyPayload;

type UITechnologyPayloadDescriptorProps = IUICommonProps & {
	payload: TPlayerTechnologyPayload;
	player?: IPlayer;
};

const getKnowledgeSegments = (bonusValue: number, currentValue: number): ISVGSegmentedBarSegment[] => {
	const filledPercent = currentValue;
	const bonusPercent = Math.min(1 - currentValue, bonusValue);
	const emptySpace = Math.max(0, 1 - filledPercent - bonusPercent);
	const newValueColors = {
		colorTop: getTemplateColor('light_blue'),
		colorBottom: getTemplateColor('light_pink'),
	};
	const currentValueColors = {
		colorTop: getTemplateColor('dark_pink'),
		colorBottom: getTemplateColor('light_purple'),
	};
	if (currentValue >= 1)
		return [
			{
				value: bonusValue * 100,
				colorTop: getTemplateColor('dark_purple'),
				colorBottom: getTemplateColor('light_blue'),
			},
			{
				value: (1 - bonusValue) * 100,
				...currentValueColors,
			},
		];
	return [
		currentValue > ArithmeticPrecision
			? {
					value: currentValue * 100,
					...currentValueColors,
			  }
			: null,
		{
			value: bonusPercent * 100,
			...newValueColors,
			blink: true,
		},
		emptySpace > ArithmeticPrecision
			? {
					value: emptySpace * 100,
					colorTop: getTemplateColor('text_dark'),
					colorBottom: getTemplateColor('tooltip_bg'),
			  }
			: null,
	].filter(Boolean);
};

export default class UITechnologyPayloadDescriptor extends React.PureComponent<UITechnologyPayloadDescriptorProps, {}> {
	public render() {
		return (
			<dl
				className={classnames(
					Style.payloadDescriptor,
					this.props.className || '',
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				)}
			>
				{Object.keys(this.props.payload).reduce(
					(list, technologyType) =>
						list.concat(
							(Object.keys(this.props.payload[technologyType]) || [])
								.map((technologyId) =>
									technologyType === 'chassis' &&
									this.props.player &&
									!DIContainer.getProvider(
										DIInjectableCollectibles.chassis,
									).isChassisAvailableToFaction(
										DIContainer.getProvider(DIInjectableCollectibles.chassis).get(technologyId),
										this.props.player.faction,
									) ? null : (
										<React.Fragment key={`${technologyType}-${technologyId}`}>
											<dt>
												<UITechnologyCaption
													technologyType={technologyType}
													technologyId={technologyId}
												/>
											</dt>
											<dd className={Style.payloadValue}>
												{wellRounded(this.props.payload[technologyType][technologyId] * 100, 0)}
												%&nbsp;
												<SVGSegmentedBar
													width={80}
													segments={getKnowledgeSegments(
														this.props.payload[technologyType][technologyId],
														this.props.player?.arsenal?.[technologyType]?.[technologyId] ??
															0,
													)}
												/>
											</dd>
										</React.Fragment>
									),
								)
								.filter(Boolean),
						),
					[] as JSX.Element[],
				)}
			</dl>
		);
	}
}

import * as React from 'react';
import TalentRegistry from '../../../../../definitions/player/Talents/TalentRegistry';
import { TPlayerTalentDefinition } from '../../../../../definitions/player/Talents/types';
import Players from '../../../../../definitions/player/types';
import { wellRounded } from '../../../../utils/wellRounded';
import { completeIcon, progressIcon } from '../../../Symbols/commonIcons';
import { classnames, getTemplateColor, IUICommonProps } from '../../index';
import UIPrototypeModifier from '../../Prototype/PrototypeModifier';
import UIRankCaption from '../../Prototype/Rank';
import UIXPCaption from '../../Prototype/XP';
import SVGPercentBar from '../../SVG/PercentBar';
import Tokenizer from '../../Templates/Tokenizer';
import UITalentCaption from '../TalentCaption';
import UITechnologyPayloadDescriptor from '../TechnologyPayloadDescriptor';
import Style from './index.scss';
import TalentBgTexture from './texture';
import IPlayer = Players.IPlayer;

export type UITalentDescriptorProps = IUICommonProps & {
	talent: TPlayerTalentDefinition;
	player?: IPlayer;
	x?: number;
	y?: number;
	active?: boolean;
};

export const UITalentDescriptor: React.FC<UITalentDescriptorProps> = ({
	tooltip,
	talent,
	player = null,
	x,
	y,
	className,
}) => {
	const active = player?.talentPoints && !player?.isTalentLearned(talent.id) && player?.isTalentAvailable(talent.id);
	const learned = player?.isTalentLearned(talent.id);
	const cpt = talent.caption.split('(')[0].trim();
	const meta = (talent.caption.split('(')[1] || '').replace(')', '').trim();
	return (
		<div
			className={classnames(
				Style.talentCard,
				active ? Style.active : null,
				learned ? Style.learned : null,
				className,
			)}
			style={
				x * y > 0
					? {
							position: 'absolute',
							left: x + 'px',
							top: y + 'px',
					  }
					: {}
			}
		>
			<div className={Style.talentBg}>
				<TalentBgTexture active={active} />
			</div>
			<div className={Style.talentContent}>
				<dl className={Style.talentDescriptor}>
					<dt className={Style.talentCaption}>
						{UITalentCaption.getIcon(player.talents?.[talent.id]?.learned)}
						{cpt}
					</dt>
					<dd className={Style.levelReq}>
						{!learned && !!talent.levelReq && (
							<UIXPCaption level={true} value={talent.levelReq || null} tooltip={tooltip} />
						)}
					</dd>
					<dt className={Style.talentProgress}>
						{player.isTalentLearned(talent.id) ? completeIcon : progressIcon}
						{talent.description}
					</dt>
					{!!talent.achievement && !player.isTalentLearned(talent.id) && (
						<>
							<dt className={Style.progressBar}>
								<SVGPercentBar
									className={Style.svgBar}
									strokeColor={getTemplateColor('dark_green')}
									value={talent.achievement(player) || 0}
									colorTop={getTemplateColor('light_teal')}
									colorBottom={getTemplateColor('light_blue')}
								/>
							</dt>
							<dd className={Style.progressValue}>{wellRounded(talent.achievement(player) * 100, 0)}%</dd>
						</>
					)}
				</dl>
				<div className={Style.talentLore}>
					<Tokenizer description={meta} />
				</div>
				{!!talent.technologyReward && !player.isTalentLearned(talent.id) && (
					<>
						<hr />
						<UITechnologyPayloadDescriptor payload={talent.technologyReward} player={player} />
					</>
				)}
			</div>
		</div>
	);
};

import * as React from 'react';
import romanize from '../../../../utils/romanize';
import { UITalentIcon } from '../../../Symbols/commonIcons';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import SVGIconContainer from '../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../SVG/SmallTexturedIcon';
import UITooltip from '../../Templates/Tooltip';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import Style from './index.scss';

type UIRankCaptionProps = IUICommonProps & {
	value?: number;
	highlight?: boolean;
};

export default class UITalentCaption extends React.PureComponent<UIRankCaptionProps, {}> {
	public static getIcon(highlight?: boolean, className: string = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon
				style={highlight ? TSVGSmallTexturedIconStyles.gold : TSVGSmallTexturedIconStyles.pink}
				className={className}
			>
				<use {...SVGIconContainer.getXLinkProps(UITalentIcon)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption(point = false) {
		return point ? 'Talent Points' : 'Talent';
	}

	public static getCaptionClass() {
		return Style.talentCaption;
	}

	public static getTooltip() {
		return termTooltipTemplate(
			UITalentCaption.getIcon(false, UITooltip.getIconClass()),
			UITalentCaption.getCaption(),
			'As a Player progresses through game and Levels, they may obtain bonuses from Talents, which are applied forever, and each of them can only be gained once. Most Talents can be acquired in exchange for Talent Points, but not all of them',
		);
	}

	public static getEntityCaption(point = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UITalentCaption.getIcon(point)}
				<span className={UITalentCaption.getCaptionClass()}>{UITalentCaption.getCaption(point)}</span>
			</em>
		);
	}

	public render() {
		return (
			<span
				className={classnames(
					this.props.className || '',
					UIContainer.captionClass,
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				)}
			>
				{UITalentCaption.getIcon(this.props.highlight, Style.talentIcon)}
				{!!this.props.highlight && (
					<span className={Style.talentCaptionHighlight}>{UITalentCaption.getCaption()} </span>
				)}
				{!!this.props.value && (
					<span className={this.props.highlight ? Style.talentHighlight : Style.talentValue}>
						{romanize(this.props.value)}
					</span>
				)}
				{!!this.props.tooltip && <UITooltip>{UITalentCaption.getTooltip()}</UITooltip>}
			</span>
		);
	}
}

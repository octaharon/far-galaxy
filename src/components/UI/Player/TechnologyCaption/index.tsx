import * as React from 'react';
import DIContainer from '../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../definitions/core/DI/injections';
import Players from '../../../../../definitions/player/types';
import UIBuildingTypeDescriptor from '../../Base/Buildings/BuildingTypeDescriptor';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import UIArmorGroupDescriptor from '../../Parts/Armor/ArmorGroupDescriptor';
import UIArmorSlotDescriptor from '../../Parts/Armor/ArmorSlotDescriptor';
import UIChassisClassDescriptor from '../../Parts/Chassis/ChassisClassDescriptor';
import UIEquipmentSlotDescriptor from '../../Parts/Equipment/EquipmentSlotDescriptor';
import UIShieldGroupDescriptor from '../../Parts/Shield/ShieldGroupDescriptor';
import UIShieldSlotDescriptor from '../../Parts/Shield/ShieldSlotDescriptor';
import UIWeaponGroupDescriptor from '../../Parts/Weapon/WeaponGroupDescriptor';
import UITooltip from '../../Templates/Tooltip';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import Style from './index.scss';
import TPlayerTechnologyPayload = Players.TPlayerTechnologyPayload;

type UITechnologyCaptionProps = IUICommonProps & {
	technologyType?: keyof TPlayerTechnologyPayload;
	technologyId?: number;
};

export default class UITechnologyCaption extends React.PureComponent<UITechnologyCaptionProps, {}> {
	public static getEntity(technologyType: keyof TPlayerTechnologyPayload, technologyId: number) {
		switch (technologyType) {
			case 'armor':
				return DIContainer.getProvider(DIInjectableCollectibles.armor).get(technologyId);
			case 'shield':
				return DIContainer.getProvider(DIInjectableCollectibles.shield).get(technologyId);
			case 'weapons':
				return DIContainer.getProvider(DIInjectableCollectibles.weapon).get(technologyId);
			case 'chassis':
				return DIContainer.getProvider(DIInjectableCollectibles.chassis).get(technologyId);
			case 'equipment':
				return DIContainer.getProvider(DIInjectableCollectibles.equipment).get(technologyId);
			case 'establishments':
				return DIContainer.getProvider(DIInjectableCollectibles.establishment).get(technologyId);
			case 'factories':
				return DIContainer.getProvider(DIInjectableCollectibles.factory).get(technologyId);
			case 'facilities':
				return DIContainer.getProvider(DIInjectableCollectibles.facility).get(technologyId);
			default:
				return null;
		}
	}

	public static getCaptionClass(technologyType: keyof TPlayerTechnologyPayload) {
		switch (technologyType) {
			case 'armor':
				return UIArmorGroupDescriptor.getCaptionClass();
			case 'shield':
				return UIShieldGroupDescriptor.getCaptionClass();
			case 'chassis':
				return UIChassisClassDescriptor.getCaptionClass();
			case 'equipment':
				return UIEquipmentSlotDescriptor.getCaptionClass();
			case 'weapons':
				return UIWeaponGroupDescriptor.getCaptionClass();
			default:
				return UIBuildingTypeDescriptor.getCaptionClass();
		}
	}

	public static getIcon(
		technologyType: keyof TPlayerTechnologyPayload,
		technologyId: number,
		className: string = null,
	) {
		switch (technologyType) {
			case 'weapons':
				return UIWeaponGroupDescriptor.getIcon(
					DIContainer.getProvider(DIInjectableCollectibles.weapon).get(technologyId).groupType,
					className,
				);
			case 'armor':
				return (
					<UIArmorSlotDescriptor
						className={className}
						slotType={
							DIContainer.getProvider(DIInjectableCollectibles.armor).getItemMeta(technologyId).slot
						}
					/>
				);
			case 'shield':
				return (
					<UIShieldSlotDescriptor
						className={className}
						slotType={
							DIContainer.getProvider(DIInjectableCollectibles.shield).getItemMeta(technologyId).slot
						}
					/>
				);
			case 'equipment':
				return (
					<UIEquipmentSlotDescriptor
						group={
							DIContainer.getProvider(DIInjectableCollectibles.equipment).getItemMeta(technologyId).group
						}
						className={className}
					/>
				);
			case 'chassis':
				return UIChassisClassDescriptor.getIcon(
					DIContainer.getProvider(DIInjectableCollectibles.chassis).get(technologyId).type,
					className,
				);
			case 'establishments':
				return UIBuildingTypeDescriptor.getIcon(
					DIContainer.getProvider(DIInjectableCollectibles.establishment).get(technologyId).type,
					className,
				);
			case 'facilities':
				return UIBuildingTypeDescriptor.getIcon(
					DIContainer.getProvider(DIInjectableCollectibles.facility).get(technologyId).type,
					className,
				);
			case 'factories':
				return UIBuildingTypeDescriptor.getIcon(
					DIContainer.getProvider(DIInjectableCollectibles.factory).get(technologyId).type,
					className,
				);
			default:
				return null;
		}
	}

	public static getTooltip(technologyType: keyof TPlayerTechnologyPayload, technologyId: number) {
		return termTooltipTemplate(
			UITechnologyCaption.getIcon(technologyType, technologyId, UITooltip.getIconClass()),
			UITechnologyCaption.getEntity(technologyType, technologyId).caption,
			UITechnologyCaption.getEntity(technologyType, technologyId).description,
		);
	}

	public render() {
		return (
			<span
				className={classnames(
					this.props.className || '',
					UIContainer.captionClass,
					Style.technologyCaption,
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				)}
			>
				{UITechnologyCaption.getIcon(this.props.technologyType, this.props.technologyId, Style.technologyIcon)}
				<span className={UITechnologyCaption.getCaptionClass(this.props.technologyType)}>
					{UITechnologyCaption.getEntity(this.props.technologyType, this.props.technologyId).caption}
				</span>
				{!!this.props.tooltip && (
					<UITooltip>
						{UITechnologyCaption.getTooltip(this.props.technologyType, this.props.technologyId)}
					</UITooltip>
				)}
			</span>
		);
	}
}

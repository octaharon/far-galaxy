import * as React from 'react';
import _ from 'underscore';
import WebFont from 'webfontloader';
import { getFaction } from '../../../definitions/world/factionIndex';
import PoliticalFactions from '../../../definitions/world/factions';
import GlobalStyle from '../../sass/index.scss';
import { INFINITY_SYMBOL } from '../../utils/wellRounded';
import TemplateStyle from './index.scss';

export function getTemplateColor(name: string): string {
	return TemplateStyle[`color-${name}`] || '#FFF';
}

export function getFactionClass(factionID: PoliticalFactions.TFactionID = null): string {
	return TemplateStyle[
		`faction-${getFaction(factionID || PoliticalFactions.TFactionID.none)?.captionShort?.toLowerCase()}`
	];
}

export function getFactionColor(factionID: PoliticalFactions.TFactionID = null, type: 'primary' | 'secondary'): string {
	if (!factionID) return type === 'primary' ? getTemplateColor('text_highlight') : getTemplateColor('light_gray');
	return (
		TemplateStyle[
			`faction-${type}-${getFaction(factionID || PoliticalFactions.TFactionID.none).captionShort.toLowerCase()}`
		] || getTemplateColor('golden')
	);
}

export const MetaHeader: React.FC<React.PropsWithChildren<{ className?: string; rightElement?: JSX.Element }>> = ({
	children,
	className = TemplateStyle.meta,
	rightElement = null,
}) => (
	<dl className={className}>
		<dt className={classnames(TemplateStyle.descriptor, TemplateStyle.metaHeader)}>{children}</dt>
		{rightElement ? <dd>{rightElement}</dd> : null}
	</dl>
);

/**
 * Deduplicate class names and build a string of them
 * @param classNames
 */
export const classnames = (...classNames: string[]) =>
	_.uniq((classNames || []).filter(Boolean).reduce((arr, cn) => arr.concat(cn.split(' ')), []))
		.map((t) => t.trim())
		.filter(Boolean)
		.join(' ');

export const UI_SYMBOLS: Record<string, string> = {
	Infinity: INFINITY_SYMBOL,
	GiveOrTake: '±',
};

export type IUICommonProps = React.PropsWithChildren<{
	className?: string;
	tooltip?: boolean | string | React.ReactNode | JSX.Element;
	onClick?: React.MouseEventHandler;
}>;

document.addEventListener('DOMContentLoaded', function (event) {
	WebFont.load({
		active: () => {
			console.warn('Fonts loaded');
		},
		google: {
			families: [
				`${TemplateStyle['font-serif']}:400,700:latin,cyrillic-ext`,
				`${TemplateStyle['font-main']}:400,700:latin,cyrillic-ext`,
				`${TemplateStyle['font-caption']}:300,800:latin,cyrillic-ext`,
			],
		},
	});
	console.warn('DOM fully loaded and parsed');
});

const UIContainer = {
	UIIncluded: true,
	GeneralStyles: GlobalStyle,
	hasTooltipClass: TemplateStyle.__hasTooltip || '__hasTooltip',
	tooltipClass: TemplateStyle.tooltipGlobal || 'tooltipGlobal',
	tooltipIconClass: TemplateStyle.tooltipIcon || 'tooltipIcon',
	descriptorClass: TemplateStyle.descriptor || 'descriptor',
	descriptorGroupClass: TemplateStyle.descriptorGroup || 'descriptorGroup',
	iconClass: TemplateStyle.inlineIcon || 'inlineIcon',
	captionClass: TemplateStyle.captionWithIcon || 'captionWithIcon',
	captionWithoutIconClass: TemplateStyle.captionWithoutIcon || 'captionWithoutIcon',
	panelClass: TemplateStyle.__panel || '__panel',
	preloaderClass: 'preloader-img',
};

// console.log(UIContainer);
export default UIContainer;

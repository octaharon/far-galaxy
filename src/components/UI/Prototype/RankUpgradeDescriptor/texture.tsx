// eslint-disable-next-line max-classes-per-file
import { Canvas, extend, ReactThreeFiber, useFrame } from '@react-three/fiber';
import React from 'react';
import * as THREE from 'three';
import { TWithGraphicSettingsProps, withGraphicSettings } from '../../../../contexts/GraphicSettings';
import { doesQualitySettingIncludes } from '../../../../contexts/GraphicSettings/helpers';
import EnhancedMaterial from '../../../../utils/three-texture';
import { classnames, IUICommonProps } from '../../index';
import Style from './index.scss';

type TTextureProps = IUICommonProps & TWithGraphicSettingsProps & { active?: boolean };
const planeCoords: ConstructorParameters<typeof THREE.PlaneGeometry> = [2048, 512, 3, 3];
const uniforms = {
	uTime: { value: 0 },
	uGridSize: { value: [9, 3] },
	uDistortion: { value: [0.2, 0.12] },
	uPhase: {
		value: Math.random() * 10,
	},
	uSpeed: {
		value: Math.random() * 0.5 + 0.8,
	},
};

class RankHoverTexture extends EnhancedMaterial {
	constructor() {
		super({
			uniforms: {
				...uniforms,
				uPhase: {
					value: Math.random() * 10,
				},
				uSpeed: {
					value: Math.random() * 0.1 + 0.9,
				},
			},
			varyingParameters: [
				`
        varying vec2 vUv;
      `,
			],
			vertexPosition: [
				`
        vUv = uv;
      `,
			],
			fragmentFunctions: [
				`
        struct Voronoi {
          float distance;
          vec2 point;
          vec2 center;
        };

        vec2 distort (in vec2 st, vec2 distortion, float time) {
          vec2 p = st * 2.0 - 1.0;
          return vec2(
            p.x + sin(p.y * PI + time) * distortion.x,
            p.y + sin(p.x * PI + time) * distortion.y
          ) / 2.0 - st + vec2(0.5);
        }

        vec2 random2 (vec2 p) {
          return fract(sin(vec2(dot(p, vec2(127.1, 311.7)), dot(p, vec2(269.5, 183.3)))) * 43758.5453);
        }

        vec2 getCellPoint (vec2 gridCell, float time) {
          vec2 rand = random2(gridCell);
          return 0.5 + 0.5 * vec2(sin(time + 6.2831 * rand));
        }

        Voronoi voronoiDistance (in vec2 st, float time) {
          vec2 stI = floor(st);
          vec2 stF = fract(st);
          float minDist = 8.0;
          vec2 minPoint;
          vec2 minNeighbor;
          for (int y = -1; y <= 1; y++) {
            for (int x = -1; x <= 1; x++) {
              vec2 neighbor = vec2(float(x), float(y));
              vec2 cellPoint = getCellPoint(stI + neighbor, time);
              vec2 point = neighbor + cellPoint - stF;
              float dist = dot(point, point);
              if (dist < minDist) {
                minDist = dist;
                minPoint = point;
                minNeighbor = neighbor;
              }
            }
          }
          minDist = 1.0;
          for (int y = -2; y <= 2; y++) {
            for (int x = -2; x <= 2; x++) {
              vec2 neighbor = minNeighbor + vec2(float(x), float(y));
              vec2 cellPoint = getCellPoint(stI + neighbor, time);
              vec2 point = neighbor + cellPoint - stF;
              float dist = dot(0.5 * (minPoint + point), normalize(point - minPoint));
              minDist = min(minDist, dist);
            }
          }
          vec2 currentPoint = minPoint - minNeighbor + stF;
          vec2 center = stI + minPoint + stF;
          return Voronoi(minDist, currentPoint, center);
        }
      `,
			],
			fragmentParameters: [
				`
        uniform float uTime;
        uniform vec2 uGridSize;
        uniform vec2 uDistortion;
      `,
			],
			fragmentDiffuse: [
				`
        vec2 size = vec2(uGridSize);
        vec2 st = vUv * size;
        vec2 p = st;
        st = st + distort(st, uDistortion, uTime);
        Voronoi voronoi = voronoiDistance(st, uTime);
        diffuseColor.rgb = vec3(0,0,0);
        diffuseColor.g += 0.2 * voronoi.distance; // distance gradient
        diffuseColor.rb += 1.1 * voronoi.center / uGridSize; // cell coloring
        diffuseColor.rb += abs(cos(12.0 * voronoi.distance)) * 0.1; // distance circles
        //diffuseColor.r -= 0.1 - smoothstep(0.001, 0.4, voronoi.distance); // cell borders
      `,
			],
		});
	}
}

const shaderSample = new RankHoverTexture();
const shaders = {
	vertex: shaderSample.vertexShader,
	fragment: shaderSample.fragmentShader,
};

const FillTexture = RankHoverTexture;

extend({ FillTexture });
declare global {
	namespace JSX {
		interface IntrinsicElements {
			fillTexture: ReactThreeFiber.Object3DNode<
				THREE.ShaderMaterial,
				ConstructorParameters<typeof THREE.ShaderMaterial>
			>;
		}
	}
}

const textureCoords = new THREE.Vector3(0, 0, 0);

const AnimatedTexture: React.FC<{ anim?: boolean; active?: boolean }> = ({ anim = true, active = true }) => {
	const materialRef = React.useRef();
	useFrame(({ clock }) => {
		//console.log(materialRef.current);
		if (!anim || !materialRef?.current) return false;

		const t = materialRef.current as THREE.ShaderMaterial;
		t.uniforms.uTime.value =
			clock.getElapsedTime() * (active ? 0.85 : 0.25) * t.uniforms.uSpeed.value + t.uniforms.uPhase.value;
	});
	return (
		<mesh position={textureCoords}>
			<fillTexture ref={materialRef} attach={'material'} />
			{/*<meshBasicMaterial color="royalblue" opacity={0.25} />*/}
			<planeGeometry args={planeCoords} />
		</mesh>
	);
};
const camPosition = new THREE.Vector3(0, 0, 268);
const camOptions = { fov: 70, near: 100, far: 10000, position: camPosition, aspect: 3 };

class RankBgTexture extends React.PureComponent<TTextureProps, never> {
	render() {
		const { className, graphicSettings, active = false } = this.props;
		if (!doesQualitySettingIncludes(graphicSettings?.qualityLevel, 'Med')) return null;
		return (
			<Canvas className={classnames(Style.upgradeBgTexture, className)} camera={camOptions}>
				<ambientLight color={0xd0ff00} />
				<AnimatedTexture
					anim={doesQualitySettingIncludes(graphicSettings?.qualityLevel, 'Max')}
					active={active}
				/>
			</Canvas>
		);
	}
}

export default withGraphicSettings(RankBgTexture);

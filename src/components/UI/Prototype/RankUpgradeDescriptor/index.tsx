import * as React from 'react';
import UnitTalents from '../../../../../definitions/tactical/units/levels/unitTalents';
import { classnames, IUICommonProps } from '../../index';
import Tokenizer from '../../Templates/Tokenizer';
import UIRankCaption from '../Rank';
import Style from './index.scss';
import RankBgTexture from './texture';

export type UIRankUpgradeDescriptorProps = IUICommonProps & {
	talent: UnitTalents.IUnitTalentTreeItem;
	active?: boolean;
};

export const UIURankUpgradeDescriptor: React.FC<UIRankUpgradeDescriptorProps> = ({
	tooltip,
	talent,
	className,
	active = true,
}) => {
	return (
		<div className={classnames(Style.upgradeCard, active ? Style.active : null, className)}>
			<div className={Style.upgradeBg}>
				<RankBgTexture active={active} />
			</div>
			<div className={Style.upgradeContent}>
				<dl className={Style.upgradeDescriptor}>
					<dt className={Style.upgradeCaption}>{talent.talent.caption}</dt>
					<dd className={Style.upgradeRank}>
						{!active && <UIRankCaption value={talent.talent.minimumLevel} tooltip={tooltip} />}
					</dd>
				</dl>
				<div className={Style.upgradeLore}>
					<Tokenizer description={talent.talent.description} />
				</div>
			</div>
		</div>
	);
};

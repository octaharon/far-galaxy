import * as React from 'react';
import _ from 'underscore';
import { isEmptyProtectionContainer } from '../../../../../definitions/core/helpers';
import { IPrototypeModifier } from '../../../../../definitions/prototypes/types';
import { stackWeaponGroupModifiers } from '../../../../../definitions/technologies/weapons/WeaponManager';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import UIAbilityPowerDescriptor from '../../Parts/Ability/AbilityPowerDescriptor';
import UICooldownDescriptor from '../../Parts/Ability/CooldownDescriptor';
import UIDurationDescriptor, {
	DURATION_TYPE_ABILITY,
	DURATION_TYPE_RESISTANCE,
} from '../../Parts/Ability/DurationDescriptor';
import UIArmorGroupDescriptor from '../../Parts/Armor/ArmorGroupDescriptor';
import UIAttackRangeDescriptor from '../../Parts/Attack/RangeDescriptor';
import UIActionDescriptor from '../../Parts/Chassis/ActionDescriptor';
import UIHitPointsDescriptor from '../../Parts/Chassis/HitPointsDescriptor';
import UIScanRangeDescriptor from '../../Parts/Chassis/ScanRangeDescriptor';
import UISpeedDescriptor from '../../Parts/Chassis/SpeedDescriptor';
import UIDamageProtectionDescriptor from '../../Parts/Defense/DamageProtectionDescriptor';
import UIDefenseFlagsDescriptor from '../../Parts/Defense/DefenseFlagsDescriptor';
import UIDodgeModifierDescriptor from '../../Parts/Defense/DodgeModifierDescriptor';
import UIShieldCapacityModifierDescriptor from '../../Parts/Shield/ShieldCapacityModifierDescriptor';
import UIShieldGroupDescriptor from '../../Parts/Shield/ShieldGroupDescriptor';
import UIShieldOverchargeDescriptor from '../../Parts/Shield/ShieldOverchargeDescriptor';
import UIWeaponModifierDescriptor from '../../Parts/Weapon/WeaponModifierDescriptor';
import UIActionCounter from '../ActionCount';
import UIUnitCostDescriptor from '../UnitCostDescriptor';
import Style from './index.scss';

export type UIHealthModifierDescriptorProps = IUICommonProps & {
	modifier: IPrototypeModifier;
};

const LocalCostDescriptor: React.FC<{ modifier: IPrototypeModifier }> = ({ modifier }) => {
	const weaponCostMod = stackWeaponGroupModifiers(...(modifier?.weapon || []));
	// that's a cheaty way to look up a field presence. May break if at some point the field names are changed
	if (!JSON.stringify(_.omit(modifier, 'chassis', 'baseCost')).includes('baseCost')) return null;
	const isDefaultWeaponMod = Object.keys(weaponCostMod).length === 1 && !!weaponCostMod.default;
	return (
		<div className={classnames(Style.groupCostDescriptor, Style.descriptorGroup)}>
			<UIUnitCostDescriptor
				value={null}
				modifier={null}
				className={UIContainer.captionWithoutIconClass}
				tooltip={true}
			/>
			<dl className={Style.groups}>
				{Object.keys(weaponCostMod)
					.filter((wg) => !!weaponCostMod[wg].baseCost)
					.sort((a, b) => (a === 'default' ? 1 : -1))
					.map((wg) => (
						<React.Fragment key={wg}>
							<dt className={classnames(UIContainer.descriptorClass)}>
								{UIWeaponModifierDescriptor.getGroupCaption(wg, isDefaultWeaponMod)}
							</dt>
							<dd className={UIContainer.descriptorClass}>
								<UIUnitCostDescriptor
									modifier={weaponCostMod[wg].baseCost}
									short={true}
									tooltip={false}
								/>
							</dd>
						</React.Fragment>
					))}
				{modifier?.armor
					?.filter((a) => !!a.baseCost)
					.map((a, ix) => (
						<React.Fragment key={ix}>
							<dt className={classnames()}>
								<UIArmorGroupDescriptor group={null} tooltip={true} />
							</dt>
							<dd className={UIContainer.descriptorClass}>
								<UIUnitCostDescriptor
									modifier={a.baseCost}
									short={true}
									totalCost={false}
									tooltip={true}
								/>
							</dd>
						</React.Fragment>
					))}
				{modifier?.shield
					?.filter((a) => !!a.baseCost)
					.map((a, ix) => (
						<React.Fragment key={ix}>
							<dt className={classnames()}>
								<UIShieldGroupDescriptor group={null} tooltip={true} />
							</dt>
							<dd className={UIContainer.descriptorClass}>
								<UIUnitCostDescriptor
									modifier={a.baseCost}
									short={true}
									totalCost={false}
									tooltip={true}
								/>
							</dd>
						</React.Fragment>
					))}
			</dl>
		</div>
	);
};

export default class UIPrototypeModifier extends React.PureComponent<UIHealthModifierDescriptorProps, {}> {
	public static getCaption() {
		return 'Prototype Modifier';
	}

	public render() {
		const { modifier } = this.props;
		return (
			<div
				className={classnames(Style.prototypeDescriptor, this.props.className || '')}
				onClick={this.props.onClick || null}
			>
				{!!modifier?.unitModifier?.abilityPowerModifier &&
					modifier.unitModifier.abilityPowerModifier.map((m, ix) => (
						<UIAbilityPowerDescriptor
							key={`abilityPower-${ix}`}
							modifier={m}
							tooltip={true}
							isPlayer={false}
						/>
					))}
				{!!modifier?.unitModifier?.abilityRangeModifier &&
					modifier.unitModifier.abilityRangeModifier.map((m, ix) => (
						<UIAttackRangeDescriptor
							key={`abilityRange-${ix}`}
							modifier={m}
							tooltip={true}
							isWeapon={false}
						/>
					))}
				{!!modifier?.unitModifier?.abilityCooldownModifier &&
					modifier.unitModifier.abilityCooldownModifier.map((m, ix) => (
						<UICooldownDescriptor key={`abilityCd-${ix}`} modifier={m} tooltip={true} />
					))}
				{!!modifier?.unitModifier?.abilityDurationModifier &&
					modifier.unitModifier.abilityDurationModifier.map((m, ix) => (
						<UIDurationDescriptor
							key={`duration-${ix}`}
							type={DURATION_TYPE_ABILITY}
							modifier={m}
							tooltip={true}
						/>
					))}
				{!!modifier?.unitModifier?.statusDurationModifier &&
					modifier.unitModifier.statusDurationModifier.map((m, ix) => (
						<UIDurationDescriptor
							key={`resistance-${ix}`}
							type={DURATION_TYPE_RESISTANCE}
							modifier={m}
							tooltip={true}
						/>
					))}
				{!!modifier?.shield &&
					modifier.shield.map((m, ix) => (
						<React.Fragment key={`shield-${ix}`}>
							{!!m?.overchargeDecay && <UIShieldOverchargeDescriptor modifier={m.overchargeDecay} />}
							{!!m?.shieldAmount && (
								<UIShieldCapacityModifierDescriptor modifier={m.shieldAmount} tooltip={true} />
							)}
							{!isEmptyProtectionContainer(m) && (
								<UIDamageProtectionDescriptor
									protection={m}
									kind={'shield'}
									short={false}
									className={Style.descriptorGroup}
								/>
							)}
						</React.Fragment>
					))}
				{!!modifier?.armor &&
					modifier.armor.map((m, ix) => (
						<React.Fragment key={`armor-${ix}`}>
							{!isEmptyProtectionContainer(m) && (
								<UIDamageProtectionDescriptor
									kind={'armor'}
									protection={m}
									short={false}
									className={Style.descriptorGroup}
								/>
							)}
						</React.Fragment>
					))}

				{!!modifier?.weapon &&
					modifier.weapon.map((m, ix) => (
						<UIWeaponModifierDescriptor
							modifier={m}
							key={`weapon-${ix}`}
							hideCost={true}
							groupClassName={Style.descriptorGroup}
						/>
					))}
				{!!modifier?.chassis &&
					modifier.chassis.map((m, ix) => (
						<React.Fragment key={`chassis-${ix}`}>
							{!!m?.defenseFlags && (
								<UIDefenseFlagsDescriptor
									flags={m.defenseFlags}
									className={Style.metaCaption}
									tooltip={true}
								/>
							)}
							{!!m?.dodge && <UIDodgeModifierDescriptor tooltip={true} dodgeModifier={m.dodge} />}
							{!!m?.speed && <UISpeedDescriptor modifier={m.speed} tooltip={true} />}
							{!!m?.scanRange && <UIScanRangeDescriptor modifier={m.scanRange} tooltip={true} />}
							{!!m?.hitPoints && <UIHitPointsDescriptor modifier={m.hitPoints} tooltip={true} />}
							{!!m.actionCount && (
								<UIActionCounter
									actions={null}
									modifier={m.actionCount}
									iconAtCaption={true}
									tooltip={true}
								/>
							)}
							{!!m.actions && <UIActionDescriptor actions={m.actions} tooltip={true} />}
						</React.Fragment>
					))}
				<LocalCostDescriptor modifier={modifier} />
				{!!modifier?.chassis &&
					modifier.chassis
						.filter((v) => !!v.baseCost)
						.map((m, ix) => (
							<UIUnitCostDescriptor modifier={m.baseCost} tooltip={true} totalCost={true} key={ix} />
						))}
			</div>
		);
	}
}

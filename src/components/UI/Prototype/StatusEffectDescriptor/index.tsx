import * as React from 'react';
import Units from '../../../../../definitions/tactical/units/types';
import { IStatusEffect } from '../../../../../definitions/tactical/statuses/types';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import UIDurationDescriptor from '../../Parts/Ability/DurationDescriptor';
import UIDodecahedron from '../../Templates/Dodecahedron';
import ParticlesFactory from '../../Templates/ParticleTexture';
import Tokenizer from '../../Templates/Tokenizer';
import UITooltip from '../../Templates/Tooltip';
import Style from './index.scss';

export type UIStatusEffectDescriptorProps = IUICommonProps & {
	effect: IStatusEffect<any>;
	target?: Units.IUnit;
};

export const UIStatusEffectDescriptor: React.FC<UIStatusEffectDescriptorProps> = ({
	tooltip,
	effect,
	target,
	className,
}) => {
	const bgRef = React.useRef(null);
	const colors = [
		[
			getTemplateColor('light_blue'),
			getTemplateColor('light_green'),
			getTemplateColor('text_highlight'),
			getTemplateColor('light_golden'),
		],
		[
			getTemplateColor('light_purple'),
			getTemplateColor('light_pink'),
			getTemplateColor('light_orange'),
			getTemplateColor('text_highlight'),
		],
	];
	const effectId = effect.getInstanceId();
	React.useEffect(() => {
		const objectCache = {} as Record<string, any>;
		objectCache.particles = new ParticlesFactory(bgRef.current, {
			numParticles: 50,
			minRadius: 0.25,
			maxRadius: 1,
			connectionRange: 30,
			colorAlpha: 0.8,
			minSpeedY: -1,
			maxSpeedY: -0.2,
			minSpeedX: -1,
			maxSpeedX: 1,
			colors: colors[effect.static.negative ? 1 : 0],
		});
		return () => {
			objectCache.particles?.destroy();
			delete objectCache.particles;
		};
	}, [effect.duration, effect.meta, effectId]);
	return (
		<div
			className={classnames(
				Style.effectCard,
				effect.static.negative ? Style.negative : Style.positive,
				className,
			)}
		>
			<div className={Style.bg} ref={bgRef} />
			<div className={classnames(Style.content, tooltip ? UIContainer.hasTooltipClass : null)}>
				<dl className={Style.effectDescriptor}>
					<dt className={classnames(UIContainer.captionClass, UIContainer.descriptorClass)}>
						<span className={classnames(UIContainer.iconClass, Style.effectIcon)}>
							<UIDodecahedron type={effect.static.negative ? 'red' : 'blue'} />
						</span>
						<span
							className={classnames(
								Style.effectCaption,
								effect.static.negative ? Style.negative : Style.positive,
							)}
						>
							{effect.caption}
						</span>
					</dt>
					<dd className={Style.duration}>
						{!effect.static.unremovable && (
							<UIDurationDescriptor
								duration={effect.duration}
								short={true}
								tooltip={!tooltip}
								type={'duration'}
								modifier={target?.effectModifiers?.statusDurationModifier?.[0]}
							/>
						)}
					</dd>
				</dl>
				<div className={classnames(Style.effectLore, effect.static.negative ? Style.negative : Style.positive)}>
					<Tokenizer description={effect.description} />
				</div>
				{!!tooltip && (
					<UITooltip>
						<span className={classnames(UIContainer.iconClass, UIContainer.tooltipIconClass)}>
							<UIDodecahedron type={effect.static.negative ? 'red' : 'blue'} />
						</span>
						<p>
							<span
								className={classnames(
									Style.effectCaption,
									effect.static.negative ? Style.negative : Style.positive,
									Style.tooltipCaption,
								)}
							>
								{effect.static.negative ? 'Negative' : 'Non-Negative'} Status Effect
							</span>
						</p>
						<p
							className={classnames(
								Style.effectExpiration,
								effect.static.unremovable ? Style.unremovable : null,
							)}
						>
							{effect.static.unremovable ? "Can't" : 'Can'} be removed
						</p>
						<p className={Style.effectPersistence}>
							{effect.duration < 0 ? (
								<>Doesn't expire</>
							) : (
								<>
									Will expire in{' '}
									<em>
										{effect.duration}
										<UIDurationDescriptor
											type={'duration'}
											tooltip={false}
											duration={effect.duration}
											short={true}
										/>{' '}
										Turns
									</em>
								</>
							)}
						</p>
					</UITooltip>
				)}
			</div>
		</div>
	);
};

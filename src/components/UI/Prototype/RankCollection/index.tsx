import React from 'react';
import UnitTalents from '../../../../../definitions/tactical/units/levels/unitTalents';
import { IUICommonProps } from '../../index';
import { UIURankUpgradeDescriptor } from '../RankUpgradeDescriptor';

type UIRankCollectionProps = IUICommonProps & {
	talents: UnitTalents.ITalentTree;
	talentPoints?: number;
	filter?: (t: UnitTalents.IUnitTalentTreeItem) => boolean;
};

const UIRankCollection: React.FC<UIRankCollectionProps> = ({ talentPoints, tooltip, className, talents, filter }) => {
	const stack = talents.talentOptions.slice();
	const result = [];
	while (stack.length) {
		const tItem = stack.shift();
		if (!tItem) continue;
		if (tItem.unlocks) tItem.unlocks.forEach((t) => stack.push(t));
		if (!filter || filter(tItem)) result.push(tItem);
	}
	return (
		<React.Fragment>
			{result.map((e) => (
				<React.Fragment key={e.talent.talentId}>
					<UIURankUpgradeDescriptor
						talent={e}
						className={className}
						tooltip={tooltip}
						active={(talentPoints ?? 0) > 0}
					/>
				</React.Fragment>
			))}
		</React.Fragment>
	);
};

export default UIRankCollection;

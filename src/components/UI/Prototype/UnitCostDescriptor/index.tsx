import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import { wellRounded } from '../../../../utils/wellRounded';
import UIProductionSpeedDescriptor from '../../Economy/ProductionSpeedDescriptor';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import UIModifierDescriptor from '../../Templates/ModifierDescriptor';
import UITooltip from '../../Templates/Tooltip';
import UIRawMaterialsDescriptor from '../../Economy/RawMaterialsDescriptor';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import Style from './index.scss';
export type IUIUnitCostDescriptorPureProps = {
	modifier?: IModifier;
	value?: number;
	totalCost?: boolean;
};

export type UIUnitCostDescriptorProps = IUICommonProps &
	IUIUnitCostDescriptorPureProps & {
		short?: boolean;
		caption?: string | JSX.Element;
	};

export default class UIUnitCostDescriptor extends React.PureComponent<UIUnitCostDescriptorProps, {}> {
	public static getCaption(totalCost = false) {
		return totalCost ? 'Unit Cost' : 'Production Cost';
	}

	public static getCaptionClass(totalCost = false) {
		return totalCost ? UIProductionSpeedDescriptor.getCaptionClass() : UIRawMaterialsDescriptor.getCaptionClass();
	}

	public static getTooltip(totalCost = false) {
		return termTooltipTemplate(
			UIUnitCostDescriptor.getIcon(totalCost, UIContainer.tooltipIconClass),
			UIUnitCostDescriptor.getCaption(totalCost),
			totalCost
				? 'Unit Cost is a combined Production Cost of all parts of the Prototype, and is equal to amount of Raw Materials that will be expended by Factories (at rate of their Production Speed) to finish building a Unit out of that Prototype'
				: 'Production Cost is amount of Raw Materials that Chassis, Shield, Armor, Weapon and Equipment contribute to the Unit Cost of a Prototype',
		);
	}

	public static getIcon(totalCost = false, className = UIContainer.iconClass) {
		return totalCost ? UIProductionSpeedDescriptor.getIcon(className) : UIRawMaterialsDescriptor.getIcon(className);
	}

	public static getEntityCaption(totalCost = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIUnitCostDescriptor.getIcon(totalCost)}
				<span className={UIUnitCostDescriptor.getCaptionClass(totalCost)}>
					{UIUnitCostDescriptor.getCaption(totalCost)}
				</span>
			</em>
		);
	}

	public static getValue(props: IUIUnitCostDescriptorPureProps) {
		const { value, modifier, totalCost } = props;
		const modOnly = !Number.isFinite(value);
		if (modOnly && !modifier) return '';
		const isPositive = 100 > BasicMaths.applyModifier(100, modifier);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false, false, false, 3).join('/').trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(value) && (
					<span className={totalCost ? Style.totalCostValue : Style.productionCostValue}>
						{value === 0 ? '-' : wellRounded(value, 2)}
					</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	public render() {
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
						Style.costModifierDescriptor,
					)}>
					{UIUnitCostDescriptor.getIcon(this.props.totalCost)}
					{UIUnitCostDescriptor.getValue(this.props)}
					{this.props.tooltip && (
						<UITooltip>{UIUnitCostDescriptor.getTooltip(this.props.totalCost)}</UITooltip>
					)}
				</span>
			);
		return (
			<dl
				className={classnames(this.props.className || '', Style.costModifierDescriptor)}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIUnitCostDescriptor.getIcon(this.props.totalCost)}
					<span className={UIUnitCostDescriptor.getCaptionClass(this.props.totalCost)}>
						{this.props.caption || UIUnitCostDescriptor.getCaption(this.props.totalCost)}
					</span>
					{this.props.tooltip && (
						<UITooltip>{UIUnitCostDescriptor.getTooltip(this.props.totalCost)}</UITooltip>
					)}
				</dt>
				<dd>{UIUnitCostDescriptor.getValue(this.props)}</dd>
			</dl>
		);
	}
}

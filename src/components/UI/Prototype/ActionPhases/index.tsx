import React from 'react';
import UnitActions from '../../../../../definitions/tactical/units/actions/types';
import { classnames, IUICommonProps } from '../../index';
import UIActionButton from '../../Parts/Chassis/ActionButton';
import Style from './index.scss';
type UIActionPhaseDescriptorProps = IUICommonProps & {
	actions: UnitActions.TUnitActionOptions[];
};

export const ActionPhaseCaption = (phaseNumber: number) => (phaseNumber > 0 ? `Action Phase ${phaseNumber}` : null);

const UIActionPhaseDescriptor: React.FC<UIActionPhaseDescriptorProps> = ({ actions, className, tooltip }) => (
	<dl className={classnames(Style.actionPhaseDescriptor, className)}>
		{actions.map((actionList, ix) => (
			<React.Fragment key={ix}>
				<dt className={Style.actionPhaseCaption}>{ActionPhaseCaption(ix + 1)}</dt>
				<dt className={Style.actionPhaseContent}>
					{UnitActions.defaultActionOrder
						.filter((v) => actionList.includes(v))
						.map((actionType) => (
							<UIActionButton
								key={actionType}
								className={Style.actionPhaseButton}
								tooltip={tooltip}
								actionType={actionType}
							/>
						))}
				</dt>
			</React.Fragment>
		))}
	</dl>
);

export default UIActionPhaseDescriptor;

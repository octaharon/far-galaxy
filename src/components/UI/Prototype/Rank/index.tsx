import * as React from 'react';
import romanize from '../../../../utils/romanize';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import { UIRankIcon } from '../../../Symbols/commonIcons';
import SVGIconContainer from '../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../SVG/SmallTexturedIcon';
import UITooltip from '../../Templates/Tooltip';
import UIXPCaption from '../XP';
import Style from './index.scss';

type UIRankCaptionProps = IUICommonProps & {
	value?: number;
	highlight?: boolean;
};

export default class UIRankCaption extends React.PureComponent<UIRankCaptionProps, {}> {
	public static getIcon(highlight?: boolean, className: string = UIContainer.iconClass) {
		return (
			<SVGSmallTexturedIcon
				style={highlight ? TSVGSmallTexturedIconStyles.light : TSVGSmallTexturedIconStyles.blue}
				className={className}>
				<use {...SVGIconContainer.getXLinkProps(UIRankIcon)} />
			</SVGSmallTexturedIcon>
		);
	}

	public static getCaption() {
		return 'Rank';
	}

	public static getCaptionClass() {
		return Style.rankCaption;
	}

	public static getTooltip() {
		return (
			<span>
				{UIRankCaption.getIcon(true, UITooltip.getIconClass())}
				<b>{UIRankCaption.getCaption()}</b> is a measure of <em>Unit's</em> progression. They are gained from{' '}
				{UIXPCaption.getEntityCaption()} and do increase <em>Unit's</em> combat capabilities based on its{' '}
				<em>Promotion Class</em>.
			</span>
		);
	}

	public static getEntityCaption() {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIRankCaption.getIcon()}
				<span className={UIRankCaption.getCaptionClass()}>{UIRankCaption.getCaption()}</span>
			</em>
		);
	}

	public render() {
		return (
			<span
				className={classnames(
					this.props.className || '',
					UIContainer.captionClass,
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				)}>
				{UIRankCaption.getIcon(this.props.highlight, Style.rankIcon)}
				{!!this.props.highlight && (
					<span className={Style.rankCaptionHighlight}>{UIRankCaption.getCaption()} </span>
				)}
				{!!this.props.value && (
					<span className={this.props.highlight ? Style.rankHighlight : Style.rankValue}>
						{romanize(this.props.value)}
					</span>
				)}
				{!!this.props.tooltip && <UITooltip>{UIRankCaption.getTooltip()}</UITooltip>}
			</span>
		);
	}
}

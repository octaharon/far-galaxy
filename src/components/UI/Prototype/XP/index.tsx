import * as React from 'react';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import UIDamageModifierInlineDescriptor from '../../Parts/Damage/DamageModifierInlineDescriptor';
import SVGHexIcon from '../../SVG/HexIcon';
import UITooltip from '../../Templates/Tooltip';
import { basicTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import UIRankCaption from '../Rank';
import Style from './index.scss';

type UIXPCaptionProps = IUICommonProps & {
	value?: number;
	level?: boolean;
};

export default class UIXPCaption extends React.PureComponent<UIXPCaptionProps, {}> {
	public static getIcon(level = false, className: string = UIContainer.iconClass) {
		return (
			<SVGHexIcon
				colorTop={getTemplateColor(level ? 'golden' : 'golden')}
				colorBottom={getTemplateColor(level ? 'light_teal' : 'light_red')}
				className={className}
			>
				<text
					x="50%"
					y="50%"
					textAnchor="middle"
					dominantBaseline="middle"
					className={Style.iconText}
					stroke={getTemplateColor(level ? 'light_teal' : 'dark_orange')}
					strokeWidth="0.25"
					fill={getTemplateColor('text_dark')}
				>
					{level ? 'Lv' : 'XP'}
				</text>
			</SVGHexIcon>
			/*<SVGSmallTexturedIcon style={TSVGSmallTexturedIconStyles.gold} className={className}>
				<use {...SVGIconContainer.getXLinkProps(UIExperienceIcon)} />
			</SVGSmallTexturedIcon>*/
		);
	}

	public static getCaption(level = false) {
		return level ? 'Level' : 'Experience';
	}

	public static getCaptionClass(level = false) {
		return level ? Style.lvlCaption : Style.xpCaption;
	}

	public static getTooltip(level = false) {
		return basicTooltipTemplate(
			UIXPCaption.getIcon(level, UITooltip.getIconClass()),
			UIXPCaption.getCaption(level),
			level
				? `is an indicator of Player's development that is gained with XP. Each new Level rewards
					with a Talent Point which can be exchanged for an eligible Talent or a Building Upgrade Point`
				: `allows Units and Players to gain Levels and Rank. XP is rewarded for dealing Damage to Hit Points of enemy Units and Orbital Base, mostly, but can be granted on other special occasions. With more XP, Units benefit from Ranks, while Players gain Talents`,
		);
	}

	public static getEntityCaption(level = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIXPCaption.getIcon(level)}
				<span className={UIXPCaption.getCaptionClass(level)}>{UIXPCaption.getCaption(level)}</span>
			</em>
		);
	}

	public render() {
		return (
			<span
				className={classnames(
					this.props.className || '',
					UIContainer.captionClass,
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				)}
			>
				{UIXPCaption.getIcon(this.props.level, Style.xpIcon)}
				{Number.isFinite(this.props.value) && (
					<span className={this.props.level ? Style.lvlValue : Style.xpValue}>{this.props.value}</span>
				)}
				{!!this.props.tooltip && <UITooltip>{UIXPCaption.getTooltip(this.props.level)}</UITooltip>}
			</span>
		);
	}
}

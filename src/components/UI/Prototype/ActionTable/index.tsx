import * as React from 'react';
import UnitActions from '../../../../../definitions/tactical/units/actions/types';
import romanize from '../../../../utils/romanize';
import UIContainer, { IUICommonProps, classnames, getTemplateColor } from '../../index';
import UIActionButton from '../../Parts/Chassis/ActionButton';
import UIActionDescriptor from '../../Parts/Chassis/ActionDescriptor';
import SVGSegmentedBar from '../../SVG/SegmentedBar';
import UITooltip from '../../Templates/Tooltip';
import UIActionCounter from '../ActionCount';
import Style from './index.scss';
import _ from 'underscore';

export type UIActionTableProps = IUICommonProps & {
	actions: UnitActions.TUnitActionOptions[];
};

export default class UIActionTable extends React.PureComponent<UIActionTableProps, {}> {
	public static getClass(): string {
		return Style.actionTable;
	}

	public render() {
		const total = this.props.actions.length;
		const actionOrder = UnitActions.defaultActionOrder.filter((v) => this.props.actions.some((a) => a.includes(v)));
		return (
			<dl className={classnames(UIActionTable.getClass(), this.props.className)}>
				<dt className={Style.tableWrapper}>
					<div className={Style.tr}>
						<div className={classnames(Style.td, Style.actionPhaseCaption, Style.actionType)}>
							{UIActionCounter.getCaption(false)}
						</div>
						{_.range(total).map((ix) => (
							<div key={ix} className={classnames(Style.td, Style.actionPhaseNumber, Style.actionBar)}>
								{romanize(ix + 1)}
							</div>
						))}
					</div>
					{actionOrder.map((actionType) => (
						<div className={Style.tr} key={actionType}>
							<div
								className={classnames(
									Style.td,
									Style.actionType,
									UIContainer.descriptorClass,
									this.props.tooltip ? UIContainer.hasTooltipClass : '',
								)}
							>
								{/*<UIActionButton
									key={actionType}
									tooltip={this.props.tooltip}
									actionType={actionType}
									className={Style.actionButton}
								/>*/}
								{UIActionDescriptor.getEntityCaption(actionType)}
								<UITooltip>{UIActionDescriptor.getTooltip(actionType)}</UITooltip>
							</div>
							<div className={classnames(Style.td, Style.actionBar)}>
								<SVGSegmentedBar
									className={Style.actionSegments}
									width={200}
									strokeColor={getTemplateColor('text_dark')}
									segments={_.range(total).map((v) => ({
										value: 100,
										colorTop: getTemplateColor(
											this.props.actions[v].includes(actionType) ? 'dark_orange' : 'gray_bg',
										),
										colorBottom: getTemplateColor(
											this.props.actions[v].includes(actionType) ? 'golden' : 'text_dark',
										),
									}))}
								/>
							</div>
							{/*{_.range(total).map((ix) => (
								<div key={ix} className={classnames(Style.td, Style.actionBar)}>
									{this.props.actions[ix].includes(actionType) && (
										<span className={Style.filled}>&nbsp;</span>
									)}
								</div>
							))}*/}
						</div>
					))}
				</dt>
			</dl>
		);
	}
}

import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import SVGIconContainer, {
	SVGBasicEmbossFilterID,
	SVGBasicNoiseFilterID,
	SVGFilterWrapper,
} from '../../SVG/IconContainer';
import UITooltip from '../../Templates/Tooltip';
import { basicTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import { SVGProductionSpeedIconID } from '../ProductionSpeedDescriptor';
import UIRawMaterialsDescriptor, { SVGRawMaterialFillGradient } from '../RawMaterialsDescriptor';
import Style from './index.scss';

type IUIMaxUnitCostDescriptorPureProps = {
	maxUnitCost: number;
	output?: boolean;
	modifier?: IModifier;
};

export type UIMaxUnitCostDescriptorProps = IUICommonProps &
	IUIMaxUnitCostDescriptorPureProps & {
		short?: boolean;
	};

export default class UIMaxUnitCostDescriptor extends React.PureComponent<UIMaxUnitCostDescriptorProps, {}> {
	public static getClass(): string {
		return Style.maxUnitCostDescriptor;
	}

	public static getCaptionClass(): string {
		return UIRawMaterialsDescriptor.getCaptionClass();
	}

	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGFilterWrapper className={className}>
				<g filter={SVGIconContainer.getUrlRef(SVGBasicEmbossFilterID)}>
					<g
						fill={SVGIconContainer.getUrlRef(SVGRawMaterialFillGradient)}
						filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)}>
						<use {...SVGIconContainer.getXLinkProps(SVGProductionSpeedIconID)} />
					</g>
				</g>
			</SVGFilterWrapper>
		);
	}

	public static getCaption(output = false) {
		return !output ? 'Max Unit Cost' : 'Max Unit Cost';
	}

	public static getEntityCaption(output = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIMaxUnitCostDescriptor.getIcon()}
				<span className={UIMaxUnitCostDescriptor.getCaptionClass()}>
					{UIMaxUnitCostDescriptor.getCaption(output)}
				</span>
			</em>
		);
	}

	public static getTooltip() {
		return basicTooltipTemplate(
			UIMaxUnitCostDescriptor.getIcon(UIContainer.tooltipIconClass),
			UIMaxUnitCostDescriptor.getCaption(false),
			' is a quality of Factories which, as the name suggests, limits the maximum Prototype Unit Cost the Factory can build. It can be improved with Building Upgrades',
		);
	}

	public static getValue(props: IUIMaxUnitCostDescriptorPureProps) {
		const { maxUnitCost, modifier } = props;
		const modOnly = !Number.isFinite(maxUnitCost);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = 100 < BasicMaths.applyModifier(100, modifier);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false).join('/').trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(maxUnitCost) && (
					<span className={Style.maxUnitCostValue}>{maxUnitCost === 0 ? '-' : maxUnitCost}</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	constructor(props: UIMaxUnitCostDescriptorProps) {
		super(props);
	}

	public render() {
		const value = UIMaxUnitCostDescriptor.getValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						UIMaxUnitCostDescriptor.getClass(),
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIMaxUnitCostDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && <UITooltip>{UIMaxUnitCostDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(UIMaxUnitCostDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIMaxUnitCostDescriptor.getIcon()}
					<span className={UIMaxUnitCostDescriptor.getCaptionClass()}>
						{UIMaxUnitCostDescriptor.getCaption(this.props.output)}
					</span>
					{!!this.props.tooltip && <UITooltip>{UIMaxUnitCostDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

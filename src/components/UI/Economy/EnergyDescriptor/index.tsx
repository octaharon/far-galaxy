import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import { wellRounded } from '../../../../utils/wellRounded';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import SVGIconContainer, {
	SVGBasicEmbossFilterID,
	SVGFilterWrapper,
	SVGLargeGlowFilterID,
} from '../../SVG/IconContainer';
import UIIconWithValue from '../../Templates/IconWithValue';
import UITooltip from '../../Templates/Tooltip';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import { UIRawMaterialsDescriptorType } from '../RawMaterialsDescriptor';
import Style from './index.scss';

export enum UIEnergyDescriptorType {
	default = 'energy-default',
	output = 'energy-output',
	consumption = 'energy-consumption',
	storage = 'energy-storage',
}

export type UIEnergyDescriptorPureProps = {
	value: number;
	type?: UIEnergyDescriptorType;
	modifier?: IModifier;
};

export type UIEnergyDescriptorProps = IUICommonProps &
	UIEnergyDescriptorPureProps & {
		short?: boolean;
	};

export const SVGEnergyFillGradient = 'gradient-energy';
export const SVGEnergyIconShapeID = 'icon-energy';

SVGIconContainer.registerGradient(
	SVGEnergyFillGradient,
	<linearGradient x1="50%" y1="0" x2="75%" y2="100%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor={getTemplateColor('light_purple')} />
		<stop offset="1" stopColor={getTemplateColor('dark_blue')} />
	</linearGradient>,
);

SVGIconContainer.registerSymbol(
	SVGEnergyIconShapeID,
	<symbol viewBox="0 0 512 512">
		<path
			d="M500.647,211.454l-25.536-6.138c-5.596-24.404-15.088-47.358-28.301-68.467l12.964-21.621
			c3.545-5.903,2.607-13.462-2.256-18.325l-42.422-42.422c-4.863-4.878-12.407-5.771-18.325-2.256L375.15,65.189
			c-21.108-13.213-44.063-22.705-68.467-28.301l-6.138-25.536C298.876,4.688,292.885,0,286,0h-60
			c-6.885,0-12.876,4.688-14.546,11.353l-6.138,25.536c-24.404,5.596-47.358,15.088-68.467,28.301l-21.621-12.964
			c-5.918-3.545-13.447-2.607-18.325,2.256L54.481,96.903c-4.863,4.863-5.801,12.422-2.256,18.325l12.964,21.621
			c-13.213,21.108-22.705,44.063-28.301,68.467l-25.536,6.138C4.688,213.124,0,219.115,0,226v60c0,6.885,4.688,12.876,11.353,14.546
			l25.536,6.138c5.596,24.404,15.088,47.358,28.301,68.467l-12.964,21.621c-3.545,5.903-2.607,13.462,2.256,18.325l42.422,42.422
			c4.849,4.878,12.407,5.815,18.325,2.256l21.621-12.964c21.108,13.213,44.063,22.705,68.467,28.301l6.138,25.536
			C213.124,507.313,219.115,512,226,512h60c6.885,0,12.876-4.688,14.546-11.353l6.138-25.536
			c24.404-5.596,47.358-15.088,68.467-28.301l21.621,12.964c5.918,3.53,13.447,2.607,18.325-2.256l42.422-42.422
			c4.863-4.863,5.801-12.422,2.256-18.325l-12.964-21.621c13.213-21.108,22.705-44.063,28.301-68.467l25.536-6.138
			C507.313,298.876,512,292.885,512,286v-60C512,219.115,507.313,213.124,500.647,211.454z M256,421c-90.981,0-165-74.019-165-165
			S165.019,91,256,91s165,74.019,165,165S346.981,421,256,421z"
		/>
		<path
			d="M359.051,218.603C356.385,213.901,351.405,211,346,211h-75v-75c0-6.738-4.497-12.656-11.001-14.458
c-6.445-1.831-13.403,0.952-16.86,6.738l-90,150c-2.783,4.644-2.856,10.415-0.19,15.117C155.615,298.099,160.595,301,166,301h75
v75c0,6.738,4.497,12.656,11.001,14.458c1.318,0.366,2.666,0.542,3.999,0.542c5.171,0,10.107-2.681,12.861-7.28l90-150
C361.644,229.076,361.717,223.305,359.051,218.603z"
		/>
	</symbol>,
);

export default class UIEnergyDescriptor extends React.PureComponent<UIEnergyDescriptorProps, {}> {
	public static getClass(): string {
		return Style.energyDescriptor;
	}

	public static getGradientClass(): string {
		return Style['energy-gradient'] || Style.energyGradient;
	}

	public static getCaptionClass(): string {
		return Style.energyCaption;
	}

	public static getCaption(type: UIEnergyDescriptorType = UIEnergyDescriptorType.default): string {
		return type === UIEnergyDescriptorType.output
			? 'Energy Output'
			: type === UIEnergyDescriptorType.consumption
			? 'Energy Upkeep'
			: type === UIEnergyDescriptorType.storage
			? 'Energy Storage'
			: 'Energy';
	}

	public static getTooltip(type: UIEnergyDescriptorType = UIEnergyDescriptorType.default) {
		return termTooltipTemplate(
			UIEnergyDescriptor.getIcon(UIContainer.tooltipIconClass),
			UIEnergyDescriptor.getCaption(),
			'Energy is arguably the most important value in economy of a Starbase and is required to keep other goods flowing. While most Buildings consume Energy every Turn, some Facilities do produce it, usually consuming Resources in exchange. Unpowered Buildings only provide part of their bonuses. ' +
				(type === UIEnergyDescriptorType.storage
					? 'Excessive Energy can be accumulated in base Storage and used later, even when missing Resources for Facilities to prouce Energy'
					: type === UIEnergyDescriptorType.output
					? 'The modifier is applied to each of the Establishments that are in operating mode'
					: type === UIEnergyDescriptorType.consumption
					? 'The modifier is applied to all operating Establishments, Factories and Facilities that are in operating mode'
					: ''),
		);
	}

	public static getValue(props: UIEnergyDescriptorPureProps) {
		const { value, modifier, type } = props;
		const modOnly = !Number.isFinite(value);
		if (modOnly && !modifier) return '';
		const isPositive = [UIEnergyDescriptorType.consumption].includes(type)
			? BasicMaths.applyModifier(100, modifier) < 100
			: BasicMaths.applyModifier(100, modifier) >= 100;
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false).join('/').trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(value) && (
					<span className={Style.energyValue}>{value === 0 ? '-' : wellRounded(value, 1)}</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	public static getEntityCaption(type: UIEnergyDescriptorType = UIEnergyDescriptorType.default) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIEnergyDescriptor.getIcon()}
				<span className={UIEnergyDescriptor.getCaptionClass()}>{UIEnergyDescriptor.getCaption(type)}</span>
			</em>
		);
	}

	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGFilterWrapper className={className}>
				<g
					filter={SVGIconContainer.getUrlRef(SVGBasicEmbossFilterID)}
					style={{
						transform: 'scale(1)',
						transformOrigin: '50% 50%',
					}}>
					<g
						fill={SVGIconContainer.getUrlRef(SVGEnergyFillGradient)}
						filter={SVGIconContainer.getUrlRef(SVGLargeGlowFilterID)}>
						<use {...SVGIconContainer.getXLinkProps(SVGEnergyIconShapeID)} />
					</g>
				</g>
			</SVGFilterWrapper>
		);
	}

	public render() {
		return this.props.short ? (
			<UIIconWithValue
				svgContent={UIEnergyDescriptor.getIcon()}
				className={classnames(Style.energy, this.props.className)}
				tooltip={UIEnergyDescriptor.getTooltip(this.props.type)}>
				<span className={Style.energyValue}>{UIEnergyDescriptor.getValue(this.props)}</span>
			</UIIconWithValue>
		) : (
			<dl
				className={classnames(UIEnergyDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIEnergyDescriptor.getIcon()}
					<span className={UIEnergyDescriptor.getCaptionClass()}>
						{UIEnergyDescriptor.getCaption(this.props.type || UIEnergyDescriptorType.default)}
					</span>
					{!!this.props.tooltip && <UITooltip>{UIEnergyDescriptor.getTooltip(this.props.type)}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{UIEnergyDescriptor.getValue(this.props)}</span>
				</dd>
			</dl>
		);
	}
}

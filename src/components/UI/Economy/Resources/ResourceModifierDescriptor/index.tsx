import * as React from 'react';
import { isValidModifier } from '../../../../../../definitions/core/helpers';
import BasicMaths from '../../../../../../definitions/maths/BasicMaths';
import { ResourceDictionary } from '../../../../../../definitions/world/resources/constants';
import Resources from '../../../../../../definitions/world/resources/types';
import predicateComparator from '../../../../../utils/predicateComparator';
import { outputIcon, upkeepIcon } from '../../../../Symbols/commonIcons';
import UIContainer, { classnames, IUICommonProps, MetaHeader } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIResourceDescriptor from '../ResourceDescriptor';
import Style from './index.scss';
import TResourceModifier = Resources.TResourceModifier;

export type UIResourceModifierDescriptorProps = IUICommonProps & {
	resourceModifier: TResourceModifier;
	consumption?: boolean;
	tooltip?: boolean;
};

export default class UIResourceModifierDescriptor extends React.PureComponent<UIResourceModifierDescriptorProps, {}> {
	public static getCaption(consumption = false) {
		return consumption ? 'Resource Costs and Upkeep' : 'Resource Output';
	}

	public render() {
		const payload = this.props.resourceModifier;
		let defaultOnly = true;
		const ResourceList: Resources.TResourceModifier = Object.keys(ResourceDictionary).reduce(
			(acc, resourceType) => {
				const isValid = isValidModifier(payload?.[resourceType]);
				if (isValid) defaultOnly = false;
				return isValid
					? {
							...acc,
							[resourceType]: payload[resourceType],
					  }
					: acc;
			},
			isValidModifier(payload?.default)
				? {
						default: payload.default,
				  }
				: {},
		);
		defaultOnly &&= !!ResourceList.default;
		if (!Object.keys(ResourceList).length) return null;
		const isPositive = (f: keyof Resources.TResourceModifier) =>
			Math.sign(BasicMaths.applyModifier(100, ResourceList[f]) - 100) === (this.props.consumption ? -1 : 1);
		return (
			<div className={classnames(UIContainer.descriptorGroupClass, this.props.className)}>
				<MetaHeader>
					{UIResourceModifierDescriptor.getCaption(this.props.consumption)}
					{this.props.consumption ? upkeepIcon : outputIcon}
				</MetaHeader>
				<dl className={Style.resourceModifierDescriptor}>
					{Object.keys(ResourceList)
						.sort(
							predicateComparator([(v) => v !== 'default'], (v) =>
								v === 'default' ? Infinity : -Object.keys(ResourceDictionary).indexOf(v),
							),
						)
						.map((f) =>
							f === 'default' ? (
								<React.Fragment key={'default'}>
									<dt
										className={classnames(
											UIContainer.descriptorClass,
											this.props.tooltip ? UIContainer.hasTooltipClass : '',
										)}>
										<UIResourceDescriptor
											resource={null}
											caption={defaultOnly ? 'All Resources' : 'Other Resources'}
											tooltip={!!this.props.tooltip}
										/>
									</dt>
									<dd className={UIResourceDescriptor.getValueClass()}>
										<span
											className={classnames(
												isPositive(f) ? Style.positive : Style.negative,
												Style.modifier,
											)}>
											{BasicMaths.describeModifier(payload.default, false).join('/').trim()}
										</span>
									</dd>
								</React.Fragment>
							) : (
								<React.Fragment key={f}>
									<dt
										className={classnames(
											UIContainer.descriptorClass,
											this.props.tooltip ? UIContainer.hasTooltipClass : '',
										)}>
										<UIResourceDescriptor resource={f} tooltip={!!this.props.tooltip} />
										{!!this.props.tooltip && (
											<UITooltip>{UIResourceDescriptor.getTooltip(f)}</UITooltip>
										)}
									</dt>
									<dd className={UIResourceDescriptor.getValueClass()}>
										<span
											className={classnames(
												isPositive(f) ? Style.positive : Style.negative,
												Style.modifier,
											)}>
											{BasicMaths.describeModifier(
												defaultOnly ? payload.default : ResourceList[f],
												false,
											)
												.join('/')
												.trim()}
										</span>
									</dd>
								</React.Fragment>
							),
						)}
				</dl>
			</div>
		);
	}
}

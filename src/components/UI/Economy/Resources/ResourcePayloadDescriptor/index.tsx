import * as React from 'react';
import { ResourceDictionary } from '../../../../../../definitions/world/resources/constants';
import Resources from '../../../../../../definitions/world/resources/types';
import { wellRounded } from '../../../../../utils/wellRounded';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import UITooltip from '../../../Templates/Tooltip';
import UIResourceDescriptor from '../ResourceDescriptor';
import Style from './index.scss';
import TResourcePayload = Resources.TResourcePayload;

export type UIResourcePayloadDescriptorProps = IUICommonProps & {
	resourcePayload: TResourcePayload;
	tooltip?: boolean;
	short?: boolean;
};

export default class UIResourcePayloadDescriptor extends React.PureComponent<UIResourcePayloadDescriptorProps, {}> {
	public render() {
		const payload = this.props.resourcePayload || {};
		const hasDetailedTooltip = this.props.short && !!this.props.tooltip;
		const cls = classnames(
			Style.resourcePayloadDescriptor,
			hasDetailedTooltip ? UIContainer.hasTooltipClass : null,
			this.props.className || null,
		);
		const WrappingTag = (props: React.PropsWithChildren<{ key?: React.Key; className?: string }>) =>
			this.props.short ? <span {...props} /> : <dt {...props} />;
		const ResourceList = Object.keys(ResourceDictionary)
			.map((f) =>
				payload[f] ? (
					<React.Fragment key={f}>
						<WrappingTag
							className={classnames(
								Style.item,
								this.props.short ? Style.short : UIContainer.descriptorClass,
							)}>
							<UIResourceDescriptor resource={f} tooltip={!!this.props.tooltip} />
						</WrappingTag>
						{!this.props.short && (
							<dd className={UIResourceDescriptor.getValueClass()}>{wellRounded(payload[f], 4)}</dd>
						)}
					</React.Fragment>
				) : null,
			)
			.filter((v) => !!v);
		if (!ResourceList.length) return this.props.short ? <>&nbsp;</> : null;
		return this.props.short ? (
			<span className={cls}>{ResourceList}</span>
		) : (
			<dl className={cls}>
				{ResourceList}
				{hasDetailedTooltip ? (
					<UITooltip>
						{Object.keys(payload).map((resourceId) => (
							<section key={resourceId}>
								<UIResourceDescriptor resource={resourceId} />:{' '}
								<b>{wellRounded(payload[resourceId])}</b>
							</section>
						))}
					</UITooltip>
				) : null}
			</dl>
		);
	}
}

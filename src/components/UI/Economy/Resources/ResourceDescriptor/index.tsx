import * as React from 'react';
import { ResourceCaptions, ResourceDictionary } from '../../../../../../definitions/world/resources/constants';
import Resources from '../../../../../../definitions/world/resources/types';
import UIContainer, { classnames, IUICommonProps } from '../../../index';
import SVGIconContainer, { SVGContrastGlowFilterID } from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UITooltip from '../../../Templates/Tooltip';
import { termTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import styles from './index.scss';

export interface UIResourceDescriptorProps extends IUICommonProps {
	resource: Resources.resourceId;
	caption?: string | JSX.Element;
	tooltip?: boolean;
}

const SVGResourceIconFilterId = 'resource-icon-filter';
SVGIconContainer.registerFilter(
	SVGResourceIconFilterId,
	<filter
		id="filter"
		x="-20%"
		y="-20%"
		width="140%"
		height="140%"
		filterUnits="objectBoundingBox"
		primitiveUnits="userSpaceOnUse"
		colorInterpolationFilters="linearRGB">
		<feFlood floodColor="#95f2fe" floodOpacity=".85" x="0%" y="0%" width="100%" height="100%" result="flood1" />
		<feBlend mode="color-dodge" in="flood1" in2="SourceGraphic" result="blend1" />
		<feComposite in="blend1" in2="SourceAlpha" operator="in" result="composite2" />
		<feGaussianBlur stdDeviation="2" in="composite2" edgeMode="wrap" result="blur" />
		<feBlend mode="screen" x="0%" y="0%" width="100%" height="100%" in="blur" in2="merge" result="blend2" />
		<feMerge x="0%" y="0%" width="100%" height="100%" result="merge">
			<feMergeNode in="blur" />
			<feMergeNode in="SourceGraphic" />
		</feMerge>
	</filter>,
);

const iconMapping: Partial<EnumProxy<Resources.resourceId, any>> = {
	[Resources.resourceId.hydrogen]: require('../../../../../../assets/img/svg/resources/hydrogen.svg'),
	[Resources.resourceId.proteins]: require('../../../../../../assets/img/svg/resources/proteins.svg'),
	[Resources.resourceId.helium]: require('../../../../../../assets/img/svg/resources/helium.svg'),
	[Resources.resourceId.nitrogen]: require('../../../../../../assets/img/svg/resources/nitrogen.svg'),
	[Resources.resourceId.oxygen]: require('../../../../../../assets/img/svg/resources/oxygen.svg'),
	[Resources.resourceId.ores]: require('../../../../../../assets/img/svg/resources/ores.svg'),
	[Resources.resourceId.carbon]: require('../../../../../../assets/img/svg/resources/carbon.svg'),
	[Resources.resourceId.minerals]: require('../../../../../../assets/img/svg/resources/minerals.svg'),
	[Resources.resourceId.halogens]: require('../../../../../../assets/img/svg/resources/halogens.svg'),
	[Resources.resourceId.isotopes]: require('../../../../../../assets/img/svg/resources/isotopes.svg'),
};

export const SVGResourceIconId = 'resource-icon-default';
SVGIconContainer.registerSymbol(
	SVGResourceIconId,
	<symbol viewBox="0 0 512 512">
		<g
			style={{
				transform: 'scale(0.95)',
				transformOrigin: '50% 50%',
			}}>
			<path d="m105.667 177h300.666v-128h-43.963v30.33h-30v-30.33h-61.367v30.33h-30v-30.33h-61.367v30.33h-30v-30.33h-43.969z" />
			<path d="m436.333 207h-103.111v46.445h178.778v-129.445c0-41.355-33.645-75-75-75h-.667z" />
			<path d="m178.778 253.445v-46.445h-103.111v-158h-.667c-41.355 0-75 33.645-75 75v129.445z" />
			<path d="m406.331 283.444h-74.583c-7.003 35.424-38.299 62.222-75.748 62.222s-68.745-26.798-75.748-62.222h-74.587v179.556h43.971v-30.805h30v30.805h61.367v-30.805h30v30.805h61.367v-30.805h30v30.805h43.961z" />
			<path d="m436.331 283.444h75.669v179.555h-75.669z" />
			<path d="m0 283.444h75.665v179.555h-75.665z" />
			<path d="m303.222 268.444v-61.444h-94.444v61.444c0 26.038 21.184 47.222 47.222 47.222s47.222-21.183 47.222-47.222zm-62.222-15.004h30v30h-30z" />
		</g>
	</symbol>,
);

export default class UIResourceDescriptor extends React.PureComponent<UIResourceDescriptorProps, {}> {
	public static getClass(): string {
		return styles.resourceDescriptor;
	}

	public static getCaption(t: Resources.resourceId): string {
		return ResourceCaptions[t] || 'Resources';
	}

	public static getValueClass(): string {
		return styles.resourceValue;
	}

	public static getTooltip(t: Resources.resourceId) {
		return termTooltipTemplate(
			UIResourceDescriptor.getIcon(t, UIContainer.tooltipIconClass, true),
			UIResourceDescriptor.getCaption(t),
			ResourceDictionary[t]?.description ||
				'Resources are primary building blocks of the Universe, as well as of all Buildings at your Base. Resources are obtained from controlled Planetary Zones, from Trading and from some Facilities. Resources logistics is so extremely complex, that their supply is considered globally available for the Player who owns them',
		);
	}

	public static getEntityCaption(t: Resources.resourceId) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIResourceDescriptor.getIcon(t)}
				<span className={UIResourceDescriptor.getCaptionClass()}>{UIResourceDescriptor.getCaption(t)}</span>
			</em>
		);
	}

	public static getCaptionClass() {
		return styles.resourceCaption;
	}

	public static getIcon(type: Resources.resourceId, className?: string, glow = false) {
		const w = 100;
		const cls = classnames(UIContainer.iconClass, styles.icon, className);
		if (!type)
			return (
				<SVGSmallTexturedIcon className={cls} style={TSVGSmallTexturedIconStyles.gold}>
					<use {...SVGIconContainer.getXLinkProps(SVGResourceIconId)} />
				</SVGSmallTexturedIcon>
			);
		return (
			<svg className={cls} viewBox={`0 0 ${w} ${w}`}>
				<g filter={glow ? SVGIconContainer.getUrlRef(SVGResourceIconFilterId) : null}>
					<image
						xlinkHref={iconMapping[type] || null}
						x={0}
						y={0}
						width={w}
						height={w}
						filter={SVGIconContainer.getUrlRef(SVGContrastGlowFilterID)}
					/>
				</g>
			</svg>
		);
	}

	public render() {
		return (
			<span
				className={classnames(
					UIResourceDescriptor.getClass(),
					this.props.className,
					this.props.tooltip ? UIContainer.hasTooltipClass : null,
				)}>
				{UIResourceDescriptor.getIcon(this.props.resource)}
				<span className={UIResourceDescriptor.getCaptionClass()}>
					{this.props.caption || UIResourceDescriptor.getCaption(this.props.resource)}
				</span>
				{!!this.props.tooltip && <UITooltip>{UIResourceDescriptor.getTooltip(this.props.resource)}</UITooltip>}
			</span>
		);
	}
}

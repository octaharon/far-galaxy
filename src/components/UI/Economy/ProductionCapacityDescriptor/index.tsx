import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import SVGIconContainer, {
	SVGBasicEmbossFilterID,
	SVGBasicGlowFilterID,
	SVGFilterWrapper,
} from '../../SVG/IconContainer';
import Tokenizer from '../../Templates/Tokenizer';
import UITooltip from '../../Templates/Tooltip';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import Style from './index.scss';

type IUIProductionCapacityDescriptorPureProps = {
	productionCapacity: number;
	output?: boolean;
	modifier?: IModifier;
};

export type UIProductionCapacityDescriptorProps = IUICommonProps &
	IUIProductionCapacityDescriptorPureProps & {
		short?: boolean;
	};

export const SVGProductionCapacityIcon = 'productionCapacity-point-icon-shape';
export const SVGProductionCapacityGradient = 'gradient-productionCapacity';

SVGIconContainer.registerGradient(
	SVGProductionCapacityGradient,
	<linearGradient x1="50%" y1="0%" x2="75%" y2="100%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor={getTemplateColor('light_golden')} />
		<stop offset="1" stopColor={getTemplateColor('light_teal')} />
	</linearGradient>,
).registerSymbol(
	SVGProductionCapacityIcon,
	<symbol viewBox="0 0 512 512">
		<g
			style={{
				transformOrigin: '50% 50%',
				transform: 'scale(1)',
			}}>
			<path d="M497,76c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15c8.291,0,15-6.709,15-15V91C512,82.709,505.291,76,497,76z" />
			<path
				d="M497,166c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15c8.291,0,15-6.709,15-15v-30C512,172.709,505.291,166,497,166
			z"
			/>
			<path
				d="M497,256c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15c8.291,0,15-6.709,15-15v-30C512,262.709,505.291,256,497,256
			z"
			/>
			<path
				d="M497,346c-8.291,0-15,6.709-15,15v40.796l-222.367-55.342c-2.373-0.615-4.893-0.615-7.266,0L30,401.796V361
			c0-8.291-6.709-15-15-15s-15,6.709-15,15v60c0,7.072,5.409,13.043,11.367,14.546l241,60c1.187,0.308,2.417,0.454,3.633,0.454
			s2.446-0.146,3.633-0.454l241-60C506.586,434.044,512,427.953,512,421v-60C512,352.709,505.291,346,497,346z"
			/>
			<path d="M15,76C6.709,76,0,82.709,0,91v30c0,8.291,6.709,15,15,15s15-6.709,15-15V91C30,82.709,23.291,76,15,76z" />
			<path d="M15,166c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15s15-6.709,15-15v-30C30,172.709,23.291,166,15,166z" />
			<path d="M15,256c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15s15-6.709,15-15v-30C30,262.709,23.291,256,15,256z" />
			<path d="M75,16c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15s15-6.709,15-15V31C90,22.709,83.291,16,75,16z" />
			<path d="M75,106c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15s15-6.709,15-15v-30C90,112.709,83.291,106,75,106z" />
			<path d="M75,196c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15s15-6.709,15-15v-30C90,202.709,83.291,196,75,196z" />
			<path d="M75,286c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15s15-6.709,15-15v-30C90,292.709,83.291,286,75,286z" />
			<path d="M437,16c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15c8.291,0,15-6.709,15-15V31C452,22.709,445.291,16,437,16z" />
			<path
				d="M437,106c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15c8.291,0,15-6.709,15-15v-30C452,112.709,445.291,106,437,106
			z"
			/>
			<path
				d="M437,196c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15c8.291,0,15-6.709,15-15v-30C452,202.709,445.291,196,437,196
			z"
			/>
			<path
				d="M437,286c-8.291,0-15,6.709-15,15v30c0,8.291,6.709,15,15,15c8.291,0,15-6.709,15-15v-30C452,292.709,445.291,286,437,286
			z"
			/>
			<path d="M262.973,17.714c-4.365-2.285-9.58-2.285-13.945,0L136.489,76.8L256,148.51L375.513,76.8L262.973,17.714z" />
			<path d="M121,102.491V226c0,5.171,2.666,9.976,7.046,12.715L241,309.311V174.492L121,102.491z" />
			<path d="M271,174.493v134.818l112.954-70.596c4.38-2.739,7.046-7.544,7.046-12.715V102.494L271,174.493z" />
		</g>
	</symbol>,
);

export default class UIProductionCapacityDescriptor extends React.PureComponent<
	UIProductionCapacityDescriptorProps,
	{}
> {
	public static getClass(): string {
		return Style.productionCapacityDescriptor;
	}

	public static getCaptionClass(): string {
		return Style.productionCapacityCaption;
	}

	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGFilterWrapper className={className} svgProps={{ viewBox: '0 0 512 512' }}>
				<g
					filter={SVGIconContainer.getUrlRef(SVGBasicGlowFilterID)}
					style={{
						transform: 'scale(0.95) translateY(-5%)',
						transformOrigin: '50% 50%',
					}}>
					<g
						fill={SVGIconContainer.getUrlRef(SVGProductionCapacityGradient)}
						filter={SVGIconContainer.getUrlRef(SVGBasicEmbossFilterID)}>
						<use {...SVGIconContainer.getXLinkProps(SVGProductionCapacityIcon)} />
					</g>
				</g>
			</SVGFilterWrapper>
		);
	}

	public static getCaption(output = false) {
		return !output ? 'Production Capacity' : 'Production Capacity';
	}

	public static getEntityCaption(output = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIProductionCapacityDescriptor.getIcon()}
				<span className={UIProductionCapacityDescriptor.getCaptionClass()}>
					{UIProductionCapacityDescriptor.getCaption(output)}
				</span>
			</em>
		);
	}

	public static getTooltip() {
		return termTooltipTemplate(
			UIProductionCapacityDescriptor.getIcon(UIContainer.tooltipIconClass),
			UIProductionCapacityDescriptor.getCaption(false),
			'Production Capacity describes the capability of the Starbase to mobilize in times of war and allows to instantly build Units at any of the operating Factories just before the Combat starts, spending no more than given amount of Raw Materials',
		);
	}

	public static getValue(props: IUIProductionCapacityDescriptorPureProps) {
		const { productionCapacity, modifier } = props;
		const modOnly = !Number.isFinite(productionCapacity);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = 100 < BasicMaths.applyModifier(100, modifier);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false).join('/').trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(productionCapacity) && (
					<span className={Style.productionCapacityValue}>
						{productionCapacity === 0 ? '-' : productionCapacity}
					</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	constructor(props: UIProductionCapacityDescriptorProps) {
		super(props);
	}

	public render() {
		const value = UIProductionCapacityDescriptor.getValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						UIProductionCapacityDescriptor.getClass(),
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIProductionCapacityDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && <UITooltip>{UIProductionCapacityDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(UIProductionCapacityDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIProductionCapacityDescriptor.getIcon()}
					<span className={UIProductionCapacityDescriptor.getCaptionClass()}>
						{UIProductionCapacityDescriptor.getCaption(this.props.output)}
					</span>
					{!!this.props.tooltip && <UITooltip>{UIProductionCapacityDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import SVGIconContainer, {
	SVGBasicEmbossFilterID,
	SVGBasicShadowFilterID,
	SVGFilterWrapper,
} from '../../SVG/IconContainer';
import UITooltip from '../../Templates/Tooltip';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import { SVGProductionCapacityGradient } from '../ProductionCapacityDescriptor';
import Style from './index.scss';

type IUIProductionSpeedDescriptorPureProps = {
	productionSpeed: number;
	output?: boolean;
	modifier?: IModifier;
};

export type UIProductionSpeedDescriptorProps = IUICommonProps &
	IUIProductionSpeedDescriptorPureProps & {
		short?: boolean;
	};

export const SVGProductionSpeedIconID = 'prod-speed-icon';

SVGIconContainer.registerSymbol(
	SVGProductionSpeedIconID,
	<symbol viewBox="0 0 133 143">
		<path d="M 62.5625 69.902344 L 54.328125 69.199219 L 48.988281 63.875 L 42.617188 57.511719 C 38.878906 69.414062 45.496094 82.089844 57.390625 85.824219 C 61.566406 87.136719 66.03125 87.207031 70.238281 86.035156 L 113.867188 129.820312 C 117.882812 134.304688 124.777344 134.679688 129.261719 130.664062 C 133.742188 126.644531 134.117188 119.75 130.101562 115.269531 C 129.835938 114.972656 129.558594 114.695312 129.261719 114.425781 L 85.707031 70.878906 C 89.371094 58.949219 82.671875 46.304688 70.742188 42.640625 C 66.515625 41.347656 62 41.3125 57.761719 42.554688 L 63.984375 48.777344 L 69.34375 54.140625 L 70.054688 62.367188 L 66.320312 66.097656 Z M 80.980469 79.390625 L 119.242188 117.660156 C 119.992188 118.367188 120.039062 119.546875 119.335938 120.300781 C 118.632812 121.058594 117.449219 121.097656 116.695312 120.394531 C 116.667969 120.367188 116.632812 120.332031 116.601562 120.300781 L 78.355469 82.015625 Z M 80.980469 79.390625 " />
		<path d="M 22.535156 88.597656 L 14.410156 105.242188 L 23.9375 114.761719 L 40.375 106.414062 C 44.023438 108.492188 47.925781 110.085938 51.984375 111.152344 L 57.996094 128.636719 L 71.472656 128.636719 L 77.195312 111.152344 C 80.398438 110.296875 83.5 109.105469 86.453125 107.597656 L 74.160156 95.300781 C 57.101562 100.617188 38.960938 91.089844 33.652344 74.03125 C 28.34375 56.96875 37.867188 38.832031 54.925781 33.523438 C 71.984375 28.210938 90.121094 37.738281 95.4375 54.796875 C 97.496094 61.414062 97.375 68.519531 95.089844 75.066406 L 107.234375 87.238281 C 109.003906 83.953125 110.386719 80.476562 111.351562 76.875 L 128.859375 70.878906 L 128.859375 57.390625 L 111.316406 51.664062 C 110.226562 47.605469 108.609375 43.714844 106.5 40.078125 L 114.617188 23.46875 L 105.097656 13.921875 L 88.660156 22.273438 C 85.003906 20.191406 81.101562 18.589844 77.042969 17.496094 L 71.039062 0 L 57.546875 0 L 51.824219 17.558594 C 47.777344 18.644531 43.882812 20.261719 40.253906 22.359375 L 23.625 14.25 L 14.082031 23.792969 L 22.449219 40.230469 C 20.359375 43.871094 18.746094 47.761719 17.644531 51.816406 L 0.222656 57.824219 L 0.222656 71.335938 L 17.78125 77.046875 C 18.847656 81.089844 20.445312 84.972656 22.535156 88.597656 Z M 22.535156 88.597656 " />
	</symbol>,
);

export default class UIProductionSpeedDescriptor extends React.PureComponent<UIProductionSpeedDescriptorProps, {}> {
	public static getClass(): string {
		return Style.productionSpeedDescriptor;
	}

	public static getCaptionClass(): string {
		return Style.productionSpeedCaption;
	}

	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGFilterWrapper className={className}>
				<g filter={SVGIconContainer.getUrlRef(SVGBasicShadowFilterID)}>
					<g
						fill={SVGIconContainer.getUrlRef(SVGProductionCapacityGradient)}
						filter={SVGIconContainer.getUrlRef(SVGBasicEmbossFilterID)}>
						<use {...SVGIconContainer.getXLinkProps(SVGProductionSpeedIconID)} />
					</g>
				</g>
			</SVGFilterWrapper>
		);
	}

	public static getCaption(output = false) {
		return !output ? 'Production Speed' : 'Production Speed';
	}

	public static getEntityCaption(output = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIProductionSpeedDescriptor.getIcon()}
				<span className={UIProductionSpeedDescriptor.getCaptionClass()}>
					{UIProductionSpeedDescriptor.getCaption(output)}
				</span>
			</em>
		);
	}

	public static getTooltip() {
		return termTooltipTemplate(
			UIProductionSpeedDescriptor.getIcon(UIContainer.tooltipIconClass),
			UIProductionSpeedDescriptor.getCaption(false),
			'Production Speed is the amount of Raw Materials that is consumed by Factories every Turn when Building Units. Higher values allow more reinforcements to be built in Combat, while lower values might make building Prototypes with higher Unit Cost straight useless',
		);
	}

	public static getValue(props: IUIProductionSpeedDescriptorPureProps) {
		const { productionSpeed, modifier } = props;
		const modOnly = !Number.isFinite(productionSpeed);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = 100 < BasicMaths.applyModifier(100, modifier);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false).join('/').trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(productionSpeed) && (
					<span className={Style.productionSpeedValue}>{productionSpeed === 0 ? '-' : productionSpeed}</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	constructor(props: UIProductionSpeedDescriptorProps) {
		super(props);
	}

	public render() {
		const value = UIProductionSpeedDescriptor.getValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						UIProductionSpeedDescriptor.getClass(),
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIProductionSpeedDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && <UITooltip>{UIProductionSpeedDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(UIProductionSpeedDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIProductionSpeedDescriptor.getIcon()}
					<span className={UIProductionSpeedDescriptor.getCaptionClass()}>
						{UIProductionSpeedDescriptor.getCaption(this.props.output)}
					</span>
					{!!this.props.tooltip && <UITooltip>{UIProductionSpeedDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

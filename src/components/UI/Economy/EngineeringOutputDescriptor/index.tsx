import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGFilterWrapper,
	SVGLargeGlowFilterID,
} from '../../SVG/IconContainer';
import UITooltip from '../../Templates/Tooltip';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import Style from './index.scss';

type IUIEngineeringPointsDescriptorPureProps = {
	engineeringPoints: number;
	output?: boolean;
	modifier?: IModifier;
};

export type UIEngineeringPointsDescriptorProps = IUICommonProps &
	IUIEngineeringPointsDescriptorPureProps & {
		short?: boolean;
	};

export const SVGEngineeringPointsIcon = 'engineering-point-icon-shape';
export const SVGEngineeringPointsGradient = 'engineering-engineering';

SVGIconContainer.registerGradient(
	SVGEngineeringPointsGradient,
	<linearGradient x1="40%" y1="0" x2="45%" y2="100%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor={getTemplateColor('light_teal')} />
		<stop offset="1" stopColor={getTemplateColor('light_green')} />
	</linearGradient>,
).registerSymbol(
	SVGEngineeringPointsIcon,
	<symbol viewBox="0 0 64 64">
		<g
			style={{
				transformOrigin: '50% 50%',
				//transform: 'scale(4)',
			}}>
			<path d="m12.871 17c.783-.797 1.701-1.457 2.713-1.958-.997-1.092-1.584-2.526-1.584-4.042 0-.698.126-1.366.346-1.99l-2.146-1.61c-.344-.258-.77-.4-1.2-.4h-8c0 1.103.897 2 2 2h4v2h-4-.001c.001 1.103.898 2 2.001 2h4v2h-4-.001c.001 1.103.898 2 2.001 2z" />
			<path d="m17.714 14.274c.474-.112.96-.191 1.457-.232.402-1.183 1.512-2.042 2.829-2.042v2c-.551 0-1 .449-1 1s.449 1 1 1v2c-1.287 0-2.378-.819-2.803-1.959-4.035.404-7.197 3.819-7.197 7.959 0 4.349 3.491 7.892 7.817 7.991.136-.233.287-.456.452-.669-.806-.907-1.269-2.087-1.269-3.322 0-2.757 2.243-5 5-5v2c-1.654 0-3 1.346-3 3 0 .73.269 1.429.74 1.97.94-.611 2.058-.97 3.26-.97.398 0 .799.04 1.19.118l-.394 1.961c-.261-.053-.529-.079-.796-.079-2.206 0-4 1.794-4 4s1.794 4 4 4 4-1.794 4-4v-29c0-1.654-1.346-3-3-3s-3 1.346-3 3 1.346 3 3 3v2c-2.344 0-4.302-1.628-4.84-3.808-.376-.114-.761-.192-1.16-.192-2.206 0-4 1.794-4 4 0 1.309.652 2.529 1.714 3.274z" />
			<path d="m20.236 38.633c-.155.774-.236 1.564-.236 2.367 0 4.003 1.978 7.546 5 9.727v-2.727h-2v-6h2v4h2v5.895c.639.294 1.307.531 2 .711v-6.606h2v6.949c.331.028.662.051 1 .051s.669-.023 1-.051v-4.949h2v4.605c.693-.179 1.361-.416 2-.711v-5.894h2v-2h2v4h-2v2.727c3.022-2.181 5-5.724 5-9.727 0-.803-.081-1.593-.236-2.367-1.097 1.435-2.822 2.367-4.764 2.367-3.309 0-6-2.691-6-6v-5.956c-.664-.054-1.336-.054-2 0v5.956c0 3.309-2.691 6-6 6-1.942 0-3.667-.931-4.764-2.367zm12.764 5.367h2v2h-2zm-4-2h2v2h-2z" />
			<path d="m33 27.038v-6.038h-2v6.038c.665-.046 1.335-.046 2 0z" />
			<path d="m8.999 19c.001.925.636 1.698 1.49 1.926.219-.677.513-1.32.865-1.925h-2.354c0-.001-.001-.001-.001-.001z" />
			<path d="m53 7c-.43 0-.856.142-1.199.4l-2.147 1.61c.22.624.346 1.292.346 1.99 0 1.516-.587 2.95-1.583 4.042 1.012.501 1.93 1.161 2.713 1.958h3.87c1.103 0 2-.897 2-2h-4v-2h4c1.103 0 2-.897 2-2h-4v-2h4c1.103 0 2-.897 2-2z" />
			<path d="m53.512 20.925c.854-.227 1.488-1 1.488-1.925h-2.353c.352.606.645 1.248.865 1.925z" />
			<path d="m46.802 34.913c-.169-.405-.278-.818-.342-1.231-.506.129-1.024.225-1.557.273.06.34.097.688.097 1.045 0 .241-.018.478-.046.712.687 1.68 1.046 3.456 1.046 5.288 0 7.72-6.28 14-14 14s-14-6.28-14-14c0-1.832.36-3.608 1.046-5.287-.028-.235-.046-.471-.046-.713 0-.357.037-.705.097-1.046-.532-.048-1.05-.144-1.555-.272-.062.404-.165.803-.324 1.184-.797 1.922-2.611 3.134-4.641 3.134h-.577v6h.577c2.03 0 3.844 1.212 4.621 3.087.808 1.933.386 4.083-1.054 5.523l-.41.41 4.246 4.246.41-.41c1.439-1.441 3.589-1.862 5.477-1.074 1.921.797 3.133 2.611 3.133 4.641v.577h6v-.577c0-2.03 1.212-3.844 3.087-4.621 1.932-.809 4.082-.387 5.523 1.054l.41.41 4.246-4.246-.41-.41c-1.44-1.44-1.862-3.59-1.074-5.477.797-1.921 2.611-3.133 4.641-3.133h.577v-6h-.577c-2.03 0-3.844-1.212-4.621-3.087z" />
			<path d="m44.803 16.041c-.425 1.14-1.516 1.959-2.803 1.959v-2c.551 0 1-.449 1-1s-.449-1-1-1v-2c1.317 0 2.427.859 2.829 2.042.497.041.983.12 1.457.232 1.062-.745 1.714-1.965 1.714-3.274 0-2.206-1.794-4-4-4-.399 0-.784.078-1.16.192-.538 2.18-2.496 3.808-4.84 3.808v-2c1.654 0 3-1.346 3-3s-1.346-3-3-3-3 1.346-3 3v29c0 2.206 1.794 4 4 4s4-1.794 4-4-1.794-4-4-4c-.267 0-.535.026-.796.079l-.394-1.961c.391-.078.792-.118 1.19-.118 1.202 0 2.32.359 3.26.97.471-.541.74-1.24.74-1.97 0-1.654-1.346-3-3-3v-2c2.757 0 5 2.243 5 5 0 1.235-.463 2.415-1.27 3.322.165.212.316.435.452.669 4.327-.099 7.818-3.642 7.818-7.991 0-4.14-3.161-7.555-7.197-7.959z" />
		</g>
	</symbol>,
);

export default class UIEngineeringPointsDescriptor extends React.PureComponent<UIEngineeringPointsDescriptorProps, {}> {
	public static getClass(): string {
		return Style.engineeringDescriptor;
	}

	public static getCaptionClass(): string {
		return Style.engineeringCaption;
	}

	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGFilterWrapper className={className}>
				<g
					filter={SVGIconContainer.getUrlRef(SVGLargeGlowFilterID)}
					style={{
						transform: 'scale(0.98)',
						transformOrigin: '50% 50%',
					}}>
					<g
						fill={SVGIconContainer.getUrlRef(SVGEngineeringPointsGradient)}
						filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}>
						<use {...SVGIconContainer.getXLinkProps(SVGEngineeringPointsIcon)} />
					</g>
				</g>
			</SVGFilterWrapper>
		);
	}

	public static getCaption(output = false) {
		return !output ? 'Engineering Points' : 'Engineering Output';
	}

	public static getEntityCaption(output = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIEngineeringPointsDescriptor.getIcon()}
				<span className={UIEngineeringPointsDescriptor.getCaptionClass()}>
					{UIEngineeringPointsDescriptor.getCaption(output)}
				</span>
			</em>
		);
	}

	public static getTooltip(output = false) {
		return termTooltipTemplate(
			UIEngineeringPointsDescriptor.getIcon(UIContainer.tooltipIconClass),
			UIEngineeringPointsDescriptor.getCaption(false),
			'Engineering Points are generated by Establishments, and are added up to achieve Engineering Goals, allowing to obtain Technologies that are only partly researched, improve them further  or to invest into the Economy by upgrading Buildings' +
				(output ? '. The modifier is applied to each of the operating Establishments' : ''),
		);
	}

	public static getValue(props: IUIEngineeringPointsDescriptorPureProps) {
		const { engineeringPoints, modifier } = props;
		const modOnly = !Number.isFinite(engineeringPoints);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = 100 < BasicMaths.applyModifier(100, modifier);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false).join('/').trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(engineeringPoints) && (
					<span className={Style.engineeringPoints}>{engineeringPoints === 0 ? '-' : engineeringPoints}</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	constructor(props: UIEngineeringPointsDescriptorProps) {
		super(props);
	}

	public render() {
		const value = UIEngineeringPointsDescriptor.getValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						UIEngineeringPointsDescriptor.getClass(),
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIEngineeringPointsDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && (
						<UITooltip>{UIEngineeringPointsDescriptor.getTooltip(this.props.output)}</UITooltip>
					)}
				</span>
			);
		return (
			<dl
				className={classnames(UIEngineeringPointsDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIEngineeringPointsDescriptor.getIcon()}
					<span className={UIEngineeringPointsDescriptor.getCaptionClass()}>
						{UIEngineeringPointsDescriptor.getCaption(this.props.output)}
					</span>
					{!!this.props.tooltip && (
						<UITooltip>{UIEngineeringPointsDescriptor.getTooltip(this.props.output)}</UITooltip>
					)}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

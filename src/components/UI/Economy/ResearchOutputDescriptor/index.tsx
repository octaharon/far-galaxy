import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGBasicGlowFilterID,
	SVGFilterWrapper,
	SVGLargeGlowFilterID,
} from '../../SVG/IconContainer';
import UITooltip from '../../Templates/Tooltip';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import Style from './index.scss';

type IUIResearchPointsDescriptorPureProps = {
	researchPoints: number;
	output?: boolean;
	modifier?: IModifier;
};

export type UIResearchPointsDescriptorProps = IUICommonProps &
	IUIResearchPointsDescriptorPureProps & {
		short?: boolean;
	};

export const SVGResearchPointsIcon = 'research-point-icon-shape';
export const SVGResearchPointsGradient = 'gradient-research';

SVGIconContainer.registerGradient(
	SVGResearchPointsGradient,
	<linearGradient x1="70%" y1="0" x2="25%" y2="100%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor={getTemplateColor('light_orange')} />
		<stop offset="1" stopColor={getTemplateColor('light_golden')} />
	</linearGradient>,
).registerSymbol(
	SVGResearchPointsIcon,
	<symbol viewBox="0 0 64 64">
		<g
			style={{
				transformOrigin: '50% 50%',
				transform: 'scale(1, 1)',
			}}>
			<path d="m11.825 29c-.551 0-1 .448-1 1s.449 1 1 1h8.929l1.797-1.79c.343-.343.426-.866.216-1.368-.219-.529-.655-.842-1.128-.842h-2.639v2z" />
			<path d="m4 15h9v2h-7c-.551 0-1 .448-1 1s.449 1 1 1h9v2h-7c-.551 0-1 .448-1 1s.449 1 1 1h9v2h-7.175c-.551 0-1 .448-1 1s.449 1 1 1h9.175v-6h2.639c.473 0 .909-.313 1.11-.8.225-.536.141-1.073-.197-1.409l-1.76-1.753c-1.614-.866-2.889-2.295-3.529-4.038h-13.263c-.551 0-1 .448-1 1s.449 1 1 1z" />
			<path d="m23.516 31.071 1.413 1.413.444-.446c.915-.919 2.312-1.174 3.56-.648 1.268.526 2.067 1.686 2.067 2.97v.64h2v-.64c0-1.284.799-2.444 2.035-2.957 1.281-.538 2.678-.284 3.592.635l.445.446 1.413-1.413-.447-.445c-.875-.87-1.15-2.164-.719-3.376.019-.054.07-.182.084-.216.513-1.235 1.673-2.034 2.958-2.034h.639v-2h-.639c-1.285 0-2.445-.799-2.958-2.034-.539-1.282-.284-2.68.635-3.593l.218-.217-.114-.57-1.07-1.071-.444.446c-.915.919-2.311 1.174-3.56.648-1.269-.525-2.068-1.685-2.068-2.969v-.64h-2v.64c0 1.284-.799 2.444-2.035 2.957-1.279.54-2.678.284-3.592-.635l-.445-.446-1.07 1.071-.114.57.219.218c.918.912 1.173 2.31.648 3.558-.527 1.268-1.687 2.067-2.972 2.067h-.639v2h.639c1.285 0 2.445.799 2.958 2.034.538 1.281.284 2.678-.634 3.592zm8.484-13.071c3.31 0 6 2.69 6 6s-2.69 6-6 6-6-2.69-6-6 2.69-6 6-6z" />
			<path d="m32 61c2.206 0 4-1.794 4-4h-8c0 2.206 1.794 4 4 4z" />
			<path d="m18.97 11.758c.42 1.678 1.619 3.039 3.174 3.713l2.788-2.787 1.858 1.868c.341.343.865.425 1.367.215.536-.222.843-.648.843-1.127v-2.64h6v2.64c0 .479.307.905.8 1.109.544.229 1.067.145 1.41-.197l1.858-1.868 2.788 2.787c1.555-.674 2.755-2.035 3.174-3.713l.189-.758h2.261c-3.543-4.971-9.318-8-15.48-8s-11.938 3.029-15.481 8h2.261z" />
			<path d="m41.251 27.8-.049.122c-.168.474-.074.968.247 1.287l1.797 1.791h8.754c.551 0 1-.448 1-1s-.449-1-1-1h-7v-2h-2.639c-.473 0-.909.313-1.11.8z" />
			<path d="m43.208 17.038-1.759 1.752c-.338.337-.423.874-.216 1.368.219.529.655.842 1.128.842h2.639v6h9.175c.551 0 1-.448 1-1s-.449-1-1-1h-7.175v-2h9c.551 0 1-.448 1-1s-.449-1-1-1h-7v-2h9c.551 0 1-.448 1-1s-.449-1-1-1h-7v-2h9c.551 0 1-.448 1-1s-.449-1-1-1h-13.263c-.64 1.743-1.915 3.172-3.529 4.038z" />
			<path d="m26 49h12v2h-12z" />
			<path d="m47.938 33h-6.554l-2.317 2.316-1.858-1.868c-.341-.342-.865-.425-1.367-.215-.535.222-.842.648-.842 1.127v2.64h-6v-2.64c0-.479-.307-.905-.8-1.109-.544-.229-1.067-.146-1.41.197l-1.858 1.868-2.317-2.316h-6.554c1.634 2.646 3.7 4.795 5.56 6.72 2.392 2.477 4.475 4.653 5.178 7.28h10.402c.703-2.627 2.786-4.803 5.178-7.28 1.86-1.925 3.926-4.074 5.559-6.72z" />
			<path d="m26 53h12.001v2h-12.001z" />
			<circle cx="32" cy="24" r="2" />
			<path d="m54 55c-1.302 0-2.402.839-2.816 2h-11.182-2.002c0 .702-.128 1.373-.35 2h13.534c.414 1.161 1.514 2 2.816 2 1.654 0 3-1.346 3-3s-1.346-3-3-3z" />
			<path d="m52 45c1.654 0 3-1.346 3-3s-1.346-3-3-3c-1.302 0-2.402.839-2.816 2h-5.262c-.033.035-.071.075-.105.109-.63.652-1.228 1.278-1.783 1.891h7.15c.414 1.161 1.514 2 2.816 2z" />
			<path d="m60 47c-1.302 0-2.402.839-2.816 2h-17.184v2h17.184c.414 1.161 1.514 2 2.816 2 1.654 0 3-1.346 3-3s-1.346-3-3-3z" />
			<path d="m26 57h-2-11.184c-.414-1.161-1.514-2-2.816-2-1.654 0-3 1.346-3 3s1.346 3 3 3c1.302 0 2.402-.839 2.816-2h13.534c-.222-.627-.35-1.298-.35-2z" />
			<path d="m24 51v-2h-17.184c-.414-1.161-1.514-2-2.816-2-1.654 0-3 1.346-3 3s1.346 3 3 3c1.302 0 2.402-.839 2.816-2z" />
			<path d="m20.078 41h-5.262c-.414-1.161-1.514-2-2.816-2-1.654 0-3 1.346-3 3s1.346 3 3 3c1.302 0 2.402-.839 2.816-2h7.149c-.555-.613-1.153-1.238-1.783-1.891-.033-.034-.071-.074-.104-.109z" />
		</g>
	</symbol>,
);

export default class UIResearchPointsDescriptor extends React.PureComponent<UIResearchPointsDescriptorProps, {}> {
	public static getClass(): string {
		return Style.researchDescriptor;
	}

	public static getCaptionClass(): string {
		return Style.researchCaption;
	}

	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGFilterWrapper className={className}>
				<g
					filter={SVGIconContainer.getUrlRef(SVGLargeGlowFilterID)}
					style={{
						transform: 'scale(1)',
						transformOrigin: '50% 50%',
					}}>
					<g
						fill={SVGIconContainer.getUrlRef(SVGResearchPointsGradient)}
						filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}>
						<use {...SVGIconContainer.getXLinkProps(SVGResearchPointsIcon)} />
					</g>
				</g>
			</SVGFilterWrapper>
		);
	}

	public static getCaption(output = false) {
		return !output ? 'Research Points' : 'Research Output';
	}

	public static getEntityCaption(output = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIResearchPointsDescriptor.getIcon()}
				<span className={UIResearchPointsDescriptor.getCaptionClass()}>
					{UIResearchPointsDescriptor.getCaption(output)}
				</span>
			</em>
		);
	}

	public static getTooltip(output = false) {
		return termTooltipTemplate(
			UIResearchPointsDescriptor.getIcon(UIContainer.tooltipIconClass),
			UIResearchPointsDescriptor.getCaption(false),
			'Research Points are generated by Establishments and are added up for Research Projects (including those of Factions), if some Research Focus is selected by the Player' +
				(output ? '. The modifier is applied to each of the Establishments that are in operating mode' : ''),
		);
	}

	public static getValue(props: IUIResearchPointsDescriptorPureProps) {
		const { researchPoints, modifier } = props;
		const modOnly = !Number.isFinite(researchPoints);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = 100 < BasicMaths.applyModifier(100, modifier);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false).join('/').trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(researchPoints) && (
					<span className={Style.researchPoints}>{researchPoints === 0 ? '-' : researchPoints}</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	constructor(props: UIResearchPointsDescriptorProps) {
		super(props);
	}

	public render() {
		const value = UIResearchPointsDescriptor.getValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						UIResearchPointsDescriptor.getClass(),
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIResearchPointsDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && (
						<UITooltip>{UIResearchPointsDescriptor.getTooltip(this.props.output)}</UITooltip>
					)}
				</span>
			);
		return (
			<dl
				className={classnames(UIResearchPointsDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIResearchPointsDescriptor.getIcon()}
					<span className={UIResearchPointsDescriptor.getCaptionClass()}>
						{UIResearchPointsDescriptor.getCaption(this.props.output)}
					</span>
					{!!this.props.tooltip && (
						<UITooltip>{UIResearchPointsDescriptor.getTooltip(this.props.output)}</UITooltip>
					)}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

import { load, on } from 'kontra';
import { setLoaderTitle } from './tools';

export const preloadResources = (resources: Array<string[] | string> = [], cb?: CallableFunction) => {
	let resourcesLoaded = 0;
	const dictionary = Object.fromEntries(
		resources.flatMap((resourceList: string | string[]) =>
			[]
				.concat(resourceList)
				.reduce((arr, resourceUrl) => [...arr, [resourceUrl, false]], [] as Array<[string, boolean]>),
		),
	);
	const totalResources = Object.keys(dictionary).length;
	on('assetLoaded', (asset: any, url: string) => {
		dictionary[url] = true;
		resourcesLoaded++;
		setLoaderTitle(`Loading assets: ${resourcesLoaded}/${totalResources}`);
		if (cb) cb(url, asset);
	});

	return Promise.all(
		Object.keys(dictionary).map((resourceUrl) =>
			load(String(resourceUrl)).then((objects) => {
				return objects[0];
			}),
		),
	).then((resources) => {
		return resources;
		/*	const r = images.map((img) => img.src);
    images.forEach((img) => img.remove());
    return r;*/
	});
};

export const preloadImage = (src: string) =>
	src
		? new Promise<HTMLImageElement>((resolve, reject) => {
				const img = new Image();
				img.onload = () => {
					resolve(img);
				};
				img.onerror = (err) => reject(new Error(`Loading ${src} failed: ${String(err)}`));
				img.src = src;
		  })
		: null;

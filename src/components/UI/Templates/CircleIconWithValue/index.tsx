import * as React from 'react';
import icon_frame from '../../../../../assets/img/svg/UI/silver_frame_small.svg';
import { IUICommonProps } from '../../index';
import Style from './index.scss';

export type UICircleIconWithValueProps = IUICommonProps & {
	circleClassName?: string;
	iconContent?: React.ReactNode;
	tooltip?: string;
	withFrame?: boolean;
};

export default class UICircleIconWithValue extends React.PureComponent<UICircleIconWithValueProps, {}> {
	public static getClass(): string {
		return Style.iconWithValue;
	}

	public static getIconClass(): string {
		return Style.icon;
	}

	public static getFrameClass(): string {
		return Style.frame;
	}

	public render() {
		return (
			<ins
				title={this.props.tooltip || ''}
				className={[this.props.className || '', UICircleIconWithValue.getClass()].join(' ')}
			>
				<div className={[this.props.circleClassName, UICircleIconWithValue.getIconClass()].join(' ')}>
					<div className={Style.svg}>{this.props.iconContent}</div>
				</div>
				{this.props.children}
				{this.props.withFrame && <img src={icon_frame} className={UICircleIconWithValue.getFrameClass()} />}
			</ins>
		);
	}
}

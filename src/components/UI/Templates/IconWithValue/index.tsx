import * as React                    from "react";
import UIContainer, {IUICommonProps} from "../../index";
import {UISVGIconViewbox}            from "../../SVG/IconContainer";
import UITooltip                     from "../Tooltip";
import Style                         from './index.scss';

export type UIIconWithValueProps = IUICommonProps & {
    iconClassName?: string;
    svgContent?: React.ReactNode;
    viewBox?: number;
};

export default class UIIconWithValue extends React.PureComponent<UIIconWithValueProps, {}> {

    public static getClass(): string {
        return Style.iconWithValue;
    }

    public static getIconClass(): string {
        return Style.icon;
    }

    public render() {
        return <ins
            className={[
                this.props.className || '',
                Style.iconWithValue,
                this.props.tooltip ? UIContainer.hasTooltipClass : ''
            ].join(' ')}
        >
            {this.props.tooltip && <UITooltip>{this.props.tooltip}</UITooltip>}
            <svg className={`${Style.icon} ${this.props.iconClassName || ''}`}
                 xmlns="http://www.w3.org/2000/svg" shapeRendering="geometricPrecision"
                 viewBox={`0 0 ${this.props.viewBox || UISVGIconViewbox[0]} ${this.props.viewBox ||
                 UISVGIconViewbox[1]}`}>
                {this.props.svgContent}
            </svg>
            {this.props.children}
        </ins>;
    }
}
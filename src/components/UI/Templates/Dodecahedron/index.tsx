import React from 'react';
import { classnames, IUICommonProps } from '../../index';
import style from './index.scss';

type UIDodecahedronType = 'red' | 'blue' | 'green';
const ColorClassnames: Record<UIDodecahedronType, string> = {
	red: style.red,
	blue: style.blue,
	green: style.green,
};

const UIDodecahedron: React.FC<
	{
		type: UIDodecahedronType;
	} & IUICommonProps
> = ({ className, type }) => (
	<div className={classnames(style.view, className)}>
		<div className={classnames(style.plane, style.main)}>
			{new Array(6).fill(0).map((d, ix) => (
				<div key={ix} className={classnames(style.circle, ColorClassnames[type])} />
			))}
		</div>
	</div>
);

export default UIDodecahedron;

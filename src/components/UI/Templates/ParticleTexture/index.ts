import Vectors from '../../../../../definitions/maths/Vectors';
import { doesQualitySettingIncludes } from '../../../../contexts/GraphicSettings/helpers';
import { PersistentConfiguration } from '../../../../stores/persistentConfiguration';

const defaultOptions = {
	numParticles: 50,
	colorAlpha: 0.6,
	minSpeedX: -1,
	maxSpeedX: 1,
	maxSpeedY: 1,
	minSpeedY: -1,
	minRadius: 0.5,
	connectionRange: 10,
	maxRadius: 2,
	colors: ['#874773', '#ab5105', '#3F0545', '#b10500'],
};
type optionsType = typeof defaultOptions;
type IParticle = {
	x: number;
	y: number;
	rad: number;
	vx: number;
	vy: number;
	rgba?: string;
};

const createParticle: (params: optionsType & { width: number; height: number }) => IParticle = ({
	colors,
	width,
	height,
	maxSpeedX,
	maxSpeedY,
	minSpeedX,
	minSpeedY,
	maxRadius,
	minRadius,
}) => ({
	x: Math.random() * width,
	y: Math.random() * height,
	rad: Math.random() * (maxRadius - minRadius) + minRadius,
	rgba: colors[Math.floor(Math.random() * colors.length)],
	vx: Math.random() * (maxSpeedX - minSpeedX) + minSpeedX,
	vy: Math.random() * (maxSpeedY - minSpeedY) + minSpeedY,
});

class ParticlesFactory {
	protected _width: number;
	protected _height: number;
	protected _options: optionsType;
	protected _canvas: HTMLCanvasElement;
	protected _context: CanvasRenderingContext2D;
	protected _destroy: boolean;
	protected _lastRender: number;
	protected _particles: IParticle[];

	constructor(rootElement: HTMLElement, options?: Partial<optionsType>) {
		this._canvas = null;
		this._context = null;
		this._width = 0;
		this._height = 0;
		this._particles = [];
		this._destroy = false;
		this._lastRender = 0;
		this._options = Object.assign({}, defaultOptions);
		this.draw = this.draw.bind(this);
		this.setOptions(options);
		this.init(rootElement);
	}

	destroy() {
		for (let i = 0; i < this._particles.length; i++) delete this._particles[i];
		this._destroy = true;
	}

	setOptions(options: Partial<optionsType>) {
		if (typeof options === 'object') {
			this._options = Object.assign({}, this._options, options);
		}
	}

	// Initialize the ribbons effect
	init(rootElement: HTMLElement) {
		if (!doesQualitySettingIncludes(PersistentConfiguration.getState().graphicSettings.qualityLevel, 'Med'))
			return false;
		try {
			rootElement = rootElement || document?.body;
			this._canvas = document.createElement('canvas');
			this._canvas.style['display'] = 'block';
			this._canvas.style['margin'] = '0';
			this._canvas.style['padding'] = '0';
			this._canvas.style['border'] = '0';
			this._canvas.style['outline'] = '0';
			this._canvas.style['left'] = '0';
			this._canvas.style['top'] = '0';
			this._canvas.style['width'] = '100%';
			this._canvas.style['height'] = '100%';
			this._width = rootElement.offsetWidth;
			this._height = rootElement.offsetHeight;
			this._canvas.width = this._width;
			this._canvas.height = this._height;

			this._context = this._canvas.getContext('2d');
			this._context.globalAlpha = this._options.colorAlpha;

			this._context.clearRect(0, 0, this._width, this._height);
			this._particles = new Array(this._options.numParticles).fill(0).map((i) =>
				createParticle({
					...this._options,
					width: this._width,
					height: this._height,
				}),
			);

			rootElement.appendChild(this._canvas);
		} catch (e) {
			console.warn('Canvas Context Error: ' + e.toString());
			return;
		}
		requestAnimationFrame(this.draw);
	}

	draw() {
		this._context.clearRect(0, 0, this._width, this._height);
		this._context.globalCompositeOperation = 'lighter';
		for (let i = 0; i < this._options.numParticles; i++) {
			const temp = this._particles[i];
			if (!temp) continue;
			let factor = 1;

			this._context.fillStyle = temp.rgba;
			this._context.strokeStyle = temp.rgba;

			for (let j = 0; j < this._options.numParticles; j++) {
				const temp2 = this._particles[j];
				this._context.lineWidth = 0.5;

				if (
					temp.rgba === temp2.rgba &&
					Vectors.module()(Vectors.create(temp, temp2)) < this._options.connectionRange
				) {
					this._context.beginPath();
					this._context.moveTo(temp.x, temp.y);
					this._context.lineTo(temp2.x, temp2.y);
					this._context.stroke();
					factor++;
				}
			}

			this._context.beginPath();
			this._context.arc(temp.x, temp.y, temp.rad * factor, 0, Math.PI * 2, true);
			this._context.fill();
			this._context.closePath();

			this._context.beginPath();
			//this._context.arc(temp.x, temp.y, (temp.rad+5)*factor, 0, Math.PI*2, true);
			this._context.stroke();
			this._context.closePath();

			temp.x += temp.vx;
			temp.y += temp.vy;

			if (temp.x > this._width) temp.x = 0;
			if (temp.x < 0) temp.x = this._width;
			if (temp.y > this._height) temp.y = 0;
			if (temp.y < 0) temp.y = this._height;
		}
		if (doesQualitySettingIncludes(PersistentConfiguration.getState().graphicSettings.qualityLevel, 'Max'))
			requestAnimationFrame(this.draw);
	}
}

export default ParticlesFactory;

import * as React       from "react";
import {IUICommonProps} from "../../index";
import SVGHexIcon       from "../../SVG/HexIcon";
import Style            from './index.scss';

export type UIHexIconWithValueProps = IUICommonProps & {
    iconContent?: React.ReactNode;
    iconClassName?: string;
    transparent?: boolean;
    colorTop: string;
    colorBottom: string;
    tooltip?: string;
};

export default class UIHexIconWithValue extends React.PureComponent<UIHexIconWithValueProps, {}> {

    public static getClass(): string {
        return Style.iconWithValue;
    }

    public static getIconClass(): string {
        return Style.icon;
    }

    public static getFrameClass(): string {
        return Style.frame;
    }

    public render() {
        return <ins title={this.props.tooltip || ''}
                    className={[
                        this.props.className || '',
                        UIHexIconWithValue.getClass()
                    ].join(' ')}>
            <div className={[
                this.props.iconClassName,
                UIHexIconWithValue.getIconClass()
            ].join(' ')}>
                <SVGHexIcon className={Style.svg} colorTop={this.props.colorTop} colorBottom={this.props.colorBottom}
                            transparent={this.props.transparent || false}>
                    {this.props.iconContent}
                </SVGHexIcon>
            </div>
            {this.props.children}
        </ins>;
    }
}
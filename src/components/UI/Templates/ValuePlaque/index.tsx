import * as React                   from "react";
import icon_frame                   from "../../../../../assets/img/svg/UI/silver_frame_small.svg";
import {classnames, IUICommonProps} from "../../index";
import Style                        from './index.scss';

export type UIIconValuePlaqueProps = IUICommonProps & {
    iconClassName?: string;
    withFrame?: boolean;
    icon?: React.ReactNode;
};

export default class UIIconValuePlaque extends React.PureComponent<UIIconValuePlaqueProps, {}> {

    public static getClass(): string {
        return Style.valuePlaque;
    }

    public static getIconClass(): string {
        return Style.icon;
    }

    public static getFrameClass(): string {
        return Style.frame;
    }

    public render() {
        return <ins className={classnames(
                        this.props.className || '',
                        UIIconValuePlaque.getClass()
                    )}
        >
            <span className={classnames(
                UIIconValuePlaque.getIconClass(),
                this.props.iconClassName
            )}>
                {this.props.icon}
            </span>
            {this.props.children}
            {!!this.props.withFrame && <img src={icon_frame} className={Style.frame}/>}
        </ins>;
    }
}
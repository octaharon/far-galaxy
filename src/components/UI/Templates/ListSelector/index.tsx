import React from 'react';
import { classnames, IUICommonProps } from '../../index';
import SVGIconContainer, { SVGIconArrowLeftID, SVGIconArrowRightID } from '../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../SVG/SmallTexturedIcon';
import styles from './index.scss';

type TUIListSelectorProps<ItemType extends string | number> = IUICommonProps & {
	options: ItemType[];
	selectedItem?: ItemType;
	selectedIndex?: number;
	optionRender: (t: ItemType) => JSX.Element | React.ComponentType;
	onSelect: (t: ItemType) => any;
	buttonColor: TSVGSmallTexturedIconStyles;
};
export const UIListSelector = <ItemType extends string | number>(
	props: React.PropsWithChildren<TUIListSelectorProps<ItemType>>,
) => {
	const canRotate = (props.options || []).length > 1;
	const curIndex = canRotate
		? props.selectedIndex >= 0
			? props.selectedIndex
			: Math.max(0, props.options?.findIndex((v) => v === props.selectedItem) || 0)
		: 0;
	const prevItem = canRotate
		? curIndex > 0
			? props.options[curIndex - 1]
			: props.options[props.options.length - 1]
		: null;
	const nextItem = canRotate
		? curIndex < props.options.length - 1
			? props.options[curIndex + 1]
			: props.options[0]
		: null;
	return (
		<div className={classnames(styles.listSelector, props.className)}>
			<span className={styles.listSelectorContent}>
				<>
					{canRotate && (
						<span
							className={[styles.scrollButton, styles.scrollLeft].join(' ')}
							onClick={() => props.onSelect(prevItem)}>
							<SVGSmallTexturedIcon style={props.buttonColor} className={styles.icon}>
								<use {...SVGIconContainer.getXLinkProps(SVGIconArrowLeftID)} />
							</SVGSmallTexturedIcon>
						</span>
					)}
					{props.optionRender(props.selectedItem)}
					{canRotate && (
						<span
							className={[styles.scrollButton, styles.scrollRight].join(' ')}
							onClick={() => props.onSelect(nextItem)}>
							<SVGSmallTexturedIcon style={props.buttonColor} className={styles.icon}>
								<use {...SVGIconContainer.getXLinkProps(SVGIconArrowRightID)} />
							</SVGSmallTexturedIcon>
						</span>
					)}
				</>
			</span>
		</div>
	);
};

export default UIListSelector;

import React, { useState } from 'react';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import { classnames } from '../../../index';
import styles from './index.scss';

export type UITabsList<K extends number | string = string> = Array<{
	caption: JSX.Element | string;
	content: JSX.Element | string;
	id: K;
	selected?: boolean;
}>;
export type UITabsWrapperProps<K extends number | string = string> = React.PropsWithChildren<{
	className?: string;
	tabClassName?: string;
	selectedTabClassName?: string;
	onSelect?: (tabId: K, event: Event) => any;
	tabs: UITabsList<K>;
}>;

const UITabsWrapper: React.FC<UITabsWrapperProps> = ({
	onSelect = () => null,
	children,
	className,
	tabClassName,
	selectedTabClassName,
	tabs,
}) => {
	const [tabIndex, setTabIndex] = useState(
		Math.max(
			0,
			tabs.findIndex((t) => !!t.selected),
		),
	);
	React.useEffect(() => {
		setTabIndex(
			Math.max(
				0,
				tabs.findIndex((t) => !!t.selected),
			),
		);
	}, [onSelect, tabs]);
	return (
		<Tabs
			className={classnames(className, styles.tabsWrapper)}
			selectedIndex={tabIndex}
			onSelect={(index, lastIndex, e) => {
				setTabIndex(index);
				return onSelect(tabs[index].id, e);
			}}>
			<dl className={styles.tabsHeader}>
				<dt>
					<TabList className={styles.tabsList}>
						{tabs.map((t) => (
							<Tab
								key={t.id}
								className={classnames(tabClassName, styles.tabItem)}
								selectedClassName={classnames(selectedTabClassName, styles.selectedItem)}>
								{t.caption}
							</Tab>
						))}
					</TabList>
				</dt>
			</dl>
			{tabs.map((t) => (
				<TabPanel key={t.id}>{t.content}</TabPanel>
			))}
			{children}
		</Tabs>
	);
};

export default UITabsWrapper;

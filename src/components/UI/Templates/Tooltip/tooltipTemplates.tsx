import * as React from 'react';
import Tokenizer from '../Tokenizer';

export const termTooltipTemplate = (
	icon: JSX.Element | string,
	title: JSX.Element | string,
	body: string,
	extra?: JSX.Element | string,
) => (
	<React.Fragment>
		{icon}
		<strong>{title}</strong>
		{!!body && (
			<>
				<hr />
				<Tokenizer description={' ' + body?.trim()} />
				{extra}
			</>
		)}
	</React.Fragment>
);

export const basicTooltipTemplate = (icon: JSX.Element | string, title: JSX.Element | string, body: string) => (
	<React.Fragment>
		{icon}
		<b>{title}</b> <Tokenizer description={' ' + body?.trim()} />
	</React.Fragment>
);

import * as React from 'react';
import MouseTooltip from 'react-sticky-mouse-tooltip';
import UIContainer, { classnames, IUICommonProps } from '../../index';
import Style from './index.scss';

type UITooltipProps = IUICommonProps & {};
type UITooltipState = {
	visible: boolean;
};

export const TOOLTIP_HOVER_TIMEOUT = 250;

export default class UITooltip extends React.PureComponent<UITooltipProps, UITooltipState> {
	private readonly ref: React.RefObject<HTMLElement>;
	private tooltipContainer: HTMLElement;

	constructor(props: UITooltipProps) {
		super(props);
		this.ref = React.createRef();
		this.tooltipContainer = null;
		this.bindListeners = this.bindListeners.bind(this);
		this.enable = this.enable.bind(this);
		this.disable = this.disable.bind(this);
		this.state = {
			visible: false,
		};
		this.setState = this.setState.bind(this);
	}

	public static getClass(): string {
		return Style.tooltip;
	}

	public static getIconClass(): string {
		return UIContainer.tooltipIconClass;
	}

	public enable() {
		this.setState({ visible: true });
	}

	public disable() {
		this.setState({ visible: false });
	}

	protected bindListeners() {
		let parent = this.ref.current as HTMLElement;
		while (parent.parentElement && !parent.classList.contains(UIContainer.hasTooltipClass)) {
			parent = parent.parentElement;
		}
		if (!parent) return null;
		if (!parent.classList.contains(UIContainer.hasTooltipClass)) return false;
		parent.addEventListener('pointerenter', this.enable);
		parent.addEventListener('pointerleave', this.disable);
		this.tooltipContainer = parent;
	}

	public componentWillUnmount() {
		if (this.tooltipContainer) {
			this.tooltipContainer.removeEventListener('pointerenter', this.enable);
			this.tooltipContainer.removeEventListener('pointerleave', this.disable);
		}
	}

	public componentDidMount() {
		if (this.ref.current) this.bindListeners();
	}

	public render() {
		return (
			<span ref={this.ref} className={Style.tooltip}>
				{this.state.visible ? (
					<MouseTooltip visible={this.state.visible} offsetX={5} offsetY={5} style={{ zIndex: 50000 }}>
						<div
							className={classnames(
								Style.tooltipInner,
								UIContainer.tooltipClass,
								this.props.className || '',
							)}
						>
							{this.props.children}
						</div>
					</MouseTooltip>
				) : null}
			</span>
		);
	}
}

import React from 'react';
import { TBaseBuildingType } from '../../../../../definitions/orbital/types';
import UnitAttack from '../../../../../definitions/tactical/damage/attack';
import Damage from '../../../../../definitions/tactical/damage/damage';
import UnitDefense from '../../../../../definitions/tactical/damage/defense';
import { AbilityPowerUIToken } from '../../../../../definitions/tactical/statuses/constants';
import UnitActions from '../../../../../definitions/tactical/units/actions/types';
import { ChassisClassTitles } from '../../../../../definitions/technologies/chassis';
import UnitChassis from '../../../../../definitions/technologies/types/chassis';
import WeaponGroups from '../../../../../definitions/technologies/weapons/Groups';
import UIBuildingDescriptor from '../../Base/Buildings/BuildingDescriptor';
import UIBuildingTypeDescriptor from '../../Base/Buildings/BuildingTypeDescriptor';
import UIEnergyDescriptor, { UIEnergyDescriptorType } from '../../Economy/EnergyDescriptor';
import UIEngineeringPointsDescriptor from '../../Economy/EngineeringOutputDescriptor';
import UIProductionCapacityDescriptor from '../../Economy/ProductionCapacityDescriptor';
import UIRawMaterialsDescriptor, { UIRawMaterialsDescriptorType } from '../../Economy/RawMaterialsDescriptor';
import UIRepairPointsDescriptor from '../../Economy/RepairOutputDescriptor';
import UIResearchPointsDescriptor from '../../Economy/ResearchOutputDescriptor';
import UIContainer from '../../index';
import UIAbilityPowerDescriptor from '../../Parts/Ability/AbilityPowerDescriptor';
import UICooldownDescriptor from '../../Parts/Ability/CooldownDescriptor';
import UIDurationDescriptor from '../../Parts/Ability/DurationDescriptor';
import UIArmorDescriptor from '../../Parts/Armor/ArmorDescriptor';
import UIAoeRadiusDescriptor from '../../Parts/Attack/AOERadiusDescriptor';
import UIAPTDescriptor from '../../Parts/Attack/APTDescriptor';
import UIAttackDescriptor from '../../Parts/Attack/AttackDescriptor';
import UIAttackDeliveryDescriptor from '../../Parts/Attack/DeliveryDescriptor';
import UIHitChanceDescriptor from '../../Parts/Attack/HitChanceDescriptor';
import UIAttackPriorityDescriptor from '../../Parts/Attack/PriorityDescriptor';
import UIAttackRangeDescriptor from '../../Parts/Attack/RangeDescriptor';
import UIAttackRateDescriptor from '../../Parts/Attack/RateDescriptor';
import UIActionDescriptor from '../../Parts/Chassis/ActionDescriptor';
import UIChassisClassDescriptor from '../../Parts/Chassis/ChassisClassDescriptor';
import UIHitPointsInlineDescriptor from '../../Parts/Chassis/HitPointsInlineDescriptor';
import UIScanRangeDescriptor from '../../Parts/Chassis/ScanRangeDescriptor';
import UISpeedDescriptor from '../../Parts/Chassis/SpeedDescriptor';
import UITargetTypeDescriptor from '../../Parts/Chassis/TargetTypeDescriptor';
import UIDamageModifierInlineDescriptor from '../../Parts/Damage/DamageModifierInlineDescriptor';
import UIDamageTypeDescriptor from '../../Parts/Damage/DamageTypeDescriptor';
import UIDamageProtectionDescriptor from '../../Parts/Defense/DamageProtectionDescriptor';
import UIDefenseFlagsDescriptor from '../../Parts/Defense/DefenseFlagsDescriptor';
import UIDodgeCaption from '../../Parts/Defense/DodgeCaption';
import UIEquipmentSlotDescriptor from '../../Parts/Equipment/EquipmentSlotDescriptor';
import UIShieldCapacityDescriptor from '../../Parts/Shield/ShieldCapacityDescriptor';
import UIShieldDescriptor from '../../Parts/Shield/ShieldDescriptor';
import UIShieldOverchargeDescriptor from '../../Parts/Shield/ShieldOverchargeDescriptor';
import UIWeaponDescriptor from '../../Parts/Weapon/WeaponDescriptor';
import UIWeaponGroupDescriptor from '../../Parts/Weapon/WeaponGroupDescriptor';
import UIReconLevelPointsDescriptor from '../../Player/ReconLevelDescriptor';
import UIRankCaption from '../../Prototype/Rank';
import UIUnitCostDescriptor from '../../Prototype/UnitCostDescriptor';
import UIXPCaption from '../../Prototype/XP';

const entityMapping = () => ({
	'Attacks Per Turn': UIAPTDescriptor.getEntityCaption(),
	'Damage Reduction': UIDamageProtectionDescriptor.getEntityCaption(),
	'Damage Protection': UIDamageProtectionDescriptor.getEntityCaption('factor'),
	'Armor Protection': UIDamageProtectionDescriptor.getEntityCaption('factor', 'armor'),
	'Armor Absorption': UIDamageProtectionDescriptor.getEntityCaption('factor', 'armor'),
	'Shield Protection': UIDamageProtectionDescriptor.getEntityCaption('factor', 'shield'),
	'Shields Protection': UIDamageProtectionDescriptor.getEntityCaption('factor', 'shield'),
	'Shield Absorption': UIDamageProtectionDescriptor.getEntityCaption('factor', 'shield'),
	'Movement Points': UISpeedDescriptor.getEntityCaption('Movement'),
	Movement: UISpeedDescriptor.getEntityCaption('Movement'),
	...Object.values(UnitChassis.movementType).reduce(
		(obj, df) =>
			Object.assign(obj, {
				[UISpeedDescriptor.getMovementCaption(df, true)]: UISpeedDescriptor.getMovementCaption(df),
			}),
		{},
	),
	...Object.values(UnitDefense.defenseFlags).reduce(
		(obj, df) =>
			Object.assign(obj, {
				[UIDefenseFlagsDescriptor.getDefenseFlagCaption(df)]: UIDefenseFlagsDescriptor.getEntityCaption(df),
			}),
		{},
	),
	...Object.values(UnitChassis.targetType).reduce(
		(obj, tt) =>
			Object.assign(obj, {
				[UITargetTypeDescriptor.getCaption(tt)]: UITargetTypeDescriptor.getEntityCaption(tt),
			}),
		{},
	),
	Mechanical: UITargetTypeDescriptor.getEntityCaption('default'),
	...Object.keys(ChassisClassTitles).reduce(
		(obj, cl) =>
			Object.assign(obj, {
				[' ' + ChassisClassTitles[cl]]: UIChassisClassDescriptor.getEntityCaption(
					cl === 'default' ? undefined : cl,
				),
			}),
		{},
	),
	...UnitActions.defaultActionOrder.reduce(
		(obj, action) =>
			Object.assign(obj, {
				[' ' + UIActionDescriptor.getCaption(action)]: <> {UIActionDescriptor.getEntityCaption(action)} </>,
			}),
		{},
	),
	...Damage.TDamageTypesArray.reduce(
		(obj, dt) =>
			Object.assign(obj, {
				[`${UIDamageTypeDescriptor.getAbbr(dt)} Damage`]: (
					<UIDamageModifierInlineDescriptor
						modifier={null}
						damageType={dt}
						caption={' ' + UIDamageModifierInlineDescriptor.getCaption()}
					/>
				),
				[UIDamageTypeDescriptor.getAbbr(dt)]: <UIDamageTypeDescriptor damageType={dt} />,
			}),
		{},
	),
	...Object.values(TBaseBuildingType).reduce(
		(obj, buildingType) =>
			Object.assign(obj, {
				[UIBuildingTypeDescriptor.getCaption(buildingType)]:
					UIBuildingDescriptor.getEntityCaption(buildingType),
			}),
		{},
	),
	...WeaponGroups.reduce(
		(obj, grp) =>
			Object.assign(obj, {
				[grp.caption]: UIWeaponGroupDescriptor.getEntityCaption(grp.id),
				...(grp.aliases || []).reduce(
					(o, alias) =>
						Object.assign(o, {
							[alias.toString()]: UIWeaponGroupDescriptor.getEntityCaption(grp.id),
						}),
					{},
				),
			}),
		{},
	),
	'Production Speed': <em>Production Speed</em>,
	Buildings: UIBuildingDescriptor.getEntityCaption(null),
	'Ability Duration': UIDurationDescriptor.getEntityCaption('ability'),
	'Effect Penetration': UIDurationDescriptor.getEntityCaption('ability'),
	'Status Effect Duration': UIDurationDescriptor.getEntityCaption('resistance'),
	'Status Effect Resistance': UIDurationDescriptor.getEntityCaption('resistance'),
	'Effect Duration': UIDurationDescriptor.getEntityCaption('duration'),
	Duration: UIDurationDescriptor.getEntityCaption('duration'),
	Cooldowns: UICooldownDescriptor.getEntityCaption(true),
	Cooldown: UICooldownDescriptor.getEntityCaption(false),
	'Energy Storage': UIEnergyDescriptor.getEntityCaption(UIEnergyDescriptorType.storage),
	'Energy Output': UIEnergyDescriptor.getEntityCaption(UIEnergyDescriptorType.output),
	'Raw Materials Storage': UIRawMaterialsDescriptor.getEntityCaption(UIRawMaterialsDescriptorType.storage),
	'Raw Materials Output': UIRawMaterialsDescriptor.getEntityCaption(UIRawMaterialsDescriptorType.output),
	'Raw Materials': UIRawMaterialsDescriptor.getEntityCaption(),
	'Materials Storage': UIRawMaterialsDescriptor.getEntityCaption(UIRawMaterialsDescriptorType.storage),
	'Materials Output': UIRawMaterialsDescriptor.getEntityCaption(UIRawMaterialsDescriptorType.output),
	'Unit Cost': UIUnitCostDescriptor.getEntityCaption(true),
	'Production Cost': UIUnitCostDescriptor.getEntityCaption(false),
	'Production Capacity': UIProductionCapacityDescriptor.getEntityCaption(),
	'Research Points': UIResearchPointsDescriptor.getEntityCaption(),
	'Research Output': UIResearchPointsDescriptor.getEntityCaption(true),
	'Engineering Points': UIEngineeringPointsDescriptor.getEntityCaption(),
	'Engineering Output': UIEngineeringPointsDescriptor.getEntityCaption(true),
	'Repair Points': UIRepairPointsDescriptor.getEntityCaption(),
	'Repair Output': UIRepairPointsDescriptor.getEntityCaption(true),
	'Counter-Reconnaissance Level': UIReconLevelPointsDescriptor.getEntityCaption(true),
	'Counter-Recon Level': UIReconLevelPointsDescriptor.getEntityCaption(true),
	'Reconnaissance Level': UIReconLevelPointsDescriptor.getEntityCaption(),
	'Recon Level': UIReconLevelPointsDescriptor.getEntityCaption(),
	Energy: UIEnergyDescriptor.getEntityCaption(),
	Cost: UIUnitCostDescriptor.getEntityCaption(false),
	'Shield Capacity': UIShieldCapacityDescriptor.getEntityCaption(),
	Capacity: UIShieldCapacityDescriptor.getEntityCaption(),
	"Shields' Capacity": UIShieldCapacityDescriptor.getEntityCaption(),
	'Shields Capacity': UIShieldCapacityDescriptor.getEntityCaption(),
	'AoE Radius': UIAoeRadiusDescriptor.getEntityCaption(),
	'AoE Range': UIAoeRadiusDescriptor.getEntityCaption(),
	'AOE Radius': UIAoeRadiusDescriptor.getEntityCaption(),
	'AOE Range': UIAoeRadiusDescriptor.getEntityCaption(),
	'Effect Radius': UIAoeRadiusDescriptor.getEntityCaption(true),
	'Aura Range': UIAoeRadiusDescriptor.getEntityCaption(true),
	Radius: UIAoeRadiusDescriptor.getEntityCaption(true),
	'Deferred Damage': UIAttackDescriptor.getEntityCaption(
		{
			[UnitAttack.attackFlags.Deferred]: true,
			delivery: UnitAttack.deliveryType.immediate,
		},
		true,
	),
	'Scatter Attack': UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.AoE]: true,
		aoeType: UnitAttack.TAoEDamageSplashTypes.scatter,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	'Direct Attack': UIAttackDescriptor.getEntityCaption(null),
	Deferred: UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.Deferred]: true,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	'Deferred Attack': UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.Deferred]: true,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	'DoT Attack': UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.DoT]: true,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	DOT: UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.DoT]: true,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	'Damage Over Time': UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.DoT]: true,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	'Area Attack': UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.AoE]: true,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	'Precision Attack': UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.Disruptive]: true,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	'Disruptive Attack': UIAttackDescriptor.getEntityCaption({
		[UnitAttack.attackFlags.Disruptive]: true,
		delivery: UnitAttack.deliveryType.immediate,
	}),
	Damage: <UIDamageModifierInlineDescriptor modifier={null} />,
	HP: <UIHitPointsInlineDescriptor value={null} />,
	'Hit Points': <UIHitPointsInlineDescriptor value={null} />,
	XP: UIXPCaption.getEntityCaption(),
	Experience: UIXPCaption.getEntityCaption(),
	Rank: UIRankCaption.getEntityCaption(),
	'Hit Chance': UIHitChanceDescriptor.getEntityCaption(),
	'Attack Rate': UIAttackRateDescriptor.getEntityCaption(),
	'Weapon Rate': UIAttackRateDescriptor.getEntityCaption(),
	Priority: UIAttackPriorityDescriptor.getEntityCaption(),
	Accuracy: UIHitChanceDescriptor.getEntityCaption(),
	Speed: UISpeedDescriptor.getEntityCaption(),
	'Scan Range': UIScanRangeDescriptor.getEntityCaption(),
	Dodge: UIDodgeCaption.getEntityCaption(),
	'Attack Range': UIAttackRangeDescriptor.getEntityCaption(true),
	'Ability Range': UIAttackRangeDescriptor.getEntityCaption(false),
	Range: UIAttackRangeDescriptor.getEntityCaption(true),
	Equipment: UIEquipmentSlotDescriptor.getEntityCaption(),
	Shields: UIShieldDescriptor.getEntityCaption(),
	Shield: UIShieldDescriptor.getEntityCaption(),
	Armor: UIArmorDescriptor.getEntityCaption(),
	Weapons: UIWeaponDescriptor.getEntityCaption(true),
	Weapon: UIWeaponDescriptor.getEntityCaption(),
	Decay: UIShieldOverchargeDescriptor.getEntityCaption(),
	Units: <em>Units</em>,
	Unit: <em>Unit</em>,
	Abilities: <em>Abilities</em>,
	Aura: <em>Aura</em>,
	'Player Ability Power': UIAbilityPowerDescriptor.getEntityCaption(true),
	'Player AP': UIAbilityPowerDescriptor.getEntityCaption(true, true),
	'Ability Power': UIAbilityPowerDescriptor.getEntityCaption(),
	[AbilityPowerUIToken]: UIAbilityPowerDescriptor.getEntityCaption(false, true),
	Ability: <em>Ability</em>,
	'Action Phase': <em>Action Phase</em>,
	Deployment: <em>Deployment</em>,
	Deployed: <em>Deployed</em>,
	Deploy: <em>Deploy</em>,
	Attacks: UIAttackDescriptor.getEntityCaption(null, true),
	Attack: UIAttackDescriptor.getEntityCaption(null),
	...['Turn End', 'Turn Start', 'Turns', 'Turn'].reduce(
		(reducer, token) => ({
			...reducer,
			[token]: (
				<em style={{ whiteSpace: 'nowrap' }}>
					{UIDurationDescriptor.getIcon(UIContainer.iconClass, 'duration')}
					{token}
				</em>
			),
		}),
		{},
	),
	...Object.values(UnitAttack.deliveryType).reduce(
		(obj, dt) =>
			Object.assign(obj, {
				[UIAttackDeliveryDescriptor.getCaption(dt)]: <UIAttackDeliveryDescriptor delivery={dt} />,
			}),
		{},
	),
});

const Tokenizer: React.FC<{ description: string }> = ({ description }) => {
	const entities: Array<string | JSX.Element> = [description];
	// tokenizing distance
	const distanceMatches = description.match(/\s*(\d+\.)?\d+\s+DU(?=[\s:,.!;+'-]|$)/gm);
	distanceMatches
		?.sort((a, b) => b.length - a.length)
		?.forEach((modifier) => {
			for (let i = 0; i < entities.length; i++) {
				const e = entities[i];
				if (typeof e !== 'string') continue;
				entities.splice(
					i,
					1,
					...e.split(modifier).reduce(
						(arr, str, ix, all) =>
							ix < all.length - 1
								? arr.concat(
										str,
										<em style={{ whiteSpace: 'nowrap' }}>
											{' '}
											{UISpeedDescriptor.getIcon()}
											<strong>{modifier}</strong>
										</em>,
								  )
								: arr.concat(str),
						[],
					),
				);
			}
		});
	// tokenizing AP modifiers
	const apMatches = description.match(new RegExp(`[+-]?${AbilityPowerUIToken}\\s*\\d+(\\.\\d+)?%?`, 'uigs'));
	apMatches
		?.sort((a, b) => b.length - a.length)
		?.forEach((apValue) => {
			for (let i = 0; i < entities.length; i++) {
				const e = entities[i];
				if (typeof e !== 'string') continue;
				entities.splice(
					i,
					1,
					...e.split(apValue).reduce(
						(arr, str, ix, all) =>
							ix < all.length - 1
								? arr.concat(
										str,
										<em>
											<strong>
												{apValue.split(AbilityPowerUIToken, 2)[0]}
												{apValue.split(AbilityPowerUIToken, 2)[1]}&times;
											</strong>
											{UIAbilityPowerDescriptor.getIcon()}
										</em>,
								  )
								: arr.concat(str),
						[],
					),
				);
			}
		});
	// emphasizing numbers
	const matches = description.match(/\s*[-+]?\s*(\d+\.)?\d+%?(?=[\s:,.!;+'-]|AP|$)/gim);
	matches
		?.sort((a, b) => b.length - a.length)
		?.forEach((modifier) => {
			for (let i = 0; i < entities.length; i++) {
				const e = entities[i];
				if (typeof e !== 'string') continue;
				entities.splice(
					i,
					1,
					...e.split(modifier).reduce(
						(arr, str, ix, all) =>
							ix < all.length - 1
								? arr.concat(
										str,
										<em>
											<strong>{modifier}</strong>
										</em>,
								  )
								: arr.concat(str),
						[],
					),
				);
			}
		});
	// applying tokens
	Object.keys(entityMapping()).forEach((token) => {
		for (let i = 0; i < entities.length; i++) {
			const e = entities[i];
			if (typeof e !== 'string' || !e.includes(token)) continue;
			const tokenized = e
				.split(token)
				.reduce(
					(arr, str, ix, all) =>
						ix < all.length - 1 ? arr.concat(str, entityMapping()[token]) : arr.concat(str),
					[],
				);
			entities.splice(i, 1, ...tokenized);
		}
	});
	return (
		<>
			{entities.map((e, ix) => (
				<React.Fragment key={ix}>{e}</React.Fragment>
			))}
		</>
	);
};

export default Tokenizer;

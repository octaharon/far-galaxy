import { doesQualitySettingIncludes } from '../../../../contexts/GraphicSettings/helpers';
import { PersistentConfiguration } from '../../../../stores/persistentConfiguration';

const defaultOptions = {
	colorSaturation: 0.8,
	colorBrightness: 0.5,
	vertical: false,
	colorAlpha: 0.5,
	loopTime: 2500, //ms
	colorLoopTime: 25000, //ms
	delayTime: 500, //ms
	minHStep: 0.05,
	maxHStep: 0.1,
	minVStep: -0.5,
	maxVStep: 0.5,
	minHue: 0,
	maxHue: 360,
	ribbonCount: 5,
	strokeSize: 2,
};

type optionsType = typeof defaultOptions;

type TSection = {
	point1: Point;
	point2: Point;
	point3?: Point;
	color: number;
	delay: number;
	dir: boolean;
	alpha: number;
	phase: number;
};

const random: (...r: number[]) => number = function (...params) {
	if (params.length === 1) {
		// only 1 argument
		return random(0, params[0]); // assume numeric
	} else if (params.length === 2) {
		return Math.random() * (params[1] - params[0]) + params[0];
	} else if (params.length >= 3) {
		return params[Math.floor(Math.random() * params.length)];
	}
	return 0; // default
};

const interpColor: (options: optionsType, phase: number) => number = ({ minHue, maxHue }, phase) =>
	Math.round(minHue + (maxHue - minHue) * 0.5 * (1 + Math.cos(phase * 2 * Math.PI)));

const getColor: (options: optionsType, section: TSection) => string = (options = defaultOptions, { color, alpha }) =>
	`hsla(${interpColor(options, color)}, ${Math.round(options.colorSaturation * 100)}%, ${Math.round(
		options.colorBrightness * 100,
	)}%, ${alpha})`;

const interpTime = (loopTime: number, phaseTime: number) => phaseTime / (loopTime ?? 1);

const interpSection: (section: TSection, options: optionsType, phase: number) => TSection = (
	section,
	{ colorLoopTime, loopTime, delayTime },
	timePhase: number,
) => {
	if (!section) return null;
	section = {
		...section,
		color: section.color + interpTime(colorLoopTime, timePhase),
	};
	if (section.phase >= 1 && section.alpha <= 0.05) {
		return null;
	}
	if (section.delay <= 0) {
		section.phase = section.phase + interpTime(loopTime, timePhase);
		section.alpha = Math.pow(Math.sin(section.phase * Math.PI), 2);

		const mod = (section.dir ? 1 : -1) * Math.sin(section.phase * Math.PI * 2) * 0.1;

		section.point1.add(0, mod);
		section.point2.add(0, mod);
		section.point3.add(0, mod);
		section.point1.add(mod, 0);
		section.point2.add(mod, 0);
		section.point3.add(mod, 0);
	} else {
		section.alpha = 0;
		section.delay -= interpTime(delayTime, timePhase);
	}
	return section;
};

class Point {
	x: number;
	y: number;

	constructor(x = 0, y = 0) {
		this.x = x;
		this.y = y;
		this.set(x, y);
		return this;
	}

	set(x: number, y: number) {
		this.x = x || 0;
		this.y = y || 0;
		return this;
	}

	copy(point: { x: number; y: number }) {
		this.x = point.x || 0;
		this.y = point.y || 0;
		return this;
	}

	multiply(x: number, y: number) {
		this.x *= x || 1;
		this.y *= y || 1;
		return this;
	}

	divide(x: number, y: number) {
		this.x /= x || 1;
		this.y /= y || 1;
		return this;
	}

	add(x: number, y: number) {
		this.x += x || 0;
		this.y += y || 0;
		return this;
	}

	subtract(x: number, y: number) {
		this.x -= x || 0;
		this.y -= y || 0;
		return this;
	}

	clampX(min: number, max: number) {
		this.x = Math.max(min, Math.min(this.x, max));
		return this;
	}

	clampY(min: number, max: number) {
		this.y = Math.max(min, Math.min(this.y, max));
		return this;
	}

	flipX() {
		this.x *= -1;
		return this;
	}

	flipY() {
		this.y *= -1;
		return this;
	}

	clone() {
		return new Point(this.x, this.y);
	}
}

class RibbonFactory {
	protected _width: number;
	protected _height: number;
	protected _options: optionsType;
	protected _canvas: HTMLCanvasElement;
	protected _context: CanvasRenderingContext2D;
	protected _ribbons: TSection[][];
	protected _destroy: boolean;
	protected _lastRender: number;

	constructor(rootElement: HTMLElement, options?: Partial<optionsType>) {
		this._canvas = null;
		this._context = null;
		this._width = 0;
		this._height = 0;
		this._ribbons = [];
		this._destroy = false;
		this._lastRender = 0;
		this._options = Object.assign({}, defaultOptions);
		this.interpRibbons = this.interpRibbons.bind(this);
		this.draw = this.draw.bind(this);
		this.setOptions(options);
		this.init(rootElement);
	}

	destroy() {
		for (let i = 0; i < this._ribbons.length; i++) delete this._ribbons[i];
		this._destroy = true;
	}

	setOptions(options: Partial<optionsType>) {
		if (typeof options === 'object') {
			this._options = Object.assign({}, this._options, options);
		}
	}

	// Initialize the ribbons effect
	init(rootElement: HTMLElement) {
		try {
			if (!doesQualitySettingIncludes(PersistentConfiguration.getState().graphicSettings.qualityLevel, 'Med'))
				return;
			rootElement = rootElement || document?.body;
			this._canvas = document.createElement('canvas');
			this._canvas.style['display'] = 'block';
			this._canvas.style['margin'] = '0';
			this._canvas.style['padding'] = '0';
			this._canvas.style['border'] = '0';
			this._canvas.style['outline'] = '0';
			this._canvas.style['left'] = '0';
			this._canvas.style['top'] = '0';
			this._canvas.style['width'] = '100%';
			this._canvas.style['height'] = '100%';
			this._width = rootElement.offsetWidth;
			this._height = rootElement.offsetHeight;
			this._canvas.width = this._width;
			this._canvas.height = this._height;

			this._context = this._canvas.getContext('2d');

			this._context.clearRect(0, 0, this._width, this._height);
			this._context.globalAlpha = this._options.colorAlpha;

			rootElement.appendChild(this._canvas);
		} catch (e) {
			console.warn('Canvas Context Error: ' + e.toString());
			return;
		}
		if (doesQualitySettingIncludes(PersistentConfiguration.getState().graphicSettings.qualityLevel, 'Max'))
			requestAnimationFrame(this.interpRibbons);
		else this.interpRibbons(this._options.loopTime * 1.5);
	}

	addRibbon() {
		const { vertical, minHStep, maxHStep, minVStep, maxVStep, colorLoopTime, loopTime, delayTime } = this._options;
		const dir = Math.random() > 0.5;
		let maxPoints = 256;
		const hide = (vertical ? this._height : this._width) * 0.25;
		const min = 0 - hide;
		const max = (vertical ? this._height : this._width) + hide;
		const startX = vertical ? Math.random() * this._width : dir ? 0 : this._width;
		const startY = vertical ? (dir ? 0 : this._height) : Math.random() * this._height;
		const controlPoint: (p: Point) => number = (p) => (vertical ? p.y : p.x);

		// ribbon sections data
		const ribbon: TSection[] = [];
		const point1 = new Point(startX, startY);
		const point2 = new Point(startX, startY);
		let point3 = null;

		// build ribbon sections
		while (maxPoints--) {
			const speedX = (Math.random() * (maxHStep - minHStep) + minHStep) * this._width;
			const speedY = (Math.random() * (maxVStep - minVStep) + minVStep) * this._height;
			const sign = dir ? 1 : -1;
			const timePhase = (controlPoint(point2) - min) / (max - min);
			const color = Math.random() + (timePhase * loopTime) / colorLoopTime;

			point3 = point2.clone().add(speedX * sign, speedY * sign);

			ribbon.push({
				// single ribbon section
				point1: point1.clone(),
				point2: point2.clone(),
				point3,
				color,
				delay: 0,
				dir,
				alpha: 0,
				phase: 0,
			});
			if ((dir && controlPoint(point2) > max) || (!dir && controlPoint(point2) < min)) break;

			point1.copy(point2);
			point2.copy(point3);
		}
		this._ribbons.push(
			ribbon.map((v, ix) => ({
				...v,
				delay: Math.random() * 0.5 + ((ix / ribbon.length) * loopTime) / (delayTime || 1),
			})),
		);
	}

	draw() {
		this._context.clearRect(0, 0, this._width, this._height);
		(this._ribbons || []).forEach((r) =>
			r.forEach((section) => {
				const c = getColor(this._options, section);
				this._context.save();

				this._context.beginPath();
				this._context.moveTo(section.point1.x, section.point1.y);
				this._context.lineTo(section.point2.x, section.point2.y);
				this._context.lineTo(section.point3.x, section.point3.y);
				this._context.fillStyle = c;
				this._context.fill();

				if (this._options.strokeSize > 0) {
					this._context.lineWidth = this._options.strokeSize;
					this._context.strokeStyle = c;
					this._context.lineCap = 'round';
					this._context.stroke();
				}
				this._context.restore();
			}),
		);
	}

	interpRibbons(timePhase = 0) {
		if (!this || this._destroy) return false;
		this._ribbons = (this._ribbons || [])
			.map((ribbon) => {
				if (!ribbon) return ribbon;
				const r = ribbon
					.map((section) => interpSection(section, this._options, timePhase - this._lastRender))
					.filter((v) => Boolean(v));
				if (!r) return null;
				return r;
			})
			.filter((v) => Boolean(v) && v.length);
		this._lastRender = timePhase;
		this.draw();
		if (this._ribbons.length < this._options.ribbonCount) this.addRibbon(); // 1 per cycle
		if (doesQualitySettingIncludes(PersistentConfiguration.getState().graphicSettings.qualityLevel, 'Max'))
			requestAnimationFrame(this.interpRibbons);
	}
}

export default RibbonFactory;

/*
new Ribbons()({
	colorSaturation: '60%',
	colorBrightness: '50%',
	colorAlpha: 0.5,
	phaseSpeed: 2,
	verticalPosition: 'random',
	horizontalSpeed: 200,
	ribbonCount: 35,
	strokeSize: 4,
	parallaxAmount: -0.2,
	animateSections: true,
});
*/

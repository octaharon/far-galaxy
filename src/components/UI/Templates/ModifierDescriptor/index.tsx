import * as React from 'react';
import { wellRounded } from '../../../../utils/wellRounded';
import { IUICommonProps } from '../../index';
import Style from './index.scss';

export type UIModifierDescriptorProps = IUICommonProps & {
	modifier: IModifier;
	multiplier?: number;
	withZeroBounds?: boolean;
	precision?: number;
};

/**
 * @TODO: https://gitlab.com/octaharon/far-galaxy/-/issues/5
 * @deprecated
 */
export default class UIModifierDescriptor extends React.PureComponent<UIModifierDescriptorProps, {}> {
	public static getClass(): string {
		return Style.modifierDescriptor;
	}

	public render() {
		const m = this.props.modifier || {};
		const multiplier = this.props.multiplier || 1;
		return (
			<div className={[this.props.className || '', UIModifierDescriptor.getClass()].join(' ')}>
				{!isNaN(m.factor) && m.factor !== 1 && (
					<span>
						{m.factor > 1 ? '+' : ''}
						{Math.round((m.factor - 1) * 100)}%
					</span>
				)}
				{!isNaN(m.bias) && m.bias !== 0 && (
					<span>
						{m.bias > 0 ? '+' : ''}
						{wellRounded(m.bias * multiplier, this.props.precision || 2)}
					</span>
				)}
				{!isNaN(m.min) && m.min > -Infinity && (m.min !== 0 || this.props.withZeroBounds) && (
					<span>{wellRounded(m.min * multiplier, this.props.precision || 2)}↑</span>
				)}
				{!isNaN(m.max) && m.max < Infinity && (m.max !== 0 || this.props.withZeroBounds) && (
					<span>{wellRounded(m.max * multiplier, this.props.precision || 2)}↓</span>
				)}
			</div>
		);
	}
}

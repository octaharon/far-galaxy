import * as React from 'react';
import DIContainer from '../../../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../../../definitions/core/DI/injections';
import { BASE_TIER_LIMIT } from '../../../../../../definitions/orbital/base/constants';
import { TPlayerBaseModifier } from '../../../../../../definitions/orbital/base/types';
import { IEstablishment } from '../../../../../../definitions/orbital/establishments/types';
import { IFacility } from '../../../../../../definitions/orbital/facilities/types';
import BuildingManager from '../../../../../../definitions/orbital/GenericBaseBuildingManager';
import { TBaseBuildingType, TTypedBaseBuilding } from '../../../../../../definitions/orbital/types';
import Players from '../../../../../../definitions/player/types';
import { orderedChassisList } from '../../../../../../definitions/technologies/chassis/constants';
import { isEmptyResourcePayload } from '../../../../../../definitions/world/resources/helpers';
import Resources from '../../../../../../definitions/world/resources/types';
import romanize from '../../../../../utils/romanize';
import { UITalentBackgroundGradientID } from '../../../../Symbols/backdrops';
import UIBuildingSprite from '../../../../Symbols/BuildingSprite';
import {
	BUILDING_SPRITE_STATE_ACTIVE,
	BUILDING_SPRITE_STATE_BASE,
	BUILDING_SPRITE_STATE_OUTPUT,
	BUILDING_SPRITE_STATE_RESOURCE,
} from '../../../../Symbols/BuildingSprite/types';
import { outputIcon, upkeepIcon } from '../../../../Symbols/commonIcons';
import UIEnergyDescriptor, { UIEnergyDescriptorType } from '../../../Economy/EnergyDescriptor';
import UIEngineeringPointsDescriptor from '../../../Economy/EngineeringOutputDescriptor';
import UIMaxUnitCostDescriptor from '../../../Economy/MaxUnitCostDescriptor';
import UIProductionCapacityDescriptor from '../../../Economy/ProductionCapacityDescriptor';
import UIProductionSpeedDescriptor from '../../../Economy/ProductionSpeedDescriptor';
import UIRawMaterialsDescriptor, { UIRawMaterialsDescriptorType } from '../../../Economy/RawMaterialsDescriptor';
import UIRepairPointsDescriptor from '../../../Economy/RepairOutputDescriptor';
import UIResearchPointsDescriptor from '../../../Economy/ResearchOutputDescriptor';
import UIResourceModifierDescriptor from '../../../Economy/Resources/ResourceModifierDescriptor';
import UIResourcePayloadDescriptor from '../../../Economy/Resources/ResourcePayloadDescriptor';
import UIContainer, { classnames, IUICommonProps, MetaHeader } from '../../../index';
import UIAbilityDescriptor from '../../../Parts/Ability/AbilityDescriptor';
import UIAbilityPowerDescriptor from '../../../Parts/Ability/AbilityPowerDescriptor';
import UIDurationDescriptor from '../../../Parts/Ability/DurationDescriptor';
import UIChassisClassDescriptor from '../../../Parts/Chassis/ChassisClassDescriptor';
import UIHitPointsDescriptor from '../../../Parts/Chassis/HitPointsDescriptor';
import UIDamageProtectionDescriptor from '../../../Parts/Defense/DamageProtectionDescriptor';
import UIReconLevelPointsDescriptor from '../../../Player/ReconLevelDescriptor';
import UIBackdrop from '../../../SVG/Backdrop';
import UITabsWrapper, { UITabsList } from '../../../Templates/Tabs/TabsWrapper';
import UITooltip from '../../../Templates/Tooltip';
import { termTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import UIBayCapacityDescriptor from '../../BayCapacityDescriptor';
import UIBuildingTypeDescriptor from '../BuildingTypeDescriptor';

import styles from './index.scss';

type UIBuildingDescriptorProps = IUICommonProps & {
	buildingType: TBaseBuildingType;
	buildingId: number;
	player?: Players.IPlayer;
	details?: boolean;
	modifiers?: TPlayerBaseModifier[];
};

type TBuildingTabs = 'cost' | 'active' | 'output' | 'resource' | 'production';

const hasOutput = (b: TTypedBaseBuilding, selectedTab: TBuildingTabs) => {
	if (!b) return false;
	switch (selectedTab) {
		case 'active':
			if (BuildingManager.isEstablishment(b)) {
				return b.engineeringOutput || b.researchOutput;
			}
			return true;
		case 'output':
			if (BuildingManager.isFacility(b)) return b.materialOutput || b.energyOutput || b.repairOutput;
			return false;
		case 'resource':
			if (BuildingManager.isFacility(b)) return !isEmptyResourcePayload(b.static.resourceProduction);
			return false;
		default:
			return false;
	}
};

const hasConsumption = (b: TTypedBaseBuilding, selectedTab: TBuildingTabs) => {
	if (!b) return false;
	if (BuildingManager.isFacility(b)) {
		return (
			(Number.isFinite(b.energyMaintenanceCost) && b.energyMaintenanceCost > 0) ||
			!isEmptyResourcePayload(b.static.resourceConsumption)
		);
	}
	return !!b.energyMaintenanceCost;
};

export const UIBuildingStaticModifiers: React.FC<
	React.PropsWithChildren<{
		Building: TTypedBaseBuilding;
		tooltip: IUICommonProps['tooltip'];
	}>
> = ({ children, Building, tooltip }) => (
	<div className={UIContainer.descriptorGroupClass}>
		{Building.static.resourceConsumptionModifier && (
			<UIResourceModifierDescriptor
				tooltip={!!tooltip}
				resourceModifier={Building.static.resourceConsumptionModifier}
				consumption={true}
			/>
		)}
		{Building.static.resourceOutputModifier && (
			<UIResourceModifierDescriptor
				tooltip={!!tooltip}
				resourceModifier={Building.static.resourceOutputModifier}
				consumption={false}
			/>
		)}
		<MetaHeader>Global Effects</MetaHeader>
		{Building.static.energyConsumptionModifier && (
			<UIEnergyDescriptor
				value={null}
				tooltip={!!tooltip}
				modifier={Building.static.energyConsumptionModifier}
				type={UIEnergyDescriptorType.consumption}
			/>
		)}
		{Building.static.energyOutputModifier && (
			<UIEnergyDescriptor
				value={null}
				tooltip={!!tooltip}
				modifier={Building.static.energyOutputModifier}
				type={UIEnergyDescriptorType.output}
			/>
		)}
		{Building.static.materialOutputModifier && (
			<UIRawMaterialsDescriptor
				value={null}
				tooltip={!!tooltip}
				modifier={Building.static.materialOutputModifier}
				type={UIRawMaterialsDescriptorType.output}
			/>
		)}
		{Building.static.repairSpeedModifier && (
			<UIRepairPointsDescriptor
				repairPoints={null}
				tooltip={!!tooltip}
				modifier={Building.static.repairSpeedModifier}
				output={true}
			/>
		)}
		{Building.static.researchOutputModifier && (
			<UIResearchPointsDescriptor
				researchPoints={null}
				tooltip={!!tooltip}
				modifier={Building.static.researchOutputModifier}
				output={true}
			/>
		)}
		{Building.static.engineeringOutputModifier && (
			<UIEngineeringPointsDescriptor
				engineeringPoints={null}
				tooltip={!!tooltip}
				modifier={Building.static.engineeringOutputModifier}
				output={true}
			/>
		)}
		{Building.static.maxRawMaterials && (
			<UIRawMaterialsDescriptor
				value={null}
				tooltip={!!tooltip}
				modifier={Building.static.maxRawMaterials}
				type={UIRawMaterialsDescriptorType.storage}
			/>
		)}
		{Building.static.maxEnergyCapacity && (
			<UIEnergyDescriptor
				value={null}
				tooltip={!!tooltip}
				modifier={Building.static.maxEnergyCapacity}
				type={UIEnergyDescriptorType.storage}
			/>
		)}
		{Building.static.productionCapacity && (
			<UIProductionCapacityDescriptor
				productionCapacity={null}
				tooltip={!!tooltip}
				modifier={Building.static.productionCapacity}
				output={true}
			/>
		)}
		{Building.static.abilityPowerModifier && (
			<UIAbilityPowerDescriptor
				power={null}
				tooltip={!!tooltip}
				modifier={Building.static.abilityPowerModifier}
				isPlayer={true}
			/>
		)}
		{BuildingManager.isFacility(Building) && Building.static.maxHitPoints && (
			<UIHitPointsDescriptor
				modifier={Building.static.maxHitPoints}
				caption={'Base Hit Points'}
				tooltip={!!tooltip}
			/>
		)}
		{Building.static.bayCapacity && (
			<UIBayCapacityDescriptor bayCapacity={null} modifier={Building.static.bayCapacity} tooltip={!!tooltip} />
		)}
		{BuildingManager.isEstablishment(Building) && Building.static.armor && (
			<UIDamageProtectionDescriptor
				protection={Building.static.armor}
				caption={'Base Protection'}
				tooltip={!!tooltip}
			/>
		)}
		{children}
	</div>
);

export default class UIBuildingDescriptor extends React.PureComponent<
	UIBuildingDescriptorProps,
	{
		selectedTab: TBuildingTabs;
	}
> {
	public constructor(props: UIBuildingDescriptorProps) {
		super(props);
		this.state = {
			selectedTab: 'cost',
		};
	}

	public static getClass(): string {
		return styles.buildingDescriptor;
	}

	public static getCaption(type: TBaseBuildingType) {
		switch (type) {
			case TBaseBuildingType.establishment:
				return 'Establishment';
			case TBaseBuildingType.facility:
				return 'Facility';
			case TBaseBuildingType.factory:
				return 'Factory';
			default:
				return 'Base Building';
		}
	}

	public static getEntityCaption(type: TBaseBuildingType) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIBuildingTypeDescriptor.getIcon(type)}
				{UIBuildingTypeDescriptor.getCaption(type)}
			</em>
		);
	}

	public render() {
		const provider = BuildingManager.getProvider(this.props.buildingType);
		if (!provider) return false;
		const Turn = (
			<span style={{ whiteSpace: 'nowrap' }}>
				{UIDurationDescriptor.getIcon(UIContainer.iconClass, 'duration')}
				Turn
			</span>
		);
		const Building = provider.instantiate(null, this.props.buildingId);
		const UpkeepHeader = <MetaHeader rightElement={Turn}>Upkeep{upkeepIcon}</MetaHeader>;
		const OutputHeader = <MetaHeader rightElement={Turn}>Output{outputIcon}</MetaHeader>;
		const energyUpkeep =
			Building.energyMaintenanceCost > 0 ? (
				<UIEnergyDescriptor
					tooltip={!!this.props.tooltip}
					value={(Building as IFacility).energyMaintenanceCost}
				/>
			) : null;

		const Abilities = Building.static.abilities?.length ? (
			<div className={UIContainer.descriptorGroupClass}>
				<MetaHeader>Combat Abilities</MetaHeader>
				{Building.static.abilities.map((appliance, ix) => {
					return (
						<UIAbilityDescriptor
							ability={DIContainer.getProvider(DIInjectableCollectibles.ability).instantiate(
								null,
								appliance,
							)}
							key={ix}
							className={styles.fullWidth}
						/>
					);
				})}
			</div>
		) : null;

		const ActiveModifiers = (
			<UIBuildingStaticModifiers Building={Building} tooltip={this.props.tooltip}>
				{Building.static.intelligenceLevel && (
					<UIReconLevelPointsDescriptor
						level={null}
						modifier={Building.static.intelligenceLevel}
						tooltip={!!this.props.tooltip}
					/>
				)}
				{BuildingManager.isEstablishment(Building) && Building.static.counterIntelligenceLevel && (
					<UIReconLevelPointsDescriptor
						level={null}
						modifier={Building.static.counterIntelligenceLevel}
						counter={true}
						tooltip={!!this.props.tooltip}
					/>
				)}
			</UIBuildingStaticModifiers>
		);
		return (
			<div className={classnames(UIBuildingDescriptor.getClass(), this.props.className || '')}>
				<UIBackdrop className={styles.frame} gradientId={UITalentBackgroundGradientID} />
				<div className={classnames(this.props.details ? '' : styles.short, styles.content)}>
					<dl className={styles.meta}>
						<dt className={classnames(UIContainer.descriptorClass, styles.buildingCaption)}>
							<UIBuildingTypeDescriptor group={Building.static.type} tooltip={!!this.props.tooltip} />
						</dt>
						<dd
							className={classnames(
								styles.buildingTier,
								this.props.tooltip ? UIContainer.hasTooltipClass : null,
							)}>
							<b>Tier {romanize(Building.static.buildingTier)}</b>
							{!!this.props.tooltip && (
								<UITooltip>
									{termTooltipTemplate(
										null,
										'Building Tier',
										`A metaphorical rank that describes technological and
									economic value of the Building, as well as administrative and logistical expenses required to
									support its very existence. No Starbase can support more than ${BASE_TIER_LIMIT} Establishments, Factories or Facilities of the same Tier`,
									)}
								</UITooltip>
							)}
						</dd>
					</dl>
					<dl className={styles.meta}>
						<dt className={styles.title}>{Building.static.caption}</dt>
						{Building.energyMaintenanceCost > 0 && (
							<dd className={styles.cost}>
								<UIEnergyDescriptor
									short={true}
									tooltip={!!this.props.tooltip}
									value={Building.energyMaintenanceCost}
									className={styles.costDescriptor}
								/>
							</dd>
						)}
					</dl>
					<div className={styles.fullWidth}>
						<UIBuildingSprite
							buildingId={Building.static.id}
							buildingType={Building.static.type}
							state={
								this.state.selectedTab === 'cost'
									? BUILDING_SPRITE_STATE_BASE
									: this.state.selectedTab === 'resource'
									? BUILDING_SPRITE_STATE_RESOURCE
									: this.state.selectedTab === 'output'
									? BUILDING_SPRITE_STATE_OUTPUT
									: BUILDING_SPRITE_STATE_ACTIVE
							}
							onSwitch={(state) =>
								this.setState({
									...this.state,
									selectedTab:
										state === BUILDING_SPRITE_STATE_OUTPUT
											? 'output'
											: state === BUILDING_SPRITE_STATE_RESOURCE
											? 'resource'
											: state === BUILDING_SPRITE_STATE_ACTIVE
											? this.props.buildingType === TBaseBuildingType.factory
												? 'production'
												: this.props.buildingType === TBaseBuildingType.establishment
												? 'active'
												: 'output'
											: 'cost',
								})
							}
						/>
					</div>
					{this.props.details && <div className={styles.description}>{Building.static.description}</div>}
					<UITabsWrapper
						onSelect={(id: TBuildingTabs) =>
							this.setState({
								selectedTab: id,
							})
						}
						className={styles.tabs}
						tabs={
							[
								{
									caption: 'Passive',
									id: 'cost',
									selected: 'cost' === this.state.selectedTab,
									content: (
										<>
											<UIBuildingStaticModifiers
												Building={Building}
												tooltip={this.props.tooltip}
											/>
											<div className={UIContainer.descriptorGroupClass}>
												<MetaHeader>Building Cost</MetaHeader>
												<UIResourcePayloadDescriptor
													tooltip={!!this.props.tooltip}
													short={!this.props.details}
													resourcePayload={Building.static.buildingCost}
												/>
											</div>
										</>
									),
								},
								...(Building.static.type === TBaseBuildingType.establishment
									? [
											{
												caption: 'Operating',
												id: 'active',
												selected: 'active' === this.state.selectedTab,
												content: (
													<React.Fragment>
														{ActiveModifiers}
														{hasConsumption(Building, this.state.selectedTab) && (
															<div className={UIContainer.descriptorGroupClass}>
																{UpkeepHeader}
																{energyUpkeep}
															</div>
														)}
														{hasOutput(Building, this.state.selectedTab) && (
															<div className={UIContainer.descriptorGroupClass}>
																{OutputHeader}
																{(Building as IEstablishment).researchOutput > 0 && (
																	<UIResearchPointsDescriptor
																		tooltip={!!this.props.tooltip}
																		researchPoints={
																			(Building as IEstablishment).researchOutput
																		}
																	/>
																)}
																{(Building as IEstablishment).engineeringOutput > 0 && (
																	<UIEngineeringPointsDescriptor
																		tooltip={!!this.props.tooltip}
																		engineeringPoints={
																			(Building as IEstablishment)
																				.engineeringOutput
																		}
																	/>
																)}
															</div>
														)}
														{Abilities}
													</React.Fragment>
												),
											},
									  ]
									: BuildingManager.isFactory(Building)
									? [
											{
												caption: 'Production',
												id: 'production',
												selected: 'production' === this.state.selectedTab,
												content: (
													<React.Fragment>
														{ActiveModifiers}
														{hasConsumption(Building, this.state.selectedTab) && (
															<div className={UIContainer.descriptorGroupClass}>
																{UpkeepHeader}
																{energyUpkeep}
															</div>
														)}
														<div className={UIContainer.descriptorGroupClass}>
															<MetaHeader>Unit Production</MetaHeader>
															<UIMaxUnitCostDescriptor
																maxUnitCost={Building.maxProductionCost}
																tooltip={!!this.props.tooltip}
															/>
															<UIProductionSpeedDescriptor
																productionSpeed={Building.currentProductionSpeed}
																tooltip={!!this.props.tooltip}
															/>
															<MetaHeader>Supported Chassis</MetaHeader>
															{orderedChassisList
																.filter((v) =>
																	Building.static.productionType?.includes(v),
																)
																.map((chClass) => (
																	<UIChassisClassDescriptor
																		className={styles.factoryClass}
																		key={chClass}
																		tooltip={!!this.props.tooltip}
																		chassisType={chClass}
																	/>
																))}
														</div>
														{Abilities}
													</React.Fragment>
												),
											},
									  ]
									: [
											{
												caption: 'Operating',
												id: 'output',
												selected: 'output' === this.state.selectedTab,
												content: (
													<>
														{ActiveModifiers}
														{hasConsumption(Building, this.state.selectedTab) && (
															<div className={UIContainer.descriptorGroupClass}>
																{UpkeepHeader}
																{energyUpkeep}
																<UIResourcePayloadDescriptor
																	tooltip={true}
																	short={!this.props.details}
																	resourcePayload={
																		Building.static.resourceConsumption
																	}
																/>
															</div>
														)}
														{hasOutput(Building, this.state.selectedTab) && (
															<div className={UIContainer.descriptorGroupClass}>
																{OutputHeader}
																{(Building as IFacility).energyOutput > 0 && (
																	<UIEnergyDescriptor
																		tooltip={!!this.props.tooltip}
																		value={(Building as IFacility).energyOutput}
																	/>
																)}
																{(Building as IFacility).repairOutput > 0 && (
																	<UIRepairPointsDescriptor
																		tooltip={!!this.props.tooltip}
																		repairPoints={
																			(Building as IFacility).repairOutput
																		}
																	/>
																)}
																{(Building as IFacility).materialOutput > 0 && (
																	<UIRawMaterialsDescriptor
																		tooltip={!!this.props.tooltip}
																		value={(Building as IFacility).materialOutput}
																	/>
																)}
															</div>
														)}
														{Abilities}
													</>
												),
											},
											(Building as IFacility).isResourceModeEnabled()
												? {
														caption: 'Resources',
														id: 'resource',
														selected: 'resource' === this.state.selectedTab,
														content: (
															<>
																{ActiveModifiers}
																{hasConsumption(Building, this.state.selectedTab) && (
																	<div className={UIContainer.descriptorGroupClass}>
																		{UpkeepHeader}
																		{energyUpkeep}
																		<UIResourcePayloadDescriptor
																			tooltip={true}
																			short={!this.props.details}
																			resourcePayload={
																				Building.static.resourceConsumption
																			}
																		/>
																	</div>
																)}
																{hasOutput(Building, this.state.selectedTab) && (
																	<div className={UIContainer.descriptorGroupClass}>
																		{OutputHeader}
																		<UIResourcePayloadDescriptor
																			tooltip={true}
																			short={!this.props.details}
																			resourcePayload={
																				Building.static.resourceProduction
																			}
																		/>
																	</div>
																)}
																{Abilities}
															</>
														),
												  }
												: null,
									  ]),
							].filter(Boolean) as UITabsList<TBuildingTabs>
						}
					/>
				</div>
			</div>
		);
	}
}

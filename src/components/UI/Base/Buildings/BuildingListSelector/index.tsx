import * as React from 'react';
import { ICollectibleFactory } from '../../../../../../definitions/core/Collectible';
import BuildingManager from '../../../../../../definitions/orbital/GenericBaseBuildingManager';
import {
	IBaseBuildingProvider,
	IBaseBuildingStatic,
	TBaseBuildingType,
} from '../../../../../../definitions/orbital/types';
import Players from '../../../../../../definitions/player/types';
import { classnames, IUICommonProps } from '../../../index';
import { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import UIListSelector from '../../../Templates/ListSelector';
import UIBuildingTypeDescriptor from '../BuildingTypeDescriptor';
import styles from './index.scss';

const buildingListSelectorClass = styles.buildingListSelector;

type UIBuildingListSelectorProps = IUICommonProps & {
	onBuildingSelected?: (buildingType: TBaseBuildingType, buildingId: number) => any;
	selectedBuildingId?: number;
	buildingType: TBaseBuildingType;
	player?: Players.IPlayer;
};

type IBuildingTypeHash = EnumProxy<TBaseBuildingType, number[]>;

interface UIBuildingListSelectorState {
	buildingsAvailable: IBuildingTypeHash;
}

export default class UIBuildingListSelector extends React.PureComponent<
	UIBuildingListSelectorProps,
	UIBuildingListSelectorState
> {
	public static getClass(): string {
		return buildingListSelectorClass;
	}

	protected get buildingOptions() {
		return this.state.buildingsAvailable?.[this.buildingType] || [];
	}

	protected get buildingType(): TBaseBuildingType {
		return this.props.buildingType || TBaseBuildingType.factory;
	}

	protected getSelectedBuildingId(group: TBaseBuildingType = this.buildingType): number {
		if (!group) return null;
		if (
			this.state.buildingsAvailable[group].some(
				(buildingId) => String(buildingId) === String(this.props.selectedBuildingId),
			)
		)
			return this.props.selectedBuildingId;
		return this.state.buildingsAvailable[group][0];
	}

	public static getDerivedStateFromProps(props: UIBuildingListSelectorProps, state: UIBuildingListSelectorState) {
		return {
			...(state || {}),
			buildingsAvailable: UIBuildingListSelector.buildBuildingTypeHash(props),
		};
	}

	public componentDidUpdate() {
		if (this.getSelectedBuildingId() !== this.props.selectedBuildingId && this.props.onBuildingSelected)
			this.props.onBuildingSelected(this.buildingType, this.state.buildingsAvailable[this.buildingType][0]);
	}

	public componentDidMount() {
		if (this.getSelectedBuildingId() !== this.props.selectedBuildingId && this.props.onBuildingSelected)
			this.props.onBuildingSelected(this.buildingType, this.state.buildingsAvailable[this.buildingType][0]);
	}

	protected static buildBuildingTypeHash(props: UIBuildingListSelectorProps) {
		const r = {} as IBuildingTypeHash;
		Object.values(TBaseBuildingType).forEach((buildingType: TBaseBuildingType) => {
			const provider = BuildingManager.getProvider(buildingType);
			Object.keys((provider as IBaseBuildingProvider).getAll()).forEach((buildingId) => {
				if (!props.player || props.player.isBuildingAvailable(buildingType, buildingId)) {
					const key = buildingType;
					r[key] = (r[key] || []).concat(parseInt(String(buildingId)));
				}
			});
			r[buildingType].sort(
				(a, b) =>
					(provider as IBaseBuildingProvider).get(a).buildingTier -
					(provider as IBaseBuildingProvider).get(b).buildingTier,
			);
		});
		return r;
	}

	public constructor(props: UIBuildingListSelectorProps) {
		super(props);
		const buildingsAvailable = UIBuildingListSelector.buildBuildingTypeHash(props);
		this.state = {
			buildingsAvailable,
		};
	}

	public render() {
		const provider = BuildingManager.getProvider(this.buildingType);
		const buildingTypeCaption = (buildingType: TBaseBuildingType) => (
			<UIBuildingTypeDescriptor group={buildingType} tooltip={true} />
		);
		const onGroupSelect = (buildingType: TBaseBuildingType) => {
			this.props.onBuildingSelected
				? this.props.onBuildingSelected(buildingType, this.getSelectedBuildingId(buildingType))
				: undefined;
		};
		const buildingCaption = (buildingType: TBaseBuildingType, buildingId: number) =>
			buildingId ? (
				<span className={classnames(styles.buildingCaption)}>
					{(provider as IBaseBuildingProvider).get(buildingId).caption}
				</span>
			) : null;
		const onBuildingSelected = (buildingId: number) => {
			this.props.onBuildingSelected ? this.props.onBuildingSelected(this.buildingType, buildingId) : undefined;
		};
		return (
			<div className={[UIBuildingListSelector.getClass(), this.props.className].join(' ')}>
				<UIListSelector
					options={Object.keys(this.state.buildingsAvailable)}
					selectedIndex={Object.keys(this.state.buildingsAvailable).indexOf(this.buildingType)}
					selectedItem={this.buildingType}
					buttonColor={TSVGSmallTexturedIconStyles.gray}
					optionRender={buildingTypeCaption}
					onSelect={onGroupSelect}
				/>
				<UIListSelector
					options={this.buildingOptions}
					selectedItem={this.getSelectedBuildingId()}
					selectedIndex={this.buildingOptions.findIndex(
						(v) => String(v) === String(this.getSelectedBuildingId()),
					)}
					buttonColor={TSVGSmallTexturedIconStyles.gray}
					optionRender={(id) => buildingCaption(this.buildingType, id)}
					onSelect={onBuildingSelected}
				/>
			</div>
		);
	}
}

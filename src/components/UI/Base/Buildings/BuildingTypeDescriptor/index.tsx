import * as React from 'react';
import { TBaseBuildingType } from '../../../../../../definitions/orbital/types';
import UIContainer, { IUICommonProps } from '../../../index';
import SVGIconContainer from '../../../SVG/IconContainer';
import SVGSmallTexturedIcon, { TSVGSmallTexturedIconStyles } from '../../../SVG/SmallTexturedIcon';
import Tokenizer from '../../../Templates/Tokenizer';
import UITooltip from '../../../Templates/Tooltip';
import { basicTooltipTemplate } from '../../../Templates/Tooltip/tooltipTemplates';
import styles from './index.scss';

export const buildingTypeDescriptorClass = styles.buildingType;

export type UIBuildingTypeDescriptorProps = IUICommonProps & {
	group: TBaseBuildingType;
	highlight?: boolean;
	tooltip?: boolean;
};

const buildingTypeCaption: EnumProxy<TBaseBuildingType, string> = {
	[TBaseBuildingType.factory]: 'Factories',
	[TBaseBuildingType.establishment]: 'Establishments',
	[TBaseBuildingType.facility]: 'Facilities',
};

const buildingTypeIconId: EnumProxy<TBaseBuildingType, string> = {
	[TBaseBuildingType.factory]: 'bb-factories-icon',
	[TBaseBuildingType.establishment]: 'bb-establishments-icon',
	[TBaseBuildingType.facility]: 'bb-facilities-icon',
};

const buildingTypeIconSprites = {
	[TBaseBuildingType.factory]: (
		<symbol viewBox="0 0 512 512">
			<path d="m505.854 150.336c-3.691-1.749-8.149-1.173-11.349 1.451l-110.507 92.096v-83.883c0-4.139-2.389-7.915-6.144-9.664-3.712-1.749-8.149-1.173-11.349 1.451l-110.507 92.096v-83.883c0-4.224-2.496-8.064-6.379-9.771-3.84-1.749-8.384-.96-11.499 1.899l-66.859 61.291c-2.432 2.24-3.712 5.483-3.413 8.789l24.021 276.267 10.581-.363-10.667 3.221c0 5.888 4.779 10.667 10.667 10.667h298.88c5.888 0 10.667-4.779 10.667-10.667v-341.333c.001-4.139-2.388-7.915-6.143-9.664zm-185.856 244.331c0 5.888-4.779 10.667-10.667 10.667h-64c-5.888 0-10.667-4.779-10.667-10.667v-64c0-5.888 4.779-10.667 10.667-10.667h64c5.888 0 10.667 4.779 10.667 10.667zm128 0c0 5.888-4.779 10.667-10.667 10.667h-64c-5.888 0-10.667-4.779-10.667-10.667v-64c0-5.888 4.779-10.667 10.667-10.667h64c5.888 0 10.667 4.779 10.667 10.667z" />
			<path d="m127.956 9.749c-.47-5.525-5.099-9.749-10.624-9.749h-64c-5.525 0-10.155 4.224-10.624 9.749l-42.667 490.667c-.256 2.987.747 5.931 2.752 8.128s4.885 3.456 7.872 3.456h149.333c.128 0 .277-.021.427 0 5.888 0 10.667-4.779 10.667-10.667 0-1.365-.256-2.667-.725-3.861z" />
		</symbol>
	),
	[TBaseBuildingType.establishment]: (
		<symbol viewBox="0 0 512 512">
			<path d="m243.955 143.77c5.997 8.081 18.093 8.081 24.091 0l35.932-48.417c29.258-39.425 1.117-95.353-47.978-95.353-49.095 0-77.236 55.928-47.977 95.352zm12.045-98.77c8.284 0 15 6.716 15 15s-6.716 15-15 15-15-6.716-15-15 6.716-15 15-15z" />
			<path d="m496 182h-90v330h90c8.284 0 15-6.716 15-15v-300c0-8.284-6.716-15-15-15zm-37.5 270c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15z" />
			<path d="m1 197v300c0 8.284 6.716 15 15 15h90v-330h-90c-8.284 0-15 6.716-15 15zm52.5 45c8.284 0 15 6.716 15 15s-6.716 15-15 15-15-6.716-15-15 6.716-15 15-15zm0 60c8.284 0 15 6.716 15 15s-6.716 15-15 15-15-6.716-15-15 6.716-15 15-15zm0 60c8.284 0 15 6.716 15 15s-6.716 15-15 15-15-6.716-15-15 6.716-15 15-15zm0 60c8.284 0 15 6.716 15 15s-6.716 15-15 15-15-6.716-15-15 6.716-15 15-15z" />
			<path d="m231 432h50v80h-50z" />
			<path d="m361 102h-25.812c-2.076 3.869-4.444 7.625-7.12 11.23l-35.933 48.418c-8.449 11.385-21.958 18.182-36.136 18.182s-27.687-6.797-36.136-18.183l-35.932-48.416c-2.676-3.605-5.044-7.362-7.12-11.231h-25.811c-8.284 0-15 6.716-15 15v395h65v-95c0-8.284 6.716-15 15-15h80c8.284 0 15 6.716 15 15v95h65v-395c0-8.284-6.716-15-15-15zm-165 270c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm60 180c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm60 120c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15zm0-60c-8.284 0-15-6.716-15-15s6.716-15 15-15 15 6.716 15 15-6.716 15-15 15z" />
		</symbol>
	),
	[TBaseBuildingType.facility]: (
		<symbol viewBox="0 0 510 510">
			<path d="m360 428.866v81.134h150v-135h-164.963z" />
			<path d="m336.704 345h173.296v-105h-202.463z" />
			<path d="m412.5 63.003v-63.003h-262.5v92h-45v43h120v-43h-45v-62h202.5v33.003c-50.741 6.792-90.881 47.533-96.752 98.558l13.456 48.439h210.796v-35.5c0-56.947-42.533-104.141-97.5-111.497z" />
			<circle cx="165" cy="315" r="30" />
			<path d="m0 450h330v60h-330z" />
			<path d="m255.568 165h-181.136l-70.833 255h322.803zm-90.568 210c-33.084 0-60-26.916-60-60s26.916-60 60-60 60 26.916 60 60-26.916 60-60 60z" />
		</symbol>
	),
};

export default class UIBuildingTypeDescriptor extends React.PureComponent<UIBuildingTypeDescriptorProps, {}> {
	public static getClass(): string {
		return buildingTypeDescriptorClass;
	}

	public static getCaptionClass(): string {
		return styles.buildingTypeCaption;
	}

	public static getCaption(t: TBaseBuildingType): string {
		return buildingTypeCaption[t] || 'Buildings';
	}

	public static getIcon(t: TBaseBuildingType, iconClass: string = UIContainer.iconClass) {
		if (!t) return null;
		return (
			<SVGSmallTexturedIcon className={iconClass} style={TSVGSmallTexturedIconStyles.teal}>
				<g style={{ transform: 'scale(0.9)', transformOrigin: '50% 50%' }}>
					<use {...SVGIconContainer.getXLinkProps(buildingTypeIconId[t])} />
				</g>
			</SVGSmallTexturedIcon>
		);
	}

	public static getTooltip(t: TBaseBuildingType) {
		return basicTooltipTemplate(
			UIBuildingTypeDescriptor.getIcon(t, UIContainer.tooltipIconClass),
			UIBuildingTypeDescriptor.getCaption(t),
			t === TBaseBuildingType.factory
				? ' run on Raw Materials and Energy and build Units from Prototypes. Every Factory can only produce certain Chassis and is characterized by Production Speed and Production Cost limit.'
				: t === TBaseBuildingType.facility
				? 'are technological Buildings including, but not limited to, Energy plants, storages, Repair stations and various Resource converters. Most importantly, Facilities can produce Repair Points, Raw Materials and Energy'
				: t === TBaseBuildingType.establishment
				? ' give home to the highest skilled specialists and provide them with creative freedom, affecting many aspects of Base performance, notably its Reconnaissance Level and resilience to Orbital Attack. Most Establishments consume Energy each Turn and produce Engineering Points, Research Points or both.'
				: ' form the core infrastructure of Base and are the foundation of Economy. While Facilities produce Resources or convert them into Energy, Repair Points and Raw Materials, Establishments provide Research Points and Engineering Points, and Factories build Units from Prototypes, consuming Raw Materials. Most of Buildings that do not produce Energy require it to operate each Turn. While active, some Buildings grant a Player additional Abilities in Combat',
		);
	}

	public render() {
		return (
			<span
				className={[
					UIBuildingTypeDescriptor.getClass(),
					this.props.className || '',
					this.props.tooltip ? UIContainer.hasTooltipClass : '',
				].join(' ')}
			>
				{UIBuildingTypeDescriptor.getIcon(this.props.group, styles.buildingTypeIcon)}
				<span className={UIBuildingTypeDescriptor.getCaptionClass()}>
					{UIBuildingTypeDescriptor.getCaption(this.props.group)}
				</span>
				{this.props.tooltip && <UITooltip>{UIBuildingTypeDescriptor.getTooltip(this.props.group)}</UITooltip>}
			</span>
		);
	}
}

Object.keys(buildingTypeCaption).forEach((buildingType) => {
	SVGIconContainer.registerSymbol(buildingTypeIconId[buildingType], buildingTypeIconSprites[buildingType]);
});

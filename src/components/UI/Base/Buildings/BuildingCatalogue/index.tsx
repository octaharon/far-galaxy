import * as React from 'react';
import { doesCollectibleSetIncludeItem } from '../../../../../../definitions/core/Collectible';
import { TPlayerBaseModifier } from '../../../../../../definitions/orbital/base/types';
import BuildingManager from '../../../../../../definitions/orbital/GenericBaseBuildingManager';
import { IBaseBuildingProvider, TBaseBuildingType } from '../../../../../../definitions/orbital/types';
import Players from '../../../../../../definitions/player/types';
import { classnames, IUICommonProps } from '../../../index';
import UIBuildingDescriptor from '../BuildingDescriptor';
import UIBuildingListSelector from '../BuildingListSelector';
import styles from './index.scss';
import IPlayer = Players.IPlayer;

const buildingCatalogueClass = styles.buildingCatalogue;

type UIBuildingCatalogueProps = IUICommonProps & {
	player?: IPlayer;
	defaultSelectedBuilding?: number;
	buildingType?: TBaseBuildingType;
	short?: boolean;
	modifiers?: TPlayerBaseModifier[];
	onBuildingSelected?: (buildingType: TBaseBuildingType, buildingId: number) => any;
};

export interface UIBuildingCatalogueState {
	selectedBuilding: number;
	buildingType: TBaseBuildingType;
}

/*
const stackBuildingMods = (props: UIBuildingCatalogueProps) =>
	DefenseManager.stackBuildingModifiers(
		...(props.modifiers || []),
		...extractPlayerUnitModifier(getFactionUnitModifiers(props.player?.faction), 'armor', props.forChassis),
	);
*/

let provider: IBaseBuildingProvider = null;

const getBuildingId = (props: UIBuildingCatalogueProps, buildingId?: number) => {
	provider = BuildingManager.getProvider(props.buildingType);
	if (!provider) return null;
	let aId = buildingId || props.defaultSelectedBuilding;
	const possibleBuildingOptions = props.player
		? Object.keys((provider as IBaseBuildingProvider).getAll()).filter((id) =>
				props.player.isBuildingAvailable(props.buildingType, id),
		  )
		: Object.keys((provider as IBaseBuildingProvider).getAll());
	if (!doesCollectibleSetIncludeItem(aId, possibleBuildingOptions)) aId = possibleBuildingOptions[0];
	return aId;
};

export default class UIBuildingCatalogue extends React.PureComponent<
	UIBuildingCatalogueProps,
	UIBuildingCatalogueState
> {
	public static getClass(): string {
		return buildingCatalogueClass;
	}

	public static getDerivedStateFromProps(props: UIBuildingCatalogueProps, state?: UIBuildingCatalogueState) {
		const buildingId = getBuildingId(
			{ ...props, buildingType: props?.buildingType || state?.buildingType },
			state?.selectedBuilding,
		);
		return {
			buildingType: props.buildingType ?? state?.buildingType ?? TBaseBuildingType.factory,
			selectedBuilding: buildingId,
		};
	}

	public componentDidMount() {
		if (!this.props.defaultSelectedBuilding && this.props.onBuildingSelected)
			this.props.onBuildingSelected(this.state.buildingType, this.state.selectedBuilding);
	}

	public constructor(props: UIBuildingCatalogueProps) {
		super(props);
		this.onBuildingSelected = this.onBuildingSelected.bind(this);
		this.state = UIBuildingCatalogue.getDerivedStateFromProps(props);
	}

	public render() {
		const provider = BuildingManager.getProvider(this.state?.buildingType);
		if (!provider) return null;
		return (
			<div className={classnames(UIBuildingCatalogue.getClass(), this.props.className)}>
				<UIBuildingListSelector
					buildingType={this.state.buildingType}
					selectedBuildingId={this.state.selectedBuilding}
					onBuildingSelected={this.onBuildingSelected}
					player={this.props.player}
					className={styles.selector}
				/>
				<UIBuildingDescriptor
					buildingId={this.state.selectedBuilding}
					buildingType={this.state.buildingType}
					details={!this.props.short}
					tooltip={true}
				/>
			</div>
		);
	}

	protected onBuildingSelected(buildingType: TBaseBuildingType, buildingId: number) {
		const selectedBuilding = getBuildingId({ ...this.props, buildingType }, buildingId);
		this.setState(
			{
				...this.state,
				selectedBuilding,
				buildingType,
			},
			() => {
				if (this.props.onBuildingSelected) this.props.onBuildingSelected(buildingType, selectedBuilding);
			},
		);
	}
}

import * as React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import UIContainer, { classnames, getTemplateColor, IUICommonProps } from '../../index';
import SVGIconContainer, {
	SVGBasicBevelFilterID,
	SVGBasicEmbossFilterID,
	SVGBasicNoiseFilterID,
	SVGBasicShadowFilterID,
	SVGFilterWrapper,
} from '../../SVG/IconContainer';
import UITooltip from '../../Templates/Tooltip';
import { termTooltipTemplate } from '../../Templates/Tooltip/tooltipTemplates';
import { SVGProductionCapacityGradient } from '../../Economy/ProductionCapacityDescriptor';
import { SVGRawMaterialFillGradient } from '../../Economy/RawMaterialsDescriptor';
import Style from './index.scss';

type IUIBayCapacityDescriptorPureProps = {
	bayCapacity: number;
	output?: boolean;
	modifier?: IModifier;
};

export type UIBayCapacityDescriptorProps = IUICommonProps &
	IUIBayCapacityDescriptorPureProps & {
		short?: boolean;
	};

export const SVGBayCapacityIconID = 'prod-bay-capacity-icon';
export const SVGBayCapacityGradientID = 'prod-bay-capacity-gradient';

SVGIconContainer.registerGradient(
	SVGBayCapacityGradientID,
	<linearGradient x1="0" y1="0" x2="100%" y2="100%" gradientUnits="objectBoundingBox">
		<stop offset="0" stopColor={getTemplateColor('text_highlight')} />
		<stop offset="1" stopColor={getTemplateColor('rust')} />
	</linearGradient>,
).registerSymbol(
	SVGBayCapacityIconID,
	<symbol viewBox="0 0 567 567">
		<path
			d="M283.528,0C126.703,0,0,126.703,0,283.528s127.182,283.528,283.528,283.528s283.528-127.182,283.528-283.528
		S440.354,0,283.528,0z M245.756,463.304h-66.459c-7.65,0-14.344-6.216-14.344-14.344c0-7.65,6.216-14.344,14.344-14.344h65.981
		c7.65,0,14.344,6.215,14.344,14.344C260.1,456.609,253.406,463.304,245.756,463.304z M245.756,415.969h-94.668
		c-7.65,0-14.344-6.216-14.344-14.344c0-7.649,6.215-14.344,14.344-14.344h94.668c7.65,0,14.344,6.216,14.344,14.344
		S253.406,415.969,245.756,415.969z M245.756,368.635H122.878c-7.65,0-14.344-6.216-14.344-14.344
		c0-7.65,6.215-14.344,14.344-14.344h122.878c7.65,0,14.344,6.216,14.344,14.344S253.406,368.635,245.756,368.635z M245.756,321.3
		H94.191c-7.65,0-14.344-6.215-14.344-14.344c0-8.128,6.215-14.344,14.344-14.344h151.087c7.65,0,14.344,6.216,14.344,14.344
		C260.1,315.085,253.406,321.3,245.756,321.3z M245.756,273.966H94.191c-7.65,0-14.344-6.216-14.344-14.344
		s6.215-14.344,14.344-14.344h151.087c7.65,0,14.344,6.216,14.344,14.344C260.1,267.75,253.406,273.966,245.756,273.966z
		 M245.756,226.631H122.878c-7.65,0-14.344-6.215-14.344-14.344c0-7.65,6.215-14.344,14.344-14.344h122.878
		c7.65,0,14.344,6.216,14.344,14.344C260.1,220.416,253.406,226.631,245.756,226.631z M245.756,179.297h-94.668
		c-7.65,0-14.344-6.215-14.344-14.344c0-7.65,6.215-14.344,14.344-14.344h94.668c7.65,0,14.344,6.216,14.344,14.344
		C260.1,173.082,253.406,179.297,245.756,179.297z M245.756,132.441h-66.459c-7.65,0-14.344-6.216-14.344-14.344
		c0-8.128,6.216-14.344,14.344-14.344h65.981c7.65,0,14.344,6.215,14.344,14.344C260.1,125.747,253.406,132.441,245.756,132.441z
		 M321.3,103.753h65.981c7.65,0,14.344,6.215,14.344,14.344c0,8.128-6.216,14.344-14.344,14.344H321.3
		c-7.649,0-14.344-6.216-14.344-14.344C306.956,109.969,313.65,103.753,321.3,103.753z M321.3,151.088h94.669
		c7.65,0,14.344,6.215,14.344,14.344c0,7.65-6.216,14.344-14.344,14.344H321.3c-7.649,0-14.344-6.216-14.344-14.344
		C306.956,157.303,313.65,151.088,321.3,151.088z M321.3,198.422h122.879c7.649,0,14.344,6.216,14.344,14.344
		c0,7.65-6.216,14.344-14.344,14.344H321.3c-7.649,0-14.344-6.215-14.344-14.344C306.956,204.638,313.65,198.422,321.3,198.422z
		 M321.3,245.756h151.088c7.65,0,14.344,6.216,14.344,14.344c0,8.128-6.216,14.344-14.344,14.344H321.3
		c-7.649,0-14.344-6.215-14.344-14.344C306.956,251.972,313.65,245.756,321.3,245.756z M387.76,463.304H321.3
		c-7.649,0-14.344-6.216-14.344-14.344c0-7.65,6.216-14.344,14.344-14.344h65.981c7.65,0,14.344,6.215,14.344,14.344
		C401.625,456.609,395.409,463.304,387.76,463.304z M415.969,415.969H321.3c-7.649,0-14.344-6.216-14.344-14.344
		c0-7.649,6.216-14.344,14.344-14.344h94.669c7.65,0,14.344,6.216,14.344,14.344S423.619,415.969,415.969,415.969z M444.179,368.635
		H321.3c-7.649,0-14.344-6.216-14.344-14.344c0-7.65,6.216-14.344,14.344-14.344h122.879c7.649,0,14.344,6.216,14.344,14.344
		S452.307,368.635,444.179,368.635z M472.866,321.3H321.3c-7.649,0-14.344-6.215-14.344-14.344c0-8.128,6.216-14.344,14.344-14.344
		h151.088c7.65,0,14.344,6.216,14.344,14.344C486.731,315.085,480.516,321.3,472.866,321.3z"
		/>
	</symbol>,
);

export default class UIBayCapacityDescriptor extends React.PureComponent<UIBayCapacityDescriptorProps, {}> {
	public static getClass(): string {
		return Style.bayCapacityDescriptor;
	}

	public static getCaptionClass(): string {
		return Style.bayCapacityCaption;
	}

	public static getIcon(className: string = UIContainer.iconClass) {
		return (
			<SVGFilterWrapper className={className}>
				<g filter={SVGIconContainer.getUrlRef(SVGBasicBevelFilterID)}>
					<g
						fill={SVGIconContainer.getUrlRef(SVGBayCapacityGradientID)}
						filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)}>
						<use {...SVGIconContainer.getXLinkProps(SVGBayCapacityIconID)} />
					</g>
				</g>
			</SVGFilterWrapper>
		);
	}

	public static getCaption(output = false) {
		return !output ? 'Bay Size' : 'Bay Size';
	}

	public static getEntityCaption(output = false) {
		return (
			<em style={{ whiteSpace: 'nowrap' }}>
				{UIBayCapacityDescriptor.getIcon()}
				<span className={UIBayCapacityDescriptor.getCaptionClass()}>
					{UIBayCapacityDescriptor.getCaption(output)}
				</span>
			</em>
		);
	}

	public static getTooltip() {
		return termTooltipTemplate(
			UIBayCapacityDescriptor.getIcon(UIContainer.tooltipIconClass),
			UIBayCapacityDescriptor.getCaption(false),
			'Bay Size is the quantity of Units that can be docked at Orbital Bay, where they can travel along with Base between Combats and restore their Hit Points from Repair Output',
		);
	}

	public static getValue(props: IUIBayCapacityDescriptorPureProps) {
		const { bayCapacity, modifier } = props;
		const modOnly = !Number.isFinite(bayCapacity);
		if (modOnly && !modifier) return 'Unknown';
		const isPositive = 100 < BasicMaths.applyModifier(100, modifier);
		const mod = (
			<span className={classnames(isPositive ? Style.positive : Style.negative, Style.modifier)}>
				{BasicMaths.describeModifier(modifier, false).join('/').trim()}
			</span>
		);
		return (
			<>
				{Number.isFinite(bayCapacity) && (
					<span className={Style.bayCapacityValue}>{bayCapacity === 0 ? '-' : bayCapacity}</span>
				)}
				{!!modifier && (modOnly ? mod : <>({mod})</>)}
			</>
		);
	}

	constructor(props: UIBayCapacityDescriptorProps) {
		super(props);
	}

	public render() {
		const value = UIBayCapacityDescriptor.getValue(this.props);
		if (this.props.short)
			return (
				<span
					className={classnames(
						this.props.className || '',
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						UIBayCapacityDescriptor.getClass(),
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIBayCapacityDescriptor.getIcon()}
					<span className={Style.value}>{value}</span>
					{!!this.props.tooltip && <UITooltip>{UIBayCapacityDescriptor.getTooltip()}</UITooltip>}
				</span>
			);
		return (
			<dl
				className={classnames(UIBayCapacityDescriptor.getClass(), this.props.className || '')}
				onClick={this.props.onClick || null}>
				<dt
					className={classnames(
						UIContainer.captionClass,
						UIContainer.descriptorClass,
						this.props.tooltip ? UIContainer.hasTooltipClass : '',
					)}>
					{UIBayCapacityDescriptor.getIcon()}
					<span className={UIBayCapacityDescriptor.getCaptionClass()}>
						{UIBayCapacityDescriptor.getCaption(this.props.output)}
					</span>
					{!!this.props.tooltip && <UITooltip>{UIBayCapacityDescriptor.getTooltip()}</UITooltip>}
				</dt>
				<dd>
					<span className={Style.value}>{value}</span>
				</dd>
			</dl>
		);
	}
}

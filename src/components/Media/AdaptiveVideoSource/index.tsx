import React from 'react';
import useMedia from '../../../utils/useMedia';

type TPlayableVideoSource = { src: string; type: string; media?: string };
type AdaptiveVideoSourceProps = {
	sources: Record<number, string | TPlayableVideoSource>; // {[MediaQuery]:FilePath|{source,type}}
};

export const AdaptiveVideoSource: React.FC<AdaptiveVideoSourceProps> = ({ sources }) => {
	const sourcesArray = Object.entries(sources);
	const mediaSources = sourcesArray
		.sort((a, b) => a[0] - b[0])
		.reduce((arr, [breakpoint, source], ix, origin) => {
			const media =
				ix === origin.length - 1
					? `screen and (min-width:${breakpoint}px)`
					: `screen and (max-width:${breakpoint}px)`;
			if (typeof source === 'string') {
				return arr.concat({
					src: source,
					type: `video/${(source || '').split('.').slice(-1)[0]}`,
					media,
				});
			}
			return arr.concat({ ...source, media });
		}, [] as TPlayableVideoSource[]);
	if (!mediaSources.length) return null;
	const currentSource = useMedia(
		mediaSources.map((v) => v.media),
		mediaSources,
		mediaSources.slice(-1)[0],
	);

	return <source {...currentSource} />;
};

export default AdaptiveVideoSource;

import { Howl, Howler } from 'howler';
import { useEffect } from 'react';
import { TWithSoundSettingsProps, withSoundSettings } from '../../../contexts/SoundSettings';
import { normalizeVolume } from '../../../contexts/SoundSettings/helpers';

type TMusicPlayerProps = {
	src: string;
} & TWithSoundSettingsProps;

const BackgroundMusicPlayer: React.FC<TMusicPlayerProps> = ({ src, soundSettings }) => {
	const { musicVolume, masterVolume } = soundSettings;
	const backgroundMusic = new Howl({
		src: [src],
		onplay: () => {
			console.info(`Now playing: ${(src || '').split('.').slice(0, -1).join('.')}`);
		},
		onplayerror: () => {
			backgroundMusic.once('unlock', function () {
				backgroundMusic.play();
			});
		},
		html5: true,
		loop: true,
		autoplay: true,
	});
	useEffect(() => {
		Howler.volume(normalizeVolume(musicVolume, masterVolume));
		backgroundMusic.play();
		return () => {
			backgroundMusic.stop();
			backgroundMusic.unload();
		};
	}, []);
	return null;
};

export default withSoundSettings(BackgroundMusicPlayer);

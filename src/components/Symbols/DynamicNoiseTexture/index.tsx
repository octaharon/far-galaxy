import React from 'react';
import BasicMaths from '../../../../definitions/maths/BasicMaths';
import { TWithGraphicSettingsProps } from '../../../contexts/GraphicSettings';
import { getTextureGeneratedResolution } from '../../../contexts/GraphicSettings/helpers';
import _ from 'underscore';
import { preloadImage } from '../../UI/preloading';
import * as THREE from 'three';

/**
 *
 * @param id
 * @param blendImages key is image url or hex color, value is {ppacity, rotation,blending}
 * @param baseImage
 * @param colorOpacity
 * @param colorize
 * @param blur
 * @param onReady
 * @param graphicSettings
 * @param width
 * @param height
 * @param baseRotation
 * @constructor
 */
const DynamicNoiseTexture: React.FC<
	TWithGraphicSettingsProps & {
		id: string;
		blur?: number;
		width?: number;
		height?: number;
		onReady?: (canvas: HTMLCanvasElement) => any;
		colorA?: THREE.Color;
		colorB?: THREE.Color;
	}
> = ({
	id,
	blur,
	graphicSettings,
	width = getTextureGeneratedResolution(graphicSettings.textureSize),
	height = getTextureGeneratedResolution(graphicSettings.textureSize),
	onReady,
	colorA = new THREE.Color(0x000000),
	colorB = new THREE.Color(0xffffff),
}) => {
	const canvas = React.useRef<HTMLCanvasElement>();

	React.useEffect(() => {
		if (!canvas.current) return;
		const currentColor = new THREE.Color();
		const ctx = canvas.current.getContext('2d');
		ctx.globalCompositeOperation = 'destination-atop';
		ctx.globalAlpha = 1;
		if (blur)
			ctx.filter = `blur(${
				(blur * getTextureGeneratedResolution(graphicSettings.textureSize)) /
				getTextureGeneratedResolution('QHD')
			}px)`;
		const drawPixel = (x: number, y: number, color: THREE.Color) => {
			const index = 4 * (width * y + x);

			data[index] = Math.round(color.r * 255);
			data[index + 1] = Math.round(color.g * 255);
			data[index + 2] = Math.round(color.b * 255);
			data[index + 3] = 255;
		};
		const img = ctx.createImageData(width, height);
		const data = img.data;
		for (let x = 0; x < width; x++)
			for (let y = 0; y < height; y++) {
				const noiseValue = Math.random();
				currentColor.lerpColors(colorA, colorB, noiseValue);
				drawPixel(x, y, currentColor);
			}
		ctx.putImageData(img, 0, 0);

		if (onReady) onReady(canvas.current);
	}, [canvas, blur, onReady, graphicSettings, width, height]);
	return (
		<canvas
			id={id}
			ref={canvas}
			width={width}
			height={height}
			style={{
				width: width + 'px',
				height: height + 'px',
				position: 'fixed',
				zIndex: 1e6,
				right: '150%',
			}}
		/>
	);
};

export default React.memo(DynamicNoiseTexture, _.isEqual);

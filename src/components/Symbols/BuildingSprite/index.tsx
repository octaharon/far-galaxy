import React from 'react';
import { IFacilityProvider } from '../../../../definitions/orbital/facilities/types';
import BuildingManager from '../../../../definitions/orbital/GenericBaseBuildingManager';
import { TBaseBuildingType } from '../../../../definitions/orbital/types';
import useHover from '../../../utils/useHover';
import { classnames, getTemplateColor, IUICommonProps } from '../../UI';
import SVGIconContainer, {
	SVGContrastGlowFilterID,
	SVGFractalNoiseFilterID,
	SVGMaskDilateFilterID,
} from '../../UI/SVG/IconContainer';
import BuildingSpriteCollection from '../buildingSprites';
import styles from './index.scss';
import {
	CursorFilterId,
	CursorGradientId,
	CursorMaskId,
	LightNoiseFilterID,
	TextureFilterId,
	TextureMaskId,
	viewBoxSize,
} from './partials/constants';
import { SVGEstablishmentFilter } from './partials/establishmentFilter';
import { SVGFacilityFilter } from './partials/facilityFilter';
import { BuildingFeatureCollection } from './partials/features';
import './partials/globalFilters';
import { isFacility, isStateAvailableToBuilding } from './partials/predicates';
import {
	BUILDING_SPRITE_STATE_ACTIVE,
	BUILDING_SPRITE_STATE_BASE,
	BUILDING_SPRITE_STATE_CURSOR,
	BUILDING_SPRITE_STATE_OUTLINE,
	BUILDING_SPRITE_STATE_OUTPUT,
	BUILDING_SPRITE_STATE_RESOURCE,
	TBuildingSprite,
	TBuildingSpriteState,
} from './types';

export type UIBuildingSpriteProps<K extends TBaseBuildingType> = React.PropsWithChildren<
	IUICommonProps & {
		buildingType: K;
		buildingId: number;
		transparent?: boolean;
		state?: TBuildingSpriteState<K>;
		onSwitch?: (t: TBuildingSpriteState<K>) => any;
	}
>;

function factoryStateMachine(state: TBuildingSpriteState<TBaseBuildingType.factory>): TBuildingSpriteState<any> {
	switch (state) {
		case BUILDING_SPRITE_STATE_BASE:
			return BUILDING_SPRITE_STATE_ACTIVE;
		default:
			return BUILDING_SPRITE_STATE_BASE;
	}
}

function establishmentStateMachine(
	state: TBuildingSpriteState<any>,
): TBuildingSpriteState<TBaseBuildingType.establishment> {
	switch (state) {
		case BUILDING_SPRITE_STATE_BASE:
			return BUILDING_SPRITE_STATE_ACTIVE;
		default:
			return BUILDING_SPRITE_STATE_BASE;
	}
}

function facilityStateMachine(
	state: TBuildingSpriteState<any>,
	resourceMode = false,
): TBuildingSpriteState<TBaseBuildingType.facility> {
	switch (state) {
		case BUILDING_SPRITE_STATE_BASE:
			return BUILDING_SPRITE_STATE_OUTPUT;
		case BUILDING_SPRITE_STATE_OUTPUT:
			return resourceMode ? BUILDING_SPRITE_STATE_RESOURCE : BUILDING_SPRITE_STATE_BASE;
		default:
			return BUILDING_SPRITE_STATE_BASE;
	}
}

function nextState<K extends TBaseBuildingType>(t: K, state: TBuildingSpriteState<K>, resourceMode = false) {
	switch (t) {
		case TBaseBuildingType.establishment:
			return establishmentStateMachine(state);
		case TBaseBuildingType.factory:
			return factoryStateMachine(state as TBuildingSpriteState<TBaseBuildingType.factory>);
		case TBaseBuildingType.facility:
			return facilityStateMachine(state as TBuildingSpriteState<TBaseBuildingType.facility>, resourceMode);
		default:
			return BUILDING_SPRITE_STATE_BASE;
	}
}

export function UIBuildingSprite<K extends TBaseBuildingType>({
	buildingType,
	buildingId,
	className,
	transparent = false,
	state = null,
	onSwitch,
	onClick,
}: UIBuildingSpriteProps<K>) {
	const provider = BuildingManager.getProvider(buildingType);
	const sprites = BuildingSpriteCollection?.[buildingType]?.[buildingId];

	const hasResourceMode =
		buildingType === TBaseBuildingType.facility
			? (provider as IFacilityProvider).instantiate(null, buildingId).isResourceModeEnabled()
			: false;

	const maskId = `${TextureMaskId}-${buildingType}-${buildingId}`;
	const cursorId = `${CursorMaskId}-${buildingType}-${buildingId}`;
	const textureId = `${TextureFilterId}-${buildingType}-${buildingId}`;

	const [currentState, setState] = React.useState<TBuildingSpriteState<K>>(null);

	React.useEffect(() => {
		setState(
			isStateAvailableToBuilding(buildingType, state, hasResourceMode)
				? state
				: isStateAvailableToBuilding(buildingType, currentState, hasResourceMode)
				? currentState
				: BUILDING_SPRITE_STATE_BASE,
		);
	}, [buildingType, state, hasResourceMode, buildingId, currentState]);

	const [hoverRef, isHovered] = useHover();

	const clickHandler = (ev: React.MouseEvent) => {
		const newState = nextState(buildingType, currentState, hasResourceMode) as TBuildingSpriteState<K>;
		setState(newState);
		if (onSwitch) onSwitch(newState as TBuildingSpriteState<K>);
		if (onClick) onClick(ev);
	};

	if (!sprites) return null;

	const hasOutline = [BUILDING_SPRITE_STATE_CURSOR, BUILDING_SPRITE_STATE_OUTLINE].includes(currentState);

	return (
		<div className={classnames(styles.buildingSprite, className)}>
			<div className={classnames(styles.imageWrapper, transparent ? styles.transparent : '')}>
				<div
					className={styles.floorCursor}
					style={{
						opacity: [BUILDING_SPRITE_STATE_CURSOR].includes(currentState) && isHovered ? 0.65 : 0,
					}}
				/>
				<div
					className={styles.imageLayer}
					style={{
						filter: SVGIconContainer.getUrlRef(SVGContrastGlowFilterID),
						opacity: hasOutline ? 1 : 0,
						backgroundImage: `url(${sprites.outline})`,
					}}
				/>
				<div
					className={styles.imageLayer}
					style={{
						backgroundImage: `url(${sprites.base})`,
						opacity: !hasOutline ? 1 : 0,
					}}
				/>
				{isFacility(buildingType, sprites) ? (
					<>
						<div
							className={styles.imageLayer}
							style={{
								opacity: [BUILDING_SPRITE_STATE_OUTPUT].includes(currentState) ? 1 : 0,
								backgroundImage: `url(${
									(sprites as TBuildingSprite<TBaseBuildingType.facility>).output
								}`,
							}}
						/>
						{hasResourceMode && (
							<div
								className={styles.imageLayer}
								style={{
									opacity: [BUILDING_SPRITE_STATE_RESOURCE].includes(currentState) ? 1 : 0,
									backgroundImage: `url(${
										(sprites as TBuildingSprite<TBaseBuildingType.facility>).resource
									}`,
								}}
							/>
						)}
					</>
				) : (
					<div
						className={styles.imageLayer}
						style={{
							opacity: [BUILDING_SPRITE_STATE_ACTIVE].includes(currentState) ? 1 : 0,
							backgroundImage: `url(${(sprites as TBuildingSprite<TBaseBuildingType.factory>).active}`,
						}}
					/>
				)}
				<svg viewBox={`0 0 ${viewBoxSize} ${viewBoxSize}`}>
					<defs>
						<mask id={maskId}>
							<rect x={0} y={0} width={viewBoxSize} height={viewBoxSize} fill="#000" />
							{sprites.mask && (
								<image href={sprites.mask} x={0} y={0} width={viewBoxSize} height={viewBoxSize} />
							)}
						</mask>
						{isFacility(buildingType, sprites) ? (
							<SVGFacilityFilter buildingId={buildingId} textureId={textureId} />
						) : (
							<SVGEstablishmentFilter buildingId={buildingId} textureId={textureId} />
						)}
						<mask id={cursorId}>
							<rect x={0} y={0} width={viewBoxSize} height={viewBoxSize} fill="#000" />
							<image
								href={sprites.outline}
								x={0}
								y={0}
								width={viewBoxSize}
								height={viewBoxSize}
								filter={SVGIconContainer.getUrlRef(SVGMaskDilateFilterID)}
							/>
						</mask>
					</defs>
					<g
						mask={SVGIconContainer.getUrlRef(maskId)}
						className={styles.texture}
						opacity={buildingType === TBaseBuildingType.factory ? 0.4 : 0.5}
					>
						<image
							href={sprites.base}
							x={0}
							y={0}
							width={viewBoxSize}
							height={viewBoxSize}
							className={styles.imageLayer}
							style={{
								opacity: hasOutline ? 0 : 1,
							}}
							filter={SVGIconContainer.getUrlRef(
								buildingType === TBaseBuildingType.establishment ? SVGFractalNoiseFilterID : textureId,
							)}
						/>
					</g>
					{/*	<g className={styles.sparks}>
						{isFacility ? (
							<>
								{(sprites as TBuildingSprite<TBaseBuildingType.facility>).output_features?.length &&
									[BUILDING_SPRITE_STATE_OUTPUT].includes(currentState) &&
									(sprites as TBuildingSprite<TBaseBuildingType.facility>).output_features.map(
										featureRender,
									)}
								{(sprites as TBuildingSprite<TBaseBuildingType.facility>).resource_features?.length &&
									[BUILDING_SPRITE_STATE_RESOURCE].includes(currentState) &&
									(sprites as TBuildingSprite<TBaseBuildingType.facility>).resource_features.map(
										featureRender,
									)}
							</>
						) : (
							<>
								{(sprites as TBuildingSprite<TBaseBuildingType.factory>).features?.length &&
									[BUILDING_SPRITE_STATE_ACTIVE].includes(currentState) &&
									(sprites as TBuildingSprite<TBaseBuildingType.factory>).features.map(featureRender)}
							</>
						)}
					</g>*/}
					<g className={styles.shimmer}>
						{isFacility(buildingType, sprites) ? (
							<>
								{
									<image
										href={(sprites as TBuildingSprite<TBaseBuildingType.facility>).output_light}
										filter={SVGIconContainer.getUrlRef(LightNoiseFilterID)}
										x={0}
										y={0}
										width={viewBoxSize}
										height={viewBoxSize}
										className={classnames(styles.imageLayer, styles.light)}
										style={{
											opacity: [BUILDING_SPRITE_STATE_OUTPUT].includes(currentState) ? 1 : 0,
										}}
									/>
								}
								{hasResourceMode && (
									<image
										href={(sprites as TBuildingSprite<TBaseBuildingType.facility>).resource_light}
										filter={SVGIconContainer.getUrlRef(LightNoiseFilterID)}
										x={0}
										y={0}
										width={viewBoxSize}
										height={viewBoxSize}
										className={classnames(styles.imageLayer, styles.light)}
										style={{
											opacity: [BUILDING_SPRITE_STATE_RESOURCE].includes(currentState) ? 1 : 0,
										}}
									/>
								)}
							</>
						) : (
							<>
								{
									<image
										href={(sprites as TBuildingSprite<TBaseBuildingType.factory>).light}
										filter={SVGIconContainer.getUrlRef(LightNoiseFilterID)}
										x={0}
										y={0}
										width={viewBoxSize}
										height={viewBoxSize}
										className={classnames(styles.imageLayer, styles.light)}
										style={{
											opacity: [BUILDING_SPRITE_STATE_ACTIVE].includes(currentState) ? 1 : 0,
										}}
									/>
								}
							</>
						)}
					</g>
					<g
						filter={hasOutline ? SVGIconContainer.getUrlRef(CursorFilterId) : null}
						style={{
							mixBlendMode: hasOutline ? 'color-dodge' : 'initial',
						}}
						className={styles.cursorGroup}
					>
						<g mask={SVGIconContainer.getUrlRef(cursorId)}>
							<rect
								x={0}
								y={0}
								width={viewBoxSize}
								height={viewBoxSize}
								fill={
									hasOutline
										? SVGIconContainer.getUrlRef(CursorGradientId)
										: buildingType === TBaseBuildingType.establishment
										? getTemplateColor('light_golden')
										: buildingType === TBaseBuildingType.facility
										? getTemplateColor('light_blue')
										: getTemplateColor('text_highlight')
								}
								opacity={hasOutline || isHovered ? 0.65 : 0}
							/>
						</g>
					</g>
					<g className={styles.cursor}>
						<path
							d={sprites.outlinePath}
							fill={getTemplateColor(
								[BUILDING_SPRITE_STATE_OUTLINE].includes(currentState) ? 'tealish' : 'light_green',
							)}
							opacity={
								[BUILDING_SPRITE_STATE_OUTLINE].includes(currentState) ||
								([BUILDING_SPRITE_STATE_CURSOR].includes(currentState) && isHovered)
									? 0.35
									: 0.01
							}
							onClick={clickHandler}
							ref={hoverRef as React.Ref<SVGPathElement>}
						/>
					</g>
				</svg>
				<div className={styles.imageFill}>
					<BuildingFeatureCollection
						buildingType={buildingType}
						sprites={sprites}
						currentState={currentState}
					/>
				</div>
			</div>
			{!transparent && <div className={styles.border} />}
		</div>
	);
}

export default UIBuildingSprite;

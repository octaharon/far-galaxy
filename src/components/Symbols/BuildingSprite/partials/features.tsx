import React from 'react';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import { TBaseBuildingType } from '../../../../../definitions/orbital/types';
import { classnames } from '../../../UI';
import styles from '../index.scss';
import {
	BUILDING_SPRITE_STATE_ACTIVE,
	BUILDING_SPRITE_STATE_OUTPUT,
	BUILDING_SPRITE_STATE_RESOURCE,
	TBuildingSprite,
	TBuildingSpriteFeatureParams,
	TBuildingSpriteFeatures,
	TBuildingSpriteState,
} from '../types';
import { sparkAnimationDuration, viewBoxSize } from './constants';
import { isEstablishment, isFacility, isFactory } from './predicates';

const featureRender = (
	img: string,
	imgKey: string,
	imgSize: TBuildingSpriteFeatureParams['size'],
	params: TBuildingSpriteFeatureParams['params'],
	ix: number,
	total: number,
) => {
	const imgKeys = Object.keys(params);
	const currentKey = imgKeys[ix];
	const props = params[currentKey];
	if (!props) console.warn(params, imgKey, params);
	return (
		<div
			key={imgKey}
			className={classnames(styles.imageSprite, styles.spark)}
			style={{
				width: `${(imgSize.width / viewBoxSize) * 100}%`,
				height: `${(imgSize.height / viewBoxSize) * 100}%`,
				backgroundImage: `url('${img}')`,
				transform: `translate(-${(props.x / imgSize.width) * 100}%, -${(props.y / imgSize.height) * 100}%)`,
				animationDelay:
					(
						sparkAnimationDuration *
						(0.15 +
							BasicMaths.applyModifier(ix / total, {
								bias: BasicMaths.lerp({
									min: 0,
									max: 0.8 / total,
									x: Math.random(),
								}),
							}))
					).toPrecision(3) + 's',
				animationDuration: sparkAnimationDuration + 's',
			}}
		/>
	);
};

export const BuildingFeatures = <K extends TBaseBuildingType>({
	type,
	params,
	outputMode = true,
}: {
	type: K;
	params: Partial<TBuildingSpriteFeatures<K>>;
	outputMode?: boolean;
}): JSX.Element => {
	return (
		<>
			{isFacility(type, params)
				? outputMode
					? Object.keys(params?.output_features?.params ?? []).map((f, ix, arr) =>
							featureRender(
								params.output_features_image,
								f as string,
								params.output_features.size,
								params.output_features.params,
								ix,
								arr.length,
							),
					  )
					: Object.keys(params?.resource_features?.params ?? []).map((f, ix, arr) =>
							featureRender(
								params.resource_features_image,
								f as string,
								params.resource_features.size,
								params.resource_features.params,
								ix,
								arr.length,
							),
					  )
				: isEstablishment(type, params) || isFactory(type, params)
				? Object.keys(params?.features?.params ?? []).map((f, ix, arr) =>
						featureRender(
							params.features_image,
							f as string,
							params.features.size,
							params.features.params,
							ix,
							arr.length,
						),
				  )
				: null}
		</>
	);
};

export function BuildingFeatureCollection<T extends TBaseBuildingType>({
	buildingType,
	sprites,
	currentState = null,
}: {
	buildingType: T;
	sprites: TBuildingSprite<T>;
	currentState: TBuildingSpriteState<T>;
}) {
	return isFacility(buildingType, sprites) &&
		[BUILDING_SPRITE_STATE_OUTPUT].includes(currentState) &&
		sprites?.output_features_image ? (
		<BuildingFeatures type={TBaseBuildingType.facility} params={sprites} outputMode={true} />
	) : isFacility(buildingType, sprites) &&
	  [BUILDING_SPRITE_STATE_RESOURCE].includes(currentState) &&
	  sprites?.resource_features_image ? (
		<BuildingFeatures type={TBaseBuildingType.facility} params={sprites} outputMode={false} />
	) : [BUILDING_SPRITE_STATE_ACTIVE].includes(currentState) &&
	  isEstablishment(buildingType, sprites) &&
	  sprites?.features_image ? (
		<BuildingFeatures type={TBaseBuildingType.establishment} params={sprites} />
	) : [BUILDING_SPRITE_STATE_ACTIVE].includes(currentState) &&
	  isFactory(buildingType, sprites) &&
	  sprites?.features_image ? (
		<BuildingFeatures type={TBaseBuildingType.factory} params={sprites} />
	) : null;
}

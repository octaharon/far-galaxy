import { TBaseBuildingType } from '../../../../../definitions/orbital/types';
import {
	BUILDING_SPRITE_STATE_ACTIVE,
	BUILDING_SPRITE_STATE_BASE,
	BUILDING_SPRITE_STATE_CURSOR,
	BUILDING_SPRITE_STATE_OUTLINE,
	BUILDING_SPRITE_STATE_OUTPUT,
	BUILDING_SPRITE_STATE_RESOURCE,
	TBuildingSpriteFeatures,
	TBuildingSpriteState,
} from '../types';

export function isFacility(
	type: TBaseBuildingType,
	params: any,
): params is TBuildingSpriteFeatures<TBaseBuildingType.facility> {
	return type === TBaseBuildingType.facility;
}

export function isEstablishment(
	type: TBaseBuildingType,
	params: any,
): params is TBuildingSpriteFeatures<TBaseBuildingType.establishment> {
	return type === TBaseBuildingType.establishment;
}

export function isFactory(
	type: TBaseBuildingType,
	params: any,
): params is TBuildingSpriteFeatures<TBaseBuildingType.factory> {
	return type === TBaseBuildingType.factory;
}

export function isStateAvailableToBuilding<K extends TBaseBuildingType>(
	t: K,
	state: any,
	resourceMode = false,
): state is TBuildingSpriteState<K> {
	switch (t) {
		case TBaseBuildingType.facility:
			return [
				BUILDING_SPRITE_STATE_BASE,
				BUILDING_SPRITE_STATE_CURSOR,
				BUILDING_SPRITE_STATE_OUTLINE,
				BUILDING_SPRITE_STATE_OUTPUT,
				resourceMode ? BUILDING_SPRITE_STATE_RESOURCE : null,
			].includes(state);
		default:
			return [
				BUILDING_SPRITE_STATE_BASE,
				BUILDING_SPRITE_STATE_CURSOR,
				BUILDING_SPRITE_STATE_OUTLINE,
				BUILDING_SPRITE_STATE_ACTIVE,
			].includes(state);
	}
}

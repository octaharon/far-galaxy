import React from 'react';

export const SVGEstablishmentFilter: React.FC<{ textureId: string; buildingId: number }> = ({
	buildingId,
	textureId,
}) => (
	<filter
		id={textureId}
		x="-20%"
		y="-20%"
		width="140%"
		height="140%"
		filterUnits="objectBoundingBox"
		primitiveUnits="userSpaceOnUse"
		colorInterpolationFilters="sRGB"
	>
		<feTurbulence
			type="fractalNoise"
			baseFrequency="0.035 0.008"
			numOctaves="1"
			seed="2"
			stitchTiles="stitch"
			result="turbulence"
		/>
		<feTurbulence
			type="fractalNoise"
			baseFrequency="0.095 0.023"
			numOctaves="1"
			seed={buildingId || 0}
			stitchTiles="stitch"
			result="turbulence1"
		/>
		<feMerge result="merge">
			<feMergeNode in="turbulence1" result="mergeNode" />
			<feMergeNode in="turbulence" result="mergeNode1" />
		</feMerge>
		<feColorMatrix type="saturate" values="10" in="merge" result="colormatrix" />
		<feColorMatrix
			type="matrix"
			values="1 0 0 0 0
0 1 0 0 0
0 0 1 0 0
0 0 0 10 0"
			in="colormatrix"
			result="colormatrix1"
		/>
		<feMerge x="0%" y="0%" width="100%" height="100%" result="merge1">
			<feMergeNode in="colormatrix2" />
			<feMergeNode in="colormatrix2" />
		</feMerge>
		<feDisplacementMap
			in="colormatrix1"
			in2="colormatrix"
			scale="40"
			xChannelSelector="R"
			yChannelSelector="G"
			result="displacementMap"
		/>
		<feComposite in="displacementMap" in2="SourceAlpha" operator="in" result="composite1" />
		<feColorMatrix
			type="saturate"
			values="0"
			x="0%"
			y="0%"
			width="100%"
			height="100%"
			in="composite1"
			result="colormatrix2"
		/>
		<feBlend
			mode="hard-light"
			x="0%"
			y="0%"
			width="100%"
			height="100%"
			in="SourceGraphic"
			in2="colormatrix2"
			result="blend"
		/>
		<feTurbulence
			type="turbulence"
			baseFrequency="0.5 0.5"
			numOctaves="2"
			seed={buildingId || 0}
			stitchTiles="stitch"
			x="0%"
			y="0%"
			width="100%"
			height="100%"
			result="turbulence2"
		/>
		<feDiffuseLighting
			surfaceScale="5"
			diffuseConstant="0.6"
			lightingColor="#ffffff"
			x="0%"
			y="0%"
			width="100%"
			height="100%"
			in="turbulence2"
			result="diffuseLighting"
		>
			<feDistantLight azimuth="3" elevation="100" />
		</feDiffuseLighting>
		<feComposite
			in="diffuseLighting"
			in2="SourceGraphic"
			operator="in"
			x="0%"
			y="0%"
			width="100%"
			height="100%"
			result="composite3"
		/>
		<feBlend
			mode="soft-light"
			x="0%"
			y="0%"
			width="100%"
			height="100%"
			in="composite3"
			in2="blend"
			result="blend1"
		/>
	</filter>
);

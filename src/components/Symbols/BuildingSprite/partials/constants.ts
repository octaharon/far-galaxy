import { getTemplateColor } from '../../../UI';

export const sparkAnimationDuration = 3.5;
export const viewBoxSize = 500;
export const LightNoiseFilterID = 'building-filter-light-noise';
export const TextureMaskId = 'building-texture-mask';
export const TextureFilterId = 'building-texture';
export const CursorFilterId = 'building-filter-cursor';
export const CursorGradientId = 'building-cursor-gradient';
export const CursorMaskId = 'building-cursor-mask';
export const HighlightColor2 = getTemplateColor('light_teal');
export const HighlightColor1 = getTemplateColor('light_green');

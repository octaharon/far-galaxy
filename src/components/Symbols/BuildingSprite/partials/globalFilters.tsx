import React from 'react';
import SVGIconContainer from '../../../UI/SVG/IconContainer';
import { CursorFilterId, CursorGradientId, HighlightColor1, HighlightColor2, LightNoiseFilterID } from './constants';

SVGIconContainer.registerGradient(
	CursorGradientId,
	<linearGradient x1="45%" x2="55%" y1="25%" y2="75%" gradientUnits="objectBoundingBox">
		<stop offset="0.1" stopColor={HighlightColor2} />
		<stop offset="0.9" stopColor={HighlightColor1} />
	</linearGradient>,
)
	.registerFilter(
		CursorFilterId,
		<filter
			x="-20%"
			y="-20%"
			width="140%"
			height="140%"
			filterUnits="objectBoundingBox"
			primitiveUnits="objectBoundingBox"
			colorInterpolationFilters="linearRGB"
		>
			<feGaussianBlur stdDeviation="2" in="SourceAlpha" result="blur1" />
			<feOffset dx="1" dy="1" in="blur1" result="offset" />
			<feOffset dx="-1" dy="-1" in="blur1" result="offset2" />
			<feOffset dx="-2" dy="2" in="blur1" result="offset3" />
			<feOffset dx="-2" dy="-2" in="blur1" result="offset1" />
			<feFlood floodColor={HighlightColor1} floodOpacity="0.7" result="flood" />
			<feFlood floodColor={HighlightColor2} floodOpacity="0.7" result="flood1" />
			<feComposite in="flood" in2="offset" operator="in" result="composite" />
			<feComposite in="flood" in2="offset1" operator="in" result="composite3" />
			<feComposite in="flood1" in2="offset2" operator="in" result="composite1" />
			<feComposite in="flood1" in2="offset3" operator="in" result="composite2" />
			<feMerge result="merge">
				<feMergeNode in="composite" />
				<feMergeNode in="composite2" />
				<feMergeNode in="composite1" />
				<feMergeNode in="composite3" />
			</feMerge>
			<feBlend mode="lighten" in="merge" in2="SourceGraphic" result="blend1" />
		</filter>,
	)
	.registerFilter(
		LightNoiseFilterID,
		<filter
			x="-20%"
			y="-20%"
			width="140%"
			height="140%"
			filterUnits="userSpaceOnUse"
			primitiveUnits="userSpaceOnUse"
			colorInterpolationFilters="sRGB"
		>
			<feGaussianBlur
				stdDeviation="15"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				in="SourceGraphic"
				edgeMode="none"
				result="blur"
			>
				<animate
					attributeName="stdDeviation"
					attributeType="XML"
					dur="2s"
					repeatCount="indefinite"
					values="15;25;15"
					fill="freeze"
				/>
			</feGaussianBlur>
			<feFlood floodColor="#66FFFF" floodOpacity="0.3" x="0%" y="0%" width="100%" height="100%" result="flood" />
			<feTurbulence
				type="fractalNoise"
				baseFrequency=".2"
				numOctaves="1"
				seed="9"
				stitchTiles="stitch"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				result="turbulence"
			>
				<animate
					attributeName="seed"
					attributeType="XML"
					dur="1s"
					repeatCount="indefinite"
					values="10;20;10"
					fill="freeze"
				/>
			</feTurbulence>
			<feColorMatrix
				type="saturate"
				values="0"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				in="turbulence"
				result="colormatrix"
			/>
			<feBlend mode="color" x="0%" y="0%" width="100%" height="100%" in="blend" in2="flood" result="blend1" />
			<feComposite
				in="blend1"
				in2="blur"
				operator="in"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				result="composite"
			/>
			<feBlend
				mode="hard-light"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				in="blur"
				in2="composite"
				result="blend"
			/>
			<feBlend
				mode="multiply"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				in="SourceGraphic"
				in2="blend"
				result="blend2"
			/>
		</filter>,
	);

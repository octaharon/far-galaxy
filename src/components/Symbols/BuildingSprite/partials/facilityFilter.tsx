import React from 'react';

export const SVGFacilityFilter: React.FC<{ textureId: string; buildingId: number }> = ({ textureId, buildingId }) => (
	<filter
		id={textureId}
		x="-20%"
		y="-20%"
		width="140%"
		height="140%"
		filterUnits="objectBoundingBox"
		primitiveUnits="userSpaceOnUse"
		colorInterpolationFilters="sRGB"
	>
		<feTurbulence
			type="fractalNoise"
			baseFrequency="0.25"
			numOctaves="5"
			seed={buildingId || 0}
			stitchTiles="stitch"
			result="turbulence"
		/>
		<feDiffuseLighting
			surfaceScale=".5"
			diffuseConstant="2.5"
			lightingColor="#ffffff"
			in="turbulence"
			result="diffuseLighting"
		>
			<feDistantLight azimuth="100" elevation="17" />
		</feDiffuseLighting>
		<feComposite in="diffuseLighting" in2="SourceAlpha" operator="in" result="composite" />
		<feBlend
			mode="soft-light"
			x="0%"
			y="0%"
			width="100%"
			height="100%"
			in="composite"
			in2="SourceGraphic"
			result="blend"
		/>
	</filter>
);

import React from 'react';
import { TBaseBuildingType } from '../../../../definitions/orbital/types';

export type TBuildingSpriteLayers = 'base' | 'outline' | 'mask';
export type TBuildingSpriteLayersAdditionalFactory = 'active' | 'light';
export type TBuildingSpriteLayersAdditionalFacility = 'output' | 'output_light' | 'resource' | 'resource_light';
export type TBuildingSpriteLayersAdditionalEstablishment = 'active' | 'light';
export type TBuildingSpriteFeatureParams = {
	params: Record<string, { x: number; y: number }>;
	size: { width: number; height: number };
};

export type TBuildingSpriteFeatures<K extends TBaseBuildingType> = K extends TBaseBuildingType.facility
	? Record<'output_features' | 'resource_features', TBuildingSpriteFeatureParams> &
			Record<'output_features_image' | 'resource_features_image', string>
	: Record<'features', TBuildingSpriteFeatureParams> & Record<'features_image', string>;

export type TBuildingSpriteAdditionalKeys<K extends TBaseBuildingType> = K extends TBaseBuildingType.factory
	? TBuildingSpriteLayersAdditionalFactory
	: K extends TBaseBuildingType.facility
	? TBuildingSpriteLayersAdditionalFacility
	: K extends TBaseBuildingType.establishment
	? TBuildingSpriteLayersAdditionalEstablishment
	: never;

export type TBuildingSpriteKeys<K extends TBaseBuildingType> = TBuildingSpriteLayers | TBuildingSpriteAdditionalKeys<K>;

export type TBuildingSprite<K extends TBaseBuildingType> = Partial<
	TBuildingSpriteFeatures<K> & Record<TBuildingSpriteKeys<K>, string>
> & {
	outlinePath?: string;
	outlineStyle?: React.CSSProperties;
};

export const BUILDING_SPRITE_STATE_CURSOR = 'cursor';
export const BUILDING_SPRITE_STATE_BASE = 'base';
export const BUILDING_SPRITE_STATE_OUTLINE = 'outline';
export const BUILDING_SPRITE_STATE_ACTIVE = 'active';
export const BUILDING_SPRITE_STATE_OUTPUT = 'output';
export const BUILDING_SPRITE_STATE_RESOURCE = 'resource';

export type TBuildingSpriteStateBase = keyof {
	[BUILDING_SPRITE_STATE_CURSOR]: true;
	[BUILDING_SPRITE_STATE_OUTLINE]: true;
	[BUILDING_SPRITE_STATE_BASE]: true;
};
export type TBuildingSpriteStateFactory = keyof {
	[BUILDING_SPRITE_STATE_ACTIVE]: true;
};
export type TBuildingSpriteStateEstablishment = keyof {
	[BUILDING_SPRITE_STATE_ACTIVE]: true;
};
export type TBuildingSpriteStateFacility = keyof {
	[BUILDING_SPRITE_STATE_OUTPUT]: true;
	[BUILDING_SPRITE_STATE_RESOURCE]: true;
};

export type TBuildingSpriteAdditionalState<K extends TBaseBuildingType> = K extends TBaseBuildingType.factory
	? TBuildingSpriteStateFactory
	: K extends TBaseBuildingType.facility
	? TBuildingSpriteStateFacility
	: K extends TBaseBuildingType.establishment
	? TBuildingSpriteStateEstablishment
	: never;

export type TBuildingSpriteState<K extends TBaseBuildingType> =
	| TBuildingSpriteStateBase
	| TBuildingSpriteAdditionalState<K>;

export type TBuildingImageProps<K extends TBaseBuildingType> = {
	sprite: TBuildingSprite<K>;
	state: TBuildingSpriteState<K>;
};

/* eslint-disable @typescript-eslint/no-var-requires */
import React from 'react';
import BasicMaths from '../../../../definitions/maths/BasicMaths';
import useHover from '../../../utils/useHover';
import { classnames, IUICommonProps } from '../../UI';
import styles from './index.scss';

export const MenuSprites = {
	conquest: {
		base: require('../../../../assets/img/ui/menu/conquest.avif').default,
		bloom: require('../../../../assets/img/ui/menu/conquest_bloom.avif').default,
	},
	orbital: {
		base: require('../../../../assets/img/ui/menu/orbital.avif').default,
		bloom: require('../../../../assets/img/ui/menu/orbital_bloom.avif').default,
	},
	prototype: {
		base: require('../../../../assets/img/ui/menu/prototype.avif').default,
		bloom: require('../../../../assets/img/ui/menu/prototype_bloom.avif').default,
	},
	skirmish: {
		base: require('../../../../assets/img/ui/menu/skirmish.avif').default,
		bloom: require('../../../../assets/img/ui/menu/skirmish_bloom.avif').default,
	},
	galaxy: {
		base: require('../../../../assets/img/ui/menu/galaxy.avif').default,
		bloom: require('../../../../assets/img/ui/menu/galaxy_bloom.avif').default,
	},
};

type TMenuSpriteType = keyof typeof MenuSprites;

const maxNativeWidth = 2560;
const maxNativeHeight = 1440;

const MenuSpriteSizes: Record<TMenuSpriteType, [number, number]> = {
	orbital: [283, 412],
	conquest: [528, 404],
	skirmish: [336, 239],
	prototype: [319, 293],
	galaxy: [550, 424],
};
const CaptionPositions: Record<TMenuSpriteType, [number, number]> = {
	orbital: [250, 0],
	conquest: [360, 55],
	skirmish: [0, -40],
	prototype: [-220, 25],
	galaxy: [-20, 20],
};

type TMenuAnimatedSpriteProps = IUICommonProps & {
	type: TMenuSpriteType;
	top: number;
	left: number;
	caption?: string;
	href?: string;
};

const mapX = (x: number) => BasicMaths.roundToPrecision((x / maxNativeWidth) * 100) + '%';
const mapY = (y: number) => BasicMaths.roundToPrecision((y / maxNativeHeight) * 100) + '%';

const mapXToSprite = (x: number, type: TMenuSpriteType) =>
	BasicMaths.roundToPrecision((x / MenuSpriteSizes[type][0]) * 100) + '%';
const mapYToSprite = (y: number, type: TMenuSpriteType) =>
	BasicMaths.roundToPrecision((y / MenuSpriteSizes[type][1]) * 100) + '%';

export const UIMenuCaptionClass = styles.menuSpriteCaption;

export const UIMenuSprite: React.FC<TMenuAnimatedSpriteProps> = ({ top, left, type, caption, href }) => {
	const [hoverRef, isHovered] = useHover();
	return (
		<div
			ref={hoverRef as React.Ref<HTMLDivElement>}
			className={styles.menuSprite}
			style={{
				width: mapX(MenuSpriteSizes[type][0]),
				height: mapY(MenuSpriteSizes[type][1]),
				left: mapX(left),
				top: mapY(top),
			}}
		>
			<div
				className={classnames(styles.imageLayer)}
				style={{
					backgroundImage: `url(${MenuSprites[type].base})`,
				}}
			/>
			<div
				className={classnames(styles.imageLayer, styles.imageBloom)}
				style={{
					opacity: isHovered ? 1 : 0.01,
					backgroundBlendMode: 'screen',
					backgroundImage: `url(${MenuSprites[type].bloom})`,
				}}
			/>
			{!!caption && (
				<a
					href={href}
					className={UIMenuCaptionClass}
					style={{
						left: mapXToSprite(CaptionPositions[type][0], type),
						top: mapYToSprite(CaptionPositions[type][1], type),
					}}
				>
					{caption}
				</a>
			)}
		</div>
	);
};

export default UIMenuSprite;

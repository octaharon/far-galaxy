import React from 'react';
import _ from 'underscore';
import { TWithGraphicSettingsProps } from '../../../contexts/GraphicSettings';
import { getTextureGeneratedResolution } from '../../../contexts/GraphicSettings/helpers';
import { preloadImage } from '../../UI/preloading';

export type TTextureRotation = 0 | 1 | 2 | 3;
const isColor = (path: string) => /^#?[0-9a-f]{6}$/is.test(path);

/**
 *
 * @param id
 * @param blendImages key is image url or hex color, value is {ppacity, rotation,blending}
 * @param baseImage
 * @param colorOpacity
 * @param colorize
 * @param blur
 * @param onReady
 * @param graphicSettings
 * @param width
 * @param height
 * @param baseRotation
 * @constructor
 */
const TextureMapper: React.FC<
	TWithGraphicSettingsProps & {
		id: string;
		baseImage: string;
		blendImages?: Record<
			string,
			{ opacity?: number; rotation?: TTextureRotation; blending?: GlobalCompositeOperation; blur?: number }
		>;
		baseRotation?: TTextureRotation;
		blur?: number;
		width?: number;
		height?: number;
		onReady?: (...images: HTMLImageElement[]) => void;
	}
> = ({
	id,
	blendImages,
	baseImage,
	blur,
	onReady,
	graphicSettings,
	width = getTextureGeneratedResolution(graphicSettings.textureSize),
	height = getTextureGeneratedResolution(graphicSettings.textureSize),
	baseRotation = 0,
}) => {
	const canvas = React.useRef<HTMLCanvasElement>();
	const rotationAngles = [0, Math.PI / 2, Math.PI, Math.PI * 1.5];
	const rotate = (ctx: CanvasRenderingContext2D, rotation: number) => {
		ctx.translate(width / 2, height / 2);
		ctx.rotate(rotation);
		ctx.translate(-width / 2, -height / 2);
	};
	React.useEffect(() => {
		const preloadedImages = [baseImage, ...(Object.keys(blendImages || []) as string[])].filter(
			(v) => !!v && !isColor(v),
		);
		console.log('drawing texture: loading images');
		Promise.all(preloadedImages.map(preloadImage)).then((textures) => {
			if (!canvas.current) return;
			console.log('drawing texture: stacking images');
			const textureCache = textures.reduce(
				(obj, img, index) => Object.assign(obj, { [preloadedImages[index]]: img }),
				{} as Record<string, HTMLImageElement>,
			);
			const ctx = canvas.current.getContext('2d');
			ctx.globalCompositeOperation = 'destination-atop';
			ctx.globalAlpha = 1;
			ctx.clearRect(0, 0, width, height);
			ctx.save();
			if (blur)
				ctx.filter = `blur(${
					(blur * getTextureGeneratedResolution(graphicSettings.textureSize)) /
					getTextureGeneratedResolution('QHD')
				}px)`;
			if (baseRotation) {
				rotate(ctx, rotationAngles[baseRotation]);
			}
			ctx.drawImage(textureCache[baseImage], 0, 0);
			ctx.restore();
			ctx.filter = '';

			if (blendImages && Object.keys(blendImages).length) {
				Object.keys(blendImages).forEach((blendImage) => {
					// console.log(blendImage, isColor(blendImage), textureCache);
					const params = blendImages[blendImage];
					ctx.save();
					ctx.filter = params.blur
						? `blur(${
								(params.blur * getTextureGeneratedResolution(graphicSettings.textureSize)) /
								getTextureGeneratedResolution('QHD')
						  }px)`
						: '';
					ctx.globalAlpha = params?.opacity ?? 1;
					if (isColor(blendImage)) {
						ctx.globalCompositeOperation = params?.blending ?? 'hue';
						ctx.fillStyle = `#${blendImage.trim()}`.replace(/#+/, '#');
						ctx.fillRect(0, 0, width, height);
					} else {
						ctx.globalCompositeOperation = params?.blending ?? 'destination-atop';
						if (Number.isFinite(params.rotation)) {
							rotate(ctx, rotationAngles[params.rotation % 4]);
						}
						ctx.drawImage(textureCache[blendImage], 0, 0);
					}
					ctx.restore();
				});
			}
			if (onReady) onReady(...Object.values(textureCache));
		});
	}, [canvas, baseImage, blur, onReady, blendImages, graphicSettings, width, height]);
	return (
		<canvas
			id={id}
			ref={canvas}
			width={width}
			height={height}
			style={{
				width: width + 'px',
				height: height + 'px',
				position: 'fixed',
				zIndex: 1e6,
				right: '150%',
			}}
		/>
	);
};

export default React.memo(TextureMapper, _.isEqual);

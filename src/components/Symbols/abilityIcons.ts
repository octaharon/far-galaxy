const AbilityIconList: Record<number, string> = {};

function requireAll(r: __WebpackModuleApi.RequireContext, cache: Record<number, string>) {
	return r.keys().forEach((key) => {
		cache[parseInt(key.toString().replace(/[^\d]+/, ''), 10)] = r(key);
	});
}

requireAll(require.context('../../../assets/img/svg/abilities', true, /\.svg$/), AbilityIconList);

export default AbilityIconList;

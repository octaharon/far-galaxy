import * as React from 'react';
import DIContainer from '../../../../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../../../../definitions/core/DI/injections';
import { classnames, getFactionClass } from '../../UI';
import style from './index.scss';
import { TChassisImageWrapperProps } from './types';

type TChassisImageCache = Record<string, React.ComponentType<React.SVGProps<SVGSVGElement>>>;

const ChassisImages = {} as TChassisImageCache;

function requireAll(r: __WebpackModuleApi.RequireContext, cache: TChassisImageCache) {
	return r.keys().forEach((file) => {
		const key = file.replace('./', '').replace(/\.tsx?/, '');
		if (cache[key]) throw new Error(`Chassis Image conflict id=${key}`);
		const module: any = r(file);
		if (module.default && module.default instanceof Function) cache[key] = module.default;
	});
}

requireAll(require.context('./chassis/', true, /\.tsx$/), ChassisImages);

export const ChassisImageProvider: React.FC<TChassisImageWrapperProps> = ({
	className,
	faction,
	type,
	chassisId,
	width = '100%',
	height = '100%',
}) => {
	let index = chassisId;
	if (!chassisId) return null;
	/*const ref = React.useRef(null);
  React.useEffect(() => {
    console.log(ref);
  });*/
	const keygen = (i: number) => `${type}/${i}`;
	const provider = DIContainer.getProvider(DIInjectableCollectibles.chassis);
	if (!ChassisImages[keygen(index)]) index = provider.getBaseChassis(provider.get(index))?.id;
	if (!ChassisImages[keygen(index)]) return null;
	const SVGComponent = ChassisImages[keygen(index)];
	return (
		<SVGComponent
			className={classnames(style.chassisRender, getFactionClass(faction), className)}
			x={0}
			y={0}
			width={width}
			height={height}
		/>
	);
};

export default ChassisImageProvider;

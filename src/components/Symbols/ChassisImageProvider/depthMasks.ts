type TDepthMaskStorage = Record<'massive' | 'water', Record<number, string>>;
const ChassisDepthMaskList: TDepthMaskStorage = {
	massive: {},
	water: {},
};

function requireAll(r: __WebpackModuleApi.RequireContext, cache: TDepthMaskStorage) {
	return r.keys().forEach((key) => {
		const id = parseInt(key.toString().replace(/[^\d]+/, ''), 10);
		const group = key.includes('massive') ? 'massive' : key.includes('water') ? 'water' : null;
		if (!group) return false;
		cache[group][id] = r(key);
	});
}

requireAll(
	require.context('../../../../assets/img/textures/ChassisDepthMap', true, /\.(jpg|png)$/),
	ChassisDepthMaskList,
);

export const getChassisDepthMask = (id: number) => ChassisDepthMaskList.massive[id] || null;
export const getChassisWaterMask = (id: number) => ChassisDepthMaskList.water[id] || null;

export default ChassisDepthMaskList;

import UnitChassis from '../../../../definitions/technologies/types/chassis';

export const defaultDepthCurve = [0, 0, 0.4, 0.85, 1];
export const depthCurves: Partial<Record<UnitChassis.chassisClass, number[]>> = {
	[UnitChassis.chassisClass.infantry]: [0, 0.2, 0.5, 1],
	[UnitChassis.chassisClass.mecha]: [0, 0.15, 0.3, 0.5, 0.95],
	[UnitChassis.chassisClass.fortress]: [0, 0.0, 0.3, 0.6, 1],
};
export const transformByClass: Partial<
	Record<UnitChassis.chassisClass, { scale?: number; offsetX?: number; offsetY?: number }>
> = {
	[UnitChassis.chassisClass.biotech]: {
		scale: 0.8,
		offsetX: -0.025,
		offsetY: 0.05,
	},
	[UnitChassis.chassisClass.infantry]: {
		scale: 0.75,
		offsetY: 0.05,
	},
	[UnitChassis.chassisClass.exosuit]: {
		scale: 0.75,
		offsetY: 0.05,
	},
	[UnitChassis.chassisClass.scout]: {
		scale: 0.85,
		offsetY: 0.05,
	},
	[UnitChassis.chassisClass.droid]: {
		scale: 0.9,
		offsetY: 0.05,
	},
	[UnitChassis.chassisClass.rover]: {
		scale: 0.9,
		offsetY: -0.025,
	},
	[UnitChassis.chassisClass.pointdefense]: {
		scale: 0.9,
		offsetY: -0.025,
	},
	[UnitChassis.chassisClass.hovertank]: {
		offsetX: -0.05,
		offsetY: -0.05,
		scale: 0.95,
	},
	[UnitChassis.chassisClass.combatdrone]: {
		offsetY: -0.05,
	},
	[UnitChassis.chassisClass.science_vessel]: {
		offsetY: -0.075,
		scale: 0.9,
	},
	[UnitChassis.chassisClass.bomber]: {
		offsetY: -0.1,
		scale: 0.85,
	},
	[UnitChassis.chassisClass.fighter]: {
		offsetY: -0.05,
		offsetX: 0.025,
		scale: 0.85,
	},
	[UnitChassis.chassisClass.swarm]: {
		scale: 0.85,
		offsetY: 0.05,
	},
	[UnitChassis.chassisClass.strike_aircraft]: {
		scale: 0.85,
		offsetY: -0.1,
	},
	[UnitChassis.chassisClass.tiltjet]: {
		scale: 0.85,
		offsetY: -0.05,
	},
	[UnitChassis.chassisClass.sup_aircraft]: {
		offsetY: -0.075,
		scale: 0.85,
	},
	[UnitChassis.chassisClass.sup_vehicle]: {
		offsetY: 0.075,
		scale: 0.9,
	},
	[UnitChassis.chassisClass.lav]: {
		scale: 0.95,
	},
	[UnitChassis.chassisClass.hav]: {
		offsetY: 0.05,
		scale: 0.95,
	},
	[UnitChassis.chassisClass.tank]: {
		scale: 0.9,
		offsetY: 0.05,
	},
	[UnitChassis.chassisClass.artillery]: {
		scale: 0.95,
		offsetY: 0.05,
	},
};
export const chassisCushions: Partial<
	Record<UnitChassis.chassisClass, Array<{ scale?: number; offsetX?: number; offsetY?: number }>>
> = {
	[UnitChassis.chassisClass.fortress]: [
		{
			offsetX: 1560,
			offsetY: 1360,
		},
		{
			offsetX: 1060,
			offsetY: 975,
		},
		{
			offsetX: 290,
			offsetY: 1090,
		},
		{
			offsetX: 470,
			offsetY: 1540,
		},
	],
	[UnitChassis.chassisClass.rover]: [
		{
			offsetX: 960,
			offsetY: 1020,
		},
	],
	[UnitChassis.chassisClass.pointdefense]: [
		{
			scale: 1.5,
			offsetX: 640,
			offsetY: 1050,
		},
	],
	[UnitChassis.chassisClass.launcherpad]: [
		{
			scale: 1.5,
			offsetX: 640,
			offsetY: 1050,
		},
	],
	[UnitChassis.chassisClass.science_vessel]: [
		{
			offsetX: 960,
			offsetY: 950,
		},
	],
	[UnitChassis.chassisClass.hovertank]: [
		{
			scale: 1.5,
			offsetX: 640,
			offsetY: 1080,
		},
	],
	[UnitChassis.chassisClass.combatdrone]: [
		{
			offsetX: 960,
			offsetY: 950,
		},
	],
};

import * as React from 'react';
import PoliticalFactions from '../../../../definitions/world/factions';

export interface IChassisImageDefinition {
	full: TChassisIconID;
	top: TChassisIconID;
	depthMap?: any;
}

export type TChassisIconID = string;

export type TChassisImageType = 'full' | 'topdown';
export type TChassisFullImageProps = {
	colorPrimary: string;
	colorSecondary: string;
};

export type TChassisAnimationState = 'idle' | 'moving' | 'attacking' | 'destroyed';

export type TChassisTopdownImageProps = TChassisFullImageProps & {
	moveDirection: number;
	shootDirection: number;
	animationState: TChassisAnimationState;
};

export type TChassisSpriteSpecificProps<T extends TChassisImageType> = T extends 'full'
	? TChassisFullImageProps
	: T extends 'topdown'
	? TChassisTopdownImageProps
	: never;

export type TChassisSpriteProps<T extends TChassisImageType> = React.SVGProps<SVGSVGElement> &
	TChassisSpriteSpecificProps<T>;

export type TChassisImageWrapperProps = {
	className?: string;
	type: TChassisImageType;
	chassisId: number;
	faction?: PoliticalFactions.TFactionID;
	width?: number | string;
	height?: number | string;
};

export type TChassisImageDictionary = Record<number, IChassisImageDefinition>;

export const CSS_FACTION_CLASS_PRIMARY = 'factionColorPrimary';
export const CSS_FACTION_CLASS_SECONDARY = 'factionColorSecondary';

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1136.9 830c.3 30.8 2 66 2 66s23.4 25.3 0 57c.3 34.5 0 65 0 65 18.9 24 8.1 47.3-1 58 .3 23 0 51 0 51h23l21 65-56.5 329s-2.1 118.4-30.6 187c-5.9 26.4-7 36-7 36l-5 2s-59.4 131-131.2 131c-82.2 0-130.6-131-130.6-131l-5-2s-1.2-9.5-7-36c-26.1-165.8-30.6-187-30.6-187L722 1192l21-65h23s-.3-28 0-51c-28.6-28.4-1-60-1-60s-.3-28.5 0-63c-24.7-24.1 0-59 0-59s1.7-31.2 2-62c-28.9-28-4.9-60.7 0-66V597l-18-81h29.4v-20H749l-6-154 35 1v-16l-29-1 32-146s5.8.1 15 0c2-19.6 13.4-45.2 19-56-.5-5.5 11.8-29.9 13.9-33.5C864.3 28.9 933.6 31 951.6 31c18.2 0 88.7-3.2 123.7 61 1.6 2.9 14 26.5 13.5 32 5.6 10.8 17 36.4 19 56 8.9.2 15 0 15 0l32 146-29 1v16l35.1-1-6 154h-29.5v20h29.5l-19 81 1 173s25.9 37 0 60z"
		/>
		<path
			fill="#e6e6e6"
			d="M795 1020.9c13.8 0 25.1 11.2 25.1 25.1s-11.2 25.1-25.1 25.1-25.1-11.2-25.1-25.1 11.3-25.1 25.1-25.1zm0-122c13.8 0 25.1 11.2 25.1 25.1s-11.2 25.1-25.1 25.1-25.1-11.2-25.1-25.1 11.3-25.1 25.1-25.1zm0-124c13.8 0 25.1 11.2 25.1 25.1s-11.2 25.1-25.1 25.1-25.1-11.2-25.1-25.1 11.3-25.1 25.1-25.1zm312 245.9c13.8 0 25 11.2 25 25.1 0 13.8-11.2 25.1-25 25.1s-25-11.2-25-25.1c0-13.8 11.2-25.1 25-25.1zm0-121.9c13.8 0 25 11.2 25 25.1s-11.2 25.1-25 25.1-25-11.2-25-25.1 11.2-25.1 25-25.1zm0-123.9c13.8 0 25 11.2 25 25.1 0 13.8-11.2 25.1-25 25.1s-25-11.2-25-25.1c0-13.9 11.2-25.1 25-25.1zM857 96.9c13.8 0 25.1 11.2 25.1 25.1s-11.2 25.1-25.1 25.1-25.1-11.2-25.1-25.1 11.3-25.1 25.1-25.1zm188 1c13.8 0 25.1 11.2 25.1 25.1s-11.2 25.1-25.1 25.1-25.1-11.2-25.1-25.1 11.3-25.1 25.1-25.1z"
		/>
		<linearGradient
			id="ch-topdown66002a"
			x1={1138}
			x2={770}
			y1={1484.5}
			y2={1484.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.198} stopColor="#4d4c4c" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={0.805} stopColor="#4c4c4c" />
		</linearGradient>
		<path
			fill="url(#ch-topdown66002a)"
			d="M917 179h68l1 148 4 15v153l-2 20 1 168 126 1 15-60 8 68-152-1-216 1v-70l18 65 127-1V342l-1-16 3-147z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown66002b"
			x1={1142}
			x2={762}
			y1={699}
			y2={699}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4c4c" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#4c4c4c" />
		</linearGradient>
		<path
			fill="url(#ch-topdown66002b)"
			d="m913 1127-148-2v-49s58 40.9 73-30c-19.3-72.1-76-27-76-27l1-66s65.5 43.2 73-28c-11-70.2-72-32-72-32s1-46.3 1-60c56.5 26.6 72.3-15.5 73-35 .3-9.6-18.8-61.2-70.7-30.4 1.3-36.9 2.7-75.6 2.7-75.6l149-1h64l150 1s1.4 38.8 2.7 75.6c-51.8-30.8-71 20.8-70.7 30.4.7 19.5 16.6 61.6 73 35 0 13.7 1 60 1 60s-72.9-32.3-72 32c7.5 71.2 73 28 73 28l1 66s-56.7-45.1-76 27c15 70.9 73 30 73 30v49l-148 2v299h152l-15 92s-5 125.6-34 190c-4-71.5-14.3-245-75-245-70.2 0-75 238.9-75 270 1.6 8.5 7 18 7 18h-38l1-624z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown66002c"
			cx={996.056}
			cy={202.458}
			r={260.268}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.65} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown66002c)"
			d="M822 1746c3.8 13.2 56.9 131.8 130 130 .6-28.4-1-124-1-124l-3-4s-98.6-1.5-126-2z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown66002d"
			cx={913.118}
			cy={204.579}
			r={240.652}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.65} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown66002d)"
			d="M1072.3 1768.3c-19 38.1-63.6 109.1-120.2 107.7-.6-28.4 1-104 1-104l3-4s9.7-.1 24.6-.1c1.9 0 6.3 22.1 8.3 22.1 13.1 0 40.8-.8 56 0 2.1-6.5 8.4-16.7 7.4-21.9 6.7.1 13.4.1 19.9.2z"
			opacity={0.302}
		/>
		<path fill="red" d="M986 1780h63l-4 10-55 1-4-11z" />
		<linearGradient
			id="ch-topdown66002e"
			x1={1049}
			x2={984}
			y1={145.5}
			y2={145.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.497} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#999" />
		</linearGradient>
		<path fill="url(#ch-topdown66002e)" d="M984 1769h65l-4 11h-57l-4-11z" />
		<path
			d="m997 36 2 142-11-1 1 320v191l10-3-1 442h-9v298l9 2 1 42s-28.3 16.9-42 103-14 163-14 163l13 29-50-1 1-16 6-1-2-618-5-1V687l8-1V179h-8V35s20.3-4 46-4 45 5 45 5zm-71 737v283c0 13.1 6.4 29 25 29s26-24 26-24V779c0-22.2-12.2-31-24-31-30.1 0-27 25-27 25z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-topdown66002f"
			cx={959.484}
			cy={1743.413}
			r={222}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.003} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown66002f)"
			d="M1110 179c-3.4-13.9-17.4-49.8-22-52-2.2 12.1-14.5 39-39 39s-46-20.4-46-43 18.9-42 40-42 29.8 7.1 32 10c-9.2-14.8-31.1-60-122-60S842.3 74.4 836 84c18.1-5.8 65-7.9 65 43 0 29-34 39-44 39s-36.2-9.3-42-37c-8.7 15.8-18 34.7-18 50h313z"
			opacity={0.4}
		/>
		<path fill="#f2f2f2" d="M1007 1463h19v305h-19v-305z" />
		<linearGradient
			id="ch-topdown66002g"
			x1={1085}
			x2={945}
			y1={168}
			y2={168}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown66002g)" d="m1073 1764-118 1-10-26 140 1-12 24z" opacity={0.302} />
		<linearGradient
			id="ch-topdown66002h"
			x1={1090}
			x2={943}
			y1={317.4088}
			y2={317.4088}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown66002h)"
			d="m1090 1742-147-5s4-70.8 10-137c7.8-87 39-126 39-126s34.6-26.8 55 4c25.9 39.2 39 138 39 138l4 126z"
			opacity={0.302}
		/>
		<path
			fill="#e6e6e6"
			d="m742 1127 168 1 3 65H722l20-66zm249-1 169 1 22 63s-29.6 170.2-33.3 191.7c-4.5 26.2-7.7 44.3-7.7 44.3l-151-2 1-298z"
		/>
		<path
			fill="#d9d9d9"
			d="m1154 516-40 171H989l-1-172-1-337 136 3 32 146 3 16-3 154-1 19zM915 178l-133 3s-28.1 120.9-39 162c-1.4-.8 7 152 7 152v22l38 169h126s.3-472.8 1-508z"
		/>
		<path
			d="m990 686 1-506.8 132 2 35 160.9-3 176.9-40 167H990zm29-30 73 3 32-141.9-36-49 1-89 38-38-28-129.9-78-1-2 445.8zm-230 30-40-167-3-177 35-161 132-2 1 507H789zm94-476-78 1-28 130 38 38 1 89-36 49 32 142 73-3-2-446zm-161 981 189 1 1 554-96-1-94-554zm118 526 43-1-1-492-126-2 84 495zm303-292H989v-233h190l-36 233zm2-204-125 1v173l97-1 28-173z"
			className="factionColorSecondary"
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M767 757V597l-18-81-3-174 3-16 32-146s5.8.1 15 0c2-19.6 13.4-45.2 19-56m13.9-33.5C864.3 28.9 933.6 31 951.6 31c18.2 0 88.7-3.2 123.7 61m13.5 32.1c5.6 10.8 17 36.4 19 56 8.9.2 15 0 15 0l32 146M1135.9 597l1 160m0 73c.3 30.8 2 59 2 59m0 69c.3 34.5 0 51 0 51M766 1076c-.3 23 0 51 0 51h-23l-21 65 56.4 329s4.5 21.2 30.6 187c5.8 26.5 7 36 7 36l5 2s48.4 131 130.6 131c71.8 0 132.2-134 132.2-134l5-2s.2-6.5 6-33c28.5-68.6 30.6-187 30.6-187s7-41 16.2-94.1c16.7-97.2 40.3-234.9 40.3-234.9l-21-65h-23s.3-28 0-51M765 1008s-.3-17.5 0-52m0-62s1.7-35.2 2-66"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1117 1395.9c11-63.8 23.3-135.9 30.1-175" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M952 1876v-121.5m0-152.7v-517.9m0-337.6V33" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M906 33v145.7m0 508.1v439.9m0 620.1v15.2h52.4" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M998 33v145.7m0 508.1V1126m0 299.2v41.6" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M951.5 748c14.5 0 26.2 11.7 26.2 26.2v283.6c0 14.5-11.7 26.2-26.2 26.2s-26.2-11.7-26.2-26.2V774.2c0-14.5 11.7-26.2 26.2-26.2z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M853.2 925H903m95 0h52.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M923 795h56" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M923 855h56" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M922 915h57" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M925 975h52" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M924 1035h52" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m943 1737 144 2" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1017.5 1680c9.7 0 17.5 7.8 17.5 17.5s-7.8 17.5-17.5 17.5-17.5-7.8-17.5-17.5 7.8-17.5 17.5-17.5z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M980 1767h73" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1091 1727c0-39.2-3.6-265-74-265s-74 254.8-74 274c9.8 19.3 16 31 16 31h21l9 22h56l8-22h16"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M857.5 80C881 80 900 99 900 122.5S881 165 857.5 165 815 146 815 122.5 834 80 857.5 80zm188 0c23.5 0 42.5 19 42.5 42.5s-19 42.5-42.5 42.5-42.5-19-42.5-42.5 19-42.5 42.5-42.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M802.5 742c27.9 0 50.5 22.6 50.5 50.5S830.4 843 802.5 843 752 820.4 752 792.5s22.6-50.5 50.5-50.5zm-.5 131.5c27.9 0 50.5 22.6 50.5 50.5s-22.6 50.5-50.5 50.5-50.5-22.6-50.5-50.5 22.6-50.5 50.5-50.5zm.5 118.5c27.9 0 50.5 22.6 50.5 50.5s-22.6 50.5-50.5 50.5-50.5-22.6-50.5-50.5 22.6-50.5 50.5-50.5zm298.3-250c28 0 50.7 22.6 50.7 50.5s-22.7 50.5-50.7 50.5c-28 0-50.7-22.6-50.7-50.5s22.6-50.5 50.7-50.5zm.5 131.5c28 0 50.7 22.6 50.7 50.5s-22.7 50.5-50.7 50.5c-28 0-50.7-22.6-50.7-50.5s22.6-50.5 50.7-50.5zm-.5 118.5c28 0 50.7 22.6 50.7 50.5s-22.7 50.5-50.7 50.5c-28 0-50.7-22.6-50.7-50.5s22.6-50.5 50.7-50.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M857 96.9c13.8 0 25.1 11.2 25.1 25.1s-11.2 25.1-25.1 25.1-25.1-11.2-25.1-25.1 11.3-25.1 25.1-25.1zm188.5 1.1c13.5 0 24.5 11 24.5 24.5s-11 24.5-24.5 24.5-24.5-11-24.5-24.5 11-24.5 24.5-24.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M802.5 1012.4c16.6 0 30.1 13.5 30.1 30.1s-13.5 30.1-30.1 30.1-30.1-13.5-30.1-30.1 13.5-30.1 30.1-30.1zm-.5-118.5c16.6 0 30.1 13.5 30.1 30.1s-13.5 30.1-30.1 30.1-30.1-13.5-30.1-30.1 13.5-30.1 30.1-30.1zm.5-131.5c16.6 0 30.1 13.5 30.1 30.1s-13.5 30.1-30.1 30.1-30.1-13.5-30.1-30.1 13.5-30.1 30.1-30.1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1101.3 1011.8c16.7 0 30.2 13.5 30.2 30.1s-13.5 30.1-30.2 30.1-30.2-13.5-30.2-30.1c-.1-16.6 13.5-30.1 30.2-30.1zm.5-118.4c16.7 0 30.2 13.5 30.2 30.1s-13.5 30.1-30.2 30.1-30.2-13.5-30.2-30.1c-.1-16.6 13.5-30.1 30.2-30.1zm-.5-131.4c16.7 0 30.2 13.5 30.2 30.1s-13.5 30.1-30.2 30.1-30.2-13.5-30.2-30.1c-.1-16.6 13.5-30.1 30.2-30.1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M723 1192s88.4-.2 187.7-.4m80.4-.2c100.5-.2 190.9-.4 190.9-.4"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1014.1 1220.4c100.5-.2 167.9-.4 167.9-.4" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1140 1126-151 1v298h153" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1014 1395h131" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1019 1216v184" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M910 1159h77" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M767 1127h144l1 619h-92" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M756 1222h126v494h-43l-83-494z" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M770 1302h110" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M782.9 1382H880" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M798.1 1472H880" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M812.9 1562H880" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M826.4 1642H880" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M920 1815.4c5 0 9 3.8 9 8.6s-4 8.6-9 8.6-9.1-3.8-9.1-8.6 4.1-8.6 9.1-8.6zm62 0c5 0 9 3.8 9 8.6s-4 8.6-9 8.6-9-3.8-9-8.6 4-8.6 9-8.6z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m818 688-39-172-3-175 35-161" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m777 340 38 39v90l-36 49" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M823 621h49v16h-49v-16z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M914 686s-91.1.7-126.3.9C784.2 670.4 766 592 766 592"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M782 657h131" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M914 175v516" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M884 175v516" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M819 230h49v16h-49v-16z" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M776 210h137" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M797 180h118" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M767 757V597l-18-81-3-174 3-16 32-146s5.8.1 15 0c2-19.6 13.4-45.2 19-56"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1086 688 39-171.9 3-174.9-35-160.9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1127 340.2-38 39v90l36 49" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1032 621h49v16h-49v-16z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M990 686s91.1.7 126.3.9c3.5-16.5 21.7-94.9 21.7-94.9"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1122 657H991" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M990 175.3V691" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1020 175.3V691" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1036 230.2h49v16h-49v-16z" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1128 210.2H991" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1107 180.3H989" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1137 757c-.1-6.5 0-159.9 0-159.9l18-81 3-173.9-3-16-32-145.9s-5.8.1-15 0c-2-19.5-13.4-45.2-19-56"
			/>
		</g>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M958.8 1689c101.9 0 171.5-192.6 171.5-457.6 19.6-14.9 647.2-251.7 647.2-251.7s18 100 67 100c48.9 0 70.5-74 70.5-190.6s-21.7-161.3-47-174.1c-1.3-18.8-3.5-43.5-25.8-43.5s-25.4 25.9-27 45.9c-15.9 13.5-37.6 35.6-37.6 82.3-68.9 3.8-514.5 31.8-514.5 31.8l-124.5-24.7s-90.5-205-90.5-428.2c68.3-30.9 153.4-51.3 180.9-64.7 27.5-13.4 44.6-43.7 44.6-78.8-29.9 0-121 .2-224.8.3-30.3.1-59.9 26.7-93 42.5-32.7-17.8-57-42.2-84.5-42.2-123.3.3-226.2.5-226.2.5s-7.6 56.1 42.3 76.5 157.6 43 189.1 67c0 64-45.8 321.8-96.3 428.2-24.7 9-122.2 21.2-122.2 21.2l-515.7-29.4s-9.4-69.3-39.9-84.7c-4.2-25.9-1.7-42.3-27-42.3S52 705.6 52 715c-14 7.6-47 26.6-47 180s41.7 182.3 71.7 182.3c55.7-5.3 67-69.7 67-100 28 12.7 639 248.2 639 248.2s9.6 463.5 176.1 463.5z"
		/>
		<radialGradient
			id="ch-topdown81001a"
			cx={2185.323}
			cy={1549.929}
			r={2219.158}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" />
			<stop offset={0.526} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001a)"
			d="M1088 1013.8c-14.1-5.6-98-5.9-119.8-5.9 0-66.7-4.7-607-4.7-607l84.6-21.2s-4.8 211.2 91.6 427c-40.8 18.5-51.7 81.3-51.7 207.1z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-topdown81001b"
			cx={-267.203}
			cy={1551.715}
			r={2219.158}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" />
			<stop offset={0.526} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001b)"
			d="M836.7 1013.8c14.1-5.6 98-5.9 119.8-5.9 0-66.7 4.7-607 4.7-607l-84.6-21.2s-15.9 221.4-91.6 427c40.8 18.5 51.7 81.3 51.7 207.1z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-topdown81001c"
			cx={103.374}
			cy={688.571}
			r={2067.1389}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.48} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001c)"
			d="M1056.3 1450.2c23.5 4.9 44.1 9.3 52.9 16.5 8.5-23.2 25.8-195.3 21.1-232.9-13.1-16.2-37.7-62.1-43.5-220-42-7.8-59.8-6.8-65.8-5.9-.5 60.9 7 250.6 7 250.6s24.4 45.8 28.3 191.7z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown81001d"
			cx={1824.441}
			cy={650.749}
			r={2085.667}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.48} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001d)"
			d="M881.3 1451.4c-23.6 4.9-60.8 12.8-69.6 20-8.6-23.3-31.8-199.1-27.1-236.7 13.2-16.2 37.8-62.2 43.6-220.3 42.1-7.8 60-6.8 66-5.9.5 61-7.1 250.9-7.1 250.9s-1.9 45.8-5.8 192z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown81001e"
			x1={315.2903}
			x2={412.3742}
			y1={626.2597}
			y2={879.1717}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.555} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown81001e)"
			d="M666.3 1182 143.6 977.3s3.2-31.5 3.5-48.2c26.2 12.9 531 205.9 531 205.9l-11.8 47z"
		/>
		<linearGradient
			id="ch-topdown81001f"
			x1={1604.3003}
			x2={1512.2473}
			y1={630.4933}
			y2={883.4064}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.555} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown81001f)"
			d="m1259.5 1182 522.7-204.7s-3.2-31.5-3.5-48.2c-26.2 12.9-530.9 205.9-530.9 205.9l11.7 47z"
		/>
		<linearGradient
			id="ch-topdown81001g"
			x1={413.4985}
			x2={408.8885}
			y1={1151.0269}
			y2={1081.6228}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.501} stopColor="#4b4b4b" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown81001g)"
			d="m671 870.3-522.7-31.8s-1.5-20.9-1.2-37.6c47.2 1 508.6 30.6 508.6 30.6l15.3 38.8z"
		/>
		<linearGradient
			id="ch-topdown81001h"
			x1={1509.7869}
			x2={1513.4369}
			y1={1150.3739}
			y2={1080.9698}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.501} stopColor="#4b4b4b" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown81001h)"
			d="m1251.3 870.3 522.7-31.8s1.5-20.9 1.2-37.6c-47.2 1-508.6 30.6-508.6 30.6l-15.3 38.8z"
		/>
		<path
			fill="#999"
			d="M101.3 715s-4.1-42.3-24.7-42.3c-20.5 0-23.5 25.4-23.5 43.5 13.2-5.5 48.2-1.2 48.2-1.2zm1766.7 0s-4.1-42.3-24.7-42.3c-20.5 0-23.5 25.4-23.5 43.5 13.2-5.5 48.2-1.2 48.2-1.2z"
		/>
		<path
			fill="#ccc"
			d="M1199.6 235.1h47s2.3 89.8-69.3 97.6c5.1-28.6 22.3-97.6 22.3-97.6zm-476.9 0h-47s-2.3 89.8 69.3 97.6c-5.1-28.6-22.3-97.6-22.3-97.6z"
		/>
		<path
			d="M679 1108c2.2-27.8 2.9-149.8-3-189-59.5-5.2-527-48-527-48v42s516.2 189.9 530 195zm571-189 522-48 1 45-524 193s-3.3-48.7-3-97c.3-46.7 4-93 4-93z"
			className="factionColorPrimary"
		/>
		<path
			d="m1814 1017 1-297s34.8-16.5 61 3c-.1 27.4 0 298 0 298s-17.4 6-30 6-32-10-32-10zm-119-75c-2.4 8.1 0 58 11 66-18.9 6.1-447 171-447 171s-9.7-63.2-8-71c12.6-5.6 433.1-161.1 444-166zm-446-23c1-17.2 11.3-79.9 17-88 14.9-1.9 437-26 437-26l-7 73s-426.8 39.9-447 41zm-583 263-444-173s5.2-15.8 8-33c2.7-16.8 3-35 3-35l445 168s-.9 20.9-4 40c-2.8 17.3-8 33-8 33zm10-262c-2.1-17.5-8.3-76.9-24-91-32.3-3.6-428-25-428-25s4.7 75.7 6 76 436.6 39.2 446 40zm-630 97c6 3.7 36.9 21.7 61-2-.1-37.6-1-296-1-296s-13.9-6-29-6-32 9-32 9 .9 285.9 1 295zm953 492c-4.1-15.8-2.7-81.8 0-112s10.7-138.4 29-140c11.3 1.3 17.9 87.4 19 99s13.6 135.9 6 151c-6 .9-43 2.1-54 2z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-topdown81001i"
			cx={640.161}
			cy={1030.322}
			r={1278.274}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001i)"
			d="M76.6 711.5c16.8 0 70.5-6 70.5 191.7 0 186.1-65.8 172.9-71.6 172.9-5.9 0-70.5 2.4-70.5-189.4C5 695 59.8 711.5 76.6 711.5z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown81001j"
			cx={-486.323}
			cy={1036.019}
			r={1278.274}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001j)"
			d="M76.6 711.5c16.8 0 70.5-6 70.5 191.7 0 186.1-65.8 172.9-71.6 172.9-5.9 0-70.5 2.4-70.5-189.4C5 695 59.8 711.5 76.6 711.5z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown81001k"
			cx={2406.873}
			cy={1030.224}
			r={1276.344}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001k)"
			d="M1843.4 711.8c16.8 0 70.5-6 70.5 191.4 0 185.8-65.8 172.7-71.7 172.7s-70.5 2.4-70.5-189.1 54.9-175 71.7-175z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown81001l"
			cx={1280.3879}
			cy={1035.912}
			r={1276.344}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001l)"
			d="M1843.4 711.8c16.8 0 70.5-6 70.5 191.4 0 185.8-65.8 172.7-71.7 172.7s-70.5 2.4-70.5-189.1 54.9-175 71.7-175z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown81001m"
			x1={959.9905}
			x2={1045.0895}
			y1={1680.6525}
			y2={1682.1375}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={0.503} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#4c4c4c" />
		</linearGradient>
		<path
			fill="url(#ch-topdown81001m)"
			d="m960 279.8 20-81.8h48.2s15.8 31.3 17 33.6c-4.2 2.8-5.5 2.7-14.6 4.6-23.4 14-61.1 40.3-66.9 43.9-.9.5-3.7-.3-3.7-.3z"
		/>
		<linearGradient
			id="ch-topdown81001n"
			x1={876.9442}
			x2={962.2941}
			y1={1679.7084}
			y2={1681.1984}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={0.503} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#4c4c4c" />
		</linearGradient>
		<path
			fill="url(#ch-topdown81001n)"
			d="m962.3 280.3-20-82.2H894s-16.3 32.6-17 33.8c4.2 2.9 5.6 2.7 14.7 4.6 23.5 14.1 61.3 40.5 67.1 44.2.7.5 3.5-.4 3.5-.4z"
		/>
		<radialGradient
			id="ch-topdown81001o"
			cx={1638.463}
			cy={496.287}
			r={1497.939}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.395} stopColor="gray" />
			<stop offset={0.427} stopColor="#656565" />
			<stop offset={0.49} stopColor="#7f7f7f" />
			<stop offset={0.5} stopColor="#e6e6e6" />
			<stop offset={0.51} stopColor="#7f7f7f" />
			<stop offset={0.539} stopColor="#666" />
			<stop offset={0.849} stopColor="#666" />
			<stop offset={1} stopColor="#7f7f7f" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81001o)"
			d="M808.5 1464.3c111.2-20.9 186.8-12.9 186.8-12.9s-3.5 87.1 32.9 87.1 27-80.6 27-88.2c15.8 0 52.9 16.5 52.9 16.5s-23.8 223.5-149.2 223.5c-103.4-.1-138.2-159.5-150.4-226z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M958.8 1689c101.9 0 171.5-192.6 171.5-457.6 19.6-14.9 651.9-253 651.9-253s13.3 101.2 62.3 101.2c48.9 0 70.5-74 70.5-190.6s-21.7-161.3-47-174.1c-1.3-18.8-3.5-43.5-25.8-43.5s-25.4 25.9-27 45.9c-15.9 13.5-37.6 35.6-37.6 82.3-68.9 3.8-514.5 31.8-514.5 31.8l-124.5-24.7s-90.5-205-90.5-428.2c68.3-30.9 153.4-51.3 180.9-64.7 27.5-13.4 44.6-43.7 44.6-78.8-29.9 0-136.2.2-240.1.3-21.9 12.6-43.8 28.3-74.2 47.2-30.9-20.7-44.5-27.7-70.4-46.9-123.3.3-243.8.5-243.8.5s-7.6 56.1 42.3 76.5 157.6 43 189.1 67c0 64-45.8 321.8-96.3 428.2C755.5 816.8 658 829 658 829l-515.7-29.4s-9.4-69.3-39.9-84.7c-4.2-25.9-1.7-42.3-27-42.3S52 705.6 52 715c-14 7.6-47 26.6-47 180s41.7 182.3 71.7 182.3c51.9 0 63.5-63.1 67-98.8 28 12.7 639 248.2 639 248.2s9.6 462.3 176.1 462.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m960 279.8 20-81.8h48.2s15.8 31.3 17 33.6c-4.2 2.8-5.5 2.7-14.6 4.6-23.4 14-61.1 40.3-66.9 43.9-.9.5-3.7-.3-3.7-.3zm2.3.5-20-82.2H894s-16.3 32.6-17 33.8c4.2 2.9 5.6 2.7 14.7 4.6 23.5 14.1 61.3 40.5 67.1 44.2.7.5 3.5-.4 3.5-.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1051.6 377.4-91.6 27-90.5-29.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M961 403V282" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M890 236v146" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1034 236v146" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1246.6 236.2s-5.7 68.6-34.1 83.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1199.6 236.2s-15.3 61.2-23.5 95.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M726.1 236.5s15.5 60.9 23.7 94.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M673.4 236.2s5.7 68.6 34.1 83.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M782.6 1226.7c48.7 0 81.8-418.8-1.2-418.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M834.3 1013.8c8-2.2 25.6-3.8 47.9-4.8m27.2-.9c34.4-.8 74.3-.8 108.1-.1m27.6.8c19.1.7 34.1 1.6 41.8 2.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M654.2 828.4c33.5 41.2 33.8 295.4 13.3 348.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1133.7 1232.6c-48.7 0-74.8-424.6 8.2-424.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1269.1 828.4c-29.6 36.4-28.1 262-11.4 349.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M142.4 797.3c6.4 35.7 12.6 114.9-5.9 224.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1777.8 797.3c-4.8 42.1-13.2 76.1 4.5 185.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M42.6 720.9c10.9-7.6 49.2-12.4 65.8-2.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1809.3 723.2c12.2-9.2 41.3-18.7 68.1-1.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M809.6 1471.2c36.6-19.2 98.3-24.5 149.5-24.5 21.9 0 33.5 2.5 37.2 2.3 0 1.4-6.9 90.6 30.5 90.6 34.1 0 27-78.4 27-87 21.5 0 58.7 14.1 58.7 14.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M824.9 1526.7c20.6-6.7 54.2-29.3 171.5-24.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1050.4 1514.9c11.6 4.4 38.6 17.7 42.3 21.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M877.8 1644.3c23.1-16.9 130.4-17.6 156.2 4.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M846.1 1585.5c41.8-28.8 191.2-33.1 220.8 7.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M996.4 1506.7h58.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1054 1454.9c0-25.6-7.7-197.6-28.2-197.6s-29.4 155.5-29.4 200"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M839 614c19.5-6.4 70.4-15 122-15s103.9 5.2 117 13" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m46 719-1 346m63 2-1-350" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1816 719-1 346m63 2-1-350" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M45 1019c6.2 3.3 27.4 16.4 62 0" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1813 1019c6.2 3.3 27.4 16.4 62 0" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m148 871 528 49m1 189L148 913" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1775.3 871-527.5 49m-1 189.3L1775.3 913" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M224 804c.7 4.5 4.1 36.5 6 74.8m1.1 64.4c-.7 28.9-3.5 54.8-10.1 68.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1702.3 803.8c-.7 4.5-4.1 36.5-6.1 74.8m-1.1 64.4c.7 28.9 3.6 54.8 10.3 68.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M712 932c1.5 25.3 6 106.3-2 179 1.5 40.5 38.8 44.8 60 30 10.2-7.2 11.9-20.8 14-34 6.3-40.5 9.2-141.8 3-181s-34.7-39.9-47-37-29.5 17.7-28 43zm499 .1c-1.5 25.3-6 106.4 2 179.1-1.5 40.5-38.6 44.9-59.7 30-10.2-7.2-11.9-20.9-13.9-34-6.3-40.5-9.2-141.9-3-181.1 6.2-39.2 34.6-39.9 46.8-37 12.2 2.8 29.3 17.6 27.8 43z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M883 961h26v99h-26v-99zm135 0h26v99h-26v-99z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M790 1331c12.5-4.6 48-15.8 96-21 35.7-3.8 77.5-5.3 119-2.3m40.3 7.5c29.8 4.5 59.3 10.9 83.7 21.8"
		/>
	</svg>
);

export default Component;

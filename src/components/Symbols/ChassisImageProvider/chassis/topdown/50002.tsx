import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<filter id="ch-topdown50002k">
			<feGaussianBlur stdDeviation={30} />
		</filter>
		<path
			fill="#f2f2f2"
			d="M1048.9 23.3c16.4 9.8 45 139.2 61 213.2 17.9-.2 51 10.1 58.9 71.8 5.8 45.4-21.6 72.1-25.7 75 5.2 19.9 18.2 57.9 18.2 57.9s55.6 30.2 70.5 102.5c3.3.2 5.3-.2 8.2 1.4 9.6 19.8 26.5 79.3 29 101 .2 1.6-9.4 1.8-9.2 3.4 5.6 49.7 12.9 125.9 12.9 187 0 99.4-12.8 553.9-12.8 656.6-11.5 129.1-27.4 320-50.3 364.2-4.5-.4-17.1-12.9-17.1-12.9V1823c4.7-172.4 9.6-344.9 9.6-344.9s7.7-8.9 11.4-12.1c2.3-40.1-1.3-170.2-3-234.7-4.9-5.4-12-12.4-15.3-16.5-.4-14.2-2.1-17.7-2.1-17.7s-17.5-1.8-58 0c.1-14.5-2-43.3-5-70-17.3 122-35 297.3-70 391-11.4 37.2-31.7 56.2-53 66-.3 8.9-1.7 20.6-2 29-29.2 0-62.5 1.9-93 2-.2-8.7.5-20.3 0-33-6.3-2.6-18.6 3.8-48-44-35.5-83.5-58.3-252.1-72.5-382.8-9.5-87.3-7.4 25.1-7.5 40.8-38.7-1.7-45-1.6-58.4.4-.3 6.6-.5 12.3-.7 16.9-8.6 6.1-12.2 10.4-17.7 17.2-1.7 65.1-4 193.1-1.1 234.6 7 8.1 11.8 13 11.8 13s4.9 172.5 9.6 344.9v21.3s-12.7 12.6-17.1 13c-22.9-44.2-38.8-235.1-50.3-364.2 0-102.7-12.9-557.2-12.9-656.7 0-61.2 7.3-137.5 12.9-187.1.2-2.2-9.5-2.2-9.3-4.3 3.2-27.5 27-102 27-102s1.5 8 12-1c6.2-66.2 68.7-100.9 68.7-100.9s13-38 18.2-57.9c-4.1-2.9-31.5-29.6-25.7-75 7.8-61.7 41-72 58.9-71.8 16.1-74 45.9-201.6 61-213.2 7-5.2 164.5-7.9 177.9 0z"
		/>
		<radialGradient
			id="ch-topdown50002a"
			cx={844.258}
			cy={1356.496}
			r={213.18}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50002a)"
			d="M817 543c3.6-12.4 18.7-78.9-59-100-40.1 27.3-58.6 55.4-71 102 63.4-1.5 110-2 130-2z"
			opacity={0.702}
		/>
		<radialGradient
			id="ch-topdown50002b"
			cx={1077.8781}
			cy={1359.397}
			r={213.18}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50002b)"
			d="M1101.6 543c-3.6-12.4-18.8-78.9 59.2-100 40.2 27.3 58.8 55.4 71.2 102-63.6-1.5-110.3-2-130.4-2z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-topdown50002c"
			x1={853}
			x2={905}
			y1={1433.771}
			y2={1385.229}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-topdown50002c)" d="m904 544 1-67s-46.9 14.8-52 67c28.4-.5 51 0 51 0z" opacity={0.2} />
		<path fill="#ccc" d="M886 485c-15.1 12.7-30.4 40.3-33 59-17.9-.8-38 0-38 0s6-23.5 6-28 57.9-32.4 65-31z" />
		<path
			fill="#ccc"
			d="M1034 485.1c15.1 12.6 30.4 40.3 33 58.9 17.9-.8 38 0 38 0s-6-23.5-6-28-57.9-32.3-65-30.9z"
		/>
		<linearGradient
			id="ch-topdown50002d"
			x1={1054.3849}
			x2={1002.3848}
			y1={1423.7573}
			y2={1376.4913}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-topdown50002d)" d="m1017 541-1-67s46.9 14.8 52 67c-28.4-.5-51 0-51 0z" opacity={0.2} />
		<linearGradient
			id="ch-topdown50002e"
			x1={960}
			x2={960}
			y1={897}
			y2={1538}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002e)"
			d="m929 1023-23-18-4-603 31-20-4 641zm62 0 23-18 4-603-31-20 4 641z"
			opacity={0.149}
		/>
		<radialGradient
			id="ch-topdown50002f"
			cx={2001.797}
			cy={1465.239}
			r={1225}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.932} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50002f)"
			d="M820 688c.1 22.3 26.8 181 86 245 .2-47.5-3-243-3-243l-67 2s-7.1-1.8-16-4z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown50002g"
			cx={-38.798}
			cy={1570.038}
			r={1225}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.932} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50002g)"
			d="M1100 688c-.1 22.3-26.8 181-86 245-.2-47.5 3-243 3-243l67 2s7.1-1.8 16-4z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown50002h"
			x1={758.2977}
			x2={792.1777}
			y1={1544.7135}
			y2={1691.4625}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002h)"
			d="M809 235.4c-18.5.8-48.3 15.8-55.7 57.9-7.4 42.1 8.1 76.9 25.7 88.9 5.1-4.9 28.1-119.8 30-146.8z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown50002i"
			x1={1161.3}
			x2={1131.2791}
			y1={1541.5288}
			y2={1690.4207}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002i)"
			d="M1111 235.4c25.9.4 50.2 23.2 56.8 65.3 6.6 42.1-7.5 66-26.8 83.6-3.8-23.2-23-103.9-30-148.9z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown50002j"
			x1={959.465}
			x2={959.465}
			y1={1903.8219}
			y2={1402.85}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.701} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002j)"
			d="M871.1 23.3c28.6-6.3 166.8-11.3 178.8-2.1 22.3 43.7 50.2 396.1 73.9 438.1-6.4 8.6-26.7 25.5-25.7 52.5-26.8-13.9-150.1-88.6-274.1 5.4-6.2-29.6-13.8-47.4-28.9-56.8 28.5-75.6 47.4-430.7 76-437.1z"
			opacity={0.102}
		/>
		<path
			fill="#ccc"
			d="M793 459.3c15.1-32.4 50.5-312.6 54.6-358.8-17.5 28.6-66.8 282.2-87.8 340.6 11.8 3.6 22.6 11.8 33.2 18.2zm331.9 0c-15.1-32.4-48.3-312.6-52.5-358.8 17.5 28.6 64.7 282.2 85.7 340.6-11.8 3.6-22.6 11.8-33.2 18.2z"
		/>
		<path
			fill="#dedede"
			d="M706.2 1226.3v237.8s4.6 6 11.8 11.9c.5 24.8 9 368 9 368s-7.8 8.1-16 12c-27.6-35.6-52.9-389.8-52.9-389.8l-3.2-237.8s-21.4-390.8 3.2-579.4c12.1 4.4 16 5 16 5l-2 53 16 41 7 2s8 340.6 10.5 445.7c.1 3.8 19.4 2.1 19.5 5.3.2 12.8-18.9 25.3-18.9 25.3z"
		/>
		<path
			fill="#dedede"
			d="M818 709s-15.9 38.5-16 39-5 3-5 3l-9 322h86l-3-193s-40.2-90-49-172c-1.5.6-4 1-4 1zm65 594 6 268s-57.7-48-80-267c16.2-.5 74-1 74-1zm214-614s15.9 58.5 16 59c.1.5 5 3 5 3l9 322h-86l3-193s14.9-26 27-68c10.4-36.3 17.9-86 22-124 1.5.6 4 1 4 1zm-65 614-6 268s57.7-48 80-267c-16.2-.5-74-1-74-1z"
		/>
		<path
			d="m787 1071 91 2 5 229-72 1-24-165s-1.9-55.5 0-67zm246 231 6-228 91 1 2 55-26 173-73-1zM930 950c3.9 4.5 43.5 17.6 60-2-.3-36.1-4-486-4-486s-40.6-7.9-55 4c.3 20.4-2.2 448.1-1 484z"
			className="factionColorPrimary"
		/>
		<path
			fill="#e6e6e6"
			d="m1211 1227 1.8 237.1s-4.7 6-11.8 11.9c-.5 24.8-9 368-9 368s7.8 8.1 16 12c27.6-35.6 53.1-389.8 53.1-389.8l3.2-237.8s21.4-390.8-3.2-579.4c-12.2 4.4-16 5-16 5l2 53-16 41-7 2s-9.6 341-12.1 446c-13.6-1.2-19.8 5.6-20 11-.2 4.3 19 20 19 20z"
		/>
		<path
			d="m673 653-1 54 14 41 9 1 12 446 78 2 13-448 20-40-1-24-144-32zm573 0 1 54-14 41-9 1-12 446-78 2-13-448-20-40 1-24 144-32z"
			filter="url(#ch-topdown50002k)"
		/>
		<path
			fill="#f2f2f2"
			d="m673 653-1 54 14 41 9 1 12 446 78 2 13-448 20-40-1-24-144-32zm573 0 1 54-14 41-9 1-12 446-78 2-13-448-20-40 1-24 144-32z"
		/>
		<path
			fill="#e6e6e6"
			d="M725 752h46l-12.6 32.8s-22.4 2.9-21.6 2.1c.7-.8-11.8-34.9-11.8-34.9zm424 0h46l-12.6 32.8s-22.4 2.9-21.6 2.1c.7-.8-11.8-34.9-11.8-34.9z"
		/>
		<linearGradient
			id="ch-topdown50002l"
			x1={819}
			x2={672.3245}
			y1={1239.0831}
			y2={1239.0831}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002l)"
			d="m819 687-145-34s-3.8 34.1 0 54c20.5 1.9 130.1 3 144 0 .6-12.4 1-20 1-20z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown50002m"
			x1={820}
			x2={671}
			y1={1193.0831}
			y2={1193.0831}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.501} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002m)"
			d="m820 705-149 1s7.8 23.1 16 41c20.5 1.9 100.1 3 114 0 4-10.8 19-42 19-42z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown50002n"
			x1={1250}
			x2={1101}
			y1={1193.0831}
			y2={1193.0831}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.501} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002n)"
			d="m1250 705-149 1s7.8 23.1 16 41c20.5 1.9 100.1 3 114 0 4-10.8 19-42 19-42z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown50002o"
			x1={1245.9856}
			x2={1099}
			y1={1238.9894}
			y2={1238.9894}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002o)"
			d="m1099 687.1 145.3-34.1s3.8 34.3 0 54.2c-20.6 1.9-130.4 3-144.3 0-.6-12.5-1-20.1-1-20.1z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown50002p"
			x1={959.8287}
			x2={959.8287}
			y1={961.5254}
			y2={1460.566}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50002p)"
			d="M930 950c3.9 4.5 43.5 17.6 60-2-.3-36.1-4-486-4-486s-40.6-7.9-55 4c.3 20.4-2.2 448.1-1 484z"
			opacity={0.2}
		/>
		<path fill="#f2f2f2" d="M705.2 1466.2s4 305 12.9 380.3c5.5-1 6.4-6.4 6.4-6.4l-5.3-361-14-12.9z" />
		<path fill="#f2f2f2" d="M1215.9 1466.2s-4 305-12.8 380.3c-5.5-1-6.4-6.4-6.4-6.4l5.3-361 13.9-12.9z" />
		<path
			d="m902 543 2 148-69-1-183-47 25-99 225-1zm113-1 226 1 29 103-184 43-71 3V542zm-221-82s-14.3-14.5-34-21c21.1-69.5 76.4-348.5 96-379 3.5-18.9-42 364.9-62 400zm331.1 0s14.3-14.5 33.9-21c-21-69.5-76.3-348.8-95.8-379.3-3.5-19 41.9 365.2 61.9 400.3z"
			className="factionColorSecondary"
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M725.7 1195.4c-.3 7-.5 13.1-.7 17.9-8.6 6.1-12.2 10.4-17.7 17.2-1.7 65.1-4 193.1-1.1 234.6 7 8.1 11.8 13 11.8 13s4.9 172.5 9.6 344.9v21.3s-12.7 12.6-17.1 13c-22.9-44.2-38.8-235.1-50.3-364.2 0-102.7-12.9-557.2-12.9-656.7 0-61.1 7.3-137.4 12.9-187.1m28.1-106.4c15.1-71.8 70.4-101.8 70.4-101.8s13-38 18.2-57.9c-4.1-2.9-31.5-29.6-25.7-75 7.8-61.7 41-72 58.9-71.8 16.1-74 45.9-201.6 61-213.2 6.9-5.2 164.4-7.9 177.7 0 16.4 9.8 45 139.2 61 213.2 17.9-.2 51 10.1 58.9 71.8 5.8 45.4-21.6 72.1-25.7 75 5.2 19.9 18.2 57.9 18.2 57.9s54.8 29.8 70.2 100.9m28.2 106.3c5.6 49.6 13 126.5 13 188 0 99.4-12.8 553.9-12.8 656.6-11.5 129.1-27.4 320-50.3 364.2-4.5-.4-17.1-12.9-17.1-12.9v-21.4c4.7-172.4 9.6-344.9 9.6-344.9s7.7-8.9 11.4-12.1c2.3-40.1-1.3-170.2-3-234.7-4.9-5.4-12-12.4-15.3-16.5-.2-8.2-.4-14.5-.5-18.4m-62.3-84.1c-14.5 134.4-39.7 333.9-79.3 426.9-1.7 4-12.6 32-46 44 0 11.5.1 23.6 0 31h-95c.2-10.1.1-31.5 0-31-4.2.3-30.1-7.2-50-54-39.6-93.1-62.6-288.2-75.6-419.7"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m892.5 20.1 11.8 55.7 111.6-.1 11.8-56" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M795.1 458.2c27.2-62 45.9-369 69.6-425.2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1124.9 458.2c-27.2-62.1-45.9-369.1-69.6-425.3" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m706.2 1463-47.1 2.1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m710.5 1224.1-55.7 5.3" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M707.8 1194.5c-.3 14.8-.5 26.7-.5 35" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1212.7 1463 47.1 2.1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1210.6 1224.1 53.5 5.3" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1210.1 1195.7c.3 14.2.4 25.7.4 33.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1117.4 255.8c10.5 4.1 40 36.6 20.3 106" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M806.9 248.3c-12.4 6.2-54.3 41-26.8 121" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M811.2 235.4c-3.4 19.9-26.1 120.1-35.3 154.3" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1109.9 233.3c2.8 14.9 28.1 139.4 35.3 159.6" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M757.6 442.2c14.6 1.3 70.8 30.4 63.2 78.2-1.2 7.2-2.7 14.7-4.6 22.4"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1162.1 442.2c-14.6 1.3-70.9 30.4-63.3 78 1.2 7.5 2.8 15.3 4.8 23.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1097.1 510.7c-10.6-5.8-31.5-17.4-59.2-26.7m-144.7-4.4c-23.8 7.5-48.2 19.8-72.4 38.6"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M870.6 878.3c7 241.7 19 689.9 19 689.9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M787.6 1073.1h88.9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M813 1303.4h67.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1130.2 1073.1h-88.9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1104.9 1303.4h-67.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1045 885c-6.9 242.7-18.7 684.9-18.7 684.9" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m675 653 160 38h65m-1-148H677l-27 103 23 6-1 56 15 40h8l12 448 77 1 13-449"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M835 546v143m-17 1v19l-16 39"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M817 706H672m128 42H690m35 2v8l11 28h20l12-27v-8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m903 402 3 604 23 17 3-642-29 21z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m912 1597-1-507 96-1-1 518"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m1244 653-160 38h-64m0-148h222l27 103-23 6 1 56-15 40h-8l-12 448-77 1-13-449"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1084 546v143m17 1v19l16 39"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1102 706h145m-128 42h110m-35 2v8l-11 28h-20l-12-27v-8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m1016 402-3 604-23 17-3-642 29 21z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M903 931c-58.3-71.5-79.7-212.5-82-238m31-151c4.4-13.5 19.1-51.8 49-67m31-10.8c16.7-3.5 35.3-4.2 54.8-.7m32.2 9.5c44.6 18.5 48 70 48 70M930 950c15.2 11.3 38.7 14.9 56 0m30-22c21.3-23.6 71-102.8 74-236"
			/>
		</g>
		<circle cx={960} cy={971.5} r={417.5} fill="none" stroke="#0f0" strokeWidth={5} />
	</svg>
);

export default Component;

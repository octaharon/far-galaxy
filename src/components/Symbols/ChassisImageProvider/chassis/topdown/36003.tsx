import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M746 1706h399c58.9 0 85.4-24.5 122-59s67.1-46.1 100-68 88.3-60.1 100-72c21.8-.6 112 0 112 0l-1-478-4-945-238-1s-120.5-39.4-153-50H721c-21.8 0-108.9 31.1-163 50-58 .4-239 0-239 0l2 1425h109s48.2 42.1 94 69 79.6 48.9 98 65 59.4 64 124 64z"
		/>
		<path
			fill="#ccc"
			d="M428 1507c-51.5-59.7-72.3-99.1-82-164-3.8-84.9-11-340.5-11-391s-3.9-480.7-2-531 6-247.3 73-287 130.1-43.5 145-50c-50-.5-219.9-1.4-229 1-2.1 16.7-2 1409-2 1409s-1.7 13.8 11 14 76.9-.7 97-1zm1041 0c51.4-59.7 72.3-99.1 82-164 3.9-84.9 11-340.6 11-391.1s3.9-480.8 2-531.1c-1.9-50.3-5.9-247.4-73-287.1-67-39.7-130-43.5-145-50 50-.5 219.9-1.4 228.9 1 2.1 16.7 2 1409.4 2 1409.4s1.7 13.8-11 14c-12.6.1-76.9-.8-96.9-1.1z"
		/>
		<linearGradient
			id="ch-topdown36003a"
			x1={297.9166}
			x2={409.2206}
			y1={462.3459}
			y2={507.316}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown36003a)"
			d="M431 1507c-20.3-19.4-77.3-94.2-83-165-11.7-.8-24 2-24 2s-8.2 151.4-2 160 73 3.5 109 3z"
		/>
		<linearGradient
			id="ch-topdown36003b"
			x1={1597.3184}
			x2={1486.3264}
			y1={451.4586}
			y2={514.2556}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown36003b)"
			d="M1468 1507c20.3-19.4 77.1-94.1 82.8-164.9 11.7-.8 23.9 2 23.9 2s8.2 151.3 2 159.9c-6.2 8.6-72.9 3.5-108.7 3z"
		/>
		<linearGradient
			id="ch-topdown36003c"
			x1={1566.6229}
			x2={1452.3151}
			y1={1890.8265}
			y2={1756.9895}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown36003c)"
			d="m1540 216 34 1-2-131s-184.9-4.7-220-2c64.4 18.9 161.1 21.1 188 132z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown36003d"
			x1={302.9782}
			x2={444.0302}
			y1={1891.1958}
			y2={1757.2018}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown36003d)"
			d="m354 216-34 1 2-131.1s184.9-4.7 220-2C477.6 102.8 381 105 354 216z"
			opacity={0.4}
		/>
		<path
			fill="#e6e6e6"
			d="m578 1230 747 1s3-903.6 3-921c-53.1-39.9-219.1-39-369-39s-369.3-1.5-390 41c4 31.7 3 31.1 3 51s2 866 2 866-.9 299-35 357c-25.4-15.7-136.5-80.7-169-163s-30-166.7-30-210-6-423-6-423-3-310.9-2-357 1.6-241.6 74-301c55.4-31 84.6-32.4 150-49 37.8-9.6 86.1-27.7 163-51 17.9.2 465 2 465 2l156 51s108.8 15.5 152 50 61 116.6 67 196 4.7 245.1 4 289-3.8 651.1-14 724-59.5 174.2-182 234c-91.6 50.1-139.4 110-180 125-62.6 4.2-184.8 3-226 3-281.7 6.5-275.4.4-334-62-.4-181.1-49-414-49-414z"
		/>
		<path
			fill="#b3b3b3"
			d="M502 392v451s1 11-15 11-68-1-68-1-8 1.6-8-8 2-512 2-512l89 59zm905 0v451s-1 11 15 11 68-1 68-1 8 1.6 8-8-2-512-2-512l-89 59z"
		/>
		<path
			d="m1274 1639-315-258-333 262c39.3 48.8 85 59 85 59l247-201 240 197s67.7-41.3 76-59zm-204 65-111-92-114 95 225-3zM570 302c6.6 17.6.1-172-14-216-55.7 8.7-121.2 23.2-149 45 25.3 11.9 128.4 78.4 163 171zm771-217c-4.7 16.1-22.7 119.3-13 225 13.8-24.1 62.6-134.3 166-176-12.5-7.8-127.3-51.9-153-49z"
			className="factionColorSecondary"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M746 1706h399c58.9 0 85.4-24.5 122-59s67.1-46.1 100-68 88.3-60.1 100-72c18.2-.5 84.3 0 105.4 0 1.8 0 6.6-3.7 6.6-7 0-59-1-471-1-471s-4-854.6-4-938.4c0-3.6-4.6-6.6-6.4-6.6-37.9 0-231.6-1-231.6-1s-120.5-39.4-153-50H721c-21.8 0-108.9 31.1-163 50-50.6.3-194.9 0-230.8 0-5.9 0-8.2 5.7-8.2 9 0 114.7 2 1291.9 2 1407 0 4.4.2 9 8.7 9H430s48.2 42.1 94 69 79.6 48.9 98 65 59.4 64 124 64z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M323 1343h24m1201 0h31" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m628 1644 331-263 313 258" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M707.6 1700.7C805.6 1622.9 959 1501 959 1501s144.1 118.8 236.8 195.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M839.3 1706.1c65-51.6 119.7-95.1 119.7-95.1s52.9 43.6 115.3 95"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1460.7 1512.4c47.9-46.2 84.1-103.5 90.8-194 6.7-90.5 10-425.6 10-488.9 0-325.5 20.1-593-54.9-681.9-39.5-47-152.8-57.1-175.7-67"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M435 1513c-48-46.2-84.3-103.5-91-194s-10-425.6-10-489c0-325.5-20.2-593.1 55-682 39.5-47 153-57.1 176-67"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M539 1589c25.7-66.6 35-198.2 35-409s-2-752.3-2-843-144.5-210-166-204"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M318 216h37.3m1185.1 0h34.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1359.9 1589.2c-25.7-66.6-34.9-198.2-34.9-409s2-752.3 2-843c0-90.7 144.2-205.9 165.7-200"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1273 1643c0-195.9 43.8-357.4 52-433.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M626 1643c0-195.7-43.8-357-52-433" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M574 1230h750" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1133 1148.1h140v43.9h-140v-43.9zm-489 0h140v43.9H644v-43.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M412 334c0-.5.3-.8.8-.5 8.7 5.8 89.2 59.5 89.2 59.5v454.9c0 2.1-2.4 4.1-4 4.1h-82c-.9 0-4-2.6-4-4.1V334z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 822h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 792h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 762h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 732h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 702h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 672h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M418 642h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 612h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 582h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 552h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 522h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 492h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 462h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 432h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 402h80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M417 372h52" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1496 333.6c0-.6-.3-.8-.8-.5-8.7 5.8-89.2 59.5-89.2 59.5v455.2c0 2.1 2.4 4.1 4 4.1h82c.9 0 4-2.6 4-4.1V333.6z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 822h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 792h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 761.9h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 731.9h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 701.9h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 671.9h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 641.8h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 611.8h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 581.8h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 551.8h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 521.8h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 491.7h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 461.7h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 431.7h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 401.7h-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1491 371.6h-52" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M570 308c7.2-114.2-13-227-13-227" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1326.6 308c-7.4-114.2 13.4-227 13.4-227" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M568 311c19.8-20.8 26-41 390-41 114.8 0 358.3-2.5 373 46"
		/>
		<g id="Animation">
			<path
				fill="#f2f2f2"
				d="M1138.3 373.6c52.3 49.9 106.6 154.6 143 265.1 36.4 110.4 31.6 215.2 1 339.1-30.6 123.9-150.2 155.7-226 171-.6 33.4 0 66 0 66l-29 56h-7L1001 1905l-86-1-19.3-633.2h-7l-25-56s.6-32.6 0-66c-75.8-15.3-195.4-47.1-226-171-30.6-123.9-35.4-228.6 1-339.1 36.4-110.4 90.7-215.2 143-265.1C829.5 328 934.5 332 960 332s130.5-4 178.3 41.6z"
			/>
			<path
				fill="#e6e6e6"
				d="M754 407c-10.1 41.4-11 113-11 174 0 419.3 135.8 459 213 459 238 0 200.8-356.2 212-472-1.8-65.6-4-164-4-164s58 66.1 113 220 21.5 320.3-13 400-165.6 120.5-210 126c.8-22.6 1.2-27.9 1-38-59.5-29.8-128.9-29.3-190 0-.4 18.8 0 20.5 0 37-25.8-4.7-172.7-25.9-218-147s-33.5-233.5-31-261c3.4-37.7 39.8-208.7 138-334z"
			/>
			<path
				d="M804 1005.5c-50.5 49.8-101 77.5-101 77.5 70.4 52.4 161 67 161 67s-.5-6.1 0-37c86.4-49.2 186-3 186-3s.3 9.8 1 39c74.2-6.7 145.4-51.2 162-67-28.3-12.5-68.9-48.7-98.1-77.9 10.7-16.4 78.8-102.4 80.1-372.1.2-28.5 1-171 1-171l-30-46s7 179.2-9 355c-11.3 148.9-63.1 206.3-66 209-45.6 56.6-110.7 60-133 60-72.6 0-104.4-30.7-130-59-9.9-12.8-106.8-100-76-571-24.2 32.2-35.4 44.7-38 56 .9 16.1 4.9 193.5 20 331 17.3 157.5 71.6 207.9 70 209.5z"
				className="factionColorPrimary"
			/>
			<path fill="#e6e6e6" d="M937 1041h44v50h-44v-50z" />
			<linearGradient
				id="ch-topdown36003e"
				x1={1056.1333}
				x2={865}
				y1={767.1844}
				y2={767.1844}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#b3b3b3" />
				<stop offset={0.45} stopColor="#f2f2f2" />
				<stop offset={0.552} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#b3b3b3" />
			</linearGradient>
			<path fill="url(#ch-topdown36003e)" d="M865 1111c25-12.8 106.3-39.6 191 2 .3 35.7 0 102 0 102H865v-104z" />
			<linearGradient
				id="ch-topdown36003f"
				x1={1056}
				x2={865}
				y1={676.5}
				y2={676.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a6a6a6" />
				<stop offset={0.45} stopColor="#e6e6e6" />
				<stop offset={0.552} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#a6a6a6" />
			</linearGradient>
			<path fill="url(#ch-topdown36003f)" d="m889 1271 143 1 24-57-191 1 24 55z" />
			<path fill="#e6e6e6" d="M944 1324h32l14-51h-64l18 51z" />
			<radialGradient
				id="ch-topdown36003g"
				cx={1048}
				cy={1060}
				r={42}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.101} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#ccc" />
			</radialGradient>
			<path
				fill="url(#ch-topdown36003g)"
				d="M1048 818c23.2 0 42 18.8 42 42s-18.8 42-42 42-42-18.8-42-42 18.8-42 42-42z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M717.7 458.3c-3.4 65-2 157.4 8.2 268.4 10.1 109.9 30.1 211.8 78.5 277.7m310.1-1.5c62.4-84.5 69.6-216.1 74.1-276.3 5-65.7 8.3-179.7 7.1-270.4"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1215.8 1082.1c-33.1-7.9-105.5-83-124.3-101.6M827 981c-18.7 18.7-91 94.1-124 102"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M756 404c-12.2 40.8-19.4 155.3-4 322s56.4 313 206 313c186.1 0 197.8-231.1 204-313 6-79.6 9.3-239.4 3.5-316.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1001.3 1903.8 18-632.3h7l25-56s-.6-32.6 0-66c75.8-15.3 195.4-47.1 226-171.1 30.6-124 35.4-228.7-1-339.2-29.4-89.3-70.5-174.8-112.8-230.7-10-13.3-20.2-24.9-30.2-34.4-75.2 115.1-284.3 115.2-351.6-.4-10 9.6-20.1 21.1-30.2 34.4-42.3 55.9-83.4 141.4-112.8 230.6-36.4 110.4-31.6 215.2-1 339.1 30.6 123.9 150.2 155.7 226 171 .6 33.4 0 66 0 66l25 56h7l18 633.2 87.6-.2z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1024 1270H890m-26-55 188-1m-188-55v-49s35.9-20.5 96.1-20.5c50.3 0 75.3 12.6 90.6 21.5 0 20.4.3 50 .3 50"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m927 1272 17 52h31l15-52" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1048 818c23.2 0 42 18.8 42 42s-18.8 42-42 42-42-18.8-42-42 18.8-42 42-42z"
			/>
		</g>
		<circle id="Animation-Coordinates" cx={958} cy={788} r={1} fill="#0f0" />
	</svg>
);

export default Component;

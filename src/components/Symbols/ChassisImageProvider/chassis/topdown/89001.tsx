import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<g id="Animation-Engine-Left">
			<radialGradient
				id="ch-topdown89001a"
				cx={420}
				cy={1120.5}
				r={296.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.1} stopColor="#10069f" />
				<stop offset={1} stopColor="#101820" />
			</radialGradient>
			<path
				fill="url(#ch-topdown89001a)"
				d="m464 1096-55-221-157 167-28-23 134-186-232-14v-38l232-14-141-186 29-25 166 170 77-223 37 11-52 232 227-70 13 36-209 92 191 123-21 36-201-103 31 228-41 8z"
			/>
			<path
				fill="#f2f2f2"
				d="M425 734.5c35.1 0 63.5 28.4 63.5 63.5s-28.4 63.5-63.5 63.5-63.5-28.4-63.5-63.5 28.4-63.5 63.5-63.5z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="M425 734.5c35.1 0 63.5 28.4 63.5 63.5s-28.4 63.5-63.5 63.5-63.5-28.4-63.5-63.5 28.4-63.5 63.5-63.5z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="M425 753.5c24.6 0 44.5 19.9 44.5 44.5s-19.9 44.5-44.5 44.5-44.5-19.9-44.5-44.5 19.9-44.5 44.5-44.5z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="M425 786.5c6.4 0 11.5 5.1 11.5 11.5s-5.1 11.5-11.5 11.5-11.5-5.1-11.5-11.5 5.1-11.5 11.5-11.5z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="m409 736 80-233m36 12s-50.7 225.4-53.9 239.3m2.9-8.3 227-70m13 35s-205.8 90.3-226.4 99.3M504 804l193 123m-237.2-75.6c13.5 7 202.7 104.1 216.4 111.2M473 859l31 227m-41 8-57.9-235.5M408 874l-157 168m-28-23 142-197m-7.4 9.9c-24.8-2.2-230.6-13-230.6-12.9m-1-39s231.3-13.3 243.5-14h.5m-11.9.9C346.5 751.4 218 581 218 581m28-25 165 171"
			/>
			<circle id="Animation-Coordinates-Left-Engine" cx={425} cy={798} r={1.1} fill="#00ff0c" />
		</g>
		<g id="Animation-Engine-Right">
			<radialGradient
				id="ch-topdown89001b"
				cx={1499}
				cy={1120.5}
				r={296.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.1} stopColor="#10069f" />
				<stop offset={1} stopColor="#101820" />
			</radialGradient>
			<path
				fill="url(#ch-topdown89001b)"
				d="m1455 1096 55-221 157 167 28-23-134-186 232-14v-38l-232-14 141-186-29-25-166 170-77-223-37 11 52 232-227-70-13 36 209 92-191 123 21 36 201-103-31 228 41 8z"
			/>
			<path
				fill="#f2f2f2"
				d="M1494 734.5c35.1 0 63.5 28.4 63.5 63.5s-28.4 63.5-63.5 63.5-63.5-28.4-63.5-63.5 28.4-63.5 63.5-63.5z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="M1494 734.5c35.1 0 63.5 28.4 63.5 63.5s-28.4 63.5-63.5 63.5-63.5-28.4-63.5-63.5 28.4-63.5 63.5-63.5z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="M1494 753.5c24.6 0 44.5 19.9 44.5 44.5s-19.9 44.5-44.5 44.5-44.5-19.9-44.5-44.5 19.9-44.5 44.5-44.5z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="M1494 786.5c6.3 0 11.5 5.2 11.5 11.5s-5.2 11.5-11.5 11.5-11.5-5.2-11.5-11.5 5.2-11.5 11.5-11.5z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="m1510 736-80-233m-36 12s50.7 225.4 53.8 239.3m-2.8-8.3-227-70m-13 35s209.7 92 226.9 99.5M1415 804l-193 123m238-76s-202.9 104.3-217.2 111.6M1446 859l-31 227m41 8 58-236m-3 16 157 168m28-23-142-197m6.9 9.9c22-1.2 231.1-12.9 231.1-12.9m1-39s-231.3-13.3-243.5-14h-.5m11.8.9C1572.5 751.4 1701 581 1701 581m-28-25-165 171"
			/>
			<circle id="Animation-Coordinates-Right-Engine" cx={1494} cy={798} r={1.2} fill="#13ff00" />
		</g>
		<path
			fill="#b3b3b3"
			stroke="#003"
			strokeWidth={5}
			d="M662 1540.4c24.2 20.2 31.3 26.5 31.3 26.5h528.1s19.3-15.6 32.5-26.5c-38.2-.5-161-16.9-293-16.9-131.9.1-206.6 12.3-298.9 16.9z"
		/>
		<path
			fill="#b3b3b3"
			stroke="#003"
			strokeWidth={5}
			d="M1260 1541.6c-3.1-.6-6-1.2-6-1.2s-17.2 15.7-31.3 26.5c14.4 16.4 102.3 85.9 147.1 89.2m-676-89.5c-14.4 16.4-95 83.5-139.6 86.8.6-.3.4-1 1.7-2.2 27.4-16 47.5-35.9 100.7-109.9 3.1-.6 6 0 6 0s17.1 14.5 31.2 25.3z"
		/>
		<linearGradient
			id="ch-topdown89001c"
			x1={956.775}
			x2={956.775}
			y1={366.3101}
			y2={396.4399}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89001c)"
			stroke="#003"
			strokeWidth={5}
			d="M1085.2 1527.2s13.9 14.4 20.5 21.3c-3.2 1-16.4 5.3-16.4 5.3H822.9c-4.1-1.3-15-5-15-5s.6-.7 1.6-2c8.7-6.1 25-18.3 25-18.3s81.4-4.8 126.3-4.8c44.2-.1 124.4 3.5 124.4 3.5z"
		/>
		<path
			fill="#f2f2f2"
			d="M844.1 1526.6c-90.6 4.9-147.6 13.8-187 13.8-28.4 41.5-71.7 99.3-113.3 114.5-41.6 9.6-92.8-24.1-92.8-24.1s-129.5-96.4-241-352.8c-51.4-122.2-107-302.7-107-472 .5-180.1 66.9-322.5 150-431 104.3-137.8 216.6-206.1 295-238 2.2-6.6 12.3-18.4 26-22 2.3-12.1 8.6-33.4 13-44 27.1.4 55.4.5 85 1 4.6 4.7 7 13.1 9 19 18.3 1.3 25 4 27.2 5.9 72.4-12.2 133-21.2 162.4-26.7 40.9-7.5 121.9-11.5 177.4-.2 29.4 5.5 90 14.6 162.4 26.7 2.2-1.9 8.9-4.6 27.2-5.9 1.9-5.9 4.4-14.3 9-19 29.6-.5 57.9-.6 85-1 4.4 10.6 10.7 31.9 13 44 13.8 3.6 23.8 15.4 26 22 78.4 31.9 190.8 100.2 295.2 237.9 83.1 108.5 149.6 250.8 150.1 430.9 0 169.3-55.7 349.7-107.1 471.9-111.6 256.3-241.1 352.7-241.1 352.7s-51.2 33.7-92.9 24.1c-41.6-15.2-85-72.9-113.4-114.5-39.4 0-96.4-8.9-187.1-13.8-33.8-1.8-196.2-1.2-230.2.6zM125.5 797c0 166 134.5 300.5 300.5 300.5S726.5 963 726.5 797 592 496.5 426 496.5 125.5 631 125.5 797zm1067 0c0 166 134.5 300.5 300.5 300.5S1793.5 963 1793.5 797 1659 496.5 1493 496.5 1192.5 631 1192.5 797z"
		/>
		<linearGradient
			id="ch-topdown89001d"
			x1={683}
			x2={574}
			y1={1826.5}
			y2={1826.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b1b1" />
			<stop offset={0.398} stopColor="#f2f2f2" />
			<stop offset={0.602} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path fill="url(#ch-topdown89001d)" d="m683 91-11-19h-86l-12 43s50.2-25 109-24z" />
		<linearGradient
			id="ch-topdown89001e"
			x1={1345}
			x2={1236}
			y1={1826.5}
			y2={1826.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b1b1" />
			<stop offset={0.398} stopColor="#f2f2f2" />
			<stop offset={0.602} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path fill="url(#ch-topdown89001e)" d="m1236 91 11-19h86l12 43s-50.2-25-109-24z" />
		<linearGradient
			id="ch-topdown89001f"
			x1={1077.9316}
			x2={1427.0726}
			y1={1046.6047}
			y2={1052.6997}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89001f)"
			d="M1122.5 82.2c34.5 257.7-36.5 1330.6-44.6 1443.8 40 3 143.3 14.1 182 14.5 73.1 110.2 119.4 118.1 119.4 118.1s64.9-165.1 43.4-323v-223S1184.5 1070.4 1168 798c3.1-35 6-244.7 254.7-315.8v-322s-23.5-12.1-50.7-22.2c-2.8 46.6-10 94-18.3 135.4-15.6 77.2-37.6 133.3-59.7 124.6-23.2 3.2-79.8-113.7-81.2-299.4-31.8-5.1-62.3-13.6-90.3-16.4z"
		/>
		<path
			fill="#101820"
			d="M567.4 269.5C554.8 215.4 548.2 158.4 550 153c3.5-10.2 18.1-20.2 38.1-28.5 0 2.2 2.1 68.5 6.6 109.8.3 2.9.6 4.5.9 6.8-6 2.4-23.4 7.9-28.2 28.4zm1.2 11.3c12.8 52.1 28.7 97 31.4 102.2 5.6 10.9 14.9 13.1 25 15-13.6-39.3-22.3-94.1-27.8-144.4 0-.3-9.2 3.4-16.2 10.4-5.7 5.7-12.6 15.9-12.4 16.8zm123.9-23.3c14.6-64.8 15.8-121.1 13.5-154.5-31.2-2.5-75.2 2-109.8 15.9.1 8.8 3.6 60.4 10 119.5 7.2-.8 30.8-4.5 51.8 1.6 17.3 5 34.3 18.1 34.5 17.5zM688 272c-15.9 67-41.9 125.1-55 121-10.9-29.9-19.5-87.6-25.7-142.5 9.3-1.4 32.5-2.3 51.7 3.5 13.4 4 29.2 17.2 29 18zm665.3-2.6c12.6-54.2 17.2-111.3 15.4-116.7-3.4-10.2-19.1-20.2-39.1-28.6 0 2.2-2.1 68.6-6.6 109.9-.3 2.9-.6 4.5-.9 6.8 6.1 2.6 26.4 8 31.2 28.6zm-4.2 11.2c-12.7 52.1-28.7 97.1-31.3 102.3-5.6 10.9-14.9 13.1-25 15 13.6-39.4 22.3-94.3 27.8-144.6 0-.3 9.2 3.4 16.1 10.4 5.8 5.9 12.6 16 12.4 16.9zm-123.7-23.2c-14.6-64.9-15.8-121.3-13.5-154.7 31.2-2.6 75.1 2 109.7 15.9-.1 8.8-3.6 60.5-10 119.6-7.2-.8-30.8-4.5-51.8 1.6-17.3 5-34.3 18.2-34.4 17.6zm4.5 14.5c15.8 67.1 41.8 125.3 54.9 121.1 10.9-29.9 19.5-87.7 25.7-142.7-9.3-1.4-32.5-2.3-51.7 3.5-13.3 4-29.1 17.3-28.9 18.1z"
		/>
		<linearGradient
			id="ch-topdown89001g"
			x1={486.6794}
			x2={841.6334}
			y1={1046.4495}
			y2={1052.6445}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89001g)"
			d="M797 82.2c-34.5 257.7 36.5 1330.6 44.6 1443.8-40 3-143.3 14.1-182 14.5-73.1 110.2-119.4 118.1-119.4 118.1s-81.6-252.7-43.4-323v-224s68-13.1 132.2-63.6c59.5-46.8 115.3-126.7 119.8-228.9.3-6.9 1.4-13.9 1.2-21.1-2.6-33.3-10.7-257.3-253.2-315.8v-321s20.3-9.8 51.2-23.2c15.3 163.4 48.4 267.9 76 261 6-1.5 14.4-6.3 22.4-17.9 28.2-41 62-143.9 60.6-283.1 31.1-6.3 62.8-13 90-15.8z"
		/>
		<linearGradient
			id="ch-topdown89001h"
			x1={859.9026}
			x2={1063.5056}
			y1={1446.7775}
			y2={1450.3304}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8f9bb0" />
			<stop offset={0.399} stopColor="#dae3f2" />
			<stop offset={0.604} stopColor="#dae3f2" />
			<stop offset={1} stopColor="#8f9bb0" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89001h)"
			d="M860.9 150.9c-5.2 183.7 10.6 486.6 22.9 574.8 10.8 77.2 26 209.7 77.2 209.7s64.2-147.7 74.8-202.5c20.7-107.8 33.4-490.6 25.3-584.5-5-119.7-49.5-141-100.1-141-42.2 0-73.2 14.7-88.9 61.9-6.8 20.6-10 47.3-11.2 81.6z"
		/>
		<path
			fill="#e6e6e6"
			stroke="#003"
			strokeWidth={5}
			d="M860.9 370.2c12.9-16.4 22.9-20.5 22.9-20.5V176.2c-9.7-8.8-10.5-10-21.7-21.7-6 76.5-2.4 171.1-1.2 215.7zm200.4 0c-13-16.4-23.1-20.5-23.1-20.5V176.2c9.8-8.8 10.6-10 21.9-21.7 6 76.5 2.4 171.1 1.2 215.7z"
		/>
		<linearGradient
			id="ch-topdown89001i"
			x1={888.7788}
			x2={1032.093}
			y1={1219.1416}
			y2={1221.6436}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.102} stopColor="#676767" />
			<stop offset={0.261} stopColor="#ccc" />
			<stop offset={0.297} stopColor="#ccc" />
			<stop offset={0.342} stopColor="#f2f2f2" />
			<stop offset={0.408} stopColor="#ccc" />
			<stop offset={0.596} stopColor="#b3b3b3" />
			<stop offset={0.799} stopColor="#666" />
			<stop offset={0.894} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89001i)"
			d="M967.2 579s61.6 5.5 64.8 53c1.2 55.4-23.7 188-66.2 188 0-49.6 1.4-241 1.4-241zm-13.1 241c-46.8 0-67.8-156.8-65.1-188 7.2-50.4 63.8-51 63.8-51s1.3 189.4 1.3 239z"
		/>
		<linearGradient
			id="ch-topdown89001j"
			x1={959.501}
			x2={959.501}
			y1={620}
			y2={1851}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#10069f" />
			<stop offset={1} stopColor="#101820" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89001j)"
			d="m831 1300 64-32h131l67 32s8.7-164.1 10-181 16-290.4 16-321 7-119.1 7-157 6-219.2 6-275 .3-220.6-8-283c-27.9-4.4-73-12-73-12s12 53.3 12 165-9.8 358.2-28 493-35.9 206-75 206-57.1-79.8-67-138-26.2-253.7-31-373c-.2-4.6-3-84.5-3-164s2.5-152.9 12-191c-26.2 5-72 13-72 13s-12 72.7-12 244c0 64.8 10.3 375.9 11 402s33 572 33 572z"
		/>
		<path
			fill="#10069f"
			d="m845 1515-14-201s47.3-23.3 61.2-30.1C885.9 1316.4 845 1515 845 1515zm232.1 0 13.9-200.9s-47.2-23.3-61-30.1c6.3 32.5 47.1 231 47.1 231zM216 1042c4.4 20 22 93.2 63.9 183.1 37.5 80.3 83.6 172.8 137.1 237.9-8.7 49.5-14 72-14 72s58.7 73 109 73 76-51.1 78-56c10.9 3.3 49 15 49 15s-58.8 90-112 90-95.5-39.5-135-84-130.2-151.9-211-367c-12-31.8-78-228.3-78-395 .7-.6 1 0 1 0s1.3 27.2 8.9 61c5.4 23.7 14.4 50.8 26.1 74 25.5 50.3 63.7 84.3 77 96zm1312 532s-42.8 52.9-86 76c-12.7 6.8-25.2 9-37 9-13.8 0-28.6 1.6-44-8-42.6-26.3-84.8-83.9-84-84 1.1-.1 54-14 54-14s20.8 54 72 54 107.2-59 114-71c-2.9-16.4-16-71-16-71s150.1-187.2 207-426c17.4-16.9 84.9-79.9 102-186 1.8.2 4 1 4 1s-4.3 60.2-7 80c-1 7.5-14.8 113-52 231-27.2 86.2-70.4 177-113 251-53.5 93.1-114 158-114 158z"
		/>
		<path fill="#f2f2f2" d="m828 700 1 53h21l-1-53h-21zm264 0-1 53h-21l1-53h21z" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M548 137c-154.4 69.5-249.9 159.5-348.3 315.9C102.6 607.3 103 786.3 103 798c0 172.6 56.9 367.1 105.1 477.3 35.3 80.6 60.3 122.9 93.3 176.6 66.9 109 149.6 178.9 149.6 178.9s51.2 33.7 92.8 24.1c41.6-15.2 84.9-72.9 113.3-114.5 54.1 0 141.4-16.9 303.8-16.9s228 16.9 297.8 16.9c85.6 120.5 119.4 116.9 119.4 116.9s53.3 11.4 92.8-26.5c157.5-141.8 235.5-344.1 260-401.8 17.3-40.5 85-235 85-431 0-18.5 14.4-327.3-297-575-37.6-29.9-85.8-62.3-146.6-84.9M1212 98c-90.4-17.3-161-27-161-27s-14.6-65-91-65c-64.4 0-77.7 35-91 64-54 7-142.7 24.2-161 27"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={5}
			d="M426.5 474.7c178.3 0 322.8 144.5 322.8 322.8s-144.5 322.8-322.8 322.8-322.8-144.5-322.8-322.8 144.5-322.8 322.8-322.8zm1066.5.3c178.4 0 323 144.6 323 323s-144.6 323-323 323-323-144.6-323-323 144.6-323 323-323z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeLinejoin="round"
			strokeWidth={5}
			d="m745 742 52-15h86m-53 573 66-33h129l67 33m-67-32 53 258m227-465c0 12.2-16.2 176.9-28.4 309.9-8 87.8-14.6 164.9-14.6 172.1m17 23c11.3-3.5 52-14 52-14s49 135 186-16m9 36-25-108s139.1-169.5 207-426m-813 230-53 258m-184 16s-36.4-430.6-44-481m-399-19c28.9 131.4 134.2 343.4 203 421-10.9 53.7-24 109-24 109m8-39c11.1 11.7 63.1 75 106 75s66.2-28.5 82-56c22.3 6.4 49 14 49 14m396-839 88-1 52 15"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeLinejoin="round"
			strokeWidth={5}
			d="M870 1394.8s172.2.1 181 0m7 34.2H862m162-33v11h-28v-11m-22 0v11h-28v-11m-22 0v11h-29v-12"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeLinejoin="round"
			strokeWidth={5}
			d="M548 136c0 52.2 10.5 116.3 35 200 13.5 46.1 22.3 67.5 46 60 8.8-2.8 20.9-7.1 43-73s35-107.4 35-227c-22.7-6.7-50.3-4.2-77 1-42.4 8.3-82 28.2-82 39z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeLinejoin="round"
			strokeWidth={5}
			d="m572 115 13-44h87l10 21m10 169c-5.8-8.4-23-26-67-26-59.9 0-59 39-59 39"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeLinejoin="round"
			strokeWidth={5}
			d="M1371 136.1c0 52.3-10.4 116.5-35 200.3-13.5 46.2-22.3 67.6-46 60.1-8.8-2.8-20.9-7.1-43-73.1s-35-107.5-35-227.4c22.7-6.7 50.3-4.2 77 1 42.4 8.3 82 28.3 82 39.1z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeLinejoin="round"
			strokeWidth={5}
			d="M1347 115.1 1334 71h-87l-10 21m-10 169.3c5.8-8.4 23-26 67-26 52.8 0 58.4 30.4 58.9 37.5"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={5}
			d="M426 496.5c166 0 300.5 134.5 300.5 300.5S592 1097.5 426 1097.5 125.5 963 125.5 797 260 496.5 426 496.5zm1067 0c166 0 300.5 134.5 300.5 300.5S1659 1097.5 1493 1097.5 1192.5 963 1192.5 797 1327 496.5 1493 496.5z"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M845.3 1528.4c-29.5-449.2-79.9-1179-47.3-1445.8" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1077.3 1528.2c29.5-449 73.3-1172 46.1-1445.2" />
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1050.2 68.2c22 88.8 14.6 369.6-7.2 586.5-15.8 157.2-33.2 280.8-82 280.8-61.7 0-76.2-201.3-83.2-280.5s-37.2-455.7-8-586"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M968.2 581.1c20.4 0 68.3 11.9 63.9 68.7-4.9 63.3-29.7 170.3-65.1 169.9 0-82.4 1.2-172.7 1.2-238.6zm-15.7 0c-20.3 0-68 11.9-63.6 68.7 4.8 63.3 29.6 170.3 64.8 169.9.1-82.4-1.2-172.7-1.2-238.6z"
		/>
		<g id="Animation-Gun-Right">
			<path fill="#f2f2f2" d="m1450.5 1903.5 14-476h6l17-43v-200l-19-38h-118l-19 37v201l17 42h9l12 477h81z" />
			<path fill="#e6e6e6" d="m1421.5 1467.5 11-27h-44l11 27h22z" />
			<linearGradient
				id="ch-topdown89001k"
				x1={1487.5}
				x2={1332.5}
				y1={753.5}
				y2={753.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a6a6a6" />
				<stop offset={0.401} stopColor="#e6e6e6" />
				<stop offset={0.601} stopColor="#e6e6e6" />
				<stop offset={0.997} stopColor="#a6a6a6" />
			</linearGradient>
			<path fill="url(#ch-topdown89001k)" d="m1487.5 1184.5-20-37h-116l-19 38 155-1z" />
			<linearGradient
				id="ch-topdown89001l"
				x1={1487.5}
				x2={1332.5}
				y1={634.5}
				y2={634.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#b3b1b1" />
				<stop offset={0.398} stopColor="#f2f2f2" />
				<stop offset={0.602} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#b3b3b3" />
			</linearGradient>
			<path fill="url(#ch-topdown89001l)" d="M1487.5 1384.5v-199h-155v200l155-1z" />
			<linearGradient
				id="ch-topdown89001m"
				x1={1487.5}
				x2={1332.5}
				y1={513.5}
				y2={513.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a6a6a6" />
				<stop offset={0.4} stopColor="#e6e6e6" />
				<stop offset={0.599} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#a6a6a6" />
			</linearGradient>
			<path fill="url(#ch-topdown89001m)" d="m1487.5 1386.5-17 41-121-1-17-41 155 1z" />
			<path fill="#101820" d="m1464 1428-42 86h-18l-44-87v47l36 78-1 351h30l2-350 37-69v-56z" />
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="m1450.5 1903.5 14-476h6l17-43v-200l-19-38h-118l-19 37v201l17 42h9l12 477h81z"
			/>
			<path fill="none" stroke="#003" strokeWidth={5} d="M1333.5 1184.5h153m1 201h-155m21 41 114 1" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M1432.5 1429.5v10l-11 28h-23l-12-27v-12m48 12h-46" />
		</g>
		<g id="Animation-Gun-Left">
			<path fill="#f2f2f2" d="m465.5 1903.5-14-476h-6l-17-43v-200l19-38h118l19 37v201l-17 42h-9l-12 477h-81z" />
			<path fill="#e6e6e6" d="m494.5 1467.5-11-27h44l-11 27h-22z" />
			<linearGradient
				id="ch-topdown89001n"
				x1={583.5}
				x2={428.5}
				y1={753.5}
				y2={753.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a6a6a6" />
				<stop offset={0.401} stopColor="#e6e6e6" />
				<stop offset={0.601} stopColor="#e6e6e6" />
				<stop offset={0.997} stopColor="#a6a6a6" />
			</linearGradient>
			<path fill="url(#ch-topdown89001n)" d="m428.5 1184.5 20-37h116l19 38-155-1z" />
			<linearGradient
				id="ch-topdown89001o"
				x1={583.5}
				x2={428.5}
				y1={632.5}
				y2={632.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#b3b1b1" />
				<stop offset={0.398} stopColor="#f2f2f2" />
				<stop offset={0.602} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#b3b3b3" />
			</linearGradient>
			<path fill="url(#ch-topdown89001o)" d="M428.5 1386.5v-199h155v200l-155-1z" />
			<linearGradient
				id="ch-topdown89001p"
				x1={583.5}
				x2={428.5}
				y1={513.5}
				y2={513.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a6a6a6" />
				<stop offset={0.4} stopColor="#e6e6e6" />
				<stop offset={0.599} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#a6a6a6" />
			</linearGradient>
			<path fill="url(#ch-topdown89001p)" d="m428.5 1386.5 17 41 121-1 17-41-155 1z" />
			<path fill="#101820" d="m452 1428 42 86h18l44-87v47l-36 78 1 351h-30l-2-350-37-69v-56z" />
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="m465.5 1903.5-14-476h-6l-17-43v-200l19-38h118l19 37v201l-17 42h-9l-12 477h-81z"
			/>
			<path fill="none" stroke="#003" strokeWidth={5} d="M582.5 1184.5h-153m-1 201h155m-21 41-114 1" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M483.5 1429.5v10l11 28h23l12-27v-12m-48 12h46" />
		</g>
	</svg>
);

export default Component;

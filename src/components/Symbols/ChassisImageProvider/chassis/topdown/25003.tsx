import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#e6e6e6"
			d="M975.3 1596.9c106.3 0 160.2-55.6 218.4-129.5 43.2-54.9 71.2-106.3 71.2-106.3l2 184 300-1V339l-302-1v381l-15-11s-16.8-147.1-76-213c-54.3-60.5-164.5-63.9-205-64h-18.1c-40.4.1-150.6 3.5-205 64-59.2 65.9-76 213-76 213l-15 11V338l-302 1v1205l300 1 2-184s28 51.5 71.2 106.3c58.3 74 101.1 129.5 218.4 129.5 5.5.1 25.7.1 30.9.1z"
		/>
		<path
			fill="#d9d9d9"
			d="M1180 1484c-5.9-12.4 30-175.2-27-343-21.5.5-110.9 73.7-200 76-96.8 2.5-193-66-193-66s-43.1 108.3-21 332c49.5 68.9 112.8 120.4 221 114 40.1-2.3 131.8 7.5 220-113z"
		/>
		<path
			fill="#b3b3b3"
			d="M959 464c25.5 0 114.5.6 129 77 3.3 90.3 2.4 197-127 197-85.7 0-114.3-48.8-124-92s-15.6-134 21-154 75.5-28 101-28z"
		/>
		<path
			fill="#bfbfbf"
			d="M1250 707c-37.5-298.8-181.1-259.2-290-277-250.5 2.2-266.4 132-290 278 57.5-28.2 72.9-116.9 125.2-183.6C827.1 483.7 867.1 446 961 446c102.6 0 147.3 47.7 180 102 39.4 65.5 58.9 140.9 109 159z"
		/>
		<path
			fill="#bfbfbf"
			d="M1567 1544v-131h-301l1 133 300-2zm-912-130-2 131-300-1v-130h302zm1-1076-1 218-302 1V338h303zm610 1 302 1-1 216-301-1V339z"
		/>
		<path
			fill="#ccc"
			d="m1569 1411-303 2v-54s69-98.3 69-268c0-26.3 5-324.4-70-373-.5-76.1 2-162 2-162l299 1 3 854zm-1212.7 0 297.7 2v-54s-69-98.3-69-268c0-26.3-5-324.4 70-373 .5-76.1-2-162-2-162l-298.7-1 2 856z"
		/>
		<linearGradient
			id="ch-topdown25003a"
			x1={959.9492}
			x2={959.9492}
			y1={373}
			y2={1200}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.36} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown25003a)"
			d="M852 720c55.6 57.7 173.4 45.6 216 0 1.8 12.4 13.5 71.4 28.8 151.9 21.6 113.7 81.9 324.8 85.2 386.1 7.7 117.2 6.2 214.1-7 234-21.2 27.3-37 42.4-57 54-114.2 0-256.6 1-316 1-19.2-5.9-45.2-42-60-59-7.6-75.6-14.6-188.7-1-254 9-53.3 82.7-348.6 111-514z"
			opacity={0.2}
		/>
		<path
			d="m1111 1070 46 1s-9.4 304.3-52.1 501.9c-135.7-1.4-217.6-.6-304.3.1-.9-4.3-1.8-8.6-2.6-13-22.9-119.5-38-301.8-38-491 16.5-1.2 36 1 36 1s45 116 164 116c24.8 0 62-14.4 89-38 37.6-32.8 62-78 62-78z"
			className="factionColorSecondary"
		/>
		<path
			fill="#d9d9d9"
			d="M1104 1575c-4.3 24.4-31.2 109.7-40 122-109.4.7-224 0-224 0s-38.5-91.2-40-123c41.3-1.5 252.6-.6 304 1z"
		/>
		<linearGradient
			id="ch-topdown25003b"
			x1={958.5}
			x2={958.5}
			y1={224}
			y2={851.3423}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown25003b)"
			d="M1104.9 1572.9c42.7-197.5 52.1-501.9 52.1-501.9l-46-1s-24.4 45.2-62 78c-27 23.6-64.2 38-89 38-119 0-164-116-164-116s-19.5-2.2-36-1c0 189.2 15.1 371.5 38 491 .8 4.4 1.7 8.8 2.6 13 7.5 38.3 26.7 105.5 39.4 122 28.4 1 225 1 225 1s32.6-87.8 39.9-123.1z"
			opacity={0.102}
		/>
		<radialGradient
			id="ch-topdown25003c"
			cx={1012.806}
			cy={1188.681}
			r={729.24}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0.2} />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</radialGradient>
		<path
			fill="url(#ch-topdown25003c)"
			d="M853 722c-22.3-14.2-70.4-154.6-25-231-66.5 54.4-91.4 153.7-123 189s-61.5 35.3-80 75c-19.7 29-64.8 241.8-24 475 21.4 92.7 91 197.9 139 255-4-56.2-7.2-109.7-8-144-8-101.9 96.2-444.7 121-619z"
		/>
		<radialGradient
			id="ch-topdown25003d"
			cx={893.58}
			cy={1273.897}
			r={727.797}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0.2} />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</radialGradient>
		<path
			fill="url(#ch-topdown25003d)"
			d="M1068 722c22.2-14.2 70.3-154.6 24.9-231 66.3 54.4 91.2 153.7 122.8 189 31.6 35.3 61.4 35.3 79.8 75 19.6 29 64.7 241.8 23.9 475-21.4 92.7-90.8 197.9-138.7 255 4-56.2 7.2-109.7 8-144 8.1-101.9-95.9-444.7-120.7-619z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1106.9 1556.9c32.1-22.9 59-54.1 86.9-89.5 43.2-54.9 71.2-106.3 71.2-106.3l2 184 300-1V339l-302-1v381l-15-11s-16.8-147.1-76-213c-54.3-60.5-164.5-63.9-205-64h-18.1c-40.4.1-150.6 3.5-205 64-59.2 65.9-76 213-76 213l-15 11V338l-302 1v1205l300 1 2-184s28 51.5 71.2 106.3c22.9 29 43.4 55.2 66.9 76.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m955 1190 1 384" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1566 1413h-301" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1567 556h-301" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1262 1365c14-24.6 73-103.6 73-278s-9.8-328.3-71-369" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M959.8 757.9c-72.8 0-150-19.1-150-202 0-45.2 26.7-109.2 145-110.9h9.4c119 1.5 145.8 65.6 145.8 111 0 182.9-77.2 202-150 202"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M959.9 737.8c-69.1-1.5-132-22.2-130.1-177.5.5-43.5 16.3-91.7 124.1-96.9 2-.1 10.7-.1 12.9.1 107 5.4 122.7 53.4 123.2 96.8 1.9 155.4-61 176-130.1 177.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M354 1412.8h300.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M477 1543V880.7c18.2-28.8 32.6-52.1 50-81.7V556m863 0v243c17.4 29.6 31.8 53 50 81.7V1543"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M353 556h300.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M657.9 1364.8c-14-24.6-73-103.6-73-277.9 0-174.3 9.8-328.2 71-368.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M718 660c-20.1 70.3-66.2 295.6 42.5 418" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1200.9 660c20.3 70.9 66.9 299.3-45.2 421" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1179 1482c17.3-165.8 5.3-237.1-23.6-356.2m-14.4-58.5c-19.9-81-45.1-186.3-73-345.3m-216.2-.1c-27.8 159-53 264.2-73 345.2m-18.2 74.7c-26.1 109.7-36.2 181.8-19.7 340 .1.6 1.1 7.3 1.1 7.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1253 710c-80.2-34.6-87.7-197.6-207.4-250.4m-172.6 1c-119.5 52.9-127 215.2-207 249.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M761 1068.1c0 68.2-.5 491.2 80 626.8 69.9.1 112 0 112 0l113 1s90-198.5 90-627.8c-26.9-.3-46 0-46 0s-55.2 118-157 118-154-105.1-155-118c-19.4.2-28.7 0-37 0z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M802 1573h303" />
		<g id="Animation">
			<path
				fill="#f2f2f2"
				d="M1232 1896h189l33-1122h18l38-98-3-424-91-235-180-1-92 235v116s-74.6-8.6-140.6-11.8c0-19.1-3.7-69.4-5.4-93.2-5.1 0-67.4.8-74 1-3.8 4.4-7.9 78.5-7.5 92.2C821.7 359.2 654 372 654 372s-25.4-136-134-136c-98.4 0-126.6 126.3-134 153s-14 359-14 451c48.8-.4 298 1 298 1l11-182s145.9 28.5 232.6 38.6c2.1 22.6 5.3 85.1 12.4 95.4 7.2 0 41.8.1 71 0 8.6-30.6 7.1-81.6 7.9-95.1 57.3-6.7 121-22.3 138.1-25.9 10.8 30.4 43 102 43 102h13l33 1122z"
			/>
			<radialGradient
				id="ch-topdown25003e"
				cx={962.1509}
				cy={-1092.3135}
				r={534.2616}
				gradientTransform="matrix(1 0 0 0.3631 0 918.7114)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.2991} stopColor="#f2f2f2" stopOpacity={0} />
				<stop offset={1} stopColor="#000" stopOpacity={0.5} />
			</radialGradient>
			<path
				fill="url(#ch-topdown25003e)"
				d="M960.3 701.7c152-8.5 444-95.3 516-139 7.4-11.4 34.5-47.3 2-112-9.1.1-278.4-103.5-518-96-91.6 2.9-224.3 7.8-302 17-7.7 1-8.2 4.1-19.7 11.8-27.9 25.1-31.8 126.3-31.3 135.2 2 37.3 30.1 99 65.8 114.4 3 6.1.7 15 2.3 20.9 1.3 4.8 5.9 2.7 5.9 2.7 26.3 16.3 202.7 34.2 279 45z"
				opacity={0.7}
			/>
			<path fill="#e6e6e6" d="m1275 807 104 3-25 59-52 1-27-63z" />
			<path fill="red" d="M513 844h129v19l-128 2-1-21zm-109 0h76l-.6 19-74.8 2-.6-21z" />
			<linearGradient
				id="ch-topdown25003f"
				x1={1510}
				x2={1144}
				y1={1195}
				y2={1195}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a3a3a3" />
				<stop offset={0.496} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#a3a3a3" />
			</linearGradient>
			<path fill="url(#ch-topdown25003f)" d="m1144 677 366-2-37 98-286 2-43-98z" opacity={0.8} />
			<path
				d="M922 262h78s7 189.1 7 249 .9 251.2-10 283c-25.6-1.7-72.2-1.8-74-1.1.1-2.8-17.8-84-8-276.9 0-209.5 7-254 7-254zm1 530.9c0 .1 0 .1 0 0 0 .1 0 .1 0 0zM1181 444c21.6 0 78 16.7 78 71s-57.2 72-73 72-71-18.1-71-74 44.4-69 66-69zm-38 73c0 31.7 32 43 43 43s44-10.1 44-42-27.8-45-43-45-44 12.3-44 44zm-767-55s14.1-227 140-227c5 0 48.2-9.6 82 25 31.6 32.3 54 109 54 109s-23.6 11.9-36 51c-6.7 21.2-6 50.5-6 90 1.5 99 64 126 64 126l-4 205-300 2 6-381zm96 285 82 1-1-203-19-1-1-114s-5.1-11-18-11-20 5.5-20 16v109h-21l-2 203z"
				className="factionColorPrimary"
			/>
			<path
				d="M1181 444c21.6 0 76 16.7 76 71s-48.2 72-71 72c-20.8 0-71-18.1-71-74s44.4-69 66-69zm-36 70c0 42 35 45 40 45s44-6.2 44-39c0-43-32-47-44-47s-40 12.7-40 41z"
				opacity={0.149}
			/>
			<path
				d="m557 748-21 1-1-165s-42.7-.6-42 0 1 164 1 164l-20 1V544l21-1 1 16s5.3 10 19 8 18-9 18-9l1-14 20 2 3 202z"
				opacity={0.149}
			/>
			<linearGradient
				id="ch-topdown25003g"
				x1={534}
				x2={495}
				y1={1427}
				y2={1427}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.496} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown25003g)"
				d="M512 419c7.5 0 22 3.5 22 16v124s-6.8 8-20 8-19-5-19-12V433s3-14 17-14z"
				opacity={0.302}
			/>
			<linearGradient
				id="ch-topdown25003h"
				x1={959.5443}
				x2={959.5443}
				y1={1126}
				y2={1658}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.496} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown25003h)"
				d="M922 262h78s7 189.1 7 249 .9 251.2-10 283c-26.4-1.7-75-1.8-74-1s-18-79.6-8-277c0-209.5 7-254 7-254z"
				opacity={0.2}
			/>
			<linearGradient
				id="ch-topdown25003i"
				x1={1511}
				x2={1143}
				y1={1456.5}
				y2={1456.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown25003i)"
				d="M1144 674c24 .7 367 1 367 1l-5-422-362-1-1 114s219.2 40.1 332 86c18.9 37.2 30.5 61.9 2 110-57 22.8-245.2 91-333 112z"
				opacity={0.2}
			/>
			<linearGradient
				id="ch-topdown25003j"
				x1={1326.6646}
				x2={1145.6646}
				y1={1750.2622}
				y2={1819.9962}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.2} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path fill="url(#ch-topdown25003j)" d="M1327 252V18l-92-2-89 236h181z" opacity={0.502} />
			<linearGradient
				id="ch-topdown25003k"
				x1={1508.3214}
				x2={1327.3214}
				y1={1818.056}
				y2={1752.178}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.8} stopColor="#e6e6e6" />
			</linearGradient>
			<path fill="url(#ch-topdown25003k)" d="M1327 252V18l92-2 89 236h-181z" opacity={0.4} />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M913.7 697.6C827 687.6 681 659 681 659l-6-2m330.9 40.8c57-6.8 120.2-22.2 137.1-25.8 10.8 30.4 43 102 43 102h13l33 1122h189l33-1122h18l38-98-3-424-91-235-180-1-92 235v116s-73.7-8.5-139.4-11.7m-88.3-.1C821.4 359.2 654 372 654 372"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1454 774h-72.2c.3 14.1.2 32 .2 32l-28 63-54 1-27-64-1-32h-85"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1379 808h-104" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1267 774h116" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1509 676h-364" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1506 253-362-1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1168 253 22-54h46l14 53m36 0 12-53 64 1 14 53m35 0 7-54h45l20 52"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1436 63-217-1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1140 673c47.3-6.4 261.6-75.6 337-110 12.2-23.5 18-41.7 18-54 0-12.4-9.4-50.9-18-56s-236.6-74.9-339-87"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1495 512h-237.8m-142.3 0h-106.8m-97.7 0H607" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1186 444c39.2 0 71 31.8 71 71s-31.8 71-71 71-71-31.8-71-71 31.8-71 71-71z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1186.5 471.9c24.1 0 43.6 19.5 43.6 43.6s-19.5 43.6-43.6 43.6-43.6-19.5-43.6-43.6 19.5-43.6 43.6-43.6z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M533 543h22v205h-82V543h22.2c0 5.4-.2 15-.2 15s.8 9 19 9 19-8.2 19-11v-13z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M495 543V434s.2-15 18-15 20 12 20 15v109" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m473 546 20 36v164m42 1V582l18-38m-19 38h-40" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M650 373c-9.1-48.8-30.5-138-129-138S391.3 349 382 420s-12 421-12 421h302s4.7-126.4 3-207c-29.6-13-65-49-65-130s9.7-106.1 40-131z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M400 845v18h78v-19" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M512 845v18h128v-19" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M999 261c-11.6-.5-77 0-77 0s-9 115-9 289c0 47.4-2.3 223.3 12 242 22.8.1 72 0 72 0s10-34 10-192c0-27.7-3.6-318.5-8-339z"
			/>
			<circle id="Animation-Coordinates" cx={960.5} cy={577.5} r={0.5} fill="#18ff00" />
		</g>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#bfbfbf"
			d="M1279.1 1298.8c16.4 107.1-41.2 269.7-169.1 330.2-5.2 48.1-21.5 120.7-25.9 136-3.7-35.2-20.8-107.5-23.1-112-9.9 5.7-43.8 13.1-58 14-2.7 19.6-7.9 198.6-7.9 198.6h-73L915 1665s-28.2 2.6-57-11c-5.4 17.5-23.1 97.8-23.9 106.7-6.4-23.2-23.3-124.2-26.1-132.7-14.6-8.2-184.5-82.4-170.9-329.2-12.9-.2-30.5.6-31-15.1s12.3-16.8 15-17.3c-1.5-2.4-4.4-8.7-7.8-16.9-1.5-3.6-10.5 8-14.3 4.6-8-7.1-12.9-29.1-16-38-.4-1.1 14.5-8 14.1-9-2.6-7.6-4.8-14.5-6-19.6-15.7 2.5-130.7 36.4-178-191.5 25.7 45.7 87.3 175.3 169 140.6-2.4-11.9-7-36.5-10.8-57.1-.1-.6-13.2 6.2-13.3 5.6-2.8-12.2-9-43.3-10.3-74.2 5.5-2.6 11-3.7 12.4-4.1-2.8-12.8-4.2-25.5-4.2-29.7-13.7.2-189.3 37.7-286-327 61.9 104.5 140.4 260.6 281 264 .4-11.3-1.7-22.5-1-37 .1-1.2-20.1-2.8-20-4 1.4-26.4-.8-67.8 8-93 .4-1.2 19.7 3.2 20 2 4.9-18 4.4-22.6 8-32-35.9-28.5-162.5-110.3-156-393 30.8 125.5 71.3 290.3 171 330 2.6-7.2 5.1-14.2 7.6-20.9.2-.5-10.8-5.6-10.6-6.1 7.8-21 16.6-38.2 24-55 .2-.5 12.8 4.5 13 4 2.2-4.9 4.3-9.7 6.4-14.3.3-.8.7-1.5 1-2.2C478.8 487.4 483.9 215.6 488 207c35.3 163.4 70.8 279.6 160 331 4.9-5.3 7.5-20.8 18-35 1-1.3-16-13.6-15-15 13.9-19.5 22.5-49.7 41-73 .8-1 18.4 10.1 19 9 10.3-20.3 39.4-57.1 50-75 30-44.9 61.2-88 86-126 42.7-65.5 74.2-127.3 113.1-198.5 6.2 5.5 23.8 63.4 91.9 162.5 58.6 97.2 109 153.3 148 216 3.5 6.3 7 12.5 10.3 18.5.6 1 17.1-7.6 17.7-6.5 15.2 27.8 29.4 46.2 40 69 .6 1.3-11.6 16.7-11 18 6 13.1 11.4 25.2 16.4 36.5C1392.2 484.3 1428.8 197 1433 207c3.6 8.5 15.6 271.8-135.8 387.8 2.1 5.2 4.1 10.2 5.9 14.8.2.5 15.7-6.1 15.9-5.6 6.4 16.3 17.1 41.2 22 54-5.9 5.2-13.9 8.4-18 10 .7 7.4 10.3 19.7 12 23 152.2-79.9 168-323.1 176-339 3.7 104.4-14.8 199.6-47 269-31.4 67.7-76.2 110.7-109 128 2.1 9.9 4.8 21.1 7.7 33.5.3 1.3 20-3.8 20.3-2.5 4.1 18.3 12.1 57.3 7 92-.2 1.3-16.8 2.7-17 4-2 14.3-1.6 29.2-2 39 27 7.7 160.4-4.7 283-274-8.7 38.1-88.6 360.5-289 338-1.5 14.1-.3 19.8-2 29.9 4.2.1 7.5-.2 11 3.1-1.7 17.6-7.9 59.4-11 71-5.4-.6-9.1-1.6-13-2-4.4 27-7.2 53.1-8 58.8 82.8 30.5 130.4-96.2 162-134.1-7.5 32.8-42.9 205.8-173 183.9-1.4 5.3-3 11.2-4.8 17.3-.3 1 12.1 7.2 11.8 8.2-4.2 13.7-7.6 28.6-17 39-.7.8-11.3-2.8-12-2-3.6 4.3-3.8 10.6-5.9 14.3 7.7 7 26.2 37.8-24 32.4z"
		/>
		<radialGradient
			id="ch-topdown45001a"
			cx={960.5}
			cy={1049.5}
			r={372.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown45001a)"
			d="m912 1202-8 37h-29l-4 4s-78.7-19-149-92c3.7-7.2 7-17 7-17l9 7 8-27-19-16-17 14 6 8-13 13s-92-87.4-92-272c0-84.8 69.6-363 353-363 174.7 0 346 147.7 346 404 0 106.2-63.9 209.7-131 269-63.8 56.3-131 71-131 71l-7-2-24-1-10-36-95-1z"
			opacity={0.102}
		/>
		<radialGradient
			id="ch-topdown45001b"
			cx={958.97}
			cy={574.494}
			r={558.599}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown45001b)"
			d="m1279 1298-138 1s-3.8 190.6-32 246c11 15.5 20.7 43.6 0 71-.4 4.2 1 13 1 13s93.8-52.2 136-135c42.3-82.7 33-196 33-196zm-170 1h-41l-24 171-37-13-6 211 62-15-10-38s-17.9-16.4-17-39 23.6-43.7 45-42c8.4-16.1 27-81.9 27-130s1-105 1-105zm-198 155 5 213-61-14 12-39s15.2-15.2 15-36-10-41.2-44-45c-9.4-25.6-25.4-72.3-28-235 23.5-3.3 41 1 41 1l23 170 37-15zm-100 89s-29.3-69-31-247c-21.4-.9-143 0-143 0s-6.8 119 25 174 58.9 109 148 161c-.4-7.8-1-17-1-17s-13.5-13.8-13-33c.5-16 15-38 15-38z"
			opacity={0.502}
		/>
		<path
			d="m1228 1156 109 59-14 40-128-65 33-34zm-1-743-126 87s75.2 45 88 54c12.5-10.7 80-66 80-66l-42-75zm30 219 62-28 25 56-55 25-32-53zm66 157 60-9 7 95-56 1-11-87zm-6 203s55.8 18.6 56 20-9 73-9 73l-70-27 23-66zm-625 164-109 59 14 40 128-65-33-34zm1-743 126 87s-75.2 45-88 54c-12.5-10.7-80-66-80-66l42-75zm-30 219-62-28-25 56 55 25 32-53zm-66 157-60-9-7 95 56 1 11-87zm6 203s-55.8 18.6-56 20 9 73 9 73l70-27-23-66z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown45001c"
			x1={645.5671}
			x2={813.5671}
			y1={1497.8129}
			y2={1361.7689}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown45001c)"
			d="m693 413 126 87s-75.2 45-88 54c-12.5-10.7-80-66-80-66l42-75z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown45001d"
			x1={579.8568}
			x2={660.8568}
			y1={1298.4106}
			y2={1252.6206}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown45001d)" d="m660 631-58-24-23 50 51 28 30-54z" opacity={0.2} />
		<linearGradient
			id="ch-topdown45001e"
			x1={529.4195}
			x2={594.9355}
			y1={1094.9429}
			y2={1088.7029}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown45001e)"
			d="m595 789-55-10s-13.3 67.9-10 93c16.8 2.3 53 4 53 4l12-87z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown45001f"
			x1={546.8242}
			x2={623.8242}
			y1={874.4706}
			y2={888.4645}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown45001f)"
			d="m602 996-55 17s1.8 47.9 11 70c17.7-5 66-25 66-25l-22-62z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown45001g"
			x1={581.7235}
			x2={721.7235}
			y1={677.85}
			y2={757.834}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown45001g)" d="m690 1157-108 57 17 42 123-66-32-33z" opacity={0.2} />
		<linearGradient
			id="ch-topdown45001h"
			x1={1279.6206}
			x2={1109.6206}
			y1={1501.197}
			y2={1365.196}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown45001h)" d="m1100 501 128-88 42 75-83 66s-60.7-47.3-87-53z" opacity={0.2} />
		<linearGradient
			id="ch-topdown45001i"
			x1={1330.5179}
			x2={1245.5179}
			y1={1299.5144}
			y2={1231.5144}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown45001i)" d="m1258 631 61-26 24 53-54 27s-22.2-46.6-31-54z" opacity={0.2} />
		<linearGradient
			id="ch-topdown45001j"
			x1={1380.6014}
			x2={1314.3014}
			y1={1092.7272}
			y2={1089.171}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown45001j)"
			d="m1324 789 57-8s11.3 72.3 9 91c-18.6 2.3-56 4-56 4s-3.3-72.3-10-87z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown45001k"
			x1={1363.3894}
			x2={1286.3894}
			y1={872.3829}
			y2={895.4838}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown45001k)" d="m1316 994 56 19-12 73-65-28 21-64z" opacity={0.2} />
		<linearGradient
			id="ch-topdown45001l"
			x1={1320.53}
			x2={1179.53}
			y1={700.7904}
			y2={743.0924}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown45001l)" d="m1229 1156 108 59-13 41-128-66 33-34z" opacity={0.2} />
		<path
			d="M795 1275c-7.9 0-15 9.2-15 22s1.8 194.9 30 245c-8.2 16.2-24.2 49-1 75 2.7-27.6 7.1-66.5 30-64s31.7 23.7 27 57c9.4-7.3 17-21.8 17-35s-12.8-40.6-46-41c-6.7-25-27-62.4-27-237 0-13.3-7.1-22-15-22zm331 0c7.9 0 15 9.2 15 22s-1.8 194.9-30 245c8.2 16.2 24.2 49 1 75-2.7-27.6-7.1-66.5-30-64s-31.7 23.7-27 57c-9.4-7.3-17-21.8-17-35s12.8-40.6 46-41c6.7-25 27-62.4 27-237 0-13.3 7.1-22 15-22zm-217 179c-1.2-17.2-3-175-3-175h106l-3 177 36 13 23-170s-9-3.8-9-17 20-15 20-15l-34-26-170-2-31 25s16 1 16 17-10 17-10 17l25 170s16.1-8.5 34-14zm-244-134h29s2.8 86.6 22 119c-11 5.1-25 12-25 12s-11.6-21.9-17-49c-7.3-36.9-9-82-9-82zm591 0h-29s-2.8 86.6-22 119c11 5.1 25 12 25 12s11.6-21.9 17-49c7.3-36.9 9-82 9-82zM926 471V337l-185-2 44-62h354l41 63H995l-2 137-67-2z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-topdown45001m"
			x1={1180}
			x2={741}
			y1={1615.5}
			y2={1615.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown45001m)" d="m741 335 44-62h354l41 63s-402-5.3-439-1z" opacity={0.2} />
		<path
			d="m1044 1244 2 173 6-2 14-116s-8.9-4.8-7-18 18-16 18-16l-33-21zm-172 180 3-184-28 24s13.8 8.2 13 18-9 16-9 16l16 124 5 2z"
			opacity={0.2}
		/>
		<path fill="#dedede" d="m1007 1203-96-1-7 38h113l-10-37z" />
		<path
			fill="#f2f2f2"
			d="M871 1243s-100.2-29.7-149-93c2.1-5.7 6-15 6-15l12 7 8-29-20-17-20 18 6 8-10 12s-94-88.5-94-266c0-70.2 27.5-173.5 94-250 83.4-96 205.3-120 259-120 107.8 0 255 65.2 318 229 19.8 51.5 29 112.9 29 185-39.2 263.5-218.7 313.8-262 330 19.3 14 27 18 27 18s223.7-68.5 257-349c17.2-145-91.1-439-373-439-134 0-272.8 89.6-336.2 223-21.4 45-36.8 112.9-36.8 166 0 100.1 31.8 313.7 262 401 4.3-4.3 23-19 23-19z"
		/>
		<path fill="#f2f2f2" d="M905 1279h107l-16 587-75-1-16-586z" />
		<path
			d="M829 1226c-4.8-19.9-29.7-99.2-26-139s82.3-67.8 78-197c-12.9 41.1-38.9 106.1-90 147-15.2 12.2-19.2 35.9-20 58-1.8 52.1 23 112 23 112s28 18.3 35 19zM629 996c24.8-12.6 44.8-41.1 58-82 18.5-57.4 26.6-136 42-200 14.3-59.5 37.5-104.3 62-128-16.4 5.5-57.6 46.5-83 112-26.3 67.7-29.7 206.5-91 254 2.6 11.5 6.2 35 12 44zm316-498c8.6 22.7 41.3 142.2-47 193 32.2-52 54.3-86.4 15-191 18.1-1.4 23.4-.4 32-2zm201 58c-22.7 13.5-58.7 43.7-60 116-.7 38.8 6.6 100.3 1 164-4.8 54.8-21.7 113.2-32 140 22.8-33.9 46.8-58.4 58-126s-8.1-141 5-191 46-89 46-89-11.4-8.6-18-14zm70 64c14.5 39.6 108.3 302.2-78 480 88.5-63.7 171.9-179.7 126-408-7.9-19-32.4-60.5-48-72z"
			opacity={0.251}
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1278.4 1297.3c1.1 12.4 1.6 25.1 1.6 37.9 0 145-69.1 232-170.7 294.5m-41.6 21.2c-20.1 8.2-43.8 13.6-66.1 17.3m-86.1 0c-21.1-3.4-41.9-7.2-61-14.6m-41.6-20.8c-104.3-62.6-175.8-150.3-175.8-297.7 0-12.8.5-25.5 1.6-38"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m904.1 1238.2 8-35.7 96 1.1 9 36.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M921.1 1862.6h75l17-584.4-108 1.1 16 583.3z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1299.9 1268c3.1-5.9 6-11.9 8.8-18.1m15.3-42.2c1.9-6.5 3.8-13.5 5.7-21m10.6-49.8c3.2-16.9 6.5-35.9 9.9-57.7m10.8-71.3c1.4-9.5 2.8-19.4 4.2-29.6m-9.9-230c3.3 11.7 6 23.2 8.2 34.5m8.5 92.2v40M711 423.8c18.5-28.9 38.9-58.3 59.3-87.5m44.4-63.6c4.5-6.6 8.9-13.2 13.3-19.7 56.4-84.9 59.7-97.8 129.1-217.6 2.1-3.3 3.1-8.3 3.9-8.3s1.5 5.2 3 8c69.5 119.8 72.7 132.8 129.2 217.6 4.5 6.7 9 13.5 13.7 20.2m44.3 63.7c20.4 29 40.7 58.3 59.1 87m44.6 78.2c5.3 11 11.2 23.7 17.1 37m24.4 57.3c2 5 4.1 10 6 14.8m21.9 57.8c.9 2.8 7.7 22.3 7.7 22.3M667 500.8c-5.6 11-12 23.9-18.8 37.7m-26 54.3c-2.5 5.3-4.9 10.5-7.3 15.7m-24.7 56.2c-3.9 9.5-6.7 17.2-8.2 22.3m-15.9 61.5c-3.1 11-5.6 22.1-7.8 33.1m-8.9 92.1c0 2.4-.1 4.8-.1 7.2 0 5.6 1.7 35.1 1.7 35.1m5.2 62.6c1.4 10.3 2.8 20.2 4.2 29.6m10.5 70.1c3.5 22.3 6.9 41.8 10.1 59m10.7 49.8c1.8 7.2 3.6 13.9 5.4 20.2m15 41.8c2.9 6.6 6 12.9 9.3 19.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1074.2 1260.9c150.2-52.5 258.9-205 258.9-385 0-223.4-167.4-404.6-374-404.6-206.5 0-374 181.1-374 404.6 0 181.9 111 335.7 263.6 386.7 13-13.7 24.3-23.2 24.3-23.2l171 1.1c.1-.2 20.7 14.3 30.2 20.4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M871.1 1241.4s-8.1-2.2-14.7-4.6c-142.8-47.6-246.6-191.5-246.6-361.6 0-208.9 156.6-378.3 349.8-378.3 193.2 0 349.8 169.4 349.8 378.3 0 168.3-101.6 311-242.2 360.1-8.3 2.2-21.1 7.3-21.1 7.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M850.6 1297.5c6 45.3 16.7 125.7 22.5 170 12.6-5.8 24.6-10.1 36.2-13.2m99.5 2.1c12.7 3.7 24.8 8.4 36.3 13.2 8.3-70.5 16.8-134.4 22-172.4m-23-55.8v223.9m-170-226V1461"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M832.1 1228.4c-10.8-27.1-41-134.4-24-159 17-24.6 86.1-77.5 75-201.2-12.9 46.7-4.2 67.8-59 136.3-19.1 23.9-43.3 32.5-50 66-8.5 42.6 11 119.7 21 138.4m-99-45.3 14-22.7m-5-7.6 11-11.9m13 12.9-10 16.2"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m709.1 1113.8 29 28.1 9-28.1-20-17.3-18 17.3z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M628.1 997C761 894.3 654.9 682.7 833.4 553.2c1.9-1.4 4.2-3.4 5.7-5.1-197.8 112.6-119.2 315-223 405.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M911.1 501.6c13.8 12.2 66.8 152.4-48 205.5 1.7-.1 4.3-.7 5.5-1.1 114.8-36.7 97.2-170 73.5-210.9m203 61.6c-86.7 60.7-48.1 149.2-56.5 271-3.6 52.3-18.4 110.7-51.5 175.7 165.2-208.8 2.8-296.5 127-432.7m52 48.8c10.1 34 122.2 332.4-105 496.5 79.5-32.4 211.6-165.1 150-432.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M731.1 952.6c7-9.1 40.2-32.6 55-9.7 14.8 22.9-21.6 41.5-25 60.6-3.4 19 1.7 43.9-23 40-19.3-3-8-20.7-12-40-2.1-10.6-12.6-28.1 5-50.9z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M863.1 759c12.5 3.3 33.2 18.7 25 35.7-8.2 17-34.7 8.7-43 15.1-8.3 6.5-13.3 27.1-35 24.9-21.7-2.2-21.8-20.1-30-27-8.2-7-32.1-4.9-20-28.1 9.6-18.5 43.9-8.5 55-10.8 11.1-2.4 35.5-13.1 48-9.8z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M983.1 957c4.1 14.5 35.9 23.5 31 47.6-4.9 24-15.9 27.9-34 16.2s-21.5-42.2-31-53c-9.5-10.9-20.6-32-4-45.4 16.6-13.5 27.9-.7 38 34.6z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1071.1 1198.2c-5.1-12.4-1-31.8 14-40s27.3 1.4 37-3.2 27.2-20.3 36-5.4c8.9 14.9-23.3 38-42 46.5-18.7 8.4-39.9 14.5-45 2.1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M834.1 1761.7c3.6-30.9 25.3-126.5 30-144.9 4.7-18.4 10.9-56.3-20-62.7-30.9-6.5-52.3 43.7-10 207.6z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M867.1 1612.4c16-4.9 33.5-67.4-19-77.9-50-9.9-64.3 56.1-41 79 1.1 1.2 2.9 2.5 2.9 2.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M838.1 1534.6c-12.2-32.2-28-69.7-28-225 0-15.6-.1-34.6-15-34.6s-15 12.8-15 42.2c0 29.4.3 157.9 32 228.2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M665 1319c-.2 32.7 6.5 101.9 26 132 15.1-8.6 24-12 24-12s-20-43.5-20-120c-12.5-.1-18.5 0-30 0zm591 0c.2 32.7-6.5 101.9-26 132-15.1-8.6-24-12-24-12s20-43.5 20-120h30z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M812.1 1297.7h33s15 2.1 15-15.1c0-17.3-17.3-15.2-24-15.2h-212s-18-2.2-18 18.4c0 12.2 12.1 10.8 17 10.8 3.6 0 103.3.6 156.4.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M601.1 994.8c-9.8 3.4-46.9 14.3-53.1 16.2-.5 12.5.3 15.6 3 38 1.5 12.4 4 25.1 7 35 4.3-1.6 13.6-4.6 16-5.4 7-2.3 49-19.6 49-19.6m67 98c-26.7 14.8-63 34.5-107 57 5.6 19.3 15 41 15 41s83.2-39.3 126-65M584.5 875.8c-14.5-.5-28.5-1.1-36.5-1.8-14.1-.9-17-1-17-1s-1.1-53.8 8-94c10.9 1.9 56 9 56 9m35-104-52-25s18.7-43.7 25-55c26.3 12.2 59 27 59 27m72-78c-34.3-25.5-83-64-83-64s32.4-60 43-76c34 23.1 97.3 67.1 126 86"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M927 473V338" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M994 473V338" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1319.8 995c9.8 3.4 46.9 14.3 53.2 16.2.5 12.5-.3 15.6-3 38-1.5 12.4-4 25.2-7 35-4.3-1.6-13.6-4.6-16-5.4-7-2.3-51.1-20.6-51.1-20.6m-65.1 99c26.7 14.8 63 34.6 107.1 57-5.6 19.4-15 41-15 41s-83.3-39.3-126.1-65M1336.4 876c14.5-.5 28.5-1.1 36.5-1.8 14.1-.9 17-1 17-1s1.2-53.9-8-94c-10.9 1.9-56 9-56 9m-35-104.1 52-25s-18.7-43.7-25-55c-26.3 12.2-59 27-59 27m-72.1-78.1c34.4-25.5 83.1-64 83.1-64s-32.4-60-43-76c-34 23.1-97.4 67.1-126.1 86"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m740 336 44-63h354l44 63H740z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M579.1 1138.7c12.5-3.6 35.9-17.4 46 9.7 10.1 27.2-22.2 34.6-34 37.9-11.8 3.3-130.4 47-182-203.4 40.2 75.4 85.2 183.4 170 155.8zm-23.9-161.8c9.2-1.3 32.8-7.2 29-37.9-3.9-30.6-19.8-30.5-32-26s-165 7.2-290-292.6c20.3 121.7 123.2 380.3 293 356.5zm109.9-428.8c-38.6-23.1-122.1-48.5-177-352.6-9 228.7 73.7 364.9 153 408.9 16.5 8.8 27.8 2.2 34-14.1 5.2-13.5 2.9-33.3-10-42.2zm-79 141c-38.6-23.1-122.1-48.5-177-352.6-9 228.7 73.7 364.9 153 408.9 16.5 8.8 27.8 2.2 34-14.1 5.2-13.5 2.9-33.3-10-42.2z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1342.6 1139.5c-12.5-3.5-35.9-17.4-46 9.7-10.1 27.2 22.2 34.6 34 37.9 11.8 3.3 130.5 47 182.2-203.4-40.2 75.3-85.2 183.4-170.2 155.8zm24-161.9c-9.3-1.3-32.9-7.2-29-37.9 3.9-30.6 19.9-30.5 32-26 12.2 4.5 165.1 7.2 290.3-292.7-20.4 121.7-123.4 380.4-293.3 356.6zm-110.1-428.9c38.7-23.1 122.2-48.5 177.2-352.8 9 228.8-73.8 365-153.1 409-16.5 8.8-27.9 2.2-34-14.1-5.2-13.4-2.9-33.3 9.9-42.1zm79.1 141c38.7-23.1 122.2-48.5 177.2-352.8 9 228.8-73.8 365-153.1 409-16.5 8.8-27.9 2.2-34-14.1-5.2-13.4-2.9-33.2 9.9-42.1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1085.2 1762.5c-3.6-30.9-25.2-126.5-30-145-4.7-18.4-10.9-56.3 20-62.8 30.9-6.3 52.3 43.9 10 207.8z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1052.3 1613.2c-16-4.9-33.5-67.4 19-77.9 49.9-9.9 64.2 56.2 41 79"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1081.2 1535.3c12.2-32.2 28-69.7 28-225 0-15.6.1-34.6 15-34.6s15 12.8 15 42.2c0 29.4-.3 158-32 228.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1107.2 1298.4h-33s-15 2.1-15-15.1c0-17.3 17.3-15.2 24-15.2H1295s18-2.2 18 18.4c0 12.2-12.1 10.8-17 10.8-3.6 0-103.3.6-156.3.9"
			/>
		</g>
		<g>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="662.3,1489.6 675.9,1486.6 662.3,1484.3 648.8,1489.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="932.3,1470.7 945.9,1467.6 932.3,1465.3 918.8,1470.7"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1168.6,1676.8 1181.8,1681.6 1171.5,1672.3 1157.3,1669.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1281.3,1353.2 1291.4,1343.8 1278.5,1348.6 1269.5,1360"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1260.3,348.9 1270.5,339.5 1257.6,344.3 1248.6,355.7"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="864.2,863.1 874.5,853.7 861.5,858.5 852.5,869.9"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1038,766.1 1051.8,768.2 1039.9,761.1 1025.4,761.2"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="402.6,738.8 412.9,729.3 400,734.2 391,745.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="507.2,1353.2 517.5,1343.8 504.6,1348.6 495.5,1360"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1623.1,833.2 1633.3,823.8 1620.4,828.6 1611.4,840"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1738.7,1127.4 1752.5,1130.2 1741.7,1121.5 1726.7,1121.3"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="701.1,167.5 711.3,158 698.4,162.9 689.4,174.2"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="722.6,596.9 732.9,587.5 720,592.3 711,603.7"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="928.4,613.1 941.7,617.5 931.2,608.6 916.9,606.1"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1168.8,1041.8 1181.9,1046.3 1171.4,1037.3 1157.1,1034.8"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1535.5,1301.8 1548.8,1306.2 1538.3,1297.2 1523.9,1294.8"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="881.3,1560.3 894.5,1564.8 884,1555.8 869.7,1553.3"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="538.8,1207 552,1211.4 541.5,1202.4 527.2,1200"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="426.2,1106.4 439.3,1101.5 425.5,1101.1 412.8,1108.2"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="635.6,324.9 648.8,329.4 638.3,320.4 624,318"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="670.2,505.6 683.4,510 672.9,501 658.6,498.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1285.4,133.9 1298.6,138.4 1288.1,129.4 1273.8,127"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="543.2,1705.6 556.4,1710 545.9,1701 531.6,1698.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1354.8,1571.7 1368,1576.2 1357.5,1567.2 1343.3,1564.8"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1676.1,1636.6 1689.4,1641 1678.9,1632 1664.6,1629.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1260.6,550 1273.8,554.4 1263.3,545.4 1249,543"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1552.1,260.5 1565.3,264.9 1554.9,255.9 1540.5,253.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1492.5,535.4 1491.4,521.4 1487.3,534.6 1490.6,548.7"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1172.1,470.3 1171.1,456.4 1166.9,469.6 1170.3,483.7"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="761.7,1760 760.6,1746.1 756.4,1759.2 759.8,1773.4"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="169.2,977.9 168.1,964 164,977.1 167.3,991.2"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="889.5,42.8 888.4,28.8 884.2,42 887.6,56.1"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1635.9,612.1 1634.9,598.2 1630.8,611.4 1634,625.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1425.4,1126.4 1424.4,1112.5 1420.3,1125.6 1423.5,1139.7"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1299.1,1807.2 1298.1,1793.3 1293.9,1806.5 1297.3,1820.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="224.2,1675.8 223.2,1661.9 219,1675.1 222.3,1689.2"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="365.7,458.9 379.6,457.7 366.4,453.6 352.3,457.1"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="498.7,573.6 497.4,559.7 493.4,572.9 497,587"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="845.7,320.6 859.6,319.4 846.4,315.3 832.3,318.8"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1127.3,154.6 1141.1,153.4 1127.9,149.3 1113.9,152.8"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1358.1,637.8 1372,636.6 1358.8,632.5 1344.8,636"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1419.9,908.2 1433.8,907 1420.6,903 1406.5,906.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="929,1097.9 942.9,1096.7 929.7,1092.6 915.6,1096.1"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="721.7,1189.2 735.6,1188 722.4,1184 708.3,1187.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="234.7,1433.1 248.6,1431.9 235.4,1427.9 221.3,1431.4"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="356.3,1605.8 362.4,1605.7 356.7,1603.3 349.5,1603.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1044.9,1478.7 1050.9,1478.6 1045.3,1476.3 1038.1,1476.4"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="967.5,1630.6 971.4,1635.3 969.6,1629.3 965,1623.9"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1373.9,1206.9 1380,1206.8 1374.3,1204.5 1367.1,1204.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1248.9,1478.7 1255,1478.6 1249.3,1476.3 1242.1,1476.4"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1469.1,1701.3 1472.3,1706.5 1471.4,1700.4 1467.8,1694.3"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1568,1171.7 1574.1,1171.6 1568.3,1169.3 1561.3,1169.4"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1611.8,993.6 1617.8,993.6 1612.1,991.2 1604.9,991.3"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="722.3,1019.4 728.4,1019.3 722.6,1017 715.5,1017.1"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="812.7,746.8 818.8,746.7 813,744.3 805.9,744.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="722.3,422.7 728.4,422.7 722.6,420.3 715.5,420.4"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="210.5,330.6 216.5,330.5 210.8,328.2 203.7,328.3"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1505.3,369.8 1511.4,369.7 1505.6,367.4 1498.5,367.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1428.5,189.8 1432.8,185.5 1427.1,187.8 1422.1,192.9"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1537.6,726.3 1540.5,721 1535.8,724.8 1532.3,731.1"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1363.8,992.7 1366.6,987.3 1361.8,991.2 1358.4,997.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1034.8,1325 1037.6,1319.7 1032.8,1323.5 1029.4,1329.8"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="418.1,1817.9 421.1,1812.5 416.2,1816.4 412.8,1822.7"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="852.9,1808.7 858.9,1809.7 853.7,1806.4 846.6,1805.3"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="825.1,1331.1 831.1,1332 825.8,1328.7 818.8,1327.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="941.6,114.7 947.6,115.7 942.3,112.4 935.2,111.3"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={3}
				points="1820.4,992.4 1826.4,993.4 1821.2,990.1 1814.1,989"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="406.5,224.4 420.4,223.2 407.2,219.1 393.2,222.6"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="662.1,714.2 676,713 662.8,709 648.7,712.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1259.3,711.6 1273.3,710.4 1260,706.3 1245.9,709.8"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="1089.8,1855.2 1103.6,1854 1090.4,1849.9 1076.3,1853.4"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="107.9,1287.2 121.8,1286 108.6,1282 94.5,1285.5"
			/>
			<polygon
				fill="#fff"
				stroke="#030303"
				strokeMiterlimit={10}
				strokeWidth={5}
				points="189.8,600.8 203.7,599.6 190.5,595.6 176.4,599.1"
			/>
		</g>
		<radialGradient
			id="ch-topdown45001n"
			cx={960.7999}
			cy={1026.4}
			r={867.6}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a1a1a" stopOpacity={0.8} />
			<stop offset={1} stopColor="gray" stopOpacity={0} />
		</radialGradient>
		<circle cx={960.8} cy={893.6} r={867.6} fill="url(#ch-topdown45001n)" />
	</svg>
);

export default Component;

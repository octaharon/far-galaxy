import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#e6e6e6"
			d="M975.3 1596.9c106.3 0 160.2-55.6 218.4-129.5 43.2-54.9 71.2-106.3 71.2-106.3l2 184 300-1V339l-302-1v381l-15-11s-16.8-147.1-76-213c-54.3-60.5-164.5-63.9-205-64h-18.1c-40.4.1-150.6 3.5-205 64-59.2 65.9-76 213-76 213l-15 11V338l-302 1v1205l300 1 2-184s28 51.5 71.2 106.3c58.3 74 101.1 129.5 218.4 129.5 5.5.1 25.7.1 30.9.1z"
		/>
		<linearGradient
			id="ch-topdown25000a"
			x1={959.9731}
			x2={959.9731}
			y1={1011}
			y2={134}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#e6e6e6" />
			<stop offset={0.598} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown25000a)"
			d="M852 720c55.6 57.7 173.4 45.6 216 0 1.8 12.4 13.5 71.4 28.8 151.9 21.6 113.7 81.9 324.8 85.2 386.1 7.7 117.2 6.2 214.1-7 234-44.5 56.6-109.6 105-215 105s-154-25.8-218-109c-7.6-75.6-14.6-188.7-1-254 9-53.3 82.7-348.6 111-514z"
			opacity={0.102}
		/>
		<path
			fill="#b3b3b3"
			d="M959 464c25.5 0 114.5.6 129 77 3.3 90.3 2.4 197-127 197-85.7 0-114.3-48.8-124-92s-15.6-134 21-154 75.5-28 101-28z"
		/>
		<path
			fill="#ccc"
			d="M1177 1475c-4.9-19.5-43.9-106.2-74-123-46.7-3.7-286-2-286-2s-64.1 66.9-73 128c24.5-23.3 70.3-64 92-64s224.7 2 251 2 84.9 56.4 90 59z"
		/>
		<path
			fill="#bfbfbf"
			d="M1250 707c-37.5-298.8-181.1-259.2-290-277-250.5 2.2-266.4 132-290 278 57.5-28.2 72.9-116.9 125.2-183.6C827.1 483.7 867.1 446 961 446c102.6 0 147.3 47.7 180 102 39.4 65.5 58.9 140.9 109 159z"
		/>
		<path
			fill="#bfbfbf"
			d="M1567 1544v-131h-301l1 133 300-2zm-912-130-2 131-300-1v-130h302zm1-1076-1 218-302 1V338h303zm610 1 302 1-1 216-301-1V339z"
		/>
		<path
			fill="#ccc"
			d="m1441 1411-175 2v-54s69-98.3 69-268c0-26.3 5-324.4-70-373-.5-76.1 2-162 2-162l125 1-2 243 51 82v529zm-962 0 174.9 2v-54s-69-98.3-69-268c0-26.3-5-324.4 70-373 .5-76.1-2-162-2-162L528 557l-1 242-50 82 2 530z"
		/>
		<path
			d="m353 1413 125-1V881l49-83 1-241H353v856zm1214 0-126-1-3-532-48-81 1-244 176 2v856z"
			className="factionColorSecondary"
		/>
		<path
			d="M741 1484c-20.3-152.3-3.1-270.2 40-421 31.9-148.5 63.3-294.9 71-341-29.8-31.2-70.3-178.7-16-240-75.3 61.3-109.1 194.8-164 227s-78.9 80.7-85 289c-1.1 38-7 195.6 25 277s101.9 181.6 129 209zm437.9 0c20.3-152.3 3.1-270.2-40-421-31.8-148.5-63.3-294.9-71-341 29.7-31.2 70.3-178.7 16-240 75.3 61.3 109 194.8 163.9 227s78.8 80.7 84.9 289c1.1 38 7 195.6-25 277s-101.7 181.6-128.8 209z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-topdown25000b"
			cx={1012.813}
			cy={145.319}
			r={729.272}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0.2} />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</radialGradient>
		<path
			fill="url(#ch-topdown25000b)"
			d="M853 722c-22.3-14.2-70.4-154.6-25-231-66.5 54.4-91.4 153.7-123 189s-61.5 35.3-80 75c-19.7 29-64.8 241.8-24 475 21.4 92.7 91 197.9 139 255-4-56.2-7.2-109.7-8-144-8-101.9 96.2-444.7 121-619z"
		/>
		<radialGradient
			id="ch-topdown25000c"
			cx={893.572}
			cy={60.103}
			r={727.829}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0.2} />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</radialGradient>
		<path
			fill="url(#ch-topdown25000c)"
			d="M1068 722c22.2-14.2 70.3-154.6 24.9-231 66.3 54.4 91.2 153.7 122.8 189 31.6 35.3 61.4 35.3 79.8 75 19.6 29 64.7 241.8 23.9 475-21.4 92.7-90.8 197.9-138.7 255 4-56.2 7.2-109.7 8-144 8.1-101.9-95.9-444.7-120.7-619z"
		/>
		<path fill="#999" d="M960 1362c21 0 38 8.7 38 19.5s-17 19.5-38 19.5-38-8.7-38-19.5 17-19.5 38-19.5z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M975.3 1596.9c106.3 0 160.2-55.6 218.4-129.5 43.2-54.9 71.2-106.3 71.2-106.3l2 184 300-1V339l-302-1v381l-15-11s-16.8-147.1-76-213c-54.3-60.5-164.5-63.9-205-64h-18.1c-40.4.1-150.6 3.5-205 64-59.2 65.9-76 213-76 213l-15 11V338l-302 1v1205l300 1 2-184s28 51.5 71.2 106.3c58.3 74 101.1 129.5 218.4 129.5 5.5.1 25.7.1 30.9.1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1566 1413h-301" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1567 556h-301" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1262 1365c14-24.6 73-103.6 73-278s-9.8-328.3-71-369" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1102 1350-16 64" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M959.8 757.9c-72.8 0-150-19.1-150-202 0-45.2 26.7-109.2 145-110.9h9.4c119 1.5 145.8 65.6 145.8 111 0 182.9-77.2 202-150 202"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M959.9 737.8c-69.1-1.5-132-22.2-130.1-177.5.5-43.5 16.3-91.7 124.1-96.9 2-.1 10.7-.1 12.9.1 107 5.4 122.7 53.4 123.2 96.8 1.9 155.4-61 176-130.1 177.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M354 1412.8h300.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M477 1413V880.7c18.2-28.8 32.6-52.1 50-81.7V556m863 0v243c17.4 29.6 31.8 53 50 81.7V1413"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M353 556h300.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M657.9 1364.8c-14-24.6-73-103.6-73-277.9s9.8-328.2 71-368.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m817.9 1349.8 16 64" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M741.9 1479.8s11.1-62.4 75-131c66.6.2 219.5.4 286.1.2 63.9 68.6 75 131 75 131"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1181 1483s-46-53.5-96-69c-54.8-.3-195.3-.5-250.2-.2-50 15.5-96 69-96 69"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M960 1362c21 0 38 8.7 38 19.5s-17 19.5-38 19.5-38-8.7-38-19.5 17-19.5 38-19.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1179 1482c30.2-289.1-28.8-290.8-111-760m-216.2-.1c-82.2 469.1-141.1 470.8-111 759.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1253 710c-80.2-34.6-87.7-197.6-207.4-250.4m-172.6 1c-119.5 52.9-127 215.2-207 249.8"
		/>
		<g id="Animation">
			<path
				fill="#f2f2f2"
				d="M1232 1896h189l33-1122h18l38-98-3-424-91-235-180-1-92 235v116s-113.1-13-184-13-306 18-306 18-33.6-60-132-60-127.6 100.3-135 127-14 158-14 250c13.4-.1 35 0 35 0v20h78v-18h37v20l130-2v-20h28v-30s214.7 42 278 42 162.1-24.4 184-29c10.8 30.4 43 102 43 102h13l33 1122z"
			/>
			<linearGradient
				id="ch-topdown25000d"
				x1={1510}
				x2={1144}
				y1={139}
				y2={139}
				gradientTransform="matrix(1 0 0 1 0 586)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a3a3a3" />
				<stop offset={0.496} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#a3a3a3" />
			</linearGradient>
			<path fill="url(#ch-topdown25000d)" d="m1144 677 366-2-37 98-286 2-43-98z" opacity={0.8} />
			<linearGradient
				id="ch-topdown25000e"
				x1={1511}
				x2={1143}
				y1={-122.5}
				y2={-122.5}
				gradientTransform="matrix(1 0 0 1 0 586)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a3a3a3" />
				<stop offset={0.49} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#a3a3a3" />
			</linearGradient>
			<path
				fill="url(#ch-topdown25000e)"
				d="M1144 674c24 .7 367 1 367 1l-5-422-362-1-1 114s219.2 40.1 332 86c18.9 37.2 30.5 61.9 2 110-57 22.8-245.2 91-333 112z"
				opacity={0.8}
			/>
			<linearGradient
				id="ch-topdown25000f"
				x1={1326.6646}
				x2={1145.6646}
				y1={-416.2622}
				y2={-485.9962}
				gradientTransform="matrix(1 0 0 1 0 586)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.2} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path fill="url(#ch-topdown25000f)" d="M1327 252V18l-92-2-89 236h181z" opacity={0.502} />
			<linearGradient
				id="ch-topdown25000g"
				x1={1508.3214}
				x2={1327.3214}
				y1={-484.056}
				y2={-418.1779}
				gradientTransform="matrix(1 0 0 1 0 586)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.8} stopColor="#e6e6e6" />
			</linearGradient>
			<path fill="url(#ch-topdown25000g)" d="M1327 252V18l92-2 89 236h-181z" opacity={0.4} />
			<radialGradient
				id="ch-topdown25000h"
				cx={961.7266}
				cy={1911.531}
				r={534.3393}
				gradientTransform="matrix(1 0 0 -0.3631 0 1216.4751)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.2991} stopColor="#f2f2f2" stopOpacity={0} />
				<stop offset={1} stopColor="#000" stopOpacity={0.5} />
			</radialGradient>
			<path
				fill="url(#ch-topdown25000h)"
				d="M960 702c152-8.5 444-95.3 516-139 7.4-11.4 34.5-47.3 2-112-9.1.1-278.4-103.5-518-96-91.6 2.9-224.3 7.8-302 17-7.7 1 12.5 41.3 1 49-57 60.2-52.5 89.1-52 98 2.6 48.1 73.9 89 74 138 26.3 16.3 202.7 34.2 279 45z"
			/>
			<path
				fill="#d9d9d9"
				d="M738 445c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70zm-45 70.5c0 25.1 20.4 45.5 45.5 45.5s45.5-20.4 45.5-45.5-20.4-45.5-45.5-45.5-45.5 20.4-45.5 45.5zM1274 808l107 1-27 62-54 1-26-64z"
			/>
			<path fill="red" d="M486 709v-18h-79l1 19 78-1z" />
			<path fill="red" d="M523 689h132l-1 19-130 2-1-21z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1232 1896h189l33-1122h18l38-98-3-424-91-235-180-1-92 235v116s-113.1-13-184-13-306 18-306 18-33.6-60-132-60-127.6 100.3-135 127-14 158-14 250c13.4-.1 35 0 35 0v20h78v-18h37v20l130-2v-20h28v-30s214.7 42 278 42 162.1-24.4 184-29c10.8 30.4 43 102 43 102h13l33 1122z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 701V354" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1454 774h-72.2c.3 14.1.2 32 .2 32l-28 63-54 1-27-64-1-32h-85"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1379 808h-104" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1267 774h116" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1509 676h-364" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1506 253-362-1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1436 63-217-1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1140 673c47.3-6.4 261.6-75.6 337-110 12.2-23.5 18-41.7 18-54 0-12.4-9.4-50.9-18-56s-236.6-74.9-339-87"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M408 689h245" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M737.5 444c38.9 0 70.5 31.6 70.5 70.5S776.4 585 737.5 585 667 553.4 667 514.5s31.6-70.5 70.5-70.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M738 470c24.8 0 45 20.1 45 45 0 24.8-20.2 45-45 45s-45-20.2-45-45c0-24.9 20.2-45 45-45z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M653 372c8.3 15 7 38.8 7 46s-54 46.5-54 98 75 88.6 75 147"
			/>
		</g>
	</svg>
);

export default Component;

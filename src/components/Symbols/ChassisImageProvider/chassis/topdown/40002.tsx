import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#999"
			d="M1309 1096.8c-2.7-20.9-14.6-43.9-22.9-47.2-8.3-3.2 3.9-96.7 3.9-96.7l-153 2-71 38s77.9 100.8 78.4 103.8c.5 3.1 34.8 20.3 35.9 20.1 1.1-.2 69.8 0 69.8 0s61.7.9 58.9-20z"
		/>
		<path
			fill="#ccc"
			d="M1217.2 1139c38.2 0 98.1-11 97.8-34.1-.3-23.2-9.2-22.7-92.8-5-50.7 11.9-74.2 1.2-74.2 1.2l-4.6 13.9-1 21.1c0-.2 36.6 2.9 74.8 2.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1307.3 1087.7c-4.4-18-14.1-35.2-21.2-38-8.3-3.2 3.9-96.7 3.9-96.7l-153 2-71 38s77.9 100.8 78.4 103.8c.2.9 3 2.9 7.1 5.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1217.2 1139c38.2 0 98.1-11 97.8-34.1-.3-23.2-9.2-22.7-92.8-5-50.7 11.9-73.2.2-73.2.2l-5.6 14.9s-6.1 16.3-1 21.1c5.1 4.7 36.6 2.9 74.8 2.9z"
		/>
		<path
			fill="#999"
			d="M612 1096.6c2.8-20.9 14.6-43.9 23-47.1 8.3-3.2-3.9-96.5-3.9-96.5l153.2 2 71.1 37.9s-78 100.6-78.5 103.6-34.8 20.2-36 20c-1.1-.2-70 0-70 0s-61.6 1-58.9-19.9z"
		/>
		<path
			fill="#ccc"
			d="M704 1138.7c-38.3 0-98.3-10.9-97.9-34.1.3-23.1 9.2-22.6 92.9-5 50.8 11.9 74.3 1.2 74.3 1.2l4.6 13.9 1 21s-36.7 3-74.9 3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M613.8 1087.5c4.4-18 14.1-35.2 21.3-37.9 8.3-3.2-3.9-96.5-3.9-96.5l153.2 2 71.1 37.9s-78 100.6-78.5 103.6c-.2.9-3 2.9-7.1 5.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M704 1138.7c-38.3 0-98.3-10.9-97.9-34.1.3-23.1 9.2-22.6 92.9-5 50.8 11.9 73.3.2 73.3.2l5.6 14.8s6.1 16.2 1 21c-5.1 4.9-36.7 3.1-74.9 3.1z"
		/>
		<path
			fill="#ccc"
			d="M1179.1 143.9s21-43.1 24.8-60.9c8.6 22.5 23.7 80.6 28 80.4l21.7-79.4s57.6 100.5 38.5 108c-12.1-2.1-38-8.6-81 10s-32-58.1-32-58.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1179.1 143.9s21-43.1 24.8-60.9c8.6 22.5 23.7 80.6 28 80.4l21.7-79.4s57.6 100.5 38.5 108c-12.1-2.1-38-8.6-81 10s-32-58.1-32-58.1z"
		/>
		<path
			fill="#999"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1259.9 186.4c21.4-3.8 43 5.3 47.1 35 3 21.5 2.2 63.1-41 74-103 25.7-65.1-121.9-65.1-121.9s22.8 18 59 12.9z"
		/>
		<path fill="#e6e6e6" stroke="#1a1a1a" strokeWidth={10} d="m1232.9 226.4 60.1-1 3 91-62.1 1-1-91z" />
		<linearGradient
			id="ch-topdown40002a"
			x1={1117.5555}
			x2={1269.7015}
			y1={1670.6576}
			y2={1615.2806}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#969696" />
			<stop offset={0.352} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002a)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1161.8 121.5c75.6 8.3 153.1 145.8 80.1 268.9-73 123-108.1-120.9-108.1-120.9s-47.6-156.4 28-148z"
		/>
		<path
			fill="#ccc"
			d="M739.7 149.4s-21-43.1-24.8-60.9c-8.6 22.5-23.6 80.6-27.9 80.5l-21.6-79.5S614.7 175.6 622 196c10.9-9 30.1-11 54.8-9.6 9.3.6 19.9-2.2 31.7 2.9 42.9 18.7 31.2-39.9 31.2-39.9z"
		/>
		<path
			fill="#999"
			d="M659 187c-21.4-3.8-42.9 5.3-47 35-3 21.5-2.2 63.2 41 74 102.9 25.8 65-122 65-122s-22.9 18-59 13z"
		/>
		<path fill="#e6e6e6" d="m686 227-60-1-3 91 62 1 1-91z" />
		<linearGradient
			id="ch-topdown40002b"
			x1={649.7985}
			x2={801.7625}
			y1={1679.5402}
			y2={1624.2292}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#969696" />
			<stop offset={0.352} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002b)"
			d="M757 122c-75.5 8.4-152.9 145.9-80 269s108-121 108-121 47.5-156.4-28-148z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M674 187.9c-4.7.1-9.7-.1-15-.9-21.4-3.8-42.9 5.3-47 35-2.1 15.1-2.3 40.1 12.6 57.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M657.4 226.5c-15.4-.2-31.4-.5-31.4-.5l-3 91s13.3.2 27.5.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M757 122c-75.5 8.4-152.9 145.9-80 269s108-121 108-121 47.5-156.4-28-148z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M730.8 130.2c-6.2-13.8-13.7-31.7-15.8-41.6-8.6 22.5-23.6 80.6-27.9 80.5l-21.6-79.5s-50.7 86.1-43.4 106.5c10.9-9 30.1-11 54.8-9.6"
		/>
		<linearGradient
			id="ch-topdown40002c"
			x1={672.0032}
			x2={1248.0148}
			y1={1331.5649}
			y2={1331.5649}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.251} stopColor="#bfbfbf" />
			<stop offset={0.495} stopColor="#bfbfbf" />
			<stop offset={0.754} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002c)"
			d="M811.4 1125.4c-21.7-34.4-73.4-66.1-98.4-110-25-43.9-16-235.2-16-293.4 0-29.8-.6-76.5-7-102-6.1-24.2-16.6-46.6-17-64-.8-35.7-9.6-120.7 44.5-235.9 29.1-61.9 17-141.1 42.5-185.1S811.4 29 962.2 29s171 42.4 200.8 102 11.3 118.7 43.9 203.1c32.6 84.4 41.1 113.5 41.1 232.9.7 26.8-23 21.6-23 141s7.5 236.7-17 316c-30 46.3-64 65.5-97 81-14.2 6.6-31.8 16.5-41 23-30.5 21.8-236.9 31.8-258.6-2.6z"
		/>
		<radialGradient
			id="ch-topdown40002d"
			cx={963.627}
			cy={1638.655}
			r={375.7}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.802} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown40002d)"
			d="M738 226c3.2-28.7-3.7-203.8 223-201 50.8.6 123.5 4.4 161 46 50.5 56 58 154 58 154s-136 43.8-176 100c-10.6-42.4-33.7-87.8-43-88s-29 37.3-43 84c-9.2-10.5-102-82.7-180-95z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002e"
			x1={647.547}
			x2={895.547}
			y1={1409.577}
			y2={1409.577}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002e)"
			d="M753 934c-4.6-22.1-17.7-96.7-58-104-.6-41.3 1-87 1-87l22-1v-6l51 1s-.2-50.3 11-59c-28.1-.1-64-1-64-1l1-73 121 1s-58.2 186.6-85 329zm104-387-138 1-4-77s46 3.5 57 2 68.3-.9 86-55c-52.2-2.1-143 2-143 2v-13l-30-1s21.4-76.2 38-106 17.5-117.4 20-127 31.4-79.7 42-86 148 182 148 182l-76 278z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown40002f"
			x1={687.818}
			x2={799.818}
			y1={1351}
			y2={1351}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002f)"
			d="M715 473s93.7-.2 109-13c-.5 38.5 3 90 3 90l-111-3-1-74zm1 132 93-1-31 74-62-1v-72z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown40002g"
			x1={1122.5179}
			x2={1234.5179}
			y1={1351}
			y2={1351}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.8} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002g)"
			d="M1206 473s-93.7-.2-109-13c.6 38.5-3 90-3 90l111-3 1-74zm-1 132-93-1 31 74 62-1v-72z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown40002h"
			x1={1030.661}
			x2={1278.661}
			y1={1409.4971}
			y2={1409.4971}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.8} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002h)"
			d="M1168 934c4.6-22.1 17.7-96.7 58-104 .6-41.3-1-87-1-87l-22-1v-6l-51 1s.2-50.3-11-59c28.1-.1 64-1 64-1l-1-73-121 1c0 .1 58.2 186.7 85 329zm-104-386.9 138 1 4-77s-46 3.5-57 2-68.3-.9-86-55c52.2-2.1 143 2 143 2v-13l30-1s-21.4-76.1-38-106c-16.6-29.8-17.5-117.4-20-127s-31.4-79.7-42-86c-10.6-6.3-148 182-148 182l76 278z"
			opacity={0.2}
		/>
		<path
			fill="#a6a6a6"
			d="M791 1097s25-105.9 54-114c-1.3 48.3 1 123 1 123l16 40s-38.9-4.8-50-22-21-27-21-27zm337-3s-23-102.9-52-111c1.3 48.3-1 123-1 123l-12 26s6.5-7.8 38-21c18.9-7.9 27-17 27-17z"
		/>
		<linearGradient
			id="ch-topdown40002i"
			x1={959.7375}
			x2={959.7375}
			y1={907.6948}
			y2={1315}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002i)"
			d="M818 1012c9.3-13 17.2-32.3 29-32s227 0 227 0 20.6 15.2 28 31c-7.2-32.8-100.1-368.6-141-406-8.2 12-36.4 66-62 138-42.8 120.4-86.8 277.2-81 269z"
		/>
		<path
			fill="#999"
			d="M776 1176c9.1-5.9 38.7-36.1 34-55 19.9 16.3 45.3 25.8 53 25 7.1 15.4 16 31 16 31s-112.1 4.9-103-1zm265 1 24-46 48-27s-19.2 56.1 31 71c-54.6 1.2-103 2-103 2z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M810.4 1120.4c-22.2-34.1-73.4-66.1-98.4-110-25-43.9-15-286.2-15-344.4s-24.2-76.3-25-112-9.6-123.7 44.5-238.9c29.1-61.9 16-141.1 41.5-185.1S810.4 24 961.2 24s167 44.4 196.8 104 15.3 116.7 47.9 201.1c32.6 84.4 40.1 116.5 40.1 235.9-4.3 31.7-23 35.6-23 155s24.5 193.7 0 273-117.5 110.7-148 132.4c-15.8 11.2-57.6 26.6-114 26.6-120.9 0-140-15.4-150.6-31.6z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M737 225c38.8 7.9 146 50 180 97" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1184 225c-38.8 7.9-146 50-180 97" />
		<linearGradient
			id="ch-topdown40002j"
			x1={657.4258}
			x2={571.5588}
			y1={856.4412}
			y2={1092.3572}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.497} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002j)"
			d="m695 829-196 57s-48.3 11.1-18 100c25.6 64.3 83.6 83.6 102 78s134.3-42.5 146-46c12.4-3.7 28.8-58.5 18-108-8-36.6-22.9-77.1-52-81z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m695 829-196 57s-48.3 11.1-18 100c25.6 64.3 83.6 83.6 102 78s134.3-42.5 146-46c12.4-3.7 28.8-58.5 18-108-8-36.6-22.9-77.1-52-81z"
		/>
		<path
			fill="#e3e3e3"
			d="M578 956c8.8 17.7 32.9 97.7-8 110s-75.5-40.4-87-69-26.3-87.8 0-99c12.1-5.2 32.9-6 51 6 21.2 14.1 39.3 42.4 44 52z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M481 899c32.1-16 73 10.9 95 53 22.6 43.2 28.5 101.9-2 114-18.8 8.3-49.8 8.1-85-55-44-87.3-9.1-111.2-8-112z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M512 947.7c10.3-5 26.3 6.6 35.6 26.1s8.5 39.3-1.8 44.4c-10.3 5-23.3-6.6-32.6-26.1-9.4-19.5-11.6-39.4-1.2-44.4z"
		/>
		<linearGradient
			id="ch-topdown40002k"
			x1={1334.2235}
			x2={1275.9866}
			y1={1089.66}
			y2={856.084}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.497} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002k)"
			d="m1234.5 831 185 54.9s48.3 11.1 18 99.9c-25.6 64.2-83.6 83.5-102 77.9-18.4-5.6-134.3-42.5-146-45.9-12.4-3.7-28.8-58.4-18-107.8 8-36.7 33.9-75.1 63-79z"
		/>
		<path
			fill="#e3e3e3"
			d="M1437.5 898.9c-32.1-16-73 10.9-95 52.9-22.6 43.1-28.5 101.8 2 113.8 1.9.6 41.9 18 85-54.9 44.2-84 9.1-111 8-111.8z"
		/>
		<path fill="#ccc" d="M1208 1177H707s-7.7 456 253 456c243.3 0 248-456 248-456z" />
		<path
			fill="#bfbfbf"
			d="M961 237c-6.8 0-28.7 28.8-47 102s-65.5 235.9-72 256-77.6 258.5-84 320-15.1 124.9-20 136-26.8 61.3-25 78 22.8 43 35 30 38.3-37.4 49-79 46.8-165.7 53-186 93.4-289 111-289 77.6 205.8 100 267 51 169 51 169 24.7 81.5 33 91 34.3 42.8 55 22 1.2-57.4-8-76-26.2-144.8-30-170-53.6-210.8-58-226-60-205-60-205-47.9-177-50-185-26.2-55-33-55z"
		/>
		<path
			fill="#f2f2f2"
			d="m844 979 231 1v122l-36 77v150h10l11 147-19 2-2 144 11 2 11 142-21 1-2 144-155-1v-141l-25-1 9-146 14 1-1-150h-20l9-144h13l1-147-38-76-1-127z"
		/>
		<path fill="#e3e3e3" d="m1074 1106-229-1 38 78 156-2 35-75z" />
		<path
			fill="#dedede"
			d="M712 408s-58.2.4-105.4.6c-.8 0-1.7 10-2.5 10-31.2.2-57.1.4-57.1.4l2 313s28.9.6 54 1c3.3 2.4 3.8 8.3 4.8 10H718l-2-8h52l10-57-62-2v-72l124 1 14-57-139-1 1-75s94.7 4 116-14 24-40 24-40l-141 1-3-11zm497 0s58.7.4 106.1.6c.7 0 1.5 10 2.2 10 31.1.2 56.8.3 56.8.3l-2 313s-28.9.6-54 1c-3.3 2.4-3.8 8.3-4.8 10h-110.2l2-8h-52l-10-57 62-2v-72l-124 1-14-57 139-1-1-75s-94.7 4-116-14-24-40-24-40l141 1 2.9-10.9z"
		/>
		<path
			d="M548 417s25.6.1 56.8.3c.6 0 1.2-10 1.8-10 47.8.3 107.4.7 107.4.7l2 311H607.4c-.5 0-1.1-10-1.6-10H546l2-292zm824 1s-25.5-1.2-56-1c-.9 0-.2-9.7-1.1-9.6-47.9.3-107.9.6-107.9.6l-2 311h108.6c.5 0 1.1-10 1.6-10h59.8l-3-291zM846 979l229 2v126l-229-2V979z"
			className="factionColorSecondary"
		/>
		<path
			d="M731 1159c-9.7-5-26.4-7.9-14-53s20.4-49 24-69c5.1-28.2 13.5-106 17-124s29.5-137.6 51-200 53.2-192.7 68-236 53.8-235.6 84-240c22.7 5 53.5 137.4 83 245s74 249 74 249 36.9 131.1 47 196 13.9 110.6 17 120 39.6 94.8 16 108-41 2.7-60-42c-10.7-25.2-55.1-171.1-98-315-33.3-102.3-66.7-190-78-190-9.4 0-43.6 77.2-76 174-42.4 126.6-86.3 287.1-115 357-9.8 23.8-38.4 20.8-40 20z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-topdown40002l"
			x1={538.1832}
			x2={598.5352}
			y1={1345.0969}
			y2={1344.043}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown40002l)" d="m604 417-56 2s-.8 314.4 0 315 60 0 60 0l-4-317z" opacity={0.2} />
		<linearGradient
			id="ch-topdown40002m"
			x1={603.7007}
			x2={715.7007}
			y1={1345.5001}
			y2={1343.5452}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown40002m)" d="m714 407-109 1 3 336 109-1-3-336z" opacity={0.2} />
		<linearGradient
			id="ch-topdown40002n"
			x1={1202.7007}
			x2={1314.7007}
			y1={1345.5001}
			y2={1343.5452}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown40002n)" d="m1313 407-109 1 3 336 109-1-3-336z" opacity={0.2} />
		<linearGradient
			id="ch-topdown40002o"
			x1={1304.5824}
			x2={1364.5913}
			y1={1344.8392}
			y2={1343.7913}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002o)"
			d="m1318 417 55.7 2s.8 314.1 0 314.7c-.8.6-59.7 0-59.7 0l4-316.7z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown40002p"
			x1={709.3455}
			x2={800.3425}
			y1={833.6882}
			y2={800.5682}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002p)"
			d="M740 1048c-1.9 8-32.9 74.6-26 93s22.5 27.3 36 17 40.5-50.7 54-104c-22.9 12.1-42.9 20.2-64-6z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002q"
			x1={1215.9392}
			x2={1124.9421}
			y1={836.9902}
			y2={801.6033}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002q)"
			d="M1181 1048c1.9 7.9 32.9 74.4 26 92.7s-22.5 27.2-36 17-40.5-50.5-54-103.7c22.9 12 42.9 20.1 64-6z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002r"
			x1={734.9009}
			x2={832.0139}
			y1={928.9907}
			y2={913.6617}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002r)"
			d="M755 925c5.4 11.7 27.6 56.9 82 15-12.3 45-33 118-33 118s-23.7 12.7-41 6-24.1-15.7-23-23 14-82.8 15-116z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002s"
			x1={1185.7466}
			x2={1088.7277}
			y1={930.2825}
			y2={911.8105}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002s)"
			d="M1164.9 925c-5.4 11.7-27.6 56.9-81.9 15 12.3 45 33 118 33 118s23.6 12.7 41 6c17.3-6.7 24.1-15.7 23-23-1.2-7.3-14.1-82.8-15.1-116z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002t"
			x1={750.9094}
			x2={864.9103}
			y1={1047.9662}
			y2={1023.7342}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002t)"
			d="M782 806c-3.7 8.2-24.1 103.7-25 120 6.3 19.7 37.4 51.5 82 11 13.7-44.2 32-115 32-115s-25.4 31.3-53 21-32.3-45.2-36-37z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002u"
			x1={1161.3644}
			x2={1047.3644}
			y1={1044.7876}
			y2={1018.8876}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002u)"
			d="M1139 808.3c3.7 8.3 24.1 103.8 25 120.2-6.3 19.8-37.4 51.6-82 11-13.7-44.2-32-115.1-32-115.1s25.4 31.3 53 21c27.6-10.4 32.3-45.4 36-37.1z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002v"
			x1={776.7219}
			x2={902.1179}
			y1={1168.9719}
			y2={1130.802}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002v)"
			d="M818 681s15.1 39.4 45 43 47-10 47-10l-36 106s-32.2 37.4-59 25-32-37.5-30-45 33-119 33-119z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002w"
			x1={1136.3597}
			x2={1011.3387}
			y1={1168.2975}
			y2={1124.5405}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002w)"
			d="M1101.7 681s-15.1 39.5-44.9 43.1-46.9-10-46.9-10l35.9 106.3s32.1 37.5 58.8 25.1c26.8-12.5 31.9-37.6 29.9-45.1-1.9-7.6-32.8-119.4-32.8-119.4z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002x"
			x1={815.1857}
			x2={951.3387}
			y1={1294.3893}
			y2={1242.5333}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002x)"
			d="M955 609c-6.1 11.4-41.5 85.3-45 102-25.2 14.1-45.7 15.8-61 6s-31.9-34.1-30-41 35-115 35-115 8 42.5 44 52 41.2-.8 57-4z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002y"
			x1={1107.4004}
			x2={971.4034}
			y1={1295.9943}
			y2={1239.9954}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002y)"
			d="M967 608.9c6.1 11.4 41.5 85.2 45 101.9 25.1 14 45.6 15.8 60.9 6s31.9-34 30-41c-1.9-6.9-35-114.9-35-114.9s-8 42.5-44 51.9c-35.9 9.6-41.1-.7-56.9-3.9z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002z"
			x1={912.3211}
			x2={1021.3221}
			y1={1500.4399}
			y2={1458.9259}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002z)"
			d="M957 608c-14.4 4.9-89.6 32.3-105-54 27.8-82.7 41-138 41-138s39.6-173.5 68-178c-.4 38.4-.1 338.1-4 370z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002A"
			x1={999.5043}
			x2={890.5043}
			y1={1497.4854}
			y2={1418.2053}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002A)"
			d="M964 608c14.4 4.9 89.6 32.3 105-54-27.8-82.7-41-138-41-138s-39.6-173.5-68-178c.4 38.4.1 338.1 4 370z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002B"
			x1={878.2291}
			x2={987.2301}
			y1={1502.0573}
			y2={1484.7932}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002B)"
			d="M957 608c-14.4 4.9-89.6 32.3-105-54 27.8-82.7 41-138 41-138s39.6-173.5 68-178c-.4 38.4-.1 338.1-4 370z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002C"
			x1={1046.5911}
			x2={937.591}
			y1={1508.3666}
			y2={1480.1166}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002C)"
			d="M964 608c14.4 4.9 89.6 32.3 105-54-27.8-82.7-41-138-41-138s-39.6-173.5-68-178c.4 38.4.1 338.1 4 370z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown40002D"
			x1={959.2646}
			x2={959.2646}
			y1={821.084}
			y2={1744.3895}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40002D)"
			d="M731 1159c-9.7-5-26.4-7.9-14-53s20.4-49 24-69c5.1-28.2 13.5-106 17-124s29.5-137.6 51-200 53.2-192.7 68-236 53.8-235.6 84-240c22.7 5 53.5 137.4 83 245s74 249 74 249 36.9 131.1 47 196 13.9 110.6 17 120 39.6 94.8 16 108-41 2.7-60-42c-10.7-25.2-55.1-171.1-98-315-33.3-102.3-66.7-190-78-190-9.4 0-43.6 77.2-76 174-42.4 126.6-86.3 287.1-115 357-9.8 23.8-38.4 20.8-40 20z"
			opacity={0.2}
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M852 547c-19.2.7-134 0-134 0m-2-75s65.9 6 108-10c20.8-7.9 34-44 34-44l-143 1v-13s-61.3.4-109.7.7c-.5 0-.9 10-1.4 10-30.8.2-56 .3-56 .3v316s26-.1 55.1-.3m3.8 10c48.8-.3 110.2-.7 110.2-.7v-7h51s4.5-46.4 13-58c-32.9.3-63 0-63 0m-2-74 119 1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m717 738-2-326"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m607 738-2-326"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M962 237c-30.4 0-54.2 132-76 209s-61.5 207-103 354-26.3 213.6-55 279c-5.4 12.2-9 22.1-11.1 30.2-9.4 35.2-2.7 41.6 12.1 49.8 17.5 9.7 34.7-10.8 47.5-32 10.2-16.9 20-41 27.5-70 16.8-65.5 41-76 41-76"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M769 1177c12.1-3.7 52.6-29.7 35-66"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1147 1176c-12-3.7-52.2-32.7-34.8-69"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M806 1050c21.8-63.3 83.9-329.1 153-446"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M742 1036c-1.5 33.3 46.6 41.4 64 13m-48-128c4.3 51.6 67.5 42 81 13m38-126c-22.1 49.7-80.4 51.3-93-8m35-126c4.9 50.2 66.7 63.9 91 34m47-102c-30.4 19.1-99 12.3-103-52"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1180 1035.7c1.5 33.3-46.6 41.4-64 13m48-127.9c-4.3 51.6-67.5 42-81 13m-38-126c22.1 49.6 80.4 51.3 93-8m-36-125.9c-4.9 50.2-66.7 63.9-91 34M965 606c30.4 19.1 99 12.3 103-52"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m960 604 1-368"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1069 547.8c19.8-.1 134-1 134-1m2-74.9s-65.9 6-108-10c-20.8-7.9-34-44-34-44l143 1v-13s60.8.4 109 .7c.3 0 .6 10 .9 10 31.3.2 57 .3 57 .3v315.8s-26.4-.1-58.3-.3c-.3 0-.7 10-1 10-48.7-.3-109.7-.7-109.7-.7v-7h-51s-4.6-46.4-13-58c32.9.3 63 0 63 0m2.1-73.9-119 2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m1204 737.8 2-325.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m1314 737.8 2-325.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M959 237c30.4 0 54.2 131.9 76 208.9 21.8 77 61.5 206.9 103 353.8s26.3 213.5 55 278.9c5.4 12.2 8.9 22.1 11.1 30.2 9.4 35.2 2.7 41.6-12.1 49.8-17.5 9.7-34.7-10.8-47.5-31.9-10.2-16.9-20-40.9-27.5-70-16.8-65.4-41-76-41-76"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1115 1049.6c-21.8-63.3-83.9-328.9-153-445.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M845 980v126l37 79v145h-12l-11 145h22v147h-13l-9 147h24l-1 143h157l1-143h21l-12-147h-10l2-145h21l-11-147h-12v-148l37-77V980H845z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M881 1769h160m-162-147h160m-159-147 164 2m-163-147h160m-160-149h158m35-76H845"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M878.5 1177H707s-6 358.2 174.1 439.7m158.3-2.7c165-86.3 168.6-437 168.6-437h-166.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M777 1197c0 .5-1.2 182.8 0 254-36.1-80.2-48-234.5-48-255 7.4-.1 43.9 0 47.7 0 .2-.4.3.5.3 1zm359 .1c0 .5 1.2 182.7 0 253.9 36.1-80.2 48-234.4 48-254.9-7.4-.1-43.9 0-47.7 0-.2-.4-.3.5-.3 1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1437.5 898.9c-32.1-16-73 10.9-95 52.9-22.6 43.1-28.5 101.8 2 113.8 1.9.6 41.9 18 85-54.9 44.2-84 9.1-111 8-111.8z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1406.5 948.5c-10.3-5-26.3 6.6-35.6 26.1-9.3 19.4-8.5 39.3 1.8 44.3s26.3-6.6 35.6-26.1c9.4-19.4 8.6-39.3-1.8-44.3z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1234.5 831 185 54.9s48.3 11.1 18 99.9c-25.6 64.2-83.6 83.5-102 77.9-18.4-5.6-134.3-42.5-146-45.9-12.4-3.7-28.8-58.4-18-107.8 8-36.7 33.9-75.1 63-79z"
			/>
		</g>
	</svg>
);

export default Component;

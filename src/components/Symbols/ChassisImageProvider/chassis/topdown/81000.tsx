import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M960 1578c86.7 0 146-163.8 146-389 16.7-12.6 551-214 551-214s15.3 85 57 85 60-62.9 60-162-18.4-137.1-40-148c-1.1-15.9-3-37-22-37-19.1 0-21.6 22-23 39-13.5 11.4-32 30.3-32 70-58.7 3.2-438 27-438 27l-106-21s-77-174.3-77-364c58.1-26.3 130.6-43.6 154-55s38-37.1 38-67c-25.5 0-103 .1-191.4.3-25.8.1-51 22.7-79.1 36.2-27.8-15.2-48.5-35.9-71.9-35.9-104.9.2-192.6.4-192.6.4s-6.5 47.7 36 65 134.2 36.6 161 57c0 54.4-39 273.6-82 364-21 7.7-104 18-104 18l-439-25s-8-58.9-34-72c-3.6-22-1.5-36-23-36s-20 28-20 36c-11.9 6.5-40 22.6-40 153s35.5 155 61 155 57-59.3 57-85c23.8 10.8 544 211 544 211s8.2 394 150 394z"
		/>
		<radialGradient
			id="ch-topdown81000a"
			cx={2004.1169}
			cy={-663.2209}
			r={1886.5}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" />
			<stop offset={0.526} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000a)"
			d="M1070 1004c-12-4.8-83.5-5-102-5 0-56.7-4-516-4-516l72-18s-4.1 179.5 78 363c-34.8 15.7-44 69-44 176z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-topdown81000b"
			cx={-83.717}
			cy={-664.739}
			r={1886.5}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" />
			<stop offset={0.526} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000b)"
			d="M856 1004c12-4.8 83.5-5 102-5 0-56.7 4-516 4-516l-72-18s-13.5 188.2-78 363c34.8 15.7 44 69 44 176z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-topdown81000c"
			cx={231.73}
			cy={69.017}
			r={1757.2679}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.48} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000c)"
			d="M1043 1375c20 4.1 37.6 7.9 45 14 7.3-19.7 22-166.1 18-198-11.2-13.8-32.1-52.8-37-187-35.8-6.6-50.9-5.7-56-5-.5 51.8 6 213 6 213s20.7 38.9 24 163z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown81000d"
			cx={1696.925}
			cy={101.17}
			r={1773.0179}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.48} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000d)"
			d="M894 1376c-20 4.1-51.8 10.9-59.3 17-7.3-19.8-27-169.3-23.1-201.3 11.2-13.8 32.2-52.9 37.1-187.2 35.9-6.6 51.1-5.8 56.2-5 .5 51.8-6 213.3-6 213.3s-1.6 39-4.9 163.2z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown81000e"
			x1={412.2667}
			x2={494.7977}
			y1={122.0016}
			y2={-92.9984}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.555} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown81000e)" d="M711 1147 266 973s2.8-26.8 3-41c22.3 11 452 175 452 175l-10 40z" />
		<linearGradient
			id="ch-topdown81000f"
			x1={1509.3859}
			x2={1431.1318}
			y1={118.3765}
			y2={-96.6225}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.555} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown81000f)" d="m1216 1147 445-174s-2.8-26.8-3-41c-22.3 11-452 175-452 175l10 40z" />
		<linearGradient
			id="ch-topdown81000g"
			x1={495.7605}
			x2={491.8415}
			y1={-324.1134}
			y2={-265.1134}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.501} stopColor="#4b4b4b" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown81000g)" d="m715 882-445-27s-1.3-17.8-1-32c40.2.9 433 26 433 26l13 33z" />
		<linearGradient
			id="ch-topdown81000h"
			x1={1429.0516}
			x2={1432.1545}
			y1={-323.5623}
			y2={-264.5623}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.501} stopColor="#4b4b4b" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown81000h)" d="m1209 882 445-27s1.3-17.8 1-32c-40.2.9-433 26-433 26l-13 33z" />
		<path
			fill="#999"
			d="M230 750s-3.5-36-21-36-20 21.6-20 37c11.2-4.6 41-1 41-1zm1504 0s-3.5-36-21-36-20 21.6-20 37c11.2-4.6 41-1 41-1z"
		/>
		<path
			fill="#ccc"
			d="M1165 342h40s1.9 76.4-59 83c4.4-24.3 19-83 19-83zm-406 0h-40s-1.9 76.4 59 83c-4.4-24.3-19-83-19-83z"
		/>
		<path
			d="M1164 343c-22.8-.4-140-3-140-3l-66 42-56-41-139 2s14.1 66.5 20 83c23.5 8.4 180 60 180 60s171.1-53.1 180-60c6.5-19.9 20.3-68.1 21-83zM209 747c14.3 0 60-5.1 60 163 0 158.2-56 147-61 147s-60 2.1-60-161 46.7-149 61-149zm1505 .3c14.3 0 60-5 60 162.8 0 158-56 146.8-61 146.8s-60 2.1-60-160.7 46.7-148.9 61-148.9z"
			className="factionColorPrimary"
		/>
		<path
			d="M703 847c34.2-2.8 80.7-12.1 111-18 63.5 32 39.1 355-5 355-33.5-14.3-75.2-30.6-97-37 33.1-126.5 6.4-251.9-9-300zm520 0c-34.2-2.8-80.7-12.1-111-18-63.5 32-46.9 359.3-5 359 33.6-14.3 86.2-37.6 108-44-25.4-129.1-8.2-267.1 8-297z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-topdown81000i"
			cx={688.722}
			cy={-221.505}
			r={1086.665}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000i)"
			d="M209 747c14.3 0 60-5.1 60 163 0 158.2-56 147-61 147s-60 2.1-60-161 46.7-149 61-149z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown81000j"
			cx={-270.254}
			cy={-226.348}
			r={1086.665}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000j)"
			d="M209 747c14.3 0 60-5.1 60 163 0 158.2-56 147-61 147s-60 2.1-60-161 46.7-149 61-149z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown81000k"
			cx={2192.7219}
			cy={-221.422}
			r={1085.024}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000k)"
			d="M1713 747.3c14.3 0 60-5 60 162.8 0 158-56 146.8-61 146.8s-60 2.1-60-160.7 46.7-148.9 61-148.9z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown81000l"
			cx={1233.746}
			cy={-226.2571}
			r={1085.024}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000l)"
			d="M1713 747.3c14.3 0 60-5 60 162.8 0 158-56 146.8-61 146.8s-60 2.1-60-160.7 46.7-148.9 61-148.9z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown81000m"
			cx={1538.575}
			cy={232.478}
			r={1275.194}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.395} stopColor="gray" />
			<stop offset={0.427} stopColor="#656565" />
			<stop offset={0.49} stopColor="#7f7f7f" />
			<stop offset={0.5} stopColor="#e6e6e6" />
			<stop offset={0.51} stopColor="#7f7f7f" />
			<stop offset={0.539} stopColor="#666" />
			<stop offset={0.849} stopColor="#666" />
			<stop offset={1} stopColor="#7f7f7f" />
		</radialGradient>
		<path
			fill="url(#ch-topdown81000m)"
			d="M832 1387c94.6-17.8 159-11 159-11s-3 74 28 74 23-68.5 23-75c13.4 0 45 14 45 14s-20.3 190-127 190c-88 0-117.6-135.5-128-192z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M960 1578c86.7 0 146-163.8 146-389 16.7-12.6 555-215 555-215s11.3 86 53 86 60-62.9 60-162-18.4-137.1-40-148c-1.1-15.9-3-37-22-37-19.1 0-21.6 22-23 39-13.5 11.4-32 30.3-32 70-58.7 3.2-438 27-438 27l-106-21s-77-174.3-77-364c58.1-26.3 130.6-43.6 154-55s38-37.1 38-67c-25.5 0-116 .1-204.4.3-18.6 10.7-37.3 24.1-63.1 40.2-26.3-17.6-37.8-23.6-59.9-39.9-104.9.2-207.6.4-207.6.4s-6.5 47.7 36 65 134.2 36.6 161 57c0 54.4-39 273.6-82 364-21 7.7-104 18-104 18l-439-25s-8-58.9-34-72c-3.6-22-1.5-36-23-36s-20 28-20 36c-11.9 6.5-40 22.6-40 153s35.5 155 61 155c44.1 0 54-53.6 57-84 23.8 10.8 544 211 544 211s8.2 393 150 393z"
		/>
		<linearGradient
			id="ch-topdown81000n"
			x1={960.9919}
			x2={1033.4369}
			y1={-770.0983}
			y2={-771.3633}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={0.503} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#4c4c4c" />
		</linearGradient>
		<path
			fill="url(#ch-topdown81000n)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m961 380 17-61h41s5.3 8.1 14.4 20.1c-3.6 2.4-4.7 2.3-12.4 3.9-19.9 11.9-52 34.2-56.9 37.3-.7.5-3.1-.3-3.1-.3z"
		/>
		<linearGradient
			id="ch-topdown81000o"
			x1={890.2937}
			x2={962.9527}
			y1={-769.3029}
			y2={-770.5718}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={0.503} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#4c4c4c" />
		</linearGradient>
		<path
			fill="url(#ch-topdown81000o)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M963 380.4 946 319h-41.1s-6.2 9.9-14.5 20.2c3.6 2.4 4.7 2.3 12.5 4 20 12 52.2 34.4 57.1 37.6.6.4 3-.4 3-.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1039 463-78 23-77-25" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1205 343s-4.9 58.4-29 71" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1165 343s-13.1 52-20 81" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M761.9 343.2s13.2 51.8 20.2 80.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M717 343s4.9 58.4 29 71" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M810 1185c41.5 0 69.7-356-1-356" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M854 1004c27.5-7.5 185.3-6 215-2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M700.7 846.4c28.5 35 28.8 251.1 11.3 296.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1108.8 1190c-41.4 0-63.6-361 7-361" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1224.1 846.4c-25.2 30.9-23.9 222.7-9.7 296.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M265 820c5.5 30.4 10.7 97.7-5 191" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1657.2 820c-4.1 35.8-11.3 64.7 3.8 158" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M180 755c9.3-6.5 41.9-10.6 56-2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1684 757c10.4-7.8 35.2-15.9 58-1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M833 1392.8c31.2-16.3 83.7-20.8 127.3-20.8 18.6 0 28.5 2.1 31.7 2 0 1.2-5.9 77 26 77 29 0 23-66.6 23-74 18.3 0 50 12 50 12"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M846 1440c17.5-5.7 46.1-24.9 146-21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1038 1430c9.9 3.8 32.8 15 36 18" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M891 1540c19.7-14.4 111-15 133 4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M864 1490c35.5-24.5 162.8-28.1 188 6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M877 1277c36 0 38-4.6 38 25 0 26.1-.5 18-42 18-29.7 0-25 2-25-19 0-26.9-2.1-24 29-24z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M992 1423h50" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1041 1379c0-21.8-6.5-168-24-168s-25 132.2-25 170" />
	</svg>
);

export default Component;

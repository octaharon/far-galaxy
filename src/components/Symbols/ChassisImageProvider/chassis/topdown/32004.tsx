import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M978 1897v10h92.9s.7-106.6 0-127c2.8 1.4 10.2 11 23 11 24 0 24-44.7 24-63s.8-56 0-75c1.7-5 6.4-12.5 23-77 16.5-64.5 53.2-237.6 66-323s23.2-185.2 23-186 12.5-175.1 14-211c13.2-10.8 121.9-83 121.9-83h60v-35s24.8-5.7 41-19c12.8-25.9 10-203.8 10-232 0-28.2-1.7-131.5-5-164s-27-21-27-21l-17 19v-78s-35.5.2-76.1.4c0 6.3.2 28.5.1 33.6-3.5 0-27.7.1-38 0 0-7.7.1-30.1.1-33.4-42.2.2-80.1.4-80.1.4l-1 252-9 6s-7-25.9-10-33c-3-7.1-10.9-6.9-25-9-9.4-23.3-14.6-26.9-36-61s-48-99.1-59-132c-11-32.9-47.2-152.6-54-177-6.8-24.4-16-39-16-39s-13.4-17.5-19-26-10.3-13.3-16-13c-5.7.3-53.3.3-59 0s-10.4 4.5-16 13-19 26-19 26-9.2 14.6-16 39-43 144.1-54 177-37.6 97.9-59 132-26.6 37.8-36 61c-14.1 2.1-22 1.9-25 9s-10 33-10 33l-9-6-1-252s-37.8-.2-80.1-.4c0 3.3.1 25.8.1 33.4-10.3.1-34.5 0-38 0-.1-5.1.1-27.3.1-33.6-40.6-.2-76.1-.4-76.1-.4v78l-17-19s-23.7-11.5-27 21-5 135.8-5 164c0 28.2-2.8 206.1 10 232 16.1 13.3 41 19 41 19v35h60s108.8 72.2 122 83c1.5 35.9 14.3 210.2 14 211s10.2 100.6 23 186 49.5 258.5 66 323 21.3 72 23 77c-.8 19 0 56.7 0 75s0 63 24 63c12.8 0 20.2-9.6 23-11-.7 20.4 0 127 0 127h93v-10s9 7 19 7 18.2-7 18.2-7z"
		/>
		<linearGradient
			id="ch-topdown32004a"
			x1={1246}
			x2={662}
			y1={997.9161}
			y2={997.9161}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.449} stopColor="#e6e6e6" />
			<stop offset={0.552} stopColor="#e5e5e5" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown32004a)"
			d="M662 738.1c1.7 24.3 19.6 448.8 94 760.9 44.4 186.2 114.2 331 198 331 56 0 114.6-59.6 154-156 30.9-75.7 53.1-172.4 73-274 19-96.7 36.3-197.7 45.5-296.5 13.1-140.2 12.4-272.2 19.5-366.4-8.6-83-71.9-24.6-17-207.1-3.6-11.2-8.6-33.5-16.1-61.9-10.1-4.8-17.8-3.9-24.9-9.1-14.3-28.1-34.6-51.2-58-98-27.5-54.9-54.9-141.9-77-221-15.9-57.1-29.3-109.3-71-125-12-.8-25.8-1.4-50 0-16.5 4.8-43.8 25-72 141-5.9 25.6-26.5 84.7-55 165-20.8 48.6-61.4 116.1-75 138-13.1.9-27.3 2.6-24 12-17.1 48.4-31.8 114.7-44 267.1z"
			opacity={0.22}
		/>
		<path
			fill="#dadada"
			d="M958 12c14 0 33 1.8 42 10s26 31 26 31l27 78s23.3 91.7 35 119 31.1 97.8 49 123 48 76.7 51 88c12.7.9 26.6 5.2 26 9s-47.1 14.3-49 13c-178.9 24.3-413 2-413 2s-39-8.8-46-14c4.3-6.5 13.6-10.4 24-11 8.8-20.2 32.9-55 46-80s64.1-135.1 81-215 46.7-142.8 69-149 18-4 32-4z"
		/>
		<path
			fill="#ccc"
			d="M804 1655c-1.1 11-3.4 14.1-3 59 .5 50.6 2.6 76.9 24.7 77.7 17.7.7 27.6-15.7 35.3-23.7-9.2-15.3-38-58.6-57-113zm249.3 115.4c5.9-13.6 48.3-75.2 62.7-110.4 2.5 61.8 10.3 119.5-18 132-12.5 2.9-27.6-13.7-44.7-21.6z"
		/>
		<path
			fill="gray"
			d="m492 323-2-79 76 1s1.3 34.1 2 34 38-2 38-2l-2-32 80 1 2 250-81-71-46-42-67-60zm0 418c-.3.9 1 33 1 33l60-1s-60.7-32.9-61-32zm741-246 1-250h80l-1 33 38-1 2-35 75 2-1 76-194 175zm192 244 1 35-59-1 58-34zM847 1782l1 125 94 1-1-12s-39.2-22.5-74-125c-8-1.4-20 11-20 11zm223 0 2 125-93-1s-3.3-6.5-2-7 64.1-60.6 72-126c9.2-1.2 21 9 21 9z"
		/>
		<linearGradient
			id="ch-topdown32004b"
			x1={1398.5}
			x2={1398.5}
			y1={1131.637}
			y2={1171.637}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown32004b)" d="m1369 774 59 1-1-40-58 39z" opacity={0.702} />
		<linearGradient
			id="ch-topdown32004c"
			x1={521.5}
			x2={521.5}
			y1={1131.637}
			y2={1171.637}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown32004c)" d="m551 774-59 1 1-40 58 39z" opacity={0.702} />
		<linearGradient
			id="ch-topdown32004d"
			x1={1392}
			x2={1392}
			y1={1695.8931}
			y2={1628.8931}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.8} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown32004d)" d="M1353 308v-65l77-1 1 67-78-1z" opacity={0.6} />
		<linearGradient
			id="ch-topdown32004e"
			x1={1273}
			x2={1273}
			y1={1695.8931}
			y2={1628.8931}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.8} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown32004e)" d="M1234 308v-65l77-1 1 67-78-1z" opacity={0.6} />
		<linearGradient
			id="ch-topdown32004f"
			x1={647}
			x2={647}
			y1={1695.8931}
			y2={1628.8931}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.8} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown32004f)" d="M608 308v-65l77-1 1 67-78-1z" opacity={0.6} />
		<linearGradient
			id="ch-topdown32004g"
			x1={530}
			x2={530}
			y1={1695.8931}
			y2={1628.8931}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.8} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown32004g)" d="M491 308v-65l77-1 1 67-78-1z" opacity={0.6} />
		<linearGradient
			id="ch-topdown32004h"
			x1={959.4757}
			x2={959.4757}
			y1={150}
			y2={11}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown32004h)"
			d="M847 1909c6.5 0 47.8-.4 93.1-.8 1.2-11.9 12.8-4.1 19.9-4.2 6.2-.1 17.2-6 20 3.8 44.7-.4 84.7-.8 90-.8.4-26 2-126 2-126l-13-9s-42.6 70-98 70-90.8-60.7-101-72c-9 14.1-8.2 10.2-13 13-.3 18.9 1.3 86.8 0 126z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown32004i"
			x1={960.0069}
			x2={960.0069}
			y1={13}
			y2={154}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown32004i)"
			d="M848.1 1907c6.5 0 91.4-.3 91.9-.4.1 0-.7-7.3 2-7.6 3.4-.4 11.1 6 18 6 2.7 0 12.7-4.4 16-4 2.8.3-1.1 5.4 1.6 5.4 36.8-.2 88-.4 92.4-.4.4-26 2-124 2-124l-19-13s-37.6 60-93 60-87.8-51.7-98-63c-5.7 7.5-9.2 11.2-13.9 14-.4 18.9 1.2 87.8 0 127z"
			opacity={0.502}
		/>
		<path fill="#4d4d4d" d="m568 278-2 113 41 36-1-153-38 4zm747-2 36 1 2 116-36 32-2-149z" />
		<path
			fill="#671e75"
			d="m650 468-1 371s189.3 130.7 260 103c.9-58-2-76-2-76s-17.5-14.9-93-161-96.5-93.3-122-168c-1.1-17.2 3-34 3-34l-45-35zm620 0 1 371.1s-189.2 130.8-259.8 103c-.9-58 2-76 2-76s17.5-14.9 92.9-161 96.4-93.3 121.9-168c1.1-17.2-3-34-3-34l45-35.1z"
		/>
		<linearGradient
			id="ch-topdown32004j"
			x1={959.5}
			x2={959.5}
			y1={453.2669}
			y2={1315}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e16d1a" />
			<stop offset={0.9} stopColor="#6f1370" />
		</linearGradient>
		<path
			fill="url(#ch-topdown32004j)"
			d="M1012 874s16.8-22.5 35-56c22.1-40.7 49.8-102.6 72-141 18.6-32.2 32-46 32-46l-30-23s-21.1 30.2-141 34c-12.5 8.4-20 12-20 12s-10.9-5.3-20-11c-116.4 3.3-146-38-146-38l-26 26s13.4 13.8 32 46c22.2 38.4 50 100.3 72 141 18.2 33.5 34 57 34 57s2.4 56.5 2 68c-26.1 14.3-106.3-18.1-125-26 17.2 39.9 50.7 138.1 67 204s61.9 273.4 70 344c10.8-23.4 23.4-63 40-63s33 64 33 64 2.9 2.2 5-1c8.1-70.6 53.7-278.1 70-344s49.8-164.1 67-204c-18.7 7.9-98.9 40.3-125 26 .7-17 2-69 2-69z"
		/>
		<path
			fill="#e16d1a"
			d="M786 1610c3.9-12.3 14.6-39.1 40-38s46.9 19 56 27 15.3 16 78 16 66.2-7.1 71-13 40.8-30 63-30 38 40 38 40l10-38s-15-28-43-28-63 25-63 25l-22-54 206-365 8-75-229 402s-25.8-78-39-78-22.3 26.1-42 73c-14.1-23.1-227-396-227-396s9 87 10 87 201 346 201 346l-21 57s-42-23-66-23-36 37-36 37 3.6 15 7 28zm84 168c7.4 9.9 48.8 51 90 51s83-46.1 91-57c-1.8 32.3-56.2 132-92 132-14.8 0-37.3-23.4-54-50-23.7-37.7-39.3-81.8-35-76z"
		/>
		<linearGradient
			id="ch-topdown32004k"
			x1={413.33}
			x2={482.33}
			y1={1398.804}
			y2={1397.599}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown32004k)"
			d="m445 326-3 131v138l2 89 8 33 19 14 33 13 7-404-18-18-21-21h-14l-8 8-5 17z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown32004l"
			x1={1505.5428}
			x2={1436.5428}
			y1={1399.6866}
			y2={1397.6626}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown32004l)"
			d="m1474 326 3 131v138l-2 89-8 33-19 14-33 13-7-404 18-18 21-21h14l8 8 5 17z"
			opacity={0.502}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M978 1897v10h92.9s.7-106.6 0-127c2.8 1.4 10.2 11 23 11 24 0 24-44.7 24-63s.8-56 0-75c1.7-5 6.4-12.5 23-77 16.5-64.5 53.2-237.6 66-323s23.2-185.2 23-186 12.5-175.1 14-211c13.2-10.8 121.9-83 121.9-83h60v-35s24.8-5.7 41-19c12.8-25.9 10-203.8 10-232 0-28.2-1.7-131.5-5-164s-27-21-27-21l-17 18v-77s-35.5.2-76.1.4c0 6.3.2 28.5.1 33.6-3.5 0-27.7.1-38 0 0-7.7.1-30.1.1-33.4-42.2.2-80.1.4-80.1.4l-1 252-9 6s-7-25.9-10-33c-3-7.1-10.9-6.9-25-9-9.4-23.3-14.6-26.9-36-61s-48-99.1-59-132c-11-32.9-47.2-152.6-54-177-6.8-24.4-16-39-16-39s-11.9-16.7-19-26c-6.2-8.1-20.3-13.3-26-13-5.7.3-33.3.3-39 0s-19.5 5.1-26 13c-7.8 9.5-19 26-19 26s-9.2 14.6-16 39-43 144.1-54 177-37.6 97.9-59 132-26.6 37.8-36 61c-14.1 2.1-22 1.9-25 9s-10 33-10 33l-9-6-1-252s-37.8-.2-80.1-.4c0 3.3.1 25.8.1 33.4-10.3.1-34.5 0-38 0-.1-5.1.1-27.3.1-33.6-40.6-.2-76.1-.4-76.1-.4l1 77.5-1 .5-17-19s-23.7-11.5-27 21-5 135.8-5 164c0 28.2-2.8 206.1 10 232 16.1 13.3 41 19 41 19v35h60s108.8 72.2 122 83c1.5 35.9 14.3 210.2 14 211s10.2 100.6 23 186 49.5 258.5 66 323 21.3 72 23 77c-.8 19 0 56.7 0 75s0 63 24 63c12.8 0 20.2-9.6 23-11-.7 20.4 0 127 0 127h93v-10s9 7 19 7 18.2-7 18.2-7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M960 1401c-7.3 0-14.2 3.1-49 91-17.4 39.4-27 69.7-31 82s-5.3 40 46 40h71s59.6.9 38-48-59-147-59-147-6.7-18-16-18z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M567 264v128" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M493 738c8.8 4.3 50.9 28 62 36" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M696 502c-4.6 16.9-15.2 40.4 23 79s60.5 48.1 104 140 83 152 83 152v70s-31 34.8-232-87"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M708 473c7.9 6 66.3 18 172 18" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m690 1074 228 402m-220-324s187.4 331.2 204.7 361.8M882 1569c-19.2-10.5-78.3-50.6-102 7m8 30c3.5-10.4 23.1-66.6 94-8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M921 1467s-62.2-406.3-142-553" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M999 1467s62.2-406.3 142-553" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M605 282v142" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M490 320c10.3 11.7 170.1 156 197 177" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M650 465v372" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1268 465v372" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1352 264v128" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1425 738c-8.8 4.3-50.9 28-62 36" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1223 502c4.6 16.9 14.2 40.4-24 79s-60.5 48.1-104 140-83 152-83 152v70s31 34.8 232-87"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1212 472c-7.9 6-66.3 18-172 18" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1000 1476 228-402m-8 78-206 364m22 53c19.2-10.5 78.3-50.6 102 7m-8 30c-3.5-10.4-23.1-66.6-94-8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M800 1649c9.1 27.5 74.3 180 159 180" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1119 1649c-9.1 27.5-75.3 180-160 180" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 1829v76" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1314 282v142" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1428 319c-10.3 11.7-170.1 157-197 178" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M842 1785c8-4.8 14.2-10.6 20-18m5 5c5.1 18.8 34.1 92.7 75 125m36 0c12.2-10.9 62.1-63.6 74.8-125.8m1.1-.7c10.1 5.5 20.1 12.5 20.1 12.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M796.7 605.8C785.8 615.2 771 628 771 628" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1121.7 606.1c10.8 9.4 25.3 21.9 25.3 21.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M755 486c-9.6 43 15.6 91.4 40 119 22.5 25.4 73.5 35.4 146.1 36.8 6.4 4.4 13.1 9.6 18.9 13.2 5.4-3.9 14.6-10 19-13.2 72-1.5 122.6-11.5 145-36.8 24.4-27.6 49.5-75.9 40-119"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 1399V652" />
		<g id="Animation-Tower">
			<path
				fill="#f2f2f2"
				d="m1005 1222 7-405h13v-20h-12v-21h12v-20h-12v-19h12v-18h-12s-.1-7.7 0-20c99-47.5 158-116 158-178s-54.4-154.7-64-167-39.2-62.9-57-96-36.2-44.3-52-47-62.2-2.7-78 0-34.2 13.9-52 47-47.4 83.7-57 96-64 105-64 167 59 130.5 158 178c.1 12.3 0 20 0 20h-12v18h12v19h-12v20h12v21h-12v20h13l7 405h92z"
			/>
			<path
				fill="#dadada"
				d="M816 349c-18.5 18-70 109.6-70 175s75.4 132.9 121 153c-24.4-22.4-85-85.6-85-157s11.7-110.9 34-171zm285 0c18.5 18 70 109.6 70 175s-75.4 132.9-121 153c24.4-22.4 85-85.6 85-157s-11.7-110.9-34-171z"
			/>
			<path
				fill="#671e75"
				d="M801 393c0 2.3 104 87 104 87l-2 217s-75.2-51.6-94-92-49.5-83.7-8-212zm316.3 0c0 2.3-104.3 87-104.3 87l2 217s75.4-51.6 94.2-92 49.6-83.7 8.1-212z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1005 1222 7-405h13v-20h-12v-21h12v-20h-12v-19h12v-20h-12s-.1-5.7 0-18c99-47.5 158-116 158-178s-54.4-154.7-64-167-39.2-62.9-57-96-36.2-44.3-52-47-62.2-2.7-78 0-34.2 13.9-52 47-47.4 83.7-57 96-64 105-64 167 59 130.5 158 178c.1 12.3 0 18 0 18h-12v20h12v19h-12v20h12v21h-12v20h13l7 405h92z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M907 817h29v-20h-36" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M907 776h29v-20h-36" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M907 737h29v-20h-36" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1013 817h-29v-20h36" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1013 776h-29v-20h36" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1013 737h-29v-20h36m-7-18H903" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M902 697c-36.5-32.5-232.9-116.8-32-443" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1016.3 697.3c36.4-32.4 232.1-116.7 31.9-442.6" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M905 700V480s-77-65.9-102.7-87.9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1013 700V479.8s77-65.9 102.8-88" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1014 480H904" />
		</g>
	</svg>
);

export default Component;

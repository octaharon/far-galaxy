import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M967.1 1614.6c-38.4 0-74.4-56.6-110.2-347.1-65-42.6-272.9-192.1-272.9-192.1l-2.6 324.1-115.3-42.3S257.8 923 251 909.1c-16.8-7.5-59-25.8-59-25.8L25.4 675.7v-60.2h28.2l173 20.5 173-64 178.1 3.8 190.9 56.4 19.2-216.5s36-16.3 88.4 0c2.9 30.5 5.1 51.2 5.1 51.2s84.1-67.7 171.7 5.1c3.1-30.6 5.1-52.5 5.1-52.5s39.9-19.5 89.7-2.6c6.4 79.1 19.2 216.5 19.2 216.5l178-56.4 189.6-5.1 170.4 61.5 170.4-16.7 28.2-1.3v64l-164 201.1L1709 894l-242.3 463.1-123 37.2 2.6-315.1-270.3 185.7s-21 349.7-108.9 349.7z"
		/>
		<path fill="#ccc" d="m1880.5 616.7-175.5 18 2.6 260 32-14.1L1881.8 709l-1.3-92.3z" />
		<path fill="#ccc" d="m49.8 616.7 180.6 17.9-3.8 263.9-32-14.1L49.8 711.5v-94.8z" />
		<linearGradient
			id="ch-topdown85000a"
			x1={824.8525}
			x2={1112.078}
			y1={889.6077}
			y2={894.621}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8f9bb0" />
			<stop offset={0.399} stopColor="#dae3f2" />
			<stop offset={0.604} stopColor="#dae3f2" />
			<stop offset={1} stopColor="#8f9bb0" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85000a)"
			d="M967.1 440c-28.5 0-89.8 13.6-111.5 67.9-21.6 54.3-30.8 190.4-30.8 244.7 0 54.3 4.6 214.7 10.3 298.5 5.7 83.8 25.5 281.8 41 353.6 15.5 71.7 36.6 211.4 91 211.4s86-151.5 94.8-210.1c6.6-44.2 31.9-291.8 35.9-348.4 4-56.7 14.1-233.3 14.1-303.6 0-70.3-12-221.2-37.1-258.8-31.9-44.2-79.2-55.2-107.7-55.2z"
		/>
		<linearGradient
			id="ch-topdown85000b"
			x1={967.0601}
			x2={967.0601}
			y1={264.4}
			y2={311.8}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85000b)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M945.3 1608.2v47.4h43.6v-47.4s-9.8 6.4-20.5 6.4c-11.1 0-23.1-6.4-23.1-6.4z"
		/>
		<path
			d="m467.4 1355.8 116.6 43.6V578.3l-180.6-3.8 64 781.3zm1000.6 2.6-123 37.2V574.5l196-1.3-73 785.2z"
			className="factionColorPrimary"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M967.1 1614.6c-38.4 0-75.7-56.6-111.5-347.1-65-42.6-272.9-192.1-272.9-192.1l-1.3 324.1-115.3-42.3-214-447.1s-10.8-4.8-23.3-10.4c-16.9-7.6-37-16.5-37-16.5L25.4 684.6v-69.2h28.2l173 20.5 173-64 178.1 3.8s169.5 50.8 190.9 56.4c17.1-67.3 57.2-74.9 74.3-102.5 11.8-44.9 38.4-58.9 38.4-58.9s85.4-71.5 173 1.3c16.6 13.6 30.1 26 41 53.8 37.3 58.9 69.2 107.6 69.2 107.6l180.7-57.7 189.6-3.8 170.4 61.5 170.4-16.7 28.2-1.3v69.2l-164 196s-16.7 8.2-32.5 15.9c-8.4 17.2-240.4 460.6-240.4 460.6l-123 37.2 1.3-313.8-267.8 187c-.2 0-22.4 347.1-110.3 347.1z"
		/>
		<path
			fill="#f1f1f1"
			d="m826.1 899.8-71.7-80.7 14.1-188.3 19.2-213.9s44.3-19.1 88.4 0c1.6 15.5 2.9 24.2 5.1 52.5-64.7 41.7-56.4 336.4-55.1 430.4zm279.3 0 75.4-81-14-188.4-19.1-211.4s-44.2-19.1-88.1 0c-1.6 15.6-2.9 24.2-5.1 52.5 66.3 51.1 53.7 332.4 50.9 428.3z"
		/>
		<path
			d="m754.6 819.4 70.3-.3s-4.7-259.2 32.2-319.1c-15.3-2.7-56.3-3.4-76-2 .1-1.2-9.6 80.9-15 164-5.2 77.9-5.8 157.1-11.5 157.4zm425.4.6-70.7-.9s5-260.7-32.3-320.1c15.3-2.7 57.8-5.3 77 0 .8 13.6 8.5 90.8 14 161 6.3 80.2 12 160 12 160z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown85000c"
			x1={1196.9673}
			x2={1072.9673}
			y1={1307.8661}
			y2={1299.1951}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.199} stopColor="#000" />
			<stop offset={0.558} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown85000c)"
			d="m1180 821-70.7-.9s5-260.7-32.3-320.1c-5.9-10.3-14.3-21.6-21-30 1.2-9.7-.8-36.1 5-50 24.9-16.7 78.5-3.3 87-1 .8 13.6 14.5 171.8 20 242 6.3 80.2 12 160 12 160z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown85000d"
			x1={744.1844}
			x2={868.4324}
			y1={1309.0386}
			y2={1296.6155}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.199} stopColor="#000" />
			<stop offset={0.558} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown85000d)"
			d="m755 821 70.9-.9s-5-260.6 32.3-319.9c5.9-10.3 14.3-21.6 21-30-1.2-9.7.8-36.1-5-50-24.9-16.6-78.6-3.3-87.2-1-.8 13.6-14.5 171.6-20 241.8-6.3 80.2-12 160-12 160z"
			opacity={0.2}
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M879.9 470.7c-94.6 58.9-46.2 693.3-17.9 844.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1051.6 469.4c101.7 60.7 40.6 649.8 25.1 805.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M754.4 819.1h71.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1110.6 819.1h71.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1076 497.6h80.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M777.5 497.6h78.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M48.5 615.4V709" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m468.7 1358.4-67.9-789.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M582.7 1398.1 584 577" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M227.8 634.7v262.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1880.5 615.4V709" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1468 1354.6 71.7-781.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1346.3 1393-1.3-812.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1703.7 632.1 2.6 262.6" />
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M830 639.8c13 6.3 21.2 20.3 33.3 39.7.1 29.1 1.3 242.1 1.3 242.1S839 940.7 831.3 992c-5.2-83.6-10-280.6-1.3-352.2z"
		/>
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1104.4 639.2c-13.1 6.3-21.3 20.4-33.5 39.7-.1 29.1-1.3 242.2-1.3 242.2s23.1 19.1 31 70.5c5.1-83.7 12.6-280.8 3.8-352.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M919.7 1167.5V447.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1015.8 1168.8V447.6" />
		<linearGradient
			id="ch-topdown85000e"
			x1={896.361}
			x2={1038.7551}
			y1={619.3375}
			y2={621.8236}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.148} stopColor="#333" />
			<stop offset={0.497} stopColor="#b3b3b3" />
			<stop offset={0.844} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85000e)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M956.8 1451.9V1147s-17.1-4.9-47.4 20.5c-.6 1.5-5.6 35-13 95.3 12.2 70.5 26.7 145.4 37.4 184 1.8 2.1 3.1 3.6 5.4 5.3 13.4.2 17.6-.2 17.6-.2zm21.8-.2v-304.3s17-4.8 47.2 20.5c.6 1.5 5.6 34.9 13 95.1-12.1 70.4-26.6 145.2-37.3 183.6-1.8 2.2-3.1 3.6-5.4 5.3-14.3.2-17.5-.2-17.5-.2z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m826.1 899.8-71.7-80.7 14.1-188.3 19.2-213.9s44.3-19.1 88.4 0c1.6 15.5 2.9 24.2 5.1 52.5-64.7 41.7-56.4 336.4-55.1 430.4zm279.3 0 75.4-81-14-188.4-19.1-211.4s-44.2-19.1-88.1 0c-1.6 15.6-2.9 24.2-5.1 52.5 66.3 51.1 53.7 332.4 50.9 428.3z"
		/>
	</svg>
);

export default Component;

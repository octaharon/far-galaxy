import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#e6e6e6"
			fillRule="evenodd"
			d="M945.5 1883.4c-194.8-16.6-235-367.8-249.5-451.4-18.4 41.2-28 175.1-30 202s-11.3 136.1-20 174-27 37-27 37-99.1 1-119 1-29.3-1.2-37-7c-16.3-104.7-21-1146.3-21-1229s55.3-423.4 91-492c30.8-9.6 237.4-67 254-67h346c16.6 0 223.2 57.4 254 67 35.7 68.6 91 409.3 91 492s-4.7 1124.3-21 1229c-7.7 5.8-17.1 7-37 7s-119-1-119-1-18.3.9-27-37-18-147.1-20-174-11.6-160.8-30-202c-14.6 83.6-54.7 434.8-249.5 451.4-4.8.4-24.2.4-29 0zM852 1072c-66.3 37.6-97 102.4-97 180s75.6 158.4 98 167c-3-50.8-11-264-11-264l13 6s-1.3-63.3-3-89zm214 90 12-8s-7.6 181.5-10 264c42.7-28.8 98-96.8 98-178s-52.3-142.2-98-169c-.8 15.8-2 91-2 91z"
			clipRule="evenodd"
		/>
		<linearGradient
			id="ch-topdown64000a"
			x1={1225}
			x2={699}
			y1={170.55}
			y2={170.55}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={0.198} stopColor="#e6e6e6" />
			<stop offset={0.49} stopColor="#000" />
			<stop offset={0.805} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-topdown64000a)"
			d="m1084 999-18 35 1 37s69 39.8 87 104 9.8 106.7-1 137-45.5 80.8-85 108c.2 17.8-1 34-1 34l-8-5-5 82h6l19 48v235l-236 2-2-237 18-47 8-2-4-80-11 7v-39s-61.9-41.4-86-109-3.2-141.2 11-162 45.2-64.7 77-77c-1.2-19 0-34 0-34l-16-35-1-13-17-1-74-222s-10 134.1-18 252-24.8 386.6-29 416c4.5 36.8 21.3 151.8 45 227 19.9 63.1 55.8 129.5 88.6 165.1 13.8-19.2 35.8-9.1 128.4-9.1 31.8 0 118-4.5 128.9 7.2 9.8-10.6 19.3-22.9 28.1-37.2 49.2-79.3 76.2-191.9 85-237s20.3-108.1 22-123c-10.4-26.8-46.7-636.8-49-670-9.2 17.4-66.3 208.5-73 236-6.1 1.3-19 7-19 7z"
		/>
		<linearGradient
			id="ch-topdown64000b"
			x1={961}
			x2={961}
			y1={699}
			y2={765}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b5b5b5" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-topdown64000b)"
			d="M836 1822c.9-1.4 2-3 2-3h244l4 4s-44 62-125 62c-25.2 0-109.7-20.4-125-63z"
		/>
		<path
			fill="#ccc"
			d="M781 311c-28.5-8.6-86.8-34.2-90-111s66.9-108.3 81-112c-47.4 9.2-107.7 30.8-100 131 10.9 73.5 84.7 101.8 91 103 7.8-4.6 12.9-4.9 18-11z"
		/>
		<path
			fill="#f2f2f2"
			d="m1083 999-1-247-242 2-2 246 17 35-1 126-12-10 11 306 9-7 5 80-7 2-18 47v237h237l-1-236-18-48-5-2 4-81 8 6 10-302-11 9 1-127 16-36z"
		/>
		<path fill="none" stroke="#000" strokeWidth={10} d="M864 1460c.7-.4 80-65 80-65v-173l-88-72" opacity={0.302} />
		<path
			fill="none"
			stroke="#000"
			strokeWidth={10}
			d="M1056.9 1459.8c-.7-.4-79.9-65-79.9-65v-172.9l87.9-72"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown64000c"
			x1={961}
			x2={961}
			y1={-126.558}
			y2={-391.558}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#797979" />
			<stop offset={1} stopColor="#f1f1f1" />
		</linearGradient>
		<path fill="url(#ch-topdown64000c)" d="M1083 994h18l-7-265H828l-7 259h17l1-234 242-2 2 242z" />
		<path fill="#999" d="m902 1638 115 1-1 30H901l1-31zm-1 80s113.4.2 114 0 1 30 1 30l-114 1-1-31z" />
		<path
			fill="#d9d9d9"
			d="m1060 1532 18 47s-237.6-1.2-236-1 19-46 19-46h199zm20-529-14 33s-213.6-1.2-212-1-15-32-15-32h241z"
		/>
		<path
			fillRule="evenodd"
			d="M463 1839c-5.3-32.7-14-400-14-400s198.5-.1 245 1c-17.9 35.7-31 230.8-38 297s-12.5 90.3-26 103-165.1 7-167-1zm124-27-32-57-33 57h65zm703 28.5c-13.5-12.7-19-36.8-26-103.1s-20.1-261.5-38-297.3c46.5-1.2 245-1 245-1s-8.7 367.7-14 400.4c-1.9 8-153.5 13.8-167 1zm108-28-33-57.1-32 57.1h65z"
			className="factionColorSecondary"
			clipRule="evenodd"
		/>
		<path
			d="m781 312-16 10-9 216 70 189h269l73-195s-28.8-444.7-38-480c-17.2-1-342 0-342 0l-6 35s73-12.7 113 38 32.3 134.3-25 168c-23.7 12.9-44.9 23.3-89 19z"
			className="factionColorPrimary"
		/>
		<path
			d="m841 310-7 195 36 95 183 1 36-95-19-382-177-1s-28.9-36.5-81-39c-10.7 0-25.3.8-29 2 .6-7.9 4-34 4-34s323.1 1.1 343 1c9.7 23.5 33.7 428.9 36 477-7.9 23.7-71 197-71 197l-269 2-71-192 9-216s14.4-9.9 24-9 53-2 53-2z"
			opacity={0.051}
		/>
		<path
			d="M853 1070c-30.6 20.4-100 73.3-100 171s82.6 169.8 97 177c-1.3-34.4-6-263-6-263l10 6s0-62.2-1-91zm215 1c59.2 37.3 98 90.8 98 185-3.6 93.3-89.3 160-96 163 .2-28.6 7-264 7-264l-11 7s1-64.4 2-91z"
			opacity={0.302}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M974.5 1883.4c194.8-16.6 235-367.8 249.5-451.4 18.4 41.2 28 175.1 30 202s11.3 136.1 20 174 27 37 27 37 99.1 1 119 1 29.3-1.2 37-7c16.3-104.7 21-1146.3 21-1229s-55.3-423.4-91-492c-30.8-9.6-237.4-67-254-67H787c-16.6 0-223.2 57.4-254 67-35.7 68.6-91 409.3-91 492s4.7 1124.3 21 1229c7.7 5.8 17.1 7 37 7s119-1 119-1 18.3.9 27-37 18-147.1 20-174 11.6-160.8 30-202c14.6 83.6 54.7 434.8 249.5 451.4 4.8.4 24.2.4 29 0z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M554.9 1754c15.5 26.8 33.1 58 33.1 58h-66s19.1-34 32.9-58zm812.7 0c-15.5 26.8-33 58-33 58h65.9c-.1 0-19.1-34-32.9-58z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M694 1438H449" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M642.5 1359.1c16.2 0 29.4 13.1 29.4 29.4s-13.1 29.4-29.4 29.4-29.4-13.1-29.4-29.4 13.2-29.4 29.4-29.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M643 1378.8c5.7 0 10.3 4.6 10.3 10.3s-4.6 10.3-10.3 10.3-10.3-4.6-10.3-10.3 4.6-10.3 10.3-10.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1467 1438h-245" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1469 1408h-39" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1433.8 1436c0-97.6 20.2-685.4 6.3-845.3-13.8-158.9-52.9-365.2-90.3-482.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1278 1378.7c5.7 0 10.3 4.6 10.3 10.3s-4.6 10.3-10.3 10.3-10.3-4.6-10.3-10.3 4.6-10.3 10.3-10.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1278.5 1359c16.3 0 29.5 13.2 29.5 29.5s-13.2 29.5-29.5 29.5-29.5-13.2-29.5-29.5 13.2-29.5 29.5-29.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M488 1436c0-97.6-20.3-685.2-6.3-845 13.9-158.8 52.9-365.1 90.4-482.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M450 1408h39" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M746.4 1659.8c27.9-14.1 61.2-20.3 94.8-27m238.5.2c46.9 10 79.1 20.6 95.5 27.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M769 1648c-10.8-27.9-17.4-53.9-19-63 10.6-3.8 15-5 15-5s10.6 43.2 22 64"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M851 1419c-34-28.6-97-86.3-97-179s62.2-150 99-170" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1069.4 1419.2c33.9-28.6 96.7-86.2 96.7-178.9s-62-149.9-98.7-169.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M851 1438c-62.5-45.4-112-112.3-112-205s70.7-158.1 112-178"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1070 1438c62.6-45.4 112-112.3 112-205s-70.7-158.1-112-178"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M648 86c-18.5 796.1 41.8 1271.8 48 1353 .6.1 1.2.6 1.2-.1 8.9-80.5 44.8-657.3 46.8-690.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m669 406 64.7-10-1 15-63.7 10v-15zm0 40 62.7-10-1 15-61.7 10v-15zm0 40 60.7-10-1 15-59.7 10v-15zm584-80-64.6-10 1 15 63.7 10-.1-15zm0 40-62.7-10 1 15 61.7 10v-15zm0 40-60.7-10 1 15 59.7 10v-15z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1272.5 86.1c18.6 795.9-42 1271.5-48.2 1352.6-.6.1-1.2.6-1.2-.1-9-80.5-45-657.1-47-690.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M805.5 84C868.7 84 920 135.3 920 198.5S868.7 313 805.5 313 691 261.7 691 198.5 742.3 84 805.5 84z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M805.5 107c50.5 0 91.5 41 91.5 91.5S856 290 805.5 290 714 249 714 198.5s41-91.5 91.5-91.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m740 169 29 9 38-25 37 25 28-11" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M778 259c-5.4-2.1-26-18 30-18s31.2 17.1 28 18" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m866 257 8-90-68-51-69 52 9 89h26c14.5 6.8 56.8 6.1 68 0h26z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m807 154-1-36" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m843 180-3 46h-67l-3-46" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m841 227 25 29" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m771 228-23 27" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M901 1638h115v31H901v-31zm0 80h115v31H901v-31z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1079 1816v-237l-19-47-6-1 4-82 9 8 10-305-12 10 2-128 16-33-1-249H838l-1 249 16 33 2 128-12-10 10 305 9-8 4 82-6 1-19 47v237h238z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M842 1579h237" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M868 1531h186" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m837 988-16-1 4-258h270l5 265h-16" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m819 988-74-230 20-437" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M841 308c.2 2.7-9 196-9 196l38 96h183l36-96-19-382H890"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M810.8 82.5C802 70.4 790 54 790 54" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m782 86 4-34" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1069 124 58-71" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1131 52c9 29.6 47.1 601.2 45 694-21.6 71.8-75 251-75 251"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m756 534 69 196 45-133" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1164.3 533.7-68.7 196-44.8-133" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m832 507-76 30" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1090.8 506.6 76.3 29.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M788 312c-14 2.3-26 9-26 9s-90-25.5-90-121S744.1 84 814 84"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m838 1001 245 1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M854 1034h213" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m855 1162 79 65v163l-74 60" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1066 1162-79 65v163l74 60" />
		<g id="Animation">
			<radialGradient
				id="ch-topdown64000d"
				cx={996.5}
				cy={-793.5}
				r={82.5}
				gradientTransform="matrix(1 0 0 1 0 1120)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.15} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#bfbfbf" />
			</radialGradient>
			<path
				fill="url(#ch-topdown64000d)"
				d="M996.5 244c45.6 0 82.5 36.9 82.5 82.5s-36.9 82.5-82.5 82.5-82.5-36.9-82.5-82.5 36.9-82.5 82.5-82.5z"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M996.5 244c45.6 0 82.5 36.9 82.5 82.5s-36.9 82.5-82.5 82.5-82.5-36.9-82.5-82.5 36.9-82.5 82.5-82.5z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m927 282 8 14s-7.5 19.6 0 60c-3.4 10-7 18-7 18" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1066.6 282.5-8.1 14.1s7.6 19.7 0 60.3c3.5 10.1 7.1 18.1 7.1 18.1"
			/>
			<path fill="#f2f2f2" d="M961 375h70l-5 316-30 21-31-21-4-316z" />
			<path fill="#999" d="m962 431 70 1v11l-70-1v-11zm0-14h69l-1-12-67-1-1 13z" />
			<linearGradient
				id="ch-topdown64000e"
				x1={996}
				x2={996}
				y1={-728}
				y2={-746}
				gradientTransform="matrix(1 0 0 1 0 1120)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={1} stopColor="#fff" />
			</linearGradient>
			<path fill="url(#ch-topdown64000e)" d="m963 392 66-1-1-16-65-1v18z" opacity={0.502} />
			<path fill="#8c8c8c" d="m966 689 58 2-29 23-29-25z" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="M961 375h70l-5 316-30 21-31-21-4-316z" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="M964 689h61" />
			<path fill="#f2f2f2" d="M952 392h88v12h-88v-12zm0 25h88v12h-88v-12zm0 25h88v12h-88v-12z" />
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M952 392h88v12h-88v-12zm0 25h88v12h-88v-12zm0 25h88v12h-88v-12z"
			/>
		</g>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-topdown84001a"
			x1={937.5}
			x2={937.5}
			y1={143}
			y2={98}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#adadad" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-topdown84001a)" d="M908 1777v45h59v-44" />
		<path
			fill="#f2f2f2"
			d="M894.5 224.3C811.8 214.4 561 99 561 99l-16 9s-2.4 60.5-3 81c119.8 71.5 287 198 287 198s33.1 12.4 35 44-37.3 324.9-126 447-370 191-370 191v-71l-24-66h-38.1c-3.7 0-10.5 30-14.4 30h-31.8c-3.2 0-15.3-30-18.3-30h-38.5l-25 68v310h102c53.8 0 80.3.3 86-1 0-8.9 1-108 1-108s297 59.8 351 124 56.5 156.5 62 195 58.4 265 158 265c95.1 0 138.6-192.7 150-232 12.3-42.5 22.5-158.3 54-211 40-66.9 363-144 363-144l1 112s7.6.3 100 0c79.4-.2 84.3.4 90 0 0-9.6-1-310-1-310l-25-68H1631c-1 0-11.9 30-12.9 30h-37c-.8 0-11.6-30-12.3-30H1530l-25 69s1.5 55.8 1 67c-33.8 3.9-292-89.2-370-192s-126-386.9-126-443c-.5-37 33.1-48.1 39-51 44.9-22 225.8-155.6 281-193 .3-10.3 0-81 0-81s-8-3.7-17-9c-29.2 24.5-319 127-319 127s-4.8.6-14.2.3c-4-37.6-12.1-62.8-21.8-76.8-13.3-2.2-35.6-2.7-41.6-.2-10.5 14.4-18.9 40-21.9 75z"
		/>
		<path
			fill="#d9d9d9"
			d="m1313 102 18 6-1 83-307 207-19-16 307-200 2-80zm-754-2-16 6 2 88 303 203 23-15-309-200-3-82z"
		/>
		<linearGradient
			id="ch-topdown84001b"
			x1={1148.0945}
			x2={937.0945}
			y1={1209.5242}
			y2={1162.6301}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a1a1a" />
			<stop offset={0.492} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001b)"
			d="M938 1091h159s-.8-199 52-199c-24.4-20.1-59.9-82.7-81-160-18.9-69.2-37.6-150.5-49-214-15.8-87.9-5.9-118.5 6-122-5.8-4-22.8-11.3-32-11-27.1.8-55 3-55 3v703z"
			opacity={0.702}
		/>
		<path fill="#f2f2f2" d="M208 1096h28v138h-28v-138zm101 0h28v138h-28v-138z" />
		<linearGradient
			id="ch-topdown84001c"
			x1={722.5486}
			x2={852.2656}
			y1={530.6865}
			y2={563.0286}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={0.456} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001c)"
			d="M816 1651c40.5-28.4 37.1-438.2 34-560-29.5.1-38.4-.3-74 0 2.2 81.3-.6 218.8-54 237 70.2 80.2 41.4 213.2 94 323z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-topdown84001d"
			x1={1175.7748}
			x2={1043.7758}
			y1={522.8947}
			y2={558.2637}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.597} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001d)"
			d="M1054.8 1650c-40.6-28.4-37.2-438.1-34.1-559.9 29.6.1 38.5-.3 74.2 0-2.2 81.2 2.6 219.7 56.1 237.9-52.5 66.5-43.5 212.2-96.2 322z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-topdown84001e"
			x1={824.5826}
			x2={949.5826}
			y1={218.2531}
			y2={290.4211}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#434242" />
			<stop offset={0.301} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001e)"
			d="M841 1547c-1.4 19.1-11.6 101.7-27 105 8.6 21.4 44.6 133 125 133-1.1-102.3-.7-193 0-259-49.8-34.2-38.3-8.7-98 21z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-topdown84001f"
			x1={1049.4327}
			x2={931.4327}
			y1={217.121}
			y2={285.248}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#494949" />
			<stop offset={0.367} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001f)"
			d="M1034 1548.1c1.4 19.1 7.6 98.7 23 102-8.6 21.4-46 135.4-118 134.9 1.1-102.2.7-192.7 0-258.6 28.7.3 53.6-.4 95 21.7z"
			opacity={0.902}
		/>
		<radialGradient
			id="ch-topdown84001g"
			cx={116.273}
			cy={1876.238}
			r={1376.17}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.747} stopColor="#000" />
			<stop offset={0.8} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown84001g)"
			d="M368 1069c177.2-42.8 313.6-125.5 360-177 20.7-1.4 34.6 44.2 40 82s9 87.7 9 161H367s.4-31.4 1-66z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown84001h"
			cx={1569.353}
			cy={1481.782}
			r={1329.906}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown84001h)"
			d="M1508 1069c-177.2-42.8-313.6-125.5-360-177-20.7-1.4-34.6 44.2-40 82-5.4 37.8-9 87.7-9 161h410s-.4-31.4-1-66z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-topdown84001i"
			x1={1058.0023}
			x2={814.9685}
			y1={400.5001}
			y2={400.5001}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.313} stopColor="#e6e6e6" />
			<stop offset={0.606} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001i)"
			d="M934 1425c24.3 0 63.3-.8 97 34 27.8 28.7 27 79 27 87s-1.8 32.7-18 39c-20 7.8-41.8 4.4-62.9.3-1-.2-12.1 28.9-13.1 28.7-14-2.8-37.6-2-50-2-10.2 0-12.7-22.3-17.5-28.9-24.7 3.7-51 7.6-63.5.9-19.6-10.5-18-37.5-18-42s1.3-64 29-86 47.4-31 90-31z"
		/>
		<linearGradient
			id="ch-topdown84001j"
			x1={728.73}
			x2={937.73}
			y1={1206.3823}
			y2={1158.1313}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.458} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001j)"
			d="M846 396c5.8-.7 13.9-10 28-11 18.7-1.4 45 5.3 63 4-35.6 6-65.2 146-72 246s-18.4 287.1-14 461c-46.7-.3-76 1-76 1s6-191.6-47-205c60.1-69.4 107.8-243.5 123-354 12.8-93.1 23.4-125.7-5-142z"
			opacity={0.702}
		/>
		<path
			fill="#fff"
			d="M985 1458.9c.3 0 .5.3.7.7 0 9.8.2 28.6.3 34.3.2 6.3-.4 13.7 0 32-3 .1-4.3-10.4-6-32.8 1-13.8 1.9-19.8 5-34.2z"
		/>
		<path
			d="M178 998s192.8-2.1 192-1-26-65-26-65h-40l-11 30h-36l-16-29h-38l-25 65zm59 99v137h-29v-138l29 1zm73 0s-.7 138.2-1 138 28 0 28 0l-1-138h-26zm503 560c6.4-6.5 19.5-36.5 22-69 16.1-1.1 49.9-2.8 61-5 7.3 15.8 13 30 13 30s45-4.6 55-1c8.8-17.8 13-27 13-27s46.5 8.6 59 2c4.1 19 6.7 56.9 25 64-9.2 29.6-49.7 134-126 134s-107.5-104.3-122-128zm-429-452s242.7 44.3 334 122c16.1-4.7 38.7-26.6 48-80-39-6.3-299-44-299-44s-33-5.3-35-29c-.1-6.5 2-16 2-16l-50-2 3-90-18 4-1 132 16 3zm390-41 31-31v-63s-31.3-18.5-31-18 4.3 88 0 112zm325-112-20 18-1 67 20 20 1-105zm463 45 3 135-31 2v-136l28-1zm73-1 27 1 3 136-30 2v-139zm-146 109s-242.7 44.3-334 122c-16.1-4.7-38.7-26.6-48-80 39-6.3 299-44 299-44s33-5.3 35-29c.1-6.5-2-16-2-16l50-2-3-90 18 4 1 132-16 3zm205-207-26-65h-36l-17 31-35-1-15-30h-36l-25 65h190z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-topdown84001k"
			cx={278.796}
			cy={-251.27}
			r={1009.577}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.901} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown84001k)"
			d="M367 1200s271.4 49.5 354 127c54.8-14.2 55.9-145.4 57-192-23.2-.2-180.5.3-409 0-.2 34.9-2 65-2 65z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-topdown84001l"
			cx={1602.5389}
			cy={-210.1221}
			r={993.886}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.9} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown84001l)"
			d="M1506 1197s-112.2 24.5-218 63c-50.9 18.5-105.1 41.8-132 67-54.8-14.2-55.9-145.4-57-192 23.2-.2 178.5.2 407 0 .2 34.9 0 62 0 62z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown84001m"
			x1={1508}
			x2={1696.0035}
			y1={798.47}
			y2={798.47}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001m)"
			d="M1509 1311.1c36.6-.9 162.6-2.1 187-4 .1-34.5-2-307-2-307l-24.6-68h-38.2c-2.4 0-10.8 30-13.3 30h-37.6c-2 0-9.9-30-11.8-30h-35.9l-24.6 67c0-.1.8 266.9 1 312z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown84001n"
			x1={180}
			x2={368.0035}
			y1={798.47}
			y2={798.47}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001n)"
			d="M181 1311.1c35.4-1.5 159-1.7 187-4 .1-34.5-2-307-2-307l-24.6-68h-36.2c-6.2 0-6.7 30-13.5 30h-33c-9.2 0-8.3-30-16.7-30h-37.4L180 999s.8 267 1 312.1z"
			opacity={0.2}
		/>
		<path
			d="M876 385 563 180l-1-79s34.4 13.4 80.5 31.5c1.6 4.2 9.7 24.3 10.7 28.6 44 17.2 96.2 37.6 139.3 54.5 5-6.6 15-11.9 18.6-17.1C850.7 213.9 879 225 879 225s8.8.7 15.9 1.3c.2-14.7 7.7-58.6 20.1-78.3 5.8.4 32.6 1.5 45 2 14.7 17.9 18.5 69.6 18.9 76.1 6.7-.5 14.1-1.1 14.1-1.1s28.3-11.1 67.9-26.6c3.6 5.3 13.6 10.5 18.6 17.1 43.1-16.9 95.4-37.3 139.3-54.5.9-4.2 9.1-24.3 10.7-28.6 46.1-18 80.5-31.5 80.5-31.5l-1 79L996 385H876z"
			className="factionColorSecondary"
		/>
		<path d="m1000 384 309-202 2-82-316 127-1 154 6 3zM562 102v82l308 199 10 1 1-159-319-123z" opacity={0.102} />
		<linearGradient
			id="ch-topdown84001o"
			x1={980}
			x2={894.941}
			y1={1721.6068}
			y2={1721.6068}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84001o)"
			d="m938 250 31-25h11s-7.7-69.6-21-78c-20.8-1-45 2-45 2s-20.3 41.4-19 77c5.5-.8 10 0 10 0l33 24z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown84001p"
			cx={935.5834}
			cy={1545.1593}
			r={174.6569}
			gradientTransform="matrix(1 0 0 1.6413 0 -990.8658)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.6293} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown84001p)"
			d="M839.8 1587c18 3.5 41.2-.5 52.3-.9s12.8 15.9 20.2 27.8c6.5-2.5 41.3-.8 49.3 0s11.3-18.3 14.8-24.6 42.7-.6 60-2.1c3 22.3 12.8 61.1 21.9 65.3-9.7 36.1-55.7 135.9-125.2 131.4s-82.8-54-120-128l14.9-27.3 11.8-41.6z"
			opacity={0.5}
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M908 1777v45h59v-44" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M910.6 1612.8c-1.3.2-12.7-29.6-14-29.4-25 3.6-51.3 7.1-63.5.6-19.6-10.5-18-37.5-18-42s1.3-64 29-86 47.4-31 90-31h5.8c42.6 0 62.4 9 90.1 31 27.7 22 29 81.5 29 86.1s1.6 31.5-18 42c-12.2 6.5-38.6 3-63.6-.6-1.4-.2-12.7 29.6-14 29.4-13.7-2-16.9-2-26.3-2s-12.9-.1-26.5 1.9z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m368 1069-1 133" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m179 1029 190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m179 999 190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M386 1064v140" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M385 1155c79 4.2 391 21 391 21" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M433 1158c-1 11.4-2.3 28.5 9 34 27.9 13.6 104.2 21.8 324 54"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1505 1069 1 131" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1695 1029-190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1695 999-190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M938 250v136" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m546 192 17-9 311 201s-16.3 8.1-27.8 11.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1328 192-17-9-311 201.2s16.3 8.1 27.8 11.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M880 227v157h120" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M994 384V227" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M562 181v-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1311 181v-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1485 1064v140" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1488.5 1155c-79.8 4.3-390.5 21-390.5 21" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1441 1160c1 11.4 2.3 26.5-9 32-27.9 13.6-104.2 21.8-324 54"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M727 891c37.3 0 48.1 106.6 50.2 214.3 2.2 109.8-15.9 220.7-58.2 220.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m777 1160 29-25v-64l-31-21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1097 1162-19-27v-64l21-21" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1147.1 891c-37.3 0-48.1 106.6-50.2 214.3-2.2 109.8 15.9 220.7 58.2 220.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M815 1651c28.1.6 40.4-269.4 35.5-517.6-4.1-211 2.4-721.6 88.5-746.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1058.9 1651.5c-28.1.6-40.5-269.4-35.6-517.6 4.1-211.1.6-721.7-85.6-746.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M853 1305h170" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M853 1115h170" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M853 945h170" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M853 785h170" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M866 645h144" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M887 495h104" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M817 1519c22.5-52.3 188.7-79.4 240.6.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M822.6 1569.8c19.1-54.4 191.5-66.5 229.1 0" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M208 1096h28v138h-28v-138zm101 0h28v138h-28v-138zm1226 0h28v138h-28v-138zm101 0h28v138h-28v-138z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M937.1 1784.9c110.8-3 150.3-225.8 155.8-264 5.5-38.5 8-130.8 62-195.1s351.1-124 351.1-124 1 100.1 1 109c18.8.5 188 0 188 0v-310.1l-25-68h-36.9c-6 0-12.5 30-19.1 30h-33c-4.5 0-10.9-30-15.1-30h-37l-24 66v71s-281.3-68.9-370.1-191c-88.7-122.1-128-416.6-126-448.1 1.9-31.6 22-37 22-37l300-204.1v-82l-17-8S996.7 224.1 993.2 225.8c-9-.3-17.3-.4-24.5-.3C943.1 246.4 937 251 937 251s-7.3-5-33-26c-7.2-.1-15.5 0-24.5.3C876 223.6 559 99 559 99l-17 8v82l300 204s20.1 5.4 22 37-37.3 325.9-126 448-370 191-370 191v-71l-24-66h-37c-4.2 0-10.6 30-15.1 30h-33c-6.6 0-13.1-30-19.1-30h-36.9l-25 68v310s169.2.5 188 0c0-8.9 1-109 1-109s297 59.8 351 124 56.5 156.5 62 195c5.5 38.2 44.9 262 155.8 265 .8 0 .7-.1 1.4-.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M895.1 224.6c.1-6 .4-12 1-18 2.8-27.3 10.3-47.2 19.4-59.1 5.7 0 37-.1 42.6 0 9.1 11.9 16.6 31.7 19.4 59 .6 6.4 1 12.8 1 19.1"
		/>
	</svg>
);

export default Component;

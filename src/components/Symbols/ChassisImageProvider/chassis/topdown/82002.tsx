import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-topdown82002a"
			x1={958.71}
			x2={958.71}
			y1={238.99}
			y2={292.15}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path fill="url(#ch-topdown82002a)" d="M934.2 1627.8v53.2h49.1v-53.2s-11 7.2-23.1 7.2c-12.5 0-26-7.2-26-7.2z" />
		<linearGradient
			id="ch-topdown82002b"
			x1={763.9759}
			x2={1153.4578}
			y1={1090.7979}
			y2={1097.5962}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8f9bb0" />
			<stop offset={0.399} stopColor="#dae3f2" />
			<stop offset={0.604} stopColor="#dae3f2" />
			<stop offset={1} stopColor="#8f9bb0" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82002b)"
			d="M958.7 239.2c-32.4 6.7-61.3 6.3-171.7-221.2-7.1 1-13.4-2.7-23-2 1.1 23.9 3.7 70.4 8 112 25.3 74.2 44.6 144.7 54 180 15.1 53.4 8.3 60.3 2.7 74.2-30.6 63.7-27.5 214.7-27.5 275.9s9.5 242.2 15.9 336.6c6.4 94.4 20.1 317.9 37.6 398.7 17.5 80.9 56.4 245.6 102.6 245.6 61.3 0 92.8-178.5 105.4-244.1 23.7-123.1 31.6-329 36.1-393 4.5-63.9 14.5-268.9 14.5-348.1 0-79.3 4.2-249.5-27.5-288.9-18.5-33.5 68.7-197.4 67.9-339.5.7-13.2-17.2-6.7-20.2-10.1-104.6 222.8-128.5 214.3-174.8 223.9z"
		/>
		<linearGradient
			id="ch-topdown82002c"
			x1={854.9905}
			x2={1062.9969}
			y1={1649.2294}
			y2={1652.8595}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#adb8ca" />
			<stop offset={0.497} stopColor="#bac6d9" />
			<stop offset={1} stopColor="#adb8ca" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82002c)"
			d="M1032 205c-55 45.9-118.2 32.9-146 2-6.1 29.9-31 127-31 127s52.7-29 105-29 103 25 103 25-23.6-94.4-31-125z"
		/>
		<path fill="#8d96a6" d="M1126.8 127 1065 330l-33-129 106-185-11.2 111z" />
		<path fill="#8d96a6" d="m794 128 58.8 196.6 29.5-121.9S808.9 60.7 789 24.1c-3-5.5-7-6-7-6L794 128z" />
		<linearGradient
			id="ch-topdown82002d"
			x1={1117.0734}
			x2={1081.0693}
			y1={953.9929}
			y2={922.2299}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#bbc5d7" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown82002d)" d="m1065.3 924.9 29.2 147.9 5.8-131.5-35-16.4z" />
		<linearGradient
			id="ch-topdown82002e"
			x1={835.6085}
			x2={801.6055}
			y1={924.7089}
			y2={954.7079}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#bbc5d7" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown82002e)" d="m854 925.4-28.2 147.4-5.8-131.5 34-15.9z" />
		<linearGradient
			id="ch-topdown82002f"
			x1={-161.2253}
			x2={481.4587}
			y1={1114.8855}
			y2={773.1645}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#343434" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82002f)"
			d="M848.9 922.5c-29.7-134.1-61-254.7-40.5-501.3-73.1 39.2-445.9 421-574.9 895.7-45.7 547.5-22.7 590.9-11.6 590.9 11.2 0 341.5-836 627-985.3z"
		/>
		<linearGradient
			id="ch-topdown82002g"
			x1={2103.2566}
			x2={1460.4364}
			y1={1384.6014}
			y2={805.8044}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#343434" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82002g)"
			d="M1069.9 922.5c29.8-134.1 61-254.7 40.5-501.3 73.2 39.2 446 421 575.1 895.7 45.7 547.5 22.7 590.9 11.6 590.9-11.2 0-341.5-836-627.2-985.3z"
		/>
		<linearGradient
			id="ch-topdown82002h"
			x1={877.8886}
			x2={1040.9259}
			y1={639.6892}
			y2={642.5343}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.148} stopColor="#333" />
			<stop offset={0.497} stopColor="#b3b3b3" />
			<stop offset={0.844} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82002h)"
			d="M947.3 1451v-344.6s-19.3-1.4-53.3 24.5c-.7 1.7-7.7 39.4-16.1 107.3 13.7 79.4 31.5 163.7 43.5 207 2 2.4 3.5 4 6.1 6 15.1.2 19.8-.2 19.8-.2zm24.5-.2v-344s19-2.8 53.1 24.5c.7 1.7 7.7 39.3 16.1 107.1-13.6 79.2-31.4 163.4-43.4 206.7-2 2.4-3.5 4-6 6-16.2.1-19.8-.3-19.8-.3z"
		/>
		<path
			d="M217 1545c24.2-71.6 158-437 209-508 27.3 24.3 137 133 137 133s-103.6 145.4-172 290c-35.9 76-86.1 175-124 257-34.2 74-58 135-58 135s-13.9-76.6 8-307zm1484.6 0c-24.2-71.6-157.9-437-208.8-508-27.2 24.3-136.9 133-136.9 133s103.5 145.4 171.8 290c35.9 76 86 175 123.9 257 34.2 74 57.9 135 57.9 135s14.1-76.6-7.9-307z"
			className="factionColorPrimary"
		/>
		<path
			d="m555 1130-57-60-32 3s-10.9-11.3-37-36c104.4-212.2 254-371 254-371s-26.1-40.7-46-77c47.7-77 160.9-160.3 181-173 21.5-23.7 84.9-43 141-43h1c56.1 0 119.5 19.3 141 43 20.1 12.7 133.3 96 181 173-19.9 36.3-46 77-46 77s149.6 158.8 254 371c-26.1 24.7-37 36-37 36l-32-3-57 60 2 31-40 39s-87.6-173-258-277c-1.4-2-3.3-4.4-5.7-6.9-13.3-10.8-46.4-32.6-97.2-34-1.9 0-3.7-.1-5.6-.1s-3.8 0-5.6.1c-50.8 1.4-83.9 23.2-97.2 34-2.4 2.5-4.2 4.9-5.7 6.9-170.4 104-258 277-258 277l-40-39 2-31zm522-677c-11.8-6.8-62.8-26.4-114.8-27h-5.4c-52 .5-93.8 11-117.3 23.6-12.1 6-23.4 13.4-33.5 22.4-19.3 141.6 20.7 356.1 30 394 8.3-13.9 47.8-38 121-38h5c73.2 0 110.7 34.1 119 48 9.3-37.9 51.3-259.4 32-401-10.1-9-33.6-20.6-36-22zM784 899c0 8.8 7.1 18 16 18s17-6.8 17-18-9.7-15-17-15-16 6.3-16 15zm-28-398c3.1 6.5 13.2 5 19 1s12.4-11.8 8-18-14.1-4.3-20-1-10.1 11.5-7 18zm363 383c-7.3 0-17 3.8-17 15s8.1 18 17 18 16-9.3 16-18-8.7-15-16-15zm37-401c-5.9-3.3-15.6-5.2-20 1s2.2 14 8 18 15.9 5.5 19-1-1.1-14.7-7-18z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown82002i"
			x1={805.7457}
			x2={1114.7488}
			y1={1490.999}
			y2={1496.3923}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82002i)"
			d="M1115 477c-.8-27.1-3.3-47.6-8-57-16.8-10.4-67.1-47-150-47s-139 44-139 44-14.4 29.9-12 55c11.2-13.8 55.6-47 158-47s140.2 47.5 151 52z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown82002j"
			x1={835.2369}
			x2={1081.0922}
			y1={1041.4259}
			y2={1045.7173}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82002j)"
			d="m835 865 14 57s22.9-39 115-39c70.5 0 107 39 107 39l10-53s-47.4-40-127-40-119 36-119 36z"
			opacity={0.2}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M947.3 1451v-344.6s-19.3-1.4-53.3 24.5c-.7 1.7-7.7 39.4-16.1 107.3 13.7 79.4 31.5 163.7 43.5 207 2 2.4 3.5 4 6.1 6 15.1.2 19.8-.2 19.8-.2zm24.5-.2v-344s19-2.8 53.1 24.5c.7 1.7 7.7 39.3 16.1 107.1-13.6 79.2-31.4 163.4-43.4 206.7-2 2.4-3.5 4-6 6-16.2.1-19.8-.3-19.8-.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M934.2 1627.8v53.2h49.1v-53.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M908.2 1120.4V887.6m0-53V711c0-103.7 3.1-207.3 6.2-282.5m2.2-52.3c1.8-38.6 3.2-61.9 3.2-61.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1012.2 1120.4V890.1m0-52.9v-126c0-102.7-3.1-205.1-6.1-280.1m-2.3-54.3c-1.8-38.7-3.2-62.1-3.2-62.1-.5-5.5-.6-6.7-.6-6.7"
		/>
		<path
			fill="#c4cedf"
			d="M1136.4 19.6 1126.3 128l20.2 1.4 8.7-111.2-18.8 1.4zm-353.9 0L792.6 128l-20.2 1.4-8.7-111.2 18.8 1.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1100.4 940.4c-4.4 140-10.1 263.7-16 304.1-26.2 179.2-52.1 391.7-124.2 391.7-43.3 0-84.7-93-125.7-391.7-12-87.5-14.1-191.9-18.8-297.4-.1-1.3-.1-2.6-.2-3.9m-10.1-491.9c6.2-21.2 11.8-34.8 14.6-39.3 5.8-22.2 11.7-42.6 17.6-61.2-25.1-78.4-40.4-147.9-66.7-224.2-1.9-29.3-5.6-82.3-8.7-112.7 9.3.6 18 3.5 26 4.3C810 63 839 139.4 886.5 204.5c31.2 42.8 100.6 41.5 141.6 1.5 15.5-15.1 64.3-108.4 106.9-192.1 5.1.7 12.9 1.4 20.2 2.9-.3 29.7-4.2 71-9.7 115.8-32.1 95.4-63.5 202.9-62.5 210.7 8.2 23 10.1 36.3 23.1 73.7 1.2 2.7 2.8 6.9 4.6 12.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 237v70" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M916.6 1600c0-14.8 31.9-18 42.6-18s47.2 3.2 47.2 21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m853.3 338.9 31.3-135.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1061.9 328.2c-4.8-20.8-29.5-128-29.5-128" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M838.1 351.1c14.4-20.8 68.3-44 122.1-45.4 54.5-1.4 109.1 20.7 122.7 37.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M848.9 922.5c-29.7-134.1-61-254.7-40.5-501.3-73.1 39.2-445.9 421-574.9 895.7-45.7 547.5-22.7 590.9-11.6 590.9 11.2 0 341.5-836 627-985.3zm221 0c29.8-134.1 61-254.7 40.5-501.3 73.2 39.2 446 421 575.1 895.7 45.7 547.5 22.7 590.9 11.6 590.9-11.2 0-341.5-836-627.2-985.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M849 922c7.7-15.1 40-40 111-40s111 41 111 41" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M835.5 869.4c8.4-15.2 44-40.4 122-40.4s122 41.4 122 41.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M814 422.8c10.1-18.4 55.7-48.8 149-48.8s146 50 146 50" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M805.2 475.1C815.7 456.6 862.3 426 960 426c97.7 0 152.8 50.3 152.8 50.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M216.8 1551c60.9-173.1 141.9-387.7 207-507.4 2.5-4.6 5-9.1 7.4-13.6C523 862.5 608.9 743 685 669c-33.3-53.6-50-80-50-80"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1703.3 1550.3c-60.8-173-141.8-387.5-206.8-507.2-2.5-4.6-5-9.1-7.4-13.6-91.8-167.4-177.6-286.9-253.7-360.9 33.3-53.6 50-80 50-80"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M429 1039s104.3 101.7 163 159" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1490.5 1038.6s-104.3 101.6-163 158.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M562 1170c-36.6 44.9-94.8 146.2-155.5 260.4-74 139.1-151.6 299.4-198.5 424.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1356.9 1170.2c36.6 44.9 94.9 146.2 155.7 260.5 74 139.2 151.8 299.4 198.7 424.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m465 1072 31-2 59 59-1 32" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1454.7 1072.1-30.9-2-58.8 58.8 1 31.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1119.4 884c9.1 0 16.4 7.4 16.4 16.5s-7.3 16.5-16.4 16.5c-9.1 0-16.4-7.4-16.4-16.5s7.3-16.5 16.4-16.5zm36.5-401.9c-7.7-4.7-16.8-4-20.2 1.6-3.4 5.6.1 14 7.9 18.7 7.7 4.7 16.8 4 20.2-1.6 3.3-5.5-.2-13.9-7.9-18.7zM800 884.5c9.1 0 16.5 7.4 16.5 16.5s-7.4 16.5-16.5 16.5-16.5-7.4-16.5-16.5 7.4-16.5 16.5-16.5zm-36.7-402.2c7.8-4.8 16.9-4 20.3 1.6 3.4 5.6-.1 14-7.9 18.8-7.8 4.8-16.9 4-20.3-1.6-3.4-5.6.1-14 7.9-18.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m653 703-47-79" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1265.3 702.4 47.3-78.9" />
	</svg>
);

export default Component;

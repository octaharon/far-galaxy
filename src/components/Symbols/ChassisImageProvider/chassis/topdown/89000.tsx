import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#b3b3b3"
			stroke="#003"
			strokeWidth={5}
			d="M662 1540.4c24.2 20.2 31.3 26.5 31.3 26.5h528.1s19.3-15.6 32.5-26.5c-38.2-.5-161-16.9-293-16.9-131.9.1-206.6 12.3-298.9 16.9z"
		/>
		<path
			fill="#b3b3b3"
			stroke="#003"
			strokeWidth={5}
			d="M1222.6 1566.9c14.4 16.4 102.3 85.9 147.1 89.2-.6-.3-1.1-.7-1.7-1-47.4-27.8-54.9-39.5-108-113.5-3.1-.6-6-1.2-6-1.2s-17.2 15.7-31.4 26.5zm-528.8-.3c-14.4 16.4-95 83.5-139.6 86.8.6-.3.4-1 1.7-2.2 27.4-16 47.5-35.9 100.7-109.9 3.1-.6 6 0 6 0s17.1 14.5 31.2 25.3z"
		/>
		<linearGradient
			id="ch-topdown89000a"
			x1={958.6411}
			x2={958.6411}
			y1={366.3101}
			y2={396.4399}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89000a)"
			stroke="#003"
			strokeWidth={5}
			d="M1085.2 1527.2s15.7 19.8 23.5 25.3c1.1.8.6 1.3.6 1.3H807.9s-.2-.7 1.6-2c8.7-6.1 25-23.3 25-23.3s81.4-4.8 126.3-4.8c44.2-.1 124.4 3.5 124.4 3.5z"
		/>
		<path
			fill="#f2f2f2"
			d="M233.4 388.6c-17.5-12.7-28.3-28.9-28.3-49.7 0-32.8 20-86.9 60.9-120.2.7-.6.3-25.1 1-25.6 3.7-2.9 107.9-53.6 205-71 .1 6.2 1.2 17.3 1 20.7 153.2-36.1 338.6-61.5 397.6-72.6C883.4 39.8 895.1 7.5 961 7.5c30.5 0 62.6-.7 89.2 62.7 45.8 7.6 239.5 33.2 395 68 .6.1 1.2-16.3 1.8-16.1 152 34.1 187.2 63.7 206 71 .5.5-.3 25.4.1 25.9 58.4 62.3 61.4 100.9 61.4 120 0 10.6 1.8 29.4-32.2 49-3.8 2.2 23.6 70.7 28.7 92.1-5.5-2.4-23.6-13.2-30-15-.6.2-23.3-65.5-23.9-65.3-7.8 3.1-16.6 6.1-26.6 9.1-.4.1 16.1 42.3 15.5 42.1-7.5-2.3-17.4-7.5-23-6-.5.1-10.6-31.1-11.1-31-33.6 8.6-78.2 16.8-137.3 23.7-213.3 46.9-280.9 224.8-280.9 332.6-3.4 142.9 99.9 312.3 286.9 344.7 54.7 6.2 96.7 15.6 128.8 26.7.5.2 13.1-31.9 13.5-31.7 6.9-1.8 19.4-6 30-10-2.4 9.1-20.8 50.6-19.2 51.3 11.5 5.2 21.4 10.6 29.8 16.3.5.3 33.9-87.9 34.4-87.6 2.2 1.5 33.6-18.3 47-30-7.8 31.4-52.4 135.5-54.9 141.3 15.3 19.1 18.2 38.6 15.9 55-6.1 43.6-77.6 242.6-235.1 384.4-39.6 37.9-92.8 26.5-92.8 26.5s-33.7 3.6-119.4-116.9c-69.8 0-135.4-16.9-297.8-16.9s-249.7 16.9-303.8 16.9c-28.4 41.5-71.7 99.3-113.3 114.5-41.6 9.6-92.8-24.1-92.8-24.1S286.2 1480 217.1 1275.3c-11.9-35.2-4.4-62.3 13.3-83.2-2.7-6.2-58.4-142.1-58.4-142.1s44.6 27.7 50 31c4 10.6 34 86.1 34.8 88.2 9.4-6.2 19.8-11.7 30.5-16.5 1.3-.6-22.6-52.1-21.3-52.7 3.9 1.3 22.6 7.7 31 10 4.3 9 12.6 29.5 14.3 33.2 34.2-12 69.1-18.7 90.3-23.4 144.6 0 323.1-141.6 323.1-341.1 0-79.1-45.1-292.3-280.9-339.9-39.8-4.1-95.3-9.1-142.7-21.6-1.7-.4-8.6 20.8-11 27.6-5.8 1.4-18.5 5-24 6-.9-.3 16.3-39.1 15.4-39.4-8.3-2.7-16.2-5.8-23.6-9.1-3.3 8.4-21.9 62.7-21.9 62.7l-33 17s28.6-88 30.4-93.4z"
		/>
		<linearGradient
			id="ch-topdown89000b"
			x1={1077.9403}
			x2={1427.0813}
			y1={1060.6569}
			y2={1066.7509}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89000b)"
			d="M1122.5 82.2c34.5 257.7-36.5 1330.6-44.6 1443.8 40 3 143.3 14.1 182 14.5 73.1 110.2 119.4 118.1 119.4 118.1s64.9-165.1 43.4-323v-235s-182.9-49.7-225.4-274.8c-3.3-17.2-38.5-267.1 225.4-373.6v-317s-86.7-19.6-179.3-35.6c.7-6 1.6-25.5 1.5-32.6-19.5-11.7-76.8-22.3-111-4-.1 3.7.9 16.7 1 20.5-4.1-.4-8.3-.9-12.4-1.3z"
		/>
		<linearGradient
			id="ch-topdown89000c"
			x1={486.6693}
			x2={841.6232}
			y1={1061.4719}
			y2={1067.668}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89000c)"
			d="M797 82.2c-34.5 257.7 36.5 1330.6 44.6 1443.8-40 3-143.3 14.1-182 14.5-73.1 110.2-119.4 118.1-119.4 118.1s-81.6-252.7-43.4-323v-235s182.9-49.7 225.5-274.8c3.3-17.2 38.5-267.1-225.5-373.6v-317s86.5-17.5 179-33.6c-.5-4.2-5.6-30.9-2.8-33.7 36.1-24.4 101.7-12.8 112-5 .7 5.3-1 18.5-1 21 4-.3 9.1-1.3 13-1.7z"
		/>
		<linearGradient
			id="ch-topdown89000d"
			x1={671.981}
			x2={785.0641}
			y1={1841.9718}
			y2={1843.9451}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.504} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89000d)"
			d="M676 102s-4.3-32.2-4-32 15-17.3 59-17 54 11 54 11l-2 21-107 17z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown89000e"
			x1={1135.0074}
			x2={1248.0184}
			y1={1841.0872}
			y2={1843.0592}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.504} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89000e)"
			d="M1244 102s4.3-32.2 4-32-15-17.3-59-17-54 11-54 11l2 21 107 17z"
			opacity={0.2}
		/>
		<path
			d="M741.6 91.8c-34.7 351.1 38.3 1348.5 48.2 1436.5 36.9-1.8 44.6-1.2 44.6-1.2S755.2 589.6 785 84.6c-14.7 2.2-34.2 5.8-43.4 7.2zm436.7 0c34.8 351.1-38.4 1348.5-48.4 1436.5-37-1.8-44.8-1.2-44.8-1.2s79.4-937.6 49.6-1442.6c14.8 2.3 34.4 5.9 43.6 7.3z"
			className="factionColorSecondary"
		/>
		<path
			d="M798.4 81.6c-11.3 91.6-11.5 223.5-8.8 373.4 4.7 266.4 23.9 585.1 38.4 806.6 9.1 139.3 15.1 240.1 18 265.4 36.9-1.8 34.2.2 34.2.2S813.1 581.4 842.8 76.4c-14.6 2.2-35.1 3.8-44.4 5.2zM1123 81c25.1 253.1-7.9 849.6-30 1193.9-8.6 133.3-13.8 228.8-16 252.1-37-1.8-37.7-1-37.7-1s67.4-946.6 37.5-1451.6c14.8 2.2 37 5.2 46.2 6.6z"
			className="factionColorPrimary"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M838 1415.1h246" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M829.6 1274.7h262.8" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M818.7 1125.3h282.1" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M811.5 973.4h297.8" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M801.8 819.2h315.9" />
		<linearGradient
			id="ch-topdown89000f"
			x1={859.9026}
			x2={1063.5056}
			y1={1446.7775}
			y2={1450.3304}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8f9bb0" />
			<stop offset={0.399} stopColor="#dae3f2" />
			<stop offset={0.604} stopColor="#dae3f2" />
			<stop offset={1} stopColor="#8f9bb0" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89000f)"
			d="M860.9 150.9c-5.2 183.7 10.6 486.6 22.9 574.8 10.8 77.2 26 209.7 77.2 209.7s64.2-147.7 74.8-202.5c20.7-107.8 33.4-490.6 25.3-584.5-5-119.7-49.5-141-100.1-141-60.6.1-96 30.3-100.1 143.5z"
		/>
		<path
			fill="#e6e6e6"
			stroke="#003"
			strokeWidth={5}
			d="M860.9 370.2c12.9-16.4 22.9-20.5 22.9-20.5V176.2c-9.7-8.8-10.5-10-21.7-21.7-6 76.5-2.4 171.1-1.2 215.7zm200.4 0c-13-16.4-23.1-20.5-23.1-20.5V176.2c9.8-8.8 10.6-10 21.9-21.7 6 76.5 2.4 171.1 1.2 215.7z"
		/>
		<linearGradient
			id="ch-topdown89000g"
			x1={888.7788}
			x2={1032.093}
			y1={1219.1416}
			y2={1221.6436}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.102} stopColor="#676767" />
			<stop offset={0.261} stopColor="#ccc" />
			<stop offset={0.297} stopColor="#ccc" />
			<stop offset={0.342} stopColor="#f2f2f2" />
			<stop offset={0.408} stopColor="#ccc" />
			<stop offset={0.596} stopColor="#b3b3b3" />
			<stop offset={0.799} stopColor="#666" />
			<stop offset={0.894} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-topdown89000g)"
			d="M967.2 579s61.6 5.5 64.8 53c1.2 55.4-23.7 188-66.2 188 0-49.6 1.4-241 1.4-241zm-13.1 241c-46.8 0-67.8-156.8-65.1-188 7.2-50.4 63.8-51 63.8-51s1.3 189.4 1.3 239z"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M472.5 142.9c153.3-36.1 339-61.6 398.1-72.7C883.4 39.8 895.1 7.5 961 7.5c30.5 0 62.6-.7 89.2 62.7 45.9 7.7 240.9 33.4 396.6 68.3m205.8 79.8c58.9 62.6 61.9 101.4 61.9 120.6 0 20.1 6.4 70-239.9 98.8-213.3 46.9-280.9 224.8-280.9 332.6-3.4 142.9 99.9 312.3 286.9 344.7 201 22.9 231.5 87.7 225.5 131.4-6.1 43.6-77.6 242.6-235.1 384.4-39.6 37.9-92.8 26.5-92.8 26.5s-33.7 3.6-119.4-116.9c-69.8 0-135.4-16.9-297.8-16.9s-249.7 16.9-303.8 16.9c-28.4 41.5-71.7 99.3-113.3 114.5-41.6 9.6-92.8-24.1-92.8-24.1s-164.9-150.8-234-355.5c-38.5-114 125.8-142.5 184.4-155.5 144.6 0 323.1-141.6 323.1-341.1 0-79.1-45.1-292.3-280.9-339.9-84.2-8.7-238.7-21.5-238.7-100.1 0-32.9 20.2-87.3 61.4-120.6"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M786.2 64.1c-21.5-16.7-113.3-11.3-113.3 8.4 1.8 18.2 3.6 30.1 3.6 30.1s98.1-16.4 107.3-18.1c1.1-9.8 1.5-11.7 2.4-20.4zm347.9-.2c21.5-16.9 113.2-11.4 113.2 8.5-1.8 18.3-3.6 28-3.6 28s-97.9-14.2-107.2-15.8c-1.1-10-1.5-11.9-2.4-20.7z"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M785 74.3c-23.5-6.3-85.4-10.6-110.9 9.1" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1132.9 74.9c21.3-6.8 86.8-12.1 113.2 8.5" />
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="m631.9 211.8 65.1-4.3V219l-65.1 4.3v-11.5zm0 25.7 65.1-4.3v11.4l-65.1 4.3v-11.4zm0 25.7 65.1-4.3v11.4l-65.1 4.3v-11.4zm0 25.7 65.1-4.3V296l-65.1 4.3v-11.4zm656.4-77.5-65.1-4.3v11.4l65.1 4.3v-11.4zm0 25.6-65.1-4.3v11.4l65.1 4.3V237zm0 25.6-65.1-4.3v11.4l65.1 4.3v-11.4zm0 25.6-65.1-4.3v11.4l65.1 4.3v-11.4z"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M845.3 1528.4c-29.5-449.2-79.9-1177-47.3-1443.8" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1077.3 1528.2c29.5-449 73.3-1172 46.1-1445.2" />
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1050.2 70.2c22 88.8 14.6 367.6-7.2 584.5-15.8 157.2-33.2 280.8-82 280.8-61.7 0-76.2-201.3-83.2-280.5s-35.2-455.7-6-586"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M968.2 581.1c20.4 0 68.3 11.9 63.9 68.7-4.9 63.3-29.7 170.3-65.1 169.9 0-82.4 1.2-172.7 1.2-238.6zm-15.7 0c-20.3 0-68 11.9-63.6 68.7 4.8 63.3 29.6 170.3 64.8 169.9.1-82.4-1.2-172.7-1.2-238.6z"
		/>
		<path fill="#f2f2f2" d="m465.5 1903.5-14-476h-6l-17-43v-200l19-38h118l19 37v201l-17 42h-9l-12 477h-81z" />
		<path fill="#e6e6e6" d="m494.5 1467.5-11-27h44l-11 27h-22z" />
		<linearGradient
			id="ch-topdown89000h"
			x1={583.5}
			x2={428.5}
			y1={753.5}
			y2={753.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.401} stopColor="#e6e6e6" />
			<stop offset={0.601} stopColor="#e6e6e6" />
			<stop offset={0.997} stopColor="#a6a6a6" />
		</linearGradient>
		<path fill="url(#ch-topdown89000h)" d="m428.5 1184.5 20-37h116l19 38-155-1z" />
		<linearGradient
			id="ch-topdown89000i"
			x1={583.5}
			x2={428.5}
			y1={634.5}
			y2={634.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b1b1" />
			<stop offset={0.398} stopColor="#f2f2f2" />
			<stop offset={0.602} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path fill="url(#ch-topdown89000i)" d="M428.5 1384.5v-199h155v200l-155-1z" />
		<linearGradient
			id="ch-topdown89000j"
			x1={583.5}
			x2={428.5}
			y1={513.5}
			y2={513.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.4} stopColor="#e6e6e6" />
			<stop offset={0.599} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#a6a6a6" />
		</linearGradient>
		<path fill="url(#ch-topdown89000j)" d="m428.5 1386.5 17 41 121-1 17-41-155 1z" />
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="m465.5 1903.5-14-476h-6l-17-43v-200l19-38h118l19 37v201l-17 42h-9l-12 477h-81z"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M582.5 1184.5h-153m-1 201h155m-21 41-114 1" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M483.5 1429.5v10l11 28h23l12-27v-12m-48 12h46" />
		<path fill="#f2f2f2" d="m1450.5 1903.5 14-476h6l17-43v-200l-19-38h-118l-19 37v201l17 42h9l12 477h81z" />
		<path fill="#e6e6e6" d="m1421.5 1467.5 11-27h-44l11 27h22z" />
		<linearGradient
			id="ch-topdown89000k"
			x1={1487.5}
			x2={1332.5}
			y1={753.5}
			y2={753.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.401} stopColor="#e6e6e6" />
			<stop offset={0.601} stopColor="#e6e6e6" />
			<stop offset={0.997} stopColor="#a6a6a6" />
		</linearGradient>
		<path fill="url(#ch-topdown89000k)" d="m1487.5 1184.5-20-37h-116l-19 38 155-1z" />
		<linearGradient
			id="ch-topdown89000l"
			x1={1487.5}
			x2={1332.5}
			y1={634.5}
			y2={634.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b1b1" />
			<stop offset={0.398} stopColor="#f2f2f2" />
			<stop offset={0.602} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path fill="url(#ch-topdown89000l)" d="M1487.5 1384.5v-199h-155v200l155-1z" />
		<linearGradient
			id="ch-topdown89000m"
			x1={1487.5}
			x2={1332.5}
			y1={513.5}
			y2={513.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.4} stopColor="#e6e6e6" />
			<stop offset={0.599} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#a6a6a6" />
		</linearGradient>
		<path fill="url(#ch-topdown89000m)" d="m1487.5 1386.5-17 41-121-1-17-41 155 1z" />
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="m1450.5 1903.5 14-476h6l17-43v-200l-19-38h-118l-19 37v201l17 42h9l12 477h81z"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M1333.5 1184.5h153m1 201h-155m21 41 114 1" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1432.5 1429.5v10l-11 28h-23l-12-27v-12m48 12h-46" />
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M458.5 1225.4h22.1v123.7h-22.1v-123.7zm75.9.5h22.1v123.7h-22.1v-123.7z"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M230.6 1191.8c-11-27-45.9-112.7-58.3-143m50.4 32.2c7.3 18.9 27.2 70.5 34 88.1"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M287.9 1154.4c-7-17.2-15.8-38.7-24.1-59M265 453l16-41m19 5-11 29m8.4 662.9c4.6 11.9 9.3 24.2 13.2 34.2"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M659 1540c-14.3-4.9-51.1-16.7-101.2-60.2m-129.3-159c-30.7-49.4-62.8-110.1-95.4-184.7"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="m321.9 1194.6 4.9 8.3-68.1 33.7-4.9-8.3 68.1-33.7zm87.3 194.6 5.7 7.9-64.2 40.8-5.7-7.9 64.2-40.8z"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M460 1243h20.4m53.9 0H557" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M460 1258h20.4m53.9 0H557" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M460 1273h20.4m53.9 0H557" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M460 1288h20.4m53.9 0H557" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M460 1303h20.4m53.9 0H557" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M460 1318h20.4m53.9 0H557" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M460 1333h20.4m53.9 0H557" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M474.5 1160.6h64l2 9.7h-68l2-9.7z" />
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M267 261c109-59.6 206-76 206-76s.2-11.7 0-63c-26.4 4.4-77.4 17.6-122.7 34.1-37.4 13.6-70.9 30.4-83.3 37.9.2 6.6-.1 60.1 0 67z"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M443 150c6.1 0 11 4.9 11 11s-4.9 11-11 11-11-4.9-11-11 4.9-11 11-11zm-145 52c6.1 0 11 4.9 11 11s-4.9 11-11 11-11-4.9-11-11 4.9-11 11-11z"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M233 389s-22.9 69.5-31.1 94.3m32.2-17.4C241.6 445.2 257 403 257 403"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1439.3 1225.1h22.1v123.7h-22.1v-123.7zm-75.9.5h22.1v123.7h-22.1v-123.7z"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1689.4 1191.5c11-27 45.9-112.7 58.3-143m-50.4 32.2c-7.3 18.8-27.3 70.5-34 88.1"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1633.5 1150.8c6.8-16.6 14.9-36.6 22.7-55.6m-9.2-644.3-16-41m-19 5 11 29m-.4 664.8c-4.6 11.9-9.3 24.2-13.2 34.2"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1260.8 1539.6c13.9-4.8 49.4-16.1 97.7-57.2m129.2-156c31.8-50.4 65.3-113 99.2-190.6"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="m1598.1 1194.3-4.9 8.3 68.1 33.7 4.9-8.3-68.1-33.7zm-87.4 194.5-5.8 7.9 64.3 40.8 5.7-7.9-64.2-40.8z"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M1459.9 1242.7h-20.4m-53.9 0h-22.7" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1459.9 1257.7h-20.4m-53.9 0h-22.7" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1459.9 1272.7h-20.4m-53.9 0h-22.7" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1459.9 1287.7h-20.4m-53.9 0h-22.7" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1459.9 1302.7h-20.4m-53.9 0h-22.7" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1459.9 1317.7h-20.4m-53.9 0h-22.7" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1459.9 1332.7h-20.4m-53.9 0h-22.7" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1445.4 1160.3h-64l-2 9.8h68l-2-9.8z" />
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1653 260.9c-109-59.6-206.1-76-206.1-76s-.2-11.7 0-63c26.4 4.4 77.5 17.6 122.7 34.1 37.4 13.6 70.9 30.4 83.4 37.9-.2 6.7.2 60.2 0 67z"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1476.9 149.9c6.1 0 11 4.9 11 11s-4.9 11-11 11-11-4.9-11-11c0-6 4.9-11 11-11zm145.1 52c6.1 0 11 4.9 11 11s-4.9 11-11 11-11-4.9-11-11 4.9-11 11-11z"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M1681 386.9s22.9 69.5 31.1 94.3m-32.1-17.4c-7.5-20.7-22.9-62.9-22.9-62.9"
		/>
		<path
			fill="none"
			stroke="#003"
			strokeWidth={5}
			d="M915 16s5 77.3 5 187c6.6-.8 33-3 29-3h22c1.4 0 22.4 2.3 29 3 0-109.7 5-187 5-187"
		/>
		<path fill="none" stroke="#003" strokeWidth={5} d="M917 35c6.8-3.6 27.5-8 43-8s37.4 4.2 45 7" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M933 202v382.4" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M983 201.1V583" />
		<path fill="none" stroke="#003" strokeWidth={5} d="m816 1055-257 14" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M790 494s-139.4 2.9-205.7 4.3" />
		<path fill="none" stroke="#003" strokeWidth={5} d="m1104.1 1055.5 256.8 14" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M1129.1 494.4s139.5 2.8 205.9 4.2" />
		<g id="Animation-Propeller-Left">
			<path
				fill="#383838"
				d="M380.5 716.7c32.6 0 59.1 26.5 59.1 59.1s-26.5 59.1-59.1 59.1-59.1-26.5-59.1-59.1 26.5-59.1 59.1-59.1z"
			/>
			<path
				fill="#f1f1f1"
				d="M394.1 756.5c11 7.5 13.8 22.5 6.3 33.5-7.5 11-22.5 13.8-33.5 6.3-11-7.5-13.8-22.5-6.3-33.5 7.5-11 22.5-13.8 33.5-6.3z"
			/>
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="M380.5 716.7c32.6 0 59.1 26.5 59.1 59.1s-26.5 59.1-59.1 59.1-59.1-26.5-59.1-59.1 26.5-59.1 59.1-59.1z"
			/>
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="M380.5 752.8c13.3 0 24.2 10.8 24.2 24.1S393.9 801 380.5 801s-24.2-10.8-24.2-24.1c.1-13.2 10.9-24.1 24.2-24.1z"
			/>
			<path
				fill="#383838"
				stroke="#003"
				strokeWidth={5}
				d="M428.6 740.8c5.3-2.7 11.8-5.4 11.8-5.4l-7.8-56.2 221.1-69.3s8.9 11.4 18.9 37.5c-34.5 17.7-234.4 114.5-234.4 114.5s-3.3-13.3-9.6-21.1zm-70.1-20.9c-1.2-5.8-2.2-12.8-2.2-12.8l-56.5-7.1-9.4-231.1s13.4-5.6 41.3-8.5c8.1 37.8 49.8 255.7 49.8 255.7s-13.8-.3-23 3.8zm-37.2 54.6c-5.8-1-10.3-2.8-10.3-2.8l-27.4 50.5-219.6-76s-.3-14.7 7.3-42c38.3 6.2 256.2 47.2 256.2 47.2s-6.6 12.9-6.2 23.1zm39.8 56.8c-2.5 5.3-6.1 11.5-6.1 11.5l40.5 39.8-133.4 189.6s-14.1-3.6-37.9-18.3c16.6-35 116.3-233.6 116.3-233.6s10.8 8.6 20.6 11zm67.1-21c4.5 3.8 9.5 7.6 9.5 7.6l48.8-29.3L637 966s-7.1 12.7-27.2 32.2C580 973.3 412.3 826.3 412.3 826.3s9.8-7.3 15.9-16z"
			/>
			<circle id="Animation-Coordinates-Left" cx={381} cy={777} r={1} fill="#0f0" />
		</g>
		<g id="Animation-Propeller-Right">
			<path
				fill="#383838"
				d="M1571.1 727.3c27 18.4 33.9 55.2 15.5 82.1-18.4 26.9-55.2 33.9-82.2 15.5s-33.9-55.2-15.5-82.1c18.4-26.9 55.2-33.9 82.2-15.5z"
			/>
			<path
				fill="#f1f1f1"
				d="M1551.7 757.9c11 7.5 13.8 22.5 6.3 33.5-7.5 11-22.5 13.8-33.6 6.3s-13.8-22.5-6.3-33.5 22.5-13.8 33.6-6.3z"
			/>
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="M1571.1 727.3c27 18.4 33.9 55.2 15.5 82.1-18.4 26.9-55.2 33.9-82.2 15.5s-33.9-55.2-15.5-82.1c18.4-26.9 55.2-33.9 82.2-15.5z"
			/>
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="M1551.7 757.9c11 7.5 13.8 22.5 6.3 33.5-7.5 11-22.5 13.8-33.6 6.3s-13.8-22.5-6.3-33.5 22.5-13.8 33.6-6.3z"
			/>
			<path
				fill="#383838"
				stroke="#003"
				strokeWidth={5}
				d="M1597.2 774.4c5.9.8 12.8 2.2 12.8 2.2l25.3-50.8 221.6 67.5s.9 14.5-5.6 41.6c-38.4-4.8-258.2-37.6-258.2-37.6s4.9-13 4.1-22.9z"
			/>
			<path
				fill="#383838"
				stroke="#003"
				strokeWidth={5}
				d="M1551.1 717.6c2.2-5.5 5.4-11.8 5.4-11.8l-42.6-37.7L1636.5 472s14.3 2.9 38.9 16.3c-14.6 35.8-103.2 239.2-103.2 239.2s-11.2-8.1-21.1-9.9z"
			/>
			<path
				fill="#383838"
				stroke="#003"
				strokeWidth={5}
				d="M1489.6 741.7c-4.3-4.1-7-8.2-7-8.2l-51.1 26.3-138.4-186.6s8-12.3 29.7-30.6c28.1 26.8 184.9 183.4 184.9 183.4s-12.8 7.1-18.1 15.7z"
			/>
			<path
				fill="#383838"
				stroke="#003"
				strokeWidth={5}
				d="M1490.4 811c-5.1 3-11.5 6.1-11.5 6.1l11 55.7-217.1 81.4s-9.6-11-21-36.5c33.4-19.5 227.9-127.3 227.9-127.3s3.9 13.2 10.7 20.6z"
			/>
			<path
				fill="#383838"
				stroke="#003"
				strokeWidth={5}
				d="M1557.6 831.6c1.6 5.7 3.6 11.6 3.6 11.6l56.8 3.3 24.2 231.3s-13 6.5-40.7 11.3c-10.5-37.4-66-253.2-66-253.2s12.2-.5 22.1-4.3z"
			/>
			<circle id="Animation-Coordinates-Right" cx={1537.7} cy={776.8} r={0.5} fill="#18f700" />
		</g>
		<g>
			<path
				fill="#f1f1f1"
				d="M1731.7 493.4c156.7 107 196.9 320.7 89.9 477.3-107 156.6-320.8 196.9-477.5 89.9s-196.9-320.7-89.9-477.4c107-156.6 320.8-196.8 477.5-89.8zm-457.3 102.9c-99.3 145.4-62 343.7 83.4 443 145.4 99.3 343.8 62 443.2-83.4s62-343.7-83.4-443-343.8-61.9-443.2 83.4z"
			/>
			<path
				fill="#f1f1f1"
				d="M381.1 432.9c189.8 0 343.6 153.8 343.6 343.5s-153.8 343.5-343.6 343.5S37.5 966.1 37.5 776.4s153.8-343.5 343.6-343.5zM61.6 775.8c0 176.1 142.8 318.8 318.9 318.8s318.9-142.7 318.9-318.8S556.6 457 380.5 457 61.6 599.7 61.6 775.8z"
			/>
			<path fill="none" stroke="#003" strokeWidth={5} d="M1218 777h-23" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M1881 777h-23" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M1538 434v23" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M1538 1095v24" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M701 777h23" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M38 777h23" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M381 434v23" />
			<path fill="none" stroke="#003" strokeWidth={5} d="M381 1095v24" />
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="M380.5 457c176.1 0 318.9 142.7 318.9 318.8s-142.8 318.8-318.9 318.8S61.6 951.8 61.6 775.8 204.4 457 380.5 457z"
			/>
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="M381.1 432.9c189.8 0 343.6 153.8 343.6 343.5s-153.8 343.5-343.6 343.5S37.5 966.1 37.5 776.4s153.8-343.5 343.6-343.5z"
			/>
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="M1731.7 493.4c156.7 107 196.9 320.7 89.9 477.3-107 156.6-320.8 196.9-477.5 89.9s-196.9-320.7-89.9-477.4c107-156.6 320.8-196.8 477.5-89.8z"
			/>
			<path
				fill="none"
				stroke="#003"
				strokeWidth={5}
				d="M1717.6 512.9c145.4 99.3 182.8 297.7 83.4 443s-297.8 182.7-443.2 83.4c-145.4-99.3-182.8-297.7-83.4-443s297.8-182.7 443.2-83.4z"
			/>
		</g>
	</svg>
);

export default Component;

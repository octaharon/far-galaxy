import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<g id="Animation-Foot-Left">
			<path
				fill="#999"
				d="m627.8 928 51-237.8L648 339.8 546.9 135.7H412.1l-91.4 201.2-39.5 354.3L339 928h51l83.7-237.8h15.4L574.8 928h53z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m627.8 928 51-237.8L648 339.8 546.9 135.7H412.1l-91.4 201.2-39.5 354.3L339 928h51l83.7-237.8h15.4L574.8 928h53z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M398.9 617.1C453 644 531 640.1 569 616c-5.6-48.6-17.2-134.2-17.2-134.2s-87.2 18.3-129.8-4.6c-10.7 73.8-12.2 79.6-23.1 139.9z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M569 600.7c22.5-3 37.4-12.3 41.4-18.3-3.4-24.2-19.3-94.3-19.3-94.3s-14.9 8.6-36.6 12.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M403.5 600.7c-22.5-3-37.4-12.3-41.4-18.3 3.4-24.2 19.3-94.3 19.3-94.3s14.9 8.6 36.6 12.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m320.7 337.9 327.3 1.9m30.8 352.3-189.6-1.9m-16.4 1-191.6 1.9"
			/>
		</g>
		<g id="Animation-Foot-Right">
			<path
				fill="#999"
				d="m1306.8 928.5-51-237.8 30.8-350.4 101-204.1h134.7l91.4 201.2 39.5 354.3-57.7 236.8h-51l-83.7-237.8h-15.4l-85.7 237.8h-52.9z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1306.8 928.5-51-237.8 30.8-350.4 101-204.1h134.7l91.4 201.2 39.5 354.3-57.7 236.8h-51l-83.7-237.8h-15.4l-85.7 237.8h-52.9z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1535.7 617.6c-54.1 26.9-132 23-170-1.2 5.6-48.5 17.2-134.2 17.2-134.2s87.2 18.3 129.8-4.6c10.6 73.9 12 79.7 23 140z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1365.5 601.2c-22.5-3-37.4-12.3-41.4-18.3 3.4-24.2 19.3-94.3 19.3-94.3s14.9 8.6 36.6 12.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1531.1 601.2c22.5-3 37.4-12.3 41.4-18.3-3.4-24.2-19.3-94.3-19.3-94.3s-14.9 8.6-36.6 12.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1613.8 338.4-327.2 1.9m-30.8 352.3 189.6-1.9m16.4.9 191.5 1.9"
			/>
		</g>
		<path
			fill="#f2f2f2"
			d="M962.7 1190.8c-156.4 0-287-100.1-320.5-169.4-13.8 67.4-51.8 104-82.6 123.1 15.2 5.2 34.5 11.7 34.5 11.7l9.6 22.1-132.7.7-20 737s-176.1-1.4-186-1c.2-16.4-27.4-1022.4-33-1094-2.2-.3-10.6-1.3-15-1-5.8-15.7-20.6-54.3-37-97-.8-19.2.7-44.3-1-65-4.5.5-7.9 1-14 1 .6 9-.1 23.6 0 32-12.9.1-51.3 0-62 0-.7-9.8-1-20.1-1-32-14.7.3-39.9-1.2-50-1-1.1-314.9-1-348-1-348 336.4-3 405.1-.8 514-1 .1 5.5.5 37.2 2 47 11-.1 37.7.8 52 0 11.7-35 33.4-104.7 64.6-160.6C700.6 158.9 743 73.5 872.3 45c-.1-5.3 5.1-12.9 11.4-15.2 66-17.9 98.4-13 152.2.2 6.3 2.3 10 11.3 14 18 43.8 9.7 93.4 32.4 121 56 42.7 36.5 57.8 73.9 69 98 28.2 61.4 51.2 119.1 62.8 154.2 14.3.8 40.1-.1 51 0 .2-7.6-.1-41.5 0-47 109 .2 179.7-2 516.2 1 0 0 .1 33.1-1 348-10.1-.2-36.3 1.3-51 1 0 11.9.2 25.5 0 32-10.7 0-49.2.1-62 0 .1-8.4-.6-23 0-32-6.1 0-10.5-.5-15-1-.1 25.5-.2 45.8-1 65-16.4 42.6-32.2 81.3-38 97-4.4-.3-12.8.7-15 1-2.4 70.5-32.2 1077.5-32 1093.9-9.9-.4-185.1 1-185.1 1l-20.9-738h-115l9-21s15.2-6.5 24-11c-30.9-19.1-66.3-71.5-78-129-33.6 69.4-173.5 173.8-330 173.8"
		/>
		<path
			fill="#dedede"
			d="M1288 487c.5 13.9-1.9 31.6-3 45-4.8 56.2-22.9 104.3-49 148-27.8 46.4-64.6 87.5-103 117-37.5 28.8-78.5 45.6-110 54-.2 8.5 0 23 0 23l-126.7 2 1-22.1s-176.7-45.2-245.4-237.8c-12.1-28.5-19.6-86.2-19.8-115.1-4.4 23.6-15.8 100.9-30 110-8.9-.3-17-1-17-1s8.1 95.1 19 135c.2 1.7 7.3 21.5 16 60 15 66.3 37.9 169.9 60.7 200.9 36.1 49 94.6 92.8 166.5 106.9 35.6-6 60.6-8.7 60.6-8.7l9.6-72.2 10.6-18.3h76l9.6 16.4 10.6 75.1 60.6 6.7s111.9-21 165.6-105.9c16.4-25.9 44.4-106.9 68.3-240.7 6-32.4 19.9-119 23.2-154.3-13.4.4-21-1-21-1s-8.1-8.7-14-27c-14.4-66.9-16-87.4-19-95.9z"
		/>
		<radialGradient
			id="ch-topdown18002a"
			cx={924.025}
			cy={1185.96}
			r={598.497}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#000" />
			<stop offset={0.543} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18002a)"
			d="M917.5 1031c-.3 6.9-1.9 15.4-1.9 15.4l-7.7 57.8-60.6 7.7s-90.1-9-162.7-99.1c-14.9-18.5-29.8-62.5-42-108.6 1.1-15.6 2.1-48.6 6.3-58.2 6 20.1 14.2 40.7 25.1 58.1 32.1 52.5 132 125 243.5 126.9z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-topdown18002b"
			cx={1010.588}
			cy={1175.176}
			r={580.245}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#000" />
			<stop offset={0.543} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18002b)"
			d="M1014.7 1027.1c.3 6.9 1.9 19.3 1.9 19.3l7.7 57.8 60.6 7.7s90.1-9 162.7-99.1c9.9-12.3 19.2-40.5 28.4-68.7-.4-33.4-3.3-76.3-5.4-90-6.6 17.5-14.5 35-23.9 50-26.5 43.5-96.5 123-232 123z"
			opacity={0.4}
		/>
		<path
			fill="#bfbfbf"
			d="M1330.4 828.8v66.4s-5 26.9-16.4 61.6c-12.7 38.8-34.2 87.9-84.7 133.8-50 41.5-114.3 77.2-210.8 96.3 14.4-13.5 19.3-20.2 19.3-20.2s27-3.1 62.6-17.3c48.1-19.2 112.7-56 155.9-105.9 27.9-26.6 51.3-112.4 62.6-175.2 5.2-34.5 11.5-39.5 11.5-39.5zm-724.8 6.7c14.7 87 37.7 165.9 66.4 205 75.8 83.9 170.3 117.6 217.5 126.1 10.5 9.5 20.2 21.2 20.2 21.2s-37.6-4.1-84.7-22.1c-60.8-23.3-138.4-68-181-138.6-15.2-25.2-38.7-127.6-40.4-138.6-.8-15.1-1.5-59.4 2-53z"
		/>
		<path
			fill="#d9d9d9"
			d="m1026.2 1151.3 11.6 15.4-21.2 20.2-9.6-18.3 19.2-17.3zm-121.2 1 16.4 17.3-9.6 18.3-21.2-21.2 14.4-14.4zm3.8-48.2v18.3l-61.6 8.7v-19.3l61.6-7.7zm114.5 0 61.6 7.7-1 19.2-60.6-8.6v-18.3z"
		/>
		<path
			fill="#e6e6e6"
			d="m1028.2 1152.3 10.6 15.4s53.9-10.5 111.7-41.4c39.9-21.3 81.7-57.1 108.8-86.6 25.8-34.9 40-79.7 48.1-115.5 10-44.2 17.8-79.1 18.3-90.5 3.5 3.1 3.9-1 3.9-1l1-66.4s0-42.8-2.4-43.2c-2.8-.5-8.2 41.3-8.2 41.3s-42.7 218.8-75.7 251.8c-34.9 42.9-85.6 79.2-159.2 96.7-.2 6.8-1 18.3-1 18.3l-55.8 2.9v18.2zm-20.2 17.3 8.7 17.3s-48.8 8.7-104 1c4.5-9.7 9.6-18.3 9.6-18.3h85.7zm-104-18.3s-7.1 7.1-14.4 15.4c-53.6-11.5-90-30.1-125.1-50-13.5-9.7-79.1-50.8-101.1-88.6-33.9-58.2-47.6-138.8-59.7-200.2.2-34.7-1-60.6-1-60.6L604 742s17.6 81.8 35.3 149.4c11 42 21.7 87.4 50 127.1 26.4 35.8 85.9 81.2 157.9 94.3.5 8.1 1 18.3 1 18.3l54.9 1.9.9 18.3z"
		/>
		<linearGradient
			id="ch-topdown18002c"
			x1={1179.275}
			x2={1179.275}
			y1={799.4821}
			y2={1244.1567}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002c)"
			d="m1028.2 1152.3 10.6 15.4s53.9-10.5 111.7-41.4c39.9-21.3 81.7-57.1 108.8-86.6 25.8-34.9 40-79.7 48.1-115.5 10-44.2 17.8-79.1 18.3-90.5 3.5 3.1 3.9-1 3.9-1l1-66.4s0-42.8-2.4-43.2c-2.8-.5-8.2 41.3-8.2 41.3s-42.7 218.8-75.7 251.8c-34.9 42.9-85.6 79.2-159.2 96.7-.2 6.8-1 18.3-1 18.3l-55.8 2.9v18.2z"
		/>
		<linearGradient
			id="ch-topdown18002d"
			x1={747.1356}
			x2={756.5516}
			y1={1233.0493}
			y2={808.3463}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002d)"
			d="M904 1151.3s-7.1 7.1-14.4 15.4c-53.6-11.5-90-30.1-125.1-50-13.5-9.7-79.1-50.8-101.1-88.6-33.9-58.2-47.6-138.8-59.7-200.2.2-34.7-1-60.6-1-60.6L604 742s17.6 81.8 35.3 149.4c11 42 21.7 87.4 50 127.1 26.4 35.8 85.9 81.2 157.9 94.3.5 8.1 1 18.3 1 18.3l54.9 1.9.9 18.3z"
		/>
		<path fill="#ccc" d="m1024.3 1122.4 51 8.7-48.1 1.9-2.9-10.6zm-116.5 0L904 1133l-53.9-1.9 57.7-8.7z" />
		<linearGradient
			id="ch-topdown18002e"
			x1={538.5436}
			x2={538.5436}
			y1={844.75}
			y2={1309}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#ccc" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002e)"
			d="M603 737c.3 17.8-.3 338.3-.3 338.3L474 1075s6.7-243.3 7-256c7.7 0 17-1 17-1l38-95 1-64 29-1v-47h18s13.1 112.1 19 126z"
		/>
		<linearGradient
			id="ch-topdown18002f"
			x1={1387.9819}
			x2={1387.9819}
			y1={845}
			y2={1310}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#ccc" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002f)"
			d="M1329 705c-.3 17.8 1.4 370 1.4 370l116.6-.3s-6.7-243.2-7-255.9c-7.7 0-17-1-17-1l-38-95-1-64-29-1v-47l-13.1-1c.1.2-7 81.3-12.9 95.2z"
		/>
		<path
			fill="#bfbfbf"
			d="m471.1 1178.3 133.6 1-10.6-22.1-53.9-19.3-18.3-41.4h-49.8l-1 81.8zM1447 1096l-30.9.4-18.3 41.4-53.9 19.3-9.6 21.2h114.5l-1.8-82.3z"
		/>
		<path
			fill="#a6a6a6"
			d="M1446.4 1074.3h-115.1l-1.9-179.1s-9.2 60.1-39.5 124.2c16.5 52.2 25.8 90.3 79.9 127.1 16.8-4.8 28.9-8.7 28.9-8.7l16.4-40.4h31.4l-.1-23.1zM602.7 885.6c5.9 27.6 25.3 112.4 41.4 137.7-11.7 36-31 90.9-82.8 121.3-13-4.1-22.1-6.7-22.1-6.7l-16.4-41.4H474v-21.2l129.6-1c.1 0 .6-161.1-.9-188.7z"
		/>
		<linearGradient
			id="ch-topdown18002g"
			x1={623.9}
			x2={474.06}
			y1={810.575}
			y2={810.575}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002g)"
			d="M602.7 1075.3H474.1v21.2l47.8 1 17.3 40.4 22.1 5.8s46.3-29.8 62.6-66.4c-7.9-.7-21.2-2-21.2-2z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown18002h"
			x1={1447.52}
			x2={1308.26}
			y1={808.7709}
			y2={808.7709}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002h)"
			d="M1358.3 1075.3h89.1l.1 21.2-31.5 1-17.3 40.4-29.8 9.6c.7.3-39.8-26.3-60.6-72.2 7.9-.6 50 0 50 0z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown18002i"
			x1={1373.71}
			x2={1373.71}
			y1={949.382}
			y2={698.132}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002i)"
			d="M1457.4 1074.3h-126.1l-1.9-179.1s-9.2 60.1-39.5 124.2c16.5 52.2 25.8 90.3 79.9 127.1 16.8-4.8 28.9-8.7 28.9-8.7l16.4-40.4h40.4l1.9-23.1z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown18002j"
			x1={579.625}
			x2={579.625}
			y1={964.399}
			y2={695.819}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002j)"
			d="M517.1 1075.3h85.7V875s10.2 84.1 40.4 148.3c-16.5 52.2-26.8 83.5-80.9 120.3-16.8-4.8-23.1-5.8-23.1-5.8l-17.3-40.4h-5.8l1-22.1z"
			opacity={0.502}
		/>
		<path
			fill="#545454"
			d="m928.1 1012.7 75.1 1 10.6 16.4 9.6 74.1v18.3l4.8 11.5-2.9 17.3-17.3 18.3h-85.7l-17.3-18.3-2-18.3 6.7-11.6-1.9-21.2 10.6-69.3 9.7-18.2z"
		/>
		<path fill="#fff" d="M1014.7 1059.8v50.1h-96.3v-51l96.3.9z" opacity={0.102} />
		<path fill="#fff" d="M1013.7 1058.9V1030l-10.6-17.3-75.1 1-10.6 18.3 1 27h95.3z" opacity={0.251} />
		<linearGradient
			id="ch-topdown18002k"
			x1={1029.9399}
			x2={933.6776}
			y1={858.705}
			y2={858.705}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.43} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={0.569} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002k)"
			d="M917.5 1108c-.1-32 1-77 1-77l9.6-18.3 75.1 1 10.6 16.4v79.9c-.1-.1-68.9-.8-96.3-2z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown18002l"
			x1={904.96}
			x2={985.42}
			y1={1139.2649}
			y2={1139.2649}
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2968} stopColor="#fff" stopOpacity={0.9} />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown18002l)" d="m918.4 1108.9 67 1v59.7h-63.1l-17.3-18.3 13.4-42.4z" opacity={0.11} />
		<radialGradient
			id="ch-topdown18002m"
			cx={981.298}
			cy={837.345}
			r={25.019}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" />
			<stop offset={0.5577} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18002m)"
			d="M981 1057c13.8 0 25 11.2 25 25s-11.2 25-25 25-25-11.2-25-25 11.2-25 25-25z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown18002n"
			cx={1079.4969}
			cy={1494.3259}
			r={780.704}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3619} stopColor="#f2f2f2" />
			<stop offset={0.577} stopColor="#b3b3b3" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18002n)"
			d="M657 573c7.3 23.8 22 74.5 62.2 125.9 47.7 60.9 125 119.2 179 128 2.4-41.4 19.2-644.6-26-780.7-108.7 20.9-168.8 98.9-188.6 150.2-7 13.5-19.7 39.4-27.6 75.6-9.2 42.1-13 97-13 144 .8 75 8.9 125.5 14 157z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown18002o"
			cx={938.824}
			cy={1454.624}
			r={779.224}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1196} stopColor="#f2f2f2" />
			<stop offset={0.5046} stopColor="#b3b3b3" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18002o)"
			d="M1022 827.2c-2-31.5-9.6-390.1 4-618.2 4.3-71.9 13.5-128.7 22-161 87.9 21.6 148.7 54.9 190 151 55.3 103.8 32 328.5 32 323-3.5 42.2-24.8 118.6-65 170-44.9 52.6-116.4 118.9-183 135.2z"
			opacity={0.4}
		/>
		<path
			d="M986 874.3c20.3-.2 36-.3 36-.3s-8-287.4-2-511c3.8-140.9 8.6-273.3 28-315-2.3-6.8-7.9-13.9-12-18-34.7-8.3-77.9-22.6-153 1-4.6 3.7-9.4 10.8-12 16 12.8 31.2 19.7 86.1 25 195 11.7 239 2 633 2 633s15.8-.1 36-.3V926l-66 61s-7 3-7-7v-26s-127.7-13.9-199-162c-3 25.4-6 72-6 72s11.3 64.9 103 118c91.7 53.1 158 49 158 49l11-18h75l10 16s69.4-2.3 110-20c40.6-17.7 118.6-71.6 140-139-.3-10.5-4.9-72.4-6-76-6.2 10.2-57.7 131-198 161v27s-3 10.2-11 3-62-59-62-59-.3-44.8 0-51.7zM565 611c-10.8-20.4-27-49.3-27-132s26.7-120.9 30-123c13 .4 65 1 65 1s8 32.7 8 62-18.4 172.8-39 191c-16.4 1.5-23.3 1.6-37 1zm790-.3c10.8-20.4 27-49.2 27-131.8 0-82.6-26.7-120.7-30-122.8-13 .4-65 1-65 1s-4 28.7-4 58 13.9 174.3 39 197c16.4 1.4 19.3-.8 33-1.4z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-topdown18002p"
			x1={959.5}
			x2={959.5}
			y1={1045}
			y2={1901.9325}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.102} />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002p)"
			d="M997 837c8.4 12.9 25 37 25 37s-8-287.4-2-511c3.8-140.9 8.6-273.3 28-315-2.3-6.8-7.9-13.9-12-18-34.7-8.3-77.9-22.6-153 1-4.6 3.7-9.4 10.8-12 16 12.8 31.2 19.7 86.1 25 195 11.7 239 2 633 2 633s15.4-23.6 26-39c9.3-.2 61.2.2 73 1z"
		/>
		<path
			d="M383 372v228l-331 1-1-229h332zM102 658v34h63v-33l-63-1zm77 0 1 65 38 96h280l39-96v-65H179zm1691-286-334 1 1 228 332 1 1-230zm-53 286v34l-62-1 1-33h61zm-77 1v64l-37 96h-281l-39-96 1-65 356 1z"
			className="factionColorSecondary"
		/>
		<path
			d="M1036 30c4.1 4.1 9.7 11.2 12 18-19.4 41.7-24.2 174.1-28 315-6 223.6 2 511 2 511l-124 1s9.7-394-2-633c-5.3-108.9-12.2-163.8-25-195 2.6-5.2 7.4-12.3 12-16 7.9 27.6 15 52.9 20 96 2.6 22.3 16.4 187.7 19 358 2.5 167.2 2.3 324 1 351h74s-12.8-658.5 39-806z"
			opacity={0.102}
		/>
		<path fill="#d9d9d9" d="M960 847c13.3 0 24 4.3 24 9.5s-10.8 9.5-24 9.5-24-4.3-24-9.5 10.8-9.5 24-9.5z" />
		<path
			fill="#f2f2f2"
			d="M336.4 502H356v71h-19.6v-71zm-44.2 0h19.6v71h-19.6v-71zm-44.2 0h19.6v71H248v-71zm1317 0h19.6v71H1565v-71zm44.2 0h19.6v71h-19.6v-71zm44.2 0h19.6v71h-19.6v-71z"
		/>
		<linearGradient
			id="ch-topdown18002q"
			x1={588.632}
			x2={588.632}
			y1={1309.9689}
			y2={1563}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002q)"
			d="M632 357h-65s-30 43-30 126 21.2 121.9 29 127c12.7.1 37 0 37 0s19.2-17.6 33-146c11.5-81.7-4-107-4-107z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown18002r"
			x1={49.9998}
			x2={566.9998}
			y1={1431.9967}
			y2={1441.0208}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.302} />
			<stop offset={0.602} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002r)"
			d="m566 310-516-1 2 349h514v-47s-27-18.2-27-129c0-68.9 16.4-111.2 28-124-.5-17.5-1-48-1-48z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown18002s"
			x1={102.9913}
			x2={164.9899}
			y1={1244.4589}
			y2={1245.5409}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18002s)" d="M103 659v33l62-1-1-32h-61z" opacity={0.2} />
		<linearGradient
			id="ch-topdown18002t"
			x1={179}
			x2={537.0081}
			y1={1225.3755}
			y2={1231.6246}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18002t)" d="m179 659 1 64 357 1v-65H179z" opacity={0.2} />
		<path fill="#e0e0e0" d="M304 851h107l-27 60-54 1-26-61z" />
		<linearGradient
			id="ch-topdown18002u"
			x1={617}
			x2={617}
			y1={1012}
			y2={1329}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002u)"
			d="M613 591c1.2 26.7 19 198.1 36 256-2.5 7.1-5 37-5 61-6.4-20.9-23.4-98.5-25-107s-13.8-55.4-16-62-17.7-94.5-18-128c10.4.1 18-1 18-1s5.2-5.2 10-19z"
		/>
		<linearGradient
			id="ch-topdown18002v"
			x1={1306}
			x2={1306}
			y1={974}
			y2={1318}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002v)"
			d="M1314 602c-1.2 26.7-14.8 184.2-43 254 2.5 7.1 5 66 5 90 6.4-20.9 34.4-134.5 36-143s8.8-57.4 11-64 17.7-94.5 18-128c-10.4.1-18-1-18-1s-5.8-3-9-8z"
		/>
		<linearGradient
			id="ch-topdown18002w"
			x1={179.9942}
			x2={535.9033}
			y1={1145.7246}
			y2={1151.937}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18002w)" d="m218 820 280-1 38-96-356 1 38 96z" opacity={0.2} />
		<linearGradient
			id="ch-topdown18002x"
			x1={1331.368}
			x2={1331.368}
			y1={1309.9689}
			y2={1563}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002x)"
			d="M1288 357h65s30 43 30 126-21.2 121.9-29 127c-12.7.1-37 0-37 0s-19.2-17.6-33-146c-11.5-81.7 4-107 4-107z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown18002y"
			x1={1351.0085}
			x2={1868.0085}
			y1={1431.2269}
			y2={1440.2509}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.2} />
			<stop offset={0.398} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.302} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18002y)"
			d="m1352 310.3 516-1-2 349h-514v-47s27-18.2 27-129c0-68.9-16.4-111.2-28-124 .5-17.5 1-48 1-48z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-topdown18002z"
			x1={1755.0001}
			x2={1817.0001}
			y1={1243.9503}
			y2={1245.0323}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18002z)" d="M1817 659v33l-62-1 1-32h61z" opacity={0.2} />
		<linearGradient
			id="ch-topdown18002A"
			x1={1382.9911}
			x2={1740.9911}
			y1={1225.884}
			y2={1232.1331}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18002A)" d="m1741 659-1 64-357 1v-65h358z" opacity={0.2} />
		<path fill="#e0e0e0" d="M1616 851h-107l27 60 54 1 26-61z" />
		<linearGradient
			id="ch-topdown18002B"
			x1={1383.9943}
			x2={1739.8683}
			y1={1145.7246}
			y2={1151.9364}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18002B)" d="m1702 820-280-1-38-96 356 1-38 96z" opacity={0.2} />
		<path
			fill="#ccc"
			d="M1288 356c4.2.3 12 1 12 1s-1.5-17.9-39-106c4.9 27.9 12.6 59.7 16 148 1.4 0 3-1 3-1s2.4-35.5 8-42zm-656 0c-4.2.3-12 1-12 1s1.5-17.9 39-106c-4.9 27.9-12.6 59.7-16 148-1.4 0-3-1-3-1s-2.4-35.5-8-42z"
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={5}
				d="M336.4 502H356v71h-19.6v-71zm-44.2 0h19.6v71h-19.6v-71zm-44.2 0h19.6v71H248v-71zm1317 0h19.6v71H1565v-71zm44.2 0h19.6v71h-19.6v-71zm44.2 0h19.6v71h-19.6v-71z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M619 356h14s16.1 29.1 1 129-23.9 125-34 125h-15s9.4 83.5 18.4 128.6c4.9 18.8 10.1 34.9 14.6 64.4m-58.4 341.4c30.8-19.1 68.8-55.7 82.6-123.1 33.6 69.4 164.1 169.4 320.5 169.4 114 0 277.3-74 326.3-174.2 7.3 33.7 27.5 97.3 80.8 130.5M1303 356h-14s-11.6 39.2-1 120 19.5 123.6 34 134c9.8.1 14 0 14 0"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M645.1 1028.1c-10.2-20-30-77.9-42.4-146.3m682.5 143.4c21.6-37.6 41.5-107.1 45.2-146.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M897.7 827.4c-29 0-210.4-91.5-244.4-271.5-4.7-24.9-27-200.7 12.4-323.2m232.9 621.7C751.9 817.5 677.4 683.8 652 620c-9.2-24.6-17.4-43.1-20-122"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1287.8 479.1c-.4 5.1-.8 11.4-1.3 17-2.7 78.9-10.8 97.5-20 122-24.9 62.5-96.9 192-237.8 232-3 .9-6.7.9-6.7.9m228.8-623.8c1.2 3.4 1.9 5.4 1.9 5.6 39.5 122.5 17.1 298.3 12.4 323.2-31.4 166.7-189.4 257.5-235.7 270.1-1.6.4-8.5 1.9-8.5 1.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1313.9 601c-1.6 40.4-14.2 127.2-19.1 158.5-5 32.1-24.4 120.9-66.4 169.4-51.6 59.5-108.6 98.2-217.5 98.2-3.4-6.1-7.7-14.4-7.7-14.4h-76s-6.6 12-9.6 17.3c-77.6 0-170.5-38.1-231-108.8-44-51.4-53.3-148.4-54.9-160.8C630 748.1 614.8 633.2 613 592"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M960.7 1011.7V877" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M655.4 865.3c1-43 15.4-149.7 24.6-188.3m-19 114c18.7 34.1 60.4 131.3 200 163v27s-2.7 14.5 11 1 62-57 62-57v-49m322-80c-68.7 140.8-197 158-197 158v26c0 11.6-10 6-10 6l-63-61v-49m-338-32c-4.4 37.1-3.6 55.2-3 65"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1239 675c9.2 42.9 23.9 156.6 24.5 196.9m7.5-16.9c2.8 45.3 6.6 60.2 3 95"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1022.4 1103.2c-2.1-18.3-6.8-65.5-8.5-73.5-.6-1.3-1.1-1.6-1.1-1.6v37.5l1 43.3 11.6 43.3s1-.3 1-.7c0-2.1 1-10 1-18.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M908.4 1103c2.1-18.3 6.8-63.8 8.5-71.8.6-1.3 1.1-1.6 1.1-1.6v35.8l-1 43.4-11.5 43.4s-1-.3-1-.7c0-2.1-1-10-1-18.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M912 1187.4c-8.1-7.8-22.5-21.7-22.5-21.7l15.4-14.4s9.2 10.4 17.4 18.3h85.6l18.3-19.3 12.5 16.4s-16.7 16.1-20.5 19.6c-.5.4-1.7.7-1.7.7l-7.7-16.4m-87.5 0-9.6 17.3m102-79h-97.2m1.9-51h94.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1037.8 1166.7s102.3-8.3 215.6-120.3c40-45 57.8-139.6 72.2-213.7-7.9-17.2-14.4-31.8-14.4-31.8s-32.2 170.2-67.4 214.7c-34.4 43.5-87.2 81.5-159.8 96.3m226.2-304.3c1.6-12.1 7.4-37.7 8.7-44.3 1.2-6.6 18-102.7 23.2-154.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M893.2 1167.2s-102.4-8.3-215.7-120.4c-40.1-45.1-57.8-139.7-72.2-213.8 7.9-17.2 12.4-30.8 12.4-30.8S652 971.6 687.1 1016c34.5 43.6 87.3 81.5 159.9 96.3"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1022.4 1104.1v18.3l61.6 8.7v-19.3l-61.6-7.7z" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1084 1131.1-56.8 1.9-4.8-10.6" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M909.3 1103.9v18.1l-61.6 8.6v-19l61.6-7.7z" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m847.7 1130.6 56.8 1.9 4.8-10.5" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M960 847c13.3 0 24 4.3 24 9.5s-10.8 9.5-24 9.5-24-4.3-24-9.5 10.8-9.5 24-9.5z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m474 1075 128.7.3v-339" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1443.5 1075.3h-113.1s-.9-344.9-1-371.1m70.6 61.9c-58.6 0-70.5.2-70.5.2M602 766h-83"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1330.4 919.3h112.3" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M478 919.3h125.7" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M996.1 836.5h-72.8M897 875h127" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M473.7 1096.4h49.1l16.4 40.4 54.9 19.3 10.6 22.1s-83.7-.2-132.2-.3"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m539.2 1138.8 15.4 39.5" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1445.2 1096.9h-30.1l-16.4 40.4-54.9 19.3-10.6 22.2s56.9-.2 116.3-.4"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1398.7 1136.4-17.3 42.4" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M620 356h-55v-47H50l1 349h51v33h62v-33h15v64l38 97h16l32 1096h185l30-1097h17l39-96v-64h29v-48h21"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M51 372.3h332.5v228.4H51" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1869 372.5h-332.5v228H1869" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M103 658h61m16 0h359m27-47c-14.8-20.4-28-51.3-28-132s29-122 29-122m-30 366H180m54 96 248-1m-178 3v29l26 61h54l27-61v-30m-1 30s-105.2.2-105 0"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1299 356.5h55.1v-47h515.4l-1 349h-51.1v33h-62v-33h-15v64l-38 97h-16l-32 1096h-185.2l-30-1097h-17l-39-96v-64h-29v-48h-21"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1816.4 658.5h-61m-16 0h-359.3m-27-47c14.8-20.4 28-51.3 28-132s-29-122-29-122m30 366h357.3m-54.1 96-248.2-1m178.2 3v29l-26 61h-54l-27-61v-30m.9 30s105.3.2 105.1 0"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1300.6 357.3c-3.5-11.9-19-59.5-43.9-116.9-16.2-37.4-33-80.1-63.9-114.9-33.5-37.7-82.8-66.6-143.8-77.9-3.4-8.6-9.4-13.6-12-17-6.3-1.4-31.4-11.5-75.9-11.5-.8-.1-2.1 0-2 0-44.6 0-69.7 9.6-76 11-2.6 3.4-8.6 8.4-12 17-61.1 11.3-110.5 40.2-144 78-31 34.9-47.8 77.6-64 115-25 57.5-40.5 105.1-44 117"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M883 30c47.5 121.2 40 699.3 40 807-16.3 24.5-22.9 34.3-24.6 36.5-.3.3-1.4.5-1.4.5s20.5-730.3-26-827"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1036.8 29.9c-47.3 121.3-39.9 699.5-39.9 807.2 16.2 24.5 22.8 34.4 24.5 36.5.3.3 1.4.5 1.4.5s-20.4-730.5 25.9-827.2"
			/>
		</g>
	</svg>
);

export default Component;

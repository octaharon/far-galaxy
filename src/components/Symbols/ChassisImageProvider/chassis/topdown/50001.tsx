import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<filter id="ch-topdown50001h">
			<feGaussianBlur stdDeviation={35} />
		</filter>
		<path
			fill="#f2f2f2"
			d="M1043 84c15.3 9.1 42 129.9 57 199 16.7-.2 47.7 9.4 55 67 5.4 42.4-20.2 67.3-24 70 4.9 18.5 17 54 17 54s64.3 34.9 69 120c9.8 8.4 17 26 17 26s18 130.2 18 223-12 517.1-12 613c-10.8 120.6-25.6 298.7-47 340-4.2-.4-16-12-16-12v-20c4.4-161 9-322 9-322s7.2-8.3 10.7-11.3c2.2-37.4-1.2-158.8-2.8-219.1-4.6-5.1-11.2-11.6-14.3-15.4-.4-13.2-.6-21.2-.6-21.2s1.1-184.5-45-197c-6.6 38.5-43.7 379-99 509-3.7 8.8-39.2 34.2-74.7 34.3-35.7 0-71.5-25.3-75.3-34.2-55.3-130-92.5-470.6-99-509-39.8-1.8-43.5 170-45.5 216.8-8 5.7-11.4 9.7-16.5 16.1-1.6 60.7-3.8 180.2-1 219 6.5 7.6 11 12.1 11 12.1s4.6 161 9 322v19.9s-11.8 11.7-16 12.1c-21.4-41.3-36.2-219.4-47-340 0-95.9-12-520.2-12-613 0-92.8 18-223 18-223s7.2-17.6 17-26c4.7-85.1 69-120 69-120s12.1-35.5 17-54c-3.8-2.7-29.4-27.6-24-70 7.3-57.6 38.3-67.2 55-67 15-69.1 42.8-188.2 57-199 6.4-5 153.5-7.5 166-.1z"
		/>
		<path
			fill="#e6e6e6"
			d="M1166 685s31.3 393.8 29 522c-5.9-5.1-17-12-17-12s3.2-218-47-218c-24.2 0-21-49.9-21-57s32.4-174.8 28-245c10.1 8.1 28 10 28 10zm-414 0s-31.2 393.8-28.9 522c5.9-5.1 17-12 17-12s-3.2-218 46.9-218c24.1 0 20.9-49.9 20.9-57S775.6 745.2 780 675c-10.1 8.1-28 10-28 10z"
		/>
		<linearGradient
			id="ch-topdown50001a"
			x1={786.1586}
			x2={823.1976}
			y1={893.9342}
			y2={888.0682}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.8} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001a)"
			d="M814 930c5.3 15.8 20.2 124.2-6 200-10.7-28.1-22-152-22-152s22.2 4.5 28-48z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown50001b"
			x1={1128.5405}
			x2={1092.2345}
			y1={901.0782}
			y2={897.9022}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.8} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001b)"
			d="M1107 926c-8 17.8-19 122.1 7 189 4.8-25.3 20-136 20-136s-23.7 4.9-27-53z"
			opacity={0.502}
		/>
		<path fill="#e6e6e6" d="M934 772h51l-14 31s-24.8 2.8-24 2-13-33-13-33z" />
		<linearGradient
			id="ch-topdown50001c"
			x1={1027}
			x2={891}
			y1={1217.394}
			y2={1217.394}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001c)"
			d="M1027 729v-26s-22.3-69-69-69-63.3 62.9-67 71c.6 5.9 0 24 0 24s7.8 23.1 15 41c19.2 1.8 90 1.8 103-1 7.3-16.7 18-40 18-40z"
			opacity={0.302}
		/>
		<path d="m1007 772 20-42s-135.5-1.9-136-1 14 41 14 41l102 2z" opacity={0.149} />
		<linearGradient
			id="ch-topdown50001d"
			x1={959.5}
			x2={959.5}
			y1={1842.6866}
			y2={1375}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.701} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001d)"
			d="M877 84c26.7-5.9 155.8-10.5 167-2 20.8 40.8 46.9 369.8 69 409-6 8-24.9 23.8-24 49-25.1-13-140.2-82.7-256 5-5.8-27.6-12.9-44.3-27-53 26.6-70.5 44.3-402.1 71-408z"
			opacity={0.102}
		/>
		<path
			fill="#ccc"
			d="M804 491c14.1-30.2 47.2-291.8 51-335-16.3 26.7-62.4 263.4-82 318 11 3.4 21.1 11 31 17zm310 0c-14.1-30.2-45.2-291.8-49-335 16.3 26.7 60.4 263.4 80 318-11 3.4-21.1 11-31 17z"
		/>
		<path fill="#e6e6e6" d="m678 1431 45-2v-222l-48 2 3 222zm516-228 52 7-5 221-41-2-6-226z" />
		<path fill="#f2f2f2" d="M722 1431s3.8 284.8 12 355c5.1-1 6-6 6-6l-5-337-13-12z" />
		<path fill="#f2f2f2" d="M1199 1431s-3.8 284.8-12 355c-5.1-1-6-6-6-6l5-337 13-12z" />
		<radialGradient
			id="ch-topdown50001e"
			cx={983.817}
			cy={1520.594}
			r={444}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000004" />
			<stop offset={0.898} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown50001e)"
			d="M809 626c7.4 33.5 28.6 85.6 63 100 13 1.1 34.5 1.5 57.4 1.7L889 730l19 42 1.1 43.2-169.1.8s5.5-87.9 13-132c8.8-.3 20.3.3 28-7 13.5-24.3 17.9-25.6 28-51zm238 94c6.3-6.2 43.7-13.3 66-94 14.1 28.8 18.1 39.4 25 51 18.9 8.9 28 8 28 8l14 129-173.6.8.6-41.8 19-46.1c3.5-.2 15.6-1.6 21-6.9z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-topdown50001f"
			x1={1208.5}
			x2={1208.5}
			y1={1145}
			y2={1319}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001f)"
			d="M1167 685c15.6.1 49.6-5.7 51-84 12.6 7.8 16 19 16 19l16 146-75 9s-7-82.7-8-90z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown50001g"
			x1={709.5}
			x2={709.5}
			y1={1116}
			y2={1324.3849}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001g)"
			d="M703 599c-.6 14-.3 86.1 48 87-5.9 47.9-10 118-10 118l-73-2 19-183s16.6-34 16-20z"
			opacity={0.2}
		/>
		<path
			d="M877.5 731.8s-21.9-9.1-22-7.3c-41.5-33.4-54-99.8-54-99.8s313.4 38.4 317.1 4.2c-22.4 91.8-79.3 99.8-79.3 99.8l-18.9 45.2-13.7 443.6H910L896.4 776l-18.9-44.2z"
			filter="url(#ch-topdown50001h)"
			opacity={0.4}
		/>
		<path
			fill="#f2f2f2"
			d="M830 545c26.5-22.8 83.5-45 130-45s129 41 129 41 10.2 57.1 24 89c-6.6 20.1-40.3 99.5-89.5 100l4.5-28s-23.8-69-68-69-65 56.3-69 73c-2.5 10.8-1.8 19.3-1 24h-1s-61.7-9.3-80-108c13.1-36.3 16.3-49.4 21-77z"
		/>
		<radialGradient
			id="ch-topdown50001i"
			cx={961}
			cy={1370.2151}
			r={597.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50001i)"
			d="M830 545c26.5-22.8 83.5-45 130-45s129 41 129 41 10.2 57.1 24 89c-6.6 20.1-40.3 99.5-89.5 100l4.5-28s-23.8-69-68-69-65 56.3-69 73c-2.5 10.8-1.8 19.3-1 24h-1s-61.7-9.3-80-108c13.1-36.3 16.3-49.4 21-77z"
		/>
		<path
			d="M987 503c-7.6-4.9-54-1-54-1s0 118.9.7 139.6C904.4 659.7 889 704 889 704v28l20 40 14 432 71-1 13-431 19-42 1-28s-13.1-39.8-39.7-58.5c.3-7.4-1.1-118.9-.3-140.5zm-216-28c13.1 3.6 62.9 24.5 59 69s-40.6 125.2-54 135-44.7 12.2-58-13-17.6-58.8-12-93 23.6-71 65-98zm376.4 0c-13.1 3.6-63.1 24.5-59.2 69s40.7 125.3 54.2 135.1c13.5 9.8 44.8 12.2 58.2-13 13.4-25.2 17.6-58.9 12-93-5.6-34.2-23.7-71.1-65.2-98.1z"
			className="factionColorPrimary"
		/>
		<path
			d="M983.4 771.5c-2.3 4.9-9.7 29.2-13.4 33.5-10 .3-11.3-.2-20 0-1.6-2.6-14.2-29.6-15.5-32.5 23.3-1.6 48.9-1 48.9-1z"
			opacity={0.102}
		/>
		<linearGradient
			id="ch-topdown50001j"
			x1={1025}
			x2={890}
			y1={1171}
			y2={1171}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown50001j)" d="m1025 729-16 42H905l-15-44 135 2z" opacity={0.2} />
		<linearGradient
			id="ch-topdown50001k"
			x1={1027}
			x2={891}
			y1={1237.0173}
			y2={1237.0173}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001k)"
			d="M1027 704v27l-136-1 1-23s20.5-73.9 68-72c46.5 1.8 67 69 67 69z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown50001l"
			cx={811.817}
			cy={1334.6949}
			r={129.694}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4} stopColor="#000" stopOpacity={0} />
			<stop offset={0.899} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50001l)"
			d="M815 610s25.5-65.5 11-91-44-43-54-42-87 46-63 172c12.5 25.9 36.3 45.1 65 32 14.7-16.5 41-71 41-71z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown50001m"
			cx={1106.67}
			cy={1342.769}
			r={129.662}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4} stopColor="#000" stopOpacity={0} />
			<stop offset={0.899} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50001m)"
			d="M1104.4 609.9s-25.5-65.5-11-90.9c14.5-25.4 43.9-42.9 54-41.9 10 1 86.9 46 63 171.8-12.6 25.9-36.3 45.1-65 32-14.6-16.6-41-71-41-71z"
			opacity={0.2}
		/>
		<path
			d="m906 391 6-117-30 1-11 114 35 2zm37-2 1-113h32l3 114-36-1zm68 2-5-115h32l11 113-38 2zm12-309-10 52-104-1-12-52 126 1zm107 337c2.1-1 29.8-19.1 23-77s-53-59-53-59-18.7-99.1-35-145c3.3 47.7 12.3 140.8 16 174s22.6 166.2 34 181c11.5-9.5 23.9-15.4 33-19-8.7-23.1-20.8-66.4-18-55zM820 282c-12.2 1.2-48.2 8.8-54 56s14.3 74.9 24 80c-5.8 17.1-19 57-19 57l35 16s13.7-51.4 22-118 27.2-212.2 29-225c-6.1 17-12.2 27.2-19 53-9.8 37.1-19.3 87.9-18 81zm374 923 51 3-5 222-43-2-3-223zm-471 1-47 4 5 218 43-1-1-221z"
			className="factionColorSecondary"
		/>
		<path
			d="M1114 493c9.7-8.9 34-18 34-18s-19.1-60.8-25-84-47.9-225.7-56-244c.6 31.2.8 46.3 5 83s17.1 139.6 19 155 16.9 101.4 23 108zm-310 1c7.3-19.3 21.5-83.7 23-108s26.4-217.9 28-227c-9.5 14.2-27.9 77.1-31 99s-37.2 189.7-53 219c15.5 3.3 21.9 7.6 33 17z"
			opacity={0.102}
		/>
		<linearGradient
			id="ch-topdown50001n"
			x1={771.6248}
			x2={803.2538}
			y1={1507.4392}
			y2={1644.4392}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001n)"
			d="M819 282c-17.3.7-45.1 14.7-52 54s7.6 71.8 24 83c4.7-4.6 26.2-111.8 28-137z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown50001o"
			x1={1147.9988}
			x2={1119.9729}
			y1={1504.4648}
			y2={1643.4596}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50001o)"
			d="M1101 282c24.2.3 46.8 21.7 53 61s-7 61.6-25 78c-3.5-21.6-21.4-97-28-139z"
			opacity={0.4}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1043 84c15.3 9.1 42 129.9 57 199 16.7-.2 47.7 9.4 55 67 5.4 42.4-20.2 67.3-24 70 4.9 18.5 17 54 17 54s64.3 34.9 69 120c9.8 8.4 17 26 17 26s18 130.2 18 223-12 517.1-12 613c-10.8 120.6-25.6 298.7-47 340-4.2-.4-16-12-16-12v-20c4.4-161 9-322 9-322s7.2-8.3 10.7-11.3c2.2-37.4-1.2-158.8-2.8-219.1-4.6-5.1-11.2-11.6-14.3-15.4-.4-13.2-.6-21.2-.6-21.2s1.1-184.5-45-197c-6.6 38.5-43.7 379-99 509-3.7 8.8-39.2 34.2-74.7 34.3-35.7 0-71.5-25.3-75.3-34.2-55.3-130-92.5-470.6-99-509-39.8-1.8-43.5 170-45.5 216.8-8 5.7-11.4 9.7-16.5 16.1-1.6 60.7-3.8 180.2-1 219 6.5 7.6 11 12.1 11 12.1s4.6 161 9 322v19.9s-11.8 11.7-16 12.1c-21.4-41.3-36.2-219.4-47-340 0-95.9-12-520.2-12-613 0-92.8 18-223 18-223s7.2-17.6 17-26c4.7-85.1 69-120 69-120s12.1-35.5 17-54c-3.8-2.7-29.4-27.6-24-70 7.3-57.6 38.3-67.2 55-67 15-69.1 42.8-188.2 57-199 6.4-5 153.5-7.5 166-.1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m897 81 11 52 104.2-.1 11-52.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M882 275h32l-6.4 115h-37.7L882 275zm62 0h32l2.9 115h-37.7l2.8-115zm61.9 0h32l12.1 115h-37.7l-6.4-115z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M806 490c25.4-58 42.9-344.5 65-397" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1114 490c-25.4-58-42.9-344.5-65-397" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m723 1428-44 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m727 1205-52 5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M753 682c-15.6 61.1-29 453-29 528" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1196 1428 44 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1194 1205 50 5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1165 682c15.6 61.1 29 453 29 528" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M784 979c16.1-4 27-7.1 27-55s-31.4-134.8-29-250" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1107 301c9.8 3.9 37.4 34.1 19 99" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M817 294c-11.6 5.8-50.7 38.3-25 113" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1135.9 978c-16-4-26.9-6.1-26.9-54s31.3-134.8 28.9-250"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M821 278c-3.1 18.6-24.4 116.1-33 148" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1100 280c2.6 13.9 26.3 130.1 33 149" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M771 475c13.6 1.2 66.2 28.4 59 73-8.9 55.3-42.8 133.3-62 136s-67.9 11.7-65-95"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1148.8 475c-13.6 1.2-66.3 28.4-59.1 72.9 8.9 55.2 42.8 133.1 62.1 135.7 19.3 2.7 68 11.7 65.1-94.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1088 539c-17.8-9.8-66.9-42.2-127.9-41.3-42.3.6-85.4 8.5-130.1 43.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M809 620c1.1 25.2 30.2 109 81 109-.2-11.7 0-23 0-23s21.9-72 68-72 65.1 58.9 69 69c-.6 4.9 0 25 0 25s57.1-2.6 85-99"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M933 641V503" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M987 641V503" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m872 726 22.3 772.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M799 1064h83" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M832 1279h54" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1119 1064h-83" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1086 1279h-54" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1044 726.2-22.1 772.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m888 729 18 43 103-1 18-42" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m909 773 13 430h73l13-432" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m935 774 13 30h23l13-31" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M891 729h137" />
	</svg>
);

export default Component;

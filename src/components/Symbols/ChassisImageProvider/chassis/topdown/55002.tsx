import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#e3e3e3"
			d="M1348 1371c139.5 1.8 186.3-242 120-320 .7-2.6-23.7-17.5-23-20 89.8-372.3 85.7-436.7 89-519 29.4-13.9 62.2-99.5 27-207-17.6-53.8-75.9-121.3-141.6-129.1-3.4-4.7-6.9-9.3-10.4-13.9-14.3-20.5-31.5-38.6-51-54.4-.7-.5 6.6-19 6-19.6-27.6-23.1-62.1-41-98-55-.7-.3-7.8 18.4-8.5 18.1C1171 17.8 1065.8 6 961.4 6c-124.1 0-222.1 15.9-298 45.8-1 .4-9.4-18.1-10.3-17.7-37.6 15.1-69.9 30.3-97 52-.8.6 5.9 23.6 5.1 24.2-19.2 15.7-35.8 33.1-50.1 52-3.9 5.1-7.8 10.2-11.5 15.4-63.7 8-114.7 64-134.6 113.3-24.8 61.6-34 138.5 1.4 199.9 1.1 1.9 4.4-8.8 5.6-6.9-.1.5 13.1 1.5 13 2-2.9 31.2 5 121 5 131 0 29.5 24.2 158.5 85 410-4 5.5-18.1 17.5-23 24.1-66.3 78-19.5 321.8 120 320 105.8 200.8 300.5 178.4 389.1 182.3 85-6 260.2 22.2 386.9-182.4z"
		/>
		<linearGradient
			id="ch-topdown55002a"
			x1={1080}
			x2={838}
			y1={1733.3881}
			y2={1737.6119}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55002a)"
			d="M838 89s49.6 30 122 30c69.1 0 120-32 120-32v195s-91.5-7-120-7-122 7-122 7V89z"
			opacity={0.2}
		/>
		<path d="m839 85 240 1s-37.2 34-120 34-120-35-120-35z" opacity={0.4} />
		<path
			d="M594 307c-15.9 2.7-133.8 18.6-185 41 21.8-53.5 82.9-189.7 154-238 2.7 15.4 28.4 142.6 31 197zm111-13c.3-14.4-12.3-188.5-44-241 22-9.1 112.1-46 299-46 66 0 217.8 4.1 297 46-13.9 44.1-42 133-42 241-7.8-.6-135-10-135-10V85l-241-1 1 196s-122 12.4-135 14zM384 485c-.6 7.4-6.6 83.5 18 207 30.8 154.4 73.4 352.1 95 422 13.9-17.3 20.5-25.4 28-30-7.8-32.2-56.1-253-66-296s-38.6-188.1-34-298c-14-2.8-28.2-4.7-41-5zm78 16c16.3 6.1 35 14.9 39 23 3.1 18.2 83.4 479.6 93 516-14.1 7.6-36 19-36 19s-47.3-227.7-76-393c-15.2-87.4-20.9-155.3-20-165zm68 580c3.6 10.1 37 108 37 108l-17 13 5 16 15-11s51.4 123 57 129c-.9 11.7-12.6 92.9-13 101-9.1-11.6-48.9-72.7-69-120s-61.9-164.8-64-175c3.5-13.2 41.1-58 49-61zm857 0c6.3 5.3 42.3 40.3 51 71-11.9 33.7-51.5 139.6-54 148s-71.3 124.1-79 130c-1-14.4-11.3-85.5-15-95 9.3-18 48.9-107.6 55-128 8.8 4.6 17 10 17 10l4-16s-16.9-11.8-16-12 33.9-99.8 37-108zm27-556c4.4-3.8 38.8-23.8 43-24-.5 15.9-15.9 159.5-28 217s-70 342-70 342l-37-19s45.5-240.2 50-264 41.7-240.3 42-252zm79-35c6.3-2 41-5 41-5s-.9 127.8-15 198-98 433-98 433-21.5-25.2-26-29c4.2-19.8 88.1-399 91-444s8.8-139 7-153zm-136-384c-4.5 16.1-30.7 145.7-32 203 31.1 2.8 150.8 19.5 184 39-12.6-33.9-71.9-173.8-152-242z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown55002b"
			x1={958.595}
			x2={958.595}
			y1={1889}
			y2={1612}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown55002b)"
			d="M594 308c-1.3-19.7-23.6-195.3-40-222 18.8-12.4 82.3-50.2 99-55 8.2 19.4 52.8 133.3 51 264-26.3 1.9-85.8 8.4-110 13zm729.2 0c1.3-19.7 23.6-195.3 40-222-18.8-12.4-82.3-50.2-99.1-55-8.2 19.4-52.9 133.3-51 264 26.3 1.9 85.9 8.4 110.1 13z"
			opacity={0.2}
		/>
		<path
			fill="#bfbfbf"
			d="m839 1326 16-35-84-640s-25.1-30.4-33-53c5.5 46.3 101 728 101 728zm225-39 18 39 100-725-20 35-98 651z"
		/>
		<path fill="#8c8c8c" d="m840 1327 15-35h210l15 37-240-2z" />
		<linearGradient
			id="ch-topdown55002c"
			x1={352.3977}
			x2={457.1507}
			y1={1451.1816}
			y2={1765.5316}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55002c)"
			d="M365.5 490.4C329.2 430 317.1 229.8 499 176c-78.4 110.4-109.5 231.1-126.3 308.4-2.3 1.8-5.7 6-7.2 6z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown55002d"
			x1={1498.88}
			x2={1498.88}
			y1={1426.073}
			y2={1757.0917}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55002d)"
			d="M1422 177c180.1 49.6 171 267.1 127 316-.2-.9-9.8 15.6-12 15-3.7-1.1-.7-19.3-1-21-.1-.6 11.3-3 11.2-3.6-11.6-60.1-45.5-189.1-125.2-306.4z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown55002e"
			x1={537.437}
			x2={424.6411}
			y1={530.6524}
			y2={873.7214}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.499} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55002e)"
			d="M474 1028c2.9 8.7 17.6 65.7 23 86-5 9.6-12.4 22.8-16.4 28.9 27 82.4 65.3 192.7 90.4 227.1-85.8 16.4-194.4-156-131.2-297.3 3.3-7.4 14.8-28.8 34.2-44.7z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown55002f"
			x1={1362.4058}
			x2={1505.6708}
			y1={538.0158}
			y2={877.1707}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55002f)"
			d="M1349 1370c72.1 17 214.3-155.1 119-318-6.9-8.3-15.3-16.3-24-20-1.9 5.9-14.2 56.3-22 81-.6 1.9 12.1 27 15.8 35-20.8 65.6-49.6 148.3-88.8 222z"
			opacity={0.302}
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M562.5 109.1c-19.8 16-36.9 33.8-51.5 53.1C427.5 270.1 388.5 401.3 371.3 489m986.9-381.3c19.4 15.8 36.5 33.8 50.8 54.3 83.5 107.9 122.5 239.2 139.7 326.9M474 1029c-4.2 3.4-12.1 9.1-22 22.1-66.3 78-19.5 321.8 120 320 105.8 200.8 300.5 178.4 389.1 182.3 85-5.9 260.2 22.2 386.9-182.4 139.5 1.8 186.3-242 120-320-14.9-18.1-24-21-24-21M1258.3 51.4C1171.7 17.8 1066.1 6 961.4 6c-125 0-223.5 16.2-299.6 46.4"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m440 1254 70-25" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1479.3 1253.3-69.6-24.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M481 1147c11.3-46.3 75.6-91.9 113.8-106-25.4-135.8-69.7-350.8-93.7-513.2-15.5-28.6-94.9-44.6-128.1-43.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M502 533c-5.7-30.1-10.3-162.1-13-206" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1415 533c5.7-30.1 10.3-162.1 13-206" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M559 1059.6c-20.8-109.2-52-252-77.1-383.9-12.1-63.6-15.5-120.3-21.3-177.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M524 1085.8c-25.4-119.5-61-267-87.2-413.5-10.3-57.6-12.5-129.4-12-182.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1359.9 1059.8c20.8-109.1 51.9-251.9 77-383.8 12.1-63.6 15.5-120.3 21.2-177.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1394.9 1086c25.4-119.5 60.9-266.9 87.1-413.4 10.3-57.6 12.4-129.4 12-182.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M496.6 1114.6c-24.1-96.4-70.6-284.7-98.3-440.1-11.7-65.5-13.1-128.8-13.5-190.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1421.9 1114.8c24.2-96.4 70.8-284.9 98.6-440.3 11.7-65.6 13.1-128.8 13.6-190.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M532.2 1081.1c10.2 31.1 22.2 68.9 35.3 107.1m5.8 16.9c16.9 48.1 35.5 95.5 54.7 129.9-17.5 75.9-12 101-12 101"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M548.7 1201c.4-.2 9.1-5.4 18.2-11.9 9.3-6.6 20.6-11.2 20.6-11.2l5.8 16.1s-11.5 4.5-20.9 10.9c-9.1 6.2-18.1 12.3-18.1 12.3s-4.7-12.5-5.5-14.8c0-.3-.3-1-.1-1.4zm819.2 0c-.4-.2-9.1-5.5-18.2-12-9.3-6.6-20.6-11.3-20.6-11.3l-5.8 16.2s11.5 4.5 20.9 11c9.1 6.2 18 12.4 18 12.4s4.7-12.6 5.5-14.9c.1-.3.5-1.1.2-1.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1386 1081c-10.3 31.3-22.4 69.4-35.6 107.9m-5.6 16.1c-16.9 48.2-35.6 95.6-54.8 129.9 17.5 75.9 12 101 12 101"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1543.9 484c-33.3-.9-112.8 15.2-128.3 43.8-24 162.4-68.3 377.3-93.8 513.1 38.3 14.1 102.7 59.7 114 106 .7 1.8 1.3 4.2 1.3 4.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M560.5 860c79.7-17.5 184.3-31 207.5-31" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M742.8 847.6c4.9-.6 9.4 2.8 10.1 7.8 0 .1 3.9 29.9 3.9 30 .5 4.8-2.9 9.2-7.8 9.8-4.8.6-9.2-2.6-10-7.4 0-.1-3.9-30-4-30.1-.6-5 2.8-9.5 7.8-10.1zM589 870.9c4.9-1 9.6 2.1 10.7 7 0 .1 6.2 29.5 6.2 29.6.8 4.8-2.3 9.4-7 10.4s-9.4-1.9-10.6-6.6c0-.1-6.2-29.6-6.2-29.8-1.1-4.8 2-9.6 6.9-10.6zm587.1-22.8c-4.9-.7-9.4 2.8-10.1 7.8 0 .1-3.9 30.1-3.9 30.2-.5 4.8 3 9.2 7.8 9.9 4.8.6 9.2-2.7 10-7.4 0-.1 3.9-30.2 4-30.3.6-5-2.9-9.6-7.8-10.2zm153.7 23.4c-4.9-1-9.6 2.1-10.7 7 0 .1-6.2 29.7-6.2 29.8-.8 4.8 2.3 9.4 7 10.4s9.4-1.9 10.6-6.6c0-.1 6.2-29.8 6.2-29.9 1.1-4.9-2-9.6-6.9-10.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M412 348c130-55.6 522-71 522-71" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M593 307c-5.3-79.4-25-194.1-39-221 35.1-24.6 67.3-42.3 99-54 15.8 36.9 51 128.9 51 262"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M572 159c21.2-7.6 108-46 108-46" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1325.7 307.7c5.3-79.4 24.9-194.3 38.9-221.3-35-24.7-67.1-42.3-98.7-54.1-15.7 36.9-50.8 129.1-50.8 262.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1346.6 159.5c-21.1-7.6-107.7-46.1-107.7-46.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1356.3 860.1c-79.8-17.5-184.9-31.2-208.1-31.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1509 348c-129.9-55.6-526.8-71-526.8-71" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M839 282V89s45.4 29.5 119 30h3c73.5-.5 119-30 119-30v193"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M835 85s62.4 3 124.9 3c62.5 0 125.1-3 125.1-3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M806 288V16" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1114 288V16" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 119V86" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M930 119V86" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M900 112V86" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M870 104V86" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M990 119V86" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1020 112V86" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1050 104V86" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1164.7 627.5c-11.8 78-100.7 665.5-100.7 665.5l-209 1s-65.8-498.2-84.9-642.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1064 1288s11.9 29.6 15.8 38.4c.7 1.6 2.2 1.6 2.2 1.6s90.2-653.1 102.6-742.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1081.9 1326.3c-10.3 72-19.9 139.7-19.9 139.7l-204 1s-10.1-69.7-20.2-140.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m861 1464 21-27c.7-.9 154-1 154-1l26 30" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M855 1288s-11.9 29.6-15.8 38.4c-.7 1.6-2.2 1.6-2.2 1.6L733 575"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M839 1328h241" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m917 1291-5 38" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M846 1311h225" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1002 1291 5 38" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1126 1012-19-19H816l-24 13" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M960 276c130.3 0 236 105.7 236 236s-105.7 236-236 236-236-105.7-236-236 105.7-236 236-236z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m935 1552 2-21h45l2 23" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1062 1549-2-22 39-4 3 23" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1169 1527-10-22 34-14 11 19" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m856.5 1549.6 2-22.1-39.1-4-3 23.1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m749.4 1527.5 10-22.1-34-14.1-11 19.1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1346 1373c34.1-57.7 68.9-156.5 93.9-233.9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M574 1373c-35-59.2-70.9-162-95.9-240.3" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1420.1 175.9c152.3 33.6 185.7 243.6 130.7 319.2-10.4 14.2-17.8 18.9-17.8 18.9"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1377 1312c25.7-14.9 80.4-86.7 68-189-6.4-63.1-13-43-13-43"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M499 176c-152.7 33.6-186.1 243.5-131 319 8 8.7 16 11 16 11"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m362 301 57 24" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1559.1 300.6-57.1 23.9" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M543 1312c-25.7-14.9-80.4-86.7-68-189 2.7-27.7 13-43 13-43"
		/>
		<g id="Animation-Tower">
			<path d="M1152 200.7c5.9 5.6 13.7 25.7 21.8 51 18.2 17.6 25.4 67.3 25 89.3 7.8 30.5 13.2 53.7 13.2 53.7s22 122.1 22 174c0 51.9-22.9 173.9-81 290-10.1 9.9-31.8 14.7-48.4 17-8.4 6.5-21.6 10-21.6 10v12l-40 82 1 162h12l10 163h-23l2 165h10l14 163h-25l1 245H875l1-245h-25l14-163h10l2-165h-23l10-163h12l1-162-40-82v-11s-14.6-1.1-22-11.1c-16.5-2.3-37.9-7.1-48-16.9-58.1-116.1-81-238-81-290s22-174 22-174 5.5-23 13.4-53.3c-2.6-25 5.5-68 25.2-89.1 8.1-25.4 15.8-45.7 21.4-51.6 10.5-11 100.7-43 191.7-43.1 89.7.2 180.2 31.6 192.3 43.1z" />
			<path
				fill="#f2f2f2"
				d="M773 197.7c18.4-5.1 91.2-39.6 187-38.7 57.6.5 139.1 13.4 190 40.7 5.7 6.1 25 50 25 50s24.1 41.4 24 93c54 215.8 44.1 314.4-47 515.1-10.8 8-29.2 14.2-47 16-12.6 7.9-21 12-21 12l-1 11-39 84v159l12 2 10 162-21 2v163l10 1 14 163-24-1 1 246H875l2-244-26-3 14-161 9-1 4-162-25-1s10.9-165 11-163c.1 2 12-2 12-2l3-161s-41.4-82.7-42-82-1-10-1-10-18.9-7.3-22-13c-11.1-2-39.3-6.2-45-16-50.3-86.3-83-231.1-83-291 0-59.9 24-197.1 36-225-.4-25.3.8-61.7 25-90 10.8-30.8 14.4-52.8 26-55.1z"
			/>
			<path fill="#e6e6e6" d="m839 896 246 4-44 83-163-4-39-83z" />
			<linearGradient
				id="ch-topdown55002g"
				x1={961}
				x2={961}
				y1={1033}
				y2={1231}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={1} stopColor="#f1f1f1" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55002g)"
				d="m1084 887 21-11-1-184s-147.3 34.2-287-3c0 39.8 1 188 1 188s7.4 9.7 20 9c.3-24.8-1-177-1-177s86.1 42.3 243 5c.4 36.1 4 173 4 173z"
				opacity={0.302}
			/>
			<linearGradient
				id="ch-topdown55002h"
				x1={1245}
				x2={1102.5603}
				y1={1391.9994}
				y2={1391.9994}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#cdcdcd" />
				<stop offset={1} stopColor="#f2f2f2" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55002h)"
				d="M1104 181c20.4 4.3 42.8 12.5 49 21s21 51 21 51-47.4-13.4-39 44c20.4 58.7 55.3 45 62 45 11.6 35.2 38 170.3 38 228s-44.4 235-82 289c-15.2 10.8-41.7 16.1-48 16 8-246.3-30.8-558.4-1-694z"
			/>
			<linearGradient
				id="ch-topdown55002i"
				x1={834.969}
				x2={692.9678}
				y1={1391.9994}
				y2={1391.9994}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#cdcdcd" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55002i)"
				d="M813.6 181c-20.3 4.3-42.6 12.5-48.8 21-6.2 8.5-20.9 51-20.9 51s47.2-13.4 38.9 44c-20.4 58.7-55.1 45-61.8 45-11.7 35.2-38 170.3-38 228s44.3 235 81.8 289c15.2 10.8 41.6 16.1 47.8 16-7.9-246.3 30.7-558.4 1-694z"
			/>
			<linearGradient
				id="ch-topdown55002j"
				x1={1167.1931}
				x2={1167.1931}
				y1={1575}
				y2={1666.3822}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55002j)"
				d="M1177 254c10.8 14.8 22.7 58.9 22 88-12.6 1.7-30 3-30 3s-52.9-37.6-26-91c8.5-.7 18.6-.3 34 0z"
				opacity={0.2}
			/>
			<linearGradient
				id="ch-topdown55002k"
				x1={750.9951}
				x2={750.9951}
				y1={1575}
				y2={1666.0079}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55002k)"
				d="M741.1 254.4C730.3 269.1 718.3 313 719 342c12.6 1.7 30.1 3 30.1 3s53.2-37.4 26.1-90.6c-8.4-.7-18.6-.3-34.1 0z"
				opacity={0.2}
			/>
			<path
				d="m865 1470 188-1 16 164H852l13-163zm19 34c0 4.7-.9 16 6 16s7-10.8 7-16-1.4-14-5-14-8 9.3-8 14zm138 0c.4 6.5.4 15.5 6 16s8.4-10.7 8-17-4.9-12.3-8-12-6.4 6.5-6 13zm33-361 11 162-213-1 11-162 191 1zm-173 34c.3 6.5 2 15.1 6 15s7.3-12.8 8-18-3.3-10.6-6-11-8.3 7.5-8 14zm142 0c-1.2 6.5 3.1 14.1 7 15s7.4-8.2 7-16-3.5-13.4-7-14-5.8 8.5-7 15zM875 893l4-172s25.2 9 79 9 122-17 122-17l1 185-38 82-166-1-41-83 39-3zm-99-456-1-96h109l-1 95-107 1z"
				className="factionColorPrimary"
			/>
			<linearGradient
				id="ch-topdown55002l"
				x1={829.5}
				x2={829.5}
				y1={1483}
				y2={1579}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" stopOpacity={0.302} />
				<stop offset={0.35} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" stopOpacity={0.2} />
			</linearGradient>
			<path fill="url(#ch-topdown55002l)" d="m776 437-1-96h109l-1 95-107 1z" opacity={0.702} />
			<linearGradient
				id="ch-topdown55002m"
				x1={961.5}
				x2={961.5}
				y1={1214.6791}
				y2={1761.4923}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.3} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55002m)"
				d="M819 181c71.6-28.2 212.7-31.7 281 0-19.1 91.6 4 454.5 4 509-22.3 4.5-130.8 23.7-225.1 11.1-1-11.8 1.3-152.8 2.1-162.1 0-5.1 11.7.7 11-4-.8-5.7-.1-8.4-2-14-3.1-.8-5.4-1.8-9-2-.2-6-.5-7.6-1-15-.3-3.9 12 1.3 12-3-.1-5.6.2-10.2-1-18-.7-4.2-8.7 2.8-9-2-.3-4.6-.7-8.2-1-14-.1-2.7 10.4 1.7 10-1-.9-5.8 1-6.6 0-14-.6-4.3-7.6 3.3-8-3-1.3-21.6 0-80.6-1-110-12.9-9.5-36.8-14-51-13.9.1-64.7-2.8-119-12-144.1z"
				opacity={0.102}
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1152 200.7c5.9 5.6 13.7 25.7 21.8 51 18.2 17.6 25.4 67.3 25 89.3 7.8 30.5 13.2 53.7 13.2 53.7s22 122.1 22 174c0 51.9-22.9 173.9-81 290-10.1 9.9-31.8 14.7-48.4 17-8.4 6.5-21.6 10-21.6 10v12l-40 82 1 162h12l10 163h-23l2 165h10l14 163h-25l1 245H875l1-245h-25l14-163h10l2-165h-23l10-163h12l1-162-40-82v-11s-14.6-1.1-22-11.1c-16.5-2.3-37.9-7.1-48-16.9-58.1-116.1-81-238-81-290s22-174 22-174 5.5-23 13.4-53.3c-2.6-25 5.5-68 25.2-89.1 8.1-25.4 15.8-45.7 21.4-51.6 10.5-11 100.7-43 191.7-43.1 89.7.2 180.2 31.6 192.3 43.1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M870 169c3.4 13.7 7.5 40.7 8 88-23.8 9.2-47 21-47 21"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1050 169c-3.4 13.7-7.5 40.7-8 88 23.8 9.2 47 21 47 21"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M823.3 198.4c10.5-3 28.3-7.4 50.4-11.3m173.7 1.5c20.2 4.2 38.7 9.3 53.6 14.4"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1104 876.8 1-185s-165.1 34.7-288-4c-1 73.2-1 190-1 190"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M872 1633h177" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M871 1469.9h180" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M868 1304.9h178" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M872 1141.9h177" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M889.8 1162c3.8.2 6.8 3.7 6.6 7.9 0 .1-.6 14.8-.6 15-.3 4-3.5 7.1-7.2 7-3.7-.2-6.6-3.5-6.6-7.6 0-.1.6-14.8.6-14.9.2-4.2 3.4-7.5 7.2-7.4zm2 328c3.8.2 6.8 3.7 6.6 7.9 0 .1-.6 14.8-.6 15-.3 4-3.5 7.1-7.2 7-3.7-.2-6.6-3.5-6.6-7.6 0-.1.6-14.8.6-14.9.2-4.2 3.4-7.5 7.2-7.4zm136.6 0c-3.7.2-6.6 3.7-6.5 8 0 .1.6 14.9.6 15 .4 4.1 3.4 7.2 7.1 7 3.6-.2 6.5-3.5 6.5-7.6 0-.1-.6-14.8-.6-15-.2-4.2-3.4-7.6-7.1-7.4zm2-328c-3.7.2-6.6 3.7-6.5 8 0 .1.6 14.9.6 15 .3 4.1 3.4 7.2 7.1 7 3.6-.2 6.5-3.5 6.5-7.6 0-.1-.6-14.8-.6-15-.2-4.2-3.4-7.6-7.1-7.4z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m877 978.8 166 1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1083 891.8-2-178s-138.5 39.5-244-5c-.4 51 0 186 0 186"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m837 894.8 247 2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m837 807.8 245 1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M817 688c0-54 6.7-152.4 10.8-250.1m3-113c.3-63.3-2.5-117.5-11.8-146.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m727.3 385.9 34.7 4.8m-38.8 24.9 34.7 4.8M719 445.3l34.7 4.8m440.4-64.2-34.4 4.8m38.5 24.9-34.4 4.8m38.5 24.9-34.4 4.8"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1104.9 689c0-113.5-29.9-422.4-2-510.5" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M929 414h164" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1079.5 661c4.7 0 8.5 3.8 8.5 8.5s-3.8 8.5-8.5 8.5-8.5-3.8-8.5-8.5 3.8-8.5 8.5-8.5zm-240 0c4.7 0 8.5 3.8 8.5 8.5s-3.8 8.5-8.5 8.5-8.5-3.8-8.5-8.5 3.8-8.5 8.5-8.5z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1173 253.7h-30s-29.4 52.4 28 90c11-.4 26-1 26-1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M745.2 253.7h30.2s29.6 52.4-28.2 90c-11.1-.4-26.2-1-26.2-1"
			/>
			<path
				fill="#f1f1f1"
				d="M789 340c12.6-.2 98-1 98-1s-25.3-15-50-15-60.6 16.2-48 16zm95-1.8 25.5-1s2.3 26.9 2.2 37.4c40.1 42.9 1.8 79.1 1.8 79.1l-1.5 32.6-19.9-.6-.7 18.4-10.6-.3-1.4 17.7 11.4 1v16.3l-9.2 1.1-5.2 344.7-89.2.8-8.4-346.4-12.8-.9-.7-17 11.2-3.3.2-13.4-10.8-3.1-1.4-16.2-16.3-.1 3.3-145.1s10.8.6 24 1c.9 7.1 1.4 95 1.4 95l108 2-.9-99.7z"
			/>
			<linearGradient
				id="ch-topdown55002n"
				x1={830.7401}
				x2={830.7401}
				y1={1433.47}
				y2={1580.8}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.503} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55002n)"
				d="M884.1 339.5c16.2-.2 27.2-.3 27.2-.3l1.5 147.2-19.1.2-9.9-3.4-1.3-15.1 9.1-1.6-.3-15-8-1.4c.1-.1-.1-64.7.8-110.6zm-108.3 1.4c-14.4.2-23.9.3-23.9.3l-3.3 143.9 29.1-.7.6-14-15-1.7.8-17 13 .6c0 .1-.2-63.6-1.3-111.4z"
				opacity={0.302}
			/>
			<path
				fill="#ccc"
				d="m778.8 503.5 103.2.5.3 17-102.3.5-1.2-18zm-1.4-35.1 106.3-.3-.7 15-104.3 2.4-1.3-17.1z"
			/>
			<path
				fill="#ccc"
				d="M911.8 374.3c13 8.2 27.3 42.2 2.5 80.2-.5-38.9-4.3-49.2-2.5-80.2zm-30.5-34.4c-19.1-12.9-52.5-26.8-89.2.1 22.4 1.3 62.6-1.4 89.2-.1z"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M786 886h89l6.5-347.6 9.9-.4v-17.7l-9.9.4V503l10.6-1.1.6-19.8-9.9.4-.6-15.5 9.2.3V451l-7.8-.4.2-13.4-107.8-1.3.6 15.5-11.3.4-.8 15.6 13.5.2.7 17-13.5-.2.8 18.6h12l1 16h-12v19h13l6 348z"
			/>
			<path fill="none" stroke="#191919" strokeWidth={10} d="M776 436v-96" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="M884 436v-96" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m771 503.3 112.6-1" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m773.7 519.4 109.2 1.3" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m881.5 538.4-107-.7" />
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="m896.4 486.9 15.6-.6.4-146.4-161.5.8-1.2 144.3 12.7-.5"
			/>
			<path fill="none" stroke="#191919" strokeWidth={10} d="M913.7 457.9c9.9-12.9 28.8-52.3-1.5-88.4" />
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M784.2 340c39.8-26.3 85-11.4 99.1-2.6 1.4.8 3.7 2.5 3.7 2.5"
			/>
			<path fill="none" stroke="#191919" strokeWidth={10} d="m775.2 451.5 109.8-.9" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m770.9 467.9 114-1" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m772.3 484.1 114.7-1.8" />
			<circle id="Animation-Coordinates" cx={960.5} cy={521.5} r={0.5} fill="#32ff00" />
		</g>
		<circle cx={959.7} cy={941.9} r={418.4} fill="none" stroke="#58ff00" strokeWidth={5} />
	</svg>
);

export default Component;

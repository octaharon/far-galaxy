import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#999"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M609 1276c2.8-20.8 14.6-43.8 23-47s59-33 59-33l57 10 22 27s4.5 40 4 43-34.9 20.2-36 20-70 0-70 0-61.7.8-59-20z"
		/>
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M701 1318c-38.3 0-98.3-10.9-98-34s9.2-22.6 93-5c52.1 12.2 79 15 79 15l1 21s-36.7 3-75 3z"
		/>
		<path
			fill="#999"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1309 1276.8c-2.7-20.9-14.6-43.9-22.9-47.2-8.3-3.2 3.9-96.7 3.9-96.7l-153 2-71 38s77.9 100.8 78.4 103.8c.5 3.1 34.8 20.3 35.9 20.1 1.1-.2 69.8 0 69.8 0s61.7.9 58.9-20z"
		/>
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1217.2 1319c38.2 0 98.1-11 97.8-34.1-.3-23.2-9.2-22.7-92.8-5-50.7 11.9-67.2-5.8-67.2-5.8l-11.6 20.9-1 21.1c0-.2 36.6 2.9 74.8 2.9z"
		/>
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1179.1 323.9s21-43.1 24.8-60.9c8.6 22.5 23.7 80.6 28 80.4l21.7-79.4s57.6 100.4 38.5 108c-12.1-2.1-38-8.6-81 10s-32-58.1-32-58.1z"
		/>
		<path
			fill="#999"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1259.9 366.5c21.4-3.8 43 5.3 47.1 35 3 21.5 2.2 63.1-41 74-103 25.7-65.1-121.9-65.1-121.9s22.8 17.9 59 12.9z"
		/>
		<path fill="#e6e6e6" stroke="#1a1a1a" strokeWidth={10} d="m1232.9 406.4 60.1-1 3 91-62.1 1-1-91z" />
		<linearGradient
			id="ch-topdown40000a"
			x1={1117.5583}
			x2={1269.6833}
			y1={1490.6564}
			y2={1435.2874}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#969696" />
			<stop offset={0.352} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000a)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1161.8 301.5c75.6 8.4 153.1 145.8 80.1 268.9-73 123-108.1-120.9-108.1-120.9s-47.6-156.4 28-148z"
		/>
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M739.7 329.4s-21-43.1-24.8-60.9c-8.6 22.5-23.6 80.6-27.9 80.5l-21.6-79.5S614.7 355.6 622 376c10.9-9 30.1-11 54.8-9.6 9.3.6 19.9-2.2 31.7 2.9 42.9 18.7 31.2-39.9 31.2-39.9z"
		/>
		<path
			fill="#999"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M659 367c-21.4-3.8-42.9 5.3-47 35-3 21.5-2.2 63.2 41 74 102.9 25.8 65-122 65-122s-22.9 18-59 13z"
		/>
		<path fill="#e6e6e6" stroke="#1a1a1a" strokeWidth={10} d="m686 407-60-1-3 91 62 1 1-91z" />
		<linearGradient
			id="ch-topdown40000b"
			x1={649.8198}
			x2={801.7628}
			y1={1499.5322}
			y2={1444.2292}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#969696" />
			<stop offset={0.352} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000b)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M757 302c-75.5 8.4-152.9 145.9-80 269s108-121 108-121 47.5-156.4-28-148z"
		/>
		<linearGradient
			id="ch-topdown40000c"
			x1={959.009}
			x2={959.009}
			y1={595.9959}
			y2={1716}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.251} stopColor="#bfbfbf" />
			<stop offset={0.495} stopColor="#bfbfbf" />
			<stop offset={0.754} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000c)"
			d="M810.4 1300.4c-21.7-34.4-73.4-66.1-98.4-110-25-43.9-16-235.2-16-293.4 0-29.8-.6-76.5-7-102-6.1-24.2-16.6-46.6-17-64-.8-35.7-9.6-120.7 44.5-235.9 29.1-61.9 17-141.1 42.5-185.1s51.4-106 202.2-106 171 42.4 200.8 102 11.3 118.7 43.9 203.1c32.6 84.4 41.1 113.5 41.1 232.9.7 26.8-23 21.6-23 141s16.4 242.1-8.2 321.4c-42.8 66.3-110.4 79.3-140.9 101-30.4 21.8-242.8 29.4-264.5-5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M810.4 1300.4c-22.2-34.1-73.4-66.1-98.4-110-25-43.9-15-286.2-15-344.4s-24.2-76.3-25-112-9.6-123.7 44.5-238.9c29.1-61.9 16-141.1 41.5-185.1s52.4-106 203.2-106 167 44.4 196.8 104 15.3 116.7 47.9 201.1c32.6 84.4 40.1 116.5 40.1 235.9-4.3 31.7-23 35.6-23 155s24.5 193.7 0 273-117.5 110.7-148 132.4c-15.8 11.2-6.2-45.3-134.1-45.3-120.8 0-119.9 56.5-130.5 40.3z"
		/>
		<linearGradient
			id="ch-topdown40000d"
			x1={959.5019}
			x2={959.5019}
			y1={669}
			y2={1732}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={0.35} stopColor="#f2f2f2" />
			<stop offset={0.497} stopColor="#f2f2f2" />
			<stop offset={0.65} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000d)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M963 1214c-30.7 0-132.8-8.1-181 36-20.6-21.2-33-35-33-35l24-16s22.3-127.6-32-242c6.9-3.5 13.8-5.3 13-13-8.7-86.4 10.8-178.3 5-220 3.2-.9 25.9-25 40-59 17.4-41.8 22.5-117.1-48-140 .8-10.9 1.7-21.5 2.5-31.9 3.9-2.9 7.2-5.7 12.5-8.1 7.7-90.8 3.3-165.9-9-203 19.1-52 75.4-94 203-94s166.8 36.6 194 64c-12.9 23.2-11.3 140.4-12 228 0 1.4 24 15.6 24 17 .9 4.1.2 8.5 0 25-14.2 3-44.4 29.8-53 57-12.8 40.6.2 102.2 47 146 15.3 137.6 6.5 184.9 3 220-.3 2.8 15.3 11.3 15 14-42.5 76.4-47.1 188.1-32 237 21.7 16 22.1 16.2 23 17-1.2 1.1-33 38-33 38s-34.8-37-173-37z"
		/>
		<path
			d="M867 1212c3.5-25.9 11.6-129.1 18-311 11.7-2.8 13.3-2.6 18-3 5.2-150.2 5.1-329.3-2-466 8.7-3 13.2-5.1 13-8-6-110.8-7.8-188.9-19-227 11.5-1.1 22.5-3 33.2-3.6 36.2-2.2 68-.8 96.8 3.6-8.2 52.3-11.5 100.1-19 223 .3 5.5 13 11 13 11s-5.1 113.5-6 227c-.9 109.7 2.4 228.3 3 237 6.8.6 19 2 19 2s3.9 101.4 8.4 195.5c3.9 81.5 8.6 122.5 8.6 122.5s-43.9-4.7-94.5-5.4c-16.8-.2-33.2-1.1-50.2-.7-12.8.4-24.6 1.2-40.3 3.1z"
			className="factionColorSecondary"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1169 951c-84.6-76.8-324.1-74.2-421 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1144 482c-84.6-76.9-282.1-73.3-379 3" />
		<linearGradient
			id="ch-topdown40000e"
			x1={643.7805}
			x2={579.2665}
			y1={1206.7164}
			y2={1431.7024}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000e)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M435.1 585.8c12.3-30 28.4-70 79-80.1 72-14.4 126.2-29.9 178 2 69.9 43 66.1 44.4 84 63.1 17.9 18.7 10.5 143.2-82 143.2-29.8 0-51-70.2-57.2-93.2-.1-.4 14.2-3.6 24.2-10.9 9.5-7 11.1-6.3 11.8-8.9-1.5 2.3-2.5 4.5-12 9-12 5.6-25 7-25 7s-67.1 16.9-93.8 23.9c-26.7 7-119.3-25.1-107-55.1z"
		/>
		<linearGradient
			id="ch-topdown40000f"
			x1={771.5837}
			x2={635.7797}
			y1={1226.0269}
			y2={1387.8728}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#757575" />
			<stop offset={0.684} stopColor="#8e8e8e" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000f)"
			d="M686 573c20.4-15.3 15.9-33.6-13-69 64.8 34.9 88.2 57.3 100 73 7.1 14.4 11.9 58.8-12 95-25 37.9-52.3 38-65 38s-22.6-8.8-38-43c-6.1-13.9-11.2-28.8-15-43-.6-.3 17.3-3.9 34-20 3.3-14.1 6.5-18.4 9-31z"
		/>
		<linearGradient
			id="ch-topdown40000g"
			x1={597.0091}
			x2={614.9781}
			y1={1216.262}
			y2={1351.1171}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#bdbebe" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000g)"
			d="m554 633 21-132s48.5-13.5 69 18c13.5 20.8 19 83 19 83s-7.3 4.2-17 7c-32.6 8.1-92 24-92 24z"
		/>
		<linearGradient
			id="ch-topdown40000h"
			x1={633.1221}
			x2={687.1221}
			y1={1297.2625}
			y2={1287.7415}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#3b3b3b" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000h)"
			d="M672 693s-19.9-30.8-30-70c12-2.1 26-8.8 36-20 1-3.9 4-15.8 8-29 7.7-25.7 10 59 10 59l-24 60z"
		/>
		<linearGradient
			id="ch-topdown40000i"
			x1={1337.8569}
			x2={1273.3429}
			y1={1205.6057}
			y2={1430.5917}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000i)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1480.9 585.8c-12.3-30-28.4-70-79-80.1-72-14.4-126.2-29.9-178 2-69.9 43-66.1 44.4-84 63.1-17.9 18.7-10.5 143.2 82 143.2 29.8 0 51-70.2 57.2-93.2.1-.4-14.2-3.6-24.2-10.9-9.5-7-11.1-6.3-11.8-8.9 1.5 2.3 2.5 4.5 12 9 12 5.6 25 7 25 7s67.1 16.9 93.8 23.9c26.7 7 119.3-25.1 107-55.1z"
		/>
		<linearGradient
			id="ch-topdown40000j"
			x1={1126.5374}
			x2={1262.0514}
			y1={1192.7462}
			y2={1382.4502}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#686868" />
			<stop offset={0.868} stopColor="#babbbb" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000j)"
			d="M1223 515c-50.4 28.2-71.2 47.2-83 63-2.9 15.1-7.5 66 14 93 12.6 19.2 25.1 27.9 35.9 32.9 10.7 4.9 19.8 6.1 26.1 6.1 12.7 0 21.4-.3 41-40 6.8-15.8 14.9-33.8 16-47 .7-.3-17.9-3.5-31-16-1.2-.6-1.2-.6-3-2-1.9-2.8-3.3-21.2-10-33-9.6-17.1-25.8-32.7-6-57z"
		/>
		<linearGradient
			id="ch-topdown40000k"
			x1={1346.1893}
			x2={1318.4744}
			y1={1261.7153}
			y2={1335.6073}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#3a3a3a" />
			<stop offset={0.798} stopColor="#b7b7b6" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000k)"
			d="m1370.8 634.9-10.7-71.6s-48.5-11.8-69 18c-16 23.2-12.3 29.6-12.3 29.6l92 24z"
		/>
		<linearGradient
			id="ch-topdown40000l"
			x1={1276.9156}
			x2={1229.5907}
			y1={1290.777}
			y2={1279.642}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4c4c4c" />
			<stop offset={0.8} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000l)"
			d="M1238.8 699s25-34.3 35-75.1c-13.1-3.2-24.6-10.1-32.5-17.7-3.1-3.3-2.8-3.7-4.4-8.2-2.2-6.7-3.7-11.8-5-19-15.5 16 6.9 120 6.9 120z"
		/>
		<path
			fill="#f1f1f1"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M427.2 584.2 553.3 603l-53.8 403.7-120.2-17.5 47.9-405zm1068.9-.2L1370 602.8l53.8 404.2 120.2-17.5-47.9-405.5z"
		/>
		<linearGradient
			id="ch-topdown40000m"
			x1={657.4247}
			x2={571.5598}
			y1={676.4427}
			y2={912.3557}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.497} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000m)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m695 1009-196 57s-48.3 11.1-18 100c25.6 64.3 83.6 83.6 102 78s134.3-42.5 146-46c12.4-3.7 28.8-58.5 18-108-8-36.6-22.9-77.1-52-81z"
		/>
		<path
			fill="#f1f1f1"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M481 1079c32.1-16 73 10.9 95 53 22.6 43.2 28.5 101.9-2 114-18.8 8.3-49.8 8.1-85-55-44-87.3-9.1-111.2-8-112z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M512 1127.7c10.3-5 26.3 6.6 35.6 26.1s8.5 39.3-1.8 44.4c-10.3 5.1-23.3-6.6-32.6-26.1-9.4-19.5-11.6-39.4-1.2-44.4z"
		/>
		<linearGradient
			id="ch-topdown40000n"
			x1={1334.2236}
			x2={1275.9867}
			y1={909.6591}
			y2={676.0861}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.497} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40000n)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1234.5 1011 185 54.9s48.3 11.1 18 99.8c-25.6 64.2-83.6 83.5-102 77.9-18.4-5.6-134.3-42.5-146-45.9-12.4-3.7-28.8-58.4-18-107.8 8-36.6 33.9-75 63-78.9z"
		/>
		<path
			fill="#f1f1f1"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1437.5 1078.9c-32.1-16-73 10.9-95 52.9-22.6 43.1-28.5 101.8 2 113.8 1.9.6 41.9 18 85-54.9 44.2-84 9.1-111 8-111.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1406.5 1128.5c-10.3-5-26.3 6.6-35.6 26.1-9.3 19.4-8.5 39.3 1.8 44.3s26.3-6.6 35.6-26.1c9.4-19.4 8.6-39.3-1.8-44.3z"
		/>
		<g>
			<path
				fill="#8c8c8c"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M707 1293c0-10.5 16.8-33.3 57-40 10.3 11.2 21.3 23.6 33 36l-74 27s-16-9.9-16-23z"
			/>
			<path
				fill="#8c8c8c"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1208 1293.2c0-10.4-10.8-29.6-51-36.2-22.2 15.7-65 38-65 38l100 21s16-9.9 16-22.8z"
			/>
			<linearGradient
				id="ch-topdown40000o"
				x1={780.127}
				x2={737.561}
				y1={658.9404}
				y2={604.4584}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#4c4c4c" />
				<stop offset={0.487} stopColor="#8c8c8c" />
			</linearGradient>
			<path
				fill="url(#ch-topdown40000o)"
				d="M713 1292c-.3-6 12.2-23.9 44.1-33.5 1.5.1 3.1-.5 4.9.5 12.7 12.8 36 38 36 38s-85 39-85-5z"
			/>
			<linearGradient
				id="ch-topdown40000p"
				x1={1138.3372}
				x2={1159.9851}
				y1={657.2579}
				y2={601.5919}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#4c4c4c" />
				<stop offset={0.487} stopColor="#8c8c8c" />
			</linearGradient>
			<path
				fill="url(#ch-topdown40000p)"
				d="M1205 1300c0-19.7-13.9-30.1-46.4-38-18.4 12-61.6 34.9-61.6 35 0 0 108 44.5 108 3z"
			/>
			<linearGradient
				id="ch-topdown40000q"
				x1={957.4882}
				x2={957.4882}
				y1={167}
				y2={623}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#e6e6e6" />
				<stop offset={0.676} stopColor="#f2f2f2" />
			</linearGradient>
			<path fill="url(#ch-topdown40000q)" d="M1208 1297H707s-7.7 456 253 456c243.3 0 248-456 248-456z" />
			<path
				d="m715 1414 175.8 38 29.4 298.6s53 16.7 85.5-3.8c16-188.6 25.3-293.9 25.3-293.9l166.9-40.4s12.5-101.9 10-115.6c-58 0-461.7.1-501 0-1.2 38 8.1 117.1 8.1 117.1z"
				className="factionColorPrimary"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1208 1297H707s-7.7 456 253 456c243.3 0 248-456 248-456z"
			/>
		</g>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#e6e6e6"
			d="M746 1706h399c58.9 0 85.4-24.5 122-59s67.1-46.1 100-68 88.3-60.1 100-72c21.8-.6 112 0 112 0l-1-478-4-945-238-1s-120.5-39.4-153-50H721c-21.8 0-108.9 31.1-163 50-58 .4-239 0-239 0l2 1425h109s48.2 42.1 94 69 79.6 48.9 98 65 59.4 64 124 64z"
		/>
		<path
			fill="#ccc"
			d="M428 1507c-51.5-59.7-72.3-99.1-82-164-3.8-84.9-11-340.5-11-391s-3.9-480.7-2-531 6-247.3 73-287 130.1-43.5 145-50c-50-.5-219.9-1.4-229 1-2.1 16.7-2 1409-2 1409s-1.7 13.8 11 14 76.9-.7 97-1zm1062 0c51.4-59.7 69.3-99.1 79-164 3.9-84.9 14-340.6 14-391.1s3.9-480.8 2-531.1-5.9-247.4-73-287.1c-67-39.7-130-43.5-145-50 50-.5 219.9-1.4 228.9 1 2.1 16.7 2 1409.4 2 1409.4s1.7 13.8-11 14c-12.6.1-76.9-.8-96.9-1.1z"
		/>
		<linearGradient
			id="ch-topdown36001a"
			x1={297.9166}
			x2={409.2206}
			y1={871.6541}
			y2={826.684}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown36001a)"
			d="M431 1507c-20.3-19.4-77.3-94.2-83-165-11.7-.8-24 2-24 2s-8.2 151.4-2 160 73 3.5 109 3z"
		/>
		<linearGradient
			id="ch-topdown36001b"
			x1={1618.3226}
			x2={1507.3307}
			y1={882.5338}
			y2={819.7368}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown36001b)"
			d="M1489 1507c20.3-19.4 77.1-94.1 82.8-164.9 11.7-.8 23.9 2 23.9 2s8.2 151.3 2 159.9c-6.2 8.6-72.9 3.5-108.7 3z"
		/>
		<linearGradient
			id="ch-topdown36001c"
			x1={1591.7992}
			x2={1477.3572}
			y1={-558.2537}
			y2={-424.2597}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown36001c)"
			d="m1561 216 40 1-2-131.1s-190.9-4.7-226-2c64.4 18.9 161.1 21.1 188 132.1z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown36001d"
			x1={302.9632}
			x2={444.0152}
			y1={-557.1801}
			y2={-423.186}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown36001d)"
			d="m354 216-34 1 2-131.1s184.9-4.7 220-2C477.6 102.8 381 105 354 216z"
			opacity={0.4}
		/>
		<path
			fill="#d9d9d9"
			d="M574 1229s-2-846.1-2-866 1-19.3-3-51c20.7-42.5 240-41 390-41s329.9-.9 383 39c0 17.4-3 921-3 921l-761-1-4-1zm-33 359s67.8 42.1 86 56c58.6 62.4 52.3 68.5 334 62 41.2 0 180.6 1.2 243.1-3 17.1-6.3 41.5-15.9 63.9-34 30.8-25 63-57 116.1-86 122.5-59.8 176.8-166.1 187-239s13.2-680.1 13.9-724-2.8-209.6-8.9-289c-6-79.4-18.8-163.5-62-198s-157-48-157-48l-156-51S736.9 32.2 719 32c-76.9 23.3-125.2 41.4-163 51-65.4 16.6-94.6 18-150 49-72.4 59.4-73 254.9-74 301s2 357 2 357 6 379.7 6 423-2.5 127.7 30 210c44 99.3 171 165 171 165z"
		/>
		<path
			d="M872 34h178l-22 236-137 1-19-237zm469 674 244-18-1 179-243-21V708zm-317 522 20 475H876l20-475h128zM334 686l239 22v141l-238 19-1-182z"
			className="factionColorSecondary"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1174 1706c64.6 0 105.6-47.9 124-64s52.2-38.1 98-65 94-69 94-69h100.3c8.5 0 8.7-4.7 8.7-9 0-115.1 2-1292.3 2-1407 0-3.3-2.3-9-8.2-9-35.9 0-180.2.3-230.8 0-54.1-18.9-141.2-50-163-50H721c-21.8 0-108.9 31.1-163 50-50.6.3-194.9 0-230.8 0-5.9 0-8.2 5.7-8.2 9 0 114.7 2 1291.9 2 1407 0 4.4.2 9 8.7 9H430s48.2 42.1 94 69 79.6 48.9 98 65 59.4 64 124 64h428z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M348 1343h-24m-2-1126h34m1216 1126h24m3-1126h-35" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1483.7 1512.4c47.9-46.2 84.1-103.5 90.8-194 6.7-90.5 10-425.6 10-488.9 0-325.5 20.1-593-54.9-681.9-39.5-47-152.8-57.1-175.7-67"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M435 1513c-48-46.2-84.3-103.5-91-194s-10-425.6-10-489c0-325.5-20.2-593.1 55-682 39.5-47 153-57.1 176-67"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M539 1589c25.7-66.6 35-198.2 35-409s-2-752.3-2-843-144.5-210-166-204"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1374.9 1589.2c-25.7-66.6-34.9-198.2-34.9-409s2-752.3 2-843 148.2-209.9 169.7-204"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1288 1650c0-195.9 43.8-364.4 52-440.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M626 1643c0-195.7-43.8-357-52-433" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m694 1695 39-59h147" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1226 1695-39-59h-147" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M574 1230h762" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M570 308c7.2-114.2-13-227-13-227" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1341.6 308c-7.4-114.2 13.4-227 13.4-227" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M568 311c19.8-20.8 26-41 390-41 114.8 0 371.3-2.5 386 46"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m334 868 240-20m322 382-20 477" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1584 868-241-21m-319 383 20 477" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M891 271 871 33" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1028 271 20-238" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m334 688 240 20" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1584 688-240 20" />
		<g id="Animation">
			<path
				fill="#f2f2f2"
				d="M1257.3 397.2c57 49.2 82.2 116.5 93.1 202 14.8 116.2 1.2 218.3-52.1 335-36.3 79.5-88 152.3-139.1 195-31.4 0-63.3.7-95.1 1-.7 33.4-.3 44.9-.3 44.9l-26.9 56h-7.5l-19.4 644h-99.8L890.8 1231h-7.5l-26.9-56s.4-11.5-.3-44.9c-31.7-.3-63.6-1-95-1-51.1-42.7-102.8-115.5-139-195-53.2-116.7-66.8-218.8-52-335 10.9-85.5 36.3-152.5 93-202 50.8-44.5 115.6-47 297-47 173.7-.1 241.5-1 297.2 47.1z"
			/>
			<linearGradient
				id="ch-topdown36001e"
				x1={959}
				x2={959}
				y1={380}
				y2={-235}
				gradientTransform="matrix(1 0 0 1 0 586)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.651} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#ccc" />
			</linearGradient>
			<path
				fill="url(#ch-topdown36001e)"
				d="m912 962-70 4-21-37s12-146.9 12-177 18.8-427.9-150-369c46.2-28.5 100.1-32 200-32s222 1 222 1 89.7 3.4 130 30c-28.9-3.3-82.5-16.6-117 74s-30 207.1-30 243 3.1 179.1 11 229c-7.4 15-23 38-23 38l-57-2-12-3 1-360h-95l-1 361z"
			/>
			<path
				d="M913 1165h93l-1-565-93 2 1 563zm163-199s17.8 15.7 36 38c32.8 40.2 75 98 75 98s70.9-81.4 76-102c-20-18.5-38-34-38-34l-104-73-45 73zm-276-72 42 71s-108 136.6-108 137-64-70.4-77-102c8.8-8.3 28.2-25.6 46.7-41.9C723.2 945.7 800 894 800 894z"
				className="factionColorPrimary"
			/>
			<path
				fill="#e6e6e6"
				d="M822 930c12.6-158.8 11.8-299.9 6-362-5.7-60.8-34.4-206.6-134-189-50.6 23.4-84.7 74.6-103 125-42.9 118.1-25.3 258.5-18 284 11.7 40.7 16.1 101 84 211 19.7-18.2 89-76 89-76l53-30s14.1 22.6 23 37zm275 0c-12.6-158.7-11.8-299.7-6-361.7 5.7-60.7 34.4-206.5 134.1-188.9 50.6 23.4 84.7 74.5 103.1 124.9 42.9 118 25.3 258.3 18 283.8-11.7 40.7-16.1 100.9-84.1 210.8-19.7-18.2-89.1-75.9-89.1-75.9l-53-30c.1.1-14.1 22.6-23 37z"
			/>
			<path
				fill="#e6e6e6"
				d="m855 1125-20-57 62-102-54-1-34 39-75 99 25 26 97 2-1-6zm165-160h56l36 40s75 95.4 75 97-27 26-27 26l-95 3 2-15 18-50-65-101z"
			/>
			<path
				fill="#999"
				d="m855 1120 2-80s28.2-15.7 57-18c.9-15.8 0-60 0-60h-14s-64.6 104.7-65 105 20 53 20 53zm210.1 0-2-80s-28.1-15.7-56.7-18c-.9-15.8 0-60 0-60h13.9s64.3 104.7 64.7 105-19.9 53-19.9 53z"
			/>
			<linearGradient
				id="ch-topdown36001f"
				x1={1064.0333}
				x2={857}
				y1={507.95}
				y2={507.95}
				gradientTransform="matrix(1 0 0 1 0 586)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#b3b3b3" />
				<stop offset={0.45} stopColor="#f2f2f2" />
				<stop offset={0.552} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#b3b3b3" />
			</linearGradient>
			<path
				fill="url(#ch-topdown36001f)"
				d="M857 1041.4c10.4-5.9 29.8-14.3 55-19.5.6-.1 1.2 142.7 1.9 142.6 30.5-1.6 53.8-1.4 93.1-1.5.6 0-.6-139.1 0-139 32.8 3.8 37.6 9.3 56.9 19.8.3 42.8 0 122.2 0 122.2H857v-124.6z"
				opacity={0.8}
			/>
			<linearGradient
				id="ch-topdown36001g"
				x1={1064.1001}
				x2={855.1001}
				y1={608.5}
				y2={608.5}
				gradientTransform="matrix(1 0 0 1 0 586)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a6a6a6" />
				<stop offset={0.45} stopColor="#e6e6e6" />
				<stop offset={0.552} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#a6a6a6" />
			</linearGradient>
			<path fill="url(#ch-topdown36001g)" d="m879.8 1219.1 159.5.9 24.8-51-209 .9 24.7 49.2z" opacity={0.8} />
			<path fill="#e6e6e6" d="M944 1278s7.6 7 15.4 7c8.2 0 16.6-7 16.6-7l14-51h-64l18 51z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1257.3 397.2c57 49.2 82.2 116.5 93.1 202 14.8 116.2 1.2 218.3-52.1 335-36.3 79.5-88 152.3-139.1 195-31.4 0-63.3.7-95.1 1-.7 33.4-.3 44.9-.3 44.9l-26.9 56h-7.5l-19.4 644h-99.8L890.8 1231h-7.5l-26.9-56s.4-11.5-.3-44.9c-31.7-.3-63.6-1-95-1-51.1-42.7-102.8-115.5-139-195-53.2-116.7-66.8-218.8-52-335 10.9-85.5 36.3-152.5 93-202 50.8-44.5 115.6-47 297-47 173.7-.1 241.5-1 297.2 47.1z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M881 1222h159" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M855.6 1165v-126s20.5-10.9 56.7-16.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1063.9 1127v-88.2s-20.6-10.9-57.1-16.8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m922 1224 20.8 60s19.5 11.1 33.2-2c7.5-19 22-60 22-60m64-56H857"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M913 1164V600h94v563m-3-59h-89" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M675.1 387.2c220-84 146.9 502 146.9 541.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1098.6 929c0-39.8-73-625.7 146.6-541.8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m855 1126-20-58 64-102h-56l-44-73-95 66-49 42m187-34-33 36-74 98"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m703 957 64 99 69 9" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1065 1125.4 20-57.9-64-101.9h55.9l44-72.9L1223 966l41.7 34.5m-186.8-33.9 33 36 73.9 97.9"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1224 965-71.1 97.5-66.9 3" />
		</g>
		<circle id="Animation-Coordinates" cx={960} cy={788} r={1} fill="#0f0" />
	</svg>
);

export default Component;

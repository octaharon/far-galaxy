import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-topdown84003a"
			x1={937.5}
			x2={937.5}
			y1={143}
			y2={98}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#adadad" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-topdown84003a)" d="M908 1777v45h59v-44" />
		<path
			fill="#f2f2f2"
			d="M1526 1354c-11.3-25.2-20-44-20-44l-1-112s-323 77.1-363 144c-15.8 26.5-31.3 69-39.4 110.1-7.9 40.6-8.5 79.8-14.6 100.9-11.4 39.3-54.9 232-150 232-99.6 0-152.5-226.5-158-265s-8-130.8-62-195-351-124-351-124-1 99.1-1 108c-2.3 6.7-18 45-18 45s-149.3-.5-149-1-21-43-21-43v-310l25-68h38.5c3 0 15.1 30 18.3 30h31.8c3.9 0 10.7-30 14.4-30h38.1l24 66v71s43.9-10.8 102.5-31.3c1-.4.5-26.3 1.5-26.7 33-12.9 88.2-36.9 89-37s1.4 27.2 2.3 26.8c68.8-31.2 138-72.5 174.6-122.8 88.7-122.1 127.9-415.4 126-447s-35-44-35-44-167.2-126.5-287-198c.6-20.5 3-81 3-81l16-9s250.8 115.4 333.5 125.3c3-35 11.4-60.6 21.8-75.1 6-2.5 28.3-2 41.6.2 9.7 14.1 17.8 39.2 21.8 76.8 9.4.3 14.2-.3 14.2-.3s289.8-102.5 319-127c9 5.3 17 9 17 9s.3 70.7 0 81c-55.2 37.4-236.1 171-281 193-5.9 2.9-39.5 14-39 51 0 56.1 48 340.2 126 443 34.6 45.6 104.5 89.2 174.7 123.4.9.4 3.5-27.8 4.3-27.4 30.3 14.7 58.6 25.5 86 36 .8.3 1.7 31.2 2.5 31.5 50.3 19.1 90.5 29.9 102.5 28.5.5-11.2-1-67-1-67l25-69h38.8c.8 0 11.6 30 12.3 30h37c1 0 12-30 12.9-30h38.9l25 68s1 300.4 1 310c-3 .2-21 42-21 42s-110.7 2-148.8 2.1z"
		/>
		<path
			d="M813 1653s16.3-14.6 24-67c-15.6-10.7-30-17.6-20-66s22.3-60.5 29-64c1.2-19.4 3-77 3-77s-39.4 28.3-45 38c-2.9-3.7-4-26-4-26s-2.7-162.6 17-209 34-31 34-31l-2-28s-43.8-.1-43 0-1 23-1 23l-29 25s-3.3 141.3-56 156c19.3 27.1 46.3 73.2 53 136 3.4 31.6 8.8 79.2 18 119 9.1 39.3 22 71 22 71zm36-590-42 1-3-23s-32.6-21.2-32-20-2.7-125.9-45-129c22.7-28.6 100.9-150 135-413 5.6-39 .3-75.8-12-83 11.3-7.4 23-10 23-10l63-2s-28.2 15.7-44 111-33.4 356-36 387-7 180-7 180m210 591s-16.3-14.6-24-67c15.6-10.7 30-17.6 20-66s-22.3-60.5-29-64c-1.2-19.4-3-77-3-77s39.4 28.3 45 38c2.9-3.7 7-25.7 7-25.7.7.1-4.7-155.2-20-209.3-8.9-32.8-30-30-30-30l-2-29s43.8-.1 43 0 1 23 1 23l29 25s3.3 141.3 56 156c-19.3 27.1-46.3 73.2-53 136-3.4 31.6-8.8 79.2-18 119-9.1 39.3-22 71-22 71zm-36-590 42 1 3-23s32.6-21.2 32-20 2.7-125.9 45-129c-22.7-28.6-100.9-150-135-413-5.6-39-.3-75.8 12-83-11.3-7.4-23-10-23-10l-63-2s28.2 15.7 44 111 33.4 356 36 387 7 180 7 180"
			className="factionColorPrimary"
		/>
		<path
			fill="#d9d9d9"
			d="m1313 102 18 6-1 83-307 207-19-16 307-200 2-80zm-754-2-16 6 2 88 303 203 23-15-309-200-3-82z"
		/>
		<linearGradient
			id="ch-topdown84003b"
			x1={1148.0945}
			x2={937.0945}
			y1={1209.5242}
			y2={1162.6301}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a1a1a" />
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003b)"
			d="M938 1091h159s-.8-199 52-199c-24.4-20.1-59.9-82.7-81-160-18.9-69.2-37.6-150.5-49-214-15.8-87.9-5.9-118.5 6-122-5.8-4-22.8-11.3-32-11-27.1.8-55 3-55 3v703z"
			opacity={0.702}
		/>
		<path fill="#f2f2f2" d="M208 1096h28v138h-28v-138zm101 0h28v138h-28v-138z" />
		<linearGradient
			id="ch-topdown84003c"
			x1={722.5486}
			x2={852.2656}
			y1={530.6865}
			y2={563.0286}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={0.46} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003c)"
			d="M816 1651c40.5-28.4 37.1-438.2 34-560-29.5.1-38.4-.3-74 0 2.2 81.3-.6 218.8-54 237 70.2 80.2 41.4 213.2 94 323z"
			opacity={0.5}
		/>
		<linearGradient
			id="ch-topdown84003d"
			x1={1152.3901}
			x2={1022.3911}
			y1={533.5345}
			y2={567.8336}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={0.46} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003d)"
			d="M1058.8 1647c-40.6-28.4-37.2-438.1-34.1-559.9 29.6.1 38.5-.3 74.2 0-2.2 81.2.6 218.7 54.1 236.9-70.4 80.3-41.5 213.2-94.2 323z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown84003e"
			x1={798}
			x2={899.0269}
			y1={642.0769}
			y2={642.0769}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003e)"
			d="M798 1406c1.4-26-1.8-256.9 50-256 26-3.3 34.3 46.6 36 58s15.7 85.7 15 155c-25-10.4-48.7-17.6-68-2s-24.5 36.7-33 45z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown84003f"
			x1={974.0032}
			x2={1075.15}
			y1={641.9969}
			y2={641.9969}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003f)"
			d="M1075.2 1406c-1.4-26 1.9-256.8-50.1-255.8-26-3.3-34.3 46.5-36 58-1.7 11.4-15.7 85.7-15 154.9 25-10.4 48.8-17.6 68.1-2 19.2 15.5 24.4 36.6 33 44.9z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown84003g"
			x1={824.5826}
			x2={949.5826}
			y1={218.2531}
			y2={290.4211}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#434242" />
			<stop offset={0.301} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003g)"
			d="M841 1547c-1.4 19.1-11.6 101.7-27 105 8.6 21.4 44.6 133 125 133-1.1-102.3-.7-193 0-259-49.8-34.2-38.3-8.7-98 21z"
			opacity={0.5}
		/>
		<linearGradient
			id="ch-topdown84003h"
			x1={1049.4327}
			x2={931.4327}
			y1={217.121}
			y2={285.248}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#494949" />
			<stop offset={0.367} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003h)"
			d="M1034 1548.1c1.4 19.1 7.6 98.7 23 102-8.6 21.4-46 135.4-118 134.9 1.1-102.2.7-192.7 0-258.6 28.7.3 53.6-.4 95 21.7z"
			opacity={0.5}
		/>
		<linearGradient
			id="ch-topdown84003i"
			x1={937}
			x2={937}
			y1={133.7075}
			y2={308}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown84003i)" d="M910 1781v-168l51-1 3 166s-37.9 16.5-54 3z" opacity={0.2} />
		<radialGradient
			id="ch-topdown84003j"
			cx={116.273}
			cy={1876.238}
			r={1376.17}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.747} stopColor="#000" />
			<stop offset={0.8} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown84003j)"
			d="M368 1069c177.2-42.8 313.6-125.5 360-177 20.7-1.4 34.6 44.2 40 82s9 87.7 9 161H367s.4-31.4 1-66z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown84003k"
			cx={1569.353}
			cy={1481.782}
			r={1329.906}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown84003k)"
			d="M1508 1069c-177.2-42.8-313.6-125.5-360-177-20.7-1.4-34.6 44.2-40 82-5.4 37.8-9 87.7-9 161h410s-.4-31.4-1-66z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-topdown84003l"
			x1={1058.0023}
			x2={814.9685}
			y1={400.5001}
			y2={400.5001}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.313} stopColor="#e6e6e6" />
			<stop offset={0.606} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003l)"
			d="M934 1425c24.3 0 63.3-.8 97 34 27.8 28.7 27 79 27 87s-1.8 32.7-18 39c-20 7.8-41.8 4.4-62.9.3-1-.2-12.1 28.9-13.1 28.7-14-2.8-37.6-2-50-2-10.2 0-12.7-22.3-17.5-28.9-24.7 3.7-51 7.6-63.5.9-19.6-10.5-18-37.5-18-42s1.3-64 29-86 47.4-31 90-31z"
		/>
		<linearGradient
			id="ch-topdown84003m"
			x1={728.7785}
			x2={937.7785}
			y1={1207.1222}
			y2={1158.8712}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.458} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003m)"
			d="M846 396c5.8-.7 13.9-10 28-11 18.7-1.4 55.3-3.1 63 4-35.6 6-56.2 149-63 249s-27.4 284.1-23 458c-46.7-.3-76 1-76 1s6-191.6-47-205c60.1-69.4 107.8-243.5 123-354 12.8-93.1 23.4-125.7-5-142z"
			opacity={0.702}
		/>
		<path
			fill="#fff"
			d="M985 1458.9c.3 0 .5.3.7.7 0 9.8.2 28.6.3 34.3.2 6.3-.4 13.7 0 32-3 .1-4.3-10.4-6-32.8 1-13.8 1.9-19.8 5-34.2z"
		/>
		<path
			fill="#999"
			d="M803 1412s17.4-17.1 39-29c24.3-13.5 53-22 53-22s-35.3-16.7-62 0-30 51-30 51zm267 0s-17.4-16.9-39-28.8c-24.3-13.4-53-21.8-53-21.8s35.3-16.5 62 0c26.7 16.5 30 50.6 30 50.6z"
		/>
		<linearGradient
			id="ch-topdown84003n"
			x1={1508}
			x2={1696.0035}
			y1={777}
			y2={777}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003n)"
			d="M1696 1307.1c.1-34.5-2-307-2-307l-24.6-68h-38.2c-2.4 0-10.8 30-13.3 30h-37.6c-2 0-9.9-30-11.8-30h-35.9l-24.6 67s.8 266.9 1 312c5.7 13.1 19 41.9 19 41.9l148 1s13.3-32.9 20-46.9z"
			opacity={0.15}
		/>
		<linearGradient
			id="ch-topdown84003o"
			x1={180}
			x2={368.0035}
			y1={776.5}
			y2={776.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003o)"
			d="M368 1307.1c.1-34.5-2-307-2-307l-24.6-68h-36.2c-6.2 0-6.7 30-13.5 30h-33c-9.2 0-8.3-30-16.7-30h-37.4L180 999s.8 266.9 1 312c5.6 12 18 40.9 18 40.9l147 3c0 .1 17.6-37 22-47.8z"
			opacity={0.15}
		/>
		<path
			d="M386 1205c-.5-38.7-.2-124.3-2-141 17.8-5.6 145.3-35.6 292-125 14.2 19.7 23.2 137 17 232.1-17.5-.1-237.6-11.1-260-13.1-3.7-.3-2.2 31.6 11 38 74.5 20.4 225.1 36.5 241.4 39.2-4.4 23.7-10.5 42.4-18.6 52.9-13.8-9.5-209.6-72.5-280.8-83.1zm1102 0c.5-38.7.2-124.3 2-141-17.8-5.6-145.3-35.6-292-125-14.2 19.7-23.2 137-17 232.1 17.5-.1 237.7-11.1 260-13.1 3.7-.3 2.2 31.6-11 38-74.5 20.4-225.1 36.5-241.5 39.2 4.4 23.7 10.5 42.4 18.6 52.8 13.9-9.4 209.7-72.4 280.9-83zm-681-142-2 60h261v-60H807z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-topdown84003p"
			cx={278.796}
			cy={-251.27}
			r={1009.577}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.901} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown84003p)"
			d="M367 1200s271.4 49.5 354 127c54.8-14.2 55.9-145.4 57-192-23.2-.2-180.5.3-409 0-.2 34.9-2 65-2 65z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-topdown84003q"
			cx={1602.5389}
			cy={-210.1221}
			r={993.886}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.9} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown84003q)"
			d="M1506 1197s-112.2 24.5-218 63c-50.9 18.5-105.1 41.8-132 67-54.8-14.2-55.9-145.4-57-192 23.2-.2 178.5.2 407 0 .2 34.9 0 62 0 62z"
			opacity={0.302}
		/>
		<path d="m1000 384 309-202 2-82-316 127-1 154 6 3zM562 102v82l308 199 10 1 1-159-319-123z" opacity={0.102} />
		<linearGradient
			id="ch-topdown84003r"
			x1={980}
			x2={894.941}
			y1={1721.6068}
			y2={1721.6068}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84003r)"
			d="m938 250 31-25h11s-7.7-69.6-21-78c-20.8-1-45 2-45 2s-20.3 41.4-19 77c5.5-.8 10 0 10 0l33 24z"
			opacity={0.302}
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M908 1777v45h59v-44" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M910.6 1612.8c-1.3.2-12.7-29.6-14-29.4-25 3.6-51.3 7.1-63.5.6-19.6-10.5-18-37.5-18-42s1.3-64 29-86 47.4-31 90-31h5.8c42.6 0 62.4 9 90.1 31 27.7 22 29 81.5 29 86.1s1.6 31.5-18 42c-12.2 6.5-38.6 3-63.6-.6-1.4-.2-12.7 29.6-14 29.4-13.7-2-16.9-2-26.3-2s-12.9-.1-26.5 1.9z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m911 1613 1 166" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m963 1613 1 166" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m368 1069-1 133" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m179 1029 190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m179 999 190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M386 1064v140" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M385 1155c53.2 2.9 212.1 11.4 310.2 16.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M433 1158c-1 11.4-2.3 28.5 9 34 24.3 11.8 85 19.6 244.7 42.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1505 1069 1 131" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1695 1029-190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1695 999-190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M938 250v136" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m546 192 17-9 311 201s-16.3 8.1-27.8 11.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1328 192-17-9-311 201.2s16.3 8.1 27.8 11.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M880 227v157h120" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M994 384V227" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M562 181v-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1311 181v-80" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1485 1064v140" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1488.5 1155c-53.6 2.9-211.4 11.4-309.2 16.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1441 1160c1 11.4 2.3 26.5-9 32-24.3 11.8-85 19.6-244.4 42.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M727 891c37.3 0 48.1 106.6 50.2 214.3 2.2 109.8-15.9 220.7-58.2 220.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M675.9 937.8c13.6 37.4 18.6 102.3 20 167.7 1.6 75.3-7.7 151.1-29.4 186"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1198.2 937.7c-13.6 37.4-18.7 102.3-20 167.7-1.6 75.3 7.7 151.1 29.5 186"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m777 1170 29-25v-104l-31-21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1096.2 1170.2-29.1-25v-104.1l31.2-21" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1147.1 891c-37.3 0-48.1 106.6-50.2 214.3-2.2 109.8 15.9 220.7 58.2 220.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M815 1651c18.7.4 30.4-119.4 34.7-272.8m.8-228.8c-.1-6.6-.2-20.4-.3-27.6m-.5-57.4c-.4-118.8 11.8-282.1 24.6-418.7C887.1 508.4 900.6 398.1 939 387"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1058.9 1651c-18.8.4-30.5-119.4-34.8-272.8m-.9-228.8c.1-6.6.3-20.4.3-27.6m.6-57.5c.4-118.8-11.8-282.1-24.7-418.8-12.9-137.3-26.5-247.6-65-258.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M898 1361c-18.2-13.4-92.3-16.7-96 55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M846.6 1355.7c3.5 6.5 9.4 17.3 9.4 17.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m822 1363 11 22" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M975 1361.3c18.3-13.4 92.6-16.8 96.4 55.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1026.7 1356.1c-3.5 6.5-9.5 17.3-9.5 17.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1049.5 1367.1c-3.1 6.3-9.2 18.3-9.2 18.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M899 1305h76.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M803 1122h260" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M803 1064h260" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M853 945h170" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M863 785h148" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M874 645h125.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M893 495h89" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M208 1096h28v138h-28v-138zm101 0h28v138h-28v-138zm1226 0h28v138h-28v-138zm101 0h28v138h-28v-138z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M937.1 1784.9c110.8-3 150.3-225.8 155.8-264 5.5-38.5 8-130.8 62-195.1s351.1-124 351.1-124 1 100.1 1 109c18.8.5 188 0 188 0v-310.1l-25-68h-36.9c-6 0-12.5 30-19.1 30h-33c-4.5 0-10.9-30-15.1-30h-37l-24 66v71s-281.3-68.9-370.1-191c-88.7-122.1-128-416.6-126-448.1 1.9-31.6 22-37 22-37l300-204.1v-82l-17-8S996.7 224.1 993.2 225.8c-9-.3-17.3-.4-24.5-.3C943.1 246.4 937 251 937 251s-7.3-5-33-26c-7.2-.1-15.5 0-24.5.3C876 223.6 559 99 559 99l-17 8v82l300 204s20.1 5.4 22 37-37.3 325.9-126 448-370 191-370 191v-71l-24-66h-37c-4.2 0-10.6 30-15.1 30h-33c-6.6 0-13.1-30-19.1-30h-36.9l-25 68v310s169.2.5 188 0c0-8.9 1-109 1-109s297 59.8 351 124 56.5 156.5 62 195c5.5 38.2 44.9 262 155.8 265 .8 0 .7-.1 1.4-.1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M471 1038.8V1011l90-38v28.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1402 1038v-27.7l-90-37.8v28.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m366 1313-20 40H198l-20-40" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1507.5 1313.2 20 39.8h148l20-39.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M895.1 224.6c.1-6 .4-12 1-18 2.8-27.3 10.3-47.2 19.4-59.1 5.7 0 37-.1 42.6 0 9.1 11.9 16.6 31.7 19.4 59 .6 6.4 1 12.8 1 19.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M850 1150c41 0 50.2 145.7 50 214-39 3.9-68.4 19.5-95.9 52.3-1.2-.6-2.6-1.6-3.4-2.4-4.2-81.1 3-153.3 11.7-204.5 8.4-49.4 17.7-59.4 37.6-59.4zm173.2 0c-41.2 0-50.4 145.9-50.2 214.3 39.1 3.9 68.7 19.6 96.3 52.3 1.2-.6 2.6-1.6 3.4-2.4 4.2-81.2-3-153.5-11.8-204.9-8.4-49.3-17.7-59.3-37.7-59.3z"
		/>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-topdown82000a"
			x1={960.25}
			x2={960.25}
			y1={-36.3}
			y2={-89.4}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82000a)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M935.7 1616.6v53.1h49.1v-53.1s-11 7.2-23.1 7.2c-12.5 0-26-7.2-26-7.2z"
		/>
		<linearGradient
			id="ch-topdown82000b"
			x1={759.6075}
			x2={1153.3948}
			y1={-877.3873}
			y2={-884.2617}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8f9bb0" />
			<stop offset={0.399} stopColor="#dae3f2" />
			<stop offset={0.604} stopColor="#dae3f2" />
			<stop offset={1} stopColor="#8f9bb0" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82000b)"
			d="M958.7 239.8c-32.5 6.6-70.1 9.4-180.4-217.9-4.2 4.3-11.4-13-18.8-2.9 1.2 24 5.8 72.4 10.1 114 77.5 227.1 67.4 229 59.2 249.6-30.6 63.6-27.4 214.4-27.4 275.6s9.6 241.8 16 336.2c6.3 94.4 20.1 317.4 37.5 398.2s41.3 245.3 102.4 245.3 92.8-178.3 105.3-243.8c23.7-122.9 31.6-328.7 36.1-392.4 4.5-63.8 14.4-268.5 14.4-347.7s4.2-249.2-27.4-288.6c-18.5-33.5 68.7-197.1 67.8-339.1.7-13.1-17.2-6.6-20.2-10.1-104.4 222.6-128.4 213.9-174.6 223.6z"
		/>
		<path
			d="M908.5 1118.4c34.2-18.7 77.5-12.9 103.5 1-.2-14.5.5-263.5-.6-475.7-.9-175.2-10.8-332.8-10.8-332.8s-31.4-10.2-82.8 1.4c0 13.5-8.4 161.6-8.9 326.6-.5 212.1-.2 462.1-.4 479.5z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown82000c"
			x1={859.4429}
			x2={1057.0717}
			y1={-1435.5177}
			y2={-1438.9685}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#adb8ca" />
			<stop offset={0.497} stopColor="#bac6d9" />
			<stop offset={1} stopColor="#adb8ca" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82000c)"
			d="M1033.2 208.6c-41 39-119.5 33.3-147.2 2.5-6.2 29.9-26.5 118.3-26.5 118.3S908 306 960.2 306s96.9 18.3 96.9 18.3-16.5-85.1-23.9-115.7z"
		/>
		<path fill="#8d96a6" d="M1125.7 128.7 1064 328l-29.4-123.8 102.7-186-11.6 110.5z" />
		<path fill="#8d96a6" d="M793.3 128.7 853 323l28.4-119.8-98.7-184 10.6 109.5z" />
		<linearGradient
			id="ch-topdown82000d"
			x1={1118.2368}
			x2={1079.2805}
			y1={-741.9026}
			y2={-707.5329}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#bbc5d7" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown82000d)" d="m1064.2 924.6 30.2 147.7 5.8-131.3-36-16.4z" />
		<linearGradient
			id="ch-topdown82000e"
			x1={839.5854}
			x2={800.629}
			y1={-707.7039}
			y2={-742.0737}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#bbc5d7" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown82000e)" d="M854.2 925.2 826 1072.3 820.2 941l34-15.8z" />
		<linearGradient
			id="ch-topdown82000f"
			x1={878.0223}
			x2={1041.0726}
			y1={-426.4643}
			y2={-429.3109}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.148} stopColor="#333" />
			<stop offset={0.497} stopColor="#b3b3b3" />
			<stop offset={0.844} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82000f)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M947.3 1450.1V1106s-19.3-1.3-53.2 24.5c-.7 1.7-7.8 39.4-16.2 107.2 13.7 79.2 31.5 163.5 43.4 206.8 2 2.5 3.5 4 6.1 6.1 15.3-.1 19.9-.5 19.9-.5zm24.6-.2v-343.5s19-2.7 53.1 24.4c.7 1.7 7.6 39.2 16 106.9-13.6 79.1-31.3 163.2-43.3 206.3-2 2.5-3.5 4-6.1 5.9-16.3.3-19.7 0-19.7 0z"
		/>
		<linearGradient
			id="ch-topdown82000g"
			x1={-160.7192}
			x2={481.1953}
			y1={-902.5668}
			y2={-560.2471}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#343434" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82000g)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M849.1 922.3c-29.7-133.9-60.9-254.4-40.4-500.7-73 39.1-445.4 420.4-574.2 894.6-45.6 546.8-22.7 590.1-11.5 590.1s340.8-835 626.1-984z"
		/>
		<linearGradient
			id="ch-topdown82000h"
			x1={2101.8472}
			x2={1459.7985}
			y1={-1170.171}
			y2={-592.269}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#343434" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown82000h)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1069.8 922.3c29.7-133.9 60.9-254.4 40.4-500.7 73 39.1 445.5 420.4 574.4 894.6 45.7 546.8 22.7 590.1 11.5 590.1s-341-835-626.3-984z"
		/>
		<path
			d="M240.1 1313.3c122.2-167.7 561.5-422.5 604.8-392.9-16.3-75.7-58.9-245.2-39.4-495.8-87 69.9-226.6 219.2-346.2 405.3-93.6 145.5-176.3 313.5-219.2 483.4z"
			className="factionColorPrimary"
		/>
		<path
			d="M1682.3 1318.3c-122.2-167.7-564.3-427.5-607.8-397.9 16.3-75.7 58.4-243.7 38.9-494.3 44.8 36 102.8 89.1 164.8 161.3 152.4 177.5 334.7 455.8 404.1 730.9z"
			className="factionColorPrimary"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M908.2 1119.9V711c0-198.5 11.5-396.2 11.5-396.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1012.1 1119.9V711.1c0-198.4-11.5-398.9-11.5-398.9" />
		<path
			fill="#c4cedf"
			d="m1136.2 20.5-10.1 108.2 20.2 1.4L1155 19l-18.8 1.5zm-353.5 0 10.1 108.2-20.2 1.4-8.6-111 18.7 1.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1100.1 941.2c-4.1 139.8-9.9 263.3-15.8 303.7-26.1 178.9-51.9 391.2-124.1 391.2-43.3 0-84.5-92.9-125.5-391.2-11.4-82.6-13.8-180.4-18.1-280.1-.2-5.7-.5-15.4-.7-21"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M806.8 446.3c5.3-17 10.9-28.9 13.4-32.9 5.8-22.2 11.7-42.6 17.6-61.2-25.1-78.3-40.4-147.7-66.7-223.9-1.9-29.3-5.6-82.2-8.7-112.5 9.4.6 17.9 3.6 26 4.3 21.8 44.7 50.8 121.1 98.1 186.1 31.2 42.7 100.4 41.4 141.4 1.4 15.4-15.1 64.2-108.2 106.8-191.9 6.1 2 12.8 1.4 20.2 2.9-.3 29.7-4.2 71-9.7 115.6-32 95.4-63.5 202.7-62.5 210.5 8.2 22.9 10.1 36.2 23.1 73.6 1.4 3.2 2.5 5.8 4.9 13.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m853.4 339.4 32.3-133.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1060.6 327.1-28.3-126.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M837.1 355.2c10.1-21.6 66.1-47.6 123.1-49.1 54.5-1.4 108.9 20.6 122.6 37.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M849.1 922.3c-29.7-133.9-60.9-254.4-40.4-500.7-73 39.1-445.4 420.4-574.2 894.6-45.6 546.8-22.7 590.1-11.5 590.1s340.8-835 626.1-984z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1069.8 922.3c29.7-133.9 60.9-254.4 40.4-500.7 73 39.1 445.5 420.4 574.4 894.6 45.7 546.8 22.7 590.1 11.5 590.1s-341-835-626.3-984z"
		/>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#d9d9d9"
			d="m1669.9 1433.8-74.9 64-11 33 7 12-14 9-19 56 31 98-69.9 34-62-83-109.9-37-98.9 23-29-59 80-62 1-7s-22.4-14.3-52-41-193.9-186-193.9-186l135.9-176s212.8 215 216.9 219c4.1 4 18.2 17.1 22 25 10.9 5.2 115.9 42 115.9 42l97.9-23 26.9 59z"
		/>
		<path
			d="m1055 1288 179 171s38.5 13.8 78-10 62.2-59.9 65-88 2.6-52.9-4-65c-17.4-17.7-183-184-183-184l-135 176z"
			className="factionColorSecondary"
		/>
		<path fill="#d4d4d4" d="m1186.4 1235-21.4 21.1 107.6 108.9 21.4-21.1-107.6-108.9z" />
		<linearGradient
			id="ch-topdown35002a"
			x1={1466.2393}
			x2={1528.2454}
			y1={405.7103}
			y2={566.9043}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35002a)"
			d="M1558 1520c-7.1-4.9-87-64-87-64l-58 36s54.7-61.4 19.1-133.2c44.2 31.1 96.3 66 102.9 79 6.7 13 21.1 24.3 23 82.2z"
			opacity={0.502}
		/>
		<path
			fill="#ccc"
			d="m1588 1704.8-30-95-99.9 49 62 81 67.9-35zm8-207 73.9-65-28-58-97.9 23 52 100zm-250.8 122-47-98-79 62 30 60 96-24z"
		/>
		<path fill="#999" d="m1401.1 1501.8 118.9 85-1-12-109.9-79-8 6z" />
		<linearGradient
			id="ch-topdown35002b"
			x1={1081.8484}
			x2={1466.4854}
			y1={382.7688}
			y2={794.8088}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown35002b)"
			d="M1260 1484.6c25.4 21.1 43.3 40.7 84.2 40.2 20.7-3.4 29.5-3.3 58-25 28.5-21.6 47.4-71 40-109-7.4-38-28.8-58.2-42-66-14.1-14.6-208.9-212-208.9-212l-134.9 176c-.1 0 208.9 200.3 203.6 195.8z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown35002c"
			x1={1037.9976}
			x2={1425.3656}
			y1={757.7258}
			y2={473.6958}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.501} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35002c)"
			d="M1260 1484.6c25.4 21.1 43.3 40.7 84.2 40.2 20.7-3.4 29.5-3.3 58-25 28.5-21.6 47.4-71 40-109-7.4-38-28.8-58.2-42-66-14.1-14.6-208.9-212-208.9-212l-134.9 176c-.1 0 208.9 200.3 203.6 195.8z"
			opacity={0.302}
		/>
		<path
			fill="#999"
			d="M1380.6 1471.5c7.8-5.2 16.6-5.7 19.7-1.1 3.1 4.6-.8 12.5-8.6 17.8-7.8 5.2-16.6 5.7-19.7 1.1-3.1-4.6.8-12.6 8.6-17.8z"
		/>
		<radialGradient
			id="ch-topdown35002d"
			cx={1406}
			cy={491}
			r={44}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown35002d)"
			d="M1406 1385c24.3 0 44 19.7 44 44s-19.7 44-44 44-44-19.7-44-44 19.7-44 44-44z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1186.4 1235-21.4 21.1 107.6 108.9 21.4-21.1-107.6-108.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1469.6 1472c0 5.1-.1 11.2-.1 17.9m1.4 21.9c-.2 5.8-.4 11.8-.7 17.8m-7.2 32.1c-.5 5.9-.9 11.6-1.4 17.2m-1.5 18.8c-.7 8-1.2 14.9-1.6 20.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1402.2 1513c.2-6.1 0 .9-.4 12.7m-1.2 35c-.3 7.3-.5 13.9-.7 18.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1414.3 1589.8h21.3m49.7 0h21.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1353 1545.6c5.8 0 13.3-.1 22.1-.1m48.6-.1c7.2 0 14.5-.1 21.9-.1m43-3.9c7.7.4 15.2.7 22.4 1.1m44.4 1.9c7.6.3 14.2.5 19.6.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1531.4 1509.3v14.6m0 36.1v13.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1516.1 1500.1c-6.6-.2-14.9-.5-24-.8m-45.6-1.6c-7.5-.3-14.3-.5-19.8-.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1669.9 1433.8-74.9 64-11 33 7 12-14 9-19 56 31 98-69.9 34-62-83-109.9-37-98.9 23-29-59 80-62 1-7s-22.4-14.3-52-41-193.9-186-193.9-186l135.9-176s212.8 215 216.9 219c4.1 4 18.2 17.1 22 25 10.9 5.2 115.9 42 115.9 42l97.9-23 26.9 59z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1458.1 1656.8 99.9-48m38-111-51-100m-198.8 221-46-96m0-8c12.5 7 62.5 21.7 103.9-17 41.4-38.7 51.8-97.3 25-143m-36-38c30.4 42.8-30.9 195-136.9 163m102.9-30s8.6 10.3 19.9 22.1m13 16.4c3.5 3.8 6.9 7.2 10.1 9.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1373.1 1297.8c10.7 28.6 10.2 86.5-29.4 126.6-27.7 28-64.6 47.6-111.5 33.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1380.6 1471.5c7.8-5.2 16.6-5.7 19.7-1.1 3.1 4.6-.8 12.5-8.6 17.8-7.8 5.2-16.6 5.7-19.7 1.1-3.1-4.6.8-12.6 8.6-17.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1408.1 1496.8 120.9 85 48-30m-56 36.2-2-13m-118.9-73.2 119.9 86-65 42-118.9-86 132.9-89 120.9 88m-193.8 46s31.5-21.6 63.2-42.9m11.3-7.3c29.9-20.2 57.4-38.8 57.4-38.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1432.1 1358.8s82.6 58.2 90 65c37.5 39 35.1 81.7 35 96.2"
		/>
		<path
			fill="#d9d9d9"
			d="m245.1 1433.8 75 64 11 33-7 12 14 9 19 56-31 98 69.9 34 62-83 109.9-37 98.9 23 29-59-80-62-1-7s22.4-14.3 52-41 193.9-186 193.9-186l-135.9-176s-212.8 215-216.9 219c-4.1 4-18.2 17.1-22 25-10.9 5.2-115.9 42-115.9 42l-97.9-23-27 59z"
		/>
		<path
			d="m724 1113 136 174-179 173s-63.9 16.6-110-36-38.3-112.8-27-129c19.2-20.1 180-182 180-182z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown35002e"
			x1={477.5841}
			x2={363.8641}
			y1={402.3212}
			y2={561.5151}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35002e)"
			d="M359 1518c7.1-4.9 87-62 87-62l59 38s-57.4-60.9-22.1-135.2c-44.2 31.1-96.3 66-102.9 79-6.7 13-20.7 22.8-21 80.2z"
			opacity={0.502}
		/>
		<path
			fill="#ccc"
			d="m327 1704.8 30-95 99.9 49-62 81-67.9-35zm-8-207-74-65 28-58 97.9 23-51.9 100zm250.8 122 47-98 79 62-30 60-96-24z"
		/>
		<path fill="#999" d="m513.9 1501.8-118.9 85 1-12 109.9-79 8 6z" />
		<linearGradient
			id="ch-topdown35002f"
			x1={474.2665}
			x2={861.6355}
			y1={739.652}
			y2={400.6559}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown35002f)"
			d="M655 1484.6c-25.4 21.1-43.3 40.7-84.2 40.2-20.7-3.4-29.5-3.3-58-25-28.5-21.6-47.4-71-40-109 7.4-38 28.8-58.2 42-66 14.1-14.6 208.9-212 208.9-212l134.9 176c.1 0-208.9 200.3-203.6 195.8z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown35002g"
			x1={829.796}
			x2={442.428}
			y1={757.801}
			y2={370.4331}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35002g)"
			d="M655 1484.6c-25.4 21.1-43.3 40.7-84.2 40.2-20.7-3.4-29.5-3.3-58-25-28.5-21.6-47.4-71-40-109 7.4-38 28.8-58.2 42-66 14.1-14.6 208.9-212 208.9-212l134.9 176c.1 0-208.9 200.3-203.6 195.8z"
			opacity={0.302}
		/>
		<path fill="#d4d4d4" d="m731.3 1234.8 21.3 21.1-107.5 108.9-21.3-21.1 107.5-108.9z" />
		<path
			fill="#999"
			d="M534.4 1471.5c-7.8-5.2-16.6-5.7-19.7-1.1-3.1 4.6.8 12.5 8.6 17.8 7.8 5.2 16.6 5.7 19.7 1.1 3.1-4.6-.8-12.6-8.6-17.8z"
		/>
		<radialGradient
			id="ch-topdown35002h"
			cx={509}
			cy={491}
			r={44}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown35002h)"
			d="M509 1385c24.3 0 44 19.7 44 44s-19.7 44-44 44-44-19.7-44-44 19.7-44 44-44z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m731.3 1234.8 21.3 21.1-107.5 108.9-21.3-21.1 107.5-108.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m245.1 1433.8 75 64 11 33-7 12 14 9 19 56-31 98 69.9 34 62-83 109.9-37 98.9 23 29-59-80-62-1-7s22.4-14.3 52-41 193.9-186 193.9-186l-135.9-176s-212.8 215-216.9 219c-4.1 4-18.2 17.1-22 25-10.9 5.2-115.9 42-115.9 42l-97.9-23-27 59z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m456.9 1656.8-99.9-48m-38-111 51-100m198.8 221 46-96m0-8c-12.5 7-62.5 21.7-103.9-17-41.4-38.7-51.8-97.3-25-143m36-38c-30.4 42.8 30.9 195 136.9 163m-102.9-30s-8.6 10.3-19.9 22.1m-13 16.4c-3.5 3.8-6.9 7.2-10.1 9.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M541.9 1297.8c-10.7 28.6-10.2 86.5 29.4 126.6 27.7 28 64.6 47.6 111.5 33.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M534.4 1471.5c-7.8-5.2-16.6-5.7-19.7-1.1-3.1 4.6.8 12.5 8.6 17.8 7.8 5.2 16.6 5.7 19.7 1.1 3.1-4.6-.8-12.6-8.6-17.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m506.9 1496.8-120.9 85-48-30m57 37 2-14m117.9-73-119.9 86 65 42 118.9-86-132.9-89-120.9 88m193.8 46s-31.5-21.6-63.2-42.9m-11.3-7.3c-29.9-20.2-57.4-38.8-57.4-38.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M448 1471.5c0 5 0 11.1.1 17.9m-1.5 21.8c.2 5.7.4 11.7.7 17.8m7.2 32c.5 5.8.9 11.6 1.4 17.1m1.5 18.8c.6 8 1.2 14.9 1.6 20.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M515.4 1513.8c.1 3.2.3 8.6.4 11.3m1.2 34.8c.3 7.2.5 13.9.7 18.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M503.3 1589h-21.4m-49.7 0h-21.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M564.6 1544.9c-5.8 0-13.3 0-22.2-.1m-48.5-.1c-7.2 0-14.5-.1-22-.1m-43-3.9c-7.7.4-15.2.7-22.5 1.1m-44.4 1.9c-7.6.3-14.2.6-19.6.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M386 1508.7v14.5m0 36.1v13.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M401.4 1499.5c6.6-.2 14.9-.5 24-.8m45.6-1.5c7.5-.3 14.3-.5 19.8-.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M482.9 1358.8s-82.6 58.2-90 65c-7.4 6.8-34.9 33.8-35 86v9.2"
		/>
		<g>
			<path
				fill="#d9d9d9"
				d="m237.1 311.2 75-64 11-33-7-12 14-9 19-56-31-98 69.9-34 62 83 109.9 37 98.9-23 29 59-80 62-1 7s22.4 14.3 52 41 193.9 186 193.9 186l-135.9 176s-212.8-215-216.9-219c-4.1-4-18.2-17.1-22-25-10.9-5.2-115.9-42-115.9-42l-97.9 23-27-59z"
			/>
			<path
				d="M531.5 445.3c55.8 56.8 184.2 186.9 184.2 186.9l134.9-176S727.3 338 672.5 285.3c-60.3-17.8-166 37.6-141 160z"
				className="factionColorSecondary"
			/>
			<path fill="#d4d4d4" d="M734.3 463.1 713.9 485 602.2 380.5l20.5-21.9 111.6 104.5z" />
			<linearGradient
				id="ch-topdown35002i"
				x1={461.7923}
				x2={352.3403}
				y1={1685.1643}
				y2={1525.9702}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown35002i)"
				d="M350 227c7.1 4.9 88 63 88 63l59-39s-57 58.5-22.1 135.2c-44.2-31.1-96.3-66-102.9-79-6.7-13-14.2-22.6-22-80.2z"
				opacity={0.502}
			/>
			<path
				fill="#ccc"
				d="m319 40.2 30 95 99.9-49-62-81-67.9 35zm-8 207-74 65 28 58 97.9-23-51.9-100zm250.8-122 47 98 79-62-30-60-96 24z"
			/>
			<path fill="#999" d="m505.9 243.2-118.9-85 1 12 109.9 79 8-6z" />
			<linearGradient
				id="ch-topdown35002j"
				x1={430.3869}
				x2={817.7559}
				y1={1301.8463}
				y2={1689.2142}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path
				fill="url(#ch-topdown35002j)"
				d="M647 260.4c-25.4-21.1-43.3-40.7-84.2-40.2-20.7 3.4-29.5 3.4-58 25s-47.4 71-40 109c7.4 38 28.8 58.2 42 66 14.1 14.6 208.9 212 208.9 212l134.9-176c.1 0-208.9-200.3-203.6-195.8z"
				opacity={0.6}
			/>
			<linearGradient
				id="ch-topdown35002k"
				x1={800.3592}
				x2={412.9912}
				y1={1371.7117}
				y2={1703.7266}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.498} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown35002k)"
				d="M647 260.4c-25.4-21.1-43.3-40.7-84.2-40.2-20.7 3.4-29.5 3.4-58 25s-47.4 71-40 109c7.4 38 28.8 58.2 42 66 14.1 14.6 208.9 212 208.9 212l134.9-176c.1 0-208.9-200.3-203.6-195.8z"
				opacity={0.302}
			/>
			<path
				fill="#999"
				d="M526.4 273.5c-7.8 5.2-16.6 5.7-19.7 1.1-3.1-4.6.8-12.5 8.6-17.8 7.8-5.2 16.6-5.7 19.7-1.1 3.1 4.6-.8 12.6-8.6 17.8z"
			/>
			<radialGradient
				id="ch-topdown35002l"
				cx={501}
				cy={1604}
				r={44}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
			</radialGradient>
			<path
				fill="url(#ch-topdown35002l)"
				d="M501 272c24.3 0 44 19.7 44 44s-19.7 44-44 44-44-19.7-44-44 19.7-44 44-44z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M734.3 463.1 713.9 485 602.2 380.5l20.5-21.9 111.6 104.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M440.5 274c0-5.1 0-11.1.1-17.9m-1.5-21.9c.2-5.8.4-11.8.6-17.8m7.3-32.1c.5-5.9.9-11.6 1.4-17.2m1.5-18.8c.6-8 1.2-14.9 1.6-20.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M507.8 233c-.2 6.1 0-.9.5-12.7m1.2-35c.3-7.3.5-13.9.7-18.8"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M495.7 156.2h-21.4m-49.6 0h-21.1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M557 200.4c-5.8 0-13.3.1-22.1.1m-48.6.1c-7.1 0-14.5.1-21.9.1m-43 3.9c-7.7-.4-15.2-.7-22.4-1.1m-44.4-1.9c-7.6-.3-14.2-.6-19.6-.8"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M378.6 236.7v-14.6m0-36.1v-13.7" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M392.9 244.9c6.6.2 14.9.5 24 .8m45.6 1.6c7.5.3 14.3.5 19.8.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m237.1 311.2 75-64 11-33-7-12 14-9 19-56-31-98 69.9-34 62 83 109.9 37 98.9-23 29 59-80 62-1 7s22.4 14.3 52 41 193.9 186 193.9 186l-135.9 176s-212.8-215-216.9-219c-4.1-4-18.2-17.1-22-25-10.9-5.2-115.9-42-115.9-42l-97.9 23-27-59z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m448.9 88.2-99.9 48m-38 111 51 100m198.8-221 46 96m0 8c-12.5-7-62.5-21.7-103.9 17-41.4 38.7-51.8 97.4-25 143m36 38c-30.4-42.8 30.9-195 136.9-163m-102.9 30s-8.6-10.3-19.9-22.1m-13-16.4c-3.5-3.8-6.9-7.2-10.1-9.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M533.9 447.2c-10.7-28.6-10.2-86.5 29.4-126.6 27.7-28 64.6-47.6 111.5-33.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M526.4 273.5c-7.8 5.2-16.6 5.7-19.7 1.1-3.1-4.6.8-12.5 8.6-17.8 7.8-5.2 16.6-5.7 19.7-1.1 3.1 4.6-.8 12.6-8.6 17.8z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m498.9 248.2-120.9-85-48 30m57-37 2 14m117.9 73-119.9-86 65-42 118.9 86-132.9 89-120.9-88m193.8-46s-31.5 21.6-63.2 42.9m-11.3 7.3c-29.9 20.2-57.4 38.8-57.4 38.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M474.9 386.2s-82.6-58.2-90-65c-7.4-6.8-34.9-42.8-35-95"
			/>
		</g>
		<g>
			<path
				fill="#d9d9d9"
				d="m1680.9 311.2-74.9-64-11-33 7-12-14-9-19-56 31-98-69.9-34-62 83-109.9 37-98.9-23-29 59 80 62 1 7s-22.4 14.3-52 41-193.9 186-193.9 186l135.9 176s212.8-215 216.9-219c4.1-4 18.2-17.1 22-25 10.9-5.2 115.9-42 115.9-42l97.9 23 26.9-59z"
			/>
			<path
				d="m1243 286-177 172 135 176 185-189s20.9-67.9-26-120c-20.1-22.3-42.7-35.5-64-40-28.4-6.1-53 1-53 1z"
				className="factionColorSecondary"
			/>
			<path fill="#d4d4d4" d="m1184.5 463.5 20.4 22 111.5-104.9-20.5-22-111.4 104.9z" />
			<linearGradient
				id="ch-topdown35002m"
				x1={1462.9468}
				x2={1559.0488}
				y1={1685.7461}
				y2={1525.5521}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown35002m)"
				d="M1569 226c-7.1 4.9-89 64-89 64l-57-38s56.6 64 20.1 134.2c44.2-31.1 96.3-66 102.9-79 6.7-13 17.6-22.7 23-81.2z"
				opacity={0.502}
			/>
			<path
				fill="#ccc"
				d="m1599 40.2-30 95-99.9-49 62-81 67.9 35zm8 207 73.9 65-28 58-97.9-23 52-100zm-250.8-122-47 98-79-62 30-60 96 24z"
			/>
			<path fill="#999" d="m1412.1 243.2 118.9-85-1 12-109.9 79-8-6z" />
			<linearGradient
				id="ch-topdown35002n"
				x1={1107.8385}
				x2={1495.2075}
				y1={1656.2988}
				y2={1317.3029}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path
				fill="url(#ch-topdown35002n)"
				d="M1271 260.4c25.4-21.1 43.3-40.7 84.2-40.2 20.7 3.4 29.5 3.4 58 25s47.4 71 40 109c-7.4 38-28.8 58.2-42 66-14.1 14.6-208.9 212-208.9 212l-134.9-176c-.1 0 208.9-200.3 203.6-195.8z"
				opacity={0.6}
			/>
			<linearGradient
				id="ch-topdown35002o"
				x1={1137.698}
				x2={1508.702}
				y1={1366.681}
				y2={1778.7211}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.501} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown35002o)"
				d="M1271 260.4c25.4-21.1 43.3-40.7 84.2-40.2 20.7 3.4 29.5 3.4 58 25s47.4 71 40 109c-7.4 38-28.8 58.2-42 66-14.1 14.6-208.9 212-208.9 212l-134.9-176c-.1 0 208.9-200.3 203.6-195.8z"
				opacity={0.302}
			/>
			<path
				fill="#999"
				d="M1391.6 273.5c7.8 5.2 16.6 5.7 19.7 1.1 3.1-4.6-.8-12.5-8.6-17.8-7.8-5.2-16.6-5.7-19.7-1.1-3.1 4.6.8 12.6 8.6 17.8z"
			/>
			<radialGradient
				id="ch-topdown35002p"
				cx={1417}
				cy={1604}
				r={44}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
			</radialGradient>
			<path
				fill="url(#ch-topdown35002p)"
				d="M1417 272c24.3 0 44 19.7 44 44s-19.7 44-44 44-44-19.7-44-44 19.7-44 44-44z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1184.5 463.5 20.4 22 111.5-104.9-20.5-22-111.4 104.9z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1477.6 274c0-5.1-.1-11.1-.1-17.9m1.4-21.9c-.2-5.8-.4-11.8-.7-17.8m-7.2-32.1c-.5-5.9-.9-11.6-1.4-17.2m-1.5-18.8c-.7-8-1.2-14.9-1.6-20.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1410.2 233c.2 6.1 0-.9-.4-12.7m-1.3-35c-.3-7.3-.5-13.9-.7-18.8"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1422.3 156.2h21.3m49.7 0h21.1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1361 200.4c5.8 0 13.3.1 22.1.1m48.6.1c7.2 0 14.5.1 21.9.1m43 3.9c7.7-.4 15.2-.7 22.4-1.1m44.4-1.9c7.6-.3 14.2-.6 19.6-.8"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1539.4 236.7v-14.6m0-36.1v-13.7" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1524.1 245.9c-6.6.2-14.9.5-24 .8m-45.6 1.6c-7.5.3-14.3.5-19.8.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1680.9 311.2-74.9-64-11-33 7-12-14-9-19-56 31-98-69.9-34-62 83-109.9 37-98.9-23-29 59 80 62 1 7s-22.4 14.3-52 41-193.9 186-193.9 186l135.9 176s212.8-215 216.9-219c4.1-4 18.2-17.1 22-25 10.9-5.2 115.9-42 115.9-42l97.9 23 26.9-59z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1469.1 88.2 99.9 48m38 111-51 100m-198.8-221-46 96m0 8c12.5-7 62.5-21.7 103.9 17 41.4 38.7 51.8 97.4 25 143m-36 38c30.4-42.8-30.9-195-136.9-163m102.9 30s8.6-10.3 19.9-22.1m13-16.4c3.5-3.8 6.9-7.2 10.1-9.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1384.1 447.2c10.7-28.6 10.2-86.5-29.4-126.6-27.7-28-64.6-47.6-111.5-33.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1391.6 273.5c7.8 5.2 16.6 5.7 19.7 1.1 3.1-4.6-.8-12.5-8.6-17.8-7.8-5.2-16.6-5.7-19.7-1.1-3.1 4.6.8 12.6 8.6 17.8z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1419.1 248.2 120.9-85 48 30m-57-37-2 14m-117.9 73 119.9-86-65-42-118.9 86 132.9 89 120.9-88m-193.8-46s31.5 21.6 63.2 42.9c.3.7.8 11.1.8 11.1m10.5-3.8c29.9 20.2 57.4 38.8 57.4 38.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1443.1 386.2s82.6-58.2 90-65c7.4-6.8 34.9-42 35-94.2"
			/>
		</g>
		<path
			fill="#f2f2f2"
			d="M961 222c239.2 0 297 40 297 40l79-80 113 82 94 256-1 389-171 415-117 48-25-25-147 209H839l-146-207-26 23-116-47-172-416-1-389 94-256 113-82 79 80s57.8-40 297-40"
		/>
		<path
			fill="#d9d9d9"
			d="M675 276c15.6-9 26.6-41 285-41 47.2 0 233.8-1 287 39 4.9-4.8 10-12 10-12s-55.7-40-297-40c-72.5 0-244.8 6.5-296 41 2.8 3.8 7.2 9.1 11 13z"
		/>
		<path
			fill="#737373"
			d="M626 1037c-20-18.8-49.3-94.6-53-120-3.9-26.9-10-187.5-10-201s-2.4-206.1 6-272 16.4-76.3 24-82c-11 7.1-18.1 14.6-19 17s-20 149-20 149l1 387 56 138s10.5-11.3 15-16zm670-2c4.4 4.5 10.3 10.7 13 13 3.8-7.4 56-133 56-133l2-387-20-149-12-10s8.7 18.5 14 66c10.7 51.6 8 258.6 8 281 0 10-2.3 57.2-4 110-3.8 67.2 1.8 128.4-57 209z"
		/>
		<path fill="#e7e7e7" d="m1082 1556 126-607H711l129 607h242z" />
		<path
			fill="#d6d6d6"
			d="m1209 948 99 100-110 263 30 37-146 208 127-608zm-500 2 130 606-147-206 26-29-108-268 99-103z"
		/>
		<linearGradient
			id="ch-topdown35002q"
			x1={622.7792}
			x2={767.7792}
			y1={713.1188}
			y2={771.7028}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown35002q)" d="m755 1330-119-305-26 28 109 268 36 9z" />
		<linearGradient
			id="ch-topdown35002r"
			x1={1293.5906}
			x2={1150.5906}
			y1={721.8893}
			y2={780.0714}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown35002r)" d="m1166 1320 117-300 26 28-109 263-34 9z" />
		<linearGradient
			id="ch-topdown35002s"
			x1={701.747}
			x2={758.452}
			y1={599.3336}
			y2={536.3336}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#737373" />
			<stop offset={0.5} stopColor="#5e5e5e" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown35002s)" d="m720 1321 31 8-34 55-25-34 28-29z" />
		<linearGradient
			id="ch-topdown35002t"
			x1={1217.085}
			x2={1154.085}
			y1={595.0679}
			y2={544.6679}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.5} stopColor="#5e5e5e" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown35002t)" d="m1198 1312-31 7 40 60 23-31-32-36z" />
		<path
			fill="#dfdfdf"
			d="M668 1371 465 912V523l55-204 109-91-45-45-114 84-92 252 2 392 169 413 119 47zm585 0 203-459V523l-55-204-109-91 45-45 114 84 92 252-2 392-169 413-119 47z"
		/>
		<path
			fill="#b9b9b9"
			d="m1115 1398 27 26 10-38-30-26H798l-30 26 10 38 27-26h310zm25-120 27 26 10-38-30-26H773l-30 26 10 38 27-26h360zm27.9-112 27 26 10-38-30-26H745.5l-30 26 10 38 27-26h415.4z"
		/>
		<path
			d="m555 915 19 48-176-6-19-47 176 5zM378 520c.8-.7 176 8 176 8l-1 47-175-8s-.8-46.3 0-47zm986 395-19 48 175.7-6 19-47-175.7 5zm176.6-395c-.8-.7-175.7 8-175.7 8l1 47 174.7-8s.9-46.3 0-47z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-topdown35002u"
			cx={958.543}
			cy={1356.031}
			r={613.237}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.203} stopColor="#000" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown35002u)"
			d="M1296 1035c10.2-13 43.4-64.7 50-110s11-169 11-211c0-42.1 8.4-299.7-26-351-45.3-37.2-92-79-92-79s4.4-7 7-10c-49.7-42.4-271.1-38.9-286-39-263.8.3-269.9 33.4-286 41 3.2 3.7 6 7.9 6 7.9l-107 95-20 150 1 386s25.3 62 55.9 137.7C655.3 1008 711 950 711 950l498-1s60.2 59.3 87 86z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M960 222c239.2 0 297 40 297 40l79-80 113 82 94 256-1 389-171 415-117 48-25-26-147 210H838l-146-207-25 23-118-48-171-415-1-389 94-256 113-82 79 80s57.8-40 297-40"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1248 274c-32.1-20.3-80.7-39-288-39s-243.8 13.8-285 39" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1081 1556 127-608m102 101-101-101H711l-100 104m229 504L711 950"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1115 1396 27 26 10-38-30-26H798l-30 26 10 38 27-26h310zm25-120 27 26 10-38-30-26H773l-30 26 10 38 27-26h360zm27.9-110 27 26 10-38-30-26H745.5l-30 26 10 38 27-26h415.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m862 1557 18-39h157l20 38" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M959.5 314c137.8 0 249.5 111.7 249.5 249.5S1097.3 813 959.5 813 710 701.3 710 563.5 821.7 314 959.5 314z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1260 260-20 24 107 95 20 150-1 386s-167.2 395.8-166 397 29 35 29 35"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1253 1370 203-459V524l-56-206-108-89" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1367 915 176-5m1-390-175 8m-22-149 104-114" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1296 1035c10.2-13 43.4-64.7 50-110s11-169 11-211c0-42.1 8.4-299.7-26-351M660 259.9l20 24-107 95-20 150 1 386s165.1 405.7 164 407c-1.2 1.3-26 28.1-26 28.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m667 1369.9-202.9-459v-387l56-206 108-89" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m553.1 914.9-175.9-5m-1.1-390 174.9 8m22.1-149-104-114" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M624 1034.9c-10.2-13-43.4-64.7-50-110-6.5-45.3-11-169-11-211 0-42.1-8.4-299.7 26-351"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M563.1 715.9H761m596 .1h-198" />
		<g id="Animation">
			<path
				fill="#f2f2f2"
				d="M1321.5 564.5c0-199.4-161.6-361-361-361s-361 161.6-361 361c0 117.4 55.7 224.6 142.5 290.5-.2 44.1-.2 52.3-.2 133.9 11.3 27.9 24.2 60.1 24.2 60.1h12l18 667h114l14-667 10-1 24-56 27 56 12 1 17 667h112l17-668 13-1 21-60 1-131s53.4-30.7 91-90c34-53.7 52.5-132.5 52.5-201.5z"
			/>
			<path
				fill="#d9d9d9"
				d="M790 759s-.1 57.9 0 58 19.8 25.3 50 25 241 1 241 1 46.9-17.5 48-28c.4-13.1 0-54 0-54s-28 19.9-48 26-70.1 21-121 21c-14.7 0-77.5-5.4-120-21s-50-28-50-28z"
			/>
			<path
				d="M841 843v145l-99 1V824s.3-26.2 48-25c.1 9.8 0 18 0 18s12 11.4 25 18c12.5 6.4 26 8 26 8zm238 0v145l99 1V824s-.3-26.2-48-25c-.1 9.8 0 18 0 18s-13.1 9.4-26 16c-12.6 6.4-25 10-25 10zM761 327.8c.3 81.5-.3 340.1-.1 441.7-6.6-.1-16.9 1.4-22.9 4.5-7.5 3.9-16 13.8-16.1 16.5C688 754.5 632 677.7 632 564c0-121.3 60.5-199.1 98.5-236 10 6.6 21.8 5.8 30.5-.2zm399.1.2c-.3 81.5.3 340.3.1 442 6.6-.1 16.8 1.4 22.9 4.5 7.5 4 16 13.8 16.1 16.5 33.8-36 89.8-112.9 89.8-226.6 0-121.4-60.4-199.2-98.4-236.1-10.1 6.5-21.8 5.7-30.5-.3zM819 1049l1 17 16 38s22 5.9 31 0c6.3-13.2 18-38 18-38v-18l-66 1zm217-1s-.6 16.4 0 17 16.4 39.5 17 40 26.9 5.3 33-2c4.9-12 16-37 16-37v-18h-66z"
				className="factionColorPrimary"
			/>
			<linearGradient
				id="ch-topdown35002v"
				x1={742.0067}
				x2={841.0067}
				y1={1024.7507}
				y2={1026.4788}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.005} stopColor="#000" />
				<stop offset={1} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown35002v)"
				d="M790 799s.1 7.7.3 17.4C807.9 826.1 841 843 841 843l-1 146-98-1 1-165s3.1-24.5 47-24z"
				opacity={0.2}
			/>
			<linearGradient
				id="ch-topdown35002w"
				x1={1079.9985}
				x2={1175.9985}
				y1={1025.2429}
				y2={1026.9189}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" stopOpacity={0} />
				<stop offset={0.995} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown35002w)"
				d="M1128 799s1.2 6.4 1 17c-27.4 15.8-49 27-49 27v146l96-1-1-165s-3.1-24.5-47-24z"
				opacity={0.2}
			/>
			<linearGradient
				id="ch-topdown35002x"
				x1={741.9963}
				x2={958.9128}
				y1={900.3154}
				y2={904.1019}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#ccc" />
				<stop offset={0.5} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#ccc" />
			</linearGradient>
			<path fill="url(#ch-topdown35002x)" d="m742 988 217 1-24 59H767l-25-60z" />
			<linearGradient
				id="ch-topdown35002y"
				x1={959.9963}
				x2={1176.9128}
				y1={900.3154}
				y2={904.1019}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#ccc" />
				<stop offset={0.5} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#ccc" />
			</linearGradient>
			<path fill="url(#ch-topdown35002y)" d="m960 988 217 1-24 59H985l-25-60z" />
			<radialGradient
				id="ch-topdown35002z"
				cx={960.5}
				cy={1390.1851}
				r={326.3161}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.3} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</radialGradient>
			<path
				fill="url(#ch-topdown35002z)"
				d="M960 762c110.3 0 169-72 169-72s.4 90.5 1 109c50.9 1.1 49.3 24.5 50 54 57.9-20.2 141.5-142.6 141.5-288.5 0-199.4-161.6-361-361-361s-361 161.6-361 361c0 117.4 56 225.7 142.8 291.6.5-7.6-.3-21.3-.3-34.1 10-25.8 49-23 49-23V690s27.2 37.5 66 54c45.2 19.3 92.7 18 103 18z"
				opacity={0.149}
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1178 986-23 61h-12l-17 669s-111.6.2-112 0-17-668-17-668h-12s-22.8-50.6-26-58c-.4.1-1 0-1 0l-23 58h-11l-14 668H796l-18-668h-12s-13.6-34.7-24-61c.1-89.2 0-98.7 0-131-100.2-76.4-142.5-182.3-142.5-291.5 0-199.4 161.6-361 361-361s361 161.6 361 361c0 128-43.9 226.5-143.3 290.6-.2 17.2-.2 130.9-.2 130.9z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m747 814-6-6s-8.8-8.2-16.3-15.8c-57.9-59.3-93.6-140.4-93.6-229.8 0-95.2 40.4-180.9 105-241 7.7-7.1 41.3-33 50.5-38.6 50.5-31.4 110-49.6 173.9-49.6 57.6 0 111.8 14.8 158.9 40.8 9.6 5.3 42.9 28.1 50 33.9 73.5 60.4 120.4 152 120.4 254.5 0 100.6-42 190.7-111.6 251-.2.4-2.2 2.6-2.2 2.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M791 799c-46.4 0-49 21.8-49 27v31m98-70v201m240-1V786m-289-25s19.6 16.3 49.4 26c31.9 10.3 75.6 21 119.4 21 44.1 0 88.3-11 120.5-21.4 29.3-9.5 48.7-25.6 48.7-25.6m-1 38.5c46.8 0 49.5 22.1 49.5 27.5s.5 27 .5 27"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1178 988H743m35 60h146m-40 18h-63m-2-16 1 16 16 38s16.5 6.7 31 0c5.2-9.6 18-38 18-38v-17m217 0v17s-9.7 22.2-17 38c-14.3 6.6-32 1-32 1l-17-39v-16m2 16h63m-106-18 152-1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1081 843H839s-30-2.3-48.5-25.9V688.5s33.8 74 170 74c115.8 0 167.5-75.5 167.5-75.5l1 130s-10.3 9.6-21.4 15.6c-12.8 6.9-26.6 10.4-26.6 10.4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M790.5 690.3v-409m-29 522.3V301.8m367 388.7V279.2m30 525.8V301"
			/>
			<circle id="Animation-Coordinates" cx={960} cy={564} r={1} fill="#0f0" />
		</g>
		<g>
			<radialGradient
				id="ch-topdown35002A"
				cx={235}
				cy={716}
				r={44.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#bfbfbf" />
			</radialGradient>
			<path
				fill="url(#ch-topdown35002A)"
				d="M235 1159.5c24.6 0 44.5 19.9 44.5 44.5s-19.9 44.5-44.5 44.5-44.5-19.9-44.5-44.5 19.9-44.5 44.5-44.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M215 1179.9c3.9 0 7.1 3.2 7.1 7.1s-3.2 7.1-7.1 7.1-7.1-3.2-7.1-7.1 3.2-7.1 7.1-7.1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M235 1159.5c24.6 0 44.5 19.9 44.5 44.5s-19.9 44.5-44.5 44.5-44.5-19.9-44.5-44.5 19.9-44.5 44.5-44.5z"
			/>
			<path fill="red" d="M210.5 1238.5s25.3.2 50 0c-3.5 3.2-11.5 8.4-24.7 8.5-8.1.1-15.5-.5-25.3-8.5z" />
		</g>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<g id="Animation-Foot-Right">
			<path
				fill="#ccc"
				d="M1177.7 810.7c-.3 21.7 2.5 96.2 51 90s148.7-8.4 153-113c4.6-111.9-38.8-257.5-40-282s-38.1-52.9-120-39c-81.9 13.8-43.8 322.3-44 344z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1342.7 868.7-25-27-73 15-15 42" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1177.7 810.7c-.3 21.7 1 83.3 51 90 48.4 6.5 148.7-8.4 153-113 4.6-111.9-36-257.4-40-282-4-24.2-38.1-52.9-120-39-81.9 13.8-43.8 322.3-44 344z"
			/>
		</g>
		<g id="Animation-Foot-Left">
			<path
				fill="#ccc"
				d="M743.3 810.7c.3 21.7-2.5 96.2-51 90s-148.7-8.4-153-113c-4.6-111.9 38.8-257.5 40-282s38.1-52.9 120-39c81.9 13.8 43.8 322.3 44 344z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m578.4 868.7 25-27 73 15 15 42" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M743.3 810.7c.3 21.7-1 83.3-51 90-48.4 6.5-148.7-8.4-153-113-4.6-111.9 36-257.4 40-282 4-24.2 38.1-52.9 120-39 81.9 13.8 43.8 322.3 44 344z"
			/>
		</g>
		<path
			fill="#f2f2f2"
			d="M1288 1136h29v-43h-29v-37h29v-43h-29v-35h29v-43h-34s33-51.3 24.1-69c162.2-199 143.4-236.6 169.9-277 1.9-1.5 31.6-33.7 32-122s-70.1-134.2-105-142-68-25.5-103-45c-12.2-51.9-33.7-138.4-56-190-12.5-28.8-29.9-45.1-82-53.9-12.2-6.6-35.4-21.7-58-23.1-18.5-1.1-50.5 7.1-62.9 14.7-23.9-.5-50.8-.7-81.1-.7-31.7 0-60.1-.4-85.8-.5-15.9-7.2-39-14.9-58.2-13.5-20.5 1.4-50.9 16.6-60.9 23.5-36.1 9.9-63 29.2-85.1 66.5-32.1 88.1-49 177-49 177l-82 40s-131 19.6-131 145c0 39 17.7 101.5 21 108 48.7 59.8 91 43.5 131 68 15.5 19 97.9 138.8 160 163s119.5 19 141 19 87.1-10.4 98-24c10.5 13.5 60.2 23 96 23 29.4 0 70.7 4.5 108.3-7.5-5.4 8.8-13.6 24.5-27.3 40.5-34.6 3.3-51 52-51 52h-5l1 28h-21v43h22v35h-23v43h23v37h-22v43h26.1c-.8 42.3 5.9 765 5.9 765l185-1s9.4-732.7 10-764z"
		/>
		<path
			fill="#d9d9d9"
			d="M1302 281c-4.3-17.3-29.2-131.6-68-208-14.9-29.4-41.7-32.3-69-34 17.4 20.9 31 42 31 75s-31.5 100-98 100-99.8-57.8-102-101 32.4-76.8 47-84c-32.7.3-164-1-164-1s44 32.2 44 88-50.9 96-102 96-97-36.6-97-102c0-43.8 34-71 34-71s-59.8 7.2-88 67-47 175-47 175l13-7s19.2-54.7 42-72 38.3-21.7 50-5-7 41-7 41 63.3-9 101-9 110.7 21 139 21 86.4-20 147-20 147.4 29.4 194 51z"
		/>
		<linearGradient
			id="ch-topdown10001a"
			x1={959.5778}
			x2={959.5778}
			y1={-278.1951}
			y2={-86.1951}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001a)"
			d="M794 491s-1.9-77.3 0-89-6-93.2 166-102c130.1 0 153.4 74.6 159 92s7 100 7 100l-332-1z"
			opacity={0.149}
		/>
		<linearGradient
			id="ch-topdown10001b"
			x1={1198.7639}
			x2={1198.7639}
			y1={334.54}
			y2={253.7661}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.799} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001b)"
			d="M1288 924c7-5 27.2-38.5 20-60s-122.6-29.3-183-5c-23.8 6.5-35.8 42.2-37 48 16.6 1.5 200 1 200 1s-7 21 0 16z"
			opacity={0.302}
		/>
		<path
			fill="#d9d9d9"
			d="M1454 351c-3.5-2.7-17.7-16.3-41-21-72-14.6-185.8-41.1-202 54 24.3-.2 256.7-22.3 243-33zm-246 170c24.2 1.2 202.8 12.3 283 49-8 11.5-21.1 35.2-63 41-48.5-1.5-103.7 16-122 16s-89.4-11.3-98-106zM708 384c-3.4-14.4-16.7-44.4-27-47 .5 18.7 11.1 45 17 49 7 .3 13.4 12.4 10-2zm-81-69c-22.5-.2-133.7 6-164 36 15.4 5 124.3 29 166 29-1-15.5-4.9-38.9-2-65zm85 206c-21.7 0-207.5 12.8-281 50 5 11.3 30.1 37 56 40 11.4 1.3 32.8-1.2 55 2 28.1 4.1 57.9 14 73 14 27.1 0 88.2-21.1 97-106z"
		/>
		<path
			fill="#c2c2c2"
			d="M1472 593c-19.3 12.3-48.2 20.8-73 20 25.5 10.6 43.2 3.4 50 41 7.9-20.1 19-42.4 23-61z"
		/>
		<path
			fill="#c2c2c2"
			d="M1301 279c-8.6-37-39.5-201.2-93-230-11.5-6.2-28.8-10.7-49.1-13.9-6-6-33.3-21.9-52.9-23.1-19.9-1.3-53.4 8.3-62.2 15.2-43-.5-78.8.8-85.8.8-6.9 0-37.1-1-81-1-6.2-3.9-38-16-58-15-23.2 1.2-50.7 16.5-65 25-23.4 6.5-41.7 17.5-55 31-47.9 37.6-73.6 202.9-76 213 10.2-6.8 14-7 14-7s25.9-88 73-88c39.2 17.1 12.7 46.2 9 52 23.4-2.8 86.3-8 112-8s98 23.2 128 20 115.3-21 155-21 141.5 30.2 187 50z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown10001c"
			x1={723.3828}
			x2={769.3828}
			y1={-355.2534}
			y2={-391.1924}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001c)"
			d="M726 193c-4.1-4.8 40 31.6 41 40-17 2.4-46 7-46 7s19.2-30.2 5-47z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown10001d"
			x1={782.3444}
			x2={782.3444}
			y1={-93.8222}
			y2={-195}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path fill="url(#ch-topdown10001d)" d="m796 391-27 1s-.7 99.6 0 100 24 0 24 0v-80l3-21z" opacity={0.302} />
		<linearGradient
			id="ch-topdown10001e"
			x1={971}
			x2={971}
			y1={-204}
			y2={-357}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001e)"
			d="M551 318c23.4-14.3 77.6-41 86-43-4.1 13.1-10.2 33.2-10 42-18.7.2-45.8-1.9-76 1zm129 15c7.9 6.7 16.8 17.5 27 39 18.2 4.7 40 9 40 9s40-125 213-125c63.3 0 172.8 24.9 213 126 15.1-2.9 40-9 40-9s19.2-52.5 68-57 94.9 5.7 110 8c-42.3-16.9-175.2-95-294-95-75.4 0-85.3 21-136 21s-107.5-40.2-242-9c-17.2 17.4-39.8 57.7-39 92z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown10001f"
			x1={1174}
			x2={1174}
			y1={-25}
			y2={-211}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001f)"
			d="M1214 375c-14.8 2.3-85 16-85 16s29.3 56.1 13 155c27.6 5.4 77 15 77 15s-33.6-66.2-5-186z"
		/>
		<linearGradient
			id="ch-topdown10001g"
			x1={740.5}
			x2={740.5}
			y1={-25}
			y2={-211}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001g)"
			d="M768 410.8c-.1-15.7 0-24.8 0-24.8s-46.2-8.7-61-11c2.1 8.9 3.9 17.6 5.4 25.9 12.8 8.5 55.6 19.4 55.6 9.9zm-49.5 61.3C717.7 530 702 561 702 561s49.4-9.6 77-15c-3.2-19.5-4.6-37.3-4.9-53.4-1.7-.7-3.3-1.6-5.1-2.6-.1-4.8-.2-9.6-.3-14.2-13.3-.4-39.3-1.7-50.2-3.7z"
		/>
		<path
			fill="#e6e6e6"
			d="M1167 552c9.6 2.5 25.1 4.3 32 6-1.6 25.1-13.3 229-239 229-41.3 0-225.7-6.7-238-228 6.9-1.7 33-7 33-7s11.1 126.6 91 175c100.3 44.3 231.1 7.3 247-16 14.6-16.2 55.2-45.2 74-159zm8-171c-11.1-24-62.3-125-219-125S757.4 358.8 746 382c15.8 4.3 30 6 30 6s22-101 186-101c141.8 0 169.7 93.7 173 101 11.7-2.3 24.1-.3 40-7z"
		/>
		<path
			fill="#f2f2f2"
			d="M1084 1013 835 823s-59.3 6-120-23c-29.2-16.6-62.8-46.8-91-80-24.6-29-44.3-60.9-64-79-32.8-15.9-63.6-13.9-104-42 21.2 34.5 189 286 189 286s97.9 84 205 164c114.2 85.3 238 167 238 167l-2-79h-25l-1-43 23-1v-38l-25 1 1-44 25 1z"
		/>
		<linearGradient
			id="ch-topdown10001h"
			x1={1088}
			x2={1012.8852}
			y1={536.5}
			y2={536.5}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#7a7a7a" />
			<stop offset={1} stopColor="#d8d8d8" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001h)"
			d="M1059 1031c-10.3 5.1-44 41.3-46 82s22.6 69.4 37 78 38 23 38 23v-77l-28-1v-43h21v-38l-22-1s-1.2-11.2 0-23z"
		/>
		<linearGradient
			id="ch-topdown10001i"
			x1={740.0815}
			x2={1033.8456}
			y1={578.8094}
			y2={202.8095}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8a8a8a" />
			<stop offset={0.4585} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5147} stopColor="#000" stopOpacity={0} />
			<stop offset={0.852} stopColor="#191919" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001i)"
			d="m837 823 244 188-22 4-2 16s-85.8 69.9-20 153c-54.3-38.3-376.1-276.3-391-298 21.3-4.1 74.6-7.8 86-78 41 18.3 105 15 105 15z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown10001j"
			x1={1145.886}
			x2={1452.886}
			y1={50.6275}
			y2={275.2356}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.15} stopColor="#000" />
			<stop offset={0.5334} stopColor="#999" stopOpacity={0.102} />
			<stop offset={0.5339} stopColor="#999" stopOpacity={0.102} />
			<stop offset={0.559} stopColor="#999" stopOpacity={0.102} />
			<stop offset={0.7406} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001j)"
			d="M1308 864s-51.7-36.7-166-10c2.8-12.5 12.9-21 26-39 113.4-30.8 166.8-165.9 222-201 49.3 7.3 52.6 17.8 59 40-55.7 111.7-141 210-141 210z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-topdown10001k"
			x1={455.8083}
			x2={732.4554}
			y1={228.4923}
			y2={41.8912}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#ccc" />
			<stop offset={1} stopColor="#202020" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001k)"
			d="M456 600c12.8 12.2 91 26 102 40s104.2 147.7 174 169c-15.5 41.5-23.8 69.8-91 72-36-52.9-197.8-293.2-185-281z"
		/>
		<linearGradient
			id="ch-topdown10001l"
			x1={526.7299}
			x2={608.7119}
			y1={348.0281}
			y2={66.5781}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.896} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001l)"
			d="M456 600c12.8 12.2 91 26 102 40s104.2 147.7 174 169c-15.5 41.5-23.8 69.8-91 72-36-52.9-197.8-293.2-185-281z"
			opacity={0.4}
		/>
		<path
			fill="#ff2a00"
			d="M1110 692c-13-1.3-86.6 21-94 25 6.9 8.4 17.7 20.2 24 21 11.1-1.5 56.6-21.9 70-46zm-208 23c-9-5.5-89.9-27-97-25 10.5 12.3 41.4 40.9 74 46 11.7-6 22.1-12.7 23-21z"
		/>
		<linearGradient
			id="ch-topdown10001m"
			x1={932}
			x2={932}
			y1={238}
			y2={-26}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.7} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001m)"
			d="M722 560c6.3 185.8 145.9 226 239 226s223.8-41.1 238-226c11.8 1.7 18 5 18 5s28.1 62 90 62 52.4-15 92-15c-11.7 6.1-41.7 25.5-63 56s-85 155-246 155c-120.7 0-129-24-129-24s-25.4 25-151 25-198.7-116.7-212-136-38.9-52.4-55-56-66.1-17.6-78-29c24.9 8.8 50.8 5.8 70 9s58.8 18.6 83 15 66.8-17.5 85-64c7.1-1.6 10.6-2.1 19-3z"
			opacity={0.149}
		/>
		<linearGradient
			id="ch-topdown10001n"
			x1={1509.1537}
			x2={1202.4163}
			y1={-126}
			y2={-126}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.6} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001n)"
			d="M1212 382c24.5.9 176.7-5.2 242-33 34.4 33.9 83.1 89 35 222-31.5-11.3-126.4-43.6-281-49-8.8-50-7-94.1 4-140z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown10001o"
			x1={408.9833}
			x2={715.9}
			y1={-126}
			y2={-126}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.601} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001o)"
			d="M629.2 378.8c-55.4-4.2-125.8-13.1-165-29.8-34.5 33.9-83.2 89-35 222 31.5-11.3 126.5-43.6 281.3-49 3.1-17.7 4.9-34.7 5.4-51.3-64-14.7-76.4-60.9-86.7-91.9z"
			opacity={0.2}
		/>
		<path
			d="M1212 383c18.6-.5 191.5-4.9 241-33-19.7-14.9-50.6-29.4-71-29s-90-10.1-103-6-51.8 10.8-67 68zm-4 139c25.4-.9 227.3 21.4 283 46-8.6 17.8-35.5 42.6-64 43-11.5.2-34.1.7-54 4-29.1 4.9-56.8 12.9-71 11-24-3.2-84.4-14.3-94-104zM707 382.9c-18.5-.5-191.5-4.9-241-33 19.7-14.9 50.6-29.4 71-29s90-10.1 103-6 51.8 10.9 67 68zm4 138.9c-25.4-.9-227.3 21.4-283 46 8.6 17.8 35.5 42.6 64 43 11.5.2 34.1.7 54 4 29.1 4.9 56.8 12.9 71 11 24-3.3 84.4-14.3 94-104zM730 224c-12.8 16.1-51 51.8-51 113s64.5 85.9 90 74c0 23.2 1 65 1 65s-101 10.3-136-72 22.7-198.8 60-216c25.7-15 48.8 19.9 36 36zm365-210c21.7 0 101 15.5 101 105s-95.7 94-99 94-100-1.9-100-105c0-73.1 69.7-94 98-94zm-274-2c22.8 0 102 17.5 102 107 0 80.4-94.1 94-99 94-20.7 0-100-17.1-100-98 0-78 74.2-103 97-103z"
			className="factionColorSecondary"
		/>
		<path fill="#e5e5e5" d="M1047 110h99v11h-99v-11zm-273 0h99v11h-99v-11z" />
		<path
			d="M1124 639c-19.3 13.6-53.5 33-163 33s-153.6-17.3-169-42c-.3 26.3 7.6 53.7 14 61 24 2.5 77 15.1 95 24-6.2 12.2-13.9 20.6-21 24 30.5 5.1 125.3 13.2 161-2-13.1-8.4-20-16.9-22-22 20.4-6.4 70.7-23.2 91-23 12.3-14 23.2-50.8 14-53zm41-86c-5.8 66.2-41 121.4-49 133s-49.1 62-155 62-133.6-33.5-160-64-45.6-122.9-47-133c17.9-1.9 26-6 26-6l-6-52-5-1v-15l-53-4s8.1 44.6-15 88c14.1-1.8 26.5-2.2 22-1 3.3 196.3 167.1 226 237 226 34.4 0 123.6-7.5 174-63 52.2-57.4 65-163 65-163l19 3s-14-26-14-100 10-89 10-89l-41 8s-41.9-126-212-126c-150.3-3.9-207.8 101.2-215 126 4.9.8-26.2-6-40-8 1.4 5.4 2 10 4 24 30.2 26.4 59 13 59 13v-18l6-4s27-103 185-103c149.3 0 174 103 174 103l-4 3s17 40.6 17 81-7 74-7 74 32.3 7 25 6z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-topdown10001p"
			x1={959.9304}
			x2={959.9304}
			y1={161}
			y2={46}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10001p)"
			d="M793 632c17.9 18.2 34.1 38 169 38s159.2-28.2 165-35c1.1 14.3 6.5 112-167 112-86.7 0-174.2-29.3-167-115z"
			opacity={0.2}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1309.4 866.8c30-40.3 76.4-104.5 112.6-162.8 26.5-42.8 54-115 54-115s33-22.5 33-126c0-88.3-70.1-130.2-105-138s-68-25.5-103-45c-12.2-51.9-33.7-138.4-56-190-12.9-29.7-30.9-46.1-86.6-54.6m-115.5-7.7c-24.1-.5-51.2-.7-81.9-.7-33 0-62.5-.5-89-.5m-112.8 9.3c-37.7 9.6-65.5 28.9-88.2 67.3-32.1 88.1-49 177-49 177s-71.4 37.3-82 43c-41.5-.5-131 29.6-131 155 0 39 17.7 84.5 21 91 36.3 62 108.4 51.3 131 72 15.5 19 97.9 138.8 160 163s119.5 19 141 19 87.1-10.4 98-24c10.5 13.5 60.2 23 96 23 24 0 60.5.5 94.1-5 9.3-1.5 8.3-.4 16.8-2.9-5.7 8-17.6 24.2-29.8 40.8m-38.1 52h-18v28h-21v43h22v35h-23v43h23v37h-22v43h27l5 764h185l10-764h29v-43h-29v-37h29v-43h-29v-35h29v-43h-29v-28h-8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1449 656c-3.9-19.5-7-42.4-69-36" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1287 927s29.9-36.7 20-63c-13.8-11.8-48.1-20.2-86-20-39.3.2-82.3 9.3-95 14-18.5 6.8-33.8 28.7-40.4 50.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1296 1093s-36.9.4-71.9 0c.1 12.1-.3 24.5 0 43h71.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1072 1093s36.9.4 71.9 0c-.1 12.1.3 24.5 0 43H1072" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1072 1013s36.9.4 71.9 0c-.1 12.1.3 24.5 0 43H1072" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1072 935s36.9.4 71.9 0c-.1 12.1.3 24.5 0 43H1072" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1296 935s-36.9.4-71.9 0c.1 12.1-.3 24.5 0 43h71.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1296 1013s-36.9.4-71.9 0c.1 12.1-.3 24.5 0 43h71.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1099 907h182" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M960 300c38.5 0 164.8 0 167 146s1 169.1 1 188 .8 113-167 113-170-87.6-170-121c0-16.5 2-109.7 2-210 0-87 90.9-116 167-116z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M791 629c17.1 21.7 35 42 169 42s159.3-22.6 168-41" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1218.6 564c-24.7-5-55.9-11.5-78.6-17 20.2-89.3-11-156-11-156s55.6-11.2 85.5-17.3m-508.7-.4C737.6 380 788 391 788 391m-13.7 102c.4 16.7 2.4 34.8 6.7 54-21.7 5.2-55.1 11.4-79.2 16.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1174 382c-11.2-28.8-59.4-125-216-125S754.8 358.5 746 381m-24 180c2.2 27.9 5.5 224 239 224 95.4 0 224.9-39.8 237-224m-32-7c-1.4 19.7-19.2 97.2-53 137m-304 4c-23.2-31.1-48-82.6-53-142m19-165c6.4-22.8 44.2-102 188-102 135 0 165.3 84.1 172 103"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1044 737c-20.8-5.3-27-22-27-22l95-27m-307-1 98 28s-6.2 16.7-27 22"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m795 391-27 1 2 100 23-1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M768 411c-56.8 20.5-141-66.7-47-175 19-21.9 7.2-40.9 0-45s-25-22.2-62 33-40.8 142.7-23 181c27.5 59 72.5 71 133 71"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M628 309c10-36.6 56.3-15 53 3m26 85c-18.7-11.6-57.7 20.5-35 55"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M706.8 382.9s-4.1-.1-11.3-.3m-67-3.5c-16.6-1.3-34.3-3-51.8-5.2-57.2-7.4-111-22-111-22m-41 218.4s56.9-20.1 121-31.1c78-13.4 166-18 166-18"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M536.3 323.4c32.3-4.6 63.4-9.7 90.3-8.1m53.4 18.8c15.4 13 26.8 34.1 32.6 67m4.2 72.1c-2.4 93-28.9 134.2-75.2 147.8-52.2 15.3-80.2-9.5-122.8-9.5-47.6 0-68.1-14.1-81.1-29-2-2.3-4.9-7-8-13.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1410 327c-106.5-12.5-207-50.5-207 132 0 103.3 26.8 148.2 75.4 162.5 52.2 15.3 80.1-9.5 122.6-9.5 47.5 0 68-14.1 81-29"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1213 383s67-.9 130-9c57.2-7.4 111-22 111-22m41 218s-56.9-20.1-121-31c-78-13.3-166-18-166-18"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1306 283c-33.4-17.4-143.6-53-207-53s-101 20-138 20-81.9-20-127-20c-42.8 0-83.6 2.3-115 9.8M637.1 273c-13 6.3-22.1 11-22.1 11"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1096 13c55.2 0 100 44.8 100 100s-44.8 100-100 100-100-44.8-100-100 44.8-100 100-100zm-273 0c55.2 0 100 44.8 100 100s-44.8 100-100 100-100-44.8-100-100S767.8 13 823 13z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1096 63c27.6 0 50 22.4 50 50s-22.4 50-50 50-50-22.4-50-50 22.4-50 50-50zm-273 0c27.6 0 50 22.4 50 50s-22.4 50-50 50-50-22.4-50-50 22.4-50 50-50z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1397 612c-68.1 36.1-93.4 131.2-168 174-18.7 10.7-37.6 24.5-82 31"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M452 596s178.8 278 198 295c19 16.9 250.2 206.4 436.4 323.7m-4.3-204.1c-7.8-5.6-15.2-10.9-22.1-15.6-38.9-26.9-159-120-225.7-171.7M731 807c.5 21.7-19.5 76-86 76"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1058 1027.6c-34.8 31.1-78.3 102.7-11 162.4" />
	</svg>
);

export default Component;

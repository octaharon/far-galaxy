import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#b3b3b3"
			d="m354 1095-71-208 11-50-67-45s-1.6-109.9-1-110 79-50 79-50l12-49 55-41 50-222h66l48 221 56 41 12 48 87 51-1 110-74 47 11 50-73 208-64-1-29-207h-12l-29 206-66 1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m283 887 343 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M318 583h273m12 48 12 207" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m372 542 163-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m294 838 11-207" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m354 1095-71-208 11-50-67-45s-1.6-109.9-1-110 79-50 79-50l12-49 55-41 50-222h66l48 221 56 41 12 48 87 51-1 110-74 47 11 50-73 208-64-1-29-207h-12l-29 206-66 1z"
		/>
		<path
			fill="#b3b3b3"
			d="m1572.4 1095.5 71-208-11-50 67-45s1.6-109.8 1-110c-.6-.1-79-50-79-50l-12-49-55-41-50-222h-66l-48 221-56 41-12 48-87 51 1 110 74 47-11 50 73 208 64-1 29-207h12l29 206 66 1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1643.3 887.5-342.9 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1608.3 583.5h-272.9m-11.9 48-12 207" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1554.4 542.5-162.9-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1632.3 838.5-11-207" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1572.4 1095.5 71-208-11-50 67-45s1.6-109.8 1-110c-.6-.1-79-50-79-50l-12-49-55-41-50-222h-66l-48 221-56 41-12 48-87 51 1 110 74 47-11 50 73 208 64-1 29-207h12l29 206 66 1z"
		/>
		<path
			fill="#d9d9d9"
			d="M575.6 422.8c-3.5-9.4-4.5-12.6-4.6-29.8-.1-17.9 0-69 0-69s-.7-33.6 28-44c-.5-23.2-1-96-1-96s2.9-50 52-50 52 43.9 52 52v19s17.9-85.9 23-100 20.7-62.9 89-82C868.3 7.8 934.7 4 961 4c1.2 0 3.2.5 4.2.5 26.3 0 92.7 3.8 147.1 19 68.3 19.1 83.9 67.9 89 82 5.1 14.1 23 100 23 100v-19c0-8.1 2.9-52 52-52s52 50 52 50-.5 72.8-1 96c28.7 10.4 28 44 28 44s.1 51.1 0 69-1.2 20.6-5 31c20.8 10 20 34 20 34s.8 46.3 0 84-33 47-33 47l28 5s18.6-85 112-85c28.4 0 49.1 13.2 65 25 36.4 27.1 45 69.6 45 90 0 29.3-10 36-10 36l26 49 1 114-45 94-10 2-1 52s38 24.6 38 89c0 52.8-43.2 84.6-50 88 10.3-.4 18 0 18 0v12l-17 30h-130.1l-16-32v-11l19 1s-29.2-17.6-37-41c-10.2 0-16-1-16-1l-19-105-15-45-28 5-27-14s-4.9 67.1-9 92-22.5 126.4-50 182c-27.5 55.6-54.1 94-98 131s-65.9 42-89 42c-3.9 8.7-4.1 6.8-6 15-1.9 8.2-8.9 12-19 12h-23l-.2 2.5h-22v-3h-23c-10.1 0-17.1-3.8-19-12s-2.1-6.3-6-15c-23.2 0-45.1-5-89-42s-70.5-75.4-98-131-45.9-157.1-50-182-9-92-9-92l-27 14-28-5-15 45-19 105s-5.8 1-16 1c-7.8 23.4-37 41-37 41l19-1v11l-16 32H389l-17-30v-12s7.7-.4 18 0c-6.8-3.4-50-35.2-50-88 0-64.4 38-89 38-89l-1-52-10-2-45-94 1-114 26-49s-10-6.7-10-36c0-20.4 8.6-62.9 45-90 15.9-11.8 36.6-25 65-25 93.4 0 112 85 112 85l28-5s-32.2-9.3-33-47c-.8-37.7 0-84 0-84s-.3-22.7 14-32"
		/>
		<linearGradient
			id="ch-topdown18001a"
			x1={965.4825}
			x2={965.4825}
			y1={498.2596}
			y2={682.56}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18001a)"
			d="M918 1417c1.4-3.5 11-24.5 11-179 27.8-1.3 71 0 71 0s-3.8 148.6 13 179c-9.4 4.8-8.4 4-38 4-.1-2.7-1-4-1-4s-20.9-1.7-21-1-1 5-1 5-35.3 3.3-34-4z"
			opacity={0.2}
		/>
		<path
			fill="#b3b3b3"
			d="M420 1145c.3-3 0-8 0-8l-14 13-16 39 127 2v-8l-12-35s-18.9-14.2-19-13-1 11-1 11-57.1-1-65-1z"
		/>
		<path fill="#b3b3b3" d="m417 925 12 2-38 178-5-17 31-163z" />
		<path
			d="M557 530c4 6.3 19.1 24 55 24s42-14 42-14l-3 35-66 14s-22.7-12.8-27-33c-1.9-16.4-1.2-18.6-1-26zm0-24v-33s14.3 38 51 38 49-31 49-31l-1 35s-15.4 16-47 16-52-25-52-25zm14-108s1.4 19.7 6 24c4.3-2.4 15.1-6.9 18-7-6.7-4.2-24-17-24-17zm64 20c6.1-.3 28.8-11.2 31-17-1.5 13.3-7 49-7 49s-4.8-21.5-24-32zm-63-78c-.1 4.7 0 29 0 29s20.3 25 50 25 45.8-14.6 47-22 7-35 7-35-21.1 36-50 36-43.1-17.5-54-33zm88-56c4.9-1.3 24.2-5.3 29-16-3.9 22.5-13 62-13 62s1.4-32-16-46zm-61-22s12.5 12.2 15 13c-8.6 2.8-14 5-14 5l-1-18zm-1-62 1 30s21.8 26 54 26 42-17.6 44-21c.8-5.8 4-23 4-23s-18.1 24.6-51 21c-36.6-1.6-52-33-52-33z"
			opacity={0.2}
		/>
		<path
			fill="#bfbfbf"
			d="m904 1395 6-646s17.6-3.8 22-9c0 18.5-4 459-4 459v99s.5 103.6-12 121c-3.5-5.2-4.9-8.6-6-13-1.1-4.4-6-11-6-11z"
		/>
		<radialGradient
			id="ch-topdown18001b"
			cx={455.754}
			cy={868.183}
			r={221.25}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.298} stopColor="#000" stopOpacity={0} />
			<stop offset={0.499} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18001b)"
			d="M377 973c-9.8 4.4-36 26.4-36 99 0 40.9 48 76 48 76l17 2 14-15 65-1 21 15 11-1s30.8-19.5 37-41c-28.7.1-163 0-163 0l-5-19 8-41s-7.2-1.3-12-16c-3.5-10.6-4.9-28.1-5-58z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown18001c"
			x1={445.6817}
			x2={312.3157}
			y1={881.1768}
			y2={861.9818}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.399} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18001c)"
			d="m394 1047-8 41 4 18-25 22s-12.6-14.6-19-37c-9.7-33.8-10-84.4 32-120-3.9 71.1 16 76 16 76z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown18001d"
			x1={1489.0408}
			x2={1621.5468}
			y1={892.5984}
			y2={848.4424}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.333} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18001d)"
			d="m1533 1047 7.9 41-4 18 24.8 22s12.5-14.6 18.9-37c9.7-33.8 9.9-84.4-31.8-120 4 71.1-15.8 76-15.8 76z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown18001e"
			cx={452.478}
			cy={1303.692}
			r={187.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.11} stopColor="#000" stopOpacity={0} />
			<stop offset={0.499} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18001e)"
			d="m364 634 197-41s-4-15-11-27c-8.4-14.3-20-26-20-26s-26.5-30.8-80-30c-29.3.4-110 28.9-110 115 0 31.5 10 35 10 35"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown18001f"
			x1={339.3868}
			x2={441.8868}
			y1={946.1572}
			y2={928.0842}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18001f)" d="m376 920 3 93s.8 27.7 15 33c3.9-18.1 23-121 23-121l-41-5z" />
		<path
			fill="#b3b3b3"
			d="M1506 1145c-.3-3 0-8 0-8l14 13 16 39-127 2v-8l12-35s18.9-14.2 19-13 1 11 1 11 57.1-1 65-1z"
		/>
		<path fill="#b3b3b3" d="m1509 925-12 2 38 178 5-17-31-163z" />
		<path
			d="M1369 530c-4 6.3-19.1 24-55 24s-42-14-42-14l-4 33 73 16s22.7-12.8 27-33c1.9-16.4 1.2-18.6 1-26zm2.5-24 2.5-32s-13.9 35.7-56 37c-36.7 0-49-31-49-31l1 35s15.4 16 47 16 54.5-25 54.5-25zM1355 398s-1.4 19.7-6 24c-4.3-2.4-15.1-6.9-18-7 6.7-4.2 24-17 24-17zm-64 20c-6.1-.3-28.8-11.2-31-17 1.5 13.3 7 49 7 49s4.8-21.5 24-32zm63-78c.1 4.7 0 29 0 29s-20.3 25-50 25-45.8-14.6-47-22-7-35-7-35 21.1 36 50 36 43.1-17.5 54-33zm-88-56c-4.9-1.3-24.2-5.3-29-16 3.9 22.5 13 62 13 62s-1.4-32 16-46zm61-22s-12.5 12.2-15 13c8.6 2.8 14 5 14 5l1-18zm1-62-1 30s-2.8 8.7-13.2 15.6c-9.4 6.1-25.5 10.4-40.8 10.4-28.3-2.9-38.9-16.6-40.9-20-.8-5.8-3.4-27.5-3.4-27.5s14.3 28.2 47.3 24.5c36.6-1.6 52-33 52-33z"
			opacity={0.2}
		/>
		<path
			fill="#bfbfbf"
			d="m1022 1395-6-646s-17.6-3.8-22-9c0 18.5 4 459 4 459v99s-.5 103.6 12 121c3.5-5.2 4.9-8.6 6-13 1.1-4.4 6-11 6-11z"
		/>
		<radialGradient
			id="ch-topdown18001g"
			cx={1486.754}
			cy={868.183}
			r={221.25}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.298} stopColor="#000" stopOpacity={0} />
			<stop offset={0.499} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18001g)"
			d="M1549 973c9.8 4.4 36 26.4 36 99 0 40.9-48 76-48 76l-17 2-14-15-65-1-21 15-11-1s-30.8-19.5-37-41c28.7.1 163 0 163 0l5-19-8-41s7.2-1.3 12-16c3.5-10.6 4.9-28.1 5-58z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown18001h"
			cx={1477.478}
			cy={1303.692}
			r={187.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.11} stopColor="#000" stopOpacity={0} />
			<stop offset={0.499} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18001h)"
			d="m1562 634-197-41s4-15 11-27c8.4-14.3 20-26 20-26s26.5-30.8 80-30c29.3.4 110 28.9 110 115 0 31.5-10 35-10 35"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown18001i"
			x1={1472.8898}
			x2={1575.3898}
			y1={949.0093}
			y2={930.9363}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18001i)" d="m1550 920-3 93s-.8 27.7-15 33c-3.9-18.1-23-121-23-121l41-5z" />
		<path
			fill="#ff6900"
			d="M742.7 863.7C706.7 899.1 657 948 657 948s9.7 175.8 76 303c46.7 89.5 131.4 146.1 171 144 .1-4.7.4-21.2.6-46 .3-.3.1-2.2.4-3-31.4-15.6-60.6-38.5-84-65-23.6-26.8-49.5-58.7-75-162-.6-87.5-2.1-219.9-3.3-255.3zm440.5.3c36 35.3 85.8 84.2 85.8 84.2s-9.7 175.7-76.1 302.8c-46.7 89.4-131.6 146-171.2 143.9-.1-4.7-.4-21.2-.6-45.9 56.4-40.4 60.1-41.2 83.8-68 23.7-26.8 49.5-58.7 75.1-161.9.5-87.4 2-219.7 3.2-255.1zM556 504l1 28s13.2 20.8 53 23c37.7-2.3 49-18 49-18l-2-25s-11.9 19.4-48 18-53-26-53-26zm15-137s17.9 26.4 51 27 47-22 47-22l-5 32s-14.8 14.4-31 14c-14.3-3.7-25.7-5.7-38-3-10.1-5-21.8-14.3-23-22-.5-10.9-1-26-1-26zm28-138s1.3 32.3 1 33 16 13 16 13 28.4-4.2 44 9c10.2-2.2 27.7-8.4 30-14 1.8-9.6 6-33 6-33s-17.5 22.4-53 18c-35.5-4.4-44-26-44-26zm771 275-1 28s-13.2 20.8-53 23c-37.7-2.3-46-18-46-18l2-26s8.9 20.4 45 19 53-26 53-26zm-15-137s-17.9 26.4-51 27-47-22-47-22l5 29s14.8 17.4 31 17c14.3-3.7 25.7-5.7 38-3 10.1-5 21.8-14.3 23-22 .5-10.9 1-26 1-26zm-28-138s-1.3 32.3-1 33-16 13-16 13-28.4-4.2-44 9c-10.2-2.2-23.7-8.4-26-14-1.8-9.6-8-33-8-33s15.5 22.4 51 18c35.5-4.4 44-26 44-26z"
		/>
		<radialGradient
			id="ch-topdown18001j"
			cx={1049.781}
			cy={841.79}
			r={622.643}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.598} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18001j)"
			d="M683 1118c15 60.1 35 110.2 59.8 150.4 24 38.8 52.6 68.3 78.2 88.6 37.8 29.9 59.3 39.1 83 37 1.1-11.7 1-48 1-48s-21.7-7.8-55-38-52.4-48.7-70-88-33.2-93.7-34-102c-11.5-.4-50.7-.8-63 0z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-topdown18001k"
			cx={1360.4771}
			cy={911.294}
			r={1260}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.598} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18001k)"
			d="M747 1117h-63s-9.7-33.8-16-73c-6.7-41.6-10-88.9-10-96 6.1-8.1 85-83 85-83l4 252z"
			opacity={0.702}
		/>
		<radialGradient
			id="ch-topdown18001l"
			cx={889.618}
			cy={846.269}
			r={622.312}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.598} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18001l)"
			d="M1244 1117.9c-15 60-35 110.2-59.8 150.3-23.9 38.8-52.6 68.3-78.1 88.5-37.8 29.9-59.3 39.1-83 37-1.1-11.7-1-48-1-48s21.7-7.8 55-38 52.4-48.7 70-87.9c17.6-39.3 33.2-93.6 34-101.9 11.4-.4 50.6-.8 62.9 0z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-topdown18001m"
			cx={580.906}
			cy={952.504}
			r={1259.33}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.598} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18001m)"
			d="M1180 1116.9h63s9.7-33.8 16-73c6.7-41.6 10-88.9 10-95.9-6.1-8.1-85-83-85-83l-4 251.9z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-topdown18001n"
			x1={324.0533}
			x2={824.0533}
			y1={1158.5822}
			y2={1167.3102}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ff6900" />
			<stop offset={0.905} stopColor="#671e75" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18001n)"
			d="M324 710v114l456 3 44-88-12-6s-61.7-19.3-94-52c-41.5 3.1-394 29-394 29z"
		/>
		<linearGradient
			id="ch-topdown18001o"
			x1={1104.345}
			x2={1603.6816}
			y1={1162.2346}
			y2={1170.9504}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.095} stopColor="#671e75" />
			<stop offset={1} stopColor="#ff6900" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18001o)"
			d="M1603 710v114l-456 3-43-89 11-5s61.7-19.3 94-52c41.4 3.1 394 29 394 29z"
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M575.6 422.8c-3.5-9.4-4.5-12.6-4.6-29.8-.1-17.9 0-69 0-69s-.7-33.6 28-44c-.5-23.2-1-96-1-96s2.9-50 52-50 54 43.9 54 52-2 19-2 19 17.9-85.9 23-100 20.7-62.9 89-82C868.3 7.8 934.7 4 961 4c1.2 0 3.2.5 4.2.5 26.3 0 92.7 3.8 147.1 19 68.3 19.1 83.9 67.9 89 82 5.1 14.1 25 100 25 100s-2-10.9-2-19 2.9-52 52-52 52 50 52 50-.5 70.8-1 94c28.7 10.4 28 46 28 46s.1 51.1 0 69 1.8 19.6-2 30c18.6 10.6 20 35 20 35s-2.2 46.3-3 84c-.7 30.8-22.2 42.6-30.2 45.9m25.3 6.1s18.6-85 112-85c28.4 0 49.1 13.2 65 25 36.4 27.1 45 69.6 45 90 0 29.3-10 36-10 36l26 49 1 114-45 94-10 2-1 52s38 24.6 38 89c0 52.8-43.2 84.6-50 88 10.3-.4 18 0 18 0v12l-17 30h-130.1l-16-32v-11l19 1s-29.2-17.6-37-41c-10.2 0-16-1-16-1l-19-105-15-45-28 5-27-14s-4.9 67.1-9 92-22.5 126.4-50 182c-27.5 55.6-54.1 94-98 131s-65.9 42-89 42c-3.9 8.7-4.1 6.8-6 15-1.9 8.2-8.9 12-19 12h-23l-.2 2.5h-22v-3h-23c-10.1 0-17.1-3.8-19-12s-2.1-6.3-6-15c-23.2 0-45.1-5-89-42s-70.5-75.4-98-131-45.9-157.1-50-182-9-92-9-92l-27 14-28-5-15 45-19 105s-5.8 1-16 1c-7.8 23.4-37 41-37 41l19-1v11l-16 32H389l-17-30v-12s7.7-.4 18 0c-6.8-3.4-50-35.2-50-88 0-64.4 38-89 38-89l-1-52-10-2-45-94 1-114 26-49s-10-6.7-10-36c0-20.4 8.6-62.9 45-90 15.9-11.8 36.6-25 65-25 93.4 0 112 85 112 85m25.4-6c-7.7-3.1-29.8-14.9-30.4-46-.8-37.7 0-84 0-84s-.3-22.7 14-32"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M952 1423v-7h22v8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M905 1346c-13.1-4.5-19.1-9.6-29.2-16.8-37.3-26.7-78.1-60.2-106.8-134.2-12-32-15.6-44.4-23-78-.6-40.9-3-253-3-253"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M746 1117h-63" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1244 1117h-63" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1021.8 1345.6c13.1-4.5 19.1-9.6 29.1-16.8 37.3-26.6 78-60.2 106.6-134.1 11.9-32 15.6-44.4 23-77.9.6-40.8 3-252.8 3-252.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M571 334c.9 10.2 15.8 38 53 38s51-35.9 51-49-5-47.7-45-48c-19.6 0-33 6-33 6"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M598 192c0 3.8 9.3 41 55 41s51-33.7 51-38" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M661 574c-1.2-11.1-5.2-68.8-3-97-3 40.5-7 97-7 97m8-120c.8-17.8 12-99.9 18-126s24.8-121.8 29-142"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m403 997 8 10 177-1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m407 985 8 8 177-2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m405 1151-16 38" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m505 1148 14 41" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M420 1135v9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M485 1145v-10" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M904 1393c3.4-9.5 6-610.5 6-645" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M917 1417c4.7-9 11-49 11-123s4-555 4-555" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M823 738c7.5 3 73.7 10 87 10s22-9 22-9h31" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M390 1190s28.3-46.5 29-46 64.7 1.5 66 1 33 44 33 44"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m388 1148 18 2 14-16h67l18 14 30-1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m659 946 121-119 44-89s-73.1-22.6-115-65c-39.6-40.1-48-100-48-100l-297 61-19 35m434 158-456-4m1-114 393-28"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m375 919 280 30" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m607 957-208-35" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M379 971c.6 27-3.6 63.1 15 76" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M417 926s-31.3 161.7-31 162 4 19 4 19l179-1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m391 1106 38-178" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M556 464c0 8 9.5 46 55 46s48-40.6 48-48-3.1-49-50-49c-23.4 0-41 14-41 14"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1355 333.7c0 5.9-13.8 38-51 38s-51-35.9-51-49c0-13.1 3.4-49 48-49 19.6 0 29 6 29 6"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1328 191.6c0 3.8-7.3 41-53 41s-50-33.7-50-38" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1267 573.7c1.2-11.1 5.2-72.8 3-101m-1-19c-.8-17.8-12.1-99.9-18-126-6-26.1-24.8-121.8-29-142"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1525 996.8-8 10-177-1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1521 984.8-8 8-177-2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1523 1150.8 16 38" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1423 1147.8-14 41" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1508 1134.8v9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1443 1144.8v-10" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1024 1392.8c-3.4-9.5-6-610.6-6-645.1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1011 1416.8c-4.7-9-11-49-11-123s-4-555.1-4-555.1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1105 737.7c-7.4 3-73.7 10-87 10-13.3 0-22-9-22-9h-33"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1538 1189.8s-28.3-46.5-29-46c-.7.5-64.7 1.5-66 1-1.3-.5-33 44-33 44"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1533 1148.8-11 1-14-16h-67l-18 14-30-1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1269 945.8-121-119-44-89s73.1-22.6 115-65c39.6-40.1 48-100 48-100l297 61 19 35m-434 158 456-4m-1-114.1-393-28"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1553 918.8-280 30" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1321 956.8 208-35" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1549 970.8c-.6 27.1 3.6 63.1-15 76" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1511 925.8s31.3 161.8 31 162c-.2.3-4 19-4 19l-179-1"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1537 1105.8-38-178" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1373 463.7c-.3 15.7-10.5 46-56 46s-48-40.6-48-48c0-7.4.1-49 47-49 30.2.7 42 14 42 14"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M960.3 28c-29.9-.4-182.4-2.4-212.3 60-36.1 75.3-55.3 232.2-65 306-9.6 72.8-17.6 145.9 5 201 31.3 76.1 114.5 122 274 122 .9 0 2.3-.3 4-.3 159.5 0 242.7-45.9 274-122 22.6-55.1 14.6-128.2 5-201-9.8-73.8-28.9-230.8-65-306-29.5-61.6-178.6-60.4-211.1-60-2.2-.1-7 .3-8.6.3z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M556 500c.7 12.3 25.1 28.7 50.5 30.5 10.4.8 21.9-.8 32.2-5.1 7.3-3 13.3-7.8 18.3-17.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M571 363c.7 12.3 22.1 28.7 47.5 30.5 10.4.8 21.9-.8 32.2-5.1 7.3-3 14.3-10.8 19.3-20.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M598.4 225.5c3.2 12.1 23.4 28.3 47.1 30 10.4.8 21.9-.8 32.2-5.1 7.3-3.1 14.3-8.8 19.3-18.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M599.1 259.4c3.3 5.3 11.1 11.8 18.5 16.2m40.8 7.5c5.9-.8 11.7-2.3 17.3-4.6 5.1-2.1 10.2-5.6 14.4-10.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M571 389c.5 8.8 9.5 18.6 23.9 24.8m39.2 3.5c4.3-.9 8.5-1.1 12.6-2.8 7.3-3 13.3-7.8 18.3-17.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M556 526c.7 12.3 24.1 26.7 49.5 28.5 17.8 1.3 39.7-4.1 52.5-18.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1371.5 499.8c-.7 12.2-24.1 28.6-49.6 30.5-10.4.8-21.9-.8-32.3-5-7.3-3.1-13.4-7.8-18.3-17.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1328 228.8c-3.1 11.7-21.5 25.1-45.2 26.9-10.5.8-22-.8-32.3-5.1-7.3-3-14.4-8.8-19.3-18.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1328.3 260.5c-3.3 5.3-8.1 9.7-15.5 14.1m-42.9 8.6c-5.9-.8-11.8-2.3-17.3-4.6-5.1-2.1-10.2-5.6-14.4-10.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1355.5 363c-.7 12.2-20.1 28.7-45.6 30.5-10.4.8-21.9-.8-32.3-5.1-7.3-3-14.4-10.8-19.3-20.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1355.5 389c-.5 8.8-8.5 19.6-23 25.8m-38.2 2.4c-4.3-.9-8.6-1.1-12.7-2.8-7.3-3-14.4-7.8-19.3-17.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1370.5 525.8c-.7 12.2-22.1 26.7-47.6 28.5-17.9 1.3-39.8-4.1-52.6-18.5"
			/>
		</g>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M963 160c133.7 0 242 108.4 242 242s-108.3 242-242 242-242-108.3-242-242 108.3-242 242-242z"
			/>
			<path
				fill="#f2f2f2"
				d="M789 676c-12.9-7.4-46.1-19.3-77-75-25.9-40.4-26-78.3-26-150s38.7-236.9 42-255 17.7-98.5 32-121S803.9 5 963 5c26.7 0 103.5 1.6 158 26s62.6 84.4 72 123 30.9 156.8 40 212 8 72.6 8 133-29.2 140.3-100 175c-13.1 26.3-25 51-25 51s0 179.5 1 180 16 0 16 0l-13 425-17 1-4 163 38 15-33 385-52 7-3-5-30 1-15-98-11 113-29 5-27-4-12-116-13 98-35 2v6l-15-1-39-8-32-382 40-18-7-164h-15l-14-424 16-1V723s-15.6-32.8-22-47z"
			/>
			<path d="M959 1512s6.2-3.3 7 2c5.8 38.7 39 285 39 285l-11 113-30 4-27-3-12-107 34-294z" opacity={0.302} />
			<path fill="#bfbfbf" d="M831 1894h46l-1 9h-18l-27-9zm217 1 38-2-5 5-30 4-3-7z" />
			<path fill="#e6e6e6" d="m913 1895-20 1 43-386-138-1 35-15h268l33 15H991l43 386-16 1-54-393-2 2-49 390z" />
			<linearGradient
				id="ch-topdown18001p"
				x1={965}
				x2={965}
				y1={3}
				y2={405.9974}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown18001p)"
				d="M960 1515s6.2-3.3 7 2c5.8 38.7 27 205 27 205l11 79s-8 83.3-12 111c-6.8 1.4-29 5-29 5s-12.7-1.1-28-5c-3.7-26.7-11-110-11-110l7-86 28-201z"
				opacity={0.302}
			/>
			<linearGradient
				id="ch-topdown18001q"
				x1={736.8536}
				x2={736.8536}
				y1={1244}
				y2={1877}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.503} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown18001q)"
				d="M788 43c.2 8.6-1 633-1 633s-58.5-23.3-83-92c-18.8-33.8-18-106-18-106s-3.1-64.9 10-125c11.2-63.5 26-140 26-140s17.3-87.9 22-102 17.1-50.8 44-68z"
				opacity={0.2}
			/>
			<linearGradient
				id="ch-topdown18001r"
				x1={1188.9908}
				x2={1188.9908}
				y1={1244}
				y2={1877}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.503} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown18001r)"
				d="M1138 43c-.2 8.6 1 633 1 633s58.3-23.3 82.7-92c18.7-33.8 17.9-106 17.9-106s3.1-64.9-10-125c-11.1-63.5-25.9-140-25.9-140s-17.3-87.9-21.9-102c-4.6-14.1-17-50.8-43.8-68z"
				opacity={0.2}
			/>
			<path
				fill="#671e75"
				d="M788 241V41s10.6-7.1 32-15.9V257c-24.6-8-32-16-32-16zM963 6c38.9.9 80.3 3 116 11.3v245.2c-27.6 6.4-65.4 11.5-116 11.5-51.6 0-88.7-4.2-115-9.6V17.5C876.2 9.6 914.5 3.6 963 6zm180 40c.3 42.4 0 191 0 191s-10.2 8.6-36 17.5V25.7c13.7 5.2 25.9 11.8 36 20.3zM788 671l353 2-25 52-305-1-23-53zm6 235 17-1 118 84 1 254-105 87-16 1-15-425zm339-1-12 426-16-1-104-89-1-250 117-85 16-1zm-342 605h145l-43 385-69-1-33-384zm199-2 148 1-34 383-69 4-45-388z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M788 245V41" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1143 245V45" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1141 674c70.8-34.7 100-114.6 100-175s1.1-77.8-8-133-30.6-173.4-40-212-17.5-98.6-72-123C1071 8.6 1002.1 5.4 970.5 5H956c-31.6.4-100.6 3.6-150.8 26-54.6 24.4-62.7 84.4-72.1 123-9.4 38.6-30.9 156.8-40.1 212-9.1 55.3-8 72.6-8 133s29.3 140.3 100.2 175c2 1.2 3.8 2 3.8 2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m788 241-1 430 24 53v181l-17 1 15 424h15l7 164-40 16 31 384 38 8h16s-.1-3.9 0-5c2.8-.1 36-2 36-2l12-98 12 116 28 4 29-5 11-115 14 99h31l2 5 53-8 33-385-38-14 4-164h17l13-425h-16l-1-180 25-54 2-435s-35.9 38-180 38-175-33-175-33z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M822 1894h91m9.1-74.1c13.7-111.2 38.3-309.7 40.8-324.1.1-.5 1.1-.8 1.1-.8l54 401 86-5"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M793 1510h144l25-15 28 14h147" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M830 1494h269" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m824 1331 106-88-1-254-118-83m307 0-119 85 1 251 103 87"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1116 724H812m-23-53 352 1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m894 1892 42-382" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M990 1510c0 .7 44 384 44 384" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M964 1917v-417" />
		</g>
	</svg>
);

export default Component;

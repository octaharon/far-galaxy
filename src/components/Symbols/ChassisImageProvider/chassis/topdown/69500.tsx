import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M839 1576c-27.5 0-113.6 27.7-138 100s-43 165-43 165-115-.4-128-2c-8.8-5.9-33.6-31-63-78s-50-89-50-89l-1-597s47.2-21.8 64-36c20.2-27.1 34-84.7 34-170s-21-150-21-150-67.3-39.2-80-48c0-310 23.5-425.3 39-482s64-147 64-147l178-3s6.3 14 12 27c6.8-5.2 27-10 27-10l-1-8 23-1s4 5.2 5 7c61.4 3.1 81 45 81 45l92 145h54l92-145s19.6-41.9 81-45c1.1-1.8 5-7 5-7l23 1v8s19.2 4.8 26 10c5.8-13 12-27 12-27l178 3s48.5 90.3 64 147 39 172 39 482c-12.8 8.8-80 48-80 48s-21 64.7-21 150 13.8 142.9 34 170c16.8 14.2 64 36 64 36l-1 597s-20.6 42-50 89-54.2 72.1-63 78c-13 1.6-128 2-128 2s-18.6-92.7-43-165-110.5-100-138-100H839z"
		/>
		<radialGradient
			id="ch-topdown69500a"
			cx={615.15}
			cy={-730}
			r={72.925}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#f3f3f3" />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown69500a)"
			d="M549.3 358.1c11.8-24.3 36.8-41.1 65.7-41.1 40.3 0 73 32.7 73 73s-32.7 73-73 73c-38.2 0-69.6-29.4-72.7-66.8 8 2.1 17.7 3.8 17.7 3.8l2-10s12.7 1.6 14-8-6.5-10.1-10-11c.6-5.9 0-8 0-8s-10.1-1.9-16.7-4.9z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown69500b"
			x1={538.8792}
			x2={545.244}
			y1={-729.0745}
			y2={-761.8195}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown69500b)" d="m512 358 59 14s6.7 4.5 5 12-8.1 7.4-12 6-58-14-58-14 7.1-8.9 6-18z" />
		<linearGradient
			id="ch-topdown69500c"
			x1={1383.2294}
			x2={1376.5435}
			y1={-729.7876}
			y2={-761.7876}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500c)"
			d="M1350 373c5.4-1.4 54.1-14.1 60-15 .4 4.6 3.4 17.2 7 18-15 3.7-64 14-64 14s-6.6-2.5-8-7c-1.1-3.6 2.6-9.4 5-10z"
		/>
		<linearGradient
			id="ch-topdown69500d"
			x1={1380.1866}
			x2={1383.0237}
			y1={251.9408}
			y2={224.9408}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500d)"
			d="M1355 1345c8 .7 57.4 8.2 62 9-1.7 3.7-3.7 13.6-2 18-8.4-.1-58.3-9.2-63-10s-8.7-17.3 3-17z"
		/>
		<linearGradient
			id="ch-topdown69500e"
			x1={545.2858}
			x2={536.9459}
			y1={251.9112}
			y2={222.8272}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500e)"
			d="M506 1353c4.9-.8 54.5-9.6 59-10 7.6-1.1 16.1 9.6 6 17-11.2 2.8-62 12-62 12s-2.6-17.1-3-19z"
		/>
		<path
			fill="#666"
			d="m465 340 10-5-12 43-7-9 9-29zm985-4 10 8 7 24-6 10-11-42zm12 1013 6 10-4 26-8 8 6-44zm-1001 2 8 43-10-6-5-27 7-10z"
		/>
		<radialGradient
			id="ch-topdown69500f"
			cx={610.5127}
			cy={222.7179}
			r={72.2196}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#f3f3f3" />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown69500f)"
			d="M541.9 1338.3c1.5-27 17.9-52.2 44.6-63.5 37.2-15.8 94 12.6 92.6 64.2-1.1 40.3-28.5 76-65 76-25.9 0-56.1-16.2-63.8-38.9 8.2-1.2 17.8-2.1 17.8-2.1l-2-13s12.3-1.8 9.8-11.1c-2.5-9.3-9.9-6.7-13.5-6.2-1.8-5.7-3.1-7.4-3.1-7.4s-10.2 2.3-17.4 2z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown69500g"
			cx={1312.4746}
			cy={222.8201}
			r={72.1814}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#f3f3f3" />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown69500g)"
			d="M1381 1338.1c-1.4-27.1-17.9-52.4-44.5-63.7-37.1-15.8-94.5 20.6-92.5 64.3 1.1 40.4 28.3 76.3 64.9 76.3 25.9 0 56-16.2 63.8-38.9-8.2-1.2-15.7-2.1-15.7-2.1l1-13s-13.4-1.9-10.9-11.3c2.6-9.3 12.3-6.2 15.9-5.7 1.8-5.7.7-7.9.7-7.9s10.1 2.3 17.3 2z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown69500h"
			cx={1307}
			cy={-730}
			r={73}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#f3f3f3" />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown69500h)"
			d="M1372.9 358.1c-11.9-24.3-36.9-41.1-65.8-41.1-40.4 0-73.1 32.7-73.1 73s32.7 73 73.1 73c38.3 0 69.7-29.4 72.9-66.8-8 2.1-17.8 3.8-17.8 3.8l-2-10s-12.8 1.6-14-8c-1.3-9.6 6.5-10.1 10-11-.6-5.9 0-8 0-8s10.1-1.9 16.7-4.9z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown69500i"
			cx={960.5}
			cy={-621.5452}
			r={176.5226}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown69500i)"
			d="M971 322c118.4 0 165 120.6 165 162-.3 5.3.8 16.6 1 22-20.6 160.6-157 168-176 169-7 0-9.2-.9-12-1-57.1 0-165-51.5-165-165 .3-7.4-.5-16.2 1-22 0-80.6 78.1-165 164-165 6.4-.5 12.8 1.3 22 0z"
			opacity={0.2}
		/>
		<path
			fill="#333"
			fillRule="evenodd"
			d="M960.9 595.1c-13.8-23.3-35.4-60-35.4-60h70.1c0-.1-23.1 39.9-34.7 60zM978 545h-35s10.8 18.4 17.7 30c5.8-10 17.3-30 17.3-30zm17-34h-70s21.5-36.7 35.3-60c11.7 20.1 34.7 60 34.7 60zm-34.9-40c-6.9 11.6-17.6 30-17.6 30h34.9s-11.5-19.9-17.3-30zm-58.8 40c-13.8-23.3-35.3-60-35.3-60h70s-23 39.9-34.7 60zm17.1-50h-34.9s10.8 18.3 17.6 30c5.8-10.1 17.3-30 17.3-30zm102.9 50c-13.8-23.3-35.3-60-35.3-60h70s-23 39.9-34.7 60zm17.1-50h-34.9s10.8 18.3 17.6 30c5.8-10.1 17.3-30 17.3-30z"
			clipRule="evenodd"
		/>
		<linearGradient
			id="ch-topdown69500j"
			x1={1293.0128}
			x2={628}
			y1={-251.5}
			y2={-251.5}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.005} stopColor="#e6e6e6" />
			<stop offset={0.203} stopColor="#0c0a0a" />
			<stop offset={0.8} stopColor="#0c0a0a" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500j)"
			d="M631 482c11.2-2.5 36.8-9.9 52-25 .5 10.6 1 135 1 135h-48l-8 4s2.9-100.3 3-114zm-2 156 6 3h49v102h-48l-7 4V638zm0 150 5 4h50v102h-48l-6 3-1-109zm0 150 6 4 49 1v336s-24.3-25.2-55-28c-.2-31 0-313 0-313zm660-456c-11.2-2.5-36.8-13.9-52-29-.5 10.6-1 139-1 139h48l8 4s-2.9-100.3-3-114zm2 156-6 3h-49v102h48l7 4V638zm0 150-5 4h-50v102h48l6 3 1-109zm0 150-6 4-49 1 4 341s22.3-29.2 53-32c.2-31-2-314-2-314z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-topdown69500k"
			x1={961.5}
			x2={961.5}
			y1={298.04}
			y2={155.04}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#595959" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500k)"
			d="M1236 1290c-7.2 5-120 93-120 93H805l-118-95s35.5 76.3-13 124c15.4 10.5 33.3 19 111 19s351-1 351-1 100-1.1 113-18c-16.4-15.9-27-35.5-27-56s5.3-50.8 14-66z"
		/>
		<path
			fill="#e6e6e6"
			d="M749 85c37 4.8 57.1 23.3 59 60s-34.5 67.6-67 66-68.7-24.7-63-68 32.5-54.6 67-58c6.3-11.1 14.9-30.8 15-33-2.8-2.8-5-6-5-6l-22 1v8s-85 22.2-85 102c0 16.4 1.1 101.6 98 107 .6 2.2 1 5 1 5l24-1-1-4s14.8-2.1 18-5 14-12.4 27-14c14.5-11.5 46-52.7 46-89s-28.3-96.3-99-102c-3.2 3.5-11 24.8-13 31zm421 0c-37 4.8-57.1 23.3-59 60s34.5 67.6 67 66 68.7-24.7 63-68-32.5-54.6-67-58c-6.3-11.1-14.9-30.8-15-33 2.8-2.8 5-6 5-6l22 1v8s85 22.2 85 102c0 16.4-1.1 101.6-98 107-.6 2.2-1 5-1 5l-24-1 1-4s-14.8-2.1-18-5-13.9-12.4-27-14c-14.5-11.5-46-52.7-46-89s28.3-96.3 99-102c3.2 3.5 11 24.8 13 31z"
		/>
		<path
			fill="#e6e6e6"
			d="M550 358c7.5-15 20.9-41.5 72-40 43 1.3 68.2 46.2 67 71s-18.2 75.5-78 75c-61.2-6.2-68.5-63.1-68-68-8.3-2-18-4-18-4s-2.1 80.3 88 91c50.8 2 97-40.8 96-88 .5-37.7-19.5-94.5-90-97-62-2.3-84.5 47.8-88 55 7.9 3.5 9.5 4.2 19 5zm822 0c-7.5-14.9-28.2-40.8-75-40-43 1.3-63.2 46.2-62 70.9s10.8 65.8 73 73.9c66-5.3 71.5-62 71-66.9 8.3-2 21-4 21-4 .3-.2-6.2 92.4-94 91.9-46.3-1.8-92-39.7-91-86.9-.5-37.7 10.2-87.9 85-98.9 61.5-4.9 91.5 48.7 95 55.9-7.9 3.6-13.5 3.3-23 4.1zm33 985c-1.2-15.4-11.6-92-96-92-56 1.4-66.5 32.1-68 34s-19 23.8-19 71 43.2 83.1 95 78c54.9-6.6 72.7-45.5 78-55-9.1-.6-21-3-21-3s-21.6 39-61 39-68-35.8-68-72 33.1-73 69-73 65 34.8 69 69c15.7 2.9 13.4 2 22 4zm-886-1c-.1-11.5 6-91 97-91 49.7 1.5 64.3 27.4 67 31s17 23.1 17 65-30.5 88-84 88-74.7-33.4-88-54c21.8-3.4 39-7 39-7l-1-12s9-3.6 9-12-7.5-6.7-14-6c-1.6-4.1-3-7-3-7s-28.3 3.7-39 5z"
		/>
		<path
			fill="#bfbfbf"
			d="m720 293 6-10s26.9-.2 40-13c-12.4.1-18 0-18 0l-1-5s-52.2-8.3-66-26c17.9 26.7 39 54 39 54zm481 0-6-10s-26.9-.2-40-13c12.4.1 18 0 18 0l1-5s52.2-8.3 66-26c-17.9 26.7-39 54-39 54z"
		/>
		<path
			fill="#bfbfbf"
			d="M742 124c8.2.2 23.8 8 22 23s-10.8 19.2-21 19-23.1-9.2-22-20 12.8-22.2 21-22zm437 3c9.7 2.2 20.9 6.6 20 22s-15.5 20.5-23 20-20.1-9.1-19-23 12.3-21.2 22-19z"
		/>
		<linearGradient
			id="ch-topdown69500l"
			x1={995.1102}
			x2={1114.1102}
			y1={-890.9019}
			y2={-948.9418}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#d1d1d1" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500l)"
			d="M1107 245c-16.2-13.7-51.1-45.5-45-119-16.8 26.3-74 117-74 117s101.8 1.8 119 2z"
		/>
		<linearGradient
			id="ch-topdown69500m"
			x1={890.0079}
			x2={808.5879}
			y1={-857.7567}
			y2={-976.7567}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#d1d1d1" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500m)"
			d="M815 245c16.2-13.7 51.1-45.5 45-119 16.8 26.3 74 117 74 117s-101.8 1.8-119 2z"
		/>
		<path
			fill="#ccc"
			d="M529 1836c-3.4-11.8-68-167-68-167v-279l-2-4-4-26 6-12-1-296-43 23 1 598s68 135.6 111 163zm863 0c3.4-11.8 68-167 68-167v-279l2-4 4-26-6-12 1-296 43 23-1 598s-68 135.6-111 163zM516 46c-5.4 24.6-9.9 86.4 0 133-15.5 11.6-30.8 69-39 155-2.2.6-5 1-5 1l-9 7-6 25 4 10 11 4-12 319-46-30s-8-296.8 37-478c17-52.7 38.3-104.7 65-146zm889-1c5.5 24.6 9.7 87.3 1 134 13.3 13.9 28.9 69 37.2 155 2.2.6 7.8 0 7.8 0l8 9 6 25-2 10-14.8 3 12.1 319 46.8-30s7.5-296.8-37.7-478c-17.2-52.7-37.6-105.7-64.4-147z"
		/>
		<path
			fill="#e6e6e6"
			d="m531 1838 128 2s28.8-129.4 36-143c-45.5-5.3-233-29-233-29l69 170zm697-141 231-29-68 171-128 1-35-143z"
		/>
		<path
			fill="#d9d9d9"
			d="m463 1667 232 31s8.8-91.1 131-121c32.4-2.8 113.5-1 134-1s125.5-1.8 140 2 104.1 23.6 127 120c45.7-6.3 230-31 230-31l-164-233s-26.6-4.8-45-22c-9 7.4-35 18-110 18s-353 1-353 1-87.8 2.8-110-17c-12.2 7.4-17.4 14.9-47 20-9.5 15.4-165 233-165 233zm21-630c20.8-11.5 132.2-82 145-90 .4-3.1 1-8 1-8l-6-4v-34l6-5-1-108-5-3-1-6s-122.4-57.3-131-62c9.6 37.7 23 90.2 23 152s-8.4 124.1-31 168zm813-259 130-60s-20 75.2-20 151 8.9 133.1 32 168c-21.2-12-147-90-147-90l1-8 3-3v-35l-4-4V789l6-4-1-7zm-778 811 109-154s-64.6 11.6-101-55c-4.8.9-8 1-8 1v208zm0-253c6.1-25.6 19-92.4 110-85-.2-27.7 1-304 1-304s-34.8 24.1-111 68c0 25.3-.4 292.6 0 321z"
		/>
		<linearGradient
			id="ch-topdown69500n"
			x1={960.5}
			x2={960.5}
			y1={444.955}
			y2={369.955}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={0.31} stopColor="#e6e6e6" />
			<stop offset={0.996} stopColor="#626262" />
		</linearGradient>
		<path fill="url(#ch-topdown69500n)" d="M818 1556h285l-35-75H853l-35 75z" />
		<path
			fill="#d9d9d9"
			d="M517 41c24.8.4 177-3 177-3s6.8 14.8 12 27c-52.3 31.7-62.9 80.3-57 117s33 61 33 61 31.5 40.2 38 49c-2.7 7.6-16.6 41-21 53-16.8-37.6-70.8-48-87-48s-34 7.1-52 20c-20.4-12.4-40.1-101.6-45-137-2.7-42.5-9.6-107 2-139zm885 1c-11.2-.4-175-3-175-3s-6.8 14.8-12 27c52.3 31.7 62.9 78.3 57 115s-35 63-35 63-29.6 38.2-36 47c7.1 13.4 18.6 46 23 58 16.4-40.2 66.8-51 83-51s38 5.1 56 18c25.8-23.3 37.1-99.6 42-135 4.6-22.3 10.2-127.2-3-139z"
		/>
		<path
			fill="#d9d9d9"
			d="M786 260c-6.6 2.1-15 4-15 4v4s-17.2 12.8-45 15c-8.6 15.2-24.8 51-28 64 4.8 11.8 9.7 18.8 8 46s-5.2 44.5-23 66v825l122 99 311-1 125-97s-4-828.5-4-833c-6.3-8.2-21-28.8-21-51s4.5-46.4 7-52c0-8.9-17.1-52.9-28-66-12.7-1.8-25-5.7-32-13-7.6-1.1-13-2-13-2l1-4-16-4s30 58 30 114 3 302 3 302l1 279 1 194-85 97-250 1-84-98s-1.4-162.7-1.9-349.3c-4-238.7 2.4-501.3 36.9-539.7z"
		/>
		<linearGradient
			id="ch-topdown69500o"
			x1={786.1295}
			x2={823.1293}
			y1={147.0656}
			y2={129.0196}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500o)"
			d="M799 1283c1.6-6.1 5.3-17.9 26-14-5.4-10.4-21.2-27.5-30-29-3.3 8.3-7 17-7 17s9.4 32.1 11 26z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-topdown69500p"
			x1={1094.3263}
			x2={1132.8483}
			y1={137.7855}
			y2={144.5854}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500p)"
			d="M1094 1270c6-1.8 20.6-4.6 28.3 15.1 5.1-13.1 12.2-24.9 9.7-32.1-2.4-5.3-6-12-6-12s-24.8 7.5-32 29z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-topdown69500q"
			x1={759.1031}
			x2={795.1321}
			y1={112.787}
			y2={129.911}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500q)"
			d="m784 1264 10-23s.8-5.6-6-12-9.9-7.3-15-3-12.9 16.5-15 21c8.2-.4 22.5 4.8 26 17z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-topdown69500r"
			x1={1125.8685}
			x2={1163.4535}
			y1={128.8936}
			y2={117.0215}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500r)"
			d="m1164 1249-19-24s-3.5-.8-12 3-7.1 10.7-5 17 5 13.3 8.9 19.4c2.8-7.6 12.1-18.6 27.1-15.4z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-topdown69500s"
			x1={755.5746}
			x2={760.1726}
			y1={88.5119}
			y2={119.169}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500s)"
			d="M741 1209c9.8-1.5 19.6.1 23 0 5.7 2.7 12.6 9.2 13 16-4.7 1.2-12 8.2-15 14-5.9-.6-21-1-21-1s15.5-16.8 0-29z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown69500t"
			x1={1160.5116}
			x2={1165.0786}
			y1={88.7706}
			y2={119.2195}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown69500t)"
			d="M1180.1 1238.5c-9.8 1.7-19.6.3-23 .4-5.8-2.6-12.8-8.9-13.3-15.8 5.6-14.8 10.4-10.2 14.7-14.3 5.9.5 21 .6 21 .6s-15.1 17.2.6 29.1z"
			opacity={0.6}
		/>
		<path
			fill="gray"
			d="m700 980-9 11v276l116 101h306l117-102 3-262-8-8-3 255-115 101-294-3-113-104V980zm16-19 2 253 6 7s-1.4 11.8 8 16c7.4 1.1 12 1 12 1l11 11s-5.9 14 7 21c10 6.3 17 1 17 1l42 41 278-1 42-40s12.6 3.1 19-3 5.6-14.8 4-18c5.1-4.7 13-13 13-13s6.6 4 14-4 4-13 4-13l7-7 1-253 12 20-1 255-110 96-288-1-108-98-2-259 10-12z"
		/>
		<linearGradient
			id="ch-topdown69500u"
			x1={654}
			x2={654}
			y1={-478}
			y2={-528}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown69500u)" d="M683 592h-47l-12 8v34l12 7 48 1-1-50z" opacity={0.4} />
		<linearGradient
			id="ch-topdown69500v"
			x1={654}
			x2={654}
			y1={-327}
			y2={-377}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown69500v)" d="M683 743h-47l-12 8v34l12 7 48 1-1-50z" opacity={0.4} />
		<linearGradient
			id="ch-topdown69500w"
			x1={654}
			x2={654}
			y1={-176}
			y2={-226}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown69500w)" d="M683 894h-47l-12 8v34l12 7 48 1-1-50z" opacity={0.4} />
		<linearGradient
			id="ch-topdown69500x"
			x1={1268}
			x2={1268}
			y1={-478}
			y2={-528}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown69500x)" d="M1239 592h47l12 8v34l-12 7-48 1 1-50z" opacity={0.4} />
		<linearGradient
			id="ch-topdown69500y"
			x1={1268}
			x2={1268}
			y1={-327}
			y2={-377}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown69500y)" d="M1239 743h47l12 8v34l-12 7-48 1 1-50z" opacity={0.4} />
		<linearGradient
			id="ch-topdown69500z"
			x1={1268}
			x2={1268}
			y1={-176}
			y2={-226}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown69500z)" d="M1239 894h47l12 8v34l-12 7-48 1 1-50z" opacity={0.4} />
		<path
			fillRule="evenodd"
			d="m835 1246-84-98s-2-532-2-552c0-168.7 11.5-257.3 24-305 11.4-43.6 40-46 40-46l22-.3v130.5c-36.9 37.7-49 84-49 110.8-2.9 19.6-.4 87.2 49 136.7V1246h250V622.4c41.7-43.7 52-101 52-114.4-.2-7.5-.5-15.7-1-22 .4-18.5-10.8-71-51-111.7V244.8c9.3.1 15.7.2 17 .2 6.6 0 21.7 2.6 33 15 27.4 44.9 29.8 118 30 130s2 207 2 207l4 550-86 99s-119.4 1-125 1-125-1-125-1zM960 243s33.8.5 68.5 1h-142l73.5-1z"
			className="factionColorPrimary"
			clipRule="evenodd"
		/>
		<path
			fill="#333"
			d="M1309 366c12.2 0 22 9.9 22 22s-9.8 22-22 22-22-9.9-22-22 9.8-22 22-22zm-696.3.3c12.2 0 22 9.9 22 22 0 12.2-9.9 22-22 22-12.2 0-22-9.9-22-22s9.9-22 22-22zm-10.9 954.3c11.1-4.9 24.1.1 29 11.2s-.1 24.1-11.3 29c-11.1 4.9-24.1-.1-29-11.2s.2-24.1 11.3-29zm719.8.2c-11.1-4.9-24.1.1-29.1 11.2-4.9 11.1.1 24.1 11.3 29 11.1 4.9 24.1-.1 29.1-11.2 4.9-11.1-.2-24.1-11.3-29z"
		/>
		<path
			d="M519.3 1381.9c-13.1 1.3-21.3 2.1-21.3 2.1l-6 4-23 5-8-4 2 278s25.2-35.6 56-79c0-103.1.3-197.5.3-206.1zM518 1343l-23 5-9-5-25 5v-296c17.7-10.2 38.4-22.8 59.2-35.7.2 37.9-2.2 326.7-2.2 326.7zm3.1-612.8c-28.6-13.9-53.8-26.6-61.1-31.2-.7-155.9 12-319 12-319l14 3 8-1s19.3 6 28 10c0 100.4-.6 299-.9 338.2zm10.1-466.8c-5.5-19.1-11.1-45.3-16.2-81.4-7.2 2-9.2 9.9-16 30s-21.5 90.9-22 123c11.4 3.4 23 5 23 5l4 7s11 2.9 18.9 4.9c0-15.5 1.4-76.4 8.3-88.5zM1407 1381c13.1 1.3 16.1 3 16.1 3l6 4 22.9 5 8-4-2 278s-20.3-27.6-51-71v-215zm-1-37 24 3 5.1-4 24.9 5v-296c-17.6-10.2-33.3-22.1-54-35-.2 38 0 327 0 327zm-1-615c28.5-13.9 48.7-25.4 56-30 .7-155.9-12-319-12-319l-14 3-8-1s-15.4 4-24.1 8c.1 100.4 1.8 299.9 2.1 339zm-13-478c5.5-19.1 9.1-32.9 14.1-69 7.1 2 9.1 9.9 15.9 30s21.5 90.9 21.9 123c-11.4 3.4-22.9 5-22.9 5l-4 7s-6.2 1.9-14.1 4c.1-15.5-4-88-10.9-100z"
			className="factionColorSecondary"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M839 1576c-27.5 0-113.6 27.7-138 100s-43 165-43 165-115-.4-128-2c-8.8-5.9-33.6-31-63-78s-50-89-50-89l-1-597s47.2-21.8 64-36c20.2-27.1 34-84.7 34-170s-21-150-21-150-67.3-39.2-80-48c0-310 23.5-425.3 39-482s64-147 64-147l178-3s6.3 14 12 27c6.8-5.2 27-10 27-10l-1-8 23-1s4 5.2 5 7c61.4 3.1 82 45 82 45l91 145h54l92-145s19.6-41.9 81-45c1.1-1.8 5-7 5-7l23 1v8s19.2 4.8 26 10c5.8-13 12-27 12-27l178 3s48.5 90.3 64 147 39 172 39 482c-12.8 8.8-80 48-80 48s-21 64.7-21 150 13.8 142.9 34 170c16.8 14.2 64 36 64 36l-1 597s-20.6 42-50 89-54.2 72.1-63 78c-13 1.6-128 2-128 2s-18.6-92.7-43-165-110.5-100-138-100H839z"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M705 1665c18.2-46.8 79-233 79-233v-64" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m694 1698-231-30-45 5" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="m574.3 1660.6 52 7.8-7.8-42.5-44.2 34.7zm771.6-.4-51.8 7.8 7.8-42.5 44 34.7z"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1225.3 1697.5 231.3-30 45.1 5" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m531 1840-69-171 167-236" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1390.1 1840.1 69.1-170.8-167.2-235.9" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M462 1668s-.2-134.8-.5-278.5m0-41.6c-.1-60.9-.2-178.3-.3-296.6M461 700s-1.2-169.7 11.5-318.4m4.8-47.9c7.7-68.6 19-127.4 35.7-154.7 54.9 0 136 1 136 1"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1407.5 1597c-.1-18.4-.3-104.4-.7-214.6m-.1-39.4c-.3-94.4-.7-207.6-1-325.3m-.9-287c-.4-129.1-.8-249.2-1.1-340.2m-.1-39.1c-.1-47-5.7-82.8-10.2-99.7"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M517.5 1590c.1-18.4.3-97.4.7-207.6m.1-39.4c.3-94.4.7-209.6 1-327.3m.9-285c.4-129.1.8-249.2 1.1-340.2m.1-39.1c.1-44 5-78.2 9.3-96.3"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m417 1088 34-16v583l-32 4" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1503 1088-34-16v583l32 4" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1458.5 1669s.2-134.9.5-278.6m0-41.6c.1-60.9.2-178.3.3-296.7m.2-351.4s1.2-169.8-11.6-318.5m-4.7-47.9c-7.7-68.7-19.1-127.5-35.8-154.8-55.1 0-136.3 1-136.3 1"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M479 710c23.5 13.6 128.8 60.1 144 68" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1437.8 712.2c-22.1 12.8-116.3 54.5-140 65.9" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M474 1043c21.2-15.9 134.6-78.3 155-97" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1445.5 1043c-20.6-15.6-129.1-75.5-153.2-95.8" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M559 317c-28.2-18.2-63.2-182.6-43-273" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1363.3 314.9c28.1-18.2 61.1-180.8 41-271.4" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1215 1665c-18.2-46.8-79-233-79-233v-64" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1103 1555-35-75H853l-35 75h285z" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="m857 1555 10-14h26l7 14m40 0 6-14h29l6 14m44 0 5-13h24l6 14"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1167 47 10 36s-5.4.2-5.4.4c0 .1-11.6-29.4-11.6-29.4" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m753 47-10 35.6s5 1.3 5 1.4 12-30.1 12-30.1" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1107 244c-32.4 0-101.8-1-123-1m-50.8.4c-43.4.5-124.2 1.6-124.2 1.6"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1161 270c7.5 6.5 24.7 12.5 34 13 8.2 14.3 21.9 36.8 27 66m14 102c1.5 22.6 3 837 3 837l-124 96H805l-122-99V457m15-112s17-45.6 27-62c14.2-1.2 32.8-5.1 44-14"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1252 227-51 65" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m662 216 59 78" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1209 282c16.4 9.5 34.1 30.6 37 37m40 161c2.6 30.9 6 104.2 6 116m-1 43 1 108m0 42v108m0 42v312m-44 162c-26.8 15.1-51.5 18-288 18s-255.5 1.8-287-18m-45-163 1-311m0-42-1-110m0-41V637m0-42 2-113m32-172c5.5-7.7 28.7-38.6 39-43"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M683 641h-50l-10-7v-35l11-8h49m-1 200h-48l-11-7v-34l10-8 50 1m1 151-50-1-10 7v35l10 6 49 1m554.1-300.5h50.3l10.1-7v-35l-11.1-8h-49.3m2 200h47.3l11.1-7v-34l-10.1-8-49.3 1m0 199 48.3-1 10.1-6v-35l-10.1-7-48.3 1"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M635 592v49m0 102v48m1 103v48m648 0v-48m0-101v-49m1-103v-49"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="m716 961 2 253 6 7s-1.4 11.8 8 16c7.4 1.1 12 1 12 1l11 11s-5.9 14 7 21c10 6.3 17 1 17 1l42 41 278-1 42-40s12.6 3.1 19-3 5.6-14.8 4-18c5.1-4.7 13-13 13-13s6.6 4 14-4 4-13 4-13l5-12 1-253 14 25-1 256-110 95-288-1-108-98-2-259 10-12z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="m700 980-9 11v276l116 101h306l117-103 2-261-7-8-3 254-115 102-294-3-113-104V980z"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M725 1222c.2-5.2 8.3-20.6 17-10 9.8 11.9 1.7 26.6-6 26" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M737 1208h26s12.5 5.5 14 15c6.2 1.1 17.3 5.7 17 16 8.2 1.8 24.4 11.7 32 30 4.6 8.5-10.1 15.8-15 16s-10.3 2.7-14-5-10-24-10-24l-7 15"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m786 1259 8-19" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M799 1285c-1.7-3.9 3.1-12.7 10-15s15.7-3.5 18 3" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m755 1250 10-16s8.2-10.6 14-10" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M761 1237s-17.7 1.9-17 1" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M755 1252c6.5-12 33.5 4.4 26 18" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1195 1223.1c-.2-5.3-8.3-20.8-17-10.1-9.8 12-1.7 26.8 6 26.2"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1183 1209h-26s-12.5 5.5-14 15.1c-6.2 1.1-17.3 5.7-17 16.1-8.2 1.8-24.4 11.7-32 30.2-4.6 8.5 10.1 15.9 15 16.1s10.3 2.7 14-5c3.7-7.8 10-24.1 10-24.1l7 15.1"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1134 1260.3-8-19.1" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1155 1237.2s21.7 2.9 21 2" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1164.8 1252.7c-6.5-12-33.5 4.4-26 18.1" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1165 1251-21-27" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1121 1286.5c1.7-4-3.1-12.8-10-15.1-6.9-2.3-15.7-3.6-18 3"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1134 260c15.4 24.8 31 77 31 141s5 746 5 746l-86 99H834l-84-98s-2-438.2-2-500-1-362.9 42-391"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M847 1202h59v19h-59v-19z" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="m683 1282 69-135m83 100-31 137m312 0-31-137m84-101 72 141"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M960.5 322.5c97.2 0 176 78.8 176 176s-78.8 176-176 176-176-78.8-176-176 78.8-176 176-176z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M960.5 339.3c87.9 0 159.2 71.3 159.2 159.2s-71.3 159.2-159.2 159.2-159.2-71.3-159.2-159.2 71.3-159.2 159.2-159.2z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M960.5 374.9c68.2 0 123.6 55.3 123.6 123.6s-55.3 123.6-123.6 123.6-123.6-55.3-123.6-123.6 55.4-123.6 123.6-123.6z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M960.9 595.1c-13.8-23.3-35.4-60-35.4-60h70.1c0-.1-23.1 39.9-34.7 60zM1021 509c-13.8-23.3-35.4-60-35.4-60h70.1s-23.1 40-34.7 60zm-120.2 0c-13.8-23.3-35.4-60-35.4-60h70.1s-23.1 40-34.7 60zm59.6-58.1c-13.9 23.1-35.5 59.6-35.5 59.6h70.4s-23.2-39.6-34.9-59.6z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M954 323v16m13-16v16m101.5 42.8 11.3-11.3m9.2 9.2-11.3 11.3m-245.3-12.5 11.3 11.3m-2.1-20.4 11.3 11.3M954.5 659v16m13-16v16m102.1-58.8 11.3 11.3m-2.1-20.5 11.3 11.3m-247.4-10.5-11.3 11.3m20.5-2.1-11.3 11.3"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1179 83c15.3-.3 62.5 13.3 62 67-.5 57.9-57.8 60.4-69 59-6.3-.8-61.8-9-61-65 .8-52.4 48-60.7 68-61zm-439.5.5c-15.3-.3-62.5 13.3-62 67 .5 57.9 62.7 59.2 69 59s61.8-9 61-65c-.8-52.4-48-60.7-68-61z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M741.5 124c-5-.1-20.7 4.4-20.5 22.3.2 19.3 20.7 19.7 22.8 19.7 2.1-.1 20.5-3 20.2-21.7-.2-17.5-15.9-20.2-22.5-20.3zm435 2c-5.1-.1-20.7 4.4-20.5 22.3.2 19.3 20.7 19.7 22.8 19.7 2.1-.1 20.5-3 20.2-21.7-.2-17.4-15.9-20.2-22.5-20.3z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1067 119c-18.9 35.8 7.1 111.3 47 128 9 2 17.1 8.1 21 12 9 3.9 16 4 16 4l-1 5 24 2 2-7s59.6 1.2 86-51 9.5-115.5-48-148"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M854.2 118.2c18.8 35.8-7 112.4-46.9 129.1-9 2-17.1 8.1-21 12-9 3.9-16 4-16 4l1 5-24 2-2-7s-59.4 1.2-85.8-51S650 96.7 707.4 64.2"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1173 270 7-61" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m747.5 270.7-7-61.4" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1188 49-1 33" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m733.5 49.1 1 32.9" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1150 266 14-58" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m769.5 265.4-14.1-57.8" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1299 298c-25.3 4.6-91 21.1-83 102 7.7 77.4 83.3 84 97 84s79.8-14.8 87-94c11.4-3 31-9 31-9l5 2 26-6 3-9-6-25-10-9s-23.8 5.2-27 6c-.9 1.7-3 6-3 6l-25 7s-29.7-63.2-95-55z"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1419 347 11 37" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1412 358c4.3-1.3 9.2 15.3 6 17-4.7 2.5-11-15.5-6-17z" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1418 375-65 15s-16.2-6.1-3-18c16-4.4 62-15 62-15" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1399 352-45 11 1 7" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1405 389-43 11-3-10" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1372 358c-7.3-10.6-23.8-40-67-40s-70 31.3-70 73c0 43.2 34.2 70 78 70s65.3-42.6 66-65"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1361 361c-6.4-11.6-24.8-31-50-31s-61 16.9-61 59 38.7 57 62 57c17 0 45.5-9 55-46"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1305 366v-19h7v21" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1448 337c.3.3 11 41 11 41" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1425 342 12 43" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M622.7 298.3c27.6 2.5 89.2 20.8 85.1 102.1-3.1 60.3-60.2 85.4-99.1 82.1-13.7-1.2-77.9-11.9-85.1-91.1-11.4-3-33-10-33-10l-5 2-26-6-3-9L465 340l7.6-5.7s23.8 5.2 27 6c.9 1.7 3 7 3 7l28 7c.1 0 26.8-62.7 92.1-56z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M549.3 358.1c11.8-24.3 36.8-41.1 65.7-41.1 40.3 0 73 32.7 73 73s-32.7 73-73 73c-38.2 0-69.6-29.4-72.7-66.8"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M502.6 347.3s-7.5 25.1-10.1 34" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M509.6 358.3c-4.3-1.3-9.2 15.3-6 17 4.7 2.6 11-15.5 6-17z"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m503.6 375.4 65 15s16.3-6.1 3-18c-16-4.4-62-15-62-15" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m522.7 352.3 45 11-1 7" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m516.7 389.4 43 11 3-10" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M560.7 361.3c6.4-11.6 27.8-32 53-32S674 346.9 674 389s-38.7 61-59 61c-17 0-50.8-12.6-60.3-49.6"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M616.7 366.3v-19h-7v21" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M475.6 335.3c-.3.3-13 43-13 43" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m497.6 341.3-13 44.1" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M583.5 1254.4c49.2-12.8 102.9 2.4 113.3 59.6 12.6 69.7-17.9 101.4-50.9 116.1-12.6 5.6-79.2 19.8-117.7-49.7-11.7 1.9-32.1 4.3-32.1 4.3l-3.8 3.9-26.3 5-6.4-7-4.6-26.3 5.5-12.3s23.9-4.9 27.2-5.5c1.5 1.2 5.2 4.3 5.2 4.3l25.7-3.7c.1.1 1.7-69.8 64.9-88.7z"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m493.3 1347.8 4.9 38.3" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M504.2 1355c-4.4.6-2.3 17.7 1.4 18 5.3.4 3.8-18.7-1.4-18z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="m505.6 1373 65.6-12.6s12.4-12.2-4.5-17.7c-16.4 2.5-62.9 11.4-62.9 11.4"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m513.7 1344.3 45.7-8.2 1.9 6.8" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m523.1 1380.5 43.8-7.4-1.3-10.4" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M540.8 1338.8c2.4-12.6 5.7-46.2 45.2-63.7 39.6-17.5 79.9 11.3 89.2 41.2 9.2 29.6 1.4 75-38.8 92.8-40.1 17.8-77.1-12.5-86.8-32.7"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M552.1 1337.1c1.2-13.2 10.2-38.4 33.3-48.6s60-7.8 73.3 32.1c13.6 40.8-11.8 67.5-27.4 74.4-15.6 6.9-45.3 10.2-69-19.8"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m608.2 1362.5.2 19 7-.1-.2-21" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M461.7 1349.4c-.1.4 7.5 42.9 7.5 42.9" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m485.8 1344.7 6.4 45.2" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1339.9 1254.6c-49.3-12.8-97.8 3.6-113.3 59.6-18.3 66.1 16.4 104.1 51 116 13 4.5 79.2 18.8 117.8-50.7 11.7 1.9 32 4.3 32 4.3l3.8 3.8 26.3 5 6.4-7 4.6-25.3-5.5-12.3s-23.9-4.9-27.2-5.4c-1.5 1.2-5.2 4.3-5.2 4.3l-25.8-3.7c0 .1-1.7-69.7-64.9-88.6z"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1430.1 1348-4.9 38.3" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1419.3 1354.2c4.4.6 2.3 18.7-1.4 19-5.3.3-3.8-19.7 1.4-19z"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="m1417.7 1372.6-66.3-10.2s-12.8-11.6 3.9-17.7c16.5 1.9 63.4 9.1 63.4 9.1"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1409.8 1344.4-45.7-8.2-1.9 6.8" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1400.3 1380.7-43.9-7.4 1.3-10.4" />
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1382.6 1339c-2.4-12.6-5.7-46.2-45.3-63.7-39.6-17.5-74.7 12.8-87.3 41.2-12.6 28.4-3.4 74.9 36.8 92.7 40.2 17.7 77.1-12.5 86.8-32.7"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1371.3 1337.3c-1.2-13.2-10.2-38.4-33.3-48.6-23.1-10.2-56.3-6.4-73.4 32.1-17 38.4 11.8 67.4 27.4 74.3s45.4 10.2 69-19.8"
		/>
		<path
			fill="none"
			stroke="#180d24"
			strokeWidth={5}
			d="M1309 366c12.2 0 22 9.9 22 22s-9.8 22-22 22-22-9.9-22-22 9.8-22 22-22zm-696.3.3c12.2 0 22 9.9 22 22 0 12.2-9.9 22-22 22-12.2 0-22-9.9-22-22s9.9-22 22-22zm-10.9 954.3c11.1-4.9 24.1.1 29 11.2s-.1 24.1-11.3 29c-11.1 4.9-24.1-.1-29-11.2s.2-24.1 11.3-29zm719.8.2c-11.1-4.9-24.1.1-29.1 11.2-4.9 11.1.1 24.1 11.3 29 11.1 4.9 24.1-.1 29.1-11.2 4.9-11.1-.2-24.1-11.3-29z"
		/>
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1315.2 1362.7-.2 19-7-.1.2-21" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="M1460.7 1349.6c.2.4-6.5 42.9-6.5 42.9" />
		<path fill="none" stroke="#180d24" strokeWidth={5} d="m1437.6 1345.8-6.4 44.1" />
		<g id="Animation">
			<path
				fill="#f2f2f2"
				d="M949.6 709.2c16.8 0 55.9-2.9 62 3s27 38 47 83 57.5 129.1 61 143c3.5 14 12.2 54.9 5 92-7.2 37.1-19.4 96.7-41 147-13.9 32.4-42.1 25-51 25v18h-9l1 96-10 19v72l7-4-6 175-5-5-2 51 14 24-1 150-134.1-1v-148l13-26-1-51-6 6-5-176 7 5-1-71-10-22v-94h-9v-20h-29c-13.5 0-22.2-18-28-30-5.8-12.1-26.8-75.2-33-105-6.3-29.8-7-60.4-7-74s1.8-46.8 26-101c24-53.7 36.1-88.9 59-131 23-42.1 30.6-50 40-50h46.1z"
			/>
			<path fill="none" stroke="#333" strokeWidth={20} d="m832.5 894.2 72-38v-147m98.2 0v147.2l74.3 39" />
			<path fill="#999" d="m917.6 1682.4 72 1 1 19-72-1-1-19zm1 35h71v19l-71-1v-18z" />
			<path fill="none" stroke="#999" strokeWidth={10} d="m899.6 1576.4 43-37v-100l-47-35" />
			<path fill="none" stroke="#999" strokeWidth={10} d="m1010.6 1576.6-43-37.1v-100.1l47-35.1" />
			<path
				fill="#d9d9d9"
				d="M824.5 1179.3c7 2 35.7-11.1 50-60-7.7-32.5-38-140.9-38-220.1 0-39.9 19.9-107.7 63-189-11.5 9-24.8 28-30 38s-90.1 178.2-90.1 238.1c0 26 1.4 67.7 13 104 11.4 47.5 28.2 87.9 32.1 89zm191.2-462.1c21 20.1 96 191.4 105.1 220.1 9.1 28.7 7 62.7 7 73s-20.7 123.7-44 165c-3.8 4.9-9.9 8.4-21-6-11.2-14.5-24-44.5-26-51 4.1-17.6 38-144.6 38-202.1s-37.5-150.5-59.1-199z"
			/>
			<linearGradient
				id="ch-topdown69500A"
				x1={954.65}
				x2={954.65}
				y1={83.3}
				y2={-0.7}
				gradientTransform="matrix(1 0 0 1 0 1120)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#b1b1b1" />
				<stop offset={0.985} stopColor="#e6e6e6" />
			</linearGradient>
			<path
				fill="url(#ch-topdown69500A)"
				d="M874.6 1119.3c8.5.2 161.1 0 161.1 0s29.1 74.1 48 59c-6.9 13.4-8.1 25-50 25v-66H876.6l-1 64s-27.8-1-32-1-16.1-10.4-18-19c10-2.5 32.3-13.8 49-62z"
			/>
			<path fill="#e6e6e6" d="m888.6 1649.4 13-25h107.1l15 25H888.6zm-3-333.1h140.1l-11 21-118.1-1-11-20z" />
			<path
				fill="none"
				stroke="#180d24"
				strokeWidth={5}
				d="M949.6 709.2c16.8 0 55.9-2.9 62 3s27 38 47 83 57.5 129.1 61 143c3.5 14 12.2 54.9 5 92-7.2 37.1-19.4 96.7-41 147-13.9 32.4-42.1 25-51 25v18h-9l1 96-10 19v72l7-4-6 175-5-5-2 51 14 24-1 150-134.1-1v-148l13-26-1-51-6 6-5-176 7 5-1-71-10-22v-94h-9v-20h-29c-13.5 0-22.2-18-28-30-5.8-12.1-26.8-75.2-33-105-6.3-29.8-7-60.4-7-74s1.8-46.8 26-101c24-53.7 36.1-88.9 59-131 23-42.1 30.6-50 40-50h46.1z"
			/>
			<path fill="none" stroke="#180d24" strokeWidth={5} d="M888.6 1649.4h133.1" />
			<path
				fill="none"
				stroke="#180d24"
				strokeWidth={5}
				d="M918.6 1682.9h72.1v19h-72.1v-19zm0 34h72.1v19h-72.1v-19z"
			/>
			<path fill="none" stroke="#180d24" strokeWidth={5} d="M900.6 1624.4h109.1" />
			<path fill="none" stroke="#180d24" strokeWidth={5} d="m894.6 1407.4 45 33v97l-40 35" />
			<path fill="none" stroke="#180d24" strokeWidth={5} d="m1015.2 1407.4-45.1 33v97.1l40 35" />
			<path fill="none" stroke="#180d24" strokeWidth={5} d="M894.6 1336.3h120.1" />
			<path fill="none" stroke="#180d24" strokeWidth={5} d="M884.6 1315.3h141.1" />
			<path fill="none" stroke="#180d24" strokeWidth={5} d="M884.6 1223.3v-55l139.1 1v54" />
			<path fill="none" stroke="#180d24" strokeWidth={5} d="m1032.7 1200.3 1-63-158.1 1v67" />
			<path
				fill="none"
				stroke="#180d24"
				strokeWidth={5}
				d="M822.5 1177.3c5.2 8.7 37-8.7 51.7-58.7 17.2 0 162.4-.3 162.4-.3s28.4 80.2 48 57"
			/>
			<path
				fill="none"
				stroke="#180d24"
				strokeWidth={5}
				d="M874.6 1119.3c-16.4-52.3-39-156.8-39-220.1s54.2-179.2 64-189"
			/>
			<path
				fill="none"
				stroke="#180d24"
				strokeWidth={5}
				d="m843.5 1070 4.6-3.1 11.4 47.8-4.9 2.9-11.1-47.6zm-10.2 7 4.4-3 10.8 47.3-4.7 2.8-10.5-47.1zm-9.8 6.8 4.3-2.9 10.1 46.7-4.5 2.7-9.9-46.5zm243.2-13.4-4.6-3.1-11.4 47.5 4.9 2.9 11.1-47.3zm10.2 7-4.4-3-10.7 47 4.7 2.8 10.4-46.8zm9.8 6.7-4.3-2.9-10.1 46.5 4.5 2.7 9.9-46.3z"
			/>
			<path
				fill="none"
				stroke="#180d24"
				strokeWidth={5}
				d="M1010.6 711.2c9.8 9.8 64 134.8 64 198s-22.7 157.7-39 210.1"
			/>
			<path
				fill="none"
				stroke="#180d24"
				strokeWidth={5}
				d="M890.6 933.3c16 0 29 13 29 29s-13 29-29 29-29-13-29-29 12.9-29 29-29z"
			/>
		</g>
	</svg>
);

export default Component;

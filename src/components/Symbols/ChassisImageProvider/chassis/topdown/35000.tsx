import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#d9d9d9"
			d="m1669.9 1433.8-74.9 64-11 33 7 12-14 9-19 56 31 98-69.9 34-62-83-109.9-37-98.9 23-29-59 80-62 1-7s-22.4-14.3-52-41-193.9-186-193.9-186l135.9-176s212.8 215 216.9 219c4.1 4 18.2 17.1 22 25 10.9 5.2 115.9 42 115.9 42l97.9-23 26.9 59z"
		/>
		<linearGradient
			id="ch-topdown35000a"
			x1={1466.2393}
			x2={1528.2454}
			y1={405.7103}
			y2={566.9043}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000a)"
			d="M1558 1520c-7.1-4.9-87-64-87-64l-58 36s54.7-61.4 19.1-133.2c44.2 31.1 96.3 66 102.9 79 6.7 13 21.1 24.3 23 82.2z"
			opacity={0.502}
		/>
		<path
			fill="#ccc"
			d="m1588 1704.8-30-95-99.9 49 62 81 67.9-35zm8-207 73.9-65-28-58-97.9 23 52 100zm-250.8 122-47-98-79 62 30 60 96-24z"
		/>
		<path fill="#999" d="m1401.1 1501.8 118.9 85-1-12-109.9-79-8 6z" />
		<linearGradient
			id="ch-topdown35000b"
			x1={1081.8484}
			x2={1466.4854}
			y1={382.7688}
			y2={794.8088}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000b)"
			d="M1260 1484.6c25.4 21.1 43.3 40.7 84.2 40.2 20.7-3.4 29.5-3.3 58-25 28.5-21.6 47.4-71 40-109-7.4-38-28.8-58.2-42-66-14.1-14.6-208.9-212-208.9-212l-134.9 176c-.1 0 208.9 200.3 203.6 195.8z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown35000c"
			x1={1132.3237}
			x2={1519.6918}
			y1={688.5633}
			y2={404.5324}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.5} stopColor="#666" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000c)"
			d="M1260 1484.6c25.4 21.1 43.3 40.7 84.2 40.2 20.7-3.4 29.5-3.3 58-25 28.5-21.6 47.4-71 40-109-7.4-38-28.8-58.2-42-66-14.1-14.6-208.9-212-208.9-212l-134.9 176c-.1 0 208.9 200.3 203.6 195.8z"
		/>
		<path
			fill="#999"
			d="M1380.6 1471.5c7.8-5.2 16.6-5.7 19.7-1.1 3.1 4.6-.8 12.5-8.6 17.8-7.8 5.2-16.6 5.7-19.7 1.1-3.1-4.6.8-12.6 8.6-17.8z"
		/>
		<radialGradient
			id="ch-topdown35000d"
			cx={1406}
			cy={491}
			r={44}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown35000d)"
			d="M1406 1385c24.3 0 44 19.7 44 44s-19.7 44-44 44-44-19.7-44-44 19.7-44 44-44z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1669.9 1433.8-74.9 64-11 33 7 12-14 9-19 56 31 98-69.9 34-62-83-109.9-37-98.9 23-29-59 80-62 1-7s-22.4-14.3-52-41-193.9-186-193.9-186l135.9-176s212.8 215 216.9 219c4.1 4 18.2 17.1 22 25 10.9 5.2 115.9 42 115.9 42l97.9-23 26.9 59z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1458.1 1656.8 99.9-48m38-111-51-100m-198.8 221-46-96m0-8c12.5 7 62.5 21.7 103.9-17 41.4-38.7 51.8-97.3 25-143m-36-38c30.4 42.8-30.9 195-136.9 163m102.9-30s8.6 10.3 19.9 22.1m13 16.4c3.5 3.8 6.9 7.2 10.1 9.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1373.1 1297.8c10.7 28.6 10.2 86.5-29.4 126.6-27.7 28-64.6 47.6-111.5 33.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1380.6 1471.5c7.8-5.2 16.6-5.7 19.7-1.1 3.1 4.6-.8 12.5-8.6 17.8-7.8 5.2-16.6 5.7-19.7 1.1-3.1-4.6.8-12.6 8.6-17.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1408.1 1496.8 120.9 85 48-30m-56 36.2-2-13m-118.9-73.2 119.9 86-65 42-118.9-86 132.9-89 120.9 88m-193.8 46s31.5-21.6 63.2-42.9m11.3-7.3c29.9-20.2 57.4-38.8 57.4-38.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1432.1 1358.8s82.6 58.2 90 65c37.5 39 35.1 81.7 35 96.2"
		/>
		<path
			fill="#d9d9d9"
			d="m245.1 1433.8 75 64 11 33-7 12 14 9 19 56-31 98 69.9 34 62-83 109.9-37 98.9 23 29-59-80-62-1-7s22.4-14.3 52-41 193.9-186 193.9-186l-135.9-176s-212.8 215-216.9 219c-4.1 4-18.2 17.1-22 25-10.9 5.2-115.9 42-115.9 42l-97.9-23-27 59z"
		/>
		<linearGradient
			id="ch-topdown35000e"
			x1={477.5841}
			x2={363.8641}
			y1={402.3212}
			y2={561.5151}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000e)"
			d="M359 1518c7.1-4.9 87-62 87-62l59 38s-57.4-60.9-22.1-135.2c-44.2 31.1-96.3 66-102.9 79-6.7 13-20.7 22.8-21 80.2z"
			opacity={0.502}
		/>
		<path
			fill="#ccc"
			d="m327 1704.8 30-95 99.9 49-62 81-67.9-35zm-8-207-74-65 28-58 97.9 23-51.9 100zm250.8 122 47-98 79 62-30 60-96-24z"
		/>
		<path fill="#999" d="m513.9 1501.8-118.9 85 1-12 109.9-79 8 6z" />
		<linearGradient
			id="ch-topdown35000f"
			x1={474.2665}
			x2={861.6355}
			y1={739.652}
			y2={400.6559}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000f)"
			d="M655 1484.6c-25.4 21.1-43.3 40.7-84.2 40.2-20.7-3.4-29.5-3.3-58-25-28.5-21.6-47.4-71-40-109 7.4-38 28.8-58.2 42-66 14.1-14.6 208.9-212 208.9-212l134.9 176c.1 0-208.9 200.3-203.6 195.8z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown35000g"
			x1={829.796}
			x2={442.428}
			y1={757.801}
			y2={370.4331}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.5} stopColor="#666" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000g)"
			d="M655 1484.6c-25.4 21.1-43.3 40.7-84.2 40.2-20.7-3.4-29.5-3.3-58-25-28.5-21.6-47.4-71-40-109 7.4-38 28.8-58.2 42-66 14.1-14.6 208.9-212 208.9-212l134.9 176c.1 0-208.9 200.3-203.6 195.8z"
		/>
		<path
			fill="#999"
			d="M534.4 1471.5c-7.8-5.2-16.6-5.7-19.7-1.1-3.1 4.6.8 12.5 8.6 17.8 7.8 5.2 16.6 5.7 19.7 1.1 3.1-4.6-.8-12.6-8.6-17.8z"
		/>
		<radialGradient
			id="ch-topdown35000h"
			cx={509}
			cy={491}
			r={44}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown35000h)"
			d="M509 1385c24.3 0 44 19.7 44 44s-19.7 44-44 44-44-19.7-44-44 19.7-44 44-44z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m245.1 1433.8 75 64 11 33-7 12 14 9 19 56-31 98 69.9 34 62-83 109.9-37 98.9 23 29-59-80-62-1-7s22.4-14.3 52-41 193.9-186 193.9-186l-135.9-176s-212.8 215-216.9 219c-4.1 4-18.2 17.1-22 25-10.9 5.2-115.9 42-115.9 42l-97.9-23-27 59z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m456.9 1656.8-99.9-48m-38-111 51-100m198.8 221 46-96m0-8c-12.5 7-62.5 21.7-103.9-17-41.4-38.7-51.8-97.3-25-143m36-38c-30.4 42.8 30.9 195 136.9 163m-102.9-30s-8.6 10.3-19.9 22.1m-13 16.4c-3.5 3.8-6.9 7.2-10.1 9.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M541.9 1297.8c-10.7 28.6-10.2 86.5 29.4 126.6 27.7 28 64.6 47.6 111.5 33.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M534.4 1471.5c-7.8-5.2-16.6-5.7-19.7-1.1-3.1 4.6.8 12.5 8.6 17.8 7.8 5.2 16.6 5.7 19.7 1.1 3.1-4.6-.8-12.6-8.6-17.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m506.9 1496.8-120.9 85-48-30m57 37 2-14m117.9-73-119.9 86 65 42 118.9-86-132.9-89-120.9 88m193.8 46s-31.5-21.6-63.2-42.9m-11.3-7.3c-29.9-20.2-57.4-38.8-57.4-38.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M482.9 1358.8s-82.6 58.2-90 65c-7.4 6.8-34.9 33.8-35 86v9.2"
		/>
		<path
			fill="#d9d9d9"
			d="m237.1 311.2 75-64 11-33-7-12 14-9 19-56-31-98 69.9-34 62 83 109.9 37 98.9-23 29 59-80 62-1 7s22.4 14.3 52 41 193.9 186 193.9 186l-135.9 176s-212.8-215-216.9-219c-4.1-4-18.2-17.1-22-25-10.9-5.2-115.9-42-115.9-42l-97.9 23-27-59z"
		/>
		<linearGradient
			id="ch-topdown35000i"
			x1={461.7923}
			x2={352.3403}
			y1={1685.1643}
			y2={1525.9702}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000i)"
			d="M350 227c7.1 4.9 88 63 88 63l59-39s-57 58.5-22.1 135.2c-44.2-31.1-96.3-66-102.9-79-6.7-13-14.2-22.6-22-80.2z"
			opacity={0.502}
		/>
		<path
			fill="#ccc"
			d="m319 40.2 30 95 99.9-49-62-81-67.9 35zm-8 207-74 65 28 58 97.9-23-51.9-100zm250.8-122 47 98 79-62-30-60-96 24z"
		/>
		<path fill="#999" d="m505.9 243.2-118.9-85 1 12 109.9 79 8-6z" />
		<linearGradient
			id="ch-topdown35000j"
			x1={430.3869}
			x2={817.7559}
			y1={1301.8463}
			y2={1689.2142}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000j)"
			d="M647 260.4c-25.4-21.1-43.3-40.7-84.2-40.2-20.7 3.4-29.5 3.4-58 25s-47.4 71-40 109c7.4 38 28.8 58.2 42 66 14.1 14.6 208.9 212 208.9 212l134.9-176c.1 0-208.9-200.3-203.6-195.8z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown35000k"
			x1={800.3592}
			x2={412.9912}
			y1={1371.7117}
			y2={1703.7266}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.5} stopColor="#666" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown35000k)"
			d="M647 260.4c-25.4-21.1-43.3-40.7-84.2-40.2-20.7 3.4-29.5 3.4-58 25s-47.4 71-40 109c7.4 38 28.8 58.2 42 66 14.1 14.6 208.9 212 208.9 212l134.9-176c.1 0-208.9-200.3-203.6-195.8z"
		/>
		<path
			fill="#999"
			d="M526.4 273.5c-7.8 5.2-16.6 5.7-19.7 1.1-3.1-4.6.8-12.5 8.6-17.8 7.8-5.2 16.6-5.7 19.7-1.1 3.1 4.6-.8 12.6-8.6 17.8z"
		/>
		<radialGradient
			id="ch-topdown35000l"
			cx={501}
			cy={1604}
			r={44}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown35000l)"
			d="M501 272c24.3 0 44 19.7 44 44s-19.7 44-44 44-44-19.7-44-44 19.7-44 44-44z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m237.1 311.2 75-64 11-33-7-12 14-9 19-56-31-98 69.9-34 62 83 109.9 37 98.9-23 29 59-80 62-1 7s22.4 14.3 52 41 193.9 186 193.9 186l-135.9 176s-212.8-215-216.9-219c-4.1-4-18.2-17.1-22-25-10.9-5.2-115.9-42-115.9-42l-97.9 23-27-59z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m448.9 88.2-99.9 48m-38 111 51 100m198.8-221 46 96m0 8c-12.5-7-62.5-21.7-103.9 17-41.4 38.7-51.8 97.4-25 143m36 38c-30.4-42.8 30.9-195 136.9-163m-102.9 30s-8.6-10.3-19.9-22.1m-13-16.4c-3.5-3.8-6.9-7.2-10.1-9.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M533.9 447.2c-10.7-28.6-10.2-86.5 29.4-126.6 27.7-28 64.6-47.6 111.5-33.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M526.4 273.5c-7.8 5.2-16.6 5.7-19.7 1.1-3.1-4.6.8-12.5 8.6-17.8 7.8-5.2 16.6-5.7 19.7-1.1 3.1 4.6-.8 12.6-8.6 17.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m498.9 248.2-120.9-85-48 30m57-37 2 14m117.9 73-119.9-86 65-42 118.9 86-132.9 89-120.9-88m193.8-46s-31.5 21.6-63.2 42.9m-11.3 7.3c-29.9 20.2-57.4 38.8-57.4 38.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M474.9 386.2s-82.6-58.2-90-65c-7.4-6.8-34.9-42.8-35-95" />
		<g>
			<path
				fill="#d9d9d9"
				d="m1680.9 311.2-74.9-64-11-33 7-12-14-9-19-56 31-98-69.9-34-62 83-109.9 37-98.9-23-29 59 80 62 1 7s-22.4 14.3-52 41-193.9 186-193.9 186l135.9 176s212.8-215 216.9-219c4.1-4 18.2-17.1 22-25 10.9-5.2 115.9-42 115.9-42l97.9 23 26.9-59z"
			/>
			<linearGradient
				id="ch-topdown35000m"
				x1={1462.9468}
				x2={1559.0488}
				y1={1685.7461}
				y2={1525.5521}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown35000m)"
				d="M1569 226c-7.1 4.9-89 64-89 64l-57-38s56.6 64 20.1 134.2c44.2-31.1 96.3-66 102.9-79 6.7-13 17.6-22.7 23-81.2z"
				opacity={0.502}
			/>
			<path
				fill="#ccc"
				d="m1599 40.2-30 95-99.9-49 62-81 67.9 35zm8 207 73.9 65-28 58-97.9-23 52-100zm-250.8-122-47 98-79-62 30-60 96 24z"
			/>
			<path fill="#999" d="m1412.1 243.2 118.9-85-1 12-109.9 79-8-6z" />
			<linearGradient
				id="ch-topdown35000n"
				x1={1107.8385}
				x2={1495.2075}
				y1={1656.2988}
				y2={1317.3029}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path
				fill="url(#ch-topdown35000n)"
				d="M1271 260.4c25.4-21.1 43.3-40.7 84.2-40.2 20.7 3.4 29.5 3.4 58 25s47.4 71 40 109c-7.4 38-28.8 58.2-42 66-14.1 14.6-208.9 212-208.9 212l-134.9-176c-.1 0 208.9-200.3 203.6-195.8z"
				opacity={0.6}
			/>
			<linearGradient
				id="ch-topdown35000o"
				x1={1138.4949}
				x2={1504.7069}
				y1={1365.9747}
				y2={1778.0146}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#666" />
				<stop offset={0.501} stopColor="#666" stopOpacity={0} />
			</linearGradient>
			<path
				fill="url(#ch-topdown35000o)"
				d="M1271 260.4c25.4-21.1 43.3-40.7 84.2-40.2 20.7 3.4 29.5 3.4 58 25s47.4 71 40 109c-7.4 38-28.8 58.2-42 66-14.1 14.6-208.9 212-208.9 212l-134.9-176c-.1 0 208.9-200.3 203.6-195.8z"
			/>
			<path
				fill="#999"
				d="M1391.6 273.5c7.8 5.2 16.6 5.7 19.7 1.1 3.1-4.6-.8-12.5-8.6-17.8-7.8-5.2-16.6-5.7-19.7-1.1-3.1 4.6.8 12.6 8.6 17.8z"
			/>
			<radialGradient
				id="ch-topdown35000p"
				cx={1417}
				cy={1604}
				r={44}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
			</radialGradient>
			<path
				fill="url(#ch-topdown35000p)"
				d="M1417 272c24.3 0 44 19.7 44 44s-19.7 44-44 44-44-19.7-44-44 19.7-44 44-44z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1680.9 311.2-74.9-64-11-33 7-12-14-9-19-56 31-98-69.9-34-62 83-109.9 37-98.9-23-29 59 80 62 1 7s-22.4 14.3-52 41-193.9 186-193.9 186l135.9 176s212.8-215 216.9-219c4.1-4 18.2-17.1 22-25 10.9-5.2 115.9-42 115.9-42l97.9 23 26.9-59z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1469.1 88.2 99.9 48m38 111-51 100m-198.8-221-46 96m0 8c12.5-7 62.5-21.7 103.9 17 41.4 38.7 51.8 97.4 25 143m-36 38c30.4-42.8-30.9-195-136.9-163m102.9 30s8.6-10.3 19.9-22.1m13-16.4c3.5-3.8 6.9-7.2 10.1-9.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1384.1 447.2c10.7-28.6 10.2-86.5-29.4-126.6-27.7-28-64.6-47.6-111.5-33.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1391.6 273.5c7.8 5.2 16.6 5.7 19.7 1.1 3.1-4.6-.8-12.5-8.6-17.8-7.8-5.2-16.6-5.7-19.7-1.1-3.1 4.6.8 12.6 8.6 17.8z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1419.1 248.2 120.9-85 48 30m-57-37-2 14m-117.9 73 119.9-86-65-42-118.9 86 132.9 89 120.9-88m-193.8-46s31.5 21.6 63.2 42.9c.3.7.8 11.1.8 11.1m10.5-3.8c29.9 20.2 57.4 38.8 57.4 38.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1443.1 386.2s82.6-58.2 90-65c7.4-6.8 34.9-42 35-94.2"
			/>
		</g>
		<path
			fill="#f2f2f2"
			d="M961 222c239.2 0 297 40 297 40l79-80 113 82 94 256-1 389-171 415-117 48-25-25-147 209H839l-146-207-26 23-116-47-172-416-1-389 94-256 113-82 79 80s57.8-40 297-40"
		/>
		<path
			fill="#d9d9d9"
			d="M675 276c15.6-9 26.6-41 285-41 47.2 0 233.8-1 287 39 4.9-4.8 10-12 10-12s-55.7-40-297-40c-72.5 0-244.8 6.5-296 41 2.8 3.8 7.2 9.1 11 13z"
		/>
		<path
			fill="#737373"
			d="M626 1037c-20-18.8-49.3-94.6-53-120-3.9-26.9-10-187.5-10-201s-2.4-206.1 6-272 16.4-76.3 24-82c-11 7.1-18.1 14.6-19 17s-20 149-20 149l1 387 56 138s10.5-11.3 15-16zm670-2c4.4 4.5 10.3 10.7 13 13 3.8-7.4 56-133 56-133l2-387-20-149-12-10s8.7 18.5 14 66c10.7 51.6 8 258.6 8 281 0 10-2.3 57.2-4 110-3.8 67.2 1.8 128.4-57 209z"
		/>
		<path fill="#e7e7e7" d="m1082 1556 126-607H711l129 607h242z" />
		<path
			fill="#d6d6d6"
			d="m1209 948 99 100-110 263 30 37-146 208 127-608zm-500 2 130 606-147-206 26-29-108-268 99-103z"
		/>
		<linearGradient
			id="ch-topdown35000q"
			x1={622.7792}
			x2={767.7792}
			y1={713.1188}
			y2={771.7028}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown35000q)" d="m755 1330-119-305-26 28 109 268 36 9z" />
		<linearGradient
			id="ch-topdown35000r"
			x1={1293.5906}
			x2={1150.5906}
			y1={721.8893}
			y2={780.0714}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown35000r)" d="m1166 1320 117-300 26 28-109 263-34 9z" />
		<linearGradient
			id="ch-topdown35000s"
			x1={701.747}
			x2={758.452}
			y1={599.3336}
			y2={536.3336}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#737373" />
			<stop offset={0.5} stopColor="#5e5e5e" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown35000s)" d="m720 1321 31 8-34 55-25-34 28-29z" />
		<linearGradient
			id="ch-topdown35000t"
			x1={1217.085}
			x2={1154.085}
			y1={595.0679}
			y2={544.6679}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.5} stopColor="#5e5e5e" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown35000t)" d="m1198 1312-31 7 40 60 23-31-32-36z" />
		<path
			fill="#dfdfdf"
			d="M668 1371 465 912V523l55-204 109-91-45-45-114 84-92 252 2 392 169 413 119 47zm585 0 203-459V523l-55-204-109-91 45-45 114 84 92 252-2 392-169 413-119 47z"
		/>
		<path
			fill="#b9b9b9"
			d="m1115 1398 27 26 10-38-30-26H798l-30 26 10 38 27-26h310zm25-120 27 26 10-38-30-26H773l-30 26 10 38 27-26h360zm27.9-112 27 26 10-38-30-26H745.5l-30 26 10 38 27-26h415.4z"
		/>
		<path
			d="m668 1371 51-50-164-406-1-387 20-151 107-93s-51.9-56.5-52-56-109 91-109 91l-55 205v388l203 459zm624-1143 109 91 55 206 1 387-204 460-53-61 166-397 1-386-20-150-107-94s12.5-14.6 25.8-28.8c12.7-13.8 26.2-27.2 26.2-27.2z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-topdown35000u"
			cx={958.543}
			cy={1356.031}
			r={613.237}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.203} stopColor="#000" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown35000u)"
			d="M1296 1035c10.2-13 43.4-64.7 50-110s11-169 11-211c0-42.1 8.4-299.7-26-351-45.3-37.2-92-79-92-79s4.4-7 7-10c-49.7-42.4-271.1-38.9-286-39-263.8.3-269.9 33.4-286 41 3.2 3.7 6 7.9 6 7.9l-107 95-20 150 1 386s25.3 62 55.9 137.7C655.3 1008 711 950 711 950l498-1s60.2 59.3 87 86z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M960 222c239.2 0 297 40 297 40l79-80 113 82 94 256-1 389-171 415-117 48-25-26-147 210H838l-146-207-25 23-118-48-171-415-1-389 94-256 113-82 79 80s57.8-40 297-40"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1248 274c-32.1-20.3-80.7-39-288-39s-243.8 13.8-285 39" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1081 1556 127-608m102 101-101-101H711l-100 104m229 504L711 950"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1115 1396 27 26 10-38-30-26H798l-30 26 10 38 27-26h310zm25-120 27 26 10-38-30-26H773l-30 26 10 38 27-26h360zm27.9-110 27 26 10-38-30-26H745.5l-30 26 10 38 27-26h415.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m862 1557 18-39h157l20 38" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M959.5 314c137.8 0 249.5 111.7 249.5 249.5S1097.3 813 959.5 813 710 701.3 710 563.5 821.7 314 959.5 314z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1260 260-20 24 107 95 20 150-1 386s-167.2 395.8-166 397 29 35 29 35"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1253 1370 203-459V524l-56-206-108-89" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1367 915 176-5m1-390-175 8m-22-149 104-114" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1296 1035c10.2-13 43.4-64.7 50-110s11-169 11-211c0-42.1 8.4-299.7-26-351M660 259.9l20 24-107 95-20 150 1 386s165.1 405.7 164 407c-1.2 1.3-26 28.1-26 28.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m667 1369.9-202.9-459v-387l56-206 108-89" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m553.1 914.9-175.9-5m-1.1-390 174.9 8m22.1-149-104-114" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M624 1034.9c-10.2-13-43.4-64.7-50-110-6.5-45.3-11-169-11-211 0-42.1-8.4-299.7 26-351"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M563.1 715.9H761m596 .1h-198" />
		<g id="Animation">
			<path
				fill="#f2f2f2"
				d="M1073.5 925.5h45s.3-18.6 0-33.5c123.4-57.4 203-182.5 203-327.5 0-199.4-161.6-361-361-361s-361 161.6-361 361c0 130.9 69.7 245.5 173.9 308.8 2.7 2 6.9 4.4 12.1 7.2.3 19.4 0 44 0 44h62l226 1z"
			/>
			<path
				d="M735.5 283.5c6.1-5.9 39.7-30.7 50-34v631c-10.2-4.1-48-30.4-50-34v-563zm384-43c6.1 2.5 45.7 23.9 50 29v594c-5.8 5-42.3 26.5-50 29v-652z"
				className="factionColorPrimary"
			/>
			<radialGradient
				id="ch-topdown35000v"
				cx={960.5}
				cy={1355.5}
				r={361}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.099} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#000" />
			</radialGradient>
			<path
				fill="url(#ch-topdown35000v)"
				d="M860.5 884.5h199s1 15.1 1 40.9c26.7.1 58 .1 58 .1s.3-18.6 0-33.5c123.4-57.4 203-182.5 203-327.5 0-199.4-161.6-361-361-361s-361 161.6-361 361c0 130.9 69.7 245.5 173.9 308.8 2.7 2 6.9 4.4 12.1 7.2.3 19.4 0 44 0 44h75v-40z"
				opacity={0.2}
			/>
			<path
				fill="#f2f2f2"
				d="M786.5 677.5c60.3 78.3 147.9 83 174 83s113.2-5.2 158-69c-.1 11.3 0 33 0 33s-46.7 64-158 64c-74.9 0-138.8-31.6-174-78-.4-16.6-.5-21.8 0-33z"
			/>
			<path
				fill="#f2f2f2"
				d="M1059 881H860v109l33 66-1 125h-10l-8 126h19l-1 126h-8l-10 127h19v189h136v-189h18l-10-127h-9v-126h19l-9-126h-10v-126l32-67-1-107z"
			/>
			<path fill="#e3e3e3" d="m1060 989-199 1s31.2 67 32 67 135-2 135-2l32-66z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1060.5 925.5h58s.3-18.6 0-33.5c123.4-57.4 203-182.5 203-327.5 0-199.4-161.6-361-361-361s-361 161.6-361 361c0 130.9 69.7 245.5 173.9 308.8 2.7 2 6.9 4.4 12.1 7.2.3 19.4 0 44 0 44h75"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1169.1 821c75-60.3 120.7-153.7 120.7-258.6 0-102.5-46.9-194.1-120.4-254.5m-50-33.9c-47.1-26-101.3-40.8-158.9-40.8-63.8 0-123.4 18.1-173.9 49.6m-50.4 38.6c-64.6 60.1-105 145.8-105 241 0 95.2 40.4 180.9 105 241"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M785.5 882.5v-207s54.1 84 175 84c115.8 0 155.5-65 158-71v204"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M785.5 678.3V249.5m-50 598.8V282.8m383 407.7v-449m50 622.6V268.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M785.5 708.5s52.1 79 173 79c115.8 0 157.5-60 160-66"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M816 901.5c8.6 0 15.5 1.6 15.5 3.5s-6.9 3.5-15.5 3.5-15.5-1.6-15.5-3.5 6.9-3.5 15.5-3.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1059 881H860v109l33 66-1 125h-10l-8 126h19l-1 126h-8l-10 127h19v189h136v-189h18l-10-127h-9v-126h19l-9-126h-10v-126l32-67-1-107z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M895 1560h133m-137-127h138m-135-126h135m-137-126c-.8.2 137 0 137 0m-135-125 134-1m31-66H860"
			/>
			<circle id="Animation-Coordinates" cx={960.5} cy={563.5} r={0.5} fill="red" />
		</g>
		<g>
			<radialGradient
				id="ch-topdown35000w"
				cx={235}
				cy={716}
				r={44.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#bfbfbf" />
			</radialGradient>
			<path
				fill="url(#ch-topdown35000w)"
				d="M235 1159.5c24.6 0 44.5 19.9 44.5 44.5s-19.9 44.5-44.5 44.5-44.5-19.9-44.5-44.5 19.9-44.5 44.5-44.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M215 1179.9c3.9 0 7.1 3.2 7.1 7.1s-3.2 7.1-7.1 7.1-7.1-3.2-7.1-7.1 3.2-7.1 7.1-7.1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M235 1159.5c24.6 0 44.5 19.9 44.5 44.5s-19.9 44.5-44.5 44.5-44.5-19.9-44.5-44.5 19.9-44.5 44.5-44.5z"
			/>
			<path fill="red" d="M210.5 1238.5s25.3.2 50 0c-3.5 3.2-11.5 8.4-24.7 8.5-8.1.1-15.5-.5-25.3-8.5z" />
		</g>
	</svg>
);

export default Component;

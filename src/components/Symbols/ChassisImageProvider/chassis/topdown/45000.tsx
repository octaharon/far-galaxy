import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#bfbfbf"
			d="M1280 1268c16.4 99-45.1 287-173 343-7.3 35.9-17.6 73.9-22 88-3.7-32.5-11.7-65.8-14-70-9.9 5.3-53.8 17.1-68 18-2.7 18.2-7 182-7 182h-73l-7-182s-38.2-4.4-67-17c-5.4 16.2-13.2 56.9-14 65-6.4-21.4-18.2-75.2-21-83-14.6-7.6-183.3-88.9-176-344-12.9-.2-30.5.5-31-14s12.3-15.5 15-16c-4.9-7.4-25-53.8-30-73-15.7 2.3-130.7 33.7-178-177 25.7 42.2 87.3 162.1 169 130-4.6-20.9-17-84-17-84s-23.7 10.4-30-10 21.5-25.2 25-26c-2.8-11.8-9-53.1-9-57-13.7.2-195.8 33-285-249 61.9 96.6 144.1 232.1 283 186 1.6-46.5 8.3-100.4 19-126-35.9-26.4-158.5-97.6-152-359 30.8 116.1 73.5 272.3 178 304 33.2-51.6 109.6-124.6 193-255 81.8-112.9 166-239.7 173-352 6.2 5.1 30.5 111.6 33 120 37.5 93.8 101.9 168.1 141 226 118.3 182.3 183.1 234.5 196 258 120.7-31.9 166-280.3 174-295 7.3 190.8-81.5 315.5-148 348 6.6 29.5 19.9 84.4 18 133 27 7.1 160.4 48.9 283-200-8.7 35.2-80.2 282.6-285 261-3.2 20.3-7.3 49.7-9 59 19 .6 37.8 40.7-6 38-3.4 32.1-14 75.7-15 83 82.8 28.2 130.4-88.9 162-124-7.5 30.3-42.9 190.2-173 170-6.3 22.1-17.4 54.3-28 71 7.8 6.6 26.3 35-24 30z"
		/>
		<path
			fill="#e6e6e6"
			d="M1009 1413c9.2 3.8 32.4 10.2 36 13 .9-28.1 0-211 0-211l-170-1-1 210 36-12c.6-21.9-4-160-4-160l106-3s-2.4 129.4-3 164z"
		/>
		<path
			fill="#d9d9d9"
			d="M1046 1422c.4-16.2-1-206-1-206l34 22s-19.6 3.1-18 18 7 13 7 13-22.4 169.2-22 153zm-171-207-1 194-22-139s15.2-13.9-1-33c13.1-12.4 24-22 24-22z"
		/>
		<path
			d="m922 1828 75 1 16-579-107 1 16 577zm97-615-11-33-95-1-8 34h114zm-207 54h21c3.3 0 10.4-6.5 10.4-15.5s-3.5-13.5-14.4-13.5-211 1-211 1-25.5 29 3 29h159s2.6-22 16-22 15 21 15 21m258-27h231c16.5 0 17.8 28 4 28h-162s-2.2-21-16-21-9.8 22-16 22h-40c-4.5 0-20.2-21.2-1-29z"
			className="factionColorPrimary"
		/>
		<path
			d="M790.3 1182.9c-77.1-43.6-135.3-119.8-162.4-194.4 76.6-45.7 77.8-147.1 98.3-250.8 13.8-53.1 37.6-104.1 83.9-142.7-23.3 12.8-70.5 52.8-94 108-40.4 94.9-37.2 208-100 244-3.3-15.8-5-31.3-5-46 0-42.6 4.3-102.2 27-158 40.7-99.8 128-195.5 275.8-212.4 39.8 89.3 21.2 110.7-10.8 174.4 84-44.9 52.1-155.9 44.1-176.8 4.6-.1 9.2-.2 13.9-.2 56.3 0 123.2 16 183.2 51.2-59.1 46.3-55.6 91.9-54.2 129.8 6.8 177.3-11.2 171.7-35 261 86.6-93.7 58.3-207.8 60-265s31.3-90.4 51.1-111.9c19.9 13.7 38.7 29.6 55.8 48 51.3 175.1 36.5 287.2-20.9 373-19.3 28.9-53.7 58.8-74 79 204.2-113.3 140.8-357.2 138.1-393.3 27.8 48.5 44.9 108.2 44.9 180.3 0 266.6-257 336-257 336s13.7 12 23 17c90.1-31.4 246.1-130.8 258-336 3.7-271.9-216.2-395.4-374-395-169.9-.8-387.5 145.1-374 398 0 97 81.9 282.3 261 333 6.3-5.5 13.5-10.7 20-20-12.5-2.9-24.7-6.6-36.6-11.1-9.5-32.6-33.5-113.3-24.4-138.9s86.2-72.5 77-171c-17.7 87.3-100.9 133.6-105 158s-5.6 48.5 12.2 132.8z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown45000a"
			x1={6446.9097}
			x2={6456.8374}
			y1={2887.4468}
			y2={2923.2188}
			gradientTransform="matrix(-0.8541 -0.5201 0.5201 -0.8541 5348.8735 6850.4141)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.4783} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#999" />
		</linearGradient>
		<path
			fill="url(#ch-topdown45000a)"
			d="M1319.2 990.6c17.9 2.8 74 8.9 66.4 32.1-7.5 23.2-42.7 7.6-76.9-2.8 3.2-12.6 10.5-29.3 10.5-29.3z"
		/>
		<linearGradient
			id="ch-topdown45000b"
			x1={568.0291}
			x2={577.9567}
			y1={-2364.1257}
			y2={-2328.3538}
			gradientTransform="matrix(1 0 0 1 0 3360)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.4783} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown45000b)"
			d="M610.7 1017.5c-16.8 6.9-67.8 30.9-73.5 7.1-5.6-23.8 32.6-28.7 67.1-37.6 4 12.4 6.4 30.5 6.4 30.5z"
		/>
		<radialGradient
			id="ch-topdown45000c"
			cx={6297.1021}
			cy={-7634.6724}
			r={269.253}
			gradientTransform="matrix(-0.1131 -0.9936 0.1684 -1.915962e-02 2819.5645 7489.3696)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.4482} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5336} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000c)"
			d="M837.5 1484.1c-8.8.6-20.9 3.9-27.5 10.1-9.4-26.8-33.6-105-29-234 .9-25.8 28.8-15.4 29.9 3.3.6 10.6.9 81.9 8.1 140.1 5.5 44.4 18.5 80.5 18.5 80.5z"
		/>
		<radialGradient
			id="ch-topdown45000d"
			cx={6221.9111}
			cy={-15791.5908}
			r={269.5458}
			gradientTransform="matrix(7.910110e-02 -0.9969 0.1267 1.005522e-02 2614.7122 7721.0073)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.4431} stopColor="#000" stopOpacity={0} />
			<stop offset={0.5436} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000d)"
			d="M1111.1 1496.7c-8.8-.8-19.6-16.1-27.1-10.9 27.6-72.1 24.5-135.4 27.1-222.8 5-25.4 31.3-20.3 29.4-1.6-1.1 10.6-1.2 42.3-3.3 100.9-1.5 44.6-26.1 134.4-26.1 134.4z"
		/>
		<linearGradient
			id="ch-topdown45000e"
			x1={539.3605}
			x2={602.7085}
			y1={-2336.9758}
			y2={-2355.5408}
			gradientTransform="matrix(1 0 0 1 0 3360)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.5} />
			<stop offset={0.2741} stopColor="#000" stopOpacity={0} />
			<stop offset={0.9208} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.4} />
		</linearGradient>
		<path
			fill="url(#ch-topdown45000e)"
			d="M610.7 1017.5c-16.8 6.9-67.8 30.9-73.5 7.1-5.6-23.8 32.6-28.7 67.1-37.6 4 12.4 6.4 30.5 6.4 30.5z"
		/>
		<linearGradient
			id="ch-topdown45000f"
			x1={6770.7148}
			x2={6846.2021}
			y1={2581.2888}
			y2={2560.4387}
			gradientTransform="matrix(-0.8038 -0.5949 0.5949 -0.8038 5291.6426 7130.5293)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.3973} stopColor="#000" stopOpacity={0} />
			<stop offset={0.8449} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown45000f)"
			d="M1321.6 988.2c17.6 4.4 72.9 15.6 63.3 38-9.6 22.4-43.2 3.7-76.3-9.7 4.3-12.3 13-28.3 13-28.3z"
		/>
		<radialGradient
			id="ch-topdown45000g"
			cx={6561.4019}
			cy={-6899.9971}
			r={190.1325}
			gradientTransform="matrix(-0.1878 -1.004 0.1465 -2.567940e-02 3317.8889 8045.1973)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.9} />
			<stop offset={0.8417} stopColor="#eaeaea" stopOpacity={0.03266332} />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000g)"
			d="M1086.1 1692.2c7.1-35.4 25-75.9 23.4-141.3-.4-17.1-3.3-47.7-33.3-46.2s-23.1 43.6-15.7 74.6c6.9 29.2 25.6 112.9 25.6 112.9"
		/>
		<radialGradient
			id="ch-topdown45000h"
			cx={5705.8965}
			cy={-16273.7539}
			r={191.7785}
			gradientTransform="matrix(0.1912 -0.9816 0.1387 2.700777e-02 2021.2302 7649.8105)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000h)"
			d="M833.9 1688.5c6.7-34.7 35.4-124.7 35-141.4-.3-16.7 1.1-44.9-27.3-43.5-28.4 1.4-32.9 44.9-29.7 74.1 3.3 29.2 22 110.8 22 110.8"
		/>
		<linearGradient
			id="ch-topdown45000i"
			x1={1079.7968}
			x2={1079.7968}
			y1={-1873.9119}
			y2={-1800.4}
			gradientTransform="matrix(1 0 0 1 0 3360)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown45000i)"
			d="M1112.7 1559.6c13-24 20.3-74.4-36.8-73.5-57.2.9-36.6 70.9-23.6 69.2 0-10.3-6.9-45.7 23.9-50.7 29.8-4.7 36.5 55 36.5 55"
		/>
		<linearGradient
			id="ch-topdown45000j"
			x1={841.5596}
			x2={841.5596}
			y1={-1874.3062}
			y2={-1804.6}
			gradientTransform="matrix(1 0 0 1 0 3360)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown45000j)"
			d="M869.6 1555.4c9.1-4.9 29.9-52.1-15.9-66.9s-62.5 32.3-47.5 62.8c5.1-19.8 13.3-56.5 44.8-47.7 31.4 8.7 18.6 51.8 18.6 51.8z"
		/>
		<radialGradient
			id="ch-topdown45000k"
			cx={958.0825}
			cy={2833.8093}
			r={327.4587}
			gradientTransform="matrix(1 0 0 -1 0 4160)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.693} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.3} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000k)"
			d="m1011.4 1415.8 36.4 13.8 23-159.8h35.9s5.9 160.3-26.9 214.6c-36.5 8.5-61.8 37.3-25.7 78.4 8.2 32.4 12.9 59 14.5 65.2-12.6 5.4-55.2 17-65.3 17.9 1.8-40.8 8.1-230.1 8.1-230.1zm100.9 77.3c18.2 32.5 8.2 53.2-.9 65.7 2.1 10-1.8 49.7-1.8 49.7 89.1-50.4 133-129.7 152.8-200 21-74.6 15.2-138.7 15.2-138.7h-135.3c-.1 0 1.9 145.9-30 223.3zM873 1426c-4.4-11.1-22-145-23.3-156.4-2.7-2.8-20.6-4.8-34.7-2.2-6.9 44.7-3.1 150.6 24.1 215.4 65.2 15.2 38 69.8 29.9 74.9-2.9 3.8-11.9 46.9-17.9 71 4.8 6.9 43.8 14.8 62.8 19.7 2.9-14.1-3.1-202.4-4.1-235.4-.1 0-29 8.7-36.8 13zm-92.2-156.2c-16.9-3.1-124.7-6.8-140.7 0 0 15.6-13.7 244.1 171.8 339.3-1.9-26.7-2.8-46-2.2-50.2-6.4-7.9-21.3-32.9.3-64.7-22.5-62-29.2-224.4-29.2-224.4z"
		/>
		<radialGradient
			id="ch-topdown45000l"
			cx={960.1257}
			cy={3284.3093}
			r={356.2427}
			gradientTransform="matrix(1 0 0 -1 0 4160)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4545} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.3} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000l)"
			d="M875 1209.9h28.7l6.5-33.2h100.5l10.1 33.2 30.8 4.1s124.3-37.5 170.8-108.2c14.6-17.4 92.3-110 86.2-223.6-.8-14.2-2.8-178.8-108.8-256.1-30.6-30.4-110.3-104.2-256.2-100.2-216.8 16.8-302.1 189.8-314.3 229.8s-37.9 149.3 9.5 251.4c19.6 58.6 95.3 175.5 228.1 204l8.1-1.2z"
		/>
		<radialGradient
			id="ch-topdown45000m"
			cx={1552.2894}
			cy={1198.5515}
			r={145.568}
			gradientTransform="matrix(0.9614 -0.2753 -0.9064 -3.1648 313.5581 5333.4688)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.224} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.6} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000m)"
			d="M595.9 1166.7c3.6 14.2 16.2 61.4 27.8 69.1h209l4.8-2.8s-151.4-47.6-225.5-212.1l-43.1 16.1 16.6 81.2s27.2-14.6 41.7 10.2c14.6 24.8-31.3 38.3-31.3 38.3z"
		/>
		<radialGradient
			id="ch-topdown45000n"
			cx={5137.9468}
			cy={929.7807}
			r={151.6583}
			gradientTransform="matrix(-0.9614 -0.2753 0.9064 -3.1648 5298.4805 5471.1841)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.224} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.6} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000n)"
			d="M1332 1166.7c-3.6 14.2-16.2 61.4-27.8 69.1-34.6 0-209 6-209 6l-16.8-9.8s156.5-49 230.6-213.5l50 18.5-16.6 81.2s-31.2-14.6-45.7 10.2c-14.6 24.8 35.3 38.3 35.3 38.3z"
		/>
		<radialGradient
			id="ch-topdown45000o"
			cx={3977.176}
			cy={-642.2347}
			r={129.3578}
			gradientTransform="matrix(-0.9994 -3.483332e-02 -8.476196e-02 2.4319 4600.9741 2575.5449)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.6251} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.6} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000o)"
			d="M560.2 997.5c13.8-4.4 35.8-11.5 39.9-12.6-5.3-15.7-33.9-129.8 10.6-248.3-4.7 0-5.3.2-7.2 2.3-1.8 2.1-5 20.9-33 14.4-5.7 19-18.3 70.6-17.6 123.1 17 1.1 25.7 5.7 28.9 29.6 3.2 23.8-18.7 33.8-27.9 36.2 4.9 19.3 6.3 55.3 6.3 55.3z"
		/>
		<radialGradient
			id="ch-topdown45000p"
			cx={1772.7395}
			cy={-688.6061}
			r={131.0406}
			gradientTransform="matrix(0.9994 -3.483332e-02 8.476196e-02 2.4319 -474.2574 2612.0933)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.6251} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.6} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000p)"
			d="M1363.1 998c-13.8-4.4-41.8-11.5-45.9-12.6 5.3-15.7 39.9-129.8-4.6-248.3 4.7 0 5.3.2 7.2 2.3 1.8 2.1 5 20.9 33 14.4 5.7 19 21.3 70.6 20.6 123.1-17 1.1-28.7 5.7-31.9 29.6s18.7 33.8 27.9 36.2c2.3 25.4-6.3 55.3-6.3 55.3z"
		/>
		<radialGradient
			id="ch-topdown45000q"
			cx={2203.0894}
			cy={2473.5276}
			r={189.4737}
			gradientTransform="matrix(0.8617 -0.5074 -0.9812 -1.6664 1089.7412 5793.6802)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3519} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.5832} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000q)"
			d="M598.1 738.6c4.3-12.2 13.6-30.1-16.3-44-29.9-13.8-114.2-57-164.7-308.2-6.2 123.5 33.6 313.4 150 360 22.5 13.6 31-7.8 31-7.8z"
		/>
		<radialGradient
			id="ch-topdown45000r"
			cx={5677.8628}
			cy={1633.8949}
			r={189.4737}
			gradientTransform="matrix(-0.8617 -0.5074 0.9812 -1.6664 4647.6943 6157.6118)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3519} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.5832} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000r)"
			d="M1321.3 738.6c-4.3-12.2-13.6-30.1 16.3-44 29.9-13.8 114.2-57 164.7-308.2 6.2 123.5-33.6 313.4-150 360-22.6 13.6-31-7.8-31-7.8z"
		/>
		<radialGradient
			id="ch-topdown45000s"
			cx={5051.5054}
			cy={451.7707}
			r={499.2414}
			gradientTransform="matrix(-0.8188 -0.5741 -1.3969 1.9924 5301.2925 2343.5024)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5211} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.6565} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000s)"
			d="M593.3 696c14.7-23.6 98.3-117.5 115.8-141.5 8.3-11.4 60-80.1 116.5-164.4C893.8 301.2 955 185.2 963.1 89.6c0 0 3.7 397 4 410.7-180-.6-317.1 126.2-355.8 233.9l-18-38.2z"
		/>
		<radialGradient
			id="ch-topdown45000t"
			cx={2532.8384}
			cy={-307.9298}
			r={503.69}
			gradientTransform="matrix(0.8188 -0.5741 1.3969 1.9924 -248.2276 2415.8809)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5211} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.6565} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000t)"
			d="M1336.3 696c-14.7-23.6-98.3-117.5-115.8-141.5-8.3-11.4-60-80.1-116.5-164.4-68.2-88.9-129.4-204.9-137.5-300.5 0 0-3.2-3.1-3.3 10.5 0 3.1.1 6.4.1 10.1 0 8.2.2 17.9.3 28.6v4.6c0 5.7.1 11.7.2 17.9.1 9.5.2 19.6.3 30.1 0 5.5.1 11 .2 16.7-.1 120.8-1.5 283.8-1.7 292.3 180-.6 309.5 138.6 348.2 246.3l5.8-16.1 1.1-20.9 18.6-13.7z"
		/>
		<radialGradient
			id="ch-topdown45000u"
			cx={3387.7168}
			cy={2092.7886}
			r={248.9866}
			gradientTransform="matrix(0.581 -0.8139 -1.574 -1.1235 1764.175 5890.3687)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2914} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.4026} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000u)"
			d="M546.6 880.9c-45.7 10.6-166.1 29.2-280.1-196 26.9 148.7 152.7 269.6 285.4 253.4 34.7-4.2 27.5-28.2 26.9-36.6-6.4-31.8-32.2-20.8-32.2-20.8z"
		/>
		<radialGradient
			id="ch-topdown45000v"
			cx={5932.1699}
			cy={745.9841}
			r={249.004}
			gradientTransform="matrix(-0.581 -0.8139 1.574 -1.1235 3754.8416 6448.1909)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2914} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.4026} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000v)"
			d="M1374.2 880.9c45.7 10.6 166.1 29.2 280.1-196-26.9 148.7-152.7 269.6-285.4 253.4-34.7-4.2-27.5-28.2-26.9-36.6 6.4-31.8 32.2-20.8 32.2-20.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1279.4 1266.6c1 11.5 1.6 23.2 1.6 35 0 135.9-71.1 253.7-175 311.7m-37.4 17.3c-20.1 7.6-43.8 14.4-66.1 17.8m-86.1 0c-22.8-3.4-45.1-9.3-65.4-17m-37.2-17.6c-104.3-57.9-175.8-176-175.8-312.2 0-11.8.5-23.6 1.6-35.1"
		/>
		<linearGradient
			id="ch-topdown45000w"
			x1={851.0902}
			x2={850.7637}
			y1={-975.7599}
			y2={-996.8708}
			gradientTransform="matrix(1 0 0 1 0 2240)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.004994023} stopColor="gray" />
			<stop offset={0.5292} stopColor="#fff" />
			<stop offset={1} stopColor="gray" stopOpacity={0.3} />
		</linearGradient>
		<path
			fill="url(#ch-topdown45000w)"
			d="M841.4 1239.7c9.1.7 20.4 3 19.6 14.8s-13.4 12.2-20 11.7c2.4-13.2.4-26.5.4-26.5z"
		/>
		<linearGradient
			id="ch-topdown45000x"
			x1={3638.3164}
			x2={3637.99}
			y1={-975.0573}
			y2={-996.168}
			gradientTransform="matrix(-1 0 0 1 4710.8262 2240)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.004994023} stopColor="gray" />
			<stop offset={0.5292} stopColor="#fff" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-topdown45000x)"
			d="M1082.2 1240.4c-9.1.7-20.4 3-19.6 14.8s13.4 12.2 20 11.7c-2.4-13.2-.4-26.5-.4-26.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m905 1212 8-33 96 1 9 34" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M922 1829h75l17-580-108 1 16 579z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1303.1 1240.5c11.9-21 21-41.9 29.7-73.3m10.7-45.3c4.9-23.4 9.9-51.5 15.5-86m5.5-33.9c1.5-9.8 3.1-20 4.7-30.7 1.1-7.5 2.7-18.7 4.1-32.7m2.7-54.9c.1-40.6-4.4-88.8-20-135.1m-23.8-52.5c-8.7-14.7-18.9-28.7-31-41.6-105.1-112.2-135.1-176.2-186.1-246.9-56.5-78.5-129-164.2-147.1-308-2.7-11-6-12.6-6-12.6s-2.7 1.9-4 13c-18.2 143.8-90.5 229.5-147 308-50.9 70.8-81 134.7-186 247-12 12.8-22.2 26.7-30.7 41.2M569.9 750c-15.1 45-19.6 91.9-19.8 131.6m2.8 57.4c1.4 14.1 2.9 25.5 4.1 33 1.5 9.5 2.8 18.6 4.2 27.4m5.3 33.3c5.6 34.4 10.6 62.5 15.4 86m10.6 46.1c8.7 32.5 17.8 53.9 29.8 75.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1075.1 1233c150.3-48.5 258.9-189.6 258.9-356 0-206.5-167.4-374-374-374S586 670.5 586 877c0 168.2 111 310.4 263.7 357.5 13-12.7 24.3-21.5 24.3-21.5l171 1s20.6 13.4 30.1 19z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M872 1215s-8.1-2.1-14.7-4.3c-142.8-44-246.6-177-246.6-334.3 0-193.2 156.6-349.7 349.8-349.7s349.8 156.6 349.8 349.7c0 155.6-101.6 287.5-242.2 332.9-8.3 2-21.1 6.7-21.1 6.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M851.5 1266.8c6 41.8 16.7 116.2 22.5 157.2 12.6-5.4 24.6-9.4 36.2-12.2m99.5 2c12.7 3.4 24.8 7.8 36.3 12.2 8.3-65.2 16.8-124.3 22-159.4m-23-51.6v207m-170-209v205"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M833 1203c-10.8-25.1-41-124.3-24-147s86.1-71.7 75-186c-12.9 43.1-4.2 62.7-59 126-19.1 22-43.3 30-50 61-8.5 39.4 11 110.7 21 128m-99-42 14-21m-5-7 11-11m13 12-10 15"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m710 1097 29 26 9-26-20-16-18 16z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M629 989c132.9-95 26.8-290.6 205.3-410.2 1.9-1.3 4.2-3.2 5.7-4.8-197.8 104.1-119.2 291.3-223 375"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M912 531c13.8 11.3 66.8 140.9-48 190 1.7-.1 4.3-.7 5.5-1 114.8-33.9 97.2-157.2 73.5-195m203 57c-86.7 56.1-48.1 137.9-56.5 250.6-3.6 48.3-18.4 102.3-51.5 162.4 165.2-193.1 2.8-274.1 127-400m52 45c10.1 31.5 122.2 307.3-105 459 79.5-29.9 211.6-152.6 150-400"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M732 948c7-8.4 40.2-30.2 55-9s-21.6 38.4-25 56 1.7 40.6-23 37c-19.3-2.8-8-19.1-12-37-2.2-9.7-12.7-25.9 5-47z"
			opacity={0.6}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M858.3 753.7c12.5 3.1 33.2 17.3 25 33s-34.7 8-43 14-13.3 25.1-35 23-21.8-18.5-30-25-32.1-4.5-20-26c9.6-17.1 43.9-7.9 55-10s35.5-12.1 48-9z"
			opacity={0.6}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M984 952c4.2 13.4 35.9 21.8 31 44s-15.9 25.8-34 15-21.5-39-31-49-20.6-29.6-4-42 27.9-.6 38 32z"
			opacity={0.6}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1072 1175c-5.2-11.5-1-29.4 14-37s27.3 1.3 37-3 27.2-18.8 36-5-23.3 35.1-42 43-39.8 13.5-45 2z"
			opacity={0.6}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M835 1696c3.6-28.6 25.3-117 30-134s10.9-52.1-20-58-52.3 40.5-10 192z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M868 1558c16-4.5 33.5-62.3-19-72-50-9.2-62.3 52.9-39 74"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M839 1486c-12.2-29.8-28-64.4-28-208 0-14.4-.1-32-15-32s-15 11.8-15 39 .3 146 32 211"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M813 1267h33s15 1.9 15-14-17.3-14-24-14H625s-18-2-18 17c0 11.3 12.1 10 17 10 3.6 0 103.3.5 156.4.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M836.7 1239.7c7.1 7.5 8.1 12.6 1.7 27.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1084.9 1267.1c-6.5-14.8-5.5-19.9 1.7-27.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M580 1120c12.5-3.3 35.9-16.1 46 9s-22.2 32-34 35-130.4 43.4-182-188c40.2 69.7 85.1 169.5 170 144z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M602 987c-9.8 3.1-43.8 12.2-50 14s-21.2 8.2-16 25c5.2 16.8 23 10.1 30 8s46-16 46-16"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M551 939c9.3-1.2 32.8-6.7 29-35s-19.8-28.2-32-24-165 62.2-290-215c20.4 112.4 123.3 296 293 274z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M592 697c-38.6-21.3-122.1-44.9-177-326-9 211.5 73.7 337.3 153 378 16.5 8.1 27.8 2 34-13 5.1-12.5 2.8-30.8-10-39z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1086.2 1696.7c-3.6-28.6-25.3-117-30-134s-10.9-52.1 20-58c30.8-5.9 52.2 40.5 10 192z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1053.2 1558.7c-16-4.5-33.5-62.4 19-72 50-9.2 65.5 48.6 39 73"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1082.2 1486.7c12.2-29.8 28-64.5 28-208 0-14.4.1-32 15-32s15 11.8 15 39-.3 146-32 211"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1108.1 1267.7h-33s-15 1.9-15-14c0-16 17.3-14 24-14H1296s18-2 18 17c0 11.3-12.1 10-17 10-3.6 0-103.3.5-156.3.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1341 1120.6c-12.4-3.3-35.8-16.1-46 9-10.1 25.1 22.2 32 34 35s130.4 43.4 181.9-188c-40.1 69.7-85.1 169.6-169.9 144z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1319 987.6c9.8 3.1 43.8 12.2 50 14s21.2 8.2 16 25c-5.2 16.8-23 10.1-30 8s-46-16-46-16"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1370 939.6c-9.2-1.2-32.8-6.7-29-35s19.8-28.2 32-24 164.9 62.2 289.8-215c-20.3 112.4-123.2 296-292.8 274z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1329 697.5c38.6-21.3 122-44.9 176.9-326.1 9 211.5-73.7 337.4-152.9 378.1-16.5 8.1-27.8 2-34-13-5.1-12.4-2.8-30.8 10-39z"
		/>
		<radialGradient
			id="ch-topdown45000y"
			cx={2953.0491}
			cy={2428.3713}
			r={188.4141}
			gradientTransform="matrix(0.7139 -0.7002 -1.0378 -1.0581 946.6237 5673.46)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.368} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.4669} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000y)"
			d="M625.7 1127.5c-18.2-29.6-52.3 2.4-81.3-2.4s-79.2-32.2-129-139.7c11.4 78.4 64.7 199.9 167.2 180.5 57.3-7.4 43.1-38.4 43.1-38.4z"
		/>
		<radialGradient
			id="ch-topdown45000z"
			cx={6241.2539}
			cy={914.272}
			r={188.4141}
			gradientTransform="matrix(-0.7139 -0.7002 1.0378 -1.0581 4892.4863 6376.0464)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.368} stopColor="#000" stopOpacity={0.6} />
			<stop offset={0.4669} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown45000z)"
			d="M1294.6 1129.7c18.2-29.6 52.3 2.4 81.3-2.4s79.2-32.2 129-139.7c-11.4 78.4-64.7 199.9-167.2 180.5-57.3-7.4-43.1-38.4-43.1-38.4z"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1246.2,418 1232.6,421 1246.2,423.3 1259.7,418"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="976.2,436.9 962.6,440 976.2,442.3 989.7,436.9"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="739.9,230.8 726.8,226 737,235.3 751.2,238.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="627.3,554.4 617.1,563.8 630,559 639,547.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="648.3,1558.7 638,1568.1 650.9,1563.3 659.9,1551.9"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1044.3,1044.5 1034,1053.9 1047,1049.1 1056,1037.7"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="870.5,1141.5 856.7,1139.4 868.6,1146.5 883.1,1146.4"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1505.9,1168.8 1495.6,1178.3 1508.5,1173.4 1517.5,1162"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1401.3,554.4 1391,563.8 1403.9,559 1413,547.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="285.4,1074.4 275.2,1083.8 288.1,1079 297.1,1067.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="169.8,780.2 156,777.4 166.8,786.1 181.8,786.3"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1207.4,1740.1 1197.2,1749.6 1210.1,1744.7 1219.1,1733.4"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1185.9,1310.7 1175.6,1320.1 1188.5,1315.3 1197.5,1303.9"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="980.1,1294.5 966.8,1290.1 977.3,1299 991.6,1301.5"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="739.8,865.8 726.6,861.3 737.1,870.3 751.4,872.8"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="373,605.8 359.8,601.4 370.3,610.4 384.6,612.8"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1027.2,347.3 1014,342.8 1024.5,351.8 1038.8,354.3"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1369.7,700.6 1356.5,696.2 1367,705.2 1381.3,707.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1482.3,801.2 1469.2,806.1 1483,806.5 1495.7,799.4"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1272.9,1582.7 1259.7,1578.2 1270.2,1587.2 1284.5,1589.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1238.3,1402 1225.1,1397.6 1235.6,1406.6 1249.9,1409"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="623.1,1773.7 609.9,1769.2 620.4,1778.2 634.7,1780.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1365.3,202 1352.1,197.6 1362.6,206.6 1376.9,209"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="553.7,335.9 540.5,331.4 551,340.4 565.3,342.8"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="232.4,271 219.1,266.6 229.6,275.6 243.9,278"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="647.9,1357.6 634.7,1353.2 645.2,1362.2 659.5,1364.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="356.4,1647.1 343.2,1642.7 353.6,1651.7 368,1654.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="416,1372.2 417.1,1386.2 421.3,1373 417.9,1358.9"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="736.4,1437.3 737.4,1451.2 741.6,1438 738.3,1423.9"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1146.8,147.6 1147.9,161.5 1152.1,148.4 1148.7,134.2"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1739.3,929.7 1740.4,943.6 1744.5,930.5 1741.2,916.4"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1019,1864.8 1020.1,1878.8 1024.3,1865.6 1020.9,1851.5"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="272.6,1295.5 273.6,1309.4 277.8,1296.2 274.5,1282.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="483.1,781.2 484.1,795.1 488.3,782 485,767.9"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="609.4,100.4 610.4,114.3 614.6,101.1 611.3,87"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1684.3,231.8 1685.3,245.7 1689.5,232.5 1686.2,218.4"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1542.8,1448.7 1528.9,1449.9 1542.1,1454 1556.2,1450.5"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1409.8,1334 1411.1,1347.9 1415.1,1334.7 1411.5,1320.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1062.8,1587 1048.9,1588.2 1062.1,1592.3 1076.2,1588.8"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="781.3,1753 767.4,1754.2 780.6,1758.3 794.6,1754.8"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="550.4,1269.8 536.5,1271 549.7,1275.1 563.7,1271.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="488.6,999.4 474.7,1000.6 487.9,1004.6 502,1001.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="979.5,809.7 965.6,810.9 978.8,815 992.9,811.5"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1186.8,718.4 1172.9,719.6 1186.1,723.6 1200.2,720.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1673.8,474.5 1659.9,475.7 1673.1,479.7 1687.2,476.2"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="1552.2,301.8 1546.1,301.9 1551.8,304.3 1559,304.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="863.6,428.9 857.6,429 863.3,431.3 870.4,431.2"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="941,277 937.1,272.3 938.9,278.3 943.5,283.7"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="534.6,700.7 528.5,700.8 534.2,703.1 541.4,703"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="659.6,428.9 653.5,429 659.2,431.3 666.4,431.2"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="439.4,206.3 436.2,201.1 437.1,207.2 440.8,213.3"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="340.5,735.9 334.4,736 340.2,738.3 347.3,738.2"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="296.8,914 290.7,914 296.4,916.4 303.6,916.3"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="1186.2,888.2 1180.1,888.3 1185.9,890.6 1193,890.5"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="1095.8,1160.8 1089.7,1160.9 1095.5,1163.3 1102.6,1163.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="1186.2,1484.9 1180.1,1484.9 1185.9,1487.3 1193,1487.2"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="1698,1577 1692,1577.1 1697.7,1579.4 1704.8,1579.3"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="403.2,1537.8 397.1,1537.9 402.9,1540.2 410,1540.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="480,1717.8 475.7,1722.1 481.4,1719.8 486.4,1714.7"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="370.9,1181.3 368,1186.6 372.8,1182.8 376.2,1176.5"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="544.8,914.9 541.9,920.3 546.7,916.4 550.1,910.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="873.8,582.6 870.9,587.9 875.8,584.1 879.1,577.8"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="1490.4,89.7 1487.4,95.1 1492.3,91.2 1495.7,84.9"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="1055.6,98.9 1049.6,97.9 1054.8,101.2 1061.9,102.3"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="1083.4,576.5 1077.4,575.6 1082.7,578.9 1089.7,580"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="966.9,1792.9 960.9,1791.9 966.2,1795.2 973.3,1796.3"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={3}
			points="88.1,915.2 82.1,914.2 87.3,917.5 94.4,918.6"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1502,1683.2 1488.1,1684.4 1501.3,1688.5 1515.3,1685"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1246.4,1193.4 1232.5,1194.6 1245.7,1198.6 1259.8,1195.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="649.2,1196 635.3,1197.2 648.5,1201.3 662.6,1197.8"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="818.8,52.4 804.9,53.6 818.1,57.7 832.2,54.2"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1800.6,620.4 1786.7,621.6 1799.9,625.6 1814,622.1"
		/>
		<polygon
			fill="#fff"
			stroke="#030303"
			strokeMiterlimit={10}
			strokeWidth={5}
			points="1718.7,1306.8 1704.8,1308 1718,1312 1732.1,1308.5"
		/>
		<radialGradient id="ch-topdown45000A" cx={966.8} cy={867.5983} r={867.5983} gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a1a1a" stopOpacity={0.8} />
			<stop offset={1} stopColor="gray" stopOpacity={0} />
		</radialGradient>
		<circle cx={966.8} cy={867.6} r={867.6} fill="url(#ch-topdown45000A)" />
	</svg>
);

export default Component;

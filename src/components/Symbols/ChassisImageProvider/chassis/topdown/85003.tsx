import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M959.6 1620.2c-38.8 0-75.1-57.1-111.2-350.2-65.6-43-275.3-193.8-275.3-193.8l-2.6 326.9-116.3-42.6S229.9 928.9 215 899.1C191.8 875 9.5 673 9.5 673v-60.7h28.4L212.4 633l174.5-64.6 179.7 3.9 192.6 56.8 19.4-218.4s36.3-16.4 89.2 0c2.9 30.7 5.2 51.7 5.2 51.7s84.8-68.3 173.2 5.2c3.2-30.9 5.2-53 5.2-53s40.2-19.7 90.5-2.6c6.5 79.8 19.4 218.4 19.4 218.4l179.7-56.9 191.3-5.2 171.9 62 171.9-16.8 28.4-1.3v69.6L1706 896l-242.3 464.5-124.1 37.5 2.6-317.9-272.8 187.4c.1-.1-21.1 352.7-109.8 352.7z"
		/>
		<path fill="#ccc" d="m1881.3 613.6-177.1 18.1 2.6 262.3 175.8-187.4-1.3-93z" />
		<path fill="#ccc" d="m34 613.6 182.3 18.1-3.9 266.2L34 709.2v-95.6z" />
		<linearGradient
			id="ch-topdown85003a"
			x1={816.0975}
			x2={1105.9059}
			y1={889.1204}
			y2={894.1786}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8f9bb0" />
			<stop offset={0.399} stopColor="#dae3f2" />
			<stop offset={0.604} stopColor="#dae3f2" />
			<stop offset={1} stopColor="#8f9bb0" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85003a)"
			d="M959.6 435.3c-28.8 0-90.7 13.7-112.5 68.5-21.8 54.8-31 192-31 246.8 0 54.8 4.6 216.6 10.3 301.1 5.7 84.5 25.7 284.3 41.4 356.6 15.7 72.3 36.9 213.2 91.8 213.2s86.8-152.8 95.6-211.9c6.7-44.5 32.2-294.3 36.2-351.5 4-57.2 14.2-235.3 14.2-306.2s-12.1-223.1-37.5-261c-31.9-44.5-79.8-55.6-108.5-55.6z"
		/>
		<path
			fill="#d9d9d9"
			d="M821.3 638.2c.8-2 25.3 22.8 33.6 41.3.2 35.3 1.3 244.2 1.3 244.2S827.9 947 822.6 987c-6.7-70.5-11.1-324-1.3-348.8zm275.3 0c-7.4 5.2-28.6 28.1-31 38.8-.9 25.8-2.6 245.5-2.6 245.5s25.5 24.9 31 64.6c4.3-68.7 10-354.2 2.6-348.9z"
		/>
		<linearGradient
			id="ch-topdown85003b"
			x1={959.585}
			x2={959.585}
			y1={258.46}
			y2={306.27}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85003b)"
			d="M937.6 1613.7v47.8h44v-47.8s-9.8 6.5-20.7 6.5c-11.1 0-23.3-6.5-23.3-6.5z"
		/>
		<path
			d="m907.9 1162.8 2.6-714.6s21.8-12.9 50.4-12.9 44.2 5 49.1 11.6c-.3 27.4 1.3 714.6 1.3 714.6s-32.3-14.1-38.8-12.9c-6.5 1.2-1.3 0-1.3 0-2.5-1.1-1.3 19.4-1.3 19.4l-18.1-1.3-1.3-18.1s-32.5 3.7-42.6 14.2z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-topdown85003c"
			x1={959.585}
			x2={959.585}
			y1={819.219}
			y2={1551.869}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.749} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85003c)"
			d="m907.9 1162.8 2.6-714.6s21.8-12.9 50.4-12.9 44.2 5 49.1 11.6c-.3 27.4 1.3 714.6 1.3 714.6s-32.3-14.1-38.8-12.9c-6.5 1.2-1.3 0-1.3 0-2.5-1.1-1.3 19.4-1.3 19.4l-18.1-1.3-1.3-18.1s-32.5 3.7-42.6 14.2z"
			opacity={0.2}
		/>
		<path
			fill="#f1f1f1"
			d="M817.4 899.2 745 817.8l14.2-190L778.6 412s44.7-19.3 89.2 0c1.6 15.7 2.9 24.4 5.2 53-65.3 42.1-56.9 339.3-55.6 434.2zm281.8 0 76.1-81.8-14.2-190-19.3-213.3s-44.6-19.3-88.9 0c-1.6 15.7-2.9 24.4-5.1 53 66.9 51.5 54.2 335.3 51.4 432.1z"
		/>
		<path fill="#d9d9d9" d="m816.1 817.8-68.5 5.2 67.2 71.1 1.3-76.3zm287 1.2 67.2 1.3-72.4 76.2 5.2-77.5z" />
		<linearGradient
			id="ch-topdown85003d"
			x1={1053.9574}
			x2={1141.8594}
			y1={1469.974}
			y2={1471.509}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.003} stopColor="#ccc" />
			<stop offset={0.503} stopColor="#f1f1f1" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85003d)"
			d="M1136.7 419.8c-4.6-2.9-18.4-7.8-34.3-8.8-14.7-.9-32.3 1.6-44.6 7.5-2.4 19.3-3.9 45.2-3.9 45.2s13.1 14.4 16.8 23.3h71.1s-3-46.6-5.1-67.2z"
		/>
		<linearGradient
			id="ch-topdown85003e"
			x1={743.4318}
			x2={872.6998}
			y1={1315.9071}
			y2={1299.0491}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85003e)"
			d="M854.9 487s9.7-17.3 18.1-22c-1.8-16-3.7-38.9-5.2-53-4.6-2.9-23.6-6.5-39.4-7.5-14.7-.9-36.2 1.6-48.5 7.5-1.7 24.2-5.2 81.4-5.2 81.4l-31 321.7 71.1 1.3 5.2-177s18.4-138.2 34.9-152.4z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown85003f"
			x1={1183.4829}
			x2={1054.215}
			y1={1318.1454}
			y2={1297.7413}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85003f)"
			d="M1066.9 486.9s-9.7-17.3-18.1-22c1.8-15.9 3.7-38.9 5.2-53 4.6-2.9 23.6-6.5 39.4-7.5 14.6-.9 36.2 1.6 48.5 7.5 1.7 24.2 5.2 81.4 5.2 81.4l31 321.8-71.1 1.3-5.2-177.1c0 .1-18.4-138.2-34.9-152.4z"
			opacity={0.302}
		/>
		<path
			d="M574.4 1036.8s-.2-24.5-.4-61.7c0-1.2 38.8-2.4 38.8-3.7 0-6.5-.1-13.4-.1-20.6 0-1.1-38.8-2.2-38.8-3.3-.4-68.5-1-163.2-1.4-234.2 0-.7 38.8-1.5 38.8-2.2 0-7.9-.1-15.5-.1-22.7 0-.4-38.8-.8-38.8-1.3-.3-43.8-.4-73.6-.4-73.6l187.4 55.6-11.6 146 68.5 78.8s17.1 284.3 31 336c-41.3-24.9-272.9-193.1-272.9-193.1zm767.3 1.8s-2-28.4-1.7-65.6c0-1.2-33.6-.4-33.6-1.7 0-6.5.1-13.4.1-20.6 0-1.1 33.4.3 33.4-.8.4-68.6-1.4-167-1-238 0-.7-31-.2-31-.9.1-7.9.1-15.5.1-22.7 0-.4 30.8 1 30.8.6.3-43.8 1-72 1-72l-180.1 52.2 11.6 146-68.5 78.8s-17.1 284.3-31 336c41.3-24.8 269.4-191 269.9-191.3z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown85003g"
			x1={896.1871}
			x2={1039.8607}
			y1={616.6294}
			y2={619.1375}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.148} stopColor="#333" />
			<stop offset={0.497} stopColor="#b3b3b3" />
			<stop offset={0.844} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-topdown85003g)"
			d="M949.2 1456.1v-307.5s-17.2-4.9-47.8 20.7c-.6 1.5-5.6 35.3-13.2 96.1 12.3 71.2 26.9 146.7 37.7 185.6 1.8 2.2 3.1 3.6 5.5 5.4 13.6 0 17.8-.3 17.8-.3zm22-.2v-307s17.1-4.9 47.6 20.6c.6 1.5 5.6 35.2 13.1 96-12.2 71-26.8 146.4-37.6 185.2-1.8 2.2-3.1 3.6-5.4 5.4-14.5.1-17.7-.2-17.7-.2z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M853.6 1317.8c-10.8-57.6-23.2-185.1-30.9-324.5-4.8-82.8-5.7-66.8-5.7-102.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1044.9 465c102.6 61.2 41 655.4 25.3 812.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1104.4 817.8h72.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1069.5 493.4h81.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1881.3 612.3v94.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1465 1357.9 72.4-788.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1339.9 1083.6c-.6-231.3-1.3-506.2-1.3-506.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1702.9 629.1 2.6 264.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1098.2 636.3c-13.2 6.3-21.5 20.5-33.8 40.1-.1 29.3-1.3 244.3-1.3 244.3s23.3 19.3 31.3 71.1c5.1-84.5 12.6-283.3 3.8-355.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M911.8 1161.5V443" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1008.7 1161.5V443" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M951.8 1166.6h16.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M949.2 1456.1v-307.5s-17.2-4.9-47.8 20.7c-.6 1.5-5.6 35.3-13.2 96.1 12.3 71.2 26.9 146.7 37.7 185.6 1.8 2.2 3.1 3.6 5.5 5.4 13.6 0 17.8-.3 17.8-.3zm22-.2v-307s17.1-4.9 47.6 20.6c.6 1.5 5.6 35.2 13.1 96-12.2 71-26.8 146.4-37.6 185.2-1.8 2.2-3.1 3.6-5.4 5.4-14.5.1-17.7-.2-17.7-.2z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M873 465s86.1-70.9 174.5 2.6m111.2 162.8 182.3-58.2 191.3-3.9 171.9 62 171.9-16.8 28.4-1.3V682s-187.6 203-198.2 213.7c-10.7 19.2-242.6 464.7-242.6 464.7l-124.1 37.5 1.3-316.6-270.1 188.7s-22.4 350.2-111.2 350.2c-38.8 0-76.4-57.1-112.5-350.2-65.6-43-275.3-193.8-275.3-193.8l-1.3 326.9-116.3-42.6S222.3 913.2 214.4 898.9C205 888.5 9.5 682.1 9.5 682.1v-69.8h28.4L212.4 633l174.5-64.6 179.7 3.9s171 51.2 192.6 56.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M911.8 1572.4c4.9-10.1 23.8-19.4 49.1-19.4s49.6 13.5 51.7 20.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M817.4 899.2 745 817.8l14.2-190L778.6 412s44.7-19.3 89.2 0c1.6 15.7 2.9 24.4 5.2 53-65.3 42.1-56.9 339.3-55.6 434.2zm281.8 0 76.1-81.8-14.2-190-19.3-213.3s-44.6-19.3-88.9 0c-1.6 15.7-2.9 24.4-5.1 53 66.9 51.5 54.2 335.3 51.4 432.1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M745 817.8h72.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M821.3 636.9c13.1 6.3 21.4 20.5 33.6 40 .1 29.3 1.3 244.2 1.3 244.2s-25.8 19.3-33.6 71.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m571.8 1401.8 1.3-828.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M574.4 972.8h40.1v-23.3h-41.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M574.4 713.1h40.1v-23.3h-41.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1342.1 972.8H1306v-23.3h36.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1343.1 713.1H1306v-23.3h36.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m456.7 1361.8-68.5-796" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M213.7 631.7v264.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M32.7 612.3v94.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M768.3 493.4h78.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M937.6 1613.7v47.8h44v-47.8" />
	</svg>
);

export default Component;

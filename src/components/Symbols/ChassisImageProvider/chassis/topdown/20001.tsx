import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M262.1 932.3c-.1 42.2-.1 69-.1 69h-12.7l-6.4 621.3H70.1L58.6 998.8s-1.7-1.3-5.9-1.3c0-3.2-.2-7.7-.3-11.4-7.7-13.8-23-33.1-23-33.1l2.5-186.8 21.6-40.7V395.3l91.5 1.3v96.6h31.8l1.3-97.8 85.1 1.3s-.5 220.3-.8 395c12.5 0 21.2.1 21.2.1v-28l176.6-83.9s9.5-92.6 67.4-199.5c78.8-54.9 108-71.2 108-71.2l132.2-12.7 8.9 14s28.7-13.9 92.8-24.1c53.7-49.1 140.1-34.8 181.7 1.3 29.5 4.2 51.6 6.7 101.7 25.4.7-3.6 2.5-14 2.5-14l143.6 15.3 97.8 66.1s43.6 106.1 63.5 189.3c76.7 42.3 179.2 95.3 179.2 95.3v26.7s8.7-.1 21.2-.1c-.4-174.7-.8-395-.8-395l85.1-1.3 1.3 97.8h31.8v-96.6l91.5-1.3v327.8l21.6 43.2 2.5 186.8-29.2 44.5-11.4 625.1h-172.8l-6.4-621.3h-12.7s-.1-26.8-.2-69c-3.9 0-21.5.4-21.5.4v24.1l-109.3 55.9s-17.5 223.1-163.9 344.3c-33.8 53.9-70.1 105.6-81.3 120.7-25.4 5.5-67.3 15.3-67.3 15.3l-63.5-7.6-5.1 85.1-378.7-6.4-5.1-81.3-53.4 7.6-77.5-16.5s-64.4-89.8-78.8-122c-65.4-54.2-147.3-198.3-162.7-339.3-58.9-31.4-109.3-54.6-109.3-54.6v-25.4c-.1 0-13.5-.7-21.5-.4z"
		/>
		<linearGradient
			id="ch-topdown20001a"
			x1={962.2849}
			x2={962.2849}
			y1={905}
			y2={-189}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown20001a)"
			d="M766 397c-21.5 104.9-52 303.8-52 472s-7.3 433.9 77 605c-33.5 10-74.7 15.7-80 17-17.3-3.3-77-18-77-18s-19.8-19.7-46-63.1c-51.5-85.4-128-265.5-128-588.9 0-68.8-1.2-150.7 5-175s38-132.7 63-167c24.6-18.2 107-70 107-70s84.9-10.4 131-12zm388 0c21.5 104.9 52 303.8 52 472s7.3 433.9-77 605c33.5 10 74.7 15.7 80 17 17.3-3.3 77-18 77-18s19.8-19.7 46-63.1c58.6-68.5 132-265.5 132-588.9 0-68.8 4.3-115-9-175-6.2-24.3-38-132.7-63-167-24.6-18.2-107-70-107-70s-84.9-10.4-131-12z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown20001b"
			x1={961}
			x2={961}
			y1={223}
			y2={-192}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.749} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown20001b)"
			d="m53 809 1-413 90 1v411l-91 1zm123-2 1-413 85 2v412l-86-1zm1693 2-1-413-90 1v411l91 1zm-123-2-1-413-85 2v412l86-1z"
			opacity={0.302}
		/>
		<path fill="red" d="M769 1546.4h380v16.5H769v-16.5z" />
		<linearGradient
			id="ch-topdown20001c"
			x1={1150.3497}
			x2={766.4}
			y1={941.1167}
			y2={941.1167}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown20001c)"
			d="m766.4 1485.4 96.6-1.3 95.3 44.5 91.5-43.2 100.4 2.5s.8 79.2-1.3 81.3c-2 2.1-190.6 0-190.6 0l-189.3-3.8-2.6-80z"
			opacity={0.302}
		/>
		<path
			fill="#e6e6e6"
			d="m248 1622.7 2.5-620.1-193.2-2.5 3.8 622.6H248zm1425.8-621.4 191.9-1.3-2.5 622.6h-185.5l-3.9-621.3z"
		/>
		<path
			fill="#d9d9d9"
			d="M549.1 1341.8c-75.3-50.6-156.3-266.5-156.3-336.7s14.5-187.7 64.8-321.5c-5 140 24 513.2 91.5 658.2zm828.6 0c75.3-50.6 152.5-253.8 152.5-324s-14.6-197.1-61-334.2c5 140-24 513.2-91.5 658.2z"
		/>
		<path
			d="M455.1 686.2c-19 44.5-54.4 160.7-63.5 327.8-57.9-28.7-108-54.6-108-54.6V765c-.1 0 119.6-60.2 171.5-78.8zM263.2 809.5l-86.4 1.3s-1.3 190.3 0 189.3 87.7 0 87.7 0l-1.3-190.6zm-119.4-1.3H52.3L51 998.8h92.8V808.2zm1322.8-129.6c19 44.4 55.6 168.3 64.8 335.4 57.9-28.7 108-54.6 108-54.6V765s-120.9-67.8-172.8-86.4zm193.2 130.9 86.4 1.3s1.3 190.3 0 189.3-87.7 0-87.7 0l1.3-190.6zm119.4-1.3h91.5l1.3 190.6h-92.8V808.2zM959.6 354.6c-19.2 0-86.5 9.9-100.4 38.1s-73.5 154.2-78.8 204.6-29.2 268.2-29.2 376.1.4 367.4 99.1 505.7c81.1 37.2 106.7 48.3 106.7 48.3l114.4-54.6s86.4-262.3 86.4-473.9c0-121.8 2.4-270-16.5-386.3-11.9-71.8-36.2-138.9-95.3-228.7-37.2-27.9-67.2-29.3-86.4-29.3z"
			className="factionColorPrimary"
		/>
		<path
			d="M457.6 678.6c-33.8 62-70.8 270.3-66.1 336.7s58.7 250.8 161.4 327.8c-39-101.8-56.7-154.2-78.8-340.5-22-186.3-14.5-244.2-16.5-324zm306.3-261.7c-34.4 127.1-50.8 445.7-50.8 589.5s12.5 334 83.9 477.7c31.9-.2 61-2.5 61-2.5l-14-8.9s-92.8-128.3-92.8-457.4c0-329.2 39.5-525.3 110.5-626.4-41.5 10-78.2 14.8-97.8 28zM1465 678.6c33.7 62 70.7 270.3 66 336.7s-58.4 259.7-161 336.7c39.5-88.7 60.2-163.1 82.3-349.4 22.1-186.3 10.8-244.2 12.7-324zm-307.2-263c34.4 127.1 49.6 438.1 49.6 581.9s-10.1 345.4-81.3 489.2c-32.1-.3-71-1.7-69.9-1.3 0 0 66-34.6 97.8-470.1 4.6-163.2 3.5-294.3-11.4-393.9-15.4-85.6-55.8-180.6-94-235.1 41.5 10.1 89.6 16.1 109.2 29.3z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown20001d"
			x1={961.45}
			x2={961.45}
			y1={415.3}
			y2={222.2}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown20001d)"
			d="M52.3 808.2h91.5l1.3 190.6-92.8-1.3V808.2zm123.2 0 87.7 2.5-2.5 190.6-85.1-2.5V808.2h-.1zm1485.6 2.5 85.1-1.3 2.5 189.3-87.7 2.5.1-190.5zm118.1 0 88.9-1.3 2.5 189.3-91.5 1.3.1-189.3z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown20001e"
			x1={961.45}
			x2={961.45}
			y1={426.7}
			y2={86.2}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown20001e)"
			d="m283.5 958.1 109.3 54.6s-.2-149 61-326.5c-30.6 12.6-169 77.5-169 77.5l-1.3 194.4zm1249.2 52.1 106.7-54.6V765l-176.6-92.8c0 .1 70.1 194.4 69.9 338z"
			opacity={0.4}
		/>
		<path
			fill="gray"
			d="m175.5 997.5 1.3-504.4H145l-1.3 505.7 31.8-1.3zm1603.7 0 1.3-504.4h-31.8l-1.3 505.7 31.8-1.3z"
		/>
		<path
			fill="gray"
			d="M267 791.7h16.5l1.3 142.3-21.6-1.3 3.8-141zm1373.7 0 17.8 1.3 2.6 141-19.1 1.3-1.3-143.6z"
		/>
		<radialGradient
			id="ch-topdown20001f"
			cx={967.471}
			cy={378.286}
			r={1100.3311}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.399} stopColor="#000" stopOpacity={0} />
			<stop offset={0.549} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown20001f)"
			d="M457.6 678.6c-33.8 62-70.8 270.3-66.1 336.7s58.7 250.8 161.4 327.8c-39-101.8-56.7-154.2-78.8-340.5-22-186.3-14.5-244.2-16.5-324zm306.3-261.7c-34.4 127.1-50.8 445.7-50.8 589.5s12.5 334 83.9 477.7c31.9-.2 61-2.5 61-2.5l-14-8.9s-92.8-128.3-92.8-457.4c0-329.2 39.5-525.3 110.5-626.4-41.5 10-78.2 14.8-97.8 28zM1465 678.6c33.7 62 70.7 270.3 66 336.7s-58.4 259.7-161 336.7c39.5-88.7 60.2-163.1 82.3-349.4 22.1-186.3 10.8-244.2 12.7-324zm-307.2-263c34.4 127.1 49.6 438.1 49.6 581.9s-10.1 345.4-81.3 489.2c-32.1-.3-71-1.7-69.9-1.3 0 0 66-34.6 97.8-470.1 4.6-163.2 3.5-294.3-11.4-393.9-15.4-85.6-55.8-180.6-94-235.1 41.5 10.1 89.6 16.1 109.2 29.3z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown20001g"
			x1={954.5491}
			x2={954.5491}
			y1={941.4001}
			y2={-231.4}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown20001g)"
			d="M959.6 354.6c-19.2 0-86.5 9.9-100.4 38.1s-73.5 154.2-78.8 204.6-29.2 268.2-29.2 376.1.4 367.4 99.1 505.7c81.1 37.2 106.7 48.3 106.7 48.3l114.4-54.6s86.4-262.3 86.4-473.9c0-121.8 2.4-270-16.5-386.3-11.9-71.8-36.2-138.9-95.3-228.7-37.2-27.9-67.2-29.3-86.4-29.3z"
			opacity={0.2}
		/>
		<path
			fill="#191919"
			d="M635.5 1427.7c1.1 15.3 20.2 16.4 49.4 14.4 29.3-2 45.7-1.8 44.5-19.7-.8-10.9.8-16.6-44.3-13.5s-50.4 8-49.6 18.8zm-26.7-50.8c1.1 15.3 20.2 16.4 49.4 14.4 29.3-2.1 45.7-1.8 44.5-19.7-.8-10.9.8-16.6-44.3-13.5s-50.4 8-49.6 18.8zm-19.1-54.6c1.1 15.3 20.2 16.4 49.4 14.4 29.3-2 45.7-1.8 44.5-19.7-.8-10.9.8-16.6-44.3-13.5s-50.3 7.9-49.6 18.8zm696.5 104.8c-1.1 15.2-20.1 16.4-49.3 14.3-29.2-2-45.7-1.8-44.4-19.6.8-10.8-.8-16.6 44.2-13.4 44.8 3.2 50.2 7.9 49.5 18.7zm26.6-50.6c-1.1 15.2-20.1 16.4-49.3 14.3-29.2-2-45.7-1.8-44.4-19.6.8-10.8-.8-16.6 44.2-13.4 44.9 3.1 50.3 7.9 49.5 18.7zm19-54.5c-1.1 15.2-20.1 16.4-49.3 14.3-29.2-2-45.7-1.8-44.4-19.6.8-10.8-.8-16.5 44.2-13.4 44.9 3.1 50.3 7.9 49.5 18.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M262.1 932.3c-.1 42.2-.1 69-.1 69h-12.7v621.3H58.6V998.8s-1.7-1.3-5.9-1.3c0-3.2-.2-7.7-.3-11.4-7.7-13.8-23-33.1-23-33.1l2.5-186.8 21.6-40.7V395.3l91.5 1.3v96.6h31.8l1.3-97.8 85.1 1.3s-.5 220.3-.8 395c12.5 0 21.2.1 21.2.1v-28l176.6-83.9s9.5-92.6 67.4-199.5c78.8-54.9 108-71.2 108-71.2l132.2-12.7s5.7 9 8 12.5c.4.6-4.3 3.5-6.7 5.2-1 .7-1.3 1.3-1.3 1.3s9.6-5.4 11.2-6.1c7.9-3.4 27.9-12 81.6-20.6 42.2-42.7 143.5-46.3 190.6-1.3 29.5 4.2 51.6 6.7 101.7 25.4.7-3.6 2.5-14 2.5-14l143.6 15.3 97.8 66.1s43.6 106.1 63.5 189.3c76.7 42.3 179.2 95.3 179.2 95.3v26.7s8.7-.1 21.2-.1c-.4-174.7-.8-395-.8-395l85.1-1.3 1.3 97.8h31.8v-96.6l91.5-1.3v327.8l21.6 43.2 2.5 186.8-29.2 44.5v625.1h-190.6v-621.3h-12.7s-.1-26.8-.2-69c-3.9 0-21.5.4-21.5.4v24.1l-109.3 55.9s-17.5 223.1-163.9 344.3c-33.8 53.9-70.1 105.6-81.3 120.7-25.4 5.5-67.3 15.3-67.3 15.3l-63.5-7.6-5.1 82.6-380-2.5-3.8-82.6-53.4 7.6-77.5-16.5s-64.4-89.8-78.8-122c-65.4-54.2-147.3-198.3-162.7-339.3C334 981.2 283.6 958 283.6 958v-25.4c-.1.1-13.5-.6-21.5-.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M959.6 1524.8c0-242 2.5-1167.7 2.5-1167.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1045 1486.9c62.3.6 110.3 1.1 110.3 1.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M769 1484.2s40.7.5 95.8 1.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m53.4 808.2 207.3 1.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m143.8 997.5 1.3-504.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m175.5 997.5 1.3-504.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m57.2 998.8 195.9 2.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m261.9 946.7 1.3-155" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M283.5 790.4v142.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M53.4 696.4c0 2-1.1 308.8-1.1 308.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1869.6 808.2-207.3 1.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1779.2 997.5-1.3-504.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1747.5 997.5-1.3-504.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1865.8 998.8-195.9 2.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1661.1 946.7-1.3-155" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1639.4 790.4v142.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1869.6 696.4c0 2 1.1 308.8 1.1 308.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M392.8 1015.3c0-142 47.4-294.1 71.2-357" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1530.1 1015.3c0-142-48.9-294.3-72.8-357.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M464 657c-11.6 63.5-9.6 512.3 96.6 702.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1459.3 665.9c11.6 63.5 12.2 503.5-94.3 693.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M767.7 396.5c-45.2 146.3-104 831.3 22.9 1077.5-21.4 5.1-30.5 8.9-30.5 8.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1153.4 397.1c45.4 146.3 104.4 835-23 1081.2 21.5 5.1 30.6 8.9 30.6 8.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M861.7 386.4C838.4 440 787.5 521 775.3 632.9c-30.5 280-54.9 640.8 71.2 842.4 54.4 25.4 110.5 52.1 110.5 52.1l114.4-53.4s131.6-311.3 72.4-844.9c-12.5-113.2-94-241.4-94-241.4"
		/>
	</svg>
);

export default Component;

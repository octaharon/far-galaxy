import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<g id="Animation-Foot-Right">
			<path
				fill="#ccc"
				d="M1177.7 810.7c-.3 21.7 2.5 96.2 51 90 48.5-6.2 148.7-8.4 153-113 4.6-111.9-38.8-257.5-40-282-1.2-24.5-38.1-52.9-120-39-81.9 13.8-43.8 322.3-44 344z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1342.7 868.7-25-27-73 15-15 42" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1177.7 810.7c-.3 21.7 1 83.3 51 90 48.4 6.5 148.7-8.4 153-113 4.6-111.9-36-257.4-40-282-4-24.2-38.1-52.9-120-39-81.9 13.8-43.8 322.3-44 344z"
			/>
		</g>
		<g id="Animation-Foot-Left">
			<path
				fill="#ccc"
				d="M743.3 810.7c.3 21.7-2.5 96.2-51 90-48.5-6.2-148.7-8.4-153-113-4.6-111.9 38.8-257.5 40-282 1.2-24.5 38.1-52.9 120-39 81.9 13.8 43.8 322.3 44 344z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m578.4 868.7 25-27 73 15 15 42" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M743.3 810.7c.3 21.7-1 83.3-51 90-48.4 6.5-148.7-8.4-153-113-4.6-111.9 36-257.4 40-282 4-24.2 38.1-52.9 120-39 81.9 13.8 43.8 322.3 44 344z"
			/>
		</g>
		<path
			fill="#f2f2f2"
			d="M1317 1136v-43h-29v-37h29v-43h-29v-35h29v-43h-34s33-51.3 24.1-69c162.2-199 143.4-236.6 169.9-277 1.9-1.5 31.6-33.7 32-122s-70.1-134.2-105-142-68-25.5-103-45c-12.2-51.9-33.7-138.4-56-190s-60.2-63-284-63c-160.3 0-238.4-11.2-290 76-32.1 88.1-49 177-49 177l-82 40s-131 19.6-131 145c0 39 17.7 101.5 21 108 48.7 59.8 91 43.5 131 68 15.5 19 97.9 138.8 160 163s119.5 19 141 19 87.1-10.4 98-24c10.5 13.5 60.2 23 96 23 29.4 0 70.7 4.5 108.3-7.5-5.4 8.8-13.6 24.5-27.3 40.5-34.6 3.3-51 52-51 52h-5l1 28h-21v43h22v35h-23v43h23v37h-22v43h79.6c1.9 0 3.4 5 5.4 5 24.1 0 50.4.4 75 0 1.2 0 2.8-5 4-5h92z"
		/>
		<linearGradient
			id="ch-topdown10000a"
			x1={1198.765}
			x2={1198.765}
			y1={999.462}
			y2={1080.2491}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.799} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000a)"
			d="M1288 924c7-5 27.2-38.5 20-60s-122.6-29.3-183-5c-23.8 6.5-35.8 42.2-37 48 16.6 1.5 200 1 200 1s-7 21 0 16z"
			opacity={0.302}
		/>
		<path
			fill="#d9d9d9"
			d="M1454 351c-3.5-2.7-17.7-16.3-41-21-72-14.6-185.8-41.1-202 54 24.3-.2 256.7-22.3 243-33zm-246 170c24.2 1.2 202.8 12.3 283 49-8 11.5-21.1 35.2-63 41-48.5-1.5-103.7 16-122 16s-89.4-11.3-98-106zM708 384c-3.4-14.4-16.7-44.4-27-47 .5 18.7 11.1 45 17 49 7 .3 13.4 12.4 10-2zm-81-69c-22.5-.2-133.7 6-164 36 15.4 5 124.3 29 166 29-1-15.5-4.9-38.9-2-65zm85 206c-21.7 0-207.5 12.8-281 50 5 11.3 30.1 37 56 40 11.4 1.3 32.8-1.2 55 2 28.1 4.1 57.9 14 73 14 27.1 0 88.2-21.1 97-106z"
			opacity={0.7}
		/>
		<path
			fill="#c2c2c2"
			d="M1472 593c-19.3 12.3-48.2 20.8-73 20 25.5 10.6 43.2 3.4 50 41 7.9-20.1 19-42.4 23-61z"
		/>
		<path
			fill="#c2c2c2"
			d="M1301 279c-8.6-37-39.5-201.2-93-230S974.8 28 958 28 756.4 9.7 699 68c-47.9 37.6-73.6 202.9-76 213 10.2-6.8 14-7 14-7s25.9-88 73-88c39.2 17.1 12.7 46.2 9 52 23.4-2.8 86.3-8 112-8s98 23.2 128 20 115.3-21 155-21 141.5 30.2 187 50z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown10000b"
			x1={723.381}
			x2={769.381}
			y1={1689.2537}
			y2={1725.1926}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000b)"
			d="M726 193c-4.1-4.8 40 31.6 41 40-17 2.4-46 7-46 7s19.2-30.2 5-47z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown10000c"
			x1={782.3555}
			x2={782.3555}
			y1={1427.8445}
			y2={1529}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path fill="url(#ch-topdown10000c)" d="m796 391-27 1s-.7 99.6 0 100 24 0 24 0v-80l3-21z" opacity={0.302} />
		<linearGradient
			id="ch-topdown10000d"
			x1={971}
			x2={971}
			y1={1538}
			y2={1691}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000d)"
			d="M551 318c23.4-14.3 77.6-41 86-43-4.1 13.1-10.2 33.2-10 42-18.7.2-45.8-1.9-76 1zm129 15c7.9 6.7 16.8 17.5 27 39 18.2 4.7 40 9 40 9s40-125 213-125c63.3 0 172.8 24.9 213 126 15.1-2.9 40-9 40-9s19.2-52.5 68-57 94.9 5.7 110 8c-42.3-16.9-175.2-95-294-95-75.4 0-85.3 21-136 21s-107.5-40.2-242-9c-17.2 17.4-39.8 57.7-39 92z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown10000e"
			x1={1174}
			x2={1174}
			y1={1359}
			y2={1545}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000e)"
			d="M1214 375c-14.8 2.3-85 16-85 16s29.3 56.1 13 155c27.6 5.4 77 15 77 15s-33.6-66.2-5-186z"
		/>
		<linearGradient
			id="ch-topdown10000f"
			x1={740.5}
			x2={740.5}
			y1={1359}
			y2={1545}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000f)"
			d="M768 410.8c-.1-15.7 0-24.8 0-24.8s-46.2-8.7-61-11c2.1 8.9 3.9 17.6 5.4 25.9 12.8 8.5 55.6 19.4 55.6 9.9zm-49.5 61.3C717.7 530 702 561 702 561s49.4-9.6 77-15c-3.2-19.5-4.6-37.3-4.9-53.4-1.7-.7-3.3-1.6-5.1-2.6-.1-4.8-.2-9.6-.3-14.2-13.3-.4-39.3-1.7-50.2-3.7z"
		/>
		<path
			fill="#e6e6e6"
			d="M1167 552c9.6 2.5 25.1 4.3 32 6-1.6 25.1-13.3 229-239 229-41.3 0-225.7-6.7-238-228 6.9-1.7 33-7 33-7s11.1 126.6 91 175c100.3 44.3 231.1 7.3 247-16 14.6-16.2 55.2-45.2 74-159zm8-171c-11.1-24-62.3-125-219-125S757.4 358.8 746 382c15.8 4.3 30 6 30 6s22-101 186-101c141.8 0 169.7 93.7 173 101 11.7-2.3 24.1-.3 40-7z"
		/>
		<linearGradient
			id="ch-topdown10000g"
			x1={959.9349}
			x2={959.9349}
			y1={1173}
			y2={1288}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000g)"
			d="M793 632c17.9 18.2 34.1 38 169 38s159.2-28.2 165-35c1.1 14.3 6.5 112-167 112-86.7 0-174.2-29.3-167-115z"
			opacity={0.2}
		/>
		<path
			fill="#f2f2f2"
			d="M1084 1013 835 823s-59.3 6-120-23c-29.2-16.6-62.8-46.8-91-80-24.6-29-44.3-60.9-64-79-32.8-15.9-63.6-13.9-104-42 21.2 34.5 189 286 189 286s97.9 84 205 164c114.2 85.3 238 167 238 167l-2-79h-25l-1-43 23-1v-38l-25 1 1-44 25 1z"
		/>
		<linearGradient
			id="ch-topdown10000h"
			x1={1088}
			x2={1012.8871}
			y1={797.5}
			y2={797.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#7a7a7a" />
			<stop offset={1} stopColor="#d8d8d8" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000h)"
			d="M1059 1031c-10.3 5.1-44 41.3-46 82s22.6 69.4 37 78 38 23 38 23v-77l-28-1v-43h21v-38l-22-1s-1.2-11.2 0-23z"
		/>
		<linearGradient
			id="ch-topdown10000i"
			x1={739.6802}
			x2={1033.4442}
			y1={754.6768}
			y2={1130.6768}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0.8} />
			<stop offset={0.4677} stopColor="#000" stopOpacity={0.05} />
			<stop offset={0.5391} stopColor="#000" stopOpacity={0.05} />
			<stop offset={1} stopColor="#000004" stopOpacity={0.8} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000i)"
			d="m837 823 244 188-22 4-2 16s-85.8 69.9-20 153c-54.3-38.3-376.1-276.3-391-298 21.3-4.1 74.6-7.8 86-78 41 18.3 105 15 105 15z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-topdown10000j"
			x1={1145.8821}
			x2={1452.8821}
			y1={1283.3671}
			y2={1058.759}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.15} stopColor="#000" />
			<stop offset={0.4882} stopColor="#999" stopOpacity={0.102} />
			<stop offset={0.5385} stopColor="#999" stopOpacity={0.102} />
			<stop offset={0.6859} stopColor="#999" />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000j)"
			d="M1308 864s-51.7-36.7-166-10c2.8-12.5 12.9-21 26-39 113.4-30.8 166.8-165.9 222-201 49.3 7.3 52.6 17.8 59 40-55.7 111.7-141 210-141 210z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-topdown10000k"
			x1={455.7981}
			x2={732.4491}
			y1={1105.5161}
			y2={1292.1191}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#ccc" />
			<stop offset={1} stopColor="#202020" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000k)"
			d="M456 600c12.8 12.2 91 26 102 40s104.2 147.7 174 169c-15.5 41.5-23.8 69.8-91 72-36-52.9-197.8-293.2-185-281z"
		/>
		<linearGradient
			id="ch-topdown10000l"
			x1={526.7286}
			x2={608.7106}
			y1={985.9717}
			y2={1267.4227}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.896} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000l)"
			d="M456 600c12.8 12.2 91 26 102 40s104.2 147.7 174 169c-15.5 41.5-23.8 69.8-91 72-36-52.9-197.8-293.2-185-281z"
			opacity={0.4}
		/>
		<path
			fill="#ff2a00"
			d="M1110 692c-13-1.3-86.6 21-94 25 6.9 8.4 17.7 20.2 24 21 11.1-1.5 56.6-21.9 70-46zm-208 23c-9-5.5-89.9-27-97-25 10.5 12.3 41.4 40.9 74 46 11.7-6 22.1-12.7 23-21z"
		/>
		<path
			d="M1213 382c30.1 1 165.4-4.6 241-32 25.2 20.2 54 53.8 54 111s-10.1 97.4-19 111c-25.1-10.3-85.7-40.6-282-50-4.3-44.6-9.8-82.7 6-140zm-584.6-3.3c-52.2-4.1-117-12.7-161.3-28.7-25.2 20.2-58.1 55.8-58.1 113s14.1 95.4 23 109c25.1-10.3 85.8-40.6 282.4-50 1.7-17.1 3.5-33.2 4.2-49.8-65.8-15.8-74.8-52.4-90.2-93.5zM722 560c6.3 185.8 145.9 226 239 226s223.8-41.1 238-226c11.8 1.7 18 5 18 5s28.1 62 90 62 52.4-15 92-15c-11.7 6.1-41.7 25.5-63 56s-85 155-246 155c-120.7 0-129-24-129-24s-25.4 25-151 25-198.7-116.7-212-136-38.9-52.4-55-56-66.1-17.6-78-29c24.9 8.8 50.8 5.8 70 9s58.8 18.6 83 15 66.8-17.5 85-64c7.1-1.6 10.6-2.1 19-3z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown10000m"
			x1={932}
			x2={932}
			y1={1096}
			y2={1360}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.7} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000m)"
			d="M722 560c6.3 185.8 145.9 226 239 226s223.8-41.1 238-226c11.8 1.7 18 5 18 5s28.1 62 90 62 52.4-15 92-15c-11.7 6.1-41.7 25.5-63 56s-85 155-246 155c-120.7 0-129-24-129-24s-25.4 25-151 25-198.7-116.7-212-136-38.9-52.4-55-56-66.1-17.6-78-29c24.9 8.8 50.8 5.8 70 9s58.8 18.6 83 15 66.8-17.5 85-64c7.1-1.6 10.6-2.1 19-3z"
			opacity={0.149}
		/>
		<linearGradient
			id="ch-topdown10000n"
			x1={1509.1487}
			x2={1202.4088}
			y1={1460}
			y2={1460}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.6} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000n)"
			d="M1212 382c24.5.9 176.7-5.2 242-33 34.4 33.9 83.1 89 35 222-31.5-11.3-126.4-43.6-281-49-8.8-50-7-94.1 4-140z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown10000o"
			x1={408.9748}
			x2={715.84}
			y1={1460}
			y2={1460}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.601} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown10000o)"
			d="M629.2 378.8c-55.4-4.2-125.8-13.1-165-29.8-34.5 33.9-83.2 89-35 222 31.5-11.3 126.5-43.6 281.3-49 3.1-17.7 4.9-34.7 5.4-51.3-64-14.7-76.4-60.9-86.7-91.9z"
			opacity={0.2}
		/>
		<path
			d="M952 301c37.8 0 155.1-9.2 175 130 .9 111.6 0 196.7 0 200s-17.9 41-172 41-164.9-44.9-164-46 2-219 2-219 5.2-106 159-106zm138 840 5 760 185-3 10-757h-200z"
			className="factionColorPrimary"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1309.4 866.8c30-40.3 76.4-104.5 112.6-162.8 26.5-42.8 54-115 54-115s33-22.5 33-126c0-88.3-70.1-130.2-105-138s-68-25.5-103-45c-12.2-51.9-33.7-138.4-56-190s-60.2-63-284-63c-160.3 0-238.4-11.2-290 76-32.1 88.1-49 177-49 177s-71.4 37.3-82 43c-41.5-.5-131 29.6-131 155 0 39 17.7 84.5 21 91 36.3 62 108.4 51.3 131 72 15.5 19 97.9 138.8 160 163s119.5 19 141 19 87.1-10.4 98-24c10.5 13.5 60.2 23 96 23 24 0 60.5.5 94.1-5 9.3-1.5 8.3-.4 16.8-2.9-5.7 8-17.6 24.2-29.8 40.8M1099 907h-18v28h-21v43h22v35h-23v43h23v37h-22v43h27l5 764h185l10-764h29v-43h-29v-37h29v-43h-29v-35h29v-43h-29v-28h-8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1449 656c-3.9-19.5-7-42.4-69-36" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1287 927s29.9-36.7 20-63c-13.8-11.8-48.1-20.2-86-20-39.3.2-82.3 9.3-95 14-18.5 6.8-33.8 28.7-40.4 50.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1296 1093s-36.9.4-71.9 0c.1 12.1-.3 24.5 0 43h71.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1072 1093s36.9.4 71.9 0c-.1 12.1.3 24.5 0 43H1072" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1072 1013s36.9.4 71.9 0c-.1 12.1.3 24.5 0 43H1072" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1072 935s36.9.4 71.9 0c-.1 12.1.3 24.5 0 43H1072" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1296 935s-36.9.4-71.9 0c.1 12.1-.3 24.5 0 43h71.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1296 1013s-36.9.4-71.9 0c.1 12.1-.3 24.5 0 43h71.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1099 907h182" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M960 300c38.5 0 164.8 0 167 146s1 169.1 1 188 .8 113-167 113-170-87.6-170-121c0-16.5 2-109.7 2-210 0-87 90.9-116 167-116z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M791 629c17.1 21.7 35 42 169 42s159.3-22.6 168-41" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1218.6 564c-24.7-5-55.9-11.5-78.6-17 20.2-89.3-11-156-11-156s55.6-11.2 85.5-17.3m-508.7-.4C737.6 380 788 391 788 391m-13.7 102c.4 16.7 2.4 34.8 6.7 54-21.7 5.2-55.1 11.4-79.2 16.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1174 382c-11.2-28.8-59.4-125-216-125S754.8 358.5 746 381m-24 180c2.2 27.9 5.5 224 239 224 95.4 0 224.9-39.8 237-224m-32-7c-1.4 19.7-19.2 97.2-53 137m-304 4c-23.2-31.1-48-82.6-53-142m19-165c6.4-22.8 44.2-102 188-102 135 0 165.3 84.1 172 103"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1044 737c-20.8-5.3-27-22-27-22l95-27m-307-1 98 28s-6.2 16.7-27 22"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m795 391-27 1 2 100 23-1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M768 411c-56.8 20.5-141-66.7-47-175 19-21.9 7.2-40.9 0-45s-25-22.2-62 33-40.8 142.7-23 181c27.5 59 72.5 71 133 71"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M706.8 382.9s-4.1-.1-11.3-.3m-67-3.5c-16.6-1.3-34.3-3-51.8-5.2-57.2-7.4-111-22-111-22m-41 218.4s56.9-20.1 121-31.1c78-13.4 166-18 166-18"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M536.3 323.4c32.3-4.6 63.4-9.7 90.3-8.1m53.4 18.8c15.4 13 26.8 34.1 32.6 67m4.2 72.1c-2.4 93-28.9 134.2-75.2 147.8-52.2 15.3-80.2-9.5-122.8-9.5-47.6 0-68.1-14.1-81.1-29-2-2.3-4.9-7-8-13.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1410 327c-106.5-12.5-207-50.5-207 132 0 103.3 26.8 148.2 75.4 162.5 52.2 15.3 80.1-9.5 122.6-9.5 47.5 0 68-14.1 81-29"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1213 383s67-.9 130-9c57.2-7.4 111-22 111-22m41 218s-56.9-20.1-121-31c-78-13.3-166-18-166-18"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1306 283c-33.4-17.4-143.6-53-207-53s-101 20-138 20-81.9-20-127-20c-42.8 0-83.6 2.3-115 9.8M637.1 273c-13 6.3-22.1 11-22.1 11"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1173.9 40.4c13.8 17.1 22.1 38.9 22.1 62.7 0 55.2-44.8 99.9-100 99.9s-100-44.7-100-99.9c0-29.9 13.2-56.8 34-75.1m-139-.2c20.9 18.3 34 45.2 34 75.2 0 55.2-44.8 100-100 100s-100-44.8-100-100c0-23.8 8.3-45.6 22.1-62.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1397 612c-68.1 36.1-93.4 131.2-168 174-18.7 10.7-37.6 24.5-82 31"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M452 596s178.8 278 198 295c19 16.9 250.2 206.4 436.4 323.7m-4.3-204.1c-7.8-5.6-15.2-10.9-22.1-15.6-38.9-26.9-159-120-225.7-171.7M731 807c.5 21.7-19.5 76-86 76"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1058 1027.6c-34.8 31.1-78.3 102.7-11 162.4" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<g id="Animation-Foot-Left">
			<path fill="#999" d="m663 1018 53-247-32-364-105-212H439l-95 209-41 368 60 246h53l87-247h16l89 247h55z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m663 1018 53-247-32-364-105-212H439l-95 209-41 368 60 246h53l87-247h16l89 247h55z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M425.3 695.1C481.5 723 562.5 719 602 693.9c-5.8-50.4-17.9-139.4-17.9-139.4s-90.6 19-134.9-4.8c-11.1 76.8-12.6 82.7-23.9 145.4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M587 574c22.5-4.1 38-13 38-13s16.4 72.9 20 98c-4.1 6.2-19.6 15.9-43 19-.9.1-3 1-3 1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M430 678c-23.4-3.1-38.9-12.8-43-19 3.6-25.1 20-98 20-98s15.5 8.9 38 13"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m344 405 340 2m32 366-197-2m-17 1-199 2" />
		</g>
		<g id="Animation-Foot-Right">
			<path
				fill="#999"
				d="m1368.5 1018.5-53-247 32-364 105-212h140l95 209 41 368-60 246h-53l-87-247h-16l-89 247h-55z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1368.5 1018.5-53-247 32-364 105-212h140l95 209 41 368-60 246h-53l-87-247h-16l-89 247h-55z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1606.2 695.6c-56.2 27.9-137.2 23.9-176.6-1.2 5.8-50.4 17.9-139.4 17.9-139.4s90.5 19 134.9-4.8c11 76.8 12.5 82.7 23.8 145.4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1429.5 678.5c-23.3-3.1-38.9-12.8-43-19 3.6-25.1 20-98 20-98s15.5 8.9 38 13"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1601.5 678.5c23.3-3.1 38.9-12.8 43-19-3.6-25.1-20-98-20-98s-15.5 8.9-38 13"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1687.5 405.5-339.9 2m-32.1 366 197-2m17 1 199 2" />
		</g>
		<path
			fill="#f2f2f2"
			d="M665 334c12.5 3.5 24.4 16.2 25 19 17.6-38 75.8-232.5 217-249 4.3-5.1 15.4-16.6 22-19 48.6-22.5 130.4-10.1 167-2 7 5.8 12.5 13.3 20 21 116 15.5 169.5 115.8 189 157 44.5-13.3 122.8-12.8 146-12 8.8-6.2 34-14 34-14V4l382-1 1 258s-25.6 49.5-36 71c11.7 18.8 9 102.9 9 159 0 118.3-13 175-13 175l15 209h-37v241h17l19 245h-35l-1 363h-261l1-363h-37s4.5-47.9 7.2-81.9c-33.6-.3-119.2-.1-119.2-.1l10-24s13.8-5.1 27-9.4c-55.4-34.6-76.4-100.6-84-135.6-50.9 104.2-220.5 181-339 181-152.9 0-282-92-325.8-162.9-16.9 43.7-33.2 85.9-87.2 119.9 15.8 5.4 25 9 25 9l11 23s-224.8-2-242-2c4.5-7.9 12-23 12-23l23-7-20-12v8s-81.9 43.8-82 44-4 76-4 76l94 7-22 50-44-2-18 49 42 3-21 51-39-1-17 50 33 1-21 50-29-1-17 49 24 2-23 50-24-1-17 51 21 2-23 49h-18l-15 49 12 2-32 43-26-46h10l-6-51-19-2-15-51 20 1-9-52-26-1-15-53 25 2-11-51-27-2-17-53 34 2-11-53-35-2-17-52 42 2-10-51-44-3-8-51 87 3 2-69-84-46s5.2-69.1 14-186c30.5-14.6 66.8-30.8 91-43 .3-17.9 1.3-37.5 4-58-44.7-49.7-60.9-177.3 57-237 11.5-23 19.8-37.7 30-53 18.8-204.8 12.6-283 153-324 22.5-4.3 82-14.3 145-15 68.9-.8 141.6 8.2 164 12zM344 972l-1 40 19 11v-67c-1.3 3.5-18 16-18 16z"
		/>
		<path
			fill="#d9d9d9"
			d="M380 924v89l242-1-7-209s-7.8 13-36 13-102-9-102-9l-64 71s-15.3 10.1-19 11c-1.2 4.2-3.5 13.5-7 22-2.7 6.5-7 13-7 13zm1033-73 3 159h129l1-133h-36l2-22-99-4z"
		/>
		<linearGradient
			id="ch-topdown18003a"
			x1={707}
			x2={707}
			y1={1282.9813}
			y2={1463}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003a)"
			d="m697 458 28-1s14 3.6 14 77-34.7 98.4-39 103c-11.2.2-25-1-25-1s10.5-41.8 16-86c5.5-44.8 6-92 6-92z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown18003b"
			x1={672.1988}
			x2={672.1988}
			y1={1238}
			y2={1275.0004}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003b)"
			d="M672 645c.5.1 11.6 5.2 9 20s-4 17-4 17h-14s8.5-37.1 9-37z"
			opacity={0.302}
		/>
		<path
			d="M615 815v197H387V911s-14 34.6-26 46c0 8.2 3 68 3 68l45 25 10 14-7 109 235-1v-159s-5-20.5-10.3-50.5c-9.5-54.8-22-136.4-21.7-147.5zm811 38v157l119-1-1 107-14 1-5 53h-132l-1-320 34 3zM55 1403l293 17-17 49-267-15s-4.8-18.3-9-51zm18 103 240 15-17 49-211-11-12-53zm22 106 182 10-14 48-158-6-10-52zm27 104 117 5s-17.2 52.6-17 52-91-5-91-5l-9-52zm23 104 6 51 37 1 13-49-56-3zm7-507L7 1239l13-187 92-42 59-3-19 306zm191-300-18 273 82-41 12-181-9-14-67-37z"
			className="factionColorSecondary"
		/>
		<path
			fill="#dedede"
			d="M650 850c.4 4.7 30.5 198.1 68 249s98.3 96.4 173 111c37-6.3 63-9 63-9l10-75 11-19h79l10 17 11 78 63 8s116.3-22.9 172-111c17-26.9 46.1-111 71-250-11.2-2-42-3.3-42-50 0-25.3-1.2-69.3-3-108-34.6 114.2-149.7 222-255 250-.2 8.9-1 23-1 23H942l1-23s-183.6-46.9-255-247c-2.9-16.5-4.7-25.9-5-32-2.9 12.6-4.2 17.6-6 21-8.7.2-16-1-16-1l-10 32-16 39s11.7 70.3 15 97z"
		/>
		<radialGradient
			id="ch-topdown18003c"
			cx={1044.825}
			cy={1106.2419}
			r={625.6}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#000" />
			<stop offset={0.543} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18003c)"
			d="M1065 1121c.3 7.2 2 20 2 20l8 60 63 8s93.6-9.4 169-103c34.6-43 69.3-218.7 73-256-7.3-2.7-17.7-6.2-23-9-.7 7.8-16 96.4-51 152-27.5 45.4-100.2 128-241 128z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown18003d"
			cx={975.19}
			cy={1119.845}
			r={646.1}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#535353" />
			<stop offset={0.543} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18003d)"
			d="M965 1121c-.3 7.2-2 20-2 20l-8 60-63 8s-93.6-9.4-169-103c-34.6-43-76.6-239.9-89-355 4.6-8.3 12.1-31.4 17-39 .5 5.4 10.8 107.6 27 193 8.5 45 25.3 75.9 36 93 36.8 46 110.2 123 251 123z"
			opacity={0.502}
		/>
		<path
			fill="#bfbfbf"
			d="M1393 915v69s-5.2 28-17 64c-13.2 40.3-35.5 91.3-88 139-51.9 43.1-118.7 80.2-219 100 15-14 20-21 20-21s28-3.2 65-18c49.9-20 117.1-58.2 162-110 29-27.7 53.3-116.8 65-182 5.4-35.8 12-41 12-41zm-753 7c15.2 90.3 39.1 172.3 69 213 78.8 87.1 177 122.1 226 131 11 9.8 21 22 21 22s-39-4.3-88-23c-63.2-24.2-143.8-70.6-188-144-15.8-26.2-40.2-132.5-42-144-1-15.7-1.6-61.7 2-55z"
		/>
		<path
			fill="#d9d9d9"
			d="m1077 1250 12 16-22 21-10-19 20-18zm-126 1 17 18-10 19-22-22 15-15zm4-50v19l-64 9v-20l64-8zm119 0 64 8-1 21-63-10v-19z"
		/>
		<path
			fill="#e6e6e6"
			d="m1079 1251 11 16s55.9-10.9 116-43c41.4-22.2 84.9-59.3 113-90 26.8-36.2 41.6-82.8 50-120 10.4-45.9 18.5-82.1 19-94 3.6 3.2 4-1 4-1l1-69-11-2s-44.4 227.3-78.6 261.6c-36.2 44.5-88.9 82.3-165.4 100.4-.2 7.1-1 19-1 19l-58 3v19zm-21 18 9 18s-50.7 9.1-108 1c4.7-10 10-19 10-19h89zM638 914c12.5 63.8 26.8 147.5 62 208 22.8 39.3 90.9 81.9 105 92 36.5 20.7 74.3 40 130 52 7.7-8.6 15-16 15-16l-1-19-57-2s-.4-10.5-1-19c-74.7-13.6-136.6-60.8-164-98-29.5-41.2-40.6-88.3-52-132-18.3-70.3-26-130-26-130s-11.8-83.6-14-96c-6 12.4-10.5 26.3-12 32 2 16.9 8.6 76.3 15 128z"
		/>
		<linearGradient
			id="ch-topdown18003e"
			x1={599.4867}
			x2={728.4867}
			y1={1109.5605}
			y2={1082.3995}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.197} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003e)"
			d="M701 637h-27s-12.7 58.9-60 166c6.7 61.7 22.2 176.3 32 210 9.8 33.7 97-293 97-293l-42-83z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown18003f"
			x1={477.187}
			x2={477.187}
			y1={1158.7271}
			y2={1652.0068}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.498} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#dedede" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003f)"
			d="M257 396c8.8-11.4 30.9-35.4 74-50s80.1-24 175-24c37.7 0 151.2 8.8 160 12s27.7 13.9 30 40 1 60.7 1 74c0 6.2 0 46.8-6 99-7 60.8-23.2 140.5-74 248-12.8 27.7-45.5 18.9-60 19s-156-17-156-17-77.7-11.6-93-16c8.6-17.3 30-52.7 46-109s35.8-145.1 37-225-33-80-48-80-50.9 7.6-86 29z"
		/>
		<path
			fill="#dedede"
			d="M391 449c0 154.7-69.2 307.2-83 332-10.5-2.4-71.2-22.2-91-55s-13-83.5-13-95 10.7-104 14-122c3.3-18 12.6-88.2 38-110 13.6-9.6 43.3-31 84-31s44 24 47 35 4 32.9 4 46z"
		/>
		<linearGradient
			id="ch-topdown18003g"
			x1={150.3436}
			x2={462.0676}
			y1={1193.9084}
			y2={1015.8163}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b8b8b8" />
			<stop offset={0.502} stopColor="#dedede" />
			<stop offset={1} stopColor="#b8b8b8" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003g)"
			d="M202 663s-25 37.4-31 60-3.3 38.5-2 50 12.3 50.9 33 73 49.1 62 110 62c6.5 0 51.5-3.6 81-18 13.2-6.4 20-11 20-11l66-73s-123-10.8-172-25c-27.9-8.1-66.2-20.5-87-51-15.7-23-18-67-18-67z"
		/>
		<path
			d="m112 1011 56-3-130 51-12 190-19-11 13-185 92-42zm230 2v13s76.3 36.9 76 37-8-13-8-13l-68-37z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown18003h"
			cx={302.725}
			cy={1144.246}
			r={302.068}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#999" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18003h)"
			d="M172 714C58 777.3 68 894.7 117 953c40.7 23.9 51.3 32.8 98 37s105.2 2.2 130-18c4.5-3.7 37.1-30.7 48-82-17.7 6.2-38.7 17-84 17s-104.9-38.2-129-97c-22.1-55.3-8.4-86.5-8-96z"
		/>
		<linearGradient
			id="ch-topdown18003i"
			x1={5.4699}
			x2={440.6699}
			y1={786.6642}
			y2={726.6621}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#999" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003i)"
			d="M115 953c8.6 6 38.1 30 99 37s118.5-5.3 131-18c-1 25.6-24 392-24 392l-232-11 3-70 60 31 19-307-59 3s2.7-37 3-57z"
		/>
		<path fill="#ccc" d="m1075 1220 53 9-50 2-3-11zm-121 0-6 11-54-2 60-9z" />
		<path
			fill="#ccc"
			d="m293 1123-77-2 1 169 81 3-16 17s-6-12.2-32-13-36.7 3.6-38 5c-.3 5.1-1 15-1 15l-19-1 8-204 83 3 10 8zm-82 215c0-4 8.3-7 33-7s37 4.6 37 9-32.1 4-37 4-33-2-33-6z"
		/>
		<linearGradient
			id="ch-topdown18003j"
			x1={210.0053}
			x2={282.0053}
			y1={601.2064}
			y2={602.4635}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003j)"
			d="M211 1302c8.7-3.4 55.4-13 71 7-.1 10.8 0 30 0 30s-9.1-8-37-8-35 6-35 6 .5-23.4 1-35z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown18003k"
			x1={0.9966}
			x2={414.9966}
			y1={531.0789}
			y2={538.305}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003k)" d="m1 1350 414 21-22 50-384-20-8-51z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003l"
			x1={50.9967}
			x2={328.9966}
			y1={327.7659}
			y2={332.618}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003l)" d="m51 1557 278 16-22 50-240-12-16-54z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003m"
			x1={80.9963}
			x2={286.9963}
			y1={225.4114}
			y2={229.0074}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003m)" d="m81 1663 206 9-24 51-166-9-16-51z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003n"
			x1={109.9967}
			x2={241.9967}
			y1={124.54}
			y2={126.8439}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003n)" d="m110 1766 132 8-22 49-94-4-16-53z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003o"
			x1={140.9953}
			x2={197.9953}
			y1={26.273}
			y2={27.268}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003o)" d="m141 1871 57 3-31 42-26-45z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003p"
			x1={21.9968}
			x2={372.9968}
			y1={429.6202}
			y2={435.7462}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003p)" d="m22 1453 351 17-21 52-314-16-16-53z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003q"
			x1={53.9973}
			x2={347.9973}
			y1={481.091}
			y2={486.2231}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003q)" d="m54 1404 294 16-18 49-265-14-11-51z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003r"
			x1={73.9973}
			x2={312.9973}
			y1={379.071}
			y2={383.2431}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003r)" d="m74 1507 239 14-18 50-210-12-11-52z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003s"
			x1={94.9976}
			x2={277.9976}
			y1={277.0422}
			y2={280.2363}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003s)" d="m95 1613 183 8s-16.2 49.2-16 49-157-7-157-7l-10-50z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003t"
			x1={121.9973}
			x2={238.9973}
			y1={175.136}
			y2={177.178}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003t)" d="m122 1716 117 6-18 50-90-5-9-51z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003u"
			x1={144.9977}
			x2={201.9455}
			y1={73.6334}
			y2={74.6275}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.499} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown18003u)" d="m145 1820 57 2-15 50-36-1-6-51z" opacity={0.302} />
		<linearGradient
			id="ch-topdown18003v"
			x1={1407.677}
			x2={1323.771}
			y1={1038.2489}
			y2={1005.5359}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003v)"
			d="M1393 992V850s-30.5-1.2-43-14-11-26.2-11-32c-11 16.9-86.1 169.5 54 188z"
		/>
		<linearGradient
			id="ch-topdown18003w"
			x1={1469.4849}
			x2={1469.4849}
			y1={1070}
			y2={749}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003w)"
			d="M1393 850c17.8 0 106.7 5.8 118 5 0 2.4.3 8.5 0 21 8.1-.8 26.1.7 35 2-.1 99-1.5 175-2 237-6 1-8.7 1.3-14 2-3 31-5 54-5 54h-130s-2.3-307.8-2-321z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown18003x"
			x1={504.5182}
			x2={504.5182}
			y1={1118}
			y2={747}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003x)"
			d="m393 889 19-9s63.6-70.7 67-74c9 1.2 90.3 10.4 103 10s27.4-7 32-14c1.4 15.8 20.6 178.8 32 210 1.3 21.9 1 160 1 160l-235 1 8-108-12-15-46-26v-68s29.1-40.5 31-67z"
			opacity={0.4}
		/>
		<path
			fill="#bfbfbf"
			d="M392 1279h241l-11-23-56-20-19-43h-70s-10.4 27-17 43c-10.1 3.7-43.2 14.8-57 20-6 12.3-11 23-11 23zm1131-85-41-1-19 43-56 20-10 22h119l7-84z"
		/>
		<path
			fill="#a6a6a6"
			d="M1525 1170h-131l-2-186s-9.6 62.4-41 129c17.2 54.2 26.8 93.8 83 132 17.5-4.9 30-9 30-9l17-42h42l2-24zm-1113 2h236s-4.5-165.3-6-194c6.1 28.7 26.3 123.7 43 150-12.1 37.4-32.2 87.4-86 119-13.5-4.3-33-9-33-9l-17-43-71-1-19 42-33 12-19-14 5-62z"
		/>
		<linearGradient
			id="ch-topdown18003y"
			x1={1525}
			x2={1370}
			y1={711.6275}
			y2={711.6275}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003y)"
			d="M1422 1171h103l-2 22-41 1-18 42-31 10c.7.3-41.3-27.3-63-75 8.3-.6 52 0 52 0z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown18003z"
			x1={1438}
			x2={1438}
			y1={857.694}
			y2={596.694}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003z)"
			d="M1525 1170h-131l-2-186s-9.6 62.4-41 129c17.2 54.2 26.8 93.8 83 132 17.5-4.9 30-9 30-9l17-42h42l2-24z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown18003A"
			x1={545.5}
			x2={545.5}
			y1={835.7939}
			y2={601.7939}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003A)"
			d="M412 1172h235v-158s9.3 52.9 37 116c-20.4 52.6-28.8 78.8-85 117-17.5-4.9-33-11-33-11l-18-42h-72l-16 42-33 12-20-13 5-63z"
			opacity={0.502}
		/>
		<path fill="#ccc" d="M1089 964s-144.5-1-154-1c8.8-12.4 14.7-17.2 34-40 7.3.1 85 0 85 0l35 41z" />
		<path fill="#545454" d="m975 1106 78 1 11 17 10 77v19l5 12-3 18-18 19h-89l-18-19-2-19 7-12-2-22 11-72 10-19z" />
		<path fill="#fff" d="M1065 1155v52H965v-53l100 1z" opacity={0.102} />
		<path fill="#fff" d="M1064 1154v-30l-11-18-78 1-11 19 1 28h99z" opacity={0.251} />
		<linearGradient
			id="ch-topdown18003B"
			x1={1080.847}
			x2={980.8336}
			y1={763.5}
			y2={763.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.43} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={0.569} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003B)"
			d="M964 1205c-.2-33.3 1-80 1-80l10-19 78 1 11 17v83s-71.5-.7-100-2z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown18003C"
			x1={950.9996}
			x2={1074.9792}
			y1={681.4442}
			y2={683.6078}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#555" />
			<stop offset={0.576} stopColor="#3e3e3e" />
			<stop offset={0.649} stopColor="#909090" />
			<stop offset={0.7} stopColor="#575757" />
		</linearGradient>
		<path fill="url(#ch-topdown18003C)" d="m965 1206 100 1s5.5 22.8 10 42c-7.2 8.4-17 20-17 20h-89l-18-19 14-44z" />
		<radialGradient
			id="ch-topdown18003D"
			cx={1030.303}
			cy={741.309}
			r={26}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" />
			<stop offset={0.5} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18003D)"
			d="M1030 1152c14.4 0 26 11.6 26 26s-11.6 26-26 26-26-11.6-26-26 11.6-26 26-26z"
			opacity={0.702}
		/>
		<path
			d="m969 924-36 38s4-50.4 4-80V612c0-20.2 5-436.5-31-509 8.5-6.6 22-18 22-18s78.9-29.8 167-1c6.4 5.2 21 21 21 21s-8.7 39.8-15.5 115.9c-7.1 79.5-12.5 198.5-12.5 353.1 0 69.5 3 388 3 388h-5l-32-39-85 1zm-2-368-16 18 1 114 16 17-1-149zm90-1-2 151 18-19 1-113-17-19zm487 561s1.4-182.7 1.8-239.1c5.2.2 252.8.2 260.2.1v240l-262-1zm263 245.6c-.2 64-1 363.4-1 363.4l-262 1 1.9-364.4c3.1 0 254.5-.1 261.1 0zM1699 254c-46.7-4.4-116.3-6-135-6-29.1 0-80 2-80 2l1-246 382-1-1 259-123 1s-18.2-6.5-44-9z"
			className="factionColorPrimary"
		/>
		<radialGradient id="ch-topdown18003E" cx={1614.5} cy={638.5} r={302.5004} gradientUnits="userSpaceOnUse">
			<stop offset={0.3473} stopColor="#000" />
			<stop offset={0.7687} stopColor="#fff" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18003E)"
			d="m1512 854-2 22 36 1-2 239 262-1V877l37-1-15-207s-19 93.7-106 144c-93.3 56.3-210 41-210 41z"
		/>
		<path
			d="M1090 964s-2.1-89.2-2-101c0-6.7-2.2-221.1 2.5-421.8 3.6-153.6 11.7-299 25.5-337.2-7.2-8.3-13-15.3-20-20-23.8 59.4-31.5 212.8-36.1 376-5.1 179.7-5.9 371.4-5.9 463 16.3 18.8 36 41 36 41zm-156.8-4.8c.6-18.7 2.9-86.1 2.8-96.2 0-6.7 2.3-221.1.5-421.8-2-214.4-15.7-299-29.5-337.2 7.2-8.3 13-15.3 20-20 23.8 59.4 31.5 212.8 36.1 376 5.1 179.7 5.9 371.4 5.9 463-12.7 14.6-27.5 31.4-33.4 38.1-.7.1-2.4-.6-2.4-1.9z"
			opacity={0.102}
		/>
		<linearGradient
			id="ch-topdown18003F"
			x1={1011.5}
			x2={1011.5}
			y1={996}
			y2={1848.5369}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003F)"
			d="M1095 84c-88.1-28.8-167 1-167 1s12.4 30.1 21 93c10.8 78.9 14 196.6 18 345 3.4 127 1.7 171.8 2 197s-1 204-1 204l86-1s1-179.7 1-211c0-17.2 3.1-164.6 7-316 3.7-140.9 12.1-252 33-312z"
			opacity={0.102}
		/>
		<linearGradient
			id="ch-topdown18003G"
			x1={1012.5}
			x2={1012.5}
			y1={1407.8595}
			y2={1847.9226}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003G)"
			d="M1058 493s31 44.4 31-2c0-248.3 27-386 27-386s-12.2-16.8-20-21c-80.2-23.9-140.2-5.4-167 0-7.8 4.2-20 21-20 21s11.5 43.6 19 126c5.8 64 8 151.4 8 259.9 0 46.4 31 2 31 2 2.4 0 91 .1 91 .1z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown18003H"
			cx={839.847}
			cy={1432.823}
			r={807.49}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.399} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#999" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18003H)"
			d="M1117 104c106 15.9 167 105.4 187 157-11.9 7-22.8 13.2-23 27 0 10.5 20.7 72 32 143 13.5 85.4 20 179.7 20 209-7.6 24.7-26.2 86.7-67.9 140-47.3 60.5-119.1 118.5-174.3 131.5-1.9-36.8-8.2-306.6 1.3-523.5 6.9-142.3 13.8-261.1 24.9-284z"
		/>
		<radialGradient
			id="ch-topdown18003I"
			cx={1151.0179}
			cy={1417.973}
			r={806.671}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.399} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#999" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18003I)"
			d="M906.6 104.2c-66 8-110.5 51.6-143.6 98.8-44.9 64-66.4 136.4-71 152 1.2 10.1 9 22.5 6 103 9.9-.7 24.1-2.9 30 2 11.7 11.3 26.3 122-26 176-3.6 1.2-7.8 1.8-10 2 7.6 24.7 24.2 88.7 66 142 46.6 59.5 120.6 116.7 175.9 130.9 4.7-135.8 8-376.7.8-561.2-4.7-122.6-15.8-220.2-28.1-245.5z"
		/>
		<linearGradient
			id="ch-topdown18003J"
			x1={758.6342}
			x2={670.6342}
			y1={1532.0851}
			y2={1508.9331}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.799} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003J)"
			d="M734 465s-6.6-114.7-35-132c-2.4 5.5-6.5 15.6-9 21 4.8 12.2 8.7 19.2 7 103 3.6-.1 14.3 1.3 23 1 7.4-.3 14 7 14 7z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown18003K"
			x1={1239.201}
			x2={1800.4067}
			y1={1367.5}
			y2={1367.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18003K)"
			d="M1280 286c5.4-12.6 13.3-38 116-38 51.6 0 197 1 197 1s147.5-1.8 187 29c47.3 36.8 61 26.2 61 146 0 17.6-1 95.1-1 121s-3.9 99.7-20 149c-17.4 53.2-93.3 163-264 163-9.3 0-26.2-1.5-45.8-2.4-48.5-2-113.9-4.6-125.2-5.6-15.8-1.4-46-4.8-46-53 0-225.8-22.4-367.5-40-443-9.4-40.3-20.9-62.6-19-67z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown18003L"
			cx={1639}
			cy={1403}
			r={88}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.198} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#ccc" />
		</radialGradient>
		<path
			fill="url(#ch-topdown18003L)"
			d="M1639 429c48.6 0 88 39.4 88 88s-39.4 88-88 88-88-39.4-88-88 39.4-88 88-88z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1516.2 1279.1c-2.7 34-7.2 81.9-7.2 81.9h37l-1 363h261l1-363h35l-19-245h-17V875h37l-15-209s13-56.7 13-175c0-56.1 2.7-140.2-9-159 10.4-21.5 36-71 36-71l-1-258h-382v232s-25.2 7.8-34 14c-23.2-.8-101.5-2.3-146 11-9.4-18-35.4-82.2-105-126-18.4-11.6-40.3-19.3-65.1-27.2-2.9-.9-18.9-3.8-18.9-3.8m-209 0c-18.9 3-137.6 11.9-216 252-7.6-16.6-21.7-19.4-24-20m-310 2c-38.5 13-108.5 26.6-130 126s-21.5 161.2-24 197c-15.3 23.8-29 52-29 52s-91 40.8-91 143c0 63.1 33 95 33 95l-4 60-92 42-13 186 85 48-3 67-87-4 7 51 45 3 10 51-41-1 15 52 36 1 11 53-34-2 16 53 28 2 11 51-25-1 15 52 26 1 9 52-20-1 15 52 19 1 7 52-11-1s25.4 47.4 26 47 32-44 32-44l-12-1 15-50 18 1 23-50-21-1 17-51 24 1 24-51-25-1 17-49 29 1 22-50-34-2 18-50 38 2 22-52-43-2 18-49 45 2 22-51-95-6 4-77s82.7-43.5 82-43 1-10 1-10 7 5.7 19.5 12.5m169.9.8c6.3-4.3 12.1-8.8 18.6-13.3 30.9-21.6 56.7-63.1 68.8-106.5 43.4 71 172.8 163.5 326.2 163.5 118.5 0 288.1-76.8 339-181 7.6 35 28.6 101 84 135.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M670 336c-112.2-17.5-239.6-20.1-315 2m-101 61c33.1-22.2 129.6-68.4 136 21 4.5 63.2-13.8 237.6-83 362m-10-3c-10-14.7-92.1-77.9-85.4-229M691 354c5.5 16 8.7 61.6 6 102s-3.9 160.3-52 269c-8.6 24.6-28 71-28 71s-5.3 22-44 20-125.6-13.3-154-16-83.4-10.3-104-16c-13.4-3.7-63.3-15.1-92-49-15.3-18.1-20-46.3-20-75"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M391 447s126.8-19 307-1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M174 713c-4.9 12.8-25.5 86.7 53 160s186 6 186 6l68-73M293 906c-7.6 40.8-13.9 73.1-49 86"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M394.5 889.7C365.8 984.3 322.3 992 247 992c-75.8 0-127.2-34.2-133-42m-3 61 61-5s-.3 1.1-.8 1.3C159.8 1011.8 38 1059 38 1059l-18-6m18 7-12 189 126 64 20-306m174-36-21 317m82-49 13-175-77-38m-1-14 68 38 9 14"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M362 957v66m50 149h235v-154m-1-6H362" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m189 1358 2-42 25-27s.1-168 0-170c-1.7-.7-16.6-7.1-17-8s7 0 7 0l8-120m68 318c-22.8-22.7-71-7-71-7s-1 24.9-1 36c31.9 12.6 71 2 71 2v-30l17-16-1-172-16-7 9-124m-85 121 74 3m17 8-81-3m66 219c0-8.8-71-11.6-71-2m87-44-81-3m-17-178-8 204 20 1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m319 1364-232-11m69 467s-1.3-20.2-3.3-52.1m-3.2-52.2c-1-15.8-2.1-32.8-3.2-50.5m-3.2-50.6c-1.1-17.4-2.2-35-3.3-52.4m-3.3-53.3c-1.1-18.2-2.2-35.6-3.3-51.8m-3.1-50.3c-1.9-30.6-3.1-49.8-3.1-49.8m82 3s-1.4 19.6-3.8 50.7m-3.7 50.7c-1.2 15.9-2.4 32.9-3.7 50.7m-4 53c-1.2 16.8-2.5 33.9-3.8 50.7m-3.7 51.1c-1.3 17.7-2.6 34.8-3.7 50.6m-3.8 51.7c-2.3 31.2-3.8 50.8-3.8 50.8m174-401-298-16m13 51 269 14m-260 38 240 14m-230 39 213 11m-204 42 188 9m-177 42 160 7m-143 45 120 6m-110 46 93 5m-78 47 58 3m-15 50-37-1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M685 1130s-2.7-5.6-4-8c-10.5-20.8-31.2-80.9-44-152-5.6-31.2-22.1-136.6-23-169m732 316c21.6-37.5 41.6-103.7 46.5-145.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1090.5 911.1c49.4-14.3 210-107.8 243.2-278.2m2.3 49.1c1.8 41.8-90.8 210.5-246.1 256"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M934.2 912c-47.6-13-209.1-105.7-243.8-275.9m-14.4.9c4.5 0 24.8.6 25 0s38-29.2 38-107-15-72-15-72h-27m236.5 480.6c-146-42.9-236.7-194.9-245.3-247.5-.5-3.3-7.2-34.6-9.2-56.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M673 644s15.9 2.1 4 38h-15" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1356 843c-5.2 33.3-25.3 125.6-69 176-53.6 61.8-112.9 102-226 102-3.6-6.3-8-15-8-15h-79s-6.8 12.5-10 18c-80.7 0-177.1-39.6-240-113-45.7-53.4-55.4-154.1-57-167s-12-96.5-16-131"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1012 1105V965" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M691.7 952.9c1.1-44.6 14.7-154.8 24.3-194.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1323.5 959.7c-.6-41.8-13.4-148.4-22.9-192.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1073 1200c-2.2-19-7.1-68-8.8-76.3-.6-1.3-1.2-1.7-1.2-1.7v39l1 45 12 45s1-.3 1-.8c0-2.1 1-10.4 1.1-19.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M954.5 1199.8c2.2-19.1 7.1-66.3 8.8-74.5.6-1.3 1.2-1.7 1.2-1.7v37.1l-1 45.1-12 45.1s-1-.3-1-.8c0-2.1-1-10.4-1-19.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M958.3 1287.5c-8.4-8.1-23.3-22.5-23.3-22.5l16-15s9.6 10.9 18.1 19h88.9l19-20 13 17s-17.3 16.7-21.3 20.3c-.5.4-1.7.7-1.7.7l-8-17m-91 0-10 18m106-82H963m2-53h98"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1012.7 1254.5c1.6 0 2.3-1.6 2.3-1.6s9.2-14.1 12.3-22.5c3.1-8.4-4-7.9-4-7.9h-22.1s-6.6-.1-3.5 8.4c3.1 8.5 12.6 22.5 12.6 22.5s.9 1.1 2.4 1.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1089 1266s106.3-8.6 224-125c41.6-46.8 60-145 75-222-8.2-17.8-15-33-15-33s-33.5 176.8-70 223c-35.8 45.2-90.6 84.7-166 100m235-316c1.6-12.6 7.7-39.1 9-46"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M938.8 1266.5s-106.4-8.6-224.1-125.1c-41.6-46.8-60-145.2-75-222.1C635.9 896 626 819.7 623 780m267.8 429.5c-75.4-15.4-130.3-54.8-166.1-100.1-36.5-46.2-70.1-223.1-70.1-223.1s-2.8-17.9-5.6-37.3c-.5-3.2-11-69-14-99"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1073 1201v19l64 9v-20l-64-8z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1137 1229-59 2-5-11" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M955.5 1200.8v18.8l-64 8.9v-19.8l64-7.9z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m891.5 1228.5 59 2 5-10.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1012.5 934.3c13.9 0 25.2 4.3 25.2 9.7s-11.3 9.7-25.2 9.7-25.2-4.3-25.2-9.7 11.3-9.7 25.2-9.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1808 1361h-264m-35 0 21-245h288m-274 2 2-242m259.5 0H1510l2-22s259.5 33.5 316-187m6-331c-5.4-18.4-40.9-74.8-138-82-126-9.3-210.8-4-244-4m416 12s-108.7-1-129-1m-254-29v15"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1524 1171h-131l-1-321s58.2 5 119 5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1393 1009h151" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1395 850c-9-.9-29.8-4-38-9s-18-18.1-18-41c0-22.7-2.8-363.3-58-509-.7-1.8-.4-6 0-8 6-15.1 21.7-24.2 33-26"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1639 429c48.6 0 88 39.4 88 88s-39.4 88-88 88-88-39.4-88-88 39.4-88 88-88z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1639.1 457.8c32.7 0 59.3 26.5 59.3 59.3 0 32.7-26.5 59.3-59.3 59.3-32.7 0-59.3-26.5-59.3-59.3s26.5-59.3 59.3-59.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1638.5 442.7c40.8 0 73.8 33 73.8 73.8s-33 73.8-73.8 73.8-73.8-33-73.8-73.8 33-73.8 73.8-73.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1116.2 103.5c-17.1 58.9-25.9 251.2-28.4 438.9-2.8 212.7 2.1 419.6 2.1 419.6H932c3.2 0 6.3-136.2 5-341-.1-14.2 0-85.8 0-100 .5-185.5-8.2-366.3-30-416"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1088 963-33-40h-85l-37 40m-26-859c7.6-7.4 15.7-17.2 24-20m165-1c7.5 7.3 14.1 13.8 21 22M968 926s10.4-698.4-34.2-827.9C932 92.7 930 88.3 928 85c65.7-21.5 121.4-13.9 168-2-2.3 5.1-17.3 53.9-23 112-3.8 38.1-14.8 184.6-18 508-.8 74.9-1.1 144.5-2 221"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m968 706-16-18-1-114 16-20" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1057 706 15.8-17.9 1.7-114.2-15.5-20.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1523.7 1193.5H1481l-17 42-57 20-11 23s59.1-.2 120.8-.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1464 1234.5-18 44" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m634 1278.5-11-23-57.2-20-17-42-71.6-.5-17 42-57.1 20-11 23 241.9.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m564.8 1234.5 18 44" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m460.1 1234-18 44" />
	</svg>
);

export default Component;

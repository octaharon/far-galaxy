import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-topdown84000a"
			x1={937.5}
			x2={937.5}
			y1={657}
			y2={702}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#adadad" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-topdown84000a)" stroke="#1a1a1a" strokeWidth={10} d="M908 1777v45h59v-44" />
		<path
			fill="#f2f2f2"
			fillRule="evenodd"
			d="M937 251s-7.3-5-33-26c-70.8-.7-264 16-264 16l-85-83s-10.1 38 8 65 54 80.8 63 94 41.9 21.2 50 25 146 51 146 51 20.1 5.4 22 37-17.3 325.9-106 448-370 191-370 191v-41l-24-116H203l-25 118v310s10.5 25 102 25c53.8 0 86-17.1 86-26s1-138 1-138 297 59.8 351 124 56.5 156.5 62 195 58.4 265 158 265c95.1 0 138.6-192.7 150-232 12.3-42.5 22.5-158.3 54-211 40-66.9 363-144 363-144l1 142s2.6 24.3 95 24c79.4-.2 93.2-20.5 95-26 0-9.6-1-308-1-308l-25-118h-140l-25 119s1.5 25.8 1 37c-33.8 3.9-292-89.2-370-192s-106-390.9-106-447c-.5-37 18.1-34.1 24-37s164-55 164-55 28.4-8.4 38-30 55-89 55-89 14.4-31.2 6-59c-29.2 24.5-84 84-84 84s-178.9-17.3-260-17c-34 23.7-36 25-36 25z"
			clipRule="evenodd"
		/>
		<linearGradient
			id="ch-topdown84000b"
			x1={274.0017}
			x2={274.0017}
			y1={244.0249}
			y2={-207.9}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.25} stopColor="#f2f2f2" />
			<stop offset={0.75} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000b)"
			d="M181 1341.1c15.6 31.3 169 31.1 187-4 .1-34.5-2-307-2-307l-24.6-118H204.6L180 1029s.8 267 1 312.1z"
		/>
		<linearGradient
			id="ch-topdown84000c"
			x1={1147.7762}
			x2={936.7762}
			y1={-409.4535}
			y2={-362.5594}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a1a1a" />
			<stop offset={0.492} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000c)"
			d="M938 1091h159s-.8-199 52-199c-24.4-20.1-52.8-85.9-73.9-163.2-38.9-142.4-60.2-323.9-37.1-330.8-28.9-2.5-100-10-100-10v703z"
		/>
		<linearGradient
			id="ch-topdown84000d"
			x1={1602.0017}
			x2={1602.0017}
			y1={244.0249}
			y2={-207.9}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.25} stopColor="#f2f2f2" />
			<stop offset={0.75} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000d)"
			d="M1509 1341.1c15.6 31.3 169 31.1 187-4 .1-34.5-2-307-2-307l-24.6-118h-136.8l-24.6 117c0-.1.8 266.9 1 312z"
		/>
		<linearGradient
			id="ch-topdown84000e"
			x1={722.5486}
			x2={852.2686}
			y1={269.3136}
			y2={236.9706}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={0.456} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000e)"
			d="M816 1651c40.5-28.4 37.1-438.2 34-560-29.5.1-38.4-.3-74 0 2.2 81.3-.6 218.8-54 237 70.2 80.2 41.4 213.2 94 323z"
		/>
		<linearGradient
			id="ch-topdown84000f"
			x1={1175.7788}
			x2={1043.7769}
			y1={277.0939}
			y2={241.7239}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.597} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000f)"
			d="M1054.8 1650c-40.6-28.4-37.2-438.1-34.1-559.9 29.6.1 38.5-.3 74.2 0-2.2 81.2 2.6 219.7 56.1 237.9-52.5 66.5-43.5 212.2-96.2 322z"
		/>
		<linearGradient
			id="ch-topdown84000g"
			x1={824.5805}
			x2={949.5805}
			y1={581.746}
			y2={509.5769}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#434242" />
			<stop offset={0.301} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000g)"
			d="M841 1547c-1.4 19.1-11.6 101.7-27 105 8.6 21.4 44.6 133 125 133-1.1-102.3-.7-193 0-259-49.8-34.2-38.3-8.7-98 21z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-topdown84000h"
			x1={1049.4305}
			x2={931.4305}
			y1={582.8828}
			y2={514.7558}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#494949" />
			<stop offset={0.367} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000h)"
			d="M1034 1548.1c1.4 19.1 7.6 98.7 23 102-8.6 21.4-46 135.4-118 134.9 1.1-102.2.7-192.7 0-258.6 28.7.3 53.6-.4 95 21.7z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-topdown84000i"
			x1={528.7267}
			x2={588.7227}
			y1={202.6962}
			y2={10.6633}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.526} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000i)"
			d="M367 1200s271.4 49.5 354 127c54.8-14.2 55.9-145.4 57-192-23.2-.2-180.5.3-409 0-.2 34.9-2 65-2 65z"
		/>
		<linearGradient
			id="ch-topdown84000j"
			x1={493.0574}
			x2={622.2734}
			y1={-220.3972}
			y2={22.6218}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.611} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000j)"
			d="M368 1069c177.2-42.8 313.6-125.5 360-177 20.7-1.4 34.6 44.2 40 82s9 87.7 9 161H367s.4-31.4 1-66z"
		/>
		<linearGradient
			id="ch-topdown84000k"
			x1={1342.0988}
			x2={1290.6508}
			y1={203.2964}
			y2={11.2924}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.526} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000k)"
			d="M1506 1197s-112.2 24.5-218 63c-50.9 18.5-105.1 41.8-132 67-54.8-14.2-55.9-145.4-57-192 23.2-.2 178.5.2 407 0 .2 34.9 0 62 0 62z"
		/>
		<linearGradient
			id="ch-topdown84000l"
			x1={1372.674}
			x2={1264.4879}
			y1={-221.4961}
			y2={21.492}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.611} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000l)"
			d="M1508 1069c-177.2-42.8-313.6-125.5-360-177-20.7-1.4-34.6 44.2-40 82s-9 87.7-9 161h410s-.4-31.4-1-66z"
		/>
		<path
			fill="#b3b3b3"
			d="M556 159s36 34.9 85 82c-1.2 17.2-3.2 32-5 81 123 28 302 63 302 63s180.8-35.3 302-63c-.4-38.6-3.9-55-5-80 82.6-88 83-83 83-83s7.5 25.4-4 53c-5.6 13.4-22 36.4-36 57-12.3 18.2-21.7 43.9-37 56-69.4 30.8-127.2 46-200 72-57.9-4.5-103-8-103-8l-106 9s-14.2-7.1-44-17c-40.4-13.5-97.5-34-129-46-3.1-1.2-17.6-3.9-27-13-15.4-19.9-53.8-75.3-70-101-15.8-35.1-6-62-6-62z"
		/>
		<linearGradient
			id="ch-topdown84000m"
			x1={1058.0023}
			x2={814.9685}
			y1={387.3371}
			y2={387.3371}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.313} stopColor="#e6e6e6" />
			<stop offset={0.606} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000m)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M934 1425c24.3 0 63.3-.8 97 34 27.8 28.7 27 79 27 87s-1.8 32.7-18 39c-34.3 13.3-74-6-106-6-28.3 0-81.4 15.5-101 5s-18-37.5-18-42 1.3-64 29-86 47.4-31 90-31z"
		/>
		<linearGradient
			id="ch-topdown84000n"
			x1={729.1892}
			x2={938.1892}
			y1={-406.2769}
			y2={-358.0249}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.458} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown84000n)"
			d="M834 398c13.4-1.7 71.5-6.6 103-9-35.6 6-65.2 146-72 246s-18.4 287.1-14 461c-46.7-.3-76 1-76 1s6-191.6-47-205c60.1-69.4 88.5-202.5 103.6-313.1 12.8-93.1 19.9-164.4 2.4-180.9z"
		/>
		<path
			fill="#fff"
			d="M985 1458.9c.3 0 .5.3.7.7 0 9.8.2 28.6.3 34.3.2 6.3-.4 13.7 0 32-3 .1-4.3-10.4-6-32.8 1-13.8 1.9-19.8 5-34.2z"
		/>
		<path
			d="m384 1126 393-39s-.1 74.9-2 88c-18.2-.8-379.7-20.3-390-21-.9-6.4-1-28-1-28z"
			className="factionColorPrimary"
		/>
		<path
			d="m1485.9 1126-390-39s.1 74.9 2 88c18.2-.8 375.7-19.3 386-20 .9-6.4 2-29 2-29z"
			className="factionColorPrimary"
		/>
		<path
			d="M439 1159c9.7.5 343 18 343 18s-4.9 58.8-9 70c-18.6-3.1-273.5-34.2-321-52-18.1-7.9-12.7-21.9-13-36z"
			className="factionColorSecondary"
		/>
		<path
			d="M1440.4 1158c-9.7.5-343.4 18-343.4 18s4.9 58.8 9 70c18.6-3.1 273.9-34.2 321.4-52 18.2-7.9 12.8-21.9 13-36z"
			className="factionColorSecondary"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M937 251s-7.3-5-33-26c-70.8-.7-264 16-264 16l-85-83s-10.1 38 8 65 54 80.8 63 94 41.9 21.2 50 25 146 51 146 51 20.1 5.4 22 37-17.3 325.9-106 448-370 191-370 191v-41l-24-116H203l-25 118v310s10.5 25 102 25c53.8 0 86-17.1 86-26s1-138 1-138 297 59.8 351 124 56.5 156.5 62 195 45.5 265 158 265c95.1 0 138.6-192.7 150-232 12.3-42.5 22.5-158.3 54-211 40-66.9 363-144 363-144l1 142s2.6 24.3 95 24c79.4-.2 93.2-20.5 95-26 0-9.6-1-308-1-308l-25-118h-140l-25 119s1.5 25.8 1 37c-33.8 3.9-292-89.2-370-192s-106-390.9-106-447c-.5-37 18.1-34.1 24-37s164-55 164-55 28.4-8.4 38-30 55-89 55-89 14.4-31.2 6-59c-29.2 24.5-84 84-84 84s-178.9-17.3-260-17c-34 23.7-36 25-36 25z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m368 1069-1 133" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m179 1029 190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M386 1064v140" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M385 1155c79 4.2 391 21 391 21" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M433 1158c-1 11.4-2.3 28.5 9 34 27.9 13.6 104.2 21.8 324 54"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1506 1069-1 131" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1695 1029-190-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M938 250v136" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m640 240-7 84" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m633 323 304 61s.2 2.1 0 4c-21.8 2.6-107 9-107 9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1234.1 240 7 84" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1241.1 323-304 61s-.2 2.1 0 4c21.8 2.6 107 9 107 9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1485 1064v140" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1488.5 1155c-79.8 4.3-390.5 21-390.5 21" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1441 1160c1 11.4 2.3 26.5-9 32-27.9 13.6-104.2 21.8-324 54"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M727 891c37.3 0 48.1 106.6 50.2 214.3 2.2 109.8-15.9 220.7-58.2 220.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1147.1 891c-37.3 0-48.1 106.6-50.2 214.3-2.2 109.8 15.9 220.7 58.2 220.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M815 1651c28.1.6 40.4-269.4 35.5-517.6-4.1-211 2.4-721.6 88.5-746.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1058.9 1651.5c-28.1.6-40.5-269.4-35.6-517.6 4.1-211.1.6-721.7-85.6-746.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M817 1519c22.5-52.3 188.7-79.4 240.6.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M822.6 1569.8c19.1-54.4 191.5-66.5 229.1 0" />
	</svg>
);

export default Component;

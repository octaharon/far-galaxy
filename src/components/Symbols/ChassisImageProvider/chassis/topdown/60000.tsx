import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<filter id="ch-topdown60000k">
			<feGaussianBlur stdDeviation={25} />
		</filter>
		<path
			fill="#f1f1f1"
			d="M754.3 1355.1c-7.2-10.1-66.7-92.1-80.1-49-22.9 74.3-30.5 319.8-67.1 416-10.3 27.1-64.4 78.4-70 84-5.7 5.6-28.8 15.2-42 3-98.2-88.4-63-893.7-63-925 0-31.3 2.9-144.6 97.1-180 163.3-61.7 230.2-182 230.2-182L605.2 390v89c0 12.1 3.9 29-20 29s-22-16.7-22-26V230c0-12.1-.2-27 21-27s21 9.2 21 24v83s72.9 3 188.1 15c0-47.8-3.1-187 167.1-187S1128 266.5 1128 325c115.1-12 188-15 188-15v-83c0-14.8-.2-24 21-24s21 14.9 21 27v252c0 9.3 1.9 26-22 26s-20-16.9-20-29v-89l-154 132s66.9 120.3 230 182c94.1 35.5 97 148.7 97 180s35.1 836.6-63 925c-13.3 12.2-36.3 2.6-42-3s-59.7-56.8-70-84c-36.5-96.2-44.1-341.8-67-416-13.3-43.1-72.8 38.9-80 49s-206.5 346-206.5 346-198.9-335.8-206.2-345.9zm82.1-851.6c0 69.1 56 125.1 125.1 125.1s125.1-56 125.1-125.1-56-125.1-125.1-125.1-125.1 56-125.1 125.1z"
		/>
		<path
			d="M961.5 380.4c69.1 0 125.1 56 125.1 125.1s-56 125.1-125.1 125.1-125.1-56-125.1-125.1 56-125.1 125.1-125.1z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown60000a"
			x1={1288}
			x2={630.9243}
			y1={759.4667}
			y2={759.4667}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.5} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000a)"
			d="M960.5 1700c9-5.9 65.4-110.9 130.5-218 51.3-84.4 100.8-167.8 128-184 49.7-94.8 69-355 69-643-47.3-11.6-196.8-33-249-33-47.4 21.8-99 37.4-153-1-67.5 6.1-210.6 19.8-255 38 0 46.8-5.1 473.4 68 638 61 27.8 252.5 408.8 261.5 403z"
		/>
		<path
			fill="#ccc"
			d="M1326 1731c35.7 7.8 21.7-252.2 7-521-12.3-225.8-23.9-458.1-18-542-7.1-2.5-16.9-12.2-27-15-6.2 124.1 8.4 468.9-68 640 35.7-7 33.4 39.5 43 99 20.8 128.5 24.4 337.3 63 339zm-735 1c48.7 4.2 61.3-409.3 91-437 8.9-2.6 18-4 18-4s-62.2-39.7-72-638c-17.4 10.4-23 15-23 15s5.2 238.2-16 536c-11.4 258.4-31.4 519.5 2 528z"
		/>
		<linearGradient
			id="ch-topdown60000b"
			x1={567.1736}
			x2={847.1736}
			y1={301.3454}
			y2={468.1923}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000b)"
			d="M960 1698v-61c-5.2-8-13.7-23.2-24.8-43.1-2.4-4.3-12.6 1.8-15.3-2.9-2.8-5.1 2-21-1-26.5-61.7-111.7-170.6-305-239-326.5 5.5 30.7 17.3 53.3 21 59 27.8 17.3 69.8 83.4 115 156 67.4 117.9 144.1 245 144.1 245z"
		/>
		<linearGradient
			id="ch-topdown60000c"
			x1={1074.3446}
			x2={1354.3447}
			y1={467.6662}
			y2={296.6872}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.503} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000c)"
			d="M960 1698v-61c5.2-8 13.7-23.2 24.8-43.1 2.4-4.3 12.6 1.8 15.3-2.9 2.8-5.1-2-21 1-26.5 61.7-111.7 170.6-305 239-326.5-5.5 30.7-17.3 53.3-21 59-27.8 17.3-69.8 83.4-115 156-67.4 117.9-144.1 245-144.1 245z"
		/>
		<linearGradient
			id="ch-topdown60000d"
			x1={1358}
			x2={1315}
			y1={1564.5}
			y2={1564.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000d)"
			d="M1336 202c8.2 0 22 .6 22 34v251c0 20.7-16.5 22-21 22s-22-5.1-22-21V238c0-14.1 4.2-36 21-36z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown60000e"
			x1={606}
			x2={563}
			y1={1564.5}
			y2={1564.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000e)"
			d="M584 202c8.2 0 22 .6 22 34v251c0 20.7-16.5 22-21 22s-22-5.1-22-21V238c0-14.1 4.2-36 21-36z"
			opacity={0.302}
		/>
		<path
			fill="#e5e5e5"
			d="M1128 324c15.5-1.6 187-15 187-15l-1 82s-163.7 155.6-179 143-34.9-100-7-210zm-333.1 0c-15.5-1.6-186.9-15-186.9-15l1 82.1s163.6 155.8 178.9 143.2c22.7-11.4 34.9-100.2 7-210.3z"
		/>
		<linearGradient
			id="ch-topdown60000f"
			x1={1354.745}
			x2={1212.834}
			y1={1311.708}
			y2={1485.708}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.496} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown60000f)" d="m1315 362-197 111s2.5 61.3 20 63c55.2-38.7 178-144 178-144l-1-30z" />
		<linearGradient
			id="ch-topdown60000g"
			x1={1217.3977}
			x2={1212.8688}
			y1={1581.7849}
			y2={1629.7849}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.503} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000g)"
			d="M1128 324c15.3-1.2 180.1-14.1 187-14 .1 13.3 1 26 1 26l-197 22s2.9-30.1 9-34z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown60000h"
			x1={707.3756}
			x2={716.3167}
			y1={1576.1932}
			y2={1652.1932}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.503} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000h)"
			d="m608 308 187 18s9.9 25.4 12 58c-46.5-4-204-40-204-40l5-36z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown60000i"
			x1={707.1988}
			x2={596.8248}
			y1={1486.8027}
			y2={1311.8027}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.503} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000i)"
			d="M606 390c13.8 14.5 162.5 145 179 145s26-46.9 26-59c-19.7-16.4-206-116-206-116s-2.2 12.8 1 30z"
		/>
		<path
			d="M601 1733c-18.7 27.9-58.1 82-93 82s-63.1-130.6-71-263c-3.5-58.9-38.9-714.2 23-790s69-46.1 145-93c7.6 77.9 0 307.3-15 534-18.2 274.6-41.6 544.7 11 530zm718 0c18.7 27.9 58.1 82 92.9 82 34.8 0 63.1-130.6 70.9-263 3.5-58.9 38.9-714.2-23-790-61.8-75.8-68.9-46.1-144.8-93-7.6 77.9 0 307.3 15 534 18.1 274.6 41.6 544.7-11 530z"
			className="factionColorSecondary"
		/>
		<path
			d="M601 1733c-18.7 27.9-58.1 82-93 82s-63.1-130.6-71-263c-3.5-58.9-38.9-714.2 23-790s69-46.1 145-93c7.6 77.9 0 307.3-15 534-18.2 274.6-41.6 544.7 11 530zm718 0c18.7 27.9 58.1 82 92.9 82 34.8 0 63.1-130.6 70.9-263 3.5-58.9 38.9-714.2-23-790-61.8-75.8-68.9-46.1-144.8-93-7.6 77.9 0 307.3 15 534 18.1 274.6 41.6 544.7-11 530z"
			opacity={0.102}
		/>
		<path
			d="M632 656c36.2-10.4 204.5-34 249-34-55.9-60.2-60-73.6-60-115s26.1-139 139.5-139S1101 471.2 1101 507s-14.2 76.8-56 114c73.8 6.4 191.1 18 246 37-31.7-42.4-66.9-38.5-129-135-8.6 8-50 54.9-50-96 0-65.6 17-100.3 17-107s22.3-182-168.5-182C818.8 138 793 232.1 793 323c18.6 69.8 30.2 212-9 212-12.2-2.7-27-13-27-13s-2.7 27-125 134z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-topdown60000j"
			x1={632.0496}
			x2={1290.814}
			y1={1513.4064}
			y2={1524.9053}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown60000j)"
			d="M632 656c36.2-10.4 204.5-34 249-34-55.9-60.2-60-73.6-60-115s26.1-139 139.5-139S1101 471.2 1101 507s-14.2 76.8-56 114c73.8 6.4 191.1 18 246 37-31.7-42.4-66.9-38.5-129-135-8.6 8-50 54.9-50-96 0-65.6 17-100.3 17-107s22.3-182-168.5-182C818.8 138 793 232.1 793 323c18.6 69.8 30.2 212-9 212-12.2-2.7-27-13-27-13s-2.7 27-125 134z"
			opacity={0.502}
		/>
		<path fill="#666" d="m806 1292 46 60-2 36-47-64 3-32zm308 0-46 60 2 36 47-64-3-32z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M754.3 1355.1c-7.2-10.1-66.7-92.1-80.1-49-22.9 74.3-30.5 319.8-67.1 416-10.3 27.1-64.4 78.4-70 84-5.7 5.6-28.8 15.2-42 3-98.2-88.4-63-893.7-63-925 0-31.3 2.9-144.6 97.1-180 163.3-61.7 230.2-182 230.2-182L605.2 390v89c0 12.1 3.9 29-20 29s-22-16.7-22-26V230c0-12.1-.2-27 21-27s21 9.2 21 24v83s72.9 3 188.1 15c0-47.8-3.1-187 167.1-187S1128 266.5 1128 325c115.1-12 188-15 188-15v-83c0-14.8-.2-24 21-24s21 14.9 21 27v252c0 9.3 1.9 26-22 26s-20-16.9-20-29v-89l-154 132s66.9 120.3 230 182c94.1 35.5 97 148.7 97 180s35.1 836.6-63 925c-13.3 12.2-36.3 2.6-42-3s-59.7-56.8-70-84c-36.5-96.2-44.1-341.8-67-416-13.3-43.1-72.8 38.9-80 49s-206.5 346-206.5 346-198.9-335.8-206.2-345.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M630 660c38-17 170.5-30.8 256.6-37.5m154.2-.2c82.7 5.9 203.3 18.1 247.3 36.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M604 1726c-76 70.7 3.3-501.9 3.3-1059.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1317.3 1726.1c75.5 70.7-3.3-501.9-3.3-1059.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M683 1295c-14.9-39.1-46.1-87-94-87s-161 68-161 68" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1237.7 1295.2c14.9-39.2 46.2-87.2 94.3-87.2s162 66.3 162 66.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M701 1298c-22.2-56.8-70-141.6-70-645" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M641 987c33.4-17.8 99.6-39.4 129-39" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1280 987c-33.4-17.8-99.6-39.4-129-39" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1217.7 1298.2c22.3-56.8 70.3-141.6 70.3-645.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M960.5 1644c-4-9.5-17.7-34.7-26.5-51-3-5.5-11.4-21.2-16.2-29.4-68.4-117.8-165.9-301.7-242.8-329.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M961 1644c4-9.5 17.7-34.7 26.5-51 3-5.5 11.4-21.2 16.2-29.4 66.7-114.6 160.9-291.8 236.9-327"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1127 325c-6.7 15.9-14 71-14 101s2.9 115.3 29 109c10.7-2.6 22-15 22-15"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M794 325c14.2 20.5 33.9 210-11 210-7.4 0-30-18-30-18" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960.5 1699V895" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M893.5 1586.2V1363l50.5-54" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1026 1587v-223.5l-50.4-54.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1316 309v92" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M605 314v84" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M563 487h41" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M563 474h41" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M563 224h41" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M563 237h41" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1316 487h41" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1316 474h41" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1316 224h41" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1316 237h41" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M961.5 365.9c77.1 0 139.6 62.5 139.6 139.6s-62.5 139.6-139.6 139.6-139.6-62.5-139.6-139.6 62.5-139.6 139.6-139.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M961.5 380.4c69.1 0 125.1 56 125.1 125.1s-56 125.1-125.1 125.1-125.1-56-125.1-125.1 56-125.1 125.1-125.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m851 1391 1-37-47-64-1 34 47 67zm219 0-1-37 47-64 1 34-47 67z"
		/>
		<g id="Animation">
			<path
				fill="#e6e6e6"
				d="M962 696.6c110.1 0 199.4 89.3 199.4 199.4s-89.3 199.4-199.4 199.4-199.4-89.3-199.4-199.4S851.9 696.6 962 696.6z"
			/>
			<path
				d="M781.7 911.3c0-33.3 9.7-88.1 41.7-126.6 33.8-40.7 91.2-64.1 137.1-64.1 89.5 0 179.2 63 179.2 179.5s-99.8 156-99.8 156c0 3.7-1 11-1 11l-18 41h-6l-13 485h-83l-15-485h-6l-16-42-1-9s-32.8-10.5-63.7-55.8c-30.4-50.5-35.5-90-35.5-90z"
				filter="url(#ch-topdown60000k)"
				opacity={0.5}
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M962 696.6c110.1 0 199.4 89.3 199.4 199.4s-89.3 199.4-199.4 199.4-199.4-89.3-199.4-199.4S851.9 696.6 962 696.6z"
			/>
			<path
				fill="#f1f1f1"
				d="M960.5 718c125.9 0 180.8 92.3 180.4 180.5-.4 76.6-61.6 146.9-100.9 157.5 0 3.7-1 11-1 11l-18 41h-6l-13 485h-83l-15-485h-6l-16-42-1-9s-92.8-47-99-151c-6.1-102.7 74.4-188 178.5-188z"
			/>
			<radialGradient
				id="ch-topdown60000l"
				cx={964.9158}
				cy={1033.5184}
				r={169.4924}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.101} stopColor="#fff" />
				<stop offset={1} stopColor="#000" />
			</radialGradient>
			<path
				fill="url(#ch-topdown60000l)"
				d="M962.5 717c18.7 0 51.7 2.2 86.5 22 46.4 26.4 94 79.5 94 156 0 114.7-98.9 160-101 160 0-9-.8-22.3 0-40-4.9-12.7-26.6-48-79.5-48-43.4 0-74.5 25.3-77.5 41-1.2 26.9-2 45.5-2 48-63-34.7-98.4-98.2-96.1-160 3.5-91.4 71.9-181.1 175.6-179z"
				opacity={0.149}
			/>
			<linearGradient
				id="ch-topdown60000m"
				x1={1040}
				x2={881}
				y1={881.0441}
				y2={881.0441}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000004" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path
				fill="url(#ch-topdown60000m)"
				d="m1022 1109 18-43v-50c-25.4-71.6-159-52.5-159-3v55l18 41h123z"
				opacity={0.2}
			/>
			<path fill="#e6e6e6" d="m937 1122 44 2-10 27-24-1-10-28z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M881 1060v-48c0-9 24.2-44 79.5-44s79.5 39.5 79.5 49v41"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M882 1065h157" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M905 1108h110" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1040 1056c0 3.7-1 11-1 11l-18 41h-6l-13 485h-83l-15-485h-6l-16-42-1-9"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M935 1110v11l14 29h22l12-28v-13" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M936 1122h47" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M799 967c3.8-7 13.2-21.4 20-27 4.2 8.1 9 17 9 17s-10.9 13.6-17 31"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M881.1 1055.4c-57.7-29.6-97.1-89.7-97.1-158.9 0-98.6 79.9-178.5 178.5-178.5S1141 797.9 1141 896.5c0 70.9-41.4 132.2-101.3 161"
			/>
			<circle id="Animation-Coordinates" cx={960.5} cy={896.5} r={0.5} fill="#0ff" />
		</g>
	</svg>
);

export default Component;

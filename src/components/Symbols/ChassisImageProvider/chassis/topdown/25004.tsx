import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#e6e6e6"
			d="M975.3 1596.9c106.3 0 160.2-55.6 218.4-129.5 43.2-54.9 71.2-106.3 71.2-106.3l2 184 300-1V339l-302-1v381l-15-11s-16.8-147.1-76-213c-54.3-60.5-164.5-63.9-205-64h-18.1c-40.4.1-150.6 3.5-205 64-59.2 65.9-76 213-76 213l-15 11V338l-302 1v1205l300 1 2-184s28 51.5 71.2 106.3c58.3 74 101.1 129.5 218.4 129.5 5.5.1 25.7.1 30.9.1z"
		/>
		<path
			fill="#d9d9d9"
			d="M1180 1484c-5.9-12.4 30-175.2-27-343-21.5.5-110.9 73.7-200 76-96.8 2.5-193-66-193-66s-43.1 108.3-21 332c49.5 68.9 112.8 120.4 221 114 40.1-2.3 131.8 7.5 220-113z"
		/>
		<path
			fill="#b3b3b3"
			d="M959 464c25.5 0 114.5.6 129 77 3.3 90.3 2.4 197-127 197-85.7 0-114.3-48.8-124-92s-15.6-134 21-154 75.5-28 101-28z"
		/>
		<path
			fill="#bfbfbf"
			d="M1250 707c-37.5-298.8-181.1-259.2-290-277-250.5 2.2-266.4 132-290 278 57.5-28.2 72.9-116.9 125.2-183.6C827.1 483.7 867.1 446 961 446c102.6 0 147.3 47.7 180 102 39.4 65.5 58.9 140.9 109 159z"
		/>
		<path
			fill="#bfbfbf"
			d="M1567 1544v-131h-301l1 133 300-2zm-912-130-2 131-300-1v-130h302zm1-1076-1 218-302 1V338h303zm610 1 302 1-1 216-301-1V339z"
		/>
		<path
			fill="#ccc"
			d="m1569 1411-303 2v-54s69-98.3 69-268c0-26.3 5-324.4-70-373-.5-76.1 2-162 2-162l299 1 3 854zm-983-284.8c8.5 148 68 232.8 68 232.8v54l-297.7-2-2-856 298.7 1s2.5 85.9 2 162c-25.7 16.7-42 62.7-52.4 117.1-16 1.2-30.5 1.9-30.5 1.9l-4-155h-41l-86 155-37-1-1 291 39 1 88 150 40-1-2-151c-.1 0 11.6-1.5 17.9.2z"
		/>
		<linearGradient
			id="ch-topdown25004a"
			x1={592.4197}
			x2={592.4197}
			y1={638}
			y2={792}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown25004a)"
			d="M571 1282s-4-153.3-3-153 20-1 20-1 10 127.3 29 154c-22.4-.1-46 0-46 0z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown25004b"
			x1={1326.4855}
			x2={1326.4855}
			y1={638}
			y2={792}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown25004b)"
			d="M1347.8 1282s4-153.3 3-153c-1 .3-19.9-1-19.9-1s-9.9 127.3-28.9 154c22.3-.1 45.8 0 45.8 0z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown25004c"
			x1={612}
			x2={612}
			y1={1082.9575}
			y2={1242}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown25004c)"
			d="M570 836V678h84v40s-42.4 33.7-54 119c-12.6.3-30-1-30-1z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown25004d"
			x1={1307}
			x2={1307}
			y1={1082.9875}
			y2={1242}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown25004d)"
			d="M1349 836V678h-84v40s42.4 33.7 54 119c12.6.3 30-1 30-1z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown25004e"
			x1={959.9492}
			x2={959.9492}
			y1={373}
			y2={1200}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.36} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown25004e)"
			d="M852 720c55.6 57.7 173.4 45.6 216 0 1.8 12.4 13.5 71.4 28.8 151.9 21.6 113.7 81.9 324.8 85.2 386.1 7.7 117.2 6.2 214.1-7 234-21.2 27.3-37 42.4-57 54-114.2 0-256.6 1-316 1-19.2-5.9-45.2-42-60-59-7.6-75.6-14.6-188.7-1-254 9-53.3 82.7-348.6 111-514z"
			opacity={0.2}
		/>
		<path
			d="M1061 1137c15.3-8.7 49-69 49-69h45s3.2 5.1 2 10c21.7-19.8 48-79 48-79s53.1-136.5-5-337c-5.8-8-26.8-49.8-48-91s-41.3-65.5-67-83c47.6 52.5 13.9 170.8 5 196-9.1 33.4-61 64-61 64s-53.7 21.8-137 0c-44.9-18.4-61-61-61-61s-54.7-152.6 10-204c-15.8 10.6-42.9 32.9-67 71-30.5 48.2-57 113-57 113s-25 96.9-25 217 68 194 68 194l1-10s25-1.1 37 1c6.8 30.3 38.2 63.3 56 75-3.1 45.8-5 114-5 114l49-41 122-2 48 40s-6-102.6-7-118z"
			className="factionColorSecondary"
		/>
		<path
			d="M895 1217s-4.4-341.7 1-373c-15.3 71.5-54.7 298.8-48 411 15.4-12.4 47-38 47-38zm126 0s3.9-372.7 4-379c3.5 7.6 19 96.9 26 191s18 228 18 228l-48-40z"
			opacity={0.149}
		/>
		<path
			d="M656 1125s-25-182.1-23-290c-42-.3-62 0-62 0l-2-153-39 1-89 151-40 1 3 291 40 1 84 150 43 1-3-150 88-3zm607 0s25-182.1 23-290c42-.3 62 0 62 0l2-153 39 1 89 151 40 1-3 291-40 1-84 150-43 1 3-150-88-3z"
			className="factionColorSecondary"
		/>
		<path
			fill="#d9d9d9"
			d="M1104 1575c-4.3 24.4-31.2 109.7-40 122-109.4.7-224 0-224 0s-38.5-91.2-40-123c-6.2-30.2-40.7-185.2-38-505h35s31.9 62.8 55 78c-.6 30.6-2 110-2 110l21 1 24-16-1 12s46.1 10 67 10 62-13 62-13l2-9 22 17 21-2-8-119s41.4-39.9 50-70c14.4-.4 45 1 45 1s.2 324.1-51 506z"
		/>
		<linearGradient
			id="ch-topdown25004f"
			x1={958.5}
			x2={958.5}
			y1={68.08}
			y2={695.4223}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown25004f)"
			d="M1104.9 1572.9c42.7-197.5 52.1-501.9 52.1-501.9l-46-1s-24.4 45.2-62 78c-27 23.6-64.2 38-89 38-119 0-164-116-164-116s-19.5-2.2-36-1c0 189.2 15.1 371.5 38 491 .8 4.4 1.7 8.8 2.6 13 7.5 38.3 26.7 105.5 39.4 122 28.4 1 225 1 225 1s32.6-87.8 39.9-123.1z"
			opacity={0.102}
		/>
		<path fill="#ccc" d="m872 1259 38-28 99-1 39 29 21-2-47-39-126-1-47 41 23 1z" />
		<linearGradient
			id="ch-topdown25004g"
			x1={1025}
			x2={894}
			y1={672.9559}
			y2={672.9559}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown25004g)"
			d="m1025 1252-1-10-14-11-102 1-14 12 1 12s32.1 8 65 7c32-1 65-11 65-11z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown25004h"
			cx={1010.76}
			cy={1188.681}
			r={719.901}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0.2} />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</radialGradient>
		<path
			fill="url(#ch-topdown25004h)"
			d="M853 722c-22.3-14.2-70.4-154.6-25-231-66.5 54.4-91.4 153.7-123 189s-61.5 35.3-80 75c-6 8.9-24.2 43.2-20 83 .2 1.9 25.8-4 26-2 8.8 163.9 10.2 176.9 26 289 .1 1.7-69.2-1.4-69.1.2 2.6 34.1 6.8 69.2 13.1 104.8 21.4 92.7 91 197.9 139 255-4-56.2-7.2-109.7-8-144-8-101.9 96.2-444.7 121-619z"
		/>
		<radialGradient
			id="ch-topdown25004i"
			cx={895.933}
			cy={1273.897}
			r={717.98}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0.2} />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</radialGradient>
		<path
			fill="url(#ch-topdown25004i)"
			d="M1068 722c22.2-14.2 70.3-154.6 24.9-231 66.3 54.4 91.2 153.7 122.8 189 31.6 35.3 61.4 35.3 79.8 75 6.3 9.3 20.4 44.6 22.8 80.9-.1 1-32.3.1-32.4 1.1-7.3 69.1-13.2 174.3-22 290-.1.9 68.4-.3 68.4.6-2.6 33.3-6.8 67.7-12.9 102.4-21.4 92.7-90.8 197.9-138.7 255 4-56.2 7.2-109.7 8-144 8.1-101.9-95.9-444.7-120.7-619z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1106.9 1556.9c32.1-22.9 59-54.1 86.9-89.5 43.2-54.9 71.2-106.3 71.2-106.3l2 184 300-1V339l-302-1v381l-15-11s-16.8-147.1-76-213c-54.3-60.5-164.5-63.9-205-64h-18.1c-40.4.1-150.6 3.5-205 64-59.2 65.9-76 213-76 213l-15 11V338l-302 1v1205l300 1 2-184s28 51.5 71.2 106.3c22.9 29 43.4 55.2 66.9 76.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960.2 1262.8c.4 111.4 1 311.2 1 311.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1566 1413h-301" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1567 556h-301" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1262 1365c12.9-22.7 64.1-91.7 72-239m-14.4-290.8c-10.5-56.7-27.7-98.7-55.6-117.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M959.8 757.9c-72.8 0-150-19.1-150-202 0-45.2 26.7-109.2 145-110.9h9.4c119 1.5 145.8 65.6 145.8 111 0 182.9-77.2 202-150 202"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M959.9 737.8c-69.1-1.5-132-22.2-130.1-177.5.5-43.5 16.3-91.7 124.1-96.9 2-.1 10.7-.1 12.9.1 107 5.4 122.7 53.4 123.2 96.8 1.9 155.4-61 176-130.1 177.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M354 1412.8h300.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M477 1543v-355m50-500.5V556m863 0v127.6m50 509.1V1543" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M353 556h300.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M657.9 1364.8c-12.9-22.7-63.9-91.4-71.9-238.3m14.6-292.8c10.5-56 27.7-97.4 55.3-115.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M718 660c-20.1 70.3-66.2 295.6 42.5 418" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1200.9 660c20.3 70.9 66.9 299.3-45.2 421" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1179 1482c17.3-165.8 5.3-237.1-23.6-356.2m-14.4-58.5c-19.9-81-45.1-186.3-73-345.3m-216.2-.1c-27.8 159-53 264.2-73 345.2m-18.2 74.7c-26.1 109.7-36.2 181.8-19.7 340 .1.6 1.1 7.3 1.1 7.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1253 710c-80.2-34.6-87.7-197.6-207.4-250.4m-172.6 1c-119.5 52.9-127 215.2-207 249.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M852.4 1146.3c-35.6-31.2-53.8-70.6-54.4-78.1-19.4.2-28.7 0-37 0 0 68.2-.5 491.2 80 626.8 69.9.1 112 0 112 0l113 1s90-198.5 90-627.8c-26.9-.3-46 0-46 0s-17 36.3-49.5 68.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M802 1573h303" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M633 835H403v292l254-1s-10.4-62.6-16-130c-6.4-77.4-8-161-8-161z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M442 1128s77.3 133 87 150h41v-152" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M442 833s77.3-132.9 87-150h41v152" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1286.9 835H1517v292l-254.1-1s10.4-62.6 16-130c6.4-77.4 8-161 8-161z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1478 1128s-77.3 133-87 150h-41v-152" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1478 833s-77.3-132.9-87-150h-41v152" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M850 1258c-.6 0-1.2-.2-1-1-.2-14.8-1.2-197.9 48-423 15.3 10 40.7 14 63 14s57.5-7.8 65-14c13.8 5.1 41.5 374.9 44 424h-22l-37-27H908l-37 27h-21z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m849 1256 49-40h121l50 39" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M897 834c-7.7 181-1 352.1 0 382m-2 26v12s76.9 22.6 129-3c.2-3.4 0-7 0-7m0-405c1.4 26.7-2.5 322.3-3 376"
		/>
		<g id="Animation">
			<path
				fill="#f2f2f2"
				d="M1232 1896h189l33-1122h18l38-98-3-424-91-235-180-1-92 235v116s-50.6-8.6-116.6-11.8c0-19.1-3.7-29.4-5.4-53.2-2.2 0-29.8-23-51-39-6.8 0-18.2-.5-27.3-.4C925.6 277.8 910 292 898 303c-1.6 21.8-5.7 44.9-5.5 52.2C797.7 359.2 654 372 654 372s-25.4-136-134-136c-98.4 0-126.6 126.3-134 153s-14 359-14 451c48.8-.4 298 1 298 1l11-182s122.3 25 209 35c.9 9.9-.2 30.8 0 52 16 26.9 54 82.2 58 88 4.5 0 15.2.8 28 0 7.5-11.9 42.7-68 55-90-.1-28.6-1.6-41.3-.3-49.6 49.4-7.6 97.7-19.3 112.3-22.4 10.8 30.4 43 102 43 102h13l33 1122z"
			/>
			<path fill="#e3e3e3" d="m1145 251 91-236 180 3 90 232-361 1z" />
			<path
				d="M653 371c-10.3 6.4-44.8 24.6-43 141 .4 26.7 9.5 99 65 122 .7 9.3 1 24 1 24s145.2 28.6 215 35c.1-50.6 0-336 0-336s-198.1 11.3-238 14zm374-14c3 44 5.7 304.3 4 336 29.9-4.3 197.2-35 285-71 82.9-27.8 162-59 162-59s17-31.6 17-54-18-57-18-57-283.9-97.7-450-95zm142-105 21-53 45 1 14 52h-80zm114 2 14-54 64 1 14 51-92 2zm128-2 8-52 45-2 19 54h-72z"
				className="factionColorPrimary"
			/>
			<radialGradient
				id="ch-topdown25004j"
				cx={962.8976}
				cy={-1087.875}
				r={532.7469}
				gradientTransform="matrix(1 0 0 0.3631 0 918.7114)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.2991} stopColor="#f2f2f2" stopOpacity={0} />
				<stop offset={1} stopColor="#000" stopOpacity={0.5} />
			</radialGradient>
			<path
				fill="url(#ch-topdown25004j)"
				d="M958.2 703.3c152-8.5 444-95.3 516-139 7.4-11.4 34.5-47.3 2-112-9.1.1-278.4-103.5-518-96-91.6 2.9-224.3 7.8-302 17-7.7 1-21.4 13.8-32.2 29-12.4 39.7-14.6 98.8-14.1 107.7 2.2 40.3 10.1 100 64.5 124.2 3 8.1 4.8 16.1 4.8 24.1 26.3 16.3 202.7 34.2 279 45z"
				opacity={0.7}
			/>
			<path fill="#e6e6e6" d="m1275 807 104 3-25 59-52 1-27-63z" />
			<path fill="red" d="M513 844h129v19l-128 2-1-21zm-109 0h76l-.6 19-74.8 2-.6-21z" />
			<linearGradient
				id="ch-topdown25004k"
				x1={1510}
				x2={1144}
				y1={1195}
				y2={1195}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#a3a3a3" />
				<stop offset={0.496} stopColor="#e6e6e6" />
				<stop offset={1} stopColor="#a3a3a3" />
			</linearGradient>
			<path fill="url(#ch-topdown25004k)" d="m1144 677 366-2-37 98-286 2-43-98z" opacity={0.8} />
			<path
				fill="#e0e0e0"
				d="M1181 444c21.6 0 76 16.7 76 71s-48.2 72-71 72c-20.8 0-71-18.1-71-74s44.4-69 66-69zm-36 70c0 42 35 45 40 45s44-6.2 44-39c0-43-32-47-44-47s-40 12.7-40 41z"
			/>
			<path
				d="m557 748-21 1-1-165s-42.7-.6-42 0 1 164 1 164l-20 1V544l21-1 1 16s5.3 10 19 8 18-9 18-9l1-14 20 2 3 202z"
				opacity={0.149}
			/>
			<linearGradient
				id="ch-topdown25004l"
				x1={534}
				x2={495}
				y1={1427}
				y2={1427}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.496} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown25004l)"
				d="M512 419c7.5 0 22 3.5 22 16v124s-6.8 8-20 8-19-5-19-12V433s3-14 17-14z"
				opacity={0.302}
			/>
			<path
				fill="#dedede"
				d="M975 831c18.7-24.5 42.2-63.7 53-83 4.2-45.6 4-181.1 4-241 0-42.1-5.9-146-9.5-204.1-15.6-13.7-37.7-32.5-49.6-40.9.2 23-2.8 570.1 2.1 569zm-31.8-569c-13.7 11.5-35.9 30.6-46.2 41-2.6 35.3-7 98.7-7 208-1.7 197.2 1 236.8 0 236-.5-.4 39.9 63.4 58 87 0-3.5-.4-572-4.8-572z"
			/>
			<path fill="#f2eded" d="M945 261h27l1 571h-27l-1-571z" />
			<linearGradient
				id="ch-topdown25004m"
				x1={960.7235}
				x2={960.7235}
				y1={1085.999}
				y2={1658}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.496} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown25004m)"
				d="M943.2 262h29.7c11.9 8.4 34.1 27.3 49.6 40.9 3.5 58.1 9.5 162 9.5 204.1 0 59.9.2 195.4-4 241-10.8 19.3-34.3 58.5-53 83-7.8 1.8-19.5 3-27 3-18.1-23.6-58.5-87.4-58-87 1 .8-1.7-38.8 0-236 0-109.3 4.4-172.7 7-208 10.3-10.4 32.6-29.5 46.2-41z"
				opacity={0.2}
			/>
			<linearGradient
				id="ch-topdown25004n"
				x1={1511}
				x2={1143}
				y1={1456.5}
				y2={1456.5}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown25004n)"
				d="M1144 674c24 .7 367 1 367 1l-5-422-362-1-1 114s219.2 40.1 332 86c18.9 37.2 30.5 61.9 2 110-57 22.8-245.2 91-333 112z"
				opacity={0.2}
			/>
			<linearGradient
				id="ch-topdown25004o"
				x1={1326.6646}
				x2={1145.6646}
				y1={1750.2622}
				y2={1819.9962}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.2} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path fill="url(#ch-topdown25004o)" d="M1327 252V18l-92-2-89 236h181z" opacity={0.4} />
			<linearGradient
				id="ch-topdown25004p"
				x1={1508.3214}
				x2={1327.3214}
				y1={1818.056}
				y2={1752.178}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.803} stopColor="#000" stopOpacity={0} />
			</linearGradient>
			<path fill="url(#ch-topdown25004p)" d="M1327 252V18l92-2 89 236h-181z" opacity={0.4} />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M890.4 694.7C803.2 682.9 681 659 681 659l-6-2m356.3 37.3c49.2-7.6 97.3-19.2 111.7-22.3 10.8 30.4 43 102 43 102h13l33 1122h189l33-1122h18l38-98-3-424-91-235-180-1-92 235v116s-57.3-6.6-115.9-10.4m-136.3-.3C795.3 361.2 654 372 654 372"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1454 774h-72.2c.3 14.1.2 32 .2 32l-28 63-54 1-27-64-1-32h-85"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1379 808h-104" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1267 774h116" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1509 676h-364" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1506 253-362-1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1168 253 22-54h46l14 53m36 0 12-53 64 1 14 53m35 0 7-54h45l20 52"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1436 63-217-1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1140 673c47.3-6.4 261.6-75.6 337-110 12.2-23.5 18-41.7 18-54 0-12.4-9.4-50.9-18-56s-236.6-74.9-339-87"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1495 512h-237.8m-142.3 0h-82.8m-144.7 0H607" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1186 444c39.2 0 71 31.8 71 71s-31.8 71-71 71-71-31.8-71-71 31.8-71 71-71z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1186.5 471.9c24.1 0 43.6 19.5 43.6 43.6s-19.5 43.6-43.6 43.6-43.6-19.5-43.6-43.6 19.5-43.6 43.6-43.6z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M533 543h22v205h-82V543h22.2c0 5.4-.2 15-.2 15s.8 9 19 9 19-8.2 19-11v-13z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M495 543V434s.2-15 18-15 20 12 20 15v109" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m473 546 20 36v164m42 1V582l18-38m-19 38h-40" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M650 373c-9.1-48.8-30.5-138-129-138S391.3 349 382 420s-12 421-12 421h302s4.7-126.4 3-207c-29.6-13-65-49-65-130s9.7-106.1 40-131z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M400 845v18h78v-19" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M512 845v18h128v-19" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1021 301c-.7-.6-26.9-22.9-48-40.4-8.9-.1-18.1-.1-27-.2-21.5 18-47 39.6-47 39.6s-9 9.9-9 245c0 157.5 1 202 1 202l56 86h28l55-88s2-95.2 2-186c0-201.7-10.7-255.9-11-258z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M947 832V262" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M972 832V262" />
			<circle id="Animation-Coordinates" cx={960.5} cy={577.5} r={0.5} fill="#18ff00" />
		</g>
	</svg>
);

export default Component;

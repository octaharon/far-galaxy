import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M26.6 1868c35.9 0 306.8-402.7 354.8-419.5 16.2 32.2 30.9 61.8 30.9 61.8l147.2-1.5-5.9-61.8s193.9 12.8 304.7 184c23 119.4 49.6 220.8 101.6 220.8 64.3 0 101.6-168.1 101.6-225.2 24.6-41.4 118.2-159.4 303.2-178.1-1.2 27.5-2.9 60.4-2.9 60.4h148.7l29.4-61.8s321.5 418 350.3 418c28.9 0 13.2-494.6 13.2-494.6s20.6-57.4-103-260.5S1556 714 1556 714s2.6-139.1-14.7-139.8-67.7 0-67.7 0-13.7 12.1-13.2 36.8c-33.3-38-194.3-225.2-194.3-225.2l-42.7-297.3h-106l-25 172.2s-46.1-12.2-78-25c-5.9-26.9-30.9-206.1-30.9-206.1h-48.6l-28 206.1-75.1 25-26.5-175.3-104.5 2.9-44.2 294.4-197.3 226.7s1.5-38.3-19.1-38.3-45.3-1.5-53-1.5-25 19.1-25 144.3C299.2 814 84 1183.5 84 1183.5s-69.2 94.6-69.2 237S2.3 1868 26.6 1868z"
		/>
		<path
			d="M359.2 1386.7c.1 2.6 94.2-2.9 94.2-2.9L424 572.6s-19.1-1.8-38.3-1.5c-24.7 11.4-24.1 97.2-26.5 815.6zm1205.6-.8c-.1 2.6-94.2-2.9-94.2-2.9l29.4-810.3s19.1-1.8 38.3-1.5c24.7 11.3 24.1 97.1 26.5 814.7z"
			className="factionColorPrimary"
		/>
		<path
			d="M695.4 1491.2c10-5.6 21.3-4.4 21.3-30.9 0-83.9-149.4-1001 244.8-1001 207.4 0 263.3 261.4 263.3 520.4 0 354.5-39 499.7-8.1 511.8-63 38-128.8 100.7-145.9 115-29.6 23.3-217.3 16.2-217.3 16.2s-72.1-90-158.1-131.5z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-topdown80000a"
			x1={656.5753}
			x2={828.9996}
			y1={-1471.3715}
			y2={-1474.3806}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown80000a)"
			d="m700.7 88.4 103-4.4s13.4 94.5 25.2 177.4C733.5 311 656.5 382.8 656.5 382.8l44.2-294.4z"
			opacity={0.3}
		/>
		<linearGradient
			id="ch-topdown80000b"
			x1={1093.7747}
			x2={1261.5826}
			y1={-1471.697}
			y2={-1474.6262}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown80000b)"
			d="M1115.8 88.4s25-6.9 50.3-8.1c26.2-1.2 52.7 3.7 52.7 3.7l42.7 300.3L1093.8 262l22-173.6z"
			opacity={0.3}
		/>
		<linearGradient
			id="ch-topdown80000c"
			x1={792.0034}
			x2={1126.2001}
			y1={-521.7612}
			y2={-527.5941}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8495b0" />
			<stop offset={0.399} stopColor="#dae3f2" />
			<stop offset={0.604} stopColor="#dae3f2" />
			<stop offset={1} stopColor="#8495b0" />
		</linearGradient>
		<path
			fill="url(#ch-topdown80000c)"
			d="M959.8 510.8c-32.8 0-95.8 25.9-120.7 88.3S792 813.5 792 875.9s9.7 246.7 16.2 343c6.5 96.3 32.2 323.8 50 406.3s39.2 226.7 101.6 226.7 94.4-157.9 104.5-225.2c7.7-50.8 44-335.3 48.6-400.4 4.6-65.1 13.2-268.1 13.2-348.9s-26.9-251.3-55.9-294.4c-36.5-50.7-77.6-72.2-110.4-72.2z"
		/>
		<radialGradient
			id="ch-topdown80000d"
			cx={3441.1973}
			cy={1174.6866}
			r={559.7024}
			gradientTransform="matrix(-0.9991 4.132865e-02 -0.1306 -3.1585 4791.4561 4707.5732)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4654} stopColor="#8495b0" stopOpacity={0} />
			<stop offset={0.6293} stopColor="#fff" stopOpacity={0.5} />
			<stop offset={0.761} stopColor="#8495b0" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown80000d)"
			d="M959.8 510.8c-32.8 0-95.8 25.9-120.7 88.3S792 813.5 792 875.9s9.7 246.7 16.2 343c6.5 96.3 32.2 323.8 50 406.3s39.2 226.7 101.6 226.7 94.4-157.9 104.5-225.2c7.7-50.8 44-335.3 48.6-400.4 4.6-65.1 13.2-268.1 13.2-348.9s-26.9-251.3-55.9-294.4c-36.5-50.7-77.6-72.2-110.4-72.2z"
		/>
		<path fill="#dadada" d="M934.8 318h45.6l-2.9 132.5-35.3 2.9-7.4-135.4z" />
		<path
			fill="#ccc"
			d="m983.4 459.3 25-225.2-23.6-184L970.1 318l13.3 141.3zm-48.6 0-23.6-229.6 22.1-178.1 13.2 262-11.7 145.7z"
		/>
		<linearGradient
			id="ch-topdown80000e"
			x1={1377.8275}
			x2={1387.1777}
			y1={-262.8109}
			y2={-223.0669}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown80000e)"
			d="M1397 1448.5s2.9 11.8-2.9 20.6c-7.2 10.7-23.6 19.1-23.6 19.1l-7.4-39.7h33.9z"
		/>
		<linearGradient
			id="ch-topdown80000f"
			x1={534.744}
			x2={544.0955}
			y1={-262.8718}
			y2={-223.1278}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown80000f)"
			d="M520.4 1448.5s-3.1 11.8 2.9 20.6c7.4 10.7 24 19.1 24 19.1l7.5-39.7h-34.4z"
		/>
		<linearGradient
			id="ch-topdown80000g"
			x1={873.0407}
			x2={1049.4773}
			y1={-257.8043}
			y2={-260.8845}
			gradientTransform="matrix(1 0 0 1 0 1706)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.092} stopColor="#333" />
			<stop offset={0.174} stopColor="#676767" />
			<stop offset={0.286} stopColor="#b3b3b3" />
			<stop offset={0.355} stopColor="#e5e5e5" />
			<stop offset={0.428} stopColor="#b3b3b3" />
			<stop offset={0.497} stopColor="#b3b3b3" />
			<stop offset={0.731} stopColor="#666" />
			<stop offset={0.866} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-topdown80000g)"
			d="M947.9 1641v-387.9s-21.2-6.2-58.7 26.1c-.7 1.9-6.9 44.6-16.2 121.3 15 89.6 30.2 183.6 43.4 232.6 2.2 2.8 3.8 4.6 6.6 6.8 16.8.1 24.9 1.1 24.9 1.1zm27.1-.3v-387.1s21-6.2 58.4 26.1c.7 1.9 6.9 44.5 16 121-15 89.5-30 183.1-43.3 232.1-2.2 2.8-3.8 4.6-6.6 6.8-17.6.1-24.5 1.1-24.5 1.1z"
		/>
		<path
			fill="#ccc"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M802.3 1092.3c7.1-15 30.9-82.4 30.9-82.4V777.3c-12.5-11.5-14.7-21.9-25-41.2-10.2-18.8-17.5 193.9-8.8 356.2 1.9 0 1.7.3 2.9 0z"
		/>
		<path
			fill="#ccc"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1117.8 1092c-7.2-15-31.2-82.4-31.2-82.4V777c12.7-11.5 14.7-21.9 25.3-41.2 10.3-18.8 17.8 193.9 9 356.2-2.1 0-2 .4-3.1 0z"
		/>
		<path
			fill="#ccc"
			d="m359.2 1388.1 51.5 123.6 147.2-5.9-4.4-58.9s-11-2.6-26.5-5.9c-13.4-36.9-114.2-715.1-67.7-830.2-.7-18.5.4-38.3-33.9-38.3 2.8 36.5 25 812.5 25 812.5l-91.2 3.1zm1201.2 0-51.5 123.6-147.2-5.9 4.4-58.9s10.9-2.6 26.5-5.9c13.4-36.9 114.2-715.1 67.7-830.2.7-18.5-.4-38.3 33.9-38.3-2.8 36.5-25 812.5-25 812.5l91.2 3.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M26.6 1868c35.9 0 306.8-402.7 354.8-419.5 16.2 32.2 30.9 61.8 30.9 61.8l147.2-1.5-5.9-61.8s193.9 12.8 304.7 184c23 119.4 49.6 220.8 101.6 220.8 64.3 0 91.7-149.1 103-225.2 24.6-41.4 116.7-159.4 301.8-178.1-1.2 27.5-2.9 60.4-2.9 60.4h148.7l29.4-61.8s321.5 418 350.3 418c28.9 0 13.4-489 13.2-494.6 1.2-11 1.3-70.7-103-260.5C1676.7 906.8 1556 714 1556 714s2.6-139.1-14.7-139.8-67.7 0-67.7 0-13.7 12.1-13.2 36.8c-33.3-38-194.3-225.2-194.3-225.2l-42.7-297.3s-26.5-7.4-53-7.4-53 7.4-53 7.4l-25 173.7s-41.1-14.7-79.5-26.5c-8.6-33.3-31-207.7-31-207.7h-47.1l-29.4 207.6-75.1 26.4-25-176.6S778.5 80.2 752 81c-25.8.7-51.2 7.4-51.2 7.4l-44.2 294.4-197.3 226.6s1.5-38.3-19.1-38.3-45.3-1.5-53-1.5-25 19.1-25 144.3C299.2 814 84 1183.5 84 1183.5s-69.2 94.6-69.2 237S2.3 1868 26.6 1868z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1562.3 1192.2c184.1 168.5 324.1 347.8 347 385.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M11.5 1572c51.1-51.1 178.3-212.4 346.7-371.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M981.9 29.5 970.1 318h-23.6L934.8 29.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M970.1 313.6 976 452h-33.9l4.4-138.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M805.3 83.9s13.4 96.7 24.9 180.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M861.2 1648.7c-35.6-260.8-179.6-1137.9 95.7-1137.9 291.2 0 124.8 979 104.5 1124.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M947.9 1641v-387.9s-21.2-6.2-58.7 26.1c-.7 1.9-6.9 44.6-16.2 121.3 15 89.6 30.2 183.6 43.4 232.6 2.2 2.8 3.8 4.6 6.6 6.8 16.8.1 24.9 1.1 24.9 1.1zm27.1-.3v-387.1s21-6.2 58.4 26.1c.7 1.9 6.9 44.5 16 121-15 89.5-30 183.1-43.3 232.1-2.2 2.8-3.8 4.6-6.6 6.8-17.6.1-24.5 1.1-24.5 1.1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1507.5 1508.8 54.3-122.2-5.9-672.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1495.8 572.9-26.5 812.2h95.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1469.1 1385.2-107.5 120.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m410.8 1508.8-54.5-122.2 5.9-672.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m425.5 572.6 26.5 812.5h-95.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m449 1383.7 109 119.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1076.1 256.2c72 28.9 166.8 103.8 200.2 142.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M845.1 255.6c-72.1 28.8-167 103.9-200.6 142.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1128.2 903.7c104.8 37.8 211.7 105.5 311.3 184" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M477.6 1095.2c100.1-81.7 207.7-152.5 315.3-191.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M558 1448.5c-10.7-1.5-30.9-5.9-30.9-5.9s-.1-.9-.4-2.5c-14.9-10.3-103.5-779.1-67.3-830.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1361.2 1448.6c10.6-1.5 30.8-5.9 30.8-5.9s.1-.9.4-2.5c14.7-10.3 104.2-776.9 68.3-828.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1441.3 1469.4 24.7-14.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1478 1457.3 10.3 19.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1482.4 1447 24.1-15.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1470 1444.2-10.5-21.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m451.5 1447.4 11.8-23.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m453.4 1457.3 25 17.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m442 1460.1-11.8 22.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m417.2 1433 23.4 16.8" />
	</svg>
);

export default Component;

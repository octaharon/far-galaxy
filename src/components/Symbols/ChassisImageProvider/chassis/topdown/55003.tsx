import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<filter id="ch-topdown55003t">
			<feGaussianBlur stdDeviation={30} />
		</filter>
		<path
			fill="#e3e3e3"
			d="M1348 1372c139.5 1.8 184.3-242 118-320 2.3-20.2 3.1-66 2-92-2.2-52.2-6.4-89.1 1-132 10.4-60.2 26.2-104.7 49-151 24.9-50.6 27.8-89.4 31-109 1.7-10.4 9.3-28.5.2-75.5 10.8-7.3 47-79 11.8-186.5-17.6-53.8-75.9-121.3-141.6-129.1-3.4-4.7-6.9-9.3-10.4-13.9C1324.8 42.2 1141.7 7 961.4 7 727.3 7 586.1 63.7 511 163.2c-3.9 5.1-7.8 10.2-11.5 15.4-63.6 8.1-114.6 64.1-134.5 113.4-25.6 63.6-34.5 143.3 4.9 205.6-8.3 44.4-.3 90.1 5.1 111.4 15.2 59.1 44 76.1 79 233 13.2 59.2-11.8 151-2 210.1-66.3 78-19.5 321.8 120 320 105.8 200.8 300.5 178.4 389.1 182.3 85-6 260.2 22.2 386.9-182.4z"
		/>
		<linearGradient
			id="ch-topdown55003a"
			x1={959}
			x2={959}
			y1={366}
			y2={693.5328}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.303} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003a)"
			d="M862 1554s-27.8-292.4-39-327c54.8-1.6 272 1 272 1s-39 276.1-39 325c-40.1.5-194 1-194 1z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown55003b"
			x1={735}
			x2={960.7689}
			y1={1014.5}
			y2={1014.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.801} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003b)"
			d="M960 1202s1.7-249.6 0-453c-177.5 0-225-165-225-165s74.6 561.4 88 643c27.8-26.2 136-26.3 137-25z"
			opacity={0.251}
		/>
		<linearGradient
			id="ch-topdown55003c"
			x1={959.9811}
			x2={1186}
			y1={1014.5}
			y2={1014.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.199} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003c)"
			d="M960.8 1202s-1.7-249.6 0-453c177.7 0 225.3-165 225.3-165s-74.7 561.4-88.1 643c-28-26.2-136.2-26.3-137.2-25z"
			opacity={0.251}
		/>
		<path
			fill="#999"
			d="M959.5 278c130.1 0 235.5 105.4 235.5 235.5S1089.6 749 959.5 749 724 643.6 724 513.5 829.4 278 959.5 278z"
		/>
		<path fill="gray" d="M827 1228c2.4-15 70.3-25.1 133-26 60.7-.9 121.3 16.2 132 25-29.5 2.1-218.5.9-265 1z" />
		<linearGradient
			id="ch-topdown55003d"
			x1={438.219}
			x2={741.219}
			y1={1295}
			y2={1295}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#888" />
			<stop offset={0.503} stopColor="#e3e3e3" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003d)"
			d="M392 421c-3.3 30.4-8.3 174.3 17 201s62 4 62 4 14 27.1 39 23 37.7-24 47-57 100 9 100 9 6.8 116.8 14 162c-35.8 5-150.6 14.6-217 66-24.5-121-86-187.9-86-265 0-52.1 5.1-105 24-143z"
			opacity={0.3}
		/>
		<linearGradient
			id="ch-topdown55003e"
			x1={1162.9771}
			x2={1465.9771}
			y1={1295}
			y2={1295}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#e3e3e3" stopOpacity={0} />
			<stop offset={1} stopColor="#888" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003e)"
			d="M1526 421c3.3 30.4 8.3 174.3-17 201s-62 4-62 4-14 27.1-39 23-37.7-24-47-57-100 9-100 9-6.8 116.8-14 162c35.8 5 150.6 14.6 217 66 24.5-121 86-187.9 86-265 0-52.1-5.1-105-24-143z"
			opacity={0.3}
		/>
		<linearGradient
			id="ch-topdown55003f"
			x1={473.5839}
			x2={758.5839}
			y1={1807.1239}
			y2={1522.1239}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#888" />
			<stop offset={0.503} stopColor="#e3e3e3" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003f)"
			d="M448 263c9.9-22.1 69.7-141.1 172-189 34.7 25.7 91.5 75 113 110-21 27.4-71 117.3-70 224-72.5.3-91-1-91-1s16.3-99.8 21-113 5.3-76-46-76c-43.5 0-58 56-58 56s-9-14.2-41-11z"
		/>
		<linearGradient
			id="ch-topdown55003g"
			x1={1438.7418}
			x2={1167.4148}
			y1={1836.9325}
			y2={1502.9456}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#888" />
			<stop offset={0.503} stopColor="#e3e3e3" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003g)"
			d="M1468 263c-9.9-22.1-69.7-141-172-189-34.7 25.7-91.5 74.9-113 110 21 27.4 71 117.3 70 224 72.4.3 91-1 91-1s-16.3-99.8-21-113-5.3-76 46-76c43.5 0 58 56 58 56s9-14.2 41-11z"
		/>
		<path
			fill="#b3b3b3"
			d="M540 789c3.5 93.5 22.7 242.3 90 402-12.8 1.7-23.8 2.8-31 5-11.7-22.1-31.4-69.6-68-203-24.5-119.5-26-194-26-194s21.2-10.5 35-10z"
		/>
		<path
			d="m508 846 32-8 4 42-31 9-5-43zm13 82 32-8 4 42-31 9-5-43zm18.6 89.8 30.6-6.1 7.9 39.6-29.9 8.7-8.6-42.2zM565 1110l33-9 16.1 47.3-29.9 8.7-19.2-47z"
			opacity={0.302}
		/>
		<path
			fill="#b3b3b3"
			d="M1380 789c-3.5 93.5-22.7 242.3-90 402 12.8 1.7 23.8 2.8 31 5 11.7-22.1 31.4-69.6 68-203 24.5-119.5 26-194 26-194s-21.2-10.5-35-10z"
		/>
		<path
			d="m1412 846-32-8-4 42 31 9 5-43zm-13 82-32-8-4 42 31 9 5-43zm-18.6 89.8-30.6-6.1-7.9 39.6 29.9 8.7 8.6-42.2zM1355 1110l-33-9-16.1 47.3 29.9 8.7 19.2-47z"
			opacity={0.302}
		/>
		<path
			fill="#02cecd"
			d="M1415 1237c-33.4-15.6-106.2-46.9-127-47-15.4 25-37 70-37 70s-48-22.9-153-31c-13.8 76.7-42 254.2-42 323 48.3-4.2 151.5-19.6 222-93s101.4-132 137-222zm-591-9c-33.1 2.3-127.5 23.3-157 32-15.7-29.2-37-70-37-70s-80.4 19.7-121 48c38.3 84.5 103.7 308.3 355 313 .8-38.2-23.6-218.3-40-323zm-372-174c-20.5 15.6-54.4 105.9-5 221 43.3 99.7 120.1 96.8 125 97-6.7-10.7-10-18-10-18s-57.4 3.9-101-103c-33.9-90.6-8-156-3-165-2.1-14.1-3.5-22.9-6-32zm-79-583c-16.6-17.8-28-219.4 109-270 6-8.9 15-24 15-24s-100.3 18.6-137 130-.8 179.4 10 190c1.2-9.9 3.2-16.6 3-26zm1050-291c27.5 4.7 123.1 37.3 149 176 11.9 95.9-22 138-22 138l-4-27s22.2-67-5-147-66-100.7-104-118c-10-11.2-8.6-15.7-14-22zm38 905c11.1 26.2 23.1 85.9 0 159s-66.5 103.5-103 110c-3.5 7.3-11 20-11 20s96.5-.3 130-112 17.4-183.3-12-208c-1.4 12.6-3.2 22.1-4 31zm-275-899c18.9-24.3 83.8-97.2 116-109-58.6-31.3-145.9-70-342-70S653 56.4 620 72c27.3 19 97.6 88.6 113 113 22.3-27.3 82-105 227-105s202.4 74.6 226 106z"
		/>
		<radialGradient
			id="ch-topdown55003h"
			cx={891.5}
			cy={733.671}
			r={722}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={0.598} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown55003h)"
			d="M824 1228c-33.1 2.3-127.5 23.3-157 32-15.7-29.2-37-70-37-70s-80.4 19.7-121 48c38.3 84.5 103.7 308.3 355 313 .8-38.2-23.6-218.3-40-323z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown55003i"
			cx={1039.401}
			cy={732.5179}
			r={722}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={0.598} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown55003i)"
			d="M1097 1228c33.1 2.3 127.5 23.3 157 32 15.7-29.2 37-70 37-70s80.4 19.7 121 48c-38.3 84.5-103.7 308.3-355 313-.8-38.2 23.6-218.3 40-323z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown55003j"
			cx={363.343}
			cy={926.214}
			r={536}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown55003j)"
			d="M458 1087s65.6 221.9 104 268c-27.3-5.5-70.5-22.7-104-108s0-160 0-160z"
			opacity={0.902}
		/>
		<radialGradient
			id="ch-topdown55003k"
			cx={1470.2729}
			cy={848.349}
			r={296.206}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown55003k)"
			d="M1459.1 1087s-65.7 221.9-104.1 268c27.3-5.5 70.6-22.7 104.1-108s0-160 0-160z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown55003l"
			x1={1390.2166}
			x2={1626.5215}
			y1={1662.0824}
			y2={1487.0344}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003l)"
			d="M1438 203s18 25.9 35 63c4.1 4.6 11.9 3.3 20 23 7.8 19.1 6.6 27.4 14 51 14.7 46.9 36 110.1 38 129 2.2-10 28.9-80.5-9-164s-98-102-98-102z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown55003m"
			x1={535.8394}
			x2={299.8463}
			y1={1731.7343}
			y2={1445.1472}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003m)"
			d="M480 203s-18 25.9-35 63c-4.1 4.6-11.9 3.3-20 23-7.8 19.1-6.6 27.4-14 51-14.7 46.9-36 110.1-37.9 129-2.2-10-28.8-80.5 9-164S480 203 480 203z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown55003n"
			cx={972.709}
			cy={1282.7151}
			r={1138.053}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown55003n)"
			d="M621 75C670 52.9 740.5 7.3 960 7.3c214.3 0 316.2 53.1 341 65.7-40 27.5-93 77.7-112 111-41.5-37.3-91.1-104-230-104s-212.7 83.4-226 102c-37.9-41.5-69.1-82.6-112-107z"
			opacity={0.702}
		/>
		<path
			fill="#768692"
			d="M1454 308c32.5-2 48.4 8.2 50 15 9.3 39.6 22.8 125.6 23 194 .1 41.5-8.8 82.2-16 99-7.7 19.8-32.1 23.8-49 19 2.3-5.6 18-49.2 16-106-3.3-95.1-26.1-220.9-24-221zm-6.1 320.2c-14.3 25.5-35.4 22.8-36.9 22.8-9.5 0-29.1-9.6-33-25 4.4-25.6 22.2-174.6-15-344 15.5-4.2 60.4-8.6 73 19 .8 4.9 2.9 6.1 4 14 3.1 21.2 39.7 266.5 7.9 313.2zm-977.9 0c14 20.6 35.4 22.8 37 22.8 9.5 0 29.1-9.6 33-25-4.5-25.6-22.2-174.5 15-343.9-15.5-4.2-60.4-8.6-73 19-.8 4.9-2.9 6.1-4 14-35 242.3-12 302.7-8 313.1zm-13.6 7c-18 5.3-45.8.4-55.4-27.2-7.2-16.8-16.1-82.5-16-124 .2-68.4 17.7-116.4 27-156 7.6-19 33.3-18.5 51.2-15.8C460 326 440.8 446.8 440 536c-.5 57 17.6 98.8 16.4 99.2z"
		/>
		<linearGradient
			id="ch-topdown55003o"
			x1={-72.8956}
			x2={796.5234}
			y1={1528.8895}
			y2={1431.6755}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003o)"
			d="M456.4 635.2c-18 5.3-45.8.4-55.4-27.2-7.2-16.8-16.1-82.5-16-124 .2-68.4 17.7-116.4 27-156 3.3-8.1 7.1-49.7 29-61 9.7-5 47.9-19.3 22.2 45.2C460 326 440.8 446.8 440 536c-.5 57 17.6 98.8 16.4 99.2z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown55003p"
			x1={-238.2692}
			x2={1088.3326}
			y1={1571.3099}
			y2={1422.9769}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.51} stopColor="#000" />
			<stop offset={0.57} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003p)"
			d="M544 624c-21.7 36.7-64.4 28.5-74 1-13.9-23-10.1-99.5-10-141 .2-68.3 16.7-148.4 26-188 3.1-7.9 5.8-50.9 34-68 25.7-12.6 55-13.1 70 20 11.5 33.7-27.3 156.9-28 246-.5 57 4 92.7-18 130z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown55003q"
			x1={829.8699}
			x2={2159.9099}
			y1={1448.8588}
			y2={1535.3347}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.43} stopColor="#000" stopOpacity={0} />
			<stop offset={0.49} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003q)"
			d="M1377.3 624.3c21.8 36.7 64.5 28.5 74.2 1 13.9-23 10.1-99.6 10-141.1-.2-68.4-16.8-148.6-26.1-188.2-3.2-7.9-5.8-50.9-34.1-68.1-25.7-12.6-55.1-13.1-70.2 20-11.5 33.7 27.3 157 28.1 246.2.5 57.1-4 92.9 18.1 130.2z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown55003r"
			x1={1087.0388}
			x2={2021.7126}
			y1={1446.5288}
			y2={1507.2988}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.43} stopColor="#000" stopOpacity={0} />
			<stop offset={0.49} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003r)"
			d="M1449 628c28.6 20.6 54.4 1.4 63.6-18.2 13.2-25.7 13.9-84.5 13.9-125.6 0-68.4-17.2-134.6-26.5-174.2-3.2-7.9-.9-30.9-35-48-14.5-4.3-27.2 4.6-32 7 10.5 47.9 20.5 117.9 25 207 4.9 96.5.1 138.6-9 152z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown55003s"
			x1={960.0545}
			x2={960.0545}
			y1={1199.0081}
			y2={1882.0549}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#768692" />
			<stop offset={1} stopColor="#02cecd" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55003s)"
			d="M1251 764s-77.3-3.6-88-3c4.8-46.7 17.4-144.8 20-164 5.8-23.7 12.3-52.4 13-66s4.2-238.5-236-254c-90.2-5.8-238.1 82.6-234 229 1 36.8 7.1 73.2 11 107 9.6 83.1 21 148 21 148l-88 4s-13-106.5-13-211-1.8-269.5 75-368S915.1 82 960 82s137.7-5.6 228 104c33.3 40.4 61.4 125.5 70 222 14.7 165.3-7 356-7 356z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m733 576 104 753s26 150.7 26 224" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M512 1235c21.6-11.8 70.1-38.8 119-44 15.6 29.3 35 69 35 69s87.1-25.7 158-32c78.9.6 271 1 271 1s122.6 12.4 157 30c16.9-30.2 27.4-54.1 35-70 14.7 4.3 58.9 14 121 45 7.3 3.7 9 4 9 4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M823 1228c19-23.4 103.2-26 137-26s118 7.7 136 26" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M818 1198c11.7-9.3 20.5-35 141-35s141 35 141 35" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m960 1163-1-416" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1285 1194c25.3-52.2 95-239.2 95-410" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M633 1194c-25.3-52.2-94-239.2-94-410" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1469 831c-26.6-28.6-138.5-70-309-70" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M451 831c26.6-28.6 138.5-70 309-70" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1466 1047c-.8 23.4-22.8 174.3-122 332" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M960 277c130.3 0 236 105.7 236 236s-105.7 236-236 236-236-105.7-236-236 105.7-236 236-236z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1082 1329s92-666.1 102.9-745.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1081.9 1327.3c-10.3 72-19.9 139.7-19.9 139.7s-6 53.2-6 85"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1348 1372c139.5 1.8 185.3-242 119-320 5.9-95.5-9.2-141.5 0-215 12.7-106.7 56-150.3 75-227 2.4-9.7 16.5-67.6 8.4-111.4 34.8-55 29.8-138.3 7.6-196.6-20.5-54.1-72.9-114.3-137.3-123.4-3.8-5.3-7.7-10.5-11.7-15.6C1324.8 42.2 1141.7 7 961.4 7 727.3 7 586.1 63.7 511 163.2c-3.5 4.6-7 9.2-10.4 13.9C428 193.3 387.1 241.8 367 292c-25.2 63-39.1 148.3 2.9 205.1-8.5 163 23.1 121.6 70.1 280.4 4.8 16.3 8.8 35.6 14 56.5 13.1 58.3-8.1 136.3-2 218.1-66.3 78-19.5 321.8 120 320 105.8 200.8 300.5 178.4 389.1 182.3 85-6 260.2 22.2 386.9-182.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M452 1052c3.5 45.7 49.1 187.8 122 324" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1321 1202c21.2-50.2 86.1-216.1 94-401" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1339 1158s-33.5-9.5-34-10m18-48 32 8m17-49-34-9m12-42 34 9m10-47-33-9m7-41s33.2 7.9 34 8m6-41-34-9m6-42 31 9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1358 538c0-30.4-1.6-111.5-25-219-3.3-15.1-7.2-27.9-7.9-38.8 0-55.5 34-61.2 43.9-61.2 11.6 0 53.1-3.1 70 88s34.6 283.3 10 321c-16.6 25.6-29.6 21-40 21s-51-9.6-51-111z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1349 226c9.1 33.5 55.1 163.3 30 400" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1362 279c12.1-3.1 67.2-9.7 77 28" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1440 313c18.9-12.1 51.6-4.7 63 7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1421 179c15.9 17.9 35.1 49.4 53.9 88m34 79.4c19.9 53.3 36.1 110.8 42.1 161.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M960 81c68.8 0 153 12.3 229 105 46.1 56.2 69.1 166.1 73 279 3.9 111.7.5 167-13 302"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M960 81c-69 0-153.2 12.3-229.4 105.1-46.2 56.2-69.2 166.2-73.1 279.2-3.9 111.8-.5 167.1 13 302.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1188 185c13.1-19.1 60-79.8 117-112" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M563 1355s-20.7-2.3-28-7c-71.5-37.2-110.2-164.7-82-254.6 1.5-4.9 3-10.4 3-10.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M599 1202.4c-21.2-50.2-86.1-216.2-94-401" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M581 1158.4s33.5-9.5 34-10m-18-48-32 8m-17-49 34-9m-12-42-34 9m-10-47 33-9m-7-41s-33.2 7.9-34 8m-6-41 34-9m-6-42-31 9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M562 538.4c0-30.4 1.6-111.5 25-219 3.3-15.1 7.2-27.9 7.9-38.8 0-55.5-33.9-61.2-43.9-61.2-11.6 0-53.1-3.1-70 88s-34.5 283.3-10 321c16.6 25.6 29.6 21 40 21 10.4 0 51-9.6 51-111z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M571 226.4c-9.1 33.5-55.1 163.3-30 400" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M558 279.4c-12.1-3.1-67.2-9.7-77 28" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M488 273c-6.9-4.2-59.9-42.1-77 53.4-16.3 90.8-23.5 122.5-24 172-.2 18.5 3 52.7 8 82.1 4 23.4 11.7 43.9 26.3 50.9 8 3.9 28.1 8.6 47.7-3.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1426.6 272.9c6.9-4.2 60.1-42.1 77.3 53.4 16.3 90.9 23.6 122.6 24.1 172.2.2 18.5-3 52.8-8 82.2-4 23.5-11.7 43.9-26.4 51-8 3.9-28.2 8.6-47.9-3.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M480 313.4c-9.7-2.7-49.2-13.2-66 6.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M464 311c-5.3 22.6-46.3 232.8-9 320" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1453.8 311.2c5.3 22.6 45.8 232.7 8.9 319.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M499 179.4c-15 16.8-33 45.9-50.7 81.5m-45.7 109.7c-15.9 46.3-28.4 94.5-33.6 137.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1544.1 474.5s2.7-4.6 4.4-9.4c1.8-5 2.7-10.1 2.7-10.4.3-1.4.8-4.1.8-6.8 1.6-13.2 3.2-26.7 2.9-40-.5-21.6-1.4-39-6.1-58.8-5-21.5-13.9-48.1-21.8-64.4-24.4-50.8-58.4-70.2-74.7-77.6-6.4-3.7-18-7.3-18-7.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M374 475s-2.7-4.6-4.4-9.4c-1.8-5-2.7-10.1-2.8-10.4-.3-1.3-.8-4.1-.8-6.8-1.6-13.3-3.2-26.7-2.9-40 1-45.1 12.8-89 27.9-120.3 24.5-50.8 58.6-73.3 75-80.6 6.4-3.7 18-7.4 18-7.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M732 185.4c-13.1-19.1-60-79.8-117-112" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m506 1321.5 27-21" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1359.6 1354.3c6.6-1 17.9-3.1 22.9-6.3 71.6-37.1 110.3-164.5 82.1-254.3-1.5-4.9-3-10.4-3-10.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1411.6 1321.5-27-21" />
		<g>
			<path
				d="M770 198c9.5-28.1 94.2-73.8 190-73 153.2 11 180.8 64.8 190 75.7 5.7 6.1 26 44.3 26 44.3s11.7 21.8 28 100c15.5 61.8 22 115.4 26 163 10 118.6-13 207.6-78 350.8-10.8 8-29.2 14.2-47 16-12.6 7.9-21 12-21 12l-1 11-39 84v159l12 2 10 162-21 2v163l10 1 14 163-24-1 1 246H875l2-244-26-3 14-161 9-1 4-162-25-1s10.9-165 11-163c.1 2 12-2 12-2l3-161s-41.4-82.7-42-82-1-10-1-10-18.9-7.3-22-13c-11.1-2-39.3-6.2-45-16-50.3-86.3-83-231.1-83-291 0-59.9 18.9-181.4 30-223.8 6.1-57.5 22-94.2 54-147z"
				filter="url(#ch-topdown55003t)"
				opacity={0.4}
			/>
			<path
				fill="#f2f2f2"
				d="M770 198c9.5-28.1 94.2-73.8 190-73 153.2 11 180.8 64.8 190 75.7 5.7 6.1 26 44.3 26 44.3s11.7 21.8 28 100c15.5 61.8 22 115.4 26 163 10 118.6-13 207.6-78 350.8-10.8 8-29.2 14.2-47 16-12.6 7.9-21 12-21 12l-1 11-39 84v159l12 2 10 162-21 2v163l10 1 14 163-24-1 1 246H875l2-244-26-3 14-161 9-1 4-162-25-1s10.9-165 11-163c.1 2 12-2 12-2l3-161s-41.4-82.7-42-82-1-10-1-10-18.9-7.3-22-13c-11.1-2-39.3-6.2-45-16-50.3-86.3-83-231.1-83-291 0-59.9 18.9-181.4 30-223.8 6.1-57.5 22-94.2 54-147z"
			/>
			<path fill="#e6e6e6" d="m839 897 246 4-44 83-163-4-39-83z" />
			<linearGradient
				id="ch-topdown55003u"
				x1={961}
				x2={961}
				y1={1032}
				y2={1230}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={1} stopColor="#f1f1f1" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55003u)"
				d="m1084 888 21-11-1-184s-147.3 34.2-287-3c0 39.8 1 188 1 188s7.4 9.7 20 9c.3-24.8-1-177-1-177s86.1 42.3 243 5c.4 36.1 4 173 4 173z"
				opacity={0.302}
			/>
			<linearGradient
				id="ch-topdown55003v"
				x1={1245}
				x2={1102.5603}
				y1={1400.4994}
				y2={1400.4994}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#cdcdcd" />
				<stop offset={1} stopColor="#dedede" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55003v)"
				d="M1104 163c20.4 4.3 42.8 31.5 49 40 24.5 24.5 39.6 100.9 38 90 10.1 37.4 10.5 46.6 12 54 11.6 35.2 32 166.3 32 224s-44.4 235-82 289c-15.2 10.8-41.7 16.1-48 16 8-246.3-30.8-577.4-1-713z"
			/>
			<linearGradient
				id="ch-topdown55003w"
				x1={835.316}
				x2={692.9894}
				y1={1401.9994}
				y2={1401.9994}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#dedede" />
				<stop offset={1} stopColor="#cdcdcd" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55003w)"
				d="M814 160c-20.3 4.3-43 34.5-49.3 43-6.2 8.5-17.5 29.4-24.8 51s-17.3 54.8-24 87c-11.5 35.2-33 172.3-33 230s44.3 235 81.8 289c15.2 10.8 41.6 16.1 47.8 16-7.8-246.3 31.2-580.4 1.5-716z"
			/>
			<linearGradient
				id="ch-topdown55003x"
				x1={958.5}
				x2={958.5}
				y1={1213.6781}
				y2={1794.4011}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.3} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55003x)"
				d="M813 162c94.3-56.3 218.5-40.1 290 0-25.8 94.5 1 474.5 1 529-27.4 5.5-184.7 33.2-286-3 0-90.2 22.5-450.5-5-526z"
				opacity={0.2}
			/>
			<radialGradient
				id="ch-topdown55003y"
				cx={1026}
				cy={1307}
				r={40}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.199} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000" />
			</radialGradient>
			<path
				fill="url(#ch-topdown55003y)"
				d="M1026 573c22.1 0 40 17.9 40 40s-17.9 40-40 40-40-17.9-40-40 17.9-40 40-40z"
				opacity={0.149}
			/>
			<path
				fill="#768692"
				d="M1192 299c-18.3 3.6-55.9 20.9-59 23 5.3 24.2 32.7 168.2 32 216 34.4-.3 68-5 68-5s-21.8-194.7-41-234zm-468 0c18.3 3.6 55.9 20.9 59 23-5.3 24.2-32.7 168.2-32 216-34.4-.3-68-5-68-5s21.8-194.7 41-234z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1152 201.7c5.9 5.6 17.9 23 26 48.3 12.4 33.9 21.7 66.5 28 96 7.5 40.1 28.9 164.2 28 223.8 0 51.9-22.9 173.9-81 290-10.1 9.9-31.8 14.7-48.4 17-8.4 6.5-21.6 10-21.6 10v12l-40 82 1 162h12l10 163h-23l2 165h10l14 163h-25l1 245H875l1-245h-25l14-163h10l2-165h-23l10-163h12l1-162-40-82v-11s-14.6-1.1-22-11.1c-16.5-2.3-37.9-7.1-48-16.9-58.1-116.1-81-238-81-290S701 416 701 416s6-34.3 14-72c6.9-30.7 11.2-57.5 24-87 8.1-25.4 22.4-49 28-54.8 26-42.8 101.7-76 192.7-76.1 89.7-.3 147.4 26.2 192.3 75.6z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1105 683 26-68 23-10 24 39-74 227" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1142.9 625.7 18.7 26.1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m817.3 683.3-26.9-68-22.9-10-23.9 39 72.2 222.2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m778.5 626-18.6 26.1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1232 534s-37.8 3.3-66 5c-10.4-123.7-25.6-189.5-32-220 6.9-3.9 40-14.4 59-20m-467 0c19 5.6 52.1 16.1 59 20-6.4 30.5-21.6 96.3-32 220-28.2-1.7-66-5-66-5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1026 573c22.1 0 40 17.9 40 40s-17.9 40-40 40-40-17.9-40-40 17.9-40 40-40z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1009 577v71" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1008 623h37" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1043 577v71" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1104 877.8 1-185s-165.1 34.7-288-4c-1 73.2-1 190-1 190"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M872 1634h177" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M871 1470.9h180" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M868 1305.9h178" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M872 1142.9h177" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m877 979.8 166 1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1083 892.8-2-178s-138.5 39.5-244-5c-.4 51 0 186 0 186"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m837 895.8 247 2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m837 808.8 245 1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M817 689c0-113.4 24.6-438.9-3-527" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1104.9 690c0-113.5-29.9-438.4-2-526.5" />
			<path
				fill="#f1f1f1"
				d="m919.7 804.5 5.2-344.8 9.2-1.1v-16.3l-11.4-1 1.4-17.8 10.6.3.7-18.4 19.9.6 1.5-32.6s38.3-36.2-1.8-79.2c0-10.5-2.2-37.4-2.2-37.4l-25.5 1s-46.2-31.9-95.5 3.1c-18.1-.3-36.9-1.3-36.9-1.3l-3.3 145.2h16.3l1.4 16.2 10.8 3.1-.2 13.5-11.2 1.3.7 19 12.8.9 8.3 346.4 89.2-.7z"
			/>
			<linearGradient
				id="ch-topdown55003z"
				x1={874.095}
				x2={874.095}
				y1={1513.64}
				y2={1661.01}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={0.503} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path
				fill="url(#ch-topdown55003z)"
				d="m937.2 406.4 19-.2-1.5-147.2-159.4 2-3.3 144 29.1-.7.6-14-13-1.7-1.3-17 13 .6-1.3-16.1 106.2-.3 1.4 14.1 8 1.4.3 15-9.1 1.6 1.3 15.1 10 3.4z"
				opacity={0.302}
			/>
			<path
				fill="#ccc"
				d="m822.2 423.3 103.2.5.3 17-102.2.5-1.3-18zm-1.4-35.1 106.2-.3-.7 15-104.3 2.4-1.2-17.1z"
			/>
			<path
				fill="#bfbfbf"
				d="M792 292c-5.3 4.7-21.8 52.7 1 80 0-13.2 4.3-84.7-1-80zm163.2 2c13 8.2 27.3 42.3 2.5 80.2-.5-38.8-4.3-49.1-2.5-80.2zM936 260c-19.1-12.9-62-40.6-120 0 22.4 1.4 93.5-1.3 120 0z"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="m829.1 805.3 89.3.6 6.5-347.7 9.9-.4v-16.7l-9.9-.6v-17.7l9.6-.1.5-19.7-8.9-.6-.6-15.6 9.2.3v-16.3l-7.8-.4.2-14.5-107.7-.3.6 15.5-9.3.4.2 15.6 10.5.2.7 16.9-11.5-.2.4 19.6h10.4l.6 16.5-10.9.6-.4 16.5 12.8-.1 5.6 348.2z"
			/>
			<path fill="none" stroke="#191919" strokeWidth={10} d="M814.3 423.1h112.6" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m817.1 440.3 114.1 1.3" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m924.8 458.3-107-.7" />
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="m939.8 406.8 15.6-.6.5-146.5-161.5.8-1.4 144.4 12.7-.5"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M813 261c16.6-14.4 38.3-23 62-23 23 0 44.2 8.2 60.6 21.9m20.6 23.8C965 298.1 970 315 970 333c0 18-5 34.8-13.7 49.1m-162.9-.4c-8.5-14.2-13.4-30.9-13.4-48.7 0-17.4 4.7-33.7 12.8-47.7"
			/>
			<path fill="none" stroke="#191919" strokeWidth={10} d="m818.6 371.3 109.8-.9" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m819.3 387.7 111-1" />
			<path fill="none" stroke="#191919" strokeWidth={10} d="m813.7 405 117.7-.8" />
			<circle id="Animation-Coordinates-Tower" cx={960.5} cy={514.5} r={0.5} fill="#00ff06" />
		</g>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<filter id="ch-topdown55000e">
			<feGaussianBlur stdDeviation={35} />
		</filter>
		<path
			fill="#e3e3e3"
			d="M1348 1372c139.5 1.8 186.3-242 120-320 86.6-306.3 93-437.9 93-468 0-10.5-2.7-44.5-11.8-91.5 10.8-7.3 47-79 11.8-186.5-17.6-53.8-75.9-121.3-141.6-129.1-3.4-4.7-6.9-9.3-10.4-13.9C1324.8 42.2 1141.7 7 961.4 7 727.3 7 586.1 63.7 511 163.2c-3.9 5.1-7.8 10.2-11.5 15.4-63.6 8.1-114.6 64.1-134.5 113.4-25.6 63.6-34.5 143.3 4.9 205.6C361.5 542 359 574 359 584.2c0 30.1 6.4 161.6 93 467.9-66.3 78-19.5 321.8 120 320 105.8 200.8 300.5 178.4 389.1 182.3 85-6 260.2 22.2 386.9-182.4z"
		/>
		<path
			d="M411 348c118.9-45.1 460.5-72 549-72s473.5 29.9 551 79c12.7 29.2 26.6 80.3 36.4 129.9-29.2-.7-74.3 6.1-129.4 38.1-9.1 59.6-46.4 278.2-61 339-56.5-17-119.2-26.3-207-32 10.4-75.6 33-220.7 36-245 47.5-170.8-78.2-307-226-307S694.7 404.6 734 583c16.1 119.2 34 248 34 248s-112.8 5.6-207 30c-12.3-67.5-50.2-260.1-60-333-22.7-28.1-70.8-38.5-127.5-43.7 8-38 20-83.4 37.5-136.3z"
			className="factionColorSecondary"
		/>
		<path
			fill="#bfbfbf"
			d="m839 1327 16-35-84-640s-25.1-30.4-33-53c5.5 46.3 101 728 101 728zm225-39 18 39 100-725-20 35-98 651z"
		/>
		<path fill="#8c8c8c" d="m840 1328 15-35h210l15 37-240-2z" />
		<linearGradient
			id="ch-topdown55000a"
			x1={350.3521}
			x2={457.3211}
			y1={1444.0441}
			y2={1765.0441}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000a)"
			d="M370 498c-39.5-50.5-60.3-265 129-321-82.9 116.7-112.9 244.7-129 321z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown55000b"
			x1={1498.88}
			x2={1498.88}
			y1={1439.45}
			y2={1755.45}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000b)"
			d="M1422 178c180.1 49.6 171 267.1 127 316-9.6-54.8-42.7-192-127-316z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown55000c"
			x1={529.8824}
			x2={424.6564}
			y1={529.1384}
			y2={849.1844}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.499} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000c)"
			d="M451 1052c12.6 37.4 81.7 266.4 120 319-90.2 17.3-205.7-174.3-120-319z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown55000d"
			x1={1377.0338}
			x2={1511.8499}
			y1={533.236}
			y2={852.3909}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000d)"
			d="M1349 1371c72.1 17 214.3-155.1 119-318-19.3 55.4-55.5 198.6-119 318z"
			opacity={0.302}
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1348 1372c139.5 1.8 186.3-242 120-320 86.6-306.3 93-437.9 93-468s-22.4-253.6-152-421C1324.8 42.2 1141.7 7 961.4 7 727.3 7 586.1 63.7 511 163.2 381.5 330.5 359 554 359 584.2c0 30.1 6.4 161.6 93 467.9-66.3 78-19.5 321.8 120 320 105.8 200.8 300.5 178.4 389.1 182.3 85-6 260.2 22.2 386.9-182.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M481 1148c11.3-46.3 75.6-91.9 113.8-106-25.4-135.8-69.7-350.8-93.7-513.2-15.5-28.6-94.9-44.6-128.1-43.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1435.7 1147.8c-11.3-46.3-75.7-91.9-114-106 25.5-135.8 69.8-350.7 93.8-513.1 15.5-28.6 95-44.6 128.3-43.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M560.5 861c79.7-17.5 184.3-31 207.5-31" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M412 349c130-55.6 522-71 522-71" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1356.3 861.1c-79.8-17.5-184.9-31.2-208.1-31.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1509 349c-129.9-55.6-526.8-71-526.8-71" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1166 620-102 674-209 1-90-681" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1064 1289s11.9 29.6 15.8 38.4c.7 1.6 2.2 1.6 2.2 1.6l104-753"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1081.9 1327.3c-10.3 72-19.9 139.7-19.9 139.7l-204 1s-10.1-69.7-20.2-140.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M855 1289s-11.9 29.6-15.8 38.4c-.7 1.6-2.2 1.6-2.2 1.6L733 576"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M839 1329h241" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m917 1292-5 38" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M846 1312h225" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1002 1292 5 38" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1126 1013-19-19H816l-24 13" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M960 277c130.3 0 236 105.7 236 236s-105.7 236-236 236-236-105.7-236-236 105.7-236 236-236z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m935 1553 2-21h45l2 23" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1062 1550-2-22 39-4 3 23" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1169 1528-10-22 34-14 11 19" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m856.5 1550.6 2-22.1-39.1-4-3 23.1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m749.4 1528.5 10-22.1-34-14.1-11 19.1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1346 1374c53.6-90.7 109.2-283.6 125-334" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M574 1374c-53.6-90.7-109.2-283.6-125-334" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1543 275-27 24 13 33 36-4" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1422.1 176.9c152.3 33.6 183.7 243.6 128.7 319.2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1377 1313c25.7-14.9 80.4-86.7 68-189" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1495 1188-25 1-4 28 21 12" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1417 1350-22-13-13 8 5 21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m426 1188 25 1 4 28-21 12" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m504 1350 22-13 13 8-5 21" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M499 177c-152.7 33.6-186.1 243.5-131 319" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m375 275 27 24-13 33-36-4" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M543 1313c-25.7-14.9-80.4-86.7-68-189" />
		<path
			d="M773 198.7c18.4-5.1 91.2-39.6 187-38.7 57.6.5 139.1 13.4 190 40.7 5.7 6.1 25 50 25 50s24.1 41.4 24 93c54 215.8 44.1 314.4-47 515.1-10.8 8-29.2 14.2-47 16-12.6 7.9-21 12-21 12l-1 11-39 84v159l12 2 10 162-21 2v163l10 1 14 163-24-1 1 246H875l2-244-26-3 14-161 9-1 4-162-25-1s10.9-165 11-163c.1 2 12-2 12-2l3-161s-41.4-82.7-42-82-1-10-1-10-18.9-7.3-22-13c-11.1-2-39.3-6.2-45-16-50.3-86.3-83-231.1-83-291 0-59.9 24-197.1 36-225-.4-25.3.8-61.7 25-90 10.8-30.8 14.4-52.8 26-55.1z"
			filter="url(#ch-topdown55000e)"
			opacity={0.35}
		/>
		<path
			fill="#f2f2f2"
			d="M773 198.7c18.4-5.1 91.2-39.6 187-38.7 57.6.5 139.1 13.4 190 40.7 5.7 6.1 25 50 25 50s24.1 41.4 24 93c54 215.8 44.1 314.4-47 515.1-10.8 8-29.2 14.2-47 16-12.6 7.9-21 12-21 12l-1 11-39 84v159l12 2 10 162-21 2v163l10 1 14 163-24-1 1 246H875l2-244-26-3 14-161 9-1 4-162-25-1s10.9-165 11-163c.1 2 12-2 12-2l3-161s-41.4-82.7-42-82-1-10-1-10-18.9-7.3-22-13c-11.1-2-39.3-6.2-45-16-50.3-86.3-83-231.1-83-291 0-59.9 24-197.1 36-225-.4-25.3.8-61.7 25-90 10.8-30.8 14.4-52.8 26-55.1z"
		/>
		<path fill="#e6e6e6" d="m839 897 246 4-44 83-163-4-39-83z" />
		<linearGradient
			id="ch-topdown55000f"
			x1={961}
			x2={961}
			y1={1032}
			y2={1230}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#f1f1f1" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000f)"
			d="m1084 888 21-11-1-184s-147.3 34.2-287-3c0 39.8 1 188 1 188s7.4 9.7 20 9c.3-24.8-1-177-1-177s86.1 42.3 243 5c.4 36.1 4 173 4 173z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown55000g"
			x1={1245}
			x2={1102.5603}
			y1={1390.9994}
			y2={1390.9994}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#cdcdcd" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000g)"
			d="M1104 182c20.4 4.3 42.8 12.5 49 21s21 51 21 51-47.4-13.4-39 44c20.4 58.7 55.3 45 62 45 11.6 35.2 38 170.3 38 228s-44.4 235-82 289c-15.2 10.8-41.7 16.1-48 16 8-246.3-30.8-558.4-1-694z"
		/>
		<linearGradient
			id="ch-topdown55000h"
			x1={834.969}
			x2={692.9678}
			y1={1390.9994}
			y2={1390.9994}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#cdcdcd" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000h)"
			d="M813.6 182c-20.3 4.3-42.6 12.5-48.8 21-6.2 8.5-20.9 51-20.9 51s47.2-13.4 38.9 44c-20.4 58.7-55.1 45-61.8 45-11.7 35.2-38 170.3-38 228s44.3 235 81.8 289c15.2 10.8 41.6 16.1 47.8 16-7.9-246.3 30.7-558.4 1-694z"
		/>
		<linearGradient
			id="ch-topdown55000i"
			x1={1167.1931}
			x2={1167.1931}
			y1={1574}
			y2={1665.3822}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000i)"
			d="M1177 255c10.8 14.8 22.7 58.9 22 88-12.6 1.7-30 3-30 3s-52.9-37.6-26-91c8.5-.7 18.6-.3 34 0z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown55000j"
			x1={750.9951}
			x2={750.9951}
			y1={1574}
			y2={1665.0079}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000j)"
			d="M741.1 255.4C730.3 270.1 718.3 314 719 343c12.6 1.7 30.1 3 30.1 3s53.2-37.4 26.1-90.6c-8.4-.7-18.6-.3-34.1 0z"
			opacity={0.2}
		/>
		<path
			d="M819 182c71.6-28.2 212.7-31.7 281 0-19.1 91.6 4 454.5 4 509-27.4 5.5-184.7 33.2-286-3 0-90.2 28.5-430.5 1-506z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-topdown55000k"
			x1={961}
			x2={961}
			y1={1213.6781}
			y2={1760.4923}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000k)"
			d="M819 182c71.6-28.2 212.7-31.7 281 0-19.1 91.6 4 454.5 4 509-27.4 5.5-184.7 33.2-286-3 0-90.2 28.5-430.5 1-506z"
			opacity={0.102}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1152 201.7c5.9 5.6 13.7 25.7 21.8 51 18.2 17.6 25.4 67.3 25 89.3 7.8 30.5 13.2 53.7 13.2 53.7s22 122.1 22 174c0 51.9-22.9 173.9-81 290-10.1 9.9-31.8 14.7-48.4 17-8.4 6.5-21.6 10-21.6 10v12l-40 82 1 162h12l10 163h-23l2 165h10l14 163h-25l1 245H875l1-245h-25l14-163h10l2-165h-23l10-163h12l1-162-40-82v-11s-14.6-1.1-22-11.1c-16.5-2.3-37.9-7.1-48-16.9-58.1-116.1-81-238-81-290s22-174 22-174 5.5-23 13.4-53.3c-2.6-25 5.5-68 25.2-89.1 8.1-25.4 15.8-45.7 21.4-51.6 10.5-11 100.7-43 191.7-43.1 89.7.2 180.2 31.6 192.3 43.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1104 877.8 1-185s-165.1 34.7-288-4c-1 73.2-1 190-1 190"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M872 1634h177" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M871 1470.9h180" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M868 1305.9h178" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M872 1142.9h177" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m877 979.8 166 1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1083 892.8-2-178s-138.5 39.5-244-5c-.4 51 0 186 0 186"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m837 895.8 247 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m837 808.8 245 1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M817 689c0-113.4 29.6-421.9 2-510" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M788.5 625.5c6.2 0 11.2 8.7 11.2 19.5s-5 19.5-11.2 19.5-11.2-8.7-11.2-19.5 5-19.5 11.2-19.5zm343.5-.5c6.1 0 11.1 8.7 11.1 19.5s-5 19.5-11.1 19.5-11.1-8.7-11.1-19.5 5-19.5 11.1-19.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m727.3 386.9 34.7 4.8m-38.8 24.9 34.7 4.8M719 446.3l34.7 4.8m440.4-64.2-34.4 4.8m38.5 24.9-34.4 4.8m38.5 24.9-34.4 4.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1104.9 690c0-113.5-29.9-422.4-2-510.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1173 254.7h-30s-29.4 52.4 28 90c11-.4 26-1 26-1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M745.2 254.7h30.2s29.6 52.4-28.2 90c-11.1-.4-26.2-1-26.2-1"
		/>
		<path
			fill="#f1f1f1"
			d="m876.4 885.5 5.2-344.7 9.2-1.1v-16.3l-11.4-1 1.4-17.7 10.6.3.7-18.4 19.9.6 1.5-32.6S926.1 439 926 416c0-12-.6-25.9-14.3-40.6 0-10.5-.2-36.4-.2-36.4H884s-57.3-31.9-106.5 3.1c-18.2-.3-25.9-.3-25.9-.3s-.3 13-.7 31.3c-6.4 8.1-11.5 28.1-11.9 42.8-.3 15.3 5.7 31.5 9.9 41.9-.4 16.6-.6 28.2-.6 28.2l16.3.1 1.4 16.2 10.8 3.1-.2 13.4-11.2 3.3.7 17 12.8.9 8.3 346.3 89.2-.8z"
		/>
		<linearGradient
			id="ch-topdown55000l"
			x1={830.7401}
			x2={830.7401}
			y1={1432.47}
			y2={1579.8}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown55000l)"
			d="m893.8 487.5 19.1-.2-1.5-147.2-159.5 2-3.3 143.9 29.1-.7.6-14-15-1.7.8-17 13 .6-1.3-16.1L882 437l1.4 14.1 8 1.4.3 15-9.1 1.6 1.3 15.1 9.9 3.3z"
			opacity={0.302}
		/>
		<path fill="#ccc" d="m778.8 504.5 103.2.5.3 17-102.3.5-1.2-18zm-1.4-35.1 106.3-.3-.7 15-104.3 2.4-1.3-17.1z" />
		<path
			fill="#ccc"
			d="M911.8 375.3c12 15.4 21.3 52.5 1.2 80.8-.5-38.9-.8-52.3-1.2-80.8zm-30.5-34.4c-19.1-12.9-66.6-26.7-103.3.1 22.4 1.4 76.7-1.4 103.3-.1z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m785.7 886.3 89.3.7 6-348h10.4v-17.7l-9.9.4V504l10.6-1.1.6-19.8-9.9.4-.6-15.5 9.2.3V452l-7.8-.4.2-13.4-107.8-1.3.6 15.5-11.3.4-.8 15.6 13.5.2.7 17-13.5-.2 1.4 17.6 11.4 1 .6 15.5-11.6.5v19h13l5.7 347.3z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m771 504.3 112.6-1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m773.7 520.4 109.2 1.3" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m881.5 539.4-107-.7" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m896.4 487.9 15.6-.6.4-146.4-161.5.8-1.2 144.3 12.7-.5"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M775.3 341.5c15.5-11.6 34.8-18.5 55.7-18.5 20.4 0 39.3 6.6 54.7 17.8m27.5 31.6c6.9 13 10.8 27.9 10.8 43.6 0 16-4 31.1-11.2 44.3m-162.4 2.1c-7.9-13.6-12.4-29.5-12.4-46.4 0-17.5 4.8-33.8 13.2-47.8"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m775.2 452.5 109.8-.9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m770.9 468.9 114-1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m772.3 485.1 114.7-1.8" />
		<circle id="Animation-Coordinates-Tower" cx={960.5} cy={513.5} r={0.5} fill="#0f0" />
	</svg>
);

export default Component;

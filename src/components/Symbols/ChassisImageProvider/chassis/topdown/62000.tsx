import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			fillRule="evenodd"
			d="M1061.8 47s8.7 167.3 53 200c46.1 34 146.3 128 167 128 20.6 0 130.5-11.1 234.9 126 50.3 66.1 60.1 183.4 58 291-2.3 115.6-32.7 221-113.8 267 0 14-1.2 41.9-1.2 41.9l13 1-8 295-13-1-1 95 23 33v264l-234-2-2-259 22-34-2-99-13 1-2.7-171.9s-36.1 81.3-82.2 198.9c-41.1 105-85.3 222.6-124 326-12 32.3-14.9 85.6-30 106-11.5 15.4-82.4 15.5-93.9.1-15.1-20.4-17.3-74-30-106-39.2-98.8-82.8-221-124-326-46.1-117.7-85-207-85-207l-3 180-13-1-2 90 22 43-2 259-233.9 2v-264l24-41-1-87-13 1-9-295 13-1v-42c-81-46-109.7-151.4-112-267-2.1-107.6 7.6-224.9 58-291 104.4-137.1 214.3-126 234.9-126s118-102.8 166.9-128 53-200 53-200h206zM790 541.5c0 93.6 75.9 169.5 169.5 169.5S1129 635.1 1129 541.5 1053.1 372 959.5 372 790 447.9 790 541.5z"
			clipRule="evenodd"
		/>
		<path
			fill="none"
			stroke="#000"
			strokeWidth={7}
			d="m464 1408 87-89.4v-150.8l-96-77.8m213.8-.3-96.3 77.7v150.8l87.3 89.4"
			opacity={0.302}
		/>
		<path
			fill="none"
			stroke="#000"
			strokeWidth={7}
			d="m1452.8 1407.6-87-89.3v-150.6l96-77.7m-213.6-.3 96.2 77.6v150.6l-87.2 89.2"
			opacity={0.302}
		/>
		<path
			d="M959.5 372c93.1 0 168.5 75.4 168.5 168.5S1052.6 709 959.5 709 791 633.6 791 540.5 866.4 372 959.5 372z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown62000a"
			cx={965.725}
			cy={-574.3721}
			r={655.472}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.501} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown62000a)"
			fillRule="evenodd"
			d="M1090.8 375c-4 0 137.5-.6 236.2 2 35.6 6.5 27 84.4 27 96-159.8 14.2-172.2 150-172 183 .3 52.8 39.4 110.1 61 123 .3 18 3.2 106 1 162-20.3 4.9-122.2 5.2-165.7 5.8-13.8-84.3-41.9-190.9-118.3-190.5-38.1 0-103.3 19.5-127 189.6-32-2.2-129.7-5-161-5-.4-43.5 1.3-104.3 1-162 64.7-65 60.5-113.3 62-143 0-56-49.2-139.8-139-160-2.8-41.1-4.9-87.8-5-98 90.2-9 224.1.5 233.9.5 14.5 3.9 31.7.1 50.5-5.6C814.1 403.7 772 467.2 772 540.5 772 644 856 728 959.5 728S1147 644 1147 540.5c0-74.8-43.8-139.4-107.2-169.5 17.8 5 34.7 8 51 4z"
			clipRule="evenodd"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown62000b"
			x1={959}
			x2={959}
			y1={747.0178}
			y2={690.8979}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown62000b)"
			d="M960 1867c21.1 0 51.6 2.6 60-55-43.5 0-115-1.5-122-1 9.7 29.5-3.6 56 62 56z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown62000c"
			x1={1621.4091}
			x2={1128.2253}
			y1={-149.0114}
			y2={-149.0114}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.151} stopColor="#000004" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
			<stop offset={0.851} stopColor="#000004" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown62000c)"
			d="M1475 771c28.5-23.4 55-75.2 55-138 22.2 1.5 31.7-1.3 37-1 5.3 110 24.8 352.1-106 428-1.1-36.8-1-84-1-84l16-34s1.1-122.2-1-171zm-243-3c-35.7-27.6-49-69.4-49-130-37.2 0-105 307-105 307s22.1 219.1 7 334c51.6 17.3 99 31 99 31s45.8-78.5 57-90c-.7-27.8-1-119-1-119l8-1 3-123-17-35s-1.7-128.6-2-174z"
		/>
		<linearGradient
			id="ch-topdown62000d"
			x1={798.381}
			x2={305.7318}
			y1={-149}
			y2={-149}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.151} stopColor="#000004" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
			<stop offset={0.851} stopColor="#000004" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown62000d)"
			d="M443 769c-26.1-32.2-59.6-75.2-59.6-138-22.1 1.5-28.6.8-34 1-5.3 110-24.1 352.1 106.6 428 1.1-36.8 2-84 2-84l-20-37s3-121.2 5-170zm242 5c35.7-27.6 48.1-75.4 48.1-136C770.3 638 838 945 838 945s-22.1 219.1-7 334c-51.5 17.3-102.9 32-102.9 32s-44.9-71.5-56.1-83c.7-27.8 3.1-125 3.1-125l-8-1-.1-126 18-37s.7-121.7 0-165z"
		/>
		<linearGradient
			id="ch-topdown62000e"
			x1={955.5}
			x2={955.5}
			y1={613}
			y2={-364}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d6d6d6" />
			<stop offset={0.99} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown62000e)"
			d="M821 1187c0 192.8 87.5 546 139 546 40.1 0 130-325.3 130-562 0-256.1-35.6-415-130-415-131.6 0-139 238.2-139 431z"
			opacity={0.8}
		/>
		<path
			fill="#999"
			d="M1031 1761c42.1-117.6 164.6-446.2 210-535-76.9 85.4-201.8 368.5-210 535zm-144 0c-42.1-117.6-168.6-459.1-221-540 76.9 85.4 212.8 373.5 221 540z"
		/>
		<path
			fill="#666"
			d="M477 1572h169v33H478l-1-33zm0 89 168 1 1 34H478l-1-35zm793-88h170v34h-169l-1-34zm0 90s168.7-1.9 169-1 1 33 1 33h-170v-32z"
		/>
		<path
			fill="#bfbfbf"
			d="m465 1491 191 4 18 30-234-2 25-32zm-25-552 241 1-17 36H458l-18-37zm794 2h242l-15 34-211 1-16-35zm25 552h192l22 30-231 1 17-31z"
		/>
		<radialGradient
			id="ch-topdown62000f"
			cx={962.4}
			cy={-714.993}
			r={478.247}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e2e2e2" />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown62000f)"
			d="M1064 84c3.2 32 20.3 134.8 47 161s151.5 122 162 128c-53.1 3.8-216 4-216 4l-24-12s27.8-313 31-281zM859 60c3.8 16.7 29 305 29 305l-23 13-219-4s129.1-94.7 164-132 42.5-195.9 49-182z"
			opacity={0.702}
		/>
		<path
			d="M960 1028c30 0 100 19.2 100 184s-77.9 400-100 400-105-214-105-412c0-127.8 61.3-172 105-172z"
			className="factionColorPrimary"
		/>
		<path
			fillRule="evenodd"
			d="M685 747s-64.8 44-138 44-103-43-103-43-.9 11.2-1 24c-17.1-10-57-68.4-57-129s45.6-171 173-171 174 113 174 176-19 91.2-48 121c-.5-12.7 0-22 0-22zm-191-95c0 59.3 62.4 66 72 66s64-19.5 64-62c0-62.1-48.4-74-66-74s-70 10.7-70 70zm738 118c-29-29.8-50-60-50-123s50.7-175 178-175c114.1 0 170 110.4 170 171s-39.9 119-57 129c-.2-12.8 0-26 0-26s-44.8 45-118 45-122-42-122-42-.5 8.3-1 21zm123-188c-17.6 0-66 11.9-66 74 0 42.5 54.4 62 64 62s72-6.7 72-66-52.4-70-70-70z"
			className="factionColorSecondary"
			clipRule="evenodd"
		/>
		<radialGradient
			id="ch-topdown62000g"
			cx={1358.986}
			cy={-470.871}
			r={159.5}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.302} />
		</radialGradient>
		<path
			fill="url(#ch-topdown62000g)"
			fillRule="evenodd"
			d="M1232 772c-29-29.8-51-64-51-127s50.7-173 178-173c113 0 171 110.4 171 171s-30 111.8-57 130c-.2-12.8 0-27 0-27s-44.8 45-118 45-122-42-122-42-.5 10.3-1 23zm122-190c-17.6 0-66 11.9-66 74 0 42.5 54.4 62 64 62s72-6.7 72-66-52.4-70-70-70z"
			clipRule="evenodd"
			opacity={0.702}
		/>
		<radialGradient
			id="ch-topdown62000h"
			cx={562.966}
			cy={-470.871}
			r={159.5}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.302} />
		</radialGradient>
		<path
			fill="url(#ch-topdown62000h)"
			fillRule="evenodd"
			d="M683 748s-62.8 43-136 43-102-43-102-43-1.9 10.2-2 23c-17.1-10-57-67.4-57-128s45.6-171 173-171 174 113 174 176-21 92.2-50 122c-.5-12.7 0-22 0-22zm-189-96c0 59.3 62.4 66 72 66s64-19.5 64-62c0-62.1-48.4-74-66-74s-70 10.7-70 70z"
			clipRule="evenodd"
			opacity={0.702}
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1061.8 47s10.9 164.6 53 200c43.1 36.2 146.3 128 167 128 20.6 0 130.5-11.1 234.9 126 50.3 66.1 60.1 183.4 58 291-2.3 115.6-33.7 221-114.8 267v42l13 1-9 295-13-1v96l23 32v264l-234-2-2-259 22-34-2-99-13 1-3-180s-36.1 89.3-82.2 206.9c-41.1 105-85.3 222.6-124 326-12 32.3-14.9 85.6-30 106-11.5 15.4-82.4 15.5-93.9.1-15.1-20.4-17.3-74-30-106-39.2-98.8-82.8-221-124-326-45.8-117.7-84.8-207-84.8-207l-3 180-13-1v99l20 34-2 259-234 2v-264l23-33v-95l-13 1-9-296h14v-42c-81-46-110.5-151.4-112.8-267-2.1-107.6 7.6-224.9 58-291 104.4-137.1 214.3-126 234.9-126S760.2 281.5 803 247c43.3-35 53-200 53-200h205.8z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1018 1823.5c19.1-179.7 109.7-463.1 220.2-595.3m-558-.4C790.5 1359.9 880.9 1643.4 900 1823"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M845 1571c7.9-7.2 18.7-15.9 30.8-24.7m163.1-4.8c15.6 10.5 28.8 20.8 35.1 27.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M672 1162c14.8-13.1 80.9-46.8 147.8-74.9m269.9-1.7c68.9 30 138.6 66.9 150.3 76.6"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M638 375c36.2.9 227 3 227 3m187-2 230-1" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M821 1187c0 192.8 87.5 546 139 546 40.1 0 130-325.3 130-562 0-256.1-35.6-415-130-415-131.6 0-139 238.2-139 431z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 1734v-320" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M901 1828c9.5 0 117 2 117 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 1027V755" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M960 1027c37.6 0 101 28.8 101 202 0 142.3-76.4 185-101 185-25.4 0-106-30.6-106-184s56.9-203 106-203z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1047 1322c-3.4 44-47.7 290-87 290s-87.8-254.1-94-292m-8-148c2.9-59.8 7.1-316 102-316 102.2 0 98 289.9 98 315.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M959.5 353c103.6 0 187.5 84 187.5 187.5S1063.1 728 959.5 728 772 644 772 540.5 856 353 959.5 353z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M959.5 372c93.1 0 168.5 75.4 168.5 168.5S1052.6 709 959.5 709 791 633.6 791 540.5 866.4 372 959.5 372z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1034 366 28-307" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M887 366 859 50" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1237 1526 236-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1260.1 1494 188.9-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1452.8 1395.5-78.9-79.9v-145.8l86-69.8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1257.2 1395.5 78.9-79.9v-145.8l-82.8-68.9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1242 1223-2-121h18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1249 1099 1-124h210v97" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1250 975-17-31.8s3.8-2 5-2c22.4-.2 238.7-1.8 239-1 .3.9-17 36.8-17 36.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1232 942V748.3s121.6 95.3 241-1c1.5 96.2 4 193.7 4 193.7"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1270 1572h169v34.8h-169V1572zm0 90.2h169v34.8h-169v-34.8z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1355 581.5c37.8 0 68.5 30.7 68.5 68.5s-30.7 68.5-68.5 68.5-68.5-30.7-68.5-68.5 30.7-68.5 68.5-68.5z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1233.6 772.1c-32.5-31.7-52.6-75.9-52.6-124.8 0-96.3 78.1-174.3 174.5-174.3S1530 551 1530 647.3c0 49.6-20.7 94.4-54 126.1"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1234.1 777.9s121.5 95.4 240.8-1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m680 1526-235.9-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m655.9 1494-188.9-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m464.2 1395.5 78.9-79.9v-145.8l-89-70.8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m659.8 1395.5-78.9-79.9v-145.8l85.9-70.9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m673 1222.8 2-121h-11" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M668 1098.9 667 975H457l-1 96.9" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m666 974 18-33.6s-.7-1-1.9-1c-23-.1-242.8-.8-243.1 0-.3.9 17 36.6 17 36.6"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M684 942V748s-121.6 95.5-241-1c-1.5 96.3-4 194-4 194" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M684 778s-121.6 95.5-241-1" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M477 1571h169v34.8H477V1571zm0 90.2h169v34.8H477v-34.8z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M562 581.5c37.8 0 68.5 30.7 68.5 68.5s-30.7 68.5-68.5 68.5-68.5-30.7-68.5-68.5 30.7-68.5 68.5-68.5z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M442.9 779.5C407.4 747.3 385 700.8 385 649c0-97.2 78.8-176 176-176s176 78.8 176 176c0 48.9-19.9 93.1-52.1 125"
		/>
	</svg>
);

export default Component;

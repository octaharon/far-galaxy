import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1061.8 47s8.7 167.3 53 200c46.1 34 146.3 128 167 128 20.6 0 130.5-11.1 234.9 126 50.3 66.1 60.1 183.4 58 291-2.3 115.6-32.7 221-113.8 267 0 14-1.2 41.9-1.2 41.9l13 1-8 295-13-1-1 95 23 33v264l-234-2-2-259 22-34-2-99-13 1-2.7-171.9s-36.1 81.3-82.2 198.9c-41.1 105-85.3 222.6-124 326-12 32.3-14.9 85.6-30 106-11.5 15.4-82.4 15.5-93.9.1-15.1-20.4-17.3-74-30-106-39.2-98.8-82.8-221-124-326-46.1-117.7-85-207-85-207l-3 180-13-1-2 90 22 43-2 259-233.9 2v-264l24-41-1-87-13 1-9-295 13-1v-42c-81-46-109.7-151.4-112-267-2.1-107.6 7.6-224.9 58-291 104.4-137.1 214.3-126 234.9-126 20.6 0 118-102.8 166.9-128 48.9-25.2 53-200 53-200h206zM790 541.5c0 93.6 75.9 169.5 169.5 169.5S1129 635.1 1129 541.5 1053.1 372 959.5 372 790 447.9 790 541.5z"
		/>
		<path
			d="m372 959 66-58 1 38 19 43-1 76s-73.1-39.6-85-99zm355-251 84-50s62.2 71 149 71 146.2-67.4 148-72c13.7 10.3 84 47 84 47s15.2 49.8 39 67c-.2 48.9 1 171 1 171l20 38-3 123-8-1v59s-124.1-67.5-152-75c-1.7-37.1-4.9-331-130-331-48 0-128.4 31.4-138 334-31.4 13.5-147 70-147 70l2-58-8 1-2-126 19-37 1-165s14.5-16.1 25-33c10-16 16-33 16-33zm732 353s72.9-45 86-108c-21.2-18-68-55-68-55v43l-18 37v83z"
			className="factionColorSecondary"
		/>
		<path
			fill="none"
			stroke="#000"
			strokeWidth={7}
			d="m464 1408 87-89.4v-150.8l-96-77.8m213.8-.3-96.3 77.7v150.8l87.3 89.4"
			opacity={0.302}
		/>
		<path
			fill="none"
			stroke="#000"
			strokeWidth={7}
			d="m1452.8 1407.6-87-89.3v-150.6l96-77.7m-213.6-.3 96.2 77.6v150.6l-87.2 89.2"
			opacity={0.302}
		/>
		<path
			d="M959.5 372c93.1 0 168.5 75.4 168.5 168.5S1052.6 709 959.5 709 791 633.6 791 540.5 866.4 372 959.5 372z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown62002a"
			cx={965.724}
			cy={1374.803}
			r={656.175}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.501} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown62002a)"
			d="M1090.8 375c-4 0 137.5-.6 236.2 2 35.6 6.5 27 84.4 27 96-159.8 14.2-172.2 150-172 183 .3 52.8 39.4 110.1 61 123 .3 18 3.2 106 1 162-20.3 4.9-122.2 5.2-165.7 5.8-13.8-84.3-41.9-190.9-118.3-190.5-38.1 0-103.3 19.5-127 189.6-32-2.2-129.7-5-161-5-.4-43.5 1.3-104.3 1-162 64.7-65 60.5-113.3 62-143 0-56-49.2-139.8-139-160-2.8-41.1-4.9-87.8-5-98 90.2-9 224.1.5 233.9.5 14.5 3.9 31.7.1 50.5-5.6C814.1 403.7 772 467.2 772 540.5 772 644 856 728 959.5 728S1147 644 1147 540.5c0-74.8-43.8-139.4-107.2-169.5 17.8 5 34.7 8 51 4z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown62002b"
			x1={959}
			x2={959}
			y1={52.9818}
			y2={109.1116}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown62002b)"
			d="M960 1867c21.1 0 51.6 2.6 60-55-43.5 0-115-1.5-122-1 9.7 29.5-3.6 56 62 56z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown62002c"
			x1={1621.4189}
			x2={1128.2369}
			y1={949.0082}
			y2={949.0082}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.151} stopColor="#000004" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
			<stop offset={0.851} stopColor="#000004" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown62002c)"
			d="M1475 771c28.5-23.4 55-75.2 55-138 22.2 1.5 31.7-1.3 37-1 5.3 110 24.8 352.1-106 428-1.1-36.8-1-84-1-84l16-34s1.1-122.2-1-171zm-243-3c-35.7-27.6-49-69.4-49-130-37.2 0-105 307-105 307s22.1 219.1 7 334c51.6 17.3 99 31 99 31s45.8-78.5 57-90c-.7-27.8-1-119-1-119l8-1 3-123-17-35s-1.7-128.6-2-174z"
		/>
		<linearGradient
			id="ch-topdown62002d"
			x1={798.381}
			x2={305.7396}
			y1={949}
			y2={949}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.151} stopColor="#000004" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" />
			<stop offset={0.851} stopColor="#000004" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown62002d)"
			d="M443 778c-32.6-25.7-59.6-84.2-59.6-147-22.1 1.5-28.6.8-34 1-5.3 110-24.1 352.1 106.6 428 1.1-36.8 2-84 2-84l-20-37s3-112.2 5-161zm242-4c35.7-27.6 48.1-75.4 48.1-136C770.3 638 838 945 838 945s-22.1 219.1-7 334c-51.5 17.3-102.9 32-102.9 32s-44.9-71.5-56.1-83c.7-27.8 3.1-125 3.1-125l-8-1-.1-126 18-37s.7-121.7 0-165z"
		/>
		<linearGradient
			id="ch-topdown62002e"
			x1={955.5}
			x2={955.5}
			y1={187}
			y2={1164}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d6d6d6" />
			<stop offset={0.99} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown62002e)"
			d="M821 1187c0 192.8 87.5 546 139 546 40.1 0 130-325.3 130-562 0-256.1-35.6-415-130-415-131.6 0-139 238.2-139 431z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-topdown62002f"
			x1={1058}
			x2={859}
			y1={919.0002}
			y2={919.0002}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.5} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown62002f)"
			d="M859 1142s.7-285.4 101-285 97.7 269.4 98 288c-15.5-48.7-38-119.5-97-119-25.7.2-52.6 17.2-70 42-22.6 32.2-32 74-32 74z"
		/>
		<path
			fill="#b3b3b3"
			d="M1031 1761c42.1-117.6 164.6-446.2 210-535-76.9 85.4-201.8 368.5-210 535zm-144 0c-42.1-117.6-168.6-459.1-221-540 24.4 27.1 53.7 54.4 84 112 65.2 123.8 131.4 314.4 137 428z"
		/>
		<path fill="#f2f2f2" d="M923 729h71v31h-71v-31z" />
		<path
			fill="#666"
			d="M477 1572h169v33H478l-1-33zm0 89 168 1 1 34H478l-1-35zm793-88h170v34h-169l-1-34zm0 90s168.7-1.9 169-1 1 33 1 33h-170v-32z"
		/>
		<radialGradient
			id="ch-topdown62002g"
			cx={962.4}
			cy={1514.994}
			r={478.231}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e2e2e2" />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown62002g)"
			d="M1064 84c3.2 32 20.3 134.8 47 161s151.5 122 162 128c-53.1 3.8-216 4-216 4l-24-12s27.8-313 31-281zM859 60c3.8 16.7 29 305 29 305l-23 13-219-4s129.1-94.7 164-132 42.5-195.9 49-182z"
			opacity={0.302}
		/>
		<path
			d="m1040 1543 21-95 22-143 8-91-1-75v-53l152 77-3 62-37 51-47 81-45 104-26 75-9 31s-42.2 134.5-49 196c-6.8 61.5-10 65-10 65l-112-1s-16.6-98.3-24-130-37-125-37-125l-38-112-59-135-52-78-20-28 2-61 62-34 83-35 3 108s1.1 74.9 1 76 12 97 12 97l20 105 19 72s26.1 95.1 42 128 28.8 58 42 58 33.9-44.3 41-65 39-125 39-125zm-597 247-2-266 24-32 191 2 23 35-3 259-233 2zm33-130 1 36 170-1-2-33-169-2zm170-54s1.2-33.6 0-33-169-3-169-3v37l169-1zm595 182-3-259 23-35 191-2 24 32-2 266-233-2zm31-126-2 33 170 1 1-36-169 2zm168-55v-37s-167.8 3.6-169 3 0 33 0 33l169 1z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-topdown62002h"
			x1={958}
			x2={958}
			y1={92}
			y2={834}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-topdown62002h)"
			d="m1040 1543 21-95 22-143 8-91-1-75v-53l152 77-3 62-37 51-47 81-45 104-26 75-9 31s-42.2 134.5-49 196c-6.8 61.5-10 65-10 65l-112-1s-16.6-98.3-24-130-37-125-37-125l-38-112-59-135-52-78-20-28 2-61 62-34 83-35 3 108s1.1 74.9 1 76 12 97 12 97l20 105 19 72s26.1 95.1 42 128 28.8 58 42 58 33.9-44.3 41-65 39-125 39-125z"
			opacity={0.102}
		/>
		<path d="m465 1491 191 4 18 30-234-2 25-32zm794 2h192l22 30-231 1 17-31z" opacity={0.102} />
		<linearGradient
			id="ch-topdown62002i"
			x1={972.7675}
			x2={928.3565}
			y1={1164.4886}
			y2={186.4886}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.149} />
			<stop offset={1} stopColor="#000" stopOpacity={0.071} />
		</linearGradient>
		<path
			fill="url(#ch-topdown62002i)"
			d="M960 755c15 0 68.2 5 97 99s33 210.5 33 254-4.7 190.5-16 257-30.6 177.7-54 248c-23.4 70.3-43 120-60 120s-46.3-58.2-65-120-52.9-201.6-62-280-13.3-196-9-273 13.2-214.3 60-267c34.8-36.7 61-38 76-38zm-89 248c-7.9 49-16 193.4-16 242s29.5 170.7 37 202 34.5 164 68 164 68.4-172.5 70-184 28.4-161.7 30-176 5.8-138.3-16-264-67.8-131-84-131c-57.9 0-81.1 98-89 147z"
			opacity={0.502}
		/>
		<path d="m439 939 245-1s-18.3 35-19 37-209 0-209 0l-17-36zm794 3 243-1-16 35-211-2-16-32z" opacity={0.102} />
		<radialGradient
			id="ch-topdown62002j"
			cx={1358.986}
			cy={1270.871}
			r={159.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.302} />
		</radialGradient>
		<path
			fill="url(#ch-topdown62002j)"
			d="M1232 772c-29-29.8-51-64-51-127s50.7-173 178-173c113 0 171 110.4 171 171s-30 111.8-57 130c-.2-12.8 0-27 0-27s-44.8 45-118 45-122-42-122-42-.5 10.3-1 23zm122-190c-17.6 0-66 11.9-66 74 0 42.5 54.4 62 64 62s72-6.7 72-66-52.4-70-70-70z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown62002k"
			cx={562.966}
			cy={1270.871}
			r={159.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.302} />
		</radialGradient>
		<path
			fill="url(#ch-topdown62002k)"
			d="M683 748s-62.8 43-136 43-102-43-102-43-2.9 18.2-3 31c-17.1-10-56-75.4-56-136s45.6-171 173-171 174 113 174 176-21 92.2-50 122c-.5-12.7 0-22 0-22zm-189-96c0 59.3 62.4 66 72 66s64-19.5 64-62c0-62.1-48.4-74-66-74s-70 10.7-70 70z"
			opacity={0.302}
		/>
		<path
			fill="#e6e6e6"
			d="M960 1028c26.9 0 82.5 15.9 96 127 10.3 84.4-1.5 144.6-13.8 178.7 2.5 8.7-13 109.8-23.2 147.3-7 25.7-34.5 130-60 130-13.5 0-41.5-64.3-61-140-13.3-51.8-24.4-117.2-27.9-140.8-2.7-7.5-5.1-15.5-7.1-24.2-18.1-79.6-4.9-149 1-172s26.6-106 96-106z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1061.8 47s10.9 164.6 53 200c43.1 36.2 146.3 128 167 128 20.6 0 130.5-11.1 234.9 126 50.3 66.1 60.1 183.4 58 291-2.3 115.6-33.7 221-114.8 267v42l13 1-9 295-13-1v96l23 32v264l-234-2-2-259 22-34-2-99-13 1-3-180s-36.1 89.3-82.2 206.9c-41.1 105-85.3 222.6-124 326-12 32.3-14.9 85.6-30 106-11.5 15.4-82.4 15.5-93.9.1-15.1-20.4-17.3-74-30-106-39.2-98.8-82.8-221-124-326-45.8-117.7-84.8-207-84.8-207l-3 180-13-1v99l20 34-2 259-234 2v-264l23-33v-95l-13 1-9-296h14v-42c-81-46-110.5-151.4-112.8-267-2.1-107.6 7.6-224.9 58-291 104.4-137.1 214.3-126 234.9-126 20.6 0 124.1-93.5 166.9-128 43.3-35 53-200 53-200h205.8z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1018 1823.5c19.1-179.7 109.7-463.1 220.2-595.3m-558-.4C790.5 1359.9 880.9 1643.4 900 1823"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M845 1571c7.9-7.2 18.7-15.9 30.8-24.7m163.1-4.8c15.6 10.5 28.8 20.8 35.1 27.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M672 1162c14.8-13.1 80.9-46.8 147.8-74.9m269.9-1.7c68.9 30 138.6 66.9 150.3 76.6"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M638 375c36.2.9 227 3 227 3m187-2 230-1" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M821 1187c0 192.8 87.5 546 139 546 40.1 0 130-325.3 130-562 0-256.1-35.6-415-130-415-131.6 0-139 238.2-139 431z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 1734v-320" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M901 1828c9.5 0 117 2 117 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 1027V755" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M960 1027c37.6 0 101 28.8 101 202 0 142.3-76.4 185-101 185-25.4 0-106-30.6-106-184s56.9-203 106-203z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1047 1322c-3.4 44-47.7 290-87 290s-87.8-254.1-94-292m-8-153c2.9-59.8 7.1-311 102-311 102.2 0 98 289.9 98 315.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M959.5 353c103.6 0 187.5 84 187.5 187.5S1063.1 728 959.5 728 772 644 772 540.5 856 353 959.5 353z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M812 657s-34.6 19.3-84.1 48.6M439 899c-39.7 32.3-65 56-65 56"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1107 657s34.6 19.3 84.1 48.6M1476.9 896c39.7 32.3 68.1 59 68.1 59"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M959.5 372c93.1 0 168.5 75.4 168.5 168.5S1052.6 709 959.5 709 791 633.6 791 540.5 866.4 372 959.5 372z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1034 366 28-307" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M887 366 859 50" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1237 1526 236-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1260.1 1494 188.9-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1452.8 1395.5-78.9-79.9v-145.8l86-69.8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1257.2 1395.5 78.9-79.9v-145.8l-82.8-68.9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1242 1223-2-121h18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1249 1099 1-124h210v97" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1250 975-17-31.8s3.8-2 5-2c22.4-.2 238.7-1.8 239-1 .3.9-17 36.8-17 36.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1232 942V748.3s121.6 95.3 241-1c1.5 96.2 4 193.7 4 193.7"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1270 1572h169v34.8h-169V1572zm0 90.2h169v34.8h-169v-34.8z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1355 581.5c37.8 0 68.5 30.7 68.5 68.5s-30.7 68.5-68.5 68.5-68.5-30.7-68.5-68.5 30.7-68.5 68.5-68.5z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1233.6 772.1c-32.5-31.7-52.6-75.9-52.6-124.8 0-96.3 78.1-174.3 174.5-174.3S1530 551 1530 647.3c0 49.6-20.7 94.4-54 126.1"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m680 1526-235.9-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m655.9 1494-188.9-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m464.2 1395.5 78.9-79.9v-145.8l-89-70.8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m659.8 1395.5-78.9-79.9v-145.8l85.9-70.9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m673 1222.8 2-121h-11" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M668 1098.9 667 975H457l-1 96.9" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m666 974 18-33.6s-.7-1-1.9-1c-23-.1-242.8-.8-243.1 0-.3.9 17 36.6 17 36.6"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M684 942V748s-121.6 95.5-241-1c-1.5 96.3-4 194-4 194" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M684 798s-121.6 95.5-241-1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1234 798s121.6 95.2 241-1" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M477 1571h169v34.8H477V1571zm0 90.2h169v34.8H477v-34.8z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M572.7 582.3c32.7 5.1 57.7 33.4 57.7 67.6 0 37.8-30.7 68.5-68.5 68.5s-68.5-30.7-68.5-68.5c0-34.1 24.9-62.4 57.6-67.6.1 0 6.1-1 11.8-1 5.1 0 9.9 1 9.9 1z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M442.9 779.5C407.4 747.3 385 700.8 385 649c0-97.2 78.8-176 176-176s176 78.8 176 176c0 48.9-19.9 93.1-52.1 125"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M532 476v316.2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M592 792.2V476" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M465.5 707.2c-10.6-17.1-16.8-37.2-16.8-58.8 0-20.7 5.7-40.1 15.5-56.7"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m450 669-33 12" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m450 626-38-5" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m677.1 669.4 33.1 12" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m677.1 626.4 38.1-5" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m660 702.7 31.6 18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m659.5 596.5 33.7-19.2" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M661.5 707.6c10.7-17.1 16.8-37.2 16.8-58.8 0-20.7-5.7-40.1-15.6-56.7"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m463 703-32 18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m463.5 596.6-32.1-18.2" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M666.6 760.3c30.3-27.4 49.4-66.9 49.4-111 0-73.8-53.5-135-124-147.4"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M460.5 759.8c-30.3-27.4-49.3-66.9-49.3-111 0-72.8 52-133.4 120.9-146.9"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1385.1 476v312.5" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1325 788.5V476" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1451.7 707.4c10.7-17.1 16.8-37.2 16.8-58.8 0-20.7-5.7-40.1-15.5-56.7"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1467.2 669.2 33 12" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1467.2 626.2 38-5" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1239.9 669.6-33.1 12" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1239.9 626.6-38.1-5" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1257 702.9-32.6 19" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1257.6 596.7-33.7-19.2" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1255.5 707.8c-10.7-17.1-16.8-37.3-16.8-58.8 0-20.7 5.7-40.1 15.6-56.8"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1454.2 703.2 32 18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1453.6 596.7 32.1-18.2" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1250.5 760.6c-30.4-27.4-49.5-67-49.5-111.1 0-73.8 53.6-135.2 124.2-147.5"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1456.7 760.1c30.3-27.4 49.3-67 49.3-111.1 0-72.8-52-133.5-121-147"
		/>
	</svg>
);

export default Component;

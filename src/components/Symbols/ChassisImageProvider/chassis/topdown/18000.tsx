import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<g id="Animation-Foot-Left">
			<path fill="#999" d="m663 1182 53-247-32-364-105-212H439l-95 209-41 368 60 246h53l87-247h16l89 247h55z" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m663 1182 53-247-32-364-105-212H439l-95 209-41 368 60 246h53l87-247h16l89 247h55z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M425.3 859.1C481.5 887 562.5 883 602 857.9c-5.8-50.4-17.9-139.4-17.9-139.4s-90.6 19-134.9-4.8c-11.1 76.8-12.6 82.7-23.9 145.4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M602 842c23.4-3.1 38.9-12.8 43-19-3.6-25.1-20-98-20-98s-15.5 8.9-38 13"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M430 842c-23.4-3.1-38.9-12.8-43-19 3.6-25.1 20-98 20-98s15.5 8.9 38 13"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m344 569 340 2m32 366-197-2m-17 1-199 2" />
		</g>
		<g id="Animation-Foot-Right">
			<path
				fill="#999"
				d="m1368.5 1182.5-53-247 32-364 105-212h140l95 209 41 368-60 246h-53l-87-247h-16l-89 247h-55z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1368.5 1182.5-53-247 32-364 105-212h140l95 209 41 368-60 246h-53l-87-247h-16l-89 247h-55z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1606.2 859.6c-56.2 27.9-137.2 23.9-176.6-1.2 5.8-50.4 17.9-139.4 17.9-139.4s90.5 19 134.9-4.8c11 76.8 12.5 82.7 23.8 145.4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1429.5 842.5c-23.3-3.1-38.9-12.8-43-19 3.6-25.1 20-98 20-98s15.5 8.9 38 13"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1601.5 842.5c23.3-3.1 38.9-12.8 43-19-3.6-25.1-20-98-20-98s-15.5 8.9-38 13"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1687.5 569.5-339.9 2m-32.1 366 197-2m17 1 199 2" />
		</g>
		<path
			fill="#f2f2f2"
			d="M1434 1409.6c-55.4-34.6-76.4-100.6-84-135.6-50.9 104.2-220.5 181-339 181-162.5 0-298.1-103.9-333-176-14.4 70.1-53.8 108-85.8 127.9 15.8 5.4 35.8 12.1 35.8 12.1l10 23s-74.8.4-92 .5v31.5H363s-.9-78.1-1-111c-10.3-12.1-38-45-38-100s21-91 21-91-28.5-154.6-32-174c-6.6-2.6-20-8-20-8l-8 484H102L89 673H66v-41h21v-43H65l-1-39h22v-38H63v-35h22v-36h104v-47h269s-.3 6.3 0 19c96.5 0 224.2.8 263 8 17.6-38 61.8-126.6 196.1-156.2-.1-5.5 5.3-13.4 11.9-15.8 61.4-28.4 175.6-3 185 2s8.7 8.7 11.1 18.8c125.4 36.4 170.7 129 182.9 152.2 44.5-13.3 119.8-9.8 143-9 8.8-6.2 34-14 34-14V153h382l1 272s-25.6 49.5-36 71c11.7 18.8 9 102.8 9 159 0 118.3-13 175-13 175l15 209h-37v241h17l19 245h-35l-1 363h-261l1-363h-37s4.5-47.9 7.2-81.9c-33.6-.3-119.2-.1-119.2-.1l10-24s13.8-5.1 27-9.4z"
		/>
		<radialGradient
			id="ch-topdown18000a"
			cx={323.5}
			cy={663.5}
			r={343.5178}
			gradientTransform="matrix(1 0 0 2.3681 0 -907.7642)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2576} stopColor="#000" />
			<stop offset={0.5022} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18000a)"
			d="M293 988c-.5 22.6-2 114-2 114l-195 4-6-432 57-1-1-41H87l1-42 56-1v-38l-57-1-1-37 59-1-1-34-58-1v-36l152 1s-55.6 6.3-51 147c.3 8.2 1.1 32.1 1.9 64.4.9 36.9 1.9 84.8 7.1 132.6 9.6 87.5 29.5 174.8 97 202z"
		/>
		<linearGradient
			id="ch-topdown18000b"
			x1={454.5}
			x2={454.5}
			y1={744}
			y2={912}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.698} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000b)"
			d="M355 1008c15.9 3.4 60.4 13.5 199 10-.2 23.7-3 158-3 158l-193-4s-2.5-143.7-3-164z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown18000c"
			cx={1650.5}
			cy={868.5}
			r={297.2516}
			gradientTransform="matrix(0.5331 0.8461 -1.2636 0.7962 1868.0835 -1219.4113)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3378} stopColor="#000" />
			<stop offset={0.5358} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18000c)"
			d="m1512 1018-2 22 36 1-2 239 262-1v-238l37-1-15-207s-19 93.7-106 144c-93.3 56.3-210 41-210 41z"
		/>
		<path
			fill="#e6e6e6"
			d="M1336 855c-34.6 114.2-149.7 222-255 250-.2 8.9-1 23-1 23H942l1-23s-183.6-46.9-255-247c.3 37.5-.3 104.8 0 113s-3.1 26-20 36c-15.7 5.4-13.7 5.6-18 7 .4 4.7 30.5 198.1 68 249s98.3 96.4 173 111c37-6.3 63-9 63-9l10-75 11-19h79l10 17 11 78 63 8s116.3-22.9 172-111c17-26.9 46.1-111 71-250-11.2-2-42-3.3-42-50 0-25.3-1.2-69.3-3-108z"
		/>
		<radialGradient
			id="ch-topdown18000d"
			cx={974.593}
			cy={941.108}
			r={622.2}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#000" />
			<stop offset={0.543} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18000d)"
			d="M964 1289c-.3 7.2-2 16-2 16l-8 60-63 8s-93.6-9.4-169-103c-34.6-43-69.3-218.7-73-256 7.3-2.7 13.7-4.2 19-7 .7 7.8 8 94.4 43 150 33.4 54.7 137.2 130 253 132z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown18000e"
			cx={1044.825}
			cy={942.242}
			r={625.6}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#000" />
			<stop offset={0.543} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18000e)"
			d="M1065 1285c.3 7.2 2 20 2 20l8 60 63 8s93.6-9.4 169-103c34.6-43 69.3-218.7 73-256-7.3-2.7-17.7-6.2-23-9-.7 7.8-16 96.4-51 152-27.5 45.4-100.2 128-241 128z"
			opacity={0.502}
		/>
		<path
			fill="#bfbfbf"
			d="M1393 1079v69s-5.2 28-17 64c-13.2 40.3-35.5 91.3-88 139-51.9 43.1-118.7 80.2-219 100 15-14 20-21 20-21s28-3.2 65-18c49.9-20 117.1-58.2 162-110 29-27.7 53.3-116.8 65-182 5.4-35.8 12-41 12-41zm-753 7c15.2 90.3 39.1 172.3 69 213 78.8 87.1 177 122.1 226 131 11 9.8 21 22 21 22s-39-4.3-88-23c-63.2-24.2-143.8-70.6-188-144-15.8-26.2-40.2-132.5-42-144-1-15.7-1.6-61.7 2-55z"
		/>
		<path
			fill="#d9d9d9"
			d="m1077 1414 12 16-22 21-10-19 20-18zm-126 1 17 18-10 19-22-22 15-15zm4-50v19l-64 9v-20l64-8zm119 0 64 8-1 21-63-10v-19z"
		/>
		<path
			fill="#e6e6e6"
			d="m1079 1415 11 16s55.9-10.9 116-43c41.4-22.2 84.9-59.3 113-90 26.8-36.2 41.6-82.8 50-120 10.4-45.9 18.5-82.1 19-94 3.6 3.2 4-1 4-1l1-69-11-2s-44.4 227.3-78.6 261.6c-36.2 44.5-88.9 82.3-165.4 100.4-.2 7.1-1 19-1 19l-58 3v19zm-21 18 9 18s-50.7 9.1-108 1c4.7-10 10-19 10-19h89zm-108-19s-7.3 7.4-15 16c-55.7-12-93.5-31.3-130-52-14.1-10.1-82.2-52.8-105-92-35.2-60.5-49.5-144.2-62-208 .2-36-1-63-1-63l12-1s7.7 59.8 26 130c11.4 43.7 22.5 90.8 52 132 27.4 37.2 89.3 84.4 164 98 .6 8.5 1 19 1 19l57 2 1 19z"
		/>
		<path fill="#ccc" d="m1075 1384 53 9-50 2-3-11zm-121 0-6 11-54-2 60-9z" />
		<linearGradient
			id="ch-topdown18000f"
			x1={1407.6754}
			x2={1323.7775}
			y1={874.2484}
			y2={841.5383}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000f)"
			d="M1393 1156v-142s-30.5-1.2-43-14-11-26.2-11-32c-11 16.9-86.1 169.5 54 188z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-topdown18000g"
			x1={614.1901}
			x2={696.8451}
			y1={883.5943}
			y2={839.8313}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000g)"
			d="M637 1156v-142s26.5-1.2 39-14 12-26.2 12-32c11.1 16.9 89.2 169.5-51 188z"
			opacity={0.902}
		/>
		<path
			fill="#ccc"
			d="M553 1018c17.8 0 72.6-1.2 84-2 .3 18.4 0 319 0 319h-88s3.7-303.8 4-317zm959 1c-17.8 0-107.7-4.2-119-5-.3 18.4 2 321 2 321h130s2-22.1 4.8-54.9c4.4-.5 9.3.4 14.2-1.1 1.7-69.6.6-183.3 2-238-11.3-.9-25.1-1-35-1 .3-12.3 1.1-19.1 1-21zm-1158-11 6 219s-4.4-13.5-9-36c-13.3-65.2-38-194-38-194l41 11z"
		/>
		<linearGradient
			id="ch-topdown18000h"
			x1={593.0689}
			x2={593.0689}
			y1={904}
			y2={585}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000h)"
			d="M553 1018c17.8 0 72.6-1.2 84-2 .3 18.4 0 319 0 319h-88s3.7-303.8 4-317z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown18000i"
			x1={1469.4849}
			x2={1469.4849}
			y1={906}
			y2={585}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000i)"
			d="M1393 1014c17.8 0 106.7 5.8 118 5 0 2.4.3 8.5 0 21 8.1-.8 26.1.7 35 2-.1 99-1.5 175-2 237-6 1-8.7 1.3-14 2-3 31-5 54-5 54h-130s-2.3-307.8-2-321z"
			opacity={0.302}
		/>
		<path fill="#bfbfbf" d="m547 1442 92 1-11-23-56-20-19-43h-5l-1 85zm976-84-41-1-19 43-56 20-10 22h119l7-84z" />
		<path
			fill="#a6a6a6"
			d="M1525 1334h-131l-2-186s-9.6 62.4-41 129c17.2 54.2 26.8 93.8 83 132 17.5-4.9 30-9 30-9l17-42h42l2-24zm-888-196c6.1 28.7 26.3 116.7 43 143-12.1 37.4-32.2 94.4-86 126-13.5-4.3-23-7-23-7l-17-43h-6v-22l90-1s.5-167.3-1-196zm-292 37c3.5 8.8 15 53 15 53l3 134s-38-31.5-38-93c0-38.5 7.1-71.9 20-94z"
		/>
		<linearGradient
			id="ch-topdown18000j"
			x1={659}
			x2={548}
			y1={549.5}
			y2={549.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000j)"
			d="M637 1335h-89v22l5 1 18 42 23 6s48.1-31 65-69c-8.3-.6-22-2-22-2z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown18000k"
			x1={1525}
			x2={1370}
			y1={547.6275}
			y2={547.6275}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000k)"
			d="M1422 1335h103l-2 22-41 1-18 42-31 10c.7.3-41.3-27.3-63-75 8.3-.6 52 0 52 0z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-topdown18000l"
			x1={1438}
			x2={1438}
			y1={693.694}
			y2={432.694}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000l)"
			d="M1525 1334h-131l-2-186s-9.6 62.4-41 129c17.2 54.2 26.8 93.8 83 132 17.5-4.9 30-9 30-9l17-42h42l2-24z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown18000m"
			x1={613}
			x2={613}
			y1={709.293}
			y2={430.293}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000m)"
			d="M548 1335h89v-208s10.6 87.4 42 154c-17.2 54.2-27.8 86.8-84 125-17.5-4.9-24-6-24-6l-18-42h-6l1-23z"
			opacity={0.502}
		/>
		<path
			fill="#d9d9d9"
			d="M1079 1127s6.1-700.1 47-859c-2.5-10.7-10.4-17.1-11-16-49.9 97.6-61 833-61 833s-77.7 1.1-85 1c0-91.6 8-756.4-41-837-4.9 3.1-11.2 8.5-11 18 14.4 30.6 24 133.8 27.6 258.9 3.3 110 5.1 237.1 4 346.6-1.2 123.6-5.6 225-5.6 254.5"
		/>
		<linearGradient
			id="ch-topdown18000n"
			x1={1022.5}
			x2={1022.5}
			y1={833.6311}
			y2={1683.7983}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={0.503} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000n)"
			d="M969 1086s73.9.8 85 0c0-144.8 21.7-775.8 62-834-47.8-14.4-115.6-25.5-187-3 53.1 127.5 40 837 40 837z"
		/>
		<path fill="#545454" d="m975 1270 78 1 11 17 10 77v19l5 12-3 18-18 19h-89l-18-19-2-19 7-12-2-22 11-72 10-19z" />
		<path fill="#fff" d="M1065 1319v52H965v-53l100 1z" opacity={0.102} />
		<path fill="#fff" d="M1064 1318v-30l-11-18-78 1-11 19 1 28h99z" opacity={0.251} />
		<linearGradient
			id="ch-topdown18000o"
			x1={1080.847}
			x2={980.8336}
			y1={599.5}
			y2={599.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.43} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={0.569} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000o)"
			d="M964 1369c-.2-33.3 1-80 1-80l10-19 78 1 11 17v83s-71.5-.7-100-2z"
			opacity={0.502}
		/>
		<path fill="#fff" d="m965 1370 27 1v62h-23l-18-19 14-44z" opacity={0.102} />
		<radialGradient
			id="ch-topdown18000p"
			cx={1030.303}
			cy={577.309}
			r={26}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" />
			<stop offset={0.5} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18000p)"
			d="M1030 1316c14.4 0 26 11.6 26 26s-11.6 26-26 26-26-11.6-26-26 11.6-26 26-26z"
			opacity={0.502}
		/>
		<path
			d="M944 1077c2.5-43 20-669.6-27-811-113 21.7-175.3 102.8-196 156 11.9 7 19 7.9 19 31 0 10.5-17.8 71-29 142-13.5 85.4-21 181.7-21 211 7.6 24.7 26.2 84.7 68 138 49.5 63.2 129.9 123.8 186 133zm139.2 0c-2.4-43 5.1-666 42-806 91.4 22.4 162.2 98.8 182.8 152-11.9 7-26.8 15.2-27 29 0 10.5 20.7 72 32 143 13.5 85.4 20 179.7 20 209-7.6 24.7-26.2 86.7-67.9 140-49.5 63.2-125.8 123.8-181.9 133z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-topdown18000q"
			cx={1011.5}
			cy={671.5}
			r={489.5309}
			gradientTransform="matrix(1 0 0 1.2859 0 -191.9986)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4165} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.5} />
		</radialGradient>
		<path
			fill="url(#ch-topdown18000q)"
			d="M944 1077c2.5-43 20-669.6-27-811-113 21.7-175.3 102.8-196 156 11.9 7 19 7.9 19 31 0 10.5-17.8 71-29 142-13.5 85.4-21 181.7-21 211 7.6 24.7 26.2 84.7 68 138 49.5 63.2 129.9 123.8 186 133zm139.2 0c-2.4-43 5.1-666 42-806 91.4 22.4 162.2 98.8 182.8 152-11.9 7-26.8 15.2-27 29 0 10.5 20.7 72 32 143 13.5 85.4 20 179.7 20 209-7.6 24.7-26.2 86.7-67.9 140-49.5 63.2-125.8 123.8-181.9 133z"
		/>
		<path
			fillRule="evenodd"
			d="M1280 450c5.4-12.6 13.3-38 116-38 51.6 0 197 1 197 1s147.5-1.8 187 29c47.3 36.8 61 26.2 61 146 0 17.6-1 95.1-1 121s-3.9 99.7-20 149c-17.4 53.2-93.3 163-264 163-9.3 0-26.2-1.5-45.8-2.4-48.5-2-113.9-4.6-125.2-5.6-15.8-1.4-46-4.8-46-53 0-225.8-22.4-367.5-40-443-9.4-40.3-20.9-62.6-19-67zm-701-35c134.1 0 160 4.4 160 28 0 37.2-20.7 97.4-22 118s-34.6 171.8-29 408c.9 38.2-34.7 45.3-55 47s-149.4 3-171 3-132.6-5.4-187-41-61.4-91.2-79-185c-5.4-28.5-9-204.4-9-232 6.8-90 9-149 230-149 30.2 0 137.3 3 162 3zm973 265c0 57.8 47.6 91 86 91s90-32.7 90-88-45.1-90-92-90c-31.8 0-84 29.2-84 87z"
			className="factionColorSecondary"
			clipRule="evenodd"
		/>
		<linearGradient
			id="ch-topdown18000r"
			x1={1279.7943}
			x2={1841}
			y1={1203.5}
			y2={1203.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000r)"
			d="M1280 450c5.4-12.6 13.3-38 116-38 51.6 0 197 1 197 1s147.5-1.8 187 29c47.3 36.8 61 26.2 61 146 0 17.6-1 95.1-1 121s-3.9 99.7-20 149c-17.4 53.2-93.3 163-264 163-9.3 0-26.2-1.5-45.8-2.4-48.5-2-113.9-4.6-125.2-5.6-15.8-1.4-46-4.8-46-53 0-225.8-22.4-367.5-40-443-9.4-40.3-20.9-62.6-19-67z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown18000s"
			x1={739}
			x2={187}
			y1={1204.5}
			y2={1204.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown18000s)"
			d="M579 415c134.1 0 160 4.4 160 28 0 37.2-20.7 97.4-22 118s-34.6 171.8-29 408c.9 38.2-34.7 45.3-55 47s-149.4 3-171 3-132.6-5.4-187-41-61.4-91.2-79-185c-5.4-28.5-9-204.4-9-232 6.8-90 9-149 230-149 30.2 0 137.3 3 162 3z"
			opacity={0.2}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M917.1 264.8C782.8 294.4 738.6 383 721 421c-38.8-7.2-166.5-8-263-8-.3-12.7 0-19 0-19H189v47H85v36H63v35h23v38H64l1 39h22v43H66v41h23l13 801h183l8-484s13.4 5.4 20 8c3.5 19.4 32 174 32 174s-21 36-21 91 27.7 87.9 38 100c.1 32.9 1 111 1 111h183v-31.5m46.2-35.6c32-19.9 71.5-57.8 85.8-127.9 34.9 72.1 170.5 176 333 176 118.5 0 288.1-76.8 339-181 7.6 35 28.6 101 84 135.6m82.2 33.5c-2.7 34-7.2 81.9-7.2 81.9h37l-1 363h261l1-363h35l-19-245h-17v-241h37l-15-209s13-56.7 13-175c0-56.2 2.7-140.2-9-159 10.4-21.5 36-71 36-71l-1-272h-382v246s-25.2 7.8-34 14c-23.2-.8-98.5-4.3-143 9-12.2-23.3-57.5-115.8-182.9-152.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m344 1166 16 70" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M681 1286c-10.5-20.8-31.2-80.9-44-152m709 149c22.5-39 43.2-111.3 47-152"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1080 1077c30.1 0 218.7-95 254-282m2 51c1.9 42.7-95.2 218.4-257 259"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M943.4 1077.6c-30.1 0-218.6-95.1-253.9-282.1m-1.3 59.6C697 909 792 1067.3 944.4 1105.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1356 1007c-5.2 33.3-25.3 125.6-69 176-53.6 61.8-112.9 102-226 102-3.6-6.3-8-15-8-15h-79s-6.8 12.5-10 18c-80.7 0-177.1-39.6-240-113-45.7-53.4-55.4-154.1-57-167"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1012 1269v-140" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M691.7 1116.9c1.1-44.6 14.7-151.8 24.3-191.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1323.5 1123.7c-.6-41.8-13.4-148.4-22.9-192.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1073 1364c-2.2-19-7.1-68-8.8-76.3-.6-1.3-1.2-1.7-1.2-1.7v39l1 45 12 45s1-.3 1-.8c0-2.1 1-10.4 1.1-19.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M954.5 1363.8c2.2-19.1 7.1-66.3 8.8-74.5.6-1.3 1.2-1.7 1.2-1.7v37.1l-1 45.1-12 45.1s-1-.3-1-.8c0-2.1-1-10.4-1-19.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M958.3 1451.5c-8.4-8.1-23.3-22.5-23.3-22.5l16-15s9.6 10.9 18.1 19h88.9l19-20 13 17s-17.3 16.7-21.3 20.3c-.5.4-1.7.7-1.7.7l-8-17m-91 0-10 18m106-82H963m2-53h98"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1089 1430s106.3-8.6 224-125c41.6-46.8 60-145.1 75-222-8.2-17.8-15-33-15-33s-33.5 176.8-70 223c-35.8 45.2-90.6 84.7-166 100m235-316c1.6-12.6 7.7-39.1 9-46"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M938.8 1430.5s-106.4-8.6-224.1-125.1c-41.6-46.8-60-145.2-75-222.1 8.2-17.9 15-33 15-33s33.5 177 70.1 223.1c35.8 45.3 90.7 84.7 166.1 100.1M654 1049c-1.9-9.1-4.5-32.8-5-36"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1073 1365v19l64 9v-20l-64-8z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1137 1393-59 2-5-11" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M955.5 1364.8v18.8l-64 8.9v-19.8l64-7.9z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m891.5 1392.5 59 2 5-10.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1012.5 1098.3c13.9 0 25.2 4.3 25.2 9.7s-11.3 9.7-25.2 9.7-25.2-4.3-25.2-9.7 11.3-9.7 25.2-9.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M186 441h54" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1808 1525h-264m-35 0 21-245h288m-274 2 2-242m294 0h-330l2-22s259.5 33.5 316-187m6-331c-5.4-18.4-40.9-74.8-138-82-126-9.3-223.8-4-257-4m429 12s-108.7-1-129-1m-254-29v15"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M459 414c-17.8 0-193.5-14.1-248 49-27.1 35.9-23.5 103.5-22 172s-.3 231.8 52 311 218.2 73 264 73 121.5-1.3 134-3 48.5-6.1 49-43 1.2-139.4 2-165 14.5-215 42-315c21.3-65.3-6.4-70.1-15-72"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M188 590h-31l6 219h35" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M88 673h58v-41H84m3-43h57v-39H84m3-38h57v-35H84" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m362 1371-8-363m199 10-7 425m1-108h90v-320" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1524 1335h-131l-1-321s58.2 5 119 5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1393 1173h151" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M551 1173h87" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1395 1014c-9-.9-29.8-4-38-9s-18-18.1-18-41c0-22.7-2.8-363.3-58-509-.7-1.8-.4-6 0-8 6-15.1 21.7-24.2 33-26"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1639 593c48.6 0 88 39.4 88 88s-39.4 88-88 88-88-39.4-88-88 39.4-88 88-88z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1639 621.8c32.7 0 59.3 26.5 59.3 59.3 0 32.7-26.5 59.3-59.3 59.3s-59.3-26.5-59.3-59.3c.1-32.8 26.6-59.3 59.3-59.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1638.5 606.7c40.8 0 73.8 33 73.8 73.8s-33.1 73.8-73.8 73.8-73.8-33-73.8-73.8 33-73.8 73.8-73.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1125 267c-37.1 117.9-46 861-46 861H941c3.2 0 6.7-193.2 5.4-398-1.3-199.7-6.9-410.4-30.4-464"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1079 1128-24-41h-85l-25 42m-29-863c2.1-8.8 6.7-15.2 15-18m183 3c8.2 5.6 9.9 10.4 11 19m-157 820s10.4-698.4-34.2-827.9c-1.9-5.4-3.8-9.7-5.8-13.1 65.7-21.6 140.4-9.9 187 2-2.3 5.1-3.9 9.4-6 14-19.7 42.2-51 395.2-56 823"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M547.6 1357h6.4l17 42 57 20 11 23s-42.3-.2-92.6-.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m571 1401 16 41" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1523.7 1357.5H1481l-17 42-57 20-11 23s59.1-.2 120.8-.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1464 1398.5-18 44" />
	</svg>
);

export default Component;

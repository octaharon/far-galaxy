import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#999"
			d="M1309 1186.8c-2.7-20.9-14.6-43.9-22.9-47.2-8.3-3.2 3.9-96.7 3.9-96.7l-153 2-71 38s77.9 100.8 78.4 103.8c.5 3.1 34.8 20.3 35.9 20.1 1.1-.2 69.8 0 69.8 0s61.7.9 58.9-20z"
		/>
		<path
			fill="#ccc"
			d="M1217.2 1229c38.2 0 98.1-11 97.8-34.1-.3-23.2-9.2-22.7-92.8-5-50.7 11.9-74.2 1.2-74.2 1.2l-4.6 13.9-1 21.1c0-.2 36.6 2.9 74.8 2.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1307.3 1177.7c-4.4-18-14.1-35.2-21.2-38-8.3-3.2 3.9-96.7 3.9-96.7l-153 2-71 38s77.9 100.8 78.4 103.8c.2.9 3 2.9 7.1 5.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1217.2 1229c38.2 0 98.1-11 97.8-34.1-.3-23.2-9.2-22.7-92.8-5-50.7 11.9-73.2.2-73.2.2l-5.6 14.9s-6.1 16.3-1 21.1c5.1 4.7 36.6 2.9 74.8 2.9z"
		/>
		<path
			fill="#999"
			d="M612 1186.6c2.8-20.9 14.6-43.9 23-47.1 8.3-3.2-3.9-96.5-3.9-96.5l153.2 2 71.1 37.9s-78 100.6-78.5 103.6-34.8 20.2-36 20c-1.1-.2-70 0-70 0s-61.6 1-58.9-19.9z"
		/>
		<path
			fill="#ccc"
			d="M704 1228.7c-38.3 0-98.3-10.9-97.9-34.1.3-23.1 9.2-22.6 92.9-5 50.8 11.9 74.3 1.2 74.3 1.2l4.6 13.9 1 21s-36.7 3-74.9 3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M613.8 1177.5c4.4-18 14.1-35.2 21.3-37.9 8.3-3.2-3.9-96.5-3.9-96.5l153.2 2 71.1 37.9s-78 100.6-78.5 103.6c-.2.9-3 2.9-7.1 5.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M704 1228.7c-38.3 0-98.3-10.9-97.9-34.1.3-23.1 9.2-22.6 92.9-5 50.8 11.9 73.3.2 73.3.2l5.6 14.8s6.1 16.2 1 21c-5.1 4.9-36.7 3.1-74.9 3.1z"
		/>
		<path
			fill="#ccc"
			d="M1179.1 233.9s21-43.1 24.8-60.9c8.6 22.5 23.7 80.6 28 80.4l21.7-79.4s57.6 100.4 38.5 108c-12.1-2.1-38-8.6-81 10s-32-58.1-32-58.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1231.9 253.5 21.7-79.4s57.6 100.4 38.5 108c-12.1-2.1-38-8.6-81 10s-31.9-58.1-31.9-58.1 21-43.1 24.8-60.9c7.7 20.1 20.6 68.9 26.3 78.7"
		/>
		<path
			fill="#999"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1259.9 276.5c21.4-3.8 43 5.3 47.1 35 3 21.5 2.2 63.1-41 74-103 25.7-65.1-121.9-65.1-121.9s22.8 17.9 59 12.9z"
		/>
		<path fill="#e6e6e6" stroke="#1a1a1a" strokeWidth={10} d="m1232.9 316.4 60.1-1 3 91-62.1 1-1-91z" />
		<linearGradient
			id="ch-topdown40001a"
			x1={1117.5656}
			x2={1269.7115}
			y1={2166.6851}
			y2={2111.3079}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#969696" />
			<stop offset={0.352} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40001a)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1161.8 211.5c75.6 8.3 153.1 145.8 80.1 268.8s-108.1-120.9-108.1-120.9-47.6-156.3 28-147.9z"
		/>
		<path
			fill="#ccc"
			d="M739.7 239.4s-21-43.1-24.8-60.9c-8.6 22.5-23.6 80.6-27.9 80.5l-21.6-79.5S614.7 265.6 622 286c10.9-9 30.1-11 54.8-9.6 9.3.6 19.9-2.2 31.7 2.9 42.9 18.7 31.2-39.9 31.2-39.9z"
		/>
		<path
			fill="#999"
			d="M659 277c-21.4-3.8-42.9 5.3-47 35-3 21.5-2.2 63.2 41 74 102.9 25.8 65-122 65-122s-22.9 18-59 13z"
		/>
		<path fill="#e6e6e6" d="m686 317-60-1-3 91 62 1 1-91z" />
		<linearGradient
			id="ch-topdown40001b"
			x1={649.8002}
			x2={801.7642}
			y1={2175.5447}
			y2={2120.2339}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#969696" />
			<stop offset={0.352} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40001b)"
			d="M757 212c-75.5 8.4-152.9 145.9-80 269s108-121 108-121 47.5-156.4-28-148z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M674 277.9c-4.7.1-9.7-.1-15-.9-21.4-3.8-42.9 5.3-47 35-2.1 15.1-2.3 40.1 12.6 57.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M657.4 316.5c-15.4-.2-31.4-.5-31.4-.5l-3 91s13.3.2 27.5.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M757 212c-75.5 8.4-152.9 145.9-80 269s108-121 108-121 47.5-156.4-28-148z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M730.8 220.2c-6.2-13.8-13.7-31.7-15.8-41.6-7 18.2-18.2 59.9-24.4 74.9m-4.1 3.8c-3.1-11.5-21.2-77.8-21.2-77.8S614.7 265.7 622 286c10.9-9 30.1-11 54.8-9.6"
		/>
		<linearGradient
			id="ch-topdown40001c"
			x1={496}
			x2={1425}
			y1={1811}
			y2={1811}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.251} stopColor="#bfbfbf" />
			<stop offset={0.495} stopColor="#bfbfbf" />
			<stop offset={0.754} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40001c)"
			d="M1130 1257c-12.6 3.7-131.9 14-169 14s-130.5-5.1-175-14c12.3-5.6 17.3-21.3 25.4-41.6-20.7-32.8-68.8-63.2-94.8-104.1-1.3-2 36.6 8.5 33.4-98.3-1-31.5-25.5-91-54.5-93.5.3-44.7 1.5-85.6 1.5-107.5 0-23.5-.4-57.5-3.7-83.3C654 794 671 840 671 840s-154.8-1.6-175-1c1.8-53.9 33.2-101.9 78-157 1.8-159.2 54.4-177.2 109.7-176.3 6.8-28.8 17.4-60.8 33.8-95.6 29.1-61.9 17-141.1 42.5-185.1s51.4-106 202.2-106 171 42.4 200.8 102 11.3 118.7 43.9 203.1c12.6 32.6 21.6 56.9 27.9 81.4 16.1 2.4 52.6.1 78.2 29.5 16.8 24.7 34.8 93.4 32.2 142.4 42 64.2 79.8 129 79.8 162.6-25.6.3-121.2.6-175 0 7-56.3-12.7-89.8-22-104-7 26.8-4.3 146.1 0 185-35.9 16.1-55.4 60.9-58 92-3 35.1-.1 95.7 38 101-30 46.3-64 65.5-97 81-3.9 17.5-3.1 44.2 19 62z"
		/>
		<linearGradient
			id="ch-topdown40001d"
			x1={508.5001}
			x2={780.5861}
			y1={1876.811}
			y2={1748.255}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.103} stopColor="gray" />
			<stop offset={0.493} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40001d)"
			d="M574 681c1.2-30-2.7-148.8 71-170s114.7 20.4 120 53 3.9 87.6-47 134-58.3 111.7-47 143c-27.7-.6-176-1-176-1s2.9-72.7 79-159z"
			opacity={0.5}
		/>
		<linearGradient
			id="ch-topdown40001e"
			x1={1406.3633}
			x2={1134.3701}
			y1={1893.6353}
			y2={1717.6083}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.103} stopColor="gray" />
			<stop offset={0.493} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40001e)"
			d="M1343 680.9c-1.2-30 2.7-149-71-170.1-73.7-21.2-114.6 20.4-120 53-5.3 32.6-3.9 87.7 47 134.1 50.9 46.4 58.3 111.7 47 143.1 27.7-.6 175.9-1 175.9-1s-2.8-72.7-78.9-159.1z"
			opacity={0.5}
		/>
		<radialGradient
			id="ch-topdown40001f"
			cx={208.232}
			cy={2253.5459}
			r={585}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.795} stopColor="#000" />
			<stop offset={0.97} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown40001f)"
			d="m800 336-62-21s-6.3 55.1-21 88-34.1 91.8-34 104c21.3-.9 83.8 2.2 84 88 5.5.2 14 0 14 0s-.1-75.6 5-145c4.3-59.3 14-114 14-114z"
			opacity={0.5}
		/>
		<radialGradient
			id="ch-topdown40001g"
			cx={1702.474}
			cy={2244.7061}
			r={585}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.795} stopColor="#000" />
			<stop offset={0.97} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown40001g)"
			d="m1119.5 338 62-21s6.3 55.1 21 88 34.1 91.8 34 104c-21.3-.9-83.8 2.2-84 88-5.5.2-14 0-14 0s.1-75.6-5-145c-4.3-59.3-14-114-14-114z"
			opacity={0.5}
		/>
		<linearGradient
			id="ch-topdown40001h"
			x1={693}
			x2={752}
			y1={1663.5}
			y2={1663.5}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-topdown40001h)" d="m693 919 1-190 57-70 1 367s-11.2-99.1-59-107z" opacity={0.302} />
		<radialGradient
			id="ch-topdown40001i"
			cx={983.098}
			cy={1509.2271}
			r={484}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.599} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown40001i)"
			d="M749 1010s34.6 74.2 84 128c55.8 60.8 127 102 127 102s-103.4-.3-152-30c-21.9-24.2-87.2-96.2-90-99 11.8-1.8 13.9-5.8 18-8 19.1-32.3 13-93 13-93z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown40001j"
			cx={932.175}
			cy={1521.0601}
			r={504}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.599} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown40001j)"
			d="M1170 1010s-34.6 74.2-84 128c-55.8 60.8-127 102-127 102s34.2 3.1 74-7c28-7.1 59.9-24.7 80-37 6.1-6.8 16.5-11.7 29-19 34.8-20.3 67-60.9 69-63-11.8-1.8-23.9-8.8-28-11-19.1-32.3-13-93-13-93z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-topdown40001k"
			x1={1169}
			x2={1228}
			y1={1663.5}
			y2={1663.5}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown40001k)" d="m1228 919-1-190-57-70-1 367s11.2-99.1 59-107z" opacity={0.302} />
		<path
			fill="#f2f2f2"
			d="M804 1237c-12.1.7-38.6-3-39 13s-4.7 225.9-4 249-11.4 148.8 183 230c11.7.5 34 1 34 1s103.7-58.1 121-76 60-49.1 60-171-5-238-5-238-21.9-6.7-37-5c7.1 9.3 11 16 11 16s-68.8 14-167 14-169-14-169-14 9.3-12.9 12-19z"
		/>
		<path
			d="m877 1288-2 282s11.4 112.8 58 146c-48.3-29-117.9-80.2-137-135s-6-317-6-317 18.2 7.3 39 13c22.6 6.2 48 11 48 11zm-28 282-3-138-40-11s-2.4 136.5 5 156c16.4-3.2 38-7 38-7zm243.7-293c20.8-5.8 38.9-13 38.9-13s13.1 262.2-6 317-88.5 106-136.6 135c46.5-33.2 57.8-146 57.8-146l-2-282s25.4-4.8 47.9-11zm18 300c7.4-19.5 5-156 5-156l-39.9 11-3 138s21.5 3.8 37.9 7z"
			className="factionColorPrimary"
		/>
		<path
			fill="#ccc"
			d="M1153 1246c0-12.6-37-6-37-6l14 16s-30 12-170 12-169-13-169-13 12.1-12 13-18c-16.5-.7-38.1-.4-38 11 0 2.1-10.5 188.7-1 290 2.1 22.7 10.2 39.7 14 51 20.9 61.6 96 103 96 103s19 6.6 33 11c-94.5-74.9-86.5-85.9-110-120s-10.7-298.1-9.5-319c29.2 12.1 86.8 25 172.5 25 86.3 0 142.8-13.5 170.9-26.2.8 12.3 16.2 261.1-7.9 321.2s-96.4 108.1-104 114c10.8 1.6 18-2 25-6s80.3-46.8 97-103c3.5-11.6 12.2-27.4 14-54 6.9-102-3-286.7-3-289z"
		/>
		<path
			fill="#dedede"
			d="m806 1420 40 10 1 140-35 8s-10.9-113.8-6-158zm70 151 170-1s-12.5 110.3-67 158c-19.5.7-38 1-38 1s-26.7-29.4-43-69c-16.2-39.4-22-89-22-89zm197 1 1-141 40-11s6.3 118.3-6 158c-14.2-2.2-35-6-35-6z"
		/>
		<linearGradient
			id="ch-topdown40001l"
			x1={960}
			x2={960}
			y1={935}
			y2={1220}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path fill="url(#ch-topdown40001l)" d="m875 1570 170 1v-284s-131.4 5.5-169-1c.8 37.8-1 284-1 284z" />
		<path
			fill="#f2f2f2"
			d="m697 918-20 8v-86l-197-1-1 135 5 14s-51.8 31.7 28 144c2.7 53.2 16 507 16 507l102-1 14-504 91-28s16-22.1 16-80c0-32.6-13-68.9-28-89-11.6-15.6-26-19-26-19zm535 1 10.1 7v-86l199.3-1 1 135-5 14s47.4 33.7-32.5 146c-2.7 53.2-11.6 505-11.6 505l-102.2-1-17.2-505-88-27s-16-22.1-16-80c0-32.6 13-62.9 28-83 11.7-15.6 34.1-24 34.1-24z"
		/>
		<path fill="#dedede" d="M548 1049h59l-14 33h-28l-17-33zm764 0c.6-.6 56 0 56 0l-15 33h-26s-15.6-32.4-15-33z" />
		<path d="m479 840 1 137 198-2-1-134-198-1zm762 0h198l1 136h-198l-1-136z" className="factionColorSecondary" />
		<linearGradient
			id="ch-topdown40001m"
			x1={678}
			x2={479}
			y1={1597.5}
			y2={1597.5}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown40001m)" d="m479 840 1 137 198-2-1-134-198-1z" opacity={0.2} />
		<linearGradient
			id="ch-topdown40001n"
			x1={676}
			x2={479}
			y1={1503.5}
			y2={1503.5}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown40001n)" d="M502 1029h157l17-53H479l23 53z" opacity={0.2} />
		<linearGradient
			id="ch-topdown40001o"
			x1={1441}
			x2={1242}
			y1={1597.5}
			y2={1597.5}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown40001o)" d="m1441 840-1 137-198-2 1-134 198-1z" opacity={0.2} />
		<linearGradient
			id="ch-topdown40001p"
			x1={1438}
			x2={1242}
			y1={1503}
			y2={1503}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-topdown40001p)" d="m1260 1029 160 1 18-54h-196l18 53z" opacity={0.2} />
		<radialGradient
			id="ch-topdown40001q"
			cx={963.627}
			cy={2134.655}
			r={375.7}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.802} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-topdown40001q)"
			d="M738 316c3.2-28.7-3.7-203.8 223-201 50.8.6 123.5 4.4 161 46 50.5 56 58 154 58 154s-136 43.8-176 100c-10.6-42.4-33.7-87.8-43-88s-29 37.3-43 84c-9.2-10.5-102-82.7-180-95z"
			opacity={0.2}
		/>
		<path
			fill="#999"
			d="M1113 1194s-17.5 45 23 64c-59.3 7.7-110.5 11-175 11s-167-13-167-13 21-20.4 12-46c21 11.8 73.6 31 154 31 32.5 0 63.3-3.9 89-13 37.9-13.5 64-34 64-34z"
		/>
		<linearGradient
			id="ch-topdown40001r"
			x1={723.5934}
			x2={645.1664}
			y1={1366.6661}
			y2={1582.1442}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.497} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#999" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40001r)"
			d="M645 1134.5c37-11.7 77.3-24.5 84-26.5 12.4-3.7 28.8-58.5 18-108-8-36.6-22.9-77.1-52-81 0 0-6.7 2-17.7 5.2-.5 38.3-1.3 52.8-1.3 52.8l-19 55h-7s-5.7 72.6-5 102.5z"
		/>
		<path
			fill="#e3e3e3"
			d="M483 988c-26.3 11.2-11.5 70.4 0 99 5.5 13.8 16.4 33.1 30.7 48.1-.5-28.1-1.7-105.1-1.7-105.1l-14-1s-9.3-24.9-15-41z"
		/>
		<linearGradient
			id="ch-topdown40001s"
			x1={1258.4741}
			x2={1205.29}
			y1={1582.2905}
			y2={1368.9816}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.497} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#999" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40001s)"
			d="M1274 1134.3c-37.1-11.7-77.8-24.6-84.5-26.6-12.4-3.7-28.8-58.4-18-107.8 8-36.6 33.9-75 63-78.9 0 0 2.3.7 6.3 1.9-.7 24 .1 51.1.1 51.1l21 57 8-1c.1 0 4.5 82.4 4.1 104.3z"
		/>
		<path
			fill="#e3e3e3"
			d="M1404.5 1134.3c8-8.2 16.5-19.1 25-33.6 44.2-84 9.1-111 8-111.8-8.4 17.2-17.5 41.1-17.5 41.1l-12 1s-1.9 65.8-3.5 103.3z"
		/>
		<linearGradient
			id="ch-topdown40001t"
			x1={1156.9597}
			x2={764.2402}
			y1={1820.5129}
			y2={1820.5129}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-topdown40001t)"
			d="M1004 1199c48-5.6 143-103.9 151.2-205.8 8.4-105-16-221.2-16-415.2 0-190.8-16.2-399.1-172.2-406-2.6-.1-10.4.1-13 .2-155.8 6.9-172 215.1-172 405.8 0 193.9-24.4 310.1-16 415 8.2 101.9 103.1 200.2 151 205.8.7.1 22.4-4.8 44-4.8 21.2.1 42.3 5.1 43 5z"
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M695 920.9c-.3-69.1 2-137.5 2-164.9 0-10.8-.8-20.3-2.2-28.7m-13-222c6.8-30.2 17.6-63.8 34.7-100.1 29.1-61.9 16-141.1 41.5-185.1s52.4-106 203.2-106 167 44.4 196.8 104 15.3 116.7 47.9 201.1c13 33.8 22 59.1 28.2 85.1m-7.3 228.4c-2.3 18.6-3.8 43.3-3.8 77.3 0 42.5 3.1 79.3 6 112.6m-20.2 191.3c-24 38.6-64.8 62.9-96.8 80.3-17.9 13.4-59.1 47.7-151 47.7-56.1 0-140-15.4-150.6-31.6-20.4-31.2-65-60.8-91.6-99.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M783.8 1259c15.3-10.9 32.9-30.8 20.2-57"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1136.8 1260.7c-15.9-10.6-38.4-34.8-24.5-63.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1405.1 1133.7c7.9-8.1 16.1-18.9 24.4-33 44.2-84 9.1-111 8-111.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1406.3 1086.7c.7-1.3 1.4-2.6 2-3.9 9.3-19.4 8.5-39.3-1.8-44.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1274.3 1134.4c-37.2-11.8-78.1-24.7-84.8-26.7-12.4-3.7-28.8-58.4-18-107.8 8-36.6 33.9-75 63-78.9 0 0 2.3.7 6.3 1.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M645.2 1134.4c36.9-11.7 77.1-24.4 83.8-26.4 12.4-3.7 28.8-58.5 18-108-8-36.6-22.9-77.1-52-81 0 0-6.4 1.9-16.9 4.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1004 1199c48-5.6 143-103.9 151.2-205.8 8.4-105-16-221.2-16-415.2 0-190.8-16.2-399.1-172.2-406-2.6-.1-10.4.1-13 .2-155.8 6.9-172 215.1-172 405.8 0 193.9-24.4 310.1-16 415 8.2 101.9 103.1 200.2 151 205.8.7.1 22.4-4.8 44-4.8 21.2.1 42.3 5.1 43 5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M514.1 1136.9c-8-8.7-16.4-20.4-25.1-35.9-44-87.3-9.1-111.2-8-112"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M512.3 1080.3c-8.6-18.9-10.4-37.7-.3-42.6" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M737 315c14.4 2.9 38.2 10.6 64.1 21.4" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1184 315c-14.4 3-38.3 10.6-64.3 21.5" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M978.1 1728s92.3-48.4 118.9-72 38.6-45.3 54.9-93 2.7-255.2 3-313c-54.8 43.9-179.8 39.2-190 39.2h-8.8c-10.2 0-135.3 4.7-190.2-39.2.3 57.8-13.3 265.3 3 313s28.4 69.4 55 93 119 72 119 72h35.2z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1114.2 1237.8c39.3 2.4 43.4 2.6 39.8 13.2m-23.3 5.8c-40.4 7.8-105.4 14.8-170.4 14.7-65.3-.1-130.6-7.2-170.7-15.7M768 1250c-11.9-12.4 29.4-14.2 36-14"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M878 1289s-3 178.1-3 268 49 153.7 68 170m-9-7c-35.9-22.9-117-83.1-137-138-3.4-11.9-9.5-33.3-11-59-5.8-96.4 3-259 3-259"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1044 1289s3 178.1 3 268-49 153.7-68 170m9-7c35.9-22.9 117.1-83.1 137-138 3.4-11.9 9.5-33.3 11-59 5.8-96.4-3-259-3-259"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M784 1588c29.6-11.7 48.6-18.8 92-18 17.8.3 49.7.2 69 .1 9.6 0 21.5 0 31 .1 19.3.1 51.2.2 69-.1 43.4-.8 62.4 6.3 92 17.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M848.4 1570.4c-1.4-38.5-1.3-114.6-.4-140.4-21.8-5.4-43-11-43-11s.8 122.5 6.6 158"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1073 1570.4c1.4-38.5 1.3-114.6.4-140.4 21.6-5.4 42.6-11 42.6-11s-.8 122.6-6.6 158"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M821 1426s2.3 64.1 3.2 107c.4 21.7 2.8 37 2.8 37"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1100 1426s-2.3 64.1-3.2 107c-.5 21.7-2.8 37-2.8 37"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M527 1640h103l18-610h10l20-54V840H479v136l21 54h11l16 610z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M511 1030h137m29-54H482m66 55v16l18 35h27l15-35v-14m-2 15h-56"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M628 560c-3 14.6-2.7 67.9-61 131s-72 135.1-72 149m79-157c-1.7-57.7 9.3-126.1 42-157s148-39.5 151 57-55.3 94.9-89 178c-18.6 38.2-7 79-7 79M563 696c42.3-75.3 157.3-68.7 152 6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1392 1640h-103l-18-609.9h-10l-20-54v-136h199v136l-21 54h-11l-16 609.9z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1408 1030.1h-137m-28.9-54.1H1437m-66 55.1v16l-18 35h-27l-15-35v-14m2 14.9h56"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1291.1 560.1c3 14.6 2.7 67.9 61 131s72 135.1 72 149m-79.1-157c1.7-57.7-9.3-126.1-42-157s-147.9-39.5-151 57c-3 96.5 55.3 94.8 89 178 18.6 38.2 7 79 7 79m108-144c-42.3-75.3-157.2-68.7-152 6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1004 1199c48-5.6 143-103.9 151.2-205.8 8.4-105-16-221.2-16-415.2 0-190.8-16.2-399.1-172.2-406-2.6-.1-10.4.1-13 .2-155.8 6.9-172 215.1-172 405.8 0 193.9-24.4 310.1-16 415 8.2 101.9 103.1 200.2 151 205.8.7.1 22.4-4.8 44-4.8 21.2.1 42.3 5.1 43 5z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M856 1164c27.5-20.3 52.6-28 105-28s104 28 104 28" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M776 761c39.9 40.6 127.9 46.2 170.2 46.9 5.4.1 23.4.2 28.8.1 42.2-.7 130.1-6.3 170-47"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M785.4 446.3c43 33.1 121.6 37.9 160.8 38.6 5.4.1 23.4.2 28.8.1 39.3-.7 118.1-5.6 161-39"
			/>
		</g>
	</svg>
);

export default Component;

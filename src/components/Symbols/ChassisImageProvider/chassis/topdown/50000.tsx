import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<filter id="ch-topdown50000h">
			<feGaussianBlur stdDeviation={35} />
		</filter>
		<path
			fill="#f2f2f2"
			d="M1049.5 16.8c16.5 9.8 45.3 140.1 61.5 214.7 18-.2 51.5 10.1 59.3 72.3 5.8 45.7-21.8 72.6-25.9 75.5 5.3 20 18.3 58.3 18.3 58.3s69.4 37.6 74.4 129.5c10.6 9.1 18.3 28 18.3 28s19.4 140.5 19.4 240.6-12.9 557.8-12.9 661.3c-11.7 130.1-27.6 322.2-50.7 366.8-4.5-.4-17.3-12.9-17.3-12.9v-21.6c4.7-173.7 9.7-347.4 9.7-347.4s7.8-9 11.5-12.2c2.4-40.3-1.3-171.3-3-236.4-5-5.5-12.1-12.5-15.4-16.6-.4-14.2-.6-22.9-.6-22.9s1.2-199-48.5-212.5c-7.1 41.5-47.1 408.9-106.8 549.1-4 9.5-42.3 36.9-80.6 37-38.5 0-77.1-27.3-81.2-36.9-59.7-140.2-99.8-507.7-106.8-549.1-42.9-1.9-46.9 183.4-49.1 233.9-8.6 6.1-12.3 10.5-17.8 17.4-1.7 65.5-4.1 194.4-1.1 236.3 7 8.2 11.9 13.1 11.9 13.1s5 173.7 9.7 347.4v21.5s-12.7 12.6-17.3 13.1c-23.1-44.6-39.1-236.7-50.7-366.8 0-103.5-12.9-561.2-12.9-661.3s19.4-240.6 19.4-240.6 7.8-19 18.3-28c5.1-91.8 74.4-129.5 74.4-129.5s13.1-38.3 18.3-58.3c-4.1-2.9-31.7-29.8-25.9-75.5 7.9-62.1 41.3-72.5 59.3-72.3 16.2-74.5 46.2-203 61.5-214.7 7.2-5.6 165.9-8.3 179.3-.3z"
		/>
		<path
			fill="#e6e6e6"
			d="M1182.2 665.2s33.8 424.8 31.3 563.1c-6.4-5.5-18.3-12.9-18.3-12.9s3.5-235.2-50.7-235.2c-26.1 0-22.7-53.8-22.7-61.5s35-188.6 30.2-264.3c10.9 8.7 30.2 10.8 30.2 10.8zm-446.6 0s-33.7 424.8-31.2 563.1c6.4-5.5 18.3-12.9 18.3-12.9s-3.5-235.2 50.6-235.2c26 0 22.5-53.8 22.5-61.5S761 730.1 765.7 654.4c-10.8 8.7-30.1 10.8-30.1 10.8z"
		/>
		<linearGradient
			id="ch-topdown50000a"
			x1={756.7982}
			x2={790.9175}
			y1={2134.7236}
			y2={2282.5154}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000a)"
			d="M807.9 230.4c-18.7.8-48.7 15.9-56.1 58.3s8.2 77.5 25.9 89.5c5.1-4.9 28.2-120.6 30.2-147.8z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown50000b"
			x1={1162.8073}
			x2={1132.5825}
			y1={2131.5186}
			y2={2281.4136}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000b)"
			d="M1112.1 230.4c26.1.3 50.5 23.4 57.2 65.8s-7.6 66.5-27 84.1c-3.8-23.2-23.1-104.6-30.2-149.9z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-topdown50000c"
			x1={772.454}
			x2={812.4031}
			y1={1472.8445}
			y2={1466.5165}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.8} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000c)"
			d="M802.5 929.5c5.7 17 21.8 134-6.5 215.8-11.5-30.3-23.7-164-23.7-164s23.9 4.8 30.2-51.8z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-topdown50000d"
			x1={1141.8184}
			x2={1102.6461}
			y1={1480.549}
			y2={1477.1217}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.8} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000d)"
			d="M1118.6 925.2c-8.6 19.2-20.5 131.7 7.6 203.9 5.2-27.3 21.6-146.7 21.6-146.7s-25.7 5.2-29.2-57.2z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-topdown50000e"
			cx={800.1493}
			cy={1948.3726}
			r={139.9047}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4} stopColor="#000" stopOpacity={0} />
			<stop offset={0.899} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50000e)"
			d="M803.6 584.3s27.5-70.7 11.9-98.2-47.5-46.4-58.3-45.3-93.9 49.6-68 185.5c13.5 27.9 39.2 48.7 70.1 34.5 15.9-17.7 44.3-76.5 44.3-76.5z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown50000f"
			cx={1118.2129}
			cy={1957.0837}
			r={139.8701}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4} stopColor="#000" stopOpacity={0} />
			<stop offset={0.899} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50000f)"
			d="M1115.8 584.2s-27.5-70.7-11.9-98.1 47.4-46.3 58.3-45.2c10.8 1.1 93.7 49.6 68 185.3-13.6 27.9-39.2 48.7-70.1 34.5-15.8-17.9-44.3-76.5-44.3-76.5z"
			opacity={0.302}
		/>
		<path fill="#e6e6e6" d="M931.9 759h55l-15.1 33.4s-26.8 3-25.9 2.2c.9-.8-14-35.6-14-35.6z" />
		<linearGradient
			id="ch-topdown50000g"
			x1={959.5}
			x2={959.5}
			y1={2496.4009}
			y2={1991.8999}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.701} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000g)"
			d="M870.5 16.8c28.8-6.4 168.1-11.3 180.2-2.2 22.4 44 50.6 398.9 74.4 441.2-6.5 8.6-26.9 25.7-25.9 52.9-27.1-14-151.2-89.2-276.2 5.4-6.3-29.8-13.9-47.8-29.1-57.2 28.7-76 47.8-433.7 76.6-440.1z"
			opacity={0.102}
		/>
		<path
			fill="#ccc"
			d="M791.7 455.9c15.2-32.6 50.9-314.8 55-361.4-17.6 28.8-67.3 284.1-88.5 343 11.9 3.7 22.8 11.9 33.5 18.4zm334.4 0c-15.2-32.6-48.8-314.8-52.9-361.4 17.6 28.8 65.2 284.1 86.3 343-11.8 3.7-22.7 11.9-33.4 18.4z"
		/>
		<path
			fill="#e6e6e6"
			d="m655.8 1469.9 48.5-2.2v-239.5l-51.8 2.2 3.3 239.5zm556.6-245.9 56.1 7.6-5.4 238.4-44.2-2.2-6.5-243.8z"
		/>
		<path d="m1032.3 711.6-146.6.1 18.2 47.3 16.2 464.1 75.5-.2 14-465 22.7-46.3" />
		<path
			d="M659 1469.9c5.9-4.1 35.6-3.9 44.2-2.2 3.8 2 10.8 12.2 11.9 12.9 2.7 108.3 12.9 321.3 12.9 366.8-9.4 8.6-16.9 13.7-22.7 15.1-32.4-116-48.1-366.1-46.3-392.6zm21.6-897.5c10.6 90.4 32 86.3 53.9 94.9-15.1 127.8-30.2 512.2-30.2 557.7-14.2 5-45 5.1-50.7 6.5-.2-74.2-10.7-233.8-7.6-384 2.2-105 10.7-199.9 18.3-254.6 9.9-16.4 16-16.1 16.3-20.5zm580.8 897.5c-5.9-4.1-35.8-3.9-44.4-2.2-3.8 2-10.8 12.2-12 12.9-2.7 108.3-13.1 321.3-13.1 366.8 9.5 8.6 17 13.7 22.8 15.1 32.7-116 48.5-366.1 46.7-392.6zm-21.7-897.5c-10.7 90.4-32.3 86.3-54.3 94.9 15.2 127.8 30.4 512.2 30.4 557.7 14.3 5 45.3 5.1 51 6.5.2-74.2 10.8-233.8 7.6-384-2.2-105-10.8-199.9-18.4-254.6-9.8-16.4-16-16.1-16.3-20.5z"
			className="factionColorPrimary"
		/>
		<path fill="#f2f2f2" d="M703.2 1469.9s4.1 307.2 12.9 383c5.5-1.1 6.5-6.5 6.5-6.5l-5.4-363.5-14-13z" />
		<path fill="#f2f2f2" d="M1217.8 1469.9s-4.1 307.2-12.9 383c-5.5-1.1-6.5-6.5-6.5-6.5l5.4-363.5 14-13z" />
		<path
			d="m864 708.3 19.4 5.4 21.6 49.6 14 460.6h79.8l11.9-466 19.4-45.3 20.5-5.4-22.7 837.1s-66.5 54.2-139.2-5.4c-1.2-80.3-24.7-830.6-24.7-830.6z"
			className="factionColorSecondary"
		/>
		<path
			d="M919.7 1223.3c12.3.7 77 5 77.8-5.5.8-10.5 13.9-451.3 14-459.9 4.8-8.2 25.1-43.5 9.8-43S881 702 888.7 720.7c7.7 18.7 15.3 31.3 15.7 49 .3 17.6 15.3 453.6 15.3 453.6z"
			filter="url(#ch-topdown50000h)"
			opacity={0.3}
		/>
		<path
			fill="#f2f2f2"
			d="M951.4 467c48.6.7 130 24.9 147.8 41.7 4.5 27.8 8.7 61.6 27.9 99.3-10.7 24.5-34.3 88.5-95.4 103-2 7.5-12.7 46.8-24.3 47-6.3.1-9.7 2.5-51.7 0s-56 11-66.8-29.6c0 5.3 2.1-13.4-15.9-16.9s-64.8-40.8-75.8-112.8c11.3-31.5 22.2-74.3 23-85.4 12.8-10.1 72-50 131.2-46.3z"
		/>
		<linearGradient
			id="ch-topdown50000i"
			x1={1032.3}
			x2={885.6}
			y1={1821.7946}
			y2={1821.7946}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000i)"
			fillOpacity={0.7}
			d="M1032.3 712.6v-28s-24.1-74.4-74.4-74.4c-50.4 0-68.3 67.9-72.3 76.6.6 6.4 0 25.9 0 25.9s8.4 24.9 16.2 44.2c20.7 1.9 97.1 1.9 111.1-1.1 7.8-18 19.4-43.2 19.4-43.2z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-topdown50000j"
			cx={985.6903}
			cy={2113.6824}
			r={409.9328}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-topdown50000j)"
			d="M797.1 601.5c8 36.1 30.9 92.3 68 107.9 14 1.2 37.2 1.6 61.9 1.8l-43.6 2.5 20.5 45.3 1.2 46.6-182.4.9s5.9-94.8 14-142.4c9.5-.3 21.9.3 30.2-7.6 14.6-26.2 19.3-27.6 30.2-55zm256.8 101.4c6.8-6.7 47.1-14.3 71.2-101.4 15.2 31.1 19.5 42.5 27 55 20.4 9.6 30.2 8.6 30.2 8.6l15.1 139.2-187.3.9.6-45.1 20.5-49.7c3.8-.2 16.8-1.8 22.7-7.5z"
		/>
		<linearGradient
			id="ch-topdown50000k"
			x1={1228.1001}
			x2={1228.1001}
			y1={1743.7}
			y2={1931.3999}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000k)"
			d="M1183.3 665.2c16.8.1 53.5-6.1 55-90.6 13.6 8.4 17.3 20.5 17.3 20.5l17.3 157.5-80.9 9.7c-.1 0-7.6-89.3-8.7-97.1z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-topdown50000l"
			x1={689.8}
			x2={689.8}
			y1={1712.3999}
			y2={1937.2448}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000l)"
			d="M682.8 572.4c-.6 15.1-.3 92.9 51.8 93.9-6.4 51.7-10.8 127.3-10.8 127.3l-78.8-2.2L665.5 594s17.9-36.7 17.3-21.6z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-topdown50000m"
			cx={961.15}
			cy={1988.7155}
			r={620.0385}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-topdown50000m)"
			d="M819.8 514.1c28.6-24.6 90.1-48.5 140.2-48.5 50.2 0 139.2 44.2 139.2 44.2s11 61.6 25.9 96c-7.1 21.7-43.5 107.3-96.5 107.9l4.9-30.2s-25.7-74.4-73.4-74.4-70.1 60.7-74.4 78.8c-2.7 11.7-1.9 20.8-1.1 25.9h-1.1s-66.6-10-86.3-116.5c14-39.2 17.5-53.4 22.6-83.2z"
		/>
		<path
			fill="#f2f2f2"
			d="M919 1224.3c24 1.3 62.3-1 74.8.3 2.2-21.3 14.9-446.7 15.8-465.4 8.6-21.6 30.6-47.3 13.6-46.6-17 .7-144.9-11.8-138 5.9 6.9 17.7 22 36 19.9 50.4.9 34.4 13.9 455.4 13.9 455.4z"
		/>
		<linearGradient
			id="ch-topdown50000n"
			x1={885.5883}
			x2={1032.3}
			y1={735.6836}
			y2={735.6836}
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1119} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={0.8412} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-topdown50000n)"
			d="m1010.7 759 21.6-45.3s-146.2-2-146.7-1.1c-.5 1 15.1 44.2 15.1 44.2l110 2.2z"
			opacity={0.2}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1049.5 16.8c16.5 9.8 45.3 140.1 61.5 214.7 18-.2 51.5 10.1 59.3 72.3 5.8 45.7-21.8 72.6-25.9 75.5 5.3 20 18.3 58.3 18.3 58.3s69.4 37.6 74.4 129.5c10.6 9.1 18.3 28 18.3 28s19.4 140.5 19.4 240.6-12.9 557.8-12.9 661.3c-11.7 130.1-27.6 322.2-50.7 366.8-4.5-.4-17.3-12.9-17.3-12.9v-21.6c4.7-173.7 9.7-347.4 9.7-347.4s7.8-9 11.5-12.2c2.4-40.3-1.3-171.3-3-236.4-5-5.5-12.1-12.5-15.4-16.6-.4-14.2-.6-22.9-.6-22.9s1.2-199-48.5-212.5c-7.1 41.5-47.1 408.9-106.8 549.1-4 9.5-42.3 36.9-80.6 37-38.5 0-77.1-27.3-81.2-36.9-59.7-140.2-99.8-507.7-106.8-549.1-42.9-1.9-46.9 183.4-49.1 233.9-8.6 6.1-12.3 10.5-17.8 17.4-1.7 65.5-4.1 194.4-1.1 236.3 7 8.2 11.9 13.1 11.9 13.1s5 173.7 9.7 347.4v21.5s-12.7 12.6-17.3 13.1c-23.1-44.6-39.1-236.7-50.7-366.8 0-103.5-12.9-561.2-12.9-661.3s19.4-240.6 19.4-240.6 7.8-19 18.3-28c5.1-91.8 74.4-129.5 74.4-129.5s13.1-38.3 18.3-58.3c-4.1-2.9-31.7-29.8-25.9-75.5 7.9-62.1 41.3-72.5 59.3-72.3 16.2-74.5 46.2-203 61.5-214.7 7.2-5.6 165.9-8.3 179.3-.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m892 13.6 11.9 56.1 112.4-.1 11.9-56.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M793.9 454.8c27.4-62.6 46.3-371.6 70.1-428.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1126.1 454.8c-27.4-62.6-46.3-371.6-70.1-428.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m704.3 1466.7-47.5 2.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m708.6 1226.1-56.1 5.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M736.7 661.9c-16.8 65.9-31.3 488.7-31.3 569.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1214.6 1466.7 47.5 2.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1212.4 1226.1 53.9 5.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1181.1 661.9c16.8 65.9 31.3 488.7 31.3 569.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M770.1 982.3c17.4-4.3 29.1-7.7 29.1-59.3S765.4 777.6 768 653.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1118.6 250.9c10.6 4.2 40.3 36.8 20.5 106.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M805.7 243.4c-12.5 6.3-54.7 41.3-27 121.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1149.8 982.3c-17.3-4.3-29-7.7-29-59.3s33.8-145.4 31.2-269.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M810 230.4c-3.3 20.1-26.3 120.9-35.6 155.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1111 228.3c2.8 15 28.4 140.3 35.6 160.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M756.1 438.6c14.7 1.3 71.4 30.6 63.6 78.8-9.6 59.7-46.2 143.8-66.9 146.7s-73.2 12.6-70.1-102.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1163.7 438.6c-14.7 1.3-71.5 30.6-63.8 78.6 9.6 59.5 46.2 143.6 67 146.4 20.8 2.9 73.4 12.6 70.2-102.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1098.1 507.7c-31.9-17.5-156.7-86.9-278.3 7.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M797.1 595.1c1.2 27.2 32.6 117.6 87.4 117.6-.2-12.6 0-24.8 0-24.8s23.6-77.7 73.4-77.7c49.7 0 70.2 63.5 74.4 74.4-.6 5.3 0 27 0 27s61.6-2.8 91.7-106.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m865.1 709.4 24.1 833.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M786.3 1074h89.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M821.9 1306h58.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1131.5 1074H1042" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1095.9 1306h-58.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1050.6 709.6-23.8 833.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m882.3 712.6 19.4 46.4 111.1-1.1 19.4-45.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m905 760.1 14 463.9h78.8l14-466" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m933 761.2 14 32.4h24.8l14-33.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M885.6 712.6h147.8" />
	</svg>
);

export default Component;

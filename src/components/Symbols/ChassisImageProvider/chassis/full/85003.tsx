import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="m361 1042 24 6 279 167 23 27 124 42-8-49-60-99 320 93s120.3 115 223 115c85.9 0-3.9-164.5-12-187 19.7-20.3 111-117 111-117l158 117 37-72s2.6-29.9 3.6-42c.3-3.1-3.6-8-3.6-8l10-130 3-19 12-38-13-241-20-10-18 1-72 193-121 24-23-18-110 35-2 24-105 32s-77.9-118-182-118c-68.8 0-118 63.9-118 108-17.4-12.5-36.9-33.9-64-13s-5.2 58.3 7 78c-37.5-2-139-10-139-10l-14-25-151-14 1 30-142 7-102-207-25 2 134 334 27 5 8-23z"
		/>
		<path fill="#e0e0e0" d="m912 1044-93-20s14.4-68.4 71-68c10 31.3 22 88 22 88z" />
		<linearGradient
			id="ch-full85003a"
			x1={481.4026}
			x2={613.6526}
			y1={548.4438}
			y2={319.3799}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.301} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85003a)"
			d="m361 1042 301 173s-4.8-30.1-104-166c-62.5-40.8-205.2-64.5-218-63 4.7 10.7 21 56 21 56z"
			opacity={0.8}
		/>
		<path fill="#bfbfbf" d="M677 1192 462 894l1 30s145.6 181.2 200 290c6.5 7.3 22 23 22 23l-8-45z" />
		<linearGradient
			id="ch-full85003b"
			x1={762.479}
			x2={726.521}
			y1={608}
			y2={697}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path fill="url(#ch-full85003b)" d="m802 1237 9 46-126-41-7-48 124 43z" />
		<linearGradient
			id="ch-full85003c"
			x1={735.3525}
			x2={1237.2306}
			y1={727.9286}
			y2={376.5095}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#2e3033" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full85003c)"
			d="M821 879c0-44.1 49.2-109 118-109 104.1 0 182 118 182 118s42.8 58.3 84 127.8c28 47.2 54.8 100.2 69 141.3 8.1 22.5 97.9 187 12 187-99.9.5-221.7-113.9-223-115-.8-35.7-126.8-165.6-168-203-.2-6.1.3-35.2-2-72-40.1-40-64.7-68.1-72-75.1z"
		/>
		<linearGradient
			id="ch-full85003d"
			x1={971.6427}
			x2={1284.7277}
			y1={686.7755}
			y2={112.7755}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.251} stopColor="#dae3f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85003d)"
			d="M821 879c0-44.1 49.2-109 118-109 104.1 0 182 118 182 118s42.8 58.3 84 127.8c28 47.2 54.8 100.2 69 141.3 8.1 22.5 97.9 187 12 187-99.9.5-221.7-113.9-223-115-.8-35.7-126.8-165.6-168-203-.2-6.1.3-35.2-2-72-40.1-40-64.7-68.1-72-75.1z"
		/>
		<linearGradient
			id="ch-full85003e"
			x1={1062.1105}
			x2={1322.0504}
			y1={693.7586}
			y2={572.5465}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.598} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.676} stopColor="#fff" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.798} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85003e)"
			d="M1159 1066c61.2 0 68.6-28.1 117 89 8.1 22.5 97.9 187 12 187-99.9.5-221.7-113.9-223.1-115 .1-122.6 67-160.2 94.1-161z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full85003f"
			x1={798.5804}
			x2={1182.5394}
			y1={576.3774}
			y2={230.6586}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.598} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.676} stopColor="#fff" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.751} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85003f)"
			d="M821 879c0-44.1 49.2-109 118-109 104.1 0 182 118 182 118s42.8 58.3 84 127.8c-65.6-8.5-163.5-19.5-142 213.3-.8-35.7-126.8-165.6-168-203-.2-6.1.3-35.2-2-72-40.1-40-64.7-68.1-72-75.1z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full85003g"
			x1={774.1963}
			x2={774.1963}
			y1={348.755}
			y2={276.491}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#494949" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full85003g)"
			d="M758 925c-1.2-19.1 2.5-46.7 46-54 .4-3.6-20.2-27.1-48-5s3.2 78.1 2 59z"
			opacity={0.502}
		/>
		<path fill="#ccc" d="m1596 854-114-62 73-194 38 9 12 247h-9z" />
		<path
			fill="#ccc"
			d="M1259.3 857.8c-17-14.8-27.3-23.8-27.3-23.8s-3.2 7.3-4 19c6.4 5.3 12.6 10.4 18.6 15.4 4.2-4.1 10.4-7.4 12.7-10.6zm70.1 89c61.4 67.5 57.6 96.2 57.6 96.2l155 115 11-44s-127-110.8-221.3-193c-.8 4.3-2.7 10-2.7 10l-37 27 35-6s2.3-7.7 2.4-5.2zm-9.4-36.1c-15.3-13.4-29.5-25.8-41.9-36.5-18.1-3-45.1 26.8-45.1 26.8s34.6-8.5 43.7-6.3c11 9.9 20.8 19.3 29.8 28.2 5-4.4 9.6-9.2 13.5-12.2z"
		/>
		<path fill="#bfbfbf" d="m676 1020 35 15-20 7-15-22zm-34-56 36 14-22 8-14-22z" />
		<path
			fill="#d6d6d6"
			d="m850 911 35-21s40.4 48.8 85 103c-11.7 62.2-2 106-2 106-20-21.8-54-55-54-55l-22-88-42-45z"
		/>
		<path
			fill="#bfbfbf"
			d="m1554 598 1.5-.5c5.3-1.8 22.5-7.5 22.5-7.5l32 9 11 248-12 44-16 13v-18l12-39-13-239-38-10z"
		/>
		<path fill="#666" d="m1588 898 9-9h7s-16 12.5-16 13v-4z" />
		<linearGradient
			id="ch-full85003h"
			x1={1253.5}
			x2={1253.5}
			y1={852}
			y2={736}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.303} stopColor="#fff" />
			<stop offset={1} stopColor="#616161" />
		</linearGradient>
		<path fill="url(#ch-full85003h)" d="m1276 1344 43 45-34 49s-70-81.3-97-116c35.3 15.7 88 22 88 22z" />
		<path fill="#4d4d4d" d="m1284 1438-27-36 64-13-34 52-3-3z" />
		<path fill="#bfbfbf" d="m361 1043-42-111-89-182-26 6 89 219 38 94 23-5 7-21z" />
		<linearGradient
			id="ch-full85003i"
			x1={748.7906}
			x2={893.8537}
			y1={387.9622}
			y2={272.6573}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85003i)"
			d="m892 955-92-93s-32.3-11-44 5-12.8 38.8 1 64 61 90 61 90 9.4-39.9 34-50 40-16 40-16z"
			opacity={0.302}
		/>
		<path
			d="m639 947 131 8 48 68 95 20 97 104 33 43-319-92s-36.7-56.3-36-56 28-6 28-6l-8-19-44-17-8-13 30-5-13-21-34-14zm623 177 114-114s-9.3-15.5-19.5-29c-12.2-16.2-25.5-31-25.5-31l-41 12-17-12 32-29-34-30-40 11-17-12 30-24-9-2-104 34s67.4 103.6 79 125 52 101 52 101z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-full85003j"
			x1={1362.1189}
			x2={1173.85}
			y1={531.1595}
			y2={329.2655}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.251} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85003j)"
			d="m1275 1158 111-118s8.6-27.5-71-107c-28 13-117 70-117 70s21.8 37.2 41 76c19.3 38.7 36 79 36 79z"
		/>
		<linearGradient
			id="ch-full85003k"
			x1={854.4018}
			x2={914.0447}
			y1={588.1691}
			y2={380.1691}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.002632098} stopColor="#000" />
			<stop offset={0.1975} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85003k)"
			d="m744 1137 318 92s10.1-26.3-150-186c-35.6-7.1-93-21-93-21l-144-1 69 116z"
			opacity={0.5}
		/>
		<path fill="#e6e6e6" d="M819 1023c6.7-53.5 60.7-66.8 74-67 .1 2.1 21 88 21 88s-57.7-13.2-95-21z" />
		<path
			fill="#4d4d4d"
			d="M1244 1185.3c.4-.3 1.2-.6 1-1-5.6-10.8-60.1-134.8-129-217.3-28.4 9.3-42.5 20.2-60 47 5.2 42.7 14.5 83.3 34 105s99.2 78.3 110 85c9.6.1 21 .8 21 .8l.4-.4c5.5-7.9 14.9-14.6 22.6-19.1zM1182 977c-11.3-7.3-21.4-16.6-49-14 62 80.6 105.9 165.5 131 220 10.9-1.3 17.1 2.2 25 8-13.3-32-62.2-149.1-107-214z"
		/>
		<path
			d="M879 789c13.6-5 63.9-9.8 210 192 10.2-6.5 29-14 29-14l12 13 15-4-9-13h22s-106.5-162-188-191c-39.8-5.2-68.7 3.2-91 17z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-full85003l"
			cx={1838.4991}
			cy={1366.4097}
			r={229.5637}
			fx={1836.8268}
			fy={1366.4229}
			gradientTransform="matrix(0.8656 -0.502 1.522 2.4745 -2615.8003 -1283.226)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.344} stopColor="#4d4d4d" />
			<stop offset={0.5263} stopColor="gray" />
			<stop offset={0.628} stopColor="#e6e6e6" />
			<stop offset={0.7351} stopColor="gray" />
			<stop offset={0.9452} stopColor="#4d4d4d" />
		</radialGradient>
		<path
			fill="url(#ch-full85003l)"
			d="M1244 1186.3c.4-.3 1.2-.6 1-1-5.6-10.8-60.1-134.8-129-217.3-28.4 9.3-41.5 22.2-59 49 5.2 42.7 19.5 86.3 39 108s96.2 70.3 107 77c9.6.1 20-.3 20-.3l.4-.4c5.5-7.8 12.9-10.5 20.6-15z"
		/>
		<radialGradient
			id="ch-full85003m"
			cx={1135.1666}
			cy={1086.1499}
			r={325.8229}
			gradientTransform="matrix(0.5204 0.8539 -0.2366 0.1442 801.3821 -39.7889)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.344} stopColor="#4d4d4d" />
			<stop offset={0.8336} stopColor="gray" />
			<stop offset={0.8938} stopColor="#e6e6e6" />
			<stop offset={0.9656} stopColor="gray" />
			<stop offset={1} stopColor="#4d4d4d" />
		</radialGradient>
		<path
			fill="url(#ch-full85003m)"
			d="M1135.7 962c32-1.7 42.7 9.5 47.7 17s96.6 176.9 103.3 211.5c-14.7-7.5-15.7-8.8-23.3-6.5-11.4-23-127.7-222-127.7-222z"
			opacity={0.5}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1182 977c-11.3-7.3-21.4-16.6-49-14 62 80.6 105.9 165.5 131 220 10.9-1.3 17.1 2.2 25 8-13.3-32-62.2-149.1-107-214z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M464.3 896.9C482.4 922 677 1193 677 1193s111.7 37.2 124.8 41.6c.6.3 1.1 0 .6-.7C790.1 1213.2 610 908 610 908s-128-11.3-145.1-12.8c-.8-.1-.8 1.1-.6 1.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M696 1215c.1 1.3 2 18 2 18l92 30-2-18s-92.1-31.3-92-30z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m631 944 43 19 14 20s-.2.9-.7.7C681.8 981.3 644 964 644 964"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m685 983-28 3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m664 999 43 19 14 20s-.2.9-.7.7c-5.5-2.4-43.3-19.7-43.3-19.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m718 1038-28 3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1230.8 831.4c-2.4.8-2.1.3-.9 1.5C1249.1 849.5 1552 1114 1552 1114s17.9-37.6 33.4-70.3c.8-.9.9-.4-.4-1.6-17.2-16.5-243-244.1-243-244.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1257 857-43 33 14 12 42-32 1 2v19l-43 12" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1319 912-47 38 14 12 42-32 1 2v19l-43 12" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M461 921c25.7 30.5 204.4 272.2 202 293" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1480 791 122 65s0 1.7-1 2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1225 852s7.9 6.3 20.1 16.6M1385 1039c2.9-21.8-22.4-56.4-53.7-89.8m-25.8-26.2c-9.8-9.5-19.7-18.7-29-27-4.9-4.4-6.6-6-6.6-6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1282 1344 40 45-36 51s-74.7-90.9-104-127" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1254 1401 65-14" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1121 888c98.9 141.3 103.8 162.1 157 280" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1062 1229c-4.3-40.4-105.3-142.7-151.5-187.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1552 1114-9 44" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m677 1193 9 50" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m361 1043-42-111-89-182-26 6 89 219 38 94 23-5 7-21z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m893 954-79-81s-69.7-6.1-53 65c5.2 8.9 57 85 57 85" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1248 1188c.4-.3 1.2-.6 1-1-5.6-10.8-64.2-138.3-133-221-28.4 9.3-42.5 21.2-60 48 5.2 42.7 13.5 83.3 33 105s100.2 79.3 111 86c9.2.6 23 0 23 0l.4-.4c4.5-7.9 14.8-15 24.6-16.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M819 1023c6.7-53.5 60.7-66.8 74-67 .1 2.1 21 88 21 88s-57.7-13.2-95-21z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M879 787c48.2-13.4 170.9 136.9 205 190m43 2 14-2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1160 967c-34.1-53.1-149-200.1-204-196" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1296 1343c-74.7 0-46.8-31.1-88.1-53.9-81.6-45.1-172.4-150.6-242.9-230.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1224 1300c0-10.8 3-50.4 48.4-59.9 24-5 34.3-.5 38.6 4.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1593 886 12-39-13-239-38-10s.6-1.2 1.5-1.5c5.3-1.8 22.5-6.5 22.5-6.5l32 9 11 248-12 44s-10.9 8.8-18.6 15"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1358 814 124-23 72-193m38 11 18-8m-264 198-118 33-1 22-106 34s-77.9-118-182-118c-68.8 0-118 63.9-118 108-17.4-12.5-36.9-33.9-64-13s-5.2 58.3 7 78c-37.5-2-139-10-139-10l-14-25-151-14 1 30-142 7-102-207-25 2 139 343 22-4 8-23 303 173 23 27 124 42-8-49-59-100 319 94s123.1 115.5 223 115c85.9 0-3.9-164.5-12-187 19.7-20.3 111-117 111-117l158 117 37-75 6-38s-2.7-4.4-6-9c.8-37.3 16-181 16-181"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m850 911 35-21s40.4 48.8 85 103c-11.7 62.2-2 106-2 106-20-21.8-54-55-54-55l-22-88-42-45z"
		/>
	</svg>
);

export default Component;

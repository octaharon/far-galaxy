import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M212.4 1145.7s-2.5-54.5-3.6-73.5c-1.1-18.5 11.8-50.4 54-73.7 1.7-.9 1.6-3.3-.2-4.1-41.6-17.8-144.3-84.5-172.7-129.6-4.6-7.3-15.5-7.1-22.9-33.8-2.3-63.8 42.7-98.2 80.8-126.6 5.5-3.5 65.5-44.9 71.1-48.2 91.6-47.9 137.3-55.3 170-66.3-2.5-11.2 8.1-40.4 30.1-60.3 25-22.5 44-39.3 74.7-59.1 8.4-9.7 130.6-47.9 251.9-60.3 55.7-3.6 90.4-7.5 107.3-6 47 40.7 79.6 117 79.6 121.8 2.5 7.4 3.6 15.7 3.6 15.7s6.7 11 9.6 15.7c8.4-.3 20.6-1.5 35 0 14.8-10.3 53.7-6.9 90.4-1.2 35.7 5.5 81.2 19.3 126.6 35 14.3 5.8 28.7 12.4 38.6 19.3 3.6 2.5 7.4 6.4 10 10 7.9 1.4 28.4 5.5 72 16.5 78.5 12.9 133.3 46.5 148.3 59.1 7.8 6.6 17 21 12.1 30.1-4.5 8.4-19.1 17.7-31.3 26.5-10.4 17.8-148.9 89.3-151.9 97.6 373.7-28.5 413.5 84.6 415.9 97.6 1.4 9 1 30 1.2 53 7.2-1 58.6-11.5 74.7-9.6 13.8 3 38.8 31.4 3.6 115.7-35.2 84.3-174.1 82.6-245.9 68.7-71.8-13.9-110.6-12.9-153.1-10.8s-417.2 45.4-525.6 82c-78.6 35.7-33.5 51.4-147.1 79.6-144.6 36-398.6-49.5-408.7-96.4-105.8-33.7-98.1-84.4-98.1-84.4zm70.4-65.1 178.4-1.2-1.2-9.6-20.5-36.2s-92.2 14.6-156.7 47zm-24.2 35c19.7 13.3 102.3 40.4 170 41 17.8-19.2 32.5-35 32.5-35v-19.3h-15.7l-1.2 10.8c.1 0-141.5 1.7-185.6 2.5zm402.7 20.4-97.6-21.7s.2 18.8 0 24.1c-2.3 6-15.4 14.3-26.5 19.3 45.2 2.3 124.1-21.7 124.1-21.7zm-88-39.7 13.3-4.8 115.7 32.5s52.4-23.1 61.5-33.8c-3-6.8-23-26.8-50.6-33.8-28.9 5.4-149.5 32.5-149.5 32.5v4.8l9.6 2.6zm-8.5-31.4c10.3-1.8 107.3-20.5 107.3-20.5s-65.6-17.4-125.4-16.9c17 8.6 17 6.7 18.1 37.4zm906.6-120.5 35-10.8s-7-6.4-32.5-8.4c-1.3 7.5-2.5 19.2-2.5 19.2zm101.2 1.2-78.4 30.1 127.8 28.9 33.8-12.1c0 .1-14.1-27.1-83.2-46.9zm-196.5 6-43.4-31.3s-34.4-.8-95.2 20.5c28.2 15 61.5 27.7 61.5 27.7l59.1-3.6 4.8 6h15.7l-2.5-19.3z"
		/>
		<path
			fill="#d9d9d9"
			d="M931.3 524.9c-4.9-11.9-58.8-117.7-84.4-120.5-29.4.3-100.1 4.8-100.1 4.8s-17.8 13-28.9 39.8c-10 23.9-24.2 68.4-27.2 88.9-161.1-21-299.6 31.1-300.7 48.5.4-.1.9 2.1 1.4 2 16.8-4.6 86.4-23.4 160.1-29.7 74.6-3.6 129.7 15.1 165.1 8.4 16.9-.7 152-14.5 195.3-15.7 28.2-2.3 24.3-14.6 19.4-26.5z"
		/>
		<path
			fill="#ccc"
			d="M563.6 1090.2c0 3.7.1 6 .1 6l-6-2.4-13.3 6s132.3 28.7 132.6 31.3c-4.3 1.5-11 3.8-15.7 4.8-32.3-7.8-116.9-27.7-116.9-27.7l19.3 6v24.1s-16.6 21.7-39.8 21.7c-23.2 0-33.8 1.2-33.8 1.2s-6.5-.6-16.9-1.2c0-1.3 48.2-38.6 48.2-38.6l-26.5-4.8 6-8.4-20.5-4.8-18.5 18.1v-19.7c0-.6-8.1-.3-14.1.4-2.1.3-3.8 14.1-3.6 12.1.1-.7-126.4-.5-188.1 0-7.5-2.6-15.7-10.1-15.7-10.8 0-2 203.7-.7 203.7-1.2v-7.2c0-1.5 17.7-1.4 17.7-3 0-20.5-2-44-2-44s8.1 14.5 50.6 14.5 53-16 53-18.1c-.3 9.2-.2 24.8-.2 36.5 12.9-3.1 193-41.3 193-41.3l-20.5-8.4-67.5 12.1s-63.6-15.7-126.6-15.7c-45.9 0-57.4.3-61.5 2.4-6.5-8-30.1-33.8-30.1-33.8l-26.5 2.4 15.7 33.8s-53.7 10.1-95.2 22.9c-40.4 12.5-68.7 27.7-68.7 27.7l-42.2 1.2-1.2 8.4s-7.2-9.7-7.2-21.7c0-32.8 39.5-69.5 129-92.8 43.3-11.3 97.3-23 165.3-23 125.7 0 267.6 26.8 267.6 101.3 0 20.1-22.9 32.5-22.9 32.5s-24.8-27.9-50.6-32.5c26.3-5.5 43.4-9.6 43.4-9.6h-6l-39.8 8.4s-124.1 29.9-147.1 34.9z"
		/>
		<path
			fill="#d9d9d9"
			d="M979.5 559.8c7.4 9.2 19.7 29.4 143.5 61.5s128.1 4.2 127.8 3.6c2 9.7 1.2 24.1 1.2 24.1s-17.9 8.4-45.8 8.4c-13.1 0-64.3-9-114.5-26.5-56.9-19.8-113.1-48.7-123-56.7 5.5-8.4 8.2-8.5 10.8-14.4z"
		/>
		<path
			fill="#ccc"
			d="M1325.7 978.8c26-.3 54.1-.7 54.1-.7v-49.4l-54.2-42.2-22.9 1.2 74.7 54.2v10.8s-26.6-18.9-44.6-32.5c-39.1 1.3-74 12.5-95 20.8-13.9-7.3-51.1-30.8-59.6-37.9 45.6-47.7 183-46.9 249.8-46.9 79.5 0 137.8 13.1 179.3 27.9 64.4 23 88.3 53.6 88.3 72.1 0 18.8-28.9 34.1-39.8 37.4-4.5-18.6-68.3-43.9-82.1-49.7-33.1 13.2-79.4 31.6-79.4 31.6l-24.1-3.6-9.6 10.8 15.7 3.6-8.4 9.6 110.9 18.1-20.5 6-85.6-9.6-60.3-7.2 12.1-19.3-15.7-1.2-6 7.2h-43.1c-14.5-4.3-23.1-7.8-34-11.1zm146.9-20s9.8 5.6 15.7 8.4c27.6-10.9 92.5-35.9 116.9-45.8-10.4-5.7-16.1-6.9-20.5-8.4 3.3-2.1-78.3 20.1-75.9 19.3-7.4-5-32.6-7.2-37.4-7.2-.1 4.7 1.2 33.7 1.2 33.7z"
		/>
		<linearGradient
			id="ch-full88002a"
			x1={538.3347}
			x2={1116.0845}
			y1={795.0117}
			y2={1488.2035}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={0.469} stopColor="#d9d9d9" />
			<stop offset={0.551} stopColor="#e1e1e2" />
		</linearGradient>
		<path
			fill="url(#ch-full88002a)"
			d="M891.5 1174.6c-61.3 27.8-46.3 46.3-96.4 78.4-22.3-19.3-75.8-55.8-141.8-101.7 247.2-60.9 197.1-234.9-327.3-249.6-127.7-7.6-202.1-9.5-230-36.8-9.6-2.2-20-11.3-24.1-21.7-12.1-27.2-.1-83.8 47-113.3 13.7-10.6 59.4-49.3 82-60.3 41.1-25.8 31.1-26 189.3-80.8 54.1-16.9 113.2-27.3 168.8-28.9 27.1-.8 58 8.9 77.2 12.1 43.6-.3 82-4.6 100-6.4 37.1 25 77.9 34.2 82 37.7-83.7 53.7-79.3 124.3 50.6 251.9 50.4 35.3 118.8 75.2 176 108.5 54 31.4 97.7 57.5 123 49.4 55.5 40.6 99.7 75.2 138.6 97.6-67.3 7.7-364.9 51.2-414.9 63.9z"
			opacity={0.702}
		/>
		<path
			fill="#404040"
			d="m337 699.7-50.6-57.9-24.1 12.1 47 55.5 27.7-9.7zm-97.6-35-22.9 18.1 27.7 33.8 32.5-1.2-37.3-50.7zm75.9-30.1 49.4 56.7 24.1-12.1-44.6-51.8-28.9 7.2zm57.9-12.1c.3 1.1 39.8 44.6 39.8 44.6s19.4-13.7 20.5-19.3c-5.3-5.9-22.9-22.9-22.9-22.9s-8.2-1.7-16.9-2.4c-10-.8-20.7-.6-20.5 0zm751-9.6-69.9-50.6-19.3-1.3s63 47.4 63.9 47 25.3 4.9 25.3 4.9zm-106.1-50.7 50.6 35s-45.5-8.9-55.5-24.1c-9.9-15.2 4.9-10.9 4.9-10.9zm59.1 4.9c0 .5 80.8 55.5 80.8 55.5l22.9 2.4-74.7-50.6c-.1-.1-29-7.8-29-7.3zm59.7 16.2 62 41.7s14.4 3.8 15.7-1.2c-7-5-41-28.9-41-28.9l-36.7-11.6z"
		/>
		<linearGradient
			id="ch-full88002b"
			x1={1304.3594}
			x2={1485.2756}
			y1={700.6976}
			y2={959.0729}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.23} stopColor="#f2f2f2" />
			<stop offset={0.469} stopColor="#d9d9d9" stopOpacity={0.8} />
			<stop offset={0.844} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full88002b)"
			d="m1155.5 1015.5 119.3 100.1s154.1-19.3 190.5-15.7c47.7.4 220.1 67.4 282.1 35-36.5-35.1-135-115.7-135-115.7s-274.8 17.4-458.1-133.8c5.7 19.3 65.3 134.1 1.2 130.1z"
		/>
		<linearGradient
			id="ch-full88002c"
			x1={1130.9515}
			x2={1294.6633}
			y1={767.1044}
			y2={963.5965}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.09700001} stopColor="#e2e3e2" />
			<stop offset={0.498} stopColor="#f2f2f2" />
			<stop offset={0.8} stopColor="#dfdfde" />
		</linearGradient>
		<path
			fill="url(#ch-full88002c)"
			d="M1057.9 972.1s-63.5 85.4-54.2 182c61.3-10.2 308.7-48.4 394.2-53-8.5-11.3-26.6-31.1-53-51.8-44.8-35.1-102.4-63.2-163.9-91.6 5.6 18.5 14.6 56.7-22.9 56.7s-100.2-42.3-100.2-42.3z"
		/>
		<linearGradient
			id="ch-full88002d"
			x1={932.6143}
			x2={1159.6725}
			y1={1031.009}
			y2={1355.2814}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.402} stopColor="#e6e6e7" />
			<stop offset={0.55} stopColor="#d9d9d9" />
			<stop offset={0.847} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full88002d)"
			d="M1125.4 867.2c25.1-40.8 187.7-76.9 283.3-111.8 41-14.9 69.7-27.7 69.9-43.7-16-31.9-95.3-66.6-231.5-90.4 3.7 15.9 4.3 20.9 4.8 25.3-23.8 12-49.4 11.8-77.1 7.9-37.6-5.4-79.1-19.7-125.4-39.1-17.4-7.3-35.5-18.3-54.3-25.4-23.7-9-49.2-28.2-60.3-47-15.7 13.7-156.6 13.4-200.1 21.7 10.3 14.6 82 38.6 82 38.6S885 583 962.6 647.9s146 184.6 162.8 219.3z"
			opacity={0.702}
		/>
		<path
			d="M543.1 451.3c79.8-27.2 170.6-33.4 200.1-41-8.1 7-47.4 73.8-53 126.6-161.8-14.6-236.4 9.7-301.4 44.6-3.9-19.2 32.6-57.2 79.3-94 24.8-19.6 49.8-28.2 75-36.2z"
			className="factionColorSecondary"
		/>
		<path
			d="M67 819c15.1 11.7 36.6 33 213.4 45.8 30.9 2.2 264.2 15.2 327.9 28.9 34.2 7.4 101 20.4 155.4 48.7 21.2-35.8 78-68.1 114.6-81.2 4 2.8 28.4 20 45.8 32.5-62.5 17.8-116 78.5-113.2 81.1 22.5 21 37.3 47.4 37.3 80.5 0 40.8-35.5 70.9-72.3 90.4-55.1 29.2-121.6 40.6-143.5 47-17 5-79.6 15-141 21.7-70.2 7.7-139.4 12.4-143.5 24.1-15.9-6.8-33.4-19.6-35-24.1 5.1-8 36.4-21.8 94-25.3 56.7-7.1 160.6-25.9 184.4-26.5 23.8-.6 204.9-34.4 204.9-113.3 0-27.4-29.3-83.5-204.9-121.8-11.4-2.5-118.2-19.4-220.6-24.1-110.6-7.8-243.2-6-287.6-44.9C71.4 847.6 65.5 833.7 67 819zm1739.5 197.7c-.8-3.8-1.6-21.1-21.7-26.5-10.8.5-42.7 4.8-74.7 10.8-40 7.5-81.2 16.9-91.6 16.9-18.8 0-193.9 2.6-290.5-37.4 84.7 38.4 211.5 57.9 285.7 57.9 74.1 0 171.8-31.1 192.8-21.7zm-349.6-327.9c6 3.7 21.1 13.2 21.7 30.1-9.8 9.3-41.3 30.4-108.5 50.6s-199 56.1-232.7 84.4c-5.7-10.1-16.9-31.3-16.9-31.3s54.1-33.7 138.6-54.2c84.6-20.6 189.3-60.1 197.8-79.6z"
			className="factionColorPrimary"
		/>
		<path
			fill="#d9d9d9"
			d="M177.9 710.5c-.1 5.4 0 22.9 0 22.9s24.8 22.9 77.2 22.9 209.6-37.6 219.4-118.1c-4.8-7-8.4-9.6-8.4-9.6s-4 44.3-115.7 80.8c-111.9 36.4-164.4 12.7-172.5 1.1z"
		/>
		<linearGradient
			id="ch-full88002e"
			x1={1066.2411}
			x2={1243.8773}
			y1={887.8426}
			y2={1065.48}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#9a9a9a" />
		</linearGradient>
		<path
			fill="url(#ch-full88002e)"
			d="M1136.9 854c60.1-40.3 312.2-97.3 341.7-135-3.1 20.8-40 44.9-86.8 72.3-29.8 17.5-64.4 34.5-94 59.1-19.3 0-92.9 13.7-128.7 46.2-9.9-7-16.3-9.2-32.2-42.6z"
		/>
		<linearGradient
			id="ch-full88002f"
			x1={960.9678}
			x2={314.2349}
			y1={949.1104}
			y2={983.0035}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#3e3e3e" />
			<stop offset={1} stopColor="#989898" />
		</linearGradient>
		<path
			fill="url(#ch-full88002f)"
			d="M93.5 863.6c24.6 29.8 223.1 39.1 282.1 41 59 1.9 269.2 11.8 362.8 74.7-17.9-6.4-95.9-32.5-196.5-32.5s-186.3 11-277.3 49.4c-30.9-17.1-128-72.1-172.4-125.4-1.8-3.4 1.4-4.4 1.3-7.2z"
		/>
		<linearGradient
			id="ch-full88002g"
			x1={434.398}
			x2={413.9049}
			y1={903.8311}
			y2={903.1151}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={0.797} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full88002g)" d="m419 997.4 20.5 35-20.5 3.6v-38.6z" />
		<linearGradient
			id="ch-full88002h"
			x1={766.2186}
			x2={721.616}
			y1={855.3281}
			y2={853.7707}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#363636" />
		</linearGradient>
		<path fill="url(#ch-full88002h)" d="m714.3 1055.3 44.6 27.7v-36.2l-44.6 8.5z" />
		<linearGradient
			id="ch-full88002i"
			x1={560.1812}
			x2={538.4827}
			y1={788.1818}
			y2={787.4249}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full88002i)"
			d="M541.9 1110.7s0 24.3 1.2 43.4c8.3-.2 20.5-15.7 20.5-15.7v-25.3l-21.7-2.4z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full88002j"
			x1={532.59}
			x2={498.8368}
			y1={774.5517}
			y2={773.3729}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#363636" />
		</linearGradient>
		<path
			fill="url(#ch-full88002j)"
			d="M491.3 1161.4c8.3-.2 33.8-2.4 33.8-2.4v-27.7c0-.1-15.4 9.8-33.8 30.1z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full88002k"
			x1={1434.2075}
			x2={1427.0546}
			y1={919.4098}
			y2={919.16}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#363636" />
		</linearGradient>
		<path fill="url(#ch-full88002k)" d="M1424.7 1005c4-4.9 7.1-10 7.1-10v11.3c-3.6-.2-5-.5-7.1-1.3z" />
		<linearGradient
			id="ch-full88002l"
			x1={1464.3345}
			x2={1454.6907}
			y1={919.7128}
			y2={916.3086}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full88002l)" d="M1465.3 1009.5V995l-9.6-1.2v15.9c4.3.8 9.6-.2 9.6-.2z" />
		<linearGradient
			id="ch-full88002m"
			x1={1326.5157}
			x2={1292.7625}
			y1={1015.3732}
			y2={1003.4596}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#141414" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full88002m)" d="M1297.8 896.2v28.9s22-4.6 33.8-3.6c-6.7-7.3-33.8-25.3-33.8-25.3z" />
		<linearGradient
			id="ch-full88002n"
			x1={1619.0018}
			x2={1584.0431}
			y1={977.5983}
			y2={965.2603}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2164} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#3b3b3b" />
		</linearGradient>
		<path fill="url(#ch-full88002n)" d="m1610 931.1-35 13.3s23.2 14.6 35 15.7c-.4-10.8 0-29 0-29z" />
		<path
			fill="#bfbfbf"
			d="M312.9 1214.4c11 25.6 202.3 91.6 325.5 91.6 159.4 0 190.7-104.8 242.3-125.4 61.9-24.7 240-43.8 332.7-57.9 92.7-14 231.1-28.3 268.8-20.5 37.7 7.8 156.4 41 221.8 41s95.2-66.3 95.2-66.3-7.3 106.1-170 106.1c-109.2 0-95.4-19.3-203.7-19.3s-446.6 49.7-531.6 74.7c-109.5 32.2-60.6 84.9-255.6 96.4-82.9 5.1-356.2-56.1-325.4-120.4z"
		/>
		<linearGradient
			id="ch-full88002o"
			x1={623.0477}
			x2={314.2458}
			y1={768.8819}
			y2={768.8819}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-full88002o)"
			d="M208 1072.2c3.9 49.6 97.2 110.7 308.6 97.6-60.8 25.3-204.3 12.6-206 52.6-.1 1.6.4 7.1-1.3 7.7-53.6-12.4-93.4-56-94.8-75.9-1.5-20.1-7.9-63.4-6.5-82z"
		/>
		<linearGradient
			id="ch-full88002p"
			x1={1681.4635}
			x2={1594.6693}
			y1={926.7989}
			y2={926.7989}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bebebe" />
			<stop offset={1} stopColor="#4f4f4f" />
		</linearGradient>
		<path
			fill="url(#ch-full88002p)"
			d="M1625.7 1020.3c19.3-5.1 65.9-15.6 84.4-54.2 1.7 5.5 2.4 33.8 2.4 33.8s-53.9 16.3-86.8 20.4z"
		/>
		<linearGradient
			id="ch-full88002q"
			x1={691.9685}
			x2={1115.2596}
			y1={902.5898}
			y2={1198.9821}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#0e2533" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full88002q)"
			d="M762.9 697.4c-2-16.6-.2-41.5 12.9-61.6 13.7-21 39.3-37 69.1-41.8 74.1-17.4 145 50.2 153.7 59 59.1 59.9 135.6 190 144.8 212.3 11.8 32.9 43.9 90.6 42 123.8-.9 15.2-10 25.3-29.9 25.3-51.6-5.1-126.1-60.1-188.3-94.8-63.7-39.9-117.7-76.1-133.5-98.6-39.9-45-66-84-70.8-123.6z"
		/>
		<linearGradient
			id="ch-full88002r"
			x1={870.4379}
			x2={1045.0713}
			y1={840.0627}
			y2={1264.1748}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#092130" />
			<stop offset={0.699} stopColor="#dae3f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full88002r)"
			d="M762.5 702.1c-1.9-16.3-2.9-42.7 10.8-63.9 15-23.1 46-40.6 76.3-45.4 74.1-17.4 145 58.6 153.7 67.4 59.1 59.9 133.2 190 142.4 212.3 17.2 48 73.8 141.8 10.6 141.8-51.6-5.1-121-56.5-183.2-91.2-63.7-39.9-126.3-81.7-142-104.1-44.7-60.7-63.8-77.3-68.6-116.9z"
		/>
		<path
			fill="#d9d9d9"
			d="M769.8 731s6.1-19.5 12.1-20.5c10.2 9.5 60 52.7 66.3 57.9 1.6 18.2 5.1 63.1 9.6 77.2-6.9-6.4-31-28.8-51.8-56.7-19.6-26.1-36.2-57.9-36.2-57.9z"
		/>
		<radialGradient
			id="ch-full88002s"
			cx={2434.2483}
			cy={2588.7561}
			r={210.5141}
			gradientTransform="matrix(0.8025 -0.5967 -1.1764 -1.5821 1989.1213 6407.1978)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#0e2533" stopOpacity={0} />
			<stop offset={0.4949} stopColor="#dae3f2" stopOpacity={0} />
			<stop offset={0.8645} stopColor="#e6efff" />
			<stop offset={1} stopColor="#dae3f2" />
		</radialGradient>
		<path
			fill="url(#ch-full88002s)"
			d="M762.5 702.1c-1.9-16-1.6-43.2 10.8-63.9 14.2-23.7 42.7-40.1 73.9-45.1 74.6-17.5 147.1 54.8 155.7 63.7 59.5 60.3 134 191.4 143.4 213.9 17.3 48.4 74.4 142.9 10.7 142.9-32.9-3.3-69-26.2-110.2-48.6-23.9-13-46.9-27.1-69.9-39.9-15.6-9.8-35.3-21.7-49.7-31.3-45.1-30-82.9-57.6-95-74.7-34.5-39.4-64.8-77-69.7-117z"
		/>
		<path
			fill="#999"
			d="M1013.3 749.1c11.8 11.8 64.6 80.5 92.8 137.4-24.7 20.8-137.1-45.6-141-69.9-8.4-25.3 8.2-54.8 25.3-66.3 8.5-5.7 18.4-7.9 22.9-1.2zm18.1-4.8c13.4.3 42.5 7.9 51.8 18.1s51.5 86.4 55.5 98.8-20.5 22.9-20.5 22.9-42.9-81.3-86.8-139.8z"
		/>
		<radialGradient
			id="ch-full88002t"
			cx={894.548}
			cy={7627.9468}
			r={495.5387}
			gradientTransform="matrix(0.4776 0.7483 -0.1917 0.1133 2040.4396 -692.5825)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1192} stopColor="#535353" />
			<stop offset={0.2091} stopColor="#757575" />
			<stop offset={0.3133} stopColor="#999" stopOpacity={0.5} />
			<stop offset={0.4001} stopColor="#fff" stopOpacity={0.4} />
			<stop offset={0.4924} stopColor="#a6a6a6" />
			<stop offset={0.8179} stopColor="#666" />
			<stop offset={0.9703} stopColor="#333" />
		</radialGradient>
		<path
			fill="url(#ch-full88002t)"
			d="M1009.7 743.1c11 10.3 60.8 71.4 96.4 143.5-10.3 7.1-42.8-.2-76.5-17.9-26.1-13.6-65.2-40.8-66.3-57.3-4.6-53.2 27.6-61.5 46.4-68.3zm77.7 23.6c9.3 17.3 19.3 30.1 51.4 91 .3 2.3 2.3 20.2-21.1 27.6-26.8-46.7-38.8-68.8-52.7-88.5-17.1-24.2-23.2-36.3-38.2-54.8 32.6 3 50.2 14.9 60.6 24.7z"
		/>
		<path
			fill="#f2f2f2"
			d="M1426.3 912.2c14.3 1.1 44.3 4.6 44.7 18.4.4 13.8-36.1 11.7-47.1 11.9-11 .2-44.2-5.8-45.4-16.7s34.4-14.6 47.8-13.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M740.8 981.7c17-37.9 53.8-84.8 114.8-111.9 12.7-5.8 22.6-8.7 22.6-8.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M786.6 1014.3c17-37.9 53.8-84.8 114.8-111.9 12.7-5.8 22.6-8.7 22.6-8.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M68.2 819c22.9 24.9 86.4 34 158 41.5 82.3 8.6 187.3 13.3 294.1 22.4 199.5 16.9 266.9 70.4 297.8 97.6 77.8 93.5-.4 158-113.8 193.7-138.5 43.6-339.9 47-355.2 60.7 0 0-2.6.8-3.6 1.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1713.7 999.8c-20.8 1.3-41.1 19.3-153.1 19.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M83.2 858.4c.9.7 1.8 1.4 2.7 2.2 20 14.8 60.1 25.7 102.1 30.7 163.6 19.8 474.6-1.2 596.2 123"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M314.9 1235.3c-20.9-51.1 51.9-34.6 300.6-77.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M312.9 1214.4c20.6 33.3 270.4 110.4 376.1 88 135.4-28.7 137-108.3 204.9-127.8s497.8-80.7 570.2-74.7c72.4 5.9 187.6 56 264 41 52.9-10.4 76.2-71.9 77.2-74.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1137.1 853.9c43.5-30.8 161.8-59 239.2-86.4 35.8-12.7 100.5-36.1 104.3-50.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1458.1 688.8c-3.8 14.6-66.5 37.1-102.3 49.7-75.5 26.7-189.8 54.2-235.8 84.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1807.7 1016.7c-11.1-3.3-1.2-7-120.5 14.5s-296.9-12.2-413.5-73.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1178 902.8c46.8-48.1 175.3-50 269.3-46.4 124.9 4.3 247.1 45.2 247.1 100.1 0 30.5-67.1 50.3-121.1 60.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1235.7 939.1c23.6-8.5 58.4-16.5 97-18.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1468.9 923.9c17.3 1.7 37.5 6 44 8.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1571.9 945.7c25.8 7.4 72.8 26.6 83.9 44.5 0 0 1.5 1.5 1.5 3.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1377.5 942.2s-66.7-47.9-76.2-54.8c-.7-.5.1-.8.1-.8h22.9l53.2 38.8v16.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1300.2 886.5v9.6s69.8 52.4 76.6 57.5c.3.3.5 14.9.5 14.9v-28.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1378.5 925.1c0-17 92.8-19 92.8 4.8.1 19.4-92.8 15.8-92.8-4.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1425.1 921.5c14.3.4 25.8 3.1 25.8 6.1-.1 3-11.7 5.1-26 4.8-14.3-.4-25.8-3.1-25.8-6.1 0-3 11.7-5.2 26-4.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1297.3 967.8c28-1.4 60.8-2.9 60.8-2.9l4.8 6h15.7l8.4 7.2s-31.8.4-59.9.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1375.4 993.9c1.7-3.3 3.1-6.2 3.1-6.2l24.1 2.4 4.8-7.2h19.3s-7.8 11-13.8 19.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1471.4 944.4 113.3-31.3 21.7 8.4s-.1 1.3-.4 1.4c-7.7 3-118.9 45.6-118.9 45.6l-15.7-9.6v-14.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1470.1 972.1 154.3 32.5s-23.7 7.8-41 10.8c-23-4.2-115.7-18.1-115.7-18.1l9.6-10.8-16.9-3.6 9.7-10.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1606.6 920.7v11.4l-113.3 44.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1458.1 984v8.6s8.1 1.5 12.1 2.4c.5.3-4.8 2.4-4.8 2.4v12.1l75.9 8.6 24.3 1.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1429.2 980.7V995s-3.1 4.9-6.1 9.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1471.4 928.7v42.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1359.7 989h30.9v-8.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1299 850.4c-45.4 4-100.6 16.5-130.7 45.5m-17.8-14.8c105.1 83.4 242.7 134.4 396.8 134.4 48.7 0 83.9 3.6 108.9-5.5 39.9-14.5 55.9-37 57.8-55.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M263.5 997.4c60.9-29.1 160.4-51.8 254.4-51.8s278.5 18.8 278.5 103.7c0 76.7-173.6 120.5-314.6 120.5-292.9 4.1-321-117.1-218.3-172.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M223.7 1069.7c0-70.4 162.6-114.5 305-114.5 150 0 258 40.6 258 98.8 0 56.7-174.3 106.1-277.3 106.1s-285.7-19.6-285.7-90.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M440.7 1032.6c-71.7 11-131.6 31.8-161.5 46.8-3.1 1.4-8 4.6-8 4.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M539.5 1027.6c44.3.7 104.3 7.3 131 14.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M715.3 1055.6c15.6 7.2 46.6 24.2 47.3 34.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M462 1078s-2.1-4-2.7-5.2c-5.6-11.3-35.5-73-35.5-73l27.7-2.4 30.1 31.5s-19.7 3.5-19.7 21.5c0 13-.4 19.8 0 22.6.2 1.1.1 5 .1 5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M462 1102.3v19.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m462 1079.4-228.6 6-2.4 3.6s-.7 5.6 8.4 12.1c11.7.7 204.9 1.2 204.9 1.2l1.2-9.6 16.5-1.2v-12.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m562.4 1063.7 173.6-32.5 19.3 8.4s.4.2-1.2.5c-22.1 4.9-191.7 40.5-191.7 40.5v-16.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m557.6 1095.1 13.3 2.4 15.7-6 115.7 31.3s-14.9 7.1-20.5 8.4c-13.4-3.7-122.5-28-136-31-.9-.2-.2-.3-.2-.3l12-4.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m426.2 1156.5 54.2-53 22.9 4.8-9.6 8.4 25.3 4.8-45.8 38.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M522.6 1119.2v13.3l-32.5 26.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M462.2 1092.7v8.4h-16.7v12.1H253.8s-12.4-8.5-14.5-10.8h204.9l1.2-9.6h16.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M544.3 1099.9v9.6l118.1 26.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M562.4 1083v7.2l192.9-43.4v-7.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M510.6 1160.2c15.1 0 48-7.6 53-22.9v-22.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M562.4 1068.5v-27.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M513 1026.4c-22.2 0-50.6 3.1-50.6 20.5 0 12.4 28.3 14.5 51.8 14.5 23.5 0 48.2-6 48.2-20.5s-31.1-14.5-49.4-14.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M511.8 1037.5c14.3 0 25.9 3.1 25.9 7s-11.6 7-25.9 7-25.9-3.1-25.9-7 11.6-7 25.9-7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M285.5 625.4c26.2-7.5 60.5-13.5 75.9-16 37.8-6.1 76.8-6.2 99.8 11.8 24 31.8-83.2 78-109.7 88s-151 36.1-171.2 1.2c-3.8-6.6-2.9-13.7 1-20.7 5.3-8.6 12.3-16.7 12.3-16.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m981.9 557.4-12.1 15.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M935.9 539.3s1.9 12.1-27.5 12.1-211 18.1-211 18.1-61.8-6.5-80.8-7.2c-20.8-.8-71.9-16-231.5 28.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M743.2 411.6c-16.1.6-179.1 21.1-254.4 60.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M947.6 557.2c7.5 9.1 27 22.8 82.6 48.4 55.6 25.6 174.1 72.1 223 41-2.3-14.6-3.6-25.3-3.6-25.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M981.9 556.2c-2.9 3.9-2.1 9.7 25.3 25.3 13.9 7.9 83.6 33.9 144.7 45.8 49.9 9.7 94 9.8 96.5-.8.5-2.1.6-4.6-2.5-7.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1009.7 569.5c3.3 6.9 14.5 21.4 148.3 53 73.9 12.7 60.3-4.8 60.3-4.8s7.6-9.7-109.7-43.4c-86.2-23-102.2-11.8-98.9-4.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1071.5 566.1 86.4 56.5m-28.9-7.5-78.4-52.8m53.1 47s-72.3-48-72.3-48.2m-14.5 1.1 54.2 37.4m31.4-26.5 80.8 53m18 1-70.1-45.8m39 12.5 45.6 29.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m463.6 624.9 10.8 13.3s8.3 50.1-104.9 94-179.1 17.5-191.7 0c-.1-14.1 0-32.5 0-32.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M287.6 641.8c16.3-4.8 106.7-36.8 142.2-9.6 22.3 20-35.1 45.2-77.2 63.9-10.8 3.7-116.6 39.4-144.7 8.4-15.7-20.9 63.3-57.9 79.7-62.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m216.5 682.8 26.6 34.7m33.6-1.4-37.4-50.2m23-12 45.1 55.5m30.8-7.3-51.8-60.3m28.9-7.2 49.4 56.7m23.9-11.4L343 627.3m29.2-4.6 40.8 44.4m20.4-18.1-24.1-25.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M496.9 468.9c-20.7 8-46.4 31.2-69.5 51.2-40.4 36.6-36.7 62.7-37.4 68.7-45.5 11.9-80.3 27.1-126.6 45.8C223 650.9 187 680 159.8 697.3c-38.1 28.4-93.9 67.6-91.6 131.4 1.9 41.6 145.4 142.6 195.8 165.2 2.2 1-.7 4-2.8 5.2-41.5 23.2-53 54.8-51.9 73.1 1.1 19.1 3.6 73.5 3.6 73.5s-5.7 50.7 100.1 84.4c10 46.9 264.1 132.4 408.7 96.4 113.6-28.2 68.5-43.8 147.1-79.6 108.4-36.6 483.1-79.9 525.6-82s81.3-3.1 153.1 10.8 210.7 15.6 245.9-68.7 8.9-115.1-7.2-116.9c-16.2-1.9-65.1 8.6-72.3 9.6 0-23.2 1.4-41.6 0-50.6.7-11.8-42.2-127.3-415.9-98.8 3-8.4 131.7-78.5 147.1-95.2 7.9-5.4 34.1-25.6 35-30.1 3.5-19.5-11.4-31.1-22.9-37.4-36.6-29.5-149.5-55.9-205.2-67.3-20.2-14-26.5-17.6-51.5-30.3-24.2-8.9-69.2-22.3-120.5-32.5-76.3-15.2-96.4-1.2-96.4-1.2s-27.6.3-36.2 0c-4.2-5.7-10.8-14.5-10.8-14.5V537c0-3.9-7-33.8-35-73.5-8.9-12.6-17.8-27.9-28.2-40.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M689 536.9c-38.9-6.8-196.3-13.2-292.9 41-4.7 2.7-8.4 10.8-8.4 10.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M711.9 567.1c-29.8-1.2-20.5-37.4-20.5-37.4s7.4-27.6 22.9-68.7 31.3-50.6 31.3-50.6l95.2-7.2s18.4 2 31.3 20.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M991.6 647.8c94.9 86 194.1 287.8 194.1 337.5 0 49.7-70 21.9-110.9-4.8-22.9-15-177.9-95-226.6-144.7-48.7-49.6-86.8-107-86.8-139.8 0-15.7.2-44.6 16.9-66.3 18.3-23.8 55-39.8 98.8-39.8 39.5.1 84 30.3 114.5 57.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1008.5 1214.4c-10.6 1.2-24.1-2.7-24.1-60.3 0-108.7 64.9-166 73.5-182m123.9-14.7c66.8 30.9 191.9 98.5 216.1 146.2 14.7 72.2-25.7 61.8-36.2 65.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1009.7 743.1s23.1 26.5 48.6 63.9c15.9 23.4 35.2 53 47.9 79.6-39.9 14.8-113.2-39-132.6-56.7-28.2-25.6-.3-83.6 36.1-86.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M771 732.2s2.3-15.7 9.6-22.9c14.1 13.4 67.5 60.3 67.5 60.3s-.8 44.4 10.8 75.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1137.4 856.4c4 14.3-7.5 26.7-20.5 28.9-18.8-41.1-73.2-122.8-91.6-143.5 15.6 2 46.7 8.1 62.7 26.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M758.9 427.2c12.8.1 84.4-3.6 84.4-3.6s5.4-.8 14.5 13.3c9 14.1 51.7 73.7 51.8 85.6.1 11.9-14.1 13.4-22.9 13.3-8.8-.1-153.1 13.3-153.1 13.3s-24.2-.4-19.3-25.3c5-25 31.8-96.7 44.6-96.6z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m756.5 428.4 50.6 115.7m19.3-2.3 19.3-118.1" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M888 1080c6.9 7.8 28 35 48 35s118-15 118-15 248.1 120.4 430.4 208.9c1.4.7 6.3-8.6 7.6-7.9 28.6 13.9 53.5 25.1 77 38 2 1.1 2.9 12.4 4.9 13.4 59.9 29.1 100.1 48.6 100.1 48.6s12.1 9 4-45-36.2-264-76-316c.3-25.2-8.8-35.1-15-44-8.4-12.1-72.1-100-123-128-16.1-1.7-34.8-3.6-53.4-5.5C1360.2 846.8 1259 825 1259 825l19-35-6-17s13.4-13.4 35-56c-14.6-19.3-34-35-34-35s-3.1-11.1-5-23c-22.5-17.3-75.1-46.2-116-79 50.9-13.4 170-28 170-28l21-11 22-46-132-34-90 16-3-18-107-17s-19.9 3.4-24 2c-19.5-25.3-134.4-102-180-102-16.9 0-19 10-19 10l-155 7s-7.1-7-17-7c-26.9 0-125.1 169.4-155 223-13.1 6.5-27 10.6-27 34s37 89.1 54 110c-24 6-150.9 46.5-173 78-4.6 15.5-3 36-3 36l-7 15v59s117.6 106.5 125 113 17.4 10 33 10h39s-14 133-14 152 13 110 13 110l6-1 16 124 153 79 5-18 69 34 3 22s77.3 38.7 82 42 11.9-.6 15-16 13-72.8 13-120 1-338.5 3-358z"
		/>
		<path fill="#ccc" d="m402 782-67 20 4 76 80 19s-21-96.7-17-115z" />
		<linearGradient
			id="ch-full86001a"
			x1={774.5}
			x2={774.5}
			y1={1481}
			y2={1567}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full86001a)"
			d="m655 359 157-6s58.1 17.8 82 73c-35.6 2.8-196 13-196 13l-43-80z"
			opacity={0.2}
		/>
		<path fill="#b1b3b3" d="M977 452v-18l-231 18 30 15 201-15z" />
		<path
			fill="#b1b3b3"
			d="m1224 660-174-69 99-25-28-50-162-32 8 17-33-8-25-13-109-18 6 12-32-12s-10.8-1.4-19-3c-1.9-2.8-4.2-5.6-6-8-22.8-7.9-49-11-49-11l17 41 167 170 6-22 26 10 3 7 115 25 12-6 31 3s20.2 29.5 28 41c23.7 1.6 138 15 138 15l-24-43 5-21z"
		/>
		<path fill="#d7d9d9" d="m1047 590 176 71-4 20-162-45-10-46z" />
		<path fill="#979999" d="m912 481 18 9 25 51-10 5-33-65z" />
		<path fill="#b1b3b3" d="m912 482 16 6 88-3-3-13-101 10z" />
		<path fill="#b1b3b3" d="m1105 489 67 16 117 15 12-18-181-24-15 11z" />
		<path
			fill="#b1b3b3"
			d="M972 456s-16.6 5-2 9c13.2 3.6 21.7 13.5 45 17 16.6 2.5 49.3-1.2 58 1 9.8-.5 13.4 2.4 30 5 6.6-4.6 17-9 17-9l23 2-2-21-100 5-69-9z"
		/>
		<linearGradient
			id="ch-full86001b"
			x1={875}
			x2={703}
			y1={1436.5081}
			y2={1320.4919}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#979999" />
			<stop offset={0.99} stopColor="#656666" />
		</linearGradient>
		<path fill="url(#ch-full86001b)" d="m703 444 46 6 126 189-156-161-16-34z" />
		<path fill="#d7d9d9" d="m1004 606-11 12-81-18 13-5 79 11z" />
		<path
			fill="#b3b3b3"
			d="m1306 501 55-5-18 45-16 7 10-32-47 3 16-18zm-109 25-10-15-67 3 33 52 16-3-18-35 46-2zm30 138 18 31 21-34 7 21-27 44-28-44 9-18z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full86001c"
			x1={755.975}
			x2={437.249}
			y1={1080.2117}
			y2={1074.6477}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full86001c)"
			d="m613 936-150 2s-30.8-153-25-157 93-31 93-31 54.7 74.9 88 85 98 8 137 38c-40.9 17.7-143 63-143 63z"
		/>
		<linearGradient
			id="ch-full86001d"
			x1={1358.2012}
			x2={1068.2012}
			y1={1275.8729}
			y2={1436.6229}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#3f3f3f" />
		</linearGradient>
		<path
			fill="url(#ch-full86001d)"
			d="m1191 509 97 13 49-5-17 37-172 25 117 79-20 37-18-32-180-76 122-23-17-35 43-1-4-19z"
		/>
		<linearGradient
			id="ch-full86001e"
			x1={1041.8562}
			x2={977.8222}
			y1={806.2771}
			y2={1169.4331}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#454545" stopOpacity={0.8} />
			<stop offset={0.141} stopColor="#5a5a5a" stopOpacity={0.6} />
			<stop offset={0.602} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full86001e)"
			d="M706 813c86.2-33.1 359.6-86.9 549-47-11.3 25.8 2 67 2 67s42 89.8 42 149c-13.3 74.7-47.6 97.7-362 133-7.1.3-11.2-2.5-15-5 25.8-7.8-62.7-243.9-216-297z"
		/>
		<path
			fill="#d7d9d9"
			d="M641 352c8.2 0 47.4 55.3 113 211s74.6 202.3 71 203-107.3 25.7-275-91c-52.7-36.7-74.4-98.6-67-110s123.5-213 158-213z"
		/>
		<path
			fill="#e6e6e6"
			d="M582 509c5.3 5.1 57.3 55.5 94 61 10.3 22.1 25 51 25 51l1 50s-84.2 5.5-162-90c7.9-14.4 36.3-63.5 42-72z"
		/>
		<path
			fill="#d7d9d9"
			d="M881 362c34 16 83.1 39.6 127 82.9-9.8 3.8-30 5.1-30 5.1l1-17h-24s-.5-.5-1.3-1.4c-20.6-30.3-71.6-65.7-72.7-69.6z"
		/>
		<path fill="#ccc" d="M811 353c16.9 6 59.8 30.1 84 74 15.7 2.4 57 13 57 13s-99.2-130.4-141-87z" />
		<linearGradient
			id="ch-full86001f"
			x1={1137.2504}
			x2={1001.8674}
			y1={1260.9471}
			y2={1185.9031}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#4c4c4c" />
		</linearGradient>
		<path
			fill="url(#ch-full86001f)"
			d="m986 662 36 32s74.7 44.8 120 43c16.2-6.2 16.3-23.1 12-23s-51-6-51-6l-27-41-30-5-13 9-47-9z"
		/>
		<path fill="#b3b3b3" d="m529 1291 313 147s26.3 132.9 21 138-88-44-88-44l-3-23-68-32-8 18-151-79-16-125z" />
		<path
			fill="#979999"
			d="M886 1078c8.3 12.1 37.3 41.7 40 26s-2.4-82.9-117-226c-102.8-102.5-192.1-42.7-276-127 88.6 133.6 148.3 69.1 221 119 17.6-2.1 34.7-9.6 53 14s78.2 142.6 79 194zm370-319c5.4-10.8 25.4-28.6 45-33-5.7 16.2-21.5 38.6-28 48 12.7 10.8-8.2 47.3-14 52-3.6 2.9-6.9-37-8-38-3.2-2.7 7.3-26.8 5-29z"
		/>
		<path fill="#a6a6a6" d="M462 938h151s-64.8 24.6-65 49c-13.4 3.9-21.1 16.5-23 42-12.3.3-60-2-60-2l-3-89z" />
		<path
			fill="#bfbfbf"
			d="m466 1027-4-90-26-156-31 1s-3.7 29.8 13 117c-17.8-5.8-79-21-79-21l-4-34-7 14v50s128.7 121.6 138 119z"
		/>
		<path
			fill="#bbb"
			d="M844 1437c.9-33.8 21.2-275.9 39-345 7.5 31.9 2.3 322 3 359-2.1 26.1-9.2 113.8-19 119-1.6 2.7-23.9-99.2-23-133z"
		/>
		<linearGradient
			id="ch-full86001g"
			x1={933.9661}
			x2={933.9661}
			y1={1136}
			y2={1440.3181}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#cdcdcd" />
			<stop offset={0.276} stopColor="#7e7d7d" />
			<stop offset={1} stopColor="#262626" />
		</linearGradient>
		<path
			fill="url(#ch-full86001g)"
			d="M717 480c5 11.1 111 284 111 284l-12 7 9 13 66-15s171-27.1 260-18c-.5-2.9-3.2-9.1-10-15-28.2 2.2-68.2-14.3-79-20-4.7-2.4-64.2-35.6-69.7-52.5C962 658 924.5 651 919 650c-2.2-4.2-1.4-6.4-3-9-9.6-5.6-18.1-7.4-23-9-3.5 1.3-8.1 12.3-12 15-66-71.3-167.1-173.8-164-167z"
		/>
		<linearGradient
			id="ch-full86001h"
			x1={1685.1151}
			x2={1066.8711}
			y1={675.5043}
			y2={818.2373}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.2} />
			<stop offset={1} stopColor="#323232" />
		</linearGradient>
		<path
			fill="url(#ch-full86001h)"
			d="M1677 1401c3.6-6.4 4.6-58.6-11-115-26.7-27.9-219.4-217.9-370-290-24.7 40 15.4 62.1-235 103 67.5 35.7 268.4 136.5 422.5 211.9.9.4 7.6-8.4 8.5-7.9 29.1 14.3 53.3 22.9 77 36 2.2 1.2 2.7 14.7 4.9 15.8 61 29.2 102.1 48 103.1 46.2z"
		/>
		<path
			d="M530 1175v117l313 146 13-179 28-190s-23.9-109-66-166-47.6-34-83-19-147.6 63.9-172 81-13 21-13 21l148-54s26.7-14.6 23 5-35 249-35 249 5 13.9-33 8-123-19-123-19zm766-180c17.3 8.1 280.7 167.3 370 296-9.8-62.5-30-169.6-35-190s-29-78-29-78-34 7.5-57 26c-30.7-27.8-222-185-222-185l88-1-153-39s16.6 47.2 23 69 19.8 74.3 15 102zM402 782l34-1 95-32-12-16-117 49z"
			className="factionColorSecondary"
		/>
		<path
			d="m578 416 13 21-14 20 12 11s6-10.5 12-17c64.7 63.8 98.8 60.1 131 61 26.1 39 95 252 95 252s-8.7 5.2-16 8c-122.4-7.4-191.3-54.3-246-84s-82.6-93.9-85-110c3-8.9 98-162 98-162zm578 300c1.9 8-12 21-12 21l-12-23s15 1.2 24 2zM541 581c74.6 90.7 161 90 161 90l-2-52s-11.7-26.9-23-49c-40.9-4.2-89.8-55.5-95-63-4.5 8.3-31.8 56.3-41 74z"
			className="factionColorPrimary"
		/>
		<path
			d="M590 438c-3.8-6.1-6.3-11.5-10.5-19.8-5.1 6.9-9.2 12.6-11.5 15.8-9.9 13.8-84.6 138.8-87 145 3.4 16.2 22.8 61.3 58 89s83.9 64.2 172 89 106 12 106 12l10-5s-46.4-145.8-71-196c-7.7-15.7-16.5-35.8-25.6-56.5-55.2 12.3-118.5-46-129.4-60.5-12.5 21.8-24 7-24 7s8.6-12.1 13-20z"
			opacity={0.102}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M888 1080c6.9 7.8 28 35 48 35s118-15 118-15 247.4 120.1 429.6 208.6c2.4-1.6 5.7-4.7 7.4-6.6 26.1 12.7 54.6 26.1 77 37 2.4 3.4 4.3 10.2 6.2 13.5 59.8 29 99.8 48.5 99.8 48.5s12.1 9 4-45-33.4-190-47-258c-7.8-17.9-28.5-82.3-45-102-6.1-9-71.1-100-122-128-16.1-1.7-34.8-3.6-53.4-5.5C1360.2 846.8 1259 825 1259 825l22-39-9-13s13.4-13.4 35-56c2.4-4.7-16.2-18.3-34-35-2.8-8.3-4.2-15.9-7-24-61.3-39.6-120-80-120-80l174-26 21-10 23-46-131-34-91 13-1-15-108-17s-14.6 2-22 3c-19.5-25.3-136.4-104-182-104-16.9 0-19 10-19 10l-155 7s-7.1-7-17-7c-26.9 0-129.1 174.4-159 228-13.1 6.5-23 5.6-23 29s37 89.1 54 110c-24 6-150.9 46.5-173 78-4.6 15.5-3 36-3 36l-7 15v59s117.6 106.5 125 113 17.4 10 33 10h39s-14 133-14 152 13 110 13 110l6-1 16 124 153 79 5-18 69 34 3 22s77.3 38.7 82 42 11.9-.6 15-16 13-72.8 13-120 1-338.5 3-358z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m873 1559-4-78 14-219" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m871 1445-10-204" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M875 1253v30" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M875 1213v30" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M881 1191v30" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M881 1150v27" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M885 1072c-.6 1.8-33.8 177.7-42 367 10.9 70.9 21 134 21 134"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1675 1401-38-61-519-252" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1426 1240 1-163" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1362.1 1164.4c4.6-.4 8.4 3.7 8.4 9.2 0 5.4-3.8 10.1-8.4 10.5-4.6.4-8.4-3.8-8.4-9.2 0-5.4 3.8-10.1 8.4-10.5zm-37.2-18.9c4.6-.4 8.4 3.7 8.4 9.2 0 5.4-3.8 10.1-8.4 10.5-4.6.4-8.4-3.7-8.4-9.2.1-5.4 3.8-10.2 8.4-10.5zm73.5 36.8c4.6-.4 8.4 3.7 8.4 9.2 0 5.4-3.8 10.1-8.4 10.5-4.6.4-8.4-3.7-8.4-9.2 0-5.4 3.8-10.1 8.4-10.5zm-45.6-60.9c4.6-.4 8.4 3.8 8.4 9.2 0 5.4-3.8 10.1-8.4 10.5-4.6.4-8.4-3.8-8.4-9.2 0-5.4 3.8-10.1 8.4-10.5zm-37.2-18.9c4.6-.4 8.4 3.8 8.4 9.2 0 5.4-3.7 10.1-8.4 10.5s-8.4-3.8-8.4-9.2c.1-5.4 3.8-10.1 8.4-10.5zm73.5 36.8c4.6-.4 8.4 3.7 8.4 9.2 0 5.4-3.7 10.1-8.4 10.5s-8.4-3.7-8.4-9.2c0-5.4 3.8-10.1 8.4-10.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1437 953-122 50" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m704 1071 173 15" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m531 1292 310 147" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M530 1298c-1.2-22.9-1.5-107.1 1-124" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M519 1259c-1.9-45.4-6.4-87.7 12-85s135.2 23.4 142 24 12.9 5.5 19-46 29-214 29-214 8.1-16.9-19-8-142.6 51.8-153 56-20.9 2.3-26 47"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M657.5 1163c8.3.7 14.8 4.7 14.4 8.9-.4 4.2-7.4 6.9-15.7 6.2-8.3-.7-14.8-4.7-14.4-8.8.3-4.3 7.4-7.1 15.7-6.3zm890.5-150.8c6.3-1.8 12.5-.7 14.2 2.4 1.8 3.2-1.6 7.7-8.1 10-6.9 2.5-14.1 1.5-15.8-2.3-1.5-3.6 3-8.1 9.7-10.1zm-165.9-135.3c6.4 1.4 11.3 5.2 11.4 8.8.1 3.7-5.1 6-11.8 5-7.2-1.1-13.2-5.4-12.8-9.5.3-3.9 6.4-5.8 13.2-4.3zM685 951.2c8.1-2.1 15.5-.5 16.5 3.6 1 4-4.7 9-12.8 11.1-8.1 2.1-15.5.5-16.5-3.6-1.1-4.1 4.6-9 12.8-11.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M547 987c1.3-16.6 5.9-24.9 211-114 41.8-20.8 60.4 29.3 81 70 36.4 71.7 46.3 116.6 46 132"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M758 871c-43.5-30.1-113.9-26.8-143-38-28.4-10.9-75.1-65.4-104-112"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1055 1100c152.6-28.3 224.7-27.8 241-106 8.1-66.6-38.3-159.4-40-166"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M811 353c16.8 4.1 51.8 22.4 85 72 35.5 3.8 85 9 85 9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m880 405-43-4s-41.9-37.2-74-47" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m691 425 19-21s-9.2-25.8-29-45" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M697 376c8.9-.5 101-5 101-5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m902 425-203 14s-.2.4 1.1 1.1c20.5 4.4 49.9 9.9 49.9 9.9l229-16"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m715 475 164 168m9-14 29 9 2 10 115 23 11-7 32 6 27 39 144 17 25-45"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1227 661s-163.7-64.3-183.9-72.2c-1.3-.5-1.1-.8-1.1-.8l127-25-20-34 50-2-15-19-110-26-118 2 51 75 135-5-23-40 65-5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1141 554 25 8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1009 561 37 31" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1033 672-39-55-87-16 12 47" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1045 663-41-57-78-15-24 9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m748 451 134 198s.5.1 1.2-1.3c2.7-5.1 8.8-17.7 8.8-17.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1267 659-20 34-22-32s-.3-.1-.8 1.4c-1.8 5.5-6.2 18.6-6.2 18.6l25 43"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M528 750s-87 24.5-92 31 24.6 129.6 26 156 4 91 4 91" />
		<path fill="#b3b3b3" stroke="#1a1a1a" strokeWidth={10} d="m461 937 156-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M455 795h15l24 123h-15l-24-123z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m334 829 5 49 79 20s-20.4-110-14-117c9.3-10.2 113-48 113-48"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M403 781c-9.4 1.6-59.4 16.5-68 20" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M916 1107c50.7 23.4-74.5-266.7-214.3-291.8-66.6-12-135.4-8.9-191.7-96.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m915 1045 381-64" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M955 1037c-8.3-32.5-97.9-211.7-200-236-.9-.2-5-1-5-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m932 983 321-52" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m983 976 23 53" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1029 967 23 53" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1076 958 20 53" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1119 951 19 53" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1161 946 18 53" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1204 939 18 52" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1262 988c5.8-63.9-83.6-203.5-39-228" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M702 851c0-19.8-1-37-1-37s197.3-62 378-63c132.9-.8 174 16 174 16"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M811 772c6.4 2.9 13.7 9 17 11" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1141 738c4.8 4.3 8.8 9.6 12 13" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M481 576c4.7 80.5 161.3 188.6 328 195 7.2-.7 18-7 18-7s-83.2-267.9-178-410"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M579 416c2.3 6.1 6.4 13.2 11.8 20.7m9.5 12.1c29.8 34.6 82.9 72 130.7 63.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M539.6 581.3c22.1 22.5 82.7 91.8 163.4 88.7-1.1-12.9-.9-35.8-2.8-50.7-6.9-15-21.9-46.1-24-49.5-35.1-6.2-69.8-38-93.5-63-3.1 5.3-40.7 69.9-43.1 74.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m598 524-41 74" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m617 539-38 78" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m636 556-34 83" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m655 563-23 90" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m676 570-10 95" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m605 417-27 40 10 10 27-39-10-11z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M989 663s10.6 16 28 27 78.3 47 118 47c19.5 0 22-20 22-20"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M853 347c31.5 12.5 85.2 64.3 102 87" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1142 735c-1.8-3.7-5.9-12.1-12-23.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m979 429-1 21-11 2v10l-49-8-123 6 55 69 92 16-35-66 80-9-16-4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1009 472v13" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m943 547 13-5-26-53 34-4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M890 630 777 465l23-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m799 463 112 17" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m959 488 163 27" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1322 552 16-37-48 4 13-19 56-4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1287 519-111-13" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1303 500-185-21-18 9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1029 443-58 11 67 10 100-3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1039 466v17" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1219 682s-142.7-40.3-161.1-45.5c-1.3-.4-1.9.5-1.9.5l48 73"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1057 638-15-51" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1667 1289s-134.8-159.9-371-295" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1424 864c-25.8.7-89.7.1-101.2 0-1.2 0-.8 1-.8 1s120.4 85 223 185c19.6-13.5 61.4-40 61-14"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1307 717c-6.3 19.3-78.3 20.3-50 113" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1274 770c-5.8 8-16.8 17.9-24 20" />
	</svg>
);

export default Component;

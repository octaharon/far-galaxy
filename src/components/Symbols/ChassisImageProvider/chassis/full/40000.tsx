import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#bfbfbf"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1061 349h-26s-32.4 5.1-39 14-14.2 39.5-22 51-17.5 23.2-23 35-9.1 39.4-18 55c-8.9 15.6-30.5 53.2-38 134s122 19 122 19-7.7-91.7-17.3-125.7c-.7-4-.5-6.2-.7-10.3 2.6-16.8 12.6-26.4 22-50 10.8-27.2 26.6-50 43-55s-3-67-3-67z"
		/>
		<linearGradient
			id="ch-full40000a"
			x1={1054.905}
			x2={939.6151}
			y1={2044.6246}
			y2={2088.8806}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#383838" />
			<stop offset={0.614} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000a)"
			d="m1055 415-23-59s-28.1 1-35 16c-6.8 14.9-10 27.6-15 38-7.2 15-18.9 25.3-26 42s-5.3 24-15 47c-6.3 14.9 11.3 22 29 23 9.7-2.1 20.7-3 25-3-.3-9.5 13-32 13-32s2.7-3.2 5.1-8.2c1.3-2.8 1.9-6.1 3.6-9.8 5.1-11.1 11.1-24 18.3-34 9.2-12.6 20-20 20-20z"
		/>
		<path
			fill="gray"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M722 1050c10.9 46.6 19.4 87 30 99 19.5 22.1 22.3 4.8 26 4-1.7-12.5-7-34-7-34l-9-36-4-15s-23-15.3-36-18z"
		/>
		<path
			fill="#ccc"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M726.1 1104.6c36.3 21 97 21 124-.9 8.9 32 22 75.5 7.3 126.4-3.5 12.2-5.8 27.4-12.8 31.1-12.6 6.8-23.3 21.3-41.2 21.8-1.3 0 3.3-18.3 3.8-28.2-3.3 9.5-11 28.3-12.6 28.3-17.9-.1-42.3 4.7-56.6 3.7-21.5-1.4-54-13.3-76.6-36.6-17.2-17.7-23.2-50.5-29.2-81.5 26.4 17.4 43.4 35.8 70.2 41.2-16.2-18.6-15.8-46.9-16.4-66.9 23.7 20.5 23.4 15.7 31 27.5.6 1 1.9 13.1 0 32.1 6.7-15.5-9-23 9.1-98z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M734.3 1107.3c-7.6-2.8-9.8-11.5.9-12.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M844.6 1106.4c3.2-2 8.2-9-.9-12.8" />
		<path
			fill="#bfbfbf"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1053 656c49.2-5.9 229.2 228.4 243 320s-11.9 165.3-7 200 15.5 66.6 21 96 3.8 60.2 7 67c-34.2 56.6-89.9 8.5-103-5-7.2-14.2-36.1-52.7-45-87s-28.2-55.3-44-67c-15.4-11.4-25.3-76.2-72-62-.7.3-1.5 1.1-2 2-5.5 7.1-15.7 19.8-23 44-8.6 28.4-28.6 69.8-46 93s-23.4 65.8-24 92-13.1 98.3-8 129 14 72-66 72-54-84.9-54-101-10.6-37.5-20-72-22-137.3-22-156c0-15.4-16.8-108.9-28-148-2.4-8.5 1.8-94 24-146-22.4 52-25.3 142.9-28.8 140.3-22.3-16.8-90.6-81.8-125.8-71.6-2.3.7 21.8-49.4 23.6-52.7-1.4 4.5-26 52.7-27.6 54.7-10.7 13.8-18.1 29.5-19.4 32.3-28.4 94.8-23.6 136-53 159s-47 44-47 44-51.3-40.8-52-71 8.9-71.8 5-106c-3.9-34.2-1.5-103.4 5-138s18.3-145.7 67-178c43.7-29 472.8-78.1 522-84z"
		/>
		<linearGradient
			id="ch-full40000b"
			x1={1183.6244}
			x2={1315.6244}
			y1={1293.1721}
			y2={1185.1699}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.529} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000b)"
			d="M1178 1258c15.5.5 33.1-9.8 38-23 14.4 1.2 44.7-2.1 75-20 3.1 6.5 19 64.9 19 107-24.8 14.9-72.2 11.9-94 3-11.1-14.9-30.6-46.6-38-67z"
		/>
		<linearGradient
			id="ch-full40000c"
			x1={1094.1}
			x2={1338.2405}
			y1={1089.9067}
			y2={1089.9067}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.282} stopColor="#bdbdbd" />
			<stop offset={0.536} stopColor="#e6e6e6" />
			<stop offset={0.857} stopColor="#e6e6e6" />
			<stop offset={0.969} stopColor="#bdbdbd" />
		</linearGradient>
		<path
			fill="url(#ch-full40000c)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1192.9 1320.9c38.3 22 102.2 22 130.7-1 9.4 33.6 23.1 79.2 7.7 132.6-3.7 12.8-6.2 28.7-13.4 32.7-13.2 7.2-21.6 19.3-40.4 19.9-3.8-1.5 7.3-29.6 6.6-40-5.2 10.8-8.5 37.9-15.9 40.2-18.9-.1-47.6 7.9-62.7 6.9-22.7-1.5-56.9-13.9-80.7-38.4-18.1-18.6-24.4-53-30.7-85.5 27.8 18.3 45.7 37.5 74 43.2-17.1-19.5-16.7-49.2-17.3-70.1 25 21.5 24.6 16.4 32.7 28.8.7 1 2 13.7 0 33.6 7.4-22.1-10.4-24.2 9.4-102.9z"
		/>
		<path fill="#f3f3f3" d="M1197 1328c12.8 6.4 39.7 21.3 99 10-34.9 15.5-63.3 15.8-99-10z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1201.5 1323.8c-8-3-10.3-12.1 1-13.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1317.8 1322.9c3.3-2.1 8.6-9.4-1-13.4" />
		<linearGradient
			id="ch-full40000d"
			x1={865.4682}
			x2={911.1062}
			y1={1177.3896}
			y2={1052.0016}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#393939" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000d)"
			d="M814 1375c27.7-4.2 50-30 50-30s47.4 21.3 90 11c-2.3 11.8-10 66.1-10 103-14.1 4.2-46.5 24-110-1 3.1-23.2-7.7-41.8-20-83z"
		/>
		<linearGradient
			id="ch-full40000e"
			x1={972.2626}
			x2={718}
			y1={948.9361}
			y2={948.9361}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#cecece" />
			<stop offset={0.225} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full40000e)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M821 1458c39.8 22.9 106.4 22.9 136-1 9.8 35 24.1 82.4 8 138-3.8 13.3-6.4 29.9-14 34-13.8 7.5-25.5 23.2-45.1 23.8-4.1-.1 9-31.6 8.1-43.8-6.2 12.1-9.5 39.3-17.9 43.9-19.6-.1-46.4 5.1-62.1 4.1-23.6-1.6-59.2-14.5-84-40-18.8-19.3-25.4-55.1-32-89 28.9 19 47.6 39.1 77 45-17.8-20.3-17.4-51.2-18-73 26 22.4 22.6 14.1 31 27-.3 2-4.4 29.8 2 51 8.8-18.1-6.2-40.4 11-120z"
		/>
		<path fill="#f2f2f2" d="M824 1465c10.7 6.1 50.8 22 104 11-12.5 15.3-87.4 11.7-106 0 .2-3.4.5-4.9 2-11z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M830 1461c-8.3-3.1-10.7-12.6 1-14" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M951 1460c3.5-2.2 9-9.8-1-14" />
		<linearGradient
			id="ch-full40000f"
			x1={678.3471}
			x2={486.2031}
			y1={1441.4966}
			y2={1511.4315}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.264} stopColor="#494949" />
			<stop offset={0.611} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000f)"
			d="M562 1175c12.7-27.3 18.1-60.3 20-71s11-43.7 15-59 8.7-27 18-40c2.8-4 8-12.1 12.6-20.2 10.3-18.4 21.3-42 23.4-45.8 3-5.5-37-91-37-91l-68-12-65 41-9 19s-9.6 63.7-9 89 1 111 1 111-5 46.6-5 62 5 21.2 7 22 51.6 24.9 96-5z"
		/>
		<linearGradient
			id="ch-full40000g"
			x1={504.9832}
			x2={535.6971}
			y1={1433.5653}
			y2={1318.9363}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#434343" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000g)"
			d="M464 1095c10.4.2 28.2-12.5 31-21s6.3 16.7 88 26c-3.7 10.2-10 62.1-22 75s-67.2 16.9-96 4c-10.8-10.1-5.1-52.7-1-84z"
		/>
		<linearGradient
			id="ch-full40000h"
			x1={584.4219}
			x2={352.3999}
			y1={1239.1315}
			y2={1239.1315}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.257} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full40000h)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M446.5 1176.2c36.3 21 97 20.9 124-.9 8.9 32 22 75.4 7.3 126.3-3.5 12.2-5.8 27.3-12.8 31.1-12.6 6.8-23.3 21.3-41.2 21.8-5.4-2.4 4.4-18.8 3.8-28.2-5.8 7.9-6.7 26-12.7 28.3-17.9-.1-42.3 4.7-56.7 3.7-21.5-1.4-54-13.3-76.6-36.6-17.2-17.7-23.2-50.4-29.2-81.4 26.4 17.4 43.4 35.8 70.2 41.2-16.2-18.6-15.8-46.9-16.4-66.8 23.7 20.5 23.4 15.6 31 27.5.6 1-2.6 25.6-.4 38.9 2.2-11.7.1-16.8 1-40-5.5-19.6-.3-36.3 8.7-64.9z"
		/>
		<path fill="#f2f2f2" d="M449 1184c14.6 5.6 44.3 18.8 87 11-29.8 12.1-75.8 7.8-89-1 .5-4.5.9-6 2-10z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M454.7 1178.9c-7.6-2.8-9.8-11.5.9-12.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M565.1 1178c3.2-2.1 10.1-7.2.9-11" />
		<linearGradient
			id="ch-full40000i"
			x1={748.8395}
			x2={1016.0884}
			y1={1676.1989}
			y2={1334.1359}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#3f3f3f" />
			<stop offset={0.645} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000i)"
			d="M782 945c12.1 16 48.3 34.5 67 33s47.8-10.4 66-59 12.8-94.8 2-112c1.1-6 10-5 10-5l15 2 13 62s57.2 63.5 85 114c27.8 50.5 38.1 106.2 18 140-36.3 16.2-277 24-277 24l-16-73s-.1-15.6.9-31.6c1-17 3.1-34.4 3.1-34.4s7.8-46.3 13-60z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full40000j"
			x1={691.2346}
			x2={735.2476}
			y1={1600.3524}
			y2={1518.6093}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#5c5c5c" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000j)"
			d="M654 937c-3.8 10.2-6 20-6 20s89 32.5 120 23c3.3-15.1 11-43 11-43l-27-37s-29.9 55.5-98 37z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full40000k"
			x1={1100.9857}
			x2={1315.9857}
			y1={1300.6102}
			y2={1358.2191}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000k)"
			d="M1096 1126c11.2 24.1 44.1 88.5 59 98s41.4 80.2 59 100c27.7 24.1 97.6 9.3 97-2-1.8-35.1-10.9-84.1-21-111s4-83 4-83-1.1-3.3 3-38 3.4-104.4-1-111c-8.7.1-21 1-21 1s-111.7 154.6-179 146z"
			opacity={0.502}
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M626 998c13.7-20 28.6-56.4 32-64" />
		<linearGradient
			id="ch-full40000l"
			x1={1123.9895}
			x2={1338.9895}
			y1={1483.5105}
			y2={1268.5105}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.585} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000l)"
			d="M1096 1126c1 2.2 4.3 2.5 6 5 18.5 27.7 39.5 84.4 53 93 14.9 9.5 41.4 80.2 59 100 27.7 24.1 97.6 9.3 97-2-1.8-35.1-10.9-84.1-21-111s-4-82-4-82 2.9-8.3 7-43 3.4-100.4-1-107c-8.7.1-17 1-17 1s-111.7 154.6-179 146z"
		/>
		<linearGradient
			id="ch-full40000m"
			x1={677.1568}
			x2={720.8328}
			y1={1429.092}
			y2={1549.092}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8c8c8c" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full40000m)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M768 980c-34.9 8.4-107-18.3-121-24-7.1 14.5-14.7 33.5-22 43 25.5-.4 115.7 57.7 133 77 .7-9.8.1-59.4 10-96z"
		/>
		<linearGradient
			id="ch-full40000n"
			x1={632.9708}
			x2={712.9778}
			y1={1517.7076}
			y2={1510.7086}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#424242" />
			<stop offset={0.736} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000n)"
			d="M633 995c5.8-7.2 15-31.8 16-33s25.1 11.1 54 18-14 41-14 41-23.5-15.7-56-26z"
		/>
		<linearGradient
			id="ch-full40000o"
			x1={1216.5576}
			x2={1019.5667}
			y1={1673.8958}
			y2={1346.0468}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#202020" />
			<stop offset={0.697} stopColor="#b6b5b5" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000o)"
			d="M1055 1122c28.9 2.6 56.8 8.3 94-11 24.6-12.8 127.6 41.3 142-10 3.5-12.4 5.1-43.2 4.8-71.7-.2-28.1-2.2-54-1.8-58.3-32.9 9.3-198.9-8.1-288-174-5.5-.5-1 51-3 54-28.6 10.5-46 18-46 18s159.8 162.2 98 253z"
		/>
		<linearGradient
			id="ch-full40000p"
			x1={1128}
			x2={1128}
			y1={1381.1602}
			y2={1638}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.003} stopColor="#5c5c5c" />
			<stop offset={0.096} stopColor="#a6a6a6" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full40000p)"
			d="M1055 1122c28.9 2.6 56.8 8.3 94-11s139.9-138.3 142-164c-34.4 9.7-154.7 72.6-284-79-28.6 10.5-42 14-42 14s151.8 149.2 90 240z"
		/>
		<linearGradient
			id="ch-full40000q"
			x1={1264.4598}
			x2={1007.6108}
			y1={1713.1573}
			y2={1456.3094}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.376} stopColor="#1f1f1f" />
			<stop offset={0.767} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000q)"
			d="M1055 1122c28.9 2.6 56.8 8.3 94-11s139.9-138.3 142-164c-34.4 9.7-154.7 72.6-284-79-28.6 10.5-42 14-42 14s151.8 149.2 90 240z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1055 1122c28.9 2.6 56.8 8.3 94-11s139.9-138.3 142-164c-34.4 9.7-154.7 72.6-284-79-28.6 10.5-42 14-42 14s151.8 149.2 90 240z"
		/>
		<path
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1081 706.9c-61.6 11.6-82.6 85.8-79 142.1-20.8 7.7-30.6 11.5-48 18-1.7-10.4-5.8-62.9-11.7-63.7-73.2-10-216.9 17.6-354.3 59.7-4.5-7.6-7.7-15.1-11.2-15.4-65.7-5.4-91.9 23.7-119.8 49.4-9.3-157.5 89.6-197 400.8-250.7 70.7-12.2 170.6 5.3 265.2 14.6 180.3 17.9 14.7 35.4-42 46z"
			className="factionColorSecondary"
		/>
		<path
			fill="#f2f2f2"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1067.2 763.9c-61.6 11.6-68.7 28.9-65.2 85.1-20.8 7.7-30.6 11.5-48 18-1.7-10.4-5.8-62.9-11.7-63.7-73.2-10-216.9 17.6-354.3 59.7-4.5-7.6-7.7-15.5-11.2-15.4-44.8 1.1-67.1-1.8-119.8 49.4-9.3-121.6 47.8-136.3 359-190 70.7-12.2 198.6 1.5 293.2 10.8 180.2 17.9 14.7 35.4-42 46.1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M580 850c0-27.8 19.3-146.8 125-175" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M892 643c-46.1 5-60.8 41.5-67 51" />
		<path
			fill="#bfbfbf"
			d="M648 506s26.5 95.6 29.4 110.8c.9 3.2 1.6 4.3 2.1 5.5C693.2 639.5 754 768 754 768s35.3 123.4-29 159c-74.6 41.3-111-21.2-111-75s11-174.9 7-210.7c-1.6-14.5-24.9-55.7-29.7-83.7-7.8-46-16.2-81-44.3-152.6 58.7 25.7 101 101 101 101z"
		/>
		<linearGradient
			id="ch-full40000r"
			x1={645.3045}
			x2={601.5765}
			y1={2015.4136}
			y2={1880.8317}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#444" />
			<stop offset={0.674} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000r)"
			d="m649 503 26 105-58 18s-18.5-44.9-22-55-14-67.2-18-79c8.1-2.6 28.4 3.8 44.8 10.1 4.5 1.7 3.1-5.2 7.2-5.1 8 .3 20 6 20 6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M648 506s26.5 95.6 29.4 110.8c.9 3.2 1.6 4.3 2.1 5.5C693.2 639.5 754 768 754 768s35.3 123.4-29 159c-74.6 41.3-111-21.2-111-75s11-174.9 7-210.7c-1.6-14.5-24.9-55.7-29.7-83.7-7.8-46-16.2-81-44.3-152.6 58.7 25.7 101 101 101 101z"
		/>
		<linearGradient
			id="ch-full40000s"
			x1={682.8182}
			x2={682.8182}
			y1={1570}
			y2={1715.3615}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#494949" />
			<stop offset={0.531} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000s)"
			d="M621 805c-2.5 23.5-3.9 48.9.4 71.1 6.3 32.4 23.7 57.8 58.6 59.9 49.6-2.9 67-36 67-36s-15.5-20.4-31.6-41.9c-15.3-20.3-32.2-42.6-36.4-52.1-8.5-19.7-55.9-20.9-58-1z"
			opacity={0.502}
		/>
		<path
			fill="#f2f2f2"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M917 802s-68.1-87.1-77-98c-60.6-51.5-209.2 32.4-147 115 76.6 101.7 89.7 121 105 141 92.3 75.7 157.9-79.1 119-158z"
		/>
		<path
			fill="#d9d9d9"
			d="m857 734 56 71s16.9 33.9 6 85-34.8 70.4-49 79c-4.8 2.9-17.7 6-23 6s-26.1-1.9-44-18-90-120-90-120 47.8 17.2 100-24 44-79 44-79z"
		/>
		<linearGradient
			id="ch-full40000t"
			x1={723.6827}
			x2={934.3437}
			y1={1533.9863}
			y2={1744.6484}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.777} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40000t)"
			d="m857 734 56 71s16.9 33.9 6 85-33.8 70.4-48 79c-4.8 2.9-18.7 7-24 7s-26.1-1.9-44-18-91-122-91-122 48.8 18.2 101-23 44-79 44-79z"
			opacity={0.302}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M746.5 735.4c16.8-11.4 37-12.7 48 .9 11 13.5-.9 31.7-13.1 40.3s-34.2 15-47 1.1c-9.2-10.2-4.7-30.9 12.1-42.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M684 805c46.6 86.1 213.9-25.9 157-101" />
		<path
			fill="#e6e6e6"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M467 991c29.9 2.5 66.3 88.3-4 101-28.2-22.7-16.1-102.7 4-101z"
		/>
		<linearGradient
			id="ch-full40000u"
			x1={604}
			x2={496}
			y1={1435}
			y2={1435}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full40000u)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M503 1040c19.9 6 55.5 15.7 101 7-1.8 22.9-6.3 42.8-11 55-9.8-1.6-91-12.9-97-30 4.9-7.9 5.3-18.1 7-32z"
		/>
		<path
			fill="#e6e6e6"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M466.5 990.6c8.9 1.6 27.5 18.4 18.4 64-7.2 36.3-25.1 35.7-28.1 35.1-3.1-.6-24.7-9.3-23.9-51.7.7-35.6 18.9-50.1 33.6-47.4z"
		/>
		<path
			fill="#e6e6e6"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M792 1234c20.6-6.1 85 25.3 85 80 0 36.4-51.5 60.1-63 64s-41.3-138.3-22-144z"
		/>
		<path
			fill="#e6e6e6"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M795 1234c12.9-.2 48 17.4 48 84 0 53.1-29.5 58-34 58s-37.4-6.2-48-66c-8.9-50.2 12.6-75.7 34-76z"
		/>
		<path
			fill="#ccc"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M978 1263c5.9 14.4 14 47.3-16 72-1.6.4-3 3-3 3s6.8-54.9 19-75z"
		/>
		<path
			fill="#e6e6e6"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M848 1270c25.5 9.2 100.9 34.2 128 6-7.6 27.4-11.1 35.7-14 64s-97.2 8.6-107 4c2.8-25.8 1.7-48.6-7-74z"
		/>
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1133 1170c17-3.3 56.5-12.7 77 38 13.2 32.6-14 41.6-21 44"
		/>
		<path
			fill="#e6e6e6"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1294 1206c11.8-4.5 29.5-15 25-51-4.1-32.4-14.3-43.6-24-51-3.1 11.1-10.7 52.2-9 68s5.5 28.3 8 34z"
		/>
		<path
			fill="#ccc"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1134 1170c12.9-.9 34 7 52 39s8.1 41.1 2 44c-8.2 4-23.1-.6-46-29s-20.9-53.1-8-54z"
		/>
		<path
			fill="#e6e6e6"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1180 1178.5c75.8-9.6 95.8-32.5 113.1-49.1 1-.8 1.4-.9 2.9-1.4.3 1.8.5.5.2 2.8-2.3 22.3-2.7 50.4 2.8 74.2-10.2 7.1-56.3 32.4-90 24.5-6.8-21.5-7.9-26-29-51z"
		/>
		<g>
			<path
				fill="#f2f2f2"
				d="M1010 640c23.3-3.2 305-52 305-52s216.4 33.8 240 75c4.3 86.6-23.2 189.7-56 239s-93.2 78-196 78-225.1-58.5-241-74c-49-47.7-55.1-95-52-266z"
			/>
			<path
				d="m1019 642 297-52 60 11-43 33s197.7 52.9 202 54c-28.2 16.2-80.4 18.6-96 19-14.2-3.4-195-54-195-54l-183 5-42-16z"
				className="factionColorPrimary"
			/>
			<linearGradient
				id="ch-full40000v"
				x1={1282.2106}
				x2={1282.2106}
				y1={1399.8611}
				y2={1737.8611}
				gradientTransform="matrix(1 0 0 -1 0 2506)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#8c8c8c" />
				<stop offset={1} stopColor="#e6e6e6" />
			</linearGradient>
			<path
				fill="url(#ch-full40000v)"
				d="M1010 642c126.1 62.7 515.2 105 543.5 28.7.5-1.3 1.5-4.7 1.8 1.5.8 34.1-3.8 96.8-24.3 158.8-22.4 67.4-56.7 149-225 149-46.5 0-88.6-11.2-126.7-22-47.7-13.5-104.4-32.1-134.3-70-29.8-37.8-35.9-103.6-35-136 .5-20-2.4-88.7 0-110z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1010 642c126.1 62.7 515.2 105 543.5 28.7.5-1.3 1.5-4.7 1.8 1.5.8 34.1-3.8 96.8-24.3 158.8-22.4 67.4-56.7 149-225 149-46.5 0-88.6-11.2-126.7-22-47.7-13.5-104.4-32.1-134.3-70-29.8-37.8-35.9-103.6-35-136 .5-20-2.4-88.7 0-110z"
			/>
			<path
				fill="#ff2a00"
				d="M1456 800c-17.2 2-60.5 8-105 8-1.3 15.3-4 39-4 39s64.9 2.2 105-8c.4-13.2 2.5-21.8 4-39zm89-30c-5.8 4.6-18 12.2-26 15-1.5 14.2-5 40-5 40s20.5-10.4 23-16 4.5-18.7 8-39z"
			/>
			<path
				fill="#f2f2f2"
				d="M1479.8 743.7c-1.1 19.4-9.3 109.5-18.8 142.3 9.9-1.7 25-6 25-6s16.8-69.8 20-145c-8.5 3.3-18.7 4-23.6 4.7-2.3-.7-3.2.2-2.6 4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1479.8 743.7c-1.1 19.4-9.3 109.5-18.8 142.3 9.9-1.7 25-8 25-8s16.8-67.8 20-143c-8.5 3.3-18.7 4-23.6 4.7-2.3-.7-3.2.2-2.6 4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1456 800c-17.2 2-60.5 8-105 8-1.3 15.3-4 39-4 39s64.9 2.2 105-8c.4-13.2 2.5-21.8 4-39zm89-30c-5.8 4.6-18 12.2-26 15-1.5 14.2-5 40-5 40s20.5-10.4 23-16 4.5-18.7 8-39z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1010 640c23.3-3.2 305-52 305-52s216.4 33.8 240 75c4.3 86.6-23.2 189.7-56 239s-93.2 78-196 78-225.1-58.5-241-74c-49-47.7-55.1-95-52-266z"
			/>
		</g>
		<path fill="none" d="M-1380.5-30c.3 0 .5.2.5.5s-.2.5-.5.5-.5-.2-.5-.5.2-.5.5-.5z" />
		<path
			fill="#1a1a1a"
			d="M1001 523.2c-3.9.4-8.2.9-12.5 1.1-4.3.3-8.6.3-12.8-.1-4.3-.3-8.5-1-12.6-2.1s-8-2.6-11.7-4.5c4 1.1 8 1.8 12.1 2 4 .3 8.1.1 12-.4 4-.5 7.9-1.3 11.7-2.4 3.9-1 7.6-2.3 11.8-3.5l2 9.9z"
		/>
		<path
			fill="#1a1a1a"
			d="M682.3 621.6c-3.1 3.9-6.7 6.2-10.3 8s-7.3 3.1-11 4.2c-7.4 2.1-14.9 3.2-22.4 3.5 7.2-1.9 14.1-4.7 20.5-8.2 3.2-1.7 6.2-3.7 8.8-5.9s4.9-4.7 5.9-7l8.5 5.4z"
		/>
		<g>
			<path
				fill="#f2f2f2"
				d="m633 385 40 17-5 10 178 71-78 12 40 67-181-67-7 10-46-17-51-87 33-7 15 6 33-7 5-8h24z"
			/>
			<linearGradient
				id="ch-full40000w"
				x1={851.1223}
				x2={774.1223}
				y1={1969.9731}
				y2={2009.2072}
				gradientTransform="matrix(1 0 0 -1 0 2506)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#999" />
				<stop offset={1} stopColor="#404040" />
			</linearGradient>
			<path fill="url(#ch-full40000w)" d="m770 494 77-8-37 77-40-69z" />
			<path
				fill="#a8a8a8"
				d="m810 563-40-68-250-96 55 88s19.5 6.3 47.3 15.3c2.7.9 3.8-6.2 6.7-5.3 69.9 24.7 181 66 181 66z"
			/>
			<path
				fill="#595959"
				d="m608 496-47-79 5-6 18-2-6-3-19 2-4 7 45 77 8 4zm-38-92-17 1s-6.8 7.8-6 8 49 80 49 80l-6-1-49-81 5-8 15-2 9 3zm53-6 26-3-9-3-24 3 7 3zm7 3 30-1 8 2-33 2-5-3z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m850 483-41 81-182-69-5 9-47-16-56-90 35-4 11 4 35-5 5-7 28-1 41 17-7 9 183 72z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m601 393 45 18" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m556 395 52 19" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m849 483-83 11 43 67" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m768 495-191-74-10-6-45-15" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m669 409-98 8 54 81" />
		</g>
		<g>
			<path fill="#f2f2f2" d="m1364 409-33 67-243-48-6 9-62-13-26-80 70-7 63 12-4 7 241 53z" />
			<linearGradient
				id="ch-full40000x"
				x1={1354.8176}
				x2={1303.8176}
				y1={2059.187}
				y2={2084.0613}
				gradientTransform="matrix(1 0 0 -1 0 2506)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#797979" />
				<stop offset={1} stopColor="#404040" />
			</linearGradient>
			<path fill="url(#ch-full40000x)" d="m1311 419 51-7-30 62-21-55z" />
			<path fill="#a8a8a8" d="m1310 420-255-60-4-4-56-10 24 79 60 12 10-9 243 48-22-56z" />
			<path
				fill="#595959"
				d="m1061 430-28-76-4-1 28 77h4zm-7.9-84.3c-12 .7-23.1 1.3-23.1 1.3l7 3s11-.8 23.3-1.7c-3.5-1.3-5-1.9-7.2-2.6zm20 1.7c10.8-.8 19.9-1.4 19.9-1.4l-11-2s-7.5.4-16.8 1c1.5.4 5.4 1.7 7.9 2.4zM1018 352l24 74-4-2-24-74 4 2z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1364 409-33 67-243-48-6 9-62-13-26-80 70-7 63 12-4 7 241 53z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1126 350-76 7 31 79" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1360 411-50 7 19 56" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1311 419-255-58-9-7-52-9" />
		</g>
	</svg>
);

export default Component;

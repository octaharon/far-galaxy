import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#d9d9d9"
			d="M937 1368c0 5.1-12.3 49.1-4 74 20.5 61.3 87 103 87 103 10.8 4.8 45.5 8.3 67-1s34.8-14.4 32-51c-1.6-20.2-10.9-34.5-28-53-1.9-2.1 5.8-6 4-9-31-30.3-33.3-68.4-33-81-5.1-5.5-8.2-6.6-11.1-7.1 2.3-19.5 9.2-38.4 6.1-85.9 7.2-19.7 13.9-32.4 15-57s-7.6-49.2-9-56c2.5-24.4 5-72 5-121s-11.1-144.6-16-173c3.1-12.1 1.1-30.5 2-37 25.4-12.6 64.3-41.8 78-51 14.1-16.7 14-41.3 12-74-10.7-42.7-14.6-58.2-26-104 8.1-3.9 18.8-15.2 17-31-1.5-12.7-17.3-49.9-29-59-21.4-38.9-69.6-35.3-103-38 3.5-16.5 8.5-50.1 7-88 19.4 0 17.1-16 11-37-5.5-19-4.6-14.8-12-22-9.9-3.2-22.2-.5-31-2-45.1-66.5-122.3-55.1-148.1-2.2-16.9 19.2-24.2 73.2-5.8 109.9.5 12.6.9 25.3.9 37.3-13.9 5-60.1 4.3-57 46-16 7.6-29.6 16-39 28-39.3 50.2-25.7 127.6-9 115-.4 16.3-4.4 81.2-1 104 13 72.5 32.7 79.8 56 100-53 29.5-153 87-153 87l37 76 133-74s-16.7 108.8-17 213c1.3 12.3-23.5 19-12 81-2.2 14.5-24.5 19.1-18 76s8.2 52.3 9 85c-5.9 15.9-7 26.3 1 50-8.2 50.2 19.8 115.9 34 130s48.3 26.5 68 20c31.3-5.3 49.4-20.7 45-55s-16.4-78.1-21-100c-2.4-11.6 1.4-45.9-14-57 3.2-27.5 9.1-56.9 9-100 7.8-21.5 14.9-33.6 16-56s-1.9-46.3-4-59c22.3-62.9 45.3-145.9 51-176 .5.1-1.4 23.8-1 24 2.9 49.1 9.2 74.1 19 122-12.1 13.3-20 30.9-19 70-21.8 28.3 3.4 132.7-1 166z"
		/>
		<path fill="#ccc" d="m943 323 39-4-9 27-40 5 10-28z" />
		<radialGradient
			id="ch-full10002a"
			cx={1092.347}
			cy={799.934}
			r={137.293}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10002a)"
			d="M1088 1438c-15-4-44.1-4.9-76 23s-40 45.8-26 59 42.5 4.8 44 5 64.9 9.9 90-24c-3.6-30.7-8-42.6-32-63z"
		/>
		<radialGradient
			id="ch-full10002b"
			cx={889.788}
			cy={834.64}
			r={147.478}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.496} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10002b)"
			d="M888.9 1476.1c-15-4.1-58.9-16.1-90.9 11.9s-25.4 57.2-11.4 70.5c2.4 9.5 9.9-4.8 11.4-4.5s47.8 39 105-12c-3.6-30.8-8.1-40-14.1-65.9z"
		/>
		<path
			fill="#e6e6e6"
			d="M835.3 607c-5.3 2-8.5 4.5-16.3 8.3 6.2 9.5 12.6 17.6 18 20.7 15.3 8.5 127.9 25.3 237-30 6.2-27.9 8.4-58.1 7.3-66.9-1.9-5.3-7.6-33.7-11.3-33.1-5.1.9-9 2-9 2l-95 19-7 17-27 2-15-17-85.9 6 18.8 60.9s-4.4 1.8-14.6 11.1z"
		/>
		<linearGradient
			id="ch-full10002c"
			x1={917.3341}
			x2={917.3341}
			y1={-59.812}
			y2={-255.7555}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full10002c)"
			d="M934 349c-28.9-45.6-66.7-54.3-80-39-37.4 42.9-30 125.4-30 144 14.4 19.2 49.7 45 108 45s67.8-34.6 71-50 6.1-41.1 6-49 3.2-15.3 2-18c-.9-2.1-11-40.1-26.8-65.7-4.4-7-9.2 33.8-14.2 29.7-19.7 1.8-30.3 3-36 3z"
		/>
		<linearGradient
			id="ch-full10002d"
			x1={933.5}
			x2={933.5}
			y1={207}
			y2={44}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.3} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10002d)"
			d="m1063 702-99 81s-24.8.6-32 10c-10.7-6-41-18-41-18l-19 12-68-38s15.9-60.8 7-113c39.4 4.1 113.5 29.3 227-6 23.9 7.1 25 72 25 72z"
			opacity={0.2}
		/>
		<path
			fill="#e6e6e6"
			d="M893 1481c-2.8-8.1-14.9-56.9-15-93-15.5-14.3-56.1-31.5-88-3-2.4 14.5-4 54-4 54l12 51s35.1-35.8 95-9zm93-124c5.8-6.7 35.7-31.8 77-8 4.7 22.3 9 42 9 42l-3 7s23 26.8 25 29 2.7 12.4-18 11-55.8 15.4-63 23c-5.6-13.8-16-50-16-50h-5s-11.8-47.3-6-54z"
		/>
		<path fill="#f2f2f2" d="m676 954 271-155-57-24-265 158 51 21z" />
		<path fill="#f2f2f2" d="M966 812c8 .2 32.5-5.8 22-17s-52-11.6-54-3 14 9 14 9l-1 13s11-2.2 19-2z" />
		<path fill="#d9d9d9" d="m828 815 57 15-11 9-55-19 9-5z" />
		<radialGradient
			id="ch-full10002e"
			cx={924.7}
			cy={-198.9451}
			r={270.1}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.346} stopColor="#000" />
			<stop offset={0.546} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10002e)"
			d="M815 492.1c14.4-6.2 28-12.5 31-15.1 14.7 6.1 110.5 49.1 154-7 12.2 5.4 23.9 3.5 33 2 7 11.1 28 36 28 36l-95 19-6 17h-30l-10-14-87.1 3.7c-9.2-12.8 34.5-11.3-17.9-41.6z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full10002f"
			x1={769.2206}
			x2={845}
			y1={-103.5}
			y2={-103.5}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full10002f)"
			d="M823 455c4.9 6 17 18.9 22 22-7.4 6.5-67.7 31.5-75 33 0-14.6-11.4-49.6 53-55z"
			opacity={0.702}
		/>
		<path
			d="M765 1227c9.2 7.5 32.2 20.1 50 18 7 17.7 11.3 36.8 35 34s26.5-6.9 28-9c-.8 12.2-10.1 92.8-12 107-16.6-4.7-57.2-12.4-77 15-.3 4.8-1 18-1 18s-4.2 4.9-18 0c-1.3-7.3-2.7-23.6-15-19-.9-82.2-30.5-109.9 10-164zm173-29c5.5 7.9 27 29.3 58 19 7.1 22.6 26.2 63.1 63 37-3.4 26.5-11 88-11 88s-38.4-10.9-61 20c-.9 15.4 0 18 0 18s1.3 6.5-21 3c-3.4-13.8-8.3-41.5-29-16 7.6-37.4-19.1-126.4 1-169zm189.1-430c-3.5-5.4 7.3-76.5-67.1-63-23.3 22.8-95 77-95 77s32 7.8 42 52c21.3-10.9 103.2-53.9 120.1-66zM872 786l-96 59s-31.3-25.6-35-30 8.9-46.1 26-57c17.3-11 38.6-8.5 49-2 5.3 3.3 56 30 56 30z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-full10002g"
			cx={851.862}
			cy={658.0873}
			r={89.117}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000" />
			<stop offset={0.6561} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10002g)"
			d="M875 1273.3c-10.6 4.9-51.9 19-61-29-18.3-1.3-34.7-5-52-20-24.7 32.5-35.8 135 108 124 3-29.2 4.4-61.3 5-75z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full10002h"
			cx={1037.252}
			cy={634.952}
			r={95.554}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2308} stopColor="#000" />
			<stop offset={0.6614} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10002h)"
			d="M1057 1256c-10.6 4.9-41.5 23.3-63-38-25.5 4.8-39.7-3-57-18-13.1 43.8-31.8 144 112 133 3-29.2 7.4-63.3 8-77z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10002i"
			x1={787.0509}
			x2={835.2239}
			y1={244.1043}
			y2={149.5593}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full10002i)"
			d="m872 786-96 59s-31.3-25.6-35-30 8.9-46.1 26-57c17.3-11 38.6-8.5 49-2 5.3 3.3 56 30 56 30z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full10002j"
			x1={1084.3679}
			x2={985.8949}
			y1={223.9353}
			y2={93.2563}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.495} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full10002j)"
			d="M1127.1 768c-3.5-5.4 7.3-76.5-67.1-63-23.3 22.8-95 77-95 77s32 7.8 42 52c21.3-10.9 103.2-53.9 120.1-66z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full10002k"
			x1={1101.9406}
			x2={1042.2465}
			y1={196.4415}
			y2={127.7705}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full10002k)"
			d="M1041 718c15.7-1.5 60.9-5.9 69 61-7.8 5.2-13 7-13 7s2.7-72-71-53c11.8-9.3 7.4-8 15-15z"
		/>
		<linearGradient
			id="ch-full10002l"
			x1={989.739}
			x2={898.4296}
			y1={264.974}
			y2={264.974}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.638} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-full10002l)"
			d="M906 866c-8.3 5.2-23 14-23 14s18.4 15.7 40 7 29.7-15.3 33-19 30.3-45 12-53-23.7 4.2-25 7c-6.3.6-17.2 3.8-23 16s-5.7 22.8-14 28z"
		/>
		<linearGradient
			id="ch-full10002m"
			x1={1061.0422}
			x2={932.4103}
			y1={425.9049}
			y2={389.0199}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full10002m)"
			d="M958 1129c-5.7-20.2-22.9-77.2-18-155 77.8-8.2 72.3-122.6 111-129 21.4 107.3 19 228.5 11 290-10.3-1.9-39.9-16.7-60 24-9.3-27.3-30.1-35.4-44-30z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full10002n"
			x1={849.3315}
			x2={849.3315}
			y1={692.5939}
			y2={562}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.198} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full10002n)"
			d="M853 1148c-10.5 0-26.7 3.9-31 26-4.1 21.2-20 16.8-14 46 7.4 35.8 19.7 63 45 58 34.8-2.6 38.2-44.4 39-55 0-10.2-4.3-61.6-39-75z"
		/>
		<linearGradient
			id="ch-full10002o"
			x1={1030.7382}
			x2={1030.7382}
			y1={674.828}
			y2={542.8271}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.204} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full10002o)"
			d="M1033.9 1131c-18.8-.1-41.1 42.5-41.4 64.9-.4 26.7 15.7 67.3 45.5 67.1 29.6-.2 28.4-39.8 29.6-57.4 1.3-17.6 9.1-74.3-33.7-74.6z"
		/>
		<path
			fill="#a6a6a6"
			d="M1069 608c-3.2 37-54.6 43.2-80 50-24.3-20.8-35.9-18-56 7-74.7 15.9-112.7-6.1-133-47 3.2-5.9 10.6-10 13-13 9.7 16.1 17.8 28.2 22 31 7.2 4.8 62.3 13.7 124 5s81.1-20 110-33z"
		/>
		<linearGradient
			id="ch-full10002p"
			x1={900.969}
			x2={798.5989}
			y1={144.3992}
			y2={22.3992}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full10002p)"
			d="M811 605s6 21.4 34 33c13.9 5.8 32.2 6 60 6 31.4 0 16.7 22.9-5 30s-79.4 38.5-91 53c2.4-37 7.7-93.9-9-109 5-5.5 11-13 11-13z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full10002q"
			x1={765.3192}
			x2={765.3192}
			y1={41.485}
			y2={186.2278}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.349} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10002q)"
			d="M720 643c-2.7 14.4-1.2 113.5 2 119 6.3 8.3 39.3-15.9 83-13 5.9-35.7 7.3-80.1 7-105-3.7-11.8-.8-15.2-12-25-17.5 11.3-29.5 34-80 24z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10002r"
			x1={1102.5}
			x2={1102.5}
			y1={-9.4551}
			y2={125.0879}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.349} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10002r)"
			d="M1083 548c2.1 7 8.3 33 34 33 6.7 19.2 23 97 23 97l-75 4s5.2-49.8 2-74c4.4-2.8 7-4 7-4s6.9-63 9-56z"
			opacity={0.502}
		/>
		<path
			d="M733 523c-14.3 4.6-28.1 44.8-27.3 80.2.8 35.9 9.2 39 25.3 42.8 6.5-22.9 6.4-54.2 21-108.8-3.3-3.7-14.3-10-19-14.2zm302.8 13.8c4.9-.7 10.2 3 11.9 8.1 1.6 5.1-.9 9.8-5.7 10.6-4.9.8-10.3-2.7-12.1-7.9-1.7-5.3.9-10.1 5.9-10.8zm11.7 35.3c4.7-.9 9.7 2.3 11.3 7.1 1.6 4.8-.8 9.6-5.4 10.7-4.7 1.1-9.8-2-11.5-6.9-1.7-5 .8-9.9 5.6-10.9zm-34.8 7.1c5.3-1.1 11 2.3 12.8 7.4 1.8 5.1-.9 10.2-6.1 11.4-5.3 1.3-11.2-1.9-13.1-7.2-1.9-5.3 1-10.5 6.4-11.6zM1066 688c-16.4-12.1-47.8-30.3-77-29 2.3 9.3 2 37.2-26 42-23.1 0-29.4-31.6-28-36-10.9 2.9-85 17.4-126 64-2.3 7.2-4 22-4 22l23 11s115-98.2 213-40c9.8-8.2 21-17 21-17s2.4-5.8 4-17zm-82-371c-4.9-4-7.2-14.7-25-30-15-12.9-40.2-43.6-105-12 41.7-1.3 86 45 86 45s-6.1 1.4 44-3zm-5-11 25 2s-10.7 9.4-10 20c-4.9-6.6-15-22-15-22zM627 933l192-112s8.9 2.1 20.7 5.8c2.9.9-156.2 96-186.7 114.3-1.9 1.2 2.9.5 2 1 8.3-4.4 193.9-104.1 203-109 10.7 3.3 22 7 22 7l-9 47-211 121 17-54-50-21.1z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full10002s"
			x1={911.486}
			x2={812.486}
			y1={126}
			y2={126}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full10002s)"
			d="M810 637c20 22.2 28.7 40.9 95 29-7 17.6-16 51-16 51l2 57-19 13-66-37s7.4-95.2 4-113z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full10002t"
			x1={1122.0367}
			x2={1026.0367}
			y1={79.1635}
			y2={88.1705}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10002t)"
			d="m1069 621-4 84s-18.7 7.9-25 17c-.3-5.1-22.6-17.5-67-20 26.4-25.2 16-41 16-41s56-11.3 80-40z"
			opacity={0.302}
		/>
		<path d="m681 955 211-120-12 45-219 131 20-56z" opacity={0.302} />
		<path fill="#9a9a9a" d="m880 880.5 12-45 56-33-4.3 16.4-12.7 5.6s-19.2 24.6-25 41c-26.8 16.1-26 15-26 15z" />
		<linearGradient
			id="ch-full10002u"
			x1={622.7023}
			x2={673.7023}
			y1={392.7668}
			y2={371.1188}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#404040" />
		</linearGradient>
		<path fill="url(#ch-full10002u)" d="m675 955-51-21 34 74 17-53z" />
		<linearGradient
			id="ch-full10002v"
			x1={988.9065}
			x2={855.2795}
			y1={542.462}
			y2={227.6569}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full10002v)"
			d="m1054 812-50 26s3.8 26.5-15 35c-20.9 8.5-27.7 5.6-34-3-17.6 9.8-42.3 29.8-71 10-28.5 16.7-93 52-93 52s-15.6 142.2-15 163 75.3 46.9 127 21c16.9-50.2 29-112 30-123s4-16 4-16c0 52.1 7.8 119.6 15 136s113.3-12 119-29c4.5-30.9 14.4-105.4-18-233 2.7-17 1-39 1-39z"
		/>
		<linearGradient
			id="ch-full10002w"
			x1={1143.3896}
			x2={1063.7577}
			y1={63.3035}
			y2={71.1394}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={0.938} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full10002w)"
			d="M1063 703c8.8-54.5 6-95 6-95s11.2 4 13-67c10.3 36.3 20.4 37.1 34 39 6.3 13 8.2 29.4 13 49 15.2 61.3 21.8 121.4-2.4 136.3 8.5-61.3-55.8-67.5-63.6-62.3z"
			opacity={0.302}
		/>
		<path fill="#f2f2f2" d="M801 487c10.8 3.7 45.3 17.8 39 31-58.3 20.6-94.8 22.4-100 6 21-27.8 47.8-33.5 61-37z" />
		<path fill="#f2f2f2" d="M1112 507c-42.6-31.5-46.5 9.1-93-49 42.7 4.7 75.7 2.5 93 49z" />
		<path
			fill="#d9d9d9"
			d="M1120 581c11.8-10.2 11.4-44.3-4.6-75.1-11.1-5.9-24.1-8.8-40.4-12.9 24.9 84.8 34.8 64.2 45 88z"
			opacity={0.4}
		/>
		<path
			fill="#ff2a00"
			d="m904 382 48 12s.1 11-11 22c-12.7-5-37-7-37-7v-27zm104-10-37 19s-3.7 17.2 10 21c12.9-4.2 29-15 29-15l-2-25z"
		/>
		<path fill="#737373" d="M1003 449c10.1.1 23 1 23 23-6.2 5.7-18.9 2.3-28-4 4-11.3 3.7-12.2 5-19z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M824.1 412.4c-.4 17 .9 36.8.9 40.6-9.5 7.7-59.4-1.3-56.5 46.5M720 644c-.4 16.3-3.4 83.2 0 106 12.1 70.3 31.7 74 57 94-54.1 34.4-154 90-154 90l36 75 133-77s-16.7 141.8-17 217c-1.6 15.1-13.7 14.8-15 40-2.4 15.5 3.4 27.1 3 40-.4 14.7-24.1 21.2-17 78 4.6 37 6.2 30.3 7 63 .2 8.5 1.3 15.4 1 22-5 2.1-5 13.1-5 21 0 15 7.6 17.4 6 27-2.1 12.7-1.7 25.1 0 39 5.2 42.9 26.8 81.9 41 96s34.3 19.6 59 14c14.8-3.3 54.1-7.6 47-51-1.8-11.2-2.9-30.8-11-50-.7-3.8 2.8-9.3 2-13-3.1-14.5-8.7-26.1-11-40-3.5-21.4-4.3-42.9-6-51-3.1-3.1-6.5-3.1-9.5-7.1 4.4-24.4 9.6-56.1 9.5-96.9 7.8-21.5 15.9-38.6 17-61 1.3-27-.5-42.8-5.7-56.8 23.1-58.7 46.6-159.3 51.7-186.2.5.1.7.2 1.1.4.9 16.1-2.1 39.9-1.1 53.6 1 14.3 6.1 28.4 9 46 2.3 13.9 6.9 31.7 9 57-12.8 15.8-14.3 25.8-16 34-2.6 12.4-.5 26-4 35-16 41.1-2.6 86 1 130.9 2.6 32.1-4 56-4 58.1-3.9 11.4-4.4 34.5-3 43s6.4 22.4 26 50 46.9 52.3 51 56c4.1 3.8 50.5 16.3 72 7s43.6-12.5 41-46c-1.2-15.1-1.3-37.4-31-60-.9-5.4 8.5-7.9 5-13-2.8-4.1-17-17.2-21-24-2.4-4.2-8.2-3.6-8-6 2.5-2.1 4.6-3.6 6-6-4.7-21.9-7.6-34.4-10-44-3.3-3.3-9.3-4.7-12.8-7.1 1.6-17.1 4.2-42.9 8.8-84.9 7.2-19.7 13.2-36.3 14.9-60.8 1.8-25.2-10.6-57.8-10.1-65.6 2.1-32.2 6.2-85.3 6.2-124.6 0-49-12.1-126.6-17-155-.1-12.1 2.1-32.5 3-39 25.4-12.6 52.3-30.8 66-40 33-22.9 23.8-65.2 22-81-1.4-12.1-18.4-80.3-25.8-109.5M1028 470c-10.3.3-30-1-30-1s11.6-26.8 11-57c-.1-4-.3-8-.6-12.1-.3-4.2 3.6-11 3.6-15.9 0-7.3-4.1-10-6-17 15.9 2.4 21.8-6.4 22.4-18 .3-6.8-3.5-11.9-8.4-18-2.3-6.7 1.1-15.1-6-21-7.8-3.4-25.7-3.8-34.9-4.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M837 907c-5.8 34-17.3 139.3-20 172-10.3 11.1-21 28-21 28l-18-3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M867 1378c-10.2-8-50.6-14.2-77 12-1.7 22.9-5 55-5 55s3.7-1.1 4 0c2.1 8.9 8 45 8 45s36.6-35.7 94-10m-10-45c-26-13-60.6-15.4-93 9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M752 1393c1.7-2.8 6.2-3 8.5-3 5.8 0 10.5 10.8 10.5 24s-4.7 24-10.5 24c-1.6 0-3.2-.9-4.6-2.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M939 1195c1.4 12.4 22.1 30.5 55 23m7-48c.7-8.5-3.8-29.2-14-37s-21.7-8.5-33 4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M950.5 1356c8 0 14.5 12.8 14.5 28.5s-6.5 28.5-14.5 28.5-14.5-12.8-14.5-28.5 6.5-28.5 14.5-28.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1051 1342c-10.1-4.9-45.2-4.8-66 20 2.7 29.3 7 54 7 54s3.7-.7 4 0c3 7.1 16 45 16 45s35.5-32.2 79-22m-26-42c-24.7-8.1-55.7 3.3-70 22"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M965.5 1161c3.6 0 6.5 6.3 6.5 14s-2.9 14-6.5 14-6.5-6.3-6.5-14 2.9-14 6.5-14z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1007 309c6.1 0 11 5.6 11 12.5s-4.9 12.5-11 12.5-11-5.6-11-12.5 4.9-12.5 11-12.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1015.5 333c6.3 0 11.5 7.2 11.5 16s-5.2 16-11.5 16-11.5-7.2-11.5-16 5.2-16 11.5-16z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M976 344s-31.2 4.6-44 6c-14.9-29.8-50.3-44.4-63-46-16.8-2.1-45 26-45 111m160.7-97c7.3 12.7 15.8 30.9 21.3 51"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M850 375c6.1 0 11 9.2 11 20.5s-4.9 20.5-11 20.5-11-9.2-11-20.5 4.9-20.5 11-20.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1018 1545 12-20s28.1 5.4 51-1c21.5-6.1 39-22 39-22" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m787 1570 12-16s22.4 23.6 63 13c21.5-6.1 39-22 39-22" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1060 1251c-28 32.1-55-8-55-8s-13-27-13-47 12-24.6 17-43c4.1-15.1 9.5-22.3 29-23 12.8-.5 15 4.3 23 23 1.4 21.8 12 35 12 35"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M814.8 493.4c11.5-4.9 23.4-11.3 30.2-16.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M999 467c-4.7 22.9-36.1 30.7-62.9 31.8-45.1 1.8-97.8-19.5-113.1-43.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M991 482c-3.5-20.1-12-41-12-41l-27 6s-5.8 31.8-21 53" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M799.8 617.4c14.9 29.7 48.8 64 137.3 46.6m132.9-46c-22.3 30.4-61.7 36.4-78 40m51 62c-28.6-20.9-120.5-37.2-218 41m9-226.9c34.1-2.1 84-5.1 84-5.1l14 16 27-2s3.6-9.1 6-16c15.3-2.7 80.3-16.9 106.2-22.7m12.1 38.8c-2 26.8-7.8 56.6-9.3 61.9-8.3 5.8-49 25.5-112 36s-113.5 1-125-5c-4.9-2.5-11.4-11.5-18-22.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m875 786-70-37s16.7-71.9 4-117" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M809 730c13.4-16.8 57.7-47.6 109.8-62.8m76-8.1c24.8 2.6 48.9 11.3 70.3 28.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M963 643c15.5 0 28 12.5 28 28s-12.5 28-28 28-28-12.5-28-28 12.5-28 28-28z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M963 657.4c7.5 0 13.6 6.1 13.6 13.6s-6.1 13.6-13.6 13.6-13.6-6.1-13.6-13.6 6.1-13.6 13.6-13.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M905 380c1.7.6 39 6.3 47 15 .9 10.8-6.6 16.3-10 19-22-.5-31-2.1-37-6-3.4-7.2-1.7-28.6 0-28zm100-8s-21.4 5.5-34 20c.3 15.9.5 16.1 6 19s26.2-8.7 30-13"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M624 932c3.2 1.6 54 22 54 22l-20 54m19-55 268-153-53-24-121 72m180-50-8 23"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m869 888 13-49-63-18m19-15s18.4 6 18.8 6.8m19.8 6.8c14.2 5 27.4 9.4 27.4 9.4l-13 48m-29-82s8 3.5 16.9 6.7m21.4 6.1c14.3 4.9 25.7 8.2 25.7 8.2s-3.7 14.6-6.5 26.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M866 838v-13l62-34m-18-7-64 35v13" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M810 752c-21.5-11-66.7 7.5-69 59" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M858 894s15.2-8.9 24-14c32.4 24.1 60.1-3.2 73-12 3.9 4.1 11 17.3 23 11 26.5-13.3 27.7-29.8 27-39m-42-56c-12.8.7-24.9 4-30 8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m959 785 104-82s66.1-4.4 62 63m-52-160c-1.8 5.2-4.3 13.2-4.1 17.9 2.6 58.7-7.8 74.2-6.9 78.1m-5 108c-8.3 3.3-52 28-52 28s.7-50.5-44-54"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1109 780c-1.2-21.9-5.6-65.4-65-62m-17 14c25-8.6 70.3-5.3 70 56"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M881 881c6.6-3.9 24-13 24-13s16.2-28.7 18-33 12.7-19.8 18-8c3.1-7.4 14.1-20.6 18-10 1.7-4 15.3-5.7 14 10s-9.5 35.2-19 43"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M865.5 883.6c55.6 43 36.3 89.4 74.5 89.4 58 0 66.1-65.8 89-110 7-13.5 16.1-17.4 23-21m-22-17c0 .2-12.5 32.7-26.3 58.3C986.8 914.7 969 942 969 942l-41 3-28-56m-40.7 4.1C817.4 917.1 789 934 789 934"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1035.8 536.8c4.9-.7 10.2 3 11.9 8.1 1.6 5.1-.9 9.8-5.7 10.6-4.9.8-10.3-2.7-12.1-7.9-1.7-5.3.9-10.1 5.9-10.8zm11.7 35.3c4.7-.9 9.7 2.3 11.3 7.1 1.6 4.8-.8 9.6-5.4 10.7-4.7 1.1-9.8-2-11.5-6.9-1.7-5 .8-9.9 5.6-10.9zm-34.8 7.1c5.3-1.1 11 2.3 12.8 7.4 1.8 5.1-.9 10.2-6.1 11.4-5.3 1.3-11.2-1.9-13.1-7.2-1.9-5.3 1-10.5 6.4-11.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M893 1219c.3-12.9-10-34.6-15-46-6.8-15.3-14.9-26-26-26-14.1 0-29.4 12-34 33-3.3 15.3-12 8.7-12 35 0 9.6 5.9 21.5 12 35 6.2 13.6 1.9 26.8 28 29 8.1.7 33-1.8 39-23"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M814 1188c1.4-12.1-6.7-53.5-41-34m-11 64c3.8 16 41.1 30.3 55 27"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M781.5 1177c4.1 0 7.5 6.5 7.5 14.5s-3.4 14.5-7.5 14.5-7.5-6.5-7.5-14.5 3.4-14.5 7.5-14.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M809 606c-7.7 14.6-38.4 40-74 40s-31-24.6-31-35 2-67 29-91 50.1-29 66-29 48.8 19.6 42 27c-4.9 5.4-9.1 5.7-11 6-.7.1-1 1-1 1s12.2 39 16.7 53c4.3 13.6 4.1 23.3-9 29s-20.5 11.5-27.7-1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1119 595c7.3-9.8 19.3-30 14-49s-11.6-48.4-53-75c-16.9-9.9-56.6-14.2-72-14-.3.4.3 1.1.4 1.2 29.8 13.4 36.2 29.2 49.6 31.8 3.9 1.2 6.5 1.9 10 3 2 10.4 8.1 32.5 14 48 2.1 5.4 6.4 22.8 11 31 4.4 8 15.9 8.2 25 7 4.5-.7 9-1 9-1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1066 493c9.2.9 37.3 1 54.2 18m-27.9-14.7c6.7 9.3 19.9 32.7 30.7 81.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M830 524c-6.3 1.8-66 12.7-74 12s-16.9-7.2-22-13m2 124c-.8-27 .1-67.7 17-109m32-3c4.5 11 25 72 25 72"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M972 346s9.3-23 12-31c-7-14-18-34.7-39-44-44.8-19.8-114.3-11.8-130 63-4.8 23.1-5.7 60.9 9 90"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m932 351 11-27" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m984 317-44 6s-27.2-27.6-53-39c-25.1-11.1-56.6-6.7-72 54"
		/>
	</svg>
);

export default Component;

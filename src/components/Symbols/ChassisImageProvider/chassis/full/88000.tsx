import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M210.7 1146.3s-2.5-54.4-3.6-73.4c-1.1-19 14.7-52.5 60.2-75.8C223 980.2 87.4 888.5 85.5 845.4c-2.3-63.7 39-95.6 77-124C262 658.8 378 615.7 417.7 596.3c35.2-14.3 104.7-38.5 104.7-38.5v-8.4c0-3.1 11.8-48.1 172.1-48.1s179.3 37.3 179.3 44.5v6s139.6 7.7 322.5 40.9c51.7 37.1 77.2 54 102.3 68.6 78.4 12.9 122.8 39.7 122.8 39.7s36.6 21.1 18.1 53c-10.4 17.7-142.6 89.1-145.6 97.5 373.1-28.4 412.8 84.5 415.2 97.5 1.4 9 1.9 41.8 0 65 7.2-1 58.5-5.5 74.6-3.6 13.8 3 39.9 13.3 4.8 97.5s-173.8 82.5-245.5 68.6c-71.7-13.9-110.4-12.9-152.8-10.8-42.4 2-416.5 45.3-524.7 81.8-78.5 35.7-33.4 51.3-146.8 79.4-144.3 35.9-398-49.4-408-96.3-105.7-33.7-100-84.3-100-84.3zm72.2-65 178.1-1.2-1.2-9.6-20.5-36.1c0-.1-92 14.5-156.4 46.9zm-24.1 34.9c19.7 13.3 102.1 40.4 169.7 40.9 17.8-19.1 32.5-34.9 32.5-34.9v-19.3h-15.6l-1.2 10.8c-.1.1-141.4 1.7-185.4 2.5zm402 20.4-97.5-21.7s.2 18.8 0 24.1c-2.3 6-15.4 14.2-26.5 19.3 45.2 2.3 124-21.7 124-21.7zm-87.9-39.7 13.2-4.8 115.5 32.5s52.3-23 61.4-33.7c-3-6.7-23-26.8-50.5-33.7-28.8 5.4-149.2 32.5-149.2 32.5v4.8l9.6 2.4zm-8.4-31.3c10.3-1.8 107.1-20.5 107.1-20.5s-65.5-17.4-125.2-16.8c16.9 8.6 16.9 6.7 18.1 37.3zm905-120.3 34.9-10.8s-7-6.4-32.5-8.4c-1.2 7.5-2.4 19.2-2.4 19.2zm101.1 1.2-78.2 30.1 127.6 28.9 33.7-12c0-.1-14.1-27.2-83.1-47zm-99.9 77c41.1 6.8 78.2-3.6 78.2-3.6l-79.4-8.4s.8 7.5 1.2 12zm-96.3-71-43.3-31.3s-48.2-2.2-115.5 28.9c28.2 15 48.1 20.5 48.1 20.5l92.7-4.8 4.8 6h15.6l-2.4-19.3zm-7.2 53 9.6-15.6-60.2 1.2 50.6 14.4z"
		/>
		<path fill="#d9d9d9" d="m1297.4 662.5-101.1-69.8-19.3 24.1 78.2 126.4 42.2-80.7z" />
		<path
			fill="#d9d9d9"
			d="M885.8 566.2c16.5 0 180.4 22.8 292.5 53 20.6 33.6 75.8 122.8 75.8 122.8s-168.3-134.6-368.3-171c-.3-1.5-16.5-4.8 0-4.8zM351.5 830.9c27.3-83.8 77-170.4 240.7-243.1-5.6-1.2-15.6-2.4-15.6-2.4s-156.4 60.1-238.3 110.7c.9 19.9 9.2 147.1 13.2 134.8z"
		/>
		<path
			fill="#dbdbdb"
			d="M120.4 781.6c20.9-18.5 115.5-99.9 115.5-99.9L337 694.9l13.2 144.4c.1.1-163.5-41.8-229.8-57.7z"
		/>
		<linearGradient
			id="ch-full88000a"
			x1={547.2079}
			x2={1101.9092}
			y1={793.1916}
			y2={1458.728}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={0.469} stopColor="#d9d9d9" />
			<stop offset={0.549} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full88000a)"
			d="M890.6 1175.1c-61.2 27.8-46.2 46.2-96.3 78.2-22.3-19.2-75.7-55.7-141.6-101.5 246.8-60.8 196.8-234.6-326.8-249.2-127.5-7.6-201.8-9.5-229.6-36.8-30.1-31.4 3.4-104.7 65-143.2 13.7-10.6 44.9-27.6 67.4-38.5-23.8 25.4-95.4 83.2-109.5 97.5 71 19.7 200.3 50.4 229.9 56.6C379.8 721 459.4 651.3 592.2 587.9c72 12.6 167.4 7.8 195.8 7.2 9.3 8.8 14.6 16.8 23.2 24.1-72.2 45.4-92.4 120.4 37.3 247.8 97.9 68.6 257.3 171.9 309.3 155.3 55.4 40.5 97.1 67.8 136 90.3-67 7.6-353.3 49.9-403.2 62.5z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full88000b"
			x1={1301.6879}
			x2={1471.3527}
			y1={699.4393}
			y2={941.7463}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.23} stopColor="#f2f2f2" />
			<stop offset={0.469} stopColor="#d9d9d9" stopOpacity={0.8} />
			<stop offset={0.844} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full88000b)"
			d="m1155.4 1021.1 117.9 95.1s153.8-19.3 190.2-15.6c47.6.4 219.8 67.3 281.6 34.9-36.5-35.1-132.4-111.9-132.4-111.9S1324 1053 1141 902c5.6 19.3 55.5 116.6 14.4 119.1z"
		/>
		<linearGradient
			id="ch-full88000c"
			x1={961.7628}
			x2={1172.4382}
			y1={1032.2096}
			y2={1333.0851}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.389} stopColor="#f2f2f2" />
			<stop offset={0.55} stopColor="#d9d9d9" />
			<stop offset={0.844} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full88000c)"
			d="M1124.1 868.3c35.8-58.2 318-89.8 318.9-143.2-16-31.8-63.3-47.7-144.4-63.8-12.4 26.3-36 75.5-44.5 81.8-81.8-65.9-238.3-157.6-392.3-175.7-9.5 7.6-21.5 18.2-65 26.5 10.3 14.5 16.8 25.3 16.8 25.3s70.6-34.7 148 30.1c77.4 64.7 145.8 184.3 162.5 219z"
			opacity={0.702}
		/>
		<path
			d="M234.7 680.5c95.6-51.6 257-114 286.4-121.6 14.2 12 19.3 14.2 55.4 26.5-117.7 44.6-174.6 77.2-239.5 112-24-5-75.5-13-102.3-16.9zm637.9-128.8c151.3 6.8 281.7 37.7 323.7 39.7-6.5 9.2-13.5 18-19.3 26.5-150.6-31.2-250.9-53.1-311.7-51.8 3.9-4 6.4-9.4 7.3-14.4z"
			className="factionColorSecondary"
		/>
		<path
			d="M1025.4 711.8c18.7 4.5 137.7 47.2 182.9 108.3-27.3 12.2-76.7 30.6-84.2 48.1-13.2-23.4-54.2-100-98.7-156.4zm-285.2 272c7.4-14.2 33.6-74.8 113.1-111.9-17.8-16-90.3-88.1-97.5-148-33.4 8.3-194.4 55.5-261.2 192.6 65.3 1.4 193.7 24.1 245.6 67.3z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full88000d"
			x1={1047.62}
			x2={1229.3488}
			y1={876.1677}
			y2={1057.8965}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#9a9a9a" />
		</linearGradient>
		<path
			fill="url(#ch-full88000d)"
			d="M1124.1 865.8c22.3-40.2 287.9-92.5 320.1-133.6 2.8 45-72 56-148 119.1-22.2 0-116.8 18.3-142 62.6-18.8-14.9-19.1-14.6-30.1-48.1z"
		/>
		<linearGradient
			id="ch-full88000e"
			x1={959.3446}
			x2={315.4711}
			y1={948.288}
			y2={982.0319}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#3e3e3e" />
			<stop offset={1} stopColor="#989898" />
		</linearGradient>
		<path
			fill="url(#ch-full88000e)"
			d="M93.9 864.6c24.5 29.8 222.7 39 281.6 40.9 58.9 1.9 268.8 11.8 362.3 74.6-17.8-6.4-95.7-32.5-196.2-32.5s-186 11-276.8 49.3C230.5 978 132.4 926 93.9 864.6z"
		/>
		<path
			fill="#bfbfbf"
			d="m232.3 1089.7 6-9.6h46.9s29.3-23.9 154-45.7c-7.4-16.9-14.4-33.7-14.4-33.7l27.7-10.8 28.9 38.5s33-4.1 73.4 1.2c41.9 2.1 95.7 7 110.7 14.4 18.4-3.1 66.2-12 66.2-12l22.9 4.8v9.6l-39.7 9.6s45 22 49.3 37.3c42.8-31.1 41.7-131.4-227.5-136-180.1-1.3-349 61.9-304.4 132.4zm982.1-138.4c19.4-14.6 87.4-27.7 115.5-30.1-15.6-12.4-31.3-22.9-31.3-22.9v-10.8l30.1 6 46.9 32.5s4.9-13.2 49.3-13.2c35.9 0 43.3 13.2 43.3 13.2l42.1 7.2 72.2-24.1 21.7 14.4v7.2l-34.9 14.4s80.5 28.4 84.2 49.3c103-39.1-10.8-134.4-226.3-137.2-79.5-1-232.2-3.7-261.2 62.6 16.1 14.1 33.2 22.6 48.4 31.5zM533.2 551.7c0-14.5 34-40.9 157.7-40.9 52.5 0 173.3 10.6 173.3 37.3-.3 15.7-42.2 32.9-75.8 34.9-9.6-2.1-43.1-10.7-51.8-13.2 15-3.4 68-11.1 92.7-14.4-5.2-2.6-7.5-4.9-12-6-29.2 3.4-76.7 9.9-90.3 12-.4-6.3-.1-8.8 0-18.1-3.5-9-17.6-9.6-30.1-9.6-13.6 0-29.8 10.2-34.9-2.4-9.9-.1-18.6 1.2-24.1 3.6 5.5 3.6 19.8 15.7 28.9 28.9-41.4.7-103.7 3.7-121.6 1.2-7.5-1.1-12-8-12-13.3zm-291.3 551.2h202.2v-9.6l16.8-1.2 1.2 10.8h-14.4l-3.6 12-185.3 1.2c0 .1-11.9-4.1-16.9-13.2zm232.3 57.8 42.1-38.5h6v12l-34.9 27.7-13.2-1.2zm33.7-54.2v13.2l-14.4-2.4 14.4-10.8zm37.3-6c1.3 0 134.8 31.3 134.8 31.3l-19.3 6-115.5-27.7c0 .1-1.3-9.5 0-9.6zm18.1-19.2 192.6-43.3v7.2l-192.6 45.7v-9.6zM461 1122.2V1050s4 13.2 51.8 13.2 50.5-18.1 50.5-18.1v46.9l-18.1 8.4v10.8l18.1 3.6v25.3s-12 17.5-38.5 20.5c-23.1.9-34.9 1.2-34.9 1.2l32.5-28.9v-9.6l-28.9-6 9.6-8.4-22.9-4.8-19.2 18.1zm837.6-234.7 78.2 55.4v12l-78.2-57.8v-9.6zm-14.4 91.5h104.7v10.8h-78.2l-26.5-10.8zm142 7.2V997l-9.6 19.3-12-3.6 21.6-26.5zm44.5-26.5 14.4 9.6 119.1-45.7v9.6L1490 976.6l-19.3-3.6v-13.3zm-13.2 25.3 19.3 1.2-9.6 10.8 108.3 18.1-20.5 6-84.2-9.6v12s-44.6-3.5-55.4-8.4c4.8-7.9 12-18.1 12-18.1v-13.2h-21.7l-4.8 8.4-10.8-1.2-1.2-9.6-13.2-13.2v-39.7s11.3 14.4 48.1 14.4 44-4.5 46.9-8.4c-.3 13.1 0 36.1 0 36.1l-13.2 14.4zM594.6 575.8c.9-1.1 65-1.2 65-1.2s2.3-5 2.4-3.6c.1 1.2 9.8-1.3 9.6 1.2-.1.9 1.4-28.3 1.2-26.5 0 .1.2.2.6.2 17.8 8.2 39.8 2.7 53.5-.2-.1 1.6-.8 16.8-1.2 28.9-1.2 1.1 38.2 8.5 34.9 12-1.9 2-46.3.9-48.1 1.2-1.1.2-.3-8.8-1.5-8.7-5 .7-10.2.8-14.2.2-2.3-.3.8-4.5-1.2-4.8-4.2-.6-6.3-.3-8.4 0-3 .3 1 10.7-1.7 10.8-8.7.4-15.1 0-15.2 0-3.6.6-9.6 0-9.6 0s-62.1-4.4-66.1-9.5z"
		/>
		<linearGradient
			id="ch-full88000f"
			x1={659.5588}
			x2={634.2853}
			y1={1366.4629}
			y2={1366.4629}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4f4f4f" />
			<stop offset={0.98} stopColor="#bfbfbf" />
		</linearGradient>
		<path fill="url(#ch-full88000f)" d="M634.3 542.1V565l25.3-1.2-25.3-21.7z" />
		<linearGradient
			id="ch-full88000g"
			x1={832.8631}
			x2={760.653}
			y1={1346.6019}
			y2={1346.6019}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.02} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#4f4f4f" />
		</linearGradient>
		<path fill="url(#ch-full88000g)" d="M832.9 562.6v9.6s-39 12.4-50.5 12c-9-3-21.7-10.8-21.7-10.8l72.2-10.8z" />
		<linearGradient
			id="ch-full88000h"
			x1={434.5551}
			x2={415.2991}
			y1={902.4174}
			y2={901.7448}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full88000h)" d="m420.1 999.4 19.3 33.7-19.3 3.6v-37.3z" />
		<linearGradient
			id="ch-full88000i"
			x1={765.5344}
			x2={721.0049}
			y1={854.6273}
			y2={853.0724}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#363636" />
		</linearGradient>
		<path fill="url(#ch-full88000i)" d="m713.7 1056 44.5 27.7v-36.1l-44.5 8.4z" />
		<linearGradient
			id="ch-full88000j"
			x1={559.8341}
			x2={538.1711}
			y1={787.5908}
			y2={786.8351}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full88000j)"
			d="M541.6 1111.4s0 24.2 1.2 43.3c8.3-.2 20.5-15.6 20.5-15.6v-25.3l-21.7-2.4z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full88000k"
			x1={532.288}
			x2={498.59}
			y1={773.983}
			y2={772.8061}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#363636" />
		</linearGradient>
		<path
			fill="url(#ch-full88000k)"
			d="M491.1 1161.9c8.3-.2 33.7-2.4 33.7-2.4v-27.7s-15.4 9.8-33.7 30.1z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full88000l"
			x1={1435.0636}
			x2={1419.418}
			y1={913.5282}
			y2={912.9818}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#363636" />
		</linearGradient>
		<path fill="url(#ch-full88000l)" d="M1429.8 995.8v21.7l-15.6-2.4 15.6-19.3z" />
		<linearGradient
			id="ch-full88000m"
			x1={1470.2244}
			x2={1452.1719}
			y1={913.5237}
			y2={907.1524}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full88000m)" d="M1453.9 994.6v26.5l18.1 2.4v-12l-8.4-1.2v-14.4l-9.7-1.3z" />
		<linearGradient
			id="ch-full88000n"
			x1={1324.9152}
			x2={1291.2172}
			y1={1014.4106}
			y2={1002.5164}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#141414" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full88000n)" d="M1296.2 897.1V926s22-4.6 33.7-3.6c-6.5-7.2-33.7-25.3-33.7-25.3z" />
		<linearGradient
			id="ch-full88000o"
			x1={1616.9229}
			x2={1582.0212}
			y1={976.6974}
			y2={964.3796}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#3b3b3b" />
		</linearGradient>
		<path fill="url(#ch-full88000o)" d="m1607.9 932-34.9 13.2s23.2 14.6 34.9 15.6c-.4-10.6 0-28.8 0-28.8z" />
		<path
			fill="#bfbfbf"
			d="M313 1214.9c11 25.5 202 91.5 324.9 91.5 159.2 0 190.4-104.6 241.9-125.2 61.8-24.7 239.6-43.8 332.2-57.8s230.7-28.3 268.4-20.5c37.6 7.8 156.2 40.9 221.4 40.9s95.1-66.2 95.1-66.2-7.2 105.9-169.7 105.9c-109 0-95.2-19.3-203.4-19.3s-445.8 49.6-530.7 74.6c-109.3 32.2-60.5 84.8-255.1 96.3-82.9 5-355.8-56-325-120.2z"
		/>
		<linearGradient
			id="ch-full88000p"
			x1={622.6763}
			x2={314.1515}
			y1={768.3224}
			y2={768.3224}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#bebebe" />
		</linearGradient>
		<path
			fill="url(#ch-full88000p)"
			d="M208.2 1072.8c3.9 49.5 97 110.5 308.1 97.5-63.2 26.2-217.8 10.3-207 60.2-53.5-12.4-97.3-55.9-98.7-75.8-1.4-20-3.8-63.2-2.4-81.9z"
		/>
		<linearGradient
			id="ch-full88000q"
			x1={1679.2823}
			x2={1592.6301}
			y1={925.9811}
			y2={925.9811}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bebebe" />
			<stop offset={1} stopColor="#4f4f4f" />
		</linearGradient>
		<path
			fill="url(#ch-full88000q)"
			d="M1623.6 1021.1c19.3-5.1 65.8-15.6 84.2-54.2 1.7 5.5 2.4 48.1 2.4 48.1s-53.9 3.5-86.6 6.1z"
		/>
		<linearGradient
			id="ch-full88000r"
			x1={685.6152}
			x2={1102.6563}
			y1={891.1704}
			y2={1183.1857}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#0e2533" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full88000r)"
			d="M754.3 715.2c-4-33.9 23.8-95.1 81.7-104.4 73.9-17.3 144.6 51.3 153.3 60.1 58.9 59.7 132.8 189.5 142 211.8 17.1 47.9 73.6 141.5 10.6 141.5-51.5-5.1-121.8-49.2-183.9-83.7-63.5-39.8-122.2-80.8-137.9-103.2-44.7-60.6-61-82.6-65.8-122.1z"
		/>
		<linearGradient
			id="ch-full88000s"
			x1={863.1736}
			x2={1034.527}
			y1={830.5743}
			y2={1246.7212}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#092130" />
			<stop offset={0.699} stopColor="#dae3f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full88000s)"
			d="M754.3 715.2c-4-33.9 23.8-95.1 81.7-104.4 73.9-17.3 144.6 51.3 153.3 60.1 58.9 59.7 132.8 189.5 142 211.8 17.1 47.9 73.6 141.5 10.6 141.5-51.5-5.1-121.8-49.2-183.9-83.7-63.5-39.8-122.2-80.8-137.9-103.2-44.7-60.6-61-82.6-65.8-122.1z"
		/>
		<radialGradient
			id="ch-full88000t"
			cx={2416.0408}
			cy={2585.8997}
			r={208.3078}
			gradientTransform="matrix(0.8025 -0.5967 -1.1764 -1.5821 1989.1213 6407.1978)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#0e2533" stopOpacity={0} />
			<stop offset={0.4949} stopColor="#dae3f2" stopOpacity={0} />
			<stop offset={0.8645} stopColor="#e6efff" />
			<stop offset={1} stopColor="#dae3f2" />
		</radialGradient>
		<path
			fill="url(#ch-full88000t)"
			d="M754.3 715.2c-4.1-33.9 23.8-95.1 81.7-104.3 73.9-17.3 144.7 51.3 153.2 60.1 59 59.7 132.7 189.6 142 211.8 17.1 47.9 73.7 141.5 10.6 141.5-51.5-5.2-121.8-49.2-183.9-83.8-63.5-39.8-122.2-80.8-137.9-103.1-44.5-60.6-60.9-82.6-65.7-122.2z"
		/>
		<path
			fill="#ccc"
			d="M761.9 741.9c9.4-12 27-22.8 33.7-25.3 17 23.8 35.1 49.4 68.6 79.4-2.3 26.1-7.2 75.8-7.2 75.8s-44.1-37.2-61.4-65c-17.3-27.8-30.7-50.4-33.7-64.9z"
		/>
		<path
			fill="#999"
			d="M998.9 755.1c10 9.7 59.5 74.3 91.9 142.4-16.5 8.9-110.2-22.6-136-74.4-4.2-50.1 27.1-61.5 44.1-68zm75.4 24.5c8.4 16.3 22.5 38.8 51.7 96.4-2.1 3.8 1.9 13.8-21.1 17.8-24.4-44.1-38.9-70.5-51.6-89.1-15.6-22.9-23.9-34.3-37.5-51.8 21.9-4 49.1 17.4 58.5 26.7z"
		/>
		<radialGradient
			id="ch-full88000u"
			cx={906.6256}
			cy={7635.2554}
			r={431.3826}
			gradientTransform="matrix(0.5233 0.8522 -0.21 0.129 2125.844 -905.2903)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1192} stopColor="#535353" />
			<stop offset={0.2091} stopColor="#757575" />
			<stop offset={0.3133} stopColor="#999" stopOpacity={0.5} />
			<stop offset={0.4001} stopColor="#fff" stopOpacity={0.4} />
			<stop offset={0.4924} stopColor="#a6a6a6" />
			<stop offset={0.8179} stopColor="#666" />
			<stop offset={0.9703} stopColor="#333" />
		</radialGradient>
		<path
			fill="url(#ch-full88000u)"
			d="M998.9 755.1c10 9.7 59.5 74.3 91.9 142.4-16.5 8.9-110.2-22.6-136-74.4-4.2-50.1 27.1-61.5 44.1-68zm75.4 24.5c8.4 16.3 22.5 38.8 51.7 96.4-2.1 3.8 1.9 13.8-21.1 17.8-24.4-44.1-38.9-70.5-51.6-89.1-15.6-22.9-23.9-34.3-37.5-51.8 21.9-4 49.1 17.4 58.5 26.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M310.5 1230.5c10 46.9 263.7 132.2 408 96.3 113.4-28.2 68.4-43.8 146.8-79.4 108.2-36.5 482.3-79.8 524.7-81.8 42.4-2 81.1-3.1 152.8 10.8 71.7 13.9 210.4 15.6 245.5-68.6 35.1-84.2 8.9-95.6-7.2-97.5s-65 2.6-72.2 3.6c0-23.2 1.4-54.8 0-63.8.7-11.8-42.1-127.1-415.2-98.7 3-8.4 134.1-78.5 144.4-96.3 7.8-13.3 8.7-21 3.6-33.7-16.1-33-84.6-47.4-142-60.2-37-22.1-78.5-52.9-103.5-69.8-17.6-2.3-209.4-31.3-209.4-31.3l-113.1-9.6v-4.8c0-7.3-19-44.5-179.3-44.5s-172.1 45-172.1 48.1v8.4c-127.6 41.5-274.9 116.5-283.6 120.8-23.3 11.5-49.1 25.7-76.2 42.9-38 28.3-78.1 60.3-75.8 124C88.6 888.5 223 980.2 267.2 997c-45.5 23.3-61.3 56.8-60.2 75.8 1.1 19 3.6 73.4 3.6 73.4s-5.6 50.7 99.9 84.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M740.2 982.6c17-37.8 53.7-84.7 114.6-111.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M495 912.7c17-37.7 109.9-158.6 259.6-188.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1209.6 821.3c-38.1-50.7-119.6-91-186.5-110.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1256.5 743.1c-46.7-35.8-200.9-147.9-392.3-175.8-.2-.9.9-2.3 1.9-2.3 105.9 3.4 300.4 50.4 310.9 52.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1197.5 591.4-20.5 25.3 79.4 126.4 43.3-81.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1198.7 601.1 89.1 62.6-32.5 62.6-68.6-108.3 12-16.9z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={4}
			d="m1254.1 708.2-55.4-84.2s.3-2.1 1-1.5c8.2 7.7 62.8 68.9 62.8 68.9l-8.4 16.8zm-49.4-93.9 71 50.5-6 10.8s-57.8-52.5-65.4-60.5c-.5-.5.4-.8.4-.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M338.2 694.9c26.4-15.4 129.3-69.6 243.1-109.5 4.2 1.4 9.4 2 13.2 2.4C408.7 664.3 359.1 786.9 349.1 843"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m136 778 202.2 49.3-9.6-124-89.1-13.2L136 778z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m168.5 770.8 78.2-67.4s1.5-.7 2 0c-3.4 9.8-32.1 78.3-32.1 78.3l-48.1-10.9zM313 714.2l7.2 91.5-40.9-9.6s29-73.3 32.2-81.2c.2-.5 1.5-.7 1.5-.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m118 782.8 119.1-101.1 99.9 14.4 12 143.2-231-56.5z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1711.4 1013.9c-20.8 1.3-95.2 12-146.8 12" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M87.9 855c46.7 85.1 535.5-3.4 695.6 160.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M313 1237.7c-20.9-51 53.8-36.6 302.1-79.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M313 1214.9c20.5 33.2 269.9 110.2 375.5 87.9 135.2-28.7 136.8-108.1 204.6-127.6s496.9-80.6 569.3-74.6c72.3 5.9 187.3 55.9 263.6 40.9 52.8-10.4 74.9-71.7 75.8-74.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1122.9 867c35.3-50 297.5-93 321.3-136" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M977.3 661.3c94.7 85.9 193.8 287.4 193.8 337s-87.2 11.6-110.7-2.4-177.6-93.7-226.3-143.2-80.6-110.5-80.6-143.2c0-32.7 25.7-101.1 109.5-101.1 39.4-.1 83.9 25.3 114.3 52.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M761.9 741.9c9.4-12 27-22.8 33.7-25.3 17 23.8 35.1 49.4 68.6 79.4-2.3 26.1-7.2 75.8-7.2 75.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M998.9 755.1s61.1 72 93.9 140.8c-39.9 14.7-108.2-37.7-127.6-55.4-28-25.5-2.6-82.2 33.7-85.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1124.1 870.7c4 14.3-5.1 21.9-18.1 24.1-18.8-41-73.1-122.6-91.5-143.2 15.5 2 38.2 4.5 54.2 22.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M704.1 509.6c26-.3 67 3 101 10.1 32.2 6.8 58.1 17.2 59.1 27.2 1.9 18.6-46.3 42-167.3 39.7-82-1.5-164.9-15.5-164.9-37.3 0-19.6 71.6-38.7 172.1-39.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M521.2 549.3c0 21.8 43.4 46.9 215.4 46.9 109.9 0 136-34.2 136-44.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M725.4 542.4c0 9.5-54.6 10.7-54.6 0 0-10.8 54.6-11 54.6 0z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M705.4 542.1c0 1-13.4 1.1-13.4 0s13.4-1.1 13.4 0z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M670.4 542.1v43.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M724.5 542.1v30.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m670.4 540.9-9.6-9.6-20.5 3.6s22.9 23.9 27.9 29.1c.6.6-.3 1-.3 1l-9.6-1.2-20.5-21.7v-8.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M668 563.8 548.8 565s5.9 4.1 22.9 8.4c16.9.6 87.9 1.2 87.9 1.2l1.2-3.6h9.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M659.6 574.6v12" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m684.8 584.2 1.2-9.6h8.4l1.2 3.6 15.6 1.2v6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m725.8 567.4 10.8 2.4 7.2 1.2s36.6 8.9 44.5 12c-5.1 1.2-18.1 1.2-18.1 1.2s-42.8-9.2-49.6-10.6c-.6-.1.3 1 .3 1l4.8 2.4v8.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m725.8 561.4 91.5-10.8s10.7 3.6 13.7 4.6c.5.2-.4.3-.4.3s-79.7 12-94.1 14.1c-1.4.2-1 .3-1 .3l16.8 4.8 78.2-12v-4.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1163.8 922.4c29.2-66.2 176.9-69 281.6-65 124.7 4.3 246.7 45.2 246.7 99.9 0 46.8-157.9 68.3-184.1 67.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1212 950.1c18.9-11.3 65.4-25.6 119.1-28.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1467.1 924.8c17.3 1.6 34.4 5 40.9 7.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1569.9 946.6c25.8 7.3 72.7 26.5 83.8 44.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1376.8 944.1s-67.6-48.8-77.1-55.8c-.7-.5.1-.8.1-.8l28.9 6 48.1 33.7v16.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1298.6 887.5v9.6s69.7 52.3 76.5 57.4c.3.3.5 14.8.5 14.8v-28.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1376.8 926c0-16.9 92.7-18.9 92.7 4.8 0 19.4-92.7 15.8-92.7-4.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1423.3 922.4c14.3.4 25.8 3.1 25.7 6.1-.1 3-11.7 5.1-26 4.8-14.3-.4-25.8-3.1-25.7-6.1.1-3 11.7-5.1 26-4.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1257.8 970.5 98.5-4.7 4.8 6h15.6l8.4 7.2-101.2 1.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1368.4 1005.5 8.4-16.8 24.1 2.4 4.8-7.2h19.3l-20.5 28.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1469.5 945.3 113.1-34.9 19.3 13.2-116.7 45.7-15.6-9.6v-14.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1468.3 973 154 32.5s-23.7 7.8-40.9 10.8c-23-4.2-115.5-18.1-115.5-18.1l9.6-10.8-16.8-3.6 9.6-10.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1604.4 923.6v8.4l-114.2 45.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1456.3 983.8v9.6s8.1 1.5 12 2.4c.5.3-4.8 2.4-4.8 2.4v12l95.1 10.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1426.2 982.6v13.2l-12 19.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1469.5 929.6v42.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1469.5 1010.3v10.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1312.8 989.8c-.9.1 76.1 0 76.1 0v-9.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1297.4 851.4c-52.5 4.6-118.1 20.6-142.7 60.4-.2.3-12.4-9-12.6-8.7.2.2 12.5 10 12.7 10.1 70.5 56 227.3 112.6 390.5 112.6 131.3 0 164.5-43.8 164.5-74.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M263.6 998.2c60.8-29.1 160.1-51.8 253.9-51.8s278 18.8 278 103.5c0 76.6-173.3 120.4-314.1 120.4-292.2 4.2-320.3-116.9-217.8-172.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M223.9 1070.4c0-70.3 162.3-114.3 304.5-114.3 149.7 0 257.5 40.6 257.5 98.7 0 56.6-174 105.9-276.8 105.9s-285.2-19.6-285.2-90.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M440.5 1033.3c-71.6 11-131.4 31.7-161.3 46.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M539.2 1028.3c44.2.7 104.1 7.3 130.8 14.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M711.3 1054.8c13.9 6 49.8 24.8 50.5 36.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M459.8 1077.7s.9-3 .3-4.2c-5.6-11.2-36.4-72.9-36.4-72.9l27.7-7.2 30.1 36.2s-21.7 3.6-21.7 21.5c0 13 .6 19.8 1 22.6.1 1.1-1 4-1 4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M459.8 1102.9v19.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M459.8 1080.1H237.1l-6 9.6s-.7 5.6 8.4 12c11.7.7 204.6 1.2 204.6 1.2l1.2-9.6 14.4-1.2v-12z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m562.1 1064.4 168.5-31.3 24.1 4.8s.4.2-1.2.5c-22.1 4.9-191.4 42.8-191.4 42.8v-16.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m557.3 1095.7 13.2 2.4 15.6-6 115.5 31.3s-14.9 7.1-20.5 8.4c-13.4-3.7-122.3-28-135.8-31-.9-.2-.2-.3-.2-.3l12.2-4.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m426.1 1157.1 54.2-53 22.9 4.8-9.6 8.4 25.3 4.8-45.7 38.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M522.4 1119.8v13.2l-32.5 26.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M461 1093.3v8.4h-15.6v12H254s-12.4-8.5-14.4-10.8h204.6l1.2-9.6H461z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M544 1100.5v9.6l117.9 26.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M562.1 1080.5v10.4l192.6-45.7v-7.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M510.3 1160.7c15 0 47.9-7.6 53-22.9v-22.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M562.1 1069.2v-27.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M512.7 1027.1c-22.2 0-50.5 3.1-50.5 20.5 0 12.4 28.2 14.4 51.8 14.4 23.5 0 48.1-5.9 48.1-20.5s-31.1-14.4-49.4-14.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M511.5 1038.2c14.3 0 25.9 3.1 25.9 7 0 3.8-11.6 7-25.9 7-14.3 0-25.9-3.1-25.9-7s11.6-7 25.9-7z"
		/>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#d9d9d9"
			d="M567 662c-1.8-8.5 32 88 32 88s7.8 10.1 16 21 24 31 24 31 3 11.3 28 8 79-10 79-10 9.3-5.2 9-22-1-22-1-22l17-4s-3 16-3 40 2.3 52.3 8 60c-2.7 14.6-41 85-41 110s14.2 35.5 20 39c-6.9 14.3-23.3 35.8-25 72-10.9 19.3-13 32.7-13 44 0 40.3 51.2 175 176 175 33 0 51-22 51-22l20-5-2 11s24.3 7.4 48-13 42.7-49.7 45-54c1.5 1 2 2 2 2s28.3-39.5 35-81c-1.9-.3-4-1-4-1s14.8-42.3 5-86c4.5-5 7.1-15.3 6-25-1.1-9.7-9.4-47.8-27-68s-56.4-56.4-72-67-35.1-68.7-39-80-15-34-15-34l3-4s31.4 36 61 36c17.1 0 26-5 26-5l203 146 45-73-192-153 1-67s-18.2-16-40-27c.2-4.8 0-13 0-13l-10-9s-20.8-6.5-36 0c-4.6 3.3-10 8-10 8v2s-65.3-14-92-17c-17.4-8.5-33.3-13-47-13s-44.7 8-59 23c-12.7.6-28 3-28 3l-96 31s-73.6 7.4-94 10-12.2 23.5-14 15z"
		/>
		<path fill="#ccc" d="m571 660 39 42 32 100-43-53-28-89z" />
		<path fill="#f2f2f2" d="m1282 869-79 13-214-158 28-7 23 14 22-4-1-9 29-5 192 156z" />
		<path fill="#f2f2f2" d="M884 589c-24.9 2.4-56.1 2.7-71 36-6.9-3.8-18-14-18-14s32.8-48.4 89-22z" />
		<path
			fill="#e6e6e6"
			d="M881 929s-41 78.7-42 124c25.4 8.1 98.7 18.3 123 17-5-9.4-24.8-57.4-35.3-89.1 35.1-4.5 46.2-7.2 66.8-9.2 24 36.2 56.6 90.4 56.6 90.4s55.6-19.7 49-35c-6.6-15.3-68.1-98.4-118-115C936.9 920.6 881 929 881 929z"
		/>
		<path
			fill="#e6e6e6"
			d="M1012 598c0 .2-14.3 10.4-14 11 .3.6 16.6 6 29 6 12.4 0 25.8-3.5 26-6-3.7-4.7-11.7-12.1-12-11s-29-.2-29 0z"
		/>
		<linearGradient
			id="ch-full25001a"
			x1={945.0561}
			x2={1026.056}
			y1={873.1108}
			y2={935.0628}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full25001a)"
			d="M969 985c11.5 0 23.4.4 34 18s15 36 15 36-8.5-10-27-10-22.6 19-21 35c-8.2-3.1-14-8-14-8l-19-48s20.5-23 32-23z"
		/>
		<path fill="#e6e6e6" d="m1042 729-24-38s24.4-11.3 44-4c.5 17.1 0 40 0 40l-20 2z" />
		<path
			fill="#fe0000"
			d="M656.5 706.2c9 0 16.3 7.3 16.3 16.3s-7.3 16.3-16.3 16.3-16.3-7.3-16.3-16.3 7.3-16.3 16.3-16.3zm20 53c9 0 16.3 7.3 16.3 16.3s-7.3 16.3-16.3 16.3-16.4-7.3-16.4-16.3 7.4-16.3 16.4-16.3zm41-56.2c13.5 0 24.5 11 24.5 24.5S731 752 717.5 752 693 741 693 727.5s11-24.5 24.5-24.5z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				dur="1s"
				repeatCount="indefinite"
				values="#F00;#F33;#C00;#F30;#F30;#f00"
			/>
		</path>
		<linearGradient
			id="ch-full25001b"
			x1={887}
			x2={887}
			y1={652}
			y2={919}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.802} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25001b)"
			d="M1043 1092c-5.7 12.2-69.8 143.6-77 175-12.6 1-21 1-21 1s31.3-45.1-54-136-143.5-75.1-160-60c1-19.4 13.7-60.3 24-71 19 11.8 109 69 109 69s52.9 34 61 37 32 37 32 37l33-6s11.7-23.1 24-30c12.3-6.9 20.8-14.7 29-16z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full25001c"
			x1={899.7966}
			x2={798.0506}
			y1={920.8753}
			y2={751.8752}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.446} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25001c)"
			d="M891 1132c-85.3-90.9-143.5-75.1-160-60 1-19.4 13.7-60.3 24-71 19 11.8 109 69 109 69s52.9 34 61 37 32 37 32 37l-35 26s-7.3-14.3-31-38z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full25001d"
			x1={892.7039}
			x2={867.7858}
			y1={1231.6176}
			y2={1044.6176}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.378} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25001d)"
			d="m769 754 49-14 12 6 82 31s22.2 3.5 28 3c7.5 14 39.2 99.9 38 131-25.6 7.2-97 16-97 16s-70.5-8.1-103-72c-2.7-8.7-6.6-22.9-7.9-38-2.7-29.8-1.1-63-1.1-63z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full25001e"
			x1={839.9525}
			x2={814.6314}
			y1={1175.7018}
			y2={985.6799}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25001e)"
			d="m818 738-46 17s-8.9 74.1 4 104c21.4 25.8 73.9 70.5 106 69-11.1-27.9-54-185-54-185l-10-5z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full25001f"
			x1={1175.5386}
			x2={1256.5386}
			y1={1074.0411}
			y2={1017.3241}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#666" />
		</linearGradient>
		<path fill="url(#ch-full25001f)" d="m1202 882 81-12-42 73-39-61z" />
		<path
			fill="#a6a6a6"
			d="M1196 876 989 725l17 28s-8.1 9-4 12c3.9 3 20 0 20 0l8 11s-14.5 11.3-22 18c1.7 4.2 6.5 8 10 8 6.7 0 13-8 13-8l205 146-40-64z"
		/>
		<path
			fill="#8c8c8c"
			d="m848 1058 115 14-2 25s56.7-1.3 75-7c-20.5 14.4-40.8 36.6-47 48-12.9 3.9-33 6-33 6s-15.2-29.6-30-37-78-49-78-49zm202 2-11 31s43.3-21.1 48-43c-16.7 7.3-37 12-37 12z"
		/>
		<path
			fill="#8c8c8c"
			d="M962 1065v31s66.5-3.3 73-8c-9-12.2-15-18-15-18s-7.5 11-21 11-36.7-16.2-37-16z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full25001g"
			x1={981.0806}
			x2={1110.0796}
			y1={1172.9771}
			y2={1294.498}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#737373" />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full25001g)"
			d="M1008 796c-7.8-18-16.6-25.2-23.6-31.4-26.2-59.2 53.6-113.4 97.6-66.6-7.7-26.6-23.8-41.5-35-42s-60.4-9.2-86 44c-7.3 29.7-8 59.9-8 69 5.3 12.6 16.1 14 21 17 10.4 6.3 20.9 8.5 34 10z"
		/>
		<path fill="#737273" d="m992 767 32 1 8 13s-2.2 1.3-5.2 3c-.6.4 8.9 12.7 8.2 13-26.7 10.9-25-3-25-3l-18-27z" />
		<path fill="#737273" d="m983 724 24 33-19 12s-6.8-13.3-8-24c-1.3-11.7 3-21 3-21z" opacity={0.502} />
		<linearGradient
			id="ch-full25001h"
			x1={1029.425}
			x2={1029.425}
			y1={643.8896}
			y2={874}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.802} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25001h)"
			d="M1042 1094s-62.5 127.7-78 181c21.5 1.6 29.5 3.6 45-11s46-54 46-54 34.2-47.3 36-81c7.3-33.4 2-83 2-83s-15.6 19.9-24 27-27 21-27 21z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full25001i"
			x1={834.4216}
			x2={834.4216}
			y1={628.819}
			y2={860.1024}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.802} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25001i)"
			d="M731 1073c13.8-10.3 29.6-19 62-8s186.9 102.1 152 203c-17.5 26.8-54.7 23-64 23s-75.7-3.1-126-76-37.8-131.7-24-142z"
			opacity={0.4}
		/>
		<path
			fill="#3f2021"
			d="M1056 624s24.2 14.9 29 23c-20.7-2.5-55-12-55-12s23.3-4.8 26-11zm-38 2c-4.8-2.5-21.4-11.9-22-17-11.4-2.5-86-14-86-14 .5-1.1 18 17 18 17l30 11s30.4.5 62 12c-.9-4.3 2.8-6.5-2-9zm-224-23-1 10 20 16-16 8-125 53-62 9-40-46s49.7-17.2 84.3-6.8C696.1 631.6 775 604 775 604l19-1z"
		/>
		<path fill="none" stroke="#d9d9d9" strokeWidth={5} d="m799 617-151 56" />
		<path fill="none" stroke="#d9d9d9" strokeWidth={2} d="m920 604 152 31" />
		<linearGradient
			id="ch-full25001j"
			x1={915.2408}
			x2={871.2527}
			y1={1179.7477}
			y2={1311.7477}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.05} stopColor="#db0b5b" />
			<stop offset={0.899} stopColor="#3f2021" />
		</linearGradient>
		<path
			fill="url(#ch-full25001j)"
			d="m816 623 108-10 31 11 1 104s-15.8 17-29 17-32-13-32-13-27.9-47.3-50-63-43-33-43-33l14-13z"
		/>
		<path
			fill="#db0b5b"
			d="M727 1089c9.4-5.1 20.3-8 29-8s35.9 4 61 21 49 35.6 67 57 41.2 48.5 47 77 4.2 30.1 1 40c3.2 1.7 8 4 8 4s10-3.8 10-35-23.3-64.1-32-76-49.1-61-89-84-60.4-22-70-22-23.4 3.3-30 8-7.5 13.2-2 18zm242 187c1.7-6.1 21.5-61.4 45-113 20.1-44.2 44.2-85.9 48-90 3.3 1 7 3 7 3l17-28 7 5s-65.7 107.5-106 224c-8.3.1-11.8-.5-18-1zM776 857l-39 88s-8.8 35.4 12 51 89 56 89 56 8.8-67 42-121c-19-7.8-48.1-19.7-68-35-23.3-18-36-39-36-39zm217 22c.8 5.5-5 31.8-10 33 12.3 7 86.8 53.5 115 114-1.3-16.3-8.5-55.5-19-69s-86.7-83.5-86-78z"
		/>
		<path fill="none" stroke="#f2f2f2" strokeWidth={10} d="m803 626-7 7-131 53h-3l-25-30 99-34s43.5-13.6 53-13" />
		<path
			fill="none"
			stroke="#f2f2f2"
			strokeWidth={10}
			d="M650 687s-31.8 2.2-37 7c-9.7-7.7-30.4-28.1-38-40 12.8-3.5 26.9-5.8 60-7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M567 662c-1.8-8.5 32 88 32 88s7.8 10.1 16 21 24 31 24 31 3 11.3 28 8 79-10 79-10 9.3-5.2 9-22 0-22 0-22l16-4s-3 16-3 40 2.3 52.3 8 60c-2.7 14.6-41 85-41 110s14.2 35.5 20 39c-6.9 14.3-23.3 35.8-25 72-10.9 19.3-13 32.7-13 44 0 40.3 51.2 175 176 175 33 0 51-22 51-22l20-5-2 11s24.3 7.4 48-13c23.3-20 42-48.6 44.9-53.7.2 0 .2 0 .4-.1 1.1.8 2.6.5 3.4-.6 6-8.9 26.5-41.4 32.9-75.8.3-1.9-1-3.7-2.9-3.8h-.6s14.8-42.3 5-86c4.5-5 7.1-15.3 6-25-1.1-9.7-9.4-47.8-27-68s-56.4-56.4-72-67-35.1-68.7-39-80-15-33-15-33l3-5s31.4 36 61 36c17.1 0 26-5 26-5l203 146 45-73-192-153 1-67s-18.2-16-40-27c.2-4.8 0-13 0-13l-10-9s-20.8-6.5-36 0c-4.6 3.3-10 8-10 8v2s-65.3-14-92-17c-17.4-8.5-33.3-13-47-13s-44.7 8-59 23c-12.7.6-28 3-28 3l-96 31s-73.6 7.4-94 10-12.3 23.5-14.1 15z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m949 765 8-36s-2.1-85-3-93-2.3-18.3 22-12 117 27 117 27"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M798 604s-3.3 5.2-5 8c7 5.9 19 14 19 14l6-2 3-3 95-8 11 4 7-2s-17.2-17.8-42-27"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M812 625c5.2-11.4 20.9-39 90-33" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m769 753 40-12s9.6-6.2 39 8 70 29 70 29" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M829 744s10.8 32.6 20 70c11.1 45.1 21.8 97.7 34 113" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M939 779c16.1 20.1 41.2 122.8 42 133 9.3 7.2 70.7 29 119 116"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M956 726s-18.7 33.3-59 9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M998 609v6s10.8 7.9 28 12 26 0 26 0v-17" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1052 611c-7.5 2.4-32.2 7.3-54-3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1091 709c-2.5-11.3-19-54-67-54-44.9 0-64.3 34.1-67.6 49.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1284 870-82 12 35 58" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1204 882s-187.4-139-213.9-158.7m-5.7-.5c1.3-.3 33.6-6.8 33.6-6.8l-1-25s13.8-10.6 39-6c2.5.5 4.9 1.1 7.1 1.9 1.1.4 4.1 2.8 5.1 3.2M1090 712l-29 4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1089 709c-3.5-7.2-13.5-20.2-27-25m-27-2c-27 2.1-73.7 34.4-49 83"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1019 693 22 36 21-2v-38" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M983.1 721.8c6.5 8.6 25.9 34.2 25.9 34.2l-19 10 19 26 23-14-8-12h-30"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1028 783 10 14" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m816 740-19-102 17-13m119-11 25 10" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m927 980 69-8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M937 1009c-.8-15.9 53.3-47.4 72 5 7.8 13.7 13 30 13 30m-44.5 27.8-.5-.4-.5-.4-.6-.5L955 1055"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M997.5 1028.2c14.5 0 26.3 11.8 26.3 26.3s-11.8 26.3-26.3 26.3-26.3-11.8-26.3-26.3 11.8-26.3 26.3-26.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M998 1044.2c6 0 10.8 4.9 10.8 10.8s-4.9 10.8-10.8 10.8-10.8-4.9-10.8-10.8 4.8-10.8 10.8-10.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1088 1128c-7.2 5.2-30.6 51.2-35 84" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M730 1073c12.1-14.6 63.6-34.8 150 47s76.7 140.9 57 157"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M769 1181c8.1-1.7 31.4 4.5 52 35s1.5 31.3-6 31-28.4-8.9-45-35c-9.8-15.4-9.1-29.3-1-31z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M965 1274s15.5-44.1 35-90c15.6-36.7 34.2-74 47-94" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M776 851c2.9 10.4 34.7 62.3 109 78 59.1-9.1 98-18 98-18s18.7-20.9 8-39"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1097 1036c-9.8 15.2-46 24-46 24s-4.2 11.9-13 28c-23.2 8.6-77 9-77 9s.8-11.7 2-25c-22.1 1.3-99.7-10.2-122.5-16.6m-2.5-2.6c.6-19.7 21.3-90.3 43-123.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1037 1087-17-17" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M962 1070s-36.3-81.2-42-115c1.9-7.2 6.1-16.8 22-19s27.9 2.3 40 16 60 89.3 67 108"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M570 656s23.3 33.1 41 43" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m644 806-33-96s-9.5-11.7 18-15 101-10 101-10 23-5.1 23 13 2 70 2 70"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m682 635-54 20 32 35" />
		<path
			fill="none"
			stroke="#f3f3f3"
			strokeWidth={5}
			d="M717.5 714.5c6.6 0 12 5.4 12 12s-5.4 12-12 12-12-5.4-12-12 5.4-12 12-12z">
			<animate
				fill="freeze"
				attributeName="opacity"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="1;1;0.8;0.8;0.6;1"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dashoffset"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="0; 50; 0"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dasharray"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="2 3 2; 1 1 1; 1 3 3; 1 2 3; 1 1 1; 2 3 2"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-width"
				attributeType="XML"
				dur="8s"
				repeatCount="indefinite"
				values="3;6;3"
			/>
		</path>
		<path
			fill="none"
			stroke="#f3f3f3"
			strokeWidth={2}
			d="M656.5 717.1c3 0 5.4 2.4 5.4 5.4s-2.4 5.4-5.4 5.4-5.4-2.4-5.4-5.4 2.4-5.4 5.4-5.4zm18 53c3 0 5.4 2.4 5.4 5.4s-2.4 5.4-5.4 5.4-5.4-2.4-5.4-5.4 2.4-5.4 5.4-5.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M718 701c14.4 0 26 11.6 26 26s-11.6 26-26 26-26-11.6-26-26 11.6-26 26-26zm-61.5 5.2c9 0 16.3 7.3 16.3 16.3s-7.3 16.3-16.3 16.3-16.3-7.3-16.3-16.3 7.3-16.3 16.3-16.3zm0 0c9 0 16.3 7.3 16.3 16.3s-7.3 16.3-16.3 16.3-16.3-7.3-16.3-16.3 7.3-16.3 16.3-16.3zm18 53c9 0 16.3 7.3 16.3 16.3s-7.3 16.3-16.3 16.3-16.3-7.3-16.3-16.3 7.3-16.3 16.3-16.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M669 690s104.8-44 114-47 18.3-10.5 37 4 62.8 54 77 90 19 41 19 41l19 2 12-9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1093 1043c-4.6 5.6-24.8 36.8-46 46s-35.4 17.3-47 34-13 16-13 16l-31 6s-15.7-25-27-33-9.2-5-25-15-30.2-20.4-55-36c-18.5-11.6-85.5-51.8-95-61"
		/>
	</svg>
);

export default Component;

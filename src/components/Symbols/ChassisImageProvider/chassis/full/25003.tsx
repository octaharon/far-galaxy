import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1006 1187s78.3 94.6 111.8 133.5c5.1 5.6 15.9 16.4 27.1 13.8 19.1-4 58.1-11.6 80.2-16.3 9.7-2.2 19.7-5.8 17.1-22.7-4.7-49.4-17.2-179.3-17.2-179.3l-142-184-30 1-116-176 44 35s24.4 15.1 57 4c27.8 20.9 201 147 201 147l45-76-195-153s-2.7-35.2-32-52c-8.8-12.3-29.4-27.7-44-34-10.2-4.4-23.7-8.5-39.9-11.8 1-28.8-17.6-29.9-29.1-31.2-8.2-.9-28.9 7.4-28.8 24.2-9.5-.4-19.3-.6-29.5-.4-3.6.1-10.6-11.2-20.7-13.8-13.6-3.4-31 1.6-36 2-26.6 1.7-11.1 16.7-20 18.3-8.8 1.6-11 2.7-11 2.7l-59 16-18 7s-14.2-13.3-51-15c-4.2-.2-5.2 3.1-7 5-48.8 0-49.8 12.5-48 21s37 118 37 118l52 57 76-12-1-77 21-3v13s-9.3 5.6-8 6-2 96-2 96l-34 85-3 55-11-12-115 14s-3.5 100.7-5.2 148.4c-1.3 18.2 4.3 34.4 10.7 45.8 26.6 48.6 91.5 167.5 115.4 211.3 3.4 6.5 9 16.6 24 13.4 22.5-4.7 64.9-13.5 88.3-18.4 8.5-2.2 16.3-7.3 14.9-19.2-5.6-50.5-23.1-205.3-23.1-205.3s37 27 101 27c30.8.2 53-8 53-8z"
		/>
		<radialGradient
			id="ch-full25003a"
			cx={877.523}
			cy={1320.047}
			r={1016.598}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d1d1d1" />
			<stop offset={0.208} stopColor="#d1d1d1" />
			<stop offset={0.302} stopColor="#e6e6e6" />
			<stop offset={0.438} stopColor="#dedede" />
			<stop offset={0.682} stopColor="#4f4f4f" />
		</radialGradient>
		<path
			fill="url(#ch-full25003a)"
			d="M887 1134c19.6 7.9 81.8 28.6 137 14 55.2-14.6 81.6-58.6 81-74s8.3-71.1-72-151c-31.3-31.5-56.9-63.1-96-155-6.7 3-34.1 28-110 10 8.1 44.9 16.3 90.8 15 130s-1.3 106.2 6 126 25.4 90.1 39 100z"
		/>
		<path
			d="M1004 905c-5.4.2-17.2 39.4-51 47s-68.8-27.8-83-26-14 4-14 4 23.8 41.9 51.3 84.1C935.9 1058 969 1102 969 1102l107-14s-3.5-55.3-24-99-42.6-84.2-48-84z"
			className="factionColorSecondary"
		/>
		<path d="M938 769c3.3 13.5 42 111.5 113 167-32.1-54.3-59-93-59-93s-57.3-87.5-54-74z" opacity={0.2} />
		<radialGradient
			id="ch-full25003b"
			cx={935.842}
			cy={1102.6069}
			r={377}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-full25003b)"
			d="M883 1132c-11.9-26.2-37.8-76.4-42-137s7.9-96.7-13-216c-13.8-5.8-29.7-13.9-35-24-.5 24.5.2 104.4-11 127s-26.3 55.9-27 79-2.7 57.6-2 67c7.2 15.8 51.8 86.4 130 104z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full25003c"
			x1={822.9572}
			x2={757.9572}
			y1={1082.4427}
			y2={993.7937}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25003c)"
			d="M793 756c6.3 5.7 17.3 20.5 34 24 3.7 20.8 15 84 15 154-22.2-12.8-65-39-65-39s14.4-7.6 16-139z"
			opacity={0.149}
		/>
		<path
			fill="gray"
			d="m623 1139 136 236 119.7-25s1.6 19.7 2 23c.4 3.3 3.1 14.8-18 20s-89.4 18.4-94.7 19-13.3-.5-23-17-112.4-207.6-117-216-5-40-5-40zm398 40 112 124 108-22s1.2 16.2 1 20-3.5 13.6-15 17-79.1 17.4-85 17-15.3-1.5-25-14-111-133-111-133l15-9z"
		/>
		<path fill="#ccc" d="m629 995 98 193 30 184-134-237 6-140z" />
		<path fill="#b3b3b3" d="m651 694 41 117-40-45s-31-105-32-106 31 34 31 34z" />
		<path
			fill="#ccc"
			d="m653 698 126-11 1 130-78 11-49-130zm262.5-98c8.3 4.2 42.4 7 58.5 0 .3 7.1 0 22 0 22s-4.4 6-18 6-36.3-11.3-39-15-1.4-5.4-1.5-13z"
		/>
		<linearGradient
			id="ch-full25003d"
			x1={863.1945}
			x2={865.9545}
			y1={1181.6503}
			y2={1131.6503}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={0.499} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-full25003d)"
			d="M800 736s69 10.3 119-5c6.5 11.4 14 25 14 25l1 14-38 11-70-3-31-24 5-6v-12z"
		/>
		<linearGradient
			id="ch-full25003e"
			x1={888.3521}
			x2={876.6381}
			y1={1220.8073}
			y2={1352.8463}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.5} stopColor="#f3f3f3" />
		</linearGradient>
		<path
			fill="url(#ch-full25003e)"
			d="M766 741c1.2-.2 37-6 37-6s88.6 13.8 136-12c18.6-33.7 36.6-83.5 102-76-20.1-11.6-40.1-22.3-66.6-29.3-3.8 4.1-14 12.6-21.4 11.3-11.3-1.9-27.5-12.7-36.8-19.7-30.8-1.3-68.9 1.1-118.2 8.7-.3 4.3 0 11 0 11s-12.4 15.2-31 15-26.5-6.9-29-9c-4.6 1.3-12 7-12 7l36 24s2.8 75.2 4 75z"
		/>
		<path
			d="m884 731 27-2s11.3-108.4-47-137c-24.2 4-33 3-33 3s34.5 9 45 71 8 65 8 65zm39-140c7.8 3 38.7 4.3 40-3 6.1 6.6 9 11 9 11v24s-13.8 6-19 6-38-20-38-20l1-11s3.7-5.6 7-7zm-147 93-124 13-40-55s20.5-15.1 63-15c-3.8 8.4-11 30-11 30l24 23 34-2-14-42s51 26.8 68 48z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full25003f"
			x1={871.4435}
			x2={871.4435}
			y1={1189}
			y2={1328}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25003f)"
			d="m884 731 27-2s11.3-108.4-47-137c-24.2 4-33 3-33 3s34.5 9 45 71 8 65 8 65z"
			opacity={0.2}
		/>
		<path
			fill="#ccc"
			d="m872 747 13-16s-1.7-131.4-58-133c-7.7.8-18.5 6-16 16 18.4-1.1 37.9-1.6 49 43s8 90 8 90h4z"
		/>
		<path fill="#999" d="m886 733 22 1-12 16-25 1 15-18z" />
		<linearGradient
			id="ch-full25003g"
			x1={992.6241}
			x2={981.2422}
			y1={1186.2865}
			y2={1314.5765}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.5} stopColor="#f3f3f3" />
		</linearGradient>
		<path
			fill="url(#ch-full25003g)"
			d="M917 730c2.5 4.1 15 24 15 24s14.1 10.5 27 20c-21.9-28.8 10.3-143.4 103-107-10.8-10.4-22-21-22-21s-41.6-5.1-70 27-21.4 57.6-53 57z"
		/>
		<linearGradient
			id="ch-full25003h"
			x1={925.5367}
			x2={976.5367}
			y1={1199.3086}
			y2={1188.5706}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25003h)"
			d="M919 730s25.5 5.9 39-44c8.1-12.8 12 10 12 10s-27.2 36.3-12 78c-11.9-8.5-26-19-26-19l-13-25z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full25003i"
			x1={968.9017}
			x2={1048.1278}
			y1={1120.1674}
			y2={1261.0004}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.5} stopColor="#dedede" />
		</linearGradient>
		<path
			fill="url(#ch-full25003i)"
			d="M1010 800c-78.3-67.6 22.4-152.8 74-98-7.7-26.6-23.8-41.5-35-42s-60.4-9.2-86 44 2.6 79.7 13 86 20.9 8.5 34 10z"
		/>
		<linearGradient
			id="ch-full25003j"
			x1={1176.2313}
			x2={1255.2313}
			y1={1072.0051}
			y2={1016.6891}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#666" />
		</linearGradient>
		<path fill="url(#ch-full25003j)" d="m1203 879 79-10-43 76-36-66z" />
		<path fill="#a6a6a6" d="M1199 878 993 727l15 30 17 10 8 11s-14.5 11.3-22 18h23l205 146-40-64z" />
		<path fill="#dedede" d="m851 1169 28 183-121 26-32-193 125-16zm261-35 112-15 15 163-104 20-23-168z" />
		<path fill="#dedede" d="m1063 691-1 35-22 3-23-39s27-11.4 46 1z" />
		<path
			fill="#ccc"
			d="M1065 690c5.6 1.9 22.3 15 24 24-13.3.5-24-1-24-1s-5.6-24.9 0-23zm-48 0 14 26-39 4s3-8.8 9-16c6.5-7.7 16-14 16-14zm-27 37 18 28-22 13s-11.1-28.2 4-41z"
		/>
		<path fill="#a6a6a6" d="M1025 768c1.2-.1 6 11 6 11l-21 18s-21.6-22-21-22 34.8-6.9 36-7z" />
		<path fill="#a6a6a6" d="M1008 755c.4-.2 14 11 14 11l-33 8-1-7s19.6-11.8 20-12z" opacity={0.6} />
		<linearGradient
			id="ch-full25003k"
			x1={1096.1835}
			x2={985.1835}
			y1={725.9124}
			y2={779.7394}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full25003k)" d="m1022 1179 111 122-22-167-19-31-27 20s-16.9 38.8-43 56z" />
		<linearGradient
			id="ch-full25003l"
			x1={676.7731}
			x2={932.0034}
			y1={781.02}
			y2={768.85}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000004" />
			<stop offset={1} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-full25003l)"
			d="M1072 1119s-29.6 18.2-63 23-90.3 4.3-123-8-69-32-69-32l35 70s51.8 24 110 24c65.2 4.1 110-77 110-77z"
		/>
		<linearGradient
			id="ch-full25003m"
			x1={1033.2504}
			x2={1018.3553}
			y1={738.0502}
			y2={832.0972}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8c8c8c" />
			<stop offset={1} stopColor="#dedede" />
		</linearGradient>
		<path
			fill="url(#ch-full25003m)"
			d="M971 1104c.8.2 104-16 104-16s9.2 32.7 1 57-17 23-17 23-84 15-83 14 22.2-13 19-29-24.8-49.2-24-49z"
		/>
		<path
			fill="#ccc"
			d="m868 957 102 145s30.4 39.8 27 51-7.5 21.8-23 25c-15.5 3.2-38 4-38 4s-36-5.8-35-18 28.3-19.3 27-33-8.6-31.4-18-58-42-116-42-116z"
		/>
		<linearGradient
			id="ch-full25003n"
			x1={911.4882}
			x2={1005.5952}
			y1={743.1588}
			y2={805.8968}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25003n)"
			d="M926 1136s-31.7 26.7-25 30 29.8 20.4 50 19 32-8 32-8 25.2-23.3 0-51c-4.8-10.2-14-26-14-26l-41 18-2 18z"
			opacity={0.4}
		/>
		<path
			fill="red"
			d="M742.5 704c13.7 0 24.7 11.4 24.7 25.5s-11 25.5-24.7 25.5-24.7-11.4-24.7-25.5 11-25.5 24.7-25.5zm-21 62.7c9.8 0 17.8 8.2 17.8 18.3s-8 18.3-17.8 18.3-17.8-8.2-17.8-18.3 8-18.3 17.8-18.3z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				dur="1s"
				repeatCount="indefinite"
				values="#F00;#F33;#C00;#F30;#F30;#f00"
			/>
		</path>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M779.5 738c7.5-1.1 15.9-2.3 19.1-2.8.7-.1 1.4.5 1.4 1.2V748s-9.3 5.6-8 6-.4 68.1-2 96-22 63-28 79c-6.3 16.7-8 61-8 61l-12-12-115 14s-3.5 100.7-5.2 148.4c-1.3 18.2 4.3 34.4 10.7 45.8 26.6 48.6 91.5 167.5 115.4 211.3 3.4 6.5 9 16.6 24 13.4 22.5-4.7 71.9-15.5 95.3-20.4 3.9-1 15.5-13.8 14.4-23.7-.5-4.5-.7-5.3-1.4-11.3-7.9-60.8-28.2-187.5-28.2-187.5s37 27 101 27c30.8.2 53-8 53-8s78.3 94.6 111.8 133.5c5.1 5.6 15.9 16.4 27.1 13.8 19.1-4 58.1-11.6 80.2-16.3 9.7-2.2 19.7-5.8 17.1-22.7-4.7-49.4-17.2-179.3-17.2-179.3l-142-184-30 1-116-176 44 35s24.4 15.1 57 4c27.8 20.9 201 147 201 147l45-76-195-153s-2.7-35.2-32-52c-8.8-12.3-29.4-27.7-44-34-10.3-4.4-23.8-8.6-40-11.9m-56.5-6.9c-9.8-.5-20-.7-30.6-.5m-75.3 6.3c-10.2 1.7-12.6 3-12.6 3l-59 16-18 7-20-9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m933 756-16-24" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1045 649c-79.9-13.2-99.3 68.2-105 73-4.5 3.8-7.5 6.9-30.8 11.2m-41.9 5.7c-36.8 2.3-69.3-3.9-69.3-3.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M963 779c-30-36.5 6.1-140.7 95-115m29 45c-7.8-11-18.7-24.7-49-25s-68.3 40.5-54 81 43.4 33 48 32c2.1-.5 5-1 5-1l-9-14 4-4-7-12-16-11s-17.6-26.5-20.5-31.3c-.3-.5.5-.7.5-.7l210 156 85-11"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m990 720 35-3 15 11 22-2v-13h25m-24-23-1 20m-24 15-21-34"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1199 879 39 61" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1008 756-21 11m5 8 31-8m5 13-19 15" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1094 1103s-6.3 5.3-16.9 12m-157.2 25.9c-13.7-1.8-25.4-4.2-33.9-6.9-22.1-6.9-60.3-21.5-71.4-34"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M793 754s5.8 8.6 14 15c7.8 6.1 18 10 18 10s37.4 7.3 64 5c26.8-2.3 45-16 45-16v-14"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1072.1 963.1c-36.9-46.5-72.1-74.8-90.1-102.1-19.3-29.2-46-93-46-93m-109 12s10.8 64.8 14 99-2.4 109.4 5 146c7.4 36.6 37 108 37 108"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m853 1171-39-71s-31.7-29-43-43-18-30-18-30v-41" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m630 993 95 191s25.2 137.8 33.9 202.7c1.8 15.8-6.9 19.3-6.9 19.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m622 1132 136 245s50.1-13.5 119-27m364-70s-46.4 9.1-110.6 21.7C1093.1 1260.1 1022 1179 1022 1179"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M793 1407c7-2.4 14.3-15.9 12-40m28 32c12.4-12.7 11.5-26.6 11-42m330-63c0 10.3.4 32.8-11 37m37-9c5.8-2 9-12.8 9-35"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1002 1189c5.9-1.6 14.5-7.3 24-15.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M786 1177s-36.8-66.3-67-120.8c3-7.8 6.4-15.7 8.4-22.7C712.2 1006.3 701 986 701 986m399.4 37.8c23.3 30.6 74.7 98.2 74.7 98.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1225 1118s-48.8 6.6-115.8 15.8m-257.4 35C779.1 1178.6 725 1186 725 1186"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1049 928s29.2 42.3 42 72c8.6 20 18.7 41.7 12 83-2.2 13.5-10 21-10 21l18 31s18.3 129.4 24 179c1.3 11.5-7 21-7 21"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M811 614c-.7-.5-1.3-14.2 15-16s37-4 37-4 55.7 6.5 46 138c-8.3 11.1-15 15-15 15l-24 3s1.5-122.1-38-137c-9-3.2-20.4.8-21 1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M827 597c14.6-.6 57.1 20.2 56 136 8.9-1 24-4 24-4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m883 734-12 15" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M916 611v-14l9-8s22.5-11 39 0c5.2 4.6 9 8 9 8v25s-8.6 6-18 6-39-17-39-17z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M917 598c3.7 3.3 9 7 30 7s26-8 26-8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m720 639 60 46v131l-78 10-45-58-44-126s13.6-14 51-14c3.3-.8 5.9-.9 12-2 8.3-.5 17-1 17-1l19 18 10 34-34 3-24-24 13-28"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m779 686-126 11-39-52" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m652 697 47 126" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M742.5 704c13.7 0 24.7 11.4 24.7 25.5s-11 25.5-24.7 25.5-24.7-11.4-24.7-25.5 11-25.5 24.7-25.5zm-21 62.7c9.8 0 17.8 8.2 17.8 18.3s-8 18.3-17.8 18.3-17.8-8.2-17.8-18.3 8-18.3 17.8-18.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m710 645-16 1-8 32" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m676 630 16 16" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1011 908c12.9 18.5 58.5 109.1 63 157 4.5 47.9 12.4 89.1-12 103-7.5 1.9-63.6 11.3-97.3 15.3-13.5 1.6-26.7.7-26.7.7s-23.4-5-35-15 26-24.3 26-42-12.8-39.4-22-62c-7.2-17.7-45.7-118-52.1-134.7-.3-.8-.2-3 3.7-3.6 5.6-1.3 15-2.9 20.4.3 8.2 4.7 47.4 30.7 81 23 31.8-7.3 34.5-36.6 35-40s9.2-10.9 16-2z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M976 1181c8.8-7.8 17-15.1 17-33s-57.8-80.2-137-217" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m971 1103 102-14" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m958 952 68 143" />
	</svg>
);

export default Component;

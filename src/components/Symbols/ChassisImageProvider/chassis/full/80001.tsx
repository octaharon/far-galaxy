import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f3f3f3"
			d="M311 1435c61-58.3 212.3-192.1 246-214 10.9 12.5 17 20 17 20l116-61s154.7-40.7 257-15c14.1 6.9 20 13 20 13s30.9 68.2 111 55c62.5 12.7 126.1 9.7 86-93 49.3-37.4 107.2-35.6 125-35 15.6 12.7 45 46 45 46l98 27 1-68 426 98s-18.8-62.5-261-166c-86.9-34.2-249-78-249-78l-174-86-9 15s-161.8-92.8-241-122c-45.7-24.4-72-23-76-23-7.3-2.9-28-10-28-10s-51.4-145.8-70-191c-11.3-10.3-30-19.5-33-12s7 162 7 162 9.1 42.5-76 85c-14.7-5.4-23 0-23 0s-39.7-1.7-37 35c3.3 16 2 29 2 29l-96 113-29-22-34 21 14 52s-149.3 208.2-177 248c-8.4 46.8 29.4 156.8 42 177z"
		/>
		<linearGradient
			id="ch-full80001a"
			x1={738.1518}
			x2={756.0798}
			y1={1375.4703}
			y2={1142.4703}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={0.598} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full80001a)"
			d="m717 544 24 12 68 196s-32.4 9-47 25c-16.7-7.7-63-28-63-28s27-23.8 27-46-9-159-9-159z"
		/>
		<linearGradient
			id="ch-full80001b"
			x1={1620.4293}
			x2={1649.9823}
			y1={656.4517}
			y2={782.4517}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80001b)" d="m1437 1111 423 98s-16.4-23.4-33-35c-27.6-9.3-391-91-391-91l1 28z" />
		<linearGradient
			id="ch-full80001c"
			x1={633.9924}
			x2={414.8104}
			y1={365.9957}
			y2={624.9957}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80001c)" d="m312 1433 246-213-26-46-243 215 23 44z" />
		<linearGradient
			id="ch-full80001d"
			x1={582.012}
			x2={641.0701}
			y1={1089.3683}
			y2={1152.3683}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path fill="url(#ch-full80001d)" d="m648 782-24-1s-39.4 1.3-36 35c5.3 20.6 4 28 4 28l56-62z" opacity={0.502} />
		<path fill="gray" d="m974 1182 99 49s-75.3 15.3-99-49z" />
		<path
			fill="#b3b3b3"
			d="M502 1065s58-30.8 83-46c19.1 23.8 65.6 107.5 105 162-36.1 20.2-119 61-119 61l-69-177z"
		/>
		<path fill="#b3b3b3" d="m500 1063-60-88 5 33 115 210-60-155z" />
		<path
			fill="#ccc"
			d="m1431 1176-95-25-44-46s43 1.9 43-10-21-128.2-169-200c3.5-6.3 7-16 7-16l197 111 62 20-1 166z"
		/>
		<linearGradient
			id="ch-full80001e"
			x1={669.8815}
			x2={1098.8304}
			y1={711.7252}
			y2={1012.0781}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a2633" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full80001e)"
			d="M745 838.2c0-63.2 42.1-91.2 100.8-91.2 89 0 155.6 98.9 155.6 98.9s36.5 49.9 71.8 109.3c24 40.4 46.8 85.7 59 120.8 6.9 19.3 83.6 160 10.3 160-55.8.3-77.4 3-187.4-66-9.4-7.4 18.4-.5 41 0C781.8 958.8 773.3 882 745 838.2z"
		/>
		<radialGradient
			id="ch-full80001f"
			cx={143.407}
			cy={372.403}
			r={2094.6011}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.51} stopColor="#f2f2f2" />
			<stop offset={0.52} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full80001f)"
			d="M745 838.2c0-63.2 42.1-91.2 100.8-91.2 89 0 155.6 98.9 155.6 98.9s36.5 49.9 71.8 109.3c24 40.4 46.8 85.7 59 120.8 6.9 19.3 83.6 160 10.3 160-55.8.3-77.4 3-187.4-66-9.4-7.4 18.4-.5 41 0C781.8 958.8 773.3 882 745 838.2z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full80001g"
			x1={1278.7772}
			x2={1332.7772}
			y1={823.7682}
			y2={798.0583}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#303030" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80001g)" d="m1292 1107 42-10 12 6-10 46-44-42z" />
		<linearGradient
			id="ch-full80001h"
			x1={960.8979}
			x2={1007.494}
			y1={761.9857}
			y2={725.9857}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.503} stopColor="#2e3135" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80001h)" d="m1012 1179-1 22-65-36 66 14z" />
		<path
			fill="#666"
			d="M1098.2 1088.6c.3-.3 1.1-.5.9-.9-5.2-9.8-58.6-126.6-121.5-202.3-25.9 8.5-38.9 19.4-54.8 43.9 4.7 39.1 12.3 76.3 30.2 96.1s91.6 72.6 101.4 78.7c8.4.5 21 0 21 0l.3-.3c4.1-7.2 13.5-13.7 22.5-15.2zm-58.5-193.8c-10.3-6.7-14.6-15.2-39.8-12.8 56.7 73.8 96.9 151.5 119.8 201.4 10-1.2 15.7-1 22.9 4.3-12.2-29.3-61.9-133.5-102.9-192.9z"
		/>
		<path
			d="M753.6 847.9c8.7 28.2 25 57.5 36.4 77.1-41.6 18.5-99.2 57.7-139.5 86.7-1.8-11.6-3.8-66.8-.5-74.7 8.8-12.3 102.9-91.3 103.6-89.1zm-117.8 174.5C615.9 1037 603 1047 603 1047l-17-26s-40.6-26.7-66-43c124.5-161.8 171.3-201.2 188-212 12.6-1.4 36.7 8.3 51 15-12.1 12.7-13.4 31.7-9.2 52.3C736 841.5 650 910.1 634 931c-3 25.8-1.1 76 1.8 91.4zm355.9-190.8c-20.3-23.9-41.8-47-54.7-56.6 32.7 11.1 189.6 97 218 112s62.1 37.4 80 52c-7.3-2.3-37.4-9.8-72-18.1-6.2-8.4-49.7-43.3-63-50.9s-89.2-36.5-108.3-38.4zm147.5 83.6c-46.3-11-93.2-21.9-101.2-23.2-6.2-10-18.3-26.2-32.3-43.5 11.6-.2 64.7 19.1 84.3 29.5s53.1 38.1 49.2 37.2z"
			className="factionColorSecondary"
		/>
		<path
			d="M615 1068s75.1-58.6 91-67c15.9-8.4 98-54 98-54l67 86s-164.8 37.2-227 76c-10.9-16.6-29-41-29-41zm37 54s134.8-63 228-76c27.9 29.9 116 125 116 125s-91.7-17.5-177-13-128 21-128 21l-39-57zm510 20c18.9-12.1 60.4-42.6 130-37 25.8 1.8 47.3-.6 41-21s-26-56-26-56-126.7-20.7-197-10c11.5 21.4 42.3 86.6 52 124zm-57-138c21.4-2.7 166.4 4 191 5-13.7-20-41-50-41-50l-205-46s39.1 55.9 55 91z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-full80001i"
			cx={1305.1851}
			cy={451.571}
			r={388.642}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.9} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full80001i)"
			d="M1165 1142s35.5-41.7 161-40c10.4-1.1 12-19.1-18-75-50.1-1.3-180.2-25.8-196-7 5.4 14.5 53 122 53 122z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full80001j"
			cx={854.845}
			cy={138.7791}
			r={652.483}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.897} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full80001j)"
			d="M690 1178c34.7-10 171-40 307-6-56.4-58.5-116-124-116-124s-63.7 13.7-121 32c-57.2 18.2-108 41-108 41s28.4 41 38 57z"
			opacity={0.702}
		/>
		<radialGradient
			id="ch-full80001k"
			cx={2050.3494}
			cy={222.0083}
			r={188.0428}
			gradientTransform="matrix(0.8598 -0.5106 1.4743 2.4824 -1164.6193 1545.8221)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1577} stopColor="#333" />
			<stop offset={0.4367} stopColor="#666" />
			<stop offset={0.5342} stopColor="gray" />
			<stop offset={0.6306} stopColor="#e6e6e6" />
			<stop offset={0.6937} stopColor="#999" />
			<stop offset={0.8316} stopColor="#666" />
			<stop offset={1} stopColor="#333" />
		</radialGradient>
		<path
			fill="url(#ch-full80001k)"
			d="M1099.1 1088.6c.3-.3 1.1-.5.9-.9-5.2-9.8-58.6-126.6-121.5-202.3-25.9 8.5-38.9 19.4-54.8 43.9 4.7 39.1 12.3 76.3 30.2 96.1s91.6 72.6 101.4 78.7c8.4.5 21 0 21 0l.3-.3c4.1-7.2 13.5-13.7 22.5-15.2zm-58.5-193.8c-10.3-6.7-14.6-15.2-39.8-12.8 56.7 73.8 96.9 151.5 119.8 201.4 10-1.2 15.7-1 22.9 4.3-12.2-29.3-61.9-133.5-102.9-192.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1100.2 1088.6c.3-.3 1.1-.5.9-.9-5.2-9.8-58.6-126.6-121.5-202.3-25.9 8.5-40.9 19.4-56.8 43.9 4.7 39.1 12.3 76.3 30.2 96.1s91.6 72.6 101.4 78.7c8.4.5 21 0 21 0l.3-.3c4.1-7.2 15.5-13.7 24.5-15.2zm-60.5-193.8c-10.3-6.7-14.6-15.2-39.8-12.8 56.7 73.8 96.9 151.5 119.8 201.4 10-1.2 15.7-1 22.9 4.3-12.2-29.3-61.9-133.5-102.9-192.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M687 1178s157.7-38.7 260-13c14.1 6.9 20 13 20 13s30.9 68.2 111 55c62.5 12.7 126.1 9.7 86-93 49.3-37.4 107.2-35.6 125-35 15.6 12.7 45 46 45 46l102 27m-1-66s404.4 91.8 424 96c-32.7-44.8-60-74.7-261-166-86.9-34.2-249-78-249-78s-129.9-63.9-174.3-85.8c-1.9 3.6-7.7 14.8-7.7 14.8m-12-5s-150.8-87.8-230-117c-45.7-24.4-72-23-76-23-7.3-2.9-28-10-28-10s-51.4-145.8-70-191c-11.3-10.3-30-19.5-33-12s7 162 7 162 9.1 42.5-76 85c-11.5-.4-23-1-23-1s-39.7-1.7-37 35c3.3 16 2 30 2 30l-96 112-29-21-34 21 15 51s-150.3 210.2-178 250c-8.4 46.8 29.4 155.8 42 176 61-58.3 213.3-192.1 247-214m9 23c24.2-13.3 125-61 125-61"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M636 783c-10.1 1.2-38.6 16.8-43 47" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1338 1120 93 23" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1343 1088 89 22" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1351.8 1057.2c20.1 5 68.8 17 81.3 20.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1357.6 1022.9c18.8 5 57.3 15.4 74.1 19.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1054.1 912.9c32.2 7.5 67.2 12.7 102 21.5 33.5 8.4 67.9 20.3 102.4 29.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1036.1 891.9c32.2 7.5 66.2 12.7 101 21.5 33.5 8.4 67.9 20.3 102.4 29.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1166 921c-22.2-18.6-47.4-41.8-74-55-21-8-59-25.4-104.1-39.3m13.1 16.4c34.6 11.8 71.5 27.3 87 33.9 33.1 18.6 50 36 50 36"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M615 1066.6c69.1-52.8 131.1-93.4 191.9-121" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M602 1042.6c69.1-52.8 131.1-93.4 191.9-121" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M635 1019s-6-66.8 0-89c17.4-23.9 95.9-83.3 115-96" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M648.8 1010.3c-1.4-21.2-3-57.9 1.2-73.3 14.7-20.2 73.1-65.8 102.3-87.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M278.3 1354.2c90.7-77.2 171.3-153.6 240.4-210.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M386.5 1259.6c-14.4-33-37.3-83.5-35.5-117.6m13-15c.3 28.5 18.6 85.2 35.6 119.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1762 1124c-42.1-23.3-172.3-67.3-327.1-112.4-5.2-2.6-12.9-3.6-12.9-3.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1559 1048c-19.6-17-77-45-77-45m-37-12c24.6 15.8 70 44 70 44"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M722.1 712.6s34.4 32.8 55.2 52.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M760 779c-23-8-51.2-19.1-81-14" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M519 977c43.1-48.2 160.1-211.3 197-211" />
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M761 868c1.3-5 13-16 29-13 13.7 14.2 70 82 70 82s-16.5 41.5-7 74c-29.7-34.8-75-96.4-92-143z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M585 1020s68.5 104.2 109.7 166.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M654 1121c69.1-36.3 199.7-66.9 227-74m230-30.1c82.4-4.1 167.8 5.4 198 11.1m-12-17c-22.6-1.9-108.5-11.3-194.1-6.8M872 1034c-33.8 5.1-79.5 18.2-122.2 32.2-44.1 14.5-84.9 30-105.8 42.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m559 1207 113-55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m539 1172 113-55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M528.4 1131.4c17.4-8.5 72.6-35.3 96.5-46.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M512 1098s61.7-32 93.4-47.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m501 1061 70 180" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1432 1180v-168l-66-25-35 162" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1371 989s-122.1-75.7-176-100" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1435 1014c-13.1-9.1-69.5-43.2-76-47" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1281 1105c13.8-1.3 48.2 1.5 52-8 7.1-17.9-51.6-153.2-179-209"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M588 849c11.1-12.8 29-46 72-72" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M935 1162c21.8 2.8 63 10 63 10S662.1 844.1 768 772c40-28.8 86-24 86-24"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M964 1176c18.2 10.9 96.5 51.5 117 57" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M927 772c86.2 56.3 174.5 203.6 241 379" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M495.6 958.7c34.4 24 86.4 60.3 86.4 60.3l-80 45-70-104"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m446 1008 112 214" />
	</svg>
);

export default Component;

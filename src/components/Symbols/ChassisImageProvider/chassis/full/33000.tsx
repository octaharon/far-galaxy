import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M603 1477c7.1 14.3 8.8 25.2 33 21s144.3-26.7 154-29 14-17.1 17-26 31-98.2 31-145-7.8-71.1-12-81c43.7 15.9 71.9 23 124 23s119.9-7.6 195-24c43 41.6 128.5 130.8 140 141s16.5 12.7 34 10 105.7-20 120-23 23.2-10.9 36-30 51.5-79 63-122-9.8-98.8-30-129-90.5-69-103-81c-6.9-12.9-15.7-27.2-30-44-11.9-20.7-57.8-100.4-131-186-73.3-85.6-110.7-101.8-137-111-8.1-6.8-14.7-13.7-24-17-15 .5-112 10-112 10s-8.9-70.4-31-96c-87.7-100.6-137-101-137-101s-47.3-4-84-4-136.8 24-174 173c-26.5 10.5-68 33-68 33s-25 41.4-33 55-20.9 17.1-21 32 8.3 25.8 35 65c-37.9 1.7-62.7 6.3-70 7s-11 19.6-10 40 10.9 94.6 17 140 10.7 74.5 19 94 54.8 119.5 68 148 113.9 242.7 121 257z"
		/>
		<path
			fill="#e6e6e6"
			d="M476 640c13.9 19.7 97.7 123.6 121 143s53.1-12.1 61-18c-20.3 32.4-44.4 75.1-61 142-31.4-35.7-81.7-102.3-93-118-11.3-15.7-59.4-76.6-61-93 10.1-17.5 19.6-33.7 33-56z"
		/>
		<path
			fill="#ebebeb"
			d="M443 699c10.1 20.2 113.6 165.8 155 208-3.5 18.4-10.2 57.4-13 70-16.7-24.5-142.5-207.3-157-231s8.1-43.2 15-47z"
		/>
		<path
			fill="#d9d9d9"
			d="M547 597c9.8 28.1 22.8 78.8 192 93-10.4 6.1-16.1 10.9-20 14-13.5-.2-102.1-8-139-33s-39.7-44.7-33-74z"
		/>
		<path
			fill="#d9d9d9"
			d="M384 809c3.5.7 12.6 14 23 29 25.1 36.2 67 113.2 123 214 79.2 142.7 87.9 143.2 103 207s11.7 116.8-1 200c-19.8-34.4-218.5-427.8-233-461-6.3-36.8-21.9-148.6-18.1-175.9.5-4.2 1.5-13.4 3.1-13.1z"
		/>
		<linearGradient
			id="ch-full33000a"
			x1={344.9601}
			x2={571.9601}
			y1={914.2841}
			y2={1067.3971}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.601} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full33000a)"
			d="M459 792c13.9 21.4 141.3 208.8 146 215-14.5 1.6-91.9 8.8-97 9-69.3-125.3-120.1-229.8-130-194 .3-17-3.9-29.2 81-30z"
		/>
		<linearGradient
			id="ch-full33000b"
			x1={344.9601}
			x2={571.9601}
			y1={914.2841}
			y2={1067.3971}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.601} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full33000b)"
			d="M459 792c13.9 21.4 141.3 208.8 146 215-14.5 1.6-91.9 8.8-97 9-69.3-125.3-120.1-229.8-130-194 .3-17-3.9-29.2 81-30z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full33000c"
			cx={755.332}
			cy={983.846}
			r={588.348}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#000004" />
			<stop offset={0.402} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full33000c)"
			d="M605 1006c14.6 19.3 88.9 93.6 161 108 11.6 13 30.1 38.9 35 51-23.4 4-179.1 27.5-188 29-10.9-18.5-102-177-102-177s77.3-10.2 94-11z"
		/>
		<radialGradient
			id="ch-full33000d"
			cx={755.332}
			cy={983.846}
			r={588.348}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#000004" />
			<stop offset={0.402} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full33000d)"
			d="M605 1006c14.6 19.3 88.9 93.6 161 108 11.6 13 30.1 38.9 35 51-23.4 4-179.1 27.5-188 29-10.9-18.5-102-177-102-177s77.3-10.2 94-11z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full33000e"
			cx={1342.532}
			cy={931.663}
			r={309.821}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#000004" />
			<stop offset={0.402} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full33000e)"
			d="M1346 1085c12.4-.8 163.4-23.3 158-29s-70.6-57.7-97-70c3.6 27.6 7.8 54.4-66 89 2.7 3.3 3.2 6.3 5 10z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full33000f"
			x1={759.7075}
			x2={725.8835}
			y1={408.6925}
			y2={611.6924}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.803} stopColor="#ccc" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full33000f)"
			d="M634 1462c21.5-3.6 158.4-24.5 175-27 8.8-26.3 37.7-125 26-176-28.3 3.3-185.6 33.6-193 35 1.5 14.8 9.3 38.9-8 168z"
		/>
		<path
			fill="#e6e6e6"
			d="M642 1292c-2.6-16.7-25-90-30-97 37.9-6 176.2-27.2 189-30 9.2 14.1 29 51.6 34 95-24.1 4.2-170.8 29.3-193 32zm708-209c12.5-3 142-21.4 153-23 7.9 6.2 29.1 44.8 35 80-18.2 2.9-144.1 25.7-154 27-2.3-23.5-2.9-45.7-34-84z"
		/>
		<linearGradient
			id="ch-full33000g"
			x1={1451.8674}
			x2={1417.3763}
			y1={545.4255}
			y2={752.4255}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.803} stopColor="#ccc" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full33000g)"
			d="M1321 1347c13.8-2.7 137.5-27.1 152-30 14.8-21.9 79-113.5 66-177-17.9 2.7-144.8 24.4-155 26-1.3 14.8 8.6 58.1-63 181z"
		/>
		<linearGradient
			id="ch-full33000h"
			x1={1391.2437}
			x2={1177.5447}
			y1={679.6195}
			y2={774.7645}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.975} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-full33000h)"
			d="M1169 1207c7.6 7.5 138.7 131.3 148 139 12.4-5.6 27.7-28.2 53-100s11.9-143.1-32-168c-16.7 25.8-50.1 60.6-169 129z"
		/>
		<linearGradient
			id="ch-full33000i"
			x1={687.4979}
			x2={1256.4979}
			y1={767.7763}
			y2={757.8443}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.099} stopColor="#4d4d4d" />
			<stop offset={0.45} stopColor="#b3b3b3" />
			<stop offset={0.645} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#b2b2b2" />
		</linearGradient>
		<path
			fill="url(#ch-full33000i)"
			d="M768 1116c39.4 8.7 144 21.3 234 20s203.6-12.5 255-29c51.3-16.5 76.6-28.2 80-26-3.2 8.5-22 27.3-51 51-31 25.3-92.5 66.7-115 74s-95.4 27.6-144 30-135.9 12.9-201-20c-16.5-41.3-34.6-78.2-58-100z"
		/>
		<linearGradient
			id="ch-full33000j"
			x1={1258.0364}
			x2={1133.5874}
			y1={696.2612}
			y2={844.5732}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full33000j)"
			d="M1338 1079c-7.8 16.8-53.7 57.5-96 84s-74.4 57.3-155 64-22.8-93.3-8-94 129.2-.4 259-54z"
		/>
		<path
			d="M1085 624c18.4 14.1 68.8 55 109 208 25 128.3 23 153.3 23 166-4.5 1.2-64.4 19.7-153 24-15.8-38.8-158.5-388.8-255-362 5.7-9.6 6.6-14.3 41-17s194.3-14.6 235-19z"
			className="factionColorPrimary"
		/>
		<path
			d="M858 484c-10.5-9.4-57-45.7-93-50 40 2.3 83.8 11.9 127 46-3.8.9-11 1-11 1s-12.7 2-23 3zm-236-11c14.1 1.6 63.4 16.4 96 94s31.2 105.7 32 117c-9.6 5.9-11.8 8.4-16 8-45.2-16.6-167.7-9.6-187-93 12.6-54.6 54.9-115.6 75-126zm316 165c4.6-.3 29.8-3.5 35-4-.6-9.2-12.8-33.4-23-40-8.4 2.3-25 4-25 4s-8.1 12.2-14 22c2.5 7-2.1 21.9 27 18z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-full33000k"
			cx={768.86}
			cy={1649.65}
			r={813.56}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#000" stopOpacity={0} />
			<stop offset={0.51} stopColor="#000004" />
			<stop offset={1} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-full33000k)"
			d="M750 682c12.3-7 51.9-22.1 60-23 6-7.3 6.8-12.7 25-14s129.9-9.3 137-10c-1.6-9.9-1.3-21.3-6-25-2.2 1-4.4-.9-8-3-6-3.8-2.8-10.5-7-10-8.2.9-18.5-3.4-25 0-7.4 3.9-10 19-10 19l-37-2-12-17-26 1-14 19s-71.5-82.3-128-119c20.5-14.1 42.6-6.7 72-10 12-1.3 109.8-2.1 120-6-21.7-28.2-79-43.2-110-44-107.3-19.6-150.7 27.6-161 35-42.3 38.8-109.3 146.7-55 188 37.8 28.7 119.4 41 150 42 14.6.5 34.1-25.7 35-21z"
			opacity={0.149}
		/>
		<linearGradient
			id="ch-full33000l"
			x1={717.5685}
			x2={912.1365}
			y1={1335.8906}
			y2={1477.7196}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.503} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full33000l)"
			d="M671 494c4.7 2.8 14.4-2.7 23 3 36.4 23.9 98.5 80 134 120 6 16 3 30 3 30s-20.5-2.5-21 13c-16.1 1.4-51 16.3-60 23s-184.9-223.3-79-189z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full33000m"
			x1={884.1761}
			x2={825.9092}
			y1={1300.7294}
			y2={1346.7294}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.503} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full33000m)"
			d="M828 617s2 6.6 3 14c1 7.6 1 13 1 13s111.7-7.9 119-8c-2-15.1-5.1-31.5-9-37-4.5 1.8-11.4-3-16 0-6.5 4.2-10 16-10 16l-40-1-5-14-29-2-14 19z"
			opacity={0.502}
		/>
		<path
			fill="gray"
			d="M400 1001s226.9 455.4 232 462c10.8-1.4 177-26 177-26s-6.8 29.7-22 33-146 27.6-159 29-21.2-13.5-28-28-183.6-392.1-189-406-11-64-11-64zm770 207s136.8 129.1 147 137c15-.6 146.8-23.6 154-26-3.1 8.7-18 22.1-34 25s-123.9 24.3-132 24-23.4-12.6-33-22-127-130-127-130l25-8z"
		/>
		<path
			fill="#4d4d4d"
			d="M1170 1206c7.3 7.8 141.3 135.1 148 141-8.6 7.2-25 20.3-46-1s-120.1-122.3-126-129c10.1-4.1 14.6-7.3 24-11z"
		/>
		<linearGradient
			id="ch-full33000n"
			x1={1144.8536}
			x2={1316.8536}
			y1={696.7628}
			y2={576.3268}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full33000n)"
			d="M1170 1206c7.3 7.8 141.3 135.1 148 141-8.6 7.2-25 20.3-46-1s-120.1-122.3-126-129c10.1-4.1 14.6-7.3 24-11z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full33000o"
			x1={712.9001}
			x2={701.829}
			y1={441.3881}
			y2={504.1761}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.2} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full33000o)"
			d="M614 1495c7.8 4.7 24.8 7.6 179-27 10.3-2.6 12.8-26.9 14-33-9.9.5-166 26.1-175 28-3.7 7.5-4.1 18.2-18 32z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full33000p"
			x1={1383.0243}
			x2={1374.3773}
			y1={567.6837}
			y2={616.7238}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.2} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full33000p)"
			d="M1289 1358c8 7.7 20.6 13.1 54 5s96.6-18.1 103-20 15.7-11.2 26-25c-20.4 2.9-139.6 26.7-152 29-6.1 4.8-18.3 12.2-31 11z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full33000q"
			cx={865.543}
			cy={1565.088}
			r={1399.02}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.201} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-full33000q)"
			d="M585 977c39.5 51.4 78.3 104.7 171 135s280.3 20 315 20c218.3 0 338-84.1 338-128 0-25-18.9-42.6-58-106s-154.7-221.7-238-254c-12.8-10.6-25.2-17.8-30-19s-250.4 19.9-261 21-9.8 9.8-12 13c-14.3 2.5-198.3 52.3-225 318z"
			opacity={0.302}
		/>
		<path
			fill="#b3b3b3"
			d="M829 616s-.5-.8-.5-1.4C820.8 607.2 714.8 506.1 698 496c19.8-2.6 33-5 33-5l95-5 62-5 21-8 172-9-5 11 72-2-90 110-41 11-12 23-48-10-7-11-24 1-10 19-38-4-9-16h-29s-9.1 15.7-10.7 18.5c.3.6-.3 1.5-.3 1.5z"
		/>
		<path fill="#bfbfbf" d="m1057 583 90-109-145 6 55 103z" />
		<linearGradient
			id="ch-full33000r"
			x1={1116.0452}
			x2={1025.0452}
			y1={1393.4779}
			y2={1398.2468}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#404040" />
		</linearGradient>
		<path fill="url(#ch-full33000r)" d="m1116 490-58 70-33-68 91-2z" />
		<g>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M892.8 480.9C836.9 437.5 789 436 789 436s-14.4-2.3-34.7-3.2c-10.6-.5-22.7.2-35.3.2-36.7 0-136.8 24-174 173-26.5 10.5-70 29-70 29s-23 45.4-31 59-20.9 17.1-21 32 8.3 25.8 35 65c-37.9 1.7-62.7 6.3-70 7s-11 19.6-10 40 10.9 94.6 17 140 10.7 74.5 19 94 54.8 119.5 68 148 113.9 242.7 121 257 8.8 25.2 33 21 144.3-26.7 154-29 14-17.1 17-26 31-98.2 31-145-7.8-71.1-12-81c43.7 15.9 71.9 23 124 23s119.9-7.6 195-24c43 41.6 128.5 130.8 140 141s16.5 12.7 34 10 105.7-20 120-23 23.2-10.9 36-30 51.5-79 63-122-9.8-98.8-30-129-90.5-69-103-81c-6.9-12.9-15.7-27.2-30-44-11.9-20.7-57.8-100.4-131-186-73.3-85.6-110.7-101.8-137-111-8.1-6.8-14.7-13.7-24-17-15 .5-111 9-111 9s-1.5-13.3-7-26"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M1141 1217c24.4-4.4 61.2-25.6 87-44 26.1-18.6 91.7-57.9 114-96"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M443 699c3.6 14.1 43.1 65.7 86 123.7 65 87.8 149.7 178.3 179 197.3 26.3-41.6 40-70 40-70s59.6 44 172 47c15.7 30.2 37 78.6 50.2 109.1 16.5-.5 103.7 4.1 184.8-6.1 74.2-9.3 139.3-30.5 152.7-35.2-2.3-40.9-6.7-121.8-6.7-121.8s39.2-14.1 48-45"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M477 636c-4.4 7 32.2 48.7 44 62s58.2 69.5 71 83 38.2 12.8 65-17 84.7-92.5 149-104c5.5-7.7 8.4-11.5 17-13s169-15 169-15"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M671 749c-16.5 17.7-60.7 85.8-74 160s-11.1 63.6-12 69"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M546 603c-2.9 14.7-8.7 29.9 16 55s97.1 42.6 159 44m29-20c-1.4-18.1-26.1-189.9-129.6-210.7M547 598c12.8 31.4 19 74.4 190 91"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M1106 641c20.8 20.5 98.3 129.4 112 357-14.3 3.8-41.3 10.7-70 15-41.7 6.2-85 8-85 8S916.9 634.4 807 660"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M1066.8 626c29 31.2 113.5 140.2 126.2 351-14.3 3.8-27.3 6.7-56 11-41.7 6.2-59 5-59 5S969.2 705 868.7 642.6"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M457 790s116.9 172 143 209c26.1 36.9 95.7 105.2 189 122 104.6 18.9 264.8 16.9 363 7s230.6-56.8 252-100c9.5-18.8 3.3-40.5 1-45"
			/>
			<path fill="none" stroke="#191919" strokeWidth={10} d="M831 1231c-8.1-29.3-41.7-100.3-68-117" />
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M607 1485c8.3 14.5 21.4-2.6 26-33s21.7-126-1-198-28.6-75.3-51-113c-9.4-15.7-39-67.5-71.3-123.9-45-78.8-99-176.6-105.8-188.1-8.4-14.3-16.3-22.9-21.8-23"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M508 1016s55.6-5.2 99.1-9.3" />
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M398 995s218.7 443.6 233 468c10-1.9 179-29 179-29m359-226 149 139 154-31"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M681 1491c9.7-8.7 18-29.8 18-37m57-10c-2.5 10.7-8.7 31.3-22 36"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M1378 1336c-3.6 5-9 17.3-19 24m47-8c4.8-4.8 14.9-18.7 17-26"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M647 1188c8 19.1 23.6 41.7 32 98m834-143c-8.6-39.4-23.6-70.4-37-81"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M612 1194s79.2-12 190.5-28.8m542.7-81.9c95.4-14.4 160.8-24.3 160.8-24.3m31.9 80.9s-62.3 10.6-154.1 26.3M836 1259.4C721.3 1279 639 1293 639 1293"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M1281 1353c7.4 6.6 36.6 10.3 56-31s43.4-95.7 45-125c2-37.7 3.7-84.7-43-120"
			/>
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M828.5 614.6C820.8 607.2 714.8 506.1 698 496c19.8-2.6 33-5 33-5l95-5 62-5 21-8 172-9-5 11 72-2-90 110-41 11-12 23-48-10-7-11-24 1-10 19-38-4-9-16h-29s-9.1 15.7-10.7 18.5c.5.9-.3 1.8-.8.1z"
			/>
			<path fill="none" stroke="#191919" strokeWidth={10} d="M828 613c1 3.5 4.7 21.3 5 33" />
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="m824 609-93-116m109 103-66-94s-35.4-10.6-36-11m36 12 24-7m71 100-75-108m35-1 86 129m-5-142 94 141m80-140-151 8 74 114 14-3m35-11-53-101m-119 1 68 116"
			/>
			<path fill="none" stroke="#191919" strokeWidth={10} d="m1024 491 94-3-59 72-35-69z" />
			<path
				fill="none"
				stroke="#191919"
				strokeWidth={10}
				d="M915 637c-.3-6.9-2.7-13.9-5.5-20.8M857.6 484c-19.4-19-50.7-42.7-91.6-50"
			/>
		</g>
	</svg>
);

export default Component;

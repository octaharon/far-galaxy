import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1145 1553c24-4.8 115.6-24.6 137-31s18-30.3 19-45 1.5-43.6-11-63c4.6-8.8 9.2-27.9-10-65s-39.3-49-50-62c10.2-15.1 24.5-52.9 8-127-9.8-44-23.7-83.1-35.3-111.3 5.5-10.9 11.3-15.8 10.3-50.7 1-15.9 19.9-10.6 20-24s3.4-88.2-23-109-97.7-75.2-108-81-35.7-15.4-39-16c-7.6-10-17.9-18.6-28-33s-20.1-30.4-25-38c5.9-10.4 23-38 23-38l-102-81 4-8-2-3s5.3-26.6-7-49-29.7-34.6-83-42c-23.4-3.3-57.3-9.6-83-6-32.9 4.6-55.5 21.5-60 37-7.9 27.7-2.3 58 21 75s42 26 42 26-44.1 7-53 41c-21.2 19.5-43 40.7-50 85-19.8 20.4-50.8 52.6-63 63s-52.9 32.6-89 43-82.3 20-83 57c-.5 29.6 3.4 92.2 5 109s6 4.6 8 27c.9 10 .4 33.4 13 48 11.8 13.7 41.8 58.2 48 62 27.7 5.4 101.6-6.2 79-3 30.6-4.3 42.4-6.2 56-9s18.7-9.7 19-28-3.6-84.9-6-103-5.5-41.2-25-78c23.1-8.3 41.2-16.7 49-23s30.4-32.6 34-39c10 18.2 105.1 185.5 240 307 1.2 1.1 4.8 44.9 6 46 17.7 16.6 36.4 24 55 37-13.7 30.7-29 71.3-29 96s-1.2 71.7 40 114c16 16.9 24.3 12.3 28 11 15 25.9 41.2 59.9 50 69s13.7 16 32 16c2.4 0 7.1-1 18-3z"
		/>
		<path
			fill="#e6e6e6"
			d="M958 1103s145.8 128 178 121c29.1-10.1 66.4-16.2 75-15 2.6-19 12.3-89.7-9-150 26.1 47.6 34.6 84.8 36 114s-10.9 86.4-78 84c-29.1-1.1-74.9-12.2-115-37-52.4-32.4-96.5-80.2-96-80 .9.4 9-37 9-37z"
		/>
		<path
			fill="#e6e6e6"
			d="M629 906c-9.7 3.6-151 57-151 57l-48-84s0-13.2 38-25 94-39 94-39 49 67.1 67 91zm435-138s67.8 20.8 143 99c-48.8-12.5-117-27-117-27s-.1-22.9-7-42c-6-16.7-19-30-19-30z"
		/>
		<linearGradient
			id="ch-full32002a"
			x1={1067.1821}
			x2={528.504}
			y1={757.8793}
			y2={1209.8834}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.206} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#0d0d0d" />
		</linearGradient>
		<path
			fill="url(#ch-full32002a)"
			d="M707 862c89.1 151.1 161.5 232.8 238 304 1.1 1 5 37.8 6 38.8 96.5 88.9 177.1 121.3 228 116.2 91.5-9.3 63.4-137.7 60-156-3.8 36.7-10.2 94-75 94-68.6 0-141.3-47.2-213.6-121.8-.7-.8.3-8.4-.4-9.2-74.9-77.8-151.1-177.9-220-292-15.1 11.7-17.9 14.7-23 26z"
		/>
		<path
			fill="#e6e6e6"
			d="M948 689c-9.6-7.9-54-43-54-43l-5 7s-151.5 19.5-160-30c-16.3 14.2-18 23.2-20 32s18.6 55 76 55 172.6-13.1 163-21z"
		/>
		<radialGradient
			id="ch-full32002b"
			cx={842.011}
			cy={1403.542}
			r={235.444}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.491} stopColor="#000" />
			<stop offset={0.651} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full32002b)"
			d="M765 609c15.5 8.8 70.8 28.7 95 18 4.1 5.4 11.9 16.9 25 24-18.4 3.2-151.1 18.5-157-28 19.2-9.9 24.8-15 37-14z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full32002c"
			x1={772.663}
			x2={839.405}
			y1={1294.1527}
			y2={1425.1406}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#dedede" />
		</linearGradient>
		<path
			fill="url(#ch-full32002c)"
			d="M820 561c-19.3-3.9-84.5-35.4-114-61-10.6 18.1-12.6 55.9 9 77 24.5 19.6 69.8 47.5 103 52s35.2.4 39-2c-6.2-12.9-27.2-46.6-37-66zm94-7c4.7-7.9 14.2-28.8 6-45 12.3 24 15.9 34.7 13 59-11.9-6.9-12-9.6-19-14z"
		/>
		<path
			fill="#ccc"
			d="M483 1088c1.4.7 4.8.4 9-.4 8 13.9 20.1 24.5 20 22.4 1.2-22.2 6.4-49.5 1-142 10.1-7.4 14.6-8.4 25-12 15-7.4 47.6-21.6 129-54 18.8-7.5 33-39.3 43-52-1.2-.3 32.8-24 94-46 2.2-4.5-5.4-7.8-16-14-20 10.2-66.9 31.5-70 34-29.5 21.7-28.8 41.7-58 63-81.5 40.2-164.5 57.5-178 77-15.4-27.3-52-81-52-81s-6.9 60.9 0 123c16.8 52.2 43.4 75.3 53 82zm508-360c7.2 12 20.8 45.8 34 53 10.7 5.8 30.1 8.6 40 25 23.8 39.3 21.4 28.8 26 25-4.4-16.8-8.5-37.5-15-45-11.9-3.4-47.4-13-71-78-6.7 13.7-9.1 13.3-14 20z"
		/>
		<path
			d="M564 817c66.6-28.3 114.5-136 171-120 26.9 23.6 52.2 60.4 57 78 7.6 21.8-58.3 36.7-80 55s-42 61.5-83 74c-21.4-24.4-47.5-68.1-65-87zm441-113c3.1 10.3 26 75.6 69 82-13.6-20.8-52.8-69.6-64-89-.7 2.4-4.6 1.9-5 7z"
			className="factionColorSecondary"
		/>
		<path
			fill="#ccc"
			d="M1006 1250c-7.6 14.2-37.5 73-27 140 8.8 62.2 59.6 88.9 68 80 4.3-4.6 8.3-13.1 12.6-23.6 2.1-5.2 60 78.6 62.4 72.6 3.2-8.1-4.6-57.2-44.8-114.9 16.5-35 41.6-70.1 89.8-70.1 76.4-3.2 113.3 65.3 124 81 3.8-12.7 15-56.5-60-126-21.6 24.3-33.9 32-65 32s-110.6-32.2-160-71z"
		/>
		<radialGradient
			id="ch-full32002d"
			cx={1135.894}
			cy={756.4139}
			r={605.9}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.198} stopColor="#000004" />
			<stop offset={0.349} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full32002d)"
			d="M1006 1250c-7.6 14.2-37.5 73-27 140 8.8 62.2 59.6 88.9 68 80 4.3-4.6 8.3-13.1 12.6-23.6 2.1-5.2 60 78.6 62.4 72.6 3.2-8.1-4.6-57.2-44.8-114.9 16.5-35 41.6-70.1 89.8-70.1 76.4-3.2 113.3 65.3 124 81 3.8-12.7 15-56.5-60-126-21.6 24.3-33.9 32-65 32s-110.6-32.2-160-71z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full32002e"
			x1={1120.8976}
			x2={1059.8976}
			y1={450.0942}
			y2={473.5102}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.701} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32002e)"
			d="M1061 1447c1.6-11.2 11.2-41.5 15-44 7.3 10.2 43.4 52.2 46 117-14.3-18.2-51.9-65.2-61-73z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32002f"
			x1={1226.6663}
			x2={1186.2813}
			y1={305.8723}
			y2={495.8723}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={0.701} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full32002f)"
			d="M1122 1524c22.3-3.9 161.3-33 178-37 12.5-45.4-31.7-153-132-153-55.3 0-81.2 39.1-94 71.5 13.5 13.5 52.5 68 48 118.5z"
		/>
		<linearGradient
			id="ch-full32002g"
			x1={1194.5562}
			x2={1167.8492}
			y1={465.7724}
			y2={603.1644}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.303} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32002g)"
			d="M1076 1405c5.3-16.6 34.3-66.7 87-71s100.8 25.9 137 102c-39.8 7.6-185 35-185 35s-26.3-54-39-66z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full32002h"
			x1={596.3311}
			x2={566.2381}
			y1={801.6973}
			y2={991.6973}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full32002h)"
			d="M516 1114c11.6-.9 131.2-19.2 137-20 0-119.3 10.5-139.7-30-170-23.6 6.6-92.3 31.1-112 43 7.3 17.7-1 13.8 5 147z"
		/>
		<linearGradient
			id="ch-full32002i"
			x1={590.222}
			x2={576.193}
			y1={916.2852}
			y2={982.2852}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32002i)"
			d="M512 966c9.6-7.8 102.6-39.6 111-42 16.6 4.7 31 49 31 49l-137 17s-1.3-19.4-5-24z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full32002j"
			x1={1164.7163}
			x2={1142.5422}
			y1={915.7539}
			y2={1055.7539}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full32002j)"
			d="M1184 1003c2.5-25.7 10.2-95.4-10-126-27-6.9-57.5-11.8-67-14 15 28.1 39.9 70.4 77 140z"
		/>
		<path
			fill="#ccc"
			d="M1212.8 1015.2c-.3-11.2-1-24.1-.8-27.2 7 0 19-4.3 19-14 1.4-37.7 4.7-95.3-22-107-14.1-3.5-101.2-17.8-120-22 3.7 7.8 12.5 18.1 13 19 25.8 51.2 56.8 114.7 78.9 155.2 11.6-2.7 22.2-3.4 31.9-4z"
		/>
		<linearGradient
			id="ch-full32002k"
			x1={1155.1228}
			x2={1155.1228}
			y1={1057}
			y2={996}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.15} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full32002k)" d="m1211 916-77 8-35-61s93.6 14.3 104 20 8 33 8 33z" opacity={0.6} />
		<linearGradient
			id="ch-full32002l"
			x1={1034.3596}
			x2={962.3106}
			y1={1208.9657}
			y2={1253.9877}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path fill="url(#ch-full32002l)" d="M958 668c1.5.5 72-6 72-6l-41 67s-32.5-61.5-31-61z" />
		<path
			fill="#b3b3b3"
			d="M853 580c.1 0 12.9 14.5 14.3 15.5C881.8 605.9 958 666 958 666l28 58-93-78s-3.2 8.3-4 7c-1.7-2.6-24.5-15.3-28-22-6.8-13.2-35-56.7-42-69 5-.6 33.6 18.1 34 18z"
		/>
		<path
			fill="#575656"
			d="M894.7 566.2c16.5-1.7 30.3-3.2 30.3-3.2l-3-2s-13.1 1.3-29.1 3c-5.5.5-3.8 2.8 1.8 2.2zm-31 3.2c-13.9 1.5-24.7 2.6-24.7 2.6l39 69-4-4-40-67s12.6-1.3 28.2-2.9c4.4-.4 5.8 1.9 1.5 2.3z"
		/>
		<path
			d="M802 804c18.7 31 149.5 225 179 258-8.5 11.6-25 36.1-29 65-28.4-23.8-181.2-200.7-220-291 23.5-12.9 53.8-22.3 70-32zm245-10 125 236 15 7s-93.6-200-121-235c-8.4-4.5-19-8-19-8z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full32002m"
			x1={692.678}
			x2={1037.678}
			y1={884.5068}
			y2={1154.0498}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32002m)"
			d="M727 838c22.1 35.2 163.9 241.7 223 287 17.4-27.4 8.6-92.6 122-83-33-63.2-175.3-307.6-198-337-18.6 1.2-112.5 15.1-137-7 29.3 30.9 108.3 85.2 53 112-35.9 13.4-37.1 8.3-63 28z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full32002n"
			x1={1258.0597}
			x2={905.0597}
			y1={1112.9309}
			y2={931.0459}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32002n)"
			d="M1228 1122c-10-33.5-79.5-182.2-169-324-23.2-10.8-48.7-15.2-67-73-14.8-12-42-35-42-35s-10.7 11.5-75 15c42.4 64.2 210.3 364.9 227 396 33.9-4.2 101.3-11.3 126 21z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full32002o"
			x1={821.2703}
			x2={732.9612}
			y1={1049.3707}
			y2={1083.2687}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.198} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32002o)"
			d="M810 783c4.5 12.9 18.9 66.2-12 162-16.9-24.1-65.8-98.7-69-107 12.1-9.8 25.2-16.4 55-25s23.9-24.6 26-30z"
			opacity={0.502}
		/>
		<path
			fill="#838383"
			d="M1180 1019c.9.2 30.4-5.2 31-5s.9 28-12 35c-10-11.3-19.9-30.2-19-30zm-743 8s21.3 33.8 40 60c4.4 4.6 10.6-.3 15 3 12 8.9 21 22 21 22l141-18s6.9 34-18 38-125.1 20.1-135 13-61-79-61-79-3.4-26.7-3-39zm624 420c9.5 12 60 78 60 78l180-38s1.9 28.5-20 36-136.5 31-147 33-17.7-.1-23-2-56.3-65-65-85c7-6.3 8.9-7.4 15-22z"
		/>
		<linearGradient
			id="ch-full32002p"
			x1={1047.9803}
			x2={1086.9803}
			y1={450.2855}
			y2={432.9545}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32002p)"
			d="M1062 1446c6.6 6.5 22 27 22 27l-15 36-24-36s15.3-17.3 17-27z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32002q"
			x1={584.2672}
			x2={575.8482}
			y1={773.8187}
			y2={826.9707}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32002q)"
			d="M507 1144c-5.1 5.4 59.3-.5 91-5s53.3-11.3 55-27 0-19 0-19l-138 21s0 21.5-8 30z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32002r"
			x1={1210.37}
			x2={1198.24}
			y1={371.6857}
			y2={428.5107}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32002r)"
			d="M1122 1534c13.6-2.3 170.7-35 178-36 1 6-1.9 18.3-17 24s-155.1 41.2-172 31c5.4-4.1 8.9-11.6 11-19z"
			opacity={0.502}
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M949 1209c18.7 17 38 28.5 57 41-13.7 30.7-29 71.3-29 96s-1.2 71.7 40 114c16 16.9 24.3 12.3 28 11 15 25.9 41.2 59.9 50 69s13.7 16 32 16c2.4 0 7.1-1 18-3 24-4.8 115.6-24.6 137-31s18-30.3 19-45 1.5-43.6-11-63c4.6-8.8 9.2-27.9-10-65s-39.3-49-50-62c10.2-15.1 24.5-52.9 8-127-10.1-45.2-24.5-85.2-36.3-113.6 3.9-6 10.2-11 10.3-19.4 0-11.1 0-20.6-.1-37.8 9.5-2.2 18-6.2 18.1-13.2.3-17 .5-61.6 0-75-.8-21.2-15.6-31.2-42-52s-45.7-38.2-60-48c-9.8-6.7-34.7-22.4-38-23-2.7-3.5-20.1-9.9-30-12-10.5-12-18.5-21.7-25-31-10.1-14.4-20.1-30.4-25-38 5.9-10.4 23-38 23-38l-102-81 4-8-2-3s5.3-26.6-7-49-26.7-36.6-80-44c-19.9-2.8-57.5-7.2-80-5-37.7 3.8-61 20.7-66 38-7.9 27.7-2.3 58 21 75 19.4 14.1 35.6 22.7 40.5 25.2.5.2.4 1-.1 1.1-8.1 1.7-43.5 10.6-51.3 40.7-21.2 19.5-43 40.7-50 85-19.8 20.4-50.8 52.6-63 63s-70.2 32.8-105 47c-40 16.3-67.8 10.5-65 62 1.6 29.6 1.4 82.2 3 99s38.3 66.5 49 85m224.7-230.8c-8.3 15.1-25.3 33.8-32.8 39.8-7.6 6.1-24.7 15.1-46.7 23.2-1.1.4-1.4.8-.6 1.6 11.3 11.6 29.4 26.9 29.3 44.2-.1 23.4.3 116.7 0 135s-5.4 25.2-19 28-25.4 4.7-56 9-74.6 11.5-80 4c-4.8-6.8-37-49.6-53.2-72.9-3.8-5.4-6.8-6.4-6.8-46.1m267.8-166.7c13.4 23.7 105.5 189.2 239.3 309.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1173 1548c12-9.7 17-28.1 17-37m61-14c0 13.7.6 24.2-15 36"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M558.7 1106c0 7 2.9 31.8-8.7 38m58-8c8.7-5.2 10.9-14.8 9.7-37"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1002 1247c20 17 112.4 81.4 180 73 26.3 0 43.2-24.7 50-37"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1043 1472c9.3-4.3 15.2-16.4 20-39s36.1-92.3 90-99 101.2 18.5 136 78"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1032 1470c10.2-10.8 18.6-53.2 26-72 6.2-15.6 33-46 33-93"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1109 1551c5.4 1 11.4-8 12-25 .4-11.4 2.1-29.2-4-47-13.1-38-29.9-58.9-41-74"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1114 1469 184-35" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1301 1486-178 38-64-83" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M984 1314c4.1 8.2 13.6 25.5 28 45s36.6 37.2 45 37 21 0 21 0m176-28c13.2-4.9 26.2-15.7 23-25"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M430 877c1.5 6.9 50 89 50 89" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m630 905-67-88" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M475 1082c-.3-29.6 0-62.9 0-79s-2.8-37.4 13-44 120.6-46.7 139-53 48.2-26.8 60-48c22.8-40.8 79.9-53.6 100-66 17.2-10.7-7.5-57-57-95"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M476 1088c8.6 1.1 16 4.9 16-8v-85c0-3.4.4-13 5-20 4.4-6.8 45.8-22 60-28 29-12.3 55.8-20 70-26m70.1-50.9c3.4-9.2 21.6-38.2 83.9-56.1 46.6-10.3 23-46 17-56-7.3-12.2-34.4-43-65-59s-62.2 25.7-75 38"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M501 1146c13.9 5.8 12.8-22.6 13-64 .3-68.3 3.7-105.3-3-114"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m490 1085 23 28 139-20m530.6-74.8c18.6-2.6 29.4-4.2 29.4-4.2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M991 726s33.1-54.6 39.1-64.5c.6-1-.1-1.5-.1-1.5s-43.6 4.3-73 7c5.6 13.8 20 38.9 33.1 61.6.4.7 2.5.7 2.9 1.4 6.3 10.9 10.6 22 15 30 8.9 16.3 23.9 25.4 39.6 32.5 4.8 2.1 13.2 5.9 17.4 10.5 14.2 24 102.7 183.5 113 211 7.7 14.1 13 27 13 27"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1212 1011c.1-9.1 0-52.5.1-73.8.1-26.5-.7-52.9-13.1-55.2-24.3-6.7-100-21-100-21"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1089 841c14 2.4 88.7 16.9 122 27" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1062 768s4.9 5.8 10 11 17.3 26.7 18 51v10" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1006 706c4.7 16.3 20.2 68.4 70 80" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M710 651c1.2 17.5 12.7 62.3 88 59s133.6-10.7 146-15c2.6-1.8 5-5 5-5l41 37"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M729 624c4.2 11.2 13.1 45.4 158 27" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m861 630-43-70 92-9 26 17" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m895 648-40-67 22-2-26-21" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m879 555 28 22 26-2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m951 692-57-48-5 7-26-18-4-6s-37.8 15.4-98-20" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m959 668-97-76" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M705 499c31.3 26.5 88.3 55.2 115 61m93-9c7-5.7 12-27.3 10-36"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M764 539c-8.8 7.9-17 15-17 15s-39.5-23.8-47-32" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1237 1151c3.1 24.8.9 101.3-61 107-40.2 3.7-110.6-.2-225.2-114.9m.2-16.1c-62.9-61.1-130.9-136.9-220-288"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M804 804c39.3 83.4 135.3 197.5 178 256-49.3 63.3-31.7 148.7-31 150m239 110c13.4-28.8 13-50.7 7.9-67.9"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M900 953c37.5-20.2 87.2-57.2 205-42" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M958 1102s3.1 2.5 6 5c31 29.3 103 91.3 172 117 23.6-9.6 45.6-13 74-15 6-35.1 13.1-78.7-14-161-.8-2.3-2-5-2-5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1044 792c19.2 31.7 114.7 217.8 127 237 13.6 6.4 26.5 15.3 34.9 26.1"
			/>
		</g>
	</svg>
);

export default Component;

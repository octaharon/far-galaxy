import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1225.6 1325.9c7.2 7.3 16.5 16.3 27 29-2.1 30.3-3.4 54.1-4 76-19.6 5.1-35.6 7.1-42 8-27.4-10.9-124.1-48.5-168-70-7.4-8.1-12.7-16.6-20.1-27.8-101-31.1-192.2-98.7-257.6-185.1-56.3-74.4-102.5-153.4-146.3-207.1-2.4 2.1-12.8 11.7-25.1 23.2-11 5-19.9 7.7-31.9 11.8-14.8 13.8-27 27-27 27l-36-9-71-93 35-44 39 1s26.6-26.1 51-48c-10.9-26.1-16.3-67.2-23-111-14.8-22.1-12-40-12-40l-13-11-7-20 3-22 106-44 42 23h42l-16-10 6-17 169 50s25.5 15.2 54 33c16.3-18.2 63.4-2.8 82 20 25.6 6 35.1 13 45 24 34.1 9.5 51.4 35.2 49 42s-8 22-8 22 29.6 10.9 54.9 20.1c74-16.6 198 2.5 243.1 63.9 32.7 44.6 15.4 115.8-14.7 162.2 8 13.1 23.8 26.5 7.7 78.8-20.8 21.4-55.1 31-72.6 35.9-24 38.4-15.6 57-14.5 61.5 2.5 18.6 9.1 28.2 9.1 47.6 0 23.8-7.7 70.7-55 99z"
		/>
		<path
			fill="#d9d9d9"
			d="M819 1224c-36.5-34.4-80.3-90.9-207-276-83.5-132.1-86.6-226.8-87-240 27.4 23 62.7 47.2 71 51s19.7.3 23-10c3.7 18.4 9.1 47.9 16 46s19-19 19-19l8 38 5 44 39 24s67.1 63.2 91 84c46.2 36.3 195.8 169.9 412 166 57.5-8.3 61.9-10.7 72-13-8.3 14.5-17.9 38.8-12 63s27.8 95.7-42 144c-19.8-15.3-40-35-40-35l-40 9 5 44-16 21s-179.9-7.7-317-141z"
		/>
		<path fill="#dadada" d="m947 711 147 99-10 22-148-108 11-13z" />
		<radialGradient
			id="ch-full66000a"
			cx={1158.366}
			cy={111.179}
			r={124.95}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.75} stopColor="#9e9e9e" stopOpacity={0} />
			<stop offset={1} stopColor="#1a1a1a" />
		</radialGradient>
		<path
			fill="url(#ch-full66000a)"
			d="M1225 1326c18.1-11.7 76.3-58.7 43-147-13 24.1-25 48.6-102 63 1.3 9.8 3.7 37.2 3 53 9.1-1.3 17-3 17-3s26.8 24 39 34z"
			opacity={0.702}
		/>
		<radialGradient
			id="ch-full66000b"
			cx={832.0267}
			cy={977.6431}
			r={419.7089}
			gradientTransform="matrix(0.9271 0.3748 0.1824 -0.4511 68.8808 1332.0353)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4645} stopColor="#dadada" />
			<stop offset={0.8547} stopColor="#292929" />
		</radialGradient>
		<path
			fill="url(#ch-full66000b)"
			d="M795 1110c0 25.9 10 94 22 114 20.8 18.3 136.6 127.8 320 141 7.5-9.4 17-20 17-20l-6-44 21-7s-.5-40.5-4-52c-63.2 1.7-138.8 41.3-370-132z"
		/>
		<radialGradient
			id="ch-full66000c"
			cx={1318.7216}
			cy={1356.9165}
			r={135.0947}
			gradientTransform="matrix(0.9885 -0.1509 -8.446949e-02 -0.5532 41.5475 2028.2952)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.528} stopColor="#292929" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full66000c)"
			d="M1280 1120s-16.3 30.1-13 58c-11.7 24.5-19 46.4-102 65 3.1-54.4 29.8-93.5 46-113 32.7-1.6 69-10 69-10z"
		/>
		<radialGradient
			id="ch-full66000d"
			cx={860.6916}
			cy={1358.1743}
			r={306.2211}
			gradientTransform="matrix(0.9365 0.3506 0.1444 -0.3858 34.5558 1224.7985)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5588} stopColor="#000" />
			<stop offset={1} stopColor="#383838" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full66000d)"
			d="M1206 1131c-45.8-.6-201.4 5.6-410-165-.5 22.4 0 144 0 144s197.6 169.2 368 131c8.1-37.7 16.8-75 42-110z"
		/>
		<radialGradient
			id="ch-full66000e"
			cx={909.6693}
			cy={1386.3823}
			r={390.7834}
			gradientTransform="matrix(0.9564 0.2921 0.1353 -0.4428 1.7459 1276.5981)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={0.9849} stopColor="silver" />
		</radialGradient>
		<path
			fill="url(#ch-full66000e)"
			d="M1347 1002c13.2 13.5 23.1 33.2 11 80-17.4 19-135.1 80.1-312 25-38.6-12-120.7-37.8-250-139-52.6-44.8-69-70-69-70l189 113s42-44.5 67-60c21.5-9.6 64.7-48.7 99-45.9 11.7 1 32.3-8.3 43-3.6 36.6 24.8 61.8 66.2 73.4 68.5 89.6 7.3 135.4 18.5 148.6 32z"
		/>
		<radialGradient
			id="ch-full66000f"
			cx={832.1441}
			cy={729.7919}
			r={204.8063}
			gradientTransform="matrix(0.7985 0.602 0.5676 -0.7528 26.1216 948.567)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#dadada" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full66000f)"
			d="M1347 1002c13.2 13.5 23.1 33.2 11 80-17.4 19-135.1 80.1-312 25-38.6-12-120.7-37.8-250-139-52.6-44.8-69-70-69-70l189 113s42-44.5 67-60c21.5-9.6 64.7-48.7 99-45.9 11.7 1 32.3-8.3 43-3.6 36.6 24.8 61.8 66.2 73.4 68.5 89.6 7.3 135.4 18.5 148.6 32z"
		/>
		<linearGradient
			id="ch-full66000g"
			x1={757.0087}
			x2={622.0087}
			y1={-211.2692}
			y2={-92.1672}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={0.193} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full66000g)" d="M798 968s-82-71.1-92-86c-16.4-11-43-24-43-24l42 77 35 31 56 42 2-40z" />
		<linearGradient
			id="ch-full66000h"
			x1={1277.5}
			x2={1277.5}
			y1={-149}
			y2={-312.1707}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={1} stopColor="#e5e5e5" />
		</linearGradient>
		<path
			fill="url(#ch-full66000h)"
			d="M1177 949c12.9-44.4 51.9-180.8 167-130 28.4 27.6 30.9 51.5 34 62-20.3-23.6-30-43.5-69-46s-94.3 57.5-101 136c-10.6-1.4-18.7-3.6-31-22z"
		/>
		<linearGradient
			id="ch-full66000i"
			x1={1294.6151}
			x2={1294.6151}
			y1={-116}
			y2={-282.9785}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full66000i)"
			d="M1208 970c1.1-15 20-122.2 94-132s78.2 57.1 79 63 1.3 43.9-31 103c-10.9-8.3-35.1-17.8-43-19 32.6-9.7 56.4-22.8 60-49s4.9-69.9-51-77-65.3 61-66 69 5.9 37.4 20 49c-30.1-3.6-45.9-5.9-62-7z"
		/>
		<linearGradient
			id="ch-full66000j"
			x1={1307.5771}
			x2={1307.5771}
			y1={-141.1632}
			y2={-261.1317}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#656565" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full66000j)"
			d="M1298 978c-17.7-13.8-30.1-34-23-57s39.3-66.5 90-24c-1.4-16.8-18.2-36.5-43-38s-62.9 9.3-71 59c-5.4 40.7 16.7 54.8 20 58 7.3 1.8 17.3 4.1 27 2z"
		/>
		<linearGradient
			id="ch-full66000k"
			x1={1122.6536}
			x2={1238.5706}
			y1={-174.3544}
			y2={-352.8513}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.8744} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full66000k)"
			d="M1326 813c-26.8-7.7-103.8-25.7-150 138-19.7-21.8-60.3-59.9-69-65-9.1-15.1-23.6-41.6-22-50s10.9-42.2 38-59c30.2-4.7 136.6-16.5 203 36z"
		/>
		<linearGradient
			id="ch-full66000l"
			x1={1059.9915}
			x2={1291.9915}
			y1={275.4526}
			y2={226.6047}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.333} stopColor="#989898" />
			<stop offset={0.501} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full66000l)"
			d="m1247 1431 4-75-41 6-62-60 4 44-14 19s-105-17.5-119-22c11.2 17.3 17 26 17 26l171 70 40-8z"
		/>
		<path
			fill="#d8d8d8"
			d="M994 727c-6.4-5.7-12.5-16.5-12-21-5.2 1.2-37-11.8-45-25-11.6-3.8-36-18.6-39-24-5.1 4.9-8 11-8 11l81 53s29.4 11.7 23 6z"
		/>
		<linearGradient
			id="ch-full66000m"
			x1={762.5997}
			x2={1046.8687}
			y1={-240.4664}
			y2={-62.7664}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.609} stopColor="#bfbfbf" />
			<stop offset={0.695} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full66000m)"
			d="M715 892c31.7 32.2 150.3 145.7 281 190-5.3-66.1-5-132-5-132s-63.2 43-75 61c-25.3-13.5-232.7-151.2-201-119z"
		/>
		<linearGradient
			id="ch-full66000n"
			x1={492.3185}
			x2={772.3185}
			y1={21.1791}
			y2={-182.253}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.496} stopColor="#000004" />
			<stop offset={0.701} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full66000n)"
			d="M817 1224c-25.5-26-82.2-102.7-141.2-187.5C623.1 960.8 566.6 886.2 537 790c39.8 48.8 205.3 281.6 259 330 1.5 36.4 7.9 69.6 21 104z"
		/>
		<linearGradient
			id="ch-full66000o"
			x1={701}
			x2={701}
			y1={-295.698}
			y2={-363.698}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full66000o)"
			d="m753 768-5-22s-7.9 16.5-45 24-51.2-11.5-54-16c2.7 19.2 14 60 14 60l90-46z"
		/>
		<linearGradient
			id="ch-full66000p"
			x1={663.3264}
			x2={629.3264}
			y1={-368.5451}
			y2={-360.8201}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.197} stopColor="#595959" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path fill="url(#ch-full66000p)" d="m651 779-6-41-28-10 12 65s16.4-1.2 22-14z" />
		<linearGradient
			id="ch-full66000q"
			x1={630.2095}
			x2={599.2095}
			y1={-396.6861}
			y2={-389.6431}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.197} stopColor="#595959" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path fill="url(#ch-full66000q)" d="m619 753-6-41-25-11 10 61s15.2-.7 21-9z" />
		<path
			fill="#b3b3b3"
			d="M1205 1024c3.1 5.3.5 37.1-27 10-7.1-38.6 23.9-15.3 27-10zm55-2c-8.3-14.7 4-30.6 22-9 11.3 25.8-12.7 23.3-22 9z"
		/>
		<linearGradient
			id="ch-full66000r"
			x1={603.3019}
			x2={491.2871}
			y1={-188.2538}
			y2={-97.2537}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.094} stopColor="#4d4c4c" />
			<stop offset={0.701} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-full66000r)"
			d="M530 1012s9-11.8 27-26c8.7-6.9 22.1-9 31-15 15.7-10.5 25-23 25-23l-15-27s-10.7 11.2-24 23c-12 4.4-26.2 8.8-33 11-26.4 20.6-48 48-48 48l37 9z"
		/>
		<linearGradient
			id="ch-full66000s"
			x1={444.2059}
			x2={618.2058}
			y1={-141.6863}
			y2={-315.6862}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.404} stopColor="#e6e6e6" />
			<stop offset={0.951} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full66000s)"
			d="M547 818c5.2 18.8 45 95.9 51 105-3.2 3.3-12.4 12.4-23.9 23.7-11.2 2.5-22.3 5.7-36.1 10.3-23.7 21.3-45 43-45 43l-69-91 34-43 40-2s29.8-32.6 49-46z"
		/>
		<path fill="#979799" d="m573 944-75-78h-37l79 88 33-10z" />
		<path
			fill="#d9d9d9"
			d="m1071 876 10 27s-86.2 18.3-166 109c-51.2-31-249-155-249-155l-1-14 248 149 158-116zM717 646l-4-14-53 26 14 11 43-23zm-131 36-51-17-42-34 6 25 30 25 60 17-3-16zm91-115 152 72-37 2-123-59 8-15z"
		/>
		<linearGradient
			id="ch-full66000t"
			x1={565.3655}
			x2={534.0405}
			y1={-434.1039}
			y2={-340.1039}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.5} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full66000t)"
			d="m586 698 12 63s-54.3-31.3-71-53-13.4-37.6-13-41c9.8 10 15 13 15 13l57 18z"
		/>
		<path d="m913 992 105-79-209-144-139 76 243 147z" className="factionColorPrimary" />
		<path
			d="m657 659 57-27-42-23-135 56 51 17s10.5-31.2 69-23zm-161-25 130-54-24-12-108 44 2 22zm203-62c2.2-.1 154 45 154 45l30 25-133-40s-53.2-29.9-51-30z"
			className="factionColorSecondary"
		/>
		<path
			fill="red"
			d="M1323 878c26.3-2.8 49 14.9 45 48s-23.9 52.2-52 54.8c-28.1 2.5-39.2-14.3-42-40.8-3.2-30 4.9-57.3 49-62z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="#F00;#F33;#C00;#F30;#F30;#f00"
			/>
		</path>
		<path
			fill="none"
			stroke="#fff"
			strokeLinecap="butt"
			strokeWidth={10}
			d="M1325.6 901.5c14-.3 22.9 10.5 22.9 23 0 12.4-8.8 26.7-22.9 32.1-16.3 6.3-32.1-3.2-32.1-21.1 0-18 16-33.7 32.1-34z">
			<animate
				fill="freeze"
				attributeName="opacity"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="1;1;0.8;0.8;0.6;1"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dashoffset"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="0; 50; 0"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dasharray"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="3 5 3; 3 3 3; 3 10 10; 3 5 10; 3 3 3; 3 5 3"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-width"
				attributeType="XML"
				dur="8s"
				repeatCount="indefinite"
				values="10;5;10"
			/>
		</path>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1224 1326c7.2 7.3 16.5 16.3 27 29-2.1 30.3-3.4 54.1-4 76-19.6 5.1-35.6 7.1-42 8-27.4-10.9-124.1-48.5-168-70-7.4-8.1-12.7-16.6-20.1-27.8-101-31.1-192.2-98.7-257.6-185.1C703 1081.6 656.8 1002.7 613 949c-2.4 2.1-12.8 11.7-25.1 23.2-11 5-19.9 7.7-31.9 11.8-14.8 13.8-27 27-27 27l-36-9-71-93 35-44 39 1s26.6-26.1 51-48c-10.9-26.1-16.3-67.2-23-111-14.8-22.1-12-40-12-40l-13-11-7-20 3-22 106-44 42 23h42l-16-10 6-17 169 50s25.5 15.2 54 33c16.3-18.2 63.4-2.8 82 20 25.6 6 35.1 13 45 24 34.1 9.5 51.4 35.2 49 42s-8 22-8 22 29.6 10.9 54.9 20.1c74-16.6 198 2.5 243.1 63.9 32.7 44.6 15.4 115.8-14.7 162.2 8 13.1 23.8 26.5 7.7 78.8-20.8 21.4-55.1 31-72.6 35.9-24 38.4-15.6 57-14.5 61.5 2.5 18.6 9.1 28.2 9.1 47.6 0 23.8-7.7 70.8-55 99z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1014 1340c14.6 5.6 110.9 22.8 124 24 8.7-8.3 15-19 15-19l-6-46 39-7 42 37"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1149 1303 60 59-4 76" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1211 1362 36-6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1191.8 966.7c14.9 7.7 51.4 6 85.2 11.3 31.4 4.9 58.7 15.1 72.3 25.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1125 777c-15.3 3.6-43.1 43.5-40 63s29 52 29 52" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1177 948c11.5-35.8 51.9-183.4 165-128 4.1 2.9 11 8 11 8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1207 970c5.8-47.1 28.9-95.2 61-119 21.5-16 44.9-16.2 70.2-9.5 16.3 4.4 38.6 24.5 42.8 45.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1270 976c-10.2-10-32.3-36.9-12-79s62.6-42.7 81-34 42.2 36.5 24 81-64.7 37.6-70 37"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1370 916c-.6-20.6-29.3-53.2-72-30s-25.8 100.8 18 95" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m458 866 84 90" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M494.9 861.5s60.8 64 79 83.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m511 665 19 16 54 16" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m585 681-50-15s-41.7-37.1-41-37" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m655 658 59-26s.9-1.1.7-1.2C709.1 627.7 639 589 639 589"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m675 587 118 54s66.7 2.2 99 21c2.5-4.5 6-8 6-8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m715 631 2 14-44 21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m536 664 138-55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m495 634 130-52" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M858 649c28 15.8 96.4 58 108 69" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m863 650-68-26-117-58" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m744 599 153 49" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M700 877c91.9 92.4 182.3 160.3 269 201 115.4 54.2 222.2 64.9 304.7 42.9 4.9-1.3 8.6-2.3 11.3-3.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M818 1224.2c-31.4-61.7-23-175.1-23-256.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M856.7 1042.3c15.8 8.4 28.7 33.4 28.7 55.8s-12.9 33.7-28.7 25.2c-15.9-8.4-28.7-33.4-28.7-55.8s12.8-33.7 28.7-25.2z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M857 1066v32" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M569.7 760.2c.9 15.9 1.4 29.8 2.5 47.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M585.1 778.5c.9 16 1.4 29.8 2.5 47.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M601.3 792.7c.9 15.9 1.4 29.8 2.5 47.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M523 704c9.4 12 31.2 31.7 75 55" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M686 1051.2c-15-27-21.6-66.2-24.3-107.4C624.8 910.4 571.1 843.4 537 785"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M740 966v74l-35-42v-65l35 33z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m740 1002 54 56" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M797 1108c155.7 125.8 246 137.7 305 138 54.1.3 157.8-10.2 166-75"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1168 1297c0-14.2 2.5-44.6-5-55 1.6-30.3 24.4-89.3 46.1-110.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1144.6 1166.3c7.3 1.8 3.9 15.5-.3 32.4s-6.4 30.9-14.9 28.8-3.9-15.5.3-32.4 7.6-30.6 14.9-28.8zm76.1-4.2c6.4 2.6.6 15.6-5.3 30.4s-9.6 27.3-17.1 24.3-1.6-14.6 4.3-29.4 11.7-27.8 18.1-25.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M830.7 1133.3c1.8 19.6 4.8 45.8 10.4 58 7.6 5.7 16.1 13 16.1 13s-7.4-34.9-9.6-57.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M867.6 1159.7c1.7 19.9 4.8 49.5 10.7 62.5 7.6 5.7 16.1 12 16.1 12s-8.8-41.5-10-62.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1320 1104s-5.4-13.2-7.7-17.8c-33.1 8.2-45.1 16.1-106.3 23.8-3.7 9.4-9 19-9 19"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1264 1123c8.5-35.3 5.9-77-106-192-44.8-46.1-331.8-266.2-495-340"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M711 704c90.8-39.5 122-37.7 235-2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1181.2 1014.4c5.9-5.2 16.1-3.2 22.7 4.5 6.6 7.6 7.1 18 1.1 23.2-5.9 5.2-16.1 3.2-22.7-4.4-6.6-7.7-7.1-18.1-1.1-23.3zm77.9-11.7c5.8-4.9 15.7-1.9 21.9 6.3 5.9 7.8 6.4 17.5 1.3 22.2-5.2 4.7-14.6 2.8-21.1-4.8-7-8-8-18.8-2.1-23.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M612 947c-10.8-16.3-54.1-95.1-65-132" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M495 1000s22.6-22.3 45.3-44.8c11.9-3.2 20.8-6.1 33.3-10.1C587 931.9 597 922 597 922"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1073 876 9 26s-76.3 15.1-167 109c-72.1-41.4-248-155-248-155l-4-16"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m912 992 158-116-223-159-186 96 2 28 249 151z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m665 844 205-108" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m808 768 212 146" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M994 743s64.8 24.6 119.1 45.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1103.1 801c-23.8-17.1-43-30.8-50.1-35.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M587 692c1.6 8.2 10.8 15.7 29 16 7.3-9.5 24.1-23.9 64-22"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M617 707c-2.7 4.9-6.4 28.6 33 30 17.3-17.9 34.2-20.4 58-23"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M650 737c-7.9 15.4 7.9 50 75 26" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M721 764c20-9.5 35-22.8 21-40-8-8.2-23.5-11-33-11 1.4-10.8-2.4-27.1-32-28 1.3-13.6-11.2-36.1-61-24-30 6.7-30.9 28.2-30 33s9.5 54.9 11 61c.8 3.4 1.6 6 3.7 6.4 1.7.3 6.2 1.1 10.3-2.4 3.8-4.4 9-11 9-11l6 33s1.6 10.2 6.1 10.7c1.8.2 7.4-.8 10.9-4.7 6.5-7.3 9-12 9-12l9 41"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M666.3 747.6c-4.3-7.8 10.4-13.8 25.4-17.7 15-3.8 31.4-2.3 35.9 4.6 5.2 7.9-11.6 15.4-22.6 18.6-11.2 3.3-33.7 3.5-38.7-5.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M688 717c23.8-14-13.6-19.3-31-16-12.8 2.4-28.6 6.5-26 16 1.6 5.7 26.2 6.9 37.6 5.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M660 684c4.3-7.7-11.9-14.7-36-9-25.8 6.1-31 18.4 2 21" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1032 709c15.3 5.7 26.4 16.2 25 22s-20.6.3-32-4-26.8-13.6-25-19 16.7-4.7 32 1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m615 716 5 34" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m648 747 5 34" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m748 739 4 27" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M958 666c-9.6-7-36.8-15.9-43-13-2.3 4.2 17.8 14.8 26 18"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M899 647c-3.3 6.7-3.8 16.7 38 34 .3-6.4 4.7-21.6 43-14"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1005 690c-13.9-8.3-49.3-15-50-11-1.1 5.9 25.2 15.5 30 17"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M938 682c9 10.8 19.2 18.1 44 25 2.1-12 8.5-20.7 43-14" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M982 708c.6 8.7 15 21.4 21.9 24.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1096.1 811.9c-60.9-41.2-136.6-92.4-150-101.5-1.4-.9-1.1-1.4-1.1-1.4l125 49"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m946 707-9 17s82.9 58.3 151.1 106.2" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f3f3f3"
			d="M311 1435c61-58.3 212.3-192.1 246-214 10.9 12.5 17 20 17 20l116-61s154.7-40.7 257-15c14.1 6.9 20 13 20 13s30.9 68.2 111 55c62.5 12.7 126.1 9.7 86-93 49.3-37.4 107.2-35.6 125-35 15.6 12.7 45 46 45 46l102 30 1-68 422 95s-18.8-62.5-261-166c-86.9-34.2-249-78-249-78l-174-86-9 15s-161.8-92.8-241-122c-45.7-24.4-72-23-76-23-7.3-2.9-28-10-28-10s-51.4-145.8-70-191c-11.3-10.3-30-19.5-33-12s7 162 7 162 9.1 42.5-76 85c-14.7-5.4-23 0-23 0s-39.7-1.7-37 35c3.3 16 2 29 2 29l-96 113-29-22-34 21 14 52s-149.3 208.2-177 248c-8.4 46.8 29.4 156.8 42 177z"
		/>
		<linearGradient
			id="ch-full80002a"
			x1={738.1518}
			x2={756.0798}
			y1={1375.4703}
			y2={1142.4703}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={0.598} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full80002a)"
			d="m717 544 24 12 68 196s-32.4 9-47 25c-16.7-7.7-63-28-63-28s27-23.8 27-46-9-159-9-159z"
		/>
		<linearGradient
			id="ch-full80002b"
			x1={1620.4293}
			x2={1649.9823}
			y1={656.4517}
			y2={782.4517}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80002b)" d="m1437 1111 423 98s-16.4-23.4-33-35c-27.6-9.3-391-91-391-91l1 28z" />
		<linearGradient
			id="ch-full80002c"
			x1={633.9924}
			x2={414.8104}
			y1={365.9957}
			y2={624.9957}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80002c)" d="m312 1433 246-213-26-46-243 215 23 44z" />
		<linearGradient
			id="ch-full80002d"
			x1={835.1773}
			x2={828.4783}
			y1={715.2264}
			y2={786.2264}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="silver" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full80002d)"
			d="M686 1179c34.7-10 175-41 311-7-56.4-58.5-61-64-61-64l-271 41s11.4 14 21 30z"
		/>
		<linearGradient
			id="ch-full80002e"
			x1={1246.6125}
			x2={1237.0786}
			y1={745.2569}
			y2={846.3049}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="silver" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full80002e)"
			d="M1165 1142s35.5-41.7 161-40c10.4-1.1 13.4-19.3-11-61-50.1-1.3-156.2 26.3-172 45 5.4 14.5 22 56 22 56z"
		/>
		<linearGradient
			id="ch-full80002f"
			x1={582.012}
			x2={641.0701}
			y1={1089.3683}
			y2={1152.3683}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path fill="url(#ch-full80002f)" d="m648 782-24-1s-39.4 1.3-36 35c5.3 20.6 4 28 4 28l56-62z" opacity={0.502} />
		<path fill="gray" d="m974 1182 99 49s-75.3 15.3-99-49z" />
		<path
			fill="#b3b3b3"
			d="M502 1065s58-30.8 83-46c19.1 23.8 65.6 107.5 105 162-36.1 20.2-119 61-119 61l-69-177z"
		/>
		<path fill="#b3b3b3" d="m500 1063-60-88 5 33 115 210-60-155z" />
		<path
			fill="#ccc"
			d="m1431 1176-95-25-44-46s43 1.9 43-10-21-128.2-169-200c3.5-6.3 7-16 7-16l197 111 62 20-1 166z"
		/>
		<linearGradient
			id="ch-full80002g"
			x1={669.8815}
			x2={1098.8304}
			y1={711.7252}
			y2={1012.0781}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a2633" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full80002g)"
			d="M745 838.2c0-63.2 42.1-91.2 100.8-91.2 89 0 155.6 98.9 155.6 98.9s36.5 49.9 71.8 109.3c24 40.4 46.8 85.7 59 120.8 6.9 19.3 83.6 160 10.3 160-55.8.3-77.4 3-187.4-66-9.4-7.4 18.4-.5 41 0C781.8 958.8 773.3 882 745 838.2z"
		/>
		<linearGradient
			id="ch-full80002h"
			x1={669.8815}
			x2={1098.8304}
			y1={711.7252}
			y2={1012.0781}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a2633" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full80002h)"
			d="M745 838.2c0-63.2 42.1-91.2 100.8-91.2 89 0 155.6 98.9 155.6 98.9s36.5 49.9 71.8 109.3c24 40.4 46.8 85.7 59 120.8 6.9 19.3 83.6 160 10.3 160-55.8.3-77.4 3-187.4-66-9.4-7.4 18.4-.5 41 0C781.8 958.8 773.3 882 745 838.2z"
		/>
		<linearGradient
			id="ch-full80002i"
			x1={1278.7772}
			x2={1332.7772}
			y1={823.7682}
			y2={798.0583}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#303030" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80002i)" d="m1292 1107 42-10 12 6-10 46-44-42z" />
		<linearGradient
			id="ch-full80002j"
			x1={960.8979}
			x2={1007.494}
			y1={761.9857}
			y2={725.9857}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.503} stopColor="#2e3135" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80002j)" d="m1012 1179-1 22-65-36 66 14z" />
		<path
			d="m516 1144-235 205s17.5 56.5 20 62c25-25.7 215.7-197.3 243-215-7.5-15.9-28-52-28-52zm918-61 402 95s-73.6-61.2-132-80c-58.4-18.8-271-86-271-86l1 71z"
			className="factionColorPrimary"
		/>
		<path
			d="M967.7 1141.1c-50-50.8-149.5-175.6-162.7-194.1-88.8 41-188 121-188 121s40.8 58.6 60.7 88.2c30.9-9.6 107.6-23.4 151.3-25.2 51.1-2.1 122.4 9.4 138.7 10.1zM1153 1110c12.6-11.9 75.4-45.1 174-38-5.6-26.2-50.6-95.9-71-111-33.7-10.3-203-48-203-48s72.7 127.4 100 197z"
			className="factionColorSecondary"
		/>
		<path
			fill="#f3f3f3"
			d="M783.3 984.3c5.5-2.8 14-.8 19.5 4.6 5.9 5.8 7 13.8 1.7 17.9-5.8 4.4-16.2 2.1-22.3-5.1-5.6-6.7-4.8-14.4 1.1-17.4zm449.9-12.8c4.2-2.3 11.8-.3 17.4 5 6 5.7 7.2 12.8 2.2 15.5-4.9 2.6-13.5-.6-18.7-6.6-4.7-5.6-5-11.6-.9-13.9zM644 1066.7c5.9-3.1 14.1-1.1 18.8 4 4.6 5 4.7 11.9-.2 15.8-5.4 4.2-14.7 3.3-20.3-2.5-5.8-6.1-4.8-13.9 1.7-17.3z"
		/>
		<radialGradient
			id="ch-full80002k"
			cx={1816.8395}
			cy={1213.8578}
			r={188.0428}
			gradientTransform="matrix(0.8598 -0.5106 -1.4743 -2.4824 1152.0499 4992.9858)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1577} stopColor="#333" />
			<stop offset={0.4367} stopColor="#666" />
			<stop offset={0.5342} stopColor="gray" />
			<stop offset={0.6306} stopColor="#e6e6e6" />
			<stop offset={0.6937} stopColor="#999" />
			<stop offset={0.8316} stopColor="#666" />
			<stop offset={1} stopColor="#333" />
		</radialGradient>
		<path
			fill="url(#ch-full80002k)"
			d="M1098.1 1090.6c.3-.3 1.1-.5.9-.9-5.2-9.8-58.6-126.6-121.5-202.3-25.9 8.5-38.9 19.4-54.8 43.9 4.7 39.1 12.3 76.3 30.2 96.1s91.6 72.6 101.4 78.7c8.4.5 21 0 21 0l.3-.3c4.1-7.2 13.5-13.7 22.5-15.2zm-58.5-193.8c-10.3-6.7-14.6-15.2-39.8-12.8 56.7 73.8 96.9 151.5 119.8 201.4 10-1.2 15.7-1 22.9 4.3-12.2-29.3-61.9-133.5-102.9-192.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1100.2 1088.6c.3-.3 1.1-.5.9-.9-5.2-9.8-58.6-126.6-121.5-202.3-25.9 8.5-40.9 19.4-56.8 43.9 4.7 39.1 12.3 76.3 30.2 96.1s91.6 72.6 101.4 78.7c8.4.5 21 0 21 0l.3-.3c4.1-7.2 15.5-13.7 24.5-15.2zm-60.5-193.8c-10.3-6.7-14.6-15.2-39.8-12.8 56.7 73.8 96.9 151.5 119.8 201.4 10-1.2 15.7-1 22.9 4.3-12.2-29.3-61.9-133.5-102.9-192.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M687 1178s157.7-38.7 260-13c14.1 6.9 20 13 20 13s30.9 68.2 111 55c62.5 12.7 126.1 9.7 86-93 49.3-37.4 107.2-35.6 125-35 15.6 12.7 45 46 45 46l102 27m-1-66s404.4 91.8 424 96c-32.7-44.8-60-74.7-261-166-86.9-34.2-249-78-249-78s-129.9-63.9-174.3-85.8c-1.9 3.6-7.7 14.8-7.7 14.8m-12-5s-150.8-87.8-230-117c-45.7-24.4-72-23-76-23-7.3-2.9-28-10-28-10s-51.4-145.8-70-191c-11.3-10.3-30-19.5-33-12s7 162 7 162 9.1 42.5-76 85c-11.5-.4-23-1-23-1s-39.7-1.7-37 35c3.3 16 2 30 2 30l-96 113-29-22-34 21 15 51s-150.3 210.2-178 250c-8.4 46.8 29.4 155.8 42 176 61-58.3 212.3-192.1 246-214m9 23c24.2-13.3 126-62 126-62"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M636 783c-10.1 1.2-38.6 16.8-43 47" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1338 1120 93 23" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1343 1088 89 22" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1351.8 1057.2c20.1 5 68.8 17 81.3 20.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1357.6 1022.9c18.8 5 57.3 15.4 74.1 19.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1053.1 910.9c63.2 14.7 132.9 32.3 203.5 51.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M615 1066.6c69.1-52.8 131.1-93.4 191.9-121" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M783.3 984.3c5.5-2.8 14-.8 19.5 4.6 5.9 5.8 7 13.8 1.7 17.9-5.8 4.4-16.2 2.1-22.3-5.1-5.6-6.7-4.8-14.4 1.1-17.4zM644 1066.7c5.9-3.1 14.1-1.1 18.8 4 4.6 5 4.7 11.9-.2 15.8-5.4 4.2-14.7 3.3-20.3-2.5-5.8-6.1-4.8-13.9 1.7-17.3zm589.2-95.2c4.2-2.3 11.8-.3 17.4 5 6 5.7 7.2 12.8 2.2 15.5-4.9 2.6-13.5-.6-18.7-6.6-4.7-5.6-5-11.6-.9-13.9z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M278.3 1354.2c90.7-77.2 171.3-153.6 240.4-210.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1765 1124c-42.1-23.3-175.3-67.3-330.1-112.4-5.2-2.6-12.9-3.6-12.9-3.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M722.1 712.6s34.4 32.8 55.2 52.7" />
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M761 868c7.6-.5 20.3-6 29-13 13.7 14.2 70 82 70 82s-16.5 41.5-7 74c-29.7-34.8-75-96.4-92-143z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M585 1020s68.5 104.2 109.7 166.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m559 1207 113-55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m539 1172 113-55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M528.4 1131.4c17.4-8.5 72.6-35.3 96.5-46.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M510 1099s63.7-33 95.4-48.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m501 1061 70 180" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1432 1180v-168l-66-25-35 162" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1371 989s-122.1-75.7-176-100" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1435 1014c-13.1-9.1-69.5-44.2-76-48" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1281 1105c13.8-1.3 48.2 1.5 52-8 7.1-17.9-51.6-153.2-179-209"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M588 849c11.1-12.8 29-46 72-72" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M935 1162c21.8 2.8 63 10 63 10S662.1 844.1 768 772c40-28.8 86-24 86-24"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M964 1176c18.2 10.9 96.5 51.5 117 57" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M927 772c86.2 56.3 175.5 203.6 242 379" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M495.6 958.7c34.4 24 86.4 60.3 86.4 60.3l-80 45-70-104"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m446 1008 112 214" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			fillRule="evenodd"
			d="M1069 1394c-32.2 5-61.4 10.6-70 9s-65.3-23-95-13-107.3 123.8-163 153c-76.5 10.8-103 7.9-129 1s-68.5-65.4-175-375c-4.9-28.2 57.1-170.7 85-239 27.8-68.3 72.6-199.5 111-202s44 0 44 0-30.5-68.7-98-107c-9.3-7.7-9.2-16.5-10-28s-3-47-3-47 82 51 123 51c20.4 0 19-19.6 19-30s-.9-18 15-18 65 37 65 37 99-54.4 108-60c1.8 20.8 7.9 49.5 7 67-10.8 24.4-106.7 62.6-96 117 21.5-16.3 70-56 70-56l68-5-13-218s9.3-3.7 15 0c2.2 36.5 14 227 14 227l16 14-9-148s8.4-6 15 1c1.4 13 10 164 10 164s69.2 69.7 85 94 68.7 121.3 89 159c37.1 44.1 214.2 240.9 254 307 56.3 48.7 119.4 93.8 130 130-13.2 15.1-81 44-81 44s-61.9-49.2-117-101c-45.1 21.5-83 39-83 39l-126 19-25 103s-6.7 10.7-36-24c-9.8-12.4-12-21-12-21s-.4-22-2-44zm-398-7 119-117-142 75 23 42zm-83-146 188-126-90-172-98 298z"
			clipRule="evenodd"
		/>
		<linearGradient
			id="ch-full61000a"
			x1={757.1928}
			x2={757.1928}
			y1={-307}
			y2={-344}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#414141" />
		</linearGradient>
		<path fill="url(#ch-full61000a)" d="M718 777c16.9 3.6 62.7 2.9 78-1 .8 12.3 3.8 37-36 37s-38.6-12.9-42-36z" />
		<path
			fill="#dadada"
			d="M569 547c50.4 27.4 123.1 86.5 203 65 .6 4.9 1 10 1 10l-18 43s5.8 26 32 10c7.8-19.4 17.5-39.9 11-51-.4-9.2 2-17 2-17s67.5-68.9 95-80c2.8 15.3 9 60.5 8 68-8.9 10.3-101 68-96 101s7 72.8-1 78-100.8 20-118-24-50.1-96.7-113-133c-8.7-23.1-6.3-42.9-6-70z"
		/>
		<linearGradient
			id="ch-full61000b"
			x1={756.0811}
			x2={756.0811}
			y1={-427.031}
			y2={-506}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#dadada" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full61000b)"
			d="M715 616c10.6.7 43.1-.6 53-2 5.5 3.7 12.1 8.1 5 12-7.5 4.2-14.8 30.3-17 40 4.7 12 17.2 19.8 32 11 8.4-8.9 9-8 9-8s6.7 39.6-66 17c-8.8-40.4-13.4-60.1-16-70z"
		/>
		<radialGradient
			id="ch-full61000c"
			cx={771.5}
			cy={-452}
			r={29}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full61000c)"
			d="M772 639c16.8 0 23.6 19.6 29 33 0 16.8-11.7 25-28.5 25S742 683.3 742 666.5s13.2-27.5 30-27.5z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full61000d"
			x1={754.0031}
			x2={797.0031}
			y1={-467.2702}
			y2={-452.5982}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full61000d)"
			d="M798 645c-1.3 5.8-10 32-10 32s-28 11.7-33-9c5.3-19.8 11-34 11-34s8.5 13.8 32 11z"
		/>
		<linearGradient
			id="ch-full61000e"
			x1={751.704}
			x2={715.704}
			y1={-539.8373}
			y2={-539.8373}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000004" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full61000e)"
			d="M731 584c1.8 10.4 3 18 3 18s-29.8 5.2-36-5c7.2-8.3 12.5-30.1 13-40 2.3 1.3 4.3 2.8 4 4 1.2 3 3.7 7.3 5.5 9 5 4.7 9.4 7.7 10.5 14z"
		/>
		<linearGradient
			id="ch-full61000f"
			x1={715.2232}
			x2={768.3982}
			y1={-472.918}
			y2={-554.8}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.412} stopColor="#000004" />
			<stop offset={0.85} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full61000f)"
			d="M727 578c-8.2-6.7-13.2-17.5-14-25 10.6-4.8 25.4-1.8 47 13s40.1 26 38 43-11.6 26.8-19 23-4.1-19.3-9-28-34.8-19.3-43-26z"
		/>
		<path fill="#7c7c7c" d="m1376 1312 89 104-114-92 25-12z" />
		<path fill="#d9d9d9" d="M1471 1422s69.1-30 78-43c1.2-14-66.6-88.9-124-123 .5 13.5 0 20 0 20l-49 34 95 112z" />
		<path
			fill="#d9d9d9"
			d="M742 1544c50.9-24.9 135.4-151.2 167-156s82.9 14.1 94 14c-8.9-15.8-29-60-29-60s-47.6-23.5-96-6c-100.8 29.8-196.4 201.7-248 211 64.5 2.3 45.7 4.1 112-3z"
		/>
		<path fill="#bfbfbf" d="M977 1343c18.1 4.2 394.1-49.2 441-62-22.5 18-120 74-120 74l-293 49s-21.8-42.2-28-61z" />
		<path
			fill="#d9d9d9"
			d="M994 702c-.3-14.8-9.6-165.5-10-172-6.9 8.6-13.2 2.3-16-2 1.9 24.9 9.7 165.2 10 172 6.2 5.9 6.5 12.8 16 2z"
		/>
		<path
			fill="#d9d9d9"
			d="M962 660.3c-.3-14.8-13.6-217.8-14-224.3-6.9 8.6-13.2 5.4-16 1 1.9 25 13.7 214.4 14 221.3 6.2 5.9 6.5 12.9 16 2z"
		/>
		<linearGradient
			id="ch-full61000g"
			x1={1046}
			x2={1046}
			y1={-2}
			y2={-132}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={1} stopColor="#dadada" />
		</linearGradient>
		<path
			fill="url(#ch-full61000g)"
			d="M1034 988c48.8 0 143 22.5 143 76 0 45.1-113.2 54-125 54s-137-8.3-137-75c0-42 70.2-55 119-55z"
		/>
		<path
			fill="#e5e5e5"
			fillRule="evenodd"
			d="M764 992c36.1 72.9 206 344 206 344-31.4 5.1-55.5-24.7-120 11-85.3 45.4-184.4 207.3-229 199-39.6-.4-88.2-114.6-127-217-30.4-80.1-56.4-153-57-159-1.4-13.7 23.3-109.5 86-241 31.5-72.6 62.4-171.2 96-195 18 19.8 102.3 196.5 145 258zM606 825c-12-1.6-83.3 174.7-102 222-18.7 47.3-40 118.1-36 132s63.8 167.2 87 215 31 54.3 43 56c19.6-11.7 139.7-129.7 176-160s48.4-79 25-130-156-295-177-326c-4.5-6.5-4-7.4-16-9z"
			clipRule="evenodd"
		/>
		<linearGradient
			id="ch-full61000h"
			x1={602.5}
			x2={602.5}
			y1={156.8199}
			y2={-230}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.203} stopColor="#d9d9d9" />
			<stop offset={0.736} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#2a2a2a" />
		</linearGradient>
		<path
			fill="url(#ch-full61000h)"
			d="m687 941-97 298s-67.7 52.1-72 34c4.6-38.3 131.8-366.2 138-383 9 12.5 31 51 31 51z"
		/>
		<linearGradient
			id="ch-full61000i"
			x1={538.5}
			x2={538.5}
			y1={106}
			y2={-297.1018}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.203} stopColor="#bfbfbf" />
			<stop offset={0.651} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#050505" />
		</linearGradient>
		<path
			fill="url(#ch-full61000i)"
			d="M486 1226s122.3-396.8 124-403c-17.3-6.4-139.5 290.8-143 351 11.5 32.5 19 52 19 52z"
		/>
		<linearGradient
			id="ch-full61000j"
			x1={602.5}
			x2={602.5}
			y1={225}
			y2={330.0941}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-full61000j)"
			d="m670 1389-23-44-112 2s36.1 106.7 59 103c19.1-14.6 76-61 76-61z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full61000k"
			x1={675.2244}
			x2={675.2244}
			y1={75}
			y2={224}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#474747" />
			<stop offset={1} stopColor="#000" stopOpacity={0.502} />
		</linearGradient>
		<path fill="url(#ch-full61000k)" d="m651 1344 144-81s25.5-25.4 13-68c-43.8 21.3-269 148-269 148l112 1z" />
		<path
			d="M776 1114c11.5 17.1 30.6 57.4 33 83-37.6 19.6-274 148-274 148l-50-119s122.2-396.7 125-400c13.4 10.8 42.3 57.7 46 64-13 33-146.6 374.6-138 384 4 23.8 202.7-131.6 258-160z"
			className="factionColorSecondary"
		/>
		<path
			d="M878 656s105.4 134.9 123 141c-8.8 35.5-43 172-43 172s-29-8.7-148-198c.2-21.7 2.1-39.7-2-59 13.9-13.5 70-56 70-56z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full61000l"
			x1={1106.5}
			x2={1106.5}
			y1={365.0001}
			y2={260.719}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#737373" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full61000l)"
			d="M1069 1391c24 1.4 64.6-12.4 75-10-3.3 15.9-20.9 104.1-28 104s-33.5-13.8-46-49c-.3-37.8.5-31.2-1-45z"
		/>
		<path fill="#e6e6e6" d="m1168 945-86-158-80 12-45 170s176.1-6.8 211-24z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1069 1394c-32.2 5-61.4 10.6-70 9s-65.3-23-95-13-107.3 123.8-163 153c-76.5 10.8-103 7.9-129 1s-68.5-65.4-175-375c-4.9-28.2 57.1-170.7 85-239 27.8-68.3 72.6-199.5 111-202s44 0 44 0-30.5-68.7-98-107c-9.3-7.7-9.2-16.5-10-28s-3-47-3-47 82 51 123 51c20.4 0 19-19.6 19-30s-.9-18 15-18 65 37 65 37 99-54.4 108-60c1.8 20.8 7.9 49.5 7 67-10.8 24.4-106.7 62.6-96 117 21.5-16.3 70-56 70-56l68-5-13-218s9.3-3.7 15 0c2.2 36.5 14 227 14 227l16 14-9-148s8.4-6 15 1c1.4 13 10 164 10 164s69.2 69.7 85 94 68.7 121.3 89 159c37.1 44.1 214.2 240.9 254 307 56.3 48.7 119.4 93.8 130 130-13.2 15.1-81 44-81 44s-61.9-49.2-117-101c-38.3 23.3-57 33-57 33l-152 25-25 103s-6.7 10.7-36-24c-9.8-12.4-12-21-12-21s-.4-22-2-44z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M598.4 631.9c-1.7-19.8-6.4-73.9-6.4-73.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M750 1494.9c30.6-31.6 65.3-79.2 92.8-104.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1471 1348-61-56" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M993 687v16s-13.1 9.5-15-8c-.5-11.9-1-32-1-32" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 659c0 3-6.7 6.3-14 2-.5-6-1-13-1-13" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1067 1395 81-16" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M678 729c6.5 15 7.2 26.5 30 42s84.3 6.2 97 2c7.9-14.4 11.3-20.4 3-64"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M775.5 731c6.3 0 11.5 5.2 11.5 11.5s-5.2 11.5-11.5 11.5-11.5-5.2-11.5-11.5 5.2-11.5 11.5-11.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M718 778s.6 17.2 6 24 9.2 13.7 46 11c28-2 25.9-24.3 26-34"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M795.6 793.4c16 3.3 26.4 8.6 26.4 14.6 0 9.9-28.7 18-64 18s-64-8.1-64-18c0-6 10.4-11.3 26.5-14.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m799 642-13 35s-23.7 13.1-31-11c4.8-17.6 12-37 12-37" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M807 704c-5.2-22.9-12.6-39.7-13-46" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M771 623c-5.3 1.3-9.9 20.4 14 22s12-21 12-21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m876 538 7 75" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M798 606c11.7-13.9 71.3-65.2 90-75" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M772 614s1.6 13.2 7 17c13.4 9.5 27.6-25.9 11-45" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M623 576c34.8 21 68 50.5 149 37-2.2-6.8 6.7-9.4-38.9-32.1-.6-.3-5.1-3.6-6.1-3.9.6 1.9 4.3 4.4 4.3 5.1.3 7.4 1.2 11.1 4 18.9-3.7 6.4-40 4.5-38.3-5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M715 617s22.2 88.5 24 158" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M685 611c.6 13.6 3 130.4 15 154" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M921.5 933c13.5 19.4 27.7 39.8 42.1 60.6m85.5 122.8c76 109.2 140.9 202.6 140.9 202.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M807 768s125.2 196.1 150 201 189.1-14.5 212-24" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1132.1 922.9-56.7-102.6-52.8 6.5-28 112.4 137.5-16.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1082.2 1162.4c-42.1 5.8-201.5 27.7-201.5 27.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M900.3 900.5c-59.5 8.2-178.6 24.5-178.6 24.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m767 920 163 265" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1057.5 879c2.5 0 4.4 2 4.4 4.5s-2 4.5-4.4 4.5-4.4-2-4.4-4.5 1.9-4.5 4.4-4.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1069 1184.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M913 1206.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1150 1214.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1041 1144.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M737 900.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M859 884.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1314 1150.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1060 1397 36-36 160-29 10 27" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1108 1201c2.4.2 251-37 251-37" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M878 655c2.4 0 109.5 140.1 124 142s80-9 80-9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1001 798-42 167" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M620 736s332.2 604.2 359 608 380-51.5 446-69c-.5-8 1.3-18-5-29"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1356 1321c11-4.6 65.2-36.5 71-45" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1475 1420-100-109" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m974 1342 29 59" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M982 1345c-34.1-14.8-71.5-21.2-102-11-113.1 37.8-195.9 213.9-259 212"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m905 1390-25-57" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M467 1176c12.4 30.8 103.2 294.2 131 274s170.4-151.7 200-190c19.4-25.1 15.5-64.2-7-116-9-20.7-46.8-86.2-80.8-153.9C663 896.1 610.7 803.7 601 824c-16.8 34.9-136.6 293.8-134 352z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m670 1390-24-47" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m810 1195-276 149s.5 1 1.5 1c13.7-.2 116.5-2 116.5-2l145-81"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M610.5 824.4C592.7 882.6 488 1226 488 1226" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M588 1239s83.1-250.2 98.4-298.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M775 1115s-180 119.4-242.4 160.5c-12.8 8.5-16.2-5.6-15.6-11.5s138-373 138-373"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1019 988c24.4 0 126.5 1.5 153 58s-83.3 71-105 71c-55.4 0-152-14.1-152-71 0-52.4 79.6-58 104-58z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1120 1108c-24.6-14.8-118.2-23.4-158-10" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M919 1070c34.4-32.2 165.5-50.1 244.6 17.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M987 992v114" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#e6e6e6"
			d="m618 1561-19-162 23-90s7.1-1.7 20 1c3.9-15.9 27-114 27-114l-19-5s18.8-92.7 41-111c-8.5-6-17.3-18.7-20-26-10.3-1-58-10-58-10s3.5 7.2-5 25-28 50-28 50l-92 64-8-2 17-20-18-4-14 18s-13.6-8-18-17c2-12.2 3-16 3-16l-5-4 1-92 30-53s8.9-14.4 28-17c5.8-9.7 11-19 11-19s-8.9-11.8-4-28c7.8-17.9 25.8-56.6 48-72s36.9-17 52-17 49 9.4 66 44c7.8-4.2 13-8 13-8s-16-16-16-29 20.4-111.5 122-197c.1-6.5 0-36 0-36s-10.4 2-13-1-33-62-33-62-2.2 3.8-7-6-50-104-50-104-18.5-39.8 17-58 52.7 23 58 34 40 80 40 80 16.9-11.3 33-3c.1-17.6 0-79 0-79l-5-3v-48s16.4-5.8 25 1c.1 14.8 0 48 0 48l-5 2v93s18.1 34 36 80c3-3.9 4.5-12.7 32-15l7-21s-13-2-13-6 6-10 6-10 21-7.3 43-3c.3-7.5 0-12 0-12l-33-5v-63l41-23s4.3-31.3 5-37 11.7-32.1 44-29 29.7 43.8 29 48c8.2-.4 16 0 16 0l-1 113-27 1 2 87s157.7 54.5 187 240c-1.3 4.5-3.1 10.8-5 14 10.6 6.7 32.6 21.7 38 48-.1 21.7-2 39-2 39l-41 19s15.2 17.3 17 38c-.6 17.3-1 25-1 25l10 4-2 29-6 9 21 4s-18.9-70.6 29-97 82.8-24.2 114 8c24.7 32.1 47 82 47 82s-.2 9.1-11 19c7.2 14.1 35 72 35 72s14.8-5.8 24 6c9.6 16.5 35 71 35 71l11 140s-.7 8.5-6 13c-2.8-1.2-5-3-5-3l-4-13s-13.6.9-20 6c3.7 8.4 10 22 10 22l-7 4-21 5-10-10-4 5-102-94-36-74s-5.6-9.6 10-19c-7.3-15.9-18-37-18-37l-236-48s-39 15.6-169 12c-42.3 39.3-116 110-116 110l-19 82s18.6 2.5 17 9-20 89-20 89l-91 138s-8.4 1.5-14 0c2.7-7.3 5-10 5-10s-20.1-4.8-26-6c-1.1 6.6-4 15-4 15s-23-6.6-30-11c.6-4 1-8 1-8l-10 1zM972 453l-22 11v42l20 1 2-54z"
		/>
		<linearGradient
			id="ch-full99001a"
			x1={587.042}
			x2={803.041}
			y1={551.5001}
			y2={497.4851}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full99001a)"
			d="m667 1197 149 34-26 112 17 10-21 88-90 141h-14l26-54-40-9-10 62-32-12 7-60s-23.5-10.4-22-10-11-102-11-102l23-87 20-2 24-111z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full99001b"
			x1={432.189}
			x2={588.686}
			y1={885.0286}
			y2={795.6186}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full99001b)"
			d="M514 959s14.4 12.2 31 23c17.9 11.7 38 22 38 22s-8.1 12.8-5 27c1.4 6.3 13.7 9.9 24 11 2.6 7.3-6 21-6 21l6 19-34 46-78 56-9-2 12-22-14-3-13 20-18-16-2-23 2-90 20-38 18-3 28-48z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full99001c"
			x1={1330.5304}
			x2={1562.5304}
			y1={594.3823}
			y2={718.9363}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full99001c)"
			d="M1312 1151s35.8-.6 70-20c32.3-18.3 57-38 57-38s23.3 47.8 35 73c8.9-3.3 22 3 22 3l36 75 12 140-5 12-7-3-14-35-18 12 19 39-28 9-11-11s-3.4 7.8-2 7-102-95-102-95l-32-66 8-18-40-84z"
			opacity={0.2}
		/>
		<path
			fill="gray"
			d="M668 1519c.5 0 42 9 42 9l-22 42-26-5s5.5-46 6-46zm-57-18 24 10-6 49-10 2-8-61zm875-94-25-26-14 1 26 30 13-5zm13-37 21-10 7 20-19 7-9-17z"
		/>
		<path
			fill="#d9d9d9"
			d="m635 1511-7 58 29 12 12-61-34-9zm74 18-25 50 15 1 28-49-18-2zm752-146c.5.1 32 32 32 32l25-7-18-37s-39.5 11.9-39 12zm58-26 13 36h8l4-11-5-39-20 14zM452 1145l25 13-12 16-19-15 6-14z"
		/>
		<path fill="#8d8d8d" d="m851 1146 72 6-113 110 6-31 17 4s6.5-20.7 11-43c4.5-22.4 7-46.4 7-46z" />
		<linearGradient
			id="ch-full99001d"
			x1={1202.4137}
			x2={1223.4917}
			y1={703.611}
			y2={812.0471}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</linearGradient>
		<path
			fill="url(#ch-full99001d)"
			d="M1212 1086s6.2 3.6 15 3c5.3-.3 8.7-6.9 14-8 8-1.7 17 2 17 2s25.6 51.4 36 68c9.1 2 19 2 19 2l16 36-233-49s92.6-22.7 116-54z"
		/>
		<linearGradient
			id="ch-full99001e"
			x1={616.1912}
			x2={629.9922}
			y1={848.4601}
			y2={919.459}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0.2} />
		</linearGradient>
		<path
			fill="url(#ch-full99001e)"
			d="M606 1010c24.5-16.9 30-23.9 36-27 7.7 11 13.5 20.5 27 30 0 0-.5 28.9 0 41-27.6-5.8-58.3-10.2-86.4-16.1-14.8-17 2.4-33.9 2.4-33.9s11.2 1 21 6z"
			opacity={0.8}
		/>
		<path fill="#d9d9d9" d="m933 554-14 29s10.1 13 37 13 35-12 35-12l-13-29-45-1z" />
		<path
			fill="#d9d9d9"
			d="M891 588c8.2 5.7 75.7 32.7 126 3 4.9-7.5 8-62 8-62l-18-3-71-12-1-60 72-38 61-4 87 24v66l-84 24h-29l2 87s-12.7 7.3-22 0c-7.4 8.8-73.3 31.4-134 4 1.1-15.2 2.5-21.9 3-29zm81-136-22 11 1 43 17 2 4-56zm101 60 65-22v-42l-65-17v81zM835 365s18.8-4.4 25 1c.7 17.4 1 48 1 48l-4 5 6 333s-6.6 17-16 17-8-5.9-8-12 2-342 2-342l-6-4v-46zm-26 140 6 293s-5.8 19-12 19-6-3.7-6-13-2-299-2-299h14zM691 879s77.2 64 239 64c4.3 4.1 7 8 7 8l30-2-1-10 52-3s-54 12.7-54 88c-11.5-9.9-25.8-26.2-32-31s-22-13-37-13-42 1-42 36c7.8 23.4 32 45 32 45s13.1 19 34 19 39-26.6 39-42c0-15.4 8 1 8 1s14.4 38.1 55 62c-14.7 3.9-126.8 5-172-5-12.3-10.6-37.2-32.8-54-35-16.8-2.2-66-16.2-103 19-8.3-3.1-19.9-18.1-23-27 .6-13.3 1-37 1-37s20 12.2 32 15 61 10 61 10l-1-82s-32.7-7.5-50-27c-18.2-20.5-21-53-21-53zm157 235c18 3.5 156.1 12 204 4 38.8 4.1 66.8.4 88-22 27.1-11.1 66-32.1 70-35 6.4.3 14 3 14 3l21-19v25l-17 20-15-4s-46.5 40.7-122 55-195.8 16.7-238 4c-.7-17.5-2.9-25.4-5-31zm311-60c7.8 2.7 27-6.4 27-25s-17.5-17.5-30-15c-4-11-24-37-24-37s-44.7-37.6-57-47c8.3.4 57-15 57-15s11.6 8.5 11 9 20-6 20-6 1.6-3.7 3-9c29.4-5.6 54.7-34.2 55-35s-7 154-7 154-11.1 16-14 21-44 23-44 23 1.3-10.2 3-18z"
		/>
		<radialGradient
			id="ch-full99001f"
			cx={951.943}
			cy={1514.0239}
			r={1149.15}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.452} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full99001f)"
			d="M691 879s77.2 64 239 64c4.3 4.1 7 8 7 8l30-2-1-10 52-3s-54 12.7-54 88c-11.5-9.9-25.8-26.2-32-31s-22-13-37-13-42 1-42 36c7.8 23.4 32 45 32 45s13.1 19 34 19 39-26.6 39-42c0-15.4 8 1 8 1s14.4 38.1 55 62c-14.7 3.9-126.8 5-172-5-12.3-10.6-37.2-32.8-54-35-16.8-2.2-66-16.2-103 19-8.3-3.1-19.9-18.1-23-27 .6-13.3 1-37 1-37s20 12.2 32 15 61 10 61 10l-1-82s-32.7-7.5-50-27c-18.2-20.5-21-53-21-53zm465 193s41.1-18 44-23 14-21 14-21 7.3-154.8 7-154-25.6 29.4-55 35c-1.4 5.3-2 8-2 8s-21.6 7.5-21 7-11-9-11-9-48.7 15.4-57 15c12.3 9.4 57 47 57 47s20 26 24 37c12.5-2.5 30-3.6 30 15s-19.2 27.7-27 25c-1.7 7.8-3 18-3 18z"
		/>
		<linearGradient
			id="ch-full99001g"
			x1={1039.1051}
			x2={1039.1051}
			y1={849.938}
			y2={1009.938}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.499} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full99001g)"
			d="M1012 1093c-55-107.8 66-158.9 103-124-2.6-10-25.3-34.2-46.1-36-21.9 1.5-62.1 8-62.1 8s-64.1 23.9-36.8 112c15.5 18 34.4 36.4 42 40z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full99001h"
			x1={860.0443}
			x2={929.3873}
			y1={871.1641}
			y2={954.3632}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.499} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full99001h)"
			d="M884.4 1064c-27.6-53.9 42.1-77.5 60.6-60-1.3-5-12.9-13.5-28-21-16.7-3.1-34-2-34-2s-41.7 3.9-28 48c7.7 9 25.6 33.2 29.4 35z"
			opacity={0.702}
		/>
		<path
			fill="red"
			d="M1087.3 960.3c22.4 2.4 80.1 28.3 72.7 88.2-7.1 57-65.6 73.9-74.2 74.6-4.2.3-27.6 0-49.8-13-16.1-9.4-32.4-23.4-36.7-58.8-10.4-86.7 74.6-92.5 88-91z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="#F00;#F33;#C00;#F30;#F30;#f00"
			/>
		</path>
		<path
			fill="red"
			d="M1170 932c7.6.8 17.1 4.8 18 19 1.2 19.4-16.1 21.8-19 22-17.2-3.1-17.4-1.9-19-16-2.5-22.6 15.5-25.5 20-25z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				begin="0.2s"
				dur="2.5s"
				repeatCount="indefinite"
				values="#F00;#F42;#C00;#F00;#E00;#f00"
			/>
		</path>
		<path
			fill="red"
			d="M1170 1014c7.6.8 17.1 4.8 18 19 1.2 19.4-16.1 21.8-19 22-17.2-3.1-17.4-1.9-19-16-2.5-22.6 15.5-25.5 20-25z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				begin="0.25s"
				dur="1.5s"
				repeatCount="indefinite"
				values="#F00;#F42;#C00;#F00;#E00;#f00"
			/>
		</path>
		<path
			fill="red"
			d="M920 1000c30.3 0 36.7 18 37 32 .7 29.5-19.4 37.3-34 44-3.9 1.8-13.5 4.5-25-2-16.3-9.2-13-14-15.7-22.8-10.1-33.2 24.3-51.2 37.7-51.2z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				begin="0.5s"
				dur="2.5s"
				repeatCount="indefinite"
				values="#F00;#F42;#C00;#F00;#E00;#f00"
			/>
		</path>
		<path
			fill="none"
			stroke="#fff"
			strokeWidth={13}
			d="M1081 995.4c25.7 0 46.5 21.1 46.5 47.1s-20.8 47.1-46.5 47.1-46.5-21.1-46.5-47.1 20.8-47.1 46.5-47.1z">
			<animate
				fill="freeze"
				attributeName="opacity"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="1;1;0.8;0.8;0.6;1"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dashoffset"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="0; 50; 0"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dasharray"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="5 10 5; 5 5 5; 5 25 25; 5 10 25; 5 5 5; 5 10 5"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-width"
				attributeType="XML"
				dur="8s"
				repeatCount="indefinite"
				values="10;5;10"
			/>
		</path>
		<path
			fill="none"
			stroke="#fff"
			strokeWidth={10}
			d="M920.5 1023.3c9 0 16.2 7.5 16.2 16.7s-7.3 16.7-16.2 16.7-16.2-7.5-16.2-16.7 7.2-16.7 16.2-16.7z">
			<animate
				fill="freeze"
				attributeName="opacity"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="1;1;0.8;0.8;0.6;1"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dashoffset"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="0; 50; 0"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dasharray"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="5 10 5; 5 5 0; 10 0 5; 5 10 5"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-width"
				attributeType="XML"
				dur="3s"
				repeatCount="indefinite"
				values="10;0;5;0;10"
			/>
		</path>
		<path
			fill="none"
			stroke="#fff"
			strokeWidth={3}
			d="M1167.5 943.2c5.7 0 10.3 4.6 10.3 10.3s-4.6 10.3-10.3 10.3-10.3-4.6-10.3-10.3 4.6-10.3 10.3-10.3z"
		/>
		<path
			fill="none"
			stroke="#fff"
			strokeWidth={3}
			d="M1157.3 1024.1c1.9-1.8 4.4-3 7.2-3 5.7 0 10.3 4.6 10.3 10.3s-4.6 10.3-10.3 10.3c-2.1 0-4.1-.6-5.8-1.8"
		/>
		<path
			fill="#9faee5"
			d="m942 693-12 249 5 9 30-2V712s73.4 2.8 98-9c17.2 25.6 80 139.7 80 220 9.7-4.6 21-9 21-9s-27.3-166.7-101-231c-15.8 4.2-35.1 9.6-55 11-33.5 2.4-66-1-66-1zM745 549l-45-93s19-35.6 69-38c14.8 28.5 39 81 39 81l-1 6-13 1 1 14s-16.6 4.2-30 12c-11.4 6.6-20 17-20 17zm271-139-40 21 4-21h36zm-45 43 32-15-1 74-34-7 3-52zm249 423-3 100 45-22 1-41s-13.3-34.3-38-45c-1.1 1.4-5 8-5 8zm-457 84c.5 9.4 1 81 1 81s-62.8-10.9-61-11-57.3-21.5-64-60c-.6-36.5 1-52 1-52s1.4-26.4 49-39c4.8 19.1 9.1 63.5 74 81zm223-388c9.9 0 31 4.8 31 16s-43.5 15-62 15c-6.8 0-41.9.6-62-15 1-7.5 9.7-15.5 20-14 15.2.4 1.5 11.2 7 14 13.5 6.9 35 9 35 9s34-1.4 34-12-3.4-8.5-3-13zm-161-33 15-7 1 28-16-21zm35-20 32 67s-5.5 16.8-5 18-27 9-27 9v-94z"
		/>
		<path
			fill="#d539b5"
			d="M501 978s-33.2 7.5-31 33c2.2 25.4 28.2 52.7 55 64 26.8 11.3 65.2 21.2 81-1 15.8-22.2 6-30 6-30l-8-1s-3.6 16.9-16 24-62.4-.5-78-20-23.7-33.3-23-41 14-28 14-28zm40-104c3.2 12.4 30 59.7 98 73 1 12.7 0 27 0 27l4 9s-24.9 16.2-34 27c-21.4-4.5-92.9-34.3-98-64 1.2-30.9 13.6-44.8 30-72zm131 237c22.9-6.3 137.8 17.7 179 46-4.9 36.3-13.5 74.2-17 79-29.5-5.1-183-45-183-45s13.8-57.2 21-80zm586-32c17.3 1.8 137.5-29.3 144-83 12.9 13.5 42.5 65.5 47 79-2 19.6-80.4 80.5-156 75-15.3-25.8-27.3-44.5-35-71zm215 86c10 .7 20.3 2.3 23 8s-2.2 22.3-31 42-106.9 57.3-128 26c3.2-14 12-15 12-15l4 11s26.3 8.3 74-15c22.6-11.1 44.9-22.1 51-40-2-9.3-10.3-17.4-5-17z"
		/>
		<linearGradient
			id="ch-full99001i"
			x1={952.5}
			x2={952.5}
			y1={978}
			y2={1316}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d539b5" />
			<stop offset={1} stopColor="#9faee5" />
		</linearGradient>
		<path
			fill="url(#ch-full99001i)"
			d="M930 942c-43.5 0-242.9-15.8-256-99 13.9-87.1 86.7-163.7 120-190 .6 43.7 2 157 2 157s8.3 18 15 0 4-38 4-38l-4-131s13-10.3 29-16c-1 41.3 0 134 0 134s18 27.2 22-11c-.8-44.8-4-135-4-135s24.2-8.2 32-9c-.1 8.3-1 11-1 11s73.9 37.5 131 0c9.2 2.1 26-1 26-1s154.1 58.3 185 240c-13.8 30-32.3 42.5-67 56-8.6-30.1-29.3-152.7-101-225-53.8 9.1-104.3 12.4-118 9-4.2 30.6-11.9 199.7-15 248z"
		/>
		<radialGradient
			id="ch-full99001j"
			cx={965.581}
			cy={1595.021}
			r={1038}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.301} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full99001j)"
			d="M930 942c-43.5 0-242.9-15.8-256-99 13.9-87.1 86.7-163.7 120-190 .6 43.7 2 157 2 157s8.3 18 15 0 4-38 4-38l-4-131s13-10.3 29-16c-1 41.3 0 134 0 134s18 27.2 22-11c-.8-44.8-4-135-4-135s24.2-8.2 32-9c-.1 8.3-1 11-1 11s73.9 37.5 131 0c9.2 2.1 26-1 26-1s154.1 58.3 185 240c-13.8 30-32.3 42.5-67 56-.9 5.3-13.6 11-18 13-7.9 3.6-10.2-4.6-13-6-40.7 8.6-98.8 20.3-165 21-4.4 3-2.4 7.4-4 11-11.8 1.4-19-.7-28 1-2.9-2.6-3.8-8.2-6-8z"
			opacity={0.502}
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1198 1048c-9.3 8.2-27.5 17.8-45.4 26m4.4-20c6.4 1.5 28-4.1 28-24s-19-21.3-29-18"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m618 1561-19-162 23-90s7.1-1.7 20 1c3.9-15.9 27-114 27-114l-19-5s18.8-92.7 41-111c-8.5-6-17.3-18.7-20-26-10.3-1-58-10-58-10s3.5 7.2-5 25-28 50-28 50l-92 64-8-2 17-20-18-4-14 18s-13.6-8-18-17c2-12.2 3-16 3-16l-5-4 1-92 30-53s8.9-14.4 28-17c5.8-9.7 11-19 11-19s-8.9-11.8-4-28c7.8-17.9 25.8-56.6 48-72s36.9-17 52-17 49 9.4 66 44c7.8-4.2 13-8 13-8s-16-16-16-29 20.4-111.5 122-197c.1-6.5 0-36 0-36s-10.4 2-13-1-33-62-33-62-2.2 3.8-7-6-50-104-50-104-18.5-39.8 17-58 52.7 23 58 34 40 80 40 80 16.9-11.3 33-3c.1-17.6 0-79 0-79l-5-3v-48s16.4-5.8 25 1c.1 14.8 0 48 0 48l-5 2v93s18.1 34 36 80c3-3.9 3.5-11.7 31-14 3.3-9.9 8-22 8-22s-13-2-13-6 6-10 6-10 21-7.3 43-3c.3-7.5 0-11 0-11l-33-5v-64l41-23s4.3-31.3 5-37 11.7-32.1 44-29 29.7 43.8 29 48c8.2-.4 16 0 16 0l86 24v65l-87 24-27 2 2 86s157.7 54.5 187 240c-1.3 4.5-3.1 10.8-5 14 10.6 6.7 32.6 21.7 38 48-.1 21.7-2 39-2 39l-41 19s15.2 17.3 17 38c-.6 17.3-1 25-1 25l10 4-2 29-6 9 21 4s-18.9-70.6 29-97 82.8-24.2 114 8c24.7 32.1 47 82 47 82s-.2 9.1-11 19c7.2 14.1 35 72 35 72s14.8-5.8 24 6c9.6 16.5 35 71 35 71l11 140s-.7 8.5-6 13c-2.8-1.2-5-3-5-3l-4-13s-13.6.9-20 6c3.7 8.4 10 22 10 22l-7 4-21 5-10-10-4 5-102-94-36-74s-6.6-9.6 9-19c-8.2-17.2-18-37-18-37l-235-48s-39 15.6-169 12c-42.3 39.3-116 110-116 110l-19 82s18.6 2.5 17 9-20 89-20 89l-91 138s-8.4 1.5-14 0c2.7-7.3 5-10 5-10s-20.1-4.8-26-6c-1.1 6.6-4 15-4 15s-23-6.6-30-11c.6-4 1-8 1-8l-10 1z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M842.3 531.1c-7.7 1-14.7 3.8-20.3 8.9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M839 414c5.3 3.2 11.8 3.2 19 1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M472 1001c-2.8 5.2-3 43.6 48 72s82 9.7 86 0" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M449 1040c-2.8 5.2-3 43.6 48 72s83 9.7 87 0" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m506 971-19 34s2.6 36.1 39 54 63 12.6 70 1 9-17 9-17" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M514 955c9.4 12.6 36 42.4 94 55 14.2-14.8 23.9-20.4 36-26"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m670 1054-1-43" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M687 1084c6.6-7.6 33.5-35.5 83-28s74.5 24.3 80 69-16 110-16 110l-170-40m138 101 14-62"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M672 1113c7-8.1 47 0 86.8 8.7 39.4 8.6 78.6 20.5 93.2 34.3"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1256 1075c12.8 10.6 50.2-2.5 86-20 33.8-16.6 66.7-36.5 59-65"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M543 873c-2.6 10.8 31.7 64.6 95 73" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M926 1152c-13.7 0-62.9-4-74-6" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M677 885s-39.4 9.2-39 38 0 49 0 49 11.2 39 67 59c37.3 5.9 58 9 58 9v-82s-75.7-16.5-71-82"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M759 960c-.5 1.4-56 29-56 29v43" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M704 988s-66-21.9-66-57" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M795 926v136" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M691 877c8.5 9.5 81.4 66 240 66 3.1 4.3 5 7 5 7s21.2-.9 29-1v-10s112.7-3.3 169-23c5.2 4.5 11 8 11 8l19-8s-.9-4.8-.5-3.8m.8-1.6c8.2-2.7 51.2-18.3 62.7-47.6"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m931 943 12-249s80.9 5.2 120-10c24.7 22.7 76.6 94.2 101 231"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M965 939c0-10-1-227-1-227s71 3 100-8c14.6 23 69.6 111 79 219"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M670 1028s15.9 29.9 38 38" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1258 1079c2.2 11.6 21.1 51 35 72 19.1 3.2 74.7-1.9 148-58"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1334 1197-22-45" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1339 1245c4.4 7.6 38.9 17.4 95-13 68.6-37.2 64.9-58.2 60-64"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1471 1162 7 15s2.3 20-54 45-72 14-72 14l-10-21" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M620 1311s78.9 43.9 188 40" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m660 1570 9-51 41 9-27 53" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M729 1531c-8.5-.1-98.5-16.1-120-33" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m628 1567 7-56" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1539 1339c-9.5 10.5-38.5 41.7-94 43" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1514 1399-14-30" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1531 1390s-12.1-33.6-11-33" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M445 1136c7.1 6.4 24.4 29.4 74 25" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M599 1395c13.7 19.5 54.6 32 88.5 40.7 43.1 11 78.3 10 98.5 2.3"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1531 1242c-4.5 19.2-41.6 89.9-157 77" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1091 1141c14.4-3.9 89.8-23.7 121-55 10.7 4.3 16 5 16 5l14-18"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m846 938 2 174" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M747 550c4.3-9.5 19.3-19.6 49-32" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M701.2 452.7c5.7-11.6 30-31.5 67.2-36.7" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M978 413c9.7-6.1 35.5-9.9 65.2.1" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m796 645 2 167s.8 6 5 6 10.3-11 12-18c-.5-18-6-296-6-296l-14 1 1 113"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m841 486-2 271s-1.2 10 9 10 15.5-11.4 15-19-7-247-7-247"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M810 514s25 43.5 30.1 52.4M839 583c-7.4.8-21 5.3-26 9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M814 640c5.6-4.3 15.3-10.6 24-14" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M860 613c9.6-3.5 21.5-6.8 29-8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m974 431 34-16 47-3" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M967 533c6.9 1.1 26 4.1 26 12s-31.9 13.3-67 6" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m923 574-4 11s1.2 10 37 10 34-10 34-10l-11-28" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M893 586c0 5.3 10.6 18 67 18s57-14.7 57-18-3.1-14-30-14"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m890 584-1 32s14 15 69 15 60.2-13.2 62-15c.3-7.7-3-31-3-31"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M967 522c1-.4 39 6 39 6l39-1" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1025 528c.4.4-5 77-5 77m1 7s6.4 4.7 12 5c5.4.2 10-4 10-4"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1002 436-52 27v42l52 6v-75z" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1072 430v80l67-19v-44l-67-17z" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m897 1073-40-43s-18.5-40.3 26-49 63 22.6 70 32 11.1 39.8-14 57c-21.4 13.7-42 3-42 3z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M945 1004c-12.8-8.1-40.4-7.5-55 11s-12.8 31.7-5 45" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1032.2 934.6c-40.7 8.6-69.2 29.5-69.2 88.4 0 25.9 22 47 22 47s8.1 7.5 17 16c11.1 9.4 14.3 11.5 25 20 18.4 12.2 27 12 27 12m84-133c-2.9-8.7-36.3-35.7-67-55"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1079 960.2c44.7 0 80.8 36.2 80.8 80.8s-36.2 80.8-80.8 80.8-80.8-36.2-80.8-80.8 36.1-80.8 80.8-80.8z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1158 920 24 14s7 4.1 7 22c0 12.7-9.6 20-24 20-12.7 0-21-11-21-11s-6.3-17.2-3-27c3.6-10.6 17-18 17-18z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={5} d="M1181 933c-11.4-4.7-42.4-.5-31 35" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1202 893s3.9 124.2-5 159c.1.4.3.5.7.2 11.6-13.3 14-21.2 15.3-21.2 1.3 0 7-98.3 7-155"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1222 904 40 11" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1244 1041-17 24-14-6v29" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1226 1065v27" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M850 1114c26.3 5.5 113.3 12.7 207.6 5.4" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M845 1095c15.2 3.1 94.6 12.2 177 8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1138.1 1095.8c20.8-7.5 60.9-24.3 73.9-37.8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M966 903c11 .2 130.7-7.9 169-23" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M966 766c11.3.3 86.3-2.4 126-15" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M966 833c17.4 0 111.8-3.1 150-18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1194 1051c.3 0 19 8 19 8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1239 1038-23-8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m973 453-5 55" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M584 1004c-4.4 2.8-14 24.5 0 34 19 2.9 30 6 30 6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1038 461c5.7 0 10.3 4.9 10.3 11s-4.6 11-10.3 11-10.3-4.9-10.3-11 4.6-11 10.3-11z"
		/>
	</svg>
);

export default Component;

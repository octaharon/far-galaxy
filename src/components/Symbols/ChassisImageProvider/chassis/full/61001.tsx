import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M1069 1394c-32.2 5-61.4 10.6-70 9s-65.3-23-95-13-107.3 123.8-163 153c-76.5 10.8-103 7.9-129 1s-68.5-65.4-175-375c-4.9-28.2 57.1-170.7 85-239 27.8-68.3 72.6-199.5 111-202s44 0 44 0-30.5-68.7-98-107c-9.3-7.7-9.2-16.5-10-28s-3-47-3-47 82 51 123 51c20.4 0 19-19.6 19-30s-.9-18 15-18 65 37 65 37 99-54.4 108-60c1.8 20.8 7.9 49.5 7 67-10.8 24.4-106.7 62.6-96 117 21.5-16.3 70-56 70-56l68-5-13-218s9.3-3.7 15 0c2.2 36.5 14 227 14 227l16 14-9-148s8.4-6 15 1c1.4 13 10 164 10 164s69.2 69.7 85 94 68.7 121.3 89 159c37.1 44.1 214.2 240.9 254 307 56.3 48.7 119.4 93.8 130 130-13.2 15.1-81 44-81 44s-61.9-49.2-117-101c-45.1 21.5-83 39-83 39l-126 19-25 103s-6.7 10.7-36-24c-9.8-12.4-12-21-12-21s-.4-22-2-44zm-398-7 119-117-142 75 23 42zm-83-146 192-120-94-178-98 298z"
		/>
		<linearGradient
			id="ch-full61001a"
			x1={757.1928}
			x2={757.1928}
			y1={2515}
			y2={2552}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#414141" />
		</linearGradient>
		<path fill="url(#ch-full61001a)" d="M718 777c16.9 3.6 62.7 2.9 78-1 .8 12.3 3.8 37-36 37s-38.6-12.9-42-36z" />
		<path
			fill="#dadada"
			d="M569 547c50.4 27.4 123.1 86.5 203 65 .6 4.9 1 10 1 10l-18 43s5.8 26 32 10c7.8-19.4 17.5-39.9 11-51-.4-9.2 2-17 2-17s67.5-68.9 95-80c2.8 15.3 9 60.5 8 68-8.9 10.3-101 68-96 101s7 72.8-1 78-100.8 20-118-24-50.1-96.7-113-133c-8.7-23.1-6.3-42.9-6-70z"
		/>
		<linearGradient
			id="ch-full61001b"
			x1={756.0811}
			x2={756.0811}
			y1={2635.031}
			y2={2714}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#dadada" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full61001b)"
			d="M715 616c10.6.7 43.1-.6 53-2 5.5 3.7 12.1 8.1 5 12-7.5 4.2-14.8 30.3-17 40 4.7 12 17.2 19.8 32 11 8.4-8.9 9-8 9-8s6.7 39.6-66 17c-8.8-40.4-13.4-60.1-16-70z"
		/>
		<radialGradient
			id="ch-full61001c"
			cx={771.5}
			cy={2660}
			r={29}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full61001c)"
			d="M772 639c16.8 0 23.6 19.6 29 33 0 16.8-11.7 25-28.5 25S742 683.3 742 666.5s13.2-27.5 30-27.5z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full61001d"
			x1={754.0031}
			x2={797.0031}
			y1={2675.27}
			y2={2660.5989}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full61001d)"
			d="M798 645c-1.3 5.8-10 32-10 32s-28 11.7-33-9c5.3-19.8 11-34 11-34s8.5 13.8 32 11z"
		/>
		<linearGradient
			id="ch-full61001e"
			x1={751.704}
			x2={715.704}
			y1={2747.8374}
			y2={2747.8374}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000004" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full61001e)"
			d="M731 584c1.8 10.4 3 18 3 18s-29.8 5.2-36-5c7.2-8.3 12.5-30.1 13-40 2.3 1.3 4.3 2.8 4 4 1.2 3 3.7 7.3 5.5 9 5 4.7 9.4 7.7 10.5 14z"
		/>
		<linearGradient
			id="ch-full61001f"
			x1={715.2245}
			x2={768.3995}
			y1={2680.9199}
			y2={2762.802}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.412} stopColor="#000004" />
			<stop offset={0.85} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full61001f)"
			d="M727 578c-8.2-6.7-13.2-17.5-14-25 10.6-4.8 25.4-1.8 47 13s40.1 26 38 43-11.6 26.8-19 23-4.1-19.3-9-28-34.8-19.3-43-26z"
		/>
		<path fill="#7c7c7c" d="m1376 1312 89 104-114-92 25-12z" />
		<path
			fill="#d9d9d9"
			d="M1471 1422s13.7 1.4 29-4c24.8-8.8 56.5-27 62-35 1.2-14-79.6-92.9-137-127 7.1 14.7-6 35-6 35l-51 47 103 84z"
		/>
		<path fill="#7f7f7f" d="M1372 1337c8.2-1.3 19.4-4.5 38 17s39 45 39 45l-20-9s-56.1-48.2-57-53z" />
		<path
			fill="#d9d9d9"
			d="M742 1544c50.9-24.9 135.4-151.2 167-156s82.9 14.1 94 14c-8.9-15.8-29-60-29-60s-47.6-23.5-96-6c-100.8 29.8-196.4 201.7-248 211 64.5 2.3 45.7 4.1 112-3z"
		/>
		<path fill="#bfbfbf" d="M977 1343c18.1 4.2 408.1-54.8 450-63-24.4 23.9-66 65-66 65l-356 59s-21.8-42.2-28-61z" />
		<path
			fill="#d9d9d9"
			d="M994 702c-.3-14.8-9.6-165.5-10-172-6.9 8.6-13.2 2.3-16-2 1.9 24.9 9.7 165.2 10 172 6.2 5.9 6.5 12.8 16 2z"
		/>
		<path
			fill="#d9d9d9"
			d="M962 660.3c-.3-14.8-13.6-217.8-14-224.3-6.9 8.6-13.2 5.4-16 1 1.9 25 13.7 214.4 14 221.3 6.2 5.9 6.5 12.9 16 2z"
		/>
		<linearGradient
			id="ch-full61001g"
			x1={1046}
			x2={1046}
			y1={2210}
			y2={2340}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={1} stopColor="#dadada" />
		</linearGradient>
		<path
			fill="url(#ch-full61001g)"
			d="M1034 988c48.8 0 143 22.5 143 76 0 45.1-113.2 54-125 54s-137-8.3-137-75c0-42 70.2-55 119-55z"
		/>
		<path
			fill="#e5e5e5"
			d="M764 992c36.1 72.9 206 344 206 344-31.4 5.1-55.5-24.7-120 11-85.3 45.4-184.4 207.3-229 199-39.6-.4-88.2-114.6-127-217-30.4-80.1-56.4-153-57-159-1.4-13.7 23.3-109.5 86-241 31.5-72.6 62.4-171.2 96-195 18 19.8 102.3 196.5 145 258zM606 825c-12-1.6-83.3 174.7-102 222-18.7 47.3-40 118.1-36 132s63.8 167.2 87 215 31 54.3 43 56c19.6-11.7 139.7-129.7 176-160s48.4-79 25-130-156-295-177-326c-4.5-6.5-4-7.4-16-9z"
		/>
		<linearGradient
			id="ch-full61001h"
			x1={602.5}
			x2={602.5}
			y1={2051.1802}
			y2={2443}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.203} stopColor="#d9d9d9" />
			<stop offset={0.736} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#2a2a2a" />
		</linearGradient>
		<path
			fill="url(#ch-full61001h)"
			d="m687 941-97 298s-67.7 52.1-72 34c7.8-31.9 129.8-371.2 136-388 9 12.5 33 56 33 56z"
		/>
		<linearGradient
			id="ch-full61001i"
			x1={538.5}
			x2={538.5}
			y1={2102}
			y2={2505.1018}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.203} stopColor="#bfbfbf" />
			<stop offset={0.651} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#050505" />
		</linearGradient>
		<path
			fill="url(#ch-full61001i)"
			d="M486 1226s122.3-396.8 124-403c-17.3-6.4-139.5 290.8-143 351 11.5 32.5 19 52 19 52z"
		/>
		<linearGradient
			id="ch-full61001j"
			x1={602.5}
			x2={602.5}
			y1={1983}
			y2={1877.906}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-full61001j)"
			d="m670 1389-23-44-112 2s36.1 106.7 59 103c19.1-14.6 76-61 76-61z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full61001k"
			x1={675.2244}
			x2={675.2244}
			y1={2133}
			y2={1984}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#474747" />
			<stop offset={1} stopColor="#000" stopOpacity={0.502} />
		</linearGradient>
		<path fill="url(#ch-full61001k)" d="m651 1344 144-81s25.5-25.4 13-68c-43.8 21.3-269 148-269 148l112 1z" />
		<path
			d="M971 1339s-65.3-17.9-90-4c-4-7.7-50.2-113.1-109-229-79.9-157.3-178-336-178-336s6.1-29.6 26-34c0 0 88.4 161 178 315 44.2 75.9 86.2 151 119 205 13.4-2.9 129-18 129-18l98 87-165 20-8-6zm290-31-9-105 123-19 44 61s12.9 25 4 32c-15.6 4.2-162 31-162 31z"
			className="factionColorSecondary"
		/>
		<path
			d="M565 544c15.9 10.1 100.4 59.9 118 65s32 8 32 8 17.3 80.5 25 163c-14.4-2.3-37.7-6.6-46-20s-17.7-36.3-29-54-48.5-63.5-88-87c-5.4-7.5-8.2-15.5-9-30s-2-37.3-3-45zm241 153c3.8-20.8 26.3-45.6 39-55s58.1-41.6 58-51-8-63-8-63-67.5 39.4-96 79c-2 10.8-4 17-4 17s7.1 11.3 2 23c-2.8 14.8-1 18-1 18s3 17.7 10 32zM572 807s-37.6 85.3-44 106-72.2 180.1-79 203-16.6 37.2-4 75 97 251 97 251 48.5 104.7 77 103 39.8-11.4 50-23 53.3-58.4 77-85 85.9-80 98-85c-8.8-15.5-44-92-44-92s-82 84.7-100 100-104 92-104 92-16.4-5.9-30-33-45.9-101.7-71-165-28.8-73.6-28-82 11.5-61.4 32-112 82.4-203.8 93-220c-8.5-14.9-20-33-20-33z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full61001l"
			x1={1106.5}
			x2={1106.5}
			y1={1842.9999}
			y2={1947.281}
			gradientTransform="matrix(1 0 0 -1 0 3328)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#737373" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full61001l)"
			d="M1069 1391c24 1.4 64.6-12.4 75-10-3.3 15.9-20.9 104.1-28 104s-33.5-13.8-46-49c-.3-37.8.5-31.2-1-45z"
		/>
		<path fill="#e6e6e6" d="m1168 945-86-158-80 12-45 170s176.1-6.8 211-24z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1369.2 1337c50.3 45.9 103.8 88 103.8 88s48.9-2.8 90-43c-55.1-84-140.2-135.9-144.2-136.5-43.7-69.3-215.3-260.1-251.8-303.5-20.3-37.7-73.2-134.7-89-159s-85-94-85-94-8.6-151-10-164c-6.6-7-15-1-15-1l9 148-16-14s-11.8-190.5-14-227c-5.7-3.7-15 0-15 0l13 218-68 5s-48.5 39.7-70 56c-10.7-54.4 85.2-92.6 96-117 .9-17.5-5.2-46.2-7-67-9 5.6-108 60-108 60s-49.1-37-65-37-15 7.6-15 18 1.4 30-19 30c-41 0-123-51-123-51s2.2 35.5 3 47 .7 20.3 10 28c67.5 38.3 98 107 98 107s-5.6-2.5-44 0-83.2 133.7-111 202c-27.9 68.3-89.9 210.8-85 239 106.5 309.6 149 368.1 175 375s52.5 9.8 129-1c55.8-29.2 133.3-143 163-153s86.4 11.4 95 13 37.8-4 70-9c1.6 22 2 44 2 44s2.2 8.6 12 21c29.3 34.7 36 24 36 24l25-103 215-34s6.5-8.3 10.2-9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1052.6 836.1c23.6 0 44.1 19.2 45.6 42.8 1.6 23.6-16.3 42.8-40 42.8s-44.1-19.2-45.6-42.8c-1.5-23.7 16.4-42.8 40-42.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1053.7 852c14.8 0 27.6 12 28.6 26.8s-10.2 26.8-25 26.8-27.6-12-28.6-26.8 10.2-26.8 25-26.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M598.4 631.9c-1.7-19.8-6.4-73.9-6.4-73.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M750 1494.9c30.6-31.6 65.3-79.2 92.8-104.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1497 1374-87-82" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M993 687v16s-13.1 9.5-15-8c-.5-11.9-1-32-1-32" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M960 659c0 3-6.7 6.3-14 2-.5-6-1-13-1-13" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1067 1395 81-16" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M678 729c6.5 15 7.2 26.5 30 42s84.3 6.2 97 2c7.9-14.4 11.3-20.4 3-64"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M775.5 731c6.3 0 11.5 5.2 11.5 11.5s-5.2 11.5-11.5 11.5-11.5-5.2-11.5-11.5 5.2-11.5 11.5-11.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M718 778s.6 17.2 6 24 9.2 13.7 46 11c28-2 25.9-24.3 26-34"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M795.6 793.4c16 3.3 26.4 8.6 26.4 14.6 0 9.9-28.7 18-64 18s-64-8.1-64-18c0-6 10.4-11.3 26.5-14.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m799 642-13 35s-23.7 13.1-31-11c4.8-17.6 12-37 12-37" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M807 704c-5.2-22.9-12.6-39.7-13-46" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M771 623c-5.3 1.3-9.9 20.4 14 22s12-21 12-21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m876 538 7 75" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M798 606c11.7-13.9 71.3-65.2 90-75" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M772 614s1.6 13.2 7 17c13.4 9.5 27.6-25.9 11-45" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M623 576c34.8 21 68 50.5 149 37-2.2-6.8 6.7-9.4-38.9-32.1-.6-.3-5.1-3.6-6.1-3.9.6 1.9 4.3 4.4 4.3 5.1.3 7.4 1.2 11.1 4 18.9-3.7 6.4-40 4.5-38.3-5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M715 617s22.2 88.5 24 158" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M685 611c.6 13.6 3 130.4 15 154" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M921.5 933c13.5 19.4 27.7 39.8 42.1 60.6m85.5 122.8c18.7 26.8 36.7 52.7 53.2 76.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M807 768s125.2 196.1 150 201c18-1.4 196.2-22.2 212-24" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M900.3 900.5c-59.5 8.2-178.6 24.5-178.6 24.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1062 1178.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1035 1255.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1281 1216.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M737 900.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M859 884.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1115 1169.6c1.1 0 2 .2 2 .4s-.9.4-2 .4-2-.2-2-.4.9-.4 2-.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1083 1392 36-36 160-25 20 23" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M878 655c2.4 0 109.5 140.1 124 142s80-9 80-9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1001 798-42 167" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M965 937c-6.5-9.3-41-56-41-56l28-90 38 48" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m868 863 69-134" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m964 809-28 89" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M620 736s332.2 604.2 359 608 382-50.5 448-68c-.5-8-.7-19-7-30"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1424 1280c11.8-7.6 21.7-18.7 59 11" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1092 1279.6c49.1-7.5 114.4-17.9 164-25.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M913 1255c1-.1 61.1-7.2 130.9-18 4.2 3.3 101.1 90 101.1 90m228-143c-1.8.3-113.5 19.2-115.7 19.5 0 3.2 1.7 101.5 1.7 101.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M895 1225c2.7-.1 411-63.5 456-70.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M804 1065c.9 0 47.5-7.2 110.7-17" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1176.1 1058.8c42.2-6.5 74.1-11.5 85-13.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1367.9 1336.9c18.6-15.7 56.2-52.7 61.1-59.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1475 1423s-41.4-44.3-70.9-77.3c-8.7-9.7-23.7-10.1-43.1.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m974 1342 29 59" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M982 1345c-34.1-14.8-71.5-21.2-102-11-113.1 37.8-195.9 213.9-259 212"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m905 1390-25-57-100-214-188-350m-18 34 19 38m206.1 419.6c26.7 54.3 44.9 91.4 44.9 91.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M467 1176c12.4 30.8 103.2 294.2 131 274s170.4-151.7 200-190c19.4-25.1 15.5-64.2-7-116-9-20.7-46.8-86.2-80.8-153.9C663 896.1 610.7 803.7 601 824c-16.8 34.9-136.6 293.8-134 352z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m670 1390-24-47" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m810 1195-276 149s.5 1 1.5 1c13.7-.2 116.5-2 116.5-2l145-81"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M610.5 824.4C592.7 882.6 488 1226 488 1226" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M588 1239s83.1-250.2 98.4-298.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M778 1120s-183 114.4-245.4 155.5c-12.8 8.5-13.2-.6-12.6-6.5s132-384 132-384"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1019 988c24.4 0 126.5 1.5 153 58s-83.3 71-105 71c-55.4 0-152-14.1-152-71 0-52.4 79.6-58 104-58z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1120 1108c-24.6-14.8-118.2-23.4-158-10" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M919 1070c34.4-32.2 165.5-50.1 244.6 17.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M987 992v114" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-full84001a"
			x1={1228.0504}
			x2={1252.2355}
			y1={605.8352}
			y2={508.8352}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666363" />
			<stop offset={0.397} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-full84001a)" d="m1259 1401 30-56-49-41s-28.8 4.8-66 7c12.2 12.7 85 90 85 90z" />
		<path fill="gray" d="m1257 1394 24-44-41 10 17 34z" />
		<path fill="gray" d="m1232 1366-4-8c-7.8-8.9-24-26-24-26l-4 1 32 33z" />
		<path
			fill="#f2f2f2"
			d="M1039 725c-41.2-16.1-33.5-18.6-116.3-13.5-25.6 1.6-44.3 7.6-49.7 7.5-12.3-.2-25.3-7.3-39-10 13.1-35.5 156-312 156-312l-53-21-171 283h-40L468 429l-54-24-13 14 224 221 33 27s6.3 19.1 14 35c-.7 8.9 23.2 54.8 29 70 .7 3.4 1.8 7.1 3 11.2-22 8-227 86.8-227 86.8l-35-59s-21.9-18.6-50.4-21.3l-4.2-.3c0 2.5-2.1 9-2.4 10.6-10.8 1.9-29.1 5.8-34 9-.6.3-9.6-6.7-10.2-6.3-29.3 15.8-48.7 41.3-43.8 82.3 12.1 46.6 28 83 28 83l29 67s36.8 57.6 87 46 93.3-33.6 87-113c26.7-.6 239.1-5.9 257-6 22.7 48.7 50.4 91.3 89 135s91.4 104.7 109 120 50.2 54.9 93 83 76.9 29 101 29 95.1-7.1 123-49c7.7-11.6 13.8-28.1 15.5-46.7 4.4-48.3-9-110.4-12.5-135.3-3.6-25.8-23.8-122.4-27-130-1.5-3.5-10.7-25.2-24.6-51.6C1267 906.8 1363 851 1363 851s9.2-16.2 4-40-19.7-40.8-68-47-215.6-30.4-260-39z"
		/>
		<path
			fill="#f2f2f2"
			d="M765 761 477 871l51 97 324-6s19.5-6.2 27-24c8.2-19.4 4.2-51.4-6-75-10.4-24-31.3-55.1-53-73-27.3-22.5-55-29-55-29z"
		/>
		<path
			d="M444 815c-12.3-6.8-30-15.3-58-13-10.1.8-22.3 2.8-34.3 7.9-21 9-41.1 28.9-47.7 69.1-.7-5.7-6-8-6-8s11.8-57.1 45-70c10.4 3.8 10 6 10 6l33-9 1-7s40.4.6 57 24zm694 320c10.4 19.2 61.9 121.3 42 194 32-2.1 87.8-8.3 114-40s14.3-141.7 0-185c-16.1-5.7-33-5-33-5l3 30-51 6-27-30s-39.7 14.7-48 30zM839 809l12-23-37-27-30 8s44.7 24.7 55 42zM527 968c1 .2 324-8 324-8s32.5-7.7 33-39-4-33-4-33l-310 38s-15.1-7-15-26c-21.4.8-40 6-40 6s-8.4-31.5-28-39c-6.9 2.2-10 3-10 3s43.8 77.4 50 98zm-57-78-24-51-18-5 23 50 19 6zm-100 9-14 14-22-46 14-13 22 45zm979-184c-9.9 6.2-30.8 26.1-31 53-18.3-3.1-33-4-33-4s13.6-55.1 37-60c14.8 3.8 31 5 31 5l6-14 19 8s-19.1 5.8-29 12zm17 98s-14.9-1.9-27-1c-.5 23.1-16 24-16 24l-110 18 40 62s116.4-70.4 134-79c1.4-3 16.4-40.6-18-60-18.5-1.4-33-6-33-6s30.6 15.6 30 42z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-full84001b"
			cx={1194.167}
			cy={822.2271}
			r={345.038}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.803} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full84001b)"
			d="M1138 1135c10.4 19.2 61.9 121.3 42 194 32-2.1 87.8-8.3 114-40s14.3-141.7 0-185c-16.1-5.7-33-5-33-5l3 30-51 6-27-30s-39.7 14.7-48 30z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full84001c"
			cx={1163.08}
			cy={836.061}
			r={328.476}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.803} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full84001c)"
			d="M1137 1131c-7.4 6.7-48.9 37.3-89-21-15.3 24.3-37.8 109.6-10 160 39.8 32.9 80.1 65.1 144 58 7.1-30 7.7-77.1-45-197z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full84001d"
			x1={1322.4723}
			x2={539.4722}
			y1={1171.7393}
			y2={501.1013}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.6428} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full84001d)"
			d="M1047 1107s-38.5 107.8-9 160c-36.4-30.6-118.8-120.8-157-163s-93.3-133.8-95-143c17 .8 69-1 69-1s50.2-11.7 17-101c31.7 7.4 158 154 158 154s-8.7 25.1-4 51c3.7 20.4 21 43 21 43z"
			opacity={0.7}
		/>
		<linearGradient
			id="ch-full84001e"
			x1={1305.6125}
			x2={1321.5696}
			y1={1126.8739}
			y2={1190.8739}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84001e)"
			d="M1289 764c-.5-10.3 4.6-49.5 36-60 13.7 3.8 27 8 27 8s-33.4 13.8-34 56c-16.1-3.2-21.7-2.2-29-4z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full84001f"
			x1={688.2917}
			x2={679.9457}
			y1={945.6407}
			y2={1151.6407}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84001f)"
			d="M851 960c-6.1 0-323 7-323 7s-7.5-4-11-14c-2.5-7.2.1-35.2-2-46-6.7-34-29-40-29-40s270.3-104.5 274-106c59 12.2 92.2 60.2 103 81s46.6 101.7-12 118z"
			opacity={0.2}
		/>
		<path fill="#e6e6e6" d="m771 702-32 17-289-276 14-13 307 272zm39 1 164-312 12 8-154 310-22-6z" />
		<path fill="#999" d="m405 422 46 23 280 267-93-58-233-232z" />
		<linearGradient
			id="ch-full84001g"
			x1={674.8017}
			x2={772.9457}
			y1={1249.6353}
			y2={1140.6353}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4f4f4f" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84001g)"
			d="M769 704c-3 0-17.1 19.5-30 13-32.5-16.5-79.3-48.6-83-49 4.6 11 49 109 49 109l92-38s-7.7-17.5-14-15c-2.9-9-10-20-14-20z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full84001h"
			x1={924.9672}
			x2={791.5782}
			y1={889.6077}
			y2={948.8837}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.389} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#313131" />
		</linearGradient>
		<path
			fill="url(#ch-full84001h)"
			d="M784 961s62.3.7 72-1 17.6-12.6 21-26c90.7 18 1.7 117.6-8 149-39.2-36.2-85-122-85-122z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full84001i"
			x1={1345.184}
			x2={1492.309}
			y1={1013.213}
			y2={1223.3301}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.63} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full84001i)"
			d="m1364 710 21-8 15 6 117 58s11.3 6.5 17 10c-104.9-11.3-85.7 110.2-55 136 2.9 2.4-51.1-30.4-102.9-66-.3-1.1 4.2-3.4 9.9-9 6.7-6.5 6.3-21.9 3-35-5.3-20.9-12.5-19.4-17-25 0-.9-29.3-5.3-53.3-9-.8-39.6 35.3-57 35.3-57l10-1z"
		/>
		<path fill="#ccc" d="M1506 767c42.5 0 77 34.7 77 77.5s-34.5 77.5-77 77.5-77-34.7-77-77.5 34.5-77.5 77-77.5z" />
		<path
			fill="#8c8c8c"
			d="M1504 786c18.8-.7 56.7 15.6 60.9 50.5 4.1 34.7-24 62.7-51.9 65.5-32.1 3.1-59.2-18.7-63.1-47.1-3.9-28.2 19.6-67.7 54.1-68.9z"
		/>
		<linearGradient
			id="ch-full84001j"
			x1={493.8575}
			x2={493.8575}
			y1={991.659}
			y2={1064.9076}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full84001j)" d="m481 867-8 2 37 71s12.4-26.9-3-52-26-21-26-21z" opacity={0.502} />
		<linearGradient
			id="ch-full84001k"
			x1={306.8103}
			x2={498.7543}
			y1={900.8907}
			y2={1114.0657}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.637} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full84001k)"
			d="M514 944c-6.7-18.8-47.2-62.7-103-44-23.9 8-49.4 29.6-59 48-16.9 32.5-2.2 51.3-2.2 66.6 0 5.5 8.7 12.9.6 4.5C342.1 995.5 316 941 316 941s-6.5-18.6-11.2-32.1c-2.7-7.8-3.2-9.1-4.8-13.9 9.7-47.6 61.1-113.9 150-72 14.6 24.4 59.4 106.6 64 121z"
		/>
		<linearGradient
			id="ch-full84001l"
			x1={1273.059}
			x2={1206.546}
			y1={978.4864}
			y2={1169.4865}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84001l)"
			d="m1252 915 135-77s20.5-37.9-17-61c-43.9-8-339-53-339-53 69.1 29.9 164.5 98.2 221 191z"
			opacity={0.302}
		/>
		<path fill="#d9d9d9" d="M436 896c51.4 0 93 41.6 93 93s-41.6 93-93 93-93-41.6-93-93 41.6-93 93-93z" />
		<path fill="#8c8c8c" d="M437 920c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70z" />
		<linearGradient
			id="ch-full84001m"
			x1={1025.9247}
			x2={1305.6887}
			y1={826.9396}
			y2={923.2706}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.413} stopColor="#666" />
			<stop offset={0.64} stopColor="#adadad" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.757} stopColor="#adadad" />
			<stop offset={0.877} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-full84001m)"
			d="M1282 990c14 68.3 22 117 22 117s-16.5-5.5-41.2-7.7c-.7-.1 1.9 31.7 1.2 31.7-19.9-1.6-23.3-2.3-49 5-1.2.3-27-29.8-28.2-29.4-20.2 6.2-40.8 16.6-59.8 33.4-51.5 24.7-80.2-35.5-89-49-43.1-61.7 21.5-144.8 103-154 90.5-10.2 120.2 6.5 141 53z"
		/>
		<linearGradient
			id="ch-full84001n"
			x1={296.5663}
			x2={514.9183}
			y1={975.5867}
			y2={1091.6857}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full84001n)"
			d="M345 1009c-2-20-4.2-95.1 67-109s93.4 33.2 103 47c-11.5-26.6-50.4-99.1-63-122-9.2-16.7-36-34.6-64.8-36.1-10.7-.6 2.3 5.2-8.2 10.1-2.7 1.3-27.4 8.7-30 10-1.9 1-5.7-8.2-7.6-7.2-32 17.5-54.7 42.4-40.4 95.2 12.1 24.5 40.4 105 44 112z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full84001o"
			x1={1333.811}
			x2={1436.511}
			y1={972.5937}
			y2={1157.8688}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84001o)"
			d="M1531 775c-21.4-10.3-74.2-10.8-93 32-20.3 46.2 5.1 77.8 9.6 90.3-25.9-13-70.7-51.3-70.6-51.3l12-8s4-15.8 4-27-16.4-30.6-25-34c-11.7-1.9-32-4-32-4s1.7-57.3 67-61c66.6 31 107.9 51.3 128 63z"
			opacity={0.4}
		/>
		<path d="m809 708 166-315-38-15-175 284-40-1 45 44s28.6-5.3 42 3z" className="factionColorSecondary" />
		<path fill="#e6e6e6" d="m886 463 3 20-65 106-19 4 81-130z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m355.6 914.5-21.2-46.9s7.8-11.8 13.6-15.5c2.6 6.4 23 48 23 48s-8 6-10.7 8.8c-2.4 2.3-4.7 5.6-4.7 5.6zm97-29-27.2-50.9s12.2 1.4 20.6 7.5c2.3 5.8 22.6 42.2 26.4 48.9.2.4.1.7-.4.4-1.6-1-5-3.1-9-4.3-4.6-1.6-10.4-1.6-10.4-1.6z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M351.4 1029.1C343.1 1005.5 316 941 316 941l-16-46s-12-66.5 43-95c2 1 6.2 4.6 8.7 6.3 3.4-1.3 6.2-3 12.3-4.3 8.3-1.9 18.8-3.8 23-4 0-1 0-7.4-.2-8.2 30.3.1 53.2 16.5 63.2 33.2 11 18.4 48.6 85 61.4 111.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M741 721s-.4-1.4-1.3-2.3C718.8 697.9 446 441 446 441l-45-23s-.5.5.5 1.5C422.4 440.4 637 651 637 651l104 70z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M399.9 419.2c4.4-4.1 15.1-14.2 15.1-14.2l47 22s247.6 214.8 310 276c-10.3 4.3-29.9 15.3-34 21"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M736 658c7.2-1.3 13.1-.5 29 1 13.2-23 173-284 173-284l38 14.5-165 314"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m978 391 12 11-156 307" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M448 443s11-9.5 17.1-14.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M308 918c-11.3-44.6 18-102.3 75.4-107.9 19.2-1.9 41.6 3 66.6 14.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M881 889c-23.7 2.7-306 35-306 35s-19.6 7.5-19-26c-11.6 1.8-42 7-42 7l352-57"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1073 1132c2.3-50.7 103.9-124.9 218-81" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1034 1081c4.8-16.2 20.8-40.2 45.2-59 41.8-32.5 111.1-55.8 202.8-25"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1048 1104c-11.3 18.4-39.2 139.9-3 173" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M829 731c127.8 54.8 389.8 471.9 354 590-2.2 6.6-4 11-4 11"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1006.3 714.4c81.7 46.1 182 123.7 228.5 224.3 63.6 137.7 91 281 65 339.1-.8 1.7-1.6 3.3-2.3 4.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M801 742c-10.7-12-8.7-21.1-8-37 .7-2.7 1-4 1-4s51 11 83 19"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M769 703c9.4-3.3 32.2-7.2 44-4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m795 701-60-43-10 1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M751 765c29-11 87-55 218-55 27.9 0 44 8 44 8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m373 1004 130-19" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1361 852c14.1-32.7 5.9-64.1-17-74-17.9-7.8-27-9-27-9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1211 854 117-19s13.4-4.5 11-23c14.2-.4 16.6-.8 28-1-4.6 0-186 8-186 8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1451.9 855 113.1-18.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1288 1344-55 13 26 45" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1234 1356-29-31" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1354 711s-25.1-5.6-31.1-7.1c-16.3 6.9-33.6 22.8-34.8 59.7m88.5 82.6c35.2 24.2 72.9 53.7 91.4 64.8m76-134c-5.7-3.5-24-13-24-13l-120-56-15-6-27-8-7 4 13 12"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M436 896c51.4 0 93 41.6 93 93s-41.6 93-93 93-93-41.6-93-93 41.6-93 93-93z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m418 898-40-85" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1388 706c-44.6 0-68.1 24.3-70 63.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1409 712c-43.6 0-69.5 23.3-72.8 61.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1506 767c42.5 0 77 34.7 77 77.5s-34.5 77.5-77 77.5-77-34.7-77-77.5 34.5-77.5 77-77.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1384 736 67 35s-10.5 6.3-17 15c-11-5-64-36-64-36s3.1-4.4 14-14z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1507 785.2c32.5 0 58.8 26.4 58.8 58.8s-26.3 58.8-58.8 58.8-58.8-26.4-58.8-58.8 26.3-58.8 58.8-58.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1252 915 135-77s20.5-37.9-17-61c-43.9-8-337-53-337-53"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M785 962c22.7 48.7 50.4 91.3 89 135s91.4 104.7 109 120 50.2 54.9 93 83 76.9 29 101 29 95.1-7.1 123-49c7.7-11.6 13.8-28.1 15.5-46.7 4.4-48.3-9-110.4-12.5-135.3-3.6-25.8-23.8-122.4-27-130s-42.3-99.3-86-141c-41.7-39.8-114.8-86.9-156-103-22.9-9-39.7-12.4-62.9-13.8m-267.3 72.4c-1.2-3.8-2.1-7.4-2.8-10.6-4.5-11.9-20.1-42.6-26.4-59.7-1.7-4.7-21.6-50.3-21.6-50.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M515.9 941.5c-.2-11.3.6-27-.9-34.5-6.7-34-29-40-29-40s270.3-104.5 274-106c59 12.2 92.2 60.2 103 81s46.6 101.7-12 118c-5.7 0-291.5 6.1-329 6.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m838 808 14-23s-16.4-15.5-38-25c-17.7 5-35 6-35 6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M437 920c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1282 990c14 68.3 22 117 22 117s-17.1-5.7-42.4-7.8c-4.5-.4 6.6 31.4 1.6 31.3-14.6-.3-31 .9-48 4.8-4.5 1-25-29.8-29.5-28.3-19.9 6.3-40.1 16.6-58.7 33.1-51.5 24.7-80.2-35.5-89-49-25.3-36.2-13.5-79.8 17-111.5 21.5-22.3 55.3-34.7 89-38.5 45.1-5.1 72.1-7.5 93.5 1.2 21.4 8.6 34.1 24.4 44.5 47.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1002 901c47.9-37.1 126.1-51 178-44" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M940 832c46.5-36 121.8-50.3 173.6-44.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M880 770c39.8-30.9 105.7-37.7 154.9-37.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1190.8 1328.6c25.5 26.9 68.3 72.4 68.3 72.4l30-56s-22.6-18.9-37.1-31"
		/>
		<path fill="none" d="M-114-138c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2z" />
	</svg>
);

export default Component;

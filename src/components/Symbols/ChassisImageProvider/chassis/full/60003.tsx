import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M922 708c-.1.7 33.8 24.4 51 78 42.8 31.7 121.7 83.2 185.2 127.6 23.1 16.2 44.2 43.4 60.8 56.4 62 48.7 466.7 306.5 410 423-85.4 61.7-269.5-143.5-452-283-18 29.7-78 123-78 123s-184.9-43.9-225-53c-6.8 29.7-17 97-17 97s-20.4 10.7-50-47c-2.7-34.7-3-65-3-65s-112.9-27.6-137 10c4.4 37.6 47.8 174 45 196-2.8 22-60.2 206.8-78 216s-43.7-14.1-72-52-183.6-359.1-195-401c-11.4-41.9 37.7-97.6 109-185 57-69.9 80.4-82.7 102-141 4.8-25.8 5.3-46.5-10-66-6-7.7-6.8-20.4-6-27 4.4-25.2 12-40 12-40s17.9-43 119-43c81.7 0 102.8 12 118 23 22.1 1.6 45.4 7.6 52 19"
		/>
		<path
			fill="#ddd"
			d="M692 1374c-7 24.6-44.3 156.7-81.7 206-31.7-24.1-47.5-36.7-81.9-103.9s-89.8-189.3-118.8-248.1-50-98.5-41-126.6S427.8 998.2 473 949c10-12.7 15.3-23.1 19.6-14s32.1 48.7 38.6 75c-7.5 8.7-37.5 36.7-48.2 55 3.3 11 5.5 19.6 12.6 12.4 7.1-7.2 39.2-49.9 48.1-50.1 3.3 5.7 10 14.4 1.6 22.7-8.3 8.3-39 45.3-43 54 1.8 6.5 4 23 13.3 11-4.3 13.7-.5 34.6 8.1 23.2 17.3-23.2 35.9-51.5 44.3-51.2 2.6 8 46.9 133.8 82.7 216 19.3 44.4 41.3 71 41.3 71z"
			opacity={0.5}
		/>
		<path
			fill="#ddd"
			d="M1494 1177.5c46 37 160 148.5 136 210-5.9 0-6.7-5.7-15.3-14.3-8.7-8.7-110.6-123.7-114.1-126.7 18.4-20.7-6.6-69-6.6-69z"
		/>
		<linearGradient
			id="ch-full60003a"
			x1={757.5006}
			x2={757.5006}
			y1={1214.8811}
			y2={1265}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-full60003a)"
			d="M653 682c0-7.7 1.5-27 106-27s103 17.2 103 23c-11.6 1.5-77.5.4-98 27-20.7 1.1-111-5.2-111-23z"
		/>
		<linearGradient
			id="ch-full60003b"
			x1={810.8021}
			x2={935.8251}
			y1={1059.6835}
			y2={1207.4185}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#747474" />
			<stop offset={0.499} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#bababa" />
		</linearGradient>
		<path
			fill="url(#ch-full60003b)"
			d="M947 787c-13.3-39.2-43.6-59-84-59s-51 52.3-51 80 18.1 74.2 65 67c-10.2-12.5-16.7-15.9-20-25s-16-115.7 90-63z"
		/>
		<linearGradient
			id="ch-full60003c"
			x1={861.8}
			x2={885.209}
			y1={650.3672}
			y2={766.3552}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60003c)"
			d="M677 1164c58.2-26.8 219.8 22 421 67-5.2-19.7-12.5-44.6-18-53-81.3-9.6-345.1-78.1-416-60 9.4 13.5 13.5 40.6 13 46z"
		/>
		<linearGradient
			id="ch-full60003d"
			x1={1274.7607}
			x2={1140.6556}
			y1={728.2702}
			y2={816.9982}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60003d)"
			d="M1099 1231c19.2-30 75.5-121.6 95-141 15.5-27.2 34.8-46.2 14-98-36.8 76.1-51.2 96.3-125 187 8.9 29 13.6 35.1 16 52z"
		/>
		<linearGradient
			id="ch-full60003e"
			x1={842.5}
			x2={842.5}
			y1={1014.0176}
			y2={1240}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#f1f1f1" />
		</linearGradient>
		<path
			fill="url(#ch-full60003e)"
			d="M905 876c8.3 6.8 19.1 16.1 24 20-20.8 6.5-205.3 34.7-224-47 1.4-74.8 35.6-169 130-169s145 88.7 145 139c-10.2-7.1-33-24-33-24s.4-65-80-65c-52.6 0-57 57-57 72s9.1 93.7 95 74z"
			opacity={0.6}
		/>
		<path
			d="M729 749c-8.8 18.6-27.2 66.3-23 106 22.3 31.8 39.5 50.7 159 50 3.1-9.8 8-14 8-14l46-5-14-11s-31.5 7.3-42 3-40.2-10.9-49-51-2-49-2-49-59.5-5.8-83-29z"
			className="factionColorPrimary"
		/>
		<path
			d="M729 749c-8.8 18.6-27.2 66.3-23 106 22.3 31.8 39.5 50.7 159 50 3.1-9.8 8-14 8-14l46-5-14-11s-31.5 7.3-42 3-40.2-10.9-49-51-2-49-2-49-59.5-5.8-83-29z"
			opacity={0.149}
		/>
		<linearGradient
			id="ch-full60003f"
			x1={512.247}
			x2={620.2469}
			y1={1048.5336}
			y2={1199.7206}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60003f)"
			d="M565 844c29.7-32.2 66.8-63 104-82-33.5-25-66.3-81.3-81-100-10 7-13.1 10.6-13 13-12.5 24.6-14 40-14 40l6 24s36.3 28-2 105z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full60003g"
			x1={610.4755}
			x2={583.3676}
			y1={1147.571}
			y2={1086.571}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full60003g)" d="M583 778s27.6 8.3 31 6 8-3 8-3l7 6-61 52 15-61z" opacity={0.4} />
		<path fill="gray" d="m1102 929-37 62-36-54 73-8z" />
		<path fill="#b2b2b2" d="m875 834 15 13 7 10-24 20s15 1 30-2c13.5 10.1 157 114 157 114l-30-51-161-121 6 17z" />
		<path fill="#191919" d="m897 857-18 15-19-19 16-17 21 21z" opacity={0.302} />
		<path
			fill="#ccc"
			d="m947 798-20 1-2-22s16.2 6.4 22 21zm-61-21 11 23s-24.4 3.5-36 5.1c-1.3.2 6.1 16.4 12 28.9-6.2 6.5-15.6 14.9-16 15-.6.1-2-43-2-43s5.9-23.5 31-29z"
		/>
		<linearGradient
			id="ch-full60003h"
			x1={1047.4307}
			x2={1405.8057}
			y1={998.8622}
			y2={680.3472}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a1a1a" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-full60003h)"
			d="M1193 1088c55.2 49.9 249.2 199.5 305 159 31.2 35.1 108.2 121.3 130 147-85 72.9-316.4-190.5-452-284 7.4-11.1 10.6-14.1 17-22z"
		/>
		<path
			fill="#d9d9d9"
			d="M651 685c-4.9 11.8-6.4 16.9-8 23 21.7 11.2 74.4 19 101 15 7.3-9.6 12.3-15.3 16-19-28.7-.1-94.2 1.8-109-19z"
		/>
		<path
			fill="#b2b2b2"
			d="M626 1589c13.6 0 30.4-39.9 57-116s33.3-105.6 17-99-16.6 42.2-43 112-53.3 103.8-31 103z"
		/>
		<path
			fill="#e6e6e6"
			d="M1189 941c53.3 72 368.2 224.7 311 306-67.7 42.1-288.1-142.4-306-157 18.5-30.5 43-65.8-7-137-4.3-6.2-.8-8.7 2-12z"
		/>
		<path
			d="M533 1010c-9.2-25.2-42.3-76.9-42-78s9.4-9.8 15-14c13 9.5 123 152.1 149 191s21 58 21 58-6.1 3.9-10 8c3.8 12.8 41.6 160.9 45 177s-11.3 22.5-18 22-24.6-28.9-43-75-83-214-83-214-35.7 34.8-47 58c-6-7.5-11-20-11-20s37.5-49 49-57c-3.2-10.5-9-22-9-22s-45.8 55.5-49 62c-3.6-9.1-9-25-9-25s41.7-50.5 52-55c-5.8-8.5-6.6-10.7-10-16zm660 81 120 89s131.9 100.8 189 62c22.7-29.8-27.5-77.1-34-85s-245-185-245-185l-46-40s20.1 31.8 28 48 12 38.3 12 60-24 51-24 51z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-full60003i"
			x1={839}
			x2={839}
			y1={641.9999}
			y2={755}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#737373" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full60003i)"
			d="M804 1165c20.3 4.8 59.6 13.6 70 16-3.3 15.9-12.9 97.1-20 97s-35.5-12.8-48-48c-.3-37.8-.5-51.2-2-65z"
		/>
		<path
			fill="#ccc"
			d="m417 693 2 33 53 63 26 20h62v-32l-62 2-25-21-56-65zm455-28 60 33-11 11s-38.9-23.9-47-24c-1.4-6.5-2-20-2-20zm85 47 61 31 33 19s43.8-5.2 43-5 6.1 24.8 6 26-51 5-51 5l-35-14-54-34s3-17.4-3-28z"
		/>
		<path
			fill="#f1f1f1"
			d="m546 756-69 4-58-69 52-3s27 24.5 48.7 44.1C539.2 736 600 749 600 749s22.9 11.1 24 21-5.4 15.7-19 12-50-15-50-15l-9-11zm413.8-46c30.7 16.3 58.2 31 58.2 31l48-6-137-76-52 7s27.4 14.6 58 30.9c-8.3 4.6-13 11.1-13 11.1l31 31 8-3s-.7-12.4-1.2-26z"
		/>
		<linearGradient
			id="ch-full60003j"
			x1={568.437}
			x2={581.4019}
			y1={1135.3267}
			y2={1187.3267}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full60003j)" d="m553 767 57 18s21.1-7.2 14-15-23-20-23-20l-74-17 26 34z" opacity={0.6} />
		<path
			fill="#e6e6e6"
			d="M545 755c.3.8 13 20 13 20l-61 1-20-18s67.7-3.8 68-3zm477-14s49.5-4.2 50-4 19 19 19 19l-43 4-26-19z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M867 681s-2.2-4.1-4-7c-6.6-11.4-29.9-17.4-52-19-15.2-11-36.3-23-118-23-101.1 0-119 43-119 43s-17.6 23.8-8 56c1 3.3 3 9 3 9m353-32c-.1.7 33.8 24.4 51 78 42.8 31.7 121.7 83.2 185.2 127.6 23.1 16.2 44.2 43.4 60.8 56.4 62 48.7 466.7 306.5 410 423-85.4 61.7-269.5-143.5-452-283-18 29.7-78 123-78 123s-184.9-43.9-225-53c-6.8 29.7-17 97-17 97s-20.4 10.7-50-47c-2.7-34.7-3-65-3-65s-112.9-27.6-137 10c4.4 37.6 47.8 174 45 196-2.8 22-60.2 206.8-78 216s-43.7-14.1-72-52-183.6-359.1-195-401c-11.4-41.9 37.7-97.6 109-185 57-69.9 80.4-82.7 102-141"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M765 1091c32.8 8 88.3 16.7 106 21-11.7-15.7-16.5-21.3-23-29-18.3-3-78.5-13.7-101-17 6.7 9.5 13.3 19.1 18 25z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1134 1038 6 28 34-49-6-26-34 47z" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M661 1118c33.3-4.7 79.9-11.8 334 44 60.7 12.4 89 18 89 18s111.4-136.6 123-191"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M550 861c47.1-58.5 142.4-124 185-124" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M672 1169c30.5-16.4-68.4-137.7-166-255" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M571 988c37.4-33.1 122.2-91.2 137-98m306.5-50.1c19.8 4.1 43.1 10.2 61.1 18.7"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M575 816c4.3-12.4 7.9-25 5.7-40" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M811 655c-29.4-2.1-159-7.6-159 26 0 26.3 113 23 113 23"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m653 676-10 31s16.6 16.3 100 18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1098 1232c-9.7-37.9-25.7-101.3-201-328" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M947 796c12.7 10.6 157 132 157 132l-40 65-159-118s-92.9 28.3-94-76c-.7-69.3 52.2-70 59-70 23.6 0 78 14.9 77 67z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M728 748c13.1 9.3 23 24.9 84 30m125.9-16.4c9.9-5.8 15.1-12.9 13.1-21.6"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1103 928-75 9 36 54" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1030 940 860 807s-.6-.3.1.8c4.1 6.7 17.9 29.2 17.9 29.2l14 11 7 11 3 15"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m860 806 32-5 14 10 20-2v-10l21-2" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M888 848s-24.1 5.4-28 6c-1.8 2.6 6.6 9.4 6.9 9.9.3.5 11.1 8.1 11.1 8.1l17-14"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m853 854 23-19" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M844 870c-37.6-31.6-10.7-153.2 90-112" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M945 789c-24.6-31.5-111.7-22.2-89 62" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m924 776 2 32" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m884 775 22 35" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M881 776v25" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M972 784c5.3 14.8 7.4 25.4 10 41" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M924 710c-18.4-15.7-50-30-89-30-107.5 0-129 116.8-129 169s132.5 71 226 48"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m861 908 9-17s28.4-.8 50-5" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M708 832s-20 5.2-20 23c0 50 128.6 81.2 220.9 62.7 4.1-.8 35.2-5.5 43.1-9.7m62.1-57.3c3.9-12.4-3.3-24.1-31.1-31.7"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M802 1165c17.6 3.7 52.4 11.8 70.7 14.5 1.2.2 3.3 1.3 4.3 1.5"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1176 1112c16.8-30.7 97-78.1-23-202" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1192 1089c47.9 41.9 266.9 211.1 310 153 21.9-29.6-28.7-79.5-36-87"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1499 1248 130 145" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M710.9 1359.1C675 1455.8 581.2 1049.2 490 935" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M519 1143s-19.6-43.3-36-78c12-17.2 24-34.6 50-56m8 18c-10.2 8.4-37.5 41.2-49 57m9 21c9.7-15.4 37.2-49.1 49-62m9 22c-13.4 11.6-34.2 37.5-49 58"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M693 1373c-11.2 35.4-55.2 181.7-81 211" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M667 1176c-19.4-33.8-64.1-113.6-98-93s-114.8 165.3-130 216"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1216 1106c12-42.3 15.7-109.4 37-112" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M553 766c14.2 5.4 36.1 13.3 56 18 39.9-4.4-8-35-8-35s-55.5-12-82.6-17.9C510 724.1 470 686 470 686l-53 3v39l58 66 20 16 65-1v-29l-13-23-27-25m-102-41 59 70 22 21 58-2"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M872 685v-22l57-4 148 81 20 17 2 26-51 4-27-9s-44.7-26-64.6-37.6"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1071 736-55 4v37m31 9v-26l49-3m-47 5-30-20-146-77" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M958 742c4.2-6 11.4-24.5-10-35-21.5-10.5-27 3-27 3" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M497 777v33" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M473 791v-33l71-1" />
	</svg>
);

export default Component;

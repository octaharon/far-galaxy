import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-full84003a"
			x1={1228.0504}
			x2={1252.2355}
			y1={605.8352}
			y2={508.8352}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666363" />
			<stop offset={0.397} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-full84003a)" d="m1259 1401 30-56-49-41s-28.8 4.8-66 7c12.2 12.7 85 90 85 90z" />
		<path fill="gray" d="m1257 1394 24-44-41 10 17 34z" />
		<path fill="gray" d="m1232 1366-4-8c-7.8-8.9-24-26-24-26l-4 1 32 33z" />
		<path
			fill="#f2f2f2"
			d="M1039 725c-41.2-16.1-33.5-18.6-116.3-13.5-25.6 1.6-44.3 7.6-49.7 7.5-12.3-.2-25.3-7.3-39-10 13.1-35.5 156-312 156-312l-53-21-171 283h-40L468 429l-54-24-13 14 224 221 33 27s6.3 19.1 14 35c-.7 8.9 23.2 54.8 29 70 .7 3.4 1.8 7.1 3 11.2-22 8-227 86.8-227 86.8l-35-59s-21.9-18.6-50.4-21.3l-4.2-.3c0 2.5-2.1 9-2.4 10.6-10.8 1.9-29.1 5.8-34 9-.6.3-9.6-6.7-10.2-6.3-29.3 15.8-48.7 41.3-43.8 82.3 12.1 46.6 28 83 28 83l29 67s36.8 57.6 87 46 93.3-33.6 87-113c26.7-.6 239.1-5.9 257-6 22.7 48.7 50.4 91.3 89 135s91.4 104.7 109 120 50.2 54.9 93 83 76.9 29 101 29 95.1-7.1 123-49c7.7-11.6 13.8-28.1 15.5-46.7 4.4-48.3-9-110.4-12.5-135.3-3.6-25.8-23.8-122.4-27-130-1.5-3.5-10.7-25.2-24.6-51.6C1267 906.8 1363 851 1363 851s9.2-16.2 4-40-19.7-40.8-68-47-215.6-30.4-260-39z"
		/>
		<path
			fill="gray"
			d="M1064 895c-15.7 5.8-61.1 25.9-78 67-9-12.8-13.5-34.5-6-50s22.3-33.7 43-31c20.7 2.7 56.7 8.2 41 14zm102-15s36.9-12.4 55-2 26 28 26 28 13.4-25.2 5-40-24.2-23.2-42-18-44 32-44 32z"
		/>
		<linearGradient
			id="ch-full84003b"
			x1={902.2282}
			x2={1052.5192}
			y1={983.9147}
			y2={1124.8007}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.4731} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84003b)"
			d="M922.7 809c-9.9 6.1-29.2 41.5 53 143 .4-22.7-.4-50.5 27-64s61 5 61 5-45.6-58-87-77-44.1-13.1-54-7z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full84003c"
			x1={1104.2089}
			x2={1215.0378}
			y1={1038.6776}
			y2={1142.5717}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.5575} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84003c)"
			d="M1246 851c-14-7-55.6-7.8-81 28-13.8-16.4-76.3-87.4-60-101 11.6-11.3 53 14 53 14s59.3 25.8 88 59z"
		/>
		<path
			fill="#f2f2f2"
			d="M765 761 477 871l51 97 324-6s19.5-6.2 27-24c8.2-19.4 4.2-51.4-6-75-10.4-24-31.3-55.1-53-73-27.3-22.5-55-29-55-29z"
		/>
		<path
			d="M1057 1286c26.8 18 46.1 37.7 122 45 5.2-14.1 12.7-31.3-4-102s-37-99-37-99-20.9 19.5-41 14-50-37-50-37-30.8-39.9-22-73 28-55 28-55l-38-56s-24.9 24.2-31 38c-14.6-16.2-47-62.2-54-79s-28.7-57.3-11-74c-11.7-15.9-44-46-44-46l-33 15 8 9s-12 22.4-11 22 43.7 48.7 44 91-8.2 52.4-23 58-73 4-73 4 40.2 87.8 75 124 144 155 144 155 24.2 28 51 46zM763 759l23 8 30-8 7 4 29-18-25-14-28 11s-12.6-21.4-7-41c-26.8 2.4-38.9 8.9-49 18-13.5 17.3-21.9 38.5-16.6 56.4C742.9 768 763 759 763 759zm511 208-38-27-37-65s32-4.1 45 30c14.1 26.3 30 62 30 62zm-170-188-72-51 23 3 67 43-18 5z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-full84003d"
			cx={1194.167}
			cy={822.2271}
			r={345.038}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.803} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full84003d)"
			d="M1138 1135c10.4 19.2 61.9 121.3 42 194 32-2.1 87.8-8.3 114-40s14.3-141.7 0-185c-16.1-5.7-33-5-33-5l3 30-51 6-27-30s-39.7 14.7-48 30z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full84003e"
			cx={1163.08}
			cy={836.061}
			r={328.476}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.803} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full84003e)"
			d="M1137 1131c-7.4 6.7-48.9 37.3-89-21-15.3 24.3-37.8 109.6-10 160 39.8 32.9 80.1 65.1 144 58 7.1-30 7.7-77.1-45-197z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full84003f"
			x1={1316.0469}
			x2={533.0469}
			y1={1166.2358}
			y2={495.598}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4539} stopColor="#000" stopOpacity={0} />
			<stop offset={0.6731} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full84003f)"
			d="M1047 1107s-38.5 107.8-9 160c-36.4-30.6-118.8-120.8-157-163s-93.3-133.8-95-143c17 .8 69-1 69-1s50.2-11.7 17-101c31.7 7.4 158 154 158 154s-8.7 25.1-4 51c3.7 20.4 21 43 21 43z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full84003g"
			x1={1305.6125}
			x2={1321.5696}
			y1={1126.8739}
			y2={1190.8739}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84003g)"
			d="M1289 764c-.5-10.3 4.6-49.5 36-60 13.7 3.8 27 8 27 8s-33.4 13.8-34 56c-16.1-3.2-21.7-2.2-29-4z"
			opacity={0.502}
		/>
		<path fill="#e6e6e6" d="m771 702-32 17-289-276 14-13 307 272zm39 1 164-312 12 8-154 310-22-6z" />
		<path fill="#999" d="m405 422 46 23 280 267-93-58-233-232z" />
		<linearGradient
			id="ch-full84003h"
			x1={680.0473}
			x2={778.1913}
			y1={1254.3585}
			y2={1145.3585}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84003h)"
			d="M769 704c-3 0-17.1 19.5-30 13-32.5-16.5-79.3-48.6-83-49 4.6 11 49 109 49 109l92-38s-6.9-25.8-8-36c-6.9-1.4-16 1-20 1z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full84003i"
			x1={917.0186}
			x2={783.6296}
			y1={893.14}
			y2={952.416}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.389} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#313131" />
		</linearGradient>
		<path
			fill="url(#ch-full84003i)"
			d="M784 961s62.3.7 72-1 17.6-12.6 21-26c90.7 18 1.7 117.6-8 149-39.2-36.2-85-122-85-122z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full84003j"
			x1={1327.1477}
			x2={1477.3857}
			y1={1013.648}
			y2={1228.2109}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.63} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full84003j)"
			d="M1364 710s-11.9-10.1-7-12c5.6-2.1 28 4 28 4l15 6 117 58s11.3 6.5 17 10c-104.9-11.3-85.7 110.2-55 136 2.9 2.4-51.1-30.4-102.9-66-.3-1.1 4.2-3.4 9.9-9 6.7-6.5 6.3-21.9 3-35-5.3-20.9-12.5-19.4-17-25 0-.9-29.3-5.3-53.3-9-10.4-1.4-25.6-4.6-31.7-4 2.9-42.8 24.7-53.6 32-59 5.1-3.8 35 6 35 6l10-1z"
		/>
		<path fill="#ccc" d="M1506 767c42.5 0 77 34.7 77 77.5s-34.5 77.5-77 77.5-77-34.7-77-77.5 34.5-77.5 77-77.5z" />
		<path
			fill="#8c8c8c"
			d="M1504 786c18.8-.7 56.7 15.6 60.9 50.5 4.1 34.7-24 62.7-51.9 65.5-32.1 3.1-59.2-18.7-63.1-47.1-3.9-28.2 19.6-67.7 54.1-68.9z"
		/>
		<linearGradient
			id="ch-full84003k"
			x1={493.8575}
			x2={493.8575}
			y1={991.659}
			y2={1064.9076}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full84003k)" d="m481 867-8 2 37 71s12.4-26.9-3-52-26-21-26-21z" opacity={0.502} />
		<linearGradient
			id="ch-full84003l"
			x1={306.8103}
			x2={498.7543}
			y1={900.8907}
			y2={1114.0657}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.637} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full84003l)"
			d="M514 944c-6.7-18.8-47.2-62.7-103-44-23.9 8-49.4 29.6-59 48-16.9 32.5-2.2 51.3-2.2 66.6 0 2.7 2.1 5.8 3.2 7.4-.5-.6-1.4-1.5-2.6-2.8C342.1 995.5 316 941 316 941s-6.5-18.6-11.2-32.1c-2.7-7.8-3.2-9.1-4.8-13.9 9.7-47.6 61.1-113.9 150-72 14.6 24.4 59.4 106.6 64 121zm-86-106 25 47 19 4-25-48-19-3zm-82 16-12 13 23 48 14-14-25-47z"
		/>
		<path fill="#d9d9d9" d="M436 896c51.4 0 93 41.6 93 93s-41.6 93-93 93-93-41.6-93-93 41.6-93 93-93z" />
		<path fill="#8c8c8c" d="M437 920c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70z" />
		<linearGradient
			id="ch-full84003m"
			x1={1025.9247}
			x2={1305.6887}
			y1={826.9396}
			y2={923.2706}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.413} stopColor="#666" />
			<stop offset={0.64} stopColor="#adadad" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.757} stopColor="#adadad" />
			<stop offset={0.877} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-full84003m)"
			d="M1282 990c14 68.3 22 117 22 117s-16.5-5.5-41.2-7.7c-.7-.1 1.9 31.7 1.2 31.7-19.9-1.6-23.3-2.3-49 5-1.2.3-27-29.8-28.2-29.4-20.2 6.2-40.8 16.6-59.8 33.4-51.5 24.7-80.2-35.5-89-49-43.1-61.7 21.5-144.8 103-154 90.5-10.2 120.2 6.5 141 53z"
		/>
		<linearGradient
			id="ch-full84003n"
			x1={297.9208}
			x2={514.6948}
			y1={976.3063}
			y2={1091.5673}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full84003n)"
			d="M345 1009c-2-20-4.2-95.1 67-109s93.4 33.2 103 47c-11.5-26.6-50.4-99.1-63-122-9.2-16.7-36-34.6-64.8-36.1-3 2.7-.1 9-3.2 11.1-2.7 1.3-27.4 5.7-30 7-1.9 1-10.7-6.2-12.6-5.2-19.7 10.8-32.3 24.9-38.4 46.2-3.8 13.2-7.5 28.8-2 49 12.1 24.5 40.4 105 44 112z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full84003o"
			x1={1333.811}
			x2={1436.511}
			y1={972.5937}
			y2={1157.8688}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84003o)"
			d="M1531 775c-21.4-10.3-74.2-10.8-93 32-20.3 46.2 5.1 77.8 9.6 90.3-25.9-13-70.7-51.3-70.6-51.3l12-8s4-15.8 4-27-16.4-30.6-25-34c-11.7-1.9-32-4-32-4s1.7-57.3 67-61c66.6 31 107.9 51.3 128 63z"
			opacity={0.4}
		/>
		<path
			d="M721.4 775.2C650.1 802.5 505.5 858 487 866c16.3 3.7 29 42 29 42s-.7 23.6 0 33c6.9 8.5 9 27 9 27s221.5-5.4 300.5-7.3c10.1-17.7 15.6-41.3 1.5-66.7-28 4.1-254 32-254 32s-9.6-1.1-14-11-4-17-4-17l256-43s-17.8-51.4-89.6-79.8zM1250 916l113-65s24.8-64.1-35-81c-73.6-11.8-291-44-291-44l86 47s76.7 40.4 87 46c21.9-1.9 130-6 130-6s4.8 20.4-14 23-81 14-81 14 20.8 22.8 1 54c1.7 4.8 4 12 4 12z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-full84003p"
			x1={1266.4535}
			x2={1199.9406}
			y1={997.4543}
			y2={1188.4552}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84003p)"
			d="m1252 915 135-77s20.5-37.9-17-61c-43.9-8-339-53-339-53 69.1 29.9 164.5 98.2 221 191z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full84003q"
			x1={688.2917}
			x2={679.9457}
			y1={945.6407}
			y2={1151.6407}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84003q)"
			d="M851 960c-6.1 0-323 7-323 7s-7.5-4-11-14c-2.5-7.2.1-35.2-2-46-6.7-34-29-40-29-40s270.3-104.5 274-106c59 12.2 92.2 60.2 103 81s46.6 101.7-12 118z"
			opacity={0.2}
		/>
		<path fill="#e6e6e6" d="m886 463 3 20-65 106-19 4 81-130z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m355.6 914.5-21.2-46.9s7.8-11.8 13.6-15.5c2.6 6.4 23 48 23 48s-8 6-10.7 8.8c-2.4 2.3-4.7 5.6-4.7 5.6zm97-29-27.2-50.9s12.2 1.4 20.6 7.5c2.3 5.8 22.6 42.2 26.4 48.9.2.4.1.7-.4.4-1.6-1-5-3.1-9-4.3-4.6-1.6-10.4-1.6-10.4-1.6z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M351.4 1029.1C343.1 1005.5 316 941 316 941l-16-46s-12-66.5 43-95c2 1 6.2 4.6 8.7 6.3 3.4-1.3 6.2-3 12.3-4.3 8.3-1.9 18.8-3.8 23-4 0-1 0-7.4-.2-8.2 30.3.1 53.2 16.5 63.2 33.2 11 18.4 48.6 85 61.4 111.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M739.7 718.7C718.8 697.9 446 441 446 441l-45-23s-.5.5.5 1.5C422.4 440.4 637 651 637 651l102.3 68.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M399.9 419.2c4.4-4.1 15.1-14.2 15.1-14.2l47 22s247.6 214.8 310 276c-10.3 4.3-29.9 15.3-34 21-4.1 5.7-13.5 16-12 50"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M736 658c7.2-1.3 13.1-.5 29 1 13.2-23 173-284 173-284l38 14.5-165 314"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m978 391 12 11-156 307" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M448 443s11-9.5 17.1-14.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M308 918c-11.3-44.6 18-102.3 75.4-107.9 19.2-1.9 41.6 3 66.6 14.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M828.2 895C739.8 905.1 575 924 575 924s-19.6 7.5-19-26c-11.6 1.8-42 7-42 7s201.4-32.6 299.5-48.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1048 1104c-11.3 18.4-39.2 139.9-3 173" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M829 731c7.3 3.1 15.1 7.5 23.2 12.8m22.4 16.8c15.3 12.6 31.5 28.2 48.1 46m94.7 119.8c99.9 143.5 187.1 323.6 165.6 394.6-2.2 6.6-4 11-4 11"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1006.3 714.4c30.7 17.3 64 39 96 64.8m92.9 93.9c15.5 20.6 29 42.4 39.6 65.5 63.6 137.7 91 281 65 339.1-.8 1.7-1.6 3.3-2.3 4.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M801 742c-10.7-12-8.7-21.1-8-37 .7-2.7 1-4 1-4s47.6 10.3 79.7 18.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M769 703c9.4-3.3 32.2-7.2 44-4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m795 701-60-43-10 1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M751 765c29-11 87-55 218-55 27.9 0 44 8 44 8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m373 1004 130-19" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1361 852c14.1-32.7 5.9-64.1-17-74-17.9-7.8-27-9-27-9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1244.2 848.6c34.6-5.6 83.8-13.6 83.8-13.6s13.4-4.5 11-23c14.2-.4 16.6-.8 28-1-3.5 0-110.4 4.7-160.6 6.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1451.9 855 113.1-18.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1288 1344-55 13 26 45" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1234 1356-29-31" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1354 711s-25.1-5.6-31.1-7.1c-16.3 6.9-33.6 22.8-34.8 59.7m88.5 82.6c35.2 24.2 72.9 53.7 91.4 64.8m76-134c-5.7-3.5-24-13-24-13l-120-56-15-6-27-8-7 4 13 12"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M436 896c51.4 0 93 41.6 93 93s-41.6 93-93 93-93-41.6-93-93 41.6-93 93-93z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m418 898-40-85" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1388 706c-44.6 0-68.1 24.3-70 63.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1409 712c-43.6 0-69.5 23.3-72.8 61.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1506 767c42.5 0 77 34.7 77 77.5s-34.5 77.5-77 77.5-77-34.7-77-77.5 34.5-77.5 77-77.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1384 736 67 35s-10.5 6.3-17 15c-11-5-64-36-64-36s3.1-4.4 14-14z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1507 785.2c32.5 0 58.8 26.4 58.8 58.8s-26.3 58.8-58.8 58.8-58.8-26.4-58.8-58.8 26.3-58.8 58.8-58.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1252 915 135-77s20.5-37.9-17-61c-43.9-8-337-53-337-53"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M785 962c22.7 48.7 50.4 91.3 89 135s91.4 104.7 109 120 50.2 54.9 93 83 76.9 29 101 29 95.1-7.1 123-49c7.7-11.6 13.8-28.1 15.5-46.7 4.4-48.3-9-110.4-12.5-135.3-3.6-25.8-23.8-122.4-27-130-1.9-4.6-17-40.1-38.4-76.6m-105-110.6c-35-24.8-73-46.7-98.6-56.8-22.9-9-39.7-12.4-62.9-13.8m-267.3 72.4c-1.2-3.8-2.1-7.4-2.8-10.6-4.5-11.9-20.1-42.6-26.4-59.7-1.7-4.7-21.6-50.3-21.6-50.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M515.9 941.5c-.2-11.3.6-27-.9-34.5-6.7-34-29-40-29-40s270.3-104.5 274-106c59 12.2 92.2 60.2 103 81s46.6 101.7-12 118c-5.7 0-291.5 6.1-329 6.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M720.7 776.7c52.4 15.4 82.2 58.8 92.3 78.3 9.2 17.7 36.5 78.9 8.3 106.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m838 808 14-23s-18.4-16.5-40-26c-19 4.2-33 7-33 7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M437 920c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1282 990c14 68.3 22 117 22 117s-17.1-5.7-42.4-7.8c-4.5-.4 6.6 31.4 1.6 31.3-14.6-.3-31 .9-48 4.8-4.5 1-25-29.8-29.5-28.3-19.9 6.3-40.1 16.6-58.7 33.1-51.5 24.7-80.2-35.5-89-49-25.3-36.2-13.5-79.8 17-111.5 21.5-22.3 55.3-34.7 89-38.5 45.1-5.1 72.1-7.5 93.5 1.2 21.4 8.6 34.1 24.4 44.5 47.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1018.9 845.1c32.2-12.3 67.9-18.9 99.9-19.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M843 777c39.8-30.9 142.7-44.7 191.9-44.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M818 764c39.8-30.9 144.7-46.7 193.9-46.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1190.8 1328.6c25.5 26.9 68.3 72.4 68.3 72.4l30-56s-22.6-18.9-37.1-31"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1205 1126c19.2 22.2 60.6 161.5 27 194m45.3-17.8c10.2-18.5 23.4-53.9-13.3-179.2"
		/>
		<path fill="none" d="M-114-138c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1068 894.6c-40.8 12.7-63.9 32.4-84 65.4-31.1-34.6-97.3-125.3-63-152 33.9-26.3 104.5 36.4 147.2 84 .8.9.8 2.2-.2 2.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1167.1 880.4c-16.3-16.6-90.8-94.9-55.1-105.4 13.8.6 29.8 8.9 58 23 26.7 13.3 53.7 26.2 73 49 10.3 12.1 14.5 29.5 8 45-1.3 3.8-5 12-5 12s-11.4-21.2-31-28c-15.4-5.4-38.3 3.1-47.9 4.4z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1167 875c16-19.4 56.4-42.1 84-16" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1065 892c-30.8-14.8-55-13.5-71 1-12.6 11.5-25.6 41.7-12 65"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1019 880 16 25" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1198 874 20-25m20 4-17 24m13 10c1.1-.3 16-22 16-22" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M998 893c1.4-.1 19 28 19 28" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M979 912c1.4-.1 19 28 19 28" />
	</svg>
);

export default Component;

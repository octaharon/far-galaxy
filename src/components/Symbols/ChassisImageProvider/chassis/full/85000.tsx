import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="m361 1042 24 6 279 167 23 27 124 42-8-49-60-99 320 93s120.3 115 223 115c85.9 0-3.9-164.5-12-187 19.7-20.3 111-117 111-117l158 117 37-72 4-47-1-142 10-10 12-38-13-241-20-10-18 1-72 193-121 24-23-18-110 35-2 24-105 32s-77.9-118-182-118c-68.8 0-118 63.9-118 108-17.4-12.5-36.9-33.9-64-13s-5.2 58.3 7 78c-37.5-2-139-10-139-10l-14-25-151-14 1 30-142 7-102-207-25 2 134 334 27 5 8-23z"
		/>
		<linearGradient
			id="ch-full85000a"
			x1={854.4018}
			x2={914.0447}
			y1={745.8309}
			y2={953.8309}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.306} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full85000a)" d="m744 1137 318 92s10.1-26.3-150-186c-35.6-7.1-93-21-93-21l-144-1 69 116z" />
		<linearGradient
			id="ch-full85000b"
			x1={794.1111}
			x2={968.6441}
			y1={706.3469}
			y2={914.3469}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.303} stopColor="#949494" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85000b)"
			d="m744 1137 318 92s10.1-26.3-150-186c-35.6-7.1-93-21-93-21l-144-1 69 116z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full85000c"
			x1={1362.116}
			x2={1173.847}
			y1={802.8378}
			y2={1004.7318}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.251} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85000c)"
			d="m1275 1158 111-118s8.6-27.5-71-107c-28 13-117 70-117 70s21.8 37.2 41 76c19.3 38.7 36 79 36 79z"
		/>
		<linearGradient
			id="ch-full85000d"
			x1={481.4025}
			x2={613.6525}
			y1={785.5562}
			y2={1014.6201}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.301} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85000d)"
			d="m361 1042 26 6 275 167s-4.8-30.1-104-166c-62.5-40.8-205.2-64.5-218-63 4.7 10.7 21 56 21 56z"
			opacity={0.8}
		/>
		<path fill="#bfbfbf" d="M677 1192 462 894l1 30s145.6 181.2 200 290c6.5 7.3 22 23 22 23l-8-45z" />
		<linearGradient
			id="ch-full85000e"
			x1={762.479}
			x2={726.521}
			y1={726}
			y2={637}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path fill="url(#ch-full85000e)" d="m802 1237 9 46-126-41-7-48 124 43z" />
		<linearGradient
			id="ch-full85000f"
			x1={735.329}
			x2={1237.207}
			y1={606.105}
			y2={957.5241}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#2e3033" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full85000f)"
			d="M821 879c0-44.1 49.2-109 118-109 104.1 0 182 118 182 118s42.8 58.3 84 127.8c28 47.2 54.8 100.2 69 141.3 8.1 22.5 97.9 187 12 187-99.9.5-221.7-113.9-223-115-.8-35.7-126.8-165.6-168-203-.2-6.1.3-35.2-2-72-40.1-40-64.7-68.1-72-75.1z"
		/>
		<linearGradient
			id="ch-full85000g"
			x1={1062.0995}
			x2={1322.2001}
			y1={640.2285}
			y2={761.5153}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.598} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.676} stopColor="#fff" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.798} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85000g)"
			d="M1159 1066c63.5-.2 68.8-28.1 117 89 8.1 22.5 97.9 187 12 187-99.9.5-221.7-113.9-223.1-115 .1-122.6 67-160.2 94.1-161z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full85000h"
			x1={798.5567}
			x2={1182.5156}
			y1={757.6489}
			y2={1103.3678}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.598} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.676} stopColor="#fff" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.751} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85000h)"
			d="M821 879c0-44.1 49.2-109 118-109 104.1 0 182 118 182 118s42.8 58.3 84 127.8c-65.6-8.5-163.5-19.5-142 213.3-.8-35.7-126.8-165.6-168-203-.2-6.1.3-35.2-2-72-40.1-40-64.7-68.1-72-75.1z"
			opacity={0.302}
		/>
		<path
			fill="#4d4d4d"
			d="M1242 1184.3c.4-.3 1.2-.6 1-1-5.6-10.8-60.1-129.8-129-212.3-28.4 9.3-35.5 17.2-53 44 5.2 42.7 13.5 80.3 33 102s96.2 76.3 107 83c9.6.1 20-.3 20-.3l.4-.4c5.5-7.8 12.9-10.5 20.6-15z"
		/>
		<path
			fill="#4d4d4d"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1182 977c-11.3-7.3-21.4-16.6-49-14 62 80.6 105.9 165.5 131 220 10.9-1.3 17.1 2.2 25 8-13.3-32-62.2-149.1-107-214z"
		/>
		<radialGradient
			id="ch-full85000i"
			cx={2078.8169}
			cy={448.9235}
			r={229.5637}
			fx={2077.1445}
			fy={448.9367}
			gradientTransform="matrix(0.8656 -0.502 -1.522 -2.4745 -60.8824 3329.4553)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.344} stopColor="#4d4d4d" />
			<stop offset={0.5263} stopColor="gray" />
			<stop offset={0.628} stopColor="#e6e6e6" />
			<stop offset={0.7351} stopColor="gray" />
			<stop offset={0.9452} stopColor="#4d4d4d" />
		</radialGradient>
		<path
			fill="url(#ch-full85000i)"
			d="M1244 1186.3c.4-.3 1.2-.6 1-1-5.6-10.8-60.1-134.8-129-217.3-28.4 9.3-41.5 22.2-59 49 5.2 42.7 19.5 86.3 39 108s96.2 70.3 107 77c9.6.1 20-.3 20-.3l.4-.4c5.5-7.8 12.9-10.5 20.6-15z"
		/>
		<radialGradient
			id="ch-full85000j"
			cx={1165.5114}
			cy={-711.717}
			r={325.8229}
			gradientTransform="matrix(0.5204 0.8539 0.2366 -0.1442 696.9999 -11.707)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.344} stopColor="#4d4d4d" />
			<stop offset={0.8336} stopColor="gray" />
			<stop offset={0.8938} stopColor="#e6e6e6" />
			<stop offset={0.9656} stopColor="gray" />
			<stop offset={1} stopColor="#4d4d4d" />
		</radialGradient>
		<path
			fill="url(#ch-full85000j)"
			d="M1135.7 962c32-1.7 42.7 9.5 47.7 17s96.6 176.9 103.3 211.5c-14.7-7.5-15.7-8.8-23.3-6.5-11.4-23-127.7-222-127.7-222z"
			opacity={0.5}
		/>
		<linearGradient
			id="ch-full85000k"
			x1={971.6187}
			x2={1284.7037}
			y1={647.2376}
			y2={1221.2375}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.251} stopColor="#dae3f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85000k)"
			d="M821 879c0-44.1 49.2-109 118-109 104.1 0 182 118 182 118s42.8 58.3 84 127.8c28 47.2 54.8 100.2 69 141.3 8.1 22.5 97.9 187 12 187-99.9.5-221.7-113.9-223-115-.8-35.7-126.8-165.6-168-203-.2-6.1.3-35.2-2-72-40.1-40-64.7-68.1-72-75.1z"
		/>
		<linearGradient
			id="ch-full85000l"
			x1={774.1971}
			x2={774.1971}
			y1={985.245}
			y2={1057.5385}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#494949" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path fill="url(#ch-full85000l)" d="M758 925c-1.2-19.1 2.5-46.7 46-54 .4-3.6-20.2-27.1-48-5s3.2 78.1 2 59z" />
		<path fill="#bfbfbf" d="m1582 894 11-8 12-38-12-241-38-9-73 194 105 63-14 10 9 29z" />
		<linearGradient
			id="ch-full85000m"
			x1={1253.5}
			x2={1253.5}
			y1={482}
			y2={598}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.303} stopColor="#fff" />
			<stop offset={1} stopColor="#616161" />
		</linearGradient>
		<path fill="url(#ch-full85000m)" d="m1276 1344 43 45-34 49s-70-81.3-97-116c35.3 15.7 88 22 88 22z" />
		<path fill="#4d4d4d" d="m1284 1438-27-36 64-13-34 52-3-3z" />
		<path fill="#bfbfbf" d="m1232 834 321 280-11 44-155-115s7.3-54.5-159-190c.8-11.7 4-19 4-19z" />
		<path
			fill="#e0e0e0"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m850 911 35-21s40.4 48.8 85 103c-11.7 62.2-2 106-2 106-20-21.8-54-55-54-55l-22-88-42-45z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M461 921c25.7 30.5 204.4 272.2 202 293" />
		<path
			fill="#bfbfbf"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1554 597s.2-1.1.6-1.2c.2-.1.6-.2 1-.3 5.3-1.8 22.5-5.5 22.5-5.5l32 9 11 248-12 44-26 21v-16l10-10 12-39-13-239-38.1-11z"
		/>
		<path fill="#666" d="m1588 898 9-9h7s-16 12.5-16 13v-4z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1480 791 122 65s0 1.7-1 2c-5.4 2-24 6-24 6l2 32" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1225 852s167.2 132.7 160 187" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1121 888c98.9 141.3 104.8 162.1 158 280" />
		<path
			d="M464.3 896.9C482.4 922 677 1193 677 1193s111.7 37.2 124.8 41.6c.6.3 1.1 0 .6-.7C790.1 1213.2 610 908 610 908s-128-11.3-145.1-12.8c-.8-.1-.8 1.1-.6 1.7zm765.6-64.1C1249.1 849.5 1552 1114 1552 1114s17.9-37.6 33.4-70.3c.8-.9.9-.4-.4-1.6-17.2-16.5-243-244.1-243-244.1s-96.1 28.2-111.2 33.4c-2.4.8-2.1.3-.9 1.4z"
			className="factionColorPrimary"
		/>
		<path d="M761 938c5.2 8.9 57 85 57 85l74 2 1-71-79-81s-69.7-6.1-53 65z" className="factionColorSecondary" />
		<linearGradient
			id="ch-full85000n"
			x1={792.1966}
			x2={881.0959}
			y1={999.8378}
			y2={898.7802}
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.6248} stopColor="#0d0d0d" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full85000n)"
			fillOpacity={0.3}
			d="M761 938c5.2 8.9 57 85 57 85l74 2 1-71-79-81s-69.7-6.1-53 65z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1062 1229c-5.5-52.4-174-209-174-209" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1552 1114-9 44" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m677 1193 9 50" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m463 896 215 299 124 40m-53-91L624 931m607-97 321 280 32-72-227-229"
		/>
		<path
			fill="#bfbfbf"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m361 1043-42-111-89-182-26 6 89 219 38 94 23-5 7-21z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M761 938c5.2 8.9 57 85 57 85l74 2 1-71-79-81s-69.7-6.1-53 65z"
		/>
		<path
			fill="#e6e6e6"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M819 1023c6.7-53.5 60.7-66.8 74-67 .1 2.1 21 88 21 88s-57.7-13.2-95-21z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1081 984c-34.1-53.1-158.8-206.4-207-193" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1157.7 963.5C1121.1 907.7 1009.8 767 956 771" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1296 1343c-74.7 0-46.8-31.1-88.1-53.9-81.6-45.1-172.4-150.6-242.9-230.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1282 1344 40 45-36 51s-74.7-90.9-104-127" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1254 1401 65-14" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m361 1042 24 6 279 167 23 27 124 42-8-49-59-100 319 94s123.1 115.5 223 115c85.9 0-3.9-164.5-12-187 19.7-20.3 111-117 111-117l158 117 37-75 6-38s-2.7-4.4-6-9c.8-37.3-1-139-1-139l14-10 12-38-13-241-38-9-72 193-124 23-17-17-113 35-1 22-106 34s-77.9-118-182-118c-68.8 0-118 63.9-118 108-17.4-12.5-36.9-33.9-64-13s-5.2 58.3 7 78c-37.5-2-139-10-139-10l-14-25-151-14 1 30-142 7-102-207-25 2 139 343 22-4 8-23z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1248 1188c.4-.3 1.2-.6 1-1-5.6-10.8-64.2-138.3-133-221-28.4 9.3-42.5 21.2-60 48 5.2 42.7 13.5 83.3 33 105s100.2 79.3 111 86c9.2.6 23 0 23 0l.4-.4c4.5-7.9 14.8-15 24.6-16.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1182 977c-11.3-7.3-21.4-16.6-49-14 62 80.6 105.9 165.5 131 220 10.9-1.3 17.1 2.2 25 8-13.3-32-62.2-149.1-107-214z"
		/>
	</svg>
);

export default Component;

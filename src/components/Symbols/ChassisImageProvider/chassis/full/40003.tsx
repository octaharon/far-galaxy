import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#ccc"
			d="M684.1 1085.6c36.3 21 58.6 15.8 90.9 4.4 30.8 64.6 32.6 60.5 25 121-1.6 12.6-5.1 23.2-12 27-11.7 9.1-15 15.9-24 20-7.8 2.5-14 3.7-20 5-17.9-.1-33.7 5.8-48 4.9-21.5-1.4-54-13.3-76.6-36.6-17.2-17.7-23.2-50.5-29.2-81.5 26.4 17.4 43.4 35.8 70.2 41.2-16.2-18.6-15.8-46.9-16.4-66.9 23.7 20.5 23.4 15.7 31 27.5.1-4.5-9 9 9.1-66z"
		/>
		<path
			fill="#d9d9d9"
			d="M350 1213c-10.6-3.8-8.6-1.5-40-34 0 28.5 5.5 56.8 27 76-41.2-9.2-54.1-25.8-73-48 5.8 70.8 30.2 87.8 51 103s51 21.3 76 21 29.3-4.4 40-10c13.7 11.7 37-15 43-22s14-28.1 14-70-12.8-76.9-17-84c-13.7 7.6-54 32.2-115 2-6.4 28.7-12.5 69.8-4 133"
		/>
		<radialGradient
			id="ch-full40003a"
			cx={395.5}
			cy={796.001}
			r={374.585}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.479} stopColor="#000" stopOpacity={0} />
			<stop offset={0.599} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full40003a)"
			d="M350 1213c-10.6-3.8-8.6-1.5-40-34 0 28.5 5.5 56.8 27 76-41.2-9.2-54.1-25.8-73-48 5.8 70.8 30.2 87.8 51 103s51 21.3 76 21 29.3-4.4 40-10c13.7 11.7 37-15 43-22s14-28.1 14-70-12.8-76.9-17-84c-13.7 7.6-54 32.2-115 2-6.4 28.7-12.5 69.8-4 133"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-full40003b"
			cx={717.981}
			cy={854.8621}
			r={366.511}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.479} stopColor="#000" stopOpacity={0} />
			<stop offset={0.599} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full40003b)"
			d="M667 1193s-46.9-17.6-75-44c3.9 26.4 11.1 58.1 20 71s51.4 47 88 47 65.8-9.6 75-16c5.9-9.1 18.9-17.3 25-18 1.4-19.9 7-81.1-27-143-26.6 8.2-57.7 22.2-88-5-6.2 21.7-9.1 54.9-11 104-2.3 1.8-7 4-7 4z"
			opacity={0.2}
		/>
		<path
			fill="#bfbfbf"
			d="M1003 392c-9.2 9.4-15.1 14.6-19.6 24.2-9.3 19.6-18.9 25-24.4 36.8s-11.1 36.4-20 52-30.5 53.2-38 134 122 19 122 19-7.7-91.7-17.3-125.7c1.1-11.2 10.3-27 22.3-49.3 2.1-5.3 6.5-20.1 9-25 16.8-48.8-2.1-45.6-34-66z"
		/>
		<linearGradient
			id="ch-full40003c"
			x1={1039.6161}
			x2={944.3311}
			y1={1434.0999}
			y2={1470.6758}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#383838" />
			<stop offset={0.614} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003c)"
			d="M998 395c-3.8 9.8-6.4 18.5-10 26-7.2 15-18.9 25.3-26 42s-5.3 24-15 47c-6.3 14.9 11.3 22 29 23 9.7-2.1 20.7-3 25-3-.3-9.5 13-32 13-32s2.7-3.2 5.1-8.2c1.3-2.8 1.9-6.1 3.6-9.8 5.1-11.1 11.1-24 18.3-34-16.8-10.1-30.6-37.3-43-51z"
		/>
		<path
			fill="#bfbfbf"
			d="M1059 657c49.2-5.9 229.2 228.4 243 320s-7.9 164.3-3 199 .4 33.4-7 39c19.1 66.5 19.5 117.5 18 129-31 20.9-81.9 13.5-95 0-7.2-14.2-31.1-61.7-40-96s-25.4-61.1-37-77c-6.2-8.4-10.2-28.3-25-45-9.5-10.7-35.7-12.6-54-7-.7.3-9.2-1.9-19 0-9.6 3.4-38.7 21.8-46 46-8.6 28.4-4.6 63.5-14 91 1.6 31.6-15.4 67.8-16 94s-13.1 98.3-8 129 14 72-66 72-54-84.9-54-101c0-16.1-10.6-37.5-20-72s-22-123.3-22-142c0-10.9 16.3-89.6-4-155-65.4 37.6-90.1 12.9-104 1 6.3-13.4 26.5-41.1 48-56-14-12-61.6-41-126-46-10.7 13.8-28.7 49.2-30 52 6.8 4.8-4.6 47.4-8 61-.8 3.1-6.3-1.8-7 1-49.7 36.6-68.2 34.7-90 50-47 28.1-94.9 14.1-114 3 17-44.6 45.2-63.7 67.6-83.8-4.6-9.5-7.5-20.6-7.6-31.2-.2-16.5 7.1-45.4 23.2-50.5.8-23.6 2.4-45.3 4.8-58.5 4-21.5 33.1-76.7 48.9-120 18.6-20.5 40.4-45.7 131.1-74 140.6-26.5 394.8-67.6 432-72z"
		/>
		<path
			d="M481 1064c5.1 7.6 53.7 24.8 78 28-15.8 17.2-64.4 39-86 52s-65.4 28.3-114 2c12.1-24.7 43.2-71.4 66-80 11.6 13.8 29.9 24.3 56-2z"
			opacity={0.149}
		/>
		<radialGradient
			id="ch-full40003d"
			cx={557.708}
			cy={962.394}
			r={289.175}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.449} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full40003d)"
			d="M481 1064c5.1 7.6 53.7 24.8 78 28-15.8 17.2-64.4 39-86 52s-65.4 28.3-114 2c12.1-24.7 43.2-71.4 66-80 11.6 13.8 29.9 24.3 56-2z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full40003e"
			cx={552.269}
			cy={880.838}
			r={290.666}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.448} stopColor="#000" stopOpacity={0} />
			<stop offset={0.552} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full40003e)"
			d="m622 880-48 15-8-16s-69.3-13.7-122 49c-.5 23.3-1 55-1 55l158 11s37.5-62.8 36-80c-6.5-11.8-15-34-15-34z"
			opacity={0.4}
		/>
		<path
			d="M735 1026c12.7 5.2 48 27.8 53 33 2 10.2 2 20 2 20s-26.7 22-61 22-44-17-44-17 5.4-20.4 50-58z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full40003f"
			x1={1182.1677}
			x2={1318.1677}
			y1={697.5912}
			y2={586.3162}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.529} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003f)"
			d="M1176 1257c15.5.5 41.1-7.8 46-21 14.4 1.2 39.7-4.1 70-22 3.1 6.5 20 85.9 20 128-29.6 20.8-72.2 16.9-94 8-11.1-14.9-34.6-72.6-42-93z"
		/>
		<linearGradient
			id="ch-full40003g"
			x1={1095.8367}
			x2={1324.5826}
			y1={504.799}
			y2={504.799}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.282} stopColor="#bdbdbd" />
			<stop offset={0.536} stopColor="#e6e6e6" />
			<stop offset={0.8936} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#bdbdbd" stopOpacity={0.9} />
		</linearGradient>
		<path
			fill="url(#ch-full40003g)"
			d="M1194.8 1320.1c38.3 22 86.7 22 115.1-.9 20.5 53.7 19.1 124.4-2 161-14.2 11.9-25.8 23.5-44.6 24-3.8-1.5-1.9-2.1-9.3.1-18.9-.1-31.6 7.9-46.7 6.9-22.7-1.5-56.9-13.9-80.7-38.4-18.1-18.6-24.4-53-30.7-85.5 27.8 18.3 45.7 37.5 74 43.2-17.1-19.5-16.7-49.2-17.3-70.1 25 21.5 24.6 16.4 32.7 28.8.5 1-10.3 9.5 9.5-69.1z"
		/>
		<path fill="#f3f3f3" d="M1203 1329c12.8 6.4 39.7 21.3 99 10-34.9 15.5-63.3 15.8-99-10z" />
		<linearGradient
			id="ch-full40003h"
			x1={871.4692}
			x2={917.1031}
			y1={590.3882}
			y2={465.0092}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#393939" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003h)"
			d="M820 1376c27.7-4.2 50-30 50-30s47.4 21.3 90 11c-2.3 11.8-10 66.1-10 103-14.1 4.2-46.5 24-110-1 3.1-23.2-7.7-41.8-20-83z"
		/>
		<radialGradient
			id="ch-full40003i"
			cx={928.995}
			cy={759.71}
			r={405.983}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" />
			<stop offset={0.531} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full40003i)"
			d="M815 1379c27.7-4.2 55-33 55-33s52.4 17.3 95 7c-2.3 11.8-7 72.1-7 109-14.1 4.2-54.5 22-125 1.6 2.5-22.9-5.8-43.3-18-84.6z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full40003j"
			x1={972.1038}
			x2={724}
			y1={361.9421}
			y2={361.9421}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#cecece" />
			<stop offset={0.225} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full40003j)"
			d="M827 1459c39.8 22.9 106.4 22.9 136-1 9.8 35 16.8 86.4-7 157-4.4 13.1-15.4 20.9-23 25-13.8 7.5-11.5 12.1-31.1 12.7-4.1-.1-1.3-4.5-9.7.1-19.6-.1-36.4 6.2-52.1 5.2-23.6-1.6-59.2-14.5-84-40-18.8-19.3-25.4-55.1-32-89 28.9 19 47.6 39.1 77 45-17.8-20.3-17.4-51.2-18-73 26 22.4 22.6 14.1 31 27-.4 2-4.3 10.6 12.9-69z"
		/>
		<path fill="#f2f2f2" d="M830 1466c10.7 6.1 50.8 22 104 11-12.5 15.3-87.4 11.7-106 0 .2-3.4.5-4.9 2-11z" />
		<linearGradient
			id="ch-full40003k"
			x1={683.7952}
			x2={467.0135}
			y1={899.3272}
			y2={978.2288}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.264} stopColor="#494949" />
			<stop offset={0.611} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003k)"
			d="M562 1094c7.7 1.9 10-7.7 14-23s3.8-20.1 3-36c-3.5-8.1 24.5-46.9 29-55 10.3-18.4 27.9-59.2 30-63-3.4-7.7-37.1-74-37.1-74L533 831l-65 41-27 29s3.7 53.8 3.5 79.1c-6 4.2-23.4 9.8-27.5 53.9 10.5 46.2 27.9 47 28 47 0 0 14 1.7 37-13.5 19.8 12 65.3 23.5 80 26.5z"
		/>
		<linearGradient
			id="ch-full40003l"
			x1={763.0017}
			x2={1028.6387}
			y1={1089.3513}
			y2={749.3514}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#3f3f3f" />
			<stop offset={0.645} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003l)"
			d="M799 932c12.1 16 37.3 48.5 56 47s47.8-10.4 66-59c11.3-30.1 13.5-59.3 11-81.2-1.5-13.5-20.9-23.3-25-29.8 1.1-6 26-6 26-6l15 2 13 62s57.2 63.5 85 114c27.8 50.5 38.1 106.2 18 140-36.3 16.2-262 22-262 22l-12-63s-4-36-3-52c-.5 8.3-6-59.1 12-96z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full40003m"
			x1={1120.3276}
			x2={1310.5347}
			y1={718.9386}
			y2={769.9045}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003m)"
			d="M1115.8 1125.1c2.8 10.9 12.1 27.2 21.2 46.9 10 21.7 16.2 48.1 24 53 14.9 9.5 31.9 80.7 43 99 27.7 24.1 102.6 10.3 102-1-1.8-35.1-12.1-84.2-10-111-10.1-26.9 4-83 4-83s-1.1-3.3 3-38 3.4-104.4-1-111c-8.7.1-21 1-21 1s-97.7 152.7-165.2 144.1z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full40003n"
			x1={1133.775}
			x2={1332.86}
			y1={883.6149}
			y2={684.5301}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.585} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003n)"
			d="M1111 1124c1 2.2 4.3 2.5 6 5 18.5 27.7 30.5 87.4 44 96 5.3 3.3 7.7 15.2 15 29 9.2 17.4 19.5 50.8 28.7 73.9 57.1 16.6 84.2 8.8 105.3-4.7-2.8-35.8-9.3-86-18-109.2-10.1-26.9 0-84 0-84s2.9-8.3 7-43 3.4-100.4-1-107c-8.7.1-17 1-17 1s-102.7 151.6-170 143z"
		/>
		<radialGradient
			id="ch-full40003o"
			cx={1227.1851}
			cy={808.1219}
			r={265.263}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.449} stopColor="#000" />
			<stop offset={0.521} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full40003o)"
			d="M1291 1213s-32.7 20.8-70 19c-6.1 9.7-20.7 26.6-43 22 7.3 22.4 26 71 26 71s65.6 30 106-1c-5.3-39-19-111-19-111z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full40003p"
			x1={687.2281}
			x2={749.8312}
			y1={858.1673}
			y2={1030.1674}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8c8c8c" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full40003p)"
			d="M798 934c-3.8-11.1-9.2-22.7-20-31-3.7-4-8.8-11.2-15-17-7.9 5-16.8 32.1-22 36-16.5 12.4-43.2 23.2-55.7 19-19.3-6.5-35-12.8-40.3-15-7.1 14.5-19.7 43.5-27 53 89.7 28.3 133.3 54.4 170 79-3-43-.2-102.8 10-124z"
		/>
		<linearGradient
			id="ch-full40003q"
			x1={613.9622}
			x2={718.9742}
			y1={963.1733}
			y2={953.9854}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#424242" />
			<stop offset={0.736} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003q)"
			d="M614 976c1.5-8.8 24-54.8 25-56 .6-.7 10.1 15.9 24.3 19.9 10.3 2.9 38.3-4.4 53-9 14.4 26-32.3 71.2-32.3 71.2s-37.5-15.8-70-26.1z"
		/>
		<linearGradient
			id="ch-full40003r"
			x1={1134}
			x2={1134}
			y1={794.1476}
			y2={1051}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.003} stopColor="#5c5c5c" />
			<stop offset={0.096} stopColor="#a6a6a6" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full40003r)"
			d="M1061 1123c28.9 2.6 56.8 8.3 94-11s139.9-138.3 142-164c-34.4 9.7-154.7 72.6-284-79-28.6 10.5-42 14-42 14s151.8 149.2 90 240z"
		/>
		<radialGradient
			id="ch-full40003s"
			cx={1410.718}
			cy={960.891}
			r={642.126}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.451} stopColor="#000" />
			<stop offset={0.552} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full40003s)"
			d="M1061 1123c28.9 2.6 56.8 8.3 94-11s139.9-138.3 142-164c-34.4 9.7-154.7 72.6-284-79-28.6 10.5-42 14-42 14s151.8 149.2 90 240z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full40003t"
			x1={1319.4581}
			x2={1062.6071}
			y1={1131.1602}
			y2={874.3101}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.376} stopColor="#1f1f1f" />
			<stop offset={0.767} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003t)"
			d="M1110 1118c28.9 2.6 56.8 8.3 94-11s139.9-138.3 142-164c-34.4 9.7-154.7 72.6-284-79-28.6 10.5-42 14-42 14s151.8 149.2 90 240z"
		/>
		<path
			fill="#f2f2f2"
			d="M1179 714c-6.9 8.5-12.8 21.8-20 32-28.7-.2-164.8-11.6-151 104-20.8 7.7-30.6 11.5-48 18-1.7-10.4-5.8-62.9-11.7-63.7-73.2-10-235.9 47.6-373.3 89.7-4.5-7.6-7.5-15.7-11-16-83.6-6.9-70.5-23.2-84-52 23.1-47.4 61-78.7 144-109 28-10.2 58.3-17.4 94-32 34.4-23.2 87.9-39 168-53 70.7-12.2 231.6-42.2 346 54 7.1-5.9 14.1-11.8 29-32 48.2 20.7 54.3 25.8 70 45-27.7-2.6-35.1 2-63 1-61.9 2.8-76.8 11.7-89 14z"
		/>
		<linearGradient
			id="ch-full40003u"
			x1={596.2682}
			x2={596.2682}
			y1={1025}
			y2={1201}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full40003u)"
			d="M606 882c-3.2-17 10.7-111.2 20-127-.3-21.8 0-36 0-36l-22 7s-57.5 86.7-30 169c14.8-6.8 24.1-10.1 32-13z"
		/>
		<linearGradient
			id="ch-full40003v"
			x1={932}
			x2={932}
			y1={1139}
			y2={1240}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full40003v)"
			d="M928 683c-16.5 9.1-59.5 40.8-71 62 10.6 9.9 33 36 33 36s49.6-68.6 117-101c-36.8 1.1-61.2.8-79 3z"
		/>
		<path
			fill="#d9d9d9"
			d="M1193 701s-88.6-82.5-217-79-224.9 32.8-257 65c7.1 12.3 13 21 13 21s138.7-22.6 191-24 194.5-21.8 256 23c8.5-3.8 14-6 14-6z"
		/>
		<path
			fill="#bfbfbf"
			d="M654 507s2 7.4 5.1 18.4c7.8 28.5 22.2 81.5 24.3 92.5.9 3.2 1.6 4.3 2.1 5.5C699.2 640.5 760 769 760 769s35.3 123.4-29 159c-74.6 41.3-111-21.2-111-75s11-174.9 7-210.7c-1.6-14.5-24.9-55.7-29.7-83.7-2.8-16.6-5.7-31.8-9.6-47.8-13.6-56.2 66.3-3.8 66.3-3.8z"
		/>
		<linearGradient
			id="ch-full40003w"
			x1={654.6223}
			x2={617.0552}
			y1={1391.3831}
			y2={1275.7642}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#444" />
			<stop offset={0.674} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003w)"
			d="M664.7 542.8C672.2 573.1 686 629 686 629l-57 16s-17.5-41.9-21-52c-2.9-8.3-13.8-38.3-17.6-63.6 12.8 4.1 32.7 11 46.6 15.6 4.5 1.7 1-8.1 5-8 5.7 1.4 12.3 1.7 22.7 5.8z"
		/>
		<linearGradient
			id="ch-full40003x"
			x1={694.3189}
			x2={694.3189}
			y1={982.6517}
			y2={1129.0541}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#494949" />
			<stop offset={0.531} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003x)"
			d="M627 806c-2.5 23.5-3.9 48.9.4 71.1 6.3 32.4 23.7 57.8 58.6 59.9 61 5.4 78-54 78-54s-9.8-13.5-26-35c-15.3-20.3-34-58.7-53-57-21.3 1.9-55.9-4.9-58 15z"
			opacity={0.502}
		/>
		<path
			fill="#f2f2f2"
			d="M923 827s-50.2-66.1-59-77c-60.5-51.5-177.4 20.6-130 93 76.5 101.7 70.7 98 86 118 79.9 68.6 141.8-55.2 103-134z"
		/>
		<path
			fill="#d9d9d9"
			d="m872 768 56 65s12.9 25.9 2 77-32.8 58.4-47 67c-4.8 2.9-28.7 2-34 2s-15.2-6.1-32.5-22.8C799.5 939.8 744.2 855 744 855c0 0 43.5 15.7 88-20 52.2-41.2 40-67 40-67z"
		/>
		<linearGradient
			id="ch-full40003y"
			x1={755.894}
			x2={946.6011}
			y1={942.2529}
			y2={1132.96}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.777} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full40003y)"
			d="m869 759 56 70s15.6 34.8 5 86c-10.9 52.4-38.7 60.9-53 62.7-5.6.7-19.8 2.3-25 2.3s-16.1-7.9-34-24-75-102-75-102 41.8 21.2 94-20 32-75 32-75z"
			opacity={0.302}
		/>
		<path fill="#e6e6e6" d="M452.5 983c29.9 2.5 66.2 88.3-4 101-28-22.7-16-102.6 4-101z" />
		<linearGradient
			id="ch-full40003z"
			x1={580}
			x2={482}
			y1={857}
			y2={857}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full40003z)"
			d="M489 1032c19.9 6 45.5 16.7 91 8-1.8 22.9-6.3 41.8-11 54-9.8-1.6-81-12.9-87-30 4.9-7.9 5.3-18.1 7-32z"
		/>
		<path
			fill="#e6e6e6"
			d="M456.5 983.6c8.9 1.6 27.5 18.4 18.4 64-7.2 36.3-25.1 35.7-28.1 35.1-3.1-.6-24.7-9.3-23.9-51.7.7-35.6 18.9-50.1 33.6-47.4z"
		/>
		<path fill="#e6e6e6" d="M798 1235c20.6-6.1 85 25.3 85 80 0 36.4-51.5 60.1-63 64s-41.3-138.3-22-144z" />
		<path
			fill="#e6e6e6"
			d="M801 1235c12.9-.2 48 17.4 48 84 0 53.1-29.5 58-34 58s-37.4-6.2-48-66c-8.9-50.2 12.6-75.7 34-76z"
		/>
		<path fill="#ccc" d="M984 1264c5.9 14.4 14 47.3-16 72-1.6.4-3 3-3 3s6.8-54.9 19-75z" />
		<path
			fill="#e6e6e6"
			d="M854 1271c25.5 9.2 100.9 34.2 128 6-7.6 27.4-11.1 35.7-14 64s-97.2 8.6-107 4c2.8-25.8 1.7-48.6-7-74z"
		/>
		<path fill="#d9d9d9" d="M1139 1171c17-3.3 56.5-12.7 77 38 13.2 32.6-14 41.6-21 44" />
		<path
			fill="#e6e6e6"
			d="M1300 1207c11.8-4.5 29.5-15 25-51-4.1-32.4-14.3-43.6-24-51-3.1 11.1-10.7 52.2-9 68s5.5 28.3 8 34z"
		/>
		<path fill="#bfbfbf" d="M1140 1171c12.9-.9 34 7 52 39s8.1 41.1 2 44c-8.2 4-23.1-.6-46-29s-20.9-53.1-8-54z" />
		<path
			fill="#e6e6e6"
			d="M1186 1179.5c75.8-9.6 95.8-32.5 113.1-49.1 1-.8 1.4-.9 2.9-1.4.3 1.8.5.5.2 2.8-2.3 22.3-2.7 50.4 2.8 74.2-10.2 7.1-56.3 32.4-90 24.5-6.8-21.5-7.9-26-29-51z"
		/>
		<path fill="none" d="M-1380.5-50c.3 0 .5.2.5.5s-.2.5-.5.5-.5-.2-.5-.5.2-.5.5-.5z" />
		<path
			fill="#f2f2f2"
			d="m649 426 40 17-5 10 178 71-78 12 40 67-181-67-7 10-46-17-51-87 33-7 15 6 33-7 5-8h24z"
		/>
		<linearGradient
			id="ch-full40003A"
			x1={867.1223}
			x2={790.1223}
			y1={1345.9733}
			y2={1385.2073}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={1} stopColor="#333" />
		</linearGradient>
		<path fill="url(#ch-full40003A)" d="m786 532 77-8-37 77-40-69z" />
		<path
			fill="#a8a8a8"
			d="m826 604-40-68-250-96 55 88s19.5 6.3 47.3 15.3c2.7.9 3.8-6.2 6.7-5.3 69.9 24.7 181 66 181 66z"
		/>
		<path
			fill="#595959"
			d="m624 537-47-79 5-6 18-2-6-3-19 2-4 7 45 77 8 4zm-38-92-17 1s-6.8 7.8-6 8 49 80 49 80l-6-1-49-81 5-8 15-2 9 3zm53-6 26-3-9-3-24 3 7 3zm7 3 30-1 8 2-33 2-5-3z"
		/>
		<path fill="#f2f2f2" d="m1370 460-33 67-243-48-6 9-62-13-26-80 70-7 63 12-4 7 241 53z" />
		<path fill="#a8a8a8" d="m1316 471-255-60-4-4-56-10 24 79 60 12 10-9 243 48-22-56z" />
		<path
			fill="#595959"
			d="m1067 481-28-76-4-1 28 77h4zm-7.9-84.3c-12 .7-23.1 1.3-23.1 1.3l7 3s11-.8 23.3-1.7c-3.5-1.3-5-1.9-7.2-2.6zm20 1.7c10.8-.8 19.9-1.4 19.9-1.4l-11-2s-7.5.4-16.8 1c1.5.4 5.4 1.7 7.9 2.4zM1024 403l24 74-4-2-24-74 4 2z"
		/>
		<linearGradient
			id="ch-full40003B"
			x1={1372.061}
			x2={1318.061}
			y1={1417.2881}
			y2={1444.802}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={1} stopColor="#333" />
		</linearGradient>
		<path fill="url(#ch-full40003B)" d="m1316 471 54-9-34 62-20-53z" />
		<linearGradient
			id="ch-full40003C"
			x1={761.16}
			x2={683.544}
			y1={1382.0046}
			y2={1485.0046}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#016617" />
			<stop offset={1} stopColor="#ffe35b" />
		</linearGradient>
		<path fill="url(#ch-full40003C)" d="m783 536 79-11-180-74-25 1-37-19-32 6 28 16-25 4 192 77z" />
		<linearGradient
			id="ch-full40003D"
			x1={1233.4279}
			x2={1173.8978}
			y1={1451.6298}
			y2={1530.6298}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#016617" />
			<stop offset={1} stopColor="#ffe35b" />
		</linearGradient>
		<path fill="url(#ch-full40003D)" d="m1313 470 51-10-235-52-6-7-24 2-52-12-8 1 46 11-21 7 249 60z" />
		<path
			fill="#016617"
			d="M935 877c0 70-46.7 105-66 105s-46-18-46-18-26.5-36.8-50-70c38.8 17 66.7 1.2 97-18s37.1-60.6 32-73c20.3 23.7 33.9 33.7 33 74zm-77-134c9.7 7.3 30.1 44.2-19 88s-96.6 23.7-107 7-20.4-52.4 19-81 84.9-30.7 107-14zm-86 65c11.2 13.3 28.3 6.2 36 1s26.7-16.9 14-34-30.5-6.7-39-3-22.2 22.7-11 36zm458-121c3.7-4.6 23-23 23-23s40.2 25.8 47 33c-21-.1-59 1-59 1s-14.7-6.4-11-11z"
		/>
		<path
			fill="#ffe35b"
			d="M1141 745c-15.5-1-73.4-1.9-103 24-29.5 25.9-37.6 32.8-64 34s-73-1-73-1l-16-27s41.1-65.1 114-94c75.8-3.4 140.9-2.4 177 26-14.3 4.2-22 9-22 9s-8 15.2-13 29zm-215-61c-12 7.3-60.1 41.6-68 59-23.1-8.8-28.4-11-43-11s-54.1 12.5-65 25c-6.2-14.2-17-47-17-47s68.7-17.8 193-26zm-302 64s-8.8 99.6 0 130c-9.3 4.4-18 7-18 7s-5.8-37.4-2-63c5-33.8 20-74 20-74zm-21-22c-7.1 10.9-49.5 90.7-32 159-11.3-7.2-32.3-8.7-56-4s-59.2 32.9-71 46c-3.5-19.4-1.2-52.9 4-73s32.3-98.5 155-128z"
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M801 1235c12.9-.2 48 17.4 48 84 0 53.1-29.5 58-34 58s-37.4-6.2-48-66c-8.9-50.2 12.6-75.7 34-76z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M452.5 981.6c8.9 1.6 27.5 18.4 18.4 64-7.2 36.3-25.1 35.7-28.1 35.1-3.1-.6-24.7-9.3-23.9-51.7.7-35.6 18.9-50.1 33.6-47.4z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M489 1031c19.9 6 45.5 15.7 91 7-1.8 22.9-6.3 42.8-11 55-9.8-1.6-81-12.9-87-30 4.9-7.9 5.3-18.1 7-32z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M968 1336c30-24.7 21.9-57.6 16-72-1-3.5-3-8-3-8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M798 1235c15-4.4 53.2 11 72.9 40.9m-3.3 72.8c-16.1 17.1-40.2 27.8-47.6 30.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1139 1171c12.1-2.4 35.7-7.8 55.7 7.7m24 53.4c-3.8 14.2-18.8 19.2-23.7 20.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M854 1271c25.5 9.2 100.9 34.2 128 6 .4.1.9.5.8.7-7.5 26.9-12 35.3-14.8 63.3-2.9 28.3-97.2 8.6-107 4 2.8-25.8 1.7-48.6-7-74z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1300 1207c11.8-4.5 29.5-15 25-51-4.1-32.4-14.3-43.6-24-51"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1140 1171c12.9-.9 34 7 52 39s8.1 41.1 2 44c-8.2 4-23.1-.6-46-29s-20.9-53.1-8-54z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1186 1179.5c75.8-9.6 95.8-32.5 113.1-49.1 1-.8 1.4-.9 2.9-1.4.3 1.8.5.5.2 2.8-2.3 22.3-2.7 50.4 2.8 74.2-10.2 7.1-56.3 32.4-90 24.5-6.8-21.5-7.9-26-29-51z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m866 524-41 81-182-69-5 9-47-16-56-90 35-4 11 4 35-5 5-7 28-1 41 17-7 9 183 72z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m617 434 45 18" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m572 436 52 19" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m865 524-83 11 43 67" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m784 536-191-74-10-6-45-15" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m685 450-98 8 54 81" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m1370 460-33 67-243-48-6 9-62-13-26-80 70-7 63 12-4 7 241 53z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1132 401-76 7 31 79" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1366 462-50 7 19 56" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1317 470-255-58-9-7-52-9" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1017 616.4c-3-25.3-7-58.6-11.4-74.1-.7-4-.5-6.2-.7-10.3 2.6-16.8 12.6-26.4 22-50 .8-2 1.6-4 2.5-6m-32.1-79.5c-18.6 21.4-38.8 60.3-40.3 63.5-5.5 11.8-9.1 39.4-18 55-7.9 13.8-25.6 49.6-34.8 112.1"
			/>
			<path
				fill="#1a1a1a"
				d="M1006.9 533.6c-4.5 1.4-9.1 2.5-13.7 3.1-4.6.6-9.3.8-13.8.7-4.6-.2-9.1-.7-13.5-1.7-4.4-.9-8.7-2.2-12.9-3.7 4.4.7 8.8 1.2 13.2 1.3 4.4.1 8.7-.2 13-.8 4.3-.6 8.4-1.6 12.4-3 4-1.3 7.8-3.1 11.5-5l3.8 9.1z"
			/>
			<path
				fill="#1a1a1a"
				d="M689.9 620.9c.1.6.1.8.1 1.1v.9c0 .6-.1 1.1-.1 1.7 0 .6-.2 1.1-.2 1.6-.1.5-.2 1.1-.4 1.6-.5 2.1-1.4 4-2.3 5.7-2 3.6-4.7 6.4-7.6 8.7-.8.5-1.4 1.2-2.2 1.6l-2.3 1.4c-.8.5-1.6.8-2.4 1.2l-2.4 1.2c-1.7.6-3.3 1.3-5 1.8l-5 1.4 4.5-2.6c1.4-.9 2.8-1.9 4.2-2.9l1.9-1.6c.6-.5 1.3-1 1.9-1.6l1.7-1.8c.6-.6 1-1.3 1.6-1.9 2-2.5 3.5-5.3 4.2-8.1.3-1.4.6-2.8.6-4.2v-1c0-.3 0-.7-.1-1-.1-.3-.1-.6-.2-.9 0-.1-.1-.3-.1-.4 0-.1-.1-.3-.1-.1l9.7-1.8z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M453 982c29.9 2.5 66.3 86.3-4 99" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M779.9 772.6c14.4-9.7 31.7-10.8 41.1.8 9.5 11.6-.8 27.1-11.2 34.4-10.4 7.4-29.3 12.8-40.3.9-7.9-8.7-4-26.4 10.4-36.1z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M726.3 832c39.7 73 180.2-20.7 134.5-84.9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M758 876.4c40 73.5 184.1-23 135.4-87.1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M926 829.5s-58.4-74.4-66-83.7c-51.9-44-179.2 27.7-125.9 98.2 65.6 86.8 76.8 103.3 90 120.4 79 64.7 135.1-67.6 101.9-134.9z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M592 530.4c1.9 9.2 3.6 18.5 5.2 28.3 4.7 28 28.1 69.2 29.7 83.7 4 35.8-7 156.9-7 210.7s36.4 116.3 111 75c12.5-6.9 21.3-17.2 27.2-29.3 3.4-10.3 3.8-16.8 3.8-16.8M749 758c-6.4-21.9-13.4-39.6-18.2-49.4-17.6-35.9-38.1-76.1-45.3-85.2-.5-1.2-1.1-2.4-2.1-5.5-1.7-9-11.5-45.5-19.3-74.2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M900 799c10.8 4.5 40.6 4.2 48.3 5.3 5.9.8 10 53.3 11.7 63.7 17.4-6.5 27.2-10.3 48-18-1.8-29.2-.8-47.9 9.2-60.8 9.3-11.9 30.2-49.8 143.8-45.3"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M855 745c9.3-9.7 45.2-48.8 76-61" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M942 804c9.5.6 62.9 2.3 78-17" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1135.7 958.8c-38-12.9-79.9-39.6-122.7-89.8-28.6 10.5-42 14-42 14s151.8 149.2 90 240c28.9 2.6 56.8 8.3 94-11 13.9-7.2 37-28.4 60.5-53.4"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M574 894s-8-15.2-8-43c0-17.4 7.6-80.4 39.1-124.6" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M637 916c-3.4 10.7-21.3 53-35 73s-22.5 36.1-25 49" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M608 979s32.1 5 33.3 5.2c28.8 3.6 129 56.2 145.7 74.8m1-72c-34.9 8.4-121-24.3-135-30-8.9-3.1-27-12-27-12"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M836 1461c-8.3-3.1-10.7-12.6 1-14" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M956 1459c3.5-2.2 9-9.8-1-14" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M816.7 1528.9c1.5-19.3 4.3-42.4 10.3-69.9 39.8 22.9 106.4 22.9 136-1 12.7 50 14.1 82.4-2 138-3.8 13.3-6.4 29.9-14 34-13.8 7.5-25.5 21.2-45.1 21.8-4.1-.1-1.3-4.6-9.7.1-19.6-.1-36.4 7.1-52.1 6.1-23.6-1.6-59.2-14.5-84-40-18.8-19.3-25.4-55.1-32-89 28.9 19 47.6 39.1 77 45-17.8-20.3-17.4-51.2-18-73 25.3 21.8 26.6 16.4 34.4 28"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1307.8 1322.9c3.3-2.2 8.6-9.4-1-13.4" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1198.9 1321.9c38.3 22 87.2 22 115.7-1 9.4 33.6 23.1 79.2 7.7 132.6-3.7 12.8-7.3 27.1-13.4 32.7-13.1 11.7-21.6 19.3-40.4 19.9-3.8-1.5-1.9-2.1-9.3.1-15.7 8.6-32.6 7.9-47.7 6.9-22.7-1.5-56.9-13.9-80.7-38.4-18.1-18.6-24.4-53-30.7-85.5 27.8 18.3 45.7 37.5 74 43.2-17.1-19.5-16.7-49.2-17.3-70.1 25 21.5 24.6 16.4 32.7 28.8.4 1-6.4-4 9.4-69.2z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1290.8 1210.3c2.9 13.1 12.9 78.1 17.7 115.6m-103.5.3c-10.5-22.6-23.3-53.9-29.3-75.4m-37-78.9c-7.7-11.6-15.2-33.6-28.7-46.9m-46.7-7.1c-1.4.3-2.8.7-4.3 1.1-64.1-3-64.4 35.7-75 103-2.8 8.3-6.1 33.9-9.9 63.3M964 1350c-.4 19-7.1 76.5-8.8 114.8m-119.6-1.3c-2-19-11.3-54.9-19.6-85.5m-17-449c-1.7 1.7-2 6.3-2.3 7.4-18.1 67.7-7.2 150.7-2.7 166.6 11.2 39.1 10 103.6 10 119 0 2.9.3 7.8.8 14.1"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M443.5 982.3c.6-21.2 2.2-42.7 4.4-59.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M736.3 1026.4c-13.7 8.6-43.2 35.4-51.3 59.6" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M795.1 1234.8c2.1-3.9 3.9-9.6 3.9-17.8 0 0 .9-11.3 1-29 .6-55.3-27-98-27-98s-.7-.4-1 0c-1.4 1.7-12.5 7.3-35 10-31.5 3.6-39.1-8-51.9-15.4-16 74.9-8.5 66.9-9.1 66-7.6-11.8-7.3-7-31-27.5.6 19.9.2 48.3 16.4 66.9-26.8-5.4-43.8-23.8-70.2-41.2 6 31 12 63.8 29.2 81.5 22.6 23.4 55.1 35.2 76.6 36.6 5.8.4 13.3-1 21.3-1 27 0 54.7-12.9 54.7-12.9m-1-164c11.7-2.4 14.8-10.2 21-14"
			/>
			<path
				fill="#1a1a1a"
				d="M742.2 1258.3c2.1-.6 4.3-1.8 6.2-3.2 2-1.5 3.7-3.4 5.4-5.5 3.2-4.2 5.6-9.3 7.5-14.6.8-2.7 1.7-5.4 2.3-8.2.3-1.4.7-2.8.9-4.2l.7-4.3c.3-1.4.4-2.9.6-4.3l.5-4.3c.2-2.9.4-5.8.7-8.7l.6 8.7.1 4.4c0 1.5.1 2.9 0 4.4l-.1 4.4c0 1.5-.2 2.9-.3 4.4-.2 3-.7 5.9-1.2 8.9-1.1 5.9-2.9 11.8-6 17.4-1.6 2.8-3.5 5.5-5.8 7.9-2.4 2.4-5.2 4.5-8.5 6l-3.6-9.2z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1012 620c-177.4 7.6-266 47-266 47l-27 17m182-55c190.4-29.3 276.6 58.3 319 93m-15-7s-.8-.4-3-1c-69.6-27.3-82.3-50.5-320-26-92.2 8.2-150 21-150 21"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1232.4 685.3c8.4-9.7 18.1-21 28.6-33.3 44 21.4 71.8 44.6 89.6 64.7"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1250.7 662.5c30.3 16.3 51.8 33.4 67.2 49" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1265 722c-27.2-27.2-90.7-108.9-274-101" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M862 649.8c4.5 0 8.2 5.9 8.2 13.2s-3.7 13.2-8.2 13.2-8.2-5.9-8.2-13.2 3.7-13.2 8.2-13.2zm-73.5 13.7c3.9 0 7 4.9 7 11s-3.1 11-7 11-7-4.9-7-11 3.1-11 7-11zM937 640.2c4.8 0 8.7 6.2 8.7 13.8s-3.9 13.8-8.7 13.8-8.7-6.2-8.7-13.8 3.9-13.8 8.7-13.8z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M623 879c-9 2.6-40 12.2-49 15-4.5-7.6-7.7-15.1-11.2-15.4-65.7-5.4-91.9 23.7-119.8 49.4-6.3-107 37.3-164.6 163.5-204.1 14.7-4.6 21.5-5.9 21.5-5.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M424 1065c-11.4 8.9-52.8 46.1-65 84m113.9-5.3c23.9-11.2 84.1-38.8 92.1-52.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M350 1213c-10.6-3.8-8.6-1.5-40-34 0 28.5 5.5 56.8 27 76-41.2-9.2-54.1-25.8-73-48 5.8 70.8 30.2 87.8 51 103s51 21.3 76 21 29.3-4.4 40-10c16.7 9.1 34-5 40-12s17-38.1 17-80-12.8-76.9-17-84c-13.7 7.6-54 32.2-115 2-3.9 17.2-7.6 42.8-8.1 70.9"
			/>
			<path
				fill="#1a1a1a"
				d="M416.8 1323.1c2.2-1.5 4.1-2.7 6-4.2 1.8-1.5 3.6-3 5.2-4.7 3.3-3.4 6.1-7.3 8.4-11.6 1.2-2.1 2.1-4.4 3.1-6.7.8-2.3 1.8-4.6 2.4-7.1.8-2.4 1.2-4.9 1.8-7.3.3-1.2.4-2.5.7-3.7l.6-3.8v3.8c0 1.3.1 2.6 0 3.8-.1 2.5-.2 5.1-.6 7.7-.2 2.6-.8 5.1-1.2 7.7-.6 2.5-1.2 5.1-2.1 7.6-1.7 5-4.1 9.9-7.1 14.3-1.5 2.3-3.3 4.4-5.1 6.4-1.8 2-3.9 3.9-5.6 5.6l-6.5-7.8z"
			/>
			<path
				fill="#1a1a1a"
				d="M889.9 1647.7c2.4-1.4 4.7-2.9 6.9-4.5 2.1-1.6 4.2-3.3 6-5.2 1-.9 1.8-2 2.6-3s1.6-2.2 2.3-3.3c.7-1.2 1.4-2.3 2-3.5.6-1.2 1.2-2.5 1.8-3.7 2.2-5.1 3.6-10.6 4.5-16.2 1-5.6 1.4-11.4 1.7-17.2.5 5.7 1 11.6.8 17.4-.2 5.8-.7 11.8-2.3 17.6-.4 1.5-.9 2.9-1.3 4.3-.5 1.4-1.1 2.8-1.7 4.3-.7 1.4-1.3 2.8-2.1 4.1-.8 1.3-1.6 2.7-2.5 3.9-1.8 2.6-3.9 4.9-6.1 7.1-2.2 2.2-4.5 4.2-6.9 6l-5.7-8.1z"
			/>
			<path
				fill="#1a1a1a"
				d="M1258.6 1501.1c2.4-.9 4.6-1.9 6.6-3.3 1.1-.6 2-1.4 2.9-2.2.9-.9 1.8-1.7 2.6-2.7 3.4-3.8 5.9-8.6 7.9-13.6.5-1.2.9-2.6 1.4-3.9.4-1.3.9-2.6 1.2-4l1.1-4 .8-4.1.4-2.1.3-2.1.7-4.2c.4-2.8.6-5.6 1-8.4.1 2.8.2 5.7.3 8.5v6.4l-.1 2.1-.2 4.3-.5 4.3c-.1 1.4-.4 2.9-.6 4.3-.3 1.4-.5 2.9-.8 4.3-1.3 5.7-3.3 11.4-6.7 16.7-3.3 5.3-8.5 9.8-14 12.7l-4.3-9z"
			/>
			<path
				fill="#1a1a1a"
				d="M1188.9 1382.4c1.5 1.5 2.4 3.1 3.1 4.6.7 1.5 1.2 3 1.6 4.4.7 2.9 1.1 5.7 1.3 8.4.4 5.5.2 10.8-.3 16.1-1.1 10.6-3.3 20.9-6.8 30.7 1.9-10.2 2.4-20.6 1.8-30.8-.3-5.1-1-10.1-2.1-14.9-.6-2.4-1.3-4.7-2.2-6.7-.4-1-1-2-1.5-2.7-.5-.8-1.1-1.4-1.5-1.6l6.6-7.5z"
			/>
			<path
				fill="#1a1a1a"
				d="M352.6 1214.7V1216c.1.5.1 1 .2 1.6l.2 1.7.3 1.8c.4 2.4.8 5 1.2 7.5.7 5.1 1.2 10.2 1.3 15.3.2 5.1-.1 10.2-.7 15.2-.5 5-1.5 10-2.6 14.8.3-5 .5-10 .1-14.9-.3-4.9-.8-9.8-1.8-14.6-.9-4.8-2.2-9.5-3.7-14.2-.7-2.3-1.6-4.6-2.4-7l-.6-1.8-.6-1.9c-.2-.6-.4-1.4-.6-2.1-.1-.3-.2-.8-.2-1.2l-.2-1.3 10.1-.2z"
			/>
			<path
				fill="#1a1a1a"
				d="M822.3 1522.8c-.9 5.4-.7 11.6-.5 17.8.2 6.2.4 12.6.1 19-.3 6.4-1.2 12.8-3.1 18.9-1.9 6.1-5 11.8-9.1 16.6 3.4-5.2 5.6-11.1 6.6-17.1 1.1-6 1-12.2.5-18.2-.5-6.1-1.6-12.2-2.6-18.3-.5-3.1-1-6.2-1.4-9.4-.4-3.2-.7-6.6-.5-10.1l10 .8z"
			/>
			<path
				fill="#1a1a1a"
				d="M678.8 1146.1c1.3 2 2 3.8 2.6 5.7.6 1.8.9 3.6 1.2 5.3.6 3.5.9 6.9 1 10.2.3 6.7.1 13.3-.3 19.9-.8 13.1-2.4 26.1-4.4 38.9.4-13 .3-26-.6-38.9-.4-6.4-1.1-12.9-2.1-19.1-.5-3.1-1.2-6.2-2.1-9-.4-1.4-.9-2.8-1.5-4-.6-1.2-1.2-2.3-1.8-3l8-6z"
			/>
		</g>
		<g>
			<path
				fill="#f2f2f2"
				d="M1198.5 768.5c-1.4-21.5 59.6-32.6-20.1-56.7 2.9-17 58.8-7.9 88.6-13 29.9-5.2 53.3 2.5 97.9 20.2 56.1 6.8 97 5 97 5s116.1 62 147.9 128.4c-1.4 7.6-9.7 17.2-18.6 24.1 6.7 17.7 9.5 39.7 6.8 66.9-9.7 33.2-16.7 52.5-32.6 75.8-17.2 5.4-36.3 9.5-57.4 12.3 7-31.3 8.7-38.2 8.4-51.5-4.1 1.4-7.9 1-15.4 1.2.4-10.7.9-75-31.9-85.1-97.2-26.1-220.4-64.3-270.6-127.6z"
			/>
			<linearGradient
				id="ch-full40003E"
				x1={1471.5469}
				x2={1471.5469}
				y1={929.155}
				y2={1072.1737}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#d9d9d9" />
				<stop offset={1} stopColor="#f2f2f2" />
			</linearGradient>
			<path
				fill="url(#ch-full40003E)"
				d="M1420 845c14.6 6.9 83.9 34.9 82 138 10.2-4.7 17-4 17-4s13.7-54-9-90c-31.2-51.5-78-49-78-49s-8.4 3.3-12 5z"
			/>
			<linearGradient
				id="ch-full40003F"
				x1={1350.8917}
				x2={1350.8917}
				y1={602.176}
				y2={1041.873}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#8c8c8c" />
				<stop offset={1} stopColor="#e6e6e6" />
			</linearGradient>
			<path
				fill="url(#ch-full40003F)"
				d="M1160.7 714.1c13.4-.5 27.6.5 42.6 8.3 47.3 24.3-20.3 23.4-1.2 53.2 32.5 35.5 96.7 64.4 150.1 83.9 41.8 15.2 95.5 27 130 41.4 15.5 2 19.4 64.2 20.1 82.7 5.4-.1 11.7-3.1 16.5-4.7-2.8 16.7-12.2 36.7-5.9 36.6 8.7 7.6 31.2 10.1 55.6 0 27.8-32.7 35.2-100.2 23.6-140.6 6.7-8.5 13.6-16.2 14.8-19.3.6-1.6 2.9-6.7 2.2 1.8 1 40.3 8.6 74.2-15.8 147.4-26.5 79.7-81.1 143.1-177.3 146.5-60.5 5.1-112.2 2.1-157.2-10.6-56.3-15.9-107.1-36.9-138.3-72.1-15.6-15.8-20-81.1-18.9-119.4-5.2-8-42-94.6 59.1-235.1z"
			/>
			<path
				fill="#016617"
				d="M1435 830c-9.4-9.2-27.2-27.4-53-41-.8-12.7-1-18-1-18s-85-47.8-118-62c-19.8 1.5-39 4-39 4s17.6 12.9-1 33c-13 1.4-23.2 8.8-23 10s-3.8 20.3 14 34 109.4 61.2 176 80 97.3 36.9 101 53c1.4-17.3-30.7-60.2-71-79 14.2-8.1 24.4-4.8 15-14zm24-23c-7.3-6.3-36.3-30.1-49-35-14.7-3.4-23-5-23-5s-61.3-40.6-109-58c14-.2 20-1 20-1l40 14 35 1 69 2s25.2-.8 41 12 109.6 68.9 125 111c-.1 10.2-9.1 21.6-15 25-8.9-13.2-40.1-60.9-106-69-14.3.8-20.7 9.3-28 3z"
			/>
			<linearGradient
				id="ch-full40003G"
				x1={1530.075}
				x2={1530.075}
				y1={878.749}
				y2={1097.3719}
				gradientTransform="matrix(1 0 0 -1 0 1920)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#ccc" />
				<stop offset={1} stopColor="#f2f2f2" />
			</linearGradient>
			<path
				fill="url(#ch-full40003G)"
				d="M1535.4 1024.9c.9.2 28.4-7.1 28.4-7.1s21.3-47.6 21.3-83.9-22.5-116.4-98.1-127.6c-7 1.8-11.8 9.5-11.8 9.5s68 14.9 78 88.6c10.8 80.1-18.7 120.4-17.8 120.5z"
			/>
			<path
				fill="#d9d9d9"
				d="m1512.9 1014.3 23.6 8.3s21.3-45.6 21.3-81.5c0-36-14.2-121.8-101.7-128.8-10.3.6-13.8 7.2-13.8 7.2l-9.8 18.8s59.2 8.5 78 56.7c21.8 55.6 2.4 119.3 2.4 119.3z"
			/>
			<path
				fill="red"
				d="M1545 1020c6.3-11.4 34.1-54.5 19-124s-77.9-84.1-99-87c24.8-4.5 41-6 41-6s67.2 10.7 84 99c9.6 62.2-20 117-20 117s-17.3 3-25 1z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1091.3 876.9c4.7-58.7 29.5-92.5 64.7-161.6 1-.4 2.6-2 3.5-2.4 39.2-13.9 90.6-17.2 140.4-15.2 17.4.7 50.6 19.5 67.1 21.2 60.7 6.4 92.9 3.9 103.8 10 36.8 20.6 111.3 71.2 139.1 119.9 5.1 102.3-12.2 166.6-51.8 224.3-40.5 58.8-81.1 80.7-202.3 80.6-121.5-.1-211.7-60.9-230.5-79.2-23.3-22.6-18.6-65.3-22.5-93.4-5.6-41.3-12.3-69.2-11.5-104.2z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1156.7 713.9c12.2-.8 56.5-1.7 66.6 25.1-4.1 15.9-36.9 5.6-18.9 37.8 18 32.2 152.1 88.5 238.8 109.9 56.3 11.8 46.1 58.8 46.1 72.1s-22.1 150.1-79.2 192.6"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1104 1002.5c-4.1-34.3-4.9-183.2 96.9-248.2" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1430.2 838.2c55.7 6.3 75.2 39.1 83.9 62.6 7.9 21.2 9.6 65.4-1.7 118.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1529.5 1027.3c12.8-18.7 35.3-56.2 24.8-115.8-9.3-53-40.7-96.2-103-99.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1512.9 1014.3s10.7 4.7 22.7 10c7.5-1.8 16.8-3.9 28.1-6.4 20.5-19.4 29.6-56.7 29.6-56.7s9.1-33.4 5.9-55.5c-3.2-22.1-8.6-49.8-43.7-75.6-43-31.6-78-24.8-78-24.8l-28.8 5.9-16.1 27.2s-5.5 2.4-14.2 5.9c95.2 42.6 82.7 129 82.7 138.3 7.1-2.1 11.4-2.9 17.7-4.7"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1608.7 847.7c-.6 12.6-9.2 21-15.4 24.8" />
		</g>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f3f3f3"
			d="M779 551c17.6 2.2 78.2 19.7 140 48 62.2 31.8 70 98.3 87 132 4 7.9 21.3 11 27 15 30 21.1 90.1 66.9 197 155 8.2 17 14 32 14 32l120 92 16-8 174 161 26 61s-360.6-330.4-463-293c3.8 6.1 26.9 38.2 32.8 47.7 35.1 57.1 67.3 127 67.3 162.3 1.5 15.3-18.2 22.6-57 33-4.7 1.3-9 7-9 7l-50 24s-13.6 5.8-72-4c-21.5-15.6-36-32-36-32l-1-12s-31.3-15.6-170-95c2.5 9.4 3.1 15.8 8 23 10.3 1.1 17-1 17-1s155.7 172.3 164 222c-9.3 19.7-14 34.9-18 39-98.4-49.6-327.4-295-388.8-378.7C509.6 878.8 583 815 583 815s-7.8-12.7-17-42c.7-62.8 41-110 41-110s-6.5-23.7 12-92c30-32.2 204.4-14.5 160-20z"
		/>
		<path
			fill="#e0e0e0"
			d="M630 774c-5.4 8.2-30.5 59.7-65 68-5.9 41.1-17.5 54.8 5 88s112.8 156.9 150 186c15.3-21.2 69.4-82.6 70-83s-23-62-23-62-100.6-125.8-127-193c-3.3-3.2-4.6-12.2-10-4zm144 400 55-70 20-4s97.3 115.9 106 128 54.7 71.9 56 93-16 39-16 39-45-29.6-93-70c-61-51.3-128-116-128-116z"
		/>
		<path fill="#d9d9d9" d="m1212 907 21-2 12 30-18 2-15-30z" />
		<path fill="#e6e6e6" d="m790 1032 41 71-58 71-56-59 73-83z" />
		<linearGradient
			id="ch-full50001a"
			x1={940.74}
			x2={601.3582}
			y1={884.1815}
			y2={884.1815}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b1b3" />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full50001a)"
			d="M760 914c26.2 27.6 69 74 69 74s95.3 112.1 142 147 68.4 47.7 85 54c-38.6-2.5-122.7-33-247-127-11.6-17.7-20-36-20-36l-14-42s-1.9-2.5-34-47-38.8-84.2 19-23z"
		/>
		<linearGradient
			id="ch-full50001b"
			x1={810.9444}
			x2={810.9444}
			y1={1129.2454}
			y2={1320}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.543} stopColor="#bebebe" />
			<stop offset={1} stopColor="#f3f3f3" />
		</linearGradient>
		<path
			fill="url(#ch-full50001b)"
			d="M807 628c-11.1-1-35 13.1-35 92 15.7 55.8 79.3 50.7 86 52s18 14 18 14-81.4 7-127 4c-45.6-3-79.4-31.3-77-102s82.7-88 132-88 94.3 20.1 117 55c8.1 12.4 12.3 17.6 14 27 15.8 25.3 15 52 15 52l-53-42s-3.6-56.3-90-64z"
		/>
		<linearGradient
			id="ch-full50001c"
			x1={877}
			x2={783.8571}
			y1={1151.9978}
			y2={1151.9978}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full50001c)"
			d="M861 773s-51-2.6-76-28c-.5 26-1.5 46.3-1 46s93-5 93-5l-16-13z"
			opacity={0.702}
		/>
		<path fill="#656666" d="M675 731s9 34.3 25 42c-12.3 8.4-25.1 16.1-32 29-7.2-6.1-15-15-15-15l22-56z" />
		<linearGradient
			id="ch-full50001d"
			x1={794.8607}
			x2={794.8607}
			y1={1095}
			y2={1332}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.543} stopColor="#bebebe" />
			<stop offset={1} stopColor="#f3f3f3" />
		</linearGradient>
		<path
			fill="url(#ch-full50001d)"
			d="M1004 728c-.1 2.2-2.6 6.7-8 8-2 8.6-9.8 32.3-17 23-7.3-9.3-28.7-18.2-31-44s-28.1-63-49-85c-21.8-16.8-51.5-31-98-31-107.6 0-120 58-123 89-.9 9.8-5 29.4-5 31 0 6.5 2.8 79.3 101 73-22.2 4.2-81 29-81 29l-59-49s-18.5 30.6-39 53c-16.8-32.2-7.3-93.8 1-137 9.8-36.6 46.5-71.8 67-82s67.7-18 142-18 74 2.1 115 11c51 17.6 81.8 122 84 129z"
		/>
		<linearGradient
			id="ch-full50001e"
			x1={750.1616}
			x2={724.2686}
			y1={1087.2139}
			y2={1148.2139}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.594} stopColor="#f3f3f3" />
			<stop offset={0.985} stopColor="#a0a0a0" />
		</linearGradient>
		<path fill="url(#ch-full50001e)" d="M792 785s-53.3 15.3-89-11c-28.2 7.7-31 21-31 21l41 40 79-50z" />
		<path
			fill="#999"
			d="m966 814-146-98s14.7 17.9 22.4 27.4c2.8 3.3-33.4 2.6-33.4 2.6s3.4 10.9 8 19c9.8 5.2 41 7 41 7l138 88-30-46zM855 707l-20-30s-4.7 19.3-4 20 24 10 24 10zm-265-8c-4.6 56.1-1 114-1 114s7.3 5.6 6 12c-1.1 5.4-10.6 11.8-12 11-1.5-.9-30.2 15.9-2-22-2-9.4-6.4-12.5-8-17-5.3-15-5-43-5-43s20.7-62.2 22-55z"
		/>
		<path fill="#d9d9d9" d="m878 693 20-3s-5.5-11.2-21-18c.3 8.5 1 21 1 21zm-47-17v21l-23 4s7-25 23-25z" />
		<linearGradient
			id="ch-full50001f"
			x1={832.1473}
			x2={832.1473}
			y1={1156}
			y2={1290.4153}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.875} stopColor="#f3f3f3" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full50001f)"
			d="M816 764c-21.3-18.5-29-123.9 75-93-18-19.9-49-45-88-41-11.5 1.2-36.4 15.5-28 101 20.8 23.5 27 26.3 41 33z"
		/>
		<path fill="#bfbfbf" d="m808 707 31 36-31-1s-10.5-23.9 0-35z" />
		<linearGradient
			id="ch-full50001g"
			x1={1026.1804}
			x2={956.1805}
			y1={1083.0111}
			y2={1107.1141}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#404040" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full50001g)" d="m1036 805-37 58-33-50 70-8z" opacity={0.851} />
		<linearGradient
			id="ch-full50001h"
			x1={1319.5}
			x2={1319.5}
			y1={667}
			y2={1063.7223}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#727273" />
		</linearGradient>
		<path
			fill="url(#ch-full50001h)"
			d="M1120 947c76.1-5.6 151.8 63.5 260 139 117.5 99.3 173 142.6 201 167-6-16.7-25.3-68-27-74-43.9-34.4-174-160-174-160l-26 16-126-97s-6.3-11.5-17-32c-81.3-62.7-129-52.2-153-42 12.2 5.3 48.4 51.6 62 83z"
		/>
		<path fill="#f3f3f3" d="m1372 1030 190 171-11-24-170-153-10 4" />
		<path
			fill="#807e80"
			d="M991 1177c26.5 9.5 94.4 30.5 165 13 4.4-4.9-35.1 52.2-124 26-14.6-6.1-29.9-23.2-41-39z"
		/>
		<path
			d="M595 696c-5.3 15.5-14.3 90.3-6 117-6.7-2.7-10-5-10-5s-14.7-28.9-12-50 33.3-77.5 28-62zm180 480 57-73-41-69-71 80 55 62zM658 609c-11.9-11.1-34-41-34-41s-19.6 43.5-17 95c14.2-24.2 28.2-44 51-54z"
			className="factionColorSecondary"
		/>
		<path
			d="M597 824c-16.1-17.4-7.7-106.3-4-129s32.1-60 39-67 41.2-9.7 48 25-26 134-26 134l-19-16s-29.4 42.8-38 53zm218-60c-12.8 1.2-39-30-39-30s-2.2-12.1-2-45 8.3-37.7 16-50c-.9-17.3-23-34.6-34-34 11.2-7.5 38.3-7.9 58-6 19.5 6.4 25 34 25 34s9.7 5.4 17 9c38.1 19.9 43 48 43 48l137 111-71 11s-140.6-96-159-110c-4.8 9-3.6 27 1.5 43.9 9.9-.4 41.5.1 41.5.1l8 23s-27.7 2.1-42-5zm15-66s25.2 12.1 25 11-17.5-28.8-25-36c-8.2 2.8-20.3 19.6-23 27 9.3-1.5 23-2 23-2zm45-26c-.6-.1 1.9 21.2 3 21s18-5 18-5-19.2-15.8-21-16zm102-8c17.5 43.7 26.6 63.7 28 70-12.6 1.7-16 27-16 27s-22.3-17.7-37-29c-8.9-60.9-32.9-80.5-51-100-11.7-18 4.4-34.7 10-37s43.9 27.8 66 69z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full50001i"
			x1={624.8293}
			x2={678.2701}
			y1={1177.1833}
			y2={1376.6257}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full50001i)"
			d="m633 772 22 16s29-89.1 25-130-43.2-36.8-49-30-36.5 37.3-41 95-2.1 94.1 7 100c9.7-8.3 36-51 36-51z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full50001j"
			x1={959.7272}
			x2={893.5502}
			y1={1226.0571}
			y2={1394.0571}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full50001j)"
			d="M910 596c-6.2 3.2-16 26.3-9 36s40.5 33 50 105c21.8 15.7 39 27 39 27s-6.9-31.2 19-33c-10.7-24.5-57.1-131.7-99-135z"
			opacity={0.2}
		/>
		<path
			d="M595 696c-5.3 15.5-14.3 90.3-6 117-6.7-2.7-10-5-10-5s-14.7-28.9-12-50 33.3-77.5 28-62z"
			opacity={0.2}
		/>
		<path
			d="M606.8 660.5C619.7 636.3 657 606 657 606l-34-38s-12.5 23.4-16 82c0 .4-.8 11.7-.2 10.5z"
			opacity={0.102}
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1007.1 734.6c-33.9-97-68.9-127.5-87.1-135.6-61.8-28.3-122.4-45.8-140-48 44.4 5.5-130-12.2-160 20-11.9 44-13 69.5-15 93-11.9 27.7-22 46-22 46s-13.8 32.1-15 52c1.9 41.7 17 51 17 51s-74.4 65.8 20.2 168.3c27.5 37.6 103.3 127.3 183.8 205.7 98.8 96.3 202.5 179.3 206.3 172.3 7-12.8 8.8-17.3 18-37C1006.2 1270.9 846 1099 846 1099s-6.7 2.3-15 3c-2.5-4.8-8.7-23.7-16-35 147.3 98.4 161.8 97.3 179 106-.4 5.7 0 11 0 11l37 31s36.5 10.9 75 4c18.9-6.9 31-14 31-14l18-15s28.4-7.4 54-18c7.1-2.5 8-2.5 8-20 0-35.4-31.2-101.2-66.3-158.3-5.8-9.5-25.9-41.6-29.8-47.7 79.3-14.3 254.8 133.5 463.8 309.3 1.1.9 1.7.1 2.2-.3-22.2-48.8-34-78-34-78l-171-160-19 7-117-91s-8.4-18.9-14-30.3c-71.4-57.4-155.8-126.6-205-160.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M992 1173c19.4 7.7 57.8 24.7 130 21.6" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1059 862c24.8-6.2 75.1-19.2 154 46 6.8 14.1 15 31 15 31l127 96 25-17"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1211 906c-36.4-31.5-187.7-151.5-215-166" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M637 623c17.9.1 42 9.1 43 32s-8 92.9-26 131" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m831.3 1102.3-56 72" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M790.3 1032.1c-25.2 29.3-74.1 86.2-74.1 86.2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1125 954c-36.8-63.4-100-126-100-126" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1156 1190c-144.4 33.1-228.6-94.1-375-254-12.8-14-52.6-55.8-58-54-24.7 8.3 40.5 85.5 45 91 13.1 32.8 14.2 34.5 23 59 9.5 14.9 22.2 35.3 31 48"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M972 1134c0-38.4 35.8-57.3 72.6-67.1m101.2-23.1c2.7-.3 5.3-.6 8.1-.8 3 0 20.7-.7 27.1 7.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M769 790c31.9 21.7 179.3 162.9 277.3 276.8 45 52.2 68.5 98.7 76.7 125.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M948.7 830.1c88.3 87.4 221.8 222.3 267.5 315.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M829 988c0-31 11.1-55.3 70.6-77.7m108.4-21.8c6.1-.6 12.5-1 19-1.5 22.9-1.5 42.3 1 53.3 2.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M669 799c11.8-12.8 21-20.3 35-24" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M757 959c-27.1-38.2-61-75.7-121-186-5.5-10.2-31 64-72 70"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M742 941c6.1-25.1 18.9-18.9 18-32-2.5-35.3-104.5-116.1-123-134"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M916 598c-95.4-19.6-231.5-9.7-259 10-22 15.7-38.8 31.9-52.2 55"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M607 660c-26.2 52.3-25.8 163-10 159" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M910 597c-11 2.7-13.5 13.1-9 35 25.1 22.6 46 59.3 51 105"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1032 746c-9.6-4.5-21.3-11-30-11s-17.3 16-10 29" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M672.6 725.4c10.7 47.6 46 64.6 88.4 64.6 51.7 0 106.5-4 121-4 15.4 10.1 117 78 117 78l38-62-103-81m-32.8-87.7C880.5 613.9 847 599 793 599c-23.4 0-88.3 10-112.7 54"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M678.5 691.5C702 712.9 734.6 720 775 720m131.5-20.2c17.1-7.2 25-23.3 20.5-37.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M890 791s-18.6-11.1-29-20c-40.1 0-62.6-8.8-80-32s-11.5-109 28-109 65 23 65 23 25 20.8 25 41c10.3 9.6 37 28 37 28"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M790.5 640.1c-3-17.4-18-36.2-40.5-35.1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M840 635c-.9-13.5-9.5-28.8-23.7-35.6" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M745.5 675c4.7 0 8.5 4.5 8.5 10s-3.8 10-8.5 10-8.5-4.5-8.5-10 3.8-10 8.5-10z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M626.2 569.3c5.2 12.8 23.5 30.4 30.8 37.7" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M860 644c-39.4-2.3-86.3 13.1-85 78" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M898 691c-5.9-16-25.8-25-47-25-31.4 0-72.2 46-33 98" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1033 803-71 8-155-108" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m962 810 37 51" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m809 699 22-2 1-24s1-.5 2 1c5 7.5 23 36 23 36l22-4-5-35"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M832 697c.2.6 26 14 26 14" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m873 693 23-3" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m809 706 17 24-17 10" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M810 745h34s4.5 2.6 3.6 1.8C843.2 743.1 825 729 825 729"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m848 747 12 24" />
		<radialGradient
			id="ch-full50001k"
			cx={865.2675}
			cy={1109.1167}
			r={340.0131}
			gradientTransform="matrix(0.7393 0.6734 0.1416 -0.1555 68.5335 400.6359)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.6753} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full50001k)"
			d="M700 775s27.4 77.6 45 101c10 13.3 163.3 173.1 246 199.2 62.7 19.8 102.2-18.2 131-32.3 66.8-32.5-66.1-176.8-98-212.9-9.3 12.7-24 33-24 33l-121-77s-105 5.2-123 4-56-15-56-15z"
			opacity={0.3}
		/>
	</svg>
);

export default Component;

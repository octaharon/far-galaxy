import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M922 708c-.1.7 33.8 24.4 51 78 42.8 31.7 121.7 83.2 185.2 127.6 23.1 16.2 44.2 43.4 60.8 56.4 62 48.7 466.7 306.5 410 423-85.4 61.7-269.5-143.5-452-283-18 29.7-78 123-78 123s-184.9-43.9-225-53c-6.8 29.7-17 97-17 97s-20.4 10.7-50-47c-2.7-34.7-3-65-3-65s-112.9-27.6-137 10c4.4 37.6 47.8 174 45 196-2.8 22-60.2 206.8-78 216s-43.7-14.1-72-52-183.6-359.1-195-401c-11.4-41.9 37.7-97.6 109-185 57-69.9 80.4-82.7 102-141 4.8-25.8 5.3-46.5-10-66-6-7.7-6.8-20.4-6-27 4.4-25.2 12-40 12-40s17.9-43 119-43c81.7 0 102.8 12 118 23 22.1 1.6 45.4 7.6 52 19"
		/>
		<linearGradient
			id="ch-full60002a"
			x1={667.8344}
			x2={619.7394}
			y1={661.3827}
			y2={809.4037}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60002a)"
			d="M668 1175c5.8-4.9 9-12 9-12s.7-40.3-59-111-50 31-50 31 16.8-8.3 32 5 26.3 23.3 68 87z"
		/>
		<path
			fill="#e6e6e6"
			d="M697 1375c7-2.4 16.9-5.1 10-29s-43.3-170.2-42-171-54.5-116.8-98-92c16.3 41.6 89.3 236.2 95 246 5.7 9.8 28 48.4 35 46z"
		/>
		<linearGradient
			id="ch-full60002b"
			x1={757.5006}
			x2={757.5006}
			y1={1214.8811}
			y2={1265}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-full60002b)"
			d="M653 682c0-7.7 1.5-27 106-27s103 17.2 103 23c-11.6 1.5-77.5.4-98 27-20.7 1.1-111-5.2-111-23z"
		/>
		<linearGradient
			id="ch-full60002c"
			x1={810.8021}
			x2={935.8251}
			y1={1059.6835}
			y2={1207.4185}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#747474" />
			<stop offset={0.499} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#bababa" />
		</linearGradient>
		<path
			fill="url(#ch-full60002c)"
			d="M947 787c-13.3-39.2-43.6-59-84-59s-51 52.3-51 80 18.1 74.2 65 67c-10.2-12.5-16.7-15.9-20-25s-16-115.7 90-63z"
		/>
		<linearGradient
			id="ch-full60002d"
			x1={861.8}
			x2={885.209}
			y1={650.3672}
			y2={766.3552}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60002d)"
			d="M677 1164c58.2-26.8 219.8 22 421 67-5.2-19.7-12.5-44.6-18-53-81.3-9.6-345.1-78.1-416-60 9.4 13.5 13.5 40.6 13 46z"
		/>
		<linearGradient
			id="ch-full60002e"
			x1={1274.7607}
			x2={1140.6556}
			y1={728.2702}
			y2={816.9982}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60002e)"
			d="M1099 1231c19.2-30 75.5-121.6 95-141 15.5-27.2 34.8-46.2 14-98-36.8 76.1-51.2 96.3-125 187 8.9 29 13.6 35.1 16 52z"
		/>
		<linearGradient
			id="ch-full60002f"
			x1={512.247}
			x2={620.2469}
			y1={1048.5336}
			y2={1199.7206}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60002f)"
			d="M565 844c29.7-32.2 66.8-63 104-82-33.5-25-66.3-81.3-81-100-10 7-13.1 10.6-13 13-12.5 24.6-14 40-14 40l6 24s36.3 28-2 105z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full60002g"
			x1={610.4755}
			x2={583.3676}
			y1={1147.571}
			y2={1086.571}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full60002g)" d="M583 778s27.6 8.3 31 6 8-3 8-3l7 6-61 52 15-61z" opacity={0.4} />
		<linearGradient
			id="ch-full60002h"
			x1={1033.2306}
			x2={1106.2306}
			y1={987.8644}
			y2={947.4004}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path fill="url(#ch-full60002h)" d="m1102 929-37 62-36-54 73-8z" />
		<path fill="#b2b2b2" d="m875 834 15 13 7 10-24 20s15 1 30-2c13.5 10.1 157 114 157 114l-30-51-161-121 6 17z" />
		<path fill="#191919" d="m897 857-18 15-19-19 16-17 21 21z" opacity={0.302} />
		<path
			fill="#ccc"
			d="m947 798-20 1-2-22s16.2 6.4 22 21zm-61-21 11 23s-24.4 3.5-36 5.1c-1.3.2 6.1 16.4 12 28.9-6.2 6.5-15.6 14.9-16 15-.6.1-2-43-2-43s5.9-23.5 31-29z"
		/>
		<linearGradient
			id="ch-full60002i"
			x1={1033.0116}
			x2={1382.2677}
			y1={979.3876}
			y2={668.9766}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a1a1a" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-full60002i)"
			d="M1193 1088c38.5 34.8 144.4 118 223.9 151.5 22.7 28.7 114.5 128.4 135.6 158.9-107.1-40.7-271-215.6-376.5-288.4 7.4-11.1 10.6-14.1 17-22z"
		/>
		<path
			fill="#d9d9d9"
			d="M651 685c-4.9 11.8-6.4 16.9-8 23 21.7 11.2 74.4 19 101 15 7.3-9.6 12.3-15.3 16-19-28.7-.1-94.2 1.8-109-19z"
		/>
		<path
			fill="#b2b2b2"
			d="M626 1589c13.6 0 30.4-39.9 57-116s33.3-105.6 17-99-16.6 42.2-43 112-53.3 103.8-31 103z"
		/>
		<path
			d="M599.7 1578.1c-37.4-30-82.6-115.6-126.8-208.2-42.6-89.4-84.9-185.4-108.4-243.9-4.4-51.8 106-169.5 126.5-191 73.3 98 145.2 361.2 191.7 427.4-25.5 79.7-36.7 112-83 215.7zM1483 1169c13.1 17.3 24.2 35.4 24.1 54 24.4 30.8 87.5 99.9 125.3 140.3-4.5-75.6-120.1-166.6-149.4-194.3z"
			opacity={0.07}
		/>
		<path
			d="M786 904c0-20.6-5.5-145.1 26-188 20-1.2 30-3 30-3s-14-36-53-24c28.5-7.8 38.8-9.5 58-8 17.9 8.3 26 22 30 30 11.6-1.4 25-2 25-2s57.6 23.5 75 110c-15.2-10.1-28-23-28-23s.9-43.5-39-57-82.7-11.2-97 34 13 92.3 34 99 46 10.7 61 4c6.7 4.5 11 10 11 10l-48 4-8 18s-50.9-.3-77-4zm-282 214s-40.2-63-61-72-17.8 16.4-16 29 16.9 75.9 30 96c-2.6-23-3.6-37.2 7-47 16.7-16.2 40-6 40-6zm178 249c-5.8 19-55 179.3-83 211 13 6.3 29.9 14.4 37 5s63.9-137.8 74-214c-8.4 4.2-13.9 10-28-2zm736-127c16.2 5.8 81.3 31.7 90-13 34.3 35.2 127 140 127 140s4.7 57.9-82 30c-50.8-57.9-135-157-135-157z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full60002j"
			x1={843.1921}
			x2={843.1921}
			y1={1014.9807}
			y2={1240.1909}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60002j)"
			d="M729 749c-8.8 18.6-27.2 66.3-23 106 22.3 31.8 39.5 50.7 159 50 3.1-9.8 8-14 8-14l46-5-14-11s-31.5 7.3-42 3-40.2-10.9-49-51c-5.6-33.4-2-49-2-49s4.1-41.8 55-49c38.1-.2 53 17 53 17s27.3 13.5 28 50c11.2 10.7 33 27 33 27s-7.1-65.7-58-112-115.8-30.2-140-20-46 38.8-54 58z"
			opacity={0.2}
		/>
		<path
			d="M1418 1240c16.2 5.8 81.3 31.7 90-13 34.3 35.2 127 140 127 140s4.7 57.9-82 30c-50.8-57.9-135-157-135-157zm-736 124c5.7 7 16.4 16.3 31 3-4.6 23.2-54.8 183.3-69 206s-33.2 17-44 4c14.5-29.8 50.8-111 82-213z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full60002k"
			x1={416.8583}
			x2={495.1244}
			y1={801.0784}
			y2={832.3834}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60002k)"
			d="M504 1118s-40.2-63-61-72-17.8 16.4-16 29 16.9 75.9 30 96c-2.6-23-3.6-37.2 7-47 16.7-16.2 40-6 40-6z"
			opacity={0.702}
		/>
		<path d="M506 1121s-25.7-15.8-41 4 1 58 1 58l40-62z" opacity={0.302} />
		<path
			fill="#e6e6e6"
			d="M1189 941c53.3 72 368.2 224.7 311 306-67.7 42.1-288.1-142.4-306-157 18.5-30.5 43-65.8-7-137-4.3-6.2-.8-8.7 2-12z"
		/>
		<linearGradient
			id="ch-full60002l"
			x1={839}
			x2={839}
			y1={641.9999}
			y2={755}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#737373" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full60002l)"
			d="M804 1165c20.3 4.8 59.6 13.6 70 16-3.3 15.9-12.9 97.1-20 97s-35.5-12.8-48-48c-.3-37.8-.5-51.2-2-65z"
		/>
		<path
			fill="#f1f1f1"
			d="m546 756-69 4-58-69 52-3s27 24.5 48.7 44.1C539.2 736 600 749 600 749s22.9 11.1 24 21-5.4 15.7-19 12-50-15-50-15l-9-11zm413.8-46c30.7 16.3 58.2 31 58.2 31l48-6-137-76-52 7s27.4 14.6 58 30.9c-8.3 4.6-13 11.1-13 11.1l31 31 8-3s-.7-12.4-1.2-26z"
		/>
		<path
			fill="#e6e6e6"
			d="M545 755c.3.8 13 20 13 20l-61 1-20-18s67.7-3.8 68-3zm477-14s49.5-4.2 50-4 19 19 19 19l-43 4-26-19z"
		/>
		<path
			d="m607 785-54-16 6 13 1 29-66-1-23-21s-54.5-61.3-54-60c.5 1.3 2-37 2-37s53.2-5.4 54-5 49 47 49 47l78 12s26.8 22.7 24 32-17 7-17 7zm441 2 50-4-3-25-25-22-142-77-55 6 1 20s34.1 8.1 48 24 31 38 31 38l6-5 57 33 32 12z"
			className="factionColorSecondary"
		/>
		<path
			d="m417 693 2 33 53 63 26 20h62v-32l-62 2-25-21-56-65zm455-28 60 33-11 11s-38.9-23.9-47-24c-1.4-6.5-2-20-2-20zm85 47 61 31 33 19s43.8-5.2 43-5 6.1 24.8 6 26-51 5-51 5l-35-14-54-34s3-17.4-3-28z"
			opacity={0.102}
		/>
		<linearGradient
			id="ch-full60002m"
			x1={568.437}
			x2={581.4019}
			y1={1135.3267}
			y2={1187.3267}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full60002m)" d="m553 767 57 18s21.1-7.2 14-15-23-20-23-20l-74-17 26 34z" opacity={0.6} />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M867 681s-2.2-4.1-4-7c-6.6-11.4-29.9-17.4-52-19-15.2-11-36.3-23-118-23-101.1 0-119 43-119 43s-17.6 23.8-8 56c1 3.3 3 9 3 9m353-32c-.1.7 33.8 24.4 51 78 42.8 31.7 121.7 83.2 185.2 127.6 23.1 16.2 44.2 43.4 60.8 56.4 62 48.7 466.7 306.5 410 423-85.4 61.7-269.5-143.5-452-283-18 29.7-78 123-78 123s-184.9-43.9-225-53c-6.8 29.7-17 97-17 97s-20.4 10.7-50-47c-2.7-34.7-3-65-3-65s-112.9-27.6-137 10c4.4 37.6 47.8 174 45 196-2.8 22-60.2 206.8-78 216s-43.7-14.1-72-52-183.6-359.1-195-401c-11.4-41.9 37.7-97.6 109-185 57-69.9 80.4-82.7 102-141"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M661 1118c33.3-4.7 79.9-11.8 334 44 60.7 12.4 89 18 89 18s112.4-137.6 124-192"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M550 861c47.1-58.5 142.4-124 185-124" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M672 1169c30.5-16.4-68.4-137.7-166-255" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M571 988c37.4-33.1 122.2-91.2 137-98m306.5-50.1c19.8 4.1 43.1 10.2 61.1 18.7"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M575 816c4.3-12.4 7.9-25 5.7-40" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M811 655c-29.4-2.1-159-7.6-159 26 0 26.3 113 23 113 23"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m653 676-10 31s16.6 16.3 100 18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1098 1232c-9.7-37.9-25.7-101.3-201-328" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M947 796c12.7 10.6 157 132 157 132l-40 65-159-118s-92.9 28.3-94-76c-.7-69.3 52.2-70 59-70 23.6 0 78 14.9 77 67z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M786 904c0-20.3-9.5-141 27-188 31-4.9 85-7 85-7s58.7 19.6 80 107"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M842 712c-5.6-9.5-14.6-28-49-24" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M879 708c-5.6-9.5-19.6-32-54-28" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1103 928-75 9 36 54" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1030 940 860 807s-.6-.3.1.8c4.1 6.7 17.9 29.2 17.9 29.2l14 11 7 11 3 15"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m860 806 32-5 14 10 20-2v-10l21-2" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M888 848s-24.1 5.4-28 6c-1.8 2.6 6.6 9.4 6.9 9.9.3.5 11.1 8.1 11.1 8.1l17-14"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m853 854 23-19" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M844 870c-37.6-31.6-10.7-153.2 90-112" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M945 789c-24.6-31.5-111.7-22.2-89 62" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m924 776 2 32" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m884 775 22 35" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M881 776v25" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M972 784c5.3 14.8 7.4 25.4 10 41" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M924 710c-18.4-15.7-50-30-89-30-107.5 0-129 116.8-129 169s132.5 71 226 48"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m861 908 9-17s28.4-.8 50-5" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M708 832s-20 5.2-20 23c0 50 128.6 81.2 220.9 62.7 4.1-.8 35.2-5.5 43.1-9.7m62.1-57.3c3.9-12.4-3.3-24.1-31.1-31.7"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M802 1165c17.6 3.7 52.4 11.8 70.7 14.5 1.2.2 3.3 1.3 4.3 1.5"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1176 1112c16.8-30.7 97-78.1-23-202" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1192 1089c47.9 41.9 266.9 211.1 310 153 21.9-29.6-28.7-79.5-36-87"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1508 1226.4c19.5 21.7 127 141.6 127 141.6" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1414 1238c20.3 22.7 123.4 137.7 143.6 160.2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M710.9 1359.1C675 1455.8 581.2 1049.2 490 935" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M507 1120.9c-26.7 38.8-33.1 48.6-41.8 63.1-26.4-38.6-46.7-119.2-34.6-135.7 9.2-18.7 46.2 27 76.4 72.6z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M504.6 1118.5c-13.7-5.4-29.1-10.7-43 8.3-17.2 23.5 3.6 53.6 3.6 53.6"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M440.1 1131.6c2.7-15.9 23.5-40.6 50.1-34.5" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m458.9 1136.4 20 19.7m12.2-17.5-20-19.7" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M681 1363c-11.2 35.4-55.2 181.7-81 211" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M667 1176c-19.4-33.8-64.1-113.6-98-93s-114.8 165.3-130 216"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1216 1106c12-42.3 15.7-109.4 37-112" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M553 766c14.2 5.4 36.1 13.3 56 18 39.9-4.4-8-35-8-35s-55.5-12-82.6-17.9C510 724.1 470 686 470 686l-53 3v39l58 66 20 16 65-1v-29l-13-23-27-25m-102-41 59 70 22 21 58-2"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M872 685v-22l57-4 148 81 20 17v26l-49 4-27-9s-44.7-26-64.6-37.6"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1071 736-55 4v37m31 9v-26l49-3m-47 5-30-20-146-77" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M958 742c4.2-6 11.4-24.5-10-35-21.5-10.5-27 3-27 3" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M497 777v33" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M473 791v-33l71-1" />
	</svg>
);

export default Component;

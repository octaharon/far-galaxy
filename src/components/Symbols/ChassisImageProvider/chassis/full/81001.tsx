import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M286.7 1101.2c1 6.9 14.1 68.9 25.1 84.3 6.1 8.4 15 5.5 18 4.5 4.3-1.5 18.8-4.4 18.8-35.8 0-20.7-4-50.4-4.5-52 5.3-2.2 22-15.3 26-43 16-1.9 436.1-42.1 436.1-42.1s60.4-2.6 122.9 72.6c45.7 55 110.4 105.1 129.2 119.2 8.9 19.7 36 58.3 98.7 58.3 43.8 0 61.9-20.6 61.9-20.6s58.2 4.3 75.4-52c8.7-28.3 24.6-108.2-34.1-195.4 2.4-5.2 3.6-13.8 1.8-19.7 6.8-4.9 30.5-18.8 30.5-18.8l218-32.3s17 11.3 43.1 18.8c-6 12.8-13.4 29.3-17 30.5-5.4-1.2-57.3-26.3-75.4-24.2-18.1 2.1-21 12.7-.9 27.8 20.1 15.1 73 46.4 87.9 43.9 14.9-2.4 39.7-18.6 53.8-73.5 13.8.3 85.6-47.2-39.5-109.4-18.2-9-88.2-34.6-102.3 18.8-53.4-5-328.4-32.3-328.4-32.3s-144.5-41.1-171.4-41.2c17.9-8.2 56.3-18.6 58.3-26s11.3-68.6 14.4-160.4c.2-6.2-12.5-21.9-16.1 11.7-3.7 33.5-16.8 130.3-24.2 131.8-13.3 2.6-131 8.1-131 8.1l-7.2 15.2-36.8-10.8s-120.7 12.3-140 11.6c-11.1-56.4-26.4-151.3-38.6-155.1-12.1-3.7.9 53.5 20.6 159.5 4.6 24.6 47.9 25.7 125.6 36.8 0 27.6 23.9 30.6 34.1 49.3-28.6 6.6-466.6 124.6-466.6 124.6s-23.8-34.1-62.8-34.1-53.9 61.9-44.9 93.2c9.3 31.3 27.4 46.4 41.5 58.2z"
		/>
		<radialGradient
			id="ch-full81001a"
			cx={803.885}
			cy={746.823}
			r={296.006}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.429} stopColor="#000" />
			<stop offset={0.702} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full81001a)"
			d="M813 1017c28 3.1 64.2 15.2 109 62 7.3-7.1 29-27 24-70-23.3-15.3-85.5-32.3-115-28-2.4 11.7-3.7 24.3-18 36z"
			opacity={0.6}
		/>
		<path
			d="M812 918c11.7 10.1 38.5 61.2 0 99-39.5 3.8-389 37-389 37s13-31.6-4-61c22.4-4.9 382.8-72.6 393-75zm481 41c3.4-9.4 16.4-36.6-9-76 26.4.1 217 5 217 5s24.8 17.4 1 43c-33.5 4.4-200.6 28.5-209 28zm-66 47c-3.4-8.2-13.1-38.8 30-34-12.4-14-81.5-70.9-111-73 .2 7.7 23.2 43.8 46 72 17.5 21.6 36.5 38.6 35 35zm-889 13c-2.5-8.5-23.3-60.7-36-69-18.8-1.1-36.3 8.2-40 16 3.5 10.8 30 65 30 65s32.3-8.3 46-12zm1261-127c-10.7-14.1-56.1-55.6-96-64 22.6-1.2 89.7 18.1 128 64-12.8 0-18.8.5-32 0z"
			className="factionColorSecondary"
		/>
		<path
			d="M352 983c-.5-1.1 405-108 405-108s38.6 19.3 56 42c-24.3 4.8-450 88-450 88s-8.8-17.1-11-22zm881-145c28 2.4 209.5 18.5 233 21 31.7 6.2 49.9 12.8 71 26-21.9 1.7-252-2-252-2s-21.3-23.8-52-45z"
			className="factionColorPrimary"
		/>
		<path
			fill="#999"
			d="M286.7 1100.3c11.8 6.8 36.1 13.9 57.4 1.8 5.6 17 11.9 81.9-13.5 87.8-25.3 6-28.4-27-43.9-89.6zm1266.9-153.2c16.5 4.8 39 5.2 48.4 4.5-2.1 13.6-25.1 84.8-68.2 70.8-49.1-20.5-94.3-52-85.2-63.6 9.1-11.7 42.3-5 87.9 18.8 6.4-7.7 12.7-18.4 17.1-30.5z"
		/>
		<path fill="gray" d="M1059.3 1208.8c18.8 13.3 71.2 42 157.9 37.6-6.1 14.6-109.5 52.3-157.9-37.6z" />
		<linearGradient
			id="ch-full81001b"
			x1={322.9888}
			x2={322.9888}
			y1={730.0031}
			y2={817.86}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full81001b)"
			d="M297.5 1104.8c13 4.5 35.7 6 45.8-2.7 3.9 30 15.4 86-17 87.8-29.3 1.7-26.8-73-28.8-85.1z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full81001c"
			x1={1506.8854}
			x2={1527.2563}
			y1={897.4418}
			y2={973.8408}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full81001c)"
			d="M1553.6 948c-5.7 11.3-12.8 29.9-17.9 29.6-5.1-.3-72.3-37.9-87.9-16.1-13.2 18.4 90.9 61.7 96.9 62.7 6 1.1 33.7.8 56.5-70.8-18.3-1.1-34.1-1.6-47.6-5.4z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full81001d"
			x1={1118.6321}
			x2={932.757}
			y1={878.796}
			y2={553.4489}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full81001d)"
			d="M920.2 1077.9c95.2 95.1 134.6 167.6 293.4 167.6 84.8 0 90.6-76.6 90.6-92.3 0-15.7-224.3-150.6-224.3-150.6L922 920.2s59.6 130.8-1.8 157.7z"
		/>
		<linearGradient
			id="ch-full81001e"
			x1={608.2016}
			x2={597.8976}
			y1={844.6074}
			y2={926.1683}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full81001e)" d="m810.7 1017-440.5 43.9-.9-24.2 463-57.4s-1.1 25.2-21.6 37.7z" />
		<path
			fill="#bdbdbf"
			d="M249.9 1052.8c23.9 41.9 103.6 46.2 121.1-9.8 2.7 34.4-19 65.4-50.3 65.4-31.1 0-51.7-13.6-70.8-55.6z"
		/>
		<linearGradient
			id="ch-full81001f"
			x1={1536.7723}
			x2={1570.5194}
			y1={950.2305}
			y2={990.7215}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.996} stopColor="#bdbdbf" />
		</linearGradient>
		<path
			fill="url(#ch-full81001f)"
			d="M1555.4 911.2c15.7 7 56.6 26.8 75.4 17.9-12.2 16.8-27.2 41.4-120.2-.9 29.1-5.6 38.5-6.1 44.8-17z"
		/>
		<linearGradient
			id="ch-full81001g"
			x1={1425.7841}
			x2={1419.3301}
			y1={956.2977}
			y2={1007.3856}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full81001g)"
			d="M1298.8 934.5s227-23.5 253-24.2c.8 8.3 19.7 15.9-259.3 51.1 5.3-12 6.3-26.9 6.3-26.9z"
		/>
		<linearGradient
			id="ch-full81001h"
			x1={767.9059}
			x2={870.0809}
			y1={992.7891}
			y2={1094.964}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full81001h)"
			d="M786.5 812.6c-2.1 19.1 29.5 36.6 31.4 45.7 12.2-1 56.2-6.6 81.6 29.6 43.3-13.7-30.5-102.2-30.5-102.2l-55.6 27.8s-21.2-3.7-26.9-.9z"
		/>
		<linearGradient
			id="ch-full81001i"
			x1={791.8405}
			x2={809.9495}
			y1={1124.9451}
			y2={1090.887}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full81001i)"
			d="M820.6 814.4c-10.9 6.7-13.9 23.4-10.8 29.6-14.2-5.6-26.2-24.7-25.1-34 17.8 4.3 21.8 2.3 35.9 4.4z"
		/>
		<path
			fill="#cacacc"
			d="M812.5 815.3s51.9-25.7 64.6-26.9c12.7-1.2 89.7.9 89.7.9l52.9-28.7 14.3-159.5-13.5-11.7-27.8 154.2 18 13.5-119.3 28.7-209-3.6s-28.8-164.8-44-165.8 18.6 135.9 23.3 163.1c3.7 20.2 150.8 35.8 150.8 35.8z"
		/>
		<radialGradient
			id="ch-full81001j"
			cx={845.094}
			cy={634.424}
			r={781.689}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.169} stopColor="#4c4c4c" />
			<stop offset={0.356} stopColor="#666" />
			<stop offset={0.496} stopColor="#999" />
			<stop offset={0.506} stopColor="#e6e6e6" />
			<stop offset={0.523} stopColor="#999" />
			<stop offset={0.626} stopColor="#666" />
			<stop offset={0.767} stopColor="#4c4c4c" />
		</radialGradient>
		<path
			fill="url(#ch-full81001j)"
			d="M1260 1001c8.8 10.1 66.1 88.5 30 205-51.2 38.6-166.9 11.5-234-48-63.7-56.5-12.1-122.8 37-165s95-27 95-27 39.5 65 72 35z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M286.7 1101.2c1 6.9 14.1 68.9 25.1 84.3 6.1 8.4 15 5.5 18 4.5 4.3-1.5 18.8-4.4 18.8-35.8 0-20.7-4-50.4-4.5-52 5.3-2.2 22-15.3 26-43 16-1.9 436.1-42.1 436.1-42.1s60.4-2.6 122.9 72.6c45.7 55 110.4 105.1 129.2 119.2 8.9 19.7 36 58.3 98.7 58.3 43.8 0 61.9-20.6 61.9-20.6s60.4.9 75.4-52c8.1-28.5 24.6-108.2-34.1-195.4 2.4-5.2 3.6-13.8 1.8-19.7 6.8-4.9 30.5-18.8 30.5-18.8l218-32.3s17 11.3 43.1 18.8c-6 12.8-13.4 29.3-17 30.5-5.4-1.2-57.3-26.3-75.4-24.2-18.1 2.1-21 12.7-.9 27.8 20.1 15.1 73 46.4 87.9 43.9 14.9-2.4 34.9-16.4 53.8-73.5 13.8.3 85.6-47.2-39.5-109.4-18.2-9-88.2-34.6-102.3 18.8-53.4-5-328.4-32.3-328.4-32.3s-144.5-41.1-171.4-41.2c17.9-8.2 56.3-18.6 58.3-26s11.3-68.6 14.4-160.4c.1-4.4-12.3-17.7-13.5-9-.7 5.1-1.6 11-2.7 20.6-3.7 33.5-12.8 75.9-24.2 130-13.3 2.6-131 9.9-131 9.9l-7.2 15.2-36.8-10.8s-120.7 12.3-140 11.6c-11.1-56.4-23.7-148.6-35.9-152.4-2.5-.8-6.7-.8-9 .9.8 18.8 11.2 71.6 26.9 156 4.6 24.6 47.9 25.7 125.6 36.8 0 27.6 23.9 30.6 34.1 49.3-28.6 6.6-466.6 124.6-466.6 124.6S329 949.8 290 949.8s-53.9 61.9-44.9 93.2c9.4 31.3 27.5 46.4 41.6 58.2z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M890.6 787.5c13.5 0 72.5-1 85.2 1.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M763.2 807.3c25.2 3.4 50.3 7.2 50.3 7.2s34.6-26.9 76.3-26.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m992 743.6 21.5 12.5s-.1.7-.5.8c-9.2 2.3-122.4 30.5-122.4 30.5L683.3 782s-4.7-5.1-6.3-17.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1262.9 978.5c-20-18.5-54.8 3.2-30.4 28.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1244.5 985.6c3.2 0 5.8 2.6 5.8 5.8 0 3.2-2.6 5.8-5.8 5.8-3.2 0-5.8-2.6-5.8-5.8 0-3.2 2.6-5.8 5.8-5.8z"
			opacity={0.702}
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M920.2 1078.8c63.8-23.8 8.3-248.4-104.1-218.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M897.8 889.7c12.1-6.9 26.3-13.9 42-20.5m21.7-8.4c40.4-14.4 87.6-24.8 132.8-23.7m32.3 3c12.3 2 24.4 5 35.8 9.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M909 847c6.1 3.9 18.7 12.6 33 23 13.3 9.7 26.2 22 36 31 13.5-5.2 22-8 22-8s-18.2-17.4-37-31c-15.3-11.1-32-19-32-19s-17.8 2.4-22 4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1067 819c6.1 3.9 20.7 13.6 35 24 13.3 9.7 23.2 16 33 25 8.9-.8 26 2 26 2s-23.2-21.4-42-35c-15.3-11.1-26-15-26-15s-23.6-1.3-26-1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M807.1 1016.1c34.5-3.3 51.5-103-52-139.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m369.3 1034.9 463-54.7S886 974.7 948 1010" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M369.3 1043.9s-6.9 29.5-44 38.5c-29.7 7.3-65.8-1.9-82.6-54.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M259 963c33.5 60.2 42.7 96.8 43 121m43-11c-.9-27.5-9.1-87.9-42-121"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M291 1030c9.8-3.9 36.8-10.3 47-10" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1598 891c6.7-.7 26.2 1.4 33 4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1606 928c2.9-16.7-2.8-67.3-106-100" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m365 1005 451-87m468-36 258 5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M785 869c15 5.6 47.8 28.2 60 51s26.2 23 34 21 20.6-8.8 3-33c-17.1-23.5-36.2-45.6-68-48"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M836 858c5.8-44.4 87.2-81.8 197-56" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1153 835c15 5.6 48.8 29.2 61 52s26.2 23 34 21 20.6-8.8 3-33c-10.3-14.1-21.3-27.8-35.4-36.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M422 1056c5.1-13.3 11.4-38.4-2.1-59.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1501 931c10.8-8.2 20.1-32.4-1-45" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1300.6 933.6 251.2-25.1s74.3 39.7 83.4 9.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1210 836.8c95.5 17.1 107.8 115.9 68.2 131.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1125.7 827c49.3 20.9 94.4 82.9 106.8 121.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1057.5 1207.9c49.1 33.3 112.3 42.5 163.3 38.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1262.9 980.3c-1.5-15.5-106.6-88.1-118.4-82.5-11.9 5.7 43 66.6 42.2 66.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1158 928c-30.7-3.1-134.9 19.7-169.4 122.3-6.6 19.6-13.8 74.1 4.4 105.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1292.5 1198c-1.9 5.7-20.7 23.3-44.5 25.5-23.5 2.2-131.5 7.1-209.4-85.6-45.2-53.7 39.9-180.7 148-173.9 4.8 7.9 25.1 35.2 46.7 43.9 15.3 6.2 24.7-4.1 26.9-9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1055.7 1155.9c16-65.9 76.6-160.7 218.9-131.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1110.4 1197.1c6.5-71.1 110.6-146.4 187.5-112" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1178.6 1219.6c3.3-35.1 78.2-105.5 123.8-72.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M352.2 982c7.8 10.6 19.5 55.3 17.9 77.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M284.9 1099.4c9.8 7.3 37.8 15.7 61 .9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m297.5 1105.7 7.2 65.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1553.6 947.1c9.5 2.6 36.4 7.2 51.2 4.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1585.9 952.5c-4.7 9.1-30.5 76.1-54.7 68.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1461.2 860.2c33.8 3 92.4 20.6 92.4 52.9 0 12.9-48.4 16.1-48.4 16.1"
		/>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#cfcfcf"
			d="M1136 820s-5.2 16.2 1 43 3 28.7-6 58 14.2 37.3 23 32c-12.6 30.6-1.3 35.8 10 37 15.2-23 47.5-54.2 40-90s-2.8-69.5-5-87c12.7-9.5 8.9-9.2 11-26 6.8-13.5 14.6-10.2 15-20-4.1-31.1-8.4-67.1-12-80-3.2-11.4-22-15-22-15s-3.2-3.1-15-9c5.8-77.5 1.9-105.2-4-118 24.8-6.4 41-40 41-67s-14-54-14-54 9.3-3 6-25-23-33-23-33l-19 1-3-6s8.6-14.8 16-29c-11.1-20-57-35-57-35l-14 25s-49.4-22-66-22-30 7-30 7-2.6-5.5-3-17c-1.8-20.4-11-28-11-28s-.5-4-11-33-41.5-43.1-57-48c-3.8-8.2-9-16-9-16s-25.9-6.8-37-3-8.7 2.6-16 14c-28.9 4.8-59.1 25.9-65 49s-10.3 51.9-27 71-9 42-9 42-16.2 8.3-18 8c-7.8-12-12.1-31-25-26s-69 34-69 34-9.7-12.8-17-23c-51.8 2.3-60 40-60 40s12.3 18.3 20 30c-6.2 8.9-52 43.8-52 96s15 51.6 35 72c-16.2 70.5-1 125-1 136 0 23 6 30 6 30v19h-9v18h9v18h-11v20h9v18h-9v21h9l-9 370h82l23-370h13v-20h-11v-19h13l1-19-12-1 1-16 13-1v-18h-11l1-17s.6-.1-6-1c-.4-4.1 3.2-4.1 13-19 16.5-35.2 15-138 15-138l2-1s-.7 11.6 21 65 11.8 88.3 9 100-5.4 29.1-10 57c-13.1 6.2-14 12-14 12l-1 30s1.8 7.5 5 22c-1.3 9.8-6 45.1-5 87-15.3 100.8-18.3 186.2-18.9 209.6-.1 4.2-9.1-3.6-9.1-3.6s-9.4 19.7-12 40c-.6 5 8.7 5.9 7.6 11.4-2.9 14.2-5.2 32.1-5.6 53.6-1.2 63.4 6.9 83 14 111s4.3 54.3 3 74-15.6 30.2-13 58 29 94 29 94-.5 13.2 21 12 111-6 111-6 9.9-.3 3-19-51-132-51-132-6-16.6-10-31 4.5-69.8 17-124 12-67.6 14-93c45-82 76.7-220.7 80-232 1-3.3 2.2-5.5 3.7-6.8 2.6 4.7 8.7 7.2 13 6.3 7.3 60.9 28.6 123.1 44.3 153.5-.3 2-8.8-8.1-11-1-3.1 9.9-.3 38.4-1 49-.5 7.1 6.8 2.9 7 11.4.4 15.3 2.3 32.5 7 50.6 23.3 85.1 24 146 24 146s-16 14.1-20 26c-3.5 10.5 0 36.1 0 46 0 21.1 7 18 7 18s43.5 49.4 106 105c67-17.7 90-22 90-22s-7.3-17.7-18-56c-17.6-18.9-73.4-106.9-68-109 1.2-.5 4.9-88.1 7.6-151.9 0-1-4.6 0-4.6-1.1.3-6.3-1.6-20.7 0-27 1.1-1.2 3.9-1.8 5.8-2.9.1-1.4.1-2.8.2-4.1 30.7-41.2 18.4-75.6 6-98 18.3-92.3-5-248-5-248l8-10s5.1-23 7-31-1.8-9.3-12-17c-6.3-31.5-25.3-89.3-27-110s2-70 2-70l6-8 15 83v10l39 105 10 9z"
		/>
		<path
			fill="#768692"
			d="M741 1142c-10.5 3.9-21 12-21 12l-12 70s13.7 33.5 21 37c9.7 4.5 58 6.1 65 1 7.4-11.9 26.7-62.4 21-117-21.5-8.7-62.6-13.7-74-3zm286-37-23 25 6 78 25 12s37.1.4 50-14 47.1-77.7-13-112c-23.5 4.2-45 11-45 11zm161-429s-42.2 25.3-51 28c.4 10.1 17 91 17 91l24 3 48-29-16-85-22-8z"
		/>
		<path
			fill="#34a798"
			d="M855 179c-15.2 1.9-40.8 15.6-50 34s-15.4 54.6-22 65-26.5 35.9-19 59 32.5 111.5 84 153c3.2-37 8-79 8-79s-45.8-47.8-55-80c9.3-27.9 26-68 26-68s42.8 68 94 77c-4.2-34.4-15.6-109.3-21-118-13.4-2.6-26-8-26-8s-6.3-13.3-19-35zm62 250c0 1.4-9 74.2-9 75s-26-2-26-2l12-75s23 .6 23 2zm27-239 7 22-9 12s20.1 85.3 21 119c10.5-7 40.7-40.2 34-76-10.7-35.3-16-49.3-23-55s-30-22-30-22zm15 234c-.9 1.5-6.9 55.9-11 74 16.1-1.7 42.7-14 51-26-3.5-10-39.1-49.5-40-48zm29-26c7-12.1 20.9-46.1 23-55 3 16.2.3 72.8-1 85-12.8-15.4-19.9-17.3-22-30z"
		/>
		<linearGradient
			id="ch-full10006a"
			x1={887.342}
			x2={887.342}
			y1={1415.8619}
			y2={1741}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006a)"
			d="M855 179c-15.2 1.9-40.8 15.6-50 34s-15.4 54.6-22 65-26.5 35.9-19 59 32.5 111.5 84 153c3.2-37 8-79 8-79s-45.8-47.8-55-80c9.3-27.9 26-68 26-68s42.8 68 94 77c-4.2-34.4-15.6-109.3-21-118-13.4-2.6-26-8-26-8s-6.3-13.3-19-35zm62 250c0 1.4-9 74.2-9 75s-26-2-26-2l12-75s23 .6 23 2zm27-239 7 22-9 12s20.1 85.3 21 119c10.5-7 40.7-40.2 34-76-10.7-35.3-16-49.3-23-55s-30-22-30-22zm15 234c-.9 1.5-6.9 55.9-11 74 16.1-1.7 42.7-14 51-26-3.5-10-39.1-49.5-40-48zm29-26c7-12.1 20.9-46.1 23-55 3 16.2.3 72.8-1 85-12.8-15.4-19.9-17.3-22-30z"
			opacity={0.302}
		/>
		<path
			fill="#f2f2f2"
			d="m868 173-4 4s-8.7 1.6-8 2 17 36 17 36l24 8s-15.3-49.3-29-50zm74 48 9-9-7-24-13-4 11 37z"
		/>
		<linearGradient
			id="ch-full10006b"
			x1={1072.799}
			x2={1047.799}
			y1={1235.5}
			y2={1235.5}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full10006b)" d="m1063 634 1 62s-18.1 32.7-25 39c2.3-31 4-97 4-97l20-4z" opacity={0.702} />
		<linearGradient
			id="ch-full10006c"
			x1={1130.1504}
			x2={1048.1504}
			y1={1179.2646}
			y2={1148.5085}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full10006c)" d="m1064 701 27 103s-19.8 29.6-82 45c19.7-35.6 31-118 31-118l24-30z" />
		<linearGradient
			id="ch-full10006d"
			x1={958.3571}
			x2={1034.3572}
			y1={631.3602}
			y2={640.6922}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006d)"
			d="m1011 1210-3-10-58-11s7.2 53.7 15 79 16.3 99.4 16 108c10 5 38.9 5.4 45 5 .1-15.5-4.6-124.5-7-130s-8-41-8-41z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full10006e"
			cx={742.304}
			cy={658.861}
			r={295}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.602} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10006e)"
			d="M695 1405c16.6 4.5 22 5 22 5l1 4s52.8 5 65-9c-1.1 13.8 9.7 49.7 14 59-30.9-.6-106-1-106-1s4.9-26.6 4-58z"
		/>
		<radialGradient
			id="ch-full10006f"
			cx={1050.363}
			cy={732.756}
			r={373.474}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.602} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10006f)"
			d="M982 1376c11.8 5 29.6 6.7 43 5 .1 3.6 2 7 2 7s38.9 1.9 51-4c3.6 9 17 37 17 37l-78 10s-61 31.3-60 15 13.7-55.4 25-70z"
		/>
		<radialGradient
			id="ch-full10006g"
			cx={1140.9709}
			cy={459.717}
			r={320}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.6} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full10006g)"
			d="M982 1377c-7.2 7.7-32.6 49.9-20 91 24.5 26.7 37 37 37 37l15-71 81-15-17-35-52 5-6-6s-25.5-2.8-38-6z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10006h"
			x1={730.6537}
			x2={626.2277}
			y1={1262.0636}
			y2={1270.0986}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006h)"
			d="M706 568c2.5 15 5.9 133.3-26 166-28.6 1.2-66 3-66 3s-24.5-109.7 0-154c25.9.8 52.7 7.8 92-15z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full10006i"
			x1={896.5028}
			x2={782.2778}
			y1={850.021}
			y2={874.301}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006i)"
			d="m870 916 27 53s-54.5 197.7-82 232c1.3-11.6 0-56 0-56s-23-6.3-32-7c-3-24.3 24.3-219.1 28-228 15.1 3 59 6 59 6z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full10006j"
			x1={1126.0535}
			x2={1126.0535}
			y1={1263.144}
			y2={1385.144}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full10006j)"
			d="M1109 541s-35.3 74.6-36 76 103 46 103 46 9.6-87.8-5-119c-28.1 10-62-3-62-3z"
			opacity={0.502}
		/>
		<path fill="#e6e6e6" d="m792 1462 6 61-51 2-17-61 62-2zm223-28 68 60 29-11-21-61-76 12z" />
		<path
			fill="#d9d9d9"
			d="m730 1465 16 61 53-4s-5.5-57.8-5-57 53 131 53 131 .1 9.9-8 11-118 4-118 4-7.5 1.9-13-12-22-68-22-68l44-66zm284-30-13 74 72 66 92-18s-27.2-77.3-59-119c.2 16.7 8.5 45.1 9 48-14.8 3.9-35 6-35 6l-66-57z"
		/>
		<linearGradient
			id="ch-full10006k"
			x1={657.9863}
			x2={657.9863}
			y1={1331.062}
			y2={1596.4908}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#768692" />
			<stop offset={0.897} stopColor="#34a798" />
		</linearGradient>
		<path
			fill="url(#ch-full10006k)"
			d="M729 320c6.7 8 50.3 68.9 43 221-26.9 13.8-92 43-135 43s-82.6-27.6-90-51-10-92.7 48-130c22.3-4 48.5-12.5 59-49 26.8-14.8 68.3-42 75-34zM625 425c-11.6 22.6 13 45 13 45l41 12s20-10.1 26-26-1-38-1-38l-41-14s-26.4-1.6-38 21z"
		/>
		<radialGradient
			id="ch-full10006l"
			cx={658.532}
			cy={1508.5281}
			r={278.698}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.298} stopColor="#768692" stopOpacity={0} />
			<stop offset={0.636} stopColor="#768692" />
		</radialGradient>
		<path
			fill="url(#ch-full10006l)"
			d="M729 320c6.7 8 50.3 68.9 43 221-26.9 13.8-92 43-135 43s-82.6-27.6-90-51-10-92.7 48-130c22.3-4 48.5-12.5 59-49 26.8-14.8 68.3-42 75-34zM625 425c-11.6 22.6 13 45 13 45l41 12s20-10.1 26-26-1-38-1-38l-41-14s-26.4-1.6-38 21z"
		/>
		<path
			fill="#34a798"
			d="M659 421c4.2 1.5 13.1 12.1 10 20s-10.6 14.7-19 11-13.8-14.2-9-20 13.8-12.5 18-11zm-75-61c5.1 2.5 24.2-3.4 29-22-8.3-.1-16 6-16 6s-16.5 14.3-13 16zm591 25c-2.9 3.4-4.2 15.7 0 19s9.5 10.6 13 7 1.9-10.3 0-16-10.1-13.4-13-10z"
		/>
		<linearGradient
			id="ch-full10006m"
			x1={1126.5179}
			x2={1126.5179}
			y1={1383.5591}
			y2={1630.1996}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.015} stopColor="#768692" />
			<stop offset={0.985} stopColor="#34a798" />
		</linearGradient>
		<path
			fill="url(#ch-full10006m)"
			d="M1041 301c11.1 3.7 61.5 22.2 59 236 15.1 9.3 31.5 12 52 10s53.3-6.1 60-61c.8-43.7-13-60-13-60l-41 19s-22.9-9.5-28-26-.1-38.6 7-43c12-2.9 23-8 23-8s-13-25.3-47-41-56.8-24.9-72-26z"
		/>
		<path
			fill="#f2f2f2"
			d="m1190 673-14-9s-25.9 27-49 29-38 1-38 1l-2 10s39 6 48 1c23.7-7.5 55-32 55-32zm-84-128-23-18s-47.8 27.7-57 32c6.9 9.2 19 18 19 18s53.2-23 61-32zm-338-1s89.6 20.2 120 19c-2.2 11.3-10 30-10 30s-109.1-15.2-140-33c23.8-11.1 30-16 30-16z"
		/>
		<path
			fill="#f2f2f2"
			d="M1210 683s-35.9 30.3-45 32c-11.4-2.2-26-9-26-9s46.8-28.6 50-33c12 5.5 21 10 21 10z"
			opacity={0.149}
		/>
		<path d="m1162 715 16 85-24-5-18-87 26 7z" opacity={0.2} />
		<linearGradient
			id="ch-full10006n"
			x1={1071.0485}
			x2={1195.3665}
			y1={1140.4186}
			y2={1183.9296}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.995} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-full10006n)"
			d="M1086 693c-1.5 10.5 3 14 3 14l36 105s57.7 31.6 84-10c.2-6.9 1-22 1-22l-32 21-25-6-17-90-4-13s-33.8 4.6-46 1z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full10006o"
			x1={1178.6455}
			x2={1119.6455}
			y1={1116.9114}
			y2={1110.8564}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006o)"
			d="M1151 796s7.7 26.8 9 27 45-4.2 50-18c-2.5-17.8-2-23-2-23l-30 18-27-4z"
			opacity={0.2}
		/>
		<radialGradient
			id="ch-full10006p"
			cx={1161.67}
			cy={1148.045}
			r={141.773}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.352} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10006p)"
			d="M1135 847c10.9 2.6 59.6 5.5 64 1s-1-33-1-33-44.1 12.2-61 4c-5.8 11.7-2.2 16.8-2 28z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10006q"
			x1={916.1322}
			x2={916.1322}
			y1={1416}
			y2={1758.3829}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full10006q)"
			d="M946 501c8.2-35.1 41.4-203-27-334-19.3-8-45.3-7.1-48 3 9.9 15.1 76.1 140.4 37 334 15.3-1.5 28.1-.6 38-3z"
		/>
		<linearGradient
			id="ch-full10006r"
			x1={843.9789}
			x2={893.9789}
			y1={1451.9388}
			y2={1445.7988}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full10006r)"
			d="m857 412-13 115s1.8 19.6 13 19 18.3-7.7 20-16 17-109 17-109-.7-23.5-14-25-23 16-23 16z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10006s"
			x1={857.2919}
			x2={863.5823}
			y1={1374.1892}
			y2={1418.9506}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006s)"
			d="M846 511c-2 13.3 2.3 34.8 12 35s18.8-17.7 20-33c1.2-15.3-30-15.3-32-2z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full10006t"
			x1={1013.2952}
			x2={1022.1862}
			y1={1370.9092}
			y2={1434.1722}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006t)"
			d="m994 533 18 16s30.9-22.3 28-42-27.9-28.6-31-14c-1.3 6.3-1 16.6-4 24-3.9 9.7-11 16-11 16z"
			opacity={0.302}
		/>
		<path
			d="M635 334c-2.7 6-24.1 39.1-57 41 11.5 16.1 18 29 18 29s50.7-7.6 56-48c-7.3-11.3-14.3-28-17-22zm53 81c3.3 7.3 11.8 46.8-22 60 11 6.4 18 5 18 5s16.3-7.1 19-24 2.7-28.4-1-35-17.3-13.3-14-6zm471-45c-5.5 7-7.6 47.7 30 61-15.5 7.4-31 15-31 15s-21.5-8.1-27-27-.3-37.6 5-42 28.5-14 23-7zm3-10 13-28s-24.8-28.8-57-34c-6.3 10.5-15 25.1-14 25s47 28.4 58 37z"
			opacity={0.102}
		/>
		<linearGradient
			id="ch-full10006u"
			x1={614.825}
			x2={614.825}
			y1={709.098}
			y2={826.4324}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#34a798" />
			<stop offset={1} stopColor="#768692" />
		</linearGradient>
		<path
			fill="url(#ch-full10006u)"
			d="M654 1153c-.9 32.4-3 65-3 65h-23s1.2-23.7 2.4-47.9c6.1-4.3 15-10.8 23.6-17.1zm2-36c.1-5.1.3-10.3 1-15.1-7.8 5.5-24 17.1-24 17.1s-.3 6-.8 15c9.1-6.3 19.5-13.7 23.8-17zm-.9 13c0 .3-.1.7-.1 1-.1 3.8.2 6.6 0 10-4.5 3.5-20.7 14.6-24 16.9.2-4.1.4-8.1.6-11.8 3.7-2.6 17.5-12.1 23.5-16.1zm-78.1 25c0 .6-3 .8-3 1.5-1 30.4-1.9 62.3-1 62.5 1.6.5 27 0 27 0l2-50c-6.8-4-20.7-11.8-25-14zm1-25c0 3.8-1 8.8-1 13l1 1s19.9 11.3 24.4 13.9c.2-4.8.4-9.6.6-13.9-7-4.1-21-11.8-25-14zm1-10v-16.2c7.2 4.4 25 15.2 25 15.2s-.2 5.9-.6 14.9c-5.5-3.4-21.3-12.3-24.4-13.9z"
		/>
		<path
			fill="none"
			stroke="#34a798"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={5}
			d="m623.7 1092.2 17.3-8.7m4.8-7.4.9-18.3m-3.4-8.7-16.8-10.9m-9.4-.4-17.8 9.3m-4.5 8.4-.8 17.8m4 7.9 16.2 10.6m5-2.7.9-17.7m-4.2-7.6-16.5-10.6m25.7 11.1 17.3-8.9m-15-36.7 17.3-8.7m4.8-7.5.9-18.2m-3.4-8.7-16.8-10.9m-9.4-.5-17.8 9.3m-4.5 8.5-.8 17.8m4 7.9 16.2 10.6m5-2.8.9-17.7m-4.2-7.5-16.5-10.6m25.7 11 17.3-8.9m-14.9-39.3 17.3-8.7m4.9-7.5.9-18.3m-3.4-8.6-16.8-10.9m-9.5-.5-17.8 9.3m-4.4 8.5-.8 17.8m3.9 7.9 16.2 10.6m5.1-2.8.9-17.7m-4.3-7.5-16.5-10.6m25.7 11 17.4-8.9"
		/>
		<path
			fill="#34a798"
			d="M760 401c8.4-2.6 13-5 13-5s-13.5-42.7-23-50-3 2-3 2 12.1 36.8 13 53zm254-34c3.2 7.2 7 17 7 17s3.5-.6 11-2c-5.4-11.6-11.4-25.5-17-33s-4.2 10.8-1 18z"
		/>
		<path
			d="M1093 579c4.3 27.6 17 98.3 30 113-17.6 4-31.1 1.3-36 0-6.1-22.8-16-78-16-78s14.9-19.4 22-35z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full10006v"
			x1={638.0977}
			x2={638.0977}
			y1={1213.347}
			y2={1350}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.699} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full10006v)"
			d="M577 571c6.5 3.6 62 28.2 129-1 0 31.1 1.7 59.2-1 80s-110.6 79.6-128 47c-9-46.1-10.5-89.4 0-126z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10006w"
			x1={823.6213}
			x2={660.6213}
			y1={1218.886}
			y2={1225.974}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#000" stopOpacity={0} />
			<stop offset={0.801} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-full10006w)"
			d="M708 568c1.4 39.3 33.5 93.1 34 132s-10.9 99.8-16 109c16 18.1 29.2 41.5 145 51-5.7-62.1-26-204-26-204l-74-114-61 26"
			opacity={0.8}
		/>
		<radialGradient
			id="ch-full10006x"
			cx={1066.593}
			cy={643.46}
			r={383.872}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.954} stopColor="#959595" stopOpacity={0} />
			<stop offset={1} stopColor="#959595" />
		</radialGradient>
		<path
			fill="url(#ch-full10006x)"
			d="M681 1211c-4.1 14-8.1 71.9-4 101s16 78.9 17 94c11.6 3.5 24 4 24 4s6.5-121.9 2-128c12.7-3.1 31-2 31-2l1-13s-16.8.2-24-7-19.5-29.9-19-40c-11.1-1.9-18.3-5.6-28-9z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-full10006y"
			x1={1012.9005}
			x2={895.9005}
			y1={881.4022}
			y2={847.9642}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#000" stopOpacity={0} />
			<stop offset={0.801} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-full10006y)"
			d="M911 975c1.6 18.6 21.6 117.5 41 149 19.7 9.7 50 14 50 14l2-8 24-26-18-24-24-104-14-22s-19.4 17.8-61 21z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10006z"
			x1={994.552}
			x2={928.3898}
			y1={760.9829}
			y2={760.9829}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full10006z)"
			d="M948 1119c-5.3 7.1-9.7 53-2 68 17.6 5.9 53.8 12.6 62 12 0-8.5-5.6-59.4-7-63-10.7-.5-39.3-6.4-53-17z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full10006A"
			x1={698.2478}
			x2={713.3228}
			y1={665.1779}
			y2={714.1779}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#000" stopOpacity={0} />
			<stop offset={0.801} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-full10006A)"
			d="M676 1250c1.7-.1 47.7 8.4 51 9-6.8-9.8-17.9-33.6-19-38-11.7-7.4-26-11-26-11s-7.7 40.1-6 40z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10006B"
			x1={720.9899}
			x2={677.9899}
			y1={722.716}
			y2={732.272}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#cdcdcd" stopOpacity={0} />
			<stop offset={1} stopColor="#333" />
		</linearGradient>
		<path fill="url(#ch-full10006B)" d="m716 1168-30-8-13 41s16.8 20.5 36 23c2.9-20 7-56 7-56z" opacity={0.502} />
		<linearGradient
			id="ch-full10006C"
			x1={943.4958}
			x2={967.8708}
			y1={1032.8556}
			y2={902.8557}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-full10006C)"
			d="M852 859c30.6 2.7 102.1 8.5 170-13-.3 25.1-3.3 115.8-117 130-22-36.4-36.4-58.8-53-117z"
		/>
		<linearGradient
			id="ch-full10006D"
			x1={1014.9879}
			x2={915.1508}
			y1={873.6371}
			y2={931.4412}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.499} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full10006D)"
			d="M909 974s24 117.8 41 145c28.2-11.9 78.2-168.1 51-191-13.8 13.3-29.9 27.2-48 35-21.5 9.2-44 11-44 11z"
			opacity={0.302}
		/>
		<path
			fill="#535353"
			d="M824.7 262.1c17.8 28.4 47.8 62.5 93.3 77.9.4 21.1.2 82.1-1 89-16.5.2-24-3-24-3s1.4-27.7-12-31-19.9 7.3-23 14c-12.1-6.8-40.3-32.5-58.1-76.2 11.8-25.5 22.1-57.9 24.8-70.7zM961 344c12.7-4 38.9-31.1 37-74 8.2 9.6 8 23 8 23l3 34s-9.1 51-24 72c-3.5-1.9-16.5-.2-23 13-.4-43.3 1.4-46-1-68z"
		/>
		<radialGradient
			id="ch-full10006E"
			cx={364.482}
			cy={1559.9451}
			r={1043.839}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.474} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={0.532} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10006E)"
			d="M825.7 263.9c17.9 28 47.6 61.1 92.3 76.1.4 21.1.2 82.1-1 89-16.5.2-24-3-24-3s1.4-27.7-12-31-19.9 7.3-23 14c-12-6.7-39.9-32.2-57.7-75.3 5.7-19.6 21.7-61.4 25.4-69.8zM961 344c12.7-4 38.9-31.1 37-74 8.2 9.6 8 23 8 23l3 34s-9.1 51-24 72c-3.5-1.9-16.5-.2-23 13-.4-43.3 1.4-46-1-68z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full10006F"
			cx={366.272}
			cy={1559.6221}
			r={1040.9399}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.489} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={0.514} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10006F)"
			d="M826.2 264.6c18 27.9 47.5 60.4 91.8 75.4.4 21.1.2 82.1-1 89-16.5.2-24-3-24-3s1.4-27.7-12-31-19.9 7.3-23 14c-11.9-6.6-39.3-31.6-57.2-74 10.9-28.6 21.7-62.6 25.4-70.4zM961 344c12.7-4 38.9-31.1 37-74 8.2 9.6 8 23 8 23l3 34s-9.1 51-24 72c-3.5-1.9-16.5-.2-23 13-.4-43.3 1.4-46-1-68z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full10006G"
			cx={445.924}
			cy={1567.2161}
			r={1045.587}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.474} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.5} stopColor="#f2f2f2" />
			<stop offset={0.532} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full10006G)"
			d="M826.3 264.8c18 27.8 47.5 60.3 91.7 75.2.4 21.1.2 82.1-1 89-16.5.2-24-3-24-3s1.4-27.7-12-31-19.9 7.3-23 14c-12.1-6.8-40.3-32.5-58.1-76.2 11.7-31.5 21.4-56.3 26.4-68zM961 344c12.7-4 38.9-31.1 37-74 8.2 9.6 8 23 8 23l3 34s-9.1 51-24 72c-3.5-1.9-16.5-.2-23 13-.4-43.3 1.4-46-1-68z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full10006H"
			x1={1067}
			x2={1067}
			y1={698.7194}
			y2={827}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006H)"
			d="M1028 1103c3.9 8.2 13.9 47.6 5 118 17.3 1.3 42.4-1.4 54-15s19-36.1 19-55-12.9-49.3-33-58c-22 4.1-36.6 7.2-45 10z"
			opacity={0.102}
		/>
		<linearGradient
			id="ch-full10006I"
			x1={772.2698}
			x2={772.2698}
			y1={654.7878}
			y2={783.6854}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full10006I)"
			d="M793 1263s-56 6.6-63-3c-2-19.9-.4-102 14-122 21.8-4.9 62.9 2 71 7 .7 10.5.2 49.8-6 79-5 23.4-16 39-16 39z"
			opacity={0.102}
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1136 818s-5.2 18.2 1 45 3 28.7-6 58 14.2 37.3 23 32c-12.6 30.6-1.3 35.8 10 37 15.2-23 47.5-54.2 40-90s-2.8-69.5-5-87c12.7-9.5 8.9-9.2 11-26-.1-2.4-.3-4.7-.4-7M1191 672s-3.2-3.1-15-9c5.8-77.5 1.9-105.2-4-118 24.8-6.4 41-40 41-67s-14-52-14-52 9.3-5 6-27-23-33-23-33-5.1-1.2-9-.8c-3.9.5-10 1.8-10 1.8l-3-6s8.6-14.8 16-29c-11.1-20-57-35-57-35l-14 25s-49.4-22-66-22-30 7-30 7-2.6-5.5-3-17c-1.8-20.4-11-28-11-28s-.5-4-11-33-41.5-43.1-57-48c-3.8-8.2-9-16-9-16s-25.9-6.8-37-3-8.7 2.6-16 14c-28.9 4.8-59.1 25.9-65 49s-10.3 51.9-27 71-10 42-10 42-17.7 7.5-18 7m-13.5-25c-3.7-.5-7.3-1.2-10.5 0-12.9 5-69 34-69 34s-9.7-12.8-17-23c-51.8 2.3-60 40-60 40s12.3 18.3 20 30c-6.2 8.9-52 43.8-52 96s15 51.6 35 72c-16.2 70.5-1 125-1 136 0 23 6 30 6 30v19h-9v18h9v18h-11v20h9v18h-9v21h9l-9 370h82l23-370h13v-20h-11v-19h13l1-19-12-1 1-16 13-1v-18h-11l1-17s.6-.1-6-1c-.4-4.1 3.2-4.1 13-19 16.5-35.2 15-143 15-143l2-1s-.7 16.6 21 70 11.8 88.3 9 100-5.4 29.1-10 57c-13.1 6.2-14 12-14 12l-1 30s1.8 7.5 5 22c-1.3 9.8-6 45.1-5 87-15.1 99.7-18.2 184.2-18.9 208.8m-13.6 49c-2.8 14.1-5.1 31.8-5.5 53.2-1.2 63.4 6.9 83 14 111s4.3 54.3 3 74-15.6 30.2-13 58 29 94 29 94-.5 13.2 21 12 111-6 111-6 9.9-.3 3-19-51-132-51-132-6-16.6-10-31 4.5-69.8 17-124 12-67.6 14-93c45-82 76.7-220.7 80-232 1-3.3 2.2-5.5 3.7-6.8m13 6.3c7.1 58.9 27.3 119 42.7 150.5m-3.4 62.4c.4 15.3 2.3 32.5 7 50.6 19.5 70.9 23.2 125 23.8 141.1m98.9-142.7c.3-6.7 1.3-17.8 3.3-24.4m4-7c30.7-41.2 18.4-75.6 6-98 18.3-92.3-5-246-5-246l14-14s-.9-21 1-29-1.8-9.3-12-17c-6.3-31.5-25.3-89.3-27-110s2-70 2-70l6-8 15 83v10l39 105"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m571 849 36 1v-22h-35m1-17h38v-21l-37-1m4-17h35v-18h-33m3-19 100 1m-69-2c-5.7-31.9-19.4-86.9-1-150m76 170-33 1v19h39m-3 16-37-1v21h36m-2 19h-36v20h36"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M685.7 1530.9c5.6-8.7 43.3-67.9 43.3-67.9l65-1m53.1 138.9C838 1585.7 799 1520 799 1520l-8-55m-60 1 15 60-30 82m18-40h24l-1 41m47.8-2c-.8-8.5-3.8-41-3.8-41l22-1m-76-39 51-3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M572 564c7.7 7.6 50.9 39.1 137 4 42.3-14.1 62-27 62-27s16.6-138.5-39.9-221.4M635 332c.3 5.7-17.6 38.3-58 42m18 28c14.3-1.4 45.7-4.8 58-46"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M849.1 492.5C800.7 453.9 768 359.1 761 335m96 74c-25.8-18-44-49.8-55.5-78.9M919 340c-40.1-11.7-72.4-47.8-92.8-79.5C813.3 297.2 803 328 803 328m69-157c16.1 17.4 71.7 114.1 36.5 331m38.8-3.7C954.9 458.7 985.6 322 924 174m37 170c15.5-5.8 41.6-33.7 35-83M893 426c16.3 2.3 18.8 1.7 24 1.1m69-27.1c7.3-13.1 24-45.1 22-95M882 502c8.5.8 93.7 6.6 118-31m9.6-40.6c3.3-21.5 6.3-82.8-1.6-128.4"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m897 223-24-8s-9.1-26.2-21-36"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M941.5 223.4c2.1-2.1 9.5-9.4 9.5-9.4l-9-27"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1071 616c8.6-10.3 30.9-56.8 40.1-73.4m-.2-.8C974.2 646.2 756 574.8 737 558m152.1 6c-31.8.5-100.9-15.2-121.1-20m110 50 36-90m-37 95s.6 17.2 3 58c21.1 24.6 40.3 51.6 63 96 41.4-27.1 90-112 90-112l12-65-92-76m71.4 57.9c21.3-10 49-24.4 58.4-32M716 610c181.6 91.9 325.2 29.2 349 23"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1010 327c9.7 7.8 61.8 78.9 72 197 6 6.3 20 15 20 15s51.6 21.3 86-2m-89-2c.3-29.3 8.5-218.9-63-235m60 19c23 11.4 46.8 26 67 48-14.6 4-27 9-27 9s-16.2 15.7-6 42 29 26 29 26l41-18m-10 4c-14.5-1-26.4-13.4-31-28s-4.9-30.3 6-35"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1087 692c17.3 3.3 58.8 6.6 89-29m-54 29c-18.1-36.6-27-93.5-27.9-114.1M1189 673c-10.5 12.7-55 43.3-101 31m38 107c17.3 8.3 27.4 14.1 48 11s32.7-14.4 36-19m-18-128 20 7 14 87-48 31-24-6-18-87 27 8 15 80m32-111c-4.1 4.5-30.1 25.6-44 30"
			/>
			<path
				fill="none"
				stroke="#e6e6e6"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={4}
				d="m1180.5 744.3 26.6-15.8-3.6-20.9s-13.8 9.2-26.6 15.8c1.8 6.9 3.6 20.9 3.6 20.9zm5.5 32.7 27-16-3-19s-14 8.8-27 15c1.1 7.3 3 20 3 20z"
			/>
			<path
				fill="none"
				stroke="#e6e6e6"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={4}
				d="m1185 720 3 18m15 9 3 16"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1145 955c5 .1 18.7-2.7 27-59"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M906 614s59.8 1.6 108-9c-2.1 15.7-4 26-4 26s-42 14-101 11c-.6-9.6-3-28-3-28z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M872 859c-52.3-89.6-32-186.9-25-203m-103.1 36.9c26.6 26.1 44.2 41.3 94.2 50.6M1039 737c21.4-26.9 26-51.2 25-62m27 125s-6.5 43.7-148 62c-198.3-.1-206.1-39-219.2-56.8M852 861l21 58s14.7 35.2 31 56c28.4-6.6 111.4-2 119-130m67 13c-11 11.9-38.7 26.1-74.9 37.5m-144.1 21c-57.3-1.4-113.6-16.2-152-54.5m224-111c3.1 30.2-.8 95.6-3 110m69-15c8.6-26.6 27.6-31.4 37-206"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M757 980c.7-11.7.8-16.7 16-16s74.2 8.6 81 11 10.7 7.3 3 31c-7.7 23.7-23 84-23 84s-8.2 21-27 20-47-5-47-5-14.1 4.4-12-35 8.3-78.3 9-90z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M857 1005c15.4 1.9 23.4 0 31-7m-23 81c-7 4.4-16.7 5.3-29 4m-90-9c-14.9-2.6-30.6-2-43-18m8-69c9.3 9.4 27.2 13.8 43.3 16.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M687 1158s20.3 8.5 30 9c2.2-10.2 3-13 3-13l24-17s37.5-5.4 71 8c1.7 13.8.8 36.3-3 65-2.5 18.8-8.3 42.9-19.9 53.2-29.9 3-50.6 5.1-62.1-1.2-12.9-14.2-20.5-28.1-22-40-14-7.1-25.9-7.5-34-20 .7-12.3 13-44 13-44z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M815.7 1176.3c5.6.4 9.5.3 13-.4"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M744 1138c-3.6 10.9-17.6 49.9-14 122m62.8 73.5c-.8-20.9 2.2-44.7-3.8-55.5-18.5.4-71 1-71 1s8 75.3-.8 132.9c0 .6.4 1.2.8 1.2 37.9 4.4 55 1.1 64-7.1m-73-178c-.1-11.2 7.7-58.4 10-66"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M718 1409c-8.9-.1-19.9-1.9-23-5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M916.7 1003.3c10.1 4.8 41.6 5.8 75.9-2.3m86.7-48.6c6.6-8 12.1-17.2 16-27.5m4.7 73.1c-1.7 5.7-6.2 16.7-17 28.4m-75.6 38.6c-20 4.3-44.1 6.6-73.4 6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1098 1116c-5.1-8.4-19.2-20.8-25-23-14.9 2.8-45 10-45 10s-18.9 21-24 25.9c-1.3 1.2-.7 5.8-1 8-13.5 1.6-43.1-5.9-58-16.8-3.8 30.5-4.3 50.1 1 66 15.7 6.7 50.3 12.8 63 13 .1 5.4 1 10 1 10l27 12s31.2 2.4 50-16m-8 173.9c-2.5 8.4-20.7 7.3-53 8 2.2-40.2-4-109.8-8-138 18.1 2.1 57.7-6.5 67-10 2.1 15.6-.6 122.1-6 140z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M982 1375c11.1 6.6 36.2 8.9 44 6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1078 1383s11.2 33.6 45 86c34.3 53.3 43 89 43 89l-95 19s-89.7-88.8-110-109c-6.9-21.4-1.3-48.6 3-58s18-33 18-33m19 127 13-72s73.7-10.4 74-12c5 11.8 29 65 29 65l47 70m-92 19 8-83-64-56"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="m1118 1486-36 6m-5.7 44.8c4.4-.9 18.7-3.8 18.7-3.8l7 34m33-9-10-33 15-2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1003 1137c2.7 9.4 4.6 41 6 68m25 13c1.4-6.7 11.7-56.9-5-114"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M988 981c2.9 9.3 21 91 21 91s-.5 18.8 24 9 42-20 42-20 8.1-1.1 8-22-4.7-102-6-107-1.4-14.8-25 2-40.5 19.2-50 23-16.9 14.8-14 24z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M960 416c5.1-10.1 14-22.1 24-19 9.7 13.2 98.5 106.3 29 149m-18-13c11-14.2 41.2-36.3-35-112"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M876 395c6 0 20.8 5.6 16 34s-16 101-16 101-7.2 21.6-21 15-10.1-23.1-8-37c.5-3.5 2.3-15.4 3.1-24.1 2.6-26.3 5.3-63.3 6.9-70.9 2.1-10.2 13-18 19-18z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M846 518c4.9 5.3 19.4 12.8 30 6m4-17c-8.8-1.4-30-7-30-7m2-24 31 6m4-25s-7.8-6.2-16-8c-7.8-1.7-16 1-16 1m1-29s10.1-3 19-2c9.1 1 17 6 17 6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M975 438c.3-9.8.5-26.3 20-25m13 17c-5.9-.2-21.1 4.7-18 25m12 19c4.5-11.5 8.5-22.8 22-19m13 33c-5.3-3.6-21.6-3-27.3 8.5m-2.3 19.3c8.3-.4 22.8-.2 22.6 13.2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M649 404c13.8-2.9 50.7-.6 43 39s-45.1 33.1-56 25c-12.1-8.9-16.5-27.1-11.8-42 3.3-10.3 10.5-19 24.8-22z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M656 422c8.3 0 15 6.9 15 15.5s-6.7 15.5-15 15.5-15-6.9-15-15.5 6.7-15.5 15-15.5z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M1175.4 383.3c4.1-1.8 10.1 2.9 13.5 10.5s2.8 15.2-1.2 17c-4.1 1.8-10.1-2.9-13.5-10.5-3.4-7.6-2.9-15.2 1.2-17z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeLinecap="round"
				strokeLinejoin="round"
				strokeWidth={10}
				d="M646 473s27.9 8.3 35 7c7.1-1.3 16.3-6.3 23-23.3 3.7-9.1 2.5-26.9-1-34.8s-33-18-33-18"
			/>
		</g>
	</svg>
);

export default Component;

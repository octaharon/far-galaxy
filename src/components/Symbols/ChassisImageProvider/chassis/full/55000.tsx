import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f3f3f3"
			d="m1626 943 128-120s-178.1-52.1-177-52 10-18 10-18l-121-19-19 5-81-20 7-18-103-17-19 5-71-18-56-28s-7.3-11.5-21-16c-11.3-17.7-49.4-73-140-73-58.5 1.9-87 8-87 8l-5-24s10.7-6.3 13-28c5.6-2.6 9-4 9-4l1-10 8-3 5 6 11-4v-11l245-119-9-60-242 112-4-5-22 8-21 4-17 6-4 6s-14.9 6.7-19 8-51.4 17.8-67 23-52.7 49.2-4 97c-4.1 24-8 46-8 46s-23 4.3-34 25c-12 1.6-24 3-24 3s-10.5 2.9-6 39c-3.1 10-5.2 23.8 0 31-17.7 4-130.4 17.8-175 148-22.1 9.3-47 23-47 23s-13.1-1-9 72c3.1 56.7 77.7 114.7 89 120s64 17 64 17 13.2 1.3 19-12c8.6 3 18.1 5.7 28 3 7.3 9.8 20 28 20 28l-38 36 10 70 169 133 69-5 27-47-8-11s133.9 22.9 251-2c110.6-23.5 142.7-27.8 162-33 7.6 7.6 33 39.5 104-8 71.7-54.6 97-110.3 97-186 .5-28.4-30.9-36.5-40-42-6.4-19.5-22.5-62.5-49-105 46.5 21.2 76 34 76 34l10-22 115 24z"
		/>
		<linearGradient
			id="ch-full55000a"
			x1={534.8036}
			x2={534.8036}
			y1={834.3264}
			y2={1063}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full55000a)"
			d="M506 857c-13.4 3.2-50 24-50 24s-23.6 84.7 20 132 71 63.4 103 69 39 5.4 41-5c-16.4-6.5-155.7-43.5-114-220z"
			opacity={0.502}
		/>
		<path
			fill="#ccc"
			d="M1125 320s-147.6 67.4-216.9 99c-19.9 4.4-36 3.3-49.1 8-6.6 1.3-12.6 1.1-10 4 3.7 4.2 6.7 6.5 8 10-4.6 3.5-8.3 5.1-11 7 2.8 2.4-13.5-11.4-10-9-3.5 2.2-39 12.9-37 16 .9 1.4 12.4 17.5 26.7 35.4-6.8.9 3 46.7-6.7 63.6 33.4-31.6 46.5-19.6 52-16 10.9-8.3 10.7-21 11-27 3.4-1.3 6.1-3.1 10-4-1.3-3.4-1.2-8.2-1-10 3.3-.2 5.9-2.2 8.3-4.7 2.6 1.6 4.2 3.3 6.7 5.7 3.2-.6 5.6-1.2 9-2 2-2.7-1-9-1-9s249-122.2 249-121-38-46-38-46z"
		/>
		<linearGradient
			id="ch-full55000b"
			x1={811.1629}
			x2={811.1629}
			y1={1311}
			y2={1388}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full55000b)"
			d="M754 566c13.1 8.5 44 16 68-13 18.5-17 23-21 23-21s20 .3 26 8 4 24 4 24-84.2 16.1-128 45c2-22.4 4.6-32 7-43z"
			opacity={0.502}
		/>
		<path fill="#999" d="m858 443 44 50-10 8-46-53 12-5z" />
		<path fill="#999" d="m825 447 58 66-6 16-66-78 14-4z" />
		<path fill="#ccc" d="M769 466c18 0 45.3 3.5 55 43s-22.7 64-43 64-50-24.3-50-51c0-17.8 7-56 38-56z" />
		<linearGradient
			id="ch-full55000c"
			x1={1412.2734}
			x2={1412.2734}
			y1={656}
			y2={905}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full55000c)"
			d="M1336 1241c144.9-41.7 153.1-180.2 142-226 17.2 10.8 24.3 14.1 30.7 18.8 7 24.3 8.2 213.7-174.7 230.2-10.7-4.4-19-17-19-17s-9.2 2.7 21-6z"
		/>
		<linearGradient
			id="ch-full55000d"
			x1={771}
			x2={771}
			y1={574}
			y2={812}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#989898" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full55000d)"
			d="M673 1108c19.7 6.8 184.9 120.7 235 185-13 26.7-26 48-26 48l-70 5-170-131-8-70s24.5-27.6 39-37z"
		/>
		<path d="m811 1347 68-6s-169.6-151.8-243-192c5.9 59.2 7 66 7 66l168 132z" opacity={0.102} />
		<path
			fill="#8c8c8c"
			d="M1067 1005s40.1-11.8 81.1-17.7c42.5-6.1 85.9-6.3 85.9-6.3l22 33s-124.2 3.7-188 32c-1.3-14.9-1-41-1-41z"
		/>
		<radialGradient
			id="ch-full55000e"
			cx={1117.741}
			cy={1430.325}
			r={1494.201}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.427} stopColor="#f3f3f3" />
			<stop offset={0.556} stopColor="#b0b0b0" />
		</radialGradient>
		<path
			fill="url(#ch-full55000e)"
			d="M541 795c-13.5 32.2-22.3 96.7 24 145s144.9 134.2 228 173 168.6 79 244 79c-3 13.1-6 28-6 28h32s6.4-16.6 6-30c14.6-.2 53.6-2.2 67-5 1 10.3.9 24.2-2 31 13.7-2.1 32-4 32-4s3.9-16.1 3-28c17.5-2.4 65-9 65-9s2.9 5.2-2 28c11.5-1.3 35-6 35-6s6.2-6.6 4-28c15.9-2.1 33.8-4.1 61-13 1 11.3-.9 23.3-2 27 9.6-2.9 27-7 27-7l2-26s36.2-11.4 46-15c1.1 9.6 1 27 1 27l23-11s3.7-18.2 3-29c13.3-6.6 44.5-23.3 49-58-5.3 45.7-7.1 132.9-158 182-147.9 31.8-219 42-219 42s-99.1 13.7-201-4c-45.8-43.9-184.9-160.4-231-177-10.3-13.9-22-28-22-28s-39.4 6.5-103-40-67.2-168.9-6-244z"
		/>
		<path
			fill="#bfbfbf"
			d="m1621 856-174-53-27-14-88-27 3 7-93-22-31-17-76-18-5 4-61-20-65-13-50-9s-20-17.9-38-22c16.4 40.6 42.7 109.1 68.2 186 23.9 26 173.8-30 173.8-30 21.5 6.6 41 12 41 12l7 14 88 38 9-19 84 27 5 12 109 47 10-20 115 23-5-86z"
		/>
		<path fill="#bfbfbf" d="m1447 802 134-30 6-20-167 36 27 14zm-209-56 130-25 4-19-157 30 23 14z" />
		<path fill="#8c8c8c" d="m1447 804 64 113-10 25-82-151 28 13zm-233-71 28 14 62 106-10 20-80-140z" />
		<path fill="#404040" d="m1620 858 132-32-124 115-8-83z" />
		<linearGradient
			id="ch-full55000f"
			x1={1083.2452}
			x2={1006.1513}
			y1={1065.4745}
			y2={1232.4745}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full55000f)"
			d="m966 680 94 119 57-5 39 15s-61.2 29.2-170 38c12.4-74.8-20-167-20-167z"
			opacity={0.4}
		/>
		<path fill="#d9d9d9" d="m837 817 232 187-5 38-302-257 75 32z" />
		<linearGradient
			id="ch-full55000g"
			x1={995.5529}
			x2={970.2239}
			y1={966.4106}
			y2={1119.4105}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#626262" />
		</linearGradient>
		<path
			fill="url(#ch-full55000g)"
			d="m1177 886-44-70s-93.3 32.8-195 32c-104.2-23.7-162-57-162-57l172 153 17-22 212-36z"
		/>
		<linearGradient
			id="ch-full55000h"
			x1={815.9749}
			x2={860.4579}
			y1={1072.9141}
			y2={1325.1901}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full55000h)"
			stroke="#000"
			d="M854 604c25.5 13.7 56.2 36.5 61 49s50.2 116 71 193c-49.7 3-231.6-27.2-299-135-6.4-16.7-10.5-72.7-1-71s8 65 8 65l10-11s23.8-51 11-64c17.8-11.7 63.2-55.6 139-26z"
		/>
		<linearGradient
			id="ch-full55000i"
			x1={706.0844}
			x2={706.0844}
			y1={1216}
			y2={1283.1475}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full55000i)"
			d="M698 704c6.7-8.6 27.3-57.5 21-67-14.5-1-27 3-27 3s5 39.8 6 64z"
			opacity={0.702}
		/>
		<path fill="#595959" d="m1128 317 28-7 5 48-33-41z" />
		<path
			d="M764 950c20.8-19.5 68.7-60 121-60-42.5-36.7-182-155-182-155s-105.7 15.9-173 112c3.2 33.6 1.7 49.2 38 94 10.1-14.1 46.2-60.9 110-55 29.7 20.4 68.8 50.1 86 64zm396-142 41 14 7 15-58 4-16-23 26-10z"
			className="factionColorSecondary"
		/>
		<path
			d="M918 646c20.6.8 133.5-10.2 159-28 11.7 3.2 23 8 23 8s-36.7-66-120-66-95.1 3-103 3-68.3 18.3-83 27c28.2 1.6 62.6 9.2 124 56z"
			className="factionColorPrimary"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M870.6 435.8C886.8 428.4 912 417 912 417s151.8-70.3 215-99.5c18.1-4.8 28-7.5 28-7.5l8 55-245 119v11l-11 4-5-6-8 3-1 10s-3.4 1.4-9 4c-2.3 21.7-13 28-13 28l5 24s50.7-3 87-3c90.6 0 128.7 50.3 140 68 13.7 4.5 21 16 21 16l56 28 71 18 19-5 103 17-7 18 81 20 19-5 121 19s-8.9 18.1-10 18 177 52 177 52l-128 120-115-24-10 22s-29.5-12.8-76-34c26.5 42.5 42.6 85.5 49 105 6 3.6 17.6 11.7 34 23 .7 4.1 6.2 17.3 5.2 32.5-3 46.9-21.8 133.2-104.2 176.5-76.2 38.7-88.4 11.6-96 4-19.3 5.3-51.4 9.5-162 33-117.1 24.9-251 2-251 2l8 11-27 47-69 5-169-133-10-70 38-36s-12.7-18.2-20-28c-9.9 2.7-19.4 0-28-3-5.8 13.3-19 12-19 12s-52.7-11.7-64-17-85.9-63.3-89-120c-4.1-73 9-72 9-72s24.9-13.7 47-23c44.6-130.2 157.3-144 175-148-5.2-7.3-3.1-21 0-31-4.5-36.1 6-39 6-39s12-1.4 24-3c8.4-15 34-26 34-26s3.9-21 8-45c-48.7-47.8-11.6-91.8 4-97s62.9-21.7 67-23 11-6 11-6l9 10 11-6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M921 1284c-41.2-38.5-77-81.4-77-147" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M664 1097c-11.8-26.9-7-59.3-7-72" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1496 1148-22-5-9 19 13 17" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1367 1260-12-14-9 1 2 15" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M560 769c-26.6 19.9-43.7 82-20 132 25.6 53.8 80.1 98.9 152.2 149.8 92.3 65.2 213.3 131 338.8 141.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M566 941c41.8-54.3 82.7-54.2 112-55 38.5 29 160 120 160 120s-50.1 41.8-45 105"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M760 950c21.8-21 73.2-59 123-59" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1154.3 845.1c23.1-2.1 46.4-3.4 69.6-3.6" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1437 931c-16.9-14.9-42.3-27.4-70.3-34.3" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1369 898-44-38" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M528 848c33.7-48.5 116.7-112 177-112" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1429.5 1123.2c25.2-12.8 48-26.7 51.5-67.2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1067.2 1190.4c19.9-.7 43.7-2.1 69.8-4.2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1166 1184c12.7-.5 60.3-7.1 68-9" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1046 1221c9.4 19.6 32.8 52.2 66 65" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1418 1156c-21.5 45.3-74.1 78.7-120 96" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1066 1044 92 80s36.6-9 74-15c34.4-5.5 70-8 70-8l-48-90"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1271 1170c12.2-1.9 50.4-9.8 58-12" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1359 1150c9.3-2.7 38.8-12.6 46-16" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1224 1147c14.2-1.9 35-4 35-4s20.7 23.9 7 53c-13 2.5-36 6-36 6s12.5-28.9-6-55zm97-16c17.8-3.3 27-6 27-6s18.5 17.1 7 52c-14 4.2-26 7-26 7s8.7-37-8-53zm73-21c7.6 7.6 13.9 16.9 12 50 6.8-3.1 22-9 22-9s10.7-31.1-10-49c-11.3 2.8-15.4 4.7-24 8zm-239 43c10.5 7.3 17.3 37 9 59-8.5.7-31 4-31 4s11.8-33-7-60c17.8-2 14.7-1.3 29-3zm-95 8c8.9 12.9 10.4 42.1 2 58-9.3.7-33 0-33 0s11.5-36.9-2-57c15.1.1 23.3-.4 33-1z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m834 818 233 186-4 38-323-276" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m952 945 9-22 217-36" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1066 1046s133.4-37.1 191-31c-6.5-12.4-11.6-22.5-18-38-43.6-67.7-109-162-109-162"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1069 1003s38.9-11.5 78.8-17c42-5.9 91.2-6 91.2-6" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1112 994c-.5 1.1 1 42 1 42" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1066 1024c31.9-8.7 90.9-22 182-28" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1195 985 10 31" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1306 1250c73.4-16.3 204.2-78.4 169-240" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M663 1095c-42-57.3-72.1-61.4-94-66" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M634 1144c39.3 21 213.2 163.8 245 194" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m771 1245 22-15 16 14-8 27" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m676 1173 8-23-7-5-18 15" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M668 1106c44.3 14.8 189 129.8 236 181" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M450 902c17.2 52 17.7 95.1 123 179" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M621 1075c-17.7-2.9-159.9-46.4-116-216" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m836 437 17-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1162 364-35-45" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M916 491s-31.8-38.1-45.2-54" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m872 438-12-14-12 5 14 17" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m862 424 50-6" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m907 499-48-58" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m894 503-52-59" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m881 512-58-66" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m877 528-68-79" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m871 541-74-85" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M764 466c21.5-3.7 62 7.5 62 63 0 39.9-40 46-44 46s-24.3-.5-40-24"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M809 566c10.6-6.4 25.8-20 35.2-32.4 9.2-.9 14.1-.9 21.8 1.4"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m735 532 28-13 7 15-31 14" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1270 686-139 25 76 122 87 41" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1120 644-161 27 103 129 60-6" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1372 702-162 29s73.3 124.1 83.8 141.9c.8 1.4 1.2 1.1 1.2 1.1l8-20"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1579 751-163 37 83 151" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1299 852 89 29" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1197 820-75-26" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1123 797-61-105 121-20" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1618 856-171-53-27-14-82-26-2 7-92-25-33-13-76-19v3l-69-21-101-20"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M687 639c10 0 12.5 69 4 69s-9-32-9-32" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M693 706c9.7-.3 24-42.4 27-67-2.2-2.9-6-5-6-5" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M685 708c18.6 41 111 110.4 252 140 93.1 4.9 212.8-35.5 221-39"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M884 561c-29.2 6.4-137.8 34.3-175.1 75.7" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M940 661s-18.1-9.1-27-11c-1.3-.5 0-4 0-4s105.3-2.6 164-27c13.2 2.7 29 9 29 9"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M912 651s30.5 47.2 71 186c15.3-10.7 10.2-156.5-56-181" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M981 838c1.2-.3 167-36 167-36" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M914 649c-32.3-30.3-69-61.1-133-56" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m742.4 684.2-21 36.8m36.8-24.5-21 36.8m36.7-24.5-21 36.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M888 650c5 0 9 5.4 9 12s-4 12-9 12-9-5.4-9-12 4-12 9-12z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1750 825-131 31 7 85" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1461 736-129 26 61 130 43 21" />
	</svg>
);

export default Component;

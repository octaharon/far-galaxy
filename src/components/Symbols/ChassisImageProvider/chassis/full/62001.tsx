import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="m601.5 708.1-10-73 143-12s37.2 27.6 59 47c62.6 0 112.4 15.5 130 32 9-1.2 35 2.2 53 7 5.3-13.9 41.3-80 121-80s115 69 115 69l98 47-1 22 46 24 14-8 140 82-1 16 33 19 49-7 169 90-93 204.9-82 15-156-108-24-69-39-23-10 9s-10.1-3-16-6c-11.8 8.8-115.3 68.5-120 84-9.6 34.8 106.6 219.7 61 309.9-36.9 73.2-108.1 89.9-142 77s-106-83-106-83-26.7 105.8-48 191.9c-48.8-12.7-67-70-67-70l-8-214.9s-51.5-30.5-80-49.2c-16.4 43.3-30 79.1-30 79.1l-108 19s-75.2-121.8-108-181c-138.6-24.4-168.1-36.5-207-53-39-16.5-69.4-91.4-85-146-15.6-54.5-5.7-114.2 27-137-11.6-76.6 99.7-229.5 251-112 14.9-5.1 32-12.7 32-12.7zm152 518.9 13 17 8-22-12-8-9 13zm892-208-37 83 14 12 45-98-22 3zm-65 9-16 31 8 21 25-54-17 2z"
		/>
		<path
			d="m1092 833-15 52s-16.5 34.5-52 35-95.3-28.3-120-45-1 8-1 8 100 100 122 111 79.5 62.5 109 47 10-59 10-59 29.5 78.3 37 97 32 115 7 133-123-61.5-130-67-201.5-161.3-243-239-58.8-91.3-53-125 13.5-73.8 98-76c-15.2 11.2-37.8 36.8 6 76s141.5 78 184 70 41-18 41-18z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full62001a"
			x1={1207.2477}
			x2={770.2086}
			y1={-682.2562}
			y2={-1124.5142}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.071} />
			<stop offset={1} stopColor="#000" stopOpacity={0.149} />
		</linearGradient>
		<path
			fill="url(#ch-full62001a)"
			d="M1189.4 1211c21-17.5 14-76.5-36-206.9-4 10.7-3.7 30.8-18 36-24.1 19.8-131.3-52.2-229-151-69.9-63.9-131-124.1-124-156-21 36-32.6 68.3 10 138 39.9 67.6 144.2 183.1 249.4 262 65.5 53.1 123.9 90.5 147.6 77.9z"
		/>
		<linearGradient
			id="ch-full62001b"
			x1={1050.7198}
			x2={842.9169}
			y1={-958.7402}
			y2={-1173.9252}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0.149} />
			<stop offset={1} stopColor="#000" stopOpacity={0.071} />
		</linearGradient>
		<path
			fill="url(#ch-full62001b)"
			d="M859.3 705.1c-14.8 10.5-47 51.7 38 100 85 48.2 170.4 67.7 197 20-8.2 32.3-22.3 86-51 93-53.9 11.7-121.2-23.7-181-75-45.3-38.8-96.2-90.6-78-116 25.1-17.4 54.7-20.6 75-22z"
		/>
		<linearGradient
			id="ch-full62001c"
			x1={374.6461}
			x2={653.1991}
			y1={-846.3301}
			y2={-1026.7411}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.499} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full62001c)"
			d="M563.3 1130c-2.5-4.9-11-22-11-22l-18-1-27-36 1-22-14-2-21-31v-21l-12-3-18-30-2-21s-122.9-3-122-110c-52.5 51.9-37 113.6-7 184 29.9 70.4 187.7 117.4 251 115z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full62001d"
			x1={1131.6527}
			x2={1288.7947}
			y1={-852.2278}
			y2={-1079.1698}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.497} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full62001d)"
			d="m1090.4 846.1 55 29 25 4s10.6 39.3 44 34c16-10.5 24-12 24-12s-1.2 31.2 12 43 34.1 17.6 57-5c4.4 2.1 9 7 9 7l-7 9s-1.9 20.5 5 30c-12.2 9.2-108.1 59.8-112 88-17.9-36.5-112.3-187.9-121-198 2.4-15.1 9-29 9-29z"
			opacity={0.4}
		/>
		<path
			fill="#d9d9d9"
			d="m691.3 1346.9-100-206.9 292-38-84 225.9-108 19zm13-23 83-15 63-181-222 31 76 165zm826.2-311.9 229-30-92 203.9-85 15-52-188.9zm66 169 62-13 74-165.9-172 27 36 151.9z"
		/>
		<path
			fill="#a6a6a6"
			d="m1578.5 1195-48-182-153-93-45-24-16-12-129-81 1 9-43-27-25-20-70-37 14 28s45 39.5 25 89c30.8 17.2 56 30 56 30l25 5s11.9 37.8 42 34c17.7-10.5 27-14 27-14l1 29s16.2 32.9 41 28c8.6-6.6 27-19 27-19l8 7-6 7s-4.3 27.3 12 42 34 16 34 16l11-8 37 22 25 70 149 101zm-1027.2-90-41-82 12-3-22-26-15-7-36-47-9 24 22 30 12 1-2 20 22 33 13 2 1 22 26 34 17-1z"
		/>
		<path fill="#666" d="m1389.4 980-22 24 38 19-16-43z" />
		<linearGradient
			id="ch-full62001e"
			x1={1228.3538}
			x2={1187.7477}
			y1={-960.6737}
			y2={-1025.6569}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full62001e)"
			d="M1176.4 861.1c10.4 1.8 35 18.9 36 49 12.1-2.8 24.7-11.7 25-14s-17.1-48.6-41-51c-8.1 5.2-13.5 9.3-20 16z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full62001f"
			x1={1296.3975}
			x2={1255.7915}
			y1={-918.7031}
			y2={-983.687}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full62001f)"
			d="M1244.4 903c10.4 1.9 35 18.9 36 49 12.1-2.8 24.7-11.7 25-14s-17.1-48.6-41-51c-8 5.3-13.5 9.4-20 16z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full62001g"
			x1={1374.9204}
			x2={1326.1924}
			y1={-865.3093}
			y2={-943.2892}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full62001g)"
			d="M1312.4 948c10.4 1.8 44 27.9 45 58 12.1-2.8 26.7-21.7 27-24s-21.1-51.6-45-54c-8 5.3-20.5 13.3-27 20z"
			opacity={0.4}
		/>
		<path
			fill="#b3b3b3"
			d="M312.2 1020c15.7 44.8 166.2 110.2 250.1 112 8 11.4 15.3 27 19 34-39.8-7.5-99-14.1-154-32-48.4-15.7-93.8-37.4-115.1-114zm462.1 199 32-85-20 2-25 74 13 9zm104-96c34.8 14.5 263.4 108.3 311.1 218.9 25.7-.6 85.1-9.1 98-18 18.4 40-24.7 155-125 155-72.8 0-122.6-119.9-334.1-227.9 13.6-35.9 38.7-101.1 50-128zm342.1-18c-2.2-17-19.5-17.8 116-99-8.7-4.2-20.4-14.3-21-19-13.7 8.3-107.8 58.4-113 86 5.6 13.6 20.2 49 18 32zm-551.1 203.9-78-170-71-97 58 118 91 149zm74-167.9 18-2-25 61-11-16 18-43zm-150-504.9 68 45s-43.8 10.9-47 21c-7.1 2.5-12 4-12 4l-9-70zM492.3 989l15 7 65-34-1-15-79 42zm239-14-2-23-96-13-48-58-6 15 46 57 93 14 13 8zm777.1-110.9-96-9-84-50-1 13 79 50 103 9-1-13zm-143-9v16l-32 23-16-9 48-30z"
		/>
		<path
			d="m487 990 83-43-39-60-123-10 79 113zm97-112 72-24 72 97s-91.1-10.6-93-10-51-63-51-63zm189-135c-.3 1.1-7 24-7 24s-50.7.3-70-2-85.3-11.2-98-34 0-23 0-23l17-7s7.8-6.5 17.1-10.6c13.4-6 29.9-10.4 29.9-10.4l-69-46 143-13 55 47s-17.7 73.9-18 75zM410 855l207-22 4-24-229 23 18 23zm177-80s-56.7 24.7-103 27-116-8-116-8l-8-14s88.6 11 126 6 92-23 92-23 10.5-7 12-13c6.2 10.8 12.7 28.5 15 40-7.7-8.2-18-15-18-15zm-91-67-18-30s-44.5-2.5-55 4c9.6 10.4 24 31 24 31s38.7-.6 49-5zm-136 70-28-16s-17.8 37.8-14 64c24.1 14.2 59.5 32.9 95 31-13.6-13.5-53-79-53-79zm734-123 40-4-29-25-33 3 22 26zm-49 67s-47.8-11.2-58-28c-6 7.2-11 18-11 18l82 39-2-20 29 11s88.2-.6 106-7 36.9-17.5 40-30c-6.8-3.1-16-7-16-7s-6.9 24.4-62 26-108-2-108-2zm73 42 193-21-1 21-165 18-27-18zm201 122 41-31-73-45-101-8 133 84zm49-105-37 22s77.7 50.3 79 50 99 8 99 8l-141-80z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-full62001h"
			x1={464.6509}
			x2={464.6509}
			y1={-928}
			y2={-1194}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full62001h)"
			d="M456.3 942c-31.8-.3-130.9-6.6-137-109-4.6-61.9 44.1-157 140-157s137.5 71.4 151 119c-17.3-18.1-33-31-33-31s-75.3 39.1-218.1 13c24.5 42.2 35 58 35 58l17 19 15 25-18-1c0 .1 26.7 39.2 48.1 64z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full62001i"
			x1={1094.8}
			x2={1094.8}
			y1={-1069.1589}
			y2={-1196.1589}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full62001i)"
			d="M976.3 712.1c7.9-19 45.9-85 124-85 78.2 0 110.6 66.7 113 71-17.5 24.4-52.2 32.4-160 25 2.3 9.3 9 31 9 31s-49.4-32.9-86-42z"
			opacity={0.6}
		/>
		<path d="m392.2 832.1 228-20-4 21-207 22-17-23zm729.2-69 188-18v23l-164 14-24-19z" opacity={0.102} />
		<linearGradient
			id="ch-full62001j"
			x1={1028.139}
			x2={703.7861}
			y1={-596.9132}
			y2={-714.967}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full62001j)"
			d="m742.3 1142 108-15 .9-.1-40 112.1c-20.7-12.9-39.9-22-39.9-22l-34-20-12-14 17-41zm290.1 56c57.7 32.8-47 155-47 155s-84-66.8-155-101c-.6-.5-1.3-.9-1.9-1.4l50.4-127.3c7.7 3.3 98.3 43.3 153.5 74.7z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full62001k"
			x1={1227.7802}
			x2={1201.7552}
			y1={-346.0343}
			y2={-502.2243}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full62001k)"
			d="M1122.4 1464.9c36.7-10.3 88.3-37.4 69-121 58.9-6.5 84.3-10.7 98-21 19.8 70.3-70.1 199.2-167 142z"
			opacity={0.6}
		/>
		<path
			fill="#a6a6a6"
			d="M613.3 711.1c-5.5-8.3 33.7-43 166-43 109.3 0 134.6 26.3 137 30-35.7 2.4-105 5.9-129 28-9.6 11.1-14 21-14 21s-163.8-3.6-160-36z"
		/>
		<linearGradient
			id="ch-full62001l"
			x1={721.4042}
			x2={699.6145}
			y1={-548.0859}
			y2={-704.1154}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#656565" />
		</linearGradient>
		<path fill="url(#ch-full62001l)" d="m704.3 1321.9 81-13-103-143-47 10 69 146z" />
		<linearGradient
			id="ch-full62001m"
			x1={1624.0353}
			x2={1615.5192}
			y1={-697.1035}
			y2={-758.0875}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#656565" />
		</linearGradient>
		<path fill="url(#ch-full62001m)" d="m1582.5 1120 75 48-61 13-14-61z" />
		<linearGradient
			id="ch-full62001n"
			x1={861.9976}
			x2={638.9545}
			y1={-634.2688}
			y2={-726.3369}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8c8c8c" />
			<stop offset={1} stopColor="#595959" />
		</linearGradient>
		<path
			fill="url(#ch-full62001n)"
			d="m787.3 1308.9 63-182-44 7-40 108-12-18 34-88-30 4-22 61-14-20 18-38-53 8-60 7 8 16 47-7 105 142z"
		/>
		<linearGradient
			id="ch-full62001o"
			x1={742.277}
			x2={957.285}
			y1={-666.1998}
			y2={-848.1537}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full62001o)"
			d="m787.3 1308.9 63-182-44 7-40 108-12-18 34-88-30 4-22 61-14-20 18-38-53 8-60 7 8 16 47-7 105 142z"
		/>
		<linearGradient
			id="ch-full62001p"
			x1={1739.9059}
			x2={1565.8729}
			y1={-767.0331}
			y2={-838.8699}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8c8c8c" />
			<stop offset={1} stopColor="#595959" />
		</linearGradient>
		<path
			fill="url(#ch-full62001p)"
			d="m1660.5 1167 72-163.9-65 10-44 98-17-10 41-85-50 7-24 52-6-20 12-28-21 3 23 90 79 46.9z"
		/>
		<linearGradient
			id="ch-full62001q"
			x1={1582.8646}
			x2={1659.3885}
			y1={-714.3821}
			y2={-878.3392}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full62001q)"
			d="m1660.5 1167 72-163.9-65 10-44 98-17-10 41-85-50 7-24 52-6-20 12-28-21 3 23 90 79 46.9z"
			opacity={0.302}
		/>
		<path
			fill="#666"
			d="m1475.4 1017 19 11 28 104-16-10-31-105zm-48-30 16 8 26 100-16-9-26-99zm107.1-16 112-17-20-9-108 15 16 11zm-53.1-33 108-15-16-10-103 14 11 11zm-891.1 95 154-17 11 13-159 19-6-15zm21 29 160-17 13 14-164 18-9-15z"
		/>
		<linearGradient
			id="ch-full62001r"
			x1={971.2989}
			x2={971.2989}
			y1={-286.1}
			y2={-570}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.997} stopColor="#595959" />
		</linearGradient>
		<path
			fill="url(#ch-full62001r)"
			d="M984.3 1583.9c2.4-10.6 47-191.9 47-191.9s-56.1-54.7-120-92c-.1 40.6 3 196.4 6 213.9 10.3 21.4 33.7 65 67 70z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M569.3 719.1c-151.3-117.5-262.6 35.4-251 112-32.7 22.8-42.6 82.5-27 137s45.8 124.1 85 144c37.7 19.1 68.4 30.6 207 55 32.8 59.2 108 181 108 181l108-19s13.6-35.9 30-79.1c28.5 18.7 80 49.2 80 49.2l8 214.9s18.2 57.3 67 70c21.3-86.2 48-191.9 48-191.9s72.1 70 106 83c33.9 12.9 105.1-3.8 142-77 45.6-90.2-70.6-275.1-61-309.9 4.7-15.4 108.2-75.2 120-84 5.9 3 16 6 16 6l10-9 39 23 24 69 156 108 82-15 93-204.9-169-90-49 7-33-19 1-16-140-82-14 8-46-24 1-22-98-47s-35.3-69-115-69-115.7 70-121 84c-18-4.8-44-12.2-53-11-17.6-16.5-67.4-32-130-32-21.8-19.4-59-47-59-47l-143 12 10 73c0-.3-17.1 7.3-32 12.7z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M775 1218.1c-5-2.9-9.8-5.7-14.4-8.3" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m594.3 636.1 73 45" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M598.3 707.1c6.6-2.8 18.6-6.6 20-7" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M793.3 668.1c-19 0-193.7 7-180 46 11.3 32.2 150.8 31 159 31"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M768 767c-25.2.8-196.7-1.1-171-59" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M624.2 727.7c31.2-30.7 181.7-37.7 249.2-25.6" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1038.4 641.1c0 17.6 113 20.6 113-1" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1227.4 1127c-16.6-47.8-83.9-155.9-101.4-180.4" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1202.4 1072c8.6-27.7 73.5-60.4 113-86" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1114.4 1460.9c32.9 21.4 265.3-127.4-236-336.9" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1031.4 1196s14.8-25.2 29.3-49.9m134-20.6c14.7 5.4 28.1 11.6 37.7 18.5"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M760.3 990c14.3-30.7 42-83 57-83" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1187.4 1341.9c28.2-.6 74.6-4.1 101-20" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1189.4 1212c-6.8-32.9-22.9-103.4-53-170.9-33-51.2-96-124-96-124l12-63"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1088.4 846.1c-4 10.4-4 13-9 28 71.4 85.3 157.8 330.5 105 338.9-52.7 8.4-272.7-182-332.1-257.9-57.8-73.8-96.8-135.3-88-177s19.2-57.1 48-65c28.8-7.8 89.8-16.1 120-12s78.8 17.5 134 57"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M867.3 703.1c-34 13.3-44.7 56.5 26 100 70.7 43.4 180.2 75.7 200 22"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M783.3 730.1c-5.2 11.4.8 24.5 6.2 33.6 53.1 90.1 255.9 228.1 289.8 110.3"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M850.3 835.1c78.5 75.1 237.2 234.2 286 205 25.7-15.4 6.7-56.8 4-66"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M907.3 1296.9c31.4 17.5 109.4 78.2 128 97" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1579.5 1196-50-184 224-30" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1732.5 1002-174 27 38 151 62-12 74-166z" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1658.5 1167-75-48" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1405.4 1027-30-108 213-27" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1357.4 1009 27-24s-14.7-49.2-47-58c-13.9 11-29 24-29 24l2 21s9.4 29.5 28 31c1.5-11-15.1-32.2-26-32"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1357.4 1008c-7.2-33.6-26.9-49.1-45-59 2.9-3.1 5-5 5-5l-8-5"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1240.4 925c12.6-3.5 26.8 15.7 23 27" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1283.4 957c.8-10.7-11-46.4-43-53" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1316.4 885.1 13 48 11-6-8-32" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1327.4 928-107-67s-15.2-13.6-22-16c-4.3-6.2-13-45-13-45"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1283.4 957 24-19s-8.7-27-19-34-30-17-30-17l-19 16 1 22s2.9 18 23 28c10.7 2.6 20 4 20 4zm-46-58c-1.2-11.8-15.5-45-40-54-10.3 6.6-23 15-23 15v28s6.8 16 20 22c10.8 2.9 20 3 20 3s14.1-6.8 23-14z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1175.4 884.1c10.3.7 20.8 9.6 21 25" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1175.4 861.1c13.3 2.3 36.9 19.5 38 52" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1212.4 696.1c2.2 10.3-14 37-161 27.1-.9.5-1.5 1.2-2 1.9 38.3 23.9 67 37 67 37l28 20 40 26 1-7 136 85 11 8 44 26"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1070 737c19.6 5.8 84 5.3 119-3 32.4-7.7 40.3-19.8 41-28"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1053 723c-25-3.8-67.6-16.1-65-34" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1016 715 20-35s-23.2-9.5-28-16" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1018 673-18 27" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1117.4 763.1 192-18v22l-168 14 27 97-22-4-29-111z" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1165.4 877.1c1.7 1.5 10 10 10 10" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1145.4 875.1-57-31s12.1-23.5 3-45-21.1-37.1-24-39c-4.3-8.2-23-39-23-39"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1506.4 863.1-97-10-78-49 37-22" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1327.4 803.1v15l79 50 101 10" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1408.4 853.1v16" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1332.4 893 34-23v-14l-80-45-103-13" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1361.4 853.1c.3.9-44 31-44 31" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1530.5 1012-151-93" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1469.4 1097-27-102-17-10 28 100 16 12zm35 24-30-106 19 12 31 108-20-14z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m1534.5 970 114-16-20-10-113 16 19 10zm-45.1-28 111-15-18-10-110 15 17 10z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1666.5 1015-45 95-14-9 40-85" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1598.6 1023.3c-9.2 19.3-20.5 43.4-26.7 56.3m-5-21.8c4.5-9.5 9.6-20.5 14.3-30.5"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1595.5 1023-23 52-6-17 15-30" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M564.3 1132c-39.2-5.2-109.6-11.7-190.1-57-44.8-25.2-74.8-56.3-90-145.9"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m586.3 1173-78-152 273-31 104 110-59 159" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m562.3 1097 51 120" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m687.3 1341.9-97-202.9 293-39" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m627.3 1159 223-33-64 183-82 14-77-164z" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m591.3 1139-82-117" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m787.3 1309.9-105-144-47 7" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m682.3 1167 5-16" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m807.3 1134-41 106-11-16 34-88" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m762.3 1138-26 61-10-15 18-42" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m741.3 993-24-28-92-11-45-58v-14l75-27 76 102v22" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m586.3 881.1 46 58 96 12" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m640.3 861.1-25-27 6-24-43-47s-92.6 42.6-222.1 13c28.5 44.9 36 58 36 58l35 44-22-1 42 57-6 7 1 21 21 31 9 1 2 23 21 30 12 1 1 22 26 36 17 3"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M360 777c-13.1-3-22.4-11.3-27.5-17.6M317 822c22.3 24.2 54.5 33.1 95.7 37M576 764c13.6-7.6 13.5-14.1 8-26"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M588 774c-11 8.6-50.4 22.7-101 27-46.8 4-83.1 1.8-121-8"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m348 773 21-36s-6.9-3.3-13-11" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M566.3 717.1c12.2 9.6 33.6 42.6 45 83" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M318.2 830.1c.3 17.5 10.4 56.6 27 72 24.5 22.7 53.8 37 93 37h7.8"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M324.2 862.1c15.7 24.3 67.8 61 117 61" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M389.3 697.8c11.8 24.8 132.2 16.9 141.3-4.6" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M418 683c10.7 4.2 38.8 28.3 42 102m55-4c-2.8-23.5-15.8-87.7-39-103"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1171 722c0-19.1-30.8-82.5-72-95m-33 5c15.7 3.8 52.4 42.2 60 92"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M444.3 929s24.1 37.2 41 58c5.6 6.9 15 8 15 8l25 24" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m423.3 878.1 107 10 42 57 1 16-72 34s-7.4-8.6-9-10 77-38 77-38"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m619.3 810.1-228 22 17 22 205-21" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m609.2 1063.8 160.9-18 13.6 14-164.4 18.7-10.1-14.7z" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m586.2 1033.8 154.9-18 13.6 14-157.4 18.7-11.1-14.7z" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m442.3 941 105 159" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1145 1553c24-4.8 115.6-24.6 137-31s18-30.3 19-45 1.5-43.6-11-63c4.6-8.8 9.2-27.9-10-65s-23.3-33-34-46c10.2-15.1 15.5-76.9-1-151-9.8-44-31.4-102.8-43-131 5.6-10.9 2.9-57.1 2-92-3.9-20.9-17.3-49.2-32-64 .1-13.4-5.5-28-11-33-24.9-22.5-34.7-27.2-45-33s-27.7-10.4-31-11c-15.3-20.2-48-65.3-75-91 5.9-10.4 23-38 23-38l-102-81 4-8-2-3s5.3-26.6-7-49-25.7-30.6-79-38c-9.3-1.3-21.6-1.5-33-3-6-.8-13.5-11.7-20-13-12.3-2.5-39 .4-47 17-32.9 4.6-42.5 10.5-47 26-7.9 27.7-2.3 58 21 75s42 26 42 26-44.1 7-53 41c-7.1 6.5-14.3 13.1-20.9 20.7-14-14.5-20.3-21-36.1-38.7-8.2-2.2-18.3-5.8-29.8-10.5-.9.1-1.5.4-1.7.9-1.5 5.2 2.9 6.8-2.5 17.6 1.6 4 8.6 14.5 14 34 10.5 38.3-.4 72.4-6 87-6.5 17.1-23.4 31.4-30 37-12.2 10.4-52.9 32.6-89 43s-60.3 17-61 54c-.6 33.9 8.4 67.8 10 86 .9 10 15.7 35.2 23 53 46.2 112.9 54.8 112.2 61 116 53.6-7.5 84.6-12.5 97-15 13.6-2.8 14.7-12.7 15-31s-3.6-84.9-6-103-7.5-43.2-27-80c23.1-8.3 43.2-14.7 51-21 3-2.4 8.3-7.8 13.8-14 .7-.8 188.7 273.4 264.2 331 17.7 16.6 34.4 31 53 44-13.7 30.7-25 61.3-25 86s-1.2 71.7 40 114c16 16.9 24.3 12.3 28 11 15 25.9 41.2 59.9 50 69s13.7 16 32 16c2.4 0 7.1-1 18-3z"
		/>
		<path fill="#dedede" d="m1002 707 9-10s59 63.9 78 94c-26.2-10.3-44.4-16.6-53-27s-34-57-34-57z" />
		<linearGradient
			id="ch-full32003a"
			x1={550.7158}
			x2={764.0928}
			y1={1064.5951}
			y2={1277.9731}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-full32003a)"
			d="m624 620 151 167s4.8 9.2-20 20-57.7 28.6-65 41-29 44.9-66 52c-15.1-20.2-62-84-62-84s122.2-42.6 57-177c1.9-10.7 5-19 5-19z"
		/>
		<path fill="#e6e6e6" d="M625 902c-9.2 1-135.1 7.8-138.2 21.4L456 871s2-16.2 40-28 66-28 66-28 45 63.1 63 87z" />
		<path
			fill="#e6e6e6"
			d="M948 689c-9.6-7.9-54-43-54-43l-5 7s-151.5 19.5-160-30c-16.3 14.2-18 23.2-20 32s18.6 55 76 55 172.6-13.1 163-21z"
		/>
		<radialGradient
			id="ch-full32003b"
			cx={842.011}
			cy={1403.542}
			r={235.444}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.491} stopColor="#000" />
			<stop offset={0.651} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full32003b)"
			d="M765 609c15.5 8.8 70.8 28.7 95 18 4.1 5.4 11.9 16.9 25 24-18.4 3.2-151.1 18.5-157-28 19.2-9.9 24.8-15 37-14z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full32003c"
			x1={772.663}
			x2={839.405}
			y1={1294.1527}
			y2={1425.1406}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#dedede" />
		</linearGradient>
		<path
			fill="url(#ch-full32003c)"
			d="M820 561c-19.3-3.9-84.5-35.4-114-61-10.6 18.1-12.6 55.9 9 77 24.5 19.6 69.8 47.5 103 52s35.2.4 39-2c-6.2-12.9-27.2-46.6-37-66zm94-7c4.7-7.9 14.2-28.8 6-45 12.3 24 15.9 34.7 13 59-11.9-6.9-12-9.6-19-14z"
		/>
		<path
			fill="#ccc"
			d="M541 1098c-4.8-109.9-22.3-149.7-33.6-164.6 51.3-17.8 75.5 2.8 159.6-31.4 18.8-7.6 28-32.3 38-45 8.8-7.8 28.8-26 90-48 2.2-4.5-11.4-5.8-22-12-20 10.2-51.9 24.5-55 27-29.5 21.7-34.8 42.7-64 64-71.1 34.1-137.7 11.6-168 35-15.4-27.3-32-54-32-54s-11.9 11.9-5 74c16.9 60.5 18.5 70.7 31 87 10.7-.3 20.5-1.3 27-2 4.6 7.9 17.3 36.7 34 70zm575-243c-7.2-10.6-10.2-17.5-25-42-9.9-16.4-55.3-26.2-66-32-13.3-7.2-26.8-41-34-53 4.9-6.7 7.3-6.3 14-20 23.6 65 59.1 74.6 71 78 21.4 7.7 35.6 30.2 40 47 11.3-1.1 34.7-3.8 42-2s14.8 20.9 13 34c-11.2-10.1-20-17-20-17s-34.4 7.8-35 7z"
		/>
		<path
			fill="#ccc"
			d="M1004 1259c-7.6 14.2-35.5 64-25 131 8.8 62.2 59.6 88.9 68 80 4.3-4.6 8.3-13.1 12.6-23.6 2.1-5.2 60 78.6 62.4 72.6 3.2-8.1-4.6-57.2-44.8-114.9 16.5-35 41.6-70.1 89.8-70.1 76.4-3.2 113.3 65.3 124 81 3.8-12.7 9.4-59.6-48-112-11 24.6-45.9 27-77 27s-112.6-32.2-162-71z"
		/>
		<radialGradient
			id="ch-full32003d"
			cx={1136.554}
			cy={744.526}
			r={585.65}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.198} stopColor="#000004" />
			<stop offset={0.349} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full32003d)"
			d="M1003 1259c-7.6 14.2-34.5 64-24 131 8.8 62.2 59.6 88.9 68 80 4.3-4.6 8.3-13.1 12.6-23.6 2.1-5.2 60 78.6 62.4 72.6 3.2-8.1-4.6-57.2-44.8-114.9 16.5-35 41.6-70.1 89.8-70.1 76.4-3.2 113.3 65.3 124 81 3.8-12.7 21-48.5-54-118-7.8 8.8-4.6 19-12 24-13 8.9-37.1 9-57 9-31.1 0-115.6-32.2-165-71z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full32003e"
			x1={1120.8976}
			x2={1059.8976}
			y1={450.0942}
			y2={473.5102}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.701} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32003e)"
			d="M1061 1447c1.6-11.2 11.2-41.5 15-44 7.3 10.2 43.4 52.2 46 117-14.3-18.2-51.9-65.2-61-73z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32003f"
			x1={1226.6663}
			x2={1186.2813}
			y1={305.8723}
			y2={495.8723}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={0.701} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full32003f)"
			d="M1122 1524c22.3-3.9 161.3-33 178-37 12.5-45.4-31.7-153-132-153-55.3 0-81.2 39.1-94 71.5 13.5 13.5 52.5 68 48 118.5z"
		/>
		<linearGradient
			id="ch-full32003g"
			x1={1194.5562}
			x2={1167.8492}
			y1={465.7724}
			y2={603.1644}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.303} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32003g)"
			d="M1076 1405c5.3-16.6 34.3-66.7 87-71s100.8 25.9 137 102c-39.8 7.6-185 35-185 35s-26.3-54-39-66z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full32003h"
			x1={600.0223}
			x2={570.4043}
			y1={810.6437}
			y2={997.6437}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full32003h)"
			d="M543 1106c11.6-.9 104.2-11.2 110-12 0-119.3-17.8-155.8-36-175-23.6 6.6-90.3 2.1-110 14 7.3 17.7 30 39.8 36 173z"
		/>
		<linearGradient
			id="ch-full32003i"
			x1={582.498}
			x2={569.957}
			y1={930.1804}
			y2={989.1804}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32003i)"
			d="M507 933c9.6-7.8 100.6-11.6 109-14 16.6 4.7 24 43 24 43l-110 16s-19.4-40.4-23-45z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full32003j"
			x1={1189.715}
			x2={1171.6589}
			y1={902.4097}
			y2={1016.4097}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full32003j)"
			d="M1201 1017c2.5-25.7 2.7-85.3-5-114-24 .6-39.5 5.2-49 3 15 28.1 16.9 41.4 54 111z"
		/>
		<path
			fill="#e0e0e0"
			d="M1205 937c1.4-37.7-34.2-74-54-88-16.7 2.2-26.5 2.5-38 5 26.2 40.9 68 125.9 92 171 .7-23.1 1.9-47.2 0-88z"
		/>
		<linearGradient
			id="ch-full32003k"
			x1={1156}
			x2={1156}
			y1={1072.0078}
			y2={1010}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.15} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full32003k)" d="m1198 901-53 9-31-58s22.3-3.1 36-4c11.8-.7 48 53 48 53z" opacity={0.6} />
		<linearGradient
			id="ch-full32003l"
			x1={1034.3596}
			x2={962.3106}
			y1={1208.9657}
			y2={1253.9877}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#333" />
		</linearGradient>
		<path fill="url(#ch-full32003l)" d="M958 668c1.5.5 72-6 72-6l-41 67s-32.5-61.5-31-61z" />
		<path
			fill="#b3b3b3"
			d="M853 580c.1 0 12.9 14.5 14.3 15.5C881.8 605.9 958 666 958 666l28 58-93-78s-3.2 8.3-4 7c-1.7-2.6-24.5-15.3-28-22-6.8-13.2-35-56.7-42-69 5-.6 33.6 18.1 34 18z"
		/>
		<path
			fill="#575656"
			d="M894.7 566.2c16.5-1.7 30.3-3.2 30.3-3.2l-3-2s-13.1 1.3-29.1 3c-5.5.5-3.8 2.8 1.8 2.2zm-31 3.2c-13.9 1.5-24.7 2.6-24.7 2.6l39 69-4-4-40-67s12.6-1.3 28.2-2.9c4.4-.4 5.8 1.9 1.5 2.3z"
		/>
		<linearGradient
			id="ch-full32003m"
			x1={666.4185}
			x2={1138.4185}
			y1={810.0522}
			y2={1178.8182}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003m)"
			d="M737 698c24.5 22.1 118.4 8.2 137 7 22.7 29.4 253 381.8 286 445-62.3-5.3-101.8-1.3-130 8-5.6 1.8-41.3-34.5-46.1-32.2-15.2 7.2-27.2 16.5-38.9 27.2-59.1-45.3-214.9-262.8-237-298 25.9-19.7 52.1-30.6 88-44-.2-6.7-2.4-14-2.1-21.9C778.2 768.2 688 670 688 670l21-19s7 36.8 28 47z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full32003n"
			x1={1271.7439}
			x2={905.7439}
			y1={1114.1324}
			y2={925.5494}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003n)"
			d="M1241 1127c-10-33.5-64.5-175.2-154-317-23.2-10.8-76.7-27.2-95-85-14.8-12-42-35-42-35s-10.7 11.5-75 15c42.4 64.2 210.3 364.9 227 396 33.9-4.2 114.3-6.3 139 26z"
		/>
		<path
			fill="#838383"
			d="M507 1026s2.8 8.8 17 37c8.9 23.6 21 44 21 44l109-13s6.9 34-18 38-92.1 22.1-102 15-53-117-53-117 13.1.3 26-4zm554 421c9.5 12 60 78 60 78l180-38s1.9 28.5-20 36-136.5 31-147 33-17.7-.1-23-2-56.3-65-65-85c7-6.3 8.9-7.4 15-22z"
		/>
		<linearGradient
			id="ch-full32003o"
			x1={1047.9803}
			x2={1086.9803}
			y1={450.2855}
			y2={432.9545}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003o)"
			d="M1062 1446c6.6 6.5 22 27 22 27l-15 36-24-36s15.3-17.3 17-27z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32003p"
			x1={599.7166}
			x2={590.8985}
			y1={771.2742}
			y2={826.9512}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003p)"
			d="M538 1147c-5.1 5.4 28.3-3.5 60-8s53.3-3.3 55-19 0-27 0-27l-111 13s4 32.5-4 41z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32003q"
			x1={1210.37}
			x2={1198.24}
			y1={371.6857}
			y2={428.5107}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003q)"
			d="M1122 1534c13.6-2.3 170.7-35 178-36 1 6-1.9 18.3-17 24s-155.1 41.2-172 31c5.4-4.1 8.9-11.6 11-19z"
			opacity={0.502}
		/>
		<path
			d="m529 976 109-14s-7.6-33.6-23-44c-29.2 3-108 16-108 16l22 42zm418 182c15.3-17.6 37-33 37-33s12.2 13.7 50 38c18 35.7 47.7 110.1 36 140-33.3-20.2-73.5-46.9-115.6-83.8-38.6-27-161-167.4-267.4-333.2 2.4-8.1 12.9-19.9 23-28 30.8 47.8 115.6 174.7 237 300zm288-47c11.6 23 22 101 22 101l-46-94s.4-11.6-4-31c6.6 3.3 26.6 20.3 28 24zm-143 94 49 21s36.1 78.4 12 104c-26.1-5.4-34-8-34-8s3.9-25.5-2-51c-7.4-32.2-25-66-25-66zm117 1c6.3 12.6 25.1 66.2 23 114 11.6-12.3 18.1-31.5 19-45-2.6-23.7-18.7-74.4-32-99-4.1 12.4-7 17-10 30zm-64-297 51-6s-24.8-46-47-55c-22.6 3.9-32 7-32 7l28 54z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-full32003r"
			cx={1849.345}
			cy={1617.984}
			r={2425}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.554} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full32003r)"
			d="M773 818c19.3 33.1 105.4 168.5 196 264-6.7 11.3-11 19-11 19s38.2 37.7 76 62c18 35.7 47.7 110.1 36 140-48.7-29.6-112.3-73.1-175-142S686 884 686 884s18.6-41 87-66z"
			opacity={0.302}
		/>
		<path
			d="M1072 1303c4-17.3 6.1-49.9-56-178S791 711 791 711h24s230.4 379.3 298 545c11.8 28.9 6.5 53.2 3 66-13.9-5.8-26.4-11.4-44-19zm156 18c21.5-77-80-231-80-231L887 677l-44 3s280.8 460.2 316 528 24.5 109.6 19 121c-3.4 6.7-10 18-10 18l52-10s4.7-8 8-16zM924 700l21-2 244 379s75.9 126.6 69 160c-2.8 20.8-5 44-5 44s-6-56-43-127-286-454-286-454z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full32003s"
			x1={955.6375}
			x2={955.6375}
			y1={389.0179}
			y2={1000.0179}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.352} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003s)"
			d="M1072 1303c4-17.3 6.1-49.9-56-178S791 711 791 711h24s230.4 379.3 298 545c11.8 28.9 6.5 53.2 3 66-13.9-5.8-26.4-11.4-44-19z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full32003t"
			x1={1221.2046}
			x2={1173.1996}
			y1={518.9765}
			y2={734.9765}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.352} stopColor="#000" />
			<stop offset={0.6496} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003t)"
			d="M1229 1321c22.9-63.8-57-191-57-191s-18.7 7.1-45 16c68.7 123.6 57.8 165.3 51 184-3.4 6.1-8 16-8 16l50-9s5.9-8.8 9-16z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full32003u"
			x1={817.1672}
			x2={689.1452}
			y1={1006.6213}
			y2={1055.7654}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32003u)"
			d="M795 809c5.2 6.8 12.5 61.1 19 69 1.6 24.2-47.5 68.3-60 107-16.9-24.1-64.8-90.7-68-99 20.4-39.7 56.6-55.6 85-68 10.6-4.6 21.9-3.6 24-9z"
			opacity={0.5}
		/>
		<path
			fill="#ccc"
			d="M1156 1328c18.7-27.3-1.2-79.1-44-166-27.4-55.7-63.3-113.2-99-174-82.8-141-168-276-168-276l-1-28s44.8 70 103 165c30.5 49.8 64.5 110.6 98 163 73 114.2 117.7 206.7 127 230 22.8 57.3 7 88 7 88s-6.4 22-19 18-23.2-13.5-27-22c12.4 3.2 14.9.5 23 2z"
		/>
		<linearGradient
			id="ch-full32003v"
			x1={1161}
			x2={1161}
			y1={575.817}
			y2={634.0792}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003v)"
			d="m1137 1328 15 3s12.5-6 10-31 23 0 23 0-1.8 50.3-23 47-25-19-25-19z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full32003w"
			x1={1109.9243}
			x2={1109.9243}
			y1={1514.172}
			y2={3475.1311}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.554} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full32003w)" d="m1115 1320 38 11s42.3-20.9-58-196c-13.1-.8-37 4-37 4" />
		<linearGradient
			id="ch-full32003x"
			x1={1115.1567}
			x2={1115.1567}
			y1={485.223}
			y2={673.78}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003x)"
			d="m1115 1320 36 11s46.2-7.5-48-184c-7.3-13.8-35 8-35 8s33.2 65.7 47 114c8.3 29.1 0 51 0 51z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full32003y"
			x1={1217.2623}
			x2={1217.2623}
			y1={519.0811}
			y2={719.0811}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003y)"
			d="M1228 1320c4-16.1 14.1-59.2-44-167 .5-15.1 8-33 8-33s65 109.8 58 174c-3.3 5.8-12.9 22.5-22 26z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full32003z"
			x1={1213}
			x2={1213}
			y1={584.13}
			y2={787.13}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32003z)"
			d="M1252 1279c-4.2-31.2-12.8-80.2-83-196 9.8-4.1 18-7 18-7s63.3 108.3 70 148c-.4 18.8-2.4 38.5-5 55z"
		/>
		<path
			fill="#d8d8d8"
			d="M1093.5 790.3c23.3 6.3 50.8 26 60.5 39.5-15.8 0-28.5.4-38 1.8-4.7-10.6-22.5-41.3-22.5-41.3z"
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M687 886c208.1 307.4 298 359.5 317 372-13.7 30.7-27 63.3-27 88s-1.2 71.7 40 114c16 16.9 24.3 12.3 28 11 15 25.9 41.2 59.9 50 69s13.7 16 32 16c2.4 0 7.1-1 18-3 24-4.8 115.6-24.6 137-31s18-30.3 19-45 1.5-43.6-11-63c4.6-8.8 9.2-27.9-10-65-14.3-27.6-26.9-39-36.8-48.5m3.8-1.5c3-2.6 16.6-34.3 7-108s-42.9-149.8-54-173c-16-33.5-46.2-102.2-83-160-14.2-24.2-29-48-29-48s-15-15.7-39-17M813.8 476.6c14.3.7 28.2 2 38.2 3.4 53.3 7.4 61.7 15.6 74 38s7 49 7 49l2 3-4 8 102 81s-17.1 27.6-23 38m3-2 76 93s32.5 5.8 78 51c8.7 14.6 4 24 4 24m-13-32c-26.7-1-37.8 1.9-54 4M770 480s-11.1-1.3-18.1 0c-27.3 4.8-47.9 13.2-51.9 27-7.9 27.7-2.3 58 21 75s42 26 42 26-44.1 7-53 41c-11.6 8.8-17 13.7-21 19m-208 364s31.7 77.6 42 99 18 19 18 19 71.4-12.7 102-17c16.4-1.6 12-34.9 12-44s-3.5-131.5-33-165c-1.6-1.2-5-5-5-5m-117 21v88s-8.5 1.3-22 2c-11-22.7-16.6-35.4-20-52-3.9-19.1-8.7-36.5-10-58-3.2-51.5 4-59.7 44-76 34.8-14.2 92.2-37.5 105-47 14.5-10.7 20.9-19.8 28-35 9.5-20.5 13-40.9 13-51 0-5-1.6-49.4-18-71 .7-7.6 2-15.4 2.6-19 .2-1.1.9-1.8 2-1.4 6.2 2.3 28.4 10.4 28.4 10.4l138 154s5 3.9 5 26c-60.9 15.2-91.8 48.5-92.2 49.2-8.3 15.1-25.3 33.8-32.8 39.8-7.8 6.3-23.6 12.3-49 18-25.6 4.7-84.2 9.7-112 13-7.5.9-10 10-10 10z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M749 480c4.8-10.6 18-14 18-14s10.8-1.6 27-2c24.6 9.4 35 35 35 35l-31 3-30-22"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M797 501c-2.7-10.8-13-33.2-27-35" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1173 1548c12-9.7 17-28.1 17-37m61-14c0 13.7.6 24.2-15 36"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M571 1144c11.6-6.2 8.7-33 8.7-40m-48.7 39c15.5 11.6 10-28.8 10-36s-4.5-77.1-7-97-8.7-48.6-27-77m22 43s44.4-5.3 110.3-13.1m506.6-55.6c31.3-3.3 50.1-5.3 50.1-5.3m-588 237c8.7-5.2 10.9-17.8 9.7-40"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1002 1257c17.3 14.7 89.1 65.2 152 72.8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1043 1472c9.3-4.3 15.2-16.4 20-39 4.4-20.8 31.3-81.4 77.6-96.2m68.9 2.9c30.5 10.8 57.5 34.7 79.5 72.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1032 1470c10.2-10.8 18.6-53.2 26-72 5.9-15 30.9-43.7 32.9-87.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1109 1551c5.4 1 11.4-8 12-25 .4-11.4 2.1-29.2-4-47-13.1-38-29.9-58.9-41-74"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1114 1469 184-35" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1301 1486-178 38-64-83" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M983 1312c4.1 8.2 14.6 27.5 29 47s36.6 37.2 45 37 21 0 21 0m176-28c13.2-4.9 26.2-15.7 23-25"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M453.8 867.8C468.4 893.5 487 925 487 925" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M563 817s48.5 63.7 63 82.7c.1 6.6 0 15.3 0 15.3" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M481 1028c0-75.3-3.6-106.9 13-111 51.6-12.6 106.6-8.7 125-15s51.2-20.8 63-42c7-12.5 15.7-20.7 25.1-28 21.4-16.5 45.9-23.4 59.9-32 2.3-1.5 5.5-2.8 6.8-10.2.9-5.7-10.8-16.8-29.4-37.5-38.8-43-98.2-108.7-118.3-131.3"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m773 790 25 12m228-21 12-15" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m500 1008 42 98 110-13m552-66.9c0-10.3-2.5-78.6-3-90.1-.4-8.9-1.7-18.9-5.4-35.7-7.4-16.4-20.8-35.1-44.6-52.3-10.9 2.1-35 5-35 5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M991 726s33.1-54.6 39.1-64.5c.6-1-.1-1.5-.1-1.5s-43.6 4.3-73 7c5.6 13.8 20 38.9 33.1 61.6.4.7 2.5.7 2.9 1.4 6.3 10.9 10.6 22 15 30 4.8 8.7 11.3 15.4 18.8 20.8 6.4 4.7 14.6 8.4 21.9 11.7 1.9.9 6.4 1.5 6.4 1.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1003 709c3.3 8.2 8.5 15.9 15.4 29.1 4.9 9.3 11.4 19 20.1 27.4 9.6 9.3 17.9 13.3 27.5 15.5 2.1.5 16.6 6.4 22.2 10.1 8.2 5.3 18.8 16.5 27.8 40.9"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M710 651c1.2 17.1 12.3 60.5 83.5 59.1m21.6-1.1c9.3-.5 20.3-1.1 28.9-1.7m59.4-5.4c10.1-1.2 18.8-2.4 25.5-3.6M945 688l45 39"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M729 624c4.2 11.2 13.1 45.4 158 27" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m862 632-44-72 92-9 26 17" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m895 648-40-67 19-2-23-21" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m879 555 28 22 26-2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m951 693-57-48-5 7-26-18-3-6s-38.8 14.4-99-21" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m959 668-97-76" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M705 499c31.3 26.5 88.3 55.2 115 61m93-9c7-5.7 12-27.3 10-36"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M764 539c-8.8 7.9-17 15-17 15s-39.5-23.8-47-32" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M946 1159c-34.6-33.6-70.8-76.7-111.2-129.4-33-43-85.8-104.7-125.8-172.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M947.4 1152.1c-2.7 34.2 3.3 61.8 5.6 66.9m231.8 86.6c1-17-3.5-36.1-6.9-47.5-5.2-17.6-26.8-70.8-64.9-135.1-30-50.6-259.9-427.5-269.7-443.5-.2-.3-.3.5-.3.5l1 31s60.6 99.1 129.1 212.9c67.6 112.1 143 238.3 175.9 321.1 16.1 40.4 17.4 74 8 82-2.1 1.4-5 3-5 3m-36-10c7.7-13.7 5.5-54.5-19-105-12.1-24.9-81.7-155.8-150.1-280.8C876.8 806.1 813 710 813 710h-23s317.6 528.6 282 592"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1256 1225c-3-57.1-142.1-267.6-175-318S944 697 944 697s-10.1.4-21 2c26.7 42.7 101.7 154.2 146 224 41.2 65 187.8 279.4 181.9 357.9-.2 2.7.1 8.1.1 8.1m-3 8c-7.6 15.8-14.5 22-24 25"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M947 1153s19-15.4 35.8-29c14.4 12.6 32.6 27.4 52.9 42.2m54.7 35.6c16.1 9.2 32.5 17.2 48.6 23.2m71-16c2.1-12.4 4.4-25.9 5.2-41.8m-1.7-44.7c-1.6-13-4.3-27.3-8.4-43.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="M1204.3 1081.4c4 2.7 10.3 7 16.8 12.8 4.2 3.8 8.4 8.9 12.3 12.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={10}
				d="m843 678 42-3 241 379s134.9 196.3 100 269c-4.4 9-7 14-7 14s-45.1 9-50.5 9.9c-.3.1-.5-.9-.5-.9s16.4-9.3 17-48"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1171 1346c-7.9.1-19.2 4.9-39-20" />
		</g>
		<g>
			<animateTransform
				fill="freeze"
				attributeName="transform"
				attributeType="XML"
				calcMode="spline"
				dur="6s"
				keySplines="0.7 0 0.3 1;0.7 0 0.3 1"
				keyTimes="0;0.5;1"
				repeatCount="indefinite"
				type="translate"
				values="170 110; 170 170; 170 110"
			/>
			<path
				fill="#f2f2f2"
				d="m324.5 337.8 6.7 28s47.2-15.1 66.9 49.2c-5.5 4.6-10 6.7-10 6.7l2.2 8.9 4.5 4.5s-4 23.2-36.8 34.7-52.8-18.3-61.3-30.2c-2.7-4.2 5.6-6.7 5.6-6.7l-1.1-4.5s-8.8-2.2-10-8.9c-1.2-6.7 3.7-36.5 25.7-48.1 1.4-2.3 7.6-33.6 7.6-33.6z"
			/>
			<radialGradient
				id="ch-full32003A"
				cx={336.2738}
				cy={2275.8235}
				r={311.4731}
				gradientTransform="matrix(1 0 0 -1 0 2506)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0.491} stopColor="#f2f2f2" />
				<stop offset={0.875} stopColor="#000" />
			</radialGradient>
			<path
				fill="url(#ch-full32003A)"
				d="M398.7 415.4c-3.5-11.8-16.3-56.7-63.6-49.1-4.6 4.8-12.7 9.7-17.9 5.6-10.7 6.8-27.3 26-25.7 50.2 6.3 11.5 98.2 14.4 107.2-6.7z"
			/>
			<path
				fill="#d9d9d9"
				d="M297.8 443.1c9.8 4.4 39.1 8.9 39.1 8.9s8.4 18.1 23.4 12.3c15-5.8 16.7-15.6 16.7-15.6l15.6-7.8s-13.8 27-40.2 30.2c-9.8 1.2-17.9 1-27.9-4.5-16.8-9.3-32.9-26.3-26.7-23.5z"
			/>
			<linearGradient
				id="ch-full32003B"
				x1={344.0567}
				x2={344.0567}
				y1={2053.7795}
				y2={2083.0869}
				gradientTransform="matrix(1 0 0 -1 0 2506)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#f2f2f2" />
				<stop offset={1} stopColor="#323232" />
			</linearGradient>
			<path
				fill="url(#ch-full32003B)"
				d="M304.5 427.3c7.3 1.8 65 5.2 84.7-4.5.3 3.9 0 6.7 0 6.7l4.5 5.6s-3.9 9.7-15.6 12.3c-11.7 2.7-33.5 5.5-42.3 4.5s-31.8-3.2-38-11.2c-6-7.9-2.2-6.7-2.2-6.7s6.2-1.9 8.9-6.7z"
			/>
			<path
				fill="#5b5b5b"
				d="M337.9 430.7c-4.6 4.1-7 14-4.5 21.2 3.3-.1 5.6 4.5 5.6 4.5l5.6 5.6 6.7-12.3v-12.3l-1.1-7.8c-.1 0-7.7-2.9-12.3 1.1z"
			/>
			<path
				fill="red"
				d="M341.2 454.2c-3.3-7.1-2.6-18.1 5.6-21.2 6.8 0 17.7-1.1 22.3-2.2 3.1 2 5.6 6.6 6.7 14.5-2.8 5-4.6 17.9-19 17.9s-14.5-3.3-14.5-3.3 2.3 1.5-1.1-5.7z">
				<animate
					fill="freeze"
					attributeName="opacity"
					attributeType="XML"
					dur="2s"
					repeatCount="indefinite"
					values="1;0.65;1;0.75;0.9;0.6;1"
				/>
			</path>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M291.1 420.6c-1.1-13.7 8.6-42.5 29-50.3.6-11.5 2.1-28 2.9-36.3.3-3.1.4-2.9.4-2.9s.4-.3 1 2.7c2.1 10.3 6.8 33.2 6.8 33.2s49.8-15.9 66.9 48.1c-4.2 20.5-105.9 19.3-107 5.5zm102.6 15.8c3.1 8.3-49 71.2-95.5 6.6-.4-.7-.2-.9-.3-2 1 .4 2.1 1 2.2 1.1 8.1 6.4 21.2 9.6 35 10.4 6.5.3 1 12.6 20.7 12.1 7.9-.2 14.4-3.6 20.7-16.9 8.5-2.9 14.8-6.8 17.2-11.3z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M297.8 442c-2.9-4.2-3.9-8 0-10s3.3-4.5 3.3-4.5" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M392.6 437.5c-1.3-5.1.1-6-3.3-6.7-.8-4.5-1.1-5.6-1.1-5.6"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M317.8 372.5c5.2 2.5 11.4.6 14.5-4.5" />
		</g>
	</svg>
);

export default Component;

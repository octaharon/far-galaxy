import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M459 609c-20.5-23-26-29.7-34-40s4.3-34.2 22-20 41 41 41 41 6.2 5.5 5 14c16.1 14.2 81 71 81 71s17.9-43 119-43c81.7 0 102.8 12 118 23 22.1 1.6 45.4 7.6 52 19 17-21 89-113 89-113s4.2-1.7 10 1c2.5-3.7 5-7 5-7s-16.5-8.6-23-12c-11.1-5.8-6.1-33.7 12-26 20 8.5 62.7 29.3 70 33 17.2 8.6 11.3 40.3-13 29s-34-17-34-17l-4 7 6 5s-58.9 133.3-59 134 33.8 24.4 51 78c42.8 31.7 121.7 83.2 185.2 127.6 23.1 16.2 44.2 43.4 60.8 56.4 62 48.7 466.7 306.5 410 423-85.4 61.7-269.5-143.5-452-283-18 29.7-78 123-78 123s-184.9-43.9-225-53c-6.8 29.7-17 97-17 97s-20.4 10.7-50-47c-2.7-34.7-3-65-3-65s-112.9-27.6-137 10c4.4 37.6 47.8 174 45 196-2.8 22-60.2 206.8-78 216s-43.7-14.1-72-52-183.6-359.1-195-401c-11.4-41.9 37.7-97.6 109-185 57-69.9 80.4-82.7 102-141-14.5-22-33.6-43.7-19-92-24.6-29.7-81-98-81-98"
		/>
		<linearGradient
			id="ch-full60000a"
			x1={757.5006}
			x2={757.5006}
			y1={1214.8811}
			y2={1265}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-full60000a)"
			d="M653 682c0-7.7 1.5-27 106-27s103 17.2 103 23c-11.6 1.5-77.5.4-98 27-20.7 1.1-111-5.2-111-23z"
		/>
		<linearGradient
			id="ch-full60000b"
			x1={810.8021}
			x2={935.8251}
			y1={1059.6835}
			y2={1207.4185}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#747474" />
			<stop offset={0.499} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#bababa" />
		</linearGradient>
		<path
			fill="url(#ch-full60000b)"
			d="M947 787c-13.3-39.2-43.6-59-84-59s-51 52.3-51 80 18.1 74.2 65 67c-10.2-12.5-16.7-15.9-20-25s-16-115.7 90-63z"
		/>
		<linearGradient
			id="ch-full60000c"
			x1={860.796}
			x2={885.4499}
			y1={650.4472}
			y2={766.4352}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60000c)"
			d="M677 1164c58.2-26.8 219.8 22 421 67-5.2-19.7-12.5-44.6-18-53-81.3-9.6-345.1-78.1-416-60 9.4 13.5 13.5 40.6 13 46z"
		/>
		<linearGradient
			id="ch-full60000d"
			x1={1274.7607}
			x2={1140.6556}
			y1={728.2702}
			y2={816.9982}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60000d)"
			d="M1099 1231c19.2-30 75.5-121.6 95-141 15.5-27.2 34.8-46.2 14-98-36.8 76.1-51.2 96.3-125 187 8.9 29 13.6 35.1 16 52z"
		/>
		<linearGradient
			id="ch-full60000e"
			x1={842.5}
			x2={842.5}
			y1={1014.0176}
			y2={1240}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#f1f1f1" />
		</linearGradient>
		<path
			fill="url(#ch-full60000e)"
			d="M905 876c8.3 6.8 19.1 16.1 24 20-20.8 6.5-205.3 34.7-224-47 1.4-74.8 35.6-169 130-169s145 88.7 145 139c-10.2-7.1-33-24-33-24s.4-65-80-65c-52.6 0-57 57-57 72s9.1 93.7 95 74z"
			opacity={0.6}
		/>
		<path fill="#ebebeb" d="m923 707 58-134-10-15-21 3-88 112 3 8s40 12 58 26z" />
		<linearGradient
			id="ch-full60000f"
			x1={1037.5065}
			x2={918.4456}
			y1={1228.7324}
			y2={1299.4534}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.501} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60000f)"
			d="m968 555 13 8-5 8 5 4-60 134s-29-24.1-59-27c-.5-3.8 2-10 2-10l90-112 9 3 5-8z"
		/>
		<linearGradient
			id="ch-full60000g"
			x1={952.642}
			x2={984.149}
			y1={1320.7905}
			y2={1383.7905}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60000g)"
			d="M951 516c-10.7 1.3-15.8 18-9 27 11.3 7 70 36 70 36s-12.3-25.4 10-29c-22-11-58.3-29.1-71-34z"
		/>
		<linearGradient
			id="ch-full60000h"
			x1={399.994}
			x2={459.04}
			y1={1297.3989}
			y2={1355.261}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.503} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60000h)"
			d="M427 549c7.7-9 23.6-1.5 56 37-13.7-.8-29.7.6-23 25-18.7-18.8-45.1-47.9-33-62z"
		/>
		<linearGradient
			id="ch-full60000i"
			x1={979.0814}
			x2={963.2634}
			y1={1376.3163}
			y2={1347.3163}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full60000i)" d="m963 555 18 11-4 18-30-18 16-11z" opacity={0.8} />
		<linearGradient
			id="ch-full60000j"
			x1={429.2835}
			x2={572.7584}
			y1={1112.9073}
			y2={1259.2664}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.499} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60000j)"
			d="M481 619c8.5 15.2 103.7 128.2 111 137 14.9 2.7 44.8-6 27-38-24.9-28.6-125-112-125-112s-4.2 8.3-13 13z"
		/>
		<path
			d="M564 843c32.1-29.8 103.7-99.3 173-106 3.8-5.7 6-11 6-11s-81.8-2.8-99-18c2.3-17.3 11-36 11-36s17.5-21.8 149-17c-22.1-19.7-73.7-23-120-23s-94.6 14.3-109 44c12.9 12.4 46 41 46 41s17.2 34.6-25 40c-14.3-14.8-36-38-36-38l-4 12s47.4 38 8 112z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full60000k"
			x1={508.8536}
			x2={620.8537}
			y1={1043.7828}
			y2={1200.5698}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full60000k)"
			d="M565 844c29.7-32.2 66.8-63 104-82-33.5-25-66.3-81.3-81-100-10 7-13.1 10.6-13 13 4 3.7 46.2 36.5 50 48s-8 34.1-30 34c-12.4-13.3-37-39-37-39l-1 13s46.3 36 8 113z"
			opacity={0.502}
		/>
		<path fill="gray" d="m1102 929-37 62-36-54 73-8z" />
		<path fill="#b2b2b2" d="m875 834 15 13 7 10-24 20s15 1 30-2c13.5 10.1 157 114 157 114l-30-51-161-121 6 17z" />
		<path fill="#191919" d="m897 857-18 15-19-19 16-17 21 21z" opacity={0.302} />
		<path
			fill="#ccc"
			d="m947 798-20 1-2-22s16.2 6.4 22 21zm-61-21 11 23s-24.4 3.5-36 5.1c-1.3.2 6.1 16.4 12 28.9-6.2 6.5-15.6 14.9-16 15-.6.1-2-43-2-43s5.9-23.5 31-29z"
		/>
		<path fill="#b2b2b2" d="M556 729c13 14.7 31.8 41.8 22 77-17.5-19.4-30.4-44.5-22-77z" />
		<linearGradient
			id="ch-full60000l"
			x1={1047.4307}
			x2={1405.8057}
			y1={998.8622}
			y2={680.3472}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a1a1a" />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-full60000l)"
			d="M1193 1088c55.2 49.9 249.2 199.5 305 159 31.2 35.1 108.2 121.3 130 147-85 72.9-316.4-190.5-452-284 7.4-11.1 10.6-14.1 17-22z"
		/>
		<path
			fill="#d9d9d9"
			d="M651 685c-4.9 11.8-6.4 16.9-8 23 21.7 11.2 74.4 19 101 15 7.3-9.6 12.3-15.3 16-19-28.7-.1-94.2 1.8-109-19z"
		/>
		<path
			fill="#b2b2b2"
			d="M626 1589c13.6 0 30.4-39.9 57-116s33.3-105.6 17-99-16.6 42.2-43 112-53.3 103.8-31 103z"
		/>
		<path
			d="M489 935c66.6 67.8 158.9 411 204 440-14.2 48.4-64 189-82 210-21 .5-79.2-98.4-137.2-212.7-42.7-84.2-86.2-176.9-109.8-244.3-8.8-44.4 100.7-167.6 125-193zm1009 312c.7 1.6 130 145 130 145s51.4-52.3-134-215c9.4 20 22 55.5 4 70z"
			className="factionColorSecondary"
		/>
		<path
			d="M693 1374c-8.1 28.7-42.8 154-83 211-39.2-20.9-88.8-113.9-137-215.1-42.6-89.4-84.9-185.4-108.4-243.9-4.5-51.8 105.9-169.5 126.4-191 79 105.5 156.2 402.6 202 439zm790-205c19.1 25.2 34 52.1 16 80 20.8 23.6 117.2 130 129 143 31.8-79.3-111.9-191.7-145-223z"
			opacity={0.102}
		/>
		<path
			fill="#d9d9d9"
			d="M475 586c9.1 0 17 9.3 17 17s-5.6 16-18 16-15-8.4-15-17 6.9-16 16-16zm546-35c7.2 0 15 5.5 15 16s-8.2 13-13 13c-4.8 0-14-4.3-14-15s4.8-14 12-14z"
		/>
		<path
			fill="#e6e6e6"
			d="M1189 941c53.3 72 368.2 224.7 311 306-67.7 42.1-288.1-142.4-306-157 18.5-30.5 43-65.8-7-137-4.3-6.2-.8-8.7 2-12z"
		/>
		<path
			fill="#e6e6e6"
			d="M701 1373c23.3 2-15.2-130.9-34-196 11.3-14.2 18.7-21.2-3-54S540.9 950.5 506 914c-8.1 8.6-16 23-16 23s42.1 49.7 76 149c74.1 198.7 117.1 298.5 135 287z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full60000m"
			x1={839}
			x2={839}
			y1={641.9999}
			y2={755}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#737373" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full60000m)"
			d="M804 1165c20.3 4.8 59.6 13.6 70 16-3.3 15.9-12.9 97.1-20 97s-35.5-12.8-48-48c-.3-37.8-.5-51.2-2-65z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M459 609c-20.5-23-26-29.7-34-40s4.3-34.2 22-20 41 41 41 41 6.2 5.5 5 14c16.1 14.2 81 71 81 71s17.9-43 119-43c81.7 0 102.8 12 118 23 22.1 1.6 45.4 7.6 52 19 17-21 89-113 89-113s4.2-1.7 10 1c2.5-3.7 5-7 5-7s-16.5-8.6-23-12c-11.1-5.8-6.1-33.7 12-26 20 8.5 62.7 29.3 70 33 17.2 8.6 11.3 40.3-13 29s-34-17-34-17l-4 7 6 5s-58.9 133.3-59 134 33.8 24.4 51 78c42.8 31.7 121.7 83.2 185.2 127.6 23.1 16.2 44.2 43.4 60.8 56.4 62 48.7 466.7 306.5 410 423-85.4 61.7-269.5-143.5-452-283-18 29.7-78 123-78 123s-184.9-43.9-225-53c-6.8 29.7-17 97-17 97s-20.4 10.7-50-47c-2.7-34.7-3-65-3-65s-112.9-27.6-137 10c4.4 37.6 47.8 174 45 196-2.8 22-60.2 206.8-78 216s-43.7-14.1-72-52-183.6-359.1-195-401c-11.4-41.9 37.7-97.6 109-185 57-69.9 80.4-82.7 102-141-14.5-22-33.6-43.7-19-92-24.6-29.7-81-98-81-98"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1010 1215c-10-53.1-31.5-105.4-72-158 12.6-22.2 36-52 36-52s53.2 19.2 69 26c24.4 19.2 79.2 68.5 91 149"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M765 1091c32.8 8 88.3 16.7 106 21-11.7-15.7-16.5-21.3-23-29-18.3-3-78.5-13.7-101-17 6.7 9.5 13.3 19.1 18 25z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1134 1038 6 28 34-49-6-26-34 47z" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M661 1118c33.3-4.7 79.9-11.8 334 44 60.7 12.4 89 18 89 18s111.4-136.6 123-191"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M550 861c47.1-58.5 142.4-124 185-124" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M672 1169c30.5-16.4-68.4-137.7-166-255" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M571 988c37.4-33.1 122.2-91.2 137-98m306.5-50.1c19.8 4.1 45.1 10.2 63.1 18.7"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m983 564-20-11" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1026 550c-12.3-4.1-27.9 17.2-9.8 30.4" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M575 816c8.2-23.3 12.6-47.4-18-87" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M811 655c-29.4-2.1-159-7.6-159 26 0 24.8 99.9 23.3 111.8 23"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m653 676-10 31s16.6 16.3 100 18" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1098 1232c-9.7-37.9-25.7-101.3-201-328" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M947 796c12.7 10.6 157 132 157 132l-40 65-159-118s-92.9 28.3-94-76c-.7-69.3 52.2-70 59-70 23.6 0 78 14.9 77 67z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1103 928-75 9 36 54" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1030 940 860 807s-.6-.3.1.8c4.1 6.7 17.9 29.2 17.9 29.2l14 11 7 11 3 15"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m860 806 32-5 14 10 20-2v-10l21-2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M888 848s-24.1 5.4-28 6m18 18 17-14" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m853 854 23-19" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M844 870c-37.6-31.6-10.7-153.2 90-112" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M947.1 795.5c-.2-3-2.1-6.5-2.1-6.5-24.6-31.5-111.7-22.2-89 62 4 15.4 22.8 22.1 28 23"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m924 776 2 32" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m884 775 22 35" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M881 776v25" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M972 783c5.3 14.8 7.4 26.4 10 42" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M924 710c-18.4-15.7-50-30-89-30-107.5 0-129 116.8-129 169s132.5 71 226 48"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m861 908 9-17s28.4-.8 50-5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M724 879c0-22.7-1.3-40 4-66 9.5 3.9 14 10 14 10s-4 5.8-4 66"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M708 832s-20 5.2-20 23c0 50 128.6 81.2 220.9 62.7 4.1-.8 35.2-5.5 43.1-9.7m62.1-57.3c3.9-12.4-3.3-24.1-31.1-31.7"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M802 1165c18.7 3.9 52.9 10.7 74 15" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1176 1112c16.8-30.7 97-78.1-23-202" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1192 1089c47.9 41.9 266.9 211.1 310 153 21.9-29.6-28.7-79.5-36-87"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1499 1248 130 145" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M710.9 1359.1C675 1455.8 581.2 1049.2 490 935" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M693 1373c-11.2 35.4-55.2 181.7-81 211" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M667 1176c-19.4-33.8-64.1-113.6-98-93s-114.8 165.3-130 216"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1216 1106c12-42.3 15.7-109.4 37-112" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m557 714 37 42s44.7-2.3 25-41c-29.1-27.8-46-40-46-40" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M582 742c13.1 5.2 36.3-10.4 30-33" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M475 585c9.4 0 17 7.6 17 17s-7.6 17-17 17-17-7.6-17-17 7.6-17 17-17z"
		/>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-full43001a"
			x1={851.9901}
			x2={851.9901}
			y1={-6.1}
			y2={-1380.2}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#1a1a1a" />
		</linearGradient>
		<path
			fill="url(#ch-full43001a)"
			d="M588.3 648.6c93.5 31.4 224.5 33.9 224.5 33.9s8.5-10.2 14.2-18.6c.8-22.2-7.1-161.7-5.5-174.1 5 9.7 16.4 119.4 16.4 119.4l42.7 121.5 25.2 21.9s3.6-.5 9.6-1.2c11.2 205.3-33.8 153.9-12.9 344 16.2-111.4 58-305.4 54.4-349.3 50.3-6.4 119.8-15.4 119.8-15.4l5.5-30.7s15.4 12.9 24.1 20.8c-4.3 16.8-2 102.7-1.1 115 6.3 87.6 35 200.8 42.7 234.3 15.4 39.3 19.4 141.6 19.7 151.1 5.7 155.4 22.8 207.7 38.3 239.8 48 99.4 96 139.2 128.1 182.9-21.4-7.2-131.6-102.2-175.2-201.5-21.6-34.9-32.7-179.9-36.1-215.7-10.2-105.4-22.9-137.9-31.8-168.6-10.5-24.6-31.7-143.7-35-158.8-2.1 6.4-3.3 43.9-3.3 70.1 0 48.9 28.5 153.7 28.5 222.3 0 58.7-32.8 217.5-32.8 294.5 0 35.5 7.7 125.6 7.7 170.8 0 88.5-42.6 161.8-65.7 203.7 15-41.6 35.5-131.9 36.1-174.1 1.2-77.3-14.5-148.4-16.4-227.8-2.8-114.3 16.4-157.8 16.4-254 0-114-41.8-182.1-41.6-254-44.2 92.7-52 257.1-54.7 316.4 16.1 92.7 14 166.6 12 198.2 12.9 94.7 27.7 166.6 33.9 208-16.2-21.6-44.3-115.6-48.2-127-25.6 129.6-108.1 242.9-126.8 267.4 13.7-30.1 22.7-60.8 32.6-91.1 65.1-197.9 60.2-388.6 23-524.5-10.7 94.2-79.8 220.3-94.2 237.6-1.3 159.9-84 384.1-118.3 428.1 14-39.1 51.7-188.8 58-340.5-2 2.1-13.6 13-24.1 23 2.4-5.6 19.5-33.9 25.2-42.7 2.2-45.7.6-146.9-3.3-166.4-37.8 145.4-84.1 205.2-122.6 234.3 57.5-105.6 106.4-334.5 110.6-353.7-7.5-78.9-24-144.9-47.1-232.1 4.2 75.2-26.8 112.7-60.2 162.1-15.7 23.2-31 49.3-48.2 75.6-44 67.1-76.4 116.6-87.6 138-30.2 54.9-29.5 121.8-33.9 167.5-1.3 91.2 16.8 172.6 26.3 212.4-68.5-105.6-74.1-251.1-62.4-352.6 14.5-79.1 54.5-134.3 63.5-158.8 9.4-25.6 48.2-78.6 82.1-132.5 27-42.9 51.5-86.2 50.4-117.2 0-44.9-20.8-97.2-20.8-152.2 0-78.8 30.6-156.5 38.3-179.5zm258.4 256.2c5.5-32.6 16.8-128.9-13.1-214.6-11.3 8.4-9.8 135 13.1 214.6zm-84.3 446.8c21.7-69.3 51.6-215.5 30.7-361.3-16.8 33.5-45.4 159-48.2 168.6 1 9.1 6.5 34.8 9.9 70.1 4.2 44.8 6.6 101.6 7.6 122.6zm-6.6-671.2c-14.1-1.7-54.5-6.9-59.1-7.7-6.5 6.2-27.3 40.7-33.9 82.1 2.1 60.7 50.3 272.6 63.5 315.4 7.7-29.3 13.1-95.2 13.1-111.7s-7.7-101.1-7.7-147.8c0-46.7 1.8-110.4 24.1-130.3z"
		/>
		<path fill="#dadada" d="M1136.9 1025.3c6.6 3.7 23.9 13.8 15.3 55.8-5.3-13.3-11.9-40.2-15.3-55.8z" />
		<linearGradient
			id="ch-full43001b"
			x1={427.5789}
			x2={469.9359}
			y1={-624.6572}
			y2={-698.0206}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.005} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full43001b)"
			d="M473.3 1173.1c7.8 20.2-8.9 66.2-41.6 73.4-3.2-16.1-5.5-25.2-5.5-25.2s-.2-25.7 21.9-41.6c16.1-5.3 12.7-3.6 25.2-6.6z"
		/>
		<path fill="#e6e6e6" d="M451.4 1180.8c6.2 5-12 42.9-24.1 35 2.2-23.2 21.4-37.2 24.1-35z" />
		<linearGradient
			id="ch-full43001c"
			x1={914.6}
			x2={914.6}
			y1={-454.6001}
			y2={-496.8}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full43001c)"
			d="M914.6 1373.2c11.7 0 21.1 9.4 21.1 21.1s-9.4 21.1-21.1 21.1-21.1-9.4-21.1-21.1c0-11.6 9.4-21.1 21.1-21.1z"
		/>
		<path
			fill="#d9d9d9"
			d="M915.7 1385.5c3.7 0 8.8 4 8.8 9.9 0 5.8-4.7 8.8-8.8 8.8s-8.8-3-8.8-8.8c0-5.7 5.1-9.9 8.8-9.9z"
		/>
		<path
			fill="#f2f2f2"
			d="M1193.8 643.1c-13 11.3-44.8 19.1-77.7 25.2-33 6.1-70.1 7.7-70.1 7.7l-7.7 58-131.4 18.6-26.3-21.9-42.7-121.5s6.8 75.1-3.3 73.4c-2.5-5.6-5.3-14.1-7.7-17.5-4.8 6.3-9.1 12.9-15.3 17.5-136.3 0-265.7-47.9-278.1-52.6-1.4 4.3-6.6 55.8-6.6 55.8s-14.1-6.4-23-12c-25.4-75-7.7-173-7.7-173s4.7 3.2 8.8 5.5c6.2-62.1 45.4-117.8 71.8-149-12.8-12.1-21.5-25.1-21.5-25.1s-9.4-55-10.9-94.2c15.1-.9 98.5-2.5 109.5-5.5 17.4-4.2 31.4-8.5 40.7-10.2 8.3-.8 20.2-.4 39.3-.7 41.5-.2 226.7-5.5 226.7-5.5l10.9 6.6V75.8h-7.7V36.4l13.8-4.5 12.4 4.5v39.4h-6.6l-1.1 155.7 16.4 16.4V127.2h-3.3V99.9h15.3v27.4h-4.4s1 120.9 1.1 130.5c1.7 1.9 8.1 11.6 19.7 27.4 8.1 4.9 19 7.8 26.3 13.1 53.6 13.5 115 32.8 115 32.8l1.1 16.4 1.1 4.4 82.1-9.9 128.1 49.3-26.3 49.3-20.8 9.9-164.2 12 6.6 20.8 94.2 82.1 6.6 24.1-27.4 46-55.8-10.9c.1-.1.3 10.4.1 18.5z"
		/>
		<path fill="#ccc" d="m603.6 261-58-21.9 6.6 70.1s8.6-10.4 19.7-20.8c14.2-13.4 31.7-27.4 31.7-27.4z" />
		<path
			fill="#b3b3b3"
			d="m796.3 311.4 109.5 35 32.8 73.4 9.9-6.6-19.7-49.3 35 10.9s-10.4-17.8-9.9-17.5 159.9 53.7 159.9 53.7l25.2 41.6-132.5-8.8 35 29.6 14.2 49.3 167.5 70.1 25.2 42.7-143.3-28.5-29.6-50.4-29.5-7.6-13.1 7.7-110.6-35-15.3-49.3-1.1-9.9-112.8-144.6 3.2-6.5z"
		/>
		<path fill="#999" d="m1143.4 455.9-1.1 3.3-100.7 14.2-33.9-28.5 135.7 11z" />
		<path
			fill="#ccc"
			d="M985.8 350.8v-18.6h-12l-69 14.2 33.9 73.4 9.9-6.6-19.7-51.5 27.4-6.6 29.5-4.3zm-11-27.4v-14.2l72.3 17.5 122.6 2.2v18.6l-123.7-4.4-71.2-19.7zm139.1 87.6 70.1-8.8 17.5 18.6-51.5 6.6 15.3 31.8-24.1-4.4-27.3-43.8zm108.4 181.8 28.5 43.8 27.4-48.2-7.7-19.7-19.7 33.9-19.7-29.6-8.8 19.8zM1334 449.3l15.3-35-48.2 1.1 13.1-19.7 66.8-4.4-26.3 48.2-20.7 9.8z"
		/>
		<path fill="#e6e6e6" d="m1300 416.5 13.1-21.9-181.8-38.3-28.5 13.1 197.2 47.1z" />
		<path fill="#d9d9d9" d="m1221.2 592.8 9.9-21.9-190.5-99.6 14.2 53.7 166.4 67.8z" />
		<linearGradient
			id="ch-full43001d"
			x1={1217.6356}
			x2={1228.7147}
			y1={-1414.4558}
			y2={-1508.6239}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.504} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full43001d)"
			d="m1334 449.3 16.4-36.1-48.2 2.2-199.3-47.1-3.3 4.4 84.3 29.6 16.4 19.7-50.4 4.4 18.6 36.1 165.5-13.2z"
		/>
		<linearGradient
			id="ch-full43001e"
			x1={1262.0985}
			x2={1070.2915}
			y1={-1278.6639}
			y2={-1423.2009}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full43001e)"
			d="m1148.9 461.4 121.5 104-20.8 37.2-17.5-31.8-187.2-98.5 99.6-14.2 4.4 3.3z"
		/>
		<radialGradient
			id="ch-full43001f"
			cx={786.6617}
			cy={-1851.6793}
			r={1281.072}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.219} stopColor="#000" stopOpacity={0} />
			<stop offset={0.509} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full43001f)"
			d="M827 664c0-38.9-4.5-209.6-27.4-273.7-10.3-28.9-49.1-143.7-98.5-130.3-37.2 21.9-168.2 91.4-197.1 246.4-4.3-3-8.8-5.5-8.8-5.5s-13.6 120.1 8.8 173c11.4 7.1 23 12 23 12s4.4-47.9 7.7-54.7c9.8 3.8 141.3 50.8 279.2 51.5 4.2-5.2 6-7.4 13.1-18.7z"
			opacity={0.141}
		/>
		<path
			fill="#ccc"
			d="M1074.5 556.6 1046 677.1s107.9-2.4 150-36.1c-.4-5.5-1.1-16.4-1.1-16.4L1104 607l-29.5-50.4z"
		/>
		<path fill="#999" d="m851.1 614.7 54.7 138-25.2-23-42.7-121.5 13.2 6.5z" />
		<linearGradient
			id="ch-full43001g"
			x1={987.8506}
			x2={967.0506}
			y1={-1814.45}
			y2={-1814.45}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.493} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path fill="url(#ch-full43001g)" d="M988 36.4v38.3h-20.8V36.4s1.7 4.3 9.9 4.3c8 0 10.9-4.3 10.9-4.3z" />
		<linearGradient
			id="ch-full43001h"
			x1={1009.7906}
			x2={996.6363}
			y1={-1754.2776}
			y2={-1754.2776}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.493} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-full43001h)"
			d="M1009.9 103.1v24.1s-8.9 3.2-13.1-1.1v-23s3.3 1.1 6.6 1.1c3.2 0 6.5-1.1 6.5-1.1z"
		/>
		<linearGradient
			id="ch-full43001i"
			x1={981.3442}
			x2={972.5844}
			y1={-1716.35}
			y2={-1716.35}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.493} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path fill="url(#ch-full43001i)" d="M981.4 76.9v153.5l-8.8-7.7V78l8.8-1.1z" />
		<linearGradient
			id="ch-full43001j"
			x1={1005.4458}
			x2={998.876}
			y1={-1678.1}
			y2={-1678.1}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.493} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path fill="url(#ch-full43001j)" d="M1000 130.5h5.5v122.8l-6.6-7.7c0 .1 1.1-108.1 1.1-115.1z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M588.3 650.8c-33.6 91.6-37.2 139.6-37.2 192.7s34.4 116.2 12 179.6c-10.3 29.2-49.9 84.4-90.4 149.9m-40.9 70.5c-31.4 58.6-56.8 120.6-60.3 177.1-6.4 104.2 10.8 235.7 66.8 325.2-21.6-86.9-39-196.7-14.2-329.6 12.1-64.8 83.9-151.2 120.4-212.4 61.4-102.9 103.8-128.2 96.4-226.7M662.8 757c9.3-41.9 21.7-71.3 35-85.4M603.6 653c-9.9 30.7-16.8 97.2-6.6 165.3 12.7 84.7 202 528.5 47.1 1047.9 61.4-80.8 106.1-272.4 118.3-427 9.1-116.8-7.2-210.3-9.9-232.1-18.5-154.2-125.8-460.6-78.4-537m149.6-2.9c13 24.9 38.3 101.2 28.5 201.5S825.1 957.1 839 1091c14 133.8 119.5 353.7-47.1 743.5 51.6-92.9 129-167.6 150-376.7 12.7-126.1-38.4-284.8-44.9-421.6-5.3-111.3 33.5-136.3 18.4-284.7m-11.9 355.7c18.6-162.6 56.7-293.8 53.8-361.1m70.1-9.9c-21.7 47.4-43.9 116.2-42.7 211.3s47.6 139.7 41.6 274.8c-6 135.2-26 144.5-12 297.8 13.9 153.4 28.1 172.7-26.3 348.2 16.7-30.2 50.6-88.5 63.5-155.5 13.9-72-5.6-155.7-2.2-249.7 3-82.6 29.6-173.9 31.8-261.7 2.5-100.6-39.2-172.8-26.6-295.6 8.7-84.8 26.1-151.3 51.8-186.2.4.2.9.6.9 1.2-5.3 34.7-13.4 147 45.1 362.4 24.3 89.7 9.2 240.7 38.3 335.1 11 35.4 25.2 65.2 44.9 96.4 36.1 57.2 64.8 90.2 108.4 136.9-91.9-56.2-172.6-165.4-191.6-226.7-29-93.2-23.2-244.5-55.8-349.3-19.8-63.4-33.2-133.8-39.8-185.4m-128.1 652.3c13.2 49.8 30.3 98.3 51.7 143.1-16.5-76.4-29.7-151.8-38.2-225.6m-11-180.6c.1-117.3 15.9-229.4 53.6-334.1m-258 129.2c7.6-29.9 15.1-114.8 12.7-140.1-23.7-243.2 12.1-240.5 18.6-255.1m-1.1 1.1c-3.6 81.7 22 212.9 36.1 307.7 9.9 66 16.6 204.5-28.3 359.6m-62 155c-9.2 17.6-19.2 35.1-30.1 52.5 10.4-10.8 20.3-22 29.9-33.4m59.6-82.4c52.2-83.8 83.6-165.9 95.5-246.6m-10.9-281.4c-12.1-67.8-30-162-14.2-224.5m-39.4 301.2c-9.5 21.4-27.6 88.1-48.4 172.5m-44 178c-23.1 89.2-53.3 174.4-124.4 237.5 49.3-99.8 85.7-232.3 112.8-364.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M914.6 1373.2c11.7 0 21.1 9.4 21.1 21.1s-9.4 21.1-21.1 21.1-21.1-9.4-21.1-21.1c0-11.6 9.4-21.1 21.1-21.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M914.6 1384.7c5.3 0 9.6 4.3 9.6 9.6s-4.3 9.6-9.6 9.6-9.6-4.3-9.6-9.6 4.3-9.6 9.6-9.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M473.3 1173.1c7.8 20.2-8.9 66.2-41.6 73.4-3.2-16.1-5.5-25.2-5.5-25.2s-.2-25.7 21.9-41.6c16.1-5.3 12.7-3.6 25.2-6.6z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1138 1025.3c11.3 6.2 22.7 28.4 13.1 58" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M449.2 1179.7c6.8-.5 4.2 13.5-2.2 24.1-8 13.2-22.8 15.9-18.6 7.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M996.7 101c3.1 3.3 9.9 4.3 14.2 0" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M966.1 37.5c2.8 4.4 18.2 4.2 24.1 0" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M534.6 631.1c-11.7-3.8-33.6-14.2-42.7-26.3m12.1-98.5c9.3 6.5 24.1 16.4 24.1 16.4l25.2 58-1.1 58m144.7-88.8c10.1 69.5.9 122.8.9 122.8m-143.4-52.5c107.5 40.8 240 44.3 272.6 44.9m224.4-7.3c59.1-5 113.5-14.5 142.4-26.7m-48.2 31.8c0-15.5-4.5-41.5-7.7-49.3M601.4 349.3c4.9 4.9 9.5 9.9 13.9 14.9 2.1 2.4 9.5 11.3 11.1 13.4m9.4 12.8c31.4 44.6 48.3 92.9 57.1 136.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1013.1 126.1s-4.5 2.2-9.9 2.2c-5 0-8.8-2.2-8.8-2.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M966.1 74.7s8.3 2.2 11.1 2.2c2.7 0 11.9-2.2 11.9-2.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M619.7 401.3c2.3-1.8 9.4-7.2 16.5-11.1 8.6-4.8 17.3-9.6 17.3-9.6l-10-10.7s-8.1 4.6-16.2 9.1-16.1 11.3-16.1 11.3 5.2 7.7 7.1 10.6c.3.8.9.6 1.4.4zM677 557.7c4.5-1.1 12.5-4.9 20.8-7.7 9.5-3.2 19.4-5.5 24.1-6.6-2.5-8.8-6.6-19.7-6.6-19.7s-10.8 1.5-22.9 5.2c-7.6 2.3-15.8 6.2-22 9.1 2.1 5.4 4.4 14.5 6.6 19.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M745.2 422.4c2.9 0 5.2 2.4 5.2 5.2s-2.3 5.2-5.2 5.2-5.2-2.3-5.2-5.2 2.3-5.2 5.2-5.2zm-14-37.7c2.7-.2 4.9 1.8 4.9 4.6s-2.4 5.2-5.3 5.3c-2.7 0-4.8-2.1-4.6-4.8.2-2.7 2.4-4.9 5-5.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M596.1 543.7c8.5-5.3 17.9.8 21 12 2.7 10.1-.5 21.3-7.2 26.3-6.9 5.1-16 2.3-20.5-7.4-5.1-10.9-2.2-25.4 6.7-30.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M598.2 553.8c4.1-2.5 8.7.4 10.2 5.9 1.3 4.9-.2 10.3-3.4 12.7-3.3 2.5-7.7 1.1-9.9-3.6-2.6-5.3-1.2-12.3 3.1-15z"
		/>
		<path
			fill="#bfbfbf"
			d="M1234.3 806.3s-20.9 46.4-73.4 23c-10 5.7-26 14.8-43.6 11.8-26.6 23.3-69.8 17.8-82-3.1-17.2 4.7-36.3-.7-46.4-9.8-4.5 12.8-42.4 46.6-75.6-5.5-.3 14 4.1 43.8-35 47.1-9.5 11.4-48.6 38.9-83.2-9.9-24.4 39.1-76.4 18.2-87.6 0-16 1.3-49.4 11.7-63.5-40.5-4.4 2.4-17.3 6.4-31.8 2.2-10.4 6.2-26.7 19-62.4 5.5-1.6 13.9-81.6 41.2-101.8-35-50.7.2-59.1-32.9-60.2-48.2-20.7-6-34.4-27.6-35-50.4-.8-28.9 17.8-59.4 52.6-58 3.8-30.3 11.1-76.8 82.1-70.1 0 11.8-.1 40.9 1.1 54.7 30 .4 34.2 9.3 46 20.8 9.9-25.3 59.6-25.9 69.8 15.7 3.3-3.2 10.8-3.8 15.6-2.6s10.1 5.6 13.4 9.6c47.6 10.6 94.7 16.4 137.7 19.6 14.7-11.2 31.6-5.2 42.7 2.5 8.8-9.6 16.4-19.7 16.4-19.7s-2.7-112.7-7.7-154.4c6.2 40.9 8.4 68.9 10.9 95.3 4.9 14.1 17 47.8 27.5 76.6 19.9 8.9 26.2 24.1 26.2 24.1s4.9-19.7 18.6-27.4c19.3-10.8 50.5-7.7 60.2 21.9 3.4-2.2 9-5.8 15.3-5.5 6.4.4 11.4 2.9 16.4 5.5 20.6-29.3 40.1-28.3 52.6-23 9.6 2.8 16.1 7.2 20.5 12.4 6-12.1 24.4-12.4 24.4-12.4l26.3 5.5s2.3-19.5 25.2-21.9c18.2-1 31.8 15.3 31.8 15.3s2-14.3 9.9-23c8-8.9 21.9-12 21.9-12l7.7-13.1 31.8 6.6 5.5-9.9 8.8 7.7s5.2-14.2 32.8-14.2 50.3 30.6 36.1 62.4c13.1 9.5 17.5 15.6 17.5 30.7s-12.7 28.9-17.5 32.8c.5 8.3-.7 46-46 41.6-6.1 8.3-21 25.8-46 8.8-3.1 2.9-6.6 9.9-6.6 9.9z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001k"
			cx={927.329}
			cy={-1936.8872}
			r={1699.1027}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.466} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={0.582} stopColor="#373737" />
		</radialGradient>
		<path
			fill="url(#ch-full43001k)"
			d="M1050.4 679.3c-12.4-5.3-32-6.3-52.6 23-5-2.6-10.1-5.1-16.4-5.5-6.3-.4-11.9 3.3-15.3 5.5-9.8-29.6-44.2-32.7-63.5-21.9-13.7 7.7-18.6 27.4-18.6 27.4s-3.1-15.3-22.9-24.1c-10.4-28.8-22.5-62.5-27.5-76.6-2.5-26.3-4.7-54.4-10.9-95.3 5 41.6 7.7 154.4 7.7 154.4s-7.7 10.2-16.4 19.7c-11.1-7.7-28-13.7-42.7-2.5-43-3.2-90.1-9-137.7-19.6-3.3-4-8.6-8.5-13.4-9.6-4.9-1.2-12.4-.6-15.6 2.6-10.2-41.7-59.9-41-69.8-15.7-11.8-11.6-14.9-20.4-44.9-20.8-1.2-13.8-2.2-43-2.2-54.7-71.1-6.7-78.3 39.8-82.1 70.1-34.7-1.3-53.4 29.1-52.6 58 .6 22.8 14.3 44.4 35 50.4 1.1 15.3 9.5 48.4 60.2 48.2 20.2 76.3 100.2 48.9 101.8 35 35.7 13.5 52 .7 62.4-5.5 14.4 4.2 27.4.2 31.8-2.2 14.1 52.2 47.5 41.9 63.5 40.5 11.2 18.2 63.2 39.1 87.6 0 34.6 48.7 73.7 21.3 83.2 9.9 39.2-3.3 34.7-33.1 35-47.1 33.1 52.1 71 18.3 75.6 5.5 10 9.2 29.2 14.6 46.4 9.8 12.1 20.9 55.4 26.4 82 3.1 17.6 3 33.6-6.1 43.6-11.8 52.5 23.4 73.4-24.1 73.4-24.1l3.3-5.5 5.5-3.3s15.3 12.7 28.5 5.5c13.1-7.2 14.6-9.9 16.4-13.1 5.6.7 26.1 1.9 36.1-12 10.1-13.9 9.3-25.9 8.8-31.8 4.8-4.7 18.6-15.2 18.6-30.7s-12.4-28.9-18.6-32.8c2.7-8.6 9.9-32.4-8.8-49.3-18.6-16.9-44.7-17.1-61.3 1.1-2.9-2.4-6.6-6.6-6.6-6.6l-8.8 9.9-52.6-10.9-2.2 17.5s-9.2 13.2-71.2 25.2c-48.1 6.4-66.2 8.9-71.2 10.7z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001l"
			cx={853.2341}
			cy={3448.8735}
			r={1005.9026}
			gradientTransform="matrix(1 0 0 -0.3031 0 1666.257)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.418} stopColor="#b3b3b3" />
			<stop offset={0.534} stopColor="#bfbfbf" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001l)"
			d="M1234.3 804.1s-20.9 48.6-73.4 25.2c-10 5.7-26 14.8-43.6 11.8-26.6 23.3-69.8 17.8-82-3.1-17.2 4.7-36.3-.7-46.4-9.8-4.5 12.8-42.4 46.6-75.6-5.5-.3 14 4.1 43.8-35 47.1-9.5 11.4-48.6 38.9-83.2-9.9-24.4 39.1-76.4 18.2-87.6 0-16 1.3-49.4 11.7-63.5-40.5-4.4 2.4-17.3 6.4-31.8 2.2-10.4 6.2-26.7 19-62.4 5.5-1.6 13.9-81.6 41.2-101.8-35-50.7.2-59.1-32.9-60.2-48.2-20.7-6-34.4-27.6-35-50.4-.8-28.9 17.8-59.4 52.6-58 3.8-30.3 11.1-76.8 82.1-70.1 0 11.8 1 40.9 2.2 54.7 30 .4 33.1 9.3 44.9 20.8 9.9-25.3 59.6-25.9 69.8 15.7 3.3-3.2 10.8-3.8 15.6-2.6s10.1 5.6 13.4 9.6c47.6 10.6 94.7 16.4 137.7 19.6 14.7-11.2 31.6-5.2 42.7 2.5 8.8-9.6 16.4-19.7 16.4-19.7s-2.7-112.7-7.7-154.4c6.2 40.9 8.4 68.9 10.9 95.3 4.9 14.1 17 47.8 27.5 76.6 19.9 8.9 22.9 23 22.9 23s6-18.6 19.7-26.3c19.3-10.8 52.6-7.7 62.4 21.9 3.4-2.2 9-5.8 15.3-5.5 6.4.4 11.4 2.9 16.4 5.5 20.6-29.3 40.1-28.3 52.6-23 3.1-3.6 27.4-4.4 27.4-4.4s42.6-5.4 53.7-8.8 52-10.6 63.5-24.1c-.4-7.5 0-16.4 0-16.4l23 4.4 31.8 7.7 6.6-10.9 6.6 6.6s14.5-14.2 31.8-14.2c17.2 0 42.7 17.3 42.7 43.8-1.3 12.9-5.5 17.5-5.5 17.5s18.6 8.5 18.6 31.8c0 23.2-12.7 30.7-16.4 32.8-.4 6.7.4 48.4-46 42.7-7.1 9.2-17.8 16.9-25.2 16.4-7.4-.5-18.1-4.3-20.8-8.8-3.5 3.2-7.7 8.9-7.7 8.9z"
		/>
		<radialGradient
			id="ch-full43001m"
			cx={1168.8514}
			cy={-1113.3832}
			r={82.1233}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001m)"
			d="M1197.1 745s27.1-2.3 31.8-20.8c12.4 5.5 27.4 19.1 27.4 41.6s-21.6 40.5-43.8 40.5-33.6-14.8-35-20.8c21-15.3 19.6-40.5 19.6-40.5z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001n"
			cx={1284.4569}
			cy={-1127.8412}
			r={47.2023}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001n)"
			d="m1291.3 741.7-9.9 6.6s17.9 17.1 7.7 40.5c13.5.8 28.8-2.2 36.1-13.1 7.4-10.9 8.7-25.7 7.7-30.7-21.8 13.9-41.6-3.3-41.6-3.3z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001o"
			cx={1232.451}
			cy={-1118.5165}
			r={111.6877}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001o)"
			d="M1256.2 768c-1.3 5.1-3 18.7-14.2 28.5 5.8 5.2 14.2 7.7 20.8 7.7s29.6-9.3 29.6-30.7c0-18-13.1-25.2-13.1-25.2s-10.1 18.1-23.1 19.7z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001p"
			cx={1208.7284}
			cy={-1097.9067}
			r={102.9279}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001p)"
			d="M1173 788.8c.4 6.9 0 25.3-13.1 41.6 11.6 5.3 19.6 5.5 27.4 5.5s35.3-.8 47.1-35c-18.3 5.5-17 5.5-25.2 5.5s-28.4-9.4-31.8-21.9c-1.9.9-3.7 3-4.4 4.3z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001q"
			cx={807.4115}
			cy={-2035.0344}
			r={1699.1027}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#000" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001q)"
			d="M913.5 822.7c-.3 14 4.1 43.8-35 47.1-9.5 11.4-48.6 38.9-83.2-9.9-24.4 39.1-76.4 18.2-87.6 0-16 1.3-49.4 11.7-63.5-40.5-4.4 2.4 84-139.3 127-136.1 14.7-11.2 31.6-5.2 42.7 2.5.4-4.4 12-18.6 12-18.6s.4-2 1.1-3.3c2.1-43.8-.9-122.7-4.4-152.2 6.2 40.9 8.4 68.9 10.9 95.3 4.9 14.1 17 47.8 27.5 76.6 19.9 8.9 26.2 24.1 26.2 24.1s59.4 167.1 26.3 115z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full43001r"
			x1={897.6602}
			x2={871.9513}
			y1={-1238.036}
			y2={-1728.5859}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.49} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full43001r)"
			d="M848.5 681.1c-17-37.3-37.2-81.7-52.1-115.2-16.6-163.9-43.5-313.1-99.6-341 1.4-2.7 46-3.3 46-3.3s170.2-4.2 219-5.5c31.7 18.2 51.4 50.6 69 85.4-19.8 1.7-56.9 8.8-56.9 8.8l-1.1 2.2-49.3-1.1H795.2l-2.2 8.8s18.9 23.6 53.7 69c19.5 25.5 38.1 47.8 58 74.5 2.4 8.9 18.6 59.1 18.6 59.1l108.4 32.8 13.1-6.6 28.5 8.8-26.2 118.2s-26.7-9.2-49.3 26.3c-13.2-5.2-21.9-7.3-31.8 1.1-5.7-11.3-12-30.1-40.5-28.5s-38.1 18.9-40.5 31.8c-8.1-8.6-18.2-27.5-36.5-25.6z"
		/>
		<path d="m923.4 298.2-29.6-73.4h-14.2v7.7h10.9l25.2 67.9 7.7-2.2z" opacity={0.302} />
		<linearGradient
			id="ch-full43001s"
			x1={944.9771}
			x2={568.3048}
			y1={-1270.9945}
			y2={-1753.113}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.322} stopColor="#000" />
			<stop offset={0.555} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full43001s)"
			d="M848.5 681.1c-17-37.3-37.2-81.7-52.1-115.2-16.6-163.9-43.5-313.1-99.6-341 55.7-14.2 98.5 86.5 98.5 86.5l-2.2 8.8s18.9 23.6 53.7 69c19.5 25.5 38.1 47.8 58 74.5 2.4 8.9 18.6 59.1 18.6 59.1l108.4 32.8 13.1-6.6 28.5 8.8-26.3 118.2s-26.7-9.2-49.3 26.3c-13.2-5.2-21.9-7.3-31.8 1.1-5.7-11.3-12-30.1-40.5-28.5s-38.1 18.9-40.5 31.8c-8.1-8.6-18.2-27.5-36.5-25.6z"
			opacity={0.2}
		/>
		<path
			fill="#ccc"
			d="M717.6 290.1c10.9 18.1 23.9 46.9 37.1 94.6 13.8 50 17.9 83.7 27.4 173 5.1 2.9 48.5 122.4 53.7 123.7 4-.1 9.6-.7 12-2.2-14.7-33.1-45-100-52.6-116.1-5.2-58.8-13.3-116.6-20.8-157.7-9.8-53.5-26.2-105.6-44.6-141.1-4.1 12.7-8 22.9-8 22.9s-2.8.3-4.2 2.9z"
		/>
		<path
			fill="#c4c4c4"
			d="m729.5 263.2-7.7 23s-118.9 14-187.2 152.2c-6.1-2.4-13.1-5.5-13.1-5.5s30.9-77.8 89.8-120.4c6.1-4.4 28.4-20.1 54.7-30.7 29.8-11.9 63.5-18.6 63.5-18.6z"
		/>
		<radialGradient
			id="ch-full43001t"
			cx={922.9666}
			cy={-1149.8887}
			r={98.5808}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001t)"
			d="M862 761.4c8.8-4 21.1-15.5 23-29.6 6 8 27.1 33.3 52.6 21.9 7.9 46.8-63.2 65.9-75.6 7.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001u"
			cx={849.97}
			cy={-1140.3339}
			r={98.5808}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001u)"
			d="M862 761.4c8.8-4 21.1-15.5 23-29.6 6 8 27.1 33.3 52.6 21.9 7.9 46.8-63.2 65.9-75.6 7.7z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001v"
			cx={870.2522}
			cy={-1094.2266}
			r={108.5867}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001v)"
			d="M911.3 797.5c2.6 12.1 3.5 45.8-2.2 56.9-5.7 11.1-27.1 13.5-31.8 15.3 17-17.8 11-50.3 10.9-59.1 5-6.2 7.9-9.5 8.8-13.1 5.1-.2 8.7 0 14.3 0z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full43001w"
			cx={1013.7523}
			cy={-1114.5001}
			r={111.4829}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001w)"
			d="M995.6 793.2c14.1 4.2 39.4 4.6 52.6-16.4 2.1 16 .7 22.2 16.4 35-1.8 6.4-29.9 50.2-77.7 15.3 3-8 5.1-17.2 1.1-26.3 2-1.5 4.6-3.3 7.6-7.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001x"
			cx={1088.9904}
			cy={-1090.6328}
			r={111.4829}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001x)"
			d="M995.6 793.2c14.1 4.2 39.4 4.6 52.6-16.4 2.1 16 .7 22.2 16.4 35-1.8 6.4-29.9 50.2-77.7 15.3 3-8 5.1-17.2 1.1-26.3 2-1.5 4.6-3.3 7.6-7.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001y"
			cx={967.9768}
			cy={-1099.4573}
			r={111.4829}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001y)"
			d="M995.6 793.2c14.1 4.2 39.4 4.6 52.6-16.4 2.1 16 .7 22.2 16.4 35-1.8 6.4-29.9 50.2-77.7 15.3 3-8 5.1-17.2 1.1-26.3 2-1.5 4.6-3.3 7.6-7.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001z"
			cx={1083.7389}
			cy={-1078.5575}
			r={79.41}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001z)"
			d="M1085.4 818.3c2.8 6.1 13.5 17.1 31.8 23-8.1 7.2-44 32.3-82.1-1.1 9-5.3 24.4-14.1 29.6-28.5 5.8 2.6 10.9 4.6 20.7 6.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001A"
			cx={1031.3672}
			cy={-1063.5902}
			r={90.755}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001A)"
			d="M1085.4 818.3c2.8 6.1 13.5 17.1 31.8 23-8.1 7.2-44 32.3-82.1-1.1 9-5.3 24.4-14.1 29.6-28.5 5.8 2.6 10.9 4.6 20.7 6.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001B"
			cx={1021.5308}
			cy={-1147.6874}
			r={139.6359}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.5417} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001B)"
			d="M1072.3 690.2s16-18.7 36.1-9.9c20.2 8.9 21.9 75.6 21.9 75.6s-14.1-35.1-55.8-20.8c10-16.7-2.2-44.9-2.2-44.9z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001C"
			cx={971.3887}
			cy={-1140.6635}
			r={99.6429}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001C)"
			d="M971.5 766.9c2.5 7.1 11.7 20.4 23 27.4-8.3 7.2-20.9 14.2-32.8 14.2s-26.3-9.7-28.5-27.4c3.5-13.7 4.4-16.2 5.5-27.4 1.4-.8 4.4-2.2 4.4-2.2s12.9 13.3 28.4 15.4z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001D"
			cx={1008.238}
			cy={-1110.4114}
			r={99.6429}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001D)"
			d="M971.5 766.9c2.5 7.1 11.7 20.4 23 27.4-8.3 7.2-20.9 14.2-32.8 14.2s-26.3-9.7-28.5-27.4c3.5-13.7 4.4-16.2 5.5-27.4 1.4-.8 4.4-2.2 4.4-2.2s12.9 13.3 28.4 15.4z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001E"
			cx={902.8902}
			cy={-1112.1184}
			r={103.4754}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001E)"
			d="M862 761.4c.2 7.7 4.2 29.6 33.9 36.1-1.8 10.7-10.5 23-33.9 23s-34.8-15.4-38.3-19.7c2.3-11 4.5-17.3 2.2-30.7 2.8-4.3 4.2-7.6 5.5-8.8 2.4 1.2 17.5 3.3 30.6.1z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001F"
			cx={847.7242}
			cy={-1141.7343}
			r={103.4754}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001F)"
			d="M862 761.4c.2 7.7 4.2 29.6 33.9 36.1-1.8 10.7-10.5 23-33.9 23s-34.8-15.4-38.3-19.7c2.3-11 4.5-17.3 2.2-30.7 2.8-4.3 4.2-7.6 5.5-8.8 2.4 1.2 17.5 3.3 30.6.1z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001G"
			cx={949.4837}
			cy={-1131.1031}
			r={142.0131}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001G)"
			d="M1008.8 715.4c14.1.1 43.7 1.4 44.9 41.6-5.4 18.6-5.7 20.6-6.6 23s-19.5 23.4-51.5 14.2c-13.4-7.6-20.6-16.1-21.9-26.3 8.1 0 48.1-8 35.1-52.5z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001H"
			cx={795.1116}
			cy={-1140.5813}
			r={141.7459}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001H)"
			d="M824.8 772.4c3 7.5 5.1 34.1-21.9 50.4s-70.3 9.1-81-46c7.9-3.8 20.2-7.8 27.4-27.4.2 10.8 27.3 49.6 75.5 23z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001I"
			cx={700.5614}
			cy={-1139.6976}
			r={141.7459}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001I)"
			d="M824.8 772.4c3 7.5 5.1 34.1-21.9 50.4s-70.3 9.1-81-46c7.9-3.8 20.2-7.8 27.4-27.4.2 10.8 27.3 49.6 75.5 23z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001J"
			cx={704.4223}
			cy={-1128.1205}
			r={103.1118}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001J)"
			d="M721.9 777.8c-7.6 2.1-25.8 6-37.2-2.2-5.5 9.9-9 33.4 15.3 52.6 21 14.4 46.3.5 48.2-4.4-6.6-6.9-25.8-29.1-26.3-46z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001K"
			cx={761.7542}
			cy={-1086.7369}
			r={103.1118}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001K)"
			d="M721.9 777.8c-7.6 2.1-25.8 6-37.2-2.2-5.5 9.9-9 33.4 15.3 52.6 21 14.4 46.3.5 48.2-4.4-6.6-6.9-25.8-29.1-26.3-46z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001L"
			cx={863.5433}
			cy={-1106.8625}
			r={151.3806}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001L)"
			d="M888.3 812.9c-9.8 5.8-41.9 17.1-64.6-12-8.8 11.5-8.4 15.5-30.7 27.4-.7 15.5-1.6 59.1 51.5 59.1 51.8-5.6 47.5-67.8 43.8-74.5z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001M"
			cx={775.8499}
			cy={-1095.9358}
			r={151.3806}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001M)"
			d="M888.3 812.9c-9.8 5.8-41.9 17.1-64.6-12-8.8 11.5-8.4 15.5-30.7 27.4-.7 15.5-1.6 59.1 51.5 59.1 51.8-5.6 47.5-67.8 43.8-74.5z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001N"
			cx={964.579}
			cy={-1092.0137}
			r={87.0189}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001N)"
			d="M988 800.8c3.6 9 8.9 42.2-30.7 49.3-39.5 7-47.6-44.9-47.1-52.6 6-1.7 20-10.5 23-16.4 2.6 10.9 7.3 39.7 54.8 19.7z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001O"
			cx={903.9469}
			cy={-1100.0464}
			r={87.0189}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001O)"
			d="M988 800.8c3.6 9 8.9 42.2-30.7 49.3-39.5 7-47.6-44.9-47.1-52.6 6-1.7 20-10.5 23-16.4 2.6 10.9 7.3 39.7 54.8 19.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001P"
			cx={776.2035}
			cy={-1075.786}
			r={102.18}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001P)"
			d="M792 827.1c.6 8.3 1 25.4 4.4 31.8-7.8 11-24.1 25.9-44.9 23-20.8-2.9-45.6-3.6-52.6-54.7 18.1 7.8 27.8 13.5 50.4-3.3 12 7.4 30.6 10.2 42.7 3.2z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001Q"
			cx={725.3703}
			cy={-1071.819}
			r={102.18}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001Q)"
			d="M792 827.1c.6 8.3 1 25.4 4.4 31.8-7.8 11-24.1 25.9-44.9 23-20.8-2.9-45.6-3.6-52.6-54.7 18.1 7.8 27.8 13.5 50.4-3.3 12 7.4 30.6 10.2 42.7 3.2z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001R"
			cx={790.1777}
			cy={-1162.678}
			r={95.2499}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001R)"
			d="M822.6 708.8c7.5 7 13.1 16.2 13.1 30.7s-6.8 35.3-25.2 40.5c-18.4 5.3-57.3.1-61.3-36.1.7-18.8 1.5-32.6 10.9-38.3-.7 13.7-1.4 35 31.8 35 29.8-2.4 28.5-29.2 30.7-31.8z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001S"
			cx={1154.816}
			cy={-1109.5332}
			r={85.4082}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001S)"
			d="M1133.6 784.4c-2.6 11.2-10.1 35.9-47.1 33.9 7.6 16 26.8 23 42.7 23s44.2-11.8 43.8-52.6c-15.9 4.5-29.4 5.5-39.4-4.3z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001T"
			cx={1099.5778}
			cy={-1081.8698}
			r={85.4082}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001T)"
			d="M1133.6 784.4c-2.6 11.2-10.1 35.9-47.1 33.9 7.6 16 26.8 23 42.7 23s44.2-11.8 43.8-52.6c-15.9 4.5-29.4 5.5-39.4-4.3z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001U"
			cx={530.7272}
			cy={-1155.7927}
			r={105.3916}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001U)"
			d="M527 746.1c-2.6 5.6-10.9 29.9-1.1 55.8-15.8-5.2-34.3-17.3-36.1-44.9 7.3-2.8 17.3-10.1 23-15.3 7.4 2.5 8.2 2.3 14.2 4.4z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001V"
			cx={512.0096}
			cy={-1238.4055}
			r={145.8105}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" stopOpacity={0} />
			<stop offset={0.539} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full43001V)"
			d="M534.6 639.9c-9-9.1-20.2-21-47.1-19.7-26.8 1.3-37.7 30.2-35 47.1s11.5 36.5 35 36.1c7.2-15.4 11.9-31.1 43.8-29.6-2.4-16.3.6-22.8 3.3-33.9z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001W"
			cx={567.1669}
			cy={-1211.9574}
			r={113.0969}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001W)"
			d="M636.5 676c5.4 2.5 16.1 12.3 15.3 31.8-.8 19.4-14.2 33.5-33.9 32.8s-35.9-8.9-36.1-42.7c8.6-4.9 12-7.3 14.2-10.9 0 7.9 33.2 22.2 40.5-11z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full43001X"
			cx={608.0193}
			cy={-1199.1461}
			r={80.7831}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001X)"
			d="M636.5 676c5.4 2.5 16.1 12.3 15.3 31.8-.8 19.4-14.2 33.5-33.9 32.8s-35.9-8.9-36.1-42.7c8.6-4.9 12-7.3 14.2-10.9 0 7.9 33.2 22.2 40.5-11z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001Y"
			cx={707.7171}
			cy={-1128.7501}
			r={156.0376}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001Y)"
			d="M700 827.1c-10.3-8.5-29-31-15.3-50.4-2.1-1.5-5.5-4.4-5.5-4.4s-2.1 10.1-18.6 16.4c-.9 6.7.5 18.1-15.3 30.7 2.1 13.4 9.4 52.6 62.4 39.4-5.7-10.5-7.2-18.7-7.7-31.7z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full43001Z"
			cx={911.9019}
			cy={-1140.877}
			r={122.7229}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001Z)"
			d="M965 703.4c9.5-6 32.5-12.3 44.9 14.2 12.4 26.6-22.5 75.9-67.9 35 15.9-11.1 28.7-25 23-49.2z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001aa"
			cx={568.4075}
			cy={-1195.1167}
			r={89.1005}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001aa)"
			d="M530.3 673.8c-17.3.2-50.4 8.9-40.5 43.8 9.8 34.9 56.4 33.8 67.9 12s7.6-24.8 7.7-27.4c-8.4 1.2-27.3 0-35.1-28.4z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001ab"
			cx={525.4012}
			cy={-1164.3094}
			r={113.6039}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ab)"
			d="M539 742.8c.2 14.7 4.4 44 46 43.8 3.4 11.3 8.8 29.5 27.4 33.9-4.3 5.9-15.1 13.1-35 13.1s-43.1-8.2-52.6-35 .6-52.6 3.3-54.7c4-.1 5.8-.2 10.9-1.1z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001ac"
			cx={579.0769}
			cy={-1128.2826}
			r={113.6039}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ac)"
			d="M539 742.8c.2 14.7 4.4 44 46 43.8 3.4 11.3 8.8 29.5 27.4 33.9-4.3 5.9-15.1 13.1-35 13.1s-43.1-8.2-52.6-35 .6-52.6 3.3-54.7c4-.1 5.8-.2 10.9-1.1z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001ad"
			cx={631.3654}
			cy={-1087.0315}
			r={113.6039}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ad)"
			d="M539 742.8c.2 14.7 4.4 44 46 43.8 3.4 11.3 8.8 29.5 27.4 33.9-4.3 5.9-15.1 13.1-35 13.1s-43.1-8.2-52.6-35 .6-52.6 3.3-54.7c4-.1 5.8-.2 10.9-1.1z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001ae"
			cx={655.2556}
			cy={-1156.4902}
			r={95.5751}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ae)"
			d="M666 748.3c.9 6.7 4.8 19.4 13.1 25.2-10.3 19.4-46.3 26.7-72.3 0 4.8-5 12-17.8 12-32.8 6.3.1 12.3-2.4 15.3-4.4 3.3 5.9 16.4 13.4 31.9 12z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001af"
			cx={643.8602}
			cy={-1118.2986}
			r={100.7379}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001af)"
			d="M608 773.4c10 9.3 22 21.1 52.6 15.3-.7 10.6-7.2 35-33.9 35s-43.3-25.5-41.6-38.3c5.5-1.4 14.5-2.9 22.9-12z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001ag"
			cx={577.0151}
			cy={-1120.6417}
			r={100.7379}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ag)"
			d="M608 773.4c10 9.3 22 21.1 52.6 15.3-.7 10.6-7.2 35-33.9 35s-43.3-25.5-41.6-38.3c5.5-1.4 14.5-2.9 22.9-12z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001ah"
			cx={617.4702}
			cy={-1151.6307}
			r={169.0995}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ah)"
			d="M690.1 699c16.9-4.6 45.4-10.3 58 27.4 5.9 40.8-23.8 51.4-33.9 52.6-10.2 1.1-40.3 8.5-48.2-30.7 13.5-4.3 33.1-10.7 24.1-49.3z"
			opacity={0.451}
		/>
		<radialGradient
			id="ch-full43001ai"
			cx={580.9384}
			cy={-1141.6445}
			r={252.9398}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.45} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ai)"
			d="M552.2 828.2c-12.9-4.8-19.9-12.8-26.3-26.3-10.5-4.6-28.6-9.7-36.1-41.6-3.9 11.5-12.2 27.1-39.4 31.8.8 14.4 9.2 46.7 60.2 52.6 19.3-.9 26.3-5 41.6-16.5z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001aj"
			cx={568.2881}
			cy={-1226.0597}
			r={155.3181}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.67} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001aj)"
			d="M540.1 742.8c11.9-7 22.7-16.7 25.2-39.4 5.9-1.1 11.6-2.8 16.4-5.5.3 9.4 2.1 45.1 38.3 42.7.3 14.2-10.1 47.7-42.7 46-32.6-1.7-41.3-31.3-37.2-43.8z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001ak"
			cx={628.8524}
			cy={-1172.3729}
			r={110.942}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ak)"
			d="M540.1 742.8c11.9-7 22.7-16.7 25.2-39.4 5.9-1.1 11.6-2.8 16.4-5.5.3 9.4 2.1 45.1 38.3 42.7.3 14.2-10.1 47.7-42.7 46-32.6-1.7-41.3-31.3-37.2-43.8z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001al"
			cx={518.4809}
			cy={-1162.5991}
			r={110.942}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001al)"
			d="M540.1 742.8c11.9-7 22.7-16.7 25.2-39.4 5.9-1.1 11.6-2.8 16.4-5.5.3 9.4 2.1 45.1 38.3 42.7.3 14.2-10.1 47.7-42.7 46-32.6-1.7-41.3-31.3-37.2-43.8z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43001am"
			cx={616.9916}
			cy={-1152.1608}
			r={80.5246}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001am)"
			d="M649.6 691.3c8.3-6.2 24.4-16.2 40.5 9.9 5.1 12.1 5.2 25.2-3.3 35-11.6 13.5-34.5 19.2-51.5 0 8.6-5.4 21.7-15.2 14.3-44.9z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43001an"
			cx={476.0615}
			cy={-1156.1344}
			r={126.0396}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.501} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001an)"
			d="M427.3 705.6c-5.1-4.1-10.6-11.1-13.1-15.3-6.9 3.2-26.3 12.5-26.3 53.7s40.1 47.1 48.2 47.1 43.4 3.6 54.7-32.8c-16.7 3.3-53.3 11.6-63.5-52.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001ao"
			cx={531.7313}
			cy={-1168.5481}
			r={115.288}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ao)"
			d="M511.6 743.9c-9.2 7.8-18.8 16.4-42.7 16.4-22.1 5.1-72-61.7-17.5-92 .6 10.7 10 36.8 37.2 35-.8 10.9-2.6 21.1 23 40.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43001ap"
			cx={496.8683}
			cy={-1204.7875}
			r={115.288}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43001ap)"
			d="M511.6 743.9c-9.2 7.8-18.8 16.4-42.7 16.4-22.1 5.1-72-61.7-17.5-92 .6 10.7 10 36.8 37.2 35-.8 10.9-2.6 21.1 23 40.6z"
			opacity={0.302}
		/>
		<path
			d="m520.4 435.1-33.9-31.8s86.7-169.8 210.2-179.6c9 4.8 29.8 22.4 33.9 38.3-26 4.6-94.8 22.2-141.3 70.1-46.3 47.9-68.9 103-68.9 103zm473-194.9s52.6 26.2 85.4 66.8c-22-7.4-31.8-9.9-31.8-9.9l-25.2-15.3-28.4-41.6z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-full43001aq"
			x1={505.4551}
			x2={670.5657}
			y1={-1411.1533}
			y2={-1622.484}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full43001aq)"
			d="m520.4 435.1-33.9-31.8s86.7-169.8 210.2-179.6c9 4.8 29.8 22.4 33.9 38.3-26 4.6-94.8 22.2-141.3 70.1-46.3 47.9-68.9 103-68.9 103z"
			opacity={0.102}
		/>
		<path
			d="m915.7 250 28.5-1.1s24.2 32.8 26.3 63.5c-17-.5-30.7-1.1-30.7-1.1L915.7 250zm-90.9-30.6s12.3 12.9 18.4 31.5c.5 1.5-38.8-.1-38.1 1.4 10.7 23.6 19.9 46.4 21.9 58 11.9.1 39.4 0 39.4 0l6.6-94.2 26.3-2.2s-17.3-14.4-53.7-15.3c-36.3-1-45.9 11.6-51.5 21.9 17-.4 30.7-1.1 30.7-1.1zm79.9 127 75.6-15.3-62.4-19.7s-121.6.4-120.4 0c1.1-.4 107.2 35 107.2 35zm75.6-37.2 71.2-10.9 111.7 29.6-116.1-1.1-66.8-17.6zm39.4 35 161 58-66.8 7.7s-158.8-54.4-158.8-53.7c.1.8 64.6-12 64.6-12zm118.3 13.2 115-15.3 128.1 49.3-67.9 3.3-175.2-37.3z"
			className="factionColorPrimary"
		/>
		<path
			d="M827 310.3c-1.9-8.1-18.6-54.3-23-56.9 10.6.1 40.5-1.1 40.5-1.1s18.5 47 20.8 59.1c-15.6.1-27.9 2.3-38.3-1.1zm88.7-60.3 25.2 61.3 29.6 1.1s-7.1-43.9-28.5-63.5c-13.3-.5-26.3 1.1-26.3 1.1z"
			opacity={0.102}
		/>
		<path
			d="M865.3 308.1 873 215s-22.4-15.1-38.3-15.3-36.8 11.3-39.4 19.7c15.3 0 26 1.8 26.3 1.1.1-.4 12.1 8.8 23 30.7 10.9 22.1 20.7 56.9 20.7 56.9z"
			opacity={0.102}
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M681.6 226.6c3.9-2.8 8.6-4.2 14.2-1.9 77.7 31.8 100.7 340.5 100.7 340.5L849 681.3m-12 2.2-55.8-124.8s-13.1-188.5-64.3-270.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m781.2 559.8 15.3 6.6 65.7-5.5 28.5 50.4 109.5-6.6s11.7-39.9 15.3-51.5m59.1 4.4L1047.2 678"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m857.8 662.7-35-77.7 32.8-3.3s19.8 36.4 32.1 59.1c-.6 1.4-8 18.6-9.2 19.7-10.2 1-20.7 2.2-20.7 2.2zm167.5-90.9s-9.5 33.8-16 56.8c1 1.4 11 17.6 12 19.3 9.1-.8 18.3-1.7 18.3-1.7l16.4-76.6-30.7 2.2z"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M687 222.5s67.1-1.9 135-3.8m79.7-2.2c35-1 60.1-1.7 60.1-1.7s40.3 18.1 69 85.4"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1250.9 341.9 132.5 49.3-28.5 49.3-20.8 10.9-180.7 13.1 116.1 99.6 8.8 25.2-27.4 48.2-145.6-30.7-28.5-48.2-29.6-8.8-12 7.7-111.7-35-16.4-50.4-1.1-7.7-112.8-145.5 2.2-7.7h124.8l53.7 1.1m197.1 37.2-1.1-19.7-117.2-30.7-77.7 11v13.1l72.3 20.8 121.5 3.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m908.2 471.1 86.5 26.3 40.5 59.1m10.9-7.7-41.6-56.9-98.5-29.6m88.7 36.2 8.8-5.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m919.1 311.2 62.4 19.7-76.6 14.2 33.9 73.4s-.2 1-.5.9c-6.6-2.4-73.9-26.8-93.7-34"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m937.7 419.6 10.9-6.6-21.9-52.6 30.7-6.6m28.5-5.3V332m60.2-5.4V343"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m903.8 344.1-106.2-32.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m980.4 309 67.9 16.4 113.9 2.2" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1270.6 567.4-19.7 35-20.8-32.8-189.4-98.5 14.2 53.7 50.4 79.9m145.6 31.7-28.5-43.8-165.3-69"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1222.4 591.5 8.8-19.7" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1006.7 443.7 135.8 12 24.1 4.4-17.5-32.8 52.6-5.5-15.3-17.5-166.4-59.1-68 10.9 52.6 86.5 35 29.6m142.3-69-69 7.7 28.5 43.8m8.8 4.3L1045 473.3m-90.8-116.1c-.1.1 159.5 52.1 161 52.6"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m929 362.7 33.9 10.9" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1333 451.4 18.6-39.4-50.4 3.3-200.4-47.1 26.3-10.9 127-16.4"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1382.3 391.2-67.9 3.3-180.7-37.2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1313.3 396.6-12 17.5" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M607.1 259.8c-21.5-7.8-62.4-21.9-62.4-21.9s4.5 43.1 7.7 71"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m543.5 236.8 109.5-3.3" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m579.7 235.7 37.2 7.7s9.7-.5 16.5-.8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M564.9 256.6c3.3 0 5.9 5.3 5.9 11.9s-2.7 11.9-5.9 11.9-5.9-5.3-5.9-11.9 2.6-11.9 5.9-11.9z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1195.2 624.8c0 6.2-.4 14.7-1.1 21.1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1234.5 800.7c-19.3 49.4-67.9 32.7-75.6 28.5-2.8 2.6-6.8 5.3-15.3 8.8-26.3 9.6-53.5-6.5-59.1-20.8-28.3-2.6-38.8-26.2-37.2-41.6-15.2 27.4-46.7 19.4-51.5 17.5-2.9 2.6-5.5 5.2-8.8 6.6 8.8 17.6 0 37.6-15.3 46-19.2 10.5-48.1 4.2-60.2-30.7.3 3.3 3.6 21.2-1.1 33.9-7.2 15.9-16.4 16.1-33.9 20.8-29 35.9-74.6 8.9-81-12-22.5 39.4-75.9 23.6-87.6 1.1-53 13.2-61-28.6-63.5-40.5-6.2 4.9-20.6 6.3-30.7 2.2-19.5 17-47.8 12.9-62.4 6.6-37.4 38.4-102.5 3.4-101.8-36.1-40 2.5-57.5-17.4-61.3-47.1-68.2-28.5-26.3-116.3 17.5-108.4-1.6-66.6 61.9-76.5 82.2-72.5m-27.6 72.3c-35.9-10.5-72.9 39.5-31.8 71.2m-15.3-16.4c-10.3 3.6-28.9 22.5-25.2 55.8m42.8-104c-5.2-2.5-12.4-5-25.2-6.6m46 32.9c-54.8 18.6-9.3 136.7 60.2 74.5m-62.4 48.1c28.7-2.4 37.3-21 41.6-32.8m60.2 69c-37.1-13.2-32.9-66.4-24.1-81m13.2-2.3c-12.9 43.9 75.2 66.6 78.8-2.2m60.2 31.8c-24.1 32-63.4 10.9-70.1 1.1m-25.1 12c2.3 10.9 9.1 29.3 29.6 33.9m30.6-2.1c8.4-5.4 16.6-17.2 15.3-28.5m24.1-12c-18.1 24.2 25.4 79.5 64.6 46m-27.4-46c24.1 93.9 118.9 43.9 104-5.5m-1 28.5c5.7 14 56.5 36.4 72.3-2.2m14.2 0c-.1 4.2.7 14.7 2.2 18.6m-25.2-5.5c2 11.3 5.7 38.4-12 59.1m-13.2-108.3c-2.3 35.9 75.2 58.5 75.6-7.7m-145.7 73.4c.3 14.6.7 23 4.4 30.7m-37.2-154.5c-4 19.5 4.3 35.2 31.8 35 26.6-.2 33.7-25.9 27.4-46M748.3 747c5.3 34.3 55.4 43.8 75.7 24 10.9-10.6 20.9-46.4-3.5-63.4m9.9 53.7c13.3 3.4 49.9 6.2 53.7-27.4.8-.1 0-1.1 0-1.1s2.2-13.7 0-24.1M699 828.1c.2 8 2.5 21.9 8.8 30.7M690.3 701c8.7 29.7-7.1 43.6-24.1 46-13.7 1.9-28.2-4.4-30.7-9.9m30.7 11c2.8 49.4 90 39.7 82.1-16.4M526 801.8c-13.3-6.2-32.3-12.7-36.1-42.7m-1.1-58.1c-5.9 11.5 4.7 31 20.3 39.5 18.1 9.9 51.9 3.5 56.3-37.3m84.4-10.9c16.4 58.5-72.8 67.7-67.9 6.6m14.2-9.9c3.2 7.9 37.9 15.5 39.4-13.1M534.8 643c-6.1-11.5-18.8-22.7-38.3-23-11.5-.2-27.1 4.3-36.5 15.5-6.6 7.7-10.1 19.8-7.3 38.2 5.9 27.6 30.3 30.6 35 29.6 3-16 16.8-33.6 42.7-28.5-4.3-18.4 4.1-64.1 48.2-49.3 20.3 6.8 24 24 25.2 33.9.5-1.2-.1-3.5 1.1-4.4 7.8-5.7 29.7-2.2 30.7 18.6 3.7 4 9.7 6.6 14.2 17.5 3.2-7.6 25.3-14.9 39.4 7.7 17.7-9.6 54.2-6.7 59.1 32.2 0 .2.5.9 1.2.7 1.1-15.7 4.9-23.3 9.9-27.4 3.3-10.6 10.2-34.4 40.5-26.3 7.8 1.9 17.2 9.9 18.6 15.3 6.5-6.7 45.4-31.9 65.7 16.4 1.4-13.6 10.8-41 52.6-35 21.8 1.6 27.3 25.6 28.5 28.5 2.8-5.1 18.5-10.7 32.1-1.1 6.6-10.9 28.8-42.6 67.6-16.4 23 18 10.2 43.3 7.7 49.3 16.6-5.4 34.4-7.9 50.4 9.9 5.9 8.2 6.9 10.6 7.7 14.2.3.2.7 1.2 1.1 2.2m-79.2-1.1c.7-11.4-.9-48.6-45.2-45.3m-9.5-12.8c8.9 5.4 16.6 22.6 12 37.2-6.9 22.4-30.4 41.4-66.8 14.2m28.5 14.3c1.7 6 11.2 20.5 25.2 26.3m-9.9 5.5c-10.5 8.9-48.3 16.6-54.7-18.6m56.9 47.1c26 18.8 57.9 15.7 74.5-15.3m-28.5 27.3c10.9 13.4 51.7 27.2 82.1 1.1M965.1 704.3c10.2 33.9-40.3 81.3-79.9 27.4m188.3 2.2c-8.2 7.6-20.4 9.4-26.3 44.9m86.5 6.5c28.5 15.5 50.7 2 61.3-27.4 4.5-12.5.3-36.9-29.6-44.9-34.8-9.3-51.8 5.8-59.1 18.6m25.2 29.7c1.9 11 9.3 57.8-46 55.8m87.7-28.5c1.5 24.1-12 37.8-15.3 41.6"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1071.3 691.2c11.3-17.6 55.8-20.9 65.7 19.7" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1120.2 684.4c4.4-12.6 16.4-21.7 30.5-21.7 17.8 0 32.3 14.5 32.3 32.3 0 8.2-3 15.6-8 21.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1180.4 683.3c5.5-4 12.3-6.3 19.6-6.3 18.4 0 33.4 15 33.4 33.4s-15 33.4-33.4 33.4c-.9 0-1.8 0-2.7-.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1229.3 724.8c15.9 6.2 27.1 21.7 27.1 39.8 0 23.6-19.1 42.7-42.7 42.7-15.8 0-29.5-8.5-36.9-21.2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1177.5 679.9c.5-20.4 17.2-36.9 37.8-36.9 20.9 0 37.8 16.9 37.8 37.8 0 14.6-8.2 27.2-20.3 33.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1250.1 697.8c18.2 1.3 32.6 16.4 32.6 35 0 16.6-11.6 30.5-27.1 34.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1238.2 651.5c6.3-2.6 13.1-4.1 20.3-4.1 29 0 52.6 23.5 52.6 52.6 0 21.9-13.4 40.7-32.5 48.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1209.6 644.1c1.7-5.5 4.9-10.3 9.1-14m38.2-2.5c7.3 5 12.4 13 13.5 22.2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1279.5 748.2c7.9 5.6 13 14.7 13 25.1 0 16.9-13.7 30.7-30.7 30.7-8.1 0-15.5-3.2-21-8.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1262.2 632.6c7.8-8.4 19-13.7 31.4-13.7 23.6 0 42.7 19.1 42.7 42.7 0 6.7-1.5 13-4.3 18.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1333.4 744.3c.5 2.5.8 5 .8 7.6 0 20.9-16.9 37.8-37.8 37.8-3.2 0-6.3-.4-9.2-1.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1304.4 675.8c2.8-.7 5.8-1.1 8.9-1.1 20.6 0 37.2 16.7 37.2 37.2 0 20.6-16.7 37.2-37.2 37.2-8.6 0-16.5-2.9-22.8-7.8"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M530.4 674.8c7.9 45.8 77.4 31.2 73.4-14.2" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m894 226-2.9 5.8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m866.1 311 6.7-96.6h28l38.4 95m-66.5-95s-17.8-8.3-23.4-10.9c-1.1-.5-34.2-13.1-55.1 15.7"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m901 213.8-21.5-10.2s-24.6-8.3-49.9-3.5" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M863.2 308.1s-15.2-70.3-42.6-89" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M827.1 311.2c-4.4-19.8-13.6-43.8-24.1-58 9.7-.3 25.2-.7 42.4-1.3m70.7-2.2c16.1-.5 27.1-.9 27.1-.9s21.3 23.3 27.4 62.4"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m880.3 224.4-4.2 76.6 46.8-1.7-28.4-75h-14.2v.1z" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M880.3 232.1h8.9l24.2 67.2" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M986.8 74.5h2.4V35.1s-5.7-3.3-11.5-3.3c-6.2 0-12.6 3.3-12.6 3.3v39.4h2.2m4.4 2c-.1 21.7-1.1 144.9-1.1 144.9M525.5 435.7c-9.9 21.3-17.8 45.1-21.4 70.4-4.2-3.1-8.8-6.6-8.8-6.6s-9 62.1-2.8 120.8m41.2 9.6s2.5 1 7.3 2.8m61.9 19.4c1.6.4 3.2.9 4.8 1.3m22.9 5.7c39.4 9.3 88.6 18.1 143.3 21.7m35.7 1.6c1.1 0 2.2 0 3.3.1 7.3-8.3 14.2-17.5 14.2-17.5m219.7 11.8c66.1-5.5 126.2-15.8 147.1-34.9m-18.2-159.7c-1.8-5.9-3.9-12.4-6.4-19.2m-119.1-164.1c-8.1-6-19-12.9-28.1-18.4m-15.3-32.8c.1-39.1 0-97.8 0-118.8m4.3-1.8V98.6s-3.5-2.2-7.2-2.2c-4 0-8.2 2.2-8.2 2.2v28.5m3.3 1.6c-.1 23.3-1.1 117.9-1.1 117.9m-15.3-16.4c-.2-11.3 0-130.5 0-153.4"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M722 286c-113.2 18-171 119.8-187.2 151.2-4.7-1.2-6.6-1.6-13.8-3.7-10.7-10.1-21.3-19.1-34.4-31.5 24.2-45.8 46.2-73.2 68.2-96.8 57-61.1 99.1-74 125.6-80.6 7.4-.4 15.3-1.1 15.3-1.1m295.4 15.1c26.7 13 57.3 33.2 88 67.5M520.6 433.9c20.7-41.9 65.2-145.4 208.9-171.3-1.1 7.4-7.4 24.6-7.4 24.6"
			/>
		</g>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M373 1093c-10.7 2.9-20.3 6.6-31.6 9.5-5.3-4.1-74.5-63.6-89.4-75.5-4.9 0-29.9 1.3-36 2 17.2 17.3 76.1 73 88.3 84.6-26.1 9.3-24.9 14.6-9.5 39.2 24.2 38.8 372.6 160.5 474.7 107.9 14.9-10.6 43.4-35.3 66.5-59 18.1 3.2 47.8 6 62.5 12 3.1 2.6 2.5 11 5.1 19.3 28.2 8.2 50.9 13.3 68.5 10.7 30.6-4.6 209.5-31.4 278.2-44 15.4-2.2 32-10.4 51.4-22 1.6-.9-1.6-14 0-15 23.9-14.5 47.3-27.7 69.6-39 1.4-.4 9.3 7.5 10.7 7.1 4.8-1.2 9.3-2.3 13.4-3.1 94.8 8.5 204.5 49.6 250-23 13.6-19.8 16-43.6 18.6-50.9 6.1-21.5 1.3-33.9.1-40.9-3.6-28.2-23.4-61.9-40.9-72-9.2-2.5-16 2.4-30.2 6-6.1-13.7-26.8-36.6-41.3-45-26.1-15.2-56.4-18.3-67.5-18-4.4.1-139.7 24.4-257 12-115.2-41.3-165.3-115.9-177.5-137.7 19.7-14.9 133.1-29.6 219.7-26-9.8-7.7-16.6-25.8-116.5-29.2-1 0-24-12-25-12-31.7-1-65.8-.2-115 3-1.1.1.1 9.9-1 10-1.2.1-2.4.2-3.6.2-21.7-27.2-78.7-87.3-159.3-116.9-76.8-139.4-238.5-147-230.8 9-45.5 10.1-59.4 10.1-80.6 30-30.6 28.8-106.8 112.2-144.1 129.9-21.7 10.3-47.2 12.3-90.5 19.8-1.3.2-20.6-7.2-22-7-30.4 5.4-60.9 13.2-108 29-1.1 2-1.1 10.1-1.4 11.5-49.2 19.5-37.1 42.6-37.1 55.6.3 26.5.4 28.7 2 94.9 53.5-33.5 129.5-52.6 206.8-60.1.2 4.3-3.4 19 53.3 20.2 51.9-4.8 51.4-20.7 50.9-23.6 97.5 3 181 23.6 202.8 55.3-3.6 12-12.5 24.7-27.8 37.7-10.3 4.6-22.6 5.9-33.4 12.5-11.2 6.9-25.4 17.9-33.1 28.3-37.3 17.8-87.6 35.5-151.2 53.8-7.8-6-51.8-44.1-51.8-44.1l-37-1s54.2 50.7 57 54z"
		/>
		<linearGradient
			id="ch-full89000a"
			x1={562.6055}
			x2={731.7095}
			y1={2444.6125}
			y2={2591.6116}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full89000a)"
			d="m673 1073 101 113s-8.8 34-137 34c-27.6-45.8-66-133-66-133s43.2 16.9 102-14z"
			opacity={0.349}
		/>
		<linearGradient
			id="ch-full89000b"
			x1={542.567}
			x2={1069.1199}
			y1={2624.217}
			y2={3150.77}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f1f1f1" stopOpacity={0.569} />
			<stop offset={1} stopColor="#b3b3b3" />
		</linearGradient>
		<path
			fill="url(#ch-full89000b)"
			d="M561.9 610.3c84.9 31 168.4 130 264.1 249.8 55.7 69.7 120.5 141.2 170.3 197.8-24.7 5.3-136.7 34.7-215.7 78.9-5.2-5.9-14.1-19-14.1-19s4.2-131.7-121-130.9c6.5-5.7 50.3-29 57.5-44-.3-11.5 0-23.9 0-38-19.3-67.6-163.8-92.5-284.2-90.9-13-16.3-43.3-51-43.3-51s31.4-7.2 79.6-55c48.1-47.5 92-98.8 106.8-97.7z"
		/>
		<linearGradient
			id="ch-full89000c"
			x1={942.0782}
			x2={1337.2478}
			y1={2413.377}
			y2={2808.5479}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={1} stopColor="#f1f1f1" />
		</linearGradient>
		<path
			fill="url(#ch-full89000c)"
			d="M1174.7 734.2c-34.2 5.2-82.3 5.1-126 25 24.8 48.9 129.3 147.4 323.4 175.9-1.4 3.9-3.1 10.9-3.1 12.9 4.3 19 27.7 19.6 57.4 32.4 11.3 4.9 48.1 5.8 61.8-1.9 24 7 125 103.8 129.1 108.4-150.3 10.4-141.3-78.4-390.1-57-88.9-102.6-142.6-231.4-221.2-326.7 8.3-.1 17.5-.5 28.3-.9 1.7-.1-18.2-8.9-16.3-9 21.1-.9 71.7-2.4 110.2-2.3 3.1.8 13 6.8 24.3 10.4 43.3 1.4 102.7 13.8 114.7 28.9-9.6 1.6-72 .7-92.5 3.9z"
		/>
		<path
			d="M564 611c102 48.9 175.2 140.1 261.1 246.8 55 68.3 115.7 139.9 174.3 198.1 13.8-2.7 15.9-1.7 33.3-5-196.5-204.6-248.8-383.8-418.4-453.6-37 8.3-38.6 7.8-50.3 13.7zm219.6-19.7c209.4 45.4 306.9 355.8 411.2 440.6 11.8-1.3 23.6-2.3 30.2-5-138.1-166.4-192.6-379.9-395-448.6-37 8.3-32.7 5-46.4 13z"
			className="factionColorPrimary"
		/>
		<path
			d="M518.5 644.3c135.8 46.4 245 251.1 414.3 430.6 24.5-7.3 14.8-5.4 47.4-14-57.4-62.7-148.7-164.9-222.4-258.1-62-78.5-122.1-147-208-185.5-18.8 10.8-9.5 7.7-31.3 27zm548.4 143.9c42 75.5 120.3 179.9 171.3 240.8 15.9-2.2 18.8-.8 41.3-3-164.6-180-302.7-345.1-212.6-237.8z"
			className="factionColorSecondary"
		/>
		<path
			fill="#f1f1f1"
			d="M1222.1 808.2c-5.6-.2-83.4-.6-119.9 15-21.2-22.7-47.5-54.5-53.4-64.9 26.7-14.2 97.2-28.5 217.7-26 121.2 2.5 212.8 27.2 263.1 61 31.4 22.8 28 41.6 22.2 107.9-95.8-34.8-72.6-2.6-134.1-9 41.6-9.3 61.9-15.4 69.7-20.4 7-4.5-153.1-7.6-167.5-7.6-.2 8.4-.2 24.8 0 35-7.7.5-72.4 2.8-100.8-6 0-13.4.1-24.2 0-31 3-1.7-75.6-3.7-71.6-5-11.3-8.3-21.3-12.5-25.2-21 23-1 61.2.6 98.1 2.5 2.6.1.1-23.6 2.7-23.5 1.8 7.8 60.5 23.5 97.8 0-.2 13.4-2.2 27.4-1 27 9.1 1.4 90.9 14.2 166.3 19-45.7-39.7-156.7-45.5-165.3-46-42.5 24.2-102.4 9-98.8-7z"
		/>
		<path
			fill="#f1f1f1"
			d="m135.5 867.1 2 93.9-10.1 9s27.6 18.8 81.7 26c146.1 29.7 289.6 7 336.6-4 28.4-7.2 85.1-19.5 107.8-40-9.3 20.8 12.5 38.6-26.2 40-45.8 1.6-75.6 43-75.6 43s-101.1 42.5-224.8 72.9c-101.5-6.3-181.2-9.8-241.9-81.9-2.3-30-7.1-92.9-7.1-92.9s5.3-47.4 57.6-66z"
		/>
		<path
			fill="#3c3b3c"
			d="M1270.5 786.2c8.3 0 39.9 4.1 40.3 13s-40.3 9-40.3 9-41.5 1.5-39.3-13c1.1-7.8 30.9-9 39.3-9zm-50.4 51.9-97.8-2 15.1 13 82.7 3v-14zm-772.1 77 101.8-46 18.1 9L448 939v-23.9zm28.2 34.9 140.1 22-24.2 8-157.2-21s4.6-2.3 12.1-6c4.5.2 6.3.8 11.1 1 10-2 18.1-4 18.1-4zM330 939l-92.7-61.9 17.1-4L346 915s-1 12.7-1 20c-3.5 1.1-15 4-15 4zm62.5-59.9c13.3 0 40.1 1.9 41.3 12 1.2 10.1-26 14-37.3 15s-40.3.1-40.3-15c.1-6.7 23.1-12 36.3-12z"
		/>
		<path
			fill="#bfbfbf"
			d="M343.1 906.1c12 13.6 72.9 23.3 103.8-3-.1 22.7 0 49 0 49s-8.1 7-14.1 7c-.1 6.2 0 13 0 13l15.1 4v25s-5.3 6.6-13.1 8c-7.9 1.4-76.9 1-79.6 1s-7.4-5.8-8.1-8c-.7-2.1 0-19 0-19h17.1s.5-4.9-1-11c-6.5-1.6-13.3-5.5-19.1-8 6.1-3.2 13.1-5 13.1-5s-5.2-2.1-12.1-6c-.5-12.8-1.6-27.8-2-47zm879-90.9c17.9 13.9 67.8 19.9 97.8 0-.1 22.7 0 26 0 26s-3.6 6.4-6 12v10l5 1s-.2 10.6 0 35c-2.7-.5-24.4.6-39.3 1 1.7-8.4 12.4-19 14.1-29-4.5-.6-20.9-.2-27.3-1.9.1-3.8.7-4.5.5-7.5-10.3-.3-14-.7-16.5-.6 0 14.3-.1 22.3 0 36-22.1 1.3-27.5-1.9-32.3-5 2.3-47.1 3-39.1 4-77z"
		/>
		<path
			fill="#d9d9d9"
			d="M1231.2 795.2c-1.2 16.8 78.9 18.4 80.6 3 4.2 6 7.5 12.4 9.1 15-16 20.6-85.8 17.9-99.8-1 4.1-11.9 10.5-23.6 10.1-17z"
		/>
		<linearGradient
			id="ch-full89000d"
			x1={1317.9023}
			x2={1311.7046}
			y1={2795.019}
			y2={2795.7803}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#656565" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full89000d)" d="M1317.8 879.1v20l-6-33 6 13z" />
		<path
			fill="#ccc"
			d="M341.7 1103c10.9-2.5 20.1-8 31.3-11-5.6-5.3-48.3-43-56-53 1.6-.1 25.4 1 36 2 4.1 3.6 46.8 39.9 50.4 42.6 70.9-20.2 140.3-44 162.5-53.7 25.4-37.4 53.6-40 76.6-43 35.2-18.4 60.5-42 60.5-42s-.8-14.4-2-22c-3.1 5.2-42.7 85-301.4 96.9-264.5-1.4-301.5-47-319.5-78.9.8 43 .2 27.6 5 85.9 38.8 51.3 101.4 66.7 168.3 73.7 7.5 7.4 22.5 22.7 29.5 28.3.5-5 9.3-11.1 20.5-15.8-7.3-7.5-89.5-85.2-89.5-85.2l47 6c.1.2 74.7 63.7 80.8 69.2zm-57 22.9c170.3 56.7 329 119.3 445.5 83.9 20.8-2.4 37.6-14.1 44.8-21.8 3.3 4.4 23.3 25.6 34 39-6.9 6.9-32.3 31.3-40.5 34.8-27.6 25.1-244.2 21.2-467.7-102.9-10.3-8.6-16.6-21.5-16.1-33zm496.9 12c41.4-23.6 115.8-58.5 215.7-79.9 104.8-13.4 191.1-25 227.8-29 36.9-1 145.8-1.1 173.4 6 38.5 5.8 133.2 64.5 219.7 52 32-5.8 36.9-29.8 38.8-36.9 2.1 1.2 4.1 2.2 7 4-5.4 30.2-18 49-18 49s-19.8 42.9-94.3 42.9c-56.2-.1-85.3-11.9-156.2-19-4.7 1.1-7.2 2.3-13.1 5-3.8 1.1-6.1-5.8-11.1-8-22 12-50.7 27.3-70.6 39-1.4.8 1.5 13.7.1 14.6-18.9 11-33.4 18.1-42.4 20.4-24.8 6.5-292.3 46-292.3 46s-6.7 4.3-61.5-10.5c-.9-5.7-2.3-11.7-7-21.4-11.6-2.1-60.5-10-60.5-10s-37.4-43.2-55.5-64.2zm497.9-226.8c46.5-.2 140.4-8.5 161.3-14-20.4 15-26.1 22.1-36.3 41-26.6-3.3-106.2-17.6-125-27zm208.7-28c26.9-8.5 62-24.7 67.5-44-2.1 29.7-5 62.9-5 62.9s-26.7-17.3-62.5-18.9z"
		/>
		<path
			fill="#b3b3b3"
			d="m1510.3 865.1-22.2-3s-26.9-36.5-169.3-50c-.6-6.3-4.1-21.5-31.2-24-20.7-3.3-36.3-1-36.3-1s-25.8 2.1-28.2 21c-25.5-.4-90 2.1-120.9 13-19.8-21-34.8-34.5-49.4-57 19.1-9 126.4-27 226.8-21 179.4 3.8 323.7 73.4 230.7 122zM125.4 970c24.6-26.2 97.3-46.2 145.1-60-20.1-15.5-35.3-24-35.3-24l1-10 18.1-3 64.5 30s16.9-.7 24.2 0c21.8-33.1 86.3-26.2 103.8-5 22.3.7 21 .5 36.3 1 22.6-10.1 66.5-28 66.5-28l17.1 7 1 9-38.2 19.1c1.3.1 86.1.1 120.8 46.8 79.1-38.7 47.6-122.4-233.9-131.9-346.8 10.5-350.3 116.4-291 149z"
		/>
		<path
			fill="#bfbfbf"
			d="M1138.4 848.1c1.3.4 81.6 4 81.6 4v9l-68.5-3s-14.4-10.4-13.1-10zm175.4 6c.7.4 170.3 20 170.3 20l-9.1 4-161.3-15c.1 0-.6-9.4.1-9z"
		/>
		<path
			fill="#d9d9d9"
			d="M354.2 893.1c5.6 16.7 70.7 19.8 80.6-2 6 4.6 8.6 7.4 12.1 11-26.6 31.9-87.8 25.5-104.8 3 6.4-7.8 6.6-6.6 12.1-12z"
		/>
		<linearGradient
			id="ch-full89000e"
			x1={236.6325}
			x2={261.7845}
			y1={2761.7417}
			y2={2791.7158}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full89000e)" d="m268.6 909.1-34.3-21v30l34.3-9z" />
		<linearGradient
			id="ch-full89000f"
			x1={572.8984}
			x2={543.1295}
			y1={2767.4971}
			y2={2792.4761}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full89000f)" d="m534.7 904.1 37.3-20v25l-37.3-5z" />
		<linearGradient
			id="ch-full89000g"
			x1={365.6026}
			x2={348.2326}
			y1={2679.9214}
			y2={2694.4966}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.098} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full89000g)" d="m347.2 980 17.1-5v35s-8.2 3.4-14.1-3c-4.5-3.4-3-27-3-27z" />
		<linearGradient
			id="ch-full89000h"
			x1={423.9603}
			x2={441.0843}
			y1={2683.6396}
			y2={2691.5449}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.098} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#545454" />
		</linearGradient>
		<path fill="url(#ch-full89000h)" d="m431.3 971 16.7 4v26s-2.5 7.3-17 5.9c-.5-5.1.3-35.9.3-35.9z" />
		<path
			fill="#bfbfbf"
			d="m330 941-94-61 .3 6.1L330 947v-6zm118-1 119.9-61.9v8L448 951v-11zm-15 20c.9.6 152.1 20 152.1 20l-34.3 10L433 974s-.9-14.6 0-14zm-69 13 .3 10L206 995l-36.3-7L364 973z"
		/>
		<linearGradient
			id="ch-full89000i"
			x1={1109.65}
			x2={1109.65}
			y1={2453.2}
			y2={2625.1001}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#595959" />
		</linearGradient>
		<path fill="url(#ch-full89000i)" d="m1224.1 1052.9 44.3 120.9-317.5 51 52.4-143.9 220.8-28z" />
		<path fill="gray" d="m1224.1 1052.9 44.3 120.9-149.2-106.9 104.9-14z" />
		<linearGradient
			id="ch-full89000j"
			x1={1193.8}
			x2={1193.8}
			y1={2504.2}
			y2={2625.1001}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full89000j)" d="m1224.1 1052.9 44.3 120.9-149.2-106.9 104.9-14z" opacity={0.4} />
		<linearGradient
			id="ch-full89000k"
			x1={550.311}
			x2={849.7581}
			y1={2855.4993}
			y2={3106.7654}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#2e3033" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full89000k)"
			d="M688.9 482.4c48.9-9.1 91.4 26.8 101.8 35 10.4 8.2 29.3 25.9 56.5 65.9 18.5 27.3 35 55.2 44.3 78.9 5.4 13 50.5 98.3 6 103.9-28.8 5.1-93.2-62.5-138.1-89.9-130.7-87.6-119.8-79.2-141.1-80.9-2.5-5.4-4.4-105 70.6-112.9z"
		/>
		<linearGradient
			id="ch-full89000l"
			x1={550.311}
			x2={849.7581}
			y1={2855.4993}
			y2={3106.7654}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#2e3033" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full89000l)"
			d="M688.9 482.4c48.9-9.1 91.4 26.8 101.8 35 10.4 8.2 29.3 25.9 56.5 65.9 18.5 27.3 35 55.2 44.3 78.9 5.4 13 50.5 98.3 6 103.9-28.8 5.1-93.2-62.5-138.1-89.9-130.7-87.6-119.8-79.2-141.1-80.9-2.5-5.4-4.4-105 70.6-112.9z"
		/>
		<path
			fill="#d9d9d9"
			stroke="#010133"
			strokeWidth={5}
			d="M635.5 597.3c5.2-6.3 14.1-24 14.1-24l55.4 33s2.5 16.5 7 38c-18.6-13.5-62.7-39.6-76.5-47z"
		/>
		<path
			fill="#4d4d4d"
			d="M798.7 578.3c7.3 7.1 39.9 47.3 63.5 96.9-17.4.4-77-14.3-95.8-52-3.6-32.7 19.9-40.2 32.3-44.9zm51.5 11c7.3 7.1 15.7 19.3 39.3 68.9-1.5 2.8.7 13.1-16.1 16-39.6-71.6-48.9-74.4-62.5-97.9 16-2.9 34 10.9 39.3 13z"
		/>
		<radialGradient
			id="ch-full89000m"
			cx={714.8169}
			cy={-10458.2393}
			r={307.1306}
			gradientTransform="matrix(0.5233 0.8522 0.21 -0.129 2620.7825 -1312.0183)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1583} stopColor="#535353" />
			<stop offset={0.2091} stopColor="#757575" />
			<stop offset={0.3446} stopColor="#999" />
			<stop offset={0.4} stopColor="#fff" stopOpacity={0.4} />
			<stop offset={0.4553} stopColor="#a6a6a6" />
			<stop offset={0.8179} stopColor="#666" />
			<stop offset={0.9703} stopColor="#333" />
		</radialGradient>
		<path
			fill="url(#ch-full89000m)"
			d="M799.7 579.3c7.3 7.1 39.9 47.3 63.5 96.9-17.4.4-77-14.3-95.8-52-3.6-32.7 19.9-40.2 32.3-44.9zm51.5 11c7.3 7.1 15.7 19.3 39.3 68.9-1.5 2.8.7 13.1-16.1 16-39.6-71.6-48.9-74.4-62.5-97.9 16-2.9 34 10.9 39.3 13z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M562.4 610.5c84.8 38.8 151.2 110.6 212.6 188.4 73.6 93.2 165 195.4 222.3 258.1"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="M797 825c-16.7 15.2-97 76-97 76" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M679 692 525 822" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M328 1104.9c-49.3 14.8-52.3 17.4-33.3 48 24.2 38.8 370.5 160.5 472.7 107.9 13.1-7.6 27.2-21.7 69.5-59 25 4.7 40.7 6.8 60.5 11 4.8 1.4 5 20 9.4 21.3 26.6 7.6 48.2 12.2 65.1 9.7 30.6-4.6 209.5-31.4 278.2-44 14.9-2.1 32.8-11.3 51.6-22.4 1.3-.8-2.5-13.9-1.2-14.6 26.3-14.9 46.1-26 71.6-39 1.3-.7 9.5 8.2 10.9 7.7 4.3-1.6 8.4-2.8 12.3-3.7 94.8 8.5 204.5 47.6 250-25 27.1-44.6 29.8-141-21.2-161.9-9.8-4-21.6 3.5-31.2 6-9.1-19.9-22.7-35.5-41.3-47 1.5-14.2 5-66 5-66s13.4-82.8-287.3-101.9c-9.5-7.4-29.5-26.3-120-30.8m-116.2 1.5c-8 .5-16.2 1-24.7 1.5-21.7-27.2-78.7-87.3-159.3-116.9-76.8-139.4-238.5-147-230.8 9-45.5 10.1-59.4 10.1-80.6 30-30.7 28.8-106.9 112.2-144.2 129.9-21.4 10.1-49.3 12.4-91.5 19.9m-111.2 27.8-9.9 3.3c-60 20.2-46.4 44.2-46.4 58C96.2 886.3 78 909.3 79.1 940c1.1 30.8 7.1 85.9 7.1 85.9s22.8 59.3 166.8 75.8"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M1179.9 714.5c-7.6-3.4-51.9-23.5-51.9-23.5s-92.6.5-112 3c24.9 13.1 50 25 50 25s102.9-3.5 113.9-4c.8.1.5-.3 0-.5z"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="M1011 692v12" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M171 811v-11.7c0-.2.1-.3.3-.3 2.9.6 25.9 5.3 42.7 16.9 35.3-11 86.1-22.1 112-26.4 3.4-.3 2.2-1.1.1-1.7-7.4-3.8-45-18.9-45-18.9s-72.4 15.7-110 30.1"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M1063 702.8c5.7 0 10.3 1.2 10.3 2.8s-4.6 2.8-10.3 2.8-10.3-1.2-10.3-2.8 4.6-2.8 10.3-2.8zm63 0c5.7 0 10.3 1.2 10.3 2.8s-4.6 2.8-10.3 2.8-10.3-1.2-10.3-2.8 4.6-2.8 10.3-2.8zm-910 93c5.1 0 9.2 1.7 9.2 3.7s-4.1 3.7-9.2 3.7-9.2-1.7-9.2-3.7 4.1-3.7 9.2-3.7zm65.5-15.7c4.6 0 8.3 1.5 8.3 3.4s-3.7 3.4-8.3 3.4-8.3-1.5-8.3-3.4 3.7-3.4 8.3-3.4z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M305 1114s-58.2-53.7-92.8-86.3c-.3-.2.1-.6.3-.7 9.5 2.4 33.1 5.6 48.6 7 28.5 24 70.8 60.7 79.5 67m33.4-8s-61.9-56.2-62-55 31.1 2.2 40 2c11.2 9.4 51 43 51 43m-91-44v25l38 35"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="M212 1026v36s54.3 51.6 73.4 69.8" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m484.5 724.2 47.2-43.3 5.8 4.2-46.2 43.3-6.8-4.2zM501 735l47.8-43.1 5.9 4.2L508 740l-7-5zm18.7 13.3 47.2-43.3 5.9 4.2-46.2 43.3-6.9-4.2zm19.7 13.4 46.3-43.7 5.9 4.3-46.3 43.7-5.9-4.3z"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="m1394.4 1126.9 18.1-87.9s135.3 8.9 121.8-52.7" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m1625 1034-74-16 1-9 51 12" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m715 1216 4 57" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m1604 1087-3 53" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M615.3 596.3c13-1.9 17.8-.7 28.2 5s113.7 66.2 212.7 148.9c101.3 63.3 55.7-76.2-7.1-162.9"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M862.3 675.3c-23.2-44.2-49.7-83.3-64.5-96.9-31.4 9.2-39.2 33.4-26.2 54 14.5 20 63.7 46.6 90.7 42.9zm-51.5-99c12 13.6 51.5 72.2 63.5 97.9 10.3-2.3 22.9-5.5 9.1-32-13.9-26.4-31.2-50.9-31.2-50.9s-22.4-17.4-41.4-15z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M789 582c-8.8-8.2-41.8-48.2-88-76.5m41.2-7c22 13.7 57.6 43.2 85.8 80.5"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M724 483s15 6.9 26 14c-17.2-.2-53.5 8.4-60 12-7.2-3.6-26.7-13.5-40.2-8.7"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M391.5 822.1C191.4 826.3 96.2 887.7 94.2 927c1.7 63 143 82.9 291.3 83.9 138.2 1 304.1-50.7 303.4-101.9-.5-35.8-91.4-91.2-297.4-86.9z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M79.1 939c19.8 68 198.2 80.9 320.5 80.9 124.8 0 302.4-48.4 302.4-107.9 0-50.7-107.9-101-312.5-97.9-111.4 1.7-181.7 16.3-257 51.9"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="m96.2 965 6 80.9" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M245.4 1014s.2 9.4.5 17.6" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m461.1 1018 1 48" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m950.9 1224.8 51.4-142.9 221.8-29 45.4 121.9-318.6 50z" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m978.2 1219.8 5-16 43.3-6 20.2 11 33.3-5 2-15 44.3-6 17.1 10 33.3-3 3-16 37.3-5 20.2 11-259 40z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M394 880.1c22.8-1 40.3 5 40.9 12s-17.8 14.2-39.4 15-38.6-4.6-39.4-12.8c-.8-8.1 15.1-13.3 37.9-14.2z"
		/>
		<path
			fill="#f1f1f1"
			stroke="#010133"
			strokeWidth={5}
			d="M395.4 888c10.7-.4 18.9 1.9 19.2 4.5.3 2.6-8.4 5.3-18.5 5.6s-18.1-1.7-18.5-4.8c-.4-3 7.1-4.9 17.8-5.3z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M358.3 889.1s-11.1 10.8-16.1 16c4.6 19.1 85.8 27.3 104.8-3-4.5-4.4-14.1-14-14.1-14"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="M325 1105.9c26.1-7.6 160.3-43.9 244.9-79.9" />
		<path fill="none" stroke="#003" strokeWidth={5} d="M641.5 988c28.4-18 48.7-28.7 60.5-45 .3-6.5 0-33 0-33" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M781.6 1135.9c69.6-31.6 50.5-31.3 157.2-62.9 18.4-5.6 38.1-10.6 58.8-15.1 80.6-11.8 130.8-17.9 234.1-29.3 72.5-4 132 1.8 160.7 5.4 66.3 8.3 117.2 62.2 226.8 53 30-2.5 50-41.2 28.8-65.9"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="M792 1134c-107.5 20.6-226.6-6.4-360.9-59.3" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m341 1119.6 80.4-24.6 15.6 4.2-80.4 23.8-15.6-3.4zm213 16.4-58 37 15 4 56-38-13-3z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M284.7 1124.9c333 118.2 386.1 98.4 445.5 85.9 39.7-9.6 43.8-25.8 43.8-25.8"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="m938.8 990 234.8-31" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m875.3 921.1 252-32" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m821.9 854.1 265.1-31" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m766.5 790.2 277.2-34" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m908.6 705.2 89.7-11" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m716.1 729.2 100.8-12" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m1266.4 1172.8-148.2-106.9" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m1248.3 1116.9-79.6-58" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m345.2 952-200.6 27s20.6 9.9 50.4 16c43.2-2.4 169.3-13 169.3-13v-10"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="M347.2 982v20s0 4.8 8.1 8" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m433.9 959 154.2 21" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M449 974v27s-5.3 5.5-10.1 7" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M447 899.1v52" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m448 914.1 97.8-42s13.3 3.5 21.2 6c-.2 7 0 9 0 9L447 949v-9l118.9-59.9"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m554.8 990-122-17v-14s8.4.2 13.1-6c5.8.8 7.3 1.1 13.2 1.5 6.9-1.4 9.5-2.3 22.1-4.5 51 9.9 100.8 19.1 123.9 23"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="M318 903.1c9.4-2.1 21-2 26.2-2" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M442.9 897.1c8 0 24.3.8 38.3 3" />
		<path fill="none" stroke="#141541" strokeWidth={5} d="M650.6 952c-12-22.9-56.1-36.7-77.6-41s-42.3-5-42.3-5" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M235.3 878.1 330 938v8l-94.7-59.9v-9l19.2-4 90.7 41v38" />
		<path fill="#bfbfbf" stroke="#010133" strokeWidth={5} d="m331.1 947 14.1-3v-10l-14.1 4v9z" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M125.1 970c1.3-1.4 2.3-2 2.3-2 30.5-26.2 74.2-39 130-55 3.4-1 8.1-2 8.1-2l8.1-1"
		/>
		<path
			fill="#3d3c3c"
			stroke="#010133"
			strokeWidth={5}
			d="m174.8 989 189.5-16s-.2-1.3-1.9-1.9c-5.6-1.7-20.2-8.1-20.2-8.1s7.9-3.5 12.3-4.6c1.1-.3.8-.4.8-.4s-2.8.3-11.1-6c-30.3 4.3-197.6 27-197.6 27s16.3 8.8 28.2 10z"
		/>
		<path
			fill="#f1f1f1"
			stroke="#010133"
			strokeWidth={5}
			d="M1269.9 792.8c10.8-.1 19 2 19.2 4.3s-8.6 4.3-18.8 4.3c-10.2 0-18.2-1.9-18.5-4.5-.3-2.5 7.3-4 18.1-4.1z"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="m1314.8 863.1 159.3 14" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M1556.7 831.1c0 14.5-4.3 32.2-66.5 51" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M1269.5 910.1c49.1 1.6 141.8-6.6 171.4-14" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m1118.3 835.1 101.8 3-1 23-68.5-4s-14.3-9.2-15.1-10c16 1.2 82.7 4 82.7 4"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M1273.5 732.2c-31.4-.8-144.7-2.7-224.8 25 61.9 96.7 168.4 156.8 355.8 180.8"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="M1004.4 699.3c56.9 68.3 132.9 225.5 221.7 328.7" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M1379.3 906.1v29" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M1222.1 813.2c-.5 12 69.6 23.3 97.8 2" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M1270.6 786.6c17.4 1 39.6 4.3 40.6 11.7s-18.7 9.1-36.5 9.5c-16.5.4-43.8-1.8-43.1-10.3.7-8.4 21.6-12 39-10.9z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M1053.8 764.2c164-51.1 486.8-10.9 486.8 69.9 0 31.6-123.4 78.7-308.4 63"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M1102.1 822.1c32-11.5 103.6-15.5 119.9-14 3.3-7.3 2.8-17.1 30.2-21 9.6-.6 22.1-1.3 35.3 1 26.8 4.9 23.9 12.7 33.3 24 27.5 6.7 90.1 6.5 150.2 36 7 3.5 22.2 15 22.2 15"
		/>
		<path
			fill="#bfbfbf"
			stroke="#010133"
			strokeWidth={5}
			d="M1428.7 803.2v7l-82.6 32-5 1-11.1 4-12.1-1 2-5 108.8-38z"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="m1320.9 809.2-1 35s-3.3 4.6-6 11c-.2 3.9 0 9 0 9h6v35" />
		<path
			fill="#3c3c3c"
			stroke="#010133"
			strokeWidth={5}
			d="M1328.9 848.1s8.5-3.3 15.1-5c40.3 4.7 162.3 23 162.3 23s-7.7 4.7-15.1 7c-57.9-3.8-176.4-17-176.4-17s2.4-4.3 4.2-7.6c4.7.8 9.9-.4 9.9-.4z"
		/>
		<path fill="#bfbfbf" stroke="#010133" strokeWidth={5} d="M1184.8 777.2v8.9l36.3 37 1-11-37.3-34.9z" />
		<path
			fill="#3d3e3e"
			stroke="#010133"
			strokeWidth={5}
			d="M1242.2 898.1c2.8-11.7 5.9-24.4 9.1-37h15.1s-.4 4.6-1 8c4.5.5 24.1 1.5 28.2 2-6.3 12.7-14.1 29-14.1 29s-13.4 1.9-37.3-2z"
		/>
		<path
			fill="#3c3b3c"
			stroke="#010133"
			strokeWidth={5}
			d="m1235.2 792.2-23.2-19-24.2 1 34.3 38s-1.4-13.8 13.1-20z"
		/>
		<path
			fill="#3d3d3d"
			stroke="#010133"
			strokeWidth={5}
			d="M1319.8 842.1s95.2-35.7 108.4-39.5c.9-.3.4-.4.4-.4l-22.2-5-86.7 24c.1-.1.1 13.4.1 20.9z"
		/>
		<path
			fill="#353535"
			stroke="#010133"
			strokeWidth={5}
			d="M380.5 964c10.6.5 15.1 0 15.1 0l1 8h26.2l-7 37s-15.6 1.6-30.2 1c-2-16.7-2.8-23.1-5.1-46z"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="m1222.1 810.2-2 27-2 55" />
		<path
			fill="#f2f2f2"
			d="m843 1264 52-86-157-193s1.8-37.2-29-63c-29.3-29.9-55-58-55-58s-23.2-21.5-59-18-79 33.7-77 87c2.2 29.6 24.6 52.7 34 66s22 30 22 30-8.8 37.4-10 41-5 13.1 10 18 45.9 10.8 101-14c33.2 38.1 168 190 168 190z"
		/>
		<path fill="#ccc" d="m635 1021-28 17 3 9 39-7-14-19z" />
		<path fill="#bfbfbf" d="m657 1052-15 21s-27.9-12.6-31-27c10.5-.6 38-6 38-6l8 12z" />
		<path fill="#8c8c8c" d="M674 1073c-3.5-3.9-13.6-17.7-16-22-4.6 5.6-17 21-17 21s36.5 4.9 33 1z" />
		<path fill="#d9d9d9" d="m844 1262 50-83-103 13 53 70z" />
		<linearGradient
			id="ch-full89000n"
			x1={652.5}
			x2={652.5}
			y1={2592.9409}
			y2={2760.0906}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.99} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full89000n)"
			d="M728 972c-6.1-8.9-20.7-35-61-32s-70.2 41.3-66 77c4.2 35.7 26.5 56.3 58 57 3.9 0-41.8 3.2-64-20s-27-37.7-27-63 31-84 92-84c16.5 0 77 11 77 76-3.9-4.2-2.9-2.1-9-11z"
		/>
		<linearGradient
			id="ch-full89000o"
			x1={519.1841}
			x2={704.196}
			y1={2688.23}
			y2={2860.7561}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full89000o)"
			d="M703 917c-13.3-14.3-43.2-46.7-50-54s-45.9-29.9-92-7-52.9 83.3-31 113c21.9 29.7 48 65 48 65s-36.1-69.6 37-114c46.8-23.2 75.1-7 88-3z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full89000p"
			x1={671}
			x2={563}
			y1={2618}
			y2={2618}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-full89000p)"
			d="M575 1028c5.1 10.8 21.9 49.4 96 46-10.5 6.2-37.4 18-68 18s-40-8.4-40-15 7.6-37.3 12-49z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full89000q"
			x1={885.5455}
			x2={819.0596}
			y1={2439.9756}
			y2={2501.9756}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full89000q)" d="m844 1248 38-62-78 12 40 50z" opacity={0.749} />
		<path fill="#999" d="M792 1193 610 988l66 88 166 187-50-70z" />
		<path
			fill="#d9d9d9"
			d="M726 968c-4.9-7.8-15-20-33-26 2.2 3.5 3 7 3 7l-2 25s23.7-3.2 32-6zm-63 23-22-43s-23 9-34 37c14.5-2.4 40-7 40-7 6.3 5.1 16 13 16 13zm-56-5 28 35-29 18s-12.5-36.3 1-53z"
		/>
		<path fill="#e6e6e6" d="m692 990 4-40s-33.6-5.8-50 7c4.9 9.6 19 36 19 36l27-3z" />
		<linearGradient
			id="ch-full89000r"
			x1={620.295}
			x2={585.4728}
			y1={2666.5}
			y2={2666.5}
			gradientTransform="matrix(1 0 0 -1 0 3678)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full89000r)" d="m606 984 29 38-29 17s-13.1-37.4 0-55z" opacity={0.302} />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m843 1264 52-86-157-193s1.8-37.2-29-63c-29.3-29.9-55-58-55-58s-23.2-21.5-59-18-79 33.7-77 87c2.2 29.6 24.6 52.7 34 66s22 30 22 30-8.8 37.4-10 41-5 13.1 10 18 45.9 10.8 101-14c33.2 38.1 168 190 168 190z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m894 1178-104 14-182-207s-.8-1-.4-1.1c5.2-.6 40.4-4.9 40.4-4.9l17 14 27-3 2-16 32-4 15 19m-35-69c-17.3-9.3-56.9-25.2-101 7s-39.3 80.5-30 101 36.1 53.5 99 45c-13.3-16.9-65-86-65-86"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="m581 927-33-41s4.1-5.5 10.1-9.7c2.5-1.7 5.1-4 8.9-5.3.3.3 1.7 1.7 2 2.1 11.9 12.1 34 37.9 34 37.9s-14.7 7.1-22 16z"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M575 918c2.6-2.3 14.7-12.5 20-15m-6-9c-4.1 1.8-15.2 10.5-20 15m-8-7c3.3-2.9 16.3-13.5 21-16m-8-8c-7.2 4.7-16.3 12.1-19 15"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M694 906c-8.1-4.2-15.3-7.7-23.3-8.9-9.5-8.9-32.2-31.6-36.9-36.3-1.1-1-1.1-.9.2-.6 4.1.8 17.9 4 27 10.8"
		/>
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M661 887c4.4-.1 17.2 3.5 23 7m-12-11c-7.5-2.7-17.2-4.8-22-5m-6-7c6.2.4 17 2.2 21 4"
		/>
		<path fill="none" stroke="#010133" strokeWidth={5} d="m693 979 3-29-7-8m-49 7 5 6 19 37m-17-13-7-29" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="M696 950c-8.5-1.9-30.7-4.4-50 5" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m636 1021-30 17m5 8 36-6m10 12-15 18" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m790 1192 53 69" />
		<path fill="none" stroke="#010133" strokeWidth={5} d="m804 1197 78-11-38 62-40-51z" />
		<path
			fill="none"
			stroke="#010133"
			strokeWidth={5}
			d="M726 970c-4.6-8.1-22.6-33.1-60-30s-80.7 44.4-61 97c14.1 38.9 50.9 37 59 37"
		/>
		<g>
			<path
				fill="#f2f2f2"
				d="m1795 1123 50-76-294-169s-7.8-32.1-29-47c-25.2-12.5-115-58-115-58s-34.8-12.7-70 21-26.6 92.9-4 106 43 27 43 27-5.7 15.7-7 19-1.3 10.3 15 18 66.2 28.3 101 17c2.5-1.1 4.1-4.6 3-11 2.4-.2 7.5-1.6 10-3 27.6 14.5 297 156 297 156z"
			/>
			<path fill="#d9d9d9" d="m1796 1122 49-75-80 11 31 64z" />
			<path fill="#b3b3b3" d="m1795 1123-31-65-308-166 16 34 19 7 11 16s-12 6.9-12 7 7 11 7 11l298 156z" />
			<path
				fill="#d9d9d9"
				d="M1552 879c-.7-4-4.9-16.5-18-21 .3 7.3 2 24 2 24s9.7-1.8 16-3zm-58-17 20 36-22-14-35 8 15 33-25 15s-10.1-27.2 9-59c14.5-19.5 25.8-22.6 29-24 2.8 2 9 5 9 5z"
			/>
			<path fill="#e6e6e6" d="m1533 857 3 36-22 4-19-35s19.9-10.6 38-5z" />
			<linearGradient
				id="ch-full89000s"
				x1={1831.6512}
				x2={1781.6512}
				y1={2577.1748}
				y2={2623.8008}
				gradientTransform="matrix(1 0 0 -1 0 3678)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#666" />
				<stop offset={1} stopColor="#000" />
			</linearGradient>
			<path fill="url(#ch-full89000s)" d="m1797 1106 30-47-50 5 20 42z" opacity={0.749} />
			<path fill="#ccc" d="m1474 926-26 14 4 9 40-14-18-9z" />
			<path fill="#bfbfbf" d="m1501 948-29 18s-16.9-4.6-20-19c10.5-.6 40-12 40-12l9 13z" />
			<path fill="#8c8c8c" d="M1497 966c-3.4-3.9-5.6-5.7-8-10-6 4-17 11-17 11s19.3 6 25-1z" />
			<linearGradient
				id="ch-full89000t"
				x1={1479.236}
				x2={1479.236}
				y1={2698.8152}
				y2={2847.3135}
				gradientTransform="matrix(1 0 0 -1 0 3678)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="gray" />
				<stop offset={0.99} stopColor="#e6e6e6" />
			</linearGradient>
			<path
				fill="url(#ch-full89000t)"
				d="M1494 854c-27.5 6-53.2 36.3-49 72 3.5 23.9 17 34.8 27 43 3.9 0-30.8 5.2-53-18-3.4-8.1-20-32-6-70 11.5-21.9 21.8-47.7 74-60 16.4 0 49.4 6 64 55-5.7-10-17.1-32.4-57-22z"
			/>
			<linearGradient
				id="ch-full89000u"
				x1={1334.0842}
				x2={1522.9583}
				y1={2763.1606}
				y2={2939.2886}
				gradientTransform="matrix(1 0 0 -1 0 3678)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#999" />
				<stop offset={0.5} stopColor="#000" stopOpacity={0} />
				<stop offset={1} stopColor="#000004" />
			</linearGradient>
			<path
				fill="url(#ch-full89000u)"
				d="M1502 821c-19.7-12-83.7-42.1-95-48-28-8.1-62.4 5.1-81 35-13.9 22.4-21.7 55.2 4 90 26.8 17.5 82.4 50 90 55-46.6-82.2 51.1-141.5 82-132z"
				opacity={0.4}
			/>
			<linearGradient
				id="ch-full89000v"
				x1={1489}
				x2={1369}
				y1={2722.7}
				y2={2722.7}
				gradientTransform="matrix(1 0 0 -1 0 3678)"
				gradientUnits="userSpaceOnUse">
				<stop offset={0} stopColor="#000" />
				<stop offset={1} stopColor="#b3b3b3" />
			</linearGradient>
			<path
				fill="url(#ch-full89000v)"
				d="M1377 926c11.7 7.3 37.9 26.5 64 39 15.3 6.7 19.5 6.3 48 5-.6 4.2 4 14.2-27.9 14.6-73.4-8.7-92.1-30-92.1-36.6s3.6-10.3 8-22z"
				opacity={0.502}
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeWidth={5}
				d="m1795 1123 50-76-294-169s-7.8-32.1-29-47c-25.2-12.5-115-58-115-58s-34.8-12.7-70 21-26.6 92.9-4 106 43 27 43 27-5.7 15.7-7 19-1.3 10.3 15 18 66.2 28.3 101 17c2.5-1.1 4.1-4.6 3-11 2.4-.2 7.5-1.6 10-3 27.6 14.5 297 156 297 156z"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeWidth={5}
				d="M1375 926s57.1 34.2 68 40 32.8 5.8 46 4m8-3-8-11 12-8-10-15-20-9-16-34 36-6 23 13 21-4v-12l17-3"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeWidth={5}
				d="M1426 956c-12.4-8.6-32.4-50.8-3-93s68.4-45.2 83-40m41 43c-14.5-11.5-25.1-17.6-43-15s-49.9 16.3-58 56 22.5 64.2 40 63m11-19-28 16m-16-19.7c12.5-3.5 40-11.3 40-11.3m-19-10-25 12"
			/>
			<path
				fill="none"
				stroke="#010133"
				strokeWidth={5}
				d="m1344 813 69 36s7.6-12.8 20-20c-24.4-10.9-72-36-72-36s-5.2 3.4-9.5 7.9c-4.6 5-7.5 12.1-7.5 12.1z"
			/>
			<path fill="none" stroke="#010133" strokeWidth={5} d="M1419 823c-5.1 2.7-15.3 14.1-17 20" />
			<path fill="none" stroke="#010133" strokeWidth={5} d="M1406 817c-5.1 2.7-14.3 13.1-16 19" />
			<path fill="none" stroke="#010133" strokeWidth={5} d="M1394 810c-5.1 2.7-14.3 13.1-16 19" />
			<path fill="none" stroke="#010133" strokeWidth={5} d="M1382 803c-5.1 2.7-14.3 14.1-16 20" />
			<path fill="none" stroke="#010133" strokeWidth={5} d="M1369.5 798.6c-4.9 3-13.6 13.8-14.9 19.8" />
			<path fill="none" stroke="#010133" strokeWidth={5} d="m1843 1047-79 11 31 63m-30-62-307-167" />
			<path fill="none" stroke="#010133" strokeWidth={5} d="m1776 1064 53-7-32 50-21-43z" />
			<path
				fill="none"
				stroke="#010133"
				strokeWidth={5}
				d="m1485 859 7 25m22 13-20-35s20.9-10.4 39-4c1 14.1 2 30 2 30m-38-25-11-3"
			/>
		</g>
	</svg>
);

export default Component;

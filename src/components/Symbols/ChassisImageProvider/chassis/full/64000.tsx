import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f1f1f1"
			d="M1254 1245c17.8 20.2 56.1 76.8 89 78s79-8 96-14c27.4-14.6 114.7-61.3 132-72s22.3-26.1 4-58-94.3-162.9-160-225-131-86.7-147-97c3.1-7.4 7.3-17.7 5-21s-88.5-53.1-100-60c-9.9.9-28.3 2.6-43 4-3-2-9-7-9-7l-1-16-80-57-15 5-22-12 2-18-45-24s-5.7-14.5-10-19-34.2-28.7-47-37-38.3-7-42-7c0-12.9-.5-33.2-16-52 12.6-17.3 29-38 29-38l17-37-45 2-45 54s-54-1.8-54 70c-5.9.3-10 1-10 1l-1-10-20-13s-6.9-5-12-5c-3.6-10.1-26-82-26-82l-5-5-25-3-25 4-18 88s-9.4 5-11 7c-3.5 2-17 13-17 13l-3 127s-54.7 60.8-100 69c-16.7 25.7-21.8 38-15 72s38.3 179.7 48 211 74.4 157.3 103 216 70.4 148.6 85 148 83.9-11.9 95-15 24.9-24.4 61-76c14.2-20.5 9.4-52.8 5-76s-26-88.8-30-99c23 20.4 132.2 106.2 200 128 1.7 47.8 4 123 4 123s27.7 57.6 54 46c8.9-45.7 30-156 30-156s87-2.1 140-55z"
		/>
		<linearGradient
			id="ch-full64000a"
			x1={862.6981}
			x2={1207.0001}
			y1={0.3415}
			y2={-440.3445}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e6e6e6" />
			<stop offset={0.3} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full64000a)"
			d="M1176 1233c0-36.9-32.8-140.9-153-237-92.5-4.7-141.5 4.6-215-59-23.8-55.3 101.5-67 145-63-6.2-6.8-18-20-18-20l-22-5-100 4-99-56s72.3 179.9 87 253c3.1 11.1 5 36 5 36s180.4 183.2 370 147z"
		/>
		<linearGradient
			id="ch-full64000b"
			x1={982.6399}
			x2={1216.2549}
			y1={61.6797}
			y2={-42.3332}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={0.997} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full64000b)"
			d="M1174 1232c41.7-7.6 104.4-31.8 109-62s-15.6-98.5-69-169c-10.1 13.1-11 20-11 20l-81 14-39-32-33 5s110 105.2 124 224z"
		/>
		<path
			fill="#bfbfbf"
			d="m1118 1029-67-173-187-138-19-11-25-18-45-27 93 111s-6.6 9.4-2 22 22 28.3 38 29c3.2 12 14.1 26.3 36 30 7.1 12.4 22.3 39 41 37 8.8-5.4 16-10 16-10l35 78 86 70z"
		/>
		<path fill="#a6a6a6" d="M943 782c1 .7 16 11 16 11l7 19-12 11s-12-41.7-11-41z" />
		<linearGradient
			id="ch-full64000c"
			x1={905.7099}
			x2={874.9439}
			y1={-307.8004}
			y2={-358.8004}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full64000c)"
			d="M864 775c8.9 3.4 29.9 24.1 34 38 6.7-9 16-19 16-19s-23.2-30.2-34-32c-7.5 5.6-8.4 8-16 13z"
		/>
		<linearGradient
			id="ch-full64000d"
			x1={945.7984}
			x2={915.0324}
			y1={-278.8539}
			y2={-329.8539}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full64000d)"
			d="M904 804c8.9 3.4 29.9 24.1 34 38 6.7-9 16-19 16-19s-12.3-25.9-34-32c-7.5 5.6-8.4 8-16 13z"
		/>
		<linearGradient
			id="ch-full64000e"
			x1={994.3732}
			x2={953.3532}
			y1={-231.4256}
			y2={-299.4256}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full64000e)"
			d="M946 835c8.9 3.4 34.9 41.1 39 55 6.7-9 12-13 12-13s-8.9-40.7-35-55c-7.5 5.6-8.4 8-16 13z"
		/>
		<path fill="#d9d9d9" d="M846 707c1.6-1.1 159-12 159-12l-1-17-185 11s25.4 19.1 27 18z" />
		<path fill="#d9d9d9" d="M1002 754v13l-44 19-16-8 60-24z" />
		<path fill="#d9d9d9" d="M979 718v12l55 37 84 6.4 1-14.4-83-8-57-33z" />
		<linearGradient
			id="ch-full64000f"
			x1={886.9354}
			x2={736.5214}
			y1={-342.3024}
			y2={-181.0034}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full64000f)"
			d="M951 872c-36.9-.3-152.1 4.9-147 58 6.3 66-39 0-39 0s-38.5-110-49-132c30.1 16.8 97 55 97 55l107-7 19 11s5.1-.2 12 15z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full64000g"
			x1={611.9916}
			x2={1083.3848}
			y1={28.2875}
			y2={94.5375}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.396} stopColor="#595959" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full64000g)"
			d="M1281 1177c-15.5 19.2-33.4 41.7-105 55s-200.9 5.7-366-141c4 25.5 12.8 62.2 17 70 112.5 78 190.7 167.1 340 130 115.3-28.6 115.9-108.8 114-114z"
		/>
		<path
			fill="#d9d9d9"
			fillRule="evenodd"
			d="m1052 856 221-20-70 188-82 9-69-177zm158 3-34 94 16 9 34-95-16-8zm-38 1-25 72 14 10 29-82h-18z"
			clipRule="evenodd"
		/>
		<linearGradient
			id="ch-full64000h"
			x1={1254.5443}
			x2={1105.5443}
			y1={-139.5719}
			y2={-243.8649}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#323233" />
		</linearGradient>
		<path
			fill="url(#ch-full64000h)"
			d="m1131 1019 62-11 61-153-36 3 10 8-36 98-15-10 33-96h-20l-29 83-12-8 22-71-27 1-27 83-12 2 26 71z"
		/>
		<path fill="#4c4d4d" d="m1128 864 17-1-28 83-12 2s-1.5-4.2-3.9-10.8c7.8-17.5 26.9-73.2 26.9-73.2z" />
		<path fill="#323333" d="m1128 864-53 5 28 71 25-76z" />
		<path fill="#666" d="M850 464c1.1.5 40-2 40-2l-16 34s-25.1-32.5-24-32z" />
		<path fill="#bfbfbf" d="m873 498-47 62-27-40 48-56 26 34z" />
		<linearGradient
			id="ch-full64000i"
			x1={805}
			x2={805}
			y1={-526.42}
			y2={-603.8455}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full64000i)"
			d="M845 536c7.9 7.8 17 33.7 17 52-19.2 5.6-68.6 12.9-114 1 0-40.1 18.8-72 52-71 11.3 16.9 28 38 28 38s11.9-10.2 17-20z"
		/>
		<linearGradient
			id="ch-full64000j"
			x1={934.9989}
			x2={934.9989}
			y1={-124.4793}
			y2={-245.0408}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-full64000j)"
			d="M951 875c-25.8.2-146.6-4.9-146 53 12.2 56.3 163.3 70.6 196 67 18.4 0 59.6-6 64-9-11.8-9.1-34-27-34-27l-32-79-18 11-21-8s-3-4.5-9-8z"
		/>
		<linearGradient
			id="ch-full64000k"
			x1={1219.7355}
			x2={1463.8545}
			y1={-66.7279}
			y2={269.2721}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#d8d8d8" />
		</linearGradient>
		<path
			fill="url(#ch-full64000k)"
			d="M1459 1238c-78.3-33.7-177.4-244.7-241-249-1.5 3.7-5 14-5 14s70 83.8 70 172c-7.5 51.2-23.2 64.9-28 71 8.3 13.7 60.4 79 91 79 27.5-17 91.4-57.5 113-87z"
		/>
		<path
			fill="#d8d8d8"
			d="M694 1349s-1.5 66.4-2 77c-28.6-26.4-165.4-309.3-184-358-14-46.6-39.9-176.5-43-192 14.4 22.7 102.9 182.2 117 213s112 260 112 260z"
		/>
		<path fill="#d8d8d8" d="M650 695c1.4-17.5 4-39 4-39l27 8 79-17 52 204-98-54s-40.5-77.9-64-102z" />
		<linearGradient
			id="ch-full64000l"
			x1={661.637}
			x2={541.3442}
			y1={-460}
			y2={-460}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.303} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full64000l)"
			d="M694 600c-20 1.6-86.2 7-116-16-1.1 39.6-10.7 137.5 3 152 15.1-3.4 58.9-29.9 69-41 1.3-18.5 3-39 3-39s23.5-36.2 41-56z"
		/>
		<path
			fill="#e6e6e6"
			d="M578 584c10.2 8.6 72.8 23.1 116 15 14-2.7 43-9 43-9l-1-11-20-12s-55.3 26-120 1c-12.8 10-10 8.6-18 16z"
		/>
		<path fill="#dedede" d="m625 566 78-7-26-80-44 6-11-9-15 81 18 9z" />
		<path
			fill="#e6e6e6"
			d="m960 650-10-18-191 15 53 206 106-5s-14.8-16.5-16-24c-10.5-3-44.7-16.5-35-53-25.1-30-93-110-93-110l186-11z"
		/>
		<path
			fill="#e6e6e6"
			d="M801 1051c-17.9-65.8-82-288-150-354-22.7 16-50.9 39-69 39-7.5 0-8-24-8-24s-37.6 34.3-45 39c4.8 6 49.1 70.5 105 132 80.2 79.7 134.9 132.4 167 168z"
		/>
		<linearGradient
			id="ch-full64000m"
			x1={917.3376}
			x2={761.3376}
			y1={-390.6761}
			y2={-345.944}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.803} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full64000m)"
			d="m762 655 50 198 106-5s-14.8-16.5-16-24c-10.5-3-44.7-16.5-35-53-25.1-30-93-110-93-110l-12-6z"
		/>
		<path
			fill="#bfbfbf"
			d="M692 1426c16.3-1.2 97.5-13.5 103-17 7.7-7 60.7-76.9 60-84-9 3.5-104 25-160 25-2.2 21.5-3 63-3 76zm889-202c-18.1 5.4-80.6 17.7-122 14-19.9 20.1-67.8 60.4-105 84 31.2 1.1 78.7-8.7 87-13s148.8-79.5 140-85z"
		/>
		<path
			fillRule="evenodd"
			d="M584 1088s201-24.4 220-26c.3 63.7 71.5 206.7 54 261-6.7 9-152.5 31.7-162 26s-112-261-112-261zm675-79c14-2.8 160.3-19.4 185-22 49 47.7 147.3 210.4 143 231 .5 14.2-109.7 23.5-129 19-37-8.2-179.7-206.8-199-228zm-452 299-51-44-16 56 67-12zm681-92 56-7-57-36 1 43z"
			className="factionColorSecondary"
			clipRule="evenodd"
		/>
		<path
			d="m652 656 32 8 76-18 189-15-44-34s-32.2-10.2-45-10c-11.8 5.1-63.6 13.4-115 2-29.3 4.7-46 7-46 7l-47 60z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full64000n"
			x1={1069.9969}
			x2={1069.9969}
			y1={338}
			y2={168}
			gradientTransform="matrix(1 0 0 1 0 1120)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.997} stopColor="#595959" />
		</linearGradient>
		<path
			fill="url(#ch-full64000n)"
			d="M1083 1458c2.4-10.6 31-157 31-157s-34.9.7-88-13c-.1 40.6 1 105.5 4 123 10.3 21.4 19.6 42 53 47z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1254 1245c17.8 20.2 56.1 76.8 89 78s79-8 96-14c27.4-14.6 114.7-61.3 132-72s22.3-26.1 4-58-94.3-162.9-160-225-131-86.7-147-97c3.1-7.4 7.3-17.7 5-21s-88.5-53.1-100-60c-9.9.9-28.3 2.6-43 4-3-2-12-9-12-9l2-14-80-57-15 5-22-12 2-18-45-24s-5.7-14.5-10-19-34.2-28.7-47-37-38.3-7-42-7c0-12.9-.5-33.2-16-52 12.6-17.3 29-38 29-38l17-37-45 2-45 54s-54-1.8-54 70c-5.9.3-10 1-10 1l-1-10-20-13s-6.9-5-12-5c-3.6-10.1-26-82-26-82l-5-5-25-3-25 4-18 88s-9.4 5-11 7c-3.5 2-17 13-17 13l-3 127s-38.9 43.2-78.2 61.7c-19.4 8.6-25.8 11.9-30.4 21.4-9.6 17.4-11.8 30.7-6.3 57.9 6.8 34 38.3 179.7 48 211 9.7 31.3 74.4 157.3 103 216s70.4 148.6 85 148 83.9-11.9 95-15 24.9-24.4 61-76c14.2-20.5 9.4-52.8 5-76s-26-88.8-30-99c23 20.4 132.2 106.2 200 128 1.7 47.8 4 123 4 123s27.7 57.6 54 46c8.9-45.7 30-156 30-156s86.9-2.1 139.9-55z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1282 1171c-12.5 25.8-31.3 41.8-110 61-132.7 32.4-311.4-84.3-367-149"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1162 1294c9.1-12.8 15.3-33.9 15-60" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M888 1209c-4.5-20.8-2-48.8-2-55 21.3-56 285.1-124.7 381.3-56.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m909 1128-49-42 19-9 49 42" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m880 763-16-47 87 5 43 32-56 24 18 47" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M867 775c7.5 5 25.3 21.3 32 42" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M905 805c9.1 7.8 31.5 28.4 33 43" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M944 836c9.4 5.8 31.5 24.2 39 54" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1050 800 85-7 17 10-83 8-19-11zm37 22 81-7 18 10-80 8-19-11z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1014.4 863.5c7 19.2 22.5 62.3 28.3 78.4m25.3 20.8c-6.5-17-26.8-69.8-33.7-87.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1172 862-24 70 13 9 28-79" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1211.6 859-34.6 94 14 9 37-96-14.8-7.4-1.6.4z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1074 868 179-14-61 155-62 9-56-150z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1192 1007-76-61 29-83" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1098.4 933.4c8.1-23.2 24.6-70.4 24.6-70.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1052 857-82-61-29-18-73-56m-48.5-32.7C798.7 675.3 779 662 779 662"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m865 774 18-13s24.5 18.6 31 34c3-2.6 5-5 5-5s14 .6 33.1 22.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1024 1287c25.8 9.1 80.1 14.3 96 13" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1254 1245c21.9-22.8 66.8-94.2-41-246 0-.5 4.5-9.4 5-10 13.9.7 32 12.4 48 28s150.5 211.7 192 219 117-9.3 127-15.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M805.3 1060.1 582 1086l9 154m854-254s-78 9-187 21.7c-.7 19.7-2 63.3-2 63.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M730.9 1031.1c4.2-.3 9.8.8 12.5 2.7 3 2 .8 3.9-4.6 3.9-4.9 0-10.1-1.5-11.8-3.3-1.7-1.6 0-3 3.9-3.3zm534.9-57.7c3.3-.2 7.7.7 9.8 2.1 2.3 1.6.7 3.1-3.6 3.1-3.9 0-8-1.2-9.3-2.6-1.2-1.3.1-2.4 3.1-2.6z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M724.1 1018.3c13.4-1.8 29.2 3.5 35.4 12.2 6.6 9.3-.3 18.7-15.6 20.3-14.9 1.6-30.9-5-35.6-14.1-4.4-8.6 2.7-16.7 15.8-18.4zm535.3-57.3c12.4-1.6 27 3.2 32.7 11.3 6.1 8.6-.3 17.3-14.4 18.8-13.8 1.5-28.5-4.6-32.9-13.1-4.1-8 2.5-15.4 14.6-17z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M618 1082c-26.6-48.8-103.6-156.5-132.1-231.9-12.4-32.8-14-61.2-.9-73.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M575 1070s12.9-1.5 35.5-4.2m787-94c20.1-2.4 31.5-3.8 31.5-3.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1414 991c-25.5-26.7-44.4-53.4-151-113" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1462 1237c-10.4 16.3-80.4 68.5-117 87" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M858 1321c-10.8 11-155.5 38.7-164 26-5.3-7.8-56.8-145.7-120-278-39.5-82.7-94.9-150.1-115-221"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m694 1347-2 77" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m755.8 1260.9 52 48.1-67.9 10.1 15.9-58.2zm730.6-88.9 57.9 36.7-56.9 6.5-1-43.2z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M826 1161c-15.5-46.3-22-77.5-23-105-21.3-70-61.8-205.1-90-261s-55.8-92.5-63-100c-15.9 15.4-49.2 41-68 41-8.5 0-8-30-8-30"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M804 1057c-119.9-143.7-90.2-59.5-275-306" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m637.6 745.2 7.8 11.1-40.8 53.9-8-10.3 41-54.7zm16 25.6 7 11.1-36.6 50.5-9-9.3 38.6-52.3zm17.5 26.9 5.9 11.9-32.6 44.9-9-9.3 35.7-47.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1084.2 1001.5c-33.8 7.4-75.9 11.3-121 7.5-107.7-8.9-179.3-49-176-89.7 3-36.5 64.3-61.1 156.2-59.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1069.3 987.7c-29.4 8-68.7 11.2-111.5 7.7-87.5-7.2-156.3-39.8-153.5-72.8 2.6-31.2 68.1-51.6 149.1-47.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M826.7 955.1c26.3-14.8 73.1-22.8 126.7-20.2 4.7.2 58.1 5.7 73.6 13.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M922 876v114" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M703 775c-6.9-26.1-21-111-21-111l40-58-20-9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m654 657 27 7 79-19 192-13" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m722 606 44 5-8 36s49.5 187 53.7 202.9c.2.7-.7 1.1-.7 1.1l-99-57"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m810 852 110-4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m959 650-184 11 92 111-4 4 2 18s5 14.6 19 23c12.5 4.8 19 8 19 8l1 4s10.1 15.6 18 20c9.6 3.8 13 5 13 5l4-5s5.3 21 22 34c13.7 5.3 20 8 20 8l17-11 34 80 89 73 81-10 65-164"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m915 794-14 11 2 21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M941 856v-20l29-24" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1117 757-82-7-47-31 49-17" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1003 677-185 10 50 84" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1005 695-160 11 32 64" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1272 836-221 19 68 177" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1136 780-170 14 39 103" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m766 611 141-11" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M650 694s.4-30.5 2-36 37.4-53.9 45-62c18.7-2.3 46-8 46-8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M578 584c11 8.6 24 21.1 117 15" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M598 568c16.1 11.3 82.1 17.1 119-1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m703 559-79 7-17-9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m624 566 9-82 44-4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M659.8 500.4c9.6-.2 17.7 9.2 17.9 21.1.3 11.9-7.4 21.7-17 21.9s-17.7-9.3-17.9-21.1c-.3-11.9 7.4-21.7 17-21.9z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M746 588c29.8 11.9 96.7 5.9 115 0" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M747 570c14.2 4.7 22.3 5.1 32 5 5.7 7.4 15 20 15 20" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m784 523 43 55 24-34" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m846 534-19 25-29-39" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m836 547-29-39" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m847 465 26 32" />
	</svg>
);

export default Component;

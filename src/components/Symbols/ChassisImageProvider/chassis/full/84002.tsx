import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-full84002a"
			x1={1228.0504}
			x2={1252.2355}
			y1={728.1648}
			y2={825.1648}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666363" />
			<stop offset={0.397} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full84002a)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1259 1401 30-56-49-41s-28.8 4.8-66 7c12.2 12.7 85 90 85 90z"
		/>
		<path fill="gray" d="m1257 1394 24-44-41 10 17 34z" />
		<path fill="gray" d="M1232 1369s.6-7.8-3-15c-7.8-8.9-25-22-25-22l-4 1 32 36z" />
		<linearGradient
			id="ch-full84002b"
			x1={661.0435}
			x2={1305.3995}
			y1={643.9985}
			y2={175.8464}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.438} stopColor="#f2f2f2" stopOpacity={0.98} />
		</linearGradient>
		<path
			fill="url(#ch-full84002b)"
			d="M785 962c22.7 48.7 50.4 91.3 89 135s91.4 104.7 109 120 50.2 54.9 93 83 76.9 29 101 29 95.1-7.1 123-49c7.7-11.6 13.8-28.1 15.5-46.7 4.4-48.3-9-110.4-12.5-135.3-3.6-25.8-23.8-122.4-27-130s-42.3-99.3-86-141c-41.7-39.8-79.8-81.9-121-98s-97.5-22.6-172-18c-22.9 1.8-87.7-15.1-112-18-.2 11.2 1 20.1-18 19-30.2-1.7-59.1-5.5-95-10-.7 8.9 23.2 54.8 29 70 9.1 41.4 61.3 141.3 84 190z"
		/>
		<linearGradient
			id="ch-full84002c"
			x1={710.18}
			x2={767.806}
			y1={124.7552}
			y2={188.7552}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4f4f4f" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84002c)"
			d="M769 725c-10.4 0-87.8-11.4-93-12 4.6 11 29 64 29 64l92-38s-7.7-17.5-14-15c-5.6 2.2-10 1-14 1z"
		/>
		<linearGradient
			id="ch-full84002d"
			x1={923.7369}
			x2={796.3478}
			y1={443.8433}
			y2={387.2333}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.389} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#313131" />
		</linearGradient>
		<path
			fill="url(#ch-full84002d)"
			d="M790 961s56.3.7 66-1 17.6-12.6 21-26c90.7 18 1.7 117.6-8 149-39.2-36.2-79-122-79-122z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full84002e"
			x1={1326.8748}
			x2={1479.6086}
			y1={320.6935}
			y2={102.5676}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.63} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full84002e)"
			d="M1354 711s-25.1-5.6-31.1-7.1c-16.8 7.1-34.7 23.8-34.9 63.1 4.5 5.6 14.2 23.5 31 37 56.8 45.8 164 111.4 160 108-30.7-25.8-49.9-147.3 55-136-5.7-3.5-17-10-17-10l-117-58-15-6-27-8-7 4 13 12"
		/>
		<path
			fill="#d9d9d9"
			d="M1506 767c42.5 0 77 34.7 77 77.5s-34.5 77.5-77 77.5-77-34.7-77-77.5 34.5-77.5 77-77.5z"
		/>
		<linearGradient
			id="ch-full84002f"
			x1={493.8596}
			x2={493.8596}
			y1={354}
			y2={280.7514}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#6d6d6d" />
			<stop offset={0.842} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-full84002f)" d="m481 867-8 2 37 71s12.4-26.9-3-52-26-21-26-21z" />
		<linearGradient
			id="ch-full84002g"
			x1={295.2184}
			x2={505.0994}
			y1={430.8729}
			y2={197.7758}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.637} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full84002g)"
			d="M514 944c-12.4-13.1-50.2-51.7-106-33-50.3 16.9-58.1 74.4-58.2 103.6 0 5.5 8.7 12.9.6 4.5C342.1 995.5 316 941 316 941l-16-46s-15-64.5 40-93c3.4-1.8 6.8-3.3 10.1-4.7 8.3 8.7 23.9 1.5 35.2 1.6.5 0 2.6-9.1 3.1-9 29.5.7 51.8 16.8 61.6 33.2 14.6 24.3 76.4 134 64 120.9z"
		/>
		<linearGradient
			id="ch-full84002h"
			x1={684.864}
			x2={684.864}
			y1={381}
			y2={175}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.325} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full84002h)"
			d="M851 960c-6.1 0-333 7-333 7s2.5-4-1-14c-2.5-7.2.1-35.2-2-46-6.7-34-29-40-29-40s270.3-104.5 274-106c59 12.2 92.2 60.2 103 81s46.6 101.7-12 118z"
		/>
		<linearGradient
			id="ch-full84002i"
			x1={1270.2279}
			x2={1212.1389}
			y1={332.3736}
			y2={142.3736}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#6f6f6f" />
			<stop offset={0.383} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full84002i)"
			d="m1252 915 135-77s20.5-37.9-17-61c-43.9-8-315-52-315-52 69.1 29.9 140.5 97.2 197 190z"
		/>
		<path fill="#d9d9d9" d="M436 896c51.4 0 93 41.6 93 93s-41.6 93-93 93-93-41.6-93-93 41.6-93 93-93z" />
		<path fill="#8c8c8c" d="M437 920c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70z" />
		<linearGradient
			id="ch-full84002j"
			x1={1024.3533}
			x2={1304.1173}
			y1={502.4968}
			y2={406.1658}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.413} stopColor="#666" />
			<stop offset={0.64} stopColor="#adadad" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.757} stopColor="#adadad" />
			<stop offset={0.877} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-full84002j)"
			d="M1282 990c14 68.3 22 117 22 117s-101.5-33.8-177 33c-51.5 24.7-80.2-35.5-89-49-43.1-61.7 21.5-144.8 103-154 90.5-10.2 120.2 6.5 141 53z"
		/>
		<path
			d="M344.5 1008v-.5.5zm0-.5c-.9-9.3-20.6-55.4-32.5-79.5-16.2-60 12.4-93.9 51-112s89 9 89 9 51.5 95.4 63 122c-9.6-13.8-28.8-61.9-100-48-70.6 13.8-71.6 89-70.5 108.5zM1529 770c-15.2-5-73.1-5.2-91 38s2.1 78.8 15 92c-25.5-13-76-54-76-54l12-8s4-15.8 4-27-16.4-30.6-25-34c-11.7-1.9-32-4-32-4s1.7-57.3 67-61c66.6 31 105.9 46.3 126 58zm-156-16 65 34 14-15-66-34-13 15z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full84002k"
			x1={306.8273}
			x2={514.4543}
			y1={365.6761}
			y2={255.2782}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full84002k)"
			d="M345 1009c-2-20-4.2-95.1 67-109s93.4 33.2 103 47c-11.5-26.6-63-122-63-122s-50.4-27.1-89-9-67.2 52-51 112c12.1 24.5 29.4 74 33 81z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full84002l"
			x1={1333.8097}
			x2={1436.5096}
			y1={361.4055}
			y2={176.1305}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84002l)"
			d="M1531 775c-21.4-10.3-74.2-10.8-93 32-20.3 46.2 5.1 77.8 9.6 90.3-25.9-13-70.7-51.3-70.6-51.3l12-8s4-15.8 4-27-16.4-30.6-25-34c-11.7-1.9-32-4-32-4s1.7-57.3 67-61c66.6 31 107.9 51.3 128 63z"
		/>
		<path
			fill="#e6e6e6"
			d="m355.6 914.5-21.2-46.9s6.8-10.8 12.6-14.5c2.6 6.4 22 49 22 49s-6 4-8.7 6.8c-2.4 2.3-4.7 5.6-4.7 5.6zm96-29-26.2-50.9s12.2 1.4 20.6 7.5c2.3 5.8 21.6 42.2 25.4 48.9.2.4.1.7-.4.4-1.6-1-5-3.1-9-4.3-4.6-1.6-10.4-1.6-10.4-1.6z"
		/>
		<path
			d="M909.7 712.9c25.6-2.8 57.1-3.8 91.1.1 8.6 1 57.1 32.1 85.2 55 67.7 55.3 77.4 64.1 101 92 28.9 38.7 51.1 81.5 49 81-15.7-3.5-51-8.6-78-6-57.5 5.6-87.9 27.9-104.5 46.1-23.4-44.2-99.3-145.2-148.5-190.1-43.5-42.4-76.2-60.4-74-62 .6-.4-36.2 16.3-31.6 14.5-4-1.9-8.2-32.5-7.4-42.5 4.4.2 46 8 46 8l10-5 38 10s-7.4 2.3 23.7-1.1z"
			className="factionColorSecondary"
		/>
		<path
			fill="#f2f2f2"
			d="m541 668-65-212s27.1 15.8 41 49c13.9 33.2 52 164 52 164l116-27s14.3-2.5 32-4c7.9 4.5 26 16 26 16s-10.4-11.3-13-19c71.6-2.5 162-5 162-5l31-214s15.5 2 21 48c4.3 35.5 1.7 99.2-13 197-16.4 10.5-93 47-93 47l-44-7s-.8 23.5-31 21c-25-2.1-184.9-24-184-24s-38-30-38-30z"
		/>
		<path fill="#d9d9d9" d="M516 502s62.2 193.5 62 194-37-26-37-26-66.6-210.4-65-210 27.5 15.1 40 42z" />
		<path
			fill="#ccc"
			d="M633 705s136.8 16.3 137 18 15-5 15-5l9-17 39 7s82.4-41.7 82-40-1-8-1-8l-119 35-168 1 6 9z"
		/>
		<path
			fill="#8c8c8c"
			d="M1506 786.2c39-1.7 59.3 29.9 60.7 53.9 1.4 24-23.9 59.3-52 62.4-28.1 3.1-66.4-31.6-64.5-51.5.9-9.9 1.1-30.6 13.3-42.3s3.5-20.8 42.5-22.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m355.6 914.5-21.2-46.9s7.8-11.8 13.6-15.5c2.6 6.4 23 48 23 48s-8 6-10.7 8.8c-2.4 2.3-4.7 5.6-4.7 5.6zm96-29-26.2-50.9s12.2 1.4 20.6 7.5c2.3 5.8 21.6 42.2 25.4 48.9.2.4.1.7-.4.4-1.6-1-5-3.1-9-4.3-4.6-1.6-10.4-1.6-10.4-1.6z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M351.4 1029.1C343.1 1005.5 316 941 316 941l-16-46s-15-64.5 40-93c5.6-2.9 4.1-2.2 9.4-4 1.3 1.2 6.6 5.5 7.6 6.7 9.5-3.7 16.7-5 27.6-6.1.7-1.9 3.1-6 3.9-7.3 26 2.8 52.5 16.7 61.5 31.7 11 18.4 48.6 85 61.4 111.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m729 642 63 50" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m891 629 36 29" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m578 697 210-3 138-37" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m570 669 9 25" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m541 668-65-212s27.1 15.8 41 49c13.9 33.2 53 164 53 164l115-27s14.3-2.5 32-4c7.9 4.5 27 16 27 16s-11.4-11.3-14-19c71.6-2.5 162-5 162-5l31-214s15.5 2 21 48c4.3 35.5 1.7 99.2-13 197-16.4 10.5-93 47-93 47l-44-7s-.8 23.5-31 21c-25-2.1-184.9-24-184-24s-38-30-38-30z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M308 918c-11.3-44.6 18-102.3 75.4-107.9 19.2-1.9 41.6 3 66.6 14.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m514 905 352-57s9.5 17.9 15 41c-23.7 2.7-306 35-306 35s-19.6 7.5-19-26c-11.6 1.8-42 7-42 7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1073 1132c2.3-50.7 103.9-124.9 218-81" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1034 1082c4.9-16.5 21.4-42 46.6-61.1 42-31.9 110.7-54.4 201.4-23.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M829 731c127.8 54.8 389.8 471.9 354 590-2.2 6.6-4 11-4 11"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1006.3 714.4c81.7 46.1 182 123.7 228.5 224.3 63.6 137.7 91 281 65 339.1-.8 1.7-1.6 3.2-2.3 4.6l-2 4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M793 705c-.7 15.9-2.7 25 8 37" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M751 765c29-11 87-55 218-55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m373 1004 130-19" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1361 852c14.1-32.7 5.9-64.1-17-74-17.9-7.8-27-9-27-9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1211 854 117-19s13.4-4.5 11-23c14.2-.4 16.6-.8 28-1-4.6 0-186 8-186 8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1455 854 108-18" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1288 1344-55 13 26 44" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1234 1356-29-31" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1354 711s-25.1-5.6-31.1-7.1c-16.3 6.9-33.6 22.8-34.8 59.7m88.5 82.6c35.2 24.2 72.9 54.7 91.4 65.8m76-135c-5.7-3.5-24-13-24-13l-120-56-15-6-27-8-7 4 13 12"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M436 896c51.4 0 93 41.6 93 93s-41.6 93-93 93-93-41.6-93-93 41.6-93 93-93z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1388 706c-44.6 0-68.1 24.3-70 63.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1409 712c-43.6 0-69.5 23.3-72.8 61.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1506 767c42.5 0 77 34.7 77 77.5s-34.5 77.5-77 77.5-77-34.7-77-77.5 34.5-77.5 77-77.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1384 736 67 35s-10.5 6.3-17 15c-11-5-64-36-64-36s3.1-4.4 14-14z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1508.5 786.2c32.2 0 58.3 26.2 58.3 58.3s-26.2 58.3-58.3 58.3-58.3-26.2-58.3-58.3 26.1-58.3 58.3-58.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1252 915 135-77s20.5-37.9-17-61c-43.9-8-337-53-337-53"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M785 962c22.7 48.7 50.4 91.3 89 135s91.4 104.7 109 120 50.2 54.9 93 83 76.9 29 101 29 95.1-7.1 123-49c7.7-11.6 13.8-28.1 15.5-46.7 4.4-48.3-9-110.4-12.5-135.3-3.6-25.8-23.8-122.4-27-130s-42.3-99.3-86-141c-41.7-39.8-114.8-86.9-156-103-41.2-16.1-62.5-17.6-137-13-10.8.9-31-2.5-51.9-6.5m-170.5 7.8c6.3 17 21.9 47.7 26.4 59.7.7 3.2 1.7 6.7 2.8 10.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M515.9 941.5c-.2-11.3.6-27-.9-34.5-6.7-34-29-40-29-40s270.3-104.5 274-106c59 12.2 92.2 60.2 103 81s46.6 101.7-12 118c-5.7 0-291.5 6.1-329 6.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M437 920c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1282 990c14 68.3 22 117 22 117s-101.5-33.8-177 33c-51.5 24.7-80.2-35.5-89-49-43.1-61.7 21.5-144.8 103-154 90.5-10.2 120.2 6.5 141 53z"
		/>
		<path fill="none" d="M-114-138c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2z" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f3f3f3"
			d="M779 551c17.6 2.2 78.2 19.7 140 48 90.7 40.1 84 126 114 147 30 21.1 90.1 66.9 197 155 8.2 17 14 32 14 32l120 92 16-8 174 161 26 61s-360.6-330.4-463-293c3.8 6.1 26.9 38.2 32.8 47.7 35.1 57.1 67.3 127 67.3 162.3 1.9 19.4-66 40-66 40l-50 24s-13.6 5.8-72-4c-21.5-15.6-36-32-36-32l-1-12s-31.3-15.6-170-95c2.5 9.4 3.1 15.8 8 23 10.3 1.1 17-1 17-1s155.7 172.3 164 222c-9.3 19.7-14 34.9-18 39-98.4-49.6-327.4-295-388.8-378.7C509.6 878.8 583 815 583 815s-7.8-12.7-17-42c.7-62.8 41-110 41-110s-6.5-23.7 12-92c30-32.2 204.4-14.5 160-20z"
		/>
		<path fill="#d9d9d9" d="m1212 907 21-2 12 30-18 2-15-30z" />
		<path fill="#e6e6e6" d="m790 1032 41 71-58 71-56-59 73-83z" />
		<linearGradient
			id="ch-full50000a"
			x1={940.743}
			x2={601.3612}
			y1={884.1815}
			y2={884.1815}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b1b3" />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full50000a)"
			d="M760 914c26.2 27.6 69 74 69 74s95.3 112.1 142 147 68.4 47.7 85 54c-38.6-2.5-122.7-33-247-127-11.6-17.7-20-36-20-36l-14-42s-1.9-2.5-34-47-38.8-84.2 19-23z"
		/>
		<path
			fill="#d9d9d9"
			d="m657 606-34-38s-22.7 53.5-17.6 113.4c0 .4.3 16.6-6.4 25.6-8.8 5.1-15.5 8.9-20 16-2.8 21.9 16.9 34.9 17 36 .5-1.1 11.2-74.3 11.8-75.5C620.7 659.3 657 606 657 606z"
		/>
		<linearGradient
			id="ch-full50000b"
			x1={821.9413}
			x2={821.9413}
			y1={1129.2454}
			y2={1320}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.543} stopColor="#bebebe" />
			<stop offset={1} stopColor="#f3f3f3" />
		</linearGradient>
		<path
			fill="url(#ch-full50000b)"
			d="M807 628c-11.1-1-35 13.1-35 92 15.7 55.8 79.3 50.7 86 52s18 14 18 14-81.4 7-127 4c-45.6-3-79.4-31.3-77-102s82.7-88 132-88 100.3 20.8 117 59c6 13.8 9.2 25.5 10.9 34.9C960 725.9 972 752 972 752l-75-60s-3.6-56.3-90-64z"
		/>
		<linearGradient
			id="ch-full50000c"
			x1={877}
			x2={783.8571}
			y1={1151.9978}
			y2={1151.9978}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full50000c)"
			d="M861 773s-51-2.6-76-28c-.5 26-1.5 46.3-1 46s93-5 93-5l-16-13z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full50000d"
			x1={804.8607}
			x2={804.8607}
			y1={1095}
			y2={1332}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.543} stopColor="#bebebe" />
			<stop offset={1} stopColor="#f3f3f3" />
		</linearGradient>
		<path
			fill="url(#ch-full50000d)"
			d="M1024 738c-8.2 3.4-17.8-5-28-2-2 8.6-9.8 32.3-17 23-7.3-9.3-31-44-31-44s-31.9-33.5-49-85c-21.8-16.8-51.5-31-98-31-141.8 0-130 101.5-130 108s4.8 91.3 103 85c-22.2 4.2-81 29-81 29l-59-49s-18.5 30.6-39 53c-16.8-32.2-7.3-93.8 1-137 9.8-36.6 46.5-71.8 67-82s67.7-18 142-18 74 2.1 115 11c68.5 23.7 101.8 132 104 139z"
		/>
		<path fill="#656666" d="M675 731s9 34.3 25 42c-12.3 8.4-25.1 16.1-32 29-7.2-6.1-15-15-15-15l22-56z" />
		<linearGradient
			id="ch-full50000e"
			x1={748.9947}
			x2={723.0285}
			y1={1086.3242}
			y2={1147.4967}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.594} stopColor="#f3f3f3" />
			<stop offset={0.985} stopColor="#a0a0a0" />
		</linearGradient>
		<path fill="url(#ch-full50000e)" d="M792 785s-53.3 15.3-89-11c-28.2 7.7-32.4 25-32.4 25l40.7 37.1L792 785z" />
		<path
			fill="#999"
			d="m966 814-146-98s14.7 17.9 22.4 27.4c2.8 3.3-33.4 2.6-33.4 2.6s3.4 10.9 8 19c9.8 5.2 41 7 41 7l138 88-30-46zM855 707l-20-30s-4.7 19.3-4 20 24 10 24 10zm-265-8c-4.6 56.1-1 114-1 114s7.3 5.6 6 12c-1.1 5.4-10.6 11.8-12 11-1.5-.9-30.2 15.9-2-22-2-9.4-6.4-12.5-8-17-5.3-15-5-43-5-43s20.7-62.2 22-55z"
		/>
		<path fill="#d9d9d9" d="m878 693 20-3s-5.5-11.2-21-18c.3 8.5 1 21 1 21zm-47-17v21l-23 4s7-25 23-25z" />
		<linearGradient
			id="ch-full50000f"
			x1={832.1473}
			x2={832.1473}
			y1={1156}
			y2={1290.4153}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.875} stopColor="#f3f3f3" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full50000f)"
			d="M816 764c-21.3-18.5-29-123.9 75-93-18-19.9-49-45-88-41-11.5 1.2-36.4 15.5-28 101 20.8 23.5 27 26.3 41 33z"
		/>
		<path fill="#bfbfbf" d="m808 707 31 36-31-1s-10.5-23.9 0-35z" />
		<path fill="#676767" d="m1036 805-37 58-33-50 70-8z" />
		<linearGradient
			id="ch-full50000g"
			x1={1319.5}
			x2={1319.5}
			y1={667}
			y2={1063.7223}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#727273" />
		</linearGradient>
		<path
			fill="url(#ch-full50000g)"
			d="M1120 947c76.1-5.6 151.8 63.5 260 139 117.5 99.3 173 142.6 201 167-6-16.7-25.3-68-27-74-43.9-34.4-174-160-174-160l-26 16-126-97s-6.3-11.5-17-32c-81.3-62.7-129-52.2-153-42 12.2 5.3 48.4 51.6 62 83z"
		/>
		<path fill="#f3f3f3" d="m1372 1030 190 171-11-24-170-153-10 4" />
		<path
			fill="#807e80"
			d="M991 1177c26.5 9.5 94.4 30.5 165 13 4.4-4.9-35.1 52.2-124 26-14.6-6.1-29.9-23.2-41-39z"
		/>
		<path
			d="M634 773c17.9 32.5 68.8 119.1 126 192 22.4 28.5 16 46.9 32 67-16.1 18.7-74 85-74 85S595.4 972 585 959s-43.6-68.9-20-118c18.7 8.5 65-62.7 69-68zm141 401 57-71 14-3s158 170.2 165 222c-9.1 19.3-12.7 29.1-18 37-22.8 7-218-185-218-185z"
			className="factionColorPrimary"
		/>
		<path
			d="M775 791c47.2 36.1 343.1 320 349 405 27.3-1.7 72.8-17.2 93-30 1-66.4-131.8-194-257-325-31.7-22.8-78-55-78-55s-36 1.1-107 5z"
			className="factionColorSecondary"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1231 902.7c-71.4-57.4-155.8-126.6-205-160.7-34.5-101.5-71.3-128-107-143-61.8-28.3-123.8-45.6-140-48-5.7-1.3-130-12.2-160 20-11.9 44-13 69.5-15 93-11.9 27.7-22 46-22 46s-13.8 32.1-15 52c1.9 41.7 17 51 17 51s-74.4 65.8 20.2 168.3c27.5 37.6 103.3 127.3 183.8 205.7 98.8 96.3 202.5 179.3 206.3 172.3 7-12.8 8.8-17.3 18-37C1005.2 1270.9 845 1099 845 1099s-6.7 2.3-15 3c-2.5-4.8-8.7-23.7-16-35 147.3 98.4 161.8 97.3 179 106-.4 5.7 0 11 0 11l37 31s36.5 10.9 75 4c18.9-6.9 31-14 31-14l18-15s28.4-7.4 54-18c7.1-2.5 8-2.5 8-20 0-35.4-31.2-101.2-66.3-158.3-5.8-9.5-25.9-41.6-29.8-47.7 79.3-14.3 254.8 133.5 463.8 309.3 1.1.9 1.7.1 2.2-.3-22.2-48.8-34-78-34-78l-171-160-19 7-117-91c.1 0-8.3-18.9-13.9-30.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M992 1173c20.3 8 61.2 26.3 140 21" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1059 862c24.8-6.2 75.1-19.2 154 46 6.8 14.1 15 31 15 31l127 96 25-17"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1211 906c-36.4-31.5-187.7-151.5-215-166" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M637 623c17.9.1 42 9.1 43 32s-8 92.9-26 131" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m831.3 1102.3-56 72" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M790.3 1032.1c-25.2 29.3-74.1 86.2-74.1 86.2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1124 954c-36.8-63.4-99-126-99-126" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1156 1190c-144.4 33.1-228.6-94.1-375-254-12.8-14-52.6-55.8-58-54-24.7 8.3 40.5 85.5 45 91 13.1 32.8 14.2 34.5 23 59 9.5 14.9 22.2 35.3 31 48"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M972 1136c0-38.4 35.8-59.3 72.6-69.1m101.2-23.1c2.7-.3 5.3-.6 8.1-.8 3 0 20.7-.7 27.1 7.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M769 790c31.9 21.7 179.3 162.9 277.3 276.8 45 52.2 68.5 98.7 76.7 125.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M948.7 830.1c88.3 87.4 221.8 222.3 267.5 315.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M829 988c0-31 11.1-55.3 70.6-77.7m108.4-21.8c6.1-.6 12.5-1 19-1.5 22.9-1.5 42.3 1 53.3 2.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M669 799c11.8-12.8 21-20.3 35-24" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M757 959c-27.1-38.2-61-75.7-121-186-5.5-10.2-31 64-72 70"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M742 941c6.1-25.1 18.9-18.9 18-32-2.5-35.3-104.5-116.1-123-134"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M916 598c-95.4-19.6-231.5-9.7-259 10-24.7 17.7-42.9 35.9-57 64"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M606 660c-26.2 52.3-24.8 164-9 160" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M910 597c-11 2.7-13.5 13.1-9 35 25.1 22.6 46 59.3 51 105"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1032 746c-9.6-4.5-21.3-11-30-11s-17.3 19-10 32" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M672.6 725.4c10.7 47.6 46 64.6 88.4 64.6 51.7 0 106.5-4 121-4 15.4 10.1 117 78 117 78l38-62-103-81m-32.8-88.7C880.5 612.9 847 599 793 599c-23.4 0-88.3 10-112.7 54"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M678.5 691.5C702 712.9 734.6 720 775 720" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M890 791s-18.6-11.1-29-20c-40.1 0-62.6-8.8-80-32s-11.5-109 28-109 65 23 65 23 25 20.8 25 41c10.3 9.6 37 28 37 28"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M745.5 675c4.7 0 8.5 4.5 8.5 10s-3.8 10-8.5 10-8.5-4.5-8.5-10 3.8-10 8.5-10z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M626.2 569.3c5.2 12.8 23.5 30.4 30.8 37.7" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M860 644c-39.4-2.3-87.3 13.1-86 78" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M898 691c-5.9-16-25.8-25-47-25-31.4 0-72.2 46-33 98" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1033 803-71 8-155-108" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m962 810 37 51" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="m809 699 22-2 1-24s1-.5 2 1c5 7.5 23 36 23 36l22-4-5-35"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M832 697c.2.6 26 14 26 14" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m873 693 23-3" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m809 706 17 24-17 10" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M810 745h34s4.5 2.6 3.6 1.8C843.2 743.1 825 729 825 729"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m848 747 12 24" />
		<radialGradient
			id="ch-full50000h"
			cx={865.2675}
			cy={1109.1167}
			r={340.0131}
			gradientTransform="matrix(0.7393 0.6734 0.1416 -0.1555 68.5335 400.6359)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.6753} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full50000h)"
			d="M700 775s27.4 77.6 45 101c10 13.3 163.3 173.1 246 199.2 62.7 19.8 102.2-18.2 131-32.3 66.8-32.5-66.1-176.8-98-212.9-9.3 12.7-24 33-24 33l-121-77s-105 5.2-123 4-56-15-56-15z"
			opacity={0.3}
		/>
	</svg>
);

export default Component;

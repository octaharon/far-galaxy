import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1006 1187s78.3 94.6 111.8 133.5c5.1 5.6 15.9 16.4 27.1 13.8 19.1-4 58.1-11.6 80.2-16.3 9.7-2.2 19.7-5.8 17.1-22.7-4.7-49.4-17.2-179.3-17.2-179.3l-142-184-30 1-116-176 44 35s24.4 15.1 57 4c27.8 20.9 201 147 201 147l45-76-195-153s-2.7-35.2-32-52c-8.8-12.3-29.4-27.7-44-34-29.3-12.6-85.7-23.1-153-18-53.2 3.4-62 8-62 8v-7l-10-8s-27-8.1-39 1c-5.6 4.5-10 9-10 9v21l-18 7s-8.2-7-57-7-47.8 21.5-46 30 42 131 42 131l27 34 80-12-1-77 34-5v13s-9.3 5.6-8 6-2 96-2 96l-34 85-3 55-11-12-115 14s-3.5 100.7-5.2 148.4c-1.3 18.2 4.3 34.4 10.7 45.8 26.6 48.6 91.5 167.5 115.4 211.3 3.4 6.5 9 16.6 24 13.4 22.5-4.7 64.9-13.5 88.3-18.4 8.5-2.2 16.3-7.3 14.9-19.2-5.6-50.5-23.1-205.3-23.1-205.3s37 27 101 27c30.8.2 53-8 53-8z"
		/>
		<radialGradient
			id="ch-full25000a"
			cx={877.523}
			cy={13.9509}
			r={1016.608}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d1d1d1" />
			<stop offset={0.208} stopColor="#d1d1d1" />
			<stop offset={0.302} stopColor="#e6e6e6" />
			<stop offset={0.438} stopColor="#dedede" />
			<stop offset={0.682} stopColor="#4f4f4f" />
		</radialGradient>
		<path
			fill="url(#ch-full25000a)"
			d="M887 1134c19.6 7.9 81.8 28.6 137 14 55.2-14.6 81.6-58.6 81-74s8.3-71.1-72-151c-31.3-31.5-56.9-63.1-96-155-6.7 3-34.1 28-110 10 8.1 44.9 16.3 90.8 15 130s-1.3 106.2 6 126c7.3 19.8 25.4 90.1 39 100z"
		/>
		<path fill="#e6e6e6" d="m885 1134 37-70 114-16s40 17.6 64 45c-17.3 19.8-45.1 44.8-104 50-59 5.2-111-9-111-9z" />
		<path
			fill="#ccc"
			d="M1101 1089c-4.3-18.9-28-43.5-67-78-40.5 5.5-131 21-131 21s-19.9 18.7-18 98c7.5-14.8 36-46 36-46l115-15s27.7 4 65 20z"
			opacity={0.8}
		/>
		<path fill="#1a1a1a" d="M975 1031c6.3-.8 26.4 23.3 3 31s-14.6-34.1-3-31z" opacity={0.302} />
		<path
			d="M938 769c3.3 13.5 42 111.5 113 167-32.1-54.3-59-93-59-93s-57.3-87.5-54-74zm-55 363c-11.9-26.2-37.8-76.4-42-137s7.9-96.7-13-216c-13.8-5.8-29.7-13.9-35-24-.5 24.5.2 104.4-11 127s-26.3 55.9-27 79-2.7 57.6-2 67c7.2 15.8 51.8 86.4 130 104z"
			className="factionColorPrimary"
		/>
		<path d="M938 769c3.3 13.5 42 111.5 113 167-32.1-54.3-59-93-59-93s-57.3-87.5-54-74z" opacity={0.2} />
		<radialGradient
			id="ch-full25000b"
			cx={935.843}
			cy={231.3931}
			r={377}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" stopOpacity={0} />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-full25000b)"
			d="M883 1132c-11.9-26.2-37.8-76.4-42-137s7.9-96.7-13-216c-13.8-5.8-29.7-13.9-35-24-.5 24.5.2 104.4-11 127s-26.3 55.9-27 79-2.7 57.6-2 67c7.2 15.8 51.8 86.4 130 104z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full25000c"
			x1={822.9572}
			x2={757.9572}
			y1={251.5573}
			y2={340.2063}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25000c)"
			d="M793 756c6.3 5.7 17.3 20.5 34 24 3.7 20.8 15 84 15 154-22.2-12.8-65-39-65-39s14.4-7.6 16-139z"
			opacity={0.149}
		/>
		<path
			fill="gray"
			d="m623 1139 136 236 119.7-25s1.6 19.7 2 23 3.1 14.8-18 20-89.4 18.4-94.7 19-13.3-.5-23-17-112.4-207.6-117-216-5-40-5-40zm398 40 112 124 108-22s1.2 16.2 1 20-3.5 13.6-15 17-79.1 17.4-85 17-15.3-1.5-25-14-111-133-111-133l15-9z"
		/>
		<path fill="#ccc" d="m629 995 98 193 30 184-134-237 6-140z" />
		<path fill="#b3b3b3" d="m638 681 46 145-24-29s-40-127-41-128 19 12 19 12z" />
		<path
			fill="#ccc"
			d="m639 681 125-13 1 150-77 12-49-149zm100-66c8.3 4.2 42 7 58 0 .3 7.1 1 15 1 15s-17.5 15-31 15-24.3-6.3-27-10-.9-12.4-1-20z"
		/>
		<linearGradient
			id="ch-full25000d"
			x1={862.9253}
			x2={865.6853}
			y1={147.4727}
			y2={197.4727}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={0.499} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-full25000d)"
			d="M800 736s69 10.3 119-5c6.5 11.4 14 25 14 25l1 14-38 11-70-3-31-24 5-6v-12z"
		/>
		<linearGradient
			id="ch-full25000e"
			x1={888.3524}
			x2={876.6384}
			y1={113.1937}
			y2={-18.8413}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.5} stopColor="#f3f3f3" />
		</linearGradient>
		<path
			fill="url(#ch-full25000e)"
			d="M766 741c1.2-.2 37-6 37-6s88.6 13.8 136-12c18.6-33.7 36.6-83.5 102-76-49.3-28.4-97.8-51.5-243-29-.3 4.3 0 11 0 11s-12.4 15.2-31 15-26.5-6.9-29-9c-4.6 1.3-12 7-12 7l36 24s2.8 75.2 4 75z"
		/>
		<linearGradient
			id="ch-full25000f"
			x1={992.6242}
			x2={981.2422}
			y1={147.7135}
			y2={19.4244}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={0.5} stopColor="#f3f3f3" />
		</linearGradient>
		<path
			fill="url(#ch-full25000f)"
			d="M917 730c2.5 4.1 15 24 15 24s14.1 10.5 27 20c-21.9-28.8 10.3-143.4 103-107-10.8-10.4-22-21-22-21s-41.6-5.1-70 27-21.4 57.6-53 57z"
		/>
		<linearGradient
			id="ch-full25000g"
			x1={925.5363}
			x2={976.5363}
			y1={134.6889}
			y2={145.4268}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full25000g)"
			d="M919 730s25.5 5.9 39-44c8.1-12.8 12 10 12 10s-27.2 36.3-12 78c-11.9-8.5-26-19-26-19l-13-25z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full25000h"
			x1={968.9023}
			x2={1048.1263}
			y1={213.8353}
			y2={73.0053}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.5} stopColor="#dedede" />
		</linearGradient>
		<path
			fill="url(#ch-full25000h)"
			d="M1010 800c-78.3-67.6 22.4-152.8 74-98-7.7-26.6-23.8-41.5-35-42s-60.4-9.2-86 44 2.6 79.7 13 86 20.9 8.5 34 10z"
		/>
		<linearGradient
			id="ch-full25000i"
			x1={1176.2313}
			x2={1255.2313}
			y1={261.9948}
			y2={317.3109}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#666" />
		</linearGradient>
		<path fill="url(#ch-full25000i)" d="m1203 879 79-10-43 76-36-66z" />
		<path fill="#a6a6a6" d="M1199 878 993 727l15 30 17 10 8 11s-14.5 11.3-22 18h23l205 146-40-64z" />
		<path fill="#dedede" d="m851 1169 28 183-121 26-32-193 125-16zm261-35 112-15 15 163-104 20-23-168z" />
		<path fill="#dedede" d="m1063 691-1 35-22 3-23-39s27-11.4 46 1z" />
		<path
			fill="#ccc"
			d="M1065 690c5.6 1.9 22.3 15 24 24-13.3.5-24-1-24-1s-5.6-24.9 0-23zm-48 0 14 26-39 4s3-8.8 9-16c6.5-7.7 16-14 16-14zm-27 37 18 28-22 13s-11.1-28.2 4-41z"
		/>
		<path fill="#a6a6a6" d="M1025 768c1.2-.1 6 11 6 11l-21 18s-21.6-22-21-22 34.8-6.9 36-7z" />
		<path fill="#a6a6a6" d="M1008 755c.4-.2 14 11 14 11l-33 8-1-7s19.6-11.8 20-12z" opacity={0.6} />
		<linearGradient
			id="ch-full25000j"
			x1={1096.1835}
			x2={985.1835}
			y1={608.0876}
			y2={554.2607}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full25000j)" d="m1022 1179 111 122-22-167-19-31-27 20s-16.9 38.8-43 56z" />
		<linearGradient
			id="ch-full25000k"
			x1={676.7731}
			x2={932.0034}
			y1={552.9801}
			y2={565.1501}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000004" />
			<stop offset={1} stopColor="gray" />
		</linearGradient>
		<path
			fill="url(#ch-full25000k)"
			d="M1072 1119s-29.6 18.2-63 23-90.3 4.3-123-8-69-32-69-32l35 70s51.8 24 110 24c65.2 4.1 110-77 110-77z"
		/>
		<path
			d="m700 985 28 48-10 25 67 120-60 7-94-192 69-8zm355-50 29-1 140 183-46 8-82-106s-4.6-19.8-14-39c-11-22.5-27-45-27-45z"
			className="factionColorSecondary"
		/>
		<path
			fill="red"
			d="M728.4 687.9c10.6 1.7 23 6.2 21.1 20.1s-4.8 21.7-19.2 21.3c-14.3-.3-20-8.8-20.3-21.8s11.7-20.7 18.4-19.6z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				begin="0.5s"
				dur="2s"
				repeatCount="indefinite"
				values="#F00;#F33;#C00;#F30;#F30;#f00"
			/>
		</path>
		<path
			fill="red"
			d="M677.2 695.9c7.1 1.1 15.3 3.8 14 12.4-1.3 8.6-3.2 13.3-12.7 13.1-9.5-.2-13.3-5.4-13.5-13.4-.2-8.1 7.7-12.8 12.2-12.1z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				dur="1s"
				repeatCount="indefinite"
				values="#F00;#F33;#C00;#F30;#F30;#f00"
			/>
		</path>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1006 1187s78.3 94.6 111.8 133.5c5.1 5.6 15.9 16.4 27.1 13.8 19.1-4 58.1-11.6 80.2-16.3 9.7-2.2 18.5-18.3 15.9-35.2-4.7-49.4-16-166.8-16-166.8l-142-184-30 1-116-176 44 35s24.4 15.1 57 4c27.8 20.9 201 147 201 147l45-76-195-153s-2.7-35.2-32-52c-8.8-12.3-29.4-27.7-44-34-29.3-12.6-85.7-23.1-153-18-53.2 3.4-62 8-62 8v-7l-10-8s-27-8.1-39 1c-5.6 4.5-10 9-10 9v21l-18 7s-8.2-7-57-7-47.8 21.5-46 30 42 131 42 131l27 34 80-12-1-77 34-5v13s-9.3 5.6-8 6-.4 68.1-2 96-22 63-28 79c-6.3 16.7-8 61-8 61l-12-12-115 14s-3.5 100.7-5.2 148.4c-1.3 18.2 4.3 34.4 10.7 45.8 26.6 48.6 91.5 167.5 115.4 211.3 3.4 6.5 9 16.6 24 13.4 22.5-4.7 71.9-15.5 95.3-20.4 3.9-1 16.8-6.7 13.7-29.2-5.7-50.5-29-193.3-29-193.3s37 27 101 27c30.9.2 53.1-8 53.1-8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m933 756-16-24" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1045 649c-79.9-13.2-99.3 68.2-105 73s-9 8.5-54 15-85-2-85-2m84 4c-.9-15.2-.2-55.4-5-78s-11.3-45.7-34-50"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M963 779c-30-36.5 8.1-140.7 97-115m27 45c-7.8-11-18.7-24.7-49-25s-68.3 40.5-54 81 43.4 33 48 32c2.1-.5 5-1 5-1l-9-14 4-4-7-12-16-11s-17.6-26.5-20.5-31.3c-.3-.5.5-.7.5-.7l210 156 85-11"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m990 720 35-3 15 11 22-2v-13h25m-24-23-1 20m-24 15-21-34"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1199 879 39 61" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1008 756-21 11m5 8 31-8m5 13-19 15" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m766 740-3-75-41-23" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M798 621v7s-11.2 16-29 16c-23.7 0-30-9-30-9m3-20c5.8 4.1 34.5 7.6 53 0"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m760 668-121 12 47 147m13 0 4-28 42-5 18 24" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m640 682-21-18" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M729.5 686.4c12.2 0 22.1 10.1 22.1 22.6s-9.9 22.6-22.1 22.6-22.1-10.1-22.1-22.6 9.9-22.6 22.1-22.6zM678 696.7c6.8 0 12.2 5.5 12.2 12.3s-5.5 12.3-12.2 12.3-12.2-5.5-12.2-12.3 5.4-12.3 12.2-12.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1094 1103s-33.3 27.9-75 36c-48.8 9.5-106.5 3.3-133-5-22.1-6.9-60.3-21.5-71.4-34"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M793 754s5.8 8.6 14 15c7.8 6.1 18 10 18 10s37.4 7.3 64 5c26.8-2.3 45-16 45-16v-14"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m885 1130 46-48 99-15s50.3 10.3 71 26" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M975.5 1030c7.5 0 13.5 7.2 13.5 16s-6 16-13.5 16-13.5-7.2-13.5-16 6-16 13.5-16z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m917 1029 15 53m97-17-5-54" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M883 1131s15-77.9 31-104c20.9-3 110-16 110-16s50.9 23.8 78 72"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1072.1 963.1c-36.9-46.5-72.1-74.8-90.1-102.1-19.3-29.2-46-93-46-93m-109 12s10.8 64.8 14 99-2.4 109.4 5 146 37 108 37 108"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m853 1171-39-71s-31.7-29-43-43-18-30-18-30v-41" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m630 993 95 191s25.2 137.8 33.9 202.7c1.8 15.8-6.9 19.3-6.9 19.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m622 1132 136 245s50.1-13.5 119-27m364-70s-46.4 9.1-110.6 21.7C1093.1 1260.1 1022 1179 1022 1179"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M793 1407c7-2.4 14.3-15.9 12-40m28 32c12.4-12.7 11.5-26.6 11-42m330-63c0 10.3.4 32.8-11 37m37-9c5.8-2 9-12.8 9-35"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1002 1189c15.6-4.2 50.1-37.3 71.5-71" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M786 1177s-36.8-66.3-67-120.8c3-7.8 6.4-15.7 8.4-22.7C712.2 1006.3 701 986 701 986m399.4 37.8c23.3 30.6 74.7 98.2 74.7 98.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1225 1118s-48.8 6.6-115.8 15.8m-257.4 35C779.1 1178.6 725 1186 725 1186"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1049 928s29.2 42.3 42 72c8.6 20 18.7 41.7 12 83-2.2 13.5-10 21-10 21l18 31s18.3 129.4 24 179c1.3 11.5-7 21-7 21"
		/>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1547 1009.5c-9-22.5-24-29.8-64-34.5s-63 14.3-63 14.3l-33 52.7-3.8 43-49.9-4.2s12.7-106.4 2.8-158.5c-9.9-52.1-83.1-62.2-83.1-62.2l-5.2-5.5s-6.8-1.6-18.8-4.5c-34-185.3-180-236.1-200.7-245.9-8.7-18.6-11-12.8-11-12.8l.7-76.4 18.3-2.6 90.7-26.7 1-62.7s-81.9-23.4-82.8-23.7S982 402 982 402l-69 38.5V500l30 7-4 61.3-23.1 8.5L892 589s-1.6 3-2 7c-21.3 4-75 33-75 33l-13-373.4h-8l1 322.3-20.9-15.9-6.6-124.8 5.8-3.3s.1-16-.3-33c-8-8.7-17 0-17 0l-1.7 33.3 6.7 4.7s-1 47-4 79.2c-43.7-74.1-68.1-58.8-77-54.2s-15.7 9.3-6 44.8 49.7 96.9 49.7 96.9l6.5 7.2L759 656s1 14.7.3 24.2C749.3 690.8 702 771 702 771l-19.1-4-6 4c-77.3 2.7-74.8 76-75.7 90s2.7 73 2.7 73l-24-2.7-1-58.3s-20.9-37.6-26.6-44.9c-5.7-7.3-19.7-16.1-56.7-13.9-37 2.2-53 15.8-53 15.8L422 882l11 163 45.5 45.5s8.3 51 14.5 87.5c26 19.3 48-2 48-2l4-88 38-51v-15s58.2 13 85 21c3.7 12 15.3 34 27.4 47-52.7-2.3-80.4 22.6-80.4 22.6l-32.9 55.7L592 1384l53.7 56.4s9.8 53.3 19.4 104.6c29 26.7 54.8 1.1 54.8 1.1L734 1435l48-60-1-104 110-119s39 2.1 99 2.1c52.7-4.3 83.3-10.7 92-11.4s39.6-10.6 62-18.8l234 53.9v48.2l39 63 3 99s23 29 55 2c5-30.7 22.4-94.3 22.4-94.3l48.6-45.7 22-192s-12-26-21-48.5zM1049.7 420l61 14.3 1.6 39.9-62.6 19.8v-74zM691 960l-38.3-6.3c0-1-4.7-113.8-4.7-113.8s1-21.9 23.2-20.2c5.8 2.5 8.8 6.9 8.8 6.9s-5.7 22.4-3 28.1c2.7 5.7 15.5 27.2 15.5 27.2L691 960zm288.5-519.3C949.3 444.3 947 483 945 495l-19.3-4.3 1-46 52.8-25.9v21.9zM1280 1073l-43-11.4 4.1-16.6s8.6-63-30.4-81.3c.3-20.7.3-39.3.3-39.3l9.7-10.3 14.7-3c11.3 2 44.7 4 49.3 32.7 4.6 28.5-4.7 129.2-4.7 129.2z"
		/>
		<path
			d="M888 614c1.1 4.2 8.9 8.9 19 15-90.6 106.5-117 279-117 279s-6.7 8.8-12 12c-28.3-5-62-26.1-82-39s-16-25.8-18-48c25.5-85.1 82-150 82-150s-.1 23.6 0 42c16.8 7.7 19-10 19-10s-2.7-37.2-1-58c20.1-25.3 81.4-51.5 112-58-1 8-1.9 8.5-2 15zm129 11c162.5 118.9 171 234.8 178 271-99.7 61.2-291.4 44.1-343 38 8.2-148.6 67.3-275.7 82-302 30-.5 60.9-4.2 83-7zm192 174c-25.6-73.7-127.8-155.4-147-171 75.3 22.4 140 122.3 147 171zm-238-26c-8.2 16.6 27.9 33 40 47s21.2 53.7 41 51 9.1-26.6 20-50 24.5-55 16-64-40.4 7.4-55 9c-14.5 1.6-50.9-15.7-62 7z"
			className="factionColorPrimary"
		/>
		<path
			d="M676 1090c20.5 0 63.3 6.2 76 24s24.9 40.7 29 50c-12.3 52-172.1 74.6-198 4 14.8-27.5 30-53 30-53s17.9-25 63-25zm-23 27c0 17.7 54 13.4 54-1 0-10.6-54-16.7-54 1zM443 835c9.8-18.2 88.8-33.4 111-5 14.1 18 17.7 27.7 25 38-1.5 22.3-111.5 49.4-158 11 7.9-16.4 12.7-26.7 22-44zm76-4c0-10.7-41-5.8-41 1s41 9.7 41-1zm964 144c24.3 2.9 55.5 10.5 66 34s13.5 32.9 19 48c-11.8 21.2-115.5 52.4-181-15 14.9-25 34.4-75.5 96-67zm-27 20c-1.7 13 49.7 15.5 51 6s-49.3-19-51-6z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-full99000a"
			x1={1307.4088}
			x2={1307.4088}
			y1={524.109}
			y2={336.2586}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full99000a)"
			d="M1276 930c6.6-12.1 45.1-28.5 55-24 5.5 15.8 12.8 20.1 3 174-8.2 9.5-39.1 26.7-55-6 5.3-98.7 13.7-123.1-3-144z"
		/>
		<linearGradient
			id="ch-full99000b"
			x1={772.7832}
			x2={772.7832}
			y1={610.49}
			y2={405.0883}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full99000b)"
			d="M746 978c6.6-12.1 51-1.5 61 3-13.4 22.7-8 37.2-10.8 186.9-2.7 3.2-8 7.2-14.4 9.9.2-4.2.2-9.9-2.8-15.9-11.3-23.1-21.5-37.2-27-45-.4-5.5-12.8-11.6-13-17-1.7-56.5 1.8-101.9 7-121.9z"
		/>
		<linearGradient
			id="ch-full99000c"
			x1={628.4229}
			x2={628.4229}
			y1={393.588}
			y2={221.5149}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full99000c)"
			d="M618 795c6.6-12.1 36.1 14.2 37 32-13.4 22.7-2.8 51.8-3 125-19.6 24.6-42.6 3.3-49 6 1.6-42.4-9.5-120.4 15-163z"
		/>
		<linearGradient
			id="ch-full99000d"
			x1={1042.6708}
			x2={1042.6708}
			y1={477.3521}
			y2={326.3521}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.499} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full99000d)"
			d="M1001 1085c-47.5-120.4 82.1-149.9 119-115-2.6-10-25.2-34.2-46-36-21.8 1.5-62 8-62 8s-73.5 34-36 130c11.7 6.7 17.4 9.4 25 13z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full99000e"
			x1={614.2955}
			x2={619.9846}
			y1={489.2043}
			y2={376.8993}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full99000e)"
			d="M577 1022c15 5.1 87.7 19 96 21-1.5-15.2-4.7-62.6 14-80-26.8-7.8-34-10-34-10s-16.9 21.5-49 4c.4-13.6-1-22-1-22s-16.4-5.7-26-4-50.3 49 0 91z"
		/>
		<linearGradient
			id="ch-full99000f"
			x1={1250.6783}
			x2={1273.0323}
			y1={621.6718}
			y2={506.6718}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full99000f)"
			d="m1231 1064 47 7s17.8 44.6 58 9c22.9 3.4 46 8 46 8l-3 91-235-54s73.1-30.1 87-61z"
		/>
		<linearGradient
			id="ch-full99000g"
			x1={1235.8524}
			x2={1159.3274}
			y1={541.3636}
			y2={453.3317}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full99000g)"
			d="M1145 1125c15.5-5.5 69.5-30.2 88-60 12.8-.6 46 7 46 7l-19 81s-130.5-22.5-115-28z"
		/>
		<linearGradient
			id="ch-full99000h"
			x1={922.5829}
			x2={827.5829}
			y1={527.7886}
			y2={576.1935}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full99000h)"
			d="M891 1152c-18.4-.8-82.4-13.6-95-22 .7 17.5 2.4 30.2 3 36s42.1 27.9 57 30c9.9-10.5 27.8-31.2 35-44z"
		/>
		<linearGradient
			id="ch-full99000i"
			x1={898.7891}
			x2={785.7891}
			y1={683.1611}
			y2={575.9283}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full99000i)"
			d="m781 1271 113-120s-75.7-7.2-96-20c.5 15.2 3.4 47.2-16 49 .2 16.6-1 91-1 91z"
		/>
		<path
			fill="#ccc"
			d="M443 1053c12.2 7.4 91.9 20.2 129-3-12.8 20-27 38-27 38s-32.5 16.9-70 2c-11.7-14.1-44.2-44.4-32-37zm168 349 37 41s55.2 17.4 86-8c14.4-18.6 28-38 28-38s-82.8 36-151 5zm779-155c13.1 8.7 75.1 35.1 139 15-13.8 13.5-31.4 32.6-34 35-6.8 1.2-53.2 9-77-8-12.1-16.7-41.1-50.7-28-42z"
		/>
		<path
			fill="#e0e0e0"
			d="M477 1093c14.4 5.9 54.6 5.2 70-5-1.1 25.1-5 79.5-6 88-6.7 7.5-24.3 22.3-48 2-4.1-23-12.1-65.2-16-85zm170 349c13.1 6.9 60.8 8.8 85-5-.5 34-9.5 99.2-11 110-10.5 7.9-39.8 21.2-56-4-5.8-26.4-16.9-89.7-18-101zm770-153c9.5 7 61.9 16.7 79 8-5.9 26.5-18.3 83.3-21 93-8.4 9.8-42.4 18.4-56-6-1.2-26-1-84.2-2-95z"
		/>
		<path
			fill="#d9d9d9"
			d="M582 1173c7.7 42 153.6 61.7 199 1-1.5 50.3.9 189.7 1 202-10.5 22.9-119.3 68.1-189 11-3.1-61.6-15.7-239.6-11-214zm-4-296s1.6 44.9 1 55c-45.1 11.8-18.5 95.3 4 92 1.8 4.1 2.9 4.6 2 14-59 42.8-136.6 21.8-151 7-1.1-12.2-11.1-144.9-12-161 79.2 32.9 156-7 156-7zm990 186c-2.4 20.4-16.3 126.4-22 187-87.6 41.8-159.1-.8-168-24 .2-19.8 8.1-171.4 8-181 71 66.4 163.1 28.1 182 18zm-900-56s1.6 6.4 3 14.1c-1.4-4.4-2.4-9.1-3-14.1zm3 14.1c14.3 46.4 67 62.9 67 62.9s.4 11.7 0 20c-5.2-4.9-24.4-10-41-13-17.1-9.6-27-43-27-43s3.3-3.8 3-11c-.2-4.8-1.1-10.7-2-15.9zm127 84.9c239.8 64.9 412.4-16 440-77 .7 15.8 1.3 15.6 1 25-16.3 35.6-152.2 148.1-443 77 .1-12.1.6-18 2-25zm-19-184c-34.6 23.2-39.2 90.9-41 133-37.7-13-45-44-45-44l-2-129s59.6 31.3 88 40zm72 14c24.5 6.7 102.1 7.1 169 6-82.3 27.1-52.5 120.6-45 127s23.4 17 41 34c-99.6 3.9-163-9-163-9l-11 7s-30.4-6.8-46-11c-3.8-115.7 22.5-126 33-129s15.8-17.1 22-25zm235-3c28.2-4.7 85-20.5 118-40 1.6 6.7 10.8 14.3 12.3 21 1 4.7-5.3 10-5.3 10s-.4 74.8 0 93c-9.3 19-26.7 44.8-86 62 22.2-46.4 40-88.2-39-146zM762 441l-4-6-1-31s4 3 8 3 8-3 8-3l1 30-5 7s8.4 242.3 9 264-9.5 21.6-16 16 0-280 0-280zm33 201V259h7l13 370-20 13zm227-42s8.2 8.3-10 22c-18.2 13.8-73 9-73 9l-11-17-21 12-17-11s-.6-7.3 0-19c81.3 34.6 127 1 127 1l-16-16 6-64h-26l-69-16 1-62 70-38 63-3 81 25v62l-83 24-24 5-2 75 4 11zm27-106 64-20 1-39-63-15-2 74zm-68-76-56 26 1 48 20 3s2.3-55.3 35-53c.5-9.7 0-24 0-24zm-53 203s3.8 4 5 12c-77.5 156.3-79 300-79 300s-14.9 8.5-11 6c-.9-180.2 85-318 85-318zm113 9c135.3 101.1 147.9 173.9 165 259-2.9 4.7-11 7-11 7s-8.5-135.3-160-262c2-1.5 2.3-3.4 6-4z"
		/>
		<linearGradient
			id="ch-full99000j"
			x1={846.9572}
			x2={835.6641}
			y1={85.5183}
			y2={-129.9727}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.797} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f1f1f1" />
		</linearGradient>
		<path
			fill="url(#ch-full99000j)"
			d="m796 641-2-60-20-21 5 96 17-15zm-37 15-30-45s-88-125.7-49-147c39.1-21.4 79 58 79 58v134zm180-86 4-63 38 11 25-2-5 64s-36.5-12.4-62-10zm43-68-1-61s-25.1-9.3-36 53c23.7 6.7 37 8 37 8z"
		/>
		<linearGradient
			id="ch-full99000k"
			x1={823.8134}
			x2={823.8134}
			y1={421.444}
			y2={346.0938}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path fill="url(#ch-full99000k)" d="m848 946-15 16s-17.5-7.4-33 36 50 13 50 13l-2-65z" />
		<path fill="red" d="M1075 954c19.9 0 77.7 17.9 70 77s-69.3 77.3-78 78-73.8 11.3-76-68 76.9-87 84-87z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="#F00;#F33;#C00;#F30;#F30;#f00"
			/>
		</path>
		<path
			fill="none"
			stroke="#fff"
			strokeLinecap="butt"
			strokeWidth={12}
			d="M1076 983c12.5-1.2 39.8 9.9 39.8 40.3 0 34.5-26.7 53.2-48.2 56.8s-48.7-7.1-48.7-40.1c.1-36.8 35-54.8 57.1-57z">
			<animate
				fill="freeze"
				attributeName="opacity"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="1;1;0.8;0.8;0.6;1"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dashoffset"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="0; 50; 0"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dasharray"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="5 10 5; 5 5 5; 5 25 25; 5 10 25; 5 5 5; 5 10 5"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-width"
				attributeType="XML"
				dur="8s"
				repeatCount="indefinite"
				values="10;5;10"
			/>
		</path>
		<radialGradient
			id="ch-full99000l"
			cx={956.5}
			cy={1419.7548}
			r={413.0154}
			gradientTransform="matrix(1 0 0 -0.5941 0 1543.8929)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#000" stopOpacity={0.3} />
		</radialGradient>
		<path
			fill="url(#ch-full99000l)"
			d="M1030 607c134.8 54.1 186.2 169.7 198 243-8.3.4-21.6-6.3-24 43-61.3 27.9-120.8 59-355 42-6.3-3.7-12.6-26.6-30-30s-24.5.2-39 17c-18.3-1.8-113.8-40-102-80 13.7-46.6 47.6-130.1 80-157 1.4 13.8 1.6 37.2 4 39s15.8 3.1 17-10-1-57-1-57 40.6-41.7 110-58c1.9 9.8 3 18 3 18l16 9 15-13 7 1 6 15s55.8 6.9 84-9c3.4-2.2 5.3-7.1 11-13z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M760.9 678.1c-12.3 16.6-40.5 56-57.9 90.9-8.1-.7-20-2-20-2s-3.2.6-5 4c-99.1 0-74 115.6-74 163-11.2-2.3-23-3-23-3l-3-65s-21.7-34.3-26-40c-2.4-9.1-82-25.8-110 8-8.7 15.8-21 47-21 47l12 164 44 45 17 89s26.9 19.4 47-3c4.4-37.7 7-92 7-92l35-47v-15l85 19s8 32.5 31 50c-27-1.7-67.4-1.9-86 25-17.5 29.7-31 51-31 51l9 216 54 56 20 106s27.3 26 56 0c3.5-31.3 15-114 15-114l46-56-1-103 114-120s129.3 14.2 246-27c87.5 24.2 238 53 238 53l-2 49s26.2 45.7 39 58c2.7 22.1 4 103 4 103s11.1 12 26.8 13.5c8.5.8 20.6-1.5 28.2-10.5l23-98 49-46 21-191-22-55s-11-20.7-61-26-63.1 13.7-66 18-32 48-32 48l-4 46-49-8s2.3-60.8 6-122c3.3-54-28.2-88.2-87-97-1.5-3.9-3-5-3-5l-21-5s-21.9-178.6-205-245c-.8-7.1-3.2-12-6-14 0-6.6 1-78 1-78l25-4 83-24 1-62-84-24-62 3-69 36v62l31 8-5 62s-12.4 2.5-19 5-28 14-28 14-3 4.5-3 9c-7.5 1.7-39.3 9.1-73 30l-3-14s-9.8-357.8-10-361c-4.5-3-9 0-9 0v330l-21-25-5-120 5-3v-35s-12.7-4.1-17 1c-.2 7.6-.1 32.8 0 33s5.2 3.9 5 5-1 85-1 85-39.7-82.5-78-62 25.6 117.3 45 149c4.2 2.8 6 5 6 5s18.2 29 28 44.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M582 931c-32.8-2.8-44.4 85.6 3 92" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M580 917c-40.1-11.7-72.6 110.8 4 118" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1031.7 765.3c15.7-.6 48.8-17.6 58.1-6.6 9.3 10.9-14.1 47.5-18.6 63.9s-1.8 43.9-19.7 48.5c-17.8 4.6-32.9-42.4-42.7-52.9-9.9-10.5-48-29.8-39.5-47.4 8.5-17.6 46.7-4.9 62.4-5.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M995.2 774.1c4.1-.7 8 2.6 8.8 7.2.7 4.6-2 8.9-6.1 9.6-4.1.7-8-2.6-8.8-7.2-.8-4.7 2-8.9 6.1-9.6zm72.1-4.6c3.8-.6 7.3 2.3 8 6.4.7 4.1-1.8 8-5.6 8.5-3.8.6-7.3-2.3-8-6.4-.7-4.1 1.8-7.9 5.6-8.5zm-21 70c3.8-.6 7.3 2.3 8 6.4.7 4.1-1.8 8-5.6 8.5-3.8.6-7.3-2.3-8-6.4-.7-4.1 1.8-7.9 5.6-8.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m794 581 2 61 22-15" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M799 640c-8.5 4.9-16.8 12.7-21 17" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1250 857c-12.4 2.1-25.1 48.2-13 55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M798 1131c18.1 8 74.6 20.1 99 21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M689 964c-12.2 7.8-56.1 69.8 46 118" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M604 934c-.2 10.5.1 18.5 0 23 1.1 2 27.1 14.6 47-2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M680 769c-6.2 10.2-12.7 30.3-6 54" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M705 765c-6 11.3-21.9 49.4-25 67" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M935 571c15.5-1.6 56.7 1.5 69 12" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M913 578c-1.6 16 83.5 16.6 86 3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m891 597-1 16s3.4 10.2 20 12c6.7-6.7 10-11 10-11l11 2 2 13s48 7 78-6c-.3-6.6 1-11 1-11l13-5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M890 594c10.3 17.4 93.2 30 130 4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M913 622c-62 67.8-108.2 192.8-124 288 9.6-8 41.4-17.3 51 19 .7-.1 1 0 1.1-.6 6-121.3 53.2-271.1 88.9-308.4 2.1 7.6 3 13 3 13s-66.8 119.4-81 304"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1021 611c70.2 23.7 184.7 131.6 202 239-7.1 0-16.3-.5-17 41-12.1-57.1-21.3-172.5-193-278-.4 7.3 0 9.9 0 11 8.2 3.2 159.5 97.6 181 272 8.6-2.7 10-4 10-4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M976 441c-8.5 0-21.3 3.4-31 51" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1050 417c17.1 4.3 51.3 13.1 62 17 .5 7.9.1 33.6 0 40-10.3 4.4-53.2 18.2-62 20 0-7.2-.7-60.5 0-77z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1015 447c5.7 0 10.3 4.9 10.3 11s-4.6 11-10.3 11-10.3-4.9-10.3-11 4.6-11 10.3-11z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m940 507 42 10 24-2-5 66 17 11" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M941 566c10.3-8.1 42.5-15.2 61 1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M727 609c3.5-6.9 8.6-23.5 33-33 .3 23.9 1 147 1 147s16.8 12 18-18c-1.5-24.8-6-145-6-145"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M760 524v50" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m926 445 55-27s-.5 84.9 0 85-55-13-55-13v-45z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1234 1061 47 10s4.7-86.1 5-105 2.8-44.4-46-54c-2.7 2.5-6.3 3.3-17 1-4.7 5-11 10-11 10v39s28 18 28 60v27l-6 12z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1230 914-20.2-6.4s-4.3-2.3-4-17.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1212 960v54s2.3 36.4-93 68" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1280 1073c-1 12.5 28.2 36 54 7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1138 1126c21.7-7.1 81.6-32.2 99-70" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m681 825-7-5s-19.7-3.8-23 11 2 122 2 122l38 9s-.9-63.6 1-82c-10.6-13.1-18.8-20.3-11-55z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M689 877c19.5 17.8 52.5 36.1 91 45-38.2 36.9-42.7 78.2-42 178"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M840 926c4.3 14.2 4.6 25.4-7 36-4.4-.3-9 0-9 0s-21.2 11-26 45c-4.8 33.9 0 158 0 158s-2.8 12.2-13 13"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M830 958c5.1-38.8-25-48.7-40-47-3.8 4.3-12 11-12 11" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1195 896c-61.9 42.7-225.8 58-345 40-2.3 69.8-.8 112.9 3 160-3.3 1.5-10 6-10 6s-36.3-7.7-44-11"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M854 1096c21.4 4.5 88.4 11.1 168.5 7.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1072 955c19.9-2 73 15.2 73 63 0 54.2-43.9 85.3-78 91s-76.4-10.1-77-68 46.9-82.6 82-86z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1008 1092-28-19s-17-14.8-17-55 31.6-70.3 70-78" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1078 935c13.2 2.3 38.5 16.7 55 48" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m691 961 2 47s-.7 22 45 49" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M799 1106c27.5 9.2 80.9 19.3 146.1 22.8 45.8 2.4 97.3.6 149.9-12.8 129.1-32.9 145-84.7 145-98"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M498 825c11.8-.1 21.8.7 22 6s-12.9 6.7-21 7-22.6-.9-23-6 10.2-6.9 22-7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M444 833c2.5 26.3 110.2 18.6 110-3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M423 878c3.1 27.3 155.2 25.6 155-11" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M476 1089c7.5 6 51.9 13.8 72-4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M491 1164c13.8 7.9 40.7 10 52-3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M436 1049c13.2 10.4 103.2 32.2 145-9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1378 1188 6-118" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1420 993c3.7 35.5 129.6 38.8 127 13" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1485 990c14.3 1.6 22.6 5.6 22 12s-15.8 5.8-25 5-26.5-1.8-26-11c.4-6.9 14.7-7.6 29-6z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1389 1038c15.8 52.2 157.1 57.8 179 20" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1381 1235c16.7 29.6 121.8 48.3 158 20" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1415 1285c9.5 13.3 63.7 23.5 83 9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1419 1371c17.7 12.8 42.8 16.3 58 7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1531 1079-15 187" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M583 1169c18.2 64.4 196.8 46 198-5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m710 1203 4 204" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M644 1439c10.6 11.1 72.3 13.8 91-6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M662 1526c13.1 12.1 42.4 17.5 60 1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M596 1389c33.2 31 130.4 38.2 179-5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M781 1274v-112l-31-52s-24.3-19-62-19" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M682 1106c14.9 0 24.8 4.8 25 11 .2 6.8-9.5 11-26 11-15.6 0-27.5-3.1-27.5-9.3 0-6 8-12.7 28.5-12.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M611 1121c-.7 40.5 140.9 39.6 141-7" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-full84000a"
			x1={1228.0504}
			x2={1252.2355}
			y1={728.1648}
			y2={825.1648}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666363" />
			<stop offset={0.397} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-full84000a)" d="m1259 1401 30-56-49-41s-28.8 4.8-66 7c12.2 12.7 85 90 85 90z" />
		<path fill="gray" d="m1257 1394 24-44-41 10 17 34z" />
		<path fill="gray" d="m1232 1366-4-8c-7.8-8.9-24-26-24-26l-4 1 32 33z" />
		<linearGradient
			id="ch-full84000b"
			x1={661.0435}
			x2={1305.3995}
			y1={643.9985}
			y2={175.8464}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.438} stopColor="#f2f2f2" stopOpacity={0.98} />
		</linearGradient>
		<path
			fill="url(#ch-full84000b)"
			d="M785 962c22.7 48.7 50.4 91.3 89 135s91.4 104.7 109 120 50.2 54.9 93 83 76.9 29 101 29 95.1-7.1 123-49c7.7-11.6 13.8-28.1 15.5-46.7 4.4-48.3-9-110.4-12.5-135.3-3.6-25.8-23.8-122.4-27-130s-42.3-99.3-86-141c-41.7-39.8-79.8-81.9-121-98s-97.5-22.6-172-18c-22.9 1.8-87.7-15.1-112-18-.2 11.2 1 20.1-18 19-30.2-1.7-59.1-5.5-95-10-.7 8.9 23.2 54.8 29 70 9.1 41.4 61.3 141.3 84 190z"
		/>
		<linearGradient
			id="ch-full84000c"
			x1={714.6747}
			x2={768.6987}
			y1={127.5809}
			y2={187.5809}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4f4f4f" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full84000c)"
			d="M769 727c-10.4 0-82.8-10.4-88-11 4.6 11 27 60 27 60l89-37s-7.7-17.5-14-15c-5.6 2.2-10 3-14 3z"
		/>
		<linearGradient
			id="ch-full84000d"
			x1={923.7369}
			x2={796.3478}
			y1={443.8433}
			y2={387.2333}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.389} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#313131" />
		</linearGradient>
		<path
			fill="url(#ch-full84000d)"
			d="M790 961s56.3.7 66-1 17.6-12.6 21-26c90.7 18 1.7 117.6-8 149-39.2-36.2-79-122-79-122z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full84000e"
			x1={1327.4332}
			x2={1476.7592}
			y1={319.8915}
			y2={106.6325}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.63} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full84000e)"
			d="M1479 912c-30.7-25.8-49.9-147.3 55-136-5.7-3.5-17-10-17-10l-117-58-48-9s-63.6-6.3-64 68c4.5 5.6 14.2 23.5 31 37 56.8 45.8 164 111.4 160 108z"
		/>
		<path
			fill="#d9d9d9"
			d="M1506 767c42.5 0 77 34.7 77 77.5s-34.5 77.5-77 77.5-77-34.7-77-77.5 34.5-77.5 77-77.5z"
		/>
		<path
			fill="#8c8c8c"
			d="M1508.5 794c28.4 0 51.5 23.1 51.5 51.5s-23.1 51.5-51.5 51.5-51.5-23.1-51.5-51.5 23.1-51.5 51.5-51.5z"
		/>
		<linearGradient
			id="ch-full84000f"
			x1={493.8596}
			x2={493.8596}
			y1={354}
			y2={280.7514}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#6d6d6d" />
			<stop offset={0.842} stopColor="#fff" />
		</linearGradient>
		<path fill="url(#ch-full84000f)" d="m481 867-8 2 37 71s12.4-26.9-3-52-26-21-26-21z" />
		<linearGradient
			id="ch-full84000g"
			x1={295.2184}
			x2={505.0994}
			y1={430.8729}
			y2={197.7758}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={0.637} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full84000g)"
			d="M514 944c-12.4-13.1-50.2-51.7-106-33-50.3 16.9-58.1 74.4-58.2 103.6 0 5.5 8.7 12.9.6 4.5C342.1 995.5 316 941 316 941l-16-46s-15-64.5 40-93c52.9-27.4 95.4-3.4 110 21s76.4 134.1 64 121z"
		/>
		<linearGradient
			id="ch-full84000h"
			x1={684.864}
			x2={684.864}
			y1={381}
			y2={175}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a1a1a1" />
			<stop offset={0.325} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full84000h)"
			d="M851 960c-6.1 0-333 7-333 7s2.5-4-1-14c-2.5-7.2.1-35.2-2-46-6.7-34-29-40-29-40s270.3-104.5 274-106c59 12.2 92.2 60.2 103 81s46.6 101.7-12 118z"
		/>
		<linearGradient
			id="ch-full84000i"
			x1={1267.7985}
			x2={1209.7094}
			y1={324.4271}
			y2={134.4271}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#6f6f6f" />
			<stop offset={0.383} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full84000i)"
			d="m1252 915 135-77s20.5-37.9-17-61c-43.9-8-315-52-315-52 69.1 29.9 140.5 97.2 197 190z"
		/>
		<path fill="#d9d9d9" d="M436 896c51.4 0 93 41.6 93 93s-41.6 93-93 93-93-41.6-93-93 41.6-93 93-93z" />
		<path fill="#8c8c8c" d="M437 920c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70z" />
		<linearGradient
			id="ch-full84000j"
			x1={1024.3533}
			x2={1304.1173}
			y1={502.4968}
			y2={406.1658}
			gradientTransform="matrix(1 0 0 1 0 586)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#595959" />
			<stop offset={0.413} stopColor="#666" />
			<stop offset={0.64} stopColor="#adadad" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.757} stopColor="#adadad" />
			<stop offset={0.877} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-full84000j)"
			d="M1282 990c14 68.3 22 117 22 117s-101.5-33.8-177 33c-51.5 24.7-80.2-35.5-89-49-43.1-61.7 21.5-144.8 103-154 90.5-10.2 120.2 6.5 141 53z"
		/>
		<path
			fill="#f2f2f2"
			d="m541 668-65-212s27.1 15.8 41 49c13.9 33.2 52 164 52 164l116-27s14.3-2.5 32-4c7.9 4.5 26 16 26 16s-10.4-11.3-13-19c71.6-2.5 162-5 162-5l31-214s15.5 2 21 48c4.3 35.5 1.7 99.2-13 197-16.4 10.5-93 47-93 47l-44-7s-.8 23.5-31 21c-25-2.1-184.9-24-184-24s-38-30-38-30z"
		/>
		<path
			fill="#ccc"
			d="M633 705s136.8 16.3 137 18 15-5 15-5l9-17 39 7s82.4-41.7 82-40-1-8-1-8l-119 35-168 1 6 9z"
		/>
		<path
			d="m1212 854 120-19 6-11 1-10-158 5 31 35zm-349-4c2.7 4.5 13.1 22.4 13 39-15.5 1.5-303 35-303 35s-13.1-.5-14-9c-.6-4.7-2-16-2-16s281-45.1 306-49z"
			className="factionColorSecondary"
		/>
		<path
			d="M1183 815s179.8-5.3 179-9c-2.1-7.1-4.9-10.3-8-14-183.9-13.7-211-16-211-16s-3.8 2.6-1 5c4.3 3.8 16.6 15.8 23 22 9.4 9.2 18 12 18 12zm-355-11c-6.9 1.5-314 85-314 85s2.3 7.5 3 11c7.3-.3 130-21 130-21l212-34s8.7 1.1 1-9c-4.8-6.4-21.5-29.5-25-33-1.6-1.7-5.2.1-7 1z"
			className="factionColorPrimary"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m729 642 63 50" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M436 896c51.4 0 93 41.6 93 93s-41.6 93-93 93-93-41.6-93-93 41.6-93 93-93z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M437 920c38.7 0 70 31.3 70 70s-31.3 70-70 70-70-31.3-70-70 31.3-70 70-70z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1304 1107s-101.5-33.8-177 33c-51.5 24.7-80.2-35.5-89-49-43.1-61.7 21.5-144.8 103-154 90.5-10.2 120.2 6.5 141 53"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m891 629 36 29" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m578 697 210-3 138-37" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m570 669 9 25" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m541 668-65-212s27.1 15.8 41 49c13.9 33.2 53 164 53 164l115-27s14.3-2.5 32-4c7.9 4.5 27 16 27 16s-11.4-11.3-14-19c71.6-2.5 162-5 162-5l31-214s15.5 2 21 48c4.3 35.5 1.7 99.2-13 197-16.4 10.5-87.3 45.2-93 47-10.3 3.7-44-7-44-7s-.8 23.5-31 21c-25-2.1-184.9-24-184-24s-38-30-38-30z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M308 918c-11.3-44.6 18-102.3 75.4-107.9 11.8-1.2 24.8.2 38.9 4.2 8.8 2.5 29.9 14 34.7 20.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M878.7 889.3C843.3 893.3 575 924 575 924s-19.6 7.5-19-26c-11.6 1.8-42 7-42 7l352-57"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1073 1132c2.3-50.7 103.9-124.9 218-81 1.3.6 5 2 5 2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1282 997c-145.1-48.8-234 38-247 82-.4 1.2-2 5-2 5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M788 747c127.8 54.8 430.8 455.9 395 574-2.2 6.6-4 11-4 11"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M751 765c29-11 87-55 218-55" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m373 1004 130-19" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1361 852c14.1-32.7 5.9-64.1-17-74-18.5-8.6-27-10-27-10"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1209 854 119-19s13.4-4.5 11-23c14.2-.4 16.6-.8 28-1-4.6 0-187 9-187 9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1457.7 850.8 101.5-14.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1288 1344-55 13 26 45" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1234 1356-29-31" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1508 790c30.4 0 55 24.6 55 55s-24.6 55-55 55-55-24.6-55-55 24.6-55 55-55z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1375.6 845.6c34.7 23.9 70.5 50.6 89.6 65M1288 765.1c1.5-72.3 64-66.1 64-66.1l48 9 122 58s9.6 4.4 16.3 7.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1190.7 1328.6c25.5 26.9 68.3 72.4 68.3 72.4l30-56s-21.6-18.1-36.2-30.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1252 915 135-77s20.5-37.9-17-61c-43.9-8-315-52-315-52 69.1 29.9 140.5 97.2 197 190z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M674.4 711.9c6.1 17 22 48.1 26.6 60.1.7 3.3 1.7 6.9 2.9 10.9M785 962c22.7 48.7 50.4 91.3 89 135s91.4 104.7 109 120 50.2 54.9 93 83 76.9 29 101 29 95.1-7.1 123-49c7.7-11.6 13.8-28.1 15.5-46.7 4.4-48.3-9-110.4-12.5-135.3-3.6-25.8-23.8-122.4-27-130s-42.3-99.3-86-141c-41.7-39.8-79.8-81.9-121-98s-97.5-22.6-172-18c-11 .9-31.8-2.6-53-6.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1388 706c-44.5 0-68 24.2-70 63.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1506 767c42.5 0 77 34.7 77 77.5s-34.5 77.5-77 77.5-77-34.7-77-77.5 34.5-77.5 77-77.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M517.2 944.6c0-11.2-.6-29.5-2.2-37.6-6.7-34-29-40-29-40s270.3-104.5 274-106c59 12.2 92.2 60.2 103 81s46.6 101.7-12 118c-5.5 0-268.9 5.6-323.3 6.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M510.5 932.5C497 905.1 460.8 841 450 823c-14.6-24.4-57.1-48.4-110-21-55 28.5-40 93-40 93l16 46s23.2 55.6 33.7 83.6"
		/>
		<path fill="none" d="M-114-138c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2z" />
	</svg>
);

export default Component;

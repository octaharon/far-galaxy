import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f3f3f3"
			d="M311 1435c61-58.3 212.3-192.1 246-214 10.9 12.5 51 72 51 72l115-62-33-51s154.7-40.7 257-15c14.1 6.9 20 13 20 13s30.9 68.2 111 55c62.5 12.7 126.1 9.7 86-93 49.3-37.4 107.2-35.6 125-35 15.6 12.7 45 46 45 46l102 30 1-68 422 95s-18.8-62.5-261-166c-86.9-34.2-249-78-249-78l-189-93-9 15s-146.8-85.8-226-115c-45.7-24.4-72-23-76-23-7.3-2.9-28-10-28-10s-51.4-145.8-70-191c-11.3-10.3-30-19.5-33-12s7 162 7 162 9.1 42.5-76 85c-14.7-5.4-31-9-31-9s-39.7-1.7-37 35c3.3 16 10 38 10 38l-96 113-29-22-34 21 25 42s-178.3 180.2-206 220c-8.4 46.8 47.4 194.8 60 215z"
		/>
		<linearGradient
			id="ch-full80000a"
			x1={738.1518}
			x2={756.0798}
			y1={2495.4702}
			y2={2262.4702}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={0.598} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full80000a)"
			d="m717 544 24 12 68 196s-32.4 9-47 25c-16.7-7.7-63-28-63-28s27-23.8 27-46-9-159-9-159z"
		/>
		<linearGradient
			id="ch-full80000b"
			x1={1620.4293}
			x2={1649.9823}
			y1={1776.4517}
			y2={1902.4517}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80000b)" d="m1437 1111 423 98s-16.4-23.4-33-35c-27.6-9.3-391-91-391-91l1 28z" />
		<linearGradient
			id="ch-full80000c"
			x1={633.9923}
			x2={414.8104}
			y1={1485.9957}
			y2={1744.9957}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80000c)" d="m312 1433 246-213-26-46-243 215 23 44z" />
		<linearGradient
			id="ch-full80000d"
			x1={835.1773}
			x2={828.4783}
			y1={1835.2264}
			y2={1906.2264}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="silver" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full80000d)"
			d="M686 1179c34.7-10 175-41 311-7-56.4-58.5-61-64-61-64l-271 41s11.4 14 21 30z"
		/>
		<linearGradient
			id="ch-full80000e"
			x1={1246.6143}
			x2={1237.0802}
			y1={1865.2572}
			y2={1966.3053}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.7} stopColor="silver" />
			<stop offset={1} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full80000e)"
			d="M1165 1142s35.5-41.7 161-40c10.4-1.1 13.4-19.3-11-61-50.1-1.3-156.2 26.3-172 45 5.4 14.5 22 56 22 56z"
		/>
		<path
			d="m534 1117 79-43-149-137-34 20 104 160zm886-112-88-52-24-1 63 38 49 15z"
			className="factionColorPrimary"
		/>
		<path
			d="M780 760c-106.9 38.2-73.6 144.1-2 239 50.3 66.7 116.3 125 102 156 29.3 2.1 118 15 118 15S825.2 743.8 780 760zm422 359.7c4.1-17.7-9.1-61.8-114-144.7 16.6 31.7 77 167 77 167s12.7-13.7 37-22.3z"
			className="factionColorSecondary"
		/>
		<linearGradient
			id="ch-full80000f"
			x1={574.3314}
			x2={641.8254}
			y1={2209.928}
			y2={2281.928}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.496} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full80000f)"
			d="m648 782-32-10s-39.4 1.3-36 35c5.3 20.6 12 37 12 37l56-62z"
			opacity={0.502}
		/>
		<path fill="gray" d="m974 1182 99 49s-75.3 15.3-99-49z" />
		<path fill="#ccc" d="M534 1120s53-29.8 78-45c19.1 23.8 75.6 97.5 115 152-33.6 25-126 69-126 69l-67-176z" />
		<path fill="#b3b3b3" d="m534 1120-94-145 5 33 154 288-65-176z" />
		<path
			fill="#ccc"
			d="m1436 1181-100-30-44-46s43 1.9 43-10-34-137.2-182-209c3.5-6.3 7-16 7-16l210 120 68 24-2 167z"
		/>
		<linearGradient
			id="ch-full80000g"
			x1={669.8781}
			x2={1098.85}
			y1={1831.715}
			y2={2132.084}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a2633" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full80000g)"
			d="M745 838.2c0-63.2 42.1-91.2 100.8-91.2 89 0 155.6 98.9 155.6 98.9s36.5 49.9 71.8 109.3c24 40.4 46.8 85.7 59 120.8 6.9 19.3 83.6 160 10.3 160-55.8.3-77.4 3-187.4-66-9.4-7.4 18.4-.5 41 0C781.8 958.8 773.3 882 745 838.2z"
		/>
		<linearGradient
			id="ch-full80000h"
			x1={669.8781}
			x2={1098.85}
			y1={1831.715}
			y2={2132.084}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#1a2633" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full80000h)"
			d="M745 838.2c0-63.2 42.1-91.2 100.8-91.2 89 0 155.6 98.9 155.6 98.9s36.5 49.9 71.8 109.3c24 40.4 46.8 85.7 59 120.8 6.9 19.3 83.6 160 10.3 160-55.8.3-77.4 3-187.4-66-9.4-7.4 18.4-.5 41 0C781.8 958.8 773.3 882 745 838.2z"
		/>
		<linearGradient
			id="ch-full80000i"
			x1={1278.7772}
			x2={1332.7772}
			y1={1943.7682}
			y2={1918.0582}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#303030" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80000i)" d="m1292 1107 42-10 12 6-10 46-44-42z" />
		<linearGradient
			id="ch-full80000j"
			x1={960.8978}
			x2={1007.494}
			y1={1881.9857}
			y2={1845.9857}
			gradientTransform="matrix(1 0 0 -1 0 3040)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.503} stopColor="#2e3135" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full80000j)" d="m1012 1179-1 22-65-36 66 14z" />
		<radialGradient
			id="ch-full80000k"
			cx={1586.2809}
			cy={603.2739}
			r={188.0428}
			gradientTransform="matrix(0.8598 -0.5106 1.4743 2.4824 -1328.6532 362.4201)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1577} stopColor="#333" />
			<stop offset={0.4367} stopColor="#666" />
			<stop offset={0.5342} stopColor="gray" />
			<stop offset={0.6306} stopColor="#e6e6e6" />
			<stop offset={0.6937} stopColor="#999" />
			<stop offset={0.8316} stopColor="#666" />
			<stop offset={1} stopColor="#333" />
		</radialGradient>
		<path
			fill="url(#ch-full80000k)"
			d="M1098.2 1088.6c.3-.3 1.1-.5.9-.9-5.2-9.8-58.6-126.6-121.5-202.3-25.9 8.5-38.9 19.4-54.8 43.9 4.7 39.1 12.3 76.3 30.2 96.1s91.6 72.6 101.4 78.7c8.4.5 21 0 21 0l.3-.3c4.1-7.2 13.5-13.7 22.5-15.2zm-58.5-193.8c-10.3-6.7-14.6-15.2-39.8-12.8 56.7 73.8 96.9 151.5 119.8 201.4 10-1.2 15.7-1 22.9 4.3-12.2-29.3-61.9-133.5-102.9-192.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1100.2 1088.6c.3-.3 1.1-.5.9-.9-5.2-9.8-58.6-126.6-121.5-202.3-25.9 8.5-40.9 19.4-56.8 43.9 4.7 39.1 12.3 76.3 30.2 96.1s91.6 72.6 101.4 78.7c8.4.5 21 0 21 0l.3-.3c4.1-7.2 15.5-13.7 24.5-15.2zm-60.5-193.8c-10.3-6.7-14.6-15.2-39.8-12.8 56.7 73.8 96.9 151.5 119.8 201.4 10-1.2 15.7-1 22.9 4.3-12.2-29.3-61.9-133.5-102.9-192.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M311 1435c61-58.3 212.3-192.1 246-214 10.9 12.5 42 75 42 75l125-65-37-53s157.7-38.7 260-13c14.1 6.9 20 13 20 13s30.9 68.2 111 55c62.5 12.7 126.1 9.7 86-93 49.3-37.4 107.2-35.6 125-35 15.6 12.7 45 46 45 46l102 30 1-68s402.4 90.8 422 95c-32.7-44.8-60-74.7-261-166-86.9-34.2-249-78-249-78l-189-93-9 15s-146.8-85.8-226-115c-45.7-24.4-72-23-76-23-7.3-2.9-28-10-28-10s-51.4-145.8-70-191c-11.3-10.3-30-19.5-33-12s7 162 7 162 9.1 42.5-76 85c-14.7-5.4-31-9-31-9s-39.7-1.7-37 35c3.3 16 10 38 10 38l-96 113-29-22-34 21 15 51s-168.3 171.2-196 211c-8.4 46.8 47.4 194.8 60 215z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1053.1 910.9c64.6 15 136 33 208.1 52.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M593.8 1056.2c73.1-56.4 138.1-99.6 201.8-128.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M270 1327c90.7-77.2 168.3-144.4 237.3-201.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1434.9 1011.6c154.9 45 288.1 89.1 330.1 112.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M722.1 712.6s34.4 32.8 55.2 52.7" />
		<path
			fill="#d9d9d9"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M761 868c7.6-.5 20.3-6 29-13 13.7 14.2 70 82 70 82s-16.5 41.5-7 74c-29.7-34.8-75-96.4-92-143z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m614 1074 111 159" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m532 1116 70 180" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1436 1182v-168l-66-25-35 162" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1371 989s-122.1-75.7-176-100" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1435 1014c-13.1-9.1-69.5-44.2-76-48" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1281 1105c13.8-1.3 48.2 1.5 52-8 7.1-17.9-51.6-153.2-179-209"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M588 849c11.1-12.8 29-46 72-72" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M935 1162c21.8 2.8 63 10 63 10S662.1 844.1 768 772c40-28.8 86-24 86-24"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M964 1176c18.2 10.9 96.5 51.5 117 57" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M927 772c86.2 56.3 175.5 203.6 242 379" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m466 937 146 137-80 45-100-159" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m446 1008 112 214" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m626.2 1130.9-2.4 40" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m573.6 1158.7 35.3 20.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m635.1 1191.8 36.3 19.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m618.2 1199.1-2.4 40" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1420.5 1052-18.8 24.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1379.1 1043.4 10.8 31.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1398.9 1096.3 11.7 31.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1386.3 1092.4-18.8 24.9" />
	</svg>
);

export default Component;

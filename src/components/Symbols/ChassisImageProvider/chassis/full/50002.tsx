import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1026 742c49.2 34 133.6 103.3 205 160.7 5.6 11.4 14 30.3 14 30.3l117 91 19-7 171 160s11.8 29.2 34 78c-.6.4-1.2 1.2-2.2.3-209-175.8-384.4-323.5-463.8-309.3 3.8 6.1 23.9 38.2 29.8 47.7 35.1 57.1 66.3 123 66.3 158.3 0 17.5-.9 17.5-8 20-2.1.4-3.2 16.1-4 16s-85 24-85 24-2.2 5.3-14 7c-38.5 6.9-75-4-75-4l-37-31s-.4-5.3 0-11c-17.2-8.7-31.7-7.6-179-106 7.3 11.3 13.5 30.2 16 35 8.3-.7 15-3 15-3s160.2 171.9 167.3 223.3c-9.3 19.7-11 24.2-18 37-3.8 7.1-107.5-75.9-206.3-172.3-80.5-78.5-156.2-168.1-183.8-205.7C509.6 878.8 584 813 584 813s-15.1-9.3-17-51c.4-6.6 2.2-14.5 4.4-22.2C559 731.3 548 724 548 724l-1-59 7-31s35-15.8 56-23.4c2-11.5 4.8-24.4 9-39.6 24.3-26 144.7-17.6 166-17 7-5.3 20.3-11.2 33-9s42 21 42 21l29-1 177 53-2 39 3 6 155 100-35 60s-159.8-93.6-161-81z"
		/>
		<path
			fill="#e3e3e3"
			d="M565 843c10.7-3.1 50.5-33 66-68 6.6 4.1 13 14 13 14s69.1 121.4 122 183c20.5 50.2 25 60 25 60s30.9 48 38 72c11.2-4.1 15-5 15-5s151 164.6 168 224c-8.8 19.7-16 36-16 36s-52.8-29.3-98-70-122-115-122-115l-93-99-66-79s-49.5-52.1-56-87-6.7-62.9 4-66z"
		/>
		<path
			fill="#e3e3e3"
			d="M1071 1190c-4.5-9.9-62.9-101-75-109-16.9 6.7-39 31-39 47 19 14.8 118.5 71.9 114 62zM871 921l-56-71-20-8-18 32-117-100-17 5s37.3 88.6 72 124c-3.3-13.4 1.7-16.3 7-19s32.8 22.6 42 34 63 69 63 69 3.3-45.7 44-66zm344 243c1.3-11.7-8.1-76.5-29-103-17.8-13.2-38.3-19.5-50-15 14 20.6 58 75.3 79 118zm-195-260c-7.7-8.2-50-55-50-55l-15-59s2.4-49.6-1-74c6.3 3.8 14.8 15.1 35 19 5.6 3.5 14.1 5.7 16 11 40.4 31.8 158.4 113.2 167 133-33.8-19.1-104.4-31-115.5-12.4 18.1 18.6 36 39 43.5 52.4-25.2-12.1-60.4-15.3-80-15z"
		/>
		<path
			fill="#bfbfbf"
			d="M1018 831c16.5 13.9 34.5 32 40 38 9.8-6 58.4-22.8 118 11-23.5-22.7-185.8-152.7-186-142-14.5-6-37-19-37-19l4 83 7 21s42-2.5 54 8z"
		/>
		<linearGradient
			id="ch-full50002a"
			x1={965.5729}
			x2={1000.1219}
			y1={1185.4753}
			y2={1071.4753}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full50002a)"
			d="M952 717c8.4 8.3 30.6 21.2 45 21-4.6 7-10.5 14.6 20 93-19.7-10.9-48.3-9.5-54-8-5-10.7-7-21-7-21s-2.4-63.1-4-85z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full50002b"
			x1={810.1137}
			x2={724.9197}
			y1={937.2064}
			y2={1150.2064}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full50002b)"
			d="m871 921-56-71-20-8-18 32-117-100-17 5s37.3 88.6 72 124c-3.3-13.4 1.7-16.3 7-19s32.8 22.6 42 34 63 69 63 69 3.3-45.7 44-66z"
			opacity={0.302}
		/>
		<path
			fill="#999"
			d="M743 940c-10.7-9.4-36.6-51.3-22-58 11.9-2.2 38 34 39 35 10.8 10.3 171 184 171 184s84.9 77.8 131 89c.7 2.2-2 5-2 5s-33.8-7.4-86-32c-42.8-20.2-97.6-52.5-161-96-18.6-29-24-37-24-37l-21-59s-14.3-21.6-25-31z"
		/>
		<path
			fill="#ccc"
			d="M628 776c-7.3-1.5-56-35-56-35s-15.2 46.5 10 72c2.3.1-18.1 27.6-16 29 4.4 3 30.5-18.8 31-18 8.6-9 38.3-46.5 31-48z"
		/>
		<path
			fill="#ccc"
			d="m886 905-26-42s-50.1-276.3-125-295c3.8-5.7 7.7-12.9 18-8s40.3 29.7 75 123 58 222 58 222zm41-45c-.8-10.1-18.7-101.5-45-181-22.1-67-47.6-125.9-99-118 6-12.7 16-13 16-13s22.3-10.6 50 32 87.5 183.7 116 327c-19-22.6-28.9-32.8-38-47z"
		/>
		<radialGradient
			id="ch-full50002c"
			cx={939.048}
			cy={1523.095}
			r={635}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full50002c)"
			d="M796 840s41.2 20.7 66 21c-4.6-23.4-39.1-189.7-85-254-20.3 11.2-60.5 58.8-64 99 36.3 38 103 101 103 101l-20 33z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full50002d"
			cx={991.64}
			cy={1467.2109}
			r={445}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full50002d)"
			d="M954 792s-53-157.5-64-178c7.5 4.2 22 9.2 34 30 19 33 34.8 92.2 30 148z"
			opacity={0.502}
		/>
		<path
			fill="#8c8c8c"
			d="M1060 868s37.5-29.6 116 15c29.8 17.1 36 25 36 25l18 34s124.9 94.1 125 94 25-18 25-18l172 157 28 72-9-1s-358.1-323.5-450-299c-19.3-30.8-61-79-61-79z"
		/>
		<linearGradient
			id="ch-full50002e"
			x1={643.5494}
			x2={643.5494}
			y1={1148.7671}
			y2={1276.5629}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full50002e)"
			d="M626 769s-28-24.2-22-48 13-49.7 52-52 45.9 24.2 49 26 2-15.5-4-23-31.4-35.4-59-27-66.8 36.9-61 85c4.2 30 33 39.9 36 41s9-2 9-2z"
		/>
		<linearGradient
			id="ch-full50002f"
			x1={1001.8721}
			x2={1001.8721}
			y1={1188.6453}
			y2={1314.6096}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full50002f)"
			d="M980.1 730.3c-1.1.5-15.3-23.4-9.1-47.3 6.2-23.8 14.9-40.7 48-49 29.2-2.8 39.9 19.2 43 21s-8-14.5-14-22-29.4-34.4-57-26-54.8 33.9-49 82c4.2 30 29 40.9 32 42s6.1-.7 6.1-.7z"
		/>
		<linearGradient
			id="ch-full50002g"
			x1={816.0014}
			x2={742.0014}
			y1={1068.4971}
			y2={1105.5051}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#333" />
		</linearGradient>
		<path fill="url(#ch-full50002g)" d="m778 873 34-64-74 7 40 57z" />
		<linearGradient
			id="ch-full50002h"
			x1={1221.5011}
			x2={1159.5011}
			y1={1118.4978}
			y2={1149.5038}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#333" />
		</linearGradient>
		<path fill="url(#ch-full50002h)" d="m1191 821 27-56-62 5 35 51z" />
		<path
			fill="#999"
			d="M740 816 615 709l14 25 16 16 12 24 113 93-30-51zm242-141c1.3.3 11 21 11 21h10l13 17-4 8s7.5 13.3 8 13 165 88 165 88l-29-53s-175.3-94.3-174-94z"
		/>
		<path
			fill="#ccc"
			d="m549 724-3-59 44-12s-34.5 42.9-6 93c-18-10.3-35-22-35-22zm92-87 74-23 61-8s-51.8 37.6-62 100c-2.6-10.1-11.8-51.2-52-62-9-2.5-21-7-21-7zm242-39 46-2 64 11s-44.7 5.1-54 69c-5.4-17.9-26.5-55.5-51-65-3.8-4.8-5-13-5-13zm146 16 32 35 4-32s-31.9-3.9-36-3z"
		/>
		<path
			fill="gray"
			d="M993 1175s73.8 21.9 97 19c19.5 1 36-3 36-3s68.8-14.5 78-18c.8 11.1-1 17-1 17l-93 26s-65.3 5-79-2c-17-11.2-38-29-38-29v-10z"
		/>
		<path
			fill="#b3b3b3"
			d="m611 705 48 70s-23.6 12.5-45-23-3-47-3-47zm366-33c4.2.5 16 23 16 23h8l11 16 1 9 7 15s-28 10.3-41-5-6.2-58.5-2-58z"
		/>
		<path d="m644 754 12 20s-19.3 4.1-22 1-5-9-5-9l15-12zm344-19 23-16 8 15s-22.5 5-31 1z" opacity={0.2} />
		<path
			fill="#ccc"
			d="m646 698-13-23-24 25 37-2zm31-24s23.1 13.5 26 20c-12.3 1.3-27 3-27 3l1-23zm300-3 41-6-18-23s-18.8 15.9-23 29zm66-29 1 21 22-3s-12.6-20.7-23-18z"
		/>
		<path
			d="M957 1126c2-9.8 7.5-34.8 39-43-16.7-22.2-124-161-124-161s-44.7 17.3-43 69c27.2 31.6 106.9 117.8 128 135zm178-80-115-141s61.5 1.2 82 16c20.3 28 76.8 119.7 84 139-7.2-4-18.3-12.5-29-15-11.3-2.6-22 1-22 1zM896 867l33-9s-51.8-230.7-84-269c-32.2-38.4-62.8-28-70-29 85.1 83.9 121 307 121 307z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full50002i"
			x1={823.7916}
			x2={990.8317}
			y1={814.5153}
			y2={969.6293}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full50002i)"
			d="M957 1126c2-9.8 7.5-34.8 39-43-16.7-22.2-124-161-124-161s-44.7 17.3-43 69c27.2 31.6 106.9 117.8 128 135z"
			opacity={0.4}
		/>
		<path fill="#f3f3f3" d="m1371 1029 190 171-11-24-170-153-10 4" />
		<path
			d="m778 605-63 10-168 49 5-27s27.4-12.1 57-25.2c3.8-6.3 5.2-34.9 11-43.8 10.7 7.8 18.9 23 25 27.9 20.4-9 36-15.9 36-15.9l70-6s21.9 22.1 27 31zm81-38c10.2 11.6 21.3 27.9 23 32 11.5-1.4 45-6 45-6l86 14-7-9-112-32s-24.5-2-35 1z"
			className="factionColorSecondary"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="m778 872 37-65-104-103s.8-58-73-64-72.8 78.7-54 101c3.1 3.6 5.8 8 8.4 11 13 15.1 22.4 21.9 46.6 25 13.8.9 23-3 23-3l116 98z"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="m598 759-49-35-3-61 5-27 131-57 65-5m31 33s-43.9 15.7-63 95m5-22c0-15.3-2-65-2-65l-34-33m88 24-54 6s-46.4 14.2-90.7 27.7M593.7 650c-26 7.9-45.7 14-45.7 14"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="M712 575c66.3-52.5 127.8 178.1 151 289 9.3 17.3 17.2 30.2 30 54 12.6-.7 24 0 24 0s-12.5-31.2-20-67c-15.4-83.1-63-244.3-126-292-25.8-10.2-52 11-52 11m58-10c27.5-.5 58.5-2.4 84 62s61.9 179.2 67 237c24.3 31.2 38 51 38 51l28-4s-22.9-53.2-57-165c-14-45.9-46.7-150.9-85-180s-65.6-9.3-72-4"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="M892 916c-4.5-25.1-19.6-100-42.2-174-30.9-101-75-201.2-117.8-178"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="M798 841c8.6 5.6 33 17.6 63.9 22.4m38 1.5c8.9-.7 18-2.3 27.1-4.9m-40.8-252.4c70.1 34.5 72.5 167.6 71.8 192.4v7m98-193-168-50s-22.8 1.4-29.9 1.9M889 567l40 26-1 55m-46-51 48-4 135 24-2 36 3 9 158 99-36 62-165-91s-35.2 21.5-73.5-19.4M941 675c.6-18.2 14.2-68.8 57-69m33 8 29 31m-17-5v30l-18 2-23-28m153 125-176-99 37-5m28-3 19-2m157 102-64 8 31 50m-123-161s-10-26.1-36-24c-45.7 1.7-54.8 36.2-57 50s-1 41.8 15 50c13.1-8.5 30-22 30-22l-15-19-27 14m4-36s8 11.3 16.9 24m15.7 22.1c4.9 7 8.4 11.9 8.4 11.9"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="M966 910c-4.5-20.8-47.3-206.3-101.1-303.8-23.4-42.5-51.3-68.2-74.9-56.3"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="m774 869-38-54 77-7m-74 8L610 702l35-3m32-3c13.3-1.2 24-2 24-2l19 20"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="M636 678c7.4 12.3 17 31 17 31l22-2v-33"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="M703 694c-5.5-21.3-68.5-46.4-94 6-15.8 27.9 1 48 1 48s13.3 19.2 23 26"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="M664 645c-53.6-4.1-93 44.2-80.6 95.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="m608 743 21-9 17 16-17 17"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={10}
			d="M660 775s-6.9-9.9-15.5-22.3m-13.1-18.9C620.2 717.7 610 703 610 703"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M571.4 739.8c-2.2 7.7-4 15.6-4.4 22.2 1.9 41.7 17 51 17 51s-74.4 65.8 20.2 168.3c27.5 37.6 103.3 127.3 183.8 205.7 98.8 96.3 202.5 179.3 206.3 172.3 7-12.8 8.8-17.3 18-37C1005.2 1270.9 845 1099 845 1099s-6.7 2.3-15 3c-2.5-4.8-8.7-23.7-16-35 147.3 98.4 161.8 97.3 179 106-.4 5.7 0 11 0 11l37 31s36.5 10.9 75 4c11.8-1.7 14-7 14-7m82.9-37.6c2-.8 4.1-1.6 6.1-2.4 7.1-2.5 8-2.5 8-20 0-35.4-31.2-101.2-66.3-158.3-5.8-9.5-25.9-41.6-29.8-47.7 79.3-14.3 254.8 133.5 463.8 309.3 1.1.9 1.7.1 2.2-.3-22.2-48.8-34-78-34-78l-171-160-19 7-117-91s-8.4-18.9-14-30.3c-71.4-57.4-155.8-126.6-205-160.7M785 554c-21.3-.6-141.7-9-166 17-4.1 15.2-6.9 28.2-9 39.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M992 1173c16.4 6.5 46.3 21 98.9 23.5" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1056 865c24.8-6.2 78.1-22.2 157 43 6.8 14.1 15 31 15 31l127 96 25-17"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1211 906c-36.4-31.5-187.7-151.5-215-166" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m831.3 1102.3-56 72" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M790.3 1032.1c-25.2 29.3-74.1 86.2-74.1 86.2" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1125 954c-9.3-16.1-21.3-32.2-32.6-47.2-32.8-44-74.4-73.8-74.4-73.8s-27.6-70.6-28-93"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1019 834c-11.4-11-42.1-16.3-57-12" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1204 1172c-17.9 4.1-75.7 18.4-99.3 23.2C989.8 1191 909.3 1076 781 936c-12.8-14-52.6-55.8-58-54-24.7 8.3 40.5 85.5 45 91 13.1 32.8 14.2 34.5 23 59 9.5 14.9 22.2 35.3 31 48"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M958 1125c3.7-23.5 20-35.8 39-43.3m192-16.7c-8.9-8.7-25.9-22.6-55-20.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1105 1193c-8.2-26.6-31-54.7-76-113-39.3-50.9-50.3-69.2-98-132 36.7-3.4 92-9 92-9l180 233v17l-96 24-21-18"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1071 1189c-11.7-31.5-216.2-293.3-258-341m158-.8c34.5 38.7 104.5 121.6 162 196.8 36.1 47.1 69.7 90.7 79 120 .9 1.4 1 3 1 3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M829 988c0-25.4 7.5-46.4 43-65.4m149-20c28.8-2.5 76.7 10.9 86 25.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M669 799c3.4-3.7 6.6-6.9 9.7-9.8" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M757 959c-27-37.9-60.6-75.3-119.9-184m-5.5-.8c-10 11.4-33.5 63.8-67.6 68.8"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M742 941c6.1-25.1 18.9-18.9 18-32-2.5-34.3-98.8-111.6-121.2-132.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M586.9 749.1c-2.7 38.9 1.4 72.1 10.1 69.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1032 746c-5.8-2.8-12.4-6.2-18.7-8.5" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M626.2 569.3c3.3 8 11.5 17.8 19.1 25.8" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="m639 994 28 39 73 22-1-68s.4.2 1.1 2.3c3.5 10.4 13.9 41.7 13.9 41.7s77.9 59.7 134 91c13.8 3.3 29.2 5.4 41.9 6.8 4.7.5 9.3-3.1 13.1-2.8 8.1.7 13 5 13 5l63 2 62-22-1-14 19 4 73-61-14-29s28.1-37.8-8-62c33.8-13 39.3-35.3 41-41 4 10.2 14.1 13.3 34 8 23 16.6 79 57 79 57l65-38-76-58s7.5-29.4-15-65c-10.1-10.2-17-6-17-6l-52-36-29 2-5 6-5-5s-14-47.7-129-110c-6.7-6.1-12-17-88-17s-104 14-104 14l1 18-20-7s-68.7 49.8-95 95c2 11 7 30 7 30l16-10s1.4 0 .3 2c-4.8 8.8-18.4 36-24.3 70-21.7 13-47 30-47 30l-46-56-32 4s.1 1.9-.9 8.9c-.8-.1-1.1 0-2.1.1-3.3-4.7-9-7-9-7l-34 4s-16 18.4 1 86c9 13.3 19 27 19 27s3.6 28.7 23 43c13.4-1.3 33-4 33-4z"
		/>
		<path
			fill="#a6a6a6"
			d="M810 1071c22.1 9.2 59.5 17.5 95 1 25.6 2.8 39 5 39 5l26 15s-29.8 33.5-39 38c-10.1-.1-43-8-43-8s-100.1-60.2-78-51zm185 24s52.3 3 80-9c.4 10.9 5 26 5 26l-61 22-62-4 38-35zm106 5 71-62-5-4-63 30-3 36z"
		/>
		<path
			d="m673 874-31-42-32 4-1 12-12-10-35 6 32 42 79-12zm519-59 67-10-51-36-33 10s.8 13.1 5 22c4.3 9.1 12 14 12 14z"
			className="factionColorSecondary"
		/>
		<path
			d="m909 901 40-16 24-24s-93.7-190-143-189c-17.4 12.2-79.3 59.8-93 94 7.9 8.2 136.3 88.3 172 135zm151-215c17.6 14.8 54.7 57.9 48 163 14.5 7.8 29 13 29 13l33 2s58.3-104.2-111-188c-.3 3.8-16.6-4.8 1 10z"
			className="factionColorPrimary"
		/>
		<path
			fill="red"
			d="M1048.5 936c66.3 1 73.5 32.2 73.5 72s-34.6 77-73.5 77-84.5-2.5-75.5-77c0-39.8 36.6-72 75.5-72z">
			<animate
				fill="freeze"
				attributeName="fill"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="#F00;#F33;#C00;#F30;#F30;#f00"
			/>
		</path>
		<path
			fill="none"
			stroke="#f2f2f2"
			strokeLinecap="butt"
			strokeWidth={10}
			d="M1042.9 1060.3c26.3 0 54.4-25.1 54.4-58 0-13.6-6.3-41.7-44.6-41.7-38.3 0-57 31.5-57 59.9 0 8.7 4.1 39.8 47.2 39.8z">
			<animate
				fill="freeze"
				attributeName="opacity"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="1;1;0.8;0.8;0.6;1"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dashoffset"
				attributeType="XML"
				dur="6s"
				repeatCount="indefinite"
				values="0; 50; 0"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-dasharray"
				attributeType="XML"
				dur="4s"
				repeatCount="indefinite"
				values="5 10 5; 5 5 5; 5 25 25; 5 10 25; 5 5 5; 5 10 5"
			/>
			<animate
				fill="freeze"
				attributeName="stroke-width"
				attributeType="XML"
				dur="8s"
				repeatCount="indefinite"
				values="10;5;10"
			/>
		</path>
		<path fill="#555" d="M760 1012v73l-67-23 67-50zm584-80 61 31-62 38 1-69z" />
		<path fill="#e6e6e6" d="m760 1009-80-109-64 62 73 100 71-53zm604-78-68-53-40-14 55 41 53 26z" />
		<path
			fill="#999"
			d="m616 965 20 28-25 6 5-34zm124-193 17 15-17 11v-26zm-97 162c0-14.5-5.9-44.5-16-50 9.2-5.8 17-6 17-6s14.2 13 13 43c-6.5 6.9-8.3 6.9-14 13zm617-77c0-14.1-11-41.9-21-47s-17 2-17 2 24.1 16.2 23 43c8.3 1.3 15 16.1 15 2z"
		/>
		<path
			fill="#ccc"
			d="m561 846 36 42s14.6 19.1 15 76c10.4 11.5 27 32 27 32l-34 3s-19-14.5-24-46c-11-16.5-17-25-17-25s-19.2-59.7-3-82z"
		/>
		<linearGradient
			id="ch-full20000a"
			x1={642}
			x2={642}
			y1={932.946}
			y2={1021.946}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full20000a)" d="m615 963 71-72-15-17-73 12s16.9 28.8 17 77z" />
		<linearGradient
			id="ch-full20000b"
			x1={767.8292}
			x2={716.8862}
			y1={890.8726}
			y2={1021.8716}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full20000b)"
			d="M747 989s13.1-16.8 10-52c-4.5-5.8-8-9-8-9s-.6-56.9-14-70c-13.6 7.6-47 29-47 29l-6 13 57 85 8 4z"
		/>
		<path
			fill="#bfbfbf"
			d="M1341 1001v-69l-120-91s-11.8-26.3-30-24c-5.5-4.2-10-8-10-8v15s43.5 10.5 25 76c-10.2.6-16 3-16 3s6.2 24.1 31 13c18.9 11 120 85 120 85z"
		/>
		<linearGradient
			id="ch-full20000c"
			x1={1214.7086}
			x2={1189.0616}
			y1={1004.0421}
			y2={1069.993}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full20000c)" d="m1191 901 17-4s16.9-48.1-22-68c2.1 16.5 5 72 5 72z" />
		<linearGradient
			id="ch-full20000d"
			x1={1241.9368}
			x2={1199.8638}
			y1={952.5768}
			y2={1062.1798}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full20000d)"
			d="M1192 901s-4 25.7 28 15c6.7-22.9 16.4-90.2-28-100-6.6-5-13-7-13-7l4 18s39.7 12.8 23 70c-7.6 2.2-14 4-14 4z"
		/>
		<path
			fill="#ccc"
			d="M748 987s25.5-5.2 27-5 52.5 64.5 177 55c5.7 4.7 21 18 21 18s-3.8-53.5 3-71c-26.6.8-114 2.1-220-47-1.8 33.1 3.7 34-8 50z"
		/>
		<linearGradient
			id="ch-full20000e"
			x1={767.9749}
			x2={766.1599}
			y1={993.7543}
			y2={941.7543}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full20000e)" d="M758 937c13.9 9 22 11 22 11l-2 36-25 5s3.7-30.9 5-52z" />
		<linearGradient
			id="ch-full20000f"
			x1={989.6402}
			x2={963.6402}
			y1={944.988}
			y2={904.5509}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.3} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full20000f)" d="M951 985h26s-14.5 41.2-4 66c-11.2-4.8-21-19-21-19l-1-47z" opacity={0.902} />
		<linearGradient
			id="ch-full20000g"
			x1={883.2053}
			x2={864.1412}
			y1={1004.4882}
			y2={915.4882}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full20000g)"
			d="M953 1038s-164.8-9.2-175-55c.8-25.6-.2-22.2 2-34 17.2 7.8 100 34.4 169.5 37.3-4.1 17.6 3.5 51.7 3.5 51.7z"
		/>
		<linearGradient
			id="ch-full20000h"
			x1={983.4482}
			x2={968.3122}
			y1={745.4716}
			y2={866.4716}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={0.871} stopColor="#666" />
			<stop offset={1} stopColor="#999" />
		</linearGradient>
		<path
			fill="url(#ch-full20000h)"
			d="m960 1128 35-33s35.8-2.3 41-4c-21.5-4.7-43.4-11.4-56-28-18.6-.3-162.1 4.1-217-56-11 6.1 16.8 45.1 27 52 28.3 22.1 40 22 40 22s55 4 78-10c29.6 2.8 36 4 36 4l28 15-31 31 19 7zm116-33c1.1.9 22 6 22 6s4.2-22.2 6-35c28.4-13.8 63.3-30.4 64-37 .1-.9-4.5-9.8-11-14-8.3 9.8-31 18.7-41 24-4.7 8.2-14 23-14 23l-29 15s1.9 17.1 3 18z"
		/>
		<radialGradient
			id="ch-full20000i"
			cx={580.612}
			cy={730.493}
			r={883.291}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.445} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.499} stopColor="#666" />
		</radialGradient>
		<path
			fill="url(#ch-full20000i)"
			d="M759 785c26.2 13 114.4 75.8 147 115s-3 78-3 78-126-13.7-154-48c-2.3-39.3-5.7-58.9-15-73 5.6-16.8 7-46 25-72z"
		/>
		<path
			fill="#a6a5a6"
			d="M851 659c74.5 34.7 149 194 149 194l-24 9s-84.3-155.5-126-181c-.6-8.2.8-15.5 1-22zm253 187c-3.9-.1-12-2-12-2s-6.9-129.9-27-156c20.4 24.8 53.4 80.4 39 158z"
		/>
		<radialGradient
			id="ch-full20000j"
			cx={1082.765}
			cy={774.145}
			r={527.489}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.549} stopColor="#a6a6a6" />
		</radialGradient>
		<path
			fill="url(#ch-full20000j)"
			d="m909 901 38-14 26-25 26-9 53 9 36-19 22 4 27 15 33 4 11-36s11.1 22.6 10 74c-4.1 39.1-64.7 56.1-75 60-6.8-18.4-87-63-138 16-24.3 2.4-93.5-1.9-107-5 10.5-15.2 38-74 38-74z"
		/>
		<linearGradient
			id="ch-full20000k"
			x1={912.1512}
			x2={925.7282}
			y1={987.463}
			y2={1064.463}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.06215404} stopColor="#f2f2f2" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full20000k)" d="M940 980s-14.5-76.7-32-77-37.6 57.7-39 69c14 3.8 71 8 71 8z" />
		<path fill="#ccc" d="M1127 994s22.7-18.4 22-43c-13.2 4.3-32 14-32 14l10 29z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M762.2 1019.4c4.3 37.1 80 77 125.8 102.6 13.5 3.2 29.4 5.3 42.9 6.7 7.2.8 6.7-4.7 11.8-4.4 6.4.5 17.3 6.6 17.3 6.6l59 2 62-22-1-14 19 4 73-61-14-29s28.1-36.8-8-61c33.8-13 39.3-36.3 41-42 4 10.2 14.1 13.3 34 8 23 16.6 116 87 116 87l65-40-113-86s9.8-46.2-15-65c-10.1-10.2-17-6-17-6l-52-36-29 2-5 6-5-5s-14-47.7-129-110c-6.7-6.1-12-17-88-17s-104 14-104 14l1 18-20-7s-68.7 49.8-95 95c2 11 7 30 7 30l16-10s1.4 0 .3 2c-4.8 8.8-18.4 36-24.3 70-18.3 10.9-39.1 23.7-45.2 27.8 2.5 5.2 9.8 14.1 10.2 33.5.1 6.4-.5 2.9-.8 1 .3-22.1-9.4-34-9.8-35.2-.9-2.2-4.6-4.8-7.3-8.1-1.8-2.3-4.9-2.2-7.2-5-13.6-16.5-32.8-40-32.8-40l-32 4s.1 1.9-.9 8.9c-.8-.1-1.1 0-2.1.1-3.3-4.7-9-7-9-7l-34 4s-16 18.4 1 86c9 13.3 19 27 19 27s3.6 28.7 23 43c13.4-1.3 33-4 33-4l50 70 73 22s-.8-49.2-1-66c-.1-.9 1.4.6 1.1-.5z"
		/>
		<path
			fill="#1a1a1a"
			d="M908.8 871c-3.6-5.6 5.8-9.9 13.7-14.7 8.8-5.3 16.7-10.6 20.7-4 3.6 5.9-4.8 10.8-13.9 15.9-8.3 4.7-16.5 8.9-20.5 2.8zm219.3-37.7c1.1-5.7 9.5-2.7 17.4-1 8.7 1.9 16.8 3.4 15.3 9.8-1.3 5.8-9.4 3.9-18.2 1.6-8-1.9-15.7-4.2-14.5-10.4zm1-21c1.1-5.7 9.3-2.7 16.8-1 8.4 1.9 16.3 3.4 14.9 9.8-1.3 5.8-9.1 3.9-17.7 1.6-7.7-1.9-15.1-4.2-14-10.4zm0-20.2c1.1-5.3 9.3-2.5 16.8-.9 8.4 1.8 16.3 3.2 14.9 9.2-1.3 5.4-9.1 3.6-17.7 1.5-7.7-1.9-15.1-4-14-9.8zm-236.7 61.8c-3.6-5.6 5.8-9.9 13.7-14.7 8.8-5.3 16.7-10.6 20.7-4 3.6 5.9-4.8 10.8-13.9 15.9-8.4 4.8-16.5 9-20.5 2.8zm-15.6-18.1c-3.6-5.6 5.8-9.9 13.7-14.7 8.8-5.3 16.7-10.5 20.7-4 3.6 5.9-4.8 10.8-13.9 15.9-8.4 4.7-16.5 8.9-20.5 2.8z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m691 1063 72-54" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m564 846 28 39 80-11" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m606 845 25 34" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m762 1009-78-105-68 58 29 40" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M642 879c8.8 4.8 15 33.1 15 46" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M623 880c12.3 7.3 19.5 29.6 18 58" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M583 957c-1.9-11-6.3-67 14-67 15 0 21.6 116.7 8 107" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M586 904c12.2 2.2 17.3 76 7 76" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1214 812s31.9.1 34 46" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1341 999v-69l63 31" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1342 930s-98.5-75.8-117.8-90.6c-.5-.4-5.8-10.2-6.2-10.4-1.2-.9 3 8 3 8s20.4 24.3 4 68c-5.3 8.9-14.8 13.5-22 13"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1226 841 69 36" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1270 806-78 8" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1190 905s4.8-.5 12-2 27.8-62.6-21-79" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1222 839c-1.3-9.5-17.6-24.9-30-25-7.6-5.8-15-11-15-11"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1183 775 43 34s33.4-5.3 36 55" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1192 903c.8-27.7-5.8-70.8-11-87" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M737 770c20.8 14.8 122.2 76.7 172 131 21-7.8 39-15 39-15l26-23s-84.5-167.5-128-186"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M850 662c36.2.6 122.3 122.2 149 191 25.9 5.6 53 8 53 8l36-17s17.3-130-54-187"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1053 859c0-41.5-45.9-198.6-111-213" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1019 1134c9.2-10 26-33.9 26-45" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1061 686c20.7 13.8 56.5 67.3 47 162 15.9 7.6 29 14 29 14l33 3s22.4-36.3 1-91"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M791 1061c24.1 10.5 61.3 35.4 117 10 29.2 3.5 39 6 39 6l25 13s-29.4 36.1-45 39"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m956 1129 38-35s31.4 1.5 51-3c18.9-1.5 30-4 30-4l4 22" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m1099 1101 4-37 62-31" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M735 857c4.9 5.8 11.6 28.3 15 72 2.8 3.7 7 7 7 7s1.8 53-13 51"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="M758 936c24.1 15 95 47.9 218 48" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1116 964c7.7-2.2 35.1-13.6 47-21" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M752 985c4.2 38.4 98.9 83.6 231 77" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1113 1040c17.2-8.2 38.3-16 49-36" />
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={10}
			d="M1041 1087c40.1 0 83-38.5 83-89 0-20.9-9.6-64-68-64s-87 48.4-87 92c0 13.3 6.3 61 72 61z"
		/>
		<path fill="none" stroke="#191919" strokeWidth={10} d="m982 1064-25-23s-13-10.3-6-58" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M953 1039c-25.6 0-165-17.2-177-57-11.8 3-27 7-27 7" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="M1125 995.2c11.1-5.1 23.7-19.2 25-44.2" />
		<path fill="none" stroke="#191919" strokeWidth={10} d="m777 984 2-36" />
	</svg>
);

export default Component;

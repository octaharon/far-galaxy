import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#d9d9d9"
			d="m354.9 1468.7-42.2 14.6v20.4s-60.1 97-59.7 97.5c.5.6 0 17.5 0 17.5l23.3 48 68.4 10.2 116.4 109.2 64-10.2-8.7-97.5 77.1 84.4 61.1-10.2 8.7-139.7 29.1-18.9v-23.3l-32-43.7-104.8-81.5 37.8-10.2 32-80.1 18.9-27.7v-48s.4-3.2.6-8.5c.1-1.5-7.9-2.8-7.8-4.6.1-14.4 1.2-33-5.8-49.5-.5-18.1 0-46.6 0-46.6h16l10.2-8.7-14.6-147s-8.8-89.7-10-104.3c-.1-1.3 1.3-2 1.3-2s10.7 80.9 87.3 100.4c9.4 27.8 17.5 52.4 17.5 52.4l71.3 77.1s103.4 18.3 146.2 25.9c2.7.5 1.5-13.2 3.7-12.8 4.3.8 9.5.6 16 1.5-2 4.1-1.5 14.6-1.5 14.6l78.6-42.2 17.9-77.5s69 2.3 97.5-42.9c13.5-27.1 22.8-42.7 22.8-42.7l65.5 160.1-26.2 11.6s-75.6-30.6-117.9 46.6c-42.3 77.1 36.4 125.2 36.4 125.2l1.5 14.6-36.8-15.9-47.7 8.6-1.4 24.8 15.9 24.7-7.3 5.8-1.5 27.7-46.6 77.1v20.4l37.8 40.8 75.7 5.8 8.7 20.4 135.4 77.1 56.8-8.7v-20.4s-19.7-49.8-18.9-48 75.7 56.8 75.7 56.8l53.9-8.7v-21.8l-23.3-151.4-139.7-101.9h-16l-2.9-24.7 4.4-2.9-1.5-13.1s12.9-13.2 23.3-37.8c11.7-3 29.1-10.2 29.1-10.2s21.7 14.6 50.9 13.1c2.1 20.6 2.9 52.4 2.9 52.4l7.3 2.9 59.7-7.3s14-43 29.1-81.5c8.3-12.6 24.7-29.1 24.7-29.1l1.5-34.9-13.1-11.6-29.1-93.2-5.8-4.4-20.4 1.5-2.9-7.3 27.7-8.7 5.8-10.2L1355.9 776s7-99.4-45.1-132.5c-30.8 3.3-56.8 4.4-56.8 4.4l-43.7-14.6s-29.5-34.9-90.2-59.7c-2.4-23.6-7.3-50.9-7.3-50.9l16-11.6 46.6 7.3 37.8 62.6 225.6 23.3 42.2-69.9-10.2-34.9-186.3-122.3 208.1-33.5 62.6-8.7 30.6-14.6 40.8-71.3-216.4-45.3-120.8 17.5 1.5-14.6-16-10.2-171.8-23.3-83 10.2 2.9-24.7-132.5-5.8-26.2 2.9s-49.6-53.7-205.2-39.3-175.5 56.7-174.6 106.3c.8 49.6 11.6 100.4 11.6 100.4s-66.5-7.2-90.2 11.6c3.8 40.6 8.7 78.6 8.7 78.6l-2.9 1.5 26.2 221.2 10.2 2.9 10.2 80.1-78.6 10.2s-23.3 8.6-23.3 80.1 4.4 136.8 21.8 136.8H416l17.5 253.3s-12 17.2-16 32c-24.3 7.6-87.3 25.8-87.3 106.3 0 53.1 49.5 85.9 49.5 85.9-.1.6 0 5.8 0 5.8s-7.4-12.3-14.6-14.6c-9.3-1.1-21.8 2.9-21.8 2.9l11.6 50.9z"
		/>
		<linearGradient
			id="ch-full18001a"
			x1={945.2999}
			x2={945.2999}
			y1={1931.5907}
			y2={1993.0104}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.8} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full18001a)"
			d="M1055.2 551.4s-93.5-29.2-160.1-34.9c-42 3.2-59.7 5.8-59.7 5.8s42.3 4.2 83 13.1c40.7 8.9 97.5 31 110.6 40.8 13.2 9.7 26.2-24.8 26.2-24.8z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full18001b"
			x1={428.7943}
			x2={542.3276}
			y1={1958.5978}
			y2={1974.5551}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0.992} />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full18001b)"
			d="m542.9 581.9-16-104.8s-67.3-25.7-97.5 10.2c4 32.1 16 131 16 131l97.5-36.4z"
			opacity={0.2}
		/>
		<path
			fill="#ccc"
			d="M1053.8 701.3c.9.1 17.5-1.5 17.5-1.5s12.6-10.7 7.3-37.8-12.1-56.4-48-84.4-135.8-50-190.7-56.8c-10.8 5.6-17.5 11.6-17.5 11.6s51.3 4.6 85.9 14.6c34.6 10 81.6 27.1 112.1 53.9 12.8 11.2 25.8 26.3 30.6 45.1 6.5 26 2.2 55.2 2.8 55.3z"
		/>
		<path
			fill="#ccc"
			d="m494.8 1325.7 129.5-16s-4.7 36-13.1 68.4c-7.6 29.3-18.9 55.3-18.9 55.3l-64 8.7-33.5-116.4zm876.3-126.6 4.4 109.2 7.3 2.9 59.7-7.3s45.6-125.2 43.7-125.2c-1.6 0-115.1 20.4-115.1 20.4z"
		/>
		<linearGradient
			id="ch-full18001c"
			x1={637.566}
			x2={943.2823}
			y1={1760.2738}
			y2={1617.5856}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001c)"
			d="M625.8 688.2c-4.5 12.4-5.8 46.6-5.8 46.6s46.9 46.1 37.8 106.3c35.4 15.2 110.3 49.8 224.2 58.2 113.8 8.5-30.6-139.7-30.6-139.7s-196.2 17.8-225.6-71.4z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full18001d"
			x1={1327.95}
			x2={1327.95}
			y1={1280.1445}
			y2={1465.5769}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001d)"
			d="M1372.5 1255.9c-16.8 3.9-103.8-7.3-108-118.4 21.2-49.8 108-62.9 126.9-66.4-3.8 25.7-10 69.4-13.1 87.3-10.1 7.6-26.2 17.5-26.2 17.5v43.7l20.4 14.6c0-.2 1.4 15.9 0 21.7z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full18001e"
			x1={1396.5289}
			x2={1269.6089}
			y1={1345.824}
			y2={1318.8453}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={0.306} stopColor="#4d4d4d" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001e)"
			d="M1372.5 1255.9c-16.8 3.9-103.8-7.3-108-118.4 21.2-49.8 108-62.9 126.9-66.4-3.8 25.7-10 69.4-13.1 87.3-10.1 7.6-26.2 17.5-26.2 17.5v43.7l20.4 14.6c0-.2 1.4 15.9 0 21.7z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full18001f"
			x1={1446.7963}
			x2={1319.8761}
			y1={1283.2048}
			y2={1373.8699}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.22} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001f)"
			d="M1372.5 1255.9c-16.8 3.9-103.8-7.3-108-118.4 21.2-49.8 108-62.9 126.9-66.4-3.8 25.7-10 69.4-13.1 87.3-10.1 7.6-26.2 17.5-26.2 17.5v43.7l20.4 14.6c0-.2 1.4 15.9 0 21.7z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full18001g"
			cx={969.3912}
			cy={2144.2751}
			r={1023.9358}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#000" stopOpacity={0} />
			<stop offset={0.672} stopColor="#000" stopOpacity={0.471} />
		</radialGradient>
		<path
			fill="url(#ch-full18001g)"
			d="M629.8 875.5s14.2-5.6 20.7-27.2c27.1 25.7 198.7 80.1 346.4 80.1 152.6-6.6 180.5-21.8 180.5-21.8s5-1.1 10.2-5.3c5.7-4.6 11.6-12.2 11.6-12.2s6 81.3-42.2 128.1c-19.4 18.9-51.2 28.5-90.2 30.6-136.1 7.3-194.7 17.1-350.8-42.2-45.7-12.3-68.4-49.6-78.5-81.5-8.2-26-7.7-48.6-7.7-48.6z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full18001h"
			cx={1011.9898}
			cy={2465.687}
			r={1024.6537}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.905} stopColor="#d9d9d9" />
			<stop offset={0.968} stopColor="#d9d9d9" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full18001h)"
			d="M630.2 876s18.2-4.6 24.7-26.2c27.1 25.7 198.7 80.1 346.4 80.1 152.6-6.6 180.5-21.8 180.5-21.8s5-1.1 10.2-5.3c5.7-4.6 11.6-12.2 11.6-12.2s3.5 151.3-132.5 158.7c-136.1 7.3-194.7 17.1-350.8-42.2C628.2 989 630.2 876 630.2 876z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full18001i"
			cx={2222.7783}
			cy={1664.8804}
			r={2860.6455}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.519} stopColor="#333" stopOpacity={0} />
			<stop offset={0.546} stopColor="#343434" />
		</radialGradient>
		<path
			fill="url(#ch-full18001i)"
			d="M628.8 874.5s15.2-4.6 21.7-26.2c27.1 25.7 198.7 80.1 346.4 80.1 152.6-6.6 180.5-21.8 180.5-21.8s5-1.1 10.2-5.3c5.7-4.6 11.6-12.2 11.6-12.2s5.1 88.2-50.9 133.9c-18.8 15.4-47.3 22.9-81.5 24.7-136.1 7.3-194.7 17.1-350.8-42.2-89.6-26.7-87.2-131-87.2-131z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full18001j"
			x1={830.0522}
			x2={893.0197}
			y1={1826.5067}
			y2={1999.5055}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.297} stopColor="#f2f2f2" />
			<stop offset={0.551} stopColor="#f2f2f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001j)"
			d="M822.3 532.5C799 535.1 696 573.8 637.5 632.9c22.1 34.7 60.8 137.6 414.8 11.6-6-18.5-43.7-92-230-112z"
		/>
		<path
			fill="#ff6900"
			d="M689.9 638.7S753.8 654.3 802 662c36.3-2.3 96.9-16.1 119.4-21.8 54.6-13.8 106.3-33.5 106.3-33.5s22 27.2 24.7 39.3c-15.6 5.9-149 48.9-202.3 53.9-53.4 5-118.4 17.4-183.4-21.8 8.6-22.1 23.2-39.4 23.2-39.4zm445.4-36.4c15 .5 53 5.7 78.6 30.6 15.4 13.7 21.8 37.8 21.8 37.8s-22.9-22.3-72.8-23.3c-7.5-14.7-20.8-36.2-27.6-45.1zm-602.6-94.6 5.8 42.2s-51.5-25.8-100.4 7.3c-3.5-28.9-4.4-42.2-4.4-42.2s23.4-14.2 48-16c24.9-1.9 51 8.7 51 8.7zm-16-36.4s-50.9-26-99 8.7c-.9-10.6-4.4-36.4-4.4-36.4s46.2-31.1 97.5-8.7c4.2 22.8 5.9 36.4 5.9 36.4zm-14.6-77.1-4.4-40.8s-59.3-18.6-91.7 10.2c3.6 23.5 5.8 40.8 5.8 40.8s20-13.5 42.2-16c23-2.6 48.1 5.8 48.1 5.8z"
		/>
		<linearGradient
			id="ch-full18001k"
			x1={921.6428}
			x2={897.3656}
			y1={1795.7651}
			y2={1894.8274}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.172} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001k)"
			d="M1024.7 606.7c-11.2 5-153.7 51.1-221.2 55.3-11.5 4.2-28.4 28.9-29.1 43.7 18 .9 125.5-4.5 278-58.2-6.7-18.4-16.6-32.6-27.7-40.8z"
			opacity={0.702}
		/>
		<radialGradient
			id="ch-full18001l"
			cx={756.5164}
			cy={1962.1589}
			r={246.6455}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.492} stopColor="#000" stopOpacity={0} />
			<stop offset={0.823} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full18001l)"
			d="M799 663.5c-7.6 6.8-20.1 15.3-24.7 42.2-24.7.8-90.3-11.5-107.7-29.1 8.2-19.7 14.8-34.2 24.7-39.3 12.9 4 81.3 19.6 107.7 26.2z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-full18001m"
			x1={965.6321}
			x2={817.749}
			y1={1956.2781}
			y2={1972.4218}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full18001m)"
			d="M838.3 520.8s81.1 12 116.4 23.3c35.4 11.3 8.7 20.4 8.7 20.4s-79.5-27.8-138.3-30.6c3.7-5.3 13.2-13.1 13.2-13.1z"
			opacity={0.502}
		/>
		<path
			d="M1169.8 692.8c.7-.2 38.3 12.2 52.8 37.6 5.1-5.7 7.3-10.2 7.3-10.2s-12-18-20.4-21.8c-16.1-7.3-40.2-5.5-39.7-5.6z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full18001n"
			x1={756.975}
			x2={545.049}
			y1={1794.1388}
			y2={2142.2861}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={0.501} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full18001n)"
			d="M500.7 356.3c4.9 12.2 27.3 66.6 55.3 96.1s102.3 77.4 276.6 69.9c-6.5 5.9-10.2 11.6-10.2 11.6s-14.8-7.8-106.3 42.2c-67.5 36.9-86.7 56.4-95.2 86.5 2.3 15.4 5.5 29.8.6 40.2-.1.9-7.2-3.8-7.3-2.9-24.2-1.7-58.2-4.4-58.2-4.4l-26.2-216.9s-3.6-2.9-13.1-5.8c-1.3-16.1-8.7-67-8.7-67l-4.4-1.5c0 .1-7.8-60.2-2.9-48z"
		/>
		<linearGradient
			id="ch-full18001o"
			x1={958.9218}
			x2={1121.5265}
			y1={1991.0552}
			y2={1968.6367}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.501} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full18001o)"
			d="M895.1 515c16.7-3 79.5-11 94.6-18.9 22.5 4.3 63.5 14.2 64 14.6.6.3 2.9 40.8 2.9 40.8s-45.2-14.3-88.8-23.3c-40.9-8.6-80.8-11.7-72.7-13.2z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full18001p"
			x1={388.6857}
			x2={722.1509}
			y1={1944.8634}
			y2={2006.3663}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.348} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001p)"
			d="M500.7 356.3c4.9 12.2 27.3 66.6 55.3 96.1s102.3 77.4 276.6 69.9c-6.5 5.9-10.2 11.6-10.2 11.6s-14.8-7.8-106.3 42.2c-70.8 38.7-88.5 58.3-96.4 90.9 5.9 13.8 3.5 24.6 1.8 37.2-24.2-1.7-65.5-8.7-65.5-8.7l-26.2-216.9s-3.6-2.9-13.1-5.8c-1.3-16.1-8.7-67-8.7-67l-4.4-1.5c0 .1-7.8-60.2-2.9-48z"
		/>
		<path
			fill="#737373"
			d="M525.4 405.8c18.5 20.2 109.5 129.1 411.9 78.6 30.6 5.5 55.3 11.6 55.3 11.6s-57.9 16.2-149.9 24.7-262.9-.3-317.3-114.9z"
		/>
		<linearGradient
			id="ch-full18001q"
			x1={1087.969}
			x2={1293.2054}
			y1={1469.1862}
			y2={1521.8817}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#e3e1e1" />
			<stop offset={0.641} stopColor="#787878" />
			<stop offset={1} stopColor="#616161" />
		</linearGradient>
		<path
			fill="url(#ch-full18001q)"
			d="M1189.1 963.3c17.3 33.2 69.1 174.1 74.2 170.3 13.8-33.7 77.3-52.2 129.5-64 .6-7.9 1.5-17.2 1.5-17.5-30.5 10.4-64.4 21.2-99 32-25.1-61.1-87.3-214-87.3-214l-5.6-4.5-1.7 4.5-1.5 11.6 4.4 7.3c.1.1-1 42.9-14.5 74.3z"
		/>
		<linearGradient
			id="ch-full18001r"
			x1={670.9359}
			x2={670.9359}
			y1={2203.3999}
			y2={2392.7}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.202} stopColor="#f2f2f2" stopOpacity={0} />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full18001r)"
			d="M858.7 152.6c-21.3-17.9-49.6-39.3-173.2-39.3-124.2 3.8-201.9 39.5-202.3 93.2-.5 53.7 5.8 96.1 5.8 96.1L634.6 253l-87.3-81.5 10.2-2.9 170.3-7.3c-.1 0 108.9-4.1 130.9-8.7z"
		/>
		<linearGradient
			id="ch-full18001s"
			x1={703.8124}
			x2={703.8124}
			y1={2056.0786}
			y2={2440.3367}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.3305} stopColor="#000" />
			<stop offset={0.6562} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full18001s)"
			d="M900.9 462.6c1.6-.1 21.8 7.3 21.8 7.3l2.9 14.6s-7.9 6.3-42.2 8.7c-34.3 2.4-219.6 17.3-320.2-50.9-13.7-9.3-67.3-48.4-78.6-176.1-1.1-12.9-8.2-65.3 7.3-90.2 11-17.7 35-55.5 208.1-64 3.8-.2 123.1-.7 158.7 42.2-61.4 5-313.3 13.8-311.5 16 16.1 19.9 33.5 33.5 33.5 33.5l283.8 267.8c0-.2 34.9-8.8 36.4-8.9z"
			opacity={0.302}
		/>
		<path
			fill="#671e75"
			d="M548.7 170c-2.9-6.9-13.3-22.8-24.7-24.7 9.6-3.6 20.4-7.8 33.7-12.1 9.5 6.4 19.7 15.7 31.3 34.7-17.3.8-31.3 1.5-40.3 2.1zm160.1-56.7c5.7 0 15.8 0 28.2.9 32.9 18.8 52.2 33.5 63.6 44.6-51 2-129.7 5.3-187.8 8-6.8-11.1-17.1-24.5-32.9-39.9 30-7.5 70.4-13.6 128.9-13.6zm139.7 43.6c-4.4.1-13.1.4-24.8.9-12.7-13.7-31-27.2-51.2-39.4 29.4 5.4 60.4 16.3 76 38.5zm-174.6 20.4 345-17.5 1.5 21.8 81.5-10.2 168.8 24.7-161.6 2.9-101.9-13-59.7 4.4 80.1 13.1-104.8 13.1-167.4-29.1v11.6l-42.2 2.9-42.2-21.8s.6-.9 2.9-2.9zm675.3 87.3-97.5 5.8-246-40.8-10.2-5.8 99-5.8 254.7 46.6zm-104.8-39.3 64-8.7 100.4-14.6 211.1 45.1-94.6 7.3-280.9-29.1z"
		/>
		<linearGradient
			id="ch-full18001t"
			x1={461.7946}
			x2={905.4278}
			y1={2148.0181}
			y2={2214.5559}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.095} stopColor="#d9d9d9" />
			<stop offset={0.3} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001t)"
			d="M547.2 170c16.1 19.9 33.5 33.5 33.5 33.5l283.8 267.8s34.8-8.6 36.4-8.7 21.8 7.3 21.8 7.3l2.9 14.6s-7.9 6.3-42.2 8.7c-34.3 2.4-219.6 17.3-320.2-50.9-13.7-9.3-67.3-48.4-78.6-176.1-1.1-12.9-8.2-65.3 7.3-90.2 3-4.8 7-14.5 13.1-16 32.1-28.6 40.9 8.4 42.2 10z"
		/>
		<linearGradient
			id="ch-full18001u"
			x1={522.0182}
			x2={965.6501}
			y1={2084.0645}
			y2={2315.4963}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2074} stopColor="#d9d9d9" />
			<stop offset={0.498} stopColor="#d9d9d9" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001u)"
			d="M547.2 170c16.1 19.9 33.5 33.5 33.5 33.5l283.8 267.8s34.8-8.6 36.4-8.7 21.8 7.3 21.8 7.3l2.9 14.6s-7.9 6.3-42.2 8.7c-34.3 2.4-219.6 17.3-320.2-50.9-13.7-9.3-67.3-48.4-78.6-176.1-1.1-12.9-8.2-65.3 7.3-90.2 3-4.8 7-14.5 13.1-16 32.6-29.8 40.9 8.4 42.2 10z"
		/>
		<linearGradient
			id="ch-full18001v"
			x1={1606.2517}
			x2={1150.6625}
			y1={2046.6105}
			y2={2259.0547}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={1} stopColor="#404040" />
		</linearGradient>
		<path
			fill="url(#ch-full18001v)"
			d="M1125.1 386.9s109.5-23.8 195-42.2c-6.2-19.8-13.9-32.6-23.3-50.9 28.8-2.7 49.2-.1 69.9-4.4-3.2-8.3-7.2-15.4-11.6-24.7 70.8 7.5 158.7 18.9 158.7 18.9s38.2-3.6 67-5.8c-11.8 19.2-29.7 49.1-34.9 56.8-76.1 10.7-227.4 35.7-259.1 42.2 20 15.7 142.6 93.2 181.9 123.7-17 30.2-29.1 50.9-29.1 50.9s-17.5-29.5-30.6-49.5c-95.2-38.3-283.9-115-283.9-115z"
		/>
		<path
			fill="#f2f2f2"
			d="M656.4 842.5c143.8 61.2 266.7 58.3 308.6 61.1 9.9 8.4 17.5 11.6 17.5 11.6l62.6-10.2 16 10.2 83-10.2 8.7-8.7 50.9-11.6s-5.2 16.8-16 21.8-85.3 15.4-110.6 17.5c-25.4 2.1-67.8 12.3-152.8-1.5-31.8-5.1-66.5-8.4-101.7-14.6-59-10.4-119.2-26.7-169-55.3 1.1-3.4 2.5-7.2 2.8-10.1z"
		/>
		<linearGradient
			id="ch-full18001w"
			x1={900.0554}
			x2={853.7817}
			y1={1691.1722}
			y2={1778.2712}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.304} stopColor="#000" stopOpacity={0} />
			<stop offset={0.599} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full18001w)"
			d="M656.4 842.5c143.8 61.2 266.7 58.3 308.6 61.1 9.9 8.4 17.5 11.6 17.5 11.6l62.6-10.2 16 10.2 83-10.2 8.7-8.7 50.9-11.6s-5.2 16.8-16 21.8-85.3 15.4-110.6 17.5c-25.4 2.1-67.8 12.3-152.8-1.5-31.8-5.1-66.5-8.4-101.7-14.6-59-10.4-119.2-26.7-169-55.3 1.1-3.4 2.5-7.2 2.8-10.1z"
			opacity={0.4}
		/>
		<path
			fill="#f2f2f2"
			d="m994.1 229.7-34.9 7.3 45.1 5.8-10.2-13.1zm2.9-71.3-326 20.4-117.9-10.2 334.8-17.5 109.1 7.3z"
		/>
		<path
			fill="#5e5e5e"
			d="M1135.3 752.2c3.8 32.1 11.6 71.3 11.6 71.3s4 35.3 2.9 72.8c-.6 2.2-3.2 9.6-7.3 11.6-26.7 1.3-58.4 6.2-80.1 5.8-13.2-4-10.9-3.6-14.6-7.3 0-17.3 1.5-46.6 1.5-46.6l-20.4-4.4 29.1-129.5 71.3-5.8c.2.1 3.6 11.7 6 32.1"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-full18001x"
			x1={1164.1559}
			x2={1073.5677}
			y1={1675.4429}
			y2={1670.1564}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4c4c4c" stopOpacity={0} />
			<stop offset={0.3837} stopColor="#e6e6e6" stopOpacity={0} />
			<stop offset={0.4741} stopColor="#e6e6e6" />
			<stop offset={0.4902} stopColor="#dbdbdb" stopOpacity={0.7802} />
			<stop offset={0.5185} stopColor="#bdbdbd" stopOpacity={0.3927} />
			<stop offset={0.5473} stopColor="#999" stopOpacity={0} />
			<stop offset={1} stopColor="#666" />
		</linearGradient>
		<path
			fill="url(#ch-full18001x)"
			d="M1139.6 787.2c3.8 32.1 10.2 72.8 10.2 72.8s2.8 24.5 1.5 37.8c-.6 6.1-5.8 7.3-5.8 7.3l-84.4 11.6 11.6-158.7 61.1-5.8s3.4 14.6 5.8 35z"
			opacity={0.902}
		/>
		<linearGradient
			id="ch-full18001y"
			x1={1135.2896}
			x2={1069.75}
			y1={1788.0134}
			y2={1751.626}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4c4c4c" stopOpacity={0} />
			<stop offset={0.3928} stopColor="#999" />
			<stop offset={0.4824} stopColor="#fff" stopOpacity={0.9} />
			<stop offset={0.5544} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.9259} stopColor="#666" stopOpacity={0.5} />
		</linearGradient>
		<path fill="url(#ch-full18001y)" d="m1135.3 752.2-61.1 5.8-17.5-30.6 72.8-5.8 5.8 30.6z" />
		<linearGradient
			id="ch-full18001z"
			x1={434.5}
			x2={434.5}
			y1={1569.8121}
			y2={1798.8566}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full18001z)"
			d="M382.8 726c4.2-5 104.8-10.2 104.8-10.2l-50.9 40.8L444 892s-28.3 19.4-32 52.4c-13.3 1-30.6 0-30.6 0s23.3-6.6 23.3-67c-.1-44 5.8-124.9-21.9-151.4z"
		/>
		<linearGradient
			id="ch-full18001A"
			x1={376.85}
			x2={376.85}
			y1={1635.264}
			y2={1696.8016}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full18001A)"
			d="M363.8 814.8c4.2-5 24.7-2.9 24.7-2.9l2.9 21.8s-1.3 21 0 37.8c-13.3 1-29.1 1.5-29.1 1.5s9-8.2 8.7-29.1c-.5-11.7.1-27.9-7.2-29.1z"
		/>
		<linearGradient
			id="ch-full18001B"
			x1={638.7823}
			x2={638.7823}
			y1={1638.0354}
			y2={1773.9807}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4d4d4d" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full18001B)"
			d="M618.6 737.7c4.2-5 23.3 27.7 23.3 27.7s22.1 35 16 75.7c-7 34.1-29.1 32-29.1 32s-2.9.7-2.9-59.7c-3-24.8-6.1-51.9-7.3-75.7z"
		/>
		<path d="M372.6 811.9c-.4-4.9 3.1-16 7.3-16s7.3 8.2 7.3 14.6c-4.7 1.4-9.9 1.7-14.6 1.4z" opacity={0.502} />
		<path d="M388.6 873.1c.4 4.9-3.1 15.9-7.3 15.9s-7.3-8.2-7.3-14.5c4.6-1.3 9.8-1.6 14.6-1.4z" opacity={0.502} />
		<path
			fill="#8c8c8c"
			d="M788.9 1028.8c22.9 10.5 100.1 28.3 193.6 27.7-5.1 28.3-27.6 105.6-27.6 105.6s-73.4-5.8-150-28.4c-9.9-10.8-38.6-42.4-67-72.8-4.6-9.2-15.8-44.8-17.5-52.4 18.5 4.3 48.9 14.2 68.5 20.3zM997 1055c.7-.5 52.1-2.3 75.7-7.3-5 27.6-14.6 62.5-18.9 75.7-21.2 13.6-55.8 29.3-78.6 40.8 6.8-45.1 21.1-108.7 21.8-109.2z"
		/>
		<linearGradient
			id="ch-full18001C"
			x1={1024.6553}
			x2={1018.692}
			y1={1336.2324}
			y2={1449.6558}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.75} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full18001C)" d="m1069.6 1049.2-73.3 5.8-23.3 107.7 78.6-40.8 18-72.7z" opacity={0.4} />
		<linearGradient
			id="ch-full18001D"
			x1={861.3006}
			x2={882.6681}
			y1={1324.5297}
			y2={1459.8964}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.75} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path
			fill="url(#ch-full18001D)"
			d="M787.4 1027.3c12.4 4.2 81 30.6 192.1 27.7-9 39.1-24.7 107.7-24.7 107.7l-148.5-26.2s-16.6-89.9-18.9-109.2z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full18001E"
			x1={739.4985}
			x2={774.2602}
			y1={1354.5613}
			y2={1479.7393}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.75} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full18001E)" d="m785.9 1027.3-67-18.9 18.9 50.9 67 74.2-18.9-106.2z" opacity={0.502} />
		<path fill="#666" d="M997 1055h-17.5l-20.3 94.6h16L997 1055z" />
		<path
			fill="#6c6c6e"
			d="M462.8 1154v21.8l48-4.4 74.2-5.8 5.8 13.1s32 14.4 37.8 29.1c1.5-19.6 1.5-37.8 1.5-37.8l17.5-1.5 8.7-11.6-1.5-4.4-192 1.5z"
		/>
		<linearGradient
			id="ch-full18001F"
			x1={403.0552}
			x2={452.8761}
			y1={1458.7693}
			y2={1461.2583}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.601} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full18001F)"
			d="M465.9 1175.7c-3.9.1-25.7 11.8-30.6 21.8-4.2-53.2-16-218.3-16-218.3s-4.5-33.5-2.9-49.5c6.8-10.3 15.3-29.6 27.7-36.4.5 5.4 21.8 256.2 21.8 256.2s-.1 19.6 0 26.2z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full18001G"
			x1={451.1196}
			x2={301.7169}
			y1={1104.395}
			y2={1295.6204}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001G)"
			d="M509.4 1376.7c2.8 8.2 4.4 21.8 4.4 21.8s-23.3 12.3-64 18.9c-29.4 6.5-51.4 4.9-72.8 4.4-12.9-8.6-40.6-38.3-43.7-52.4-4.7-21.5-24.9-107.5 81.5-138.3-6.1 7.4-11.3 52.8 8.7 93.2 19.1 30.9 58.1 56.6 85.9 52.4z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full18001H"
			x1={450.9921}
			x2={351.8717}
			y1={1132.0079}
			y2={1258.8759}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001H)"
			d="M483.2 1370.9c-15.5-4.8-73.3-21.2-74.2-113.5-25.7 13.2-39.3 38.1-26.2 85.9 12.4 45.4 80.5 50.6 100.4 27.6z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full18001I"
			x1={1264.7202}
			x2={1152.1372}
			y1={1260.8813}
			y2={1404.9814}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001I)"
			d="M1318.7 1241.3c-4.8-1.5-31-21-43.7-46.6-8.5-17-9.4-40.9-9.9-52.3-4.8-9.9-8.5-17.4-10.5-20.5-28.9 9.8-46.9 21.2-74.2 33.5-24.3 18.3-19.6 49.8-11.6 71.3 6 21.9 26.8 37.2 58.2 39.3 30.4-.8 81.5-18.3 91.7-24.7z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full18001J"
			x1={460.5121}
			x2={460.5121}
			y1={1141.4957}
			y2={1347.2683}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001J)"
			d="M509.4 1376.7c-16.9 3.9-91-15.6-97.5-85.9-1.8-50.3 1.5-86.8 40.8-109.2 13.7-6.7 34.7-8.4 56.8-10.2-.3 12.7-13.1 101.9-13.1 101.9l-32 27.7 7.3 48c-.2 0 25.5 18.6 37.7 27.7z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full18001K"
			x1={460.5121}
			x2={460.5121}
			y1={1088.8641}
			y2={1294.6367}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001K)"
			d="M509.4 1376.7c-16.9 3.9-91-15.6-97.5-85.9-1.8-50.3 1.5-86.8 40.8-109.2 13.7-6.7 34.7-8.4 56.8-10.2-.3 12.7-13.1 101.9-13.1 101.9l-32 27.7 7.3 48c-.2 0 25.5 18.6 37.7 27.7z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full18001L"
			x1={1229.184}
			x2={1089.7314}
			y1={1227.7062}
			y2={1406.1951}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full18001L)"
			d="M1292.5 1253c-1.8 10.7-23.3 39.3-23.3 39.3s-7.3 2.2-48 8.7c-29.4 6.5-51.4 4.9-72.8 4.4-12.9-8.6-40.6-38.3-43.7-52.4-2.9-13.5-13.2-57.5 16-88.8 50.4-54 92.2-32.8 103.3-32-11 4-21.4 10.1-29.1 14.6-43.4 24.9-37.4 54.5-20.4 88.8 9.3 15 30.9 30.8 53.9 32 24.2 1.2 49.8-12.5 64.1-14.6z"
			opacity={0.302}
		/>
		<path
			fill="#a6a6a6"
			d="m1248.8 270.5 40.8 65.5s11.3 5.2 24.7 10.2c-33.9 8-192.1 40.8-192.1 40.8s159.1 62.5 286.7 113.5c.2 15.9-6.3 29.6-7.3 39.3 15.2 28.3 34.9 64 34.9 64s-123-11.9-225.5-24.8c-6.9-12.4-16.4-26.5-37.8-62.6-28.2-5.2-45.1-5.8-45.1-5.8l-16 10.2-184.9-39.3-4.4-13.1-24.7-7.3-33.5 8.7-314.5-295.4 2.9-5.8 116.4 10.2 42.2 23.3s22.3-1.6 43.7-2.9c-1.1-3.8.6-5.9 1.5-8.7 53.3 6.5 167.4 26.2 167.4 26.2s29.4 18.1 29.1 18.9 50.9 8.7 50.9 8.7l-8.7-17.5 253.3 43.7z"
		/>
		<path fill="#fff" d="m1410.4 503.3-291.1-116.4 20.4 78.6 262 75.7 8.7-37.9z" opacity={0.2} />
		<path
			fill="#afafaf"
			d="M1024.7 1471.3v23.3l37.8 36.4 71.3 5.8 13.1 20.4 136.8 78.6 53.9-7.3v-20.4l-50.9 5.8-136.8-135.4-13.1-17.5-71.3 48-40.8-37.7zm42.2-155.8c.9.4 40.8 40.8 40.8 40.8l-29.1 4.4-14.6-23.3s1.9-22.3 2.9-21.9zm-423.6 221.3c1.3 1.1 18.9 21.8 18.9 21.8v52.4l29.1-18.9v-24.7s-49.3-31.7-48-30.6zm10.2 192.1v21.8l-61.1 10.2s-41.6-43.9-77.1-81.5c-8.9-43.6-10.4-80.6-10.2-84.4.2-3.2 88.8 142.6 88.8 142.6l59.6-8.7zM521 1752.2l1.5 21.8-61.1 10.2-123.7-110.6-61.1-8.7-26.2-46.6 1.5-20.4 24.7 45.1 65.5-52.4 13.1 24.7 107.7 144.1 58.1-7.2zm764.2-294 34.9 104.8 75.7 52.4 53.9-10.2-1.5-17.5-52.4 5.8-103.3-136.8-7.3 1.5z"
		/>
		<linearGradient
			id="ch-full18001M"
			x1={512.4844}
			x2={379.7755}
			y1={1046.5663}
			y2={1032.6191}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.5} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#a6a6a6" />
		</linearGradient>
		<path
			fill="url(#ch-full18001M)"
			d="M506.5 1420.3s4.4-.9 5.8-2.9c.1-7.5-1.6-13.4-2.9-17.5-33.8 13.3-83 24.9-129.5 21.8-1 5.5 1.1 7.7 2.9 16 1.6 3.1 3.3 5.8 4.4 7.3 1.7 19.5 8.7 71.3 8.7 71.3s7.7 9.7 20.4 13.1c2.8-18.6 8.7-58.2 8.7-58.2l17.5 1.5 1.5-10.2s36.6 2 48-7.3c5.2 3.9 5.8 5.8 5.8 5.8l14.6-5.8-5.9-34.9z"
		/>
		<linearGradient
			id="ch-full18001N"
			x1={1269.4202}
			x2={1148.9946}
			y1={1161.3639}
			y2={1148.7063}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#a6a6a6" />
			<stop offset={0.5} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#a6a6a6" />
		</linearGradient>
		<path
			fill="url(#ch-full18001N)"
			d="M1266.3 1312.6s1.5-6.8 2.9-8.7c.1-7.5-.2-7.6-1.5-11.6-33.8 13.3-71.3 16.1-117.9 13.1-1 5.5-1.8 6.2 0 14.6 1.6 3.1 6.2 0 7.3 1.5 1.7 19.5 8.7 71.3 8.7 71.3s7.7 9.7 20.4 13.1c2.8-18.6 4.4-52.4 4.4-52.4h18.9v-7.3s28.5-7.1 42.2-8.7c.8.9 2.8 3.2 4.4 4.4 4.5-2 8.7-4.4 8.7-4.4l4.4-5.8-2.9-19.1z"
		/>
		<path
			fill="#d9d9d9"
			d="m312.9 1503.3 30.6 84.4L278 1643l-23.3-46.6 58.2-93.1zm43.7 109.2L462.9 1761l58.2-10.2-18.9-158.7 91.7 145.6 59.7-10.2 8.7-165.9-305.7 50.9zm206.6-158.7 74.2 78.6 52.4 33.5-32-40.8-94.6-71.3zm518.2-72.8 53.9 78.6-71.3 49.5-39.3-37.8 56.7-90.3zm72.8 99 270.7-46.6 21.8 152.8-52.4 8.7s-100.2-137.5-100.4-136.8c-.2.7-7.3 1.5-7.3 1.5l49.5 147-52.4 8.7-129.5-135.3z"
		/>
		<linearGradient
			id="ch-full18001O"
			x1={592.4}
			x2={500.7}
			y1={830.15}
			y2={830.15}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full18001O)"
			d="M590.9 1759.5c.5-10 1.5-21.8 1.5-21.8l-81.5-133.9-10.2-11.6 13.1 83c0-.1 42.5 49.1 77.1 84.3z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full18001P"
			x1={1395.9}
			x2={1285.3}
			y1={970.7}
			y2={970.7}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full18001P)"
			d="M1394.4 1613.9c.5-10 1.5-18.9 1.5-18.9L1294 1456.7l-8.7 1.5 36.4 103.3c-.1 0 33.9 26.3 72.7 52.4z"
			opacity={0.502}
		/>
		<path
			fill="#f2f2f2"
			d="m1209.5 787.2 184.9-27.7 16-36.4s5.5-42.6 5.1-64c-19.1-27.1-27-38-40.4-53.4-34.8-6.7-78.2-18-78.2-18L1211 579l-7.3-13.1s-48.6-8.4-86.9-16.5c-47.8-4.7-99.4-9.7-99.4-9.7s145.5 29.6 205.2 106.3c31 46.1 11.8 70.4 0 84.4-8.5 24.7-13.1 56.8-13.1 56.8z"
		/>
		<linearGradient
			id="ch-full18001Q"
			x1={1151.8921}
			x2={1299.0721}
			y1={1849.8806}
			y2={1964.8696}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.101} stopColor="#68126a" />
			<stop offset={1} stopColor="#ff6900" />
		</linearGradient>
		<path
			fill="url(#ch-full18001Q)"
			d="m1139.6 580.5 273.6 75.7-39.3-53.9-74.2-16-85.9-7.3-11.6-14.6-85.9-16-78.6-7.3 101.9 39.4z"
		/>
		<path
			fill="#d9d9d9"
			d="M1238.6 688.2c0-1.6 170.3 33.5 170.3 33.5l-14.6 36.4-183.3 29.1 7.3-45.1 5.8-14.6s14.5-6 14.5-39.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m970.7 1166.6 81.6-43.2 18.9-75.7s69 1.5 97.5-43.7c11-14.2 21.8-43.7 21.8-43.7l65.5 160.1-26.2 11.6s-81.5-22.9-117.9 46.6c-42.3 77.1 36.4 125.2 36.4 125.2l1.5 14.6-37.8-16-46.6 8.7-1.5 24.7 16 24.7-7.3 5.8-1.5 27.7-46.6 77.1v20.4l37.8 40.8 75.7 5.8 8.7 20.4 135.4 77.1 56.8-8.7v-20.4s-19.7-49.8-18.9-48 75.7 56.8 75.7 56.8l53.9-8.7v-21.8l-23.3-151.4-139.7-101.9h-16l-2.9-24.7 4.4-2.9-1.5-13.1s12.9-13.2 23.3-37.8c11.7-3 29.1-8.7 29.1-8.7s21.7 13.1 50.9 11.6c1.7 23 2.9 52.4 2.9 52.4l7.3 2.9 59.7-7.3s12.7-33.9 27.7-78.6c19.1-22.5 26.2-32 26.2-32l1.5-34.9-13.1-11.6-29.1-93.2-5.8-4.4-20.4 1.5-2.9-7.3 27.7-8.7 5.8-10.2-106.3-250.4s.2-2.6.3-7m-143-132.8s-32.4-37.8-93.2-62.6m-2.3-21.9c-1.8-15.3-3.4-27.5-3.4-27.5m17.4-11.7 43.7 5.8 37.8 62.6 225.6 23.3 42.2-69.9-10.2-34.9-186.3-122.3 208.1-33.5 62.6-8.7 30.6-14.6 40.8-71.3-216.9-45.1-120.8 17.5 1.5-14.6-16-10.2-171.8-23.3-83 10.2 2.9-24.7-132.5-5.8L863 154s-49.6-53.7-205.2-39.3S482.3 171.4 483.2 221c.8 49.6 10.2 101.9 10.2 101.9s-63.3-12.4-88.8 10.2c3.8 40.6 8.7 78.6 8.7 78.6l-2.9 1.5 26.2 221.2 10.2 2.9 10.2 80.1-77.1 7.3c0 .1-24.4 11.4-24.4 94.4 0 71.4 5.5 125.4 23 125.4h36.4l17.5 253.3s-12 17.2-16 32c-24.3 7.6-87.3 25.8-87.3 106.3 0 53.1 49.5 85.9 49.5 85.9m210.8-244.7c25 9.2 30.4 18.7 30.6 18.9 16.2 16.2 18.2 54.2 18.1 68.1 0 1.4 5.8 2.8 5.7 4-.2 5.8-.6 9.4-.6 9.4v48l-18.9 27.7-32 80.1-37.8 10.2 104.8 81.5 32 43.7v23.3l-29.1 18.9-8.7 139.7-61.1 10.2-77.1-84.4 8.7 97.5-64 10.2-116.4-109.2-68.4-10.2-23.3-48s.5-16.9 0-17.5 59.7-97.5 59.7-97.5v-20.4l42.2-14.6-10.2-52.4 20.4-4.4m267.7-195c-.5-18.1-1.5-46.6-1.5-46.6h16l10.2-8.7-14.6-147s-10.3-89.7-11.5-104.3c-.1-1.3 1.3-2 1.3-2s12.2 80.9 88.8 100.4c9.4 27.8 17.5 52.4 17.5 52.4l71.3 77.1s104 18.4 146.6 25.9m3.4-12.7 17.5-1.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1176 519.4-36.4-56.8-20.4-75.7 291.1 115 29.1 50.9 29.1-52.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1141.1 464 260.5 75.7 34.9 61.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1410.4 504.8-10.2 37.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1119.3 386.9-50.9-42.2-75.7-119.4 101.9-7.3 256.2 46.6 18.9 24.7-75.7 5.8 29.1 50.9-203.8 40.9z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1318.7 346.1-29.1-8.7-219.8 7.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m998.5 228.2 251.8 40.8 99-4.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1248.8 269 43.7 69.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1624.3 247.2c-.8 1.7-97.5 7.3-97.5 7.3l-291.1-29.1s.7-1.5 1.9-1.7c11.2-1.4 68-7 68-7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1519.5 254.4-7.3 26.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1235.7 225.3-14.6 14.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1346.3 263.2s165.4 18.8 167.4 18.9 68.4-5.8 68.4-5.8l-36.4 56.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1142.6 512.1-11.6-1.5-18.9 11.6-185-39.2s-19.2-76-18.9-74.2 150.5 25.1 151.4 24.7 69.9 77.1 69.9 77.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1034.8 429.1 78.6 90.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1002.8 242.8-50.9-7.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1033.4 219.5V202l-110.6 13.1 45.1 107.7 16-4.4-32-83 45.1-5.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m921.3 216.6-165.9-27.7s-1.3 7.7-.9 8.5c5.1 11.6 46.1 97.8 46.1 97.8L968 322.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M666.6 177.3s-85.8-4.8-96.4-5.7c-.7-.1-1.1-1.6-1.1-1.6l294-16"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M866 471.3s-295.8-278.1-318.8-300c-.2-.2-.2-1.6 1-1.9 23.1.7 52.9.6 52.9.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M550.1 170c-3.9-8.7-11.1-23.3-27.7-23.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M848.5 154c-7.3-10.8-47-29.8-64-34.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M433.7 515c7.4-14.9 72.9-26.6 99-7.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M413.3 443.7c7.4-14.9 72.9-26.6 99-7.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M407.5 363.6c6.9-13.9 63-25 91.8-10.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M411.9 404.4c7-14 63.8-25.2 92.4-10.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M417.7 478.6c7.4-14.9 72.9-26.6 99-7.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M438.1 557.2c7.4-14.9 72.9-26.6 99-7.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1066.9 699.8c6.9-.3 19-20.5 5.8-62.6-14.6-46.6-42.3-89.4-234.3-116.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m605.5 1400 20.4-90.2-39.3-142.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m496.3 1324.3 128.1-16" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M493.4 322.9s9.8 102.2 113.5 142.6c102.5 39.9 256 35.5 329.4 19.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M513.8 384c15.9 29 65.5 203.7 479.5 113.6m60.5 11.6 1.5 40.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m758.3 188.9 147-2.9 125.2 16" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1021.7 181.7-17.5 4.4v13.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1005.7 186 106.3 13.1 164.5-2.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1110.5 202v17.4m1.2 1.6H1266.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1097.4 216.6-64-11.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1004.3 186-64 4.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1018.8 158.4 668 177.3l199.4 292.6 34.9-7.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m755.4 197.7-46.6 2.9 165.9 251.8 48 16" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1024.7 1471.3 40.8 37.8v21.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1065.4 1509.1s60.7-44 67.8-49c.4-.3 2.1-.5 2.1-.5v78.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1069.8 1397.1 10.2-16 58.2 80.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1136.7 1458.2c.2.6 13.1 18.9 13.1 18.9l133.9 136.8 53.9-7.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1283.7 1615.4v20.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1146.9 1558.6v-83" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1152.7 1478.6 272.2-45.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1283.7 1458.2c-.1.6 43.7 119.4 43.7 119.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1394.4 1615.4v-21.8l53.9-7.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1395.8 1593.6-103.3-136.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1080 1359.2 77.1-13.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1064 1311.2 43.7 43.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1149.8 1321.4h5.8l8.7 67s3.8 9.4 24.7 17.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1206.6 1423.3s4-71.7 4.4-78.6c35.6-1.3 42.2-7.3 42.2-7.3l4.4 5.8 7.3-5.8 29.1 64-7.3 5.8 2.9 4.4s-9.6 6.3-21.8 11.6c-14.6 6.3-61.1 5.8-61.1 5.8v-7.3l-18.9-2.9 2.9-67 17.5 1.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1155.7 1321.4s82.9 1 112.1-14.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1148.4 1303.9c10 7.1 98.4-.9 122.3-13.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1229.9 1132.1-46.6 21.8s-39.8 23.5-8.7 80.1c24.1 41 72.8 32 72.8 32l71.3-21.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1254.6 1119 10.2 18.9s-2.8 31.5 11.6 56.8c26.6 46.6 48 49.5 48 49.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1432.2 1050.6-39.3 5.8-14.6 99-27.7 20.4v42.2s12 9.4 21.6 16.9c1 13 .2 25.3.2 25.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1379.8 1149.6-8.7 49.5 1.5 56.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1473 1104.5 17.5 71.3-43.7 119.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1487.5 1178.7-115 18.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1264.8 1135.1c6.8-14.4 16.2-40.1 123.7-65.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1436.6 1040.4-141.2 43.7-91.7-221.2-2.3-3.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1235.7 934.2 164.5-49.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1229.9 919.6 164.5-49.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1355.1 774.1c-1.5-4-1.3-3.3-3.6-9.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1254.6 1340.3 32 71.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m312.9 1501.9 39.3 107.7L464.3 1761l55.3-10.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M461.4 1758v26.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m352.2 1612.5 310-50.9s-.3 25.7-.7 52.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m663.7 1561.5-104.8-115" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m689.9 1568.8-59.7-43.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M500.7 1592.1s9.5 52.6 15.2 94.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m502.1 1589.2 90.2 148.5 62.6-10.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M592.4 1737.7v23.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m353.6 1466.9 34.9-8.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m340.5 1592.1-1.5 83" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m353.6 1609.6-1.5 72.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m343.5 1589.2-65.5 55.3v21.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m253.2 1595 24.7 46.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m376.9 1424.7 5.8 16s34-.3 67-5.8c31.8-5.4 62.6-16 62.6-16l-2.9-13.1 2.9-5.8 14.6 43.7s19.1-1 45.1-4.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m505 1420.3 10.2 56.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m385.7 1440.7 10.2 75.7s9.5 12.1 21.8 14.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M441 1472.7s-13.5-.5-17.5-1.5c-1.3 21.4-7.3 74.2-7.3 74.2s14.7-.4 18.9 0c-1 3.6 0 7.3 0 7.3s40.9 4.1 69.9-4.4 24.7-11.6 24.7-11.6l-1.5-4.4 10.2-5.8-32-69.9-7.3 4.4s-4.4-6.1-5.8-7.3c-1.9.8-4.5 10.7-49.5 7.3-1.1 7.3-2.8 11.7-2.8 11.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m441 1469.8-7.3 81.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m493.4 1455.3 33.5 81.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M375.5 1421.8c14.5 2.8 102.5-3.3 135.4-23.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M417.7 1226.8c-6.5 15-17.7 56.3 7.3 99s48.4 44.1 84.4 55.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M409 1255.9s-40 20.8-29.1 75.7 76.9 53.9 81.5 52.4 18.9-11.6 18.9-11.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M429.3 1203.5c4.6-7.4 16.8-28.9 83-30.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m509.4 1174.4-11.6 97.5-33.5 26.2 5.8 49.5 40.8 27.7L524 1435"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M614.2 1257.3c.4-.1 27.7 8.7 27.7 8.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m653.5 1154-190.7-1.5L436.6 758l83-65.5 96.1 8.7 17.5 224.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m461.4 1152.5 1.5 13.1s51.7.9 98.2 1.7m26.6.5c25.8.4 45.4.8 45.4.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m451.2 977.9 186.3 4.4m1 17.4-182.9-4.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M414.8 947.3c-1.3-12.9 10.4-45 30.6-55.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M378.4 724.6c15.1-.7 27.7 41.8 27.7 113.5 0 96.1-17.2 106.3-29.1 106.3"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M363.1 811.9c3.6 0 6.5 13.4 6.5 29.8s-2.9 29.8-6.5 29.8-6.6-13.4-6.6-29.8 3-29.8 6.6-29.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M363.8 811.9h23.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M363.8 871.6H390" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M372.6 809c.5-2.5 1.5-14.6 5.8-14.6 9.9 0 11.6 29.4 11.6 56.8s-4 37.8-8.7 37.8c-4.8 0-7.3-14.6-7.3-14.6"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m556 694-27.7-215.4s-11.4-9.8-52.4-8.7c-16.6.9-43.8 9.1-46.3 17.7 4.5 39.9 17.2 156.9 17.2 156.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m518.1 474.2-8.7-68.4s-58.3-24.8-99 10.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m503.6 402.9-10.2-80.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M721.9 1007s100.9 32.8 168.8 43.7c68 10.9 179.6-1.8 193.6-4.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m995.5 1055-23.3 110.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m978.1 1055-23.3 109.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m785.9 1027.8 22.9 107.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M620 736.2s39.3 36 39.3 85.9-27.7 52.4-27.7 52.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M656.4 852.7s70.2 57.4 337.7 77.1c25.2 1 59.7-2.9 59.7-2.9s30.1-2.3 60.7-6.2c29.6-3.8 59.5-9.1 63.1-9.8 7.2-1.4 21-7.6 24.3-20.3.2-1 1-2.4 1.9-3 .7 14.2-4.7 48-13.1 72.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1104.7 910.9 18.9 10.2s7 129.5-80.1 129.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M966.4 905.1c1 1.4 14.6 11.6 14.6 11.6l65.5-10.2 2.9-46.6-21.8 1.5-45.1 52.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1046.5 905.1c.4 1 14.6 10.2 14.6 10.2l83-10.2 11.6-10.2 43.7-10.2s3.1-.5 3.6-.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1149.8 842.5v56.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1133.8 752.2 8.7 152.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M656.4 842.5c26.1 12.4 153.5 62.8 310 62.6 35.5-32.1 56.8-48 56.8-48l7.3-2.9 26.2-128.1s71.2-5.6 72.8-5.8c10.1 53.1 20.4 120.8 20.4 120.8l42.2 39.3 8.7-1.5 3.2 6.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1072.7 758.1-16-29.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1091.8 801.3c4.5-.5 17.5-1.7 22.1-2.4 6.7-.7 14.4.6 10.5 17.6-3.4 15.6-10 33.4-11.6 37.6-1.6 4.1-7.6 18-13.9-1.2-6.3-19.1-12.6-34.6-13.9-38.7-1.5-4.1-.6-12.3 6.8-12.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1061 912.4 10.2-152.8s50.5-4.6 61.8-5.6c.8-.4 1.2-1.4 1.6-2.8-1.7-7.6-6.7-29.4-6.7-29.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1017.4 860 30.6-147 90.2-4.4 21.8 142.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1120.7 644.5c8.8 0 16 7.2 16 16s-7.2 16-16 16-16-7.2-16-16 7.2-16 16-16z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1199.3 884.7s8.5-119.2 23.3-157.2c6.8-2.6 10.2-8.7 10.2-8.7s23.8-46.1-21.8-85.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1122.2 573.2c-114.4-50.3-228.5-55.3-228.5-55.3l-53.9 1.5s-7.7 5.5-20.4 13.1c-57.8 6.6-141.2 64.2-176.1 91.7-34.9 27.5-18.9 56.8-18.9 56.8s4.7 39.1 68.4 61.1 126.4 21.4 171.8 13.1c45.4-8.3 148.9-50.6 184.9-53.9 35.9-3.2 113.5-10.2 113.5-10.2s53.5-6.1 68.4 29.1m-8.8 8.8c-11.3-12.9-28.3-42.9-93.2-34.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M834 762.4c4.6 25.1 13.1 51.1 13.1 132.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M822.3 532.5c23.1.3 167.6 16 219.8 91.7 12.5 18.1 16 48.8 11.6 75.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M669.2 679.3c22.2 12.7 55.6 24.8 92 26.4 116.2 4.9 277.9-58 292.6-59.7m107.7 0c17.3 4.6 57.7 3 76.2 29.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M684.1 637.3c53.3 11.5 91.9 20 118.5 25.3 93.1-10 194.2-45.8 225.3-55.2m107.6-4.1c17.8 3 51 3 70.3 23.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M802 662c-10.1 6.1-23.7 18.8-29.1 42.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1164.4 689.7c0-17.6 16.7-97.8-138.3-149.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m509.4 1172.9 75.7-7.3s.8 2.5 2.1 6.8" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m512.3 1381-17.5-56.8s1.9-33 4.1-62.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1209.5 787.2 184.9-27.7 16-37.8s2.3-35.4 4.4-66.2c-17-20.8-40.7-51.7-40.7-51.7l-77.1-16m-94.8-23.3s-43.5-7.7-86.7-15.2c-32.6-3.2-66.8-6.5-84.9-8.3m209.5 148.7 168.8 30.6m-16 36.3L1221.1 742s-1.5.1-2.6-.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1416.2 656.2-276.6-75.6" />
		<path
			fill="#e6e6e6"
			d="M678.2 739.1s-13.5-37.8-10.2-65.5 104.8-123.7 104.8-123.7l-161.6 11.6-333.2 118 1.5 20.4 21.8 27.7 29.8 31.5 35.7 52.9 36.4 49.5 253.3-13.1 14.6-58.2 13.1-1.5 2.9-46.6"
		/>
		<path
			fill="#d9d9d9"
			d="m676.8 736.2-292.6 75.7 30.6 46.6s152.8-2 240.2-10.2c4.8-14.2 13.1-42.5 15-57.4 8.9-2.4 14.1-3.7 14.1-3.7l2.9-48-10.2-3z"
		/>
		<linearGradient
			id="ch-full18001R"
			x1={379.558}
			x2={632.9296}
			y1={1778.9214}
			y2={1976.8772}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ff6900" />
			<stop offset={0.899} stopColor="#68126a" />
		</linearGradient>
		<path
			fill="url(#ch-full18001R)"
			d="M344.9 747.9c1.5-.9 314.4-128.1 314.4-128.1l106.3-69.9-21.8 1.5L588 564.5 279.4 680.9s64 67.8 65.5 67z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M678.2 739.1s-13.5-37.8-10.2-65.5 104.8-123.7 104.8-123.7l-181.9 14.6-312.9 115 1.5 20.4s19.2 22.7 52.1 58.9c17 26 35.3 53.1 35.3 53.1s1.7 2.3 4.3 5.9c9.6 13.1 32.1 43.6 32.1 43.6l253.3-13.1 14.6-58.2 13.1-1.5 2.9-46.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M404.6 860 671 790.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m376.9 825 4.4-13.1 295.5-74.2 13.1 5.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M279.4 680.9s11.5 13.4 58.5 61.5c16.2 26.8 37.6 60.8 37.6 60.8l37.8 55.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m764.1 552.8-107.7 67-314.7 128.1-8.1 12.6" />
	</svg>
);

export default Component;

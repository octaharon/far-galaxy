import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-full43000a"
			x1={852.4404}
			x2={852.4404}
			y1={1228.0997}
			y2={2603.3999}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#1a1a1a" />
		</linearGradient>
		<path
			fill="url(#ch-full43000a)"
			d="M588.2 647.4c93.6 31.4 224.7 34 224.7 34s8.4-10.2 14.2-18.6c.7-22.1-7.2-161.9-5.5-174.2 5 9.7 16.4 119.5 16.4 119.5l42.7 121.6 25.2 21.9s3.6-.4 9.7-1.2c11.2 205.4-33.9 154-12.9 344.1 16.2-111.4 58-305.5 54.4-349.5 50.4-6.4 119.9-15.4 119.9-15.4l5.5-30.7s15.5 12.9 24.1 20.8c-4.3 16.7-2 102.8-1.1 115.1 6.2 87.6 35 201 42.7 234.4 15.5 39.3 19.4 141.7 19.8 151.2 5.7 155.5 22.8 207.8 38.4 239.9 48 99.5 96.1 139.3 128.2 183-21.4-7.2-131.6-102.3-175.3-201.6-21.6-34.9-32.6-180.1-36.2-215.8-10.3-105.5-22.9-138-31.8-168.7-10.5-24.6-31.8-143.8-35-158.9-2.1 6.4-3.3 44-3.3 70.2 0 49 28.5 153.9 28.5 222.5 0 58.7-32.9 217.6-32.9 294.7 0 35.5 7.7 125.7 7.7 170.9 0 88.6-42.7 162-65.7 203.8 15-41.7 35.5-132 36.2-174.2 1.2-77.3-14.5-148.5-16.4-227.9-2.7-114.4 16.4-157.9 16.4-254.2 0-114-41.9-182.2-41.7-254.2-44.3 92.8-52.1 257.3-54.8 316.7 16.1 92.8 14 166.8 12.1 198.4 12.9 94.8 27.8 166.7 34 208.2-16.2-21.7-44.4-115.7-48.2-127.1-25.7 129.6-108.1 243.1-126.8 267.6 13.7-30.1 22.7-60.8 32.6-91.2 65.1-198.1 60.3-388.9 23-524.8-10.7 94.2-79.8 220.5-94.2 237.8-1.4 160-84 384.4-118.3 428.4 14-39.1 51.7-188.9 58.1-340.8-2 2.1-13.7 13-24.1 23 2.4-5.6 19.5-33.9 25.2-42.7 2.2-45.7.5-147-3.3-166.5-37.8 145.5-84.1 205.3-122.7 234.4 57.5-105.6 106.5-334.8 110.7-353.9-7.5-79-24-145-47.1-232.3 4.2 75.3-26.7 112.8-60.3 162.2-15.8 23.2-31 49.4-48.2 75.6-44.1 67.2-76.5 116.6-87.6 138.1-30.3 54.9-29.5 121.8-34 167.7-1.4 91.3 16.8 172.7 26.3 212.6-68.5-105.6-74-251.3-62.5-352.9 14.5-79.1 54.6-134.4 63.5-158.9 9.5-25.7 48.3-78.7 82.1-132.6 26.9-42.9 51.5-86.2 50.4-117.3 0-44.9-20.8-97.2-20.8-152.3-.4-79.3 30.3-157 38-179.9zm258.7 256.3c5.5-32.6 16.8-129-13.1-214.8-11.4 8.4-9.9 135.2 13.1 214.8zm-84.5 447.1c21.7-69.3 51.7-215.6 30.7-361.6-16.7 33.6-45.3 159.1-48.2 168.7.9 9 6.4 34.8 9.9 70.2 4.2 44.8 6.6 101.7 7.6 122.7zm-6.5-671.6c-14.1-1.7-54.5-6.9-59.2-7.7-6.5 6.2-27.3 40.8-34 82.1 2.1 60.8 50.3 272.8 63.5 315.5 7.7-29.2 13.1-95.2 13.1-111.8s-7.7-101.3-7.7-147.9c.2-46.5 1.9-110.4 24.3-130.2z"
		/>
		<path fill="#dadada" d="M1137.2 1024.3c6.5 3.7 23.9 13.8 15.4 55.9-5.5-13.4-12-40.4-15.4-55.9z" />
		<linearGradient
			id="ch-full43000b"
			x1={427.4125}
			x2={469.799}
			y1={1847.5046}
			y2={1920.9191}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.005} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full43000b)"
			d="M473.2 1172.2c7.8 20.2-8.8 66.3-41.7 73.4-3.3-16.1-5.5-25.2-5.5-25.2s-.2-25.7 21.9-41.7c16.1-5.2 12.7-3.5 25.3-6.5z"
		/>
		<path fill="#e6e6e6" d="M451.3 1179.9c6.2 5-12.1 42.9-24.1 35 2.1-23.2 21.3-37.2 24.1-35z" />
		<linearGradient
			id="ch-full43000c"
			x1={914.7}
			x2={914.7}
			y1={1677.4}
			y2={1719.6}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#b3b3b3" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full43000c)"
			d="M914.7 1372.4c11.6 0 21.1 9.5 21.1 21.1s-9.5 21.1-21.1 21.1-21.1-9.5-21.1-21.1 9.5-21.1 21.1-21.1z"
		/>
		<path
			fill="#d9d9d9"
			d="M915.9 1384.8c3.6 0 8.7 4.1 8.7 9.9s-4.7 8.7-8.7 8.7c-4.1 0-8.7-3.1-8.7-8.7-.1-5.8 5.1-9.9 8.7-9.9z"
		/>
		<path
			fill="#f2f2f2"
			d="M1194.2 641.9c-13 11.3-44.8 19.1-77.8 25.2-33 6-70.2 7.7-70.2 7.7l-7.7 58.1L907 751.5l-26.3-21.9L838 607.9s6.8 75.2-3.3 73.4c-2.4-5.6-5.3-14.1-7.7-17.6-4.8 6.2-9 12.9-15.4 17.6-136.4 0-265.8-48-278.3-52.6-1.5 4.3-6.5 55.9-6.5 55.9s-14.1-6.3-23-12.1c-25.5-75.1-7.7-173.1-7.7-173.1s4.7 3.3 8.7 5.5c6.2-62.2 45.4-117.9 71.8-149-12.8-12.2-30.3-25.2-30.3-25.2s-.6-55-2.2-94.2c15.1-.8 37.4-1.9 48.6-4.8-1.5-22.1-2.3-41.9-1.5-51 .5-6.3 49.6-13.6 60.3-3.3 20.6 20.2 10.8 69.6 18.9 58.5.6-.8 1.4-1.8 2-2.6 11.6-15.3 18-12.4 61.2-13.1 41.5-.2 226.8-5.5 226.8-5.5l10.9 6.5V75.9h-7.7V36.4h26.3v39.4h-6.5l-1.1 154.1 16.4 16.4v-119h-3.3V99.9h15.4v27.3h-4.4s.9 119.2 1.1 128.8c1.7 2 8.1 11.5 19.8 27.3 8.1 4.9 18.9 7.8 26.3 13.1 53.6 13.6 115.1 32.9 115.1 32.9l1.1 16.4 1.1 4.4 82.1-9.9 128.2 49.3-26.3 49.3-20.8 9.9-164.1 12 6.5 20.8 94.2 82.1 6.5 24.1-27.3 46.1-55.9-10.9c.2.3.4 10.8.2 19z"
		/>
		<path fill="#ccc" d="m574 245.2 1.1 107.4-30.7-21.9 1.1-94.2 28.5 8.7z" />
		<path
			fill="#b3b3b3"
			d="m796.4 309.9 109.6 35 32.9 73.4 9.9-6.5-19.8-49.3 35 10.9s-10.4-17.9-9.9-17.6 160 53.6 160 53.6l25.2 41.7-132.6-8.7 35 29.6 14.2 49.3 167.7 70.2 25.2 42.7-143.6-28.5-29.6-50.4-29.6-7.7-13.1 7.7-110.7-35-15.4-49.3-1.1-9.9-112.6-144.7 3.3-6.5z"
		/>
		<path fill="#999" d="m1143.8 454.5-1.1 3.3-100.8 14.2-34-28.5 135.9 11z" />
		<path
			fill="#ccc"
			d="M986 349.3v-18.6h-12.1l-69 14.2 34 73.4 9.9-6.5-19.8-51.5 27.3-6.5 29.7-4.5zM975 322v-14.2l72.4 17.6 122.7 2.2V346l-123.8-4.4L975 322zm139.1 87.6 70.2-8.7 17.6 18.6-51.5 6.5 15.4 31.8-24.1-4.4-27.6-43.8zm108.6 181.8 28.5 43.9 27.3-48.2-7.7-19.8-19.8 34-19.8-29.6-8.5 19.7zM1334.4 448l15.4-35-48.2 1.1 13.1-19.8 66.8-4.4-26.3 48.2-20.8 9.9z"
		/>
		<path fill="#e6e6e6" d="m1300.5 415 13.1-21.9-181.9-38.4-28.5 13.1 197.3 47.2z" />
		<path fill="#ccc" d="m1221.5 591.4 9.9-21.9-190.7-99.7 14.2 53.6 166.6 68z" />
		<linearGradient
			id="ch-full43000d"
			x1={1217.9867}
			x2={1229.0734}
			y1={2637.864}
			y2={2732.0977}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={0.504} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full43000d)"
			d="m1334.4 448 16.4-36.2-48.2 2.2-199.4-47.1-3.3 4.4 84.4 29.6 16.4 19.8-50.4 4.4 18.6 36.2 165.5-13.3z"
		/>
		<linearGradient
			id="ch-full43000e"
			x1={1262.5626}
			x2={1070.6223}
			y1={2502.0703}
			y2={2646.7075}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#999" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path
			fill="url(#ch-full43000e)"
			d="m1149.3 460 121.6 104.1-20.8 37.2-17.6-31.8-187.3-98.7 99.7-14.2 4.4 3.4z"
		/>
		<path
			d="M696.6 260.5c36.1 36.9 53.7 106.4 61.1 141.2-42.9 14.9-76.9 34.5-90.7 41.9-14.2-31.1-40.5-67.9-40.5-67.9l17.6-12.1-11-11s-10.2 7.6-16.4 12.1c-4.3-5.4-10.1-11.2-24.1-25.2 34.3-37.2 65.4-56.1 104-79zM667.1 415c0 3.2 2.9 5.5 5.5 5.5 2.5 0 4.4-2.1 4.4-5.5s-2-5.5-4.4-5.5c-2.4 0-5.5 2.4-5.5 5.5zm383.6-117.4c-5 .9-11.2 1.6-15.6 3.2-3.4-6.9-6.4-12.8-8.7-17.2 8.1 3.4 16.3 8.3 24.3 14zm-324.4 90.1c0 2.2.9 4.4 5.5 4.4 4.5 0 5.5-2.1 5.5-4.4s-1.5-5.5-5.5-5.5c-4.1-.1-5.5 3.3-5.5 5.5z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-full43000f"
			cx={786.7252}
			cy={3075.3896}
			r={1281.9613}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.219} stopColor="#000" stopOpacity={0} />
			<stop offset={0.509} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full43000f)"
			d="M827.1 662.6c0-38.9-4.5-209.8-27.3-273.9-10.3-28.9-49.2-143.8-98.7-130.4-37.2 21.9-168.3 91.5-197.2 246.5-4.3-3.1-8.7-5.5-8.7-5.5s-13.6 120.2 8.7 173.1c11.4 7 23 12.1 23 12.1s4.4-47.9 7.7-54.8c9.8 3.8 141.4 50.8 279.5 51.5 4-5 5.8-7.1 13-18.6z"
			opacity={0.102}
		/>
		<path
			fill="#ccc"
			d="m1074.7 555.4-28.5 120.5s107.9-2.4 150.1-36.2c-.3-5.5-1.1-16.4-1.1-16.4l-91-17.6-29.5-50.3z"
		/>
		<path fill="#999" d="m851.2 613.4 54.8 138-25.2-23-42.7-121.6 13.1 6.6z" />
		<linearGradient
			id="ch-full43000g"
			x1={671.1}
			x2={574.6862}
			y1={2815.3}
			y2={2815.3}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.496} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-full43000g)"
			d="M575.1 203.5c26.1 12.5 73.5 7.7 96-1.5-.3 6.5-.8 71.7-.6 72.7 0 0-44.2 31-93.2 76.7.2-57.1-3.9-134.6-2.2-147.9z"
		/>
		<linearGradient
			id="ch-full43000h"
			x1={988.0537}
			x2={967.2537}
			y1={3036.3999}
			y2={3036.3999}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.493} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path fill="url(#ch-full43000h)" d="M988.1 36.4v38.4h-20.8V36.4s1.6 4.3 10 4.3c8.3 0 10.8-4.3 10.8-4.3z" />
		<linearGradient
			id="ch-full43000i"
			x1={1010.0092}
			x2={996.8455}
			y1={2976.1775}
			y2={2976.1775}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.493} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path
			fill="url(#ch-full43000i)"
			d="M1010.1 103.2v24.1s-8.8 3.2-13.1-1.1v-23s3.3 1.1 6.5 1.1 6.6-1.1 6.6-1.1z"
		/>
		<linearGradient
			id="ch-full43000j"
			x1={981.5429}
			x2={972.777}
			y1={2939.1499}
			y2={2939.1499}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.493} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path fill="url(#ch-full43000j)" d="M981.6 76.9v151.9l-8.7-7.7v-143l8.7-1.2z" />
		<linearGradient
			id="ch-full43000k"
			x1={1005.6611}
			x2={999.0868}
			y1={2900.7}
			y2={2900.7}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" />
			<stop offset={0.493} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#bfbfbf" />
		</linearGradient>
		<path fill="url(#ch-full43000k)" d="M1000.2 130.7h5.5v121.2l-6.5-7.7c0-.1 1-106.6 1-113.5z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M588.2 649.5C554.6 741.1 551 789.3 551 842.4c0 53.1 34.4 116.3 12.1 179.6-10.3 29.2-49.9 84.5-90.6 150m-40.9 70.6c-31.3 58.6-56.9 120.6-60.4 177.3-6.4 104.3 10.7 235.8 66.8 325.4-21.6-86.9-39-196.9-14.2-329.8 12.1-64.8 83.9-151.4 120.5-212.6 61.4-103 103.9-128.3 96.4-226.8m22-220.3c9.4-42 21.7-71.3 35-85.5m-94.1-18.6c-9.9 30.7-16.7 97.3-6.5 165.4 12.7 84.7 202.2 528.9 47.1 1048.6 61.4-80.9 106.1-272.5 118.3-427.3 9-117-7.3-210.5-9.9-232.3-18.5-154.3-125.9-461-78.4-537.4m149.6-2.7c12.9 24.8 38.4 101.4 28.5 201.6s-27.1 88.6-13.1 222.5 119.7 353.9-47.1 744c51.6-92.9 129.1-167.7 150.1-377 12.7-126.2-38.5-285-44.9-421.9-5.3-111.3 33.6-136.4 18.4-284.9m-12 356c18.6-162.7 56.8-294 53.7-361.4m70.2-9.9c-21.7 47.4-44 116.2-42.7 211.5s47.6 139.8 41.7 275c-6 135.3-26 144.5-12.1 298.1s28.2 172.8-26.3 348.5c16.6-30.3 50.6-88.6 63.5-155.6 13.9-72.2-5.6-155.8-2.2-249.8 2.9-82.7 29.6-174.1 31.8-261.9 2.5-100.7-39.2-172.9-26.6-295.8 8.6-84.8 26.2-151.5 51.9-186.4.4.2.9.5.8 1.2-5.3 34.7-13.4 147 45.1 362.6 24.3 89.7 9.3 240.9 38.4 335.3 10.9 35.5 25.2 65.2 44.9 96.4 36.2 57.2 64.9 90.2 108.4 136.9-91.9-56.2-172.7-165.4-191.7-226.8-28.9-93.4-23.2-244.6-55.9-349.5-19.9-63.4-33.2-133.9-39.8-185.5m-128 652.6c13.1 49.9 30.4 98.3 51.7 143.3-16.4-76.5-29.7-151.9-38.3-225.7m-11-180.8c.1-117.5 15.9-229.6 53.6-334.3m-258.1 129.2c7.6-29.9 15.1-114.9 12.7-140.1-23.8-243.5 12-240.6 18.6-255.4m-1.1 1.2c-3.6 81.7 22 213 36.2 307.9 9.9 66.1 16.6 204.7-28.3 359.8m-62 155.1c-9.3 17.6-19.1 35-30.1 52.6 10.4-10.8 20.4-22 29.9-33.4m59.6-82.5c52.3-83.9 83.6-166 95.5-246.8m-10.8-281.6c-12.2-67.9-30-162.1-14.2-224.7M793.1 987c-9.6 21.5-27.7 88.2-48.4 172.6m-44 178.2c-23.1 89.2-53.3 174.6-124.4 237.7 49.3-99.8 85.8-232.6 112.9-364.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M914.7 1372.4c11.6 0 21.1 9.5 21.1 21.1s-9.5 21.1-21.1 21.1-21.1-9.5-21.1-21.1 9.5-21.1 21.1-21.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M914.7 1383.9c5.4 0 9.7 4.4 9.7 9.7s-4.4 9.7-9.7 9.7-9.7-4.4-9.7-9.7 4.3-9.7 9.7-9.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M473.2 1172.2c7.8 20.2-8.8 66.3-41.7 73.4-3.3-16.1-5.5-25.2-5.5-25.2s-.2-25.7 21.9-41.7c16.1-5.2 12.7-3.5 25.3-6.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1138.3 1024.3c11.3 6.1 22.8 28.5 13.1 58.1" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M449.1 1178.8c6.8-.5 4.3 13.5-2.2 24.1-8 13.3-22.8 16-18.6 7.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M997 101c3.1 3.4 9.9 4.3 14.2 0" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M966.2 37.5c2.8 4.4 18.3 4.2 24.1 0" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M534.5 629.8c-11.7-3.8-33.7-14.2-42.7-26.3m12.1-98.6c9.4 6.5 24.1 16.4 24.1 16.4l25.2 58.1-1.1 58.1m144.8-88.9c10.1 69.4.8 122.8.8 122.8m-143.4-52.5c107.6 40.8 240.2 44.3 272.8 44.9m224.6-7.3c59.2-4.9 113.6-14.6 142.4-26.7m-48.1 31.8c0-15.5-4.5-41.5-7.7-49.3M693 525.8c-9.7-48.1-29.1-101.6-66.6-149.7m-11.1-13.3c-7.4-8.3-15.3-16.4-23.8-24.4m76.7 105.2c27.2-17.2 57.7-30.8 89.5-41.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1013.4 126.2s-4.5 2.2-9.9 2.2c-5 0-8.7-2.2-8.7-2.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M966.2 74.7s8.3 2.2 11 2.2 12-2.2 12-2.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M677 556.4c4.5-1.1 12.5-4.9 20.8-7.7 9.6-3.2 19.4-5.5 24.1-6.5-2.5-8.7-6.5-19.8-6.5-19.8s-10.8 1.5-22.9 5.2c-7.7 2.3-15.8 6.2-22 9 2 5.4 4.3 14.7 6.5 19.8zm-65.7-170.9 32.9-21.9-11-10.9-30.7 21.9 8.8 10.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M690.7 446.5c2.9 0 5.3 2.4 5.3 5.3 0 2.8-2.4 5.3-5.3 5.3s-5.3-2.4-5.3-5.3 2.3-5.3 5.3-5.3zm-18.1-37.2c2.7 0 4.9 2.2 4.9 5s-2.4 5.4-5.3 5.4c-2.8 0-5-2.4-4.8-5.4 0-2.7 2.4-5 5.2-5zm72.7 11.6c2.8 0 5.2 2.3 5.2 5.3 0 2.8-2.3 5.3-5.2 5.3-2.8 0-5.2-2.3-5.2-5.3-.1-2.8 2.3-5.3 5.2-5.3zm-14.1-37.6c2.7-.2 4.9 1.9 4.9 4.6 0 2.8-2.4 5.3-5.3 5.3-2.7 0-4.7-2.2-4.6-4.8.3-2.7 2.5-4.9 5-5.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M594.3 472.1c10-.6 15.6 8.9 13 20.4-2.3 10.2-10.4 18.6-18.6 20-8.5 1.4-15.4-5.4-14.8-16.1.6-12.1 9.9-23.7 20.4-24.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M591.5 481.9c4.8-.3 7.6 4.4 6.3 10-1.1 4.9-5 9-8.9 9.7-4.2.6-7.4-2.6-7.2-7.8.3-5.9 4.7-11.5 9.8-11.9z"
		/>
		<path
			fill="#bfbfbf"
			d="M1234.7 805.2s-20.9 46.5-73.4 23c-10 5.7-26 14.8-43.6 11.9-26.6 23.3-69.9 17.9-82-3.1-17.2 4.7-36.4-.6-46.4-9.9-4.5 12.8-42.4 46.7-75.6-5.5-.3 14 4.2 43.9-35 47.1-9.6 11.4-48.7 38.9-83.3-9.9-24.4 39.1-76.4 18.2-87.6 0-16 1.4-49.4 11.7-63.5-40.5-4.4 2.4-17.4 6.3-31.8 2.2-10.4 6.1-26.7 18.9-62.5 5.5-1.7 13.9-81.6 41.3-101.9-35-50.7.2-59.2-32.9-60.3-48.2-20.8-6-34.4-27.7-35-50.4-.7-28.9 17.9-59.4 52.6-58.1 3.8-30.4 11-76.8 82.1-70.2 0 11.9-.1 41 1.1 54.8 30 .4 34.2 9.4 46.1 20.8 9.9-25.3 59.6-26 69.8 15.7 3.3-3.2 10.7-3.7 15.7-2.5 4.8 1.2 10.2 5.7 13.5 9.7 47.6 10.6 94.8 16.4 137.9 19.6 14.7-11.2 31.6-5.2 42.7 2.5 8.7-9.6 16.4-19.8 16.4-19.8S828 551.8 823 510.2c6.2 40.9 8.4 68.9 10.9 95.3 4.9 14.1 17.1 47.9 27.6 76.7 19.9 8.8 26.2 24.1 26.2 24.1s4.9-19.8 18.6-27.3c19.4-10.8 50.5-7.8 60.3 21.9 3.4-2.2 8.9-5.8 15.4-5.5 6.4.3 11.4 2.9 16.4 5.5 20.6-29.3 40.1-28.3 52.6-23 9.7 2.8 16.1 7.3 20.5 12.4 6-12.1 24.4-12.4 24.4-12.4l26.3 5.5s2.3-19.5 25.2-21.9c18.2-.9 31.8 15.4 31.8 15.4s2-14.3 9.9-23c8-8.8 21.9-12.1 21.9-12.1l7.7-13.1 31.8 6.5 5.5-9.9 8.7 7.7s5.3-14.2 32.9-14.2c27.7 0 50.3 30.7 36.2 62.5 13.1 9.6 17.6 15.6 17.6 30.7s-12.7 28.9-17.6 32.9c.5 8.3-.7 46.1-46.1 41.7-6 8.2-21 25.8-46.1 8.7-3.4 2.8-6.9 9.9-6.9 9.9z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000l"
			cx={853.3439}
			cy={-6041.8613}
			r={1006.6011}
			gradientTransform="matrix(1 0 0 0.3031 0 2450.8977)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.418} stopColor="#b3b3b3" />
			<stop offset={0.534} stopColor="#bfbfbf" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000l)"
			d="M1234.7 802.9s-20.9 48.7-73.4 25.2c-10 5.7-26 14.8-43.6 11.9-26.6 23.3-69.9 17.9-82-3.1-17.2 4.7-36.4-.6-46.4-9.9-4.5 12.8-42.4 46.7-75.6-5.5-.3 14 4.2 43.9-35 47.1-9.6 11.4-48.7 38.9-83.3-9.9-24.4 39.1-76.4 18.2-87.6 0-16 1.4-49.4 11.7-63.5-40.5-4.4 2.4-17.4 6.3-31.8 2.2-10.4 6.1-26.7 18.9-62.5 5.5-1.7 13.9-81.6 41.3-101.9-35-50.7.2-59.2-32.9-60.3-48.2-20.8-6-34.4-27.7-35-50.4-.7-28.9 17.9-59.4 52.6-58.1 3.8-30.4 11-76.8 82.1-70.2 0 11.9.9 41 2.2 54.8 30 .4 33.1 9.4 44.9 20.8 9.9-25.3 59.6-26 69.8 15.7 3.3-3.2 10.7-3.7 15.7-2.5 4.8 1.2 10.2 5.7 13.5 9.7 47.6 10.6 94.8 16.4 137.9 19.6 14.7-11.2 31.6-5.2 42.7 2.5 8.7-9.6 16.4-19.8 16.4-19.8s-2.7-112.9-7.7-154.5c6.2 40.9 8.4 68.9 10.9 95.3 4.9 14.1 17.1 47.9 27.6 76.7 19.9 8.8 22.9 23 22.9 23s6-18.6 19.8-26.3c19.4-10.8 52.7-7.8 62.5 21.9 3.4-2.2 8.9-5.8 15.4-5.5 6.4.3 11.4 2.9 16.4 5.5 20.6-29.3 40.1-28.3 52.6-23 3.1-3.6 27.3-4.4 27.3-4.4s42.6-5.4 53.6-8.7 52.1-10.6 63.5-24.1c-.3-7.6 0-16.4 0-16.4l23 4.4 31.8 7.7 6.5-10.9 6.5 6.5s14.6-14.2 31.8-14.2 42.7 17.4 42.7 43.9c-1.4 12.9-5.5 17.6-5.5 17.6s18.6 8.5 18.6 31.8-12.7 30.8-16.4 32.9c-.3 6.8.3 48.4-46.1 42.7-7.2 9.3-17.8 16.9-25.2 16.4-7.4-.5-18.1-4.4-20.8-8.7-3.4 2.9-7.6 8.5-7.6 8.5z"
		/>
		<radialGradient
			id="ch-full43000m"
			cx={1169.1802}
			cy={2336.5813}
			r={82.1803}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000m)"
			d="M1197.4 743.7s27-2.3 31.8-20.8c12.4 5.5 27.3 19 27.3 41.7s-21.6 40.5-43.9 40.5c-22.2 0-33.7-14.8-35-20.8 21.3-15.2 19.8-40.6 19.8-40.6z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000n"
			cx={1284.8661}
			cy={2351.0493}
			r={47.2351}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000n)"
			d="m1291.7 740.5-9.9 6.5s17.9 17.1 7.7 40.5c13.6.7 28.8-2.2 36.2-13.1 7.4-10.9 8.6-25.8 7.7-30.7-21.9 14-41.7-3.2-41.7-3.2z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000o"
			cx={1232.8239}
			cy={2341.718}
			r={111.7652}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000o)"
			d="M1256.6 766.8c-1.4 5-2.9 18.7-14.2 28.5 5.8 5.3 14.2 7.7 20.8 7.7s29.6-9.4 29.6-30.7c0-18.1-13.1-25.2-13.1-25.2s-10.1 18.1-23.1 19.7z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000p"
			cx={1209.0848}
			cy={2321.094}
			r={102.9993}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.25} stopColor="#000" />
			<stop offset={0.508} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000p)"
			d="M1173.3 787.6c.4 6.9 0 25.3-13.1 41.7 11.6 5.3 19.6 5.5 27.3 5.5 7.8 0 35.3-.7 47.1-35-18.3 5.5-17.1 5.5-25.2 5.5s-28.4-9.5-31.8-21.9c-1.7.8-3.5 2.9-4.3 4.2z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000q"
			cx={807.4894}
			cy={3258.8721}
			r={1700.2821}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#000" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000q)"
			d="M913.7 821.6c-.3 14 4.2 43.9-35 47.1-9.6 11.4-48.7 38.9-83.3-9.9-24.4 39.1-76.4 18.2-87.6 0-16 1.4-49.4 11.7-63.5-40.5-4.4 2.4 84-139.4 127.1-136.2 14.7-11.2 31.6-5.2 42.7 2.5.3-4.4 12.1-18.6 12.1-18.6s.3-2 1.1-3.3c2.1-43.9-.8-122.7-4.4-152.3 6.2 40.9 8.4 68.9 10.9 95.3 4.9 14.1 17.1 47.9 27.6 76.7 19.9 8.8 26.2 24.1 26.2 24.1s59.3 167.2 26.1 115.1z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full43000r"
			x1={897.6193}
			x2={871.8925}
			y1={2461.311}
			y2={2952.2014}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={0.49} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full43000r)"
			d="M848.5 679.9c-16.9-37.2-37.2-81.7-52.2-115.3-16.6-164.1-43.5-313.2-99.7-341.2 1.5-2.6 46.1-3.3 46.1-3.3s170.3-4.2 219.2-5.5c31.8 18.2 51.4 50.6 69 85.5-19.9 1.8-57 8.7-57 8.7l-1.1 2.2-49.3-1.1H795.3l-2.2 8.7s18.9 23.7 53.6 69c19.5 25.6 38.2 47.8 58.1 74.5 2.4 8.8 18.6 59.2 18.6 59.2l108.4 32.9 13.1-6.5 28.5 8.7-26.3 118.3s-26.6-9.3-49.3 26.3c-13.3-5.2-21.9-7.3-31.8 1.1-5.7-11.3-12-30.1-40.5-28.5-28.6 1.7-38.2 18.9-40.5 31.8-8-8.6-18.2-27.5-36.5-25.5z"
		/>
		<linearGradient
			id="ch-full43000s"
			x1={945.0148}
			x2={568.0812}
			y1={2494.196}
			y2={2976.6492}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" stopOpacity={0} />
			<stop offset={0.322} stopColor="#000" />
			<stop offset={0.555} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full43000s)"
			d="M848.5 679.9c-16.9-37.2-37.2-81.7-52.2-115.3-16.6-164.1-43.5-313.2-99.7-341.2 55.7-14.1 98.7 86.6 98.7 86.6l-2.2 8.7s18.9 23.7 53.6 69c19.5 25.6 38.2 47.8 58.1 74.5 2.4 8.8 18.6 59.2 18.6 59.2l108.4 32.9 13.1-6.5 28.5 8.7-26.3 118.3s-26.6-9.3-49.3 26.3c-13.3-5.2-21.9-7.3-31.8 1.1-5.7-11.3-12-30.1-40.5-28.5s-38.2 18.9-40.5 31.8c-8-8.7-18.2-27.6-36.5-25.6z"
			opacity={0.2}
		/>
		<path
			fill="#ccc"
			d="M835.8 680.2c4-.1 9.7-.7 12.1-2.2-14.8-33.1-45-100-52.6-116.1-5.3-58.8-13.4-116.6-20.8-157.8-13-71.5-38.2-140.6-63.5-170.9-12-14.2-26.1-11.5-30.7-7.7-14.5 12.1-8 41.9-8.7 50.4 6.3-4.1 14.6-10 25.2-14.2 11.3 11.3 34.4 36.1 58.1 121.6 13.8 50 17.9 83.7 27.3 173.1 5.1 2.8 48.5 122.5 53.6 123.8z"
		/>
		<linearGradient
			id="ch-full43000t"
			x1={666.5633}
			x2={724.9858}
			y1={2831.8501}
			y2={2831.8501}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full43000t)"
			d="M670.4 235.4s7.3-14.2 20.8-14.2 28.3 20.1 35 31.8c6.8 11.7-2.2 46.1-2.2 46.1s-16.7-30.9-28.5-38.4c-9.7 6-24.1 14.2-24.1 14.2l-1-39.5z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000u"
			cx={923.1247}
			cy={2373.1118}
			r={98.6492}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000u)"
			d="M862.1 760.2c8.8-4 21.1-15.5 23-29.6 6 8 27.1 33.3 52.6 21.9 7.9 46.9-63.2 65.9-75.6 7.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000v"
			cx={850.0775}
			cy={2363.5505}
			r={98.6492}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000v)"
			d="M862.1 760.2c8.8-4 21.1-15.5 23-29.6 6 8 27.1 33.3 52.6 21.9 7.9 46.9-63.2 65.9-75.6 7.7z"
			opacity={0.302}
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M986.9 74.7h2.4V35.3s-5.7-3.3-11.5-3.3c-6.2 0-12.6 3.3-12.6 3.3v39.4h2.2m4.3 2c-.1 21.7-1.1 143.3-1.1 143.3M600.3 329.5s-82.3 75.3-96.4 175.3c-4.2-3.1-8.7-6.5-8.7-6.5s-8.9 62.2-2.7 120.9m40.9 9.6s2.5 1.1 7.4 2.8m61.8 19.4c1.6.4 3.2.8 4.8 1.4m23 5.6c39.4 9.4 88.7 18.1 143.4 21.7m35.7 1.6c1.1 0 2.2 0 3.3.1 7.4-8.2 14.2-17.6 14.2-17.6m219.9 11.8c66.2-5.5 126.3-15.8 147.3-34.8m-18.3-159.9c-1.9-5.9-4-12.4-6.3-19.2m-119.3-164.1c-8.1-6-16.7-11.9-26-17.4-11-16.8-16.3-23.6-17.6-25.2.2-20.9 0-102.8 0-126m4.5-1.7V98.8s-3.5-2.2-7.2-2.2c-4 0-8.2 2.2-8.2 2.2v28.5m3.4 1.7c-.1 23.3-1.1 116.2-1.1 116.2m-15.4-16.4c-.2-11.3 0-128.8 0-151.8"
		/>
		<radialGradient
			id="ch-full43000w"
			cx={870.3737}
			cy={2317.4114}
			r={108.6621}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000w)"
			d="M911.4 796.3c2.6 12.1 3.5 46-2.2 57s-27.1 13.5-31.8 15.4c17.1-17.8 11-50.3 10.9-59.2 5-6.2 7.9-9.6 8.7-13.1 5.3-.3 8.9-.1 14.4-.1z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full43000x"
			cx={1013.9734}
			cy={2337.699}
			r={111.5603}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000x)"
			d="M995.8 792c14 4.2 39.4 4.6 52.6-16.4 2.1 16 .6 22.2 16.4 35-1.9 6.3-29.9 50.3-77.8 15.4 3.1-8 5-17.2 1.1-26.3 2-1.6 4.8-3.4 7.7-7.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000y"
			cx={1089.2638}
			cy={2313.8149}
			r={111.5603}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000y)"
			d="M995.8 792c14 4.2 39.4 4.6 52.6-16.4 2.1 16 .6 22.2 16.4 35-1.9 6.3-29.9 50.3-77.8 15.4 3.1-8 5-17.2 1.1-26.3 2-1.6 4.8-3.4 7.7-7.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000z"
			cx={968.1662}
			cy={2322.6455}
			r={111.5603}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000z)"
			d="M995.8 792c14 4.2 39.4 4.6 52.6-16.4 2.1 16 .6 22.2 16.4 35-1.9 6.3-29.9 50.3-77.8 15.4 3.1-8 5-17.2 1.1-26.3 2-1.6 4.8-3.4 7.7-7.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000A"
			cx={1084.0087}
			cy={2301.7312}
			r={79.4651}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000A)"
			d="M1085.7 817.2c2.8 6.1 13.6 17.1 31.8 23-8.1 7.2-44.1 32.3-82.1-1.1 8.9-5.3 24.4-14.1 29.6-28.5 5.8 2.6 10.9 4.5 20.7 6.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000B"
			cx={1031.6006}
			cy={2286.7534}
			r={90.8181}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000B)"
			d="M1085.7 817.2c2.8 6.1 13.6 17.1 31.8 23-8.1 7.2-44.1 32.3-82.1-1.1 8.9-5.3 24.4-14.1 29.6-28.5 5.8 2.6 10.9 4.5 20.7 6.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000C"
			cx={971.5805}
			cy={2363.8804}
			r={99.7121}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000C)"
			d="M971.7 765.7c2.5 7.2 11.6 20.4 23 27.3-8.3 7.3-20.9 14.2-32.9 14.2s-26.3-9.7-28.5-27.3c3.5-13.7 4.4-16.2 5.5-27.3 1.5-.8 4.4-2.2 4.4-2.2s12.9 13.2 28.5 15.3z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000D"
			cx={1008.4554}
			cy={2333.6074}
			r={99.7121}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000D)"
			d="M971.7 765.7c2.5 7.2 11.6 20.4 23 27.3-8.3 7.3-20.9 14.2-32.9 14.2s-26.3-9.7-28.5-27.3c3.5-13.7 4.4-16.2 5.5-27.3 1.5-.8 4.4-2.2 4.4-2.2s12.9 13.2 28.5 15.3z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000E"
			cx={903.0344}
			cy={2335.3157}
			r={103.5472}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000E)"
			d="M862.1 760.2c.1 7.7 4.3 29.6 34 36.2-1.9 10.6-10.5 23-34 23s-34.7-15.5-38.4-19.8c2.3-10.9 4.6-17.4 2.2-30.7 2.7-4.3 4.2-7.7 5.5-8.7 2.5 1.1 17.6 3.2 30.7 0z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000F"
			cx={847.83}
			cy={2364.9519}
			r={103.5472}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000F)"
			d="M862.1 760.2c.1 7.7 4.3 29.6 34 36.2-1.9 10.6-10.5 23-34 23s-34.7-15.5-38.4-19.8c2.3-10.9 4.6-17.4 2.2-30.7 2.7-4.3 4.2-7.7 5.5-8.7 2.5 1.1 17.6 3.2 30.7 0z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000G"
			cx={949.6602}
			cy={2354.3135}
			r={142.1117}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000G)"
			d="M1008.9 714.2c14 .1 43.8 1.5 44.9 41.7-5.4 18.6-5.7 20.6-6.5 23s-19.5 23.5-51.5 14.2c-13.4-7.7-20.6-16.1-21.9-26.3 8.1-.1 48.1-8.1 35-52.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000H"
			cx={795.1809}
			cy={2363.7981}
			r={141.8443}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000H)"
			d="M824.9 771.2c2.9 7.6 5.2 34.2-21.9 50.4s-70.4 9-81.1-46.1c7.9-3.8 20.3-7.8 27.3-27.3.3 10.8 27.5 49.6 75.7 23z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000I"
			cx={700.5652}
			cy={2362.9138}
			r={141.8443}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000I)"
			d="M824.9 771.2c2.9 7.6 5.2 34.2-21.9 50.4s-70.4 9-81.1-46.1c7.9-3.8 20.3-7.8 27.3-27.3.3 10.8 27.5 49.6 75.7 23z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000J"
			cx={704.4287}
			cy={2351.3286}
			r={103.1834}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000J)"
			d="M721.9 776.7c-7.6 2.1-25.8 6-37.2-2.2-5.5 9.9-8.9 33.4 15.4 52.6 21 14.5 46.3.5 48.2-4.4-6.7-6.9-25.9-29.2-26.4-46z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000K"
			cx={761.8004}
			cy={2309.9165}
			r={103.1834}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000K)"
			d="M721.9 776.7c-7.6 2.1-25.8 6-37.2-2.2-5.5 9.9-8.9 33.4 15.4 52.6 21 14.5 46.3.5 48.2-4.4-6.7-6.9-25.9-29.2-26.4-46z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000L"
			cx={863.6603}
			cy={2330.0559}
			r={151.4857}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000L)"
			d="M888.4 811.7c-9.9 5.8-42 17.2-64.7-12.1-8.8 11.5-8.4 15.5-30.7 27.3-.6 15.6-1.7 59.2 51.5 59.2 51.9-5.3 47.7-67.7 43.9-74.4z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000M"
			cx={775.9058}
			cy={2319.1216}
			r={151.4857}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000M)"
			d="M888.4 811.7c-9.9 5.8-42 17.2-64.7-12.1-8.8 11.5-8.4 15.5-30.7 27.3-.6 15.6-1.7 59.2 51.5 59.2 51.9-5.3 47.7-67.7 43.9-74.4z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000N"
			cx={964.7661}
			cy={2315.1968}
			r={87.0793}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000N)"
			d="M988.1 799.7c3.6 8.9 8.8 42.3-30.7 49.3s-47.6-44.9-47.1-52.6c6-1.8 20.1-10.5 23-16.4 2.8 10.8 7.3 39.6 54.8 19.7z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000O"
			cx={904.0917}
			cy={2323.2349}
			r={87.0793}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000O)"
			d="M988.1 799.7c3.6 8.9 8.8 42.3-30.7 49.3s-47.6-44.9-47.1-52.6c6-1.8 20.1-10.5 23-16.4 2.8 10.8 7.3 39.6 54.8 19.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000P"
			cx={776.2598}
			cy={2298.958}
			r={102.251}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000P)"
			d="M792.1 826c.5 8.2.9 25.5 4.4 31.8-7.8 11-24.1 25.9-44.9 23-20.8-2.8-45.7-3.6-52.6-54.8 18.2 7.9 27.9 13.5 50.4-3.3 12 7.4 30.6 10.2 42.7 3.3z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000Q"
			cx={725.3912}
			cy={2294.988}
			r={102.251}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000Q)"
			d="M792.1 826c.5 8.2.9 25.5 4.4 31.8-7.8 11-24.1 25.9-44.9 23-20.8-2.8-45.7-3.6-52.6-54.8 18.2 7.9 27.9 13.5 50.4-3.3 12 7.4 30.6 10.2 42.7 3.3z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000R"
			cx={790.2437}
			cy={2385.9102}
			r={95.3161}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000R)"
			d="M822.7 707.7c7.6 7 13.1 16.2 13.1 30.7s-6.8 35.2-25.2 40.5-57.3.1-61.3-36.2c.7-18.7 1.6-32.5 10.9-38.4-.6 13.7-1.5 35 31.8 35 29.8-2.3 28.6-29.1 30.7-31.6z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000S"
			cx={1155.1351}
			cy={2332.7285}
			r={85.4676}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000S)"
			d="M1133.9 783.2c-2.6 11.2-10.1 36-47.1 34 7.7 16 26.8 23 42.7 23s44.2-11.9 43.9-52.6c-16 4.4-29.4 5.4-39.5-4.4z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000T"
			cx={1099.8585}
			cy={2305.0459}
			r={85.4676}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000T)"
			d="M1133.9 783.2c-2.6 11.2-10.1 36-47.1 34 7.7 16 26.8 23 42.7 23s44.2-11.9 43.9-52.6c-16 4.4-29.4 5.4-39.5-4.4z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000U"
			cx={530.6131}
			cy={2379.02}
			r={105.4647}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000U)"
			d="M526.9 744.9c-2.6 5.6-10.9 29.9-1.1 55.9-15.8-5.3-34.3-17.4-36.2-44.9 7.4-2.8 17.4-10.1 23-15.4 7.5 2.5 8.2 2.3 14.3 4.4z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000V"
			cx={511.8824}
			cy={2461.6902}
			r={145.9117}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" stopOpacity={0} />
			<stop offset={0.539} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full43000V)"
			d="M534.5 638.6c-8.9-9-20.3-21-47.1-19.8-26.8 1.4-37.7 30.3-35 47.1s11.5 36.5 35 36.2c7.3-15.5 12-31.1 43.9-29.6-2.5-16.3.4-22.7 3.2-33.9z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000W"
			cx={567.0781}
			cy={2435.2239}
			r={113.1755}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000W)"
			d="M636.4 674.7c5.4 2.5 16.1 12.3 15.4 31.8s-14.2 33.6-34 32.9c-19.8-.6-36-8.9-36.2-42.7 8.5-4.9 12.1-7.4 14.2-10.9.1 7.9 33.4 22.2 40.6-11.1z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full43000X"
			cx={607.9589}
			cy={2422.4038}
			r={80.8391}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000X)"
			d="M636.4 674.7c5.4 2.5 16.1 12.3 15.4 31.8s-14.2 33.6-34 32.9c-19.8-.6-36-8.9-36.2-42.7 8.5-4.9 12.1-7.4 14.2-10.9.1 7.9 33.4 22.2 40.6-11.1z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000Y"
			cx={707.7259}
			cy={2351.9587}
			r={156.1459}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000Y)"
			d="M700 826c-10.3-8.5-29-31-15.4-50.4-2.1-1.6-5.5-4.4-5.5-4.4s-2.1 10.2-18.6 16.4c-.8 6.8.5 18.1-15.4 30.7 2.1 13.5 9.4 52.7 62.5 39.4-5.7-10.5-7-18.7-7.6-31.7z"
			opacity={0.6}
		/>
		<radialGradient
			id="ch-full43000Z"
			cx={912.0524}
			cy={2364.094}
			r={122.808}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000Z)"
			d="M965.2 702.2c9.6-6 32.5-12.4 44.9 14.2 12.4 26.6-22.6 75.9-67.9 35 15.9-11 28.7-25 23-49.2z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000aa"
			cx={568.3195}
			cy={2418.3713}
			r={89.1623}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000aa)"
			d="M530.2 672.5c-17.4.2-50.4 8.8-40.5 43.9 9.9 34.9 56.5 33.9 67.9 12.1 11.5-21.8 7.7-24.8 7.7-27.3-8.4 1-27.4-.2-35.1-28.7z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000ab"
			cx={525.2833}
			cy={2387.5427}
			r={113.6827}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ab)"
			d="M538.9 741.6c.2 14.7 4.4 44.1 46.1 43.9 3.4 11.3 8.7 29.6 27.3 34-4.3 5.9-15.1 13.1-35 13.1s-43-8.2-52.6-35 .6-52.7 3.3-54.8c4.1-.2 5.7-.3 10.9-1.2z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000ac"
			cx={578.9963}
			cy={2351.4907}
			r={113.6827}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ac)"
			d="M538.9 741.6c.2 14.7 4.4 44.1 46.1 43.9 3.4 11.3 8.7 29.6 27.3 34-4.3 5.9-15.1 13.1-35 13.1s-43-8.2-52.6-35 .6-52.7 3.3-54.8c4.1-.2 5.7-.3 10.9-1.2z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000ad"
			cx={631.3212}
			cy={2310.2112}
			r={113.6827}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ad)"
			d="M538.9 741.6c.2 14.7 4.4 44.1 46.1 43.9 3.4 11.3 8.7 29.6 27.3 34-4.3 5.9-15.1 13.1-35 13.1s-43-8.2-52.6-35 .6-52.7 3.3-54.8c4.1-.2 5.7-.3 10.9-1.2z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000ae"
			cx={655.2279}
			cy={2379.718}
			r={95.6415}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ae)"
			d="M666.1 747.1c.8 6.7 4.8 19.4 13.1 25.2-10.4 19.4-46.3 26.7-72.4 0 4.7-5 12.1-17.9 12.1-32.9 6.2.1 12.3-2.4 15.4-4.4 3.1 5.9 16.2 13.5 31.8 12.1z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000af"
			cx={643.8245}
			cy={2341.5}
			r={100.8078}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000af)"
			d="M607.9 772.2c10 9.4 22 21.1 52.6 15.4-.7 10.6-7.2 35-34 35s-43.2-25.6-41.7-38.4c5.7-1.3 14.7-2.9 23.1-12z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000ag"
			cx={576.9331}
			cy={2343.8447}
			r={100.8078}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.539} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ag)"
			d="M607.9 772.2c10 9.4 22 21.1 52.6 15.4-.7 10.6-7.2 35-34 35s-43.2-25.6-41.7-38.4c5.7-1.3 14.7-2.9 23.1-12z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000ah"
			cx={617.4163}
			cy={2374.8552}
			r={169.2169}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ah)"
			d="M690.1 697.8c16.9-4.6 45.4-10.4 58.1 27.3 5.9 40.9-23.8 51.5-34 52.6s-40.3 8.5-48.2-30.7c13.5-4.2 33.1-10.6 24.1-49.2z"
			opacity={0.451}
		/>
		<radialGradient
			id="ch-full43000ai"
			cx={580.8591}
			cy={2364.8623}
			r={253.1154}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.45} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ai)"
			d="M552 827c-12.9-4.8-20-12.8-26.3-26.3-10.5-4.6-28.6-9.7-36.2-41.7-4 11.5-12.2 27.1-39.4 31.8.8 14.3 9.3 46.7 60.3 52.6 19.4-.7 26.3-4.8 41.6-16.4z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000aj"
			cx={568.2}
			cy={2449.3359}
			r={155.4259}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.67} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000aj)"
			d="M540.1 741.6c12-7 22.7-16.6 25.2-39.4 5.9-1.1 11.6-2.7 16.4-5.5.3 9.5 2.1 45.1 38.4 42.7.3 14.2-10.1 47.6-42.7 46.1-32.8-1.6-41.4-31.5-37.3-43.9z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000ak"
			cx={628.8063}
			cy={2395.6118}
			r={111.0191}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ak)"
			d="M540.1 741.6c12-7 22.7-16.6 25.2-39.4 5.9-1.1 11.6-2.7 16.4-5.5.3 9.5 2.1 45.1 38.4 42.7.3 14.2-10.1 47.6-42.7 46.1-32.8-1.6-41.4-31.5-37.3-43.9z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000al"
			cx={518.3583}
			cy={2385.8313}
			r={111.0191}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000al)"
			d="M540.1 741.6c12-7 22.7-16.6 25.2-39.4 5.9-1.1 11.6-2.7 16.4-5.5.3 9.5 2.1 45.1 38.4 42.7.3 14.2-10.1 47.6-42.7 46.1-32.8-1.6-41.4-31.5-37.3-43.9z"
			opacity={0.302}
		/>
		<radialGradient
			id="ch-full43000am"
			cx={616.9373}
			cy={2375.3855}
			r={80.5805}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000am)"
			d="M649.5 690.1c8.3-6.2 24.4-16.2 40.5 9.9 5 12.1 5.3 25.2-3.3 35-11.6 13.5-34.5 19.1-51.5 0 8.7-5.4 21.7-15.2 14.3-44.9z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full43000an"
			cx={475.9095}
			cy={2379.3621}
			r={126.1271}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.501} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000an)"
			d="M427.2 704.3c-5.2-4.2-10.6-11-13.1-15.4-6.9 3.3-26.3 12.5-26.3 53.6s40.1 47.1 48.2 47.1 43.5 3.6 54.8-32.9c-16.8 3.8-53.5 12.1-63.6-52.4z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000ao"
			cx={531.6178}
			cy={2391.7844}
			r={115.368}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ao)"
			d="M511.5 742.7c-9.3 7.9-18.7 16.4-42.7 16.4-22.1 5.2-71.9-61.7-17.6-92 .6 10.7 10 36.8 37.2 35-.6 10.9-2.6 21.1 23.1 40.6z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full43000ap"
			cx={496.7306}
			cy={2428.0491}
			r={115.368}
			gradientTransform="matrix(1 0 0 -1 0 3092)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.604} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000ap)"
			d="M511.5 742.7c-9.3 7.9-18.7 16.4-42.7 16.4-22.1 5.2-71.9-61.7-17.6-92 .6 10.7 10 36.8 37.2 35-.6 10.9-2.6 21.1 23.1 40.6z"
			opacity={0.302}
		/>
		<path
			d="M977.2 307.7s-14.2-47.9-52.6-71.2c-42.7 2.2-132.6 4.4-132.6 4.4s18.5 30.9 32.9 69c12.7.1 78.3-.8 89.8 0 59.1 1.6 62.5-2.2 62.5-2.2zm-387.9-126-14.2 20.6s47.2 20.3 96.4 0c-8-10-17.6-22.8-17.6-22.8s-9.9 7.3-31.8 7.6c-16.2.2-32.8-5.4-32.8-5.4z"
			className="factionColorSecondary"
		/>
		<radialGradient
			id="ch-full43000aq"
			cx={1020.6563}
			cy={-1148.4589}
			r={139.6359}
			gradientTransform="matrix(1 0 0 1 0 1870)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.398} stopColor="#000" />
			<stop offset={0.5417} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full43000aq)"
			d="M1071.4 689.5s16-18.7 36.1-9.9c20.2 8.9 21.9 75.6 21.9 75.6s-14.1-35.1-55.8-20.8c10-16.8-2.2-44.9-2.2-44.9z"
			opacity={0.302}
		/>
		<g>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M670.5 239c3.1-6.8 11.7-22.2 25.1-16.7 77.8 31.9 100.8 341.8 100.8 341.8L849 680.2m-12 2.2-55.9-125s-16.9-243.9-87.6-298.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m781 558.6 15.4 6.5 65.7-5.5 28.5 50.4 109.6-6.5s11.6-39.9 15.4-51.5m59.1 4.4-27.3 120.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m857.8 661.6-35-77.8 32.9-3.3s19.9 36.5 32.1 59.1c-.6 1.5-8 18.6-9.3 19.8-10.2 1-20.7 2.2-20.7 2.2zm167.7-91s-9.6 33.9-16 56.9c.9 1.5 11 17.7 12 19.2 9-.8 18.3-1.7 18.3-1.7l16.4-76.7-30.7 2.3z"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m686.9 221.1 275-7.7s40.3 18.1 69 85.5" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1251.2 340.6 132.6 49.3-28.5 49.3-20.8 10.9-180.8 13.1 116.1 99.7 8.7 25.2-27.3 48.2-145.8-30.7-28.5-48.2-29.6-8.7-12.1 7.7-111.8-35L907 471l-1.1-7.7-112.8-145.8 2.2-7.7h125l53.6 1.1m197.2 37.4-1.1-19.8-117.3-30.7-77.8 10.9v13.1l72.4 20.8 121.6 3.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m908.2 469.8 86.6 26.3 40.5 59.2m11-7.6-41.7-57-98.7-29.6m88.8 36.1 8.7-5.5"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m919.1 309.9 62.5 19.8-76.7 14.2 34 73.4s-.2.9-.5.8c-6.5-2.4-73.9-26.8-93.8-34"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m937.7 418.3 10.9-6.5-21.9-52.6 30.7-6.5m28.6-5.6v-16.4m60.3-5.5v16.4"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m903.8 342.7-106.3-32.9" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m980.5 307.7 67.9 16.4 113.9 2.2" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1270.8 566.3-19.8 35-20.8-32.9-189.5-98.7 14.2 53.6 50.4 79.9m145.9 32.1-28.5-43.9-165.4-69"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1222.7 590.4 8.7-19.8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1006.8 442.4 135.9 12.1 24.1 4.4-17.6-32.9 52.6-5.5-15.4-17.6-166.5-59.2-67.9 11 52.6 86.6 35 29.6m142.5-69-69 7.7 28.5 43.9m8.7 4.3L1045.1 472m-91-116.2c-.1.1 159.7 52.2 161 52.6"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m929 361.4 34 10.9" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="m1333.3 450.1 18.6-39.4-50.4 3.3-200.6-47 26.3-10.9 127.1-16.4"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1382.6 389.8-67.9 3.3-180.8-37.1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1313.6 395.4-12.1 17.6" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M670.4 275.9v-71.2l-17.6-28.5s-39.2-11.3-64.7 3.3c-5.8 9.6-14.2 23-14.2 23V356"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M575.1 245.5c-19-5.7-30.7-9-30.7-9v93.2l30.7 24.1" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m543.3 235.4 31.8-1.1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M670.4 202.5c-8.6 4.6-65 16.7-96.4-1.1m18.6 8.8v23s8.2 1.2 13.1 1.1c-.1-8.7 0-23 0-23m-16.4-30.7c7.2 6.5 62.5 9 62.5-2.2"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1195.3 623.7c0 6.2-.3 14.7-1.1 21.1" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M824.4 308.9c-16.1-44.2-34.5-68.1-34.5-68.1s91.1-2.6 134.7-4.4c20 7.5 38.2 36.9 52.9 70.8"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1234.7 799.7c-19.2 49.4-67.9 32.7-75.6 28.5-2.7 2.5-6.8 5.4-15.4 8.7-26.3 9.7-53.4-6.4-59.2-20.8-28.3-2.6-38.8-26.2-37.2-41.7-15.3 27.3-46.7 19.5-51.5 17.6-2.9 2.6-5.5 5.3-8.7 6.5 8.7 17.7 0 37.6-15.4 46.1-19.1 10.5-48.2 4.3-60.3-30.7.2 3.3 3.6 21.2-1.1 34-7.3 16-16.4 16.1-34 20.8-29 36-74.6 8.9-81.1-12.1-22.5 39.4-75.9 23.6-87.6 1.1-53 13.1-61-28.6-63.5-40.5-6.2 4.9-20.6 6.2-30.7 2.2-19.5 16.9-47.8 12.9-62.5 6.5-37.4 38.4-102.6 3.4-101.9-36.2-40.1 2.4-57.5-17.5-61.3-47.1-68.3-28.5-26.3-116.3 17.6-108.4-1.6-66.6 62.1-76.5 82.1-72.7M460 634.2c-36-10.5-72.9 39.5-31.8 71.2m-15.3-16.5c-10.3 3.6-28.9 22.5-25.2 55.9m42.8-104c-5.3-2.5-12.4-5-25.2-6.5m46 32.8c-54.9 18.6-9.4 136.8 60.3 74.5m-62.5 48.2c28.7-2.4 37.3-21 41.7-32.9M551 826c-37.1-13.1-32.9-66.5-24.1-81.1m13.2-2.2c-12.9 44 75.3 66.6 78.9-2.2m60.2 31.7c-24.1 32.1-63.4 10.9-70.2 1.1m-25.2 12.1c2.3 10.8 9 29.3 29.6 34m30.7-2.2c8.4-5.4 16.5-17.2 15.4-28.5m24-12c-18.2 24.2 25.5 79.5 64.7 46.1m-27.3-46.1c24.1 94 119 44 104.1-5.5m-1.2 28.5c5.7 14 56.6 36.4 72.4-2.2m14.2 0c-.1 4.2.7 14.7 2.2 18.6m-25.2-5.5c2 11.3 5.7 38.4-12.1 59.2m-13.2-108.5c-2.3 36 75.3 58.6 75.6-7.7M792.1 827c.3 14.6.6 23 4.4 30.7m-37.4-154.5c-4.1 19.5 4.3 35.2 31.8 35 26.6-.1 33.8-25.9 27.3-46.1m-70 53.8c5.3 34.3 55.4 43.9 75.8 24 10.8-10.6 20.9-46.5-3.5-63.4m9.8 53.7c13.4 3.4 48.9 5 52.6-28.5.8-.1 1.1 0 1.1 0s2.2-13.7 0-24.1M698.9 827c.2 8 2.5 21.9 8.7 30.7M690.1 700c8.6 29.8-7.2 43.6-24.1 46.1-13.7 2-28.2-4.4-30.7-9.9m30.8 10.9c2.8 49.5 90 39.7 82.1-16.4m-222.5 70c-13.3-6.2-32.3-12.7-36.2-42.7m-1-58c-5.9 11.5 4.7 31 20.3 39.5 18.1 9.9 52 3.5 56.5-37.3m84.2-11.1c16.4 58.5-72.9 67.7-67.9 6.5m14.3-9.7c3.2 7.9 38.1 15.5 39.4-13.1m-100.8-32.9c-6.1-11.5-18.8-22.8-38.4-23-11.5-.1-27.1 4.4-36.6 15.5-6.5 7.8-10.2 19.8-7.3 38.3 5.9 27.7 30.4 30.6 35 29.6 2.9-16 16.7-33.7 42.7-28.5-4.3-18.4 4.1-64.1 48.2-49.3 20.3 6.8 24 24 25.2 34 .4-1.2-.1-3.5 1.1-4.4 7.8-5.7 29.8-2.2 30.7 18.6 3.7 4.1 9.7 6.5 14.2 17.6 3.3-7.6 25.3-14.9 39.4 7.7 17.7-9.7 54.3-6.7 59.1 32.2 0 .2.5.8 1.2.6 1.1-15.8 4.9-23.3 9.9-27.3 3.3-10.6 10.2-34.5 40.5-26.3 7.8 2 17.2 9.9 18.6 15.4 6.4-6.7 45.4-31.9 65.7 16.4 1.5-13.6 10.8-41 52.6-35 21.8 1.7 27.2 25.6 28.5 28.5 2.8-5 18.4-10.7 32.1-1.1 6.5-10.8 28.8-42.6 67.6-16.4 23.1 18.1 10.2 43.2 7.7 49.3 16.6-5.5 34.4-7.9 50.4 9.9 5.9 8.2 6.9 10.6 7.7 14.2.3.1.7 1.2 1.1 2.2m-78.6-1.5c.6-11.4-.8-48.7-45.2-45.3M998 701c8.9 5.4 16.5 22.7 12.1 37.2-6.9 22.4-30.5 41.4-66.8 14.2m28.4 14.4c1.7 6 11.2 20.5 25.2 26.3m-9.8 5.4c-10.5 8.9-48.3 16.6-54.8-18.6m57 47.1c26.1 18.8 57.8 15.8 74.5-15.4m-28.6 27.5c10.9 13.5 51.9 27.1 82.1 1.1m-152.1-137c10.2 34-40.3 81.4-79.9 27.3m188.3 2.3c-8.2 7.6-20.5 9.5-26.3 44.9m86.6 6.6c28.5 15.5 50.7 2 61.3-27.3 4.5-12.5.3-36.9-29.6-44.9-34.8-9.4-51.9 5.8-59.2 18.6m25.3 29.5c1.9 11 9.4 57.8-46.1 55.9m87.7-28.5c1.5 24.1-12 37.8-15.4 41.7"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1071.4 690.1c11.3-17.7 55.7-20.9 65.7 19.8" />
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M600.3 330.7c12.8-12.8 53.2-47.1 96-69.8" />
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1120.3 683.3c4.4-12.6 16.4-21.7 30.6-21.7 17.9 0 32.3 14.5 32.3 32.3 0 8.2-3.1 15.7-8 21.4"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1180.6 682.2c5.5-4 12.3-6.3 19.6-6.3 18.4 0 33.4 15 33.4 33.4s-15 33.4-33.4 33.4c-.8 0-1.8 0-2.6-.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1229.5 723.7c15.9 6.2 27 21.7 27 39.8 0 23.6-19.1 42.7-42.7 42.7-15.8 0-29.6-8.5-36.9-21.2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1177.8 678.8c.4-20.5 17.2-36.9 37.8-36.9 20.9 0 37.8 16.9 37.8 37.8 0 14.6-8.2 27.1-20.3 33.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1250.2 696.7c18.2 1.2 32.6 16.4 32.6 34.9 0 16.6-11.6 30.6-27 34.2"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1238.4 650.3c6.2-2.6 13.1-4.1 20.4-4.1 29 0 52.6 23.6 52.6 52.6 0 21.9-13.5 40.8-32.5 48.7"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1209.8 643c1.7-5.5 4.8-10.3 9-14m38.4-2.5c7.4 4.9 12.4 12.9 13.5 22.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1279.8 747.2c7.9 5.6 13 14.7 13 25.1 0 16.9-13.7 30.7-30.7 30.7-8.1 0-15.6-3.2-21-8.3"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1262.4 631.5c7.8-8.4 18.9-13.7 31.4-13.7 23.6 0 42.7 19.1 42.7 42.7 0 6.7-1.6 13-4.3 18.6"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1333.6 743.2c.5 2.4.7 5 .7 7.7 0 20.8-16.8 37.8-37.8 37.8-3.2 0-6.2-.4-9.3-1.1"
			/>
			<path
				fill="none"
				stroke="#1a1a1a"
				strokeWidth={5}
				d="M1304.7 674.7c2.8-.6 5.8-1.1 8.8-1.1 20.6 0 37.2 16.6 37.2 37.2s-16.6 37.2-37.2 37.2c-8.5 0-16.4-2.8-22.8-7.8"
			/>
			<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M530.2 673.7c7.9 45.8 77.5 31.2 73.4-14.2" />
		</g>
	</svg>
);

export default Component;

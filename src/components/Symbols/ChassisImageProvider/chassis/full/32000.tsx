import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M1145 1553c24-4.8 115.6-24.6 137-31s18-30.3 19-45 1.5-43.6-11-63c4.6-8.8 9.2-27.9-10-65s-39.3-49-50-62c10.2-15.1 24.5-52.9 8-127s-44.6-134.2-54-152c0-7 2-24 2-24s12 3.9 13-12 2.9-60.6 3-74-6.6-32.2-33-53-63.7-48.2-74-54-16.7-6.4-20-7c-7.6-10-29.9-34.6-40-49s-20.1-30.4-25-38c5.9-10.4 23-38 23-38l-102-81 4-8-2-3s5.3-26.6-7-49-29.7-34.6-83-42-135.1 3.3-143 31c-7.9 27.7-2.3 58 21 75s42 26 42 26-44.1 7-53 41c-21.2 19.5-43 40.7-50 85-19.8 20.4-50.8 52.6-63 63s-52.9 32.6-89 43-50.8 22.5-49 52 3.4 57.2 5 74 7.4 32.2 15 49c7.6 16.8 25.3 55.6 39 64 8.9 19.9 17.6 40.5 23 48s6.4 17.3 37 13 42.4-6.2 56-9 18.7-9.7 19-28-3.6-84.9-6-103-5.5-41.2-25-78c23.1-8.3 41.2-16.7 49-23s21.4-19.6 25-26c11.5 20.8 152.7 266.9 310 377-13.7 30.7-29 71.3-29 96s-1.2 71.7 40 114c16 16.9 24.3 12.3 28 11 15 25.9 41.2 59.9 50 69s13.7 16 32 16c2.4 0 7.1-1 18-3z"
		/>
		<linearGradient
			id="ch-full32000a"
			x1={1070.3206}
			x2={520.6345}
			y1={755.246}
			y2={1216.4871}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.206} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#0d0d0d" />
		</linearGradient>
		<path
			fill="url(#ch-full32000a)"
			d="M696 873c203.2 344.6 391.5 457.3 483 448s63.4-137.7 60-156c-3.8 36.7-10.2 94-75 94-140.3 0-304.9-192.6-441-418-15.1 11.7-21.9 20.7-27 32z"
		/>
		<path
			fill="#e6e6e6"
			d="M948 689c-9.6-7.9-54-43-54-43l-5 7s-151.5 19.5-160-30c-16.3 14.2-18 23.2-20 32s18.6 55 76 55 172.6-13.1 163-21z"
		/>
		<radialGradient
			id="ch-full32000b"
			cx={842.011}
			cy={1403.5441}
			r={235.45}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.491} stopColor="#000" />
			<stop offset={0.651} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full32000b)"
			d="M765 609c15.5 8.8 70.8 28.7 95 18 4.1 5.4 11.9 16.9 25 24-18.4 3.2-151.1 18.5-157-28 19.2-9.9 24.8-15 37-14z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full32000c"
			x1={772.662}
			x2={839.4051}
			y1={1294.1516}
			y2={1425.1406}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#dedede" />
		</linearGradient>
		<path
			fill="url(#ch-full32000c)"
			d="M820 561c-19.3-3.9-84.5-35.4-114-61-10.6 18.1-12.6 55.9 9 77 24.5 19.6 69.8 47.5 103 52s35.2.4 39-2c-6.2-12.9-27.2-46.6-37-66zm94-7c4.7-7.9 14.2-28.8 6-45 12.3 24 15.9 34.7 13 59-11.9-6.9-12-9.6-19-14z"
		/>
		<path
			fill="#d9d9d9"
			d="M513 1074c5.9 2.7 15.2 3.8 15 1 .2-3.8.3-7.8.4-12 .1-5.4 24.6 41.8 24.6 36-.1-22.4-1.7-64.3-3-86-2.3-12.6-6-41.8-12.3-53.5C552 949.4 585.5 934.4 667 902c18.8-7.5 33-39.3 43-52-1.2-.3 32.8-24 94-46 2.2-4.5-5.4-7.8-16-14-20 10.2-66.9 31.5-70 34-29.5 21.7-28.8 41.7-58 63-81.5 40.2-133.5 50.5-147 70-15.4-27.3-51-86-51-86s-5.9 37.9 1 100c16.8 52.2 40.4 96.3 50 103zm486.8-326.7c7.2 12 11.9 26.5 25.2 33.7 13.9 7.5 50.1 15.4 66 50-4.4-16.8-8.5-37.5-15-45-11.9-3.4-48-15.5-63-61-6.7 13.7-8.3 15.6-13.2 22.3z"
		/>
		<path
			fill="#e6e6e6"
			d="M591 802c56.8-38.6 87.5-121 144-105 26.9 23.6 52.2 60.4 57 78 7.6 21.8-58.3 36.7-80 55s-32.5 43.7-54 59c-21.4-24.4-49.5-68.1-67-87zm414-98c3.1 10.3 26 75.6 69 82-13.6-20.8-52.8-69.6-64-89-.7 2.4-4.6 1.9-5 7z"
		/>
		<path
			fill="#d9d9d9"
			d="M1006 1250c-7.6 14.2-37.5 73-27 140 8.8 62.2 59.6 88.9 68 80 4.3-4.6 8.3-13.1 12.6-23.6 2.1-5.2 60 78.6 62.4 72.6 3.2-8.1-4.6-57.2-44.8-114.9 16.5-35 41.6-70.1 89.8-70.1 76.4-3.2 113.3 65.3 124 81 3.8-12.7 15-56.5-60-126-21.6 24.3-33.9 32-65 32s-110.6-32.2-160-71z"
		/>
		<radialGradient
			id="ch-full32000d"
			cx={1135.897}
			cy={756.413}
			r={605.893}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.198} stopColor="#000004" />
			<stop offset={0.349} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full32000d)"
			d="M1006 1250c-7.6 14.2-37.5 73-27 140 8.8 62.2 59.6 88.9 68 80 4.3-4.6 8.3-13.1 12.6-23.6 2.1-5.2 60 78.6 62.4 72.6 3.2-8.1-4.6-57.2-44.8-114.9 16.5-35 41.6-70.1 89.8-70.1 76.4-3.2 113.3 65.3 124 81 3.8-12.7 15-56.5-60-126-21.6 24.3-33.9 32-65 32s-110.6-32.2-160-71z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full32000e"
			x1={1120.8976}
			x2={1059.8976}
			y1={450.0942}
			y2={473.5102}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.701} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32000e)"
			d="M1061 1447c1.6-11.2 11.2-41.5 15-44 7.3 10.2 43.4 52.2 46 117-14.3-18.2-51.9-65.2-61-73z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32000f"
			x1={1226.6669}
			x2={1186.2809}
			y1={305.8735}
			y2={495.8735}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#d9d9d9" />
			<stop offset={0.701} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full32000f)"
			d="M1122 1524c22.3-3.9 161.3-33 178-37 12.5-45.4-31.7-153-132-153-55.3 0-81.2 39.1-94 71.5 13.5 13.5 52.5 68 48 118.5z"
		/>
		<linearGradient
			id="ch-full32000g"
			x1={1194.5562}
			x2={1167.8502}
			y1={465.7716}
			y2={603.1616}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.303} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32000g)"
			d="M1076 1405c5.3-16.6 34.3-66.7 87-71s100.8 25.9 137 102c-39.8 7.6-185 35-185 35s-26.3-54-39-66z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full32000h"
			x1={610.2541}
			x2={581.1111}
			y1={808.876}
			y2={992.876}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full32000h)"
			d="M555 1108c11.6-.9 92.2-13.2 98-14 .7-28.4-1.8-135.3-30-170-23.6 6.6-71.2 21.4-87 37 7.3 17.7 13 13.8 19 147z"
		/>
		<linearGradient
			id="ch-full32000i"
			x1={592.2618}
			x2={582.2719}
			y1={939.2446}
			y2={986.2446}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32000i)"
			d="M536 959c9.6-7.8 78.6-32.6 87-35 6.5 9.5 15 29 15 29l-97 18s-1.3-7.4-5-12z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full32000j"
			x1={1164.7163}
			x2={1142.5424}
			y1={915.7535}
			y2={1055.7534}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full32000j)"
			d="M1184 1003c2.5-25.7 10.2-95.4-10-126-27-6.9-57.5-11.8-67-14 15 28.1 39.9 70.4 77 140z"
		/>
		<linearGradient
			id="ch-full32000k"
			x1={1146.6073}
			x2={1149.2272}
			y1={996.1046}
			y2={1056.1046}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32000k)"
			d="M1109 862s52.7 11.2 62 14 15.2 30.5 16 46c-25.5-3.9-51-7-51-7l-27-53z"
			opacity={0.8}
		/>
		<linearGradient
			id="ch-full32000l"
			x1={1172.3013}
			x2={1147.0223}
			y1={912.8371}
			y2={1072.4421}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.497} stopColor="#d9d9d9" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full32000l)"
			d="M1185 1005c.4-9.4.8-15.3 1-20 7 0 13.6.7 15-37s8.1-65.8-16-88c-14.1-3.5-67.2-9.8-86-14 3.7 7.8 7 18.2 8 18s67 13 67 13 21.1 20.9 7 128c1.5.1 2.3 1.3 4 0z"
		/>
		<linearGradient
			id="ch-full32000m"
			x1={1034.3596}
			x2={962.3097}
			y1={1208.9659}
			y2={1253.9879}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#4d4d4d" />
		</linearGradient>
		<path fill="url(#ch-full32000m)" d="M958 668c1.5.5 72-6 72-6l-41 67s-32.5-61.5-31-61z" />
		<path
			fill="#b3b3b3"
			d="M853 580c.1 0 12.9 14.5 14.3 15.5C881.8 605.9 958 666 958 666l28 58-93-78s-3.2 8.3-4 7c-1.7-2.6-24.5-15.3-28-22-6.8-13.2-35-56.7-42-69 5-.6 33.6 18.1 34 18z"
		/>
		<path
			fill="#575656"
			d="M894.7 566.2c16.5-1.7 30.3-3.2 30.3-3.2l-3-2s-13.1 1.3-29.1 3c-5.5.5-3.8 2.8 1.8 2.2zm-31 3.2c-13.9 1.5-24.7 2.6-24.7 2.6l39 69-4-4-40-67s12.6-1.3 28.2-2.9c4.4-.4 5.8 1.9 1.5 2.3z"
		/>
		<linearGradient
			id="ch-full32000n"
			x1={690.777}
			x2={1068.7771}
			y1={840.6927}
			y2={1136.0187}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32000n)"
			d="M723 840c22.1 35.2 182.7 290.2 265 336 17.4-27.4 44.2-55.2 113-72-33-63.2-204.3-369.6-227-399-18.6 1.2-112.5 15.1-137-7 29.3 30.9 108.3 85.2 53 112-35.9 13.4-41.1 10.3-67 30z"
			opacity={0.6}
		/>
		<linearGradient
			id="ch-full32000o"
			x1={1258.0597}
			x2={905.0597}
			y1={1112.9309}
			y2={931.0459}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32000o)"
			d="M1228 1122c-10-33.5-63.5-173.2-153-315-23.2-10.8-64.7-24.2-83-82-14.8-12-42-35-42-35s-10.7 11.5-75 15c42.4 64.2 210.3 364.9 227 396 33.9-4.2 101.3-11.3 126 21z"
		/>
		<linearGradient
			id="ch-full32000p"
			x1={819.5296}
			x2={728.1496}
			y1={1047.2709}
			y2={1082.3489}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.198} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</linearGradient>
		<path
			fill="url(#ch-full32000p)"
			d="M810 783c4.5 12.9 14.9 70.2-16 166-16.9-24.1-66.8-97.7-70-106 12.1-9.8 30.2-21.4 60-30s23.9-24.6 26-30z"
			opacity={0.502}
		/>
		<path
			fill="#838383"
			d="m531 1068 25 41 98-15s6.9 34-18 38-76.1 12.1-86 5-31-56-31-56 11.6-.7 12-13zm530 379c9.5 12 60 78 60 78l180-38s1.9 28.5-20 36-136.5 31-147 33-17.7-.1-23-2-56.3-65-65-85c7-6.3 8.9-7.4 15-22z"
		/>
		<linearGradient
			id="ch-full32000q"
			x1={1047.9803}
			x2={1086.9803}
			y1={450.2855}
			y2={432.9545}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32000q)"
			d="M1062 1446c6.6 6.5 22 27 22 27l-15 36-24-36s15.3-17.3 17-27z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32000r"
			x1={604.839}
			x2={597.108}
			y1={778.1821}
			y2={826.9941}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32000r)"
			d="M549 1137c-5.1 5.4 17.3 6.5 49 2s53.3-11.3 55-27 0-19 0-19l-96 16s0 19.5-8 28z"
			opacity={0.502}
		/>
		<linearGradient
			id="ch-full32000s"
			x1={1210.3706}
			x2={1198.2396}
			y1={371.6844}
			y2={428.5105}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={0.5} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full32000s)"
			d="M1122 1534c13.6-2.3 170.7-35 178-36 1 6-1.9 18.3-17 24s-155.1 41.2-172 31c5.4-4.1 8.9-11.6 11-19z"
			opacity={0.502}
		/>
		<path
			d="M1140 1223c6.1-1.7 41.6-14.2 71-16 12.1-41.1-1.4-96.3-6-146 7.6-4.6 19.6 57.7 26 68 3.7 27.9 7.1 55.1 1.8 77.7-6 17.2-20.2 39.7-50.8 49.3-26.1 8.2-61.6-3-88.2-10-42.1-17.5-112.8-73.4-135.8-97 .1-13.5.7-26.3 5-41 50.6 33 104.8 86.2 177 115z"
			className="factionColorPrimary"
		/>
		<path
			d="M513 959c4.8-11.7 20.9-16.6 49-28s86.3-32 95-42c-16.4-20.8-66-86-66-86s-9.9 8.4-45 22-83 27.3-83 50c7.8 11.4 32.4 57.9 50 84zm565-173c12.1 1.7 84.5 49.5 104 73-26-4.8-67.5-11.4-82-13-7.3-10.2-11-18-11-18s-1.3-16.8-11-42z"
			className="factionColorSecondary"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1145 1553c24-4.8 115.6-24.6 137-31s18-30.3 19-45 1.5-43.6-11-63c4.6-8.8 9.2-27.9-10-65s-39.3-49-50-62c10.2-15.1 24.5-52.9 8-127s-44.6-134.2-54-152c0-7 2-24 2-24s12 3.9 13-12 2.9-60.6 3-74-6.6-32.2-33-53-63.7-48.2-74-54-16.7-6.4-20-7c-7.6-10-29.9-34.6-40-49s-20.1-30.4-25-38c5.9-10.4 23-38 23-38l-102-81 4-8-2-3s5.3-26.6-7-49-29.7-34.6-83-42-135.1 3.3-143 31c-7.9 27.7-2.3 58 21 75s41 26 41 26-43.1 7-52 41c-21.2 19.5-43 40.7-50 85-19.8 20.4-50.8 52.6-63 63s-52.9 32.6-89 43-50.8 22.5-49 52 3.4 57.2 5 74 7.4 32.2 15 49c7.6 16.8 25.3 55.6 39 64 8.9 19.9 17.6 40.5 23 48s6.4 17.3 37 13 42.4-6.2 56-9 18.7-9.7 19-28-3.6-84.9-6-103-5.5-40.2-25-77c23.1-8.3 41.2-17.7 49-24s21.4-19.6 25-26c11.5 20.8 152.7 266.9 310 377-13.7 30.7-29 71.3-29 96s-1.2 71.7 40 114c16 16.9 24.3 12.3 28 11 15 25.9 41.2 59.9 50 69s13.7 16 32 16c2.4 0 7.1-1 18-3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1173 1548c12-9.7 17-28.1 17-37m61-14c0 13.7.6 24.2-15 36"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M588 1104c0 7 3 29.8-9 36m36-6c9-5.2 11.3-14.8 10-37" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1002 1247c20 17 112.4 81.4 180 73 26.3 0 43.2-23.7 50-36"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1043 1472c9.3-4.3 15.2-16.4 20-39s36.1-92.3 90-99 101.2 18.5 136 78"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1032 1470c10.2-10.8 18.6-53.2 26-72 6.2-15.6 33-46 33-93"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1109 1551c5.4 1 11.4-8 12-25 .4-11.4 2.1-29.2-4-47-13.1-38-29.9-58.9-41-74"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1114 1469 184-35" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1301 1486-178 38-64-83" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M984 1314c4.1 8.2 13.6 25.5 28 45s36.6 37.2 45 37 21 0 21 0m176-28c13.2-4.9 26.2-15.7 23-25"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M463 869c1.5 6.9 50 89 50 89" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m657 887-65-84" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M514 1075c-2.6-25.4-4-67.9-4-84s.2-39.4 16-46c15.8-6.6 82.6-32.7 101-39s48.2-26.8 60-48c22.8-40.8 79.9-53.6 100-66 17.2-10.7-7.5-57-57-95"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M514 1077c8.6 1.1 16 4.9 16-8s-5-78.4-5-85c0-3.4.4-13 5-20 4.4-6.8 12.8-11 27-17 29-12.3 55.8-20 70-26m71-49c-2.8 8.2 1.2-34.4 83-58 46.6-10.3 23-46 17-56-7.3-12.2-34.4-43-65-59s-62.2 25.7-75 38"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M546 1132c13.9 5.8 8.2-19.6 7-61-1.9-66.7-9.3-102.3-16-111"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m652 1093-97 15-26-47" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1187 1014c-10.3-27.5-100.5-193.1-114-211-22.9-9.7-51.2-17.8-65-43-4.4-8-8.7-19.1-15-30-.4-.7-2.5-.7-2.9-1.4-13.1-22.7-27.5-47.8-33.1-61.6 29.4-2.7 73-7 73-7s.7.5.1 1.5c-6 9.9-39.1 64.5-39.1 64.5"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1185 989c.3-20.4 8.7-80-12-113-21.2-4.9-63-14-63-14" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1097.5 844.7c14 2.4 58.1 8.5 91.3 18.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1072 779c5.1 5.2 17.3 26.7 18 51" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1006 706c4.7 16.3 20.2 68.4 70 80" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M710 651c1.2 17.5 12.7 62.3 88 59s133.6-11.7 146-16c2.6-1.8 5-5 5-5l41 38"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M729 624c4.2 11.2 13.1 45.4 158 27" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m862 632-44-72 92-9 26 17" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m895 648-40-67 19-2-23-21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m879 555 28 22 26-2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m951 691-57-47-5 7-26-18-4-6s-37.8 15.4-98-20" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m959 668-97-76" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M705 499c31.3 26.5 88.3 55.2 115 61m93-9c7-5.7 12-27.3 10-36"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M764 539c-8.8 7.9-17 15-17 15s-41.5-24.8-49-33" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1237 1156c3.1 24.8.9 96.3-61 102s-195.3-6.6-452-414" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M804 804c39.3 83.4 135.3 197.5 178 256-24.1 48.2-23 91-23 91s-6.7 50.4 17 76m214 93c13.4-28.8 13-50.7 7.9-67.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M900 953c37.5-20.2 87.2-57.2 205-42" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M964 1107c31 29.3 103 91.3 172 117 23.6-9.6 45.6-13 74-15 6-35.1 13.1-78.7-14-161"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1044 792c19.2 31.7 114.7 217.8 127 237 14.8 7 28 19.8 36.2 31.8"
		/>
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<linearGradient
			id="ch-full82000a"
			x1={1012}
			x2={1012}
			y1={619.939}
			y2={724.9189}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.303} stopColor="#fff" />
			<stop offset={0.782} stopColor="#333" />
		</linearGradient>
		<path fill="url(#ch-full82000a)" d="m1033 1216 37 45-34 50-82-105c35.3 15.7 79 10 79 10z" />
		<path fill="#666" d="m1034 1305 35-50-8-1-57 13 30 38z" />
		<linearGradient
			id="ch-full82000b"
			x1={516.9556}
			x2={991.2396}
			y1={734.6941}
			y2={1066.7911}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#2e3033" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full82000b)"
			d="M600 779c0-44.1 41.2-120 110-120 114.6 0 169 91 169 91s43.8 64.6 85 134c28 47.2 52.8 105.9 67 147 8.1 22.5 84.6 193.4 7.1 192-105.2 9.2-225.7-115.9-227.1-117-35.6-27.2-108.2-115-147-157-21.5-35.3-42.1-50.5-52-88-22.1-53.1-10.7-65.9-12-82z"
		/>
		<linearGradient
			id="ch-full82000c"
			x1={607.2615}
			x2={1081.5444}
			y1={797.9265}
			y2={1130.0236}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#0b0c0d" />
			<stop offset={0.6} stopColor="#dae3f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full82000c)"
			d="M600 779c0-44.1 41.2-120 110-120 114.6 0 169 91 169 91s43.8 64.6 85 134c28 47.2 52.8 105.9 67 147 8.1 22.5 84.6 193.4 7.1 192-105.2 9.2-225.7-115.9-227.1-117-35.6-27.2-108.2-115-147-157-21.5-35.3-42.1-50.5-52-88-22.1-53.1-10.7-65.9-12-82z"
		/>
		<linearGradient
			id="ch-full82000d"
			x1={632.3882}
			x2={1106.6721}
			y1={891.0997}
			y2={990.9608}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#333" />
			<stop offset={0.399} stopColor="#dae3f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full82000d)"
			d="M600 779c0-44.1 30.3-25.8 92-9 126.8 37.3 190.2 63 272 114 28 47.2 52.8 105.9 67 147 8.1 22.5 84.6 193.4 7.1 192-105.2 9.2-225.7-115.9-227.1-117-35.6-27.2-108.2-115-147-157-21.5-35.3-42.1-50.5-52-88-22.1-53.1-10.7-65.9-12-82z"
		/>
		<linearGradient
			id="ch-full82000e"
			x1={537.8084}
			x2={921.7994}
			y1={834.6508}
			y2={1180.3977}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.598} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.676} stopColor="#fff" />
			<stop offset={0.699} stopColor="#fff" />
			<stop offset={0.751} stopColor="#fff" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full82000e)"
			d="M598 768c0-44.1 49.2-109 118-109 104.1 0 182 118 182 118s42.8 58.3 84 127.8c-65.6-8.5-163.5-19.5-142 213.3-.8-35.7-126.9-165.6-168-203-.2-6.1.3-35.2-2-72-40.1-40-64.7-68.1-72-75.1z"
			opacity={0.302}
		/>
		<path
			fill="#b8bfcc"
			d="M771 665c.7-11.9 6-150 6-150l-23-29-35 153-24 22s-10.2.5-31 12c-14.3-8.7-36-25-36-25l-65-99s-28.6-23.7-29-21 70 224 70 224 16.9-63 65-79 60.7-14.9 102-8z"
		/>
		<path
			fill="#dae3f2"
			d="M754 491s-6 8.9-5 9c.9.1 9.9 12 16.3 19.1L777 516l-23-25zm-214 57 18 12 7-8-30-23 5 19z"
		/>
		<path
			fill="#f2f2f2"
			d="M614 751c-129 61.5-294.4 201.5-334 349 26 105.9 78.8 197.1 90 189 54.3-26 120.8-400.1 358-476 56.4-18-69.8-59-114-62zm250-18c194.1 14.2 755.1 187.4 876.6 301.4 6.2 7.9-233.5-84.5-369-130-125.1-42-238.1-76.3-459.6-107.4-17.7-25.6-22.9-36.7-48-64z"
		/>
		<path
			d="M667 672c8 1.8 36.3 23.7 65 53 29.3 30 59.1 67.8 79 96 12.1 17.1 22.8 34 29 36 24.8-12.5 35.6-20.4 72-18-34.1-53.1-133.5-181.9-184-178-20 .4-37.6 2.3-61 11z"
			className="factionColorSecondary"
		/>
		<path
			d="M734 805c-78.1 13.4-244.4 16-451 294 17.5-51.4 42.6-102.7 78-147 28.8-36.1 77-82.4 124-119 60.9-47.5 119.9-81.2 134-81 35.9 3.3 137.8 40.7 115 53zm135-70c163 17.3 423.2 94.7 593 159 54.9 20.8 111.3 39.9 129 58-69.4-31.6-561.9-210.2-676-159-3.1-4.5-10.6-6.5-13-10-14.8-21.8-20.3-33.1-33-48z"
			className="factionColorPrimary"
		/>
		<radialGradient
			id="ch-full82000f"
			cx={939.4333}
			cy={975.6285}
			r={292.7122}
			gradientTransform="matrix(0.4349 0.9005 -0.2237 0.1081 749.1197 24.2779)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.49} stopColor="#969696" />
			<stop offset={0.8284} stopColor="#474747" />
		</radialGradient>
		<path
			fill="url(#ch-full82000f)"
			d="M997.4 1066.1c.4-.3 1.2-.6 1-1-5.7-10.8-60.2-130-129.2-212.6-28.4 9.3-35.6 17.2-53.1 44.1 5.2 42.7 13.5 80.4 33 102.1 19.5 21.7 96.4 76.4 107.2 83.1 9.6.1 19.1.8 19.1.8l.4-.4c5.6-8 13.9-11.7 21.6-16.1zM949 862c-11.3-7.3-33.1-20.2-60.7-17.6 62.1 80.7 106.1 165.7 131.2 220.3 10.9-1.3 20.6 4.5 28.5 10.3-15.8-46.8-53.9-143.1-99-213z"
		/>
		<radialGradient
			id="ch-full82000g"
			cx={-106.4763}
			cy={9.1988}
			r={1764.0154}
			gradientTransform="matrix(0.8863 -0.4631 -0.7171 -1.3725 -207.3843 1924.1511)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.8576} stopColor="#666" />
			<stop offset={0.8806} stopColor="#ccc" />
			<stop offset={0.9054} stopColor="#666" />
		</radialGradient>
		<path
			fill="url(#ch-full82000g)"
			d="M998.1 1066c.4-.3 1.2-.6 1-1-5.7-10.8-60.2-130-129.2-212.6-28.4 9.3-35.6 17.2-53.1 44.1 5.2 42.7 13.5 80.4 33 102.1s96.4 76.4 107.2 83.1c9.6.1 19.1.8 19.1.8l.4-.4c5.6-8 13.9-11.7 21.6-16.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m771 665 6-150s-21.4-30.1-23-29-35 153-35 153l-24 22s-10.2.5-31 12c-12.5-7.6-31.7-21.8-36-25-.6-.5 17 36 17 36s-17.3-36.4-17.8-37.2C619.7 634.7 566 550 566 550s-33.6-27.7-34-25 72 227 72 227"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M618.4 877.3c3.6 7.8 7.5 15 11.6 21.7 37.7 61.2 304 390.1 432 314 57.2-34-122.7-447.2-252-528-41.5-25.9-127.2-47.5-179 14-14.8 17.6-23.8 37.2-28.4 57.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1004.5 1068.7c.4-.3 1.2-.6 1-1-5.7-10.8-64.3-138.5-133.2-221.3-28.4 9.3-42.6 21.2-60.1 48.1 5.2 42.7 13.5 83.4 33 105.1 19.5 21.7 100.4 79.4 111.2 86.1 9.2.6 23 0 23 0l.4-.4c4.5-7.9 14.8-15 24.7-16.6zM950 861c-11.3-7.3-33.1-20.2-60.7-17.6 62.1 80.7 106.1 165.7 131.2 220.3 10.9-1.3 20.6 4.5 28.5 10.3-15.8-46.8-53.9-143.1-99-213z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M836.7 860.3c-34.1-53.1-134.1-196.6-182.3-183.2" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M915.8 843.3C881.6 790.2 783.1 654.9 728 659" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1050 1219c-67.6 0-46.8-31-88.2-53.8-81.7-45-172.5-150.4-243.1-229.9C702 916.4 651 853 651 853"
		/>
		<linearGradient
			id="ch-full82000h"
			x1={645.4915}
			x2={747.4915}
			y1={1080.786}
			y2={1004.2861}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#575757" />
			<stop offset={1} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full82000h)"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M745 966c-3.2-34 .9-68 6-83-24.8-26.1-45.2-51.5-55-58-14.4 4.7-47 27-47 27s60 73.4 96 114z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M864 733c194.1 14.2 755.1 187.4 876.6 301.4 6.2 7.9-233.5-84.5-369-130-125.1-42-238.1-76.3-459.6-107.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M614 751c-129 61.5-294.4 201.5-334 349 26 105.9 78.8 197.1 90 189 54.3-26 120.8-400.1 358-476 56.4-18-69.8-59-114-62z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1039.4 1221.9c11.8 13.2 31.2 35 31.2 35l-35.9 50.8s-48.1-58.5-82.2-100.1"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1002.8 1268.9 64.9-14" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f3f3f3"
			d="m1626 943 128-120s-178.1-52.1-177-52 10-18 10-18l-121-19-19 5-81-20 7-18-103-17-19 5-71-18-56-28s-7.4-11.5-21-16c-8.4-13.2-31.8-43.4-80.7-57.9-2.6-9.4-19-20.7-29.3-22.1-9.6-1.3-17.6-3.4-30 14-58.5 1.9-87 1-87 1l-5-24s10.7-6.3 13-28c5.6-2.6 9-4 9-4l1-10 8-3 5 6 11-4v-11l245-119-9-60-242 112-4-5-22 8-21 4-17 6-4 6s-14.9 6.7-19 8-51.4 17.8-67 23-52.7 49.2-4 97c-4.1 24-8 46-8 46s-23 4.3-34 25c-13.3 12.5-21.8 24.7-30 42-3.1 10-1.8 11.8-2 19-9.3 2.1-50.9 4.1-89 27-22.3-11.5-42.9-9.4-56 4-11.5 11.7-30.5 23.1-8.3 85.3-4.8 8.3-9.2 17.2-13.3 26.9-2.3 5.4-6.7-30.8-35.4-8.2-24.7 30.6.6 38.2-8.7 43-5.7 3-9.3 5-9.3 5s-13.1-1-9 72c.5 8.4 2.5 16.9 5.7 25.2 1.6 4.3-10.9 12.6-8.7 16.8 1.8 3.4 8.1 20.8 14 23 5.2 2 10-11.3 12-8.5 8.7 12.2 18.8 23.5 28.5 33 1.2 1.2-13.6 8.3-12.5 9.4 4.3 4.1 13.6 16.3 22 18 6.4 1.3 12.3-8 14.8-6 6.2 4.8 10.9 7.9 13.2 9 11.3 5.3 64 17 64 17s13.2 1.3 19-12c8.6 3 18.1 5.7 28 3 7.3 9.8 20 28 20 28l-38 36 10 70s8.8 6.9 22.5 17.7c2.7 2.1-5.8 12.9-3.5 18.3 4.6 10.7 20.1 18.5 27 24 3.5 2.7 11.6-14.7 15.2-11.8 16.8 13.2 34.9 27.5 51.4 40.4 2.1 1.6-15.6 11.8-13.6 13.4 11.5 9.1 21.8 16 33 23 3.9 2.4 18.5-5.6 27-4 7.7 1.4 9.5 12.7 10 12l69-5 27-47-8-11s133.9 22.9 251-2c110.6-23.5 142.7-27.8 162-33 7.2 7.1 29.9 35.6 91.3-.1 4.1-2.4 9.3 15 13.7 12.1 2.7-2.1 23.6-13.6 26-20 3-8.1-12.6-11.5-9.5-14.1 27.5-23.7 46.7-47.8 59.4-74 .8-1.6 12.9 11.5 14.1 10.2 7.7-8.8 11.2-27.8 14-39 .4-1.5-14-11.5-13.7-13 3.9-17.5 5.7-36.1 5.7-56 .5-28.4-30.9-36.5-40-42-.9-2.6-1.9-5.7-3.1-9.1-1.6-4.5-17.4-11.6-25.9-16.9-8.6-21.9-25.7-59.7-44-89 46.5 21.2 100 44 100 44l10-22 115 24z"
		/>
		<path
			fill="#e6e6e6"
			d="M732 945s119.6 82.2 164 99c-17.3 5.9-48.3 20.7-60 31-21.6-6.5-85.8-43-125-77 10.3-28 21-53 21-53z"
		/>
		<linearGradient
			id="ch-full55003a"
			x1={780.6428}
			x2={730.6428}
			y1={911.0759}
			y2={934.392}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full55003a)" d="m753 961-22 55 24 16 26-55-28-16z" />
		<path fill="#d9d9d9" d="m724 953-13 47-39 6s26.3-48.1 52-53z" />
		<linearGradient
			id="ch-full55003b"
			x1={829.6428}
			x2={779.6428}
			y1={879.0759}
			y2={902.392}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full55003b)" d="m802 993-22 55 24 16 26-55-28-16z" />
		<linearGradient
			id="ch-full55003c"
			x1={880.975}
			x2={824.975}
			y1={849.9666}
			y2={876.0805}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full55003c)"
			d="M879.2 1051.1c2.3-4.9 3.8-8.1 3.8-8.1l-28-16-28 47s2.2 1.7 5.3 4.3c13-8 28.2-18.9 46.9-27.2z"
		/>
		<linearGradient
			id="ch-full55003d"
			x1={723.2463}
			x2={507.2462}
			y1={947.9445}
			y2={1048.6665}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.187} stopColor="#f3f3f3" />
			<stop offset={0.557} stopColor="#ccc" />
		</linearGradient>
		<path
			fill="url(#ch-full55003d)"
			d="m525 816-13 18s30 84.9 81 122c5.6 4.5 16.4 17.1 16 26 10.1 6.9 56.6 20.9 62 25 7.2-12 32.5-50.6 57-58-31.8-4.8-46-19-46-19s-24.2 38.6-57 24c-12.7-5.6-33.9-25.3-51-50-27.1-39.3-49-88-49-88z"
		/>
		<linearGradient
			id="ch-full55003e"
			x1={694.3113}
			x2={637.4482}
			y1={1029.4178}
			y2={920.4178}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.194} stopColor="#f3f3f3" />
			<stop offset={0.562} stopColor="#d9d9d9" />
		</linearGradient>
		<path
			fill="url(#ch-full55003e)"
			d="M688 901c1.1 1.4 74.9 25.7 58 38s-51.2 20.1-77 71c-19-12.5-15.1-14.3-59-26-2.1-8.7-7.1-19.2-24-35 18.7-3.1 24.2-.5 33 4s34 4.9 50-9c13.9-12 19.7-26.3 19-43z"
		/>
		<linearGradient
			id="ch-full55003f"
			x1={992.5498}
			x2={992.5498}
			y1={1315.4424}
			y2={1373.175}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full55003f)"
			d="M974 554c-5.2 6-14.5 32-14 40s54.7 18.1 65 2c2-24-21.2-66-51-42z"
			opacity={0.149}
		/>
		<linearGradient
			id="ch-full55003g"
			x1={1619.8523}
			x2={1718.4192}
			y1={1116.7477}
			y2={1001.7477}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#262626" />
			<stop offset={1} stopColor="#414141" />
		</linearGradient>
		<path fill="url(#ch-full55003g)" d="m1618 859 132-32-121 115-11-83z" />
		<linearGradient
			id="ch-full55003h"
			x1={817.4528}
			x2={862.0479}
			y1={1071.5972}
			y2={1324.5032}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#ccc" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full55003h)"
			d="M850.8 604c25.5 13.7 56.2 36.5 61 49 4.8 12.5 50.2 116 71 193-66.7 8-236.5-40.2-303.8-148 9.1-45.4 26.8-59.3 32.8-68 17.8-11.7 63.2-55.6 139-26z"
		/>
		<linearGradient
			id="ch-full55003i"
			x1={771.995}
			x2={771.995}
			y1={574}
			y2={812}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#989898" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full55003i)"
			d="M674 1108c11.9 4.1 81.3 41.2 142 89 39.7 31.3 73.2 70.6 93 96-13 26.7-26 48-26 48l-70 5s-5.9-4.5-15.4-11.9c-4.8-3.7-17.2 8.9-23.6 3.9-5.5-4.3-25.6-14.2-30-24-3.4-7.4 18.7-8.8 4-19-13.5-10.8-27.4-23.5-41-34-6.7-5.2-12.4 13.3-19 9-4.4-2.9-21.7-13.1-26-22-3.6-7.4 5.8-13.9 3-16-13.4-10.3-22-17-22-17l-8-70s24.5-27.6 39-37z"
		/>
		<linearGradient
			id="ch-full55003j"
			x1={912.0874}
			x2={760.0874}
			y1={619.2576}
			y2={726.5615}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full55003j)"
			d="M908 1292c-10.4-17.1-74.8-90.7-125-123-15.2 14.9-27 34-27 34s98.4 78.8 134 123c7.9-17 12.3-22.1 18-34z"
			opacity={0.302}
		/>
		<path
			d="m812 1347 68-6s-43.2-49.9-99-96c-49.4-40.8-109.5-77.1-144-96 5.9 59.2 7 66 7 66l21.2 16.6c3.4 2.7-6.5 9.7-3.2 16.4 4.4 8.7 22.2 17.4 28 22 3.4 2.7 9.6-11.3 13.2-8.5 16.9 13.3 35.3 27.7 52 40.9 4.1 3.2-12.8 4.1-10.2 10.6 3.2 8 25.8 21.7 30 25 5.9 4.7 16.7-6.9 21.3-3.4 9.7 7.7 15.7 12.4 15.7 12.4z"
			opacity={0.2}
		/>
		<linearGradient
			id="ch-full55003k"
			x1={1416.2987}
			x2={1416.2987}
			y1={656}
			y2={905}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="gray" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full55003k)"
			d="M1336.3 1241c71.8-20.7 113-65.2 132-110.4 19.2-45.9 15.5-92.5 9.9-115.6 17.2 10.8 21.4 13.3 27.8 18 2.3 7.9 6.6 43.6 2 80-.1 4.9 11 6.1 10.6 11.1-1.3 13.8-4.2 27.9-9.6 40.9-.3.7-13.7-12.7-14-12-11.4 26.6-34.2 51.6-59 73-1.1 1 13.9 13.5 12.8 14.4-9.3 7.5-19.8 14.1-31.8 19.6-.9.4-14.9-12.8-15.9-12.4-19 8.3-41.1 14-66.8 16.4-10.7-4.4-19-17-19-17s-9.1 2.7 21-6z"
		/>
		<linearGradient
			id="ch-full55003l"
			x1={514.4465}
			x2={514.4465}
			y1={837.8132}
			y2={1003}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#e6e6e6" />
		</linearGradient>
		<path
			fill="url(#ch-full55003l)"
			d="M458 933c-4.3-3.2-8-16-8-16s-2 43.3 6 58c1.1 3.9-8.2 15.3-7 19 3.7 11.3 8.8 11.5 13 21 1.7 3.9 13.3-9.6 15-6 8.2 17.7 22 22.3 24 31 1 4.2-9.1 8.3-10 9 6.2 10.1 12.2 12.4 18.6 19.2 2.2 2 8.7 1.1 17.4-4.2 9.3 6.8 14.9 7.5 21 12 11.3.2 30 12.4 32 2-7.8-3.1-37.3-21-66-50-31.5-31.8-37.5-49.1-56-95z"
			opacity={0.502}
		/>
		<path
			fill="#bfbfbf"
			d="m1618 860-174-53-24.9-18-85-28 2 10-119-36-77-21h-24l-51-19-108-20s-29-22.9-47-27c22.3 43.3 54.5 122.1 80 199 86.3-5.7 166-40 166-40 21.5 6.6 41 12 41 12l9 15 92 40 8-20 81 26 7 12 108 47 10-20 116 24-10.1-83z"
		/>
		<path fill="#8c8c8c" d="m1446.9 804 64 113-10 25-82-151 28 13zm-233-71 28 14 62 106-10 20-80-140z" />
		<path fill="#bfbfbf" d="m1444.9 802 134-30 6-20-167 36 27 14zm-209-56 130-25 4-19-157 30 23 14z" />
		<radialGradient
			id="ch-full55003m"
			cx={1103.894}
			cy={1301.902}
			r={1255.235}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.427} stopColor="#f3f3f3" />
			<stop offset={0.556} stopColor="#a6a6a6" />
		</radialGradient>
		<path
			fill="url(#ch-full55003m)"
			d="M596 1013c2.1-5 12.7-16.8 13.8-33 17.4 16.1 40.2 9.9 61.3 27 41.1 33.4 56.1 77.4 125.9 109 178.5 90.7 230.2 77 276 74 45.5.7 172.6-15.3 202-21 27.1-3.6 27.3-5.3 88-19 62.4-17.3 113.4-44.8 118-80-5.3 45.7-9.1 124.2-154 175.9-147.9 31.8-219 42-219 42s-99.1 13.7-201-4c-45.8-43.9-184.9-160.4-231-177-10.3-13.9-22-28-22-28s-39.4 6.5-103-40C520.7 1017 449 918.7 461 876c1.3-.7 3.1-4.1 7 3 11.9 21.4 32.8 74.8 34 74 66.9 74.1 89.6 65.9 94 60z"
		/>
		<radialGradient
			id="ch-full55003n"
			cx={407.102}
			cy={976.605}
			r={265.967}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.697} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full55003n)"
			d="M648 1076c-17.7-21.5-55-49.3-104-44 27.6 29.1 121.7 65.5 104 44z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full55003o"
			cx={745.519}
			cy={1143.1489}
			r={621.204}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full55003o)"
			d="M469 877c5.9 15.7 44.3 121.8 112 138 4.3.8 19.4-2.3 25-17 6.9-17.9 26.7 51.2 18 51-35.4-19.5-65.4-6.1-82-16s-67.1-79.8-85-149c4.2-2.5 8.1-4.7 12-7z"
			opacity={0.702}
		/>
		<radialGradient
			id="ch-full55003p"
			cx={757.001}
			cy={983.364}
			r={404.745}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.427} stopColor="#d9d9d9" />
			<stop offset={0.503} stopColor="#d9d9d9" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full55003p)"
			d="M610 984c13.3 6.3 54.6 13.9 61 25-7.9 11.6-19.5 53.1-10 83-20.4-26.7-33.3-49-87-57-14.6-15.7-17.4-24.9-7-22s26.3 7.8 32-4 10-17.1 11-25z"
		/>
		<linearGradient
			id="ch-full55003q"
			x1={1009.5981}
			x2={980.7932}
			y1={943.73}
			y2={1117.729}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1} stopColor="#fff" stopOpacity={0} />
			<stop offset={1} stopColor="#626262" />
		</linearGradient>
		<path
			fill="url(#ch-full55003q)"
			d="m1206.9 929-76-113s-93.4 32.8-195 32c-104.2-23.7-162-57-162-57l172 153 49 21 212-36z"
		/>
		<radialGradient
			id="ch-full55003r"
			cx={986.725}
			cy={1480.988}
			r={852.953}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.471} stopColor="#000" />
			<stop offset={0.526} stopColor="#000" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full55003r)"
			d="M1133 819s-88.3 24.7-152 28-145.5-18-224-71c44.2 43.7 214 187 214 187l221-57-59-87z"
			opacity={0.302}
		/>
		<path
			fill="#34a798"
			d="M1330 1156c-2.2-19.7-17.8-71.4-74-140 22.7-3.7 63-4.5 106-1 4.6-11.2 11-32 11-32s81.7-5.6 99 22c24.8 78.2-13.3 108.1-55 126-40.8 17.6-85 24-85 24s-.6.5-2 1zm-539-43c12.5-13.3 48.4-53.2 105-71 44.7 20.8 64 30 64 30l108-30s132.3 102.2 142 137c-30.5 4.1-135.4 13-173 13-37.6 0-83.7-1-246-79z"
		/>
		<radialGradient
			id="ch-full55003s"
			cx={1283.761}
			cy={1077.4761}
			r={521.675}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.487} stopColor="#000" stopOpacity={0} />
			<stop offset={0.705} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full55003s)"
			d="M1374 985c28.1-4.8 77.3-6.8 97 20 14.5 48.1 32.6 123.3-140 151-15.1-48.2-41.7-118.1-77-142 44-3.8 92.5-2.8 107 1 6.5-17.5 8.1-21.2 13-30z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full55003t"
			cx={1053.916}
			cy={1146.246}
			r={750}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.487} stopColor="#000" stopOpacity={0} />
			<stop offset={0.705} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full55003t)"
			d="M791 1113c12.5-13.3 48.4-53.2 105-71 44.7 20.8 64 30 64 30l108-30s132.3 102.2 142 137c-30.5 4.1-135.4 13-173 13-37.6 0-83.7-1-246-79z"
			opacity={0.502}
		/>
		<radialGradient
			id="ch-full55003u"
			cx={1207.563}
			cy={1031.6851}
			r={483.294}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.487} stopColor="#000" stopOpacity={0} />
			<stop offset={0.705} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full55003u)"
			d="M1208 1177c12.1 1.4 102-12.8 120-20-5.4-33.1-30.3-104.5-75-141-71.6 4.3-157.6 16-182 27 26.3 20.2 127.4 109.2 137 134z"
			opacity={0.302}
		/>
		<linearGradient
			id="ch-full55003v"
			x1={1031.7346}
			x2={1035.9966}
			y1={627.9857}
			y2={750.0396}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#017474" />
			<stop offset={0.586} stopColor="#34a798" />
		</linearGradient>
		<path
			fill="url(#ch-full55003v)"
			d="M847 1170c19.4 14 127.2 71.5 361 35 8.6 10.1 15 22 15 22s-11.3 40.2-40 48c-36.7 6.3-125 29.7-281 8-23.6-36.8-55.3-91.9-55-113z"
		/>
		<linearGradient
			id="ch-full55003w"
			x1={1380.9017}
			x2={1332.6976}
			y1={682.9489}
			y2={817.9489}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#017474" />
			<stop offset={0.586} stopColor="#34a798" />
		</linearGradient>
		<path
			fill="url(#ch-full55003w)"
			d="M1327 1178c31.3 4.9 108.3-25.9 142-54 8.6 10.1-37 62-37 62s-62.1 61.4-172 73c9.8-10 24.2-28.9 32-41 14.6-16.2 22.9-26 35-40z"
		/>
		<path
			fill="#34a798"
			d="M1495 1027c5.1 17.3 13.9 66.1-37 154-49.2 73.1-119.1 68.8-138 70 5.5 6.8 21 15 21 15s105.6-19.3 140-88 35.2-108.7 27-145c-4.6-2.6-6.2-6.3-13-6z"
		/>
		<linearGradient
			id="ch-full55003x"
			x1={1416.1437}
			x2={1416.1437}
			y1={654}
			y2={893.0228}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full55003x)"
			d="M1495 1027c5.1 17.3 13.9 66.1-37 154-49.2 73.1-119.1 68.8-138 70 5.5 6.8 21 15 21 15s105.6-19.3 140-88 35.2-108.7 27-145c-4.6-2.6-6.2-6.3-13-6z"
			opacity={0.4}
		/>
		<linearGradient
			id="ch-full55003y"
			x1={880.9768}
			x2={647.7488}
			y1={581.6241}
			y2={791.6241}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#009996" />
			<stop offset={1} stopColor="#34a798" />
		</linearGradient>
		<path
			fill="url(#ch-full55003y)"
			d="M646 1131c20.9 9.5 220.1 146.3 244 196-3.4 4.4-9 14-9 14s-49.4-58.5-116-109-119.3-82.5-131-87c4.3-6.8 6.6-11.2 12-14z"
		/>
		<linearGradient
			id="ch-full55003z"
			x1={596.8639}
			x2={450.3979}
			y1={826.5399}
			y2={1028.1329}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.352} stopColor="#00807d" />
			<stop offset={0.798} stopColor="#34a798" />
		</linearGradient>
		<path
			fill="url(#ch-full55003z)"
			d="M454 884c-1.9 7.1-8.5 25.1-3 37s31 102.6 136 163c16.5 2.3 34.1 4.1 33-9-22.8-8.2-63.7-17.2-100-64s-54.6-89.1-66-127z"
		/>
		<path
			fill="#768692"
			d="M782 786c3.2-23.9 13-57.3 34-92-26-17-81-47-81-47s-37.2 35.8-42 71c20.4 21.5 73 67.6 89 68zm-221-53c6.5 21 64 136.1 130 179-10.5 17.5-26.8 62.8-80 35-46-27.8-85-139-85-139s-8.7-55.4 35-75zm-67 111c1.3 15.8 38.7 111.4 112 150-3.6 11.7-13.5 31.3-40 15s-82.2-74.6-96-134c3.3-15.5 7-24.2 24-31z"
		/>
		<radialGradient
			id="ch-full55003A"
			cx={1115.96}
			cy={1350.891}
			r={808.269}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.702} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full55003A)"
			d="M552 718c6.5 21 73 151.1 139 194-10.5 17.5-26.8 62.8-80 35-46-27.8-85-139-85-139s-33.5-71.6 26-90z"
		/>
		<radialGradient
			id="ch-full55003B"
			cx={961.458}
			cy={1223.6951}
			r={648.838}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.702} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full55003B)"
			d="M498 824c17.1 6.7 28 74.3 109 147-.1 15.8.5 60.5-43 36-46-27.8-78-89-78-89s-49.8-86.3 12-94z"
		/>
		<linearGradient
			id="ch-full55003C"
			x1={770.3159}
			x2={770.3159}
			y1={1062.052}
			y2={1276.052}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#768692" />
			<stop offset={1} stopColor="#34a798" />
		</linearGradient>
		<path
			fill="url(#ch-full55003C)"
			d="M679 695s-24.6 4 207 198c-31.8 5.9-66 16-66 16S619.7 734.4 660 699c5.1-1.6 19-4 19-4z"
		/>
		<linearGradient
			id="ch-full55003D"
			x1={844.0188}
			x2={1057.9899}
			y1={904.1423}
			y2={1159.1423}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.503} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full55003D)"
			d="m763 784 301 255s1.1-44.6 84-57c-32.5-41.6-113-142-113-142s-61.1 18-139-1c-67.6-16.5-133-55-133-55z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full55003E"
			x1={1084.6063}
			x2={1007.5123}
			y1={1064.7141}
			y2={1231.7141}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full55003E)"
			d="m969 680 94 119 57-5 39 15s-65.2 29.2-174 38c12.4-74.8-16-167-16-167z"
			opacity={0.4}
		/>
		<path fill="#595959" d="m1126.9 317 28-7 5 48-33-41z" />
		<linearGradient
			id="ch-full55003F"
			x1={813.405}
			x2={813.405}
			y1={1312}
			y2={1388}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#fff" />
		</linearGradient>
		<path
			fill="url(#ch-full55003F)"
			d="M757.8 566c13.1 8.5 44 16 68-13 18.5-17 23-21 23-21s15.1.3 21.2 8 6 24 6 24-81.4 15.1-125.2 44c2-22.4 4.6-31 7-42z"
			opacity={0.502}
		/>
		<path
			fill="#b3b3b3"
			d="M1123.9 322S971.3 388.4 902 420c-20 4.4-24.9-2.7-38 2-6.6 1.3-20.6 3.1-18 6 3.7 4.2 2.7 3.5 4 7-4.3 2.5-10.3 5.1-13 7-.8-1.1-2.1-2.7-3-4-3.5 2.2-38.2 15.9-36.2 19 .9 1.4 12.4 17.5 26.7 35.4 5.5 23.8 3 46.7-6.7 63.6 33.5-31.6 46.5-19.6 52-16 10.9-8.3 10.7-21 11-27 3.4-1.3 6.1-3.1 10-4-1.3-3.4-1.2-8.2-1-10 3.3-.2 6-2.2 8.3-4.7 2.6 1.6 4.2 3.3 6.7 5.7 3.2-.6 5.6-1.2 9-2 2-2.7-1-9-1-9s249-122.2 249-121c.1 1.2-37.9-46-37.9-46z"
		/>
		<path
			fill="#d9d9d9"
			d="M772 465c18 0 55.3 5.7 55 51-.3 40.7-27.6 57-47.9 57-31.1 0-50.1-24.3-50.1-51 0-17.8 11.9-57 43-57z"
		/>
		<linearGradient
			id="ch-full55003G"
			x1={1160.9171}
			x2={1150.915}
			y1={876.6381}
			y2={939.7881}
			gradientTransform="matrix(1 0 0 -1 0 1920)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#8c8c8c" />
			<stop offset={1} stopColor="#333" />
		</linearGradient>
		<path
			fill="url(#ch-full55003G)"
			d="M1083 1007c11.8-9 18.1-17.1 59-23 42.5-6.1 60-5.2 84 1 26.9 15 26 29 26 29s-117.6 8.6-187 29c-.6-7.3-4.3-17.4 18-36z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M680 695c-13.9 1-53.5 9.8-89 29-16.3-8.7-35-13.6-56 2s-25 40-9 85c-6.8 12.8-13 25-13 25s-14.4-25.3-34-5-10.3 39.7-9 44c-11 4.1-14 7-14 7s-6 6.9-6 40 3.6 50.3 7 55c-3.2 4-9 15-9 15l15 25 11-9 29 33-13 7 23 20 15-5s12.5 8.9 45 18 42.6 7.9 51-6c8.8 2.9 17.7 7.7 28 5 6.4 8.7 19 27 19 27l-38 38 10 69 23 16-6 18 31 24 15-13 49 43-13 11 34 26 24-5 15 13 67-6 26-48-3-9s54.5 9 127 9c87.2 0 244-35.1 281-45 4.3 4.4 9 8 9 8s11.8 10.6 22 9 33.6-4.3 58-17c6.8 5.7 15 12 15 12s21.9-12.9 31-22c-8.1-6.3-13-10-13-10s39.8-29.9 60-76c5.4 6.4 14 13 14 13s8-22.2 11-40c-6.6-7.7-12-15-12-15s9.1-41-1-76c-12.2-9.1-32-22-32-22s-1.4-3.6-3.8-9.5c-1.2-2.9-18.5-12.5-26.2-16.5-9.8-23.7-33.7-70.9-47-93 29.9 14.1 104 48 104 48l10-22 118 24 125-120-178-52 12-20-120-19-25 7-78-20 7-20-100-14-26 4-66-18-58-29s-12.1-12.3-24-18c-8.8-15.8-33.5-43.4-80-58-4.5-14.7-38.9-28.7-51-5-24-1-48.4-2.7-93 2-1.3-9.1-5-24-5-24s8.1.3 14-28c4.5-2.6 8-5 8-5l1-8s5.7-6.5 7-6 8 9 8 9l11-6-1-11 247-120s-11-57.4-11-56-28 11-28 11L903 420l-40 1-17 8 1 8-10-2-11 9-71 24s-36.8 14-25 69c5.7 20.3 25 33 25 33-.3 1.1-6 38-6 38s-56.4 19.5-69 87z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1751 825-132 31 11 88" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M586 1084c-107-64.7-122.8-130.3-136.5-173.5M467 962l-10 15m29 20-13 12m44 24-15 8m21 23 18-9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M881 1339c-89-99.9-217.1-181.5-245-194m9-14c27.8 13.5 196.3 123.5 244 194m-205-150-19 57m81-13-43 42m114-67s-14.7 20.5-25.8 36m10.8 30-50 44m45 30 59-19"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1324 1253c155.9-3.1 189.4-184.1 170-227m14 83c-3 13.3-10.4 41.6-20 57m-53 62c-9.1 6.3-24.5 15.9-37 22m78-138 11 5"
		/>
		<path
			fill="#1a1a1a"
			d="M669 1101.2c-3.6-3.3-6.7-6.9-10-10.4-3.2-3.5-6.4-7-9.6-10.3-6.4-6.7-12.9-13.1-20.1-18.2-3.6-2.6-7.5-5-11.4-7.2-3.9-2.3-8-4.4-12.2-6.4-8.3-4.1-16.9-7.8-25.8-11.6 4.9-.2 9.7.2 14.5 1 4.8.8 9.5 1.9 14.2 3.3 4.7 1.5 9.2 3.2 13.7 5.3l1.7.8 1.6.9 3.3 1.8c2.1 1.3 4.2 2.5 6.3 4.1 8.3 5.9 14.8 13.5 20.4 21.4 5.5 7.9 9.9 16.5 13.4 25.5z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1587 751s-169.1 34.7-170 37 83 153 83 153" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1445 739-114 22 62 130 61 28" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1371 700-162 32 86 141" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1251 688-123 24 79 122 90 40 7-20 83 27" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1183 671-121 22 59 102 78 24" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1123 642s-170.1 26.2-169 30 110 128 110 128l56-7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m958 675 105 18s49.4 18.3 64 22.8c2.5.8 4-1.8 4-1.8l18 3 64 17 33 12 90 24v-8l83 27 30 18 171 51"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M693 720c2.5-16.8 18.9-59.3 44-74 24.1 17.1 59.9 34.9 82 48-14.7 20.5-37.7 60.1-37 95"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M680 696c4.7 29.9 145 152 287 152 41.7 0 75.3-9.5 101.9-14.3C1133 822 1157 808 1157 808m-17-7-155 38s29.4-147.6-69-190c-40.6-34.3-74.2-63.8-137-56-14.6 6.5-39 19-39 19"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M779 593c12.4-6 67.3-26.6 100-31" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m1105 627-29-9s-25.7 21.1-161 29c-23.1-.9-55-5-55-5l-15 18 28 79s79.4 57.8 114 105"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M910 648s50.5 95.3 73 192" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M967 563c-3.3 7.7-8 23.4-8 31s48.6 19.9 66 3c.6-10.1-2.2-29.1-13-40"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M994 606c0-10.1 6.7-54-20-54" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m868 664 21 62.3 18 12-23-71.3-16-3z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="m795 457 74 81m9-9-69-79m13-4c1 .3 61 67 61 67m9-13-58-62m15-6 56 65m15-5-61-70m50-4-35 18"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1164 365-37-44" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M753 469c8.5-5.3 45-10.3 63 15s12.9 73-19 86c-15 6.1-27.6 9-42 .4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M775.5 486c15.2 0 27.5 15 27.5 33.5S790.7 553 775.5 553 748 538 748 519.5s12.3-33.5 27.5-33.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M775.9 504.2c6.9 0 12.5 7.1 12.5 15.9s-5.6 15.9-12.5 15.9-12.5-7.1-12.5-15.9 5.6-15.9 12.5-15.9z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M806 565s34.1-26.1 38-31c9.8.1 26 4 26 4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1476 1014c4.3 16.9 27.7 83.9-61 118-33.8 13-88.5 27.9-146.6 37.8-94.1 15.9-197 23.2-233.4 23.2-58.9 0-171.5-38.2-245-81-88.7-51.7-74.1-74.1-122-107-25.7-14.8-44-12.4-59-24 1.7-18.2-34.3-34.9-60-75s-33.9-61.7-38-72"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M665 1098c-11.2-27.4-46.9-154.5 223-205" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M720 744c18.8 21.9 155.6 137.3 280.2 242.2 100.9 85 194.2 154.8 209.8 189.8.6 1.6 1 4 1 4s2.7 10.1-2 26c12.3 16.5 14 19 14 19s-6.6 40.1-40 51m60-12c22.8-11.8 32.3-18.4 49-49 21.3-20.2 33-33 33-33s4-4.2 4-24-20-83.1-73-143c-7.1-19.9-30.2-45.4-110-32-83.2 13.9-83 54-83 54m184-38c-21.7-44.7-69-119-115-180"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M734 945c26.7 19.1 101.4 78.1 226 127 21.6-6.8 105.6-32.8 198-47 87.6-13.4 171.6-15.4 204-9 5.2-16.7 11-32 11-32s73.5-15 98 20"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1372 987c6.6-23.3-5.2-72.3-35-122" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1313 1248c162.9-42.3 170.9-152.8 169-197m-9 66c-26.8 24.2-54.2 49.3-149 65m-31 34c-28.3 5-71 9-71 9m-13-19c-41.7 4.7-255.9 36.9-362-37m-2-27c.9 32.3-1.6 56.9 64 142m-3 5c-27.3-35.7-96.4-121.3-236-182m-46-32c-25.1-7.6-124.5-33.8-167-193m71 136c-21.1-25.4-27.1-58.3-29-78m-29-64c5.6 17.6 27.6 89.3 96 134 35.6 22.9 43.5-22.3 43-29m-4 16c-22.5-18.6-95.8-74.1-110-153m18-7c-18.9 1.5-42 14.9-42 44"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M591 724c5.6 13.3 17.8 39.7 37 70 27.3 43.1 93.8 112.8 37 154-66.7 48.4-130.3-113.1-139-136 .2-23.2-2-95.5 70-78m-35 0c9.4 29 50.9 108.7 124 177"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M662 697c-13.3 15.1-30.3 45.4 159 210m15 169c-65.1-26.3-126-78-126-78l17-49m26 14-21 52s.3 1 .8.9c4.7-.5 29.2-3.9 29.2-3.9m-7.4 21.6c5.8-12.1 24.4-52.6 24.4-52.6m24 16-24 47s.4 1 1.1.9c5.4-.4 28.9-2.9 28.9-2.9m19-32-26 48m25 13 28-46"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M897 1044c-22.5 4.9-66.7 26.3-104 69" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m711 999-40 8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1040 1021c1.9-17.1 23.3-43.4 100-59 74-13.6 98 20 98 20m-101-20-100-122m123 16c8.2-.7 45.1-3.6 88.9-4.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={10}
			d="M1156 991c7.2 0 13 5.8 13 13s-5.8 13-13 13-13-5.8-13-13 5.8-13 13-13zm39-2.3c6.3 0 11.3 5.1 11.3 11.3s-5.1 11.3-11.3 11.3-11.3-5.1-11.3-11.3 5.1-11.3 11.3-11.3zm-79 12c6.3 0 11.3 5.1 11.3 11.3s-5.1 11.3-11.3 11.3-11.3-5.1-11.3-11.3 5.1-11.3 11.3-11.3z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="m1114 1026-28-21" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1158.9 1024.7c-14.1-10.5-46.2-34.3-46.2-34.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={10} d="M1197.9 1016.7c-14.1-10.5-46.2-34.3-46.2-34.3" />
	</svg>
);

export default Component;

import * as React from 'react';

const Component = (props: React.SVGProps<SVGSVGElement>) => (
	<svg viewBox="0 0 1920 1920" {...props}>
		<path
			fill="#f2f2f2"
			d="M364.6 549.7c66.2 0 98.1 26.5 109.2 34.8 26-13.8 49.9-28.4 49.9-28.4v-8.5c0-3.1 11.9-48.4 172.6-48.4S876 536.9 876 544.1v4.8l56.7 28.9s15.9 3.2 38 7.6c4.7-7.7 7.4-14.4 31.4-40.1 48.8-47.4 98.1-40.2 107.7-40.2 9.6 0 54.3-1.2 93.5 28.2 39.2 29.5 58.1 73.3 64.2 92.4 5.1 2.4 19.6 10.9 19.6 10.9l18.5-2.2 56.6 19.6-7.6 14.2 40.2 19.6 17.4-2.2 65.3 24-8.7 13.1 87 48.9-93.8 76.7c220.3 13.5 244.4 100.2 244 110 1.4 9 2.5 32.3.6 55.5 7.1-1.1 48.2-5.9 64.5-3.9 16.3 1.9 51.7 3.3 19.5 87.2-3.7 8.8-12.8 26.8-18.5 33.8-49.3 60.3-160.8 58.5-225.2 46.1-71.9-14-110.8-13-153.2-10.9-42.5 2.1-417.7 45.5-526.1 82.1-25.5 11.6-40.4 18-49.5 26.2-19 17.1-20.8 32.5-97.7 51.6-144.7 36.2-399-47.6-409-94.6-105.8-33.7-100.1-84.5-100.1-84.5s-2.5-54.6-3.6-73.6c-1.2-19.1 14.7-52.6 60.2-76.1-44.4-16.9-179.3-109-181.1-152.2-2.3-64 38-96 76.1-124.4 10.1-8.4 21.7-15.2 29.7-19.5 2.6-12.7 19.7-100.2 95.7-136 12.2-5.5 37-15.4 76.3-15.4zM1454.7 848l-50-18.6-9.8 13.1s-30.2-21-45.7-33.1c-27.1 20.2-50.8 38-52.1 41.5 61.1-4.6 113.2-5.2 157.6-2.9zm5.5 2.1 2-1.6c-2.4-.1-4.9-.3-7.4-.4l5.4 2zm-998.8 230.7v-8.7s-12-23.6-20.7-39.1c-93 16.3-139.3 37.5-153.4 45.7 75.7 2.5 174.1 2.1 174.1 2.1zm-14.2 21.8v12s-147.2-.2-190.4 1.1c51.8 35.1 171.9 42.4 171.9 42.4l31.5-33.7 1.1-21.8h-14.1zm117.5 35.8c-2.5 8.8-16.4 16-42.4 24 58.4 0 137.1-24 137.1-24l-95.7-22.9c-.1 0 1.1 12.7 1 22.9zm-5.4-42.4 13.1 2.2 16.3-6.5s81.6 23.4 113.2 33.7c8.6-1.8 50.8-24.5 60.9-31.5 2.7-11.9-46.8-39.1-46.8-39.1l-152.3 37-4.4 4.2zm3.3-31.6 107.7-20.7s-58.9-14.2-117.5-14.2c8.1 6.3 10.9 8.8 10.9 15.3s-1.1 19.6-1.1 19.6zm910.7-118.6 35.9-13.1s-17.4-7.6-35.9-7.6c.1 8 0 20.7 0 20.7zm149 59.8c4.5.1 24.5-8 35.9-13.1-14.4-24.6-83.8-46.8-83.8-46.8l-78.4 31.5c.2.2 115.2 25.2 126.3 28.4zm-76.1 20.8 6.5-4.4-78.4-9.8-1.1 9.8 73 4.4zm-166.5-68.6-44.6-37s-95 7.5-117.5 29.3c13.6 6.7 47.9 20.7 47.9 20.7l93.5-4.4 6.5 5.4h14.2v-14zm-53.3 33.8 45.7 14.1 7.6-14.2h-53.3v.1z"
		/>
		<path
			fill="#d9d9d9"
			d="m1468.9 722.8 6.5-12-145.8 12 64.2 119.7 6.5-8.7-55.5-101.2 116.4-13.1 7.7 3.3zm-108.8-68.6-133.9 15.3 60.9 113.2 8.7-15.3-8.7-5.4-45.7-87 114.2-9.8 4.5-11z"
		/>
		<path
			fill="#b3b3b3"
			d="m1435.2 783.7 24 65.3-55.5-20.7-9.8 13.1s-31-20.6-52.2-38c-4-3.2-5.5-10.8-9.8-14.2-13.5-9.2-20.1-12.5-33.7-20.7-3 2.1-7.1 7.8-13.1 13.1-15.9-12.1-33.6-26.2-47.9-37-3.4-1.4-3.1-9.5-6.5-10.9-17.6-7.5-35.9-16.9-52.2-27.1-40-48.6-90.3-108.8-90.3-108.8s158.3 80.9 261.1 136c50.8 27.2 85.9 49.9 85.9 49.9z"
		/>
		<path fill="#bfbfbf" d="m1436.3 783.7 117.5-10.9-92.4 76.2-25.1-65.3z" />
		<linearGradient
			id="ch-full89500a"
			x1={1520.535}
			x2={1465.6936}
			y1={1674.9265}
			y2={1726.066}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full89500a)" d="m1465.6 834.8 63.1-51.1-78.4 8.7 15.3 42.4z" opacity={0.702} />
		<path
			fill="#d9d9d9"
			d="m577.8 809.9 9.8-20.7-183.9 15.3L506 939.4l7.6-12-45.7-51.1-41.3-63.1 144.7-12 6.5 8.7zM518 742.4l9.8-17.4L357 738.1l34.8 40.2 4.4-5.4-18.5-26.2 133.9-12 6.4 7.7z"
		/>
		<path
			fill="#b3b3b3"
			d="m427.6 814.2-22.9-8.7 100.1 133.9 7.6-13.1-44.6-48.9-40.2-63.2zm-50-65.3L358.1 738l32.6 38 5.4-2.2-18.5-24.9z"
		/>
		<path fill="#bfbfbf" d="m469 877.3 152.3-14.2-84.8 88.1-67.5-73.9z" />
		<linearGradient
			id="ch-full89500b"
			x1={586.5149}
			x2={522.34}
			y1={1581.0824}
			y2={1640.9266}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#666" />
			<stop offset={1} stopColor="#000" />
		</linearGradient>
		<path fill="url(#ch-full89500b)" d="m537.5 936.1 57.7-59.8-102.3 9.7 44.6 50.1z" opacity={0.702} />
		<path fill="#b3b3b3" d="m349.3 709.8-14.2-20.7-38-26.2 24 37 29.3 26.2L345 712l4.3-2.2z" />
		<radialGradient
			id="ch-full89500c"
			cx={339.1265}
			cy={2107.0032}
			r={721.5533}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.2} stopColor="#000" stopOpacity={0} />
			<stop offset={1} stopColor="#000004" />
		</radialGradient>
		<path
			fill="url(#ch-full89500c)"
			d="M408.1 810.9c-19.5 0-193.8 4.7-218.7-37 0-40.2 2.4-73.3 10.2-100.6 37.1-115.2 125.8-124.5 166.1-124.5 35.1 0 69 7.4 96 26.6 48.8 34.6 82.3 83 82.3 113.8 0 27.4-19 54.3-21.8 57.7-2.4-2.1-3.3-5.4-3.3-5.4l9.8-17.4-32.6-25.1-14.2 1.1-18.5-20.7 14.2-32.6-7.6-7.6s-53.6 13.1-85.9 13.1-89.1-.9-94.6-2.2c6.7 12.2 32.6 50 32.6 50l28.2 26.2 7.6 13.1 35.9 41.3 8.7 21.8c.1-.3 2.2 2.3 5.6 8.4z"
			opacity={0.278}
		/>
		<linearGradient
			id="ch-full89500d"
			x1={442.0292}
			x2={297.2282}
			y1={1775.0903}
			y2={1775.0903}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000004" />
			<stop offset={1} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full89500d)"
			d="m408.1 812-5.4-9.8-10.9-25.1-33.7-37-6.5-15.3-29.3-26.2-35-48.8-14.2 20.7s-10.7 52.3-9.8 81.6 2.2 52.2 2.2 52.2l142.6 7.7z"
			opacity={0.4}
		/>
		<radialGradient
			id="ch-full89500e"
			cx={1138.0441}
			cy={2195.9434}
			r={721.5641}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.302} stopColor="#f3f3f3" />
			<stop offset={1} stopColor="#000" />
		</radialGradient>
		<path
			fill="url(#ch-full89500e)"
			d="m1227.3 732.6-47.9-27.1-93.5-112.1h102.3c21.8 0 34.5-5.4 42.4-12 4.9 2.5 18.5 12 18.5 12l2.2 26.2s9.3 4.2 15.3 6.5c-7.7-21.4-40.6-121.9-159.9-121.9-25.1 0-115.8 2.5-157.7 131.7 9.3 7.4 35 24.8 54.4 47.9 21 11 75.7 60.9 144.7 60.9 17-.1 69.3 1.4 79.2-12.1z"
			opacity={0.369}
		/>
		<linearGradient
			id="ch-full89500f"
			x1={1228.4319}
			x2={1052.2487}
			y1={1837.5732}
			y2={1837.5732}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={0.805} stopColor="#000" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full89500f)"
			d="m1082.7 592.3-30.4 121.9s35.1 30.4 114.2 30.4c45.2 0 57.3-9.3 62-12-14.2-7.6-50-26.2-50-26.2l-95.8-114.1z"
			opacity={0.4}
		/>
		<path fill="#e6e6e6" d="m1250.2 619.4-1.1-25.1-149 9.8 45.7 22.9 104.4-7.6z" />
		<path fill="#e6e6e6" d="m335.1 688 128.4-9.8 12-28.2-175.2 13 34.8 25z" />
		<linearGradient
			id="ch-full89500g"
			x1={542.9638}
			x2={1125.8363}
			y1={1371.9847}
			y2={2071.3228}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#f2f2f2" />
			<stop offset={0.469} stopColor="#d9d9d9" />
			<stop offset={0.549} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full89500g)"
			d="M563.5 922.9c296.6 51.8 294.1 179.5 90.9 229.6 66.1 45.9 119.6 82.5 142 101.9 50.1-32.1 35.2-50.6 96.5-78.6 49.9-12.7 337.1-55.2 404.2-62.8-38.9-22.5-80.9-49.9-136.3-90.7-52.1 16.7-211.9-86.9-310.1-155.9-57-56.1-85.1-102.1-92.5-139.8-9.7-48.5 14.6-83.3 55.3-109-8.6-7.4-14-15.4-23.3-24.3-28.5.7-239.6 10.4-265.1-38.6-7.5 3.7-22.5 15.1-50.6 29.9 46.2 42.4 67.6 73.1 68.8 100.9.9 20.8-5.9 41.1-18.6 60.9 3.3 4.5 6.6 10.2 10.9 15.3 4.8-.3 8.5-.9 15.3-1.1 11 8.4 25.9 17.8 39.1 26.2-4.8 11.2-12 22.9-12 22.9l42.4 55.5c-.2.1-34.6 35.5-56.9 57.7zm-78.4-10c-47-5.3-99.7-8.9-158.3-10.6-127.8-7.6-202.2-9.5-230.3-36.9-29.3-36.9 6.5-118 96.1-164.4-3.5 14.9-3.8 56.9-3.3 72.9 27.7 38 140.4 36.2 219.8 37 4.5 8.5 26.1 39.5 76 102z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full89500h"
			x1={1304.9146}
			x2={1475.2015}
			y1={1284.6014}
			y2={1527.7972}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.23} stopColor="#f2f2f2" />
			<stop offset={0.469} stopColor="#d9d9d9" stopOpacity={0.8} />
			<stop offset={0.844} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full89500h)"
			d="m1158.4 1021.2 118.3 95.4s154.2-19.3 190.6-15.7c47.8.4 220.4 67.6 282.4 35.1-36.6-35.2-132.8-112.3-132.8-112.3s-289.4 29.6-472.9-122c5.5 19.4 55.5 117 14.4 119.5z"
		/>
		<linearGradient
			id="ch-full89500i"
			x1={849.6938}
			x2={909.9711}
			y1={1857.4629}
			y2={1943.547}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.389} stopColor="#f2f2f2" />
			<stop offset={0.55} stopColor="#d9d9d9" />
			<stop offset={0.844} stopColor="#f2f2f2" />
		</linearGradient>
		<path
			fill="url(#ch-full89500i)"
			d="M969.3 585.9c-22-4.6-37.8-7.9-37.8-7.9l-54.3-27.7s-7.1 29.2-78.5 42.2c10.3 14.6 16.8 25.4 16.8 25.4s61.7-30.2 133 18.5c3.6-9.4 12.6-37.4 20.8-50.5z"
			opacity={0.702}
		/>
		<path
			d="M587.3 789.8c86.3-27.5 151.5-57.6 169.1-62 41.5 59.6 118.3 161.3 167.7 188.5-95.4 0-238.8 2.2-352.1-.5 12.9-14.2 48.1-50.2 48.1-50.2l-41.3-54.4c.1-.3 2.3-9.8 8.5-21.4zm-165.8 39.8c-8.9 1.3-17.8 2.5-26.8 3.6-167.4 19-319.7 29.9-299.4-37.8-6.2 8.8-14.7 45.9-4.8 61.5 6.4 42.8 291.3 44.4 384.9 54.4 3.3.2 6.5.4 9.9.7-19-23.6-49.7-62.8-63.8-82.4zm612.7-109.1c11.2.5 22.5 1.1 33.7 1.8 21.5 16.3 56.1 20.7 84.3 22.3 26.5 1.5 67.8-1.2 78.4-13.5 2.7 5.3 5.4 12.4 5.4 12.4l51.1 38 9.8-14.2s16.9 10.3 29.1 17.4c-74.1 22.2-166.8 45.5-200 81.6-17.4-32.3-39.6-67.9-45.7-79.5-6-11.3-27.6-43.2-46.1-66.3z"
			className="factionColorSecondary"
		/>
		<path
			d="M393.3 1192.7c42.1 20.1 176.4 55.6 252.2 55.6 118.7 0 163.7-100.6 193-113.5 47.7-21 86.2-28.6 190.6-45.9 104.4-17.3 252.8-23.5 354.7-31.3 102-7.8 146.3 3.3 235.2 24.2 50.8 12 67.5 9.6 86.9 9.6 19.3 0 49.5-22.4 64.8-81.8.1-.5 7.7 1.9 7.8 1.4 6.6 1.3 18.7-.1 24.8 28 1 4.5-1.5 9.6-1.5 15.2-3 14.6-7.9 31.1-14.6 48-17.3 23.8-45.6 42.5-76.4 42.5-91.6 0-211.8-44.7-247.3-44.7-14.6 0-102.3 6.3-206 18.5-148.3 17.4-329.5 44.7-376.7 64.8-66.3 28-78 124.4-246.1 124.4-143.8 0-321.2-76.9-319.8-93 1.3-13.8 64.4-20.8 78.4-22z"
			className="factionColorPrimary"
		/>
		<linearGradient
			id="ch-full89500j"
			x1={1071.5879}
			x2={1200.2747}
			y1={1502.1437}
			y2={1630.8306}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#9a9a9a" />
		</linearGradient>
		<path
			fill="url(#ch-full89500j)"
			d="M1349.7 810.1c-24.5 18.4-45.7 34.3-50.1 40.8-22.3 0-117.1 18.4-142.4 62.8-18.8-15.2-19-14.8-30.1-48.4 13.7-24.8 114.7-52.9 198.2-80.5 3.6 2.1 10 6.2 10 6.2l6.5 13.1c-.2.3 3.8 3.4 7.9 6z"
		/>
		<linearGradient
			id="ch-full89500k"
			x1={961.7883}
			x2={316.2621}
			y1={1534.4045}
			y2={1568.235}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#3e3e3e" />
			<stop offset={1} stopColor="#989898" />
		</linearGradient>
		<path
			fill="url(#ch-full89500k)"
			d="M94.2 864.2c24.6 29.8 223.2 39.1 282.4 41 21.1.7 61.4 2.4 108.9 7.1 4.8 5.3 15.7 19.5 20.6 24.7.8-1.1 4.3-5.8 7.6-8.7 4.4 4 16.2 16.3 22.9 21.8 6.8-5.7 20.1-19.7 28.6-27.1 65.5 10.9 132.1 28.6 174.6 57.3-17.8-6.4-96-32.6-196.7-32.6s-186.5 11-277.5 49.5c-34.6-19.3-133-71.3-171.4-133z"
		/>
		<path
			fill="#bfbfbf"
			d="m232.9 1090.1 6-9.7h47s29.3-24.1 154.5-45.9c-7.4-16.9-14.5-33.9-14.5-33.9l28.8-3.8 27.8 31.5s33.1-4.2 73.5 1.2c42 2.1 95.8 7 111 14.5 18.5-3.1 66.4-12.1 66.4-12.1l21.5 10.6.3 9.3-38.7 4.3s45.2 22.1 49.5 37.5c43-31.2 41.8-131.9-228.1-136.4-180.5-1.3-349.8 62.2-305 132.9zm984.5-139c19.5-14.7 84-26.9 112.2-29.2-15.7-12.5-27.7-19.5-27.7-19.5v-9.8l30.1.5 47.2 32.6s4.9-13.3 49.5-13.3c36.1 0 43.4 13.3 43.4 13.3l42.2 7.3 65.6-18.8 28.5 9.1v7.3l-35.1 14.5s80.8 28.5 84.4 49.5c83.3-34.6-10.8-135-226.9-137.7-79.8-1-232.8-3.7-261.8 62.8 16 14 33.3 22.5 48.4 31.4zM534.6 550.2c0-14.5 34.1-41 158-41 52.6 0 173.8 10.7 173.8 37.5-.2 15.8-42.3 33-76.1 35.1-9.6-2.1-43.3-10.8-51.9-13.3 15.1-3.4 68.3-11.2 92.9-14.5-5.3-2.6-7.5-4.9-12.1-5.9-29.2 3.4-76.8 10-90.6 12.1-.4-6.4-.1-8.8 0-18.1-3.5-9-17.6-9.7-30.1-9.7-13.7 0-29.8 10.2-35.1-2.4-9.9-.1-18.6 1.2-24.2 3.6 5.5 3.6 19.8 15.7 28.9 28.9-41.5.7-104 3.7-121.9 1.2-7-1.3-11.6-8.3-11.6-13.5zm-292 553.1h202.7v-9.7l16.8-1.2 1.2 10.9h-14.4l-3.6 12.1-185.9 1.2s-11.9-4-16.8-13.3zm232.8 58.1 42.2-38.6h5.9v12.1l-35.1 27.7-13-1.2zm33.8-54.4v13.3l-14.5-2.4 14.5-10.9zm37.4-6c1.3 0 135.2 31.4 135.2 31.4l-19.2 6-115.8-27.7c-.2 0-1.6-9.7-.2-9.7zm18.1-19.4 190.4-38.9v9.8l-190.4 38.9v-9.8zm-102.5 41.2v-72.4s4.1 13.3 51.9 13.3 50.7-18.1 50.7-18.1v47.2l-18 8.5v10.9l18 3.6v25.4s-12.1 17.5-38.6 20.6c-23.2 1-35.1 1.2-35.1 1.2l32.5-28.9v-9.7l-28.9-6 9.7-8.5-23-4.8-19.2 17.7zm839.7-230.2 78.5 50.1v12.1l-78.5-53.6v-8.6zm-14.4 86.4h105v10.9H1317m112.9-3.7v10.9l-9.7 19.3-12.1-3.6 21.8-26.6zm44.6-26.5 14.5 9.7 119.5-45.9v9.7L1494 976.6l-19.2-3.6-.3-13.3zm-13.3 25.3 19.3 1.2-9.7 10.9 108.6 18.1-20.4 5.9-84.4-9.7v12.1s-44.7-3.6-55.5-8.5c4.8-7.9 12.1-18.1 12.1-18.1v-13.3h-21.8l-4.8 8.5-10.9-1.2-1.2-9.7-13.3-13.3v-39.8s11.3 14.5 48.2 14.5 44.1-4.6 47.2-8.5c-.3 13.2 0 36.3 0 36.3l-13.4 14.6zM596 574.3c.9-1.1 65.2-1.2 65.2-1.2s2.3-5.1 2.4-3.6c.1 1.2 9.9-1.3 9.7 1.2-.1.9 1.4-28.4 1.2-26.6 0 .1.2.2.7.2 17.8 8.2 39.9 2.7 53.6-.2-.1 1.5-.8 16.8-1.2 28.9-1.2 1.1 38.2 8.6 35.1 12.1-2 2.1-46.4.9-48.4 1.2-1.1.2-.3-8.8-1.4-8.7-5.1.7-10.2.9-14.3.2-2.3-.3.9-4.6-1.2-4.8-4.2-.5-6.3-.3-8.5 0-3.1.3 1 10.8-1.6 10.9-8.7.3-15.2-.1-15.3 0-3.6.5-9.7 0-9.7 0s-62.2-4.4-66.3-9.6z"
		/>
		<linearGradient
			id="ch-full89500l"
			x1={661.2405}
			x2={635.8885}
			y1={1954.0212}
			y2={1954.0212}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#4f4f4f" />
			<stop offset={0.98} stopColor="#bfbfbf" />
		</linearGradient>
		<path fill="url(#ch-full89500l)" d="M635.9 540.5v23l25.4-1.2-25.4-21.8z" />
		<linearGradient
			id="ch-full89500m"
			x1={834.9597}
			x2={762.53}
			y1={1934.0696}
			y2={1934.0696}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.02} stopColor="#bfbfbf" />
			<stop offset={1} stopColor="#4f4f4f" />
		</linearGradient>
		<path fill="url(#ch-full89500m)" d="M835 561v9.7s-39 12.4-50.7 12.1c-9-3-21.8-10.9-21.8-10.9L835 561z" />
		<linearGradient
			id="ch-full89500n"
			x1={435.6457}
			x2={416.3399}
			y1={1488.2961}
			y2={1487.6215}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full89500n)" d="m421.2 999.5 19.3 33.9-19.3 3.6v-37.5z" />
		<linearGradient
			id="ch-full89500o"
			x1={766.8487}
			x2={722.2039}
			y1={1440.3798}
			y2={1438.8206}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={0.803} stopColor="#363636" />
		</linearGradient>
		<path fill="url(#ch-full89500o)" d="m715.5 1056.3 44.6 27.7v-36.3l-44.6 8.6z" />
		<linearGradient
			id="ch-full89500p"
			x1={561.2492}
			x2={539.5311}
			y1={1372.9493}
			y2={1372.191}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full89500p)"
			d="M542.9 1111.9s0 24.4 1.2 43.5c8.2-.2 20.4-15.7 20.4-15.7v-25.4l-21.6-2.4z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full89500q"
			x1={533.6312}
			x2={499.8463}
			y1={1359.3302}
			y2={1358.1508}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#363636" />
		</linearGradient>
		<path
			fill="url(#ch-full89500q)"
			d="M492.3 1162.6c8.2-.1 33.7-2.4 33.7-2.4v-27.7c.1-.1-15.4 9.7-33.7 30.1z"
			opacity={0.702}
		/>
		<linearGradient
			id="ch-full89500r"
			x1={1438.7203}
			x2={1423.0359}
			y1={1499.4604}
			y2={1498.9131}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#363636" />
		</linearGradient>
		<path fill="url(#ch-full89500r)" d="M1433.4 995.8v21.8l-15.7-2.4 15.7-19.4z" />
		<linearGradient
			id="ch-full89500s"
			x1={1473.9575}
			x2={1455.8584}
			y1={1499.4528}
			y2={1493.0648}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#363636" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full89500s)" d="M1457.6 994.6v26.6l18 2.4v-12.1l-8.5-1.2v-14.5l-9.5-1.2z" />
		<linearGradient
			id="ch-full89500t"
			x1={1328.2657}
			x2={1294.481}
			y1={1600.7273}
			y2={1588.8031}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#141414" />
			<stop offset={1} stopColor="#bfbfbf" stopOpacity={0} />
		</linearGradient>
		<path fill="url(#ch-full89500t)" d="M1299.5 896.8v28.9s22.1-4.7 33.7-3.6c-6.5-7.1-33.7-25.3-33.7-25.3z" />
		<linearGradient
			id="ch-full89500u"
			x1={1621.0492}
			x2={1586.0577}
			y1={1562.8043}
			y2={1550.4546}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bfbfbf" stopOpacity={0} />
			<stop offset={1} stopColor="#3b3b3b" />
		</linearGradient>
		<path fill="url(#ch-full89500u)" d="m1612 931.9-35.1 13.3s23.3 14.7 35.1 15.7c-.3-10.8 0-29 0-29z" />
		<path
			fill="#bfbfbf"
			d="M313.7 1215.7c11 25.6 202.5 91.9 325.8 91.9 159.6 0 190.9-105 242.6-125.6 61.9-24.8 240.1-44 333-58 92.8-14.1 231.4-28.4 269.1-20.6 37.8 7.8 156.6 41 222 41 41.2 0 68.4-26.4 82.8-46-2 5.9-6.8 20.2-23.1 40.2-7.9 7.1-17.5 14-29.1 20.1-17.7 9.3-38.2 19.8-67.2 23.3-11.7 1.4-24.4 2.2-38.2 2.2-109.2 0-95.5-19.3-203.9-19.3s-447 49.8-532.1 74.8c-28.4 5.8-47.8 13.8-62 23.9-40.2 28.7-49.4 64.2-193.8 72.7-67.1 4.3-258.9-35-313.6-84.3-6.8-6.2-15.4-11.9-17.5-18.4-1.8-5.8-.4-16.1 5.2-17.9z"
		/>
		<linearGradient
			id="ch-full89500v"
			x1={624.2492}
			x2={314.8731}
			y1={1353.7014}
			y2={1353.7014}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#000" />
			<stop offset={1} stopColor="#bebebe" />
		</linearGradient>
		<path
			fill="url(#ch-full89500v)"
			d="M208.7 1073.2c4 49.6 97.3 110.9 309 97.8-63.4 26.3-218.4 10.3-207.5 60.3-53.6-12.4-97.5-56.1-98.9-76.1-1.5-19.9-4-63.3-2.6-82z"
		/>
		<linearGradient
			id="ch-full89500w"
			x1={1678.3322}
			x2={1599.5581}
			y1={1511.3101}
			y2={1511.3101}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#bebebe" />
			<stop offset={1} stopColor="#4f4f4f" />
		</linearGradient>
		<path
			fill="url(#ch-full89500w)"
			d="M1627.6 1021.2c19.2-5.2 68-25.6 77.3-53.1 1 16.4 1.5 45.7 1.5 45.7s-45.9 4.8-78.8 7.4z"
		/>
		<linearGradient
			id="ch-full89500x"
			x1={687.3206}
			x2={1105.4301}
			y1={1477.2003}
			y2={1769.9647}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#0e2533" />
			<stop offset={0.801} stopColor="#dae3f2" />
		</linearGradient>
		<path
			fill="url(#ch-full89500x)"
			d="M756.3 714.3c-4.1-34.1 23.9-95.4 81.9-104.7 74.1-17.4 145.1 51.4 153.7 60.2 59.1 59.9 133.1 190.1 142.4 212.6 17.1 48.1 73.7 142 10.6 142-51.5-5.2-122.2-49.3-184.3-84-63.6-39.9-122.5-81.1-138.2-103.5-45-60.9-61.4-83-66.1-122.6z"
		/>
		<linearGradient
			id="ch-full89500y"
			x1={865.2621}
			x2={1037.2281}
			y1={1416.2018}
			y2={1833.837}
			gradientTransform="matrix(1 0 0 -1 0 2506)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0} stopColor="#092130" />
			<stop offset={0.699} stopColor="#dae3f2" stopOpacity={0} />
		</linearGradient>
		<path
			fill="url(#ch-full89500y)"
			d="M756.3 714.3c-4.1-34.1 23.9-95.4 81.9-104.7 74.1-17.4 145.1 51.4 153.7 60.2 59.1 59.9 133.1 190.1 142.4 212.6 17.1 48.1 73.7 142 10.6 142-51.5-5.2-122.2-49.3-184.3-84-63.6-39.9-122.5-81.1-138.2-103.5-45-60.9-61.4-83-66.1-122.6z"
		/>
		<radialGradient
			id="ch-full89500z"
			cx={954.0502}
			cy={2434.7446}
			r={646.0858}
			gradientTransform="matrix(0.5721 0.8202 -0.3383 0.236 1223.5066 -476.0085)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.4121} stopColor="#fff" stopOpacity={0} />
			<stop offset={0.5146} stopColor="#fff" stopOpacity={0.3} />
			<stop offset={0.6434} stopColor="#fff" stopOpacity={0} />
		</radialGradient>
		<path
			fill="url(#ch-full89500z)"
			d="M756.3 714.3c-4.1-34.1 23.9-95.4 81.9-104.7 74.1-17.4 145.1 51.4 153.7 60.2 59.1 59.9 133.1 190.1 142.4 212.6 17.1 48.1 73.7 142 10.6 142-51.5-5.2-122.2-49.3-184.3-84-63.6-39.9-122.5-81.1-138.2-103.5-45-60.9-61.4-83-66.1-122.6z"
		/>
		<path
			fill="#999"
			d="M1002.7 756.7c11.9 11.9 64.6 80.7 92.9 137.7-24.7 20.8-133.6-44.4-137.5-68.9-12.7-37.8 32.7-80.7 44.6-68.8zm18.1-4.7c13.4.3 38.9 7.9 48.2 18 9.3 10.1 55.2 92.5 59.1 105.1 4 12.5-18 19.3-18 19.3s-45.3-84-89.3-142.4z"
		/>
		<radialGradient
			id="ch-full89500A"
			cx={935.8629}
			cy={-7398.6118}
			r={431.5287}
			gradientTransform="matrix(0.5233 0.8522 0.21 -0.129 2063.0435 -900.9443)"
			gradientUnits="userSpaceOnUse">
			<stop offset={0.1192} stopColor="#535353" />
			<stop offset={0.2091} stopColor="#757575" />
			<stop offset={0.3133} stopColor="#999" stopOpacity={0.5} />
			<stop offset={0.4001} stopColor="#fff" stopOpacity={0.4} />
			<stop offset={0.4924} stopColor="#a6a6a6" />
			<stop offset={0.8179} stopColor="#666" />
			<stop offset={0.9703} stopColor="#333" />
		</radialGradient>
		<path
			fill="url(#ch-full89500A)"
			d="M1001.5 753.7c11 10.7 57.6 65.8 93.2 140.7-16.5 8.9-82.5-5.2-125.5-53.9-4.4-4.9-7.6-11.9-11.3-17.5-4.7-54.9 24.8-62.1 43.6-69.3zm72.1 21.8c9.2 17.9 21.3 31.5 53.3 94.9-2.3 4.2 7.6 18-17.8 22.4-26.8-48.5-34.1-64.1-48-84.5-17.1-25.2-26.3-37.7-41.2-56.9 22.8-.7 43.3 13.9 53.7 24.1z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1709.6 1013.4c-19.1 2.3-89.1 12.7-141 12.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M89.1 854.5c28 51.3 215.2 39.7 395.7 57.9m79.7 10.5c90 15.1 171.2 41.5 221 92.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M334 1202.5c38.3-12.2 126-16.5 282.5-43.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M313.7 1215.7c20.6 33.3 270.6 110.7 376.4 88.1 135.6-28.7 137.1-108.5 205.1-128s498.2-80.9 570.6-74.8c72.4 5.9 187.8 56.1 264.2 41 28.9-5.7 47.8-26.6 59.3-44.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1125.8 866.6c21.2-30.1 119.4-56.6 199.3-81.4" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M759.5 680.3c2.7 16.5 27.6 84.8 69.7 125.1 2.2 11 8.7 47.9 8.7 47.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1174 999.1c-22.2 11.5-58 14.9-161-51.1-12.9 6.4-18.5 9.8-18.5 9.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M979.7 660.1c95 86.2 194.2 288.4 194.2 338.2s-87.5 11.7-111-2.4-178.2-94-226.9-143.8c-48.8-49.7-80.9-110.9-80.9-143.8S781 606.8 865 606.8c39.8.2 84.3 25.7 114.7 53.3z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1001.5 753.3s61.2 73.3 94.1 142.4c-39.9 14.8-108.5-37.9-127.9-55.6-28.1-25.6-2.6-83.7 33.8-86.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1127 870.3c4.1 14.4-5.2 21.9-18 24.2-18.8-41.1-73.2-123.1-91.8-143.8 15.6 2 38.2 4.5 54.3 23"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M705.8 507.9c26-.2 67.2 3 101.2 10.1 32.2 6.8 58.3 17.3 59.4 27.3 2 18.6-46.4 42.1-167.7 39.8-82.1-1.5-165.3-15.6-165.3-37.5-.1-19.5 71.7-38.7 172.4-39.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M522.5 547.9c0 21.9 43.5 47.2 216 47.2 110.2 0 136.3-34.4 136.3-44.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M727.1 540.8c0 9.6-54.7 10.8-54.7 0s54.7-11 54.7 0z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M707.1 540.5c0 1-13.5 1.1-13.5 0 0-1 13.5-1.1 13.5 0z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M672.1 540.5V584" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M726.4 540.5v30.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m672.1 539.3-9.7-9.7-20.4 3.6s23 24 27.9 29.1c.7.7-.2 1-.2 1l-9.7-1.2-20.6-21.8v-8.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m669.8 562.3-119.5 1.2s5.9 4.2 23 8.5c16.9.7 88.1 1.2 88.1 1.2l1.2-3.6h9.7"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M661.2 573.1v12.1" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m686.6 582.8 1.2-9.7h8.5l1.2 3.6 15.7 1.2v5.9" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m727.6 565.9 10.9 2.4 7.3 1.2s36.7 8.9 44.6 12.1c-5.2 1.2-18 1.2-18 1.2s-43-9.2-49.7-10.7c-.5-.1.3 1 .3 1l4.8 2.4v8.5"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m727.6 559.9 91.8-10.9s10.8 3.6 13.7 4.6c.4.1-.4.2-.4.2s-80 12-94.3 14.2c-1.4.2-1 .3-1 .3l16.8 4.8 79.6-13.2v-4.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1166.8 922.2c29.2-66.4 177.4-69.4 282.4-65.3 107.8 4.2 238.9 47.8 238.9 102.8 0 47-149.9 66.2-176.2 65.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1215 950c18.2-11 62.4-24.8 114.1-28.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1470.9 924.6c17.3 1.6 36.7 6.2 43.2 8.4" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1573.9 946.5c25.8 7.4 72.9 26.7 84 44.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1379.2 942.9s-66.6-42.5-76.2-49.3c-.8-.5.1-.8.1-.8l28.9.5 47.3 33.9-.1 15.7z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1301.9 892.6v9.7s69.9 47 76.7 52.1c.3.2.5 14.9.5 14.9v-29.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1380.3 925.8c0-16.9 92.9-19 92.9 4.8.1 19.5-92.9 15.9-92.9-4.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1426.9 922.2c14.4.3 25.9 3.1 25.8 6-.1 3.1-11.8 5.2-26 4.8-14.4-.3-25.9-3.1-25.8-6 0-3 11.7-5.1 26-4.8z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1261.6 970.6 98.1-4.8 4.8 5.9h15.7l8.5 7.3-102.7 1.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1371.9 1005.5 8.5-16.8 24.2 2.4 4.8-7.3h19.2l-20.4 28.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1473.3 945.2 107.9-31.8 24.7 10-117.2 45.9-15.7-9.7.3-14.4z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1583.8 1016.1c-25.6-4.6-114.1-17.8-114.1-17.8l9.7-10.9-16.8-3.6 9.5-10.9 154.4 32.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1609.6 923.4v8.5l-113.3 44.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1460 983.8v9.7s8.1 1.5 12.1 2.4c.4.3-4.8 2.4-4.8 2.4v12.1l86.3 10.9"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1429.9 982.7V996l-12.1 19.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1473.3 926.3v45.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1473.3 1010.4v10.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1316.9 989.9c-.9.1 75.5 0 75.5 0v-9.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1300.8 851c-52.6 4.7-118.4 20.7-143 60.7-.2.3-12.4-9-12.6-8.7.2.2 12.5 10 12.7 10.2 70.7 56.3 227.8 113.1 391.5 113.1 131.7 0 156.7-51.1 156.5-69.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M541.9 946.7c98.3 3 255.5 25.1 255.5 103.4 0 76.9-173.8 120.8-314.9 120.8-293 4.2-321.2-117.3-218.4-172.7 60.9-29.1 160.5-51.9 254.7-51.9 3.4 0 6.9 0 10.6.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M224.5 1070.7c0-70.7 162.7-114.7 305.3-114.7 150.1 0 258.2 40.7 258.2 99 0 56.8-174.4 106.3-277.4 106.3s-286.1-19.6-286.1-90.6z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M441.7 1033.5c-71.7 11-131.7 31.9-161.7 46.9" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M543.5 1028.6c43.4 1.3 103 7.6 128.3 14.7" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M721.1 1058.8c16.4 8 42.1 23 42.8 32.5" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M463.1 1047.2c0 13.1-2.9 28.7-2.9 31.4m1.1-4.7c-5.6-11.3-36.6-73.1-36.6-73.1l28.8-4.1 29 34.2"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M460.9 1103.3v19.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M460.9 1080.5H237.8l-5.9 9.7s-.7 5.6 8.5 12.1c11.8.7 205.1 1.2 205.1 1.2l1.2-9.7 14.5-1.2-.3-12.1z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m563.5 1064.8 168.9-33.6 24.2 11.3s.4.2-1.2.5c-22.1 5.1-191.9 38.6-191.9 38.6v-16.8z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m558.6 1096.2 13.3 2.4 15.7-6 115.8 31.4s-14.9 7-20.4 8.5c-13.5-3.7-122.7-28-136.2-31.1-.9-.2-.1-.3-.1-.3l11.9-4.9z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m427.2 1157.7 54.3-53.1 23 4.8-9.7 8.5 25.4 4.8-45.8 38.6"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M523.7 1120.3v13.3l-32.5 26.6" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M461.1 1093.7v8.5h-14.6v12.1H254.6s-12.4-8.6-14.5-10.9h205.1l1.2-9.7h14.7z"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M545.4 1101v9.7l118.3 26.6" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M563.5 1084.1v7.3l193-40.4v-7.3" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M511.6 1161.4c15.2 0 48-7.6 53.1-23v-23" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M563.5 1069.6v-27.7" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M514 1027.3c-22.2 0-50.7 3.2-50.7 20.6 0 12.4 28.2 14.5 51.9 14.5 23.5 0 48.4-5.9 48.4-20.6-.1-14.6-31.2-14.5-49.6-14.5z"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M512.8 1038.4c14.4 0 25.9 3.2 25.9 6.9s-11.7 6.9-25.9 6.9c-14.4 0-25.9-3.2-25.9-6.9s11.5-6.9 25.9-6.9z"
		/>
		<path fill="none" d="M-430.2-335.9c.3 0 .5.2.5.5s-.2.5-.5.5-.5-.2-.5-.5.2-.5.5-.5z" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1349.1 810.1c3.9 3 45.7 32.1 45.7 32.1l9.8-13.1 55.5 20.7 95.7-78.4-87-48.9 8.7-13.1-65.3-24-17.4 2.2-40.3-19.5 7.6-14.2-56.6-19.6-18.5 2.2s-14.6-8.5-19.6-10.9c-6-19.1-24.9-63-64.2-92.4-39.2-29.5-84-28.2-93.5-28.2-9.6 0-59-7-107.7 40.2-24 25.8-26.7 32.4-31.4 40.1-22.1-4.4-38-7.6-38-7.6L875.8 549v-4.8c0-7.3-19-44.7-179.7-44.7s-172.6 45.2-172.6 48.4v8.5s-24 14.6-49.9 28.4c-11.1-8.5-43.1-34.8-109.2-34.8-39.1 0-64 9.9-76.1 15.3-76.1 35.8-93.1 123.3-95.7 136-8 4.4-19.6 11.2-29.7 19.5-38 28.4-78.3 60.4-76.1 124.4 2 43.3 136.6 135.4 180.9 152.2-45.6 23.4-61.4 57-60.2 76.1 1.2 19 3.6 73.6 3.6 73.6s-5.4 49.4 95.7 83.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M951 630.3c18.2 15.6 56.6 34.6 97.9 41.3 3.8-36.7 13-56.9 15.3-68.6-18.8-4.6-68.4-12.9-85.9-30.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1353.6 813.1-13.1-8.7-6.5-13.1s-38.9-25.1-38-24c.9 1.1-9.8 15.3-9.8 15.3l-48.9-38-6.5-10.9s-23.7 10.9-56.6 10.9-66.4-1.5-92.4-14.2c-32.5-15.7-40.2-21.2-81.6-50m-51.4-43.6c1-7.7 16.2-44.4 21.8-53.3"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1464.5 833.7-15.3-42.4 79.5-8.7-64.2 51.1z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m1552.7 771.7-117.5 10.9 24 65.3" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1473.3 724.9-10.9-5.4-118.6 10.9 56.6 103.3m-7.7 6.6-64.2-118.6 146.8-12m-138 88.2-53.4-101.2 112.1-9.8m-41.3-22.9-115.3 8.7 47.8 89.2 14.2 8.7m-15.3 10.9-60.9-113.2 133.9-14.2m-67.6-18.3-108.8 8.7 47.9 88.1m-39.1-19.7-44.6-87 104.4-7.6m-3.3-25.1-147.9 9.8"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M1085.9 594.3c4.3 5.8 93.5 111 93.5 111l52.2 28.2" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M1046.8 513.9c0 3.5 13.5 16.3 68.6 16.3s63.1-7.3 63.1-10.9"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m1437.4 784.8-90.3-52.2-19.6-9.8-89.2-48.9-16.3-6.5-77.3-40.2-45.7-24-14.2-9.8s32.3 1.1 81.6 1.1c49.2 0 57.3-9.8 63.1-13.1 9.6 4.2 19.6 12 19.6 12l1.1 25.1 25.1 12"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M289.5 565c0 3.3 6.9 14.2 75.1 14.2s68.6-15.7 68.6-17.4"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M516.9 630.3c2.2 10.8-6.8 19-22.5 25.4 6 15.1 12.3 35.2 17.5 54.2m16.7 12.9c5.4-3.1 9.2-6.3 11.1-9.8"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M190.5 723.8c8.1 7.5 21.8 19.8 72.9 27.1 1.4-23.4 3.8-50.1 10.9-79.5-25.1-4.2-60.7-12.3-65.3-26.2"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="M192.7 698.9c-1.9 10.7-5.4 51.2-3.3 75.1 15.4 15.6 32.4 37 219.8 37 24.1 32.6 96.8 127.3 96.8 127.3l6.5-12 24 25.1 84.8-87-43.5-54.4 12-22.9-40.2-26.2-13.1 1.1-13.1-15.3s20.7-29 20.7-60.9S494.2 597 469 581.4"
		/>
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m518 932.8-50-55.5" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="M490.7 885.9 596.2 875l-58.8 60.9-46.7-50z" />
		<path fill="none" stroke="#1a1a1a" strokeWidth={5} d="m620.2 863.2-153.4 14.2-41.3-64.2 144.7-10.9 18.5 21.8" />
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m412.4 816.4-9.8-13.1L589.8 788m-44.7-27.1L391.7 774l9.8 27.1"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m393.9 781.5-37-43.5-5.4-10.9-29.3-28.2-34.8-49s19.3 2.2 96.8 2.2c24.9 0 83.2-11.7 85.9-13.1 4.6 5.4 6.5 7.6 6.5 7.6l-14.2 31.5 19.6 21.8 13.1-1.1 33.7 25.1-10.9 17.4 8.7 8.7"
		/>
		<path
			fill="none"
			stroke="#1a1a1a"
			strokeWidth={5}
			d="m475.6 649.9-177.4 12 37 26.2 128.4-9.8m-127.4 10.8 14.2 22.9m132.7-12L345 710.9l8.7 19.6m4.3 6.4 169.7-13.1m-6.5 20.8-8.7-9.8-136 10.9 20.7 28.2"
		/>
		<path
			fill="none"
			stroke="#191919"
			strokeWidth={5}
			d="M1352.3 811.6c-31.5 19-60.1 36-61.5 39.8 373.1-28.4 415.9 86.9 415.2 98.7 1.4 9 0 40.6 0 63.8 7.2-1 56.1-5.5 72.2-3.6s42.4 13.3 7.2 97.5c-35.1 84.2-173.8 82.5-245.5 68.6-71.7-13.9-110.4-12.9-152.8-10.8-42.4 2-416.5 45.3-524.7 81.8-78.5 35.7-33.4 51.3-146.8 79.4-144.3 35.9-398-49.4-408-96.3 0 0-7.4-20.2 30.6-29"
		/>
	</svg>
);

export default Component;

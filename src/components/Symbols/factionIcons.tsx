import React from 'react';
import PoliticalFactions from '../../../definitions/world/factions';
import SVGIconContainer from '../UI/SVG/IconContainer';
import { TSVGSmallTexturedIconStyles } from '../UI/SVG/SmallTexturedIcon';

const factionIcons: EnumProxyWithDefault<PoliticalFactions.TFactionID, JSX.Element> = {
	default: (
		<symbol viewBox="0 0 50 50">
			<g style={{ transformOrigin: '50% 50%', transform: 'scale(0.85)' }}>
				<path d="M19.7,8.8c-7.4,3.2-12,1.8-12,1.8c3.5-4.3,8.4-7.2,13.8-8.3L19.7,8.8z" />
				<path
					d="M10.9,23.3c0,0,0-2.8,0-4.5C9.2,18.5,9,18.6,9,18.6s0.1-0.5,0.5-1.7c5.8,0.7,14.1-1.5,14.1-1.5s1.1,4.5,1.5,5.7
	c4.7,0.5,9.5-1.8,9.5-1.8C30.2,12,33.8,3.6,33.8,3.6c-0.7-0.3-1.4-0.5-2.3-0.7c-2.4-0.7-4.8-0.9-7.1-0.8L22,10.5
	c0,0-5.3,1.8-8.6,2.3c-3.3,0.6-7.6,0.1-7.6,0.1c-0.8,1.5-1.9,4-2.4,5.6c-0.4,1.4-0.7,2.8-0.7,4.1C2.8,22.8,7.8,24.2,10.9,23.3z"
				/>
				<path
					d="M23.3,47.3c-1.3-0.2-2.6-0.3-3.9-0.7C9.7,44,3.3,35.5,2.8,26c0,0,8.5,1.4,11.1-0.4c-0.2-2.4-0.4-5.8-0.4-5.8s6.9-0.7,8-1.2
	l1.2,5.7c0,0,5.2-0.2,7.2-0.9c0.6,1.9,1.6,5.5,1.6,5.5s-14.3,2.8-18.5,2.9c0,0,1.2,3.3,3.8,7.2S23.3,47.3,23.3,47.3z"
				/>
				<path
					d="M47.2,31.4c-3,9.3-11.4,15.7-20.8,16.1c0,0-2.1-2.1-4.8-6.4s-3.7-5.7-4.1-6.6c1.2-0.2,14.6-2.3,15.5-2.6l2.1,4.9
	c0,0,3.3-0.9,6.4-2.4C44.4,32.9,47.2,31.4,47.2,31.4z"
				/>
				<path d="M33.3,22.5c0,0,1.9,6.5,3.4,10.2c3-0.6,4.7-1.5,6.8-2.4c2.1-1,4-1.9,4.8-2.7c0.4-2.9,0.1-6.3-0.9-9.5L33.3,22.5z" />
				<path d="M36.5,4.8c4.2,2.3,7.4,5.8,9.4,9.9c0,0,1,1.6-8.9,3.9C33.6,10.2,36.5,4.8,36.5,4.8z" />
				<path d="M39.3,50l4.8-3l-2-3.5c0,0-1.5,1-3.1,2c-1.6,1-3.2,1.7-3.2,1.7l1.6,2.8L39.3,50z" />
				<path d="M11.4,0L6.6,3l2,3.5c0,0,1.4-1.1,3-2s3.3-1.7,3.3-1.7L13.3,0L11.4,0z" />
			</g>
		</symbol>
	),
	[PoliticalFactions.TFactionID.pirates]: (
		<symbol viewBox="0 0 50 50">
			<g style={{ transformOrigin: '50% 50%', transform: 'scale(.98) translate(0, 2px)' }}>
				<circle cx="24.9" cy="20.8" r="10.1" />
				<path
					d="M24.1,40v-6c0,0-2.1,0-4.6-1.1c-1.1-0.5-2.2-1.1-3.2-2.1c-1.7-1.5-3.1-3.3-4.3-6.9c0-0.1-0.2-0.9-0.2-1c-2.4,0-9.3,0-11.8,0
	c0,0,0,1,0,1L20,40C20,40,24,40,24.1,40z"
				/>
				<path
					d="M30,40l20-16.1c0,0,0-1,0-1c-2.5,0-9.4,0-11.8,0c0,0.1-0.2,0.9-0.2,1c-1.2,3.6-2.6,5.4-4.3,6.9c-1,1-2.1,1.6-3.2,2.1
	C28,34,25.9,34,25.9,34v6C26,40,30,40,30,40z"
				/>
				<path d="M31,41.9H18.8L0,26.9L9,47h8.1c1.9-2.3,4.7-3.7,7.8-3.7s6,1.5,7.8,3.7h8.2L50,26.9L31,41.9z" />
			</g>
		</symbol>
	),
	[PoliticalFactions.TFactionID.aquarians]: (
		<symbol viewBox="0 4.5 50 55">
			<path d="M30.8,8.3l-11.3,0L0.1,40l5.9,10h38.2L50,39.8L30.8,8.3z M39.8,44.4H10.5l-3.3-4.5l15.8-26l4.5,0l14.8,26L39.8,44.4z" />
			<polygon points="17.3,38.7 33,38.7 34.2,36.4 26.3,24.1 24,24.1 16.2,36.5 " />
		</symbol>
	),
	[PoliticalFactions.TFactionID.draconians]: (
		<symbol viewBox="0 0 50 50">
			<g style={{ transformOrigin: '50% 50%', transform: 'scale(0.85)' }}>
				<path d="M26,48c14.6-17,3.6-41.6,0-47c0,0-0.7-1-1-1s-1,1-1,1c-3.6,5.4-14.6,30,0,47c0,0,0.7,0.8,1,1C25.3,48.8,26,48,26,48z" />
				<path d="M23,50c-3.9-2.7-14.2-13.8-9-35c0,0-6,5.4-6,17.1s5.8,15.2,9,18C19.7,50,23,50,23,50z" />
				<path d="M27.1,50c3.9-2.7,14.2-13.8,9-35c0,0,6,5.4,6,17.1s-5.8,15.2-9,18C30.3,50,27.1,50,27.1,50z" />
			</g>
		</symbol>
	),
	[PoliticalFactions.TFactionID.stellarians]: (
		<symbol viewBox="0 0 50 50">
			<g style={{ transformOrigin: '50% 50%', transform: 'scale(0.85) translate(0, -3px)' }}>
				<path d="M17,2l0,34c0,0,1.2,3.9,7.8,3.9S33,36,33,36l0-34c0,0-1.5,3-8,3C18.4,5,17,2,17,2z" />
				<path
					d="M26,50c15.2-0.2,22-5.3,22-14c0-8.9-13-12-13-12l0,6c0,0,7,2.1,7,6.1c0,3.9-6.6,7.7-16,7.9h-2c-9.4-0.2-16-4-16-7.9
	c0-4,7-6.1,7-6.1l0-6c0,0-13,3.1-13,12c0,8.7,6.8,13.8,22,14H26z"
				/>
			</g>
		</symbol>
	),
	[PoliticalFactions.TFactionID.reticulans]: (
		<symbol viewBox="0 0 50 50">
			<g style={{ transformOrigin: '50% 50%', transform: 'scale(.92) translate(0, -1px)' }}>
				<path d="M12.3,39l-1.6,11h12.4c0,0,0-5,0.1-11H12.3z" />
				<path d="M37.8,39l1.6,11H26.9c0,0,0-5-0.1-11H37.8z" />
				<polygon points="9.7,40.1 8,50 7,50 4,42.8 " />
				<polygon points="2.6,39.4 0,33.2 12.3,25 10.5,36 " />
				<polygon points="46,42.8 43,50 42,50 40.3,40.1 " />
				<polygon points="39.5,36 37.7,25 50,33.2 47.4,39.4 " />
				<path
					d="M33.2,7H16.9l-4.1,28h24.3L33.2,7z M26,10l8.1,11h-4.5L26,16V10z M24.1,33L16,22h4.5l3.6,5V33z M24.1,16l-3.6,5H16l8.1-11
	V16z M26,33v-6l3.6-5h4.5L26,33z"
				/>
			</g>
		</symbol>
	),
	[PoliticalFactions.TFactionID.venaticians]: (
		<symbol viewBox="0 0 50 50">
			<g style={{ transformOrigin: '50% 50%', transform: 'scale(0.75) translate(0, 3px)' }}>
				<path d="M3,0v34l6,7h5V0C14,0,3,0,3,0z" />
				<path d="M47,0v34l-6,7h-5V0C36,0,47,0,47,0z" />
				<path d="M31,5l0,45l-12,0V5C19,5,31,5,31,5z" />
			</g>
		</symbol>
	),
	[PoliticalFactions.TFactionID.anders]: (
		<symbol viewBox="0 0 50 50">
			<g style={{ transformOrigin: '50% 50%', transform: 'scale(0.9) translate(0, 0)' }}>
				<path
					d="M29.8,41.7v-8.9c6.3-1.2,14.3-5.2,14.3-5.2s0-2.1,0-4.8c-2.5,2.7-14.3,5.3-14.3,5.3v-6.5c4.4-1.3,9.5-2.1,14.3-4.8
	c0-1.8,0-4.6,0-5.9c-1-1.9-2-3.3-3.1-4.6l-7.5,9.3H16.7L9.1,6.1C8,7.4,6.9,9,6,10.8c0,1.3,0,4.2,0,5.9c4.8,2.6,9.9,3.4,14.3,4.8V28
	c0,0-11.8-2.6-14.3-5.3c0,2.6,0,4.8,0,4.8s7.8,4.2,14.3,5.2v9H29.8z"
				/>
				<path d="M26.2,0c0.5,0,1.3,0,2.3,0c2.6,0,6.8,0.6,11,4.8l-7.4,8.8H17.8l-7.3-8.8C14.8,0.5,19,0,21.7,0c1,0,1.7,0,2.1,0H26.2z" />
				<path
					d="M21,50l-3.2-1.2c-5.1-1.9-7.1-7.1-7.1-7.1v-10l3.6,1.3l3.5,1.2c0,0,0,5.6,0,9.9l2.5,0h2.8H27h2.8l2.5,0c0-4.3,0-9.9,0-9.9
	l3.5-1.2l3.6-1.3v10c0,0-2,5.2-7.1,7.1L29,50H21z"
				/>
			</g>
		</symbol>
	),
};

const factionIconStyle: EnumProxy<PoliticalFactions.TFactionID, TSVGSmallTexturedIconStyles> = {
	[PoliticalFactions.TFactionID.pirates]: TSVGSmallTexturedIconStyles.dark,
	[PoliticalFactions.TFactionID.anders]: TSVGSmallTexturedIconStyles.gray,
	[PoliticalFactions.TFactionID.draconians]: TSVGSmallTexturedIconStyles.green,
	[PoliticalFactions.TFactionID.venaticians]: TSVGSmallTexturedIconStyles.light,
	[PoliticalFactions.TFactionID.aquarians]: TSVGSmallTexturedIconStyles.blue,
	[PoliticalFactions.TFactionID.reticulans]: TSVGSmallTexturedIconStyles.gold,
	[PoliticalFactions.TFactionID.stellarians]: TSVGSmallTexturedIconStyles.gray,
};

export const getFactionIconId = (t: keyof typeof factionIcons = null) => `__icon-${t || 'default'}`;
export const getFactionIconStyle = (t: PoliticalFactions.TFactionID) => {
	return factionIconStyle[t] || TSVGSmallTexturedIconStyles.dark;
};

Object.keys(factionIcons).forEach((iconId) =>
	SVGIconContainer.registerSymbol(getFactionIconId(iconId), factionIcons[iconId]),
);

import * as React from 'react';
import _ from 'underscore';
import { MaxQualityRequired, MedQualityRequired } from '../../contexts/GraphicSettings';
import { SVGRefractFilterId } from '../PrototypeEditor/PrototypeChassisImage';
import { getTemplateColor } from '../UI';
import SVGIconContainer, {
	SVGAnimatedNoiseFilterID,
	SVGBasicEmbossFilterID,
	SVGBasicNoiseFilterID,
} from '../UI/SVG/IconContainer';

export const UIBackdropPatternID = '__backdrop-pattern';
export const UIUnitBackdropPatternID = '__unit-pattern';
export const UITalentBackdropPatternID = '__talent-pattern';
export const UITalentBackdropShapeID = '__talent-pattern-shape';
export const UIPrototypeBackdropPatternID = '__prototype_pattern';
export const UIUnitBackgroundGradientID = '__unit-background-gradient';
export const UIPrototypeBackgroundGradientID = '__prototype-background-gradient';
export const UITalentBackgroundGradientID = '__talent-background-gradient';

export const getBackdropLines = (color = 'light_green') => {
	const w = 256;
	const h = w * Math.sin(Math.PI / 6);
	const gridDensity = 10;
	const mainThickness = 1.6;
	const totalLines = gridDensity * 16;
	const secondaryThickness = 0.4;
	const animCycle = 35.5;
	const animCycle2 = 45;
	const animSequence = <K extends string | number>(
		value: K,
		key: number,
		defaultValue: K,
		radix: number = gridDensity,
	): K[] => {
		const arr = new Array(radix).fill(defaultValue);
		arr[key % radix] = value;
		return [...arr, ...arr, arr[0]];
	};
	return (
		<pattern
			viewBox={`0 0 ${w} ${h}`}
			x={0}
			y={0}
			fill="none"
			width={w}
			height={h}
			patternTransform="translate(-15 -45)"
			patternUnits="userSpaceOnUse"
			patternContentUnits="userSpaceOnUse"
		>
			{_.shuffle(_.range(0, totalLines)).map((i, ix) => (
				<g key={i} opacity={0.7}>
					<line
						opacity={i % gridDensity === 0 ? 1 : 0.75}
						strokeDasharray={i % gridDensity === 0 ? '1 1' : '2 0'}
						x1={0}
						x2={w}
						y1={(ix * h) / gridDensity - h}
						y2={(ix * h) / gridDensity}
						strokeWidth={i % gridDensity === 0 ? mainThickness : secondaryThickness}
						stroke={getTemplateColor(color)}
					>
						<MaxQualityRequired>
							<animate
								attributeType="XML"
								attributeName="opacity"
								fill="freeze"
								dur={`${animCycle}s`}
								repeatCount="indefinite"
								values={animSequence(1, ix, 0.75).join(';')}
							/>
							<animate
								attributeType="XML"
								attributeName="stroke"
								fill="freeze"
								dur={`${animCycle}s`}
								repeatCount="indefinite"
								values={animSequence(getTemplateColor('light_blue'), i, getTemplateColor(color)).join(
									';',
								)}
							/>
							<animate
								attributeType="XML"
								attributeName="stroke-dasharray"
								fill="freeze"
								dur={`${animCycle}s`}
								repeatCount="indefinite"
								values={animSequence('1 1', i, '3 0').join(';')}
							/>
							<animate
								attributeType="XML"
								attributeName="stroke-width"
								fill="freeze"
								dur={`${animCycle}s`}
								repeatCount="indefinite"
								values={animSequence(mainThickness, i, secondaryThickness).join(';')}
							/>
						</MaxQualityRequired>
					</line>
					<line
						opacity={i % gridDensity === 0 ? 1 : 0.75}
						strokeDasharray={i % gridDensity === 0 ? '1 1' : ''}
						x1={0}
						x2={w}
						y1={(ix * h) / gridDensity}
						y2={(ix * h) / gridDensity - h}
						strokeWidth={i % gridDensity === 0 ? mainThickness : secondaryThickness}
						stroke={getTemplateColor(color)}
					>
						<MaxQualityRequired>
							<animate
								attributeType="XML"
								attributeName="opacity"
								fill="Freeze"
								dur={`${animCycle2}s`}
								repeatCount="indefinite"
								values={animSequence(1, i, 0.75).reverse().join(';')}
							/>
							<animate
								attributeType="XML"
								attributeName="stroke-dasharray"
								fill="freeze"
								dur={`${animCycle2}s`}
								repeatCount="indefinite"
								values={animSequence('3 3', i, '3 0').reverse().join(';')}
							/>
							<animate
								attributeType="XML"
								attributeName="stroke"
								fill="freeze"
								dur={`${animCycle2}s`}
								repeatCount="indefinite"
								values={animSequence(getTemplateColor('light_purple'), i, getTemplateColor(color))
									.reverse()
									.join(';')}
							/>
							<animate
								attributeType="XML"
								attributeName="stroke-width"
								fill="Freeze"
								dur={`${animCycle2}s`}
								repeatCount="indefinite"
								values={animSequence(mainThickness, i, secondaryThickness).reverse().join(';')}
							/>
						</MaxQualityRequired>
					</line>
				</g>
			))}
		</pattern>
	);
};

SVGIconContainer.registerPattern(UIBackdropPatternID, getBackdropLines())
	.registerPath(
		UITalentBackdropShapeID,
		<path d="M44.1 224a5 5 0 1 1 0 2H0v-2h44.1zm160 48a5 5 0 1 1 0 2H82v-2h122.1zm57.8-46a5 5 0 1 1 0-2H304v2h-42.1zm0 16a5 5 0 1 1 0-2H304v2h-42.1zm6.2-114a5 5 0 1 1 0 2h-86.2a5 5 0 1 1 0-2h86.2zm-256-48a5 5 0 1 1 0 2H0v-2h12.1zm185.8 34a5 5 0 1 1 0-2h86.2a5 5 0 1 1 0 2h-86.2zM258 12.1a5 5 0 1 1-2 0V0h2v12.1zm-64 208a5 5 0 1 1-2 0v-54.2a5 5 0 1 1 2 0v54.2zm48-198.2V80h62v2h-64V21.9a5 5 0 1 1 2 0zm16 16V64h46v2h-48V37.9a5 5 0 1 1 2 0zm-128 96V208h16v12.1a5 5 0 1 1-2 0V210h-16v-76.1a5 5 0 1 1 2 0zm-5.9-21.9a5 5 0 1 1 0 2H114v48H85.9a5 5 0 1 1 0-2H112v-48h12.1zm-6.2 130a5 5 0 1 1 0-2H176v-74.1a5 5 0 1 1 2 0V242h-60.1zm-16-64a5 5 0 1 1 0-2H114v48h10.1a5 5 0 1 1 0 2H112v-48h-10.1zM66 284.1a5 5 0 1 1-2 0V274H50v30h-2v-32h18v12.1zM236.1 176a5 5 0 1 1 0 2H226v94h48v32h-2v-30h-48v-98h12.1zm25.8-30a5 5 0 1 1 0-2H274v44.1a5 5 0 1 1-2 0V146h-10.1zm-64 96a5 5 0 1 1 0-2H208v-80h16v-14h-42.1a5 5 0 1 1 0-2H226v18h-16v80h-12.1zm86.2-210a5 5 0 1 1 0 2H272V0h2v32h10.1zM98 101.9V146H53.9a5 5 0 1 1 0-2H96v-42.1a5 5 0 1 1 2 0zM53.9 34a5 5 0 1 1 0-2H80V0h2v34H53.9zm60.1 3.9V66H82v64H69.9a5 5 0 1 1 0-2H80V64h32V37.9a5 5 0 1 1 2 0zM101.9 82a5 5 0 1 1 0-2H128V37.9a5 5 0 1 1 2 0V82h-28.1zm16-64a5 5 0 1 1 0-2H146v44.1a5 5 0 1 1-2 0V18h-26.1zm102.2 270a5 5 0 1 1 0 2H98v14h-2v-16h124.1zM242 149.9V160h16v34h-16v62h48v48h-2v-46h-48v-66h16v-30h-16v-12.1a5 5 0 1 1 2 0zM53.9 18a5 5 0 1 1 0-2H64V2H48V0h18v18H53.9zm112 32a5 5 0 1 1 0-2H192V0h50v2h-48v48h-28.1zm-48-48a5 5 0 0 1-9.8-2h2.07a3 3 0 1 0 5.66 0H178v34h-18V21.9a5 5 0 1 1 2 0V32h14V2h-58.1zm0 96a5 5 0 1 1 0-2H137l32-32h39V21.9a5 5 0 1 1 2 0V66h-40.17l-32 32H117.9zm28.1 90.1a5 5 0 1 1-2 0v-76.51L175.59 80H224V21.9a5 5 0 1 1 2 0V82h-49.59L146 112.41v75.69zm16 32a5 5 0 1 1-2 0v-99.51L184.59 96H300.1a5 5 0 0 1 3.9-3.9v2.07a3 3 0 0 0 0 5.66v2.07a5 5 0 0 1-3.9-3.9H185.41L162 121.41v98.69zm-144-64a5 5 0 1 1-2 0v-3.51l48-48V48h32V0h2v50H66v55.41l-48 48v2.69zM50 53.9v43.51l-48 48V208h26.1a5 5 0 1 1 0 2H0v-65.41l48-48V53.9a5 5 0 1 1 2 0zm-16 16V89.41l-34 34v-2.82l32-32V69.9a5 5 0 1 1 2 0zM12.1 32a5 5 0 1 1 0 2H9.41L0 43.41V40.6L8.59 32h3.51zm265.8 18a5 5 0 1 1 0-2h18.69l7.41-7.41v2.82L297.41 50H277.9zm-16 160a5 5 0 1 1 0-2H288v-71.41l16-16v2.82l-14 14V210h-28.1zm-208 32a5 5 0 1 1 0-2H64v-22.59L40.59 194H21.9a5 5 0 1 1 0-2H41.41L66 216.59V242H53.9zm150.2 14a5 5 0 1 1 0 2H96v-56.6L56.6 162H37.9a5 5 0 1 1 0-2h19.5L98 200.6V256h106.1zm-150.2 2a5 5 0 1 1 0-2H80v-46.59L48.59 178H21.9a5 5 0 1 1 0-2H49.41L82 208.59V258H53.9zM34 39.8v1.61L9.41 66H0v-2h8.59L32 40.59V0h2v39.8zM2 300.1a5 5 0 0 1 3.9 3.9H3.83A3 3 0 0 0 0 302.17V256h18v48h-2v-46H2v42.1zM34 241v63h-2v-62H0v-2h34v1zM17 18H0v-2h16V0h2v18h-1zm273-2h14v2h-16V0h2v16zm-32 273v15h-2v-14h-14v14h-2v-16h18v1zM0 92.1A5.02 5.02 0 0 1 6 97a5 5 0 0 1-6 4.9v-2.07a3 3 0 1 0 0-5.66V92.1zM80 272h2v32h-2v-32zm37.9 32h-2.07a3 3 0 0 0-5.66 0h-2.07a5 5 0 0 1 9.8 0zM5.9 0A5.02 5.02 0 0 1 0 5.9V3.83A3 3 0 0 0 3.83 0H5.9zm294.2 0h2.07A3 3 0 0 0 304 3.83V5.9a5 5 0 0 1-3.9-5.9zm3.9 300.1v2.07a3 3 0 0 0-1.83 1.83h-2.07a5 5 0 0 1 3.9-3.9zM97 100a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-48 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 96a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-144a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-96 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm96 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-32 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM49 36a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-32 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM33 68a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 240a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm80-176a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 48a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm112 176a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-16 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM17 180a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 16a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-32a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM17 84a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm32 64a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm16-16a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />,
	)
	.registerPattern(
		UIUnitBackdropPatternID,
		<pattern
			x="0"
			y="0"
			width="250"
			height="250"
			viewBox="0 0 304 304"
			patternUnits="userSpaceOnUse"
			patternContentUnits="userSpaceOnUse"
		>
			<g filter={SVGIconContainer.getUrlRef(SVGBasicEmbossFilterID)}>
				<use {...SVGIconContainer.getXLinkProps(UITalentBackdropShapeID)}>
					<MaxQualityRequired>
						<animate
							attributeType="XML"
							attributeName="opacity"
							dur="7s"
							repeatCount="indefinite"
							fill="freeze"
							values="1;0;0;0;1"
						/>
					</MaxQualityRequired>
				</use>
				<use
					{...SVGIconContainer.getXLinkProps(UITalentBackdropShapeID)}
					style={{
						transformOrigin: '152px 152px',
						transform: 'rotate(90deg)',
					}}
				>
					<MaxQualityRequired>
						<animate
							attributeType="XML"
							attributeName="opacity"
							dur="7s"
							repeatCount="indefinite"
							fill="freeze"
							values="0;1;0;0;0"
						/>
					</MaxQualityRequired>
				</use>
				<use
					{...SVGIconContainer.getXLinkProps(UITalentBackdropShapeID)}
					style={{
						transformOrigin: '152px 152px',
						transform: 'rotate(-90deg)',
					}}
				>
					<MaxQualityRequired>
						<animate
							attributeType="XML"
							attributeName="opacity"
							dur="7s"
							repeatCount="indefinite"
							fill="freeze"
							values="0;0;1;0;0"
						/>
					</MaxQualityRequired>
				</use>
				<use
					{...SVGIconContainer.getXLinkProps(UITalentBackdropShapeID)}
					style={{
						transformOrigin: '152px 152px',
						transform: 'rotate(180deg)',
					}}
				>
					<MaxQualityRequired>
						<animate
							attributeType="XML"
							attributeName="opacity"
							dur="7s"
							repeatCount="indefinite"
							fill="freeze"
							values="0;0;0;1;0"
						/>
					</MaxQualityRequired>
				</use>
			</g>
		</pattern>,
	)
	.registerPattern(
		UITalentBackdropPatternID,
		<pattern
			x="0"
			y="0"
			width="35"
			height="62.5"
			viewBox="0 0 56 100"
			patternUnits="userSpaceOnUse"
			patternContentUnits="userSpaceOnUse"
		>
			<path
				filter={SVGIconContainer.getUrlRef(SVGAnimatedNoiseFilterID)}
				d="M28 66L0 50L0 16L28 0L56 16L56 50L28 66L28 100"
				fill="none"
				stroke={getTemplateColor('text_main')}
				style={{ mixBlendMode: 'soft-light' }}
				strokeDasharray="3 3"
				strokeWidth="2"
			>
				<MaxQualityRequired>
					<animate
						attributeType="XML"
						attributeName="stroke-dasharray"
						fill="freeze"
						dur="6s"
						repeatCount="indefinite"
						values="3 3; 10 1; 1 10; 3 3;"
					/>
					<animate
						attributeType="XML"
						attributeName="stroke-width"
						fill="freeze"
						dur="3.5s"
						repeatCount="indefinite"
						values="1;5;2;7;1"
					/>
					<animate
						attributeType="XML"
						attributeName="stroke"
						fill="freeze"
						repeatCount="indefinite"
						dur="5.5s"
						values={[
							getTemplateColor('gray_bg'),
							getTemplateColor('tealish'),
							getTemplateColor('navy_blue'),
							getTemplateColor('dark_green'),
							getTemplateColor('gray_bg'),
						].join(';')}
					/>
				</MaxQualityRequired>
			</path>
			<path
				filter={SVGIconContainer.getUrlRef(SVGRefractFilterId)}
				d="M28 0L28 34L0 50L0 84L28 100L56 84L56 50L28 34"
				fill="none"
				strokeDasharray="5 1"
				style={{ mixBlendMode: 'normal' }}
				stroke={getTemplateColor('dark_purple')}
				strokeWidth="2"
			>
				<MaxQualityRequired>
					<animate
						attributeType="XML"
						attributeName="stroke-dasharray"
						fill="freeze"
						dur="7.5s"
						repeatCount="indefinite"
						values="5 1; 1 1; 1 5; 2 2;5 1"
					/>
					<animate
						attributeType="XML"
						attributeName="stroke-width"
						fill="freeze"
						dur="2.5s"
						repeatCount="indefinite"
						values="4.5;0.5;4.5"
					/>
					<animate
						attributeType="XML"
						attributeName="stroke"
						fill="freeze"
						repeatCount="indefinite"
						dur="4.5s"
						values={[
							getTemplateColor('dark_purple'),
							getTemplateColor('blood_red'),
							getTemplateColor('dark_pink'),
							getTemplateColor('navy_blue'),
							getTemplateColor('dark_purple'),
						].join(';')}
					/>
				</MaxQualityRequired>
			</path>
		</pattern>,
	)
	.registerPattern(
		UIPrototypeBackdropPatternID,
		<pattern
			viewBox="0 0 28 49"
			width={28 * 1.5}
			height={49 * 1.5}
			patternUnits="userSpaceOnUse"
			patternContentUnits="userSpaceOnUse"
		>
			<linearGradient
				x1="20%"
				x2="50%"
				y1="0%"
				y2="100%"
				id={UIPrototypeBackdropPatternID + '-gradient'}
				gradientUnits="objectBoundingBox"
			>
				<stop stopColor={getTemplateColor('dark_blue')} offset={0} opacity={0.4}>
					<MaxQualityRequired>
						<animate
							attributeType="XML"
							attributeName="opacity"
							dur="9s"
							repeatCount="indefinite"
							fill="freeze"
							values="0.4;0.8;0.1;0.4"
						/>
					</MaxQualityRequired>
				</stop>
				<stop stopColor={getTemplateColor('dark_pink')} offset={0.4} opacity={0.2}>
					<MaxQualityRequired>
						<animate
							attributeType="XML"
							attributeName="offset"
							dur="7s"
							repeatCount="indefinite"
							fill="freeze"
							values="0.4;0.15;0.55;0.25;0.4"
						/>
					</MaxQualityRequired>
				</stop>
				<stop stopColor={getTemplateColor('dark_purple')} offset={0.6} opacity={0.3}>
					<MaxQualityRequired>
						<animate
							attributeType="XML"
							attributeName="offset"
							dur="5s"
							repeatCount="indefinite"
							fill="freeze"
							values="0.65;0.95;0.6;0.85;0.65"
						/>
					</MaxQualityRequired>
				</stop>
				<stop stopColor={getTemplateColor('dark_blue')} offset={1} opacity={0.4} />
			</linearGradient>
			<path
				fill={SVGIconContainer.getUrlRef(UIPrototypeBackdropPatternID + '-gradient')}
				filter={SVGIconContainer.getUrlRef(SVGBasicNoiseFilterID)}
				style={{ mixBlendMode: 'hard-light' }}
				fillRule={'evenodd'}
				opacity={0.8}
				d="M13.99 9.25l13 7.5v15l-13 7.5L1 31.75v-15l12.99-7.5zM3 17.9v12.7l10.99 6.34 11-6.35V17.9l-11-6.34L3 17.9zM0 15l12.98-7.5V0h-2v6.35L0 12.69v2.3zm0 18.5L12.98 41v8h-2v-6.85L0 35.81v-2.3zM15 0v7.5L27.99 15H28v-2.31h-.01L17 6.35V0h-2zm0 49v-8l12.99-7.5H28v2.31h-.01L17 42.15V49h-2z"
			/>
		</pattern>,
	)
	.registerGradient(
		UIUnitBackgroundGradientID,
		<linearGradient gradientUnits="objectBoundingBox" x1="20%" x2="80%" y1="0" y2="100%">
			<stop stopColor={getTemplateColor('dark_green')} stopOpacity="0.65" offset="0">
				<MedQualityRequired>
					<animate
						attributeType="XML"
						attributeName="stop-color"
						fill="freeze"
						repeatCount="indefinite"
						dur="7.5s"
						values={[
							getTemplateColor('secondary_bg'),
							getTemplateColor('text_dark'),
							getTemplateColor('tealish'),
							getTemplateColor('secondary_bg'),
						].join(';')}
					/>
				</MedQualityRequired>
			</stop>
			<stop stopColor={getTemplateColor('text_dark')} stopOpacity="0.85" offset="0.5">
				<MedQualityRequired>
					<animate
						attributeType="XML"
						attributeName="stop-color"
						fill="freeze"
						repeatCount="indefinite"
						dur="9.5s"
						values={[
							getTemplateColor('dark_green'),
							'#335',
							getTemplateColor('text_dark'),
							getTemplateColor('dark_green'),
						].join(';')}
					/>
					<animate
						attributeType="XML"
						attributeName="offset"
						fill="freeze"
						repeatCount="indefinite"
						dur="22.5s"
						values="0.85;0.65;0.75;0.5;0.9;0.75;0.85"
					/>
				</MedQualityRequired>
			</stop>
			<stop stopColor={getTemplateColor('secondary_bg')} stopOpacity="0.7" offset="1" />
		</linearGradient>,
	)
	.registerGradient(
		UIPrototypeBackgroundGradientID,
		<linearGradient gradientUnits="objectBoundingBox" x1="45%" x2="50%" y1="0" y2="100%">
			<stop stopColor={getTemplateColor('dark_purple')} stopOpacity="0.85" offset="0">
				<MedQualityRequired>
					<animate
						attributeType="XML"
						attributeName="stop-color"
						fill="freeze"
						repeatCount="indefinite"
						dur="9.5s"
						values={[
							getTemplateColor('dark_blue'),
							getTemplateColor('blood_red'),
							getTemplateColor('dark_purple'),
							getTemplateColor('dark_blue'),
						].join(';')}
					/>
				</MedQualityRequired>
			</stop>
			<stop stopColor={getTemplateColor('navy_blue')} stopOpacity="0.85" offset="0.7">
				<MedQualityRequired>
					<animate
						attributeType="XML"
						attributeName="stop-color"
						fill="freeze"
						repeatCount="indefinite"
						dur="7.5s"
						values={[
							getTemplateColor('navy_blue'),
							getTemplateColor('dark_brown'),
							getTemplateColor('dark_pink'),
							getTemplateColor('navy_blue'),
						].join(';')}
					/>
				</MedQualityRequired>
				<animate
					attributeType="XML"
					attributeName="offset"
					fill="freeze"
					repeatCount="indefinite"
					dur="19.5s"
					values="0.7;0.2;0.25;0.2;0.5;0.45;0.7;0.65;0.7"
				/>
			</stop>
			<stop stopColor={getTemplateColor('text_dark')} stopOpacity="0.7" offset="1" />
		</linearGradient>,
	)
	.registerGradient(
		UITalentBackgroundGradientID,
		<linearGradient gradientUnits="objectBoundingBox" x1="20%" x2="80%" y1="0" y2="100%">
			<stop stopColor={getTemplateColor('blood_red')} stopOpacity="0.85" offset="0">
				<MedQualityRequired>
					<animate
						attributeType="XML"
						attributeName="stop-color"
						fill="freeze"
						repeatCount="indefinite"
						dur="12s"
						values={[
							getTemplateColor('rust'),
							'#321800',
							getTemplateColor('blood_red'),
							getTemplateColor('rust'),
						].join(';')}
					/>
					<animate
						attributeType="XML"
						attributeName="stop-opacity"
						fill="freeze"
						repeatCount="indefinite"
						dur="5.5s"
						values="1;0.8;0.95;0.92;1"
					/>
				</MedQualityRequired>
			</stop>
			<stop stopColor={getTemplateColor('dark_brown')} stopOpacity="0.75" offset="0.7">
				<MedQualityRequired>
					<animate
						attributeType="XML"
						attributeName="offset"
						fill="freeze"
						repeatCount="indefinite"
						dur="11.7s"
						values="0.7;0.5;0.75;0.4;0.7"
					/>
					<animate
						attributeType="XML"
						attributeName="stop-opacity"
						fill="freeze"
						repeatCount="indefinite"
						dur="6.5s"
						values="0.7;0.5;0.75;0.7"
					/>
					<animate
						attributeType="XML"
						attributeName="stop-color"
						fill="freeze"
						repeatCount="indefinite"
						dur="14.5s"
						values={[
							getTemplateColor('dark_orange'),
							getTemplateColor('blood_red'),
							getTemplateColor('dark_pink'),
							getTemplateColor('dark_orange'),
						].join(';')}
					/>
					<animate
						attributeType="XML"
						attributeName="offset"
						fill="freeze"
						repeatCount="indefinite"
						dur="7.5s"
						values="0.7;0.65;0.75;0.7"
					/>
				</MedQualityRequired>
			</stop>
			<stop stopColor={getTemplateColor('text_dark')} stopOpacity="0.8" offset="1" />
		</linearGradient>,
	);

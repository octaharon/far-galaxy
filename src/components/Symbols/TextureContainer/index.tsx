import React from 'react';
import { TWithGraphicSettingsProps, withGraphicSettings } from '../../../contexts/GraphicSettings';
import DynamicNoiseTexture from '../DynamicNoiseTexture';

export const DynamicNoiseTextureCanvasId = 'noise-canvas-texture';
type TextureContainerProps = React.PropsWithChildren<TWithGraphicSettingsProps>;
const totalTextures = 1;
class TextureContainerClass extends React.PureComponent<TextureContainerProps, { readyTextures: number }> {
	constructor(props: TextureContainerProps) {
		super(props);
		this.state = {
			readyTextures: 0,
		};
	}

	onReady = () =>
		this.setState({
			readyTextures: this.state.readyTextures + 1,
		});

	render() {
		return (
			<>
				<DynamicNoiseTexture
					id={DynamicNoiseTextureCanvasId}
					graphicSettings={this.props.graphicSettings}
					onReady={this.onReady}
				/>
				{this.state.readyTextures >= totalTextures && this.props.children}
			</>
		);
	}
}

const TextureContainer = withGraphicSettings(TextureContainerClass);
export default TextureContainer;

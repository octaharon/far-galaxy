import * as Three from 'three';
import BasicMaths from '../../../../definitions/maths/BasicMaths';
import { applyDistribution } from '../../../../definitions/maths/Distributions';
import { TPlanetProps } from '../../../../definitions/space/Planets';
import { TStarSystemProps } from '../../../../definitions/space/Systems';
import {
	atmosphericColorHue,
	atmosphericColorLuminosity,
	atmosphericColorSaturation,
	surfaceColorHue,
	surfaceColorLuma,
	surfaceColorSaturation,
} from './constants';

export const generateStarColor = (system: TStarSystemProps) => {
	const color = new Three.Color(0x000000);
	color.setHSL(
		applyDistribution({ distribution: 'Uniform' }),
		BasicMaths.lerp({
			x: (system.age + system.radiationLevel) / 2,
			min: 0.2,
			max: 1,
		}),
		applyDistribution(
			{
				min: 0.7,
				max: 1,
				distribution: 'Maximal',
			},
			(system.starTemperature + system.radiationLevel * 2 - system.warpFactor / 2) / 2.5,
		),
	);
	return color.getHex();
};

export const getPlanetSurfaceColor = (planet: TPlanetProps) => {
	const color = new Three.Color(0x000000);
	const luma = applyDistribution(surfaceColorLuma(planet));
	const hue = ((applyDistribution(surfaceColorHue(planet)) + 360) % 360) / 360;
	const saturation = applyDistribution(surfaceColorSaturation(planet));
	color.setHSL(hue, saturation, luma);
	return color;
};

export const getAtmosphereColor = (planet: TPlanetProps) => {
	const color = new Three.Color(0x000000);
	const luma = applyDistribution(atmosphericColorLuminosity(planet));
	const hue = ((applyDistribution(atmosphericColorHue(planet)) + 360) % 360) / 360;
	const saturation = applyDistribution(atmosphericColorSaturation(planet));
	color.setHSL(hue, saturation, luma);
	return color;
};

export const generatePlanetColors = (planet: TPlanetProps) => {
	const atmosColor = getAtmosphereColor(planet);
	const surfaceColor = getPlanetSurfaceColor(planet);
	const starlightHsl: Three.HSL = { h: 0, s: 0, l: 0 };
	const atmosHsl: Three.HSL = { h: 0, s: 0, l: 0 };
	const surfaceHsl: Three.HSL = { h: 0, s: 0, l: 0 };
	new Three.Color(planet.starlightColorHex).getHSL(starlightHsl);
	atmosColor.getHSL(atmosHsl);
	surfaceColor.getHSL(surfaceHsl);
	const atmosHueOffset = (starlightHsl.h - atmosHsl.h) * 0.05;
	const surfaceHueOffset = (atmosHsl.h + atmosHueOffset - surfaceHsl.h) * 0.08;
	atmosColor.offsetHSL(atmosHueOffset, (starlightHsl.s - atmosHsl.s) * 0.1, 0);
	surfaceColor.offsetHSL(surfaceHueOffset, 0, (starlightHsl.l + atmosHsl.l - surfaceHsl.l) * 0.1);
	return {
		surfaceColorHex: surfaceColor.getHex(),
		atmosphericColorHex: atmosColor.getHex(),
	} as Pick<TPlanetProps, 'atmosphericColorHex' | 'surfaceColorHex'>;
};

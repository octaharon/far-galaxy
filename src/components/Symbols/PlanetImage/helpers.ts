import BasicMaths from '../../../../definitions/maths/BasicMaths';
import Distributions, { applyDistribution } from '../../../../definitions/maths/Distributions';
import { TMapTerrainType, TPlanetProps, TPlanetType } from '../../../../definitions/space/Planets';
import { getPlanetDeterminedRadix, getTerrainTypeFromProps } from '../../../../definitions/space/Planets/helpers';
import { TWithGraphicSettingsProps } from '../../../contexts/GraphicSettings';
import { IUICommonProps } from '../../UI';
import { SurfaceHeightTextureCache, SurfaceNormalTextureCache, SurfaceTextureCache } from './textures';

export type TPlanetImageProps = IUICommonProps & { planet: TPlanetProps } & TWithGraphicSettingsProps;
export type TPlanetTextureKeys = 'primary' | 'secondary' | 'normal' | 'displacement' | 'offset' | 'emissive';
export type TPlanetTextureCache = Partial<Record<TPlanetTextureKeys, string>>;
export type TPlanetTextureProps = TPlanetTextureCache & { displacementRange: number };
export const renderRadius = (size: number) =>
	BasicMaths.lerp({
		min: 45,
		max: 100,
		x: Math.sqrt(size ?? 1),
	});

const displacementSeed = 1024;

export const getMapTerrainTexture = (p: TMapTerrainType) => SurfaceTextureCache[p] as string;

export function getTerrainSecondaryTextureByType(p: TPlanetType): string {
	switch (p) {
		case TPlanetType.giant:
			return getMapTerrainTexture(TMapTerrainType.lava);
		case TPlanetType.carbon:
			return getMapTerrainTexture(TMapTerrainType.lowlands);
		case TPlanetType.hollow:
			return getMapTerrainTexture(TMapTerrainType.dirt);
		case TPlanetType.proto:
			return getMapTerrainTexture(TMapTerrainType.earthy);
		case TPlanetType.asteroid:
			return getMapTerrainTexture(TMapTerrainType.stones);
		case TPlanetType.volcanic:
			return getMapTerrainTexture(TMapTerrainType.goo);
		case TPlanetType.ice:
			return getMapTerrainTexture(TMapTerrainType.cold);
		case TPlanetType.condensed:
			return getMapTerrainTexture(TMapTerrainType.jungle);
		case TPlanetType.rogue:
			return getMapTerrainTexture(TMapTerrainType.gravel);
		case TPlanetType.chtonian:
			return getMapTerrainTexture(TMapTerrainType.rock);
		case TPlanetType.frozen:
			return getMapTerrainTexture(TMapTerrainType.mountains);
		case TPlanetType.terra:
			return getMapTerrainTexture(TMapTerrainType.landscape);
		case TPlanetType.hycean:
			return getMapTerrainTexture(TMapTerrainType.gas);
		case TPlanetType.ocean:
			return getMapTerrainTexture(TMapTerrainType.peacock);
		case TPlanetType.arid:
			return getMapTerrainTexture(TMapTerrainType.sand);
		case TPlanetType.tomb:
			return getMapTerrainTexture(TMapTerrainType.slime);
		case TPlanetType.pulsar:
			return getMapTerrainTexture(TMapTerrainType.vines);
		default:
			return getMapTerrainTexture(TMapTerrainType.gravel);
	}
}

export function getTerrainDisplacementByType(p: TPlanetProps): {
	normalMap: string;
	displacementMap: string;
	offsetMap?: string;
	displacementRange: Distributions.ISeedValue;
} {
	const seeds = getPlanetDeterminedRadix(p, 2, 24);
	switch (p?.type) {
		case TPlanetType.asteroid:
			return {
				normalMap: seeds[0]
					? SurfaceNormalTextureCache.caverns
					: seeds[1]
					? SurfaceNormalTextureCache.craters
					: SurfaceNormalTextureCache.peacock,
				displacementMap: seeds[2]
					? SurfaceHeightTextureCache.caverns
					: seeds[3]
					? SurfaceHeightTextureCache.earthy
					: SurfaceHeightTextureCache.vines,
				offsetMap: seeds[4]
					? getMapTerrainTexture(TMapTerrainType.lowlands)
					: seeds[5]
					? getMapTerrainTexture(TMapTerrainType.rock)
					: getMapTerrainTexture(TMapTerrainType.stones),
				displacementRange: {
					distribution: 'Uniform',
					min: !seeds[2] && seeds[3] ? 0.85 : 0.6,
					max: !seeds[2] && seeds[3] ? 1 : 0.8,
				},
			};
		case TPlanetType.carbon:
			return {
				normalMap: SurfaceNormalTextureCache.caverns,
				displacementMap: SurfaceHeightTextureCache.craters,
				offsetMap: SurfaceNormalTextureCache.lowlands,
				displacementRange: {
					distribution: 'Uniform',
					min: 0.12,
					max: 0.15,
				},
			};
		case TPlanetType.pulsar:
			return {
				normalMap: seeds[4] ? SurfaceNormalTextureCache.stones : SurfaceNormalTextureCache.caverns,
				displacementMap: seeds[3] ? SurfaceHeightTextureCache.stones : SurfaceHeightTextureCache.caverns,
				offsetMap: seeds[2]
					? getMapTerrainTexture(TMapTerrainType.desert)
					: getMapTerrainTexture(TMapTerrainType.gravel),
				displacementRange: {
					distribution: 'Uniform',
					min: 0.32,
					max: 0.45,
				},
			};
		case TPlanetType.arid:
			return {
				offsetMap: getMapTerrainTexture(TMapTerrainType.desert),
				normalMap: SurfaceNormalTextureCache.forest,
				displacementMap: SurfaceHeightTextureCache.earthy,
				displacementRange: {
					distribution: 'Uniform',
					min: 0.1,
					max: 0.15,
				},
			};
		case TPlanetType.tomb:
			return {
				offsetMap: SurfaceNormalTextureCache.lowlands,
				normalMap: seeds[0] ? SurfaceNormalTextureCache.sulphur : SurfaceNormalTextureCache.forest,
				displacementMap: seeds[3] ? SurfaceHeightTextureCache.vines : SurfaceHeightTextureCache.volcanic,
				displacementRange: {
					distribution: 'Uniform',
					min: 0.05,
					max: 0.1,
				},
			};
		case TPlanetType.ice:
			return {
				offsetMap: getMapTerrainTexture(TMapTerrainType.ice),
				normalMap: seeds[1] ? SurfaceNormalTextureCache.craters : SurfaceNormalTextureCache.caverns,
				displacementMap: seeds[3] ? SurfaceHeightTextureCache.stones : SurfaceHeightTextureCache.snow,
				displacementRange: {
					distribution: 'Bell',
					min: 0.12,
					max: 0.18,
				},
			};
		case TPlanetType.chtonian:
			return {
				offsetMap: seeds[2] ? SurfaceHeightTextureCache.rough : SurfaceHeightTextureCache.forest,
				normalMap: seeds[1] ? SurfaceNormalTextureCache.ice : SurfaceNormalTextureCache.snow,
				displacementMap: seeds[3] ? SurfaceHeightTextureCache.grass : SurfaceHeightTextureCache.vines,
				displacementRange: {
					distribution: 'Bell',
					min: 0.18,
					max: 0.25,
				},
			};
		case TPlanetType.frozen:
			return {
				offsetMap: getMapTerrainTexture(TMapTerrainType.goo),
				normalMap: getMapTerrainTexture(TMapTerrainType.ice),
				displacementMap: SurfaceNormalTextureCache.snow,
				displacementRange: {
					distribution: 'Uniform',
					min: 0.05,
					max: 0.2,
				},
			};
		case TPlanetType.hollow:
			return {
				offsetMap: seeds[1]
					? getMapTerrainTexture(TMapTerrainType.craters)
					: getMapTerrainTexture(TMapTerrainType.sulphur),
				displacementMap: seeds[3] ? SurfaceHeightTextureCache.rough : SurfaceHeightTextureCache.stones,
				normalMap: seeds[2] ? SurfaceNormalTextureCache.forest : SurfaceNormalTextureCache.craters,
				displacementRange: {
					distribution: 'Uniform',
					min: 0.25,
					max: 0.35,
				},
			};
		case TPlanetType.giant:
			return {
				normalMap: null,
				displacementMap: null,
				offsetMap: getMapTerrainTexture(TMapTerrainType.mountains),
				displacementRange: {
					distribution: 'Uniform',
					min: 0,
					max: 0,
				},
			};
		case TPlanetType.condensed:
			return {
				normalMap: null,
				displacementMap: null,
				offsetMap: SurfaceNormalTextureCache.peacock,
				displacementRange: {
					distribution: 'Uniform',
					min: 0,
					max: 0,
				},
			};
		case TPlanetType.hycean:
			return {
				normalMap: seeds[0] ? SurfaceNormalTextureCache.slime : SurfaceNormalTextureCache.earthy,
				displacementMap: seeds[2] ? SurfaceHeightTextureCache.sulphur : SurfaceHeightTextureCache.slime,
				offsetMap: getMapTerrainTexture(TMapTerrainType.slime),
				displacementRange: {
					distribution: 'Uniform',
					min: 0.001,
					max: 0.004,
				},
			};
		case TPlanetType.ocean:
			return {
				displacementMap: seeds[3] ? SurfaceHeightTextureCache.slime : SurfaceHeightTextureCache.sulphur,
				normalMap: seeds[2] ? SurfaceNormalTextureCache.sulphur : SurfaceNormalTextureCache.slime,
				offsetMap: getMapTerrainTexture(TMapTerrainType.lava),
				displacementRange: {
					distribution: 'Uniform',
					min: 0.04,
					max: 0.08,
				},
			};
		case TPlanetType.terra:
			return {
				displacementMap: seeds[4] ? SurfaceHeightTextureCache.grass : SurfaceHeightTextureCache.earthy,
				normalMap: SurfaceNormalTextureCache.grass,
				offsetMap: seeds[2]
					? getMapTerrainTexture(TMapTerrainType.gas)
					: getMapTerrainTexture(TMapTerrainType.landscape),
				displacementRange: {
					distribution: 'Uniform',
					min: 0.08,
					max: 0.13,
				},
			};
		case TPlanetType.proto:
			return {
				normalMap: seeds[3]
					? SurfaceNormalTextureCache.caverns
					: seeds[4]
					? SurfaceNormalTextureCache.peacock
					: SurfaceNormalTextureCache.forest,
				displacementMap: seeds[2] ? SurfaceHeightTextureCache.stones : SurfaceHeightTextureCache.earthy,
				offsetMap: seeds[4] ? SurfaceHeightTextureCache.rough : SurfaceHeightTextureCache.forest,
				displacementRange: {
					distribution: 'Uniform',
					min: seeds[3] ? 0.2 : 0.3,
					max: seeds[3] ? 0.25 : 0.35,
				},
			};
		case TPlanetType.volcanic:
			return {
				normalMap: seeds[1] ? SurfaceNormalTextureCache.caverns : SurfaceNormalTextureCache.craters,
				displacementMap: seeds[3] ? SurfaceHeightTextureCache.rough : SurfaceHeightTextureCache.peacock,
				offsetMap: getMapTerrainTexture(TMapTerrainType.mountains),
				displacementRange: {
					distribution: 'Uniform',
					min: 0.07,
					max: 0.1,
				},
			};
		case TPlanetType.rogue:
			return {
				normalMap: seeds[1] ? SurfaceNormalTextureCache.peacock : SurfaceNormalTextureCache.dirt,
				displacementMap: seeds[3] ? SurfaceHeightTextureCache.forest : SurfaceHeightTextureCache.rough,
				offsetMap: seeds[5]
					? getMapTerrainTexture(TMapTerrainType.sulphur)
					: getMapTerrainTexture(TMapTerrainType.slime),
				displacementRange: {
					distribution: 'Uniform',
					min: 0.12,
					max: 0.15,
				},
			};
		default:
			return {
				normalMap: SurfaceNormalTextureCache.stones,
				displacementMap: SurfaceHeightTextureCache.caverns,
				displacementRange: {
					distribution: 'Uniform',
					min: 0,
					max: 0.1,
				},
			};
	}
}

export const getPlanetEmissiveMap = (planet: TPlanetProps) => {
	switch (planet.type) {
		case TPlanetType.hycean:
		case TPlanetType.pulsar:
			return require('../../../../assets/img/textures/EmissiveMaps/1440p/radiation_magenta.jpg');
		case TPlanetType.arid:
		case TPlanetType.ocean:
		case TPlanetType.tomb:
		case TPlanetType.rogue:
			return require('../../../../assets/img/textures/EmissiveMaps/1440p/radiation_green.jpg');
		case TPlanetType.volcanic:
		case TPlanetType.hollow:
			return require('../../../../assets/img/textures/EmissiveMaps/1440p/radiation_red.jpg');
		case TPlanetType.giant:
		case TPlanetType.ice:
		case TPlanetType.chtonian:
			return require('../../../../assets/img/textures/EmissiveMaps/1440p/radiation_blue.jpg');
		case TPlanetType.frozen:
		case TPlanetType.proto:
			return require('../../../../assets/img/textures/EmissiveMaps/1440p/radiation_purple.jpg');
		case TPlanetType.condensed:
		case TPlanetType.terra:
		case TPlanetType.asteroid:
			return require('../../../../assets/img/textures/EmissiveMaps/1440p/radiation_white.jpg');
		default:
			return null;
	}
};

export const getPlanetTextures = (planet: TPlanetProps) => {
	const displacementProps = getTerrainDisplacementByType(planet);
	const textureCollection: TPlanetTextureProps = {
		offset: displacementProps.offsetMap,
		primary: getMapTerrainTexture(getTerrainTypeFromProps(planet)),
		secondary: getTerrainSecondaryTextureByType(planet.type),
		emissive: getPlanetEmissiveMap(planet),
		displacementRange: applyDistribution(
			displacementProps.displacementRange,
			(Math.round(
				displacementSeed *
					10 *
					(planet.size +
						planet.age +
						planet.gravity +
						planet.radiationLevel +
						planet.mass +
						planet.magneticField),
			) %
				displacementSeed) /
				displacementSeed,
		),
	};
	if (displacementProps.normalMap) textureCollection.normal = displacementProps.normalMap;
	if (displacementProps.displacementMap) textureCollection.displacement = displacementProps.displacementMap;
	return textureCollection;
};

import * as THREE from 'three';
import Distributions from '../../../../definitions/maths/Distributions';
import { TPlanetProps, TPlanetType } from '../../../../definitions/space/Planets';
import { TGraphicQualityLevel } from '../../../contexts/GraphicSettings/slice';
import { predicateSelection } from '../../../utils/predicateChoices';

const zoomLevel = 0.1;
const camPosition = new THREE.Vector3(
	-30 * (0.5 + zoomLevel * 0.4),
	130 * (0.5 + zoomLevel * 0.4),
	225 * (0.5 + zoomLevel * 0.4),
);
export const camOptions = {
	fov: 110 / (1 + zoomLevel),
	near: 1,
	far: 800 + 400 * zoomLevel,
	position: camPosition,
	aspect: 1,
};
export const rotationSpeed = 0.025;
export const planetPosition = new THREE.Vector3(0, 0, 0);
export const ambientLightPosition = new THREE.Vector3(200, 200, -100);
export const directionalLightPosition = new THREE.Vector3(350, 250, 450);
export const SSAALevel: Record<TGraphicQualityLevel, number> = {
	Max: 4,
	Med: 2,
	Min: 0,
};

export const atmosphericColorLuminosity = predicateSelection<Distributions.ISeedValue, TPlanetProps>([
	[{ min: 0.9, max: 1.0, distribution: 'Uniform' }, (p) => [TPlanetType.asteroid].includes(p.type)],
	[{ min: 0.8, max: 0.9, distribution: 'Uniform' }, (p) => [TPlanetType.terra].includes(p.type)],
	[
		{ min: 0.6, max: 0.85, distribution: 'Maximal' },
		(p) => [TPlanetType.giant, TPlanetType.ice, TPlanetType.ocean].includes(p.type),
	],
	[
		{ min: 0.65, max: 0.85, distribution: 'Uniform' },
		(p) => [TPlanetType.volcanic, TPlanetType.condensed, TPlanetType.frozen].includes(p.type),
	],
	[
		{ min: 0.55, max: 0.65, distribution: 'Bell' },
		(p) => [TPlanetType.hycean, TPlanetType.proto, TPlanetType.carbon, TPlanetType.arid].includes(p.type),
	],
	[
		{ min: 0.4, max: 0.55, distribution: 'Bell' },
		(p) => [TPlanetType.rogue, TPlanetType.hollow, TPlanetType.chtonian].includes(p.type),
	],
	[{ min: 0.1, max: 0.4, distribution: 'Bell' }, (p) => [TPlanetType.tomb, TPlanetType.pulsar].includes(p.type)],
]);

export const atmosphericColorSaturation = predicateSelection<Distributions.ISeedValue, TPlanetProps>([
	[
		{ min: 0.4, max: 0.75, distribution: 'Uniform' },
		(p) =>
			[TPlanetType.terra, TPlanetType.rogue, TPlanetType.chtonian, TPlanetType.arid, TPlanetType.hycean].includes(
				p.type,
			),
	],
	[
		{ min: 0.6, max: 0.9, distribution: 'Bell' },
		(p) => [TPlanetType.volcanic, TPlanetType.giant, TPlanetType.ice, TPlanetType.carbon].includes(p.type),
	],
	[
		{ min: 0.15, max: 0.4, distribution: 'Uniform' },
		(p) => [TPlanetType.ocean, TPlanetType.proto, TPlanetType.frozen].includes(p.type),
	],
	[{ min: 0, max: 0.15, distribution: 'Uniform' }, (p) => [TPlanetType.asteroid, TPlanetType.tomb].includes(p.type)],
]);

export const atmosphericColorHue = predicateSelection<Distributions.ISeedValue, TPlanetProps>([
	[{ min: 0, max: 360, distribution: 'Uniform' }, (p) => [TPlanetType.rogue, TPlanetType.proto].includes(p.type)],
	[{ min: 135, max: 205, distribution: 'Uniform' }, (p) => [TPlanetType.terra, TPlanetType.frozen].includes(p.type)],
	[{ min: 170, max: 280, distribution: 'Uniform' }, (p) => [TPlanetType.ocean, TPlanetType.ice].includes(p.type)],
	[{ min: 320, max: 392, distribution: 'Bell' }, (p) => [TPlanetType.chtonian, TPlanetType.carbon].includes(p.type)],
	[
		{ min: 32, max: 70, distribution: 'Uniform' },
		(p) => [TPlanetType.condensed, TPlanetType.hollow, TPlanetType.asteroid].includes(p.type),
	],
	[{ min: 40, max: 260, distribution: 'Uniform' }, (p) => [TPlanetType.giant].includes(p.type)],
	[{ min: 340, max: 420, distribution: 'Uniform' }, (p) => [TPlanetType.volcanic, TPlanetType.arid].includes(p.type)],
	[{ min: 60, max: 125, distribution: 'Bell' }, (p) => [TPlanetType.pulsar, TPlanetType.tomb].includes(p.type)],
	[{ min: 260, max: 325, distribution: 'Bell' }, (p) => [TPlanetType.hycean].includes(p.type)],
]);

export const surfaceColorHue = predicateSelection<Distributions.ISeedValue, TPlanetProps>([
	[{ min: -30, max: 45, distribution: 'Uniform' }, (p) => [TPlanetType.volcanic].includes(p.type)],
	[
		{ min: 220, max: 323, distribution: 'Uniform' },
		(p) => [TPlanetType.hycean, TPlanetType.chtonian].includes(p.type),
	],
	[{ min: 20, max: 75, distribution: 'Minimal' }, (p) => [TPlanetType.arid, TPlanetType.condensed].includes(p.type)],
	[{ min: 140, max: 260, distribution: 'Uniform' }, (p) => [TPlanetType.ice, TPlanetType.frozen].includes(p.type)],
	[
		{ min: 25, max: 225, distribution: 'Uniform' },
		(p) => [TPlanetType.giant, TPlanetType.condensed, TPlanetType.rogue].includes(p.type),
	],
	[{ min: 90, max: 215, distribution: 'Uniform' }, (p) => [TPlanetType.ocean].includes(p.type)],
	[{ min: 150, max: 260, distribution: 'Bell' }, (p) => [TPlanetType.terra].includes(p.type)],
	[
		{ min: 275, max: 350, distribution: 'Uniform' },
		(p) => [TPlanetType.carbon, TPlanetType.hollow, TPlanetType.proto].includes(p.type),
	],
]);

export const surfaceColorLuma = predicateSelection<Distributions.ISeedValue, TPlanetProps>([
	[
		{ min: 0.35, max: 0.5, distribution: 'Bell' },
		(p) =>
			[TPlanetType.ocean, TPlanetType.rogue, TPlanetType.terra, TPlanetType.giant, TPlanetType.chtonian].includes(
				p.type,
			),
	],
	[
		{ min: 0.45, max: 0.7, distribution: 'Maximal' },
		(p) => [TPlanetType.arid, TPlanetType.giant, TPlanetType.hollow].includes(p.type),
	],
	[
		{ min: 0.45, max: 0.55, distribution: 'Uniform' },
		(p) => [TPlanetType.hycean, TPlanetType.volcanic, TPlanetType.proto, TPlanetType.condensed].includes(p.type),
	],
	[{ min: 0.6, max: 0.95, distribution: 'Maximal' }, (p) => [TPlanetType.frozen, TPlanetType.ice].includes(p.type)],
	[
		{ min: 0.15, max: 0.5, distribution: 'Minimal' },
		(p) => [TPlanetType.carbon, TPlanetType.pulsar, TPlanetType.asteroid].includes(p.type),
	],
]);

export const surfaceColorSaturation = predicateSelection<Distributions.ISeedValue, TPlanetProps>([
	[
		{ min: 0.75, max: 1, distribution: 'Bell' },
		(p) => [TPlanetType.giant, TPlanetType.volcanic, TPlanetType.terra].includes(p.type),
	],
	[{ min: 0.5, max: 0.75, distribution: 'Uniform' }, (p) => [TPlanetType.proto, TPlanetType.rogue].includes(p.type)],
	[
		{ min: 0.35, max: 0.65, distribution: 'Uniform' },
		(p) => [TPlanetType.hycean, TPlanetType.arid, TPlanetType.carbon, TPlanetType.asteroid].includes(p.type),
	],
	[{ min: 0.15, max: 0.65, distribution: 'Bell' }, (p) => [TPlanetType.ice, TPlanetType.frozen].includes(p.type)],
	[{ min: 0, max: 0.25, distribution: 'Bell' }, (p) => [TPlanetType.pulsar, TPlanetType.tomb].includes(p.type)],
	[{ min: 0.5, max: 1, distribution: 'Uniform' }, (p) => [TPlanetType.ocean, TPlanetType.condensed].includes(p.type)],
]);

import { Stars } from '@react-three/drei';
import { useFrame, useThree } from '@react-three/fiber';
import React from 'react';
import * as THREE from 'three';
import _ from 'underscore';
import { TPlanetProps } from '../../../../../definitions/space/Planets';
import { TWithGraphicSettingsProps } from '../../../../contexts/GraphicSettings';
import { TGraphicQualityLevel } from '../../../../contexts/GraphicSettings/slice';
import { rotationSpeed } from '../constants';

export const BackgroundLayer = React.memo((props: TWithGraphicSettingsProps & { planet: TPlanetProps }) => {
	const { gl, camera } = useThree();
	const scene = React.useRef();
	const starsRef = React.useRef<THREE.Mesh>();
	useFrame(({ clock }) => {
		if (!starsRef.current) return;
		starsRef.current.rotation.y = clock.getElapsedTime() * rotationSpeed;
		gl.clear();
		gl.render(scene.current, camera);
	}, 0);
	const starCount: Record<TGraphicQualityLevel, number> = {
		Max: 12000,
		Med: 8000,
		Min: 2500,
	};
	return (
		<scene ref={scene}>
			<Stars
				ref={starsRef}
				radius={200} // Radius of the inner sphere (default=100)
				depth={100} // Depth of area where stars should fit (default=50)
				count={starCount[props.graphicSettings.qualityLevel]} // Amount of stars (default=5000)
				factor={7} // Size factor (default=4)
				saturation={0.8} // Saturation 0-1 (default=0)
				fade // Faded dots (default=false)
			/>
		</scene>
	);
}, _.isEqual);

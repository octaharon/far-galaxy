import * as THREE from 'three';
import { CloudFMBShaders } from '../../../../shaders/cloudFMBShaders';

class AtmosphereMaterial extends THREE.ShaderMaterial {
	constructor({
		cf = 1.0,
		power = 2,
		color = new THREE.Color('white'),
		transparency = 0.5,
		resolution,
		...rest
	}: THREE.ShaderMaterialParameters & {
		cf?: number;
		power?: number;
		color?: THREE.Color;
		transparency?: number;
		resolution?: THREE.Vector2;
	}) {
		super({
			...rest,
			uniforms: {
				...THREE.UniformsLib['lights'],
				cf: {
					value: cf ?? 1.0,
				},
				power: {
					value: power ?? 2,
				},
				glowColor: {
					value: color ?? new THREE.Color('white'),
				},
				transparency: {
					value: transparency ?? 0.5,
				},
				uTime: {
					value: Date.now() % 65536,
				},
				uResolution: { value: resolution ?? new THREE.Vector2(256, 256) },
			},
			...CloudFMBShaders,
			lights: true,
			transparent: true,
			depthWrite: true,
			depthTest: false,
			precision: 'highp',
		});
	}
}

export default AtmosphereMaterial;

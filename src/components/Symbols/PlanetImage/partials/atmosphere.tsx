import { useFrame, useThree } from '@react-three/fiber';
import React from 'react';
import * as THREE from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader';
import { TPlanetProps, TPlanetType } from '../../../../../definitions/space/Planets';
import { getPlanetHash } from '../../../../../definitions/space/Planets/helpers';
import { TWithGraphicSettingsProps } from '../../../../contexts/GraphicSettings';
import { doesQualitySettingIncludes } from '../../../../contexts/GraphicSettings/helpers';
import { colorizeShaderMaterial } from '../../../../shaders/colorizeShaderMaterial';
import { ambientLightPosition, planetPosition, rotationSpeed, SSAALevel } from '../constants';
import { getTerrainDisplacementByType, renderRadius } from '../helpers';
import AtmosphereMaterial from './atmosphereMaterial';

const Atmos: React.FC<{ planet: TPlanetProps } & TWithGraphicSettingsProps> = ({ planet, graphicSettings }) => {
	const ref = React.useRef<THREE.Mesh<THREE.IcosahedronGeometry>>();
	const { gl, camera, size } = useThree();
	const composer = React.useRef<EffectComposer>();
	const sceneRef = React.useRef();
	const matRef = React.useRef<AtmosphereMaterial>();
	const shaderRef = React.useRef<ShaderPass>();
	const baseColor = new THREE.Color(planet.atmosphericColorHex);
	React.useEffect(() => {
		if (composer.current) composer.current.setSize(size.width, size.height);
		if (shaderRef.current) shaderRef.current.material.transparent = true;
		gl.clear(true, true, true);
		matRef.current.uniformsNeedUpdate = true;
	}, [size, shaderRef, composer, planet, graphicSettings, gl, matRef]);
	useFrame(({ clock }) => {
		if (!ref.current || !sceneRef.current) return false;
		ref.current.rotation.y = clock.getElapsedTime() * rotationSpeed;
		if (matRef.current) {
			matRef.current.uniforms.uTime.value = clock.getElapsedTime();
			matRef.current.uniformsNeedUpdate = true;
		}
		gl.render(sceneRef.current, camera);
		if (shaderRef.current) shaderRef.current.material.transparent = true;
		if (composer.current) composer.current.render();
	}, 3);
	baseColor.offsetHSL(0, -0.1 * planet.distanceFromStar, -0.3 * planet.distanceFromStar);
	return (
		<>
			<scene ref={sceneRef}>
				<ambientLight
					color={0xffffff}
					intensity={3 * (1 - planet.distanceFromStar) + 1}
					position={ambientLightPosition}
				/>
				<mesh ref={ref} receiveShadow={true} position={planetPosition}>
					<icosahedronGeometry
						args={[
							renderRadius(planet.size) *
								(1.06 -
									0.02 * planet.gravity ** 0.5 -
									0.015 * planet.size ** 2 +
									0.01 * planet.atmosphericDensity +
									(0.1 + 0.05 * planet.size) *
										getTerrainDisplacementByType(planet)?.displacementRange?.max ?? 0),
							Math.floor(10 + 5 * planet.size),
						]}
					/>
					<atmosphereMaterial
						attach={'material'}
						ref={matRef}
						args={[
							{
								power: 0.85 + (1 - planet.atmosphericDensity) * 2,
								color: baseColor,
								cf: 1.3 - 0.2 * planet.gravity,
								transparency: 0.15 + planet.atmosphericDensity ** 1.5 * 0.6,
								blending: [TPlanetType.carbon].includes(planet.type)
									? THREE.AdditiveBlending
									: THREE.NormalBlending,
								resolution: new THREE.Vector2(
									size.width * (0.75 + 0.25 * planet.size),
									size.height + (0.75 + 0.25 * planet.size),
								),
							},
						]}
					/>
				</mesh>
			</scene>
			<effectComposer args={[gl]} ref={composer}>
				{doesQualitySettingIncludes(graphicSettings.qualityLevel, 'Med') ? (
					<sSAARenderPass
						attachArray="passes"
						scene={sceneRef.current}
						camera={camera}
						sampleLevel={SSAALevel[graphicSettings.qualityLevel]}
					/>
				) : (
					<renderPass attachArray="passes" scene={sceneRef.current} camera={camera} />
				)}
				{doesQualitySettingIncludes(graphicSettings.qualityLevel, 'Max') && (
					<filmPass attachArray="passes" args={[0.2 + 0.45 * planet.radiationLevel, 0]} />
				)}
				<shaderPass attachArray="passes" args={[colorizeShaderMaterial(baseColor)]} />
				<shaderPass attachArray="passes" args={[CopyShader]} renderToScreen={true} ref={shaderRef} />
			</effectComposer>
		</>
	);
};

export default React.memo(Atmos, (prop1, prop2) => getPlanetHash(prop1) === getPlanetHash(prop2));

import { useFrame, useThree } from '@react-three/fiber';
import React from 'react';
import * as THREE from 'three';

export const HTMLCanvasAnimatedMaterial: React.FC<{ canvasId: string }> = ({ canvasId }) => {
	const { gl } = useThree();
	const canvas = document.getElementById(canvasId) as HTMLCanvasElement;
	if (!canvas) return null;
	const ctx = canvas.getContext('2d');
	const texture = new THREE.CanvasTexture(ctx.canvas);
	texture.anisotropy = gl.capabilities.getMaxAnisotropy();
	texture.needsUpdate = true;
	useFrame(() => {
		texture.needsUpdate = true;
	});
	return <canvasTexture attach={'map'} image={canvas} />;
};

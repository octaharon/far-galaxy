import { useThree } from '@react-three/fiber';
import React, { useEffect } from 'react';
import * as THREE from 'three';

export const HTMLCanvasMaterial: React.FC<{ canvasID: string; attach?: string }> = ({ canvasID, attach = 'map' }) => {
	const canvas = document.getElementById(canvasID) as HTMLCanvasElement;
	useEffect(() => {
		if (!canvas) return null;
		const ctx = canvas.getContext('2d');
		const texture = new THREE.CanvasTexture(ctx.canvas);
		texture.needsUpdate = true;
	}, [canvas]);
	return <canvasTexture attach={attach} image={canvas} />;
};

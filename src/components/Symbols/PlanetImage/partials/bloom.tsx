import { useFrame, useThree } from '@react-three/fiber';
import React from 'react';
import * as THREE from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader';
import { TPlanetProps } from '../../../../../definitions/space/Planets';
import { TWithGraphicSettingsProps } from '../../../../contexts/GraphicSettings';
import { additiveTransparencyMaterial } from '../../../../shaders/additiveTransparencyMaterial';
import { ambientLightPosition, planetPosition } from '../constants';
import { renderRadius } from '../helpers';

export const Bloom: React.FC<{ planet: TPlanetProps } & TWithGraphicSettingsProps> = ({ planet, graphicSettings }) => {
	const composer = React.useRef<EffectComposer>();
	const sceneRef = React.useRef();
	const { gl, camera, size } = useThree();
	const shaderRef = React.useRef<UnrealBloomPass>();
	const baseColor = new THREE.Color(planet.atmosphericColorHex);
	React.useEffect(() => {
		if (composer.current) composer.current.setSize(size.width, size.height);
		//if (shaderRef.current) shaderRef.current.separableBlurMaterials.map((s) => (s.transparent = true));
	}, [size, shaderRef, composer]);
	useFrame(() => {
		if (sceneRef.current) gl.render(sceneRef.current, camera);
		if (composer.current && sceneRef.current) composer.current.render();
	}, 1);
	return (
		<>
			<scene ref={sceneRef}>
				<mesh position={planetPosition}>
					<ambientLight
						color={baseColor}
						intensity={
							0.25 +
							2 * (1 - planet.distanceFromStar) +
							planet.radiationLevel * 2 +
							1.5 * planet.atmosphericDensity -
							planet.size
						}
						position={ambientLightPosition}
					/>
					<sphereGeometry
						args={[
							renderRadius(planet.size) *
								(0.8 + 0.1 * planet.size + planet.atmosphericDensity * 0.04 - planet.gravity * 0.02),
							8,
							8,
						]}
					/>
					<meshBasicMaterial attach={'material'} color={baseColor} />
				</mesh>
			</scene>
			<effectComposer ref={composer} args={[gl]}>
				<renderPass attachArray="passes" scene={sceneRef.current} camera={camera} />
				<unrealBloomPass
					attachArray="passes"
					args={[
						new THREE.Vector2(size.width, size.height),
						(1.0 + planet.atmosphericDensity) *
							(0.75 + planet.magneticField * 0.25 - planet.distanceFromStar * 0.25),
						1 + planet.radiationLevel - planet.size ** 2 * 0.25 - planet.gravity * 0.5,
						0,
					]}
				/>
				<shaderPass attachArray="passes" args={[additiveTransparencyMaterial(1)]} />
				<shaderPass attachArray="passes" args={[CopyShader]} renderToScreen={true} />
			</effectComposer>
		</>
	);
};

import { useTexture } from '@react-three/drei';
import { useFrame, useThree } from '@react-three/fiber';
import React from 'react';
import * as THREE from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader';
import _ from 'underscore';
import BasicMaths from '../../../../../definitions/maths/BasicMaths';
import { TPlanetProps, TPlanetType } from '../../../../../definitions/space/Planets';
import {
	getPlanetDeterminedRadix,
	getPlanetHash,
	getPlanetShininess,
	getTerrainEmissiveIntensityAmplifier,
} from '../../../../../definitions/space/Planets/helpers';
import { TWithGraphicSettingsProps } from '../../../../contexts/GraphicSettings';
import { doesQualitySettingIncludes } from '../../../../contexts/GraphicSettings/helpers';
import { TGraphicSettings } from '../../../../contexts/GraphicSettings/slice';
import emissiveMapDisplacement from '../../../../shaders/chunks/emissiveMapDisplacement';
import emissiveSwirl from '../../../../shaders/chunks/emissiveSwirl';
import emissiveMapWarp from '../../../../shaders/chunks/emissiveWarp';
import diffuseMapWind from '../../../../shaders/chunks/diffuseWind';
import fbmDisplacement from '../../../../shaders/chunks/fbmDisplacement';
import textureMapDisplacement from '../../../../shaders/chunks/textureMapDisplacement';
import { injectShaderChunks, injectShaderChunksCollection } from '../../../../shaders/injectShaderChunks';
import { DynamicNoiseTextureCanvasId } from '../../TextureContainer';
import { ambientLightPosition, directionalLightPosition, planetPosition, rotationSpeed, SSAALevel } from '../constants';
import { renderRadius, TPlanetTextureCache, TPlanetTextureProps } from '../helpers';
import { HTMLCanvasMaterial } from './canvasTexture';

const planetLOD = (size = 0) => Math.floor(25 + 15 * BasicMaths.clipTo1(size));
const spinDirection = (planet: TPlanetProps) =>
	Math.floor((planet.mass + planet.age + planet.size + planet.warpFactor + planet.spinFactor) * 1e3) % 2 === 0
		? -1
		: 1;
const startTimer = (planet: TPlanetProps) =>
	Math.sin(2 * Math.PI * (planet.age + planet.mass + planet.magneticField)) ** 2 * 1e2;

const PlanetMesh = React.memo(
	({ size = 0, radius = null }: { size?: number; radius?: number }) => (
		<icosahedronBufferGeometry args={[radius ?? renderRadius(size), planetLOD(size)]} />
	),
	_.isEqual,
);
const getNoiseCanvas = () => {
	const c = new THREE.CanvasTexture(document.getElementById(DynamicNoiseTextureCanvasId) as HTMLCanvasElement);
	c.wrapS = THREE.RepeatWrapping;
	c.wrapT = THREE.RepeatWrapping;
	c.needsUpdate = true;
	return c;
};
const shaderInjections: EnumProxyWithDefault<
	TPlanetType,
	(
		planet: TPlanetProps,
		sm: THREE.Shader,
		textures: Proxy<TPlanetTextureCache, THREE.Texture>,
		graphicSettings?: TGraphicSettings,
		texCanvasId?: string,
	) => void
> = {
	[TPlanetType.giant]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunksCollection(sm.fragmentShader, {
			emissive: emissiveSwirl({
				quality: graphicSettings.textureSize,
				noiseResolution: graphicSettings.textureSize,
			}),
			diffuse: fbmDisplacement({
				speed: 0.07 + 0.02 * planet.spinFactor,
				resolutionX: 1,
				resolutionY: 0.75,
				quality: graphicSettings.textureSize,
				amplitude:
					0.25 +
					0.15 * planet.size -
					0.07 * planet.gravity +
					0.07 * planet.magneticField -
					0.03 * planet.temperature +
					0.03 * planet.atmosphericDensity,
				offset: ((planet.mass + planet.magneticField + planet.age) / 3) * 0.1,
				power: 2.4 - planet.distanceFromStar * 0.8 + planet.temperature * 0.5 + planet.radiationLevel * 0.4,
				spin: 0.05 + planet.spinFactor * 0.03,
				scaleX: 5.3 + (planet.magneticField + planet.size + planet.warpFactor - planet.distanceFromStar) * 4.2,
				scaleY: 12 + planet.size * 3 + (planet.age + planet.gravity + planet.temperature - planet.mass) * 2,
			}),
		});
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.texOffsetMap = { value: textures.offset ?? textures.primary };
		sm.uniforms.emissiveOffsetMap = {
			value: new THREE.CanvasTexture(document.getElementById(DynamicNoiseTextureCanvasId) as HTMLCanvasElement),
		};
	},
	[TPlanetType.hycean]: (planet, sm, textures, graphicSettings) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			emissiveMapWarp({
				speedX: 0.049,
				speedY: 0.067,
				distanceX: 0.018,
				distanceY: 0.011,
				resolution: 3,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.emissive,
		};
	},
	[TPlanetType.hollow]: (planet, sm, textures, graphicSettings) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			emissiveMapWarp({
				speedX: 0.032,
				speedY: 0.044,
				distanceX: 0.027,
				distanceY: 0.032,
				resolution: 0.65,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.secondary,
		};
	},
	[TPlanetType.frozen]: (planet, sm, textures, graphicSettings) => {
		sm.fragmentShader = injectShaderChunksCollection(sm.fragmentShader, {
			diffuse: fbmDisplacement({
				speed: 0.01 + 0.01 * planet.spinFactor,
				resolutionX: 0.271,
				resolutionY: 0.305,
				quality: graphicSettings.textureSize,
				amplitude: 0.25 + 0.15 * planet.size,
				offset: 0.01 + planet.age * 0.1,
				power: 0.2 - planet.distanceFromStar * 0.2 + planet.temperature * 0.5 + planet.radiationLevel * 0.4,
				spin: 0.01 + planet.spinFactor * 0.02,
				scaleX: 1.8 + (planet.magneticField + planet.size + planet.warpFactor - planet.distanceFromStar) * 0.2,
				scaleY: 1.34 + (planet.age + planet.gravity + planet.temperature) * 0.4,
			}),
		});
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.texOffsetMap = {
			value: textures.offset,
		};
	},
	[TPlanetType.volcanic]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			emissiveMapDisplacement({
				speed: 0.008,
				resolution: 4,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.displacement,
		};
	},
	[TPlanetType.ocean]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			emissiveMapDisplacement({
				speed: 0.02,
				resolution: 2,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.secondary,
		};
	},
	[TPlanetType.tomb]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			emissiveMapDisplacement({
				speed: 0.015,
				resolution: 1,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.offset,
		};
	},
	[TPlanetType.pulsar]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			emissiveMapDisplacement({
				speed: 0.013,
				resolution: 0.6,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.normal,
		};
	},
	[TPlanetType.rogue]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			emissiveMapWarp({
				speedX: 0.052,
				speedY: 0.064,
				distanceX: 0.047,
				distanceY: 0.032,
				resolution: 13.5,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.secondary,
		};
	},
	[TPlanetType.terra]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			emissiveMapWarp({
				speedX: 0.032,
				speedY: 0.024,
				distanceX: 0.057,
				distanceY: 0.042,
				resolution: 1.5,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.displacement,
		};
	},
	[TPlanetType.arid]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			diffuseMapWind({
				speed: 0.007,
				dampening: 4,
				maxDepth: 3,
				direction: -1.25,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.displacement,
		};
	},
	[TPlanetType.chtonian]: (planet, sm, textures, graphicSettings, texCanvasId) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			diffuseMapWind({
				speed: 0.009,
				dampening: 6,
				maxDepth: 4,
				direction: 0.25,
			}),
		);
		sm.fragmentShader = injectShaderChunks(sm.fragmentShader, {
			replacementChunks: emissiveMapWarp({
				resolution: 17,
				speedX: 0.27,
				speedY: 0.31,
				distanceX: 0.004,
				distanceY: 0.0025,
			}).replacementChunks,
		});
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.emissiveOffsetMap = {
			value: textures.secondary,
		};
	},
	[TPlanetType.condensed]: (planet, sm, textures) => {
		sm.fragmentShader = injectShaderChunks(
			sm.fragmentShader,
			textureMapDisplacement({
				speed: 0.002,
				resolution: 0.15,
			}),
		);
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.texOffsetMap = {
			value: textures.offset,
		};
	},
	default: (planet, sm, textures) => {
		/*sm.fragmentShader = injectShaderChunks(
      sm.fragmentShader,
      textureMapDisplacement({
        speed: 0.008,
        resolution: 1,
      }),
    );*/
		sm.uniforms.uTime = { value: startTimer(planet) };
		sm.uniforms.texOffsetMap = {
			value: getNoiseCanvas(),
		};
	},
};

export const Planet: React.FC<
	{
		planet: TPlanetProps;
		surfaceTextureCanvasId?: string;
		textureCollection: TPlanetTextureProps;
	} & TWithGraphicSettingsProps
> = ({ planet, graphicSettings = { qualityLevel: 'Min' }, surfaceTextureCanvasId, textureCollection }) => {
	const freqs = getPlanetDeterminedRadix(planet, 24);
	const textures = useTexture(_.pick(textureCollection, (v) => !_.isNumber(v) && !!v) as TPlanetTextureCache);
	const emissiveMap = textures.emissive;
	const shaderRef = React.useRef<ShaderPass>();
	const ref = React.useRef<THREE.Mesh<THREE.IcosahedronGeometry>>();
	const refCore = React.useRef<THREE.Mesh<THREE.IcosahedronGeometry>>();
	const { gl, camera, size } = useThree();
	const composer = React.useRef<EffectComposer>();
	const sceneRef = React.useRef();
	const lightRef = React.useRef<THREE.PointLight>();
	const mRef = React.useRef<THREE.MeshPhongMaterial>();
	const mCoreRef = React.useRef<THREE.MeshPhongMaterial>();
	const [mainShader, setMainShader] = React.useState<THREE.Shader>(null);
	const [coreShader, setCoreShader] = React.useState<THREE.Shader>(null);
	const planetHash = getPlanetHash({ planet, graphicSettings });
	if (textures.primary) {
		textures.primary.wrapS = THREE.RepeatWrapping;
		textures.primary.wrapT = THREE.RepeatWrapping;
		textures.primary.center = new THREE.Vector2(0.5, 0.5);
		textures.primary.rotation = (Math.PI / 12) * freqs[0];
		textures.primary.needsUpdate = true;
	}
	if (textures.secondary) {
		textures.secondary.wrapS = THREE.RepeatWrapping;
		textures.secondary.wrapT = THREE.RepeatWrapping;
		textures.secondary.center = new THREE.Vector2(0.5, 0.5);
		textures.secondary.rotation = (Math.PI / 12) * freqs[2];
		textures.secondary.needsUpdate = true;
	}
	if (textures.offset) {
		textures.offset.wrapS = THREE.RepeatWrapping;
		textures.offset.wrapT = THREE.RepeatWrapping;
		textures.offset.center = new THREE.Vector2(0.5, 0.5);
		textures.offset.rotation = (Math.PI / 12) * freqs[4];
		textures.offset.needsUpdate = true;
	}
	if (textures.normal) {
		textures.normal.wrapS = THREE.RepeatWrapping;
		textures.normal.wrapT = THREE.RepeatWrapping;
		textures.normal.center = new THREE.Vector2(0.5, 0.5);
		textures.normal.rotation = (Math.PI / 12) * freqs[3];
		textures.normal.needsUpdate = true;
	}
	if (textures.displacement) {
		textures.displacement.wrapS = THREE.RepeatWrapping;
		textures.displacement.wrapT = THREE.RepeatWrapping;
		textures.displacement.center = new THREE.Vector2(0.5, 0.5);
		textures.displacement.rotation = (Math.PI / 12) * freqs[5];
		textures.displacement.needsUpdate = true;
	}
	if (textures.emissive) {
		textures.emissive.wrapS = THREE.RepeatWrapping;
		textures.emissive.wrapT = THREE.RepeatWrapping;
		textures.emissive.center = new THREE.Vector2(0.5, 0.5);
		textures.emissive.rotation = (Math.PI / 12) * freqs[1];
		textures.emissive.needsUpdate = true;
	}
	const emissiveIntensity = getTerrainEmissiveIntensityAmplifier(planet);
	if (emissiveMap) {
		emissiveMap.wrapS = THREE.RepeatWrapping;
		emissiveMap.wrapT = THREE.RepeatWrapping;
		emissiveMap.flipY = Math.round((planet.age + planet.gravity) * 1e6) % 2 === 0;
		emissiveMap.center = new THREE.Vector2(0.5, 0.5);
		emissiveMap.rotation = BasicMaths.fractionModulo(
			(planet.size + planet.mass + planet.warpFactor + planet.distanceFromStar) * 100 * Math.PI,
			2 * Math.PI,
		);
		emissiveMap.needsUpdate = true;
	}
	React.useEffect(() => {
		if (composer.current) composer.current.setSize(size.width, size.height);
		if (lightRef.current) lightRef.current.lookAt(planetPosition);
		camera.lookAt(planetPosition);
		if (shaderRef.current) shaderRef.current.material.transparent = true;
		if (emissiveMap) emissiveMap.needsUpdate = true;
	}, [gl, composer, size, sceneRef, lightRef, camera, shaderRef, mRef, emissiveMap, mainShader, coreShader]);
	const isShading = doesQualitySettingIncludes(graphicSettings.qualityLevel, 'Med');
	const isSpinning = [TPlanetType.giant].includes(planet.type) ? !isShading : true;
	useFrame(({ clock }) => {
		if (!ref.current || !sceneRef.current) {
			console.log('cannot animate');
			return false;
		}
		if (isSpinning) {
			const rotation =
				clock.getElapsedTime() * rotationSpeed * (planet.spinFactor * 3 + 1) * spinDirection(planet);
			ref.current.rotation.y = rotation;
			if (refCore.current) refCore.current.rotation.y = rotation;
		}
		if (isShading) {
			if (mRef.current && mainShader) {
				mainShader.uniforms.uTime.value = startTimer(planet) + clock.getElapsedTime();
				mRef.current.needsUpdate = true;
			}
			if (mCoreRef.current && coreShader) {
				coreShader.uniforms.uTime.value = startTimer(planet) + clock.getElapsedTime();
				mCoreRef.current.needsUpdate = true;
			}
		}
		gl.render(sceneRef.current, camera);
		if (composer.current) composer.current.render();
	}, 2);
	const baseColor = new THREE.Color(planet.surfaceColorHex);
	const emissiveColor = new THREE.Color(planet.atmosphericColorHex);
	const radius = renderRadius(planet.size);
	return (
		<>
			<scene ref={sceneRef} position={planetPosition}>
				<ambientLight color={0xffffff} intensity={0.5} position={ambientLightPosition} castShadow />
				<pointLight
					ref={lightRef}
					color={0xffffff}
					power={25 + 15 * (1 - (planet?.distanceFromStar || 0) ** 2)}
					decay={0}
					position={directionalLightPosition}
					castShadow={true}
				/>
				{planet.size - textureCollection.displacementRange / 2 > 0.1 && (
					<mesh ref={refCore} receiveShadow={true}>
						<PlanetMesh
							{...([TPlanetType.proto]?.includes(planet.type)
								? {
										radius:
											radius * 0.95 +
											((0.04 * (planet.gravity + planet.age + planet.magneticField)) % 1),
								  }
								: {
										size: planet.size * 0.95,
								  })}
						/>
						<meshPhongMaterial
							ref={mCoreRef}
							color={baseColor}
							map={textures.primary}
							emissive={emissiveColor}
							precision={'mediump'}
							emissiveIntensity={emissiveIntensity > 1 ? 0.5 : emissiveIntensity}
							emissiveMap={textures.primary}
							attach={'material'}
							specular={emissiveColor}
							shininess={80}
							specularMap={textures.secondary}
							onBeforeCompile={(sm) => {
								if (!isShading) return;
								setCoreShader(sm);
								sm.fragmentShader = injectShaderChunks(
									sm.fragmentShader,
									textureMapDisplacement({
										speed: 0.002,
										resolution: 0.22,
									}),
								);
								sm.uniforms.uTime = { value: startTimer(planet) };
								sm.uniforms.texOffsetMap = {
									value: textures.offset,
								};
								if (mCoreRef.current) mCoreRef.current.needsUpdate = true;
							}}
						/>
					</mesh>
				)}
				<mesh ref={ref} castShadow={true} receiveShadow={true}>
					<PlanetMesh size={planet.size} />
					<meshPhongMaterial
						ref={mRef}
						onBeforeCompile={(sm) => {
							setMainShader(sm);
							if (!isShading) return;
							const cb = shaderInjections[planet.type] ?? shaderInjections.default;
							if (cb) {
								cb(planet, sm, textures, graphicSettings, surfaceTextureCanvasId);
							}
							if (mRef.current) mRef.current.needsUpdate = true;
							//console.log(sm.fragmentShader);
						}}
						attach={'material'}
						shininess={getPlanetShininess(planet) * 10 + 0.5}
						precision={'highp'}
						specular={new THREE.Color(planet.starlightColorHex)}
						specularMap={textures.offset}
						side={THREE.DoubleSide}
						map={textures.primary}
						color={baseColor}
						{...(emissiveMap
							? {
									emissive: emissiveColor,
									emissiveMap,
									emissiveIntensity,
							  }
							: {})}
						normalMap={textures.normal}
						{...(textureCollection.displacementRange > 0
							? {
									displacementMap: textures.displacement,
									displacementScale: 20 * textureCollection.displacementRange,
									//displacementBias: -8 * textureCollection.displacementRange,
									bumpMap: textures.offset ?? textures.primary,
									bumpScale: [TPlanetType.condensed, TPlanetType.giant].includes(planet.type)
										? 0
										: 7 +
										  3 * textureCollection.displacementRange -
										  3 * planet.gravity +
										  planet.age * 5,
							  }
							: {})}
						/*[TPlanetType.giant, TPlanetType.gas].includes(planet.type)
            ? 0
            : 1.1 * (planet.age - planet.gravity + planet.size) +
              (planet.type === TPlanetType.asteroid ? 5 : planet.type === TPlanetType.proto ? 3 : 1)*/
					>
						{surfaceTextureCanvasId && <HTMLCanvasMaterial canvasID={surfaceTextureCanvasId} />}
					</meshPhongMaterial>
				</mesh>
			</scene>
			<effectComposer args={[gl]} ref={composer}>
				{doesQualitySettingIncludes(graphicSettings.qualityLevel, 'Med') ? (
					<sSAARenderPass
						attachArray="passes"
						scene={sceneRef.current}
						camera={camera}
						sampleLevel={SSAALevel[graphicSettings.qualityLevel]}
					/>
				) : (
					<renderPass attachArray="passes" scene={sceneRef.current} camera={camera} />
				)}
				<shaderPass attachArray="passes" args={[CopyShader]} renderToScreen={true} ref={shaderRef} />
			</effectComposer>
		</>
	);
};

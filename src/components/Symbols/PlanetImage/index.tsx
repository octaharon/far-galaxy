import { Canvas } from '@react-three/fiber';
import { Stats } from '@react-three/drei';
import React from 'react';
import * as THREE from 'three';
import { TPlanetType } from '../../../../definitions/space/Planets';
import { getPlanetDeterminedRadix, getPlanetHash } from '../../../../definitions/space/Planets/helpers';
import { withGraphicSettings } from '../../../contexts/GraphicSettings';
import { doesQualitySettingIncludes } from '../../../contexts/GraphicSettings/helpers';
import { classnames } from '../../UI';
import TextureMapper, { TTextureRotation } from '../TextureMapper';
import { camOptions } from './constants';
import { getPlanetTextures, TPlanetImageProps } from './helpers';
import style from './index.scss';
import AtmosphereLayer from './partials/atmosphere';
import { BackgroundLayer } from './partials/background';
import { Bloom } from './partials/bloom';
import { Planet } from './partials/planet';
type StateType = { textureReady: boolean; textureId: string };
const getTextureId = (p: TPlanetImageProps) => `txmapper-${getPlanetHash(p)}`;
class PlanetImage extends React.PureComponent<TPlanetImageProps, StateType> {
	public state = { textureReady: false, textureId: '' };

	constructor(props: TPlanetImageProps) {
		super(props);
		this.state = {
			textureReady: false,
			textureId: getTextureId(this.props),
		};
	}

	public static getDerivedStateFromProps(nextProps: TPlanetImageProps, prevState: StateType): StateType {
		return getTextureId(nextProps) !== prevState.textureId
			? {
					textureId: '',
					textureReady: false,
			  }
			: prevState;
	}

	/*componentDidUpdate(prevProps: Readonly<TPlanetImageProps>, prevState: Readonly<{ textureReady: boolean }>) {
		if (getPlanetH) {
			this.setState({
				textureReady: false,
				textureId: '',
			});
		}
	}*/

	render() {
		//console.log(`texture id: ${this.state.textureId}, planet:`, this.props.planet, this.props.graphicSettings);
		const textureCollection = getPlanetTextures(this.props.planet);
		const iceCapTexture = require('../../../../assets/img/textures/EmissiveMaps/1440p/ice-cap.png');
		const color = new THREE.Color(this.props.planet.surfaceColorHex);
		const atmColor = new THREE.Color(this.props.planet.atmosphericColorHex);
		// array for determinated texture rotation values base on planet props
		const rotations: TTextureRotation[] = getPlanetDeterminedRadix(this.props.planet, 4, 7) as TTextureRotation[];
		const colorKey = '#' + color.getHex().toString(16).padStart(6, '0');
		const atmColorKey = '#' + atmColor.getHex().toString(16).padStart(6, '0');
		const textureMapper: Record<TPlanetType, Partial<React.ComponentProps<typeof TextureMapper>>> = {
			[TPlanetType.condensed]: {
				baseImage: textureCollection.secondary,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.primary]: {
						blending: 'color-burn',
						opacity: 0.5,
						rotation: rotations[1],
					},
					[colorKey]: { opacity: 0.8 },
					[textureCollection.offset ?? textureCollection.secondary]: {
						blending: 'overlay',
						opacity: 0.5,
						blur: 3,
						rotation: rotations[2],
					},
					[atmColorKey]: { opacity: 0.5 },
				},
			},
			[TPlanetType.giant]: {
				baseImage: textureCollection.secondary,
				baseRotation: rotations[0],
				blendImages: {
					[colorKey]: { opacity: 0.7 },
					[textureCollection.primary]: {
						blending: 'hue',
						opacity: 0.4,
						blur: 5,
						rotation: rotations[1],
					},
					[textureCollection.offset ?? textureCollection.secondary]: {
						blending: 'hard-light',
						opacity: 0.3,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.asteroid]: {
				baseImage: textureCollection.primary,
				baseRotation: rotations[0],
				blendImages: {
					[colorKey]: { opacity: 0.7 },
					[textureCollection.offset]: {
						blending: 'color-dodge',
						opacity: 0.3,
						rotation: rotations[1],
					},
					[textureCollection.secondary]: {
						blending: 'color-burn',
						opacity: 0.3,
						blur: 3,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.volcanic]: {
				baseImage: textureCollection.primary,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.secondary]: {
						blending: 'color-burn',
						opacity: 0.8,
						rotation: rotations[1],
					},
					[colorKey]: { opacity: 0.8 },
					[textureCollection.offset]: {
						blending: 'multiply',
						opacity: 0.5,
						blur: 2,
						rotation: rotations[2],
					},
					[textureCollection.emissive]: {
						blending: 'hard-light',
						opacity: 0.4,
						rotation: rotations[3],
					},
				},
			},
			[TPlanetType.hycean]: {
				baseImage: textureCollection.secondary,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.primary]: {
						blending: 'screen',
						rotation: rotations[1],
						opacity: 0.7,
					},
					[colorKey]: {
						opacity: 0.8,
						blending: 'hue',
					},
					[textureCollection.offset]: {
						blending: 'color-burn',
						rotation: rotations[3],
						blur: 2,
						opacity: 0.5,
					},
					[textureCollection.secondary]: {
						blending: 'luminosity',
						rotation: rotations[2],
						opacity: 0.5,
					},
				},
			},
			[TPlanetType.ocean]: {
				baseImage: textureCollection.primary,
				blur: 0,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.secondary]: {
						blending: 'color-burn',
						opacity: 0.43,
						rotation: rotations[4],
					},
					[colorKey]: { opacity: 0.5 },
					[textureCollection.emissive]: {
						blending: 'multiply',
						opacity: 0.35,
						rotation: rotations[3],
					},
					[textureCollection.primary]: {
						blending: 'soft-light',
						opacity: 1,
						blur: 4,
						rotation: rotations[1],
					},
					[textureCollection.offset]: {
						blending: 'hard-light',
						opacity: 0.5,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.ice]: {
				baseImage: textureCollection.secondary,
				blur: 0,
				baseRotation: rotations[0],
				blendImages: {
					[colorKey]: { opacity: 0.5 },
					[textureCollection.primary]: {
						blending: 'color-burn',
						opacity: 0.3,
						rotation: rotations[1],
					},
					[textureCollection.offset]: {
						blending: 'overlay',
						opacity: 0.5,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.pulsar]: {
				baseImage: textureCollection.primary,
				blur: 0,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.offset]: {
						blending: 'overlay',
						opacity: 0.5,
						rotation: rotations[2],
					},
					[colorKey]: { opacity: 0.5 },
					[textureCollection.secondary]: {
						blending: 'hard-light',
						opacity: 0.4,
						rotation: rotations[1],
					},
					[textureCollection.emissive]: {
						blending: 'color-dodge',
						opacity: 0.5,
						blur: 4,
						rotation: rotations[3],
					},
				},
			},
			[TPlanetType.arid]: {
				baseImage: textureCollection.primary,
				baseRotation: rotations[0],
				blendImages: {
					[colorKey]: { opacity: 0.8 },
					[textureCollection.secondary]: {
						blending: 'color-dodge',
						opacity: 0.3,
						rotation: rotations[1],
					},
					[textureCollection.offset]: {
						blending: 'hard-light',
						opacity: 0.3,
						blur: 1,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.chtonian]: {
				baseImage: textureCollection.primary,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.secondary]: {
						blending: 'overlay',
						opacity: 0.8,
						rotation: rotations[1],
					},
					[colorKey]: { opacity: 0.5 },
					[textureCollection.offset]: {
						blending: 'color-burn',
						opacity: 0.4,
						blur: 3,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.frozen]: {
				baseImage: textureCollection.primary,
				blur: 0,
				baseRotation: rotations[0],
				blendImages: {
					[colorKey]: { opacity: 0.5 },
					[textureCollection.secondary]: {
						blending: 'overlay',
						opacity: 1,
						rotation: rotations[1],
					},
					[textureCollection.offset]: {
						blending: 'color-burn',
						opacity: 0.1,
						blur: 2,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.hollow]: {
				baseImage: textureCollection.primary,
				blur: 0,
				baseRotation: rotations[0],
				blendImages: {
					[colorKey]: { opacity: 0.5 },
					[textureCollection.secondary]: {
						blending: 'color-dodge',
						opacity: 0.3,
						rotation: rotations[1],
					},
					[textureCollection.offset]: {
						blending: 'hard-light',
						opacity: 0.5,
						blur: 7,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.proto]: {
				baseImage: textureCollection.primary,
				blur: 0,
				baseRotation: rotations[0],
				blendImages: {
					[colorKey]: { opacity: 0.5 },
					[textureCollection.secondary]: {
						blending: 'color-burn',
						opacity: 0.5,
						blur: 2,
						rotation: rotations[1],
					},
					[textureCollection.offset]: {
						blending: 'hard-light',
						opacity: 0.5,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.tomb]: {
				baseImage: textureCollection.primary,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.secondary]: {
						blending: 'color-burn',
						opacity: 0.7,
						rotation: rotations[1],
					},
					[colorKey]: { opacity: 0.5 },
					[textureCollection.offset]: {
						blending: 'color-dodge',
						opacity: 0.2,
						blur: 5,
						rotation: rotations[2],
					},
				},
			},
			[TPlanetType.carbon]: {
				baseImage: textureCollection.primary,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.secondary]: {
						blending: 'soft-light',
						opacity: 0.8,
						rotation: rotations[1],
					},
					[textureCollection.offset]: {
						blending: 'color-dodge',
						opacity: 0.3,
						rotation: rotations[2],
					},
					[colorKey]: { opacity: 0.5 },
				},
			},
			[TPlanetType.terra]: {
				baseImage: textureCollection.primary,
				baseRotation: rotations[0],
				blendImages: {
					[colorKey]: { opacity: 0.7 },
					[textureCollection.secondary]: {
						blending: 'difference',
						opacity: 1,
						rotation: rotations[1],
					},
					[textureCollection.offset]: {
						blending: 'hard-light',
						opacity: 0.4,
						blur: 3,
						rotation: rotations[2],
					},
					[iceCapTexture]: {
						blending: 'screen',
						opacity: 1,
						rotation: (rotations[3] % 2) * 2,
					},
				},
			},
			[TPlanetType.rogue]: {
				baseImage: textureCollection.primary,
				blur: 2,
				baseRotation: rotations[0],
				blendImages: {
					[textureCollection.secondary]: {
						blending: 'hard-light',
						opacity: 0.5,
						rotation: rotations[1],
					},
					[colorKey]: { opacity: 0.5 },
					[textureCollection.offset]: {
						blending: 'color-burn',
						blur: 7,
						opacity: 0.4,
						rotation: rotations[2],
					},
				},
			},
		};
		return (
			<>
				<TextureMapper
					graphicSettings={this.props.graphicSettings}
					id={getTextureId(this.props)}
					key={getTextureId(this.props)}
					baseImage={textureCollection.primary}
					{...(textureMapper[this.props.planet.type] || textureMapper[TPlanetType.condensed])}
					onReady={() => this.setState({ textureReady: true, textureId: getTextureId(this.props) })}
				/>
				{this.state.textureId && this.state.textureReady && (
					<React.Suspense fallback={<div className="preloader-img" />}>
						<div className={classnames(style.planetImage, this.props.className, this.state.textureId)}>
							<Canvas
								camera={camOptions}
								key={getTextureId(this.props)}
								gl={{
									alpha: true,
									premultipliedAlpha: false,
									sortObjects: false,
									autoClear: false,
									antialias: true,
									physicallyCorrectLights: true,
								}}
							>
								<BackgroundLayer {...this.props} />
								{doesQualitySettingIncludes(this.props.graphicSettings.qualityLevel, 'Med') &&
									![TPlanetType.asteroid].includes(this.props.planet.type) && (
										<Bloom {...this.props} />
									)}
								<Planet
									{...this.props}
									surfaceTextureCanvasId={this.state.textureId}
									textureCollection={textureCollection}
								/>
								{this.props.planet?.atmosphericDensity >= 0.25 && <AtmosphereLayer {...this.props} />}
							</Canvas>
						</div>
					</React.Suspense>
				)}
			</>
		);
	}
}

const UIPlanetImage = withGraphicSettings(PlanetImage);
export default UIPlanetImage;

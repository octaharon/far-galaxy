import { extend, ReactThreeFiber } from '@react-three/fiber';
import { BloomPass } from 'three/examples/jsm/postprocessing/BloomPass';
import { FilmPass } from 'three/examples/jsm/postprocessing/FilmPass';
import { SMAAPass } from 'three/examples/jsm/postprocessing/SMAAPass';
import { SSAARenderPass } from 'three/examples/jsm/postprocessing/SSAARenderPass';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import AtmosphereMaterial from './partials/atmosphereMaterial';

(() => {
	console.log('extending interfaces');
	extend({
		SSAARenderPass,
		SMAAPass,
		UnrealBloomPass,
		AtmosphereMaterial,
		FilmPass,
		BloomPass,
		RenderPass,
		ShaderPass,
		EffectComposer,
	});
})();
declare global {
	namespace JSX {
		interface IntrinsicElements {
			unrealBloomPass: ReactThreeFiber.Object3DNode<
				UnrealBloomPass,
				ConstructorParameters<typeof UnrealBloomPass>
			>;
			bloomPass: ReactThreeFiber.Object3DNode<BloomPass, ConstructorParameters<typeof BloomPass>>;
			filmPass: ReactThreeFiber.Object3DNode<FilmPass, ConstructorParameters<typeof FilmPass>>;
			smaaPass: ReactThreeFiber.Object3DNode<SMAAPass, ConstructorParameters<typeof SMAAPass>>;
			sSAARenderPass: ReactThreeFiber.Object3DNode<SSAARenderPass, ConstructorParameters<typeof SSAARenderPass>>;
			atmosphereMaterial: ReactThreeFiber.Object3DNode<
				AtmosphereMaterial,
				ConstructorParameters<typeof AtmosphereMaterial>
			>;
		}
	}
}

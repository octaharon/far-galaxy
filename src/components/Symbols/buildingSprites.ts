import path from 'path';
import InterfaceTypes from '../../../definitions/core/InterfaceTypes';
import { TBaseBuildingType } from '../../../definitions/orbital/types';
import { polygonToCss, polygonToSvgPath } from '../../utils/polygonTools';
import { TBuildingSprite, TBuildingSpriteFeatureParams } from './BuildingSprite/types';

const BuildingSpriteCollection: {
	[T in TBaseBuildingType]?: Record<number, TBuildingSprite<T>>;
} = {};

const spriteEnumerableKeys = { output: true, resource: true, output_light: true, resource_light: true };
const spriteFeaturesKeys = { output_features: true, resource_features: true, features: true };
type TSpriteEnumerableKeys = keyof typeof spriteEnumerableKeys;
type TSpriteFeatureKey = keyof typeof spriteFeaturesKeys;
const isEnumberableSpriteKey = (str: any): str is TSpriteEnumerableKeys =>
	(Object.keys(spriteEnumerableKeys) as any[]).includes(str);
const isSpriteFeatureKey = (str: any): str is TSpriteFeatureKey =>
	(Object.keys(spriteFeaturesKeys) as any[]).includes(str);

const imageSize = 500;

function requireAll<K extends TBaseBuildingType>(
	r: __WebpackModuleApi.RequireContext,
	cache: Record<number, TBuildingSprite<K>>,
	type: K,
) {
	return r.keys().forEach((key) => {
		const buildingId = parseInt(key.toString().replace(/[^\d]+/, ''), 10);
		if (!cache[buildingId]) cache[buildingId] = {};
		const sprite = cache[buildingId] as TBuildingSprite<K>;

		if (key.match(/outline\.json$/)) {
			// converting outline mask to style
			const maskPolygon = r(key) as InterfaceTypes.TPolygon;
			sprite.outlinePath = polygonToSvgPath(maskPolygon);
			sprite.outlineStyle = {
				clipPath: polygonToCss(maskPolygon, {
					x: imageSize,
					y: imageSize,
				}),
			};
			return;
		}

		if (key.match(/resource_features\.json$/)) {
			(sprite as TBuildingSprite<TBaseBuildingType.facility>).resource_features = r(
				key,
			) as TBuildingSpriteFeatureParams;
			return;
		}

		if (key.match(/output_features\.json$/)) {
			(sprite as TBuildingSprite<TBaseBuildingType.facility>).output_features = r(
				key,
			) as TBuildingSpriteFeatureParams;
			return;
		}

		if (key.match(/features\.json$/)) {
			(sprite as TBuildingSprite<TBaseBuildingType.factory>).features = r(key) as TBuildingSpriteFeatureParams;
			return;
		}

		const baseName = path.basename(key, '.png');
		switch (true) {
			case baseName === 'base':
				sprite.base = r(key);
				break;
			case baseName === 'outline':
				sprite.outline = r(key);
				break;
			case baseName === 'mask':
				sprite.mask = r(key);
				break;
			case baseName === 'active':
				switch (type) {
					case TBaseBuildingType.facility:
						break;
					default:
						(sprite as TBuildingSprite<TBaseBuildingType.factory>).active = r(key);
				}
				break;
			case baseName === 'light':
				switch (type) {
					case TBaseBuildingType.facility:
						break;
					default:
						(sprite as TBuildingSprite<TBaseBuildingType.factory>).light = r(key);
				}
				break;
			case isEnumberableSpriteKey(baseName) && type === TBaseBuildingType.facility:
				(sprite as TBuildingSprite<TBaseBuildingType.facility>)[baseName as TSpriteEnumerableKeys] = r(key);
				break;
			case isSpriteFeatureKey(baseName):
				if (baseName === 'features')
					(sprite as TBuildingSprite<TBaseBuildingType.factory>).features_image = r(key);
				if (baseName === 'output_features')
					(sprite as TBuildingSprite<TBaseBuildingType.facility>).output_features_image = r(key);
				if (baseName === 'resource_features')
					(sprite as TBuildingSprite<TBaseBuildingType.facility>).resource_features_image = r(key);
				break;
			/*case baseName.match(/^output_feature_(\d+)$/gi) && type === TBaseBuildingType.facility:
				(sprite as TBuildingSprite<TBaseBuildingType.facility>).output_features ??= [];
				(sprite as TBuildingSprite<TBaseBuildingType.facility>).output_features[
					parseInt(baseName.replace(/^\D+/gi, ''), 10) - 1
				] = r(key);
				break;
			case baseName.match(/^resource_feature_(\d+)$/gi) && type === TBaseBuildingType.facility:
				(sprite as TBuildingSprite<TBaseBuildingType.facility>).resource_features ??= [];
				(sprite as TBuildingSprite<TBaseBuildingType.facility>).resource_features[
					parseInt(baseName.replace(/^\D+/gi, ''), 10) - 1
				] = r(key);
				break;
			case baseName.match(/^feature_(\d+)$/gi) && type !== TBaseBuildingType.facility:
				(sprite as TBuildingSprite<TBaseBuildingType.factory>).features ??= [];
				(sprite as TBuildingSprite<TBaseBuildingType.factory>).features[
					parseInt(baseName.replace(/^\D+/gi, ''), 10) - 1
				] = r(key);
				break;*/

			default:
		}
	});
}

Object.values(TBaseBuildingType).forEach((buildingType) => {
	BuildingSpriteCollection[buildingType] = {};
});
requireAll(
	require.context('../../../assets/img/buildings/factories', true, /\.(png|json)$/),
	BuildingSpriteCollection[TBaseBuildingType.factory],
	TBaseBuildingType.factory,
);
requireAll(
	require.context('../../../assets/img/buildings/facilities', true, /\.(png|json)$/),
	BuildingSpriteCollection[TBaseBuildingType.facility],
	TBaseBuildingType.facility,
);
requireAll(
	require.context('../../../assets/img/buildings/establishments', true, /\.(png|json)$/),
	BuildingSpriteCollection[TBaseBuildingType.establishment],
	TBaseBuildingType.establishment,
);

export default BuildingSpriteCollection;

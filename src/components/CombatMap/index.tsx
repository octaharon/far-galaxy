import Style from './index.scss';

export const MAP_RENDER_SIZE = 10000;
export const MAP_PIXEL_RATIO = 1;
export const MAP_VIEWBOX = MAP_RENDER_SIZE / MAP_PIXEL_RATIO;

import * as React from 'react';
import { animated, config, SpringConfig, useSpring } from 'react-spring';
import { useGesture } from 'react-use-gesture';
import * as rematrix from 'rematrix';
import InterfaceTypes from '../../../../definitions/core/InterfaceTypes';
import BasicMaths from '../../../../definitions/maths/BasicMaths';
import Geometry from '../../../../definitions/maths/Geometry';
import Vectors from '../../../../definitions/maths/Vectors';
import { TMapParameters } from '../../../../definitions/tactical/map';
import SVGIconContainer from '../../UI/SVG/IconContainer';
import Style from '../index.scss';
import { Render_MapBoundaryShapeID } from '../types';

export type TMapContainerVector = {
	scale: number;
	translateX: number;
	translateY: number;
	rotate: number;
};

export type TMapContainerState = TMapContainerVector & {
	minScaleFactor?: number;
	radius: number;
	windowSize: number;
	containerBoundingRect?: ClientRect | DOMRect;
};

export type TMapRenderState = TMapContainerState & Partial<TMapParameters>;

export const getTransformStyle = <T extends boolean>(state: TMapContainerVector): string => {
	const matrix = [
		rematrix.translate(state.translateX, state.translateY),
		rematrix.scale(state.scale),
		rematrix.rotateZ(state.rotate),
	].reduce(rematrix.multiply);

	return rematrix.toString(matrix);
};
export const translatePageXYToCenterOffset = (
	v: InterfaceTypes.IVector,
	state: TMapContainerState,
): InterfaceTypes.IVector => {
	if (!state.containerBoundingRect) return v;
	const { left, height, width, top } = state.containerBoundingRect;
	return Vectors.create(
		{
			x: left + width / 2,
			y: top + height / 2,
		},
		v,
	);
};
export const translatePageXYToRenderXY = (
	v: InterfaceTypes.IVector,
	state: TMapContainerState,
): InterfaceTypes.IVector => {
	const offsetXY = translatePageXYToCenterOffset(v, state);
	const translateXY = {
		x: (offsetXY.x - state.translateX) / state.scale,
		y: (offsetXY.y - state.translateY) / state.scale,
	};
	if (!state.rotate) return translateXY;
	const polarXY = Geometry.vector2polar(translateXY);
	polarXY.alpha -= (state.rotate * Math.PI) / 180;
	return Geometry.polar2vector(polarXY);
};
/**
 * @param {TMapContainerState} curState
 * @returns {TMapContainerVector}
 */
export const getTransformVector = (curState: TMapContainerState): TMapContainerVector => {
	const newZoom = BasicMaths.clipValue({
		min: curState.minScaleFactor,
		max: 1,
		x: curState.scale,
	});
	const maxR =
		BasicMaths.normalizeValue({
			x: newZoom,
			min: curState.minScaleFactor,
			max: 1,
		}) *
		(curState.radius - curState.windowSize / 2);
	const v: InterfaceTypes.IVector = { x: curState.translateX, y: curState.translateY };
	const p = Geometry.vector2polar(v);
	if (p.r > maxR) {
		p.r = maxR;
		const v2 = Geometry.polar2vector(p);
		return {
			translateX: v2.x,
			translateY: v2.y,
			scale: newZoom,
			rotate: curState.rotate,
		};
	}
	return {
		translateX: v.x,
		translateY: v.y,
		scale: newZoom,
		rotate: curState.rotate,
	};
};

const ArrayFromTransformVector = (t: TMapContainerVector): [number, number, number, number] => [
	t.translateX,
	t.translateY,
	t.scale,
	t.rotate,
];
const TransformVectorFromArray = (t: [number, number, number, number]): TMapContainerVector => ({
	translateX: t[0],
	translateY: t[1],
	scale: t[2],
	rotate: t[3],
});

export const Viewbox: React.FC<React.PropsWithChildren<TMapRenderState>> = React.memo((props) => {
	if (!props) return null;
	const startVector = getTransformVector(props);
	const [state, setState] = React.useState({
		...props,
		zoomCenterX: 0,
		zoomCenterY: 0,
		...startVector,
	} as TMapContainerState);
	const [transformVector, transformAnimationApi] = useSpring(
		() => ({
			values: ArrayFromTransformVector(startVector),
		}),
		[props.translateX, props.translateY, props.scale, props.rotate],
	);

	const setTransform = (s: TMapContainerVector, config: SpringConfig = {}) =>
		transformAnimationApi.start({
			values: ArrayFromTransformVector(s),
			config,
		});

	const stateUpdateHandler = (s: Partial<TMapContainerState>) => {
		const newState = {
			...state,
			...s,
		};
		setState(newState);
		return newState;
	};

	const vectorToState = (s: TMapContainerVector) => ({
		...state,
		...s,
	});

	const vectorUpdateHandler = (s: TMapContainerVector) => {
		const newState = vectorToState(s);
		return stateUpdateHandler(newState);
	};

	const setTranslate = (x: number, y: number) => stateUpdateHandler({ translateX: x, translateY: y });
	const setZoom = (scale: number) => stateUpdateHandler({ scale });
	const setRotate = (rotate: number) => stateUpdateHandler({ rotate });

	const bindGestures = useGesture({
		onWheel: (p) => {
			const { event, movement, memo = getTransformVector(state), last, altKey } = p;
			const scrollPoint = [0, 0];
			if (event && event.type === 'wheel') {
				const [x, y] = [event.pageX, event.pageY];
				const localState = vectorToState(memo);
				const localPoint = translatePageXYToRenderXY({ x, y }, localState);
				/*stateUpdateHandler({
												 zoomCenterX: localPoint.x,
												 zoomCenterY: localPoint.y
												 });*/
			}
			if (!altKey) {
				// memo[0] = scrollPoint[0];
				// memo[1] = scrollPoint[1];
				const newState = {
					...vectorToState(memo),
					scale: (memo[2] || state.scale) * (1 - movement[1] * 0.001),
				};
				const v = getTransformVector(newState);
				setTransform(v, config.default);
				if (last) vectorUpdateHandler(v);
				return v;
			} else {
				const rCoef =
					Vectors.module()({
						x: state.translateX,
						y: state.translateY,
					}) /
						state.radius +
					1;
				const newState = {
					...vectorToState(memo),
					rotate: (memo[3] || state.rotate) + (movement[1] * 0.05) / rCoef / rCoef,
				};
				const v = getTransformVector(newState);
				setTransform(v, {
					mass: Math.min(5, rCoef + Math.abs(movement[1]) / 100),
					tension: Math.max(100, 300 + Math.abs(movement[1]) / 10),
					friction: Math.round(20 * Math.sqrt(state.scale) * rCoef) + 50,
				});
				if (last) vectorUpdateHandler(v);
				return v;
			}
		},
		onDrag: ({ down, movement, last, velocity, memo = getTransformVector(state) }) => {
			const offset = [state.translateX + movement[0], state.translateY + movement[1]];
			const newState = {
				...vectorToState(memo),
				translateX: offset[0],
				translateY: offset[1],
			};
			const v = getTransformVector(newState);
			velocity = BasicMaths.clipValue({ x: velocity, min: 1, max: 8 });
			setTransform(v, {
				mass: velocity / 5,
				tension: 1000,
				friction: 50,
			});
			if (last) vectorUpdateHandler(v);
			return v;
		},
	});
	const style = {
		transform: transformVector.values.to((...values) => getTransformStyle(TransformVectorFromArray(values))),
	};
	return (
		<div className={Style.combatMapCanvas} {...bindGestures()}>
			<animated.svg
				style={style}
				className={Style.combatMapRender}
				viewBox={`-${state.radius} -${state.radius} ${2 * state.radius} ${2 * state.radius}`}>
				<defs>
					<mask id="mapBoundaryClip">
						<rect
							x={-state.radius}
							y={-state.radius}
							width={state.radius * 2}
							height={state.radius * 2}
							fill="#000"
						/>
						<use {...SVGIconContainer.getXLinkProps(Render_MapBoundaryShapeID)} fill="#FFF" />
					</mask>
					<radialGradient id="mapBoundaryShade">
						<stop offset="0" stopColor="#FFF" stopOpacity="0" />
						<stop offset="0.992" stopColor="#FFF" stopOpacity="0" />
						<stop offset="1" stopColor="#FFF" stopOpacity="1" />
					</radialGradient>
				</defs>
				<g id="mapContainerVisibilityClip" mask="url(#mapBoundaryClip)">
					{props.children}
					<use
						{...SVGIconContainer.getXLinkProps(Render_MapBoundaryShapeID)}
						fill="url(#mapBoundaryShade)"
						stroke="#FFF"
						strokeWidth="2px"
						style={{ vectorEffect: 'non-scaling-stroke' }}
					/>
					<line
						x1={-state.radius}
						x2={state.radius}
						y1={0}
						y2={0}
						stroke={'#000'}
						fill={'none'}
						strokeWidth={'6px'}
						strokeOpacity={0.7}
						strokeDasharray={'20 40'}
					/>
					<line
						x1={0}
						x2={0}
						y1={0}
						y2={state.radius}
						stroke={'#000'}
						fill={'none'}
						strokeWidth={'4px'}
						strokeOpacity={0.7}
						strokeDasharray={'20 40'}
					/>
					<text x={20} y={50}>
						20,50
					</text>
					<text x={20} y={-50}>
						20,-50
					</text>
					<text x={-150} y={40}>
						-150,40
					</text>
					<text x={-90} y={-100}>
						-90,-100
					</text>
					{new Array(9).fill(0, 0, 9).map((V, ix) => (
						<circle
							key={ix}
							r={`${(state.radius / 10) * (ix + 1)}`}
							className={Style.constantStroke}
							cx={0}
							cy={0}
							stroke={'#000'}
							fill={'none'}
							strokeWidth={'4px'}
							strokeOpacity={0.6}
							strokeDasharray={'25 25'}
						/>
					))}
				</g>
			</animated.svg>
		</div>
	);
});

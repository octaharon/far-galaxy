import * as React from 'react';
import { TCombatMapProps } from '../../../../definitions/tactical/map';
import { IUICommonProps } from '../../UI';
import SVGIconContainer from '../../UI/SVG/IconContainer';
import { MAP_RENDER_SIZE, MAP_VIEWBOX } from '../index';
import Style from '../index.scss';
import { SVGMapTerrainRenderLayer } from '../MapTerrainLayer';
import { Render_MapBoundaryShapeID } from '../types';
import { TMapContainerState, Viewbox } from '../Viewbox';

export type TMapContainerProps = IUICommonProps & Partial<TCombatMapProps> & {};

SVGIconContainer.registerSymbol(Render_MapBoundaryShapeID, <circle r={MAP_VIEWBOX / 2} cx="0" cy="0" />);

let refContainer: HTMLElement = null;

export default class CMapContainer extends React.PureComponent<TMapContainerProps, TMapContainerState> {
	public static getClass(): string {
		return Style.combatMapContainer;
	}

	public constructor(props: TMapContainerProps) {
		super(props);
		this.state = {
			scale: 1,
			translateX: 0,
			translateY: 0,
			rotate: 0,
			minScaleFactor: 0,
			radius: MAP_VIEWBOX / 2,
			windowSize: MAP_RENDER_SIZE / 1000,
		};
	}

	public componentDidMount() {
		const containerBoundingRect = refContainer ? refContainer.getBoundingClientRect() : undefined;
		const windowSize = containerBoundingRect ? containerBoundingRect.width : window ? window.innerHeight : 1000;
		const minScaleFactor = windowSize / MAP_RENDER_SIZE;
		if (document) {
			document.getElementById(Render_MapBoundaryShapeID).setAttribute('r', this.state.radius.toFixed(2));
		}
		this.setState({ scale: minScaleFactor, minScaleFactor, windowSize, containerBoundingRect });
	}

	public render() {
		return (
			<div
				ref={(e) => (refContainer = e)}
				className={[CMapContainer.getClass(), this.props.className || ''].join(' ')}
			>
				{!!this.state.containerBoundingRect && (
					<Viewbox {...this.state} {...this.props.parameters}>
						<SVGMapTerrainRenderLayer {...this.props.parameters} {...this.state}>
							{/* <image xlinkHref={require('../../../../assets/img/units/tank.png')} x={-540} y={2200} width={96}
                           height={207} style={{transform: 'rotate(-55deg)'}}/>*/}
							{this.props.children}
						</SVGMapTerrainRenderLayer>
					</Viewbox>
				)}
			</div>
		);
	}
}

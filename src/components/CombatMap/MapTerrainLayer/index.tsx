import * as React from 'react';
import { $enum } from 'ts-enum-util';
import _ from 'underscore';
import BasicMaths from '../../../../definitions/maths/BasicMaths';
import Geometry from '../../../../definitions/maths/Geometry';
import { TMapTerrainType, TPlanetSurfaceProps } from '../../../../definitions/space/Planets';
import { getTerrainTypeFromProps } from '../../../../definitions/space/Planets/helpers';
import { getTextureGeneratedResolution } from '../../../contexts/GraphicSettings/helpers';
import { getMapTerrainTexture } from '../../Symbols/PlanetImage/helpers';
import SVGIconContainer, {
	SVGFilterSaturate20ID,
	SVGFilterSaturate40ID,
	SVGFilterSaturate60ID,
	SVGFilterSaturate80ID,
} from '../../UI/SVG/IconContainer';
import { MAP_PIXEL_RATIO, MAP_RENDER_SIZE, MAP_VIEWBOX } from '../index';
import { Render_MapBoundaryShapeID, Render_MapTerrainLayerId } from '../types';
import { TMapRenderState } from '../Viewbox';
import Style from './index.scss';

export type TMapTerrainEffectType = 'gas' | 'steam' | 'dust' | 'foliage' | 'fog';

const MapTerrainEffectClass: Record<TMapTerrainEffectType, string> = {
	gas: Style.gasLike,
	fog: Style.gasLike,
	dust: Style.dustLike,
	foliage: Style.forestLike,
	steam: Style.steamLike,
};

export function getTerrainEffectFromProps(
	p: Partial<TPlanetSurfaceProps>,
): TMapTerrainEffectType | TMapTerrainEffectType[] {
	switch (true) {
		case p.gravity <= 0.25:
			return null;
		case p.temperature > 0.9:
			switch (true) {
				case p.gravity <= 0.25:
					return 'dust';
				case p.waterPresence > 0.7:
					return ['steam', 'gas'];
				case p.atmosphericDensity >= 0.25:
					return ['dust', 'gas'];
				default:
					return 'gas';
			}
		case p.temperature >= 0.75:
			return p.waterPresence >= 0.1 && p.atmosphericDensity >= 0.25
				? ['foliage', 'steam']
				: p.waterPresence >= 0.25
				? ['gas', 'steam']
				: p.atmosphericDensity >= 0.25
				? ['dust', 'steam']
				: 'steam';
		case p.temperature >= 0.5:
			switch (true) {
				case p.radiationLevel >= 0.5:
					return ['gas', 'dust'];
				case p.waterPresence >= 0.5 && p.atmosphericDensity >= 0.25:
					return ['foliage', 'steam'];
				case p.waterPresence >= 0.1 && p.atmosphericDensity >= 0.25:
					return 'foliage';
				default:
					return 'steam';
			}
		case p.temperature >= 0.25:
			return p.radiationLevel >= 0.5 ? 'gas' : p.atmosphericDensity >= 0.25 ? 'dust' : null;
		default:
			return null;
	}
}

export function getTerrainFilterFromProps(p: Partial<TPlanetSurfaceProps>) {
	switch (true) {
		case p.temperature + p.waterPresence >= 1.6:
			return getMapTerrainFilterID('fog');
		case p.temperature >= 0.75 && p.atmosphericDensity <= 0.2:
			return SVGFilterSaturate80ID;
		case p.temperature >= 0.5 && p.atmosphericDensity <= 0.4:
			return SVGFilterSaturate60ID;
		case p.temperature >= 0.25 && p.atmosphericDensity <= 0.6:
			return SVGFilterSaturate60ID;
		case p.temperature >= 0.25 && p.atmosphericDensity <= 0.85:
			return SVGFilterSaturate40ID;
		default:
			return SVGFilterSaturate20ID;
	}
}

export const getMapTerrainSize = (p: TMapTerrainType) => 2048;

export const getMapTerrainPatternID = (type: TMapTerrainType) => `__${type}-pattern`;
export const getMapTerrainFilterID = (effect: TMapTerrainEffectType) => `terrain-${effect}-filter`;

SVGIconContainer.registerFilter(
	getMapTerrainFilterID('fog'),
	<filter
		x="-20%"
		y="-20%"
		width="140%"
		height="140%"
		filterUnits="objectBoundingBox"
		primitiveUnits="userSpaceOnUse"
		colorInterpolationFilters="sRGB"
	>
		<feGaussianBlur stdDeviation="4 4" in="SourceGraphic" edgeMode="none" result="blur" />
		<feTurbulence
			type="turbulence"
			baseFrequency="0.015 0.015"
			numOctaves="2"
			seed="2"
			stitchTiles="stitch"
			result="turbulence"
		>
			<animate
				attributeName="seed"
				attributeType="XML"
				from="1"
				to="100"
				dur="5s"
				repeatCount="indefinite"
				fill="freeze"
			/>
		</feTurbulence>
		<feDisplacementMap
			in="blur"
			in2="turbulence"
			scale="20"
			xChannelSelector="B"
			yChannelSelector="B"
			result="displacementMap"
		/>
		<feBlend mode="lighten" in="SourceGraphic" in2="displacementMap" result="blend" />
		<feColorMatrix type="saturate" values="0.4" in="blend" result="colormatrix" />
	</filter>,
	true,
)
	.registerFilter(
		getMapTerrainFilterID('steam'),
		<filter
			x="-200%"
			y="-200%"
			width="500%"
			height="500%"
			filterUnits="objectBoundingBox"
			primitiveUnits="objectBoundingBox"
			colorInterpolationFilters="sRGB"
		>
			<feTurbulence
				type="fractalNoise"
				baseFrequency="0.025 0.035"
				numOctaves="6"
				seed="55"
				stitchTiles="stitch"
				result="turbulence"
			></feTurbulence>
			<feDiffuseLighting
				surfaceScale="12"
				diffuseConstant="3"
				lightingColor="#808080"
				in="turbulence"
				result="diffuseLighting"
			>
				<feDistantLight azimuth="-30" elevation="16">
					<animate
						attributeName="azimuth"
						from="-360"
						to="0"
						repeatCount="indefinite"
						dur="6.8s"
						fill="freeze"
					/>
				</feDistantLight>
			</feDiffuseLighting>
			<feComposite in="diffuseLighting" in2="SourceAlpha" operator="in" result="composite" />
			<feConvolveMatrix
				order="3 3"
				kernelMatrix="3 -1 1
 -1 0 -3
1 -1 1"
				divisor="1"
				bias="0.5"
				targetX="0"
				targetY="0"
				edgeMode="wrap"
				preserveAlpha="true"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				in="composite"
				result="convolveMatrix"
			/>
		</filter>,
		true,
	)
	.registerFilter(
		getMapTerrainFilterID('gas'),
		<filter
			x="-200%"
			y="-200%"
			width="500%"
			height="500%"
			filterUnits="objectBoundingBox"
			primitiveUnits="objectBoundingBox"
			colorInterpolationFilters="sRGB"
		>
			<feTurbulence
				type="turbulence"
				baseFrequency="0.15 0.15"
				numOctaves="6"
				seed="55"
				stitchTiles="stitch"
				result="turbulence"
			></feTurbulence>
			<feDiffuseLighting
				surfaceScale="50"
				diffuseConstant="25"
				lightingColor="#808080"
				in="turbulence"
				result="diffuseLighting"
			>
				<feDistantLight azimuth="66" elevation="5">
					<animate
						attributeName="azimuth"
						from="66"
						to="-294"
						repeatCount="indefinite"
						dur="5.8s"
						fill="freeze"
					/>
				</feDistantLight>
			</feDiffuseLighting>
			<feComposite in="diffuseLighting" in2="SourceAlpha" operator="in" result="composite" />
			<feConvolveMatrix
				order="5 5"
				kernelMatrix="1 1 1 1 1
 1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1"
				divisor="25"
				bias="0"
				targetX="0"
				targetY="0"
				edgeMode="wrap"
				preserveAlpha="true"
				x="0%"
				y="0%"
				width="100%"
				height="100%"
				in="composite"
				result="convolveMatrix"
			/>

			<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 0.8 0" />
		</filter>,
		true,
	);

const terrains = $enum(TMapTerrainType).getValues();

terrains.forEach((t) =>
	SVGIconContainer.registerSymbol(
		getMapTerrainPatternID(t),
		<symbol viewBox={`${-MAP_VIEWBOX / 2} ${-MAP_VIEWBOX / 2} ${MAP_VIEWBOX} ${MAP_VIEWBOX}`}>
			<g>
				{(() => {
					const [width, height] = new Array(2).fill(getTextureGeneratedResolution('QHD'));
					const nx = Math.ceil(MAP_RENDER_SIZE / width);
					const ny = Math.ceil(MAP_RENDER_SIZE / height);
					return _.range(nx).reduce(
						(nodes, x) =>
							nodes.concat(
								_.range(ny)
									.map((y) => {
										const cx = x * width - MAP_RENDER_SIZE / 2;
										const cy = y * height - MAP_RENDER_SIZE / 2;
										const omit = [
											[cx, cy],
											[cx + width, cy],
											[cx + width, cy + height],
											[cx, cy + height],
										].reduce(
											(flag, point) =>
												flag ||
												Geometry.isPointInRange(
													{
														x: point[0],
														y: point[1],
													},
													{
														x: 0,
														y: 0,
														radius: MAP_RENDER_SIZE / 2,
													},
												),
											false,
										);
										if (!omit) return null;
										return (
											<image
												xlinkHref={getMapTerrainTexture(t)}
												x={cx / MAP_PIXEL_RATIO}
												y={cy / MAP_PIXEL_RATIO}
												width={width / MAP_PIXEL_RATIO}
												height={height / MAP_PIXEL_RATIO}
												key={`${x}_${y}`}
											/>
										);
									})
									.filter(Boolean),
							),
						[],
					);
				})()}
			</g>
		</symbol>,
	),
);

const TerrainEffectLayer = React.memo(
	(({ type }) => {
		switch (type) {
			case 'steam':
			case 'gas':
				return (
					<circle
						className={MapTerrainEffectClass[type]}
						cx={0}
						cy={0}
						r={150}
						fill="#808080"
						{...SVGIconContainer.useFilter(getMapTerrainFilterID(type))}
					/>
				);
			default:
				return null;
		}
	}) as React.FC<{ type: TMapTerrainEffectType }>,
	(oldProps, newProps) => oldProps.type === newProps.type,
);

const MapTerrainLayer: React.FC<React.PropsWithChildren<TMapRenderState>> = (props) => {
	const bottomType = getTerrainTypeFromProps(props);
	const bottomFilter = ''; // getTerrainFilterFromProps(props);
	const bottomPattern = getMapTerrainPatternID(bottomType);
	const topEffects = [].concat(getTerrainEffectFromProps(props));
	const topOpacity = React.useMemo(
		() =>
			BasicMaths.clipValue({
				x: (props.scale - 0.5) * 2.2,
				min: 0,
				max: 1,
			}),
		[props.scale],
	);
	return (
		<g id={Render_MapTerrainLayerId} className={Style.combatMapTerrain}>
			<g>
				<use
					{...SVGIconContainer.getXLinkProps(bottomPattern)}
					x={-MAP_VIEWBOX / 2}
					y={-MAP_VIEWBOX / 2}
					width={MAP_VIEWBOX}
					height={MAP_VIEWBOX}
				/>
				<use
					{...SVGIconContainer.getXLinkProps(Render_MapBoundaryShapeID)}
					stroke="#FFF"
					fill="none"
					className={Style.bottomLayer}
					strokeWidth="2px"
				/>
			</g>
			{props.children}
			<g className={Style.topLayer} opacity={topOpacity}>
				{topEffects.map((effect) => (
					<TerrainEffectLayer key={effect} type={effect} />
				))}
			</g>
		</g>
	);
};

export const SVGMapTerrainRenderLayer = MapTerrainLayer;
export default SVGMapTerrainRenderLayer;

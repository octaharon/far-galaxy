module.exports = {
	jsx: {
		babelConfig: {
			plugins: [
				[
					'@svgr/babel-plugin-remove-jsx-attribute',
					{
						elements: ['svg'],
						attributes: ['id', 'enableBackground', 'xmlns', 'xmlSpace'],
					},
				],
				[
					'@svgr/babel-plugin-replace-jsx-attribute-value',
					{
						values: [
							{ value: 'blue', newValue: 'props.colorPrimary', literal: true },
							{ value: 'red', newValue: 'props.colorSecondary', literal: true },
						],
					},
				],
			],
		},
	},
};

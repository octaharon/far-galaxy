## Talents
```mermaid
flowchart LR
	214 -..-> 216>"Master Armsman (+2% damage)"]
	211 -..-> 216>"Master Armsman (+2% damage)"]
	213 -..-> 216>"Master Armsman (+2% damage)"]
	215 -..-> 216>"Master Armsman (+2% damage)"]
	212 -..-> 216>"Master Armsman (+2% damage)"]
	210 -..-> 216>"Master Armsman (+2% damage)"]
	310 -..-> 350>"Expert Armsman (+5% damage)"]
	311 -..-> 350>"Expert Armsman (+5% damage)"]
	312 -..-> 350>"Expert Armsman (+5% damage)"]
	314 -..-> 350>"Expert Armsman (+5% damage)"]
	313 -..-> 350>"Expert Armsman (+5% damage)"]
	315 -..-> 350>"Expert Armsman (+5% damage)"]
	300 -..-> 450>"Warlord (+10 Ground Units health)"]
	301 -..-> 450>"Warlord (+10 Ground Units health)"]
	302 -..-> 450>"Warlord (+10 Ground Units health)"]
	200 -..-> 450>"Warlord (+10 Ground Units health)"]
	351 -..-> 450>"Warlord (+10 Ground Units health)"]
	350 -..-> 450>"Warlord (+10 Ground Units health)"]
	242 -..-> 451>"Financier (+5% base Resource Output)"]
	305 -..-> 451>"Financier (+5% base Resource Output)"]
	306 -..-> 451>"Financier (+5% base Resource Output)"]
	304 -..-> 451>"Financier (+5% base Resource Output)"]
	322 -..-> 451>"Financier (+5% base Resource Output)"]
	307 -..-> 452>"Big Game Hunter (Segmentation Fault Ability)"]
	243 -..-> 452>"Big Game Hunter (Segmentation Fault Ability)"]
	241 -..-> 452>"Big Game Hunter (Segmentation Fault Ability)"]
	303 -..-> 452>"Big Game Hunter (Segmentation Fault Ability)"]
	440 -..-> 530>"Midship Man (+0.5 Corvette and Support Ship Speed)"]
	451 -..-> 530>"Midship Man (+0.5 Corvette and Support Ship Speed)"]
	440 -..-> 531>"Mariner (+10 Corvette and Support Ship Hit Points)"]
	450 -..-> 531>"Mariner (+10 Corvette and Support Ship Hit Points)"]
	441 -..-> 532>"Aviator (+1 Titljet and Support Aircraft Speed)"]
	451 -..-> 532>"Aviator (+1 Titljet and Support Aircraft Speed)"]
	441 -..-> 533>"Splasher (+5% Bombs and Rockets Damage)"]
	450 -..-> 533>"Splasher (+5% Bombs and Rockets Damage)"]
	452 -..-> 534>"Colossus (+10 Point Defense and Hovertank Points)"]
	450 -..-> 534>"Colossus (+10 Point Defense and Hovertank Points)"]
	452 -..-> 535>"Farstriker (+0.25 AOE Radius)"]
	451 -..-> 535>"Farstriker (+0.25 AOE Radius)"]
	450 -..-> 536>"Zoomaster (+10 Exosuit and Biotech Hit Points )"]
	452 -..-> 536>"Zoomaster (+10 Exosuit and Biotech Hit Points )"]
	530 -..-> 610>"Voyager (+2 Armor Protection for Naval Units)"]
	531 -..-> 610>"Voyager (+2 Armor Protection for Naval Units)"]
	535 -..-> 610>"Voyager (+2 Armor Protection for Naval Units)"]
	533 -..-> 611>"Skyman (+2 Armor Protection for Air Units)"]
	532 -..-> 611>"Skyman (+2 Armor Protection for Air Units)"]
	535 -..-> 611>"Skyman (+2 Armor Protection for Air Units)"]
	535 -..-> 612>"Controller (+0.5 Quadrapod and Combat Drone speed)"]
	534 -..-> 612>"Controller (+0.5 Quadrapod and Combat Drone speed)"]
	536 -..-> 613>"Breeder (+0.5 Hydra and Swarm speed)"]
	534 -..-> 613>"Breeder (+0.5 Hydra and Swarm speed)"]
	520 -..-> 620>"Crusher (+5% Damage)"]
	521 -..-> 620>"Crusher (+5% Damage)"]
	522 -..-> 620>"Crusher (+5% Damage)"]
	523 -..-> 620>"Crusher (+5% Damage)"]
	524 -..-> 620>"Crusher (+5% Damage)"]
	525 -..-> 620>"Crusher (+5% Damage)"]
	526 -..-> 620>"Crusher (+5% Damage)"]
	527 -..-> 620>"Crusher (+5% Damage)"]
	528 -..-> 620>"Crusher (+5% Damage)"]
	529 -..-> 620>"Crusher (+5% Damage)"]
	500 -..-> 621>"Paragon (+1 Recon and Counter-Recon Level)"]
	501 -..-> 621>"Paragon (+1 Recon and Counter-Recon Level)"]
	502 -..-> 621>"Paragon (+1 Recon and Counter-Recon Level)"]
	503 -..-> 621>"Paragon (+1 Recon and Counter-Recon Level)"]
	504 -..-> 621>"Paragon (+1 Recon and Counter-Recon Level)"]
	505 -..-> 621>"Paragon (+1 Recon and Counter-Recon Level)"]
	528 -..-> 630>"Warp Overlord (+1000 XP)"]
	525 -..-> 630>"Warp Overlord (+1000 XP)"]
	527 -..-> 630>"Warp Overlord (+1000 XP)"]
	510 -..-> 630>"Warp Overlord (+1000 XP)"]
	511 -..-> 630>"Warp Overlord (+1000 XP)"]
	512 -..-> 630>"Warp Overlord (+1000 XP)"]
	450 -..-> 630>"Warp Overlord (+1000 XP)"]
	522 -..-> 631>"Cloudstormer (+1000 XP)"]
	523 -..-> 631>"Cloudstormer (+1000 XP)"]
	524 -..-> 631>"Cloudstormer (+1000 XP)"]
	510 -..-> 631>"Cloudstormer (+1000 XP)"]
	511 -..-> 631>"Cloudstormer (+1000 XP)"]
	512 -..-> 631>"Cloudstormer (+1000 XP)"]
	450 -..-> 631>"Cloudstormer (+1000 XP)"]
	521 -..-> 632>"Annihilator (+1000 XP)"]
	520 -..-> 632>"Annihilator (+1000 XP)"]
	526 -..-> 632>"Annihilator (+1000 XP)"]
	510 -..-> 632>"Annihilator (+1000 XP)"]
	511 -..-> 632>"Annihilator (+1000 XP)"]
	512 -..-> 632>"Annihilator (+1000 XP)"]
	450 -..-> 632>"Annihilator (+1000 XP)"]
	502 -..-> 650("Craftologist (+50 Hit Points and 500 Repair Points each Base)")
	503 -..-> 650("Craftologist (+50 Hit Points and 500 Repair Points each Base)")
	502 -..-> 651("Materialist (+50 Hit Points and 500 Repair Points each Base)")
	503 -..-> 651("Materialist (+50 Hit Points and 500 Repair Points each Base)")
	611 -..-> 700>"Shatterer (Apocalypse Ability)"]
	610 -..-> 700>"Shatterer (Apocalypse Ability)"]
	640 -..-> 700>"Shatterer (Apocalypse Ability)"]
	641 -..-> 700>"Shatterer (Apocalypse Ability)"]
	601 -..-> 700>"Shatterer (Apocalypse Ability)"]
	602 -..-> 700>"Shatterer (Apocalypse Ability)"]
	630 -..-> 701>"Belligerent (+0.1 Attack Rate for Ground and Hover Units)"]
	631 -..-> 701>"Belligerent (+0.1 Attack Rate for Ground and Hover Units)"]
	632 -..-> 701>"Belligerent (+0.1 Attack Rate for Ground and Hover Units)"]
	613 -..-> 701>"Belligerent (+0.1 Attack Rate for Ground and Hover Units)"]
	600 -..-> 701>"Belligerent (+0.1 Attack Rate for Ground and Hover Units)"]
	604 -..-> 701>"Belligerent (+0.1 Attack Rate for Ground and Hover Units)"]
	601 -..-> 702>"Schemer (+1 Ability Slot)"]
	602 -..-> 702>"Schemer (+1 Ability Slot)"]
	620 -..-> 702>"Schemer (+1 Ability Slot)"]
	621 -..-> 702>"Schemer (+1 Ability Slot)"]
	611 -..-> 710>"Celestial (+5% Shield Capacity for Aerial Units)"]
	631 -..-> 710>"Celestial (+5% Shield Capacity for Aerial Units)"]
	620 -..-> 710>"Celestial (+5% Shield Capacity for Aerial Units)"]
	610 -..-> 711>"Abyssal (+5% Shield Capacity for Naval Units)"]
	630 -..-> 711>"Abyssal (+5% Shield Capacity for Naval Units)"]
	620 -..-> 711>"Abyssal (+5% Shield Capacity for Naval Units)"]
	610 -..-> 712>"Admiral (+0.5 Scan Range for Naval and Aerial Units)"]
	611 -..-> 712>"Admiral (+0.5 Scan Range for Naval and Aerial Units)"]
	621 -..-> 712>"Admiral (+0.5 Scan Range for Naval and Aerial Units)"]
	612 -..-> 712>"Admiral (+0.5 Scan Range for Naval and Aerial Units)"]
	651 -..-> 730>"Architect (+1 Bay Capacity)"]
	603 -..-> 730>"Architect (+1 Bay Capacity)"]
	650 -..-> 730>"Architect (+1 Bay Capacity)"]
	604 -..-> 730>"Architect (+1 Bay Capacity)"]
	240 -..-> 320("Defender (+5% Shield Capacity)")
	222 -..-> 320("Defender (+5% Shield Capacity)")
	240 -..-> 321("Endurant (+3% Hit Points)")
	221 -..-> 321("Endurant (+3% Hit Points)")
	204 -..-> 322("Daredevil (+5% Scan Range)")
	205 -..-> 322("Daredevil (+5% Scan Range)")
	subgraph "Level 0"
	direction LR
		100>"Warchief"]
		101>"Mechanic"]
		102>"Humanist"]
		110>"Intruder"]
		111>"Marksman"]
		112>"Trooper"]
		200>"Chieftain"]
		201("Slaughterer (+5% Infantry Damage)")
		202("Surveyor (+15% Scout Hit Points)")
		203>"Tactician (+2 Scout Scan Range)"]
		204>"Overmind (+1 Droid Speed)"]
		205("Droid Commander (+5% Droid damage)")
		210("Missile Mastery (+0.5 Cruise Missiles and Homing Missiles  Range)")
		211("Machine Gun Mastery (+2 Machine Guns Damage)")
		212("Railgun Mastery (+20% Hit Chance for Mass Drivers)")
		213("Laser Mastery (+0.5 Laser Range)")
		214("Ballistic Mastery (+20% Cannons Hit Chance)")
		215("Launcher Mastery (+10% Launcher Damage)")
		216>"Master Armsman (+2% damage)"]
		220>"Demolition man (+5% Heat Damage Reduction on Armor)"]
		221("Armor Mastery (-5% Armor Cost)")
		222("Shield Mastery (-5% Shield Cost)")
		223>"Groundbreaker (+10 Production Capacity at Factories)"]
		230>"Nailwalker (+15% Dodge vs Surface Attacks)"]
		231>"Steward (-5% Infantry, Scout and Droid Cost)"]
		232>"Scrapper (+5% Repair Output)"]
		233>"Wiseman (+1 Recon Level)"]
		234>"Suprematist (+1 Counter-Recon Level)"]
		240>"Protector (+35 Base Hit Points)"]
		241>"Survivor (+1 Ability Slot)"]
		242>"Manufacturer (Seize Droids Ability)"]
		243>"Challenger (Exploit Ability)"]
		350>"Expert Armsman (+5% damage)"]
	end
	subgraph "Level 12"
	direction TB
		400>"Castellan (Gain 2 Damage Threshold on Shields and Armor)"]
		401>"Strategist (+1 Artillery Scan Range, +0.5 Cannons range)"]
		410("Radiant (+5% Energy Weapon Damage)")
		411("Desolator (+5% Plasma Damage)")
		420>"Outlander (+15 Combat Drone and Rover Hit Points)"]
		421>"Grinder (+5 Armor Absorption for Point Defense units)"]
		422>"Jaeger (+1 Exosuit and Combat Mech speed)"]
		430>"Wayfarer (-5% Tank, HAV and LAV Cost)"]
		431>"Highlander (-10% Rover Cost)"]
		430 -..-> 440>"Into the Seas (+200 XP)"]
		431 -..-> 440>"Into the Seas (+200 XP)"]
		430 -..-> 441>"Into the Skies (+200 XP)"]
		431 -..-> 441>"Into the Skies (+200 XP)"]
		443>"Science Overwhelming (+0.25 Base AP × )"]
		450>"Warlord (+10 Ground Units health)"]
		451>"Financier (+5% base Resource Output)"]
		452>"Big Game Hunter (Segmentation Fault Ability)"]
		470("Integrated Defense (+45 Base Hit Points)")
		471("Gas Giant (+5% Hydrogen, Helium and Nitrogen Output)")
		472("Ore King (+5% Carbon, Metals and Oxygen Output)")
		473("Fallout Boy (+5% Isotopes and Halogens Output)")
		474("Lifebringer (+5% Proteins and Minerals Output)")
	end
	subgraph "Level 18"
	direction LR
		500("C-Level (+10 Production Capacity)")
		501("Talented (+2 Production Speed)")
		502("Researcher (+1 Establishments Resource Output)")
		503("Engineer (+1 Establishments Engineering Output)")
		504("Mayor (+3 Facilities Energy Output)")
		505("Sustainer (+1 Facilities Repair Output)")
		510>"Destroyer (Crumble Ability)"]
		511>"Saboteur (Great Equalizer Ability)"]
		512>"Champion (Equinox Barrage Ability)"]
		520>"Rifleman (+5% Machine Guns Hit Chance)"]
		521>"Gunlayer (+10% Launchers Attack Rate)"]
		522>"Suppressor (+10% Homing Missiles Damage)"]
		523>"Breacher (+10% Cruise Missiles Damage)"]
		524>"Leadsprayer (+10% Cannons Attack Rate)"]
		525>"Piercer (+10% Mass Drivers Attack Rate)"]
		526>"Heater (+10% Lasers Hit Chance)"]
		527>"Burner (+10% Energy Weapons Attack Rate)"]
		528>"Coiler (+0.5 Plasma Attack Range)"]
		529>"Zenithar (+0.5 Anti-Air Range)"]
		530>"Midship Man (+0.5 Corvette and Support Ship Speed)"]
		531>"Mariner (+10 Corvette and Support Ship Hit Points)"]
		532>"Aviator (+1 Titljet and Support Aircraft Speed)"]
		533>"Splasher (+5% Bombs and Rockets Damage)"]
		534>"Colossus (+10 Point Defense and Hovertank Points)"]
		535>"Farstriker (+0.25 AOE Radius)"]
		536>"Zoomaster (+10 Exosuit and Biotech Hit Points )"]
	end
	subgraph "Level 24"
	direction BT
		600>"Manifold (+0.25 Unit Ability Power)"]
		601>"Sublime (+0.25 Unit Ability Power)"]
		602>"Impregnable (+0.25 Base Ability Power)"]
		603>"Sagacious (+0.25 Base Ability Power)"]
		604>"Ascendant (+10% Dodge)"]
		610>"Voyager (+2 Armor Protection for Naval Units)"]
		611>"Skyman (+2 Armor Protection for Air Units)"]
		612>"Controller (+0.5 Quadrapod and Combat Drone speed)"]
		613>"Breeder (+0.5 Hydra and Swarm speed)"]
		620>"Crusher (+5% Damage)"]
		621>"Paragon (+1 Recon and Counter-Recon Level)"]
		630>"Warp Overlord (+1000 XP)"]
		631>"Cloudstormer (+1000 XP)"]
		632>"Annihilator (+1000 XP)"]
		640>"Sovereign (+1 Effect Penetration)"]
		641>"Steadfast (+1 Effect Resistance)"]
		650("Craftologist (+50 Hit Points and 500 Repair Points each Base)")
		651("Materialist (+50 Hit Points and 500 Repair Points each Base)")
	end
	subgraph "Level 30"
	direction LR
		700>"Shatterer (Apocalypse Ability)"]
		701>"Belligerent (+0.1 Attack Rate for Ground and Hover Units)"]
		702>"Schemer (+1 Ability Slot)"]
		710>"Celestial (+5% Shield Capacity for Aerial Units)"]
		711>"Abyssal (+5% Shield Capacity for Naval Units)"]
		712>"Admiral (+0.5 Scan Range for Naval and Aerial Units)"]
		700 -..-> 720("Cloud Marshal (+2% Aerial Units Damage)")
		701 -..-> 720("Cloud Marshal (+2% Aerial Units Damage)")
		710 -..-> 720("Cloud Marshal (+2% Aerial Units Damage)")
		711 -..-> 720("Cloud Marshal (+2% Aerial Units Damage)")
		712 -..-> 720("Cloud Marshal (+2% Aerial Units Damage)")
		700 -..-> 721("Warmonger (+5% Drone Weapons Damage)")
		701 -..-> 721("Warmonger (+5% Drone Weapons Damage)")
		710 -..-> 721("Warmonger (+5% Drone Weapons Damage)")
		711 -..-> 721("Warmonger (+5% Drone Weapons Damage)")
		712 -..-> 721("Warmonger (+5% Drone Weapons Damage)")
		700 -..-> 722("Ultimate Invader (+5000 XP)")
		701 -..-> 722("Ultimate Invader (+5000 XP)")
		710 -..-> 722("Ultimate Invader (+5000 XP)")
		711 -..-> 722("Ultimate Invader (+5000 XP)")
		712 -..-> 722("Ultimate Invader (+5000 XP)")
		730>"Architect (+1 Bay Capacity)"]
	end
	subgraph "Level 6"
	direction TB
		300>"General (+1 Armor Damage Reduction)"]
		301>"Navigator (+0.5 Scan Range)"]
		302>"Dictator (+5 Hit Points)"]
		303>"Industrious (+3% Material Output)"]
		304>"Prudent (+3% Energy Output)"]
		305>"Scholar (+3% Research Output)"]
		306>"Governor (+3% Engineering Output)"]
		307>"Netrunner (+0.25 Unit AP × )"]
		310("Rocketeer (+5% Missiles Damage)")
		311("Major Laser (+5% Laser Attack Rate)")
		312("Skirmisher (+5% Machine Gun Hit Chance)")
		313("Sniper (+5% Railgun Attack Rate)")
		314("Artillerist (+0.25 Launchers AoE Range)")
		315("Fusilier (+5% Cannon Damage)")
		320("Defender (+5% Shield Capacity)")
		321("Endurant (+3% Hit Points)")
		322("Daredevil (+5% Scan Range)")
		320 -..-> 351>"Risk Manager (+5% Dodge)"]
		321 -..-> 351>"Risk Manager (+5% Dodge)"]
	end
```

# Damage model

## Attack Batch
When an Attack Interface fires, all attacks are flattened to damage units, i.e. every distribution is collapsed into a random value, for each of Damage terms. A damage unit stores Delivery type and Damage terms, and all damage units share Range, Hit Chance and Priority. Damage units are ordered by:

### Direct Attack
Every attack that is not AoE, DoT or Deferred is considered Direct. 

### AoE Attack
Area of Effect attack is described by shape, size of shape (radius) and magnitude, which has specific meaning for different shapes, and it's damage is precalculated so it's constant in the whole area

### DoT Attack
DoT attack generates all it's damage cycles upfront, so it's defined before DoT effect is applied to target, and it has the Effect description attached to it 

### Deferred Attack
Deferred attack is also pre-calculated and carries an Effect


## Attack on Unit

Attack batch is sent to the Unit it's supposed to, where it's accuracy is adjusted for Unit's movement, passed through the Event callback and then every Damage Batch is applied to the frontmost Defense Interface

### Selective attacks
Selective attacks apply only to targets which are hostile to the Attack Batch owner

### Hit Chance Calculation

#### Movement adjustment
if the target has an angular speed relative to the shooting unit, then it gains 25% Dodge for every 1 unit of speed in direction perpendicular to the line of fire

#### Defense Perks
if Defense Interface has certain perks, which allow to negate certain kind of Delivery, then those attacks are removed from Attack Batch

#### Movement filter
When Unit receive an Attack Batch, it filters attack by Delivery depending on its Movement Type, i.e. Aerial attacks do not hit ground targets etc.

#### Miss calculation
- Hit chance is calculated for Direct attacks first, by subtracting the Dodge value relevant to Delivery of the attack from the Hit Chance of the whole Attack Batch. 
- Immediate delivery does not trigget Hit Chance calculation, as it hit always
- If any of Direct attacks Hit, all the other Attacks hit too

### Attack behaviour after hit
Every other attack has its Hit Chance calculated independently (locally), if none of Direct attacks hit.

#### Direct Attack
If Hit Roll fails, the damage is ignored.

#### AoE Attacks
if Hit Roll succeeds, the attack does full damage to the target unit, otherwise target unit is suffering only secondary AoE damage

#### DoT Attacks
- if Hit Roll succeeds, target gets full DoT effect and a primary tick
- Otherwise if Local Hit Roll succeeds, target only takes a primary tick of damage immediately

#### Deferred Attacks
- if Direct Hit Roll succeeds, target takes the damage immediately and gets a Deferred Damage Effect. 
- Otherwise if Local Hit Roll succeeds, a target gets immediate damage only only. If both miss, the damage is ignored.

## Damage application

### Disruptive  attacks
If any of attacks has Precision flag, they skip the shields

### Damage application order

- Damage is going through shields first, from outer to inner, where it's reduced by Damage Reduction factors per each Damage term and then reduced from Shield Capacity. 
- If it breaches the shield, the Damage is reduced proportionally and passed to the next shield. 
- If all shields are breached, the Damage is further reduced by Armor, then corrected by combination of Target Type (Bio/Mech/Robot), and finally subtracted from targets Health.
- If Health is reduced to zero, and the remaining damage exceeds target's Max Health (meaning it does 100% excessive damage), no Salvage is left of the unit

## Research
```mermaid
flowchart LR
		900["Establishment: Science Lab"]
	subgraph "Cost 600"
	direction TB
		100["Weapon: Small Missile"]
		200["Weapon: HE-LL"]
		300["Weapon: Auto Cannon"]
		400["Weapon: Fuze Shells"]
		500["Weapon: Flake Cannon"]
		600["Weapon: Railgun Cannon"]
		700["Armor: Kevlar Plating"]
		800["Shield: Repulsive Shield"]
		1500["Facility: Charge Depot"]
		1600["Chassis: Scout"]
		1601["Factory: Motorplant"]
		1700["Chassis: Droid"]
		1701["Factory: Robotics Bay"]
		1800["Chassis: Infantry"]
		1801["Factory: Training Facility"]
		1801 -..-> 1900["Factory: Water Dock"]
		1601 -..-> 1900
		1701 -..-> 1900
	end
	subgraph "Cost 900"
	direction TB
		100 -..-> 110["Weapon: Seeker Missile"]
		100 -..-> 111["Weapon: Corrosive Missile"]
		100 -..-> 112["Weapon: Napalm Missile"]
		200 -..-> 205["Weapon: PALM"]
		205 -..-> 215["Weapon: Superheater"]
		212["Lasers Damage I"] -..-> 215
		205 -..-> 216["Weapon: Governor"]
		211["Lasers Attack Rate I"] -..-> 216
		205 -..-> 217["Weapon: X-Ray Laser"]
		210["Lasers Precision I"] -..-> 217
		300 -..-> 305["Weapon: Big Bertha"]
		305 -..-> 315["Weapon: Anti-Aircraft Cannon"]
		312["MG Damage I"] -..-> 315
		305 -..-> 316["Weapon: Laser Assisted Gun"]
		311["MG Range I"] -..-> 316
		305 -..-> 317["Weapon: TBMG"]
		312 -..-> 317
		400 -..-> 405["Weapon: Cumulative Shells"]
	end
	subgraph "Cost 1200"
	direction TB
		100 -..-> 113["Missile Precision I"]
		100 -..-> 114["Missile Range I"]
		100 -..-> 115["Missile Damage I"]
		200 -..-> 210
		200 -..-> 211
		200 -..-> 212
		300 -..-> 310["MG Precision I"]
		300 -..-> 311
		300 -..-> 312
		400 -..-> 410["Cannon Precision I"]
		400 -..-> 411["Cannons Attack Rate I"]
		400 -..-> 412["Cannons Damage I"]
		500 -..-> 505["Weapon: Corrosive Mines"]
		600 -..-> 605["Weapon: Radium Cannon"]
		700 -..-> 705["Armor: Carbon Foam Plating"]
		800 -..-> 805["Shield: Aegis"]
		900 -..-> 905["Establishment: BrainTank"]
		900 -..-> 906["Establishment: Investigation Bureau"]
		900 -..-> 907["Establishment: Campus"]
		1500 -..-> 1505["Facility: Silo"]
		1500 -..-> 1506["Facility: Incineration Plant"]
		1600 -..-> 1605["Chassis: Vermin"]
		1600 -..-> 1606["Chassis: LAV"]
		1601 -..-> 1606
		1700 -..-> 1705["Chassis: Enforcer"]
		1700 -..-> 1706["Chassis: Combat Drone"]
		1701 -..-> 1706
		1700 -..-> 1707["Chassis: T-1024"]
		1800 -..-> 1805["Chassis: Elite Guardian"]
		1800 -..-> 1806["Chassis: Exosuit"]
		1801 -..-> 1806
		1800 -..-> 1807["Chassis: Chaplain"]
		1900 -..-> 1905["Chassis: Corvette"]
		1900 -..-> 1906["Chassis: Support Ship"]
	end
	subgraph "Cost 1800"
	direction TB
		405 -..-> 415["Weapon: EMP Shells"]
		410 -..-> 415
		405 -..-> 416["Weapon: Proton Shells"]
		411 -..-> 416
		405 -..-> 417["Weapon: Vacuum Shells"]
		412 -..-> 417
		700 -..-> 710["Strong Materials I"]
		700 -..-> 711["Light Materials I"]
		705 -..-> 712["Armor: Kevlar Armor"]
		800 -..-> 810["Battery Life I"]
		800 -..-> 811["Field Calculus I"]
		805 -..-> 812["Shield: Bulwark Shield"]
		900 -..-> 910["Cryptographic Imperative I"]
		900 -..-> 911["Fuzzy Optimization I"]
		900 -..-> 912["Dark Matter Composites I"]
		1500 -..-> 1510["Integrated Service I"]
		1500 -..-> 1511["Segmented Production I"]
		1500 -..-> 1512["Grid Dynamics I"]
		1600 -..-> 1610["Wheelcraft I"]
		1601 -..-> 1610
		1600 -..-> 1611["Mass Education I"]
		1601 -..-> 1611
		1700 -..-> 1710["Robocraft I"]
		1701 -..-> 1710
		1700 -..-> 1711["Global Network I"]
		1701 -..-> 1711
		1800 -..-> 1810["Footcraft I"]
		1801 -..-> 1810
		1800 -..-> 1811["Crossbreed Psionics I"]
		1801 -..-> 1811
		1905 -..-> 1910["Chassis: Destroyer"]
		1900 -..-> 1911["Shipcraft I"]
		1900 -..-> 1912["Subdimensional Sonars I"]
	end
	subgraph "Cost 2400"
	direction TB
		115 -..-> 120["Weapon: Rocket Artillery"]
		114 -..-> 120
		113 -..-> 120
		110 -..-> 121["Weapon: S.E.R.P.E.N.T."]
		111 -..-> 121
		112 -..-> 121
		115 -..-> 122["Weapon: Tactical Missile"]
		110 -..-> 122
		115 -..-> 123["Missile Damage II"]
		112 -..-> 123
		114 -..-> 124["Missile Range II"]
		110 -..-> 124
		113 -..-> 125["Missile Precision II"]
		111 -..-> 125
		115 -..-> 126["Cruise Missiles Damage I"]
		110 -..-> 126
		114 -..-> 127["Cruise Missiles Range I"]
		110 -..-> 127
		113 -..-> 128["Cruise Missiles Precision I"]
		110 -..-> 128
		305 -..-> 320["MG Precision II"]
		310 -..-> 320
		305 -..-> 321["MG Range II"]
		311 -..-> 321
		305 -..-> 322["MG Damage II"]
		312 -..-> 322
		500 -..-> 510["Launcher Range I"]
		500 -..-> 511["Launchers Splash I"]
		500 -..-> 512["Launchers Damage I"]
		516["Weapon: Conventional Bomb"] -..-> 537["Bombs Damage I"]
		522["Launchers Damage II"] -..-> 537
		600 -..-> 610["Mass Drivers Range I"]
		600 -..-> 611["Mass Drivers Rate I"]
		600 -..-> 612["Mass Drivers Damage I"]
		616["Weapon: Plasma Gun"] -..-> 637["Plasma Damage I"]
		622["Mass Drivers Damage II"] -..-> 637
	end
	subgraph "Cost 3000"
	direction TB
		210 -..-> 220["Lasers Precision II"]
		211 -..-> 221["Lasers Attack Rate II"]
		212 -..-> 222["Lasers Damage II"]
		215 -..-> 223["Energy Weapons Precision I"]
		210 -..-> 223
		215 -..-> 224["Energy Weapons Range I"]
		211 -..-> 224
		215 -..-> 225["Energy Weapons Damage I"]
		212 -..-> 225
		315 -..-> 325["Weapon: AMAA Cannon"]
		311 -..-> 325
		310 -..-> 325
		317 -..-> 326["Weapon: Shard Cannon"]
		316 -..-> 326
		312 -..-> 326
		405 -..-> 420["Cannons Precision II"]
		410 -..-> 420
		405 -..-> 421["Cannons Attack Rate II"]
		411 -..-> 421
		405 -..-> 422["Cannons Damage II"]
		412 -..-> 422
		505 -..-> 515["Weapon: Poison Mines"]
		511 -..-> 515
		512 -..-> 515
		510 -..-> 515
		505 -..-> 516
		510 -..-> 516
		512 -..-> 516
		511 -..-> 516
		605 -..-> 615["Weapon: Rapid Coil Gun"]
		611 -..-> 615
		612 -..-> 615
		610 -..-> 615
		605 -..-> 616
		610 -..-> 616
		612 -..-> 616
		611 -..-> 616
		711 -..-> 720["Armor: Nanocell Plating"]
		710 -..-> 721["Armor: Power Armor"]
		712 -..-> 722["Armor: Composite Armor"]
		811 -..-> 820["Shield: A Shield"]
		810 -..-> 821["Shield: Reactive Shield"]
		812 -..-> 822["Shield: Phasing Shield"]
		812 -..-> 823["Shield: Proactive Shield"]
		906 -..-> 920["Establishment: Reconnaissance Agency"]
		910 -..-> 920
		906 -..-> 921["Establishment: Data Warehouse"]
		911 -..-> 921
		907 -..-> 922["Establishment: Proving Grounds"]
		912 -..-> 922
		910 -..-> 925["Heuristic Analytics"]
		911 -..-> 925
		912 -..-> 925
		1732["Chassis: Rover"] -..-> 1300["Factory: Aeronautics Complex"]
		1621["Wheelcraft II"] -..-> 1300
		1820["Footcraft II"] -..-> 1300
		1506 -..-> 1520["Facility: Composture"]
		1511 -..-> 1520
		1506 -..-> 1521["Facility: Mineral Refinery"]
		1510 -..-> 1521
		1506 -..-> 1522["Facility: Solar Collector"]
		1512 -..-> 1522
		1606 -..-> 1615["Chassis: Tank"]
		1610 -..-> 1615
		1606 -..-> 1616["Factory: Light Vehicle Factory"]
		1611 -..-> 1616
		1605 -..-> 1617["Chassis: Leech"]
		1611 -..-> 1617
		1605 -..-> 1618["Chassis: Locust"]
		1610 -..-> 1618
		1605 -..-> 1619["Chassis: Maybug"]
		1706 -..-> 1715["Chassis: Point Defense"]
		1710 -..-> 1715
		1705 -..-> 1716["Factory: Cybernetic Forge"]
		1710 -..-> 1716
		1705 -..-> 1717["Chassis: Hunter"]
		1711 -..-> 1717
		1706 -..-> 1718["Chassis: Chaser"]
		1711 -..-> 1718
		1806 -..-> 1815["Chassis: Bruiser"]
		1811 -..-> 1815
		1805 -..-> 1816["Factory: Baracks"]
		1810 -..-> 1816
		1805 -..-> 1817["Factory: Bionic Institute"]
		1811 -..-> 1817
		1806 -..-> 1818["Chassis: Stalker"]
		1810 -..-> 1818
		1910 -..-> 1915["Chassis: Hammerhead"]
		1912 -..-> 1915
		1911 -..-> 1916["Factory: Shipyard"]
		1910 -..-> 1916
		1906 -..-> 1917["Factory: Submarine Pool"]
		1905 -..-> 1917
		1905 -..-> 1918["Chassis: Neptune"]
		1911 -..-> 1918
	end
	subgraph "Cost 4200"
	direction TB
		126 -..-> 130["Rockets Damage I"]
		120 -..-> 130
		127 -..-> 131["Rockets Range I"]
		120 -..-> 131
		128 -..-> 132["Rockets Precision  I"]
		120 -..-> 132
		126 -..-> 133["Cruise Missiles Damage II"]
		121 -..-> 133
		128 -..-> 134["Cruise Missiles Precision II"]
		121 -..-> 134
		127 -..-> 135["Cruise Missiles Range II"]
		121 -..-> 135
		210 -..-> 230["Weapon: Gamma Laser"]
		212 -..-> 230
		211 -..-> 230
		217 -..-> 230
		210 -..-> 231["Weapon: Lighting Gun"]
		212 -..-> 231
		211 -..-> 231
		215 -..-> 231
		223 -..-> 232["Weapon: Solar Beam"]
		225 -..-> 232
		224 -..-> 232
		215 -..-> 232
		415 -..-> 430["Weapon: Tau Shells"]
		411 -..-> 430
		420 -..-> 430
		417 -..-> 431["Weapon: Explosive Shells"]
		410 -..-> 431
		422 -..-> 431
		416 -..-> 432["Weapon: Antimatter Shells"]
		421 -..-> 432
		412 -..-> 432
		505 -..-> 520["Launcher Range II"]
		510 -..-> 520
		505 -..-> 521["Launchers Splash II"]
		511 -..-> 521
		505 -..-> 522
		512 -..-> 522
		605 -..-> 620["Mass Drivers Range II"]
		610 -..-> 620
		605 -..-> 621["Mass Drivers Rate II"]
		611 -..-> 621
		605 -..-> 622
		612 -..-> 622
		712 -..-> 725["Armor: Large Kevlar Armor"]
		710 -..-> 725
		711 -..-> 725
		705 -..-> 726["Armor: Composite Plating"]
		710 -..-> 726
		711 -..-> 726
		712 -..-> 727["Armor: Nano Armor"]
		710 -..-> 727
		711 -..-> 727
		822 -..-> 825["Shield: TARGA"]
		810 -..-> 825
		811 -..-> 825
		805 -..-> 826["Shield: Hystrix"]
		810 -..-> 826
		811 -..-> 826
		823 -..-> 827["Shield: Carapace Shield"]
		810 -..-> 827
		811 -..-> 827
		1300 -..-> 1305["Chassis: Tiltjet"]
		1300 -..-> 1306["Chassis: Support Aircraft"]
		1300 -..-> 1310["Chassis: Hovertank"]
		1300 -..-> 1311["Air Domination I"]
		1300 -..-> 1312["Battle Momentum I"]
		1505 -..-> 1525["Facility: Garage"]
		1512 -..-> 1525
		1511 -..-> 1525
		1510 -..-> 1525
		1606 -..-> 1620["Chassis: Lumberjack"]
		1610 -..-> 1620
		1611 -..-> 1620
		1610 -..-> 1621
		1616 -..-> 1621
		1616 -..-> 1622["Mass Education II"]
		1611 -..-> 1622
		1705 -..-> 1720["Chassis: M5"]
		1710 -..-> 1720
		1711 -..-> 1720
		1710 -..-> 1721["Robocraft II"]
		1716 -..-> 1721
		1716 -..-> 1722["Global Network II"]
		1711 -..-> 1722
		1810 -..-> 1820
		1815 -..-> 1820
		1815 -..-> 1821["Crossbreed Psionics II"]
		1811 -..-> 1821
		1911 -..-> 1920["Shipcraft II"]
		1916 -..-> 1920
		1916 -..-> 1921["Subdimensional Sonars II"]
		1912 -..-> 1921
	end
	subgraph "Cost 5400"
	direction TB
		1310 -..-> 1315["Chassis: Mammoth"]
		1312 -..-> 1315
		1311 -..-> 1316["Factory: Hoverforge"]
		1310 -..-> 1316
		1306 -..-> 1317["Chassis: Strike Aircrat"]
		1305 -..-> 1317
		1305 -..-> 1318["Chassis: Airwolf"]
		1311 -..-> 1318
		1310 -..-> 1319["Chassis: Defender Of Faith"]
	end
	subgraph "Cost 6600"
	direction TB
		126 -..-> 140["Weapon: Black Widow"]
		127 -..-> 140
		128 -..-> 140
		133 -..-> 141["Weapon: Hammerfall"]
		135 -..-> 141
		134 -..-> 141
		115 -..-> 142["Weapon: Neutron Missile"]
		114 -..-> 142
		113 -..-> 142
		130 -..-> 143["Weapon: Hellfire Rocket"]
		132 -..-> 143
		131 -..-> 143
		123 -..-> 144["Weapon: AA Missile"]
		124 -..-> 144
		125 -..-> 144
		130 -..-> 150["Rockets Damage II"]
		120 -..-> 150
		131 -..-> 151["Rockets Range II"]
		120 -..-> 151
		132 -..-> 152["Rockets Precision II"]
		120 -..-> 152
		123 -..-> 153["Missile Damage III"]
		142 -..-> 153
		124 -..-> 154["Missile Range III"]
		142 -..-> 154
		125 -..-> 155["Missile Precision III"]
		142 -..-> 155
		133 -..-> 156["Cruise Missiles Damage III"]
		140 -..-> 156
		134 -..-> 157["Cruise Missiles Precision III"]
		140 -..-> 157
		135 -..-> 158["Cruise Missiles Range III"]
		140 -..-> 158
		325 -..-> 330["Weapon: Silver Cloud"]
		322 -..-> 330
		321 -..-> 330
		320 -..-> 330
		326 -..-> 331["Weapon: Rapid Gun"]
		322 -..-> 331
		321 -..-> 331
		320 -..-> 331
		515 -..-> 530["Weapon: Grav Mortar"]
		520 -..-> 530
		522 -..-> 530
		521 -..-> 530
		516 -..-> 531["Weapon: Incendiary Bomb"]
		520 -..-> 531
		522 -..-> 531
		521 -..-> 531
		515 -..-> 532["Weapon: Breach Gun"]
		520 -..-> 532
		522 -..-> 532
		521 -..-> 532
		615 -..-> 630["Weapon: Typhoon"]
		620 -..-> 630
		622 -..-> 630
		621 -..-> 630
		616 -..-> 631["Weapon: Marauder"]
		620 -..-> 631
		622 -..-> 631
		621 -..-> 631
		705 -..-> 730["Strong Materials II"]
		710 -..-> 730
		705 -..-> 731["Light Materials II"]
		711 -..-> 731
		805 -..-> 830["Battery Life II"]
		810 -..-> 830
		805 -..-> 831["Field Calculus II"]
		811 -..-> 831
		905 -..-> 930["Cryptographic Imperative II"]
		910 -..-> 930
		905 -..-> 931["Fuzzy Optimization II"]
		911 -..-> 931
		905 -..-> 932["Dark Matter Composites II"]
		912 -..-> 932
		1311 -..-> 1320["Air Domination II"]
		1316 -..-> 1320
		1316 -..-> 1321["Battle Momentum II"]
		1312 -..-> 1321
		1510 -..-> 1530["Integrated Service II"]
		1505 -..-> 1530
		1511 -..-> 1531["Segmented Production II"]
		1505 -..-> 1531
		1512 -..-> 1532["Grid Dynamics II"]
		1505 -..-> 1532
		1525 -..-> 1533["Establishment: Nanocell Hull"]
		1615 -..-> 1630["Chassis: Armature"]
		1622 -..-> 1630
		1620 -..-> 1631["Chassis: HAV"]
		1621 -..-> 1631
		1620 -..-> 1632["Chassis: Warden"]
		1622 -..-> 1632
		1615 -..-> 1633["Chassis: SPG"]
		1621 -..-> 1633
		1622 -..-> 1633
		1615 -..-> 1634["Chassis: Support Vehicle"]
		1621 -..-> 1634
		1715 -..-> 1730["Chassis: Gun Turret"]
		1722 -..-> 1730
		1715 -..-> 1731["Chassis: Missile Turret"]
		1721 -..-> 1731
		1717 -..-> 1732
		1721 -..-> 1732
		1722 -..-> 1732
		1718 -..-> 1733["Chassis: Fowler"]
		1722 -..-> 1733
		1715 -..-> 1734["Chassis: Sentinel Turret"]
		1715 -..-> 1735["Chassis: Science Vessel"]
		1721 -..-> 1735
		1722 -..-> 1735
		1820 -..-> 1830["Chassis: Elite Guard"]
		1821 -..-> 1830
		1816 -..-> 1831["Factory: Special Forces Academy"]
		1820 -..-> 1831
		1821 -..-> 1831
		1817 -..-> 1832["Chassis: Biotech"]
		1820 -..-> 1832
		1821 -..-> 1832
		1818 -..-> 1833["Chassis: Barker"]
		1821 -..-> 1833
		1820 -..-> 1833
		1815 -..-> 1834["Chassis: Brute"]
		1831 -..-> 1843["Chassis: Quantum Ops"]
		1830 -..-> 1843
		1920 -..-> 1930["Chassis: Nereid"]
		1918 -..-> 1930
		1917 -..-> 1931["Chassis: Submarine"]
		1921 -..-> 1931
		1920 -..-> 1932["Chassis: Cruiser"]
		1916 -..-> 1932
		1915 -..-> 1933["Chassis: Seamoth"]
		1921 -..-> 1933
		1821 -..-> 2000["Automated Production"]
		1722 -..-> 2000
		1622 -..-> 2000
	end
	subgraph "Cost 7800"
	direction TB
		220 -..-> 240["Weapon: Prism"]
		222 -..-> 240
		221 -..-> 240
		230 -..-> 240
		220 -..-> 241["Lasers Precision III"]
		217 -..-> 241
		221 -..-> 242["Lasers Attack Rate III"]
		216 -..-> 242
		222 -..-> 243["Lasers Damage III"]
		230 -..-> 243
		516 -..-> 535["Bombs Rate I"]
		520 -..-> 535
		516 -..-> 536["Bombs Splash I"]
		521 -..-> 536
		616 -..-> 635["Plasma Precision I"]
		621 -..-> 635
		616 -..-> 636["Plasma Range I"]
		620 -..-> 636
		725 -..-> 735["Armor: Large Composite Armor"]
		726 -..-> 736["Armor: Small Reactive Armor"]
		730 -..-> 736
		731 -..-> 736
		722 -..-> 737["Armor: Quantum Armor"]
		730 -..-> 737
		731 -..-> 737
		725 -..-> 738["Hull Integrity I"]
		730 -..-> 738
		731 -..-> 738
		825 -..-> 835["Shield: Cluster Shield"]
		826 -..-> 836["Shield: Small Neutronium Shield"]
		830 -..-> 836
		831 -..-> 836
		827 -..-> 837["Shield: Locust Shield"]
		830 -..-> 837
		831 -..-> 837
		825 -..-> 838["Transmission Power I"]
		830 -..-> 838
		831 -..-> 838
		1320 -..-> 1330["Chassis: Blue Thunder"]
		1318 -..-> 1330
		1317 -..-> 1331["Factory: Airforge"]
		1316 -..-> 1331
		1320 -..-> 1332["Chassis: Launcher Pad"]
		1316 -..-> 1332
		1315 -..-> 1333["Chassis: Ramraider"]
		1321 -..-> 1333
		1525 -..-> 1540["Facility: Smeltery"]
		1531 -..-> 1540
		1525 -..-> 1541["Facility: Axial Compressor"]
		1530 -..-> 1541
		1525 -..-> 1542["Facility: Vacuum Cleaner"]
		1532 -..-> 1542
		1522 -..-> 1543["Facility: Atomic Pile"]
		1532 -..-> 1543
		1521 -..-> 1544["Facility: Cold Fusion Reactor"]
		1530 -..-> 1544
		1520 -..-> 1545["Facility: Cistern"]
		1531 -..-> 1545
		1621 -..-> 1635["Multiplexed Control 1"]
		1622 -..-> 1635
		1721 -..-> 1740["Sapient Nexus I"]
		1722 -..-> 1740
		1715 -..-> 1741["Chassis: AA Turret"]
		1721 -..-> 1741
		1722 -..-> 1741
		1735 -..-> 1742["Chassis: The Wanderer"]
		1820 -..-> 1840["Gene Splicing I"]
		1821 -..-> 1840
		1816 -..-> 1841["Factory: Military University"]
		1818 -..-> 1841
		1815 -..-> 1841
		1832 -..-> 1842["Factory: Bioforge"]
		1920 -..-> 1940["Fleet Commando I"]
		1921 -..-> 1940
		1920 -..-> 1941["Chassis: Battleship"]
		1921 -..-> 1941
		2000 -..-> 2010["Factory: Junkyard"]
		2000 -..-> 2011["Weapon: Missile Drone"]
		124 -..-> 2011
		2000 -..-> 2012["Ability: Mobilization"]
	end
	subgraph "Cost 10200"
	direction TB
		153 -..-> 160["Weapon: Swarm Missiles"]
		154 -..-> 160
		155 -..-> 160
		156 -..-> 161["Weapon: Phoenix Missile"]
		158 -..-> 161
		157 -..-> 161
		156 -..-> 162["Weapon: Gliding Missile"]
		158 -..-> 162
		157 -..-> 162
		153 -..-> 163["Weapon: Proton Missile"]
		154 -..-> 163
		155 -..-> 163
		150 -..-> 164["Weapon: SS-18"]
		152 -..-> 164
		151 -..-> 164
		170["Rockets Damage III"] -..-> 180["Weapon: Tiamat"]
		172["Rockets Precision III"] -..-> 180
		157 -..-> 180
		143 -..-> 180
		171["Rockets Range III"] -..-> 181["Weapon: Warp Artillery"]
		158 -..-> 181
		170 -..-> 181
		143 -..-> 181
		232 -..-> 250["Energy Weapons Precision II"]
		223 -..-> 250
		232 -..-> 251["Energy Weapons Range II"]
		224 -..-> 251
		232 -..-> 252["Energy Weapons Damage II"]
		225 -..-> 252
		220 -..-> 253["Weapon: THOR"]
		222 -..-> 253
		221 -..-> 253
		231 -..-> 253
		326 -..-> 340["MG Precision III"]
		320 -..-> 340
		326 -..-> 341["MG Range III"]
		321 -..-> 341
		326 -..-> 342["MG Damage III"]
		322 -..-> 342
		416 -..-> 440["Cannons Precision III"]
		420 -..-> 440
		417 -..-> 441["Cannons Attack Rate III"]
		421 -..-> 441
		415 -..-> 442["Cannons Damage III"]
		422 -..-> 442
		727 -..-> 740["Strong Materials III"]
		730 -..-> 740
		727 -..-> 741["Light Materials III"]
		731 -..-> 741
		735 -..-> 742["Armor: Large Nano Armor"]
		731 -..-> 742
		730 -..-> 742
		827 -..-> 840["Battery Life III"]
		830 -..-> 840
		827 -..-> 841["Field Calculus III"]
		831 -..-> 841
		836 -..-> 842["Shield: Large Neutronium Shied"]
		835 -..-> 842
		837 -..-> 843["Shield: Neutronium Shield"]
		836 -..-> 843
		931 -..-> 940["Establishment: VPR Network"]
		930 -..-> 940
		931 -..-> 941["Establishment: Simulation Chamber"]
		932 -..-> 941
		922 -..-> 942["Establishment: Grinder"]
		932 -..-> 942
		925 -..-> 943["Distributed Simulation"]
		930 -..-> 943
		931 -..-> 943
		932 -..-> 943
		1320 -..-> 1340["Strike Supremacy I"]
		1321 -..-> 1340
		1331 -..-> 1341["Chassis: Carrier"]
		1320 -..-> 1341
		1321 -..-> 1341
		1331 -..-> 1342["Chassis: Bomber"]
		1320 -..-> 1342
		1331 -..-> 1343["Chassis: Fighter"]
		1321 -..-> 1343
		1317 -..-> 1344["Chassis: Eagle"]
		1321 -..-> 1344
		1320 -..-> 1344
		1530 -..-> 1550["Integrated Service III"]
		1531 -..-> 1551["Segmented Production III"]
		1532 -..-> 1552["Grid Dynamics III"]
		1531 -..-> 1553["Infrastructure Project I"]
		1532 -..-> 1553
		1530 -..-> 1553
		1632 -..-> 1640["Chassis: Convictor"]
		1635 -..-> 1640
		1630 -..-> 1641["Chassis: Crusader"]
		1635 -..-> 1641
		1631 -..-> 1642["Chassis: Groundbreaker"]
		1635 -..-> 1642
		1633 -..-> 1643["Chassis: Licorn"]
		1635 -..-> 1643
		1633 -..-> 1644["Chassis: Colossus"]
		1635 -..-> 1644
		1721 -..-> 1745["Robocraft III"]
		1722 -..-> 1746["Global Network III"]
		1840 -..-> 1850["Chassis: Neosapient"]
		1832 -..-> 1850
		1840 -..-> 1851["Chassis: Swarm"]
		1842 -..-> 1851
		1840 -..-> 1852["Chassis: Hydra"]
		1842 -..-> 1852
		1840 -..-> 1853["Chassis: Gollywog"]
		1832 -..-> 1853
		1820 -..-> 1854["Footcraft III"]
		1821 -..-> 1855["Crossbreed Psionics III"]
		1940 -..-> 1950["Chassis: Triton"]
		1930 -..-> 1950
		1940 -..-> 1951["Chassis: Poseidon"]
		1932 -..-> 1951
		1940 -..-> 1952["Chassis: Angler"]
		1931 -..-> 1952
		2000 -..-> 2020["Massive Production"]
		1740 -..-> 2020
		1635 -..-> 2020
		1840 -..-> 2020
		2011 -..-> 2021["Drone Augmentation I"]
		1311 -..-> 2021
		2011 -..-> 2022["Weapon: Laser Drone"]
		221 -..-> 2022
	end
	subgraph "Cost 11400"
	direction TB
		736 -..-> 745["Armor: Reactive Armor"]
		741 -..-> 745
		721 -..-> 746["Armor: Advanced Power Armor"]
		740 -..-> 746
		742 -..-> 747["Armor: Large Quantum Armor"]
		741 -..-> 747
		740 -..-> 747
		736 -..-> 748["Armor: Corsair Armor"]
		741 -..-> 748
		740 -..-> 748
		826 -..-> 845["Shield: Small Nano Shield"]
		840 -..-> 845
		837 -..-> 846["Shield: Nano Shield"]
		840 -..-> 846
		835 -..-> 847["Shield: Large Nano Shield"]
		840 -..-> 847
		920 -..-> 945["Establishment: Frequency Array"]
		930 -..-> 945
		921 -..-> 946["Establishment: Tachyon Lattice"]
		931 -..-> 946
		1553 -..-> 1560["Establishment: Reactive Hull"]
		1533 -..-> 1560
		1553 -..-> 1561["Establishment: Neutrino Hull"]
		1533 -..-> 1561
		1632 -..-> 1645["Factory: Automated Assembly Yard"]
		1635 -..-> 1645
		1630 -..-> 1646["Factory: Machinery"]
		1635 -..-> 1646
		1732 -..-> 1750["Chassis: Vulture"]
		1740 -..-> 1750
		1718 -..-> 1751["Chassis: Surveyor"]
		1740 -..-> 1751
		1716 -..-> 1752["Factory: Nexus Foundry"]
		1740 -..-> 1752
		1920 -..-> 1955["Shipcraft III"]
		1921 -..-> 1956["Subdimensional Sonars III"]
	end
	subgraph "Cost 12600"
	direction TB
		1330 -..-> 1350["Chassis: Flying Fox"]
		1340 -..-> 1350
		1342 -..-> 1351["Chassis: Mauler"]
		1340 -..-> 1351
		1332 -..-> 1352["Chassis: Trebuchet"]
		1340 -..-> 1352
		1343 -..-> 1353["Chassis: Mantis"]
		2010 -..-> 2030["Factory: Titan Forge"]
		2020 -..-> 2030
		1806 -..-> 2031["Chassis: Combat Mech"]
		730 -..-> 2031
		2020 -..-> 2031
		1760["Chassis: Quadrapod"] -..-> 2032["Chassis: Fortress"]
		830 -..-> 2032
		2020 -..-> 2032
	end
	subgraph "Cost 13800"
	direction TB
		150 -..-> 170
		164 -..-> 170
		151 -..-> 171
		164 -..-> 171
		152 -..-> 172
		164 -..-> 172
		331 -..-> 350["Weapon: Chopper"]
		341 -..-> 350
		331 -..-> 351["Weapon: Poisonous Machine Gun"]
		342 -..-> 351
		331 -..-> 352["Weapon: SPARC"]
		340 -..-> 352
		341 -..-> 353["Weapon: Proton Machine Gun"]
		342 -..-> 353
		340 -..-> 353
		415 -..-> 445["Weapon: Nuke Shells"]
		416 -..-> 445
		417 -..-> 445
		515 -..-> 540["Launchers Range III"]
		520 -..-> 540
		515 -..-> 541["Launchers Splash III"]
		521 -..-> 541
		515 -..-> 542["Launchers Damage III"]
		522 -..-> 542
		615 -..-> 640["Mass Drivers Range III"]
		620 -..-> 640
		615 -..-> 641["Mass Drivers Rate III"]
		621 -..-> 641
		615 -..-> 642["Mass Drives Damage III"]
		622 -..-> 642
		738 -..-> 750["Hull Integrity II"]
		740 -..-> 750
		741 -..-> 750
		738 -..-> 751["Organic Matters I"]
		740 -..-> 751
		741 -..-> 751
		738 -..-> 752["Wave Commune I"]
		740 -..-> 752
		741 -..-> 752
		838 -..-> 850["Transmission Power II"]
		840 -..-> 850
		841 -..-> 850
		826 -..-> 851["Shield: Small Hull Shield"]
		841 -..-> 851
		837 -..-> 852["Shield: Hull Shield"]
		841 -..-> 852
		835 -..-> 853["Shield: Large Hull Shield"]
		841 -..-> 853
		922 -..-> 950["Cryptographic Imperative III"]
		930 -..-> 950
		922 -..-> 951["Fuzzy Optimization III"]
		931 -..-> 951
		922 -..-> 952["Dark Matter Composites III"]
		932 -..-> 952
		1543 -..-> 1565["Facility: Mass Relay"]
		1552 -..-> 1565
		1544 -..-> 1566["Facility: Hyperspace Compactor"]
		1550 -..-> 1566
		1545 -..-> 1567["Facility: Recycler"]
		1551 -..-> 1567
		1621 -..-> 1650["Wheelcraft III"]
		1622 -..-> 1651["Mass Education III"]
		1752 -..-> 1760
		1745 -..-> 1760
		1746 -..-> 1760
		1721 -..-> 1761["Sapient Nexus II"]
		1722 -..-> 1761
		1750 -..-> 1762["Chassis: Hawk"]
		1745 -..-> 1762
		1746 -..-> 1762
	end
	subgraph "Cost 15600"
	direction TB
		838 -..-> 855["Emission Trenching I"]
		840 -..-> 855
		841 -..-> 855
		838 -..-> 856["Metametrics I"]
		840 -..-> 856
		841 -..-> 856
	end
	subgraph "Cost 17400"
	direction TB
		142 -..-> 182["Weapon: Antimatter Missile"]
		163 -..-> 182
		156 -..-> 182
		143 -..-> 182
		171 -..-> 183["Weapon: SAM Defense"]
		157 -..-> 183
		144 -..-> 183
		141 -..-> 184["Weapon: Goliath Missile"]
		172 -..-> 184
		161 -..-> 184
		141 -..-> 185["Weapon: Jericho"]
		140 -..-> 185
		158 -..-> 185
		531 -..-> 545["Weapon: Nail Bomb"]
		536 -..-> 545
		531 -..-> 546["Weapon: Cluster Bomb"]
		537 -..-> 546
		531 -..-> 547["Weapon: Laser Guided Bomb"]
		535 -..-> 547
		542 -..-> 550["Weapon: Gravity Mines"]
		540 -..-> 550
		541 -..-> 550
		531 -..-> 551["Weapon: Reaper Bomb"]
		537 -..-> 551
		535 -..-> 551
		536 -..-> 551
		531 -..-> 556["Bombs Rate II"]
		535 -..-> 556
		531 -..-> 557["Bombs Splash II"]
		536 -..-> 557
		531 -..-> 558["Bombs Damage II"]
		537 -..-> 558
		642 -..-> 650["Weapon: Coil Mortar"]
		640 -..-> 650
		641 -..-> 650
		631 -..-> 651["Weapon: Flamethrower"]
		636 -..-> 651
		635 -..-> 651
		637 -..-> 651
		631 -..-> 652["Weapon: Antimatter Cannon"]
		642 -..-> 652
		640 -..-> 652
		641 -..-> 652
		631 -..-> 656["Plasma Precision II"]
		635 -..-> 656
		631 -..-> 657["Plasma Range II"]
		636 -..-> 657
		631 -..-> 658["Plasma Damage II"]
		637 -..-> 658
		742 -..-> 760["Strong Materials IV"]
		740 -..-> 760
		742 -..-> 761["Light Materials IV"]
		741 -..-> 761
		745 -..-> 762["Armor: Large Reactive Armor"]
		742 -..-> 762
		751 -..-> 763["Armor: Bio Armor"]
		737 -..-> 763
		751 -..-> 764["Armor: Large Bio Armor"]
		742 -..-> 764
		752 -..-> 765["Armor: Gluon Armor"]
		737 -..-> 765
		752 -..-> 766["Armor: Large Gluon Armor"]
		742 -..-> 766
		840 -..-> 860["Battery Life IV"]
		841 -..-> 861["Field Calculus IV"]
		855 -..-> 862["Shield: Plasma Shield"]
		837 -..-> 862
		855 -..-> 863["Shield: Large Plasma Shield"]
		835 -..-> 863
		837 -..-> 864["Shield: Warp Shield"]
		856 -..-> 864
		856 -..-> 865["Shield: Large Warp Shield"]
		835 -..-> 865
		942 -..-> 960["Establishment: Manufactory"]
		952 -..-> 960
		951 -..-> 960
		950 -..-> 960
		943 -..-> 961["Deep Command"]
		950 -..-> 961
		951 -..-> 961
		952 -..-> 961
		940 -..-> 962["Metablockchain"]
		941 -..-> 962
		950 -..-> 962
		951 -..-> 962
		952 -..-> 962
		1320 -..-> 1360["Air Domination III"]
		1331 -..-> 1360
		1331 -..-> 1361["Battle Momentum III"]
		1321 -..-> 1361
		1317 -..-> 1362["Chassis: Osprey"]
		1321 -..-> 1362
		1320 -..-> 1362
		1332 -..-> 1363["Chassis: Helipad"]
		1341 -..-> 1363
		1542 -..-> 1570["Facility: Null Transporter"]
		1552 -..-> 1570
		1550 -..-> 1570
		1551 -..-> 1570
		1542 -..-> 1571["Facility: Particle Condenser"]
		1552 -..-> 1571
		1550 -..-> 1571
		1551 -..-> 1571
		1540 -..-> 1572["Facility: Tempering Cauldron"]
		1552 -..-> 1572
		1550 -..-> 1572
		1551 -..-> 1572
		1541 -..-> 1573["Facility: Workshop"]
		1552 -..-> 1573
		1550 -..-> 1573
		1551 -..-> 1573
		1553 -..-> 1574["Establishment: Amphitheatre"]
		1525 -..-> 1574
		1643 -..-> 1655["Chassis: Assault Battery"]
		1651 -..-> 1655
		1643 -..-> 1656["Chassis: Rhino"]
		1650 -..-> 1656
		1641 -..-> 1657["Chassis: Dragoon"]
		1650 -..-> 1657
		1752 -..-> 1770["Factory: Self Maintained Factory"]
		1761 -..-> 1770
		1760 -..-> 1771["Chassis: Ravager"]
		1761 -..-> 1771
		1850 -..-> 1860["Chassis: Chimaera"]
		1855 -..-> 1860
		1854 -..-> 1860
		1833 -..-> 1861["Chassis: Tracer"]
		1855 -..-> 1861
		1854 -..-> 1861
		1951 -..-> 1960["Chassis: Aurora"]
		1955 -..-> 1960
		1951 -..-> 1961["Chassis: Athene"]
		1956 -..-> 1961
		1952 -..-> 1962["Chassis: Sawfish"]
		1956 -..-> 1962
		1941 -..-> 1963["Chassis: Ironclad"]
		1955 -..-> 1963
		1917 -..-> 1964["Factory: Naval Engineering Corpus"]
		1916 -..-> 1964
		2022 -..-> 2040["Drone Augmentation II"]
		1320 -..-> 2040
		2031 -..-> 2041["Titan Quest I"]
		2032 -..-> 2041
		2032 -..-> 2042["Chassis: Charon"]
		2032 -..-> 2043["Chassis: Fort-REX"]
		2031 -..-> 2044["Chassis: Jupiter"]
		2031 -..-> 2045["Chassis: Behemoth"]
	end
	subgraph "Cost 18600"
	direction TB
		250 -..-> 260["Weapon: Star Beam"]
		252 -..-> 260
		251 -..-> 260
		232 -..-> 260
		250 -..-> 261["Weapon: AALPD"]
		222 -..-> 261
		221 -..-> 261
		251 -..-> 261
		353 -..-> 360(["MG Precision ∞"])
		353 -..-> 361(["MG Range ∞"])
		353 -..-> 362(["MG Damage ∞"])
		551 -..-> 560["Weapon: Toad Mine"]
		558 -..-> 560
		556 -..-> 560
		557 -..-> 560
		651 -..-> 660["Weapon: Cathode Gun"]
		658 -..-> 660
		656 -..-> 660
		657 -..-> 660
		748 -..-> 770["Armor: Raid Armor"]
		737 -..-> 770
		761 -..-> 770
		760 -..-> 770
		748 -..-> 771["Armor: Large Raid Armor"]
		747 -..-> 771
		761 -..-> 771
		760 -..-> 771
		737 -..-> 772["Armor: Positron Armor"]
		761 -..-> 772
		737 -..-> 773["Armor: Electron Armor"]
		760 -..-> 773
		747 -..-> 774["Armor: Probability Armor"]
		761 -..-> 774
		747 -..-> 775["Armor: Inert Armor"]
		760 -..-> 775
		846 -..-> 870["Shield: SMA Shield"]
		852 -..-> 870
		861 -..-> 870
		847 -..-> 871["Shield: Large SMA Shield"]
		853 -..-> 871
		861 -..-> 871
		846 -..-> 872["Shield: Hardened Shield"]
		852 -..-> 872
		860 -..-> 872
		847 -..-> 873["Shield: Immortal Shield"]
		853 -..-> 873
		860 -..-> 873
		837 -..-> 874["Shield: Quantum Shield"]
		860 -..-> 874
		861 -..-> 874
		835 -..-> 875["Shield: Recharging Shield"]
		860 -..-> 875
		861 -..-> 875
		1341 -..-> 1370["Chassis: Leviathan"]
		1361 -..-> 1370
		1341 -..-> 1371["Chassis: Cloud Orca"]
		1360 -..-> 1371
		1360 -..-> 1373["Strike Supremacy II"]
		1361 -..-> 1373
		1340 -..-> 1373
		1551 -..-> 1575["Infrastructure Project II"]
		1552 -..-> 1575
		1550 -..-> 1575
		1553 -..-> 1575
		1650 -..-> 1660["Multiplexed Control 2"]
		1651 -..-> 1660
		1616 -..-> 1661["Factory: Heavy Vehicle Factory"]
		1651 -..-> 1661
		1650 -..-> 1661
		1634 -..-> 1662["Chassis: Scanner"]
		1651 -..-> 1662
		1650 -..-> 1662
		1762 -..-> 1780(["Cyberrule ∞"])
		1771 -..-> 1780
		1751 -..-> 1780
		1720 -..-> 1780
		1735 -..-> 1780
		1762 -..-> 1781(["Silicon Infusion ∞"])
		1771 -..-> 1781
		1751 -..-> 1781
		1720 -..-> 1781
		1735 -..-> 1781
		1840 -..-> 1865["Gene Splicing II"]
		1854 -..-> 1865
		1855 -..-> 1865
		1940 -..-> 1965["Fleet Commando II"]
		1955 -..-> 1965
		1956 -..-> 1965
		1340 -..-> 2050["Chassis: Mothership"]
		2011 -..-> 2050
		2020 -..-> 2050
		1940 -..-> 2051["Chassis: Man-Of-War"]
		2011 -..-> 2051
		2020 -..-> 2051
		2041 -..-> 2052["Paragon Coordination"]
		2012 -..-> 2052
		2022 -..-> 2053["Weapon: Anti-Personel Drone"]
		557 -..-> 2053
		2022 -..-> 2054["Drone Augmentation III"]
		1360 -..-> 2054
	end
	subgraph "Cost 22200"
	direction TB
		170 -..-> 190(["Rocketry Damage ∞"])
		156 -..-> 190
		153 -..-> 190
		172 -..-> 191(["Rocketry Precision ∞"])
		157 -..-> 191
		155 -..-> 191
		171 -..-> 192(["Rocketry Range ∞"])
		158 -..-> 192
		154 -..-> 192
		253 -..-> 265["Energy Weapons Precision III"]
		250 -..-> 265
		253 -..-> 266["Energy Weapons Range III"]
		251 -..-> 266
		253 -..-> 267["Energy Weapons Damage III"]
		252 -..-> 267
		430 -..-> 450["Weapon: Heavy Artillery"]
		431 -..-> 450
		432 -..-> 450
		551 -..-> 565["Bombs Rate III"]
		556 -..-> 565
		551 -..-> 566["Bombs Splash III"]
		557 -..-> 566
		551 -..-> 567["Bombs Damage III"]
		558 -..-> 567
		651 -..-> 665["Plasma Precision III"]
		656 -..-> 665
		651 -..-> 666["Plasma Range III"]
		657 -..-> 666
		651 -..-> 667["Plasma Damage III"]
		658 -..-> 667
		760 -..-> 780["Strong Materials V"]
		761 -..-> 781["Light Materials V"]
		750 -..-> 782["Hull Integrity III"]
		760 -..-> 782
		761 -..-> 782
		860 -..-> 880["Battery Life V"]
		861 -..-> 881["Field Calculus V"]
		850 -..-> 882["Transmission Power III"]
		860 -..-> 882
		861 -..-> 882
		961 -..-> 970["Hyperpacked Energy Cells"]
		962 -..-> 970
		961 -..-> 971["Transuranium Crystals"]
		962 -..-> 971
		961 -..-> 972["Psionic Ascendancy"]
		962 -..-> 972
		310 -..-> 1000["AA Precision I"]
		210 -..-> 1000
		113 -..-> 1000
		312 -..-> 1010["AA Damage I"]
		115 -..-> 1010
		212 -..-> 1010
		311 -..-> 1020["AA Range I"]
		127 -..-> 1020
		224 -..-> 1020
		535 -..-> 1100["Warp Weapons Rate I"]
		411 -..-> 1100
		635 -..-> 1100
		631 -..-> 1105["Weapon: Warp Projector"]
		531 -..-> 1105
		416 -..-> 1105
		537 -..-> 1110["Warp Weapons Damage I"]
		412 -..-> 1110
		637 -..-> 1110
		510 -..-> 1120["Warp Weapons Range I"]
		410 -..-> 1120
		636 -..-> 1120
		1351 -..-> 1375["Chassis: Mirage"]
		1360 -..-> 1375
		1361 -..-> 1375
		1332 -..-> 1376["Chassis: Mangonel"]
		1361 -..-> 1376
		1360 -..-> 1376
		1362 -..-> 1377["Chassis: Sparrow"]
		1361 -..-> 1377
		1360 -..-> 1377
		1575 -..-> 1580["Establishment: Holostage"]
		1574 -..-> 1580
		1575 -..-> 1581["Establishment: Observation Deck"]
		1574 -..-> 1581
		1575 -..-> 1582["Establishment: Vertebral Armor"]
		1560 -..-> 1582
		1575 -..-> 1583["Establishment: Nanite Cloud"]
		1561 -..-> 1583
		1575 -..-> 1584["Facility: Wormhole Projector"]
		1571 -..-> 1584
		1570 -..-> 1584
		1580 -..-> 1585(["Infrastructure Project ∞"])
		1581 -..-> 1585
		1584 -..-> 1585
		1645 -..-> 1665["Factory: Warfare Multiplex"]
		1646 -..-> 1665
		1660 -..-> 1665
		1657 -..-> 1666(["Mechanics ∞"])
		1656 -..-> 1666
		1655 -..-> 1666
		1634 -..-> 1666
		1657 -..-> 1667(["Wheel Drive ∞"])
		1656 -..-> 1667
		1655 -..-> 1667
		1634 -..-> 1667
		1780 -..-> 1790["Cybernetic Conversion"]
		1781 -..-> 1790
		1780 -..-> 1791["Orbital Uplink"]
		1781 -..-> 1791
		1780 -..-> 1792["Cryostasis Grid"]
		1781 -..-> 1792
		1865 -..-> 1870["Chassis: Xenoswarm"]
		1851 -..-> 1870
		1865 -..-> 1871["Chassis: Scylla"]
		1852 -..-> 1871
		1842 -..-> 1872["Factory: Cloning Vats"]
		1865 -..-> 1872
		1965 -..-> 1970["Chassis: Dreadnought"]
		1941 -..-> 1970
		1964 -..-> 1971["Factory: Navyforge"]
		1965 -..-> 1971
		1906 -..-> 1972["Chassis: Nettle"]
		1965 -..-> 1972
		2041 -..-> 2060["Titan Quest II"]
		2050 -..-> 2060
		2051 -..-> 2060
		2031 -..-> 2061["Chassis: SCV"]
		2041 -..-> 2061
		2050 -..-> 2062["Chassis: Matriarch"]
		2040 -..-> 2062
		2041 -..-> 2062
		2050 -..-> 2063["Chassis: Vindicator"]
		2050 -..-> 2064["Chassis: Admiral's Pride"]
		2053 -..-> 2065["Weapon: Hunter-Seeker Drone"]
		2040 -..-> 2065
	end
	subgraph "Cost 24600"
	direction TB
		241 -..-> 280(["Beams Precision ∞"])
		265 -..-> 280
		242 -..-> 281(["Beams Rate ∞"])
		266 -..-> 281
		243 -..-> 282(["Beams Damage ∞"])
		267 -..-> 282
		540 -..-> 570(["Launchers and Bombs Range ∞"])
		565 -..-> 570
		541 -..-> 571(["Launchers and Bombs Splash ∞"])
		566 -..-> 571
		567 -..-> 572(["Launchers and Bombs Damage ∞"])
		542 -..-> 572
		547 -..-> 575["Weapon: Nuclear Bomb"]
		567 -..-> 575
		565 -..-> 575
		566 -..-> 575
		545 -..-> 576["Weapon: Antimatter Bomb"]
		567 -..-> 576
		565 -..-> 576
		566 -..-> 576
		546 -..-> 577["Weapon: Black Hole Bomb"]
		567 -..-> 577
		565 -..-> 577
		566 -..-> 577
		640 -..-> 670(["Mass Drivers and Plasma Range ∞"])
		666 -..-> 670
		641 -..-> 671(["Mass Drivers and Plasma Precision ∞"])
		665 -..-> 671
		667 -..-> 672(["Mass Drivers and Plasma Damage ∞"])
		642 -..-> 672
		660 -..-> 675["Weapon: Grapes Of Wrath"]
		667 -..-> 675
		665 -..-> 675
		666 -..-> 675
	end
	subgraph "Cost 25800"
	direction TB
		445 -..-> 460(["Cannons Precision ∞"])
		440 -..-> 460
		445 -..-> 461(["Cannons Attack Rate ∞"])
		441 -..-> 461
		445 -..-> 462(["Cannons Damage ∞"])
		442 -..-> 462
		726 -..-> 785["Armor: Small Defense Matrix"]
		780 -..-> 785
		781 -..-> 785
		737 -..-> 786["Armor: Defense Matrix"]
		780 -..-> 786
		781 -..-> 786
		747 -..-> 787["Armor: Large Defense Matrix"]
		780 -..-> 787
		781 -..-> 787
		782 -..-> 788["Organic Matters II"]
		751 -..-> 788
		752 -..-> 789["Wave Commune II"]
		782 -..-> 789
		846 -..-> 885["Shield: Antimatter Shield"]
		880 -..-> 885
		881 -..-> 885
		863 -..-> 886["Shield: Gluon Shield"]
		880 -..-> 886
		881 -..-> 886
		865 -..-> 887["Shield: Ion Curtain"]
		880 -..-> 887
		881 -..-> 887
		882 -..-> 888["Emission Trenching II"]
		855 -..-> 888
		856 -..-> 889["Metametrics II"]
		882 -..-> 889
		320 -..-> 1001["AA Precision II"]
		220 -..-> 1001
		125 -..-> 1001
		1000 -..-> 1001
		322 -..-> 1011["AA Damage II"]
		123 -..-> 1011
		222 -..-> 1011
		321 -..-> 1021["AA Range II"]
		135 -..-> 1021
		251 -..-> 1021
		315 -..-> 1050["Weapon: Carapace ADS"]
		144 -..-> 1050
		1020 -..-> 1050
		1010 -..-> 1050
		1000 -..-> 1050
		556 -..-> 1101["Warp Weapons Rate II"]
		421 -..-> 1101
		656 -..-> 1101
		1100 -..-> 1101
		558 -..-> 1111["Warp Weapons Damage II"]
		422 -..-> 1111
		658 -..-> 1111
		1110 -..-> 1111
		1105 -..-> 1115["Weapon: Mass Condenser"]
		1100 -..-> 1115
		1110 -..-> 1115
		1120 -..-> 1115
		520 -..-> 1121["Warp Weapons Range II"]
		420 -..-> 1121
		657 -..-> 1121
		1120 -..-> 1121
		1375 -..-> 1380(["Aerobatics ∞"])
		1343 -..-> 1380
		1377 -..-> 1380
		1350 -..-> 1380
		1333 -..-> 1381(["Schemed Collision ∞"])
		1376 -..-> 1381
		1363 -..-> 1381
		1341 -..-> 1381
		1350 -..-> 1382(["Demolition ∞"])
		1363 -..-> 1382
		1333 -..-> 1382
		1373 -..-> 1382
		1306 -..-> 1383["Chassis: Support Aircraft"]
		1373 -..-> 1383
		1375 -..-> 1384["Chassis: Blackjack"]
		1373 -..-> 1384
		1343 -..-> 1385["Chassis: Falcon"]
		1373 -..-> 1385
		1585 -..-> 1590["Managed Storage"]
		1585 -..-> 1591["Unnatural Selection"]
		1585 -..-> 1592["Transmutational Logistics"]
		1665 -..-> 1670["Factory: Massive Industrial Complex"]
		1666 -..-> 1670
		1667 -..-> 1670
		1666 -..-> 1671["Covert Operations"]
		1667 -..-> 1671
		1666 -..-> 1672["Transformable Templates"]
		1667 -..-> 1672
		1870 -..-> 1880(["Biomastery ∞"])
		1871 -..-> 1880
		1870 -..-> 1881(["Cellular Augmentation ∞"])
		1871 -..-> 1881
		1880 -..-> 1885["Chassis: Charybdis"]
		1881 -..-> 1885
		1971 -..-> 1980(["Seafaring ∞"])
		1952 -..-> 1980
		1963 -..-> 1980
		1951 -..-> 1980
		1971 -..-> 1981(["Quantum Floating ∞"])
		1952 -..-> 1981
		1963 -..-> 1981
		1951 -..-> 1981
		2053 -..-> 2070["Weapon: Disruptor Drone"]
		2054 -..-> 2070
		2041 -..-> 2071["Drone Predominance ∞"]
		2054 -..-> 2071
		2060 -..-> 2072["Atlas Shrugged ∞"]
		750 -..-> 2072
		850 -..-> 2072
	end
	subgraph "Cost 28200"
	direction TB
		265 -..-> 270["Weapon: Death Ray"]
		267 -..-> 270
		266 -..-> 270
		260 -..-> 270
		265 -..-> 271["Weapon: Quantum Exciter"]
		267 -..-> 271
		266 -..-> 271
		240 -..-> 271
		889 -..-> 895(["Shield Capacity ∞"])
		888 -..-> 895
		1980 -..-> 1990["Tension Alteration"]
		1981 -..-> 1990
		1980 -..-> 1991["Mioplasma Coating"]
		1981 -..-> 1991
		1980 -..-> 1992["Spontaneous Gluonification"]
		1981 -..-> 1992
	end
	subgraph "Cost 31800"
	direction TB
		190 -..-> 193["Zero Element Fuel"]
		191 -..-> 193
		192 -..-> 193
		190 -..-> 194["Warped Warheads"]
		191 -..-> 194
		192 -..-> 194
		190 -..-> 195["Quantum Satchels"]
		191 -..-> 195
		192 -..-> 195
		190 -..-> 196["Propulsion Gateways"]
		172 -..-> 196
		158 -..-> 196
		192 -..-> 197["Proton Afterburners"]
		155 -..-> 197
		156 -..-> 197
		191 -..-> 198["Simulation Matrix"]
		154 -..-> 198
		170 -..-> 198
		280 -..-> 285["Weapon: Thetis Ray"]
		282 -..-> 285
		281 -..-> 285
		270 -..-> 285
		280 -..-> 290["Nanofoil Mirrors"]
		282 -..-> 290
		281 -..-> 290
		280 -..-> 291["Hawking Condenser"]
		282 -..-> 291
		281 -..-> 291
		280 -..-> 295["Vacuum Coolant"]
		267 -..-> 295
		271 -..-> 295
		282 -..-> 296["Elerium Capacitor"]
		242 -..-> 296
		271 -..-> 296
		281 -..-> 297["Proton Generator"]
		266 -..-> 297
		271 -..-> 297
		361 -..-> 370["Hi-Sec Tracking"]
		362 -..-> 370
		360 -..-> 370
		361 -..-> 371["Sentient Turrets"]
		362 -..-> 371
		360 -..-> 371
		361 -..-> 372["Non-Newtonian Ballistics"]
		362 -..-> 372
		360 -..-> 372
		361 -..-> 373["Subatomic Lubricant"]
		362 -..-> 373
		360 -..-> 373
		460 -..-> 470["Anamorphic Winding"]
		461 -..-> 470
		462 -..-> 470
		460 -..-> 471["Coriolis Drive"]
		461 -..-> 471
		462 -..-> 471
		460 -..-> 472["Living Metal"]
		461 -..-> 472
		462 -..-> 472
		567 -..-> 580["Reverse Kinematics"]
		565 -..-> 580
		566 -..-> 580
		567 -..-> 581["Null Payload Adapter"]
		565 -..-> 581
		566 -..-> 581
		567 -..-> 582["Strings Detachment"]
		565 -..-> 582
		566 -..-> 582
		650 -..-> 680["Weapon: Hypercharger"]
		642 -..-> 680
		640 -..-> 680
		641 -..-> 680
		670 -..-> 685["Relic Carbonification"]
		671 -..-> 685
		672 -..-> 685
		670 -..-> 686["Sublight Zoning"]
		671 -..-> 686
		672 -..-> 686
		670 -..-> 687["Crystalline Projection"]
		671 -..-> 687
		672 -..-> 687
		764 -..-> 790["Armor: SMA Armor"]
		766 -..-> 790
		781 -..-> 790
		780 -..-> 790
		789 -..-> 791["Fibrous Annihilation"]
		788 -..-> 791
		789 -..-> 792["Alloy Recombination"]
		788 -..-> 792
		865 -..-> 890["Shield: WIND"]
		863 -..-> 890
		881 -..-> 890
		880 -..-> 890
		889 -..-> 891["Velocity Suppression"]
		888 -..-> 891
		889 -..-> 892["Matter Recognition"]
		888 -..-> 892
		340 -..-> 1002["AA Precision III"]
		241 -..-> 1002
		155 -..-> 1002
		1001 -..-> 1002
		342 -..-> 1012["AA Damage IШI"]
		153 -..-> 1012
		243 -..-> 1012
		341 -..-> 1022["AA Range III"]
		158 -..-> 1022
		266 -..-> 1022
		1022 -..-> 1040["Graviton Capacitors"]
		1012 -..-> 1040
		1002 -..-> 1040
		1030(["AA Precision ∞"]) -..-> 1041["Subspace Connectivity"]
		1031(["AA Damage ∞"]) -..-> 1041
		1032(["AA Range ∞"]) -..-> 1041
		565 -..-> 1102["Warp Weapons Attack Rate III"]
		441 -..-> 1102
		665 -..-> 1102
		1101 -..-> 1102
		567 -..-> 1112["Warp Weapons Damage III"]
		442 -..-> 1112
		667 -..-> 1112
		1111 -..-> 1112
		540 -..-> 1122["Warp Weapons Range III"]
		440 -..-> 1122
		666 -..-> 1122
		1121 -..-> 1122
		1115 -..-> 1125["Weapon: Mass Disperser"]
		1101 -..-> 1125
		1111 -..-> 1125
		1121 -..-> 1125
		1125 -..-> 1135["Weapon: Warp Array"]
		1102 -..-> 1135
		1112 -..-> 1135
		1122 -..-> 1135
		1363 -..-> 1390["Aerostatic Materials"]
		1381 -..-> 1390
		1381 -..-> 1390
		1382 -..-> 1390
		1381 -..-> 1391["Suborbital Navigation"]
		1381 -..-> 1391
		1382 -..-> 1391
		1381 -..-> 1392["Amplified Ventilation"]
		1381 -..-> 1392
		1382 -..-> 1392
		1880 -..-> 1890["Amplified Hydrolysis"]
		1880 -..-> 1890
		1880 -..-> 1891["Psionic Convergence"]
		1880 -..-> 1891
		2071 -..-> 2080["Weapon: Assault Drone"]
		2065 -..-> 2080
		2071 -..-> 2081["Weapon: Pegasus Drone"]
		2065 -..-> 2081
		2072 -..-> 2082["Durable Materials"]
		2071 -..-> 2082
	end
		788 -..-> 795(["Dodge ∞"])
		789 -..-> 795
	subgraph "Cost 38400"
	direction TB
		1002 -..-> 1030
		1021 -..-> 1030
		1011 -..-> 1030
		1001 -..-> 1031
		1021 -..-> 1031
		1012 -..-> 1031
		1001 -..-> 1032
		1022 -..-> 1032
		1011 -..-> 1032
		1102 -..-> 1130(["Warp Weapons Attack Rate ∞"])
		1121 -..-> 1130
		1111 -..-> 1130
		1101 -..-> 1131(["Warp Weapons Damage ∞"])
		1121 -..-> 1131
		1112 -..-> 1131
		1101 -..-> 1132(["Warp Weapons Range ∞"])
		1122 -..-> 1132
		1111 -..-> 1132
		1135 -..-> 1140["Weapon: Gravity Hammer"]
		1130 -..-> 1140
		1131 -..-> 1140
		1132 -..-> 1140
	end
	subgraph "Cost 43800"
	direction TB
		1122 -..-> 1150["Strong Force Bending"]
		1112 -..-> 1150
		1102 -..-> 1150
		1130 -..-> 1151["Portable Black Holes"]
		1131 -..-> 1151
		1132 -..-> 1151
		1130 -..-> 1152["Meson Cloning"]
		1131 -..-> 1152
		1132 -..-> 1152
	end
```

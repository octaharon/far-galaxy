## Chassis requires a factory 

Chassis Type | Factories
:---: | :--- 
**artillery** | Light vehicle factory<br>Heavy vehicle factory<br>Warfare Multiplex
**quadrapod** | Nexus Foundry<br>Self-Maintained Factory<br>Massive Industrial Complex<br>Titan Forge
**battleship** | Shipyard<br>Naval engineering corpus<br>Navyforge<br>Massive Industrial Complex
**biotech** | Bionic Institute<br>BioForge<br>Cloning Vats
**bomber** | Airforge
**manofwar** | Naval engineering corpus<br>Massive Industrial Complex<br>Titan Forge
**carrier** | Airforge<br>Massive Industrial Complex<br>Titan Forge
**combatdrone** | Robotics bay<br>Cybernetic forge<br>Nexus Foundry<br>Self-Maintained Factory<br>Junkyard
**combatmech** | Heavy vehicle factory<br>Warfare Multiplex<br>Nexus Foundry<br>Massive Industrial Complex<br>Titan Forge
**corvette** | Water dock<br>Shipyard<br>Naval engineering corpus<br>Submarine pool<br>Navyforge
**cruiser** | Water dock<br>Shipyard<br>Naval engineering corpus<br>Navyforge
**destroyer** | Shipyard<br>Naval engineering corpus<br>Navyforge
**droid** | Military University<br>Automated Assembly Yard<br>Robotics bay<br>Cybernetic forge<br>Self-Maintained Factory<br>Junkyard
**exosuit** | Training Hall<br>Bionic Institute<br>Military University<br>Barracks<br>Cloning Vats<br>Special Forces Academy<br>Junkyard
**fighter** | Airforge
**fortress** | Nexus Foundry<br>Massive Industrial Complex<br>Titan Forge
**hav** | Heavy vehicle factory<br>Warfare Multiplex<br>Massive Industrial Complex
**tiltjet** | Aeronautics Complex<br>Hoverforge<br>Airforge
**helipad** | Hoverforge<br>Massive Industrial Complex<br>Titan Forge
**hovertank** | Warfare Multiplex<br>Self-Maintained Factory<br>Hoverforge
**hydra** | BioForge<br>Cloning Vats<br>Naval engineering corpus
**infantry** | Training Hall<br>Bionic Institute<br>Military University<br>Barracks<br>Cloning Vats<br>Special Forces Academy
**launcherpad** | Warfare Multiplex<br>Hoverforge<br>Massive Industrial Complex
**lav** | Motorworks<br>Light vehicle factory<br>Heavy vehicle factory<br>Automated Assembly Yard<br>Machinery<br>Warfare Multiplex
**mothership** | Massive Industrial Complex<br>Titan Forge
**pointdefense** | Cybernetic forge<br>Nexus Foundry<br>Self-Maintained Factory<br>Massive Industrial Complex
**rover** | Warfare Multiplex<br>Aeronautics Complex<br>Hoverforge
**science_vessel** | Robotics bay<br>Cybernetic forge<br>Nexus Foundry<br>Junkyard
**scout** | Motorworks<br>Light vehicle factory<br>Heavy vehicle factory<br>Automated Assembly Yard<br>Machinery<br>Warfare Multiplex<br>Junkyard
**strike_aircraft** | Airforge
**submarine** | Submarine pool
**sup_aircraft** | Aeronautics Complex<br>Hoverforge<br>Airforge
**sup_ship** | Water dock<br>Shipyard<br>Naval engineering corpus<br>Submarine pool<br>Navyforge
**sup_vehicle** | Motorworks<br>Light vehicle factory<br>Heavy vehicle factory<br>Automated Assembly Yard<br>Machinery<br>Warfare Multiplex
**swarm** | BioForge<br>Cloning Vats
**tank** | Light vehicle factory<br>Heavy vehicle factory<br>Machinery<br>Warfare Multiplex


## Factory can build chassis 

Factory | Chassis
:---: | :--- 
**Training Hall** | infantry<br>exosuit
**Bionic Institute** | infantry<br>exosuit<br>biotech
**Military University** | infantry<br>exosuit<br>droid
**Barracks** | infantry<br>exosuit
**BioForge** | biotech<br>swarm<br>hydra
**Cloning Vats** | biotech<br>swarm<br>hydra<br>infantry<br>exosuit
**Special Forces Academy** | infantry<br>exosuit
**Motorworks** | scout<br>lav<br>sup_vehicle
**Light vehicle factory** | scout<br>lav<br>sup_vehicle<br>tank<br>artillery
**Heavy vehicle factory** | scout<br>lav<br>sup_vehicle<br>tank<br>artillery<br>hav<br>combatmech
**Automated Assembly Yard** | lav<br>sup_vehicle<br>droid<br>scout
**Machinery** | lav<br>scout<br>sup_vehicle<br>tank
**Warfare Multiplex** | scout<br>rover<br>lav<br>sup_vehicle<br>tank<br>launcherpad<br>artillery<br>hav<br>combatmech<br>hovertank
**Robotics bay** | combatdrone<br>droid<br>science_vessel
**Cybernetic forge** | combatdrone<br>pointdefense<br>droid<br>science_vessel
**Nexus Foundry** | combatdrone<br>quadrapod<br>fortress<br>pointdefense<br>science_vessel<br>combatmech
**Self-Maintained Factory** | combatdrone<br>hovertank<br>pointdefense<br>droid<br>quadrapod
**Junkyard** | combatdrone<br>exosuit<br>droid<br>scout<br>science_vessel
**Water dock** | corvette<br>sup_ship<br>cruiser
**Shipyard** | corvette<br>sup_ship<br>destroyer<br>battleship<br>cruiser
**Naval engineering corpus** | corvette<br>sup_ship<br>manofwar<br>destroyer<br>battleship<br>cruiser<br>hydra
**Submarine pool** | corvette<br>sup_ship<br>submarine
**Navyforge** | corvette<br>sup_ship<br>destroyer<br>battleship<br>cruiser
**Aeronautics Complex** | rover<br>sup_aircraft<br>tiltjet
**Hoverforge** | rover<br>hovertank<br>launcherpad<br>tiltjet<br>sup_aircraft<br>helipad
**Airforge** | sup_aircraft<br>fighter<br>strike_aircraft<br>bomber<br>tiltjet<br>carrier
**Massive Industrial Complex** | fortress<br>manofwar<br>carrier<br>helipad<br>mothership<br>launcherpad<br>pointdefense<br>quadrapod<br>battleship<br>hav<br>combatmech
**Titan Forge** | fortress<br>manofwar<br>carrier<br>helipad<br>quadrapod<br>combatmech<br>mothership

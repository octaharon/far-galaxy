import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import AbilityManager from '../definitions/tactical/abilities/AbilityManager';
import Abilities from '../definitions/tactical/abilities/types';
import { MAX_MAP_RADIUS } from '../definitions/tactical/map/constants';
import Environment from '../src/env';
import { createAbilityArray, createAbilityMapping } from '../src/notion/collectionHelpers';
import { AbilityDatabaseId } from '../src/notion/constants';
import { deleteRemotePage } from '../src/notion/dataHelpers';
import { sanitizeCollectibleDescription, sanitizeCollectibleName } from '../src/notion/valueHelpers';

const notion = new Client({ auth: Environment.NOTION_API_KEY });
const targetTitles = {
	[Abilities.abilityTargetTypes.map]: 'Location',
	[Abilities.abilityTargetTypes.player_self]: 'Player (Self)',
	[Abilities.abilityTargetTypes.player]: 'Player (Any)',
	[Abilities.abilityTargetTypes.unit]: 'Unit (Any)',
	[Abilities.abilityTargetTypes.self]: 'Unit (Self)',
	[Abilities.abilityTargetTypes.around]: 'Unit (Around)',
	[Abilities.abilityTargetTypes.none]: '',
};

function sanitizeAbilityRange(range: number): number {
	return Number.isFinite(range) && range > 0 && range < MAX_MAP_RADIUS
		? range
		: range === MAX_MAP_RADIUS
		? Infinity
		: null;
}

function sanitizeAbilityColdown(cd: number) {
	return Number.isFinite(cd) && cd !== 0 ? cd : null;
}

function mapAbilityToPageProperties(ability: Abilities.IAbility<any>): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(ability.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Cooldown: { type: 'number', number: sanitizeAbilityColdown(ability.cooldown) },
		Description: {
			type: 'rich_text',
			rich_text: [
				{ type: 'text', text: { content: sanitizeCollectibleDescription(ability.static.description) } },
			],
		},
		ID: { type: 'number', number: ability.static.id },
		Range: { type: 'number', number: sanitizeAbilityRange(ability.baseRange) },
		Target: {
			type: 'select',
			select: { name: targetTitles[ability.targetType] },
		},
	};
}

async function updateRemotePageWithAbility(pageId: string, ability: Abilities.IAbility<any>) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapAbilityToPageProperties(ability),
	});
}

async function createRemotePageWithAbility(databaseId: string, ability: Abilities.IAbility<any>) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapAbilityToPageProperties(ability),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.ability]: AbilityManager,
});

async function syncToRemote(dBID: string) {
	const localBD = createAbilityArray();
	const abilityPageMap = await createAbilityMapping(notion);
	const pages = Object.keys(abilityPageMap);
	//update existing abilities
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const abId = abilityPageMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.ability).getIds().includes(abId)) {
			await updateRemotePageWithAbility(
				pageId,
				DIContainer.getProvider(DIInjectableCollectibles.ability).instantiate(null, abId).init(),
			);
			console.log('updating page for Ability: ' + abId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	// Add new abilities to Notion
	const newAbilities = localBD.filter((e) => !Object.values(abilityPageMap).includes(e.static.id));

	for (let i = 0; i < newAbilities.length; i++) {
		console.log(`Creating a page for ability ${newAbilities[i].static.id}`);
		await createRemotePageWithAbility(dBID, newAbilities[i]);
	}
	console.log(`Synced ${localBD.length} abilities to Notion!`);
}

syncToRemote(AbilityDatabaseId);

// {
//   Cooldown: { id: '%40_%7DC', type: 'number', number: 3 },
//   Description: { id: 'G%5Csv', type: 'rich_text', rich_text: [ [Object] ] }
//   Target: {
//     id: 'TLzH',
//     type: 'select',
//     select: { id: 'IWHS', name: 'Location', color: 'pink' }
//   },
//   ID: { id: 'hZzE', type: 'number', number: 45007 },
//   Range: { id: 'mXXK', type: 'number', number: 90 },
//   Name: { id: 'title', type: 'title', title: [ [Object] ] }
// }
// {
//   object: 'page',
//   id: '012688d5-1faf-4600-8203-23801639661a',
//   created_time: '2021-01-19T22:31:00.000Z',
//   last_edited_time: '2021-01-19T22:33:00.000Z',
//   cover: null,
//   icon: null,
//   parent: {
//     type: 'database_id',
//     database_id: '4c510a01-81ad-40a6-be31-3c47e0b8c2dd'
//   },
//   archived: false,
//   properties: {
//     Cooldown: { id: '%40_%7DC', type: 'number', number: 10 },
//     Description: { id: 'G%5Csv', type: 'rich_text', rich_text: [Array] },
//     Target: { id: 'TLzH', type: 'select', select: [Object] },
//     ID: { id: 'hZzE', type: 'number', number: 60004 },
//     Range: { id: 'mXXK', type: 'number', number: 90 },
//     Name: { id: 'title', type: 'title', title: [Array] }
//   },
//   url: 'https://www.notion.so/Weather-Storm-012688d51faf4600820323801639661a'
// }

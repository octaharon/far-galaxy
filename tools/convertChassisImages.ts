import child_process from 'child_process';
import fs from 'fs';
import path from 'path';
import { optimize, OptimizedSvg, OptimizeOptions } from 'svgo';
import {
	CSS_FACTION_CLASS_PRIMARY,
	CSS_FACTION_CLASS_SECONDARY,
} from '../src/components/Symbols/ChassisImageProvider/types';

const svgr = require('@svgr/core').default;

const transformSVG = (st: string, options: Partial<OptimizeOptions> = {}, prefix: string = null) =>
	new Promise((resolve, reject) => {
		try {
			const t = optimize(st, {
				full: true,
				plugins: [
					'cleanupEnableBackground',
					'removeComments',
					'removeMetadata',
					'removeTitle',
					'removeDesc',
					'removeEditorsNSData',
					'removeScriptElement',
					'removeRasterImages',
					'removeDoctype',
					'removeXMLProcInst',
					'removeHiddenElems',
					'removeEmptyText',
					'removeDimensions',
					{
						name: 'removeElementsByAttr',
						params: {
							id: ['Perspectiva', 'Circle', 'Cylinders'],
						},
					},
					{
						name: 'cleanupIDs',
						params: {
							prefix,
							minify: true,
							preservePrefixes: ['Animation'],
						},
					},
					'convertEllipseToCircle',
					'convertPathData',
					//'reusePaths',
					'collapseGroups',
					'minifyStyles',
					'convertColors',
					'convertStyleToAttrs',
					'removeUselessDefs',
					'removeUselessStrokeAndFill',
					'cleanupAttrs',
					'removeEmptyAttrs',
					'removeEmptyContainers',
					'sortAttrs',
				],
				...options,
			});
			if (!t.error) resolve((t as OptimizedSvg).data);
			else reject(t.error);
		} catch (e) {
			reject(e);
		}
	});

// @ts-nocheck
const tsxTpl = (
	{ template }: Record<string, any>,
	opts: any,
	{ imports, componentName, props, jsx, exports }: Record<string, any>,
) => {
	const typeScriptTpl = template.smart({ plugins: ['jsx', 'typescript'] });
	return typeScriptTpl.ast`
    import * as React from 'react';

    const Component = (props: React.SVGProps<SVGSVGElement>) => ${jsx};
    export default Component;
  `;
};

const factionColorClasses = [CSS_FACTION_CLASS_PRIMARY, CSS_FACTION_CLASS_SECONDARY];

const generateId = (n: number): string => {
	const smb = 'a'.charCodeAt(0);
	let id = '';
	let i = n;
	do {
		const d = i % 26;
		id = String.fromCharCode(smb + d) + id;
		i = (i - d) / 26 - 1;
	} while (i >= 0);
	return id;
};

const processSVG = (text: string, id: string, prefix = 'ch'): string => {
	const idRegex = /id\s?=\s?["']([^"']+)["']/gi;
	const classRegex = /class\s?=\s?["']([^"']+)["']/gi;
	const matchIds = text.matchAll(idRegex);
	let CONTENT = text;
	// Shortening IDs
	/*let nextId = 0;
	for (const group of matchIds) {
		const oldId = group[1];
		const newId = `${prefix}-${id}-${generateId(nextId++)}`;
		CONTENT = CONTENT.replace(group[0], `id="${newId}"`).replace('#' + oldId + ')', '#' + newId + ')');
	}*/
	const matchClasses = CONTENT.matchAll(classRegex);
	const styleReplacement: Record<string, string> = {};
	// In-lining Classes to styles
	for (const group of matchClasses) {
		const className = group[1];
		if (!styleReplacement[className]) {
			const styleRegex = new RegExp('\\.' + className + '\\s?\\{([^}]+)\\}', 'ig');
			const styleDef = CONTENT.matchAll(styleRegex);
			for (const sd of styleDef) {
				styleReplacement[className] = sd[1].replace(/enable-background\s?:\s?new\s+/im, '');
			}
		}
		// Detecting color blocks and assigning a Faction class to them
		let style = styleReplacement[className];
		let replaceValue = '';
		if (/(stop-color|fill)\s?:\s?#a800a8/is.test(style)) {
			replaceValue = `class="${factionColorClasses[0]}" `;
		} else if (/(stop-color|fill)\s?:\s?#6e1370/is.test(style)) {
			replaceValue = `class="${factionColorClasses[1]}" `;
		}
		if (replaceValue.length) style = style.replace(/(stop-color|fill)\s?:\s?[^;}]+;?/is, '');
		replaceValue += ` style="${style}"`;
		CONTENT = CONTENT.replace(group[0], replaceValue);
	}
	// Removing <Style> block
	CONTENT = CONTENT.replace(/<style(.*)<\/style>/is, '');
	return CONTENT;
};

console.clear();
process.stdout.write('\u001b[2J\u001b[0;0H');

console.info('Processing chassis images...');

const paths = ['full', 'topdown'];
const sourcePath = path.join('./', 'assets', 'img', 'svg', 'chassis');
const outPath = path.join('./', 'src', 'components', 'Symbols', 'ChassisImageProvider', 'chassis');

Promise.all(
	paths.reduce((promises: Array<Promise<any>>, currentPath: string) => {
		const directoryPath = path.join(sourcePath, currentPath);
		const localPath = path.join(outPath, currentPath);

		return promises.concat(
			fs.readdirSync(directoryPath).map((file) => {
				const targetPath = path.join(directoryPath, '..', 'processed', `${currentPath}-${file}`);
				const pth = path.join(directoryPath, file);
				const contents = fs.readFileSync(pth, 'utf8').toString();

				console.info(`Reading ${pth}...`);
				const id = path.basename(file, '.svg');
				const prefix = 'ch-' + currentPath;

				// Cleanup the source and prefix the IDs
				const newContents = processSVG(contents, id, prefix);

				// Optimize and save
				return (
					fs.existsSync(targetPath)
						? new Promise((resolve, reject) => {
								console.info(`Reading from cache: ${targetPath}`);
								fs.readFile(targetPath, (err, data) => {
									if (err) reject(err);
									else resolve(data.toString('utf8'));
								});
						  })
						: transformSVG(
								newContents,
								{
									path: file,
								},
								prefix + id,
						  ).then((data: string) => {
								fs.writeFileSync(targetPath, data);
								console.info(`Optimized and saved ${currentPath}/${file}`);
								return data;
						  })
				)
					.then((optimizedContent: string) => {
						// Turn into a Symbol JSX
						const viewBoxRegex = /<svg.*\sviewbox\s?=\s?["']([\d\s]+)["'][^>]*>/gis;
						const viewboxMatch = viewBoxRegex.exec(optimizedContent);
						const viewbox = viewboxMatch ? viewboxMatch[1] : '';
						let cnt = optimizedContent;
						if (viewbox) {
							cnt = cnt
								.replace(viewboxMatch[0], `<svg viewBox='${viewbox}'>`)
								.replace(/.*<svg/gims, '<svg');
						} else throw new Error(`Can't detect viewbox for ${targetPath}: ${cnt}`);
						return svgr(cnt, {
							template: tsxTpl,
						});
					})
					.then((jsx: any) => {
						// Save as TSX
						const name = path.basename(file, '.svg') + '.tsx';
						fs.writeFileSync(path.join(localPath, name), jsx);
						console.info(`Saved ${currentPath} version as ${name}`);
					})
					.catch(console.error);
			}),
		);
	}, []),
).then((results: any) => {
	console.info(`Processed ${results.length} files, applying ESLint...`);
	const cmd = `npx eslint --quiet --fix ${paths.map((p) => path.resolve(outPath, p, '*')).join(' ')}`;
	console.warn(cmd);
	process.stdout.write(
		child_process.execSync(cmd, {
			cwd: path.resolve('./'),
		}),
	);
	console.info('Jobs done');
});

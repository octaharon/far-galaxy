import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import _ from 'underscore';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import FacilityManager from '../definitions/orbital/facilities/FacilityManager';
import { IFacility } from '../definitions/orbital/facilities/types';
import Resources from '../definitions/world/resources/types';
import Environment from '../src/env';
import { createAbilityMapping, createFacilityArray, creatFacilityMapping } from '../src/notion/collectionHelpers';
import { FacilitiesDatabaseId } from '../src/notion/constants';
import { deleteRemotePage, updateDatabaseSelectOptions } from '../src/notion/dataHelpers';
import {
	sanitizeBuildingModifier,
	sanitizeCollectibleDescription,
	sanitizeCollectibleName,
	sanitizeResourcePayload,
	TNotionLabel,
} from '../src/notion/valueHelpers';
import { splitIntoChunks } from '../src/utils/splitIntoChunks';

const notion = new Client({ auth: Environment.NOTION_API_KEY });

const BaseModifierLabelCache: Record<string, TNotionLabel> = {};
const ResourceConsumptionLabelCache: Record<string, TNotionLabel> = {};
const ResourceOutputLabelCache: Record<string, TNotionLabel> = {};
const LabelCache: Record<number, { base: TNotionLabel[]; consumption: TNotionLabel[]; output: TNotionLabel[] }> = {};

function buildLabelCache(facilities: IFacility[]) {
	facilities.forEach((facility) => {
		const base = sanitizeBuildingModifier(facility);
		const consumption = sanitizeResourcePayload(facility.static.resourceConsumption);
		const production = sanitizeResourcePayload(facility.static.resourceProduction);
		LabelCache[facility.static.id] = {
			base: base.map((v) => ({ name: v.name })),
			consumption: consumption.map((v) => ({ name: v.name })),
			output: production.map((v) => ({ name: v.name })),
		};
		consumption.forEach((p) => {
			ResourceConsumptionLabelCache[p.name] = p;
		});
		production.forEach((p) => {
			ResourceOutputLabelCache[p.name] = p;
		});
		base.forEach((p) => {
			BaseModifierLabelCache[p.name] = p;
		});
	});
}

function mapFacilityToPageProperties(
	facility: IFacility,
	abilityLookupMap?: Record<number, string>,
): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(facility.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Description: {
			type: 'rich_text',
			rich_text: [
				{ type: 'text', text: { content: sanitizeCollectibleDescription(facility.static.description) } },
			],
		},
		'Base Bonuses': {
			type: 'multi_select',
			multi_select: LabelCache[facility.static.id].base,
		},
		Abilities: {
			relation: (facility.static.abilities || [])
				.filter((abilityId) => !!abilityLookupMap[abilityId])
				.map((abilityId) => ({
					id: abilityLookupMap[abilityId],
				})),
		},
		ID: { type: 'number', number: facility.static.id },
		Tier: { type: 'number', number: facility.static.buildingTier },
		'Resource Output': {
			type: 'multi_select',
			multi_select: LabelCache[facility.static.id].output,
		},
		'Resource Consumption': {
			type: 'multi_select',
			multi_select: LabelCache[facility.static.id].consumption,
		},
		'Energy Output': { type: 'number', number: facility.energyOutput || null },
		'Repair Output': { type: 'number', number: facility.repairOutput || null },
		'Raw Materials Output': { type: 'number', number: facility.materialOutput || null },
		Upkeep: { type: 'number', number: facility.energyMaintenanceCost || null },
		'Cost: Hydrogen': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.hydrogen] ?? null,
		},
		'Cost: Helium': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.helium] ?? null,
		},
		'Cost: Carbon': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.carbon] ?? null,
		},
		'Cost: Oxygen': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.oxygen] ?? null,
		},
		'Cost: Nitrogen': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.nitrogen] ?? null,
		},
		'Cost: Halogens': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.halogens] ?? null,
		},
		'Cost: Ores': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.ores] ?? null,
		},
		'Cost: Isotopes': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.isotopes] ?? null,
		},
		'Cost: Minerals': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.minerals] ?? null,
		},
		'Cost: Proteins': {
			type: 'number',
			number: facility.static.buildingCost[Resources.resourceId.proteins] ?? null,
		},
	};
}

async function updateRemotePageWithFacility(
	pageId: string,
	facility: IFacility,
	abilityLookupMap?: Record<number, string>,
) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapFacilityToPageProperties(facility, abilityLookupMap),
	});
}

async function createRemotePageWithFacility(
	databaseId: string,
	facility: IFacility,
	abilityLookupMap?: Record<number, string>,
) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapFacilityToPageProperties(facility, abilityLookupMap),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.facility]: FacilityManager,
});

async function syncToRemote(dBID: string) {
	const localBD = createFacilityArray();
	buildLabelCache(localBD);
	await updateDatabaseSelectOptions(notion, dBID, 'Base Bonuses', []);
	await updateDatabaseSelectOptions(notion, dBID, 'Resource Consumption', []);
	await updateDatabaseSelectOptions(notion, dBID, 'Resource Output', []);
	let pLabels = splitIntoChunks(Object.values(BaseModifierLabelCache), 99);
	for (const pLabelSet of pLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'Base Bonuses', pLabelSet);
	}
	pLabels = splitIntoChunks(Object.values(ResourceOutputLabelCache), 99);
	for (const pLabelSet of pLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'Resource Output', pLabelSet);
	}
	pLabels = splitIntoChunks(Object.values(ResourceConsumptionLabelCache), 99);
	for (const pLabelSet of pLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'Resource Consumption', pLabelSet);
	}
	const facilities = await creatFacilityMapping(notion);
	const notionAbilityLookupMap = _.invert(await createAbilityMapping(notion)) as Record<number, string>;
	const pages = Object.keys(facilities);

	//update existing facilities
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const facilityId = facilities[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.facility).getIds().includes(facilityId)) {
			console.log('Updating page for Facility: ' + facilityId);
			await updateRemotePageWithFacility(
				pageId,
				localBD.find((f) => f.static.id === facilityId),
				notionAbilityLookupMap,
			);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	// Add new establishments to Notion
	const newFacilities = localBD.filter((e) => !Object.values(facilities).includes(e.static.id));

	for (let i = 0; i < newFacilities.length; i++) {
		console.log(`Creating a page for Facility ${newFacilities[i].static.id}`);
		await createRemotePageWithFacility(dBID, newFacilities[i], notionAbilityLookupMap);
	}
	console.log(`Synced ${localBD.length} Facility to Notion!`);
}

syncToRemote(FacilitiesDatabaseId);

//getRemotePagesByID(notion, '0fbe5775475a4f76a294ed77995490f2').then((results) => console.log(results));

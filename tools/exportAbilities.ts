import AbilityManager from '../definitions/tactical/abilities/AbilityManager';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';

DIContainer.inject({
	[DIInjectableCollectibles.ability]: AbilityManager,
});

const abilities = DIContainer.getProvider(DIInjectableCollectibles.ability).getAll();

console.info(['Name', 'Description', 'ID', 'Cooldown', 'Target', 'Range'].join(';'));
Object.keys(abilities)
	.map((abilityId) => {
		const ability = DIContainer.getProvider(DIInjectableCollectibles.ability).instantiate(null, abilityId).init();
		return [
			ability.static.caption,
			ability.static.description,
			abilityId,
			ability.cooldown,
			ability.targetType,
			ability.baseRange,
		]
			.map((v) => `"${v}"`)
			.join(';');
	})
	.forEach((v) => console.info(v));

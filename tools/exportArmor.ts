import BasicMaths from '../definitions/maths/BasicMaths';
import ArmorManager from '../definitions/technologies/armor/ArmorManager';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';

DIContainer.inject({
	[DIInjectableCollectibles.armor]: ArmorManager,
});

const armors = DIContainer.getProvider(DIInjectableCollectibles.armor).getAll();

console.info(['Name', 'Description', 'ID', 'Base Cost', 'Slot', 'Protection', 'Modifiers'].join(';'));
Object.keys(armors)
	.map((armorId) => {
		const armor = DIContainer.getProvider(DIInjectableCollectibles.armor).instantiate(null, armorId);
		const modifiers = [DefenseManager.describeDodgeModifier(armor.static?.dodgeModifier)];
		if (armor.static?.healthModifier)
			modifiers.push(`${BasicMaths.describeModifier(armor.static.healthModifier)} Hit Points`);
		if (armor.static?.flags) modifiers.push(...Object.keys(armor.static.flags));
		return [
			armor.static.caption,
			armor.static.description
				.replace(/[\r\n\s]+/g, ' ')
				.replace(/\s+/, ' ')
				.trim(),
			armorId,
			armor.baseCost,
			armor.static.type,
			DefenseManager.describeProtection(armor.armor).replace(/\s*\|\s*/g, ', '),
			modifiers.filter(Boolean).join(', '),
		]
			.map((v) => `"${v}"`)
			.join(';');
	})
	.forEach((v) => console.info(v));

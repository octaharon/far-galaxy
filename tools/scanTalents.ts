import _ from 'underscore';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';
import { DIInjectableCollectibles, DIInjectableSerializables } from '../definitions/core/DI/injections';
import TalentRegistry from '../definitions/player/Talents/TalentRegistry';
import Factions from '../definitions/world/factions';

DIContainer.inject(DIPresetDefault);
const player = DIContainer.getProvider(DIInjectableSerializables.player).getTestPlayer(Factions.TFactionID.none, false);
TalentRegistry.getAll().forEach((talent) => {
	if (talent.technologyReward) player.addTechnologies(talent.technologyReward);
});

console.warn(`All talents: ${TalentRegistry.getAll().length}`);
console.log(
	_.sortBy(TalentRegistry.getAll(), (t) => t.caption).reduce(
		(obj, v) => Object.assign(obj, { [v.caption]: v.levelReq ?? 0 }),
		{},
	),
);

console.warn(
	`Armor: ${Object.keys(player.arsenal.armor).length}/${
		Object.keys(DIContainer.getProvider(DIInjectableCollectibles.armor).getAll()).length
	}`,
);
console.log(
	Object.values(DIContainer.getProvider(DIInjectableCollectibles.armor).getAll()).reduce(
		(acc, item) =>
			Object.assign(acc, {
				[`${item.caption}.${item.id} (${item.type})`]: player.arsenal.armor[item.id] || '---',
			}),
		{},
	),
);
console.warn(
	`Shields: ${Object.keys(player.arsenal.shield).length}/${
		Object.keys(DIContainer.getProvider(DIInjectableCollectibles.shield).getAll()).length
	}`,
);
console.log(
	Object.values(DIContainer.getProvider(DIInjectableCollectibles.shield).getAll()).reduce(
		(acc, item) =>
			Object.assign(acc, {
				[`${item.caption}.${item.id} (${item.type})`]: player.arsenal.shield[item.id] || '---',
			}),
		{},
	),
);
console.warn(
	`Weapons: ${Object.keys(player.arsenal.weapons).length}/${
		Object.keys(DIContainer.getProvider(DIInjectableCollectibles.weapon).getAll()).length
	}`,
);
console.log(
	Object.keys(DIContainer.getProvider(DIInjectableCollectibles.weapon).getAll()).reduce(
		(acc, id) =>
			Object.assign(acc, {
				[`${DIContainer.getProvider(DIInjectableCollectibles.weapon).get(id).caption} (${
					DIContainer.getProvider(DIInjectableCollectibles.weapon).get(id).groupType
				})`]: player.arsenal.weapons[id] || '---',
			}),
		{},
	),
);
console.warn(
	`Chassis: ${Object.keys(player.arsenal.chassis).length}/${
		Object.keys(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll()).length
	}`,
);
console.log(
	Object.values(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll()).reduce(
		(acc, obj) =>
			Object.assign(acc, {
				[`${obj.caption}.${obj.id} (${obj.type}) ${
					obj.exclusiveFactions?.length ? ` -${obj.exclusiveFactions.join('')}` : ''
				}`]: player.arsenal.chassis[obj.id] || '---',
			}),
		{},
	),
);
console.warn(
	`Equipment: ${Object.keys(player.arsenal.equipment).length}/${
		Object.keys(DIContainer.getProvider(DIInjectableCollectibles.equipment).getAll()).length
	}`,
);
console.log(
	Object.values(DIContainer.getProvider(DIInjectableCollectibles.equipment).getAll()).reduce(
		(acc, item) =>
			Object.assign(acc, {
				[`${item.caption}.${item.id}`]: player.arsenal.equipment[item.id] || '---',
			}),
		{},
	),
);
console.warn(
	`Factories: ${Object.keys(player.arsenal.factories).length}/${
		Object.keys(DIContainer.getProvider(DIInjectableCollectibles.factory).getAll()).length
	}`,
);
console.log(
	_.sortBy(
		Object.keys(player.arsenal.factories).map((id) =>
			DIContainer.getProvider(DIInjectableCollectibles.factory).get(id),
		),
		'buildingTier',
	).reduce(
		(acc, item) =>
			Object.assign(acc, {
				[`${item.caption}.${item.buildingTier}`]: player.arsenal.factories[item.id],
			}),
		{},
	),
);
console.warn(
	`Facilities: ${Object.keys(player.arsenal.facilities).length}/${
		Object.keys(DIContainer.getProvider(DIInjectableCollectibles.facility).getAll()).length
	}`,
);
console.log(
	_.sortBy(
		Object.keys(player.arsenal.facilities).map((id) =>
			DIContainer.getProvider(DIInjectableCollectibles.facility).get(id),
		),
		'buildingTier',
	).reduce(
		(acc, item) =>
			Object.assign(acc, {
				[`${item.caption}.${item.buildingTier}`]: player.arsenal.facilities[item.id],
			}),
		{},
	),
);
console.warn(
	`Establishments: ${Object.keys(player.arsenal.establishments).length}/${
		Object.keys(DIContainer.getProvider(DIInjectableCollectibles.establishment).getAll()).length
	}`,
);
console.log(
	_.sortBy(
		Object.keys(player.arsenal.establishments).map((id) =>
			DIContainer.getProvider(DIInjectableCollectibles.establishment).get(id),
		),
		'buildingTier',
	).reduce(
		(acc, item) =>
			Object.assign(acc, {
				[`${item.caption}.${item.buildingTier}`]: player.arsenal.establishments[item.id],
			}),
		{},
	),
);

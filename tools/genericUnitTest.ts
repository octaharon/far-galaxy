import { generatePirateFortress } from '../definitions/player/Pirates/generatePrototypes';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';

DIContainer.inject(DIPresetDefault);
const c = generatePirateFortress();
console.info(JSON.stringify(c.serialize(), null, 2));
console.info(JSON.stringify(c.final.serialize(), null, 2));

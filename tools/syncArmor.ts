import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import { isEmptyModifier } from '../definitions/core/helpers';
import BasicMaths from '../definitions/maths/BasicMaths';
import UnitDefense from '../definitions/tactical/damage/defense';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import ArmorManager from '../definitions/technologies/armor/ArmorManager';
import { IArmor } from '../definitions/technologies/types/armor';
import Environment from '../src/env';
import { splitIntoChunks } from '../src/utils/splitIntoChunks';
import { createArmorArray, createArmorMapping } from '../src/notion/collectionHelpers';
import { ArmorDatabaseId } from '../src/notion/constants';
import { deleteRemotePage, updateDatabaseSelectOptions } from '../src/notion/dataHelpers';
import {
	sanitizeCollectibleDescription,
	sanitizeCollectibleName,
	sanitizeProtection,
	TNotionLabel,
} from '../src/notion/valueHelpers';

const notion = new Client({ auth: Environment.NOTION_API_KEY });

function sanitizeArmorSlot(t: UnitDefense.TArmorSlotType) {
	return {
		[UnitDefense.TArmorSlotType.small]: 'Small',
		[UnitDefense.TArmorSlotType.medium]: 'Medium',
		[UnitDefense.TArmorSlotType.large]: 'Large',
		[UnitDefense.TArmorSlotType.none]: 'Unknown',
	}[t];
}

function sanitizeArmorModifiers(armor: IArmor) {
	const r: TNotionLabel[] = [];
	if (armor.static.dodgeModifier && Object.keys(armor.static.dodgeModifier).length) {
		r.push(
			...DefenseManager.describeDodgeModifier(armor.static.dodgeModifier)
				.split(', ')
				.map((v) => v.trim().replace(' All Attacks', '') + ' Dodge')
				.map(
					(v) =>
						({
							name: v,
							color: 'yellow',
						} as TNotionLabel),
				),
		);
	}
	if (!isEmptyModifier(armor.static.healthModifier))
		r.push({ name: `${BasicMaths.describeModifier(armor.static.healthModifier)} Hit Points`, color: 'red' });
	if (armor.static.flags && Object.keys(armor.static.flags).length)
		r.push(
			...DefenseManager.describeDefenseFlags(armor.static.flags)
				.split(',')
				.map((v) => v.trim())
				.map(
					(v) =>
						({
							name: v,
							color: 'purple',
						} as TNotionLabel),
				),
		);
	return r;
}

const ProtectionLabelsCache: Record<string, TNotionLabel> = {};
const PerksLabelsCache: Record<string, TNotionLabel> = {};
const LabelCache: Record<number, { perks: TNotionLabel[]; protection: TNotionLabel[] }> = {};

function buildLabelCache(armors: IArmor[]) {
	armors.forEach((armor) => {
		const perks = sanitizeArmorModifiers(armor);
		const protection = sanitizeProtection(armor.armor);
		LabelCache[armor.static.id] = {
			perks: perks.map((v) => ({ name: v.name })),
			protection: protection.map((v) => ({ name: v.name })),
		};
		protection.forEach((p) => {
			ProtectionLabelsCache[p.name] = p;
		});
		perks.forEach((p) => {
			PerksLabelsCache[p.name] = p;
		});
	});
}

function mapArmorToPageProperties(armor: IArmor): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(armor.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Slot: { type: 'select', select: { name: sanitizeArmorSlot(armor.static.type) } },
		Description: {
			type: 'rich_text',
			rich_text: [{ type: 'text', text: { content: sanitizeCollectibleDescription(armor.static.description) } }],
		},
		ID: { type: 'number', number: armor.static.id },
		Cost: { type: 'number', number: armor.baseCost },
		Protection: {
			multi_select: LabelCache[armor.static.id].protection,
		},
		Modifiers: {
			multi_select: LabelCache[armor.static.id].perks,
		},
	};
}

async function updateRemotePageWithArmor(pageId: string, armor: IArmor) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapArmorToPageProperties(armor),
	});
}

async function createRemotePageWithArmor(databaseId: string, armor: IArmor) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapArmorToPageProperties(armor),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.armor]: ArmorManager,
});

async function syncToRemote(dBID: string) {
	const localArmor = createArmorArray();
	buildLabelCache(localArmor);
	const armorPageMap = await createArmorMapping(notion);
	await updateDatabaseSelectOptions(notion, dBID, 'Protection', []);
	await updateDatabaseSelectOptions(notion, dBID, 'Modifiers', []);
	const pLabels = splitIntoChunks(Object.values(ProtectionLabelsCache), 99);
	for (const pLabelSet of pLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'Protection', pLabelSet);
	}
	await updateDatabaseSelectOptions(notion, dBID, 'Modifiers', Object.values(PerksLabelsCache));

	const pages = Object.keys(armorPageMap);
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const armorId = armorPageMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.armor).getIds().includes(armorId)) {
			await updateRemotePageWithArmor(
				pageId,
				DIContainer.getProvider(DIInjectableCollectibles.armor).instantiate(null, armorId),
			);
			console.log('updating page for Armor: ' + armorId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	const newArmors = localArmor.filter((e) => !Object.values(armorPageMap).includes(e.static.id));
	for (let i = 0; i < newArmors.length; i++) {
		console.log(`Creating a page for Armor ${newArmors[i].static.id}`);
		await createRemotePageWithArmor(dBID, newArmors[i]);
	}
	console.log(`Synced ${localArmor.length} Armors to Notion!`);
}

syncToRemote(ArmorDatabaseId);

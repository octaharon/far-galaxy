import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import UnitAttack from '../definitions/tactical/damage/attack';
import AttackManager from '../definitions/tactical/damage/AttackManager';
import DamageManager from '../definitions/tactical/damage/DamageManager';
import { applyAttackBatchToDefense, applyAttackEffectToDefense } from '../definitions/tactical/damage/damageModel';
import UnitDefense from '../definitions/tactical/damage/defense';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import LargeRaidArmor from '../definitions/technologies/armor/all/large/LargeRaidArmor.30008';
import LargeNeutroniumShield from '../definitions/technologies/shields/all/large/LargeNeutroniumShield.30004';
import LargePlasmaShield from '../definitions/technologies/shields/all/large/LargePlasmaShield.30005';
import Weapons from '../definitions/technologies/types/weapons';
import Hypercharger from '../definitions/technologies/weapons/all/railguns/Hypercharger.25002';
import { wellRounded } from '../src/utils/wellRounded';

DIContainer.inject(DIPresetDefault);

const attackInterface: UnitAttack.IAttackInterface = DIContainer.getProvider(DIInjectableCollectibles.weapon)
	.instantiate(null, Hypercharger.id)
	.applySlotModifier(Weapons.TWeaponSlotType.large).compiled;

const defenseInterfaceSnapshot = {
	caption: 'MegaShield (Warp)',
	[UnitDefense.defenseFlags.warp]: true,
	shields: [
		DIContainer.getProvider(DIInjectableCollectibles.shield).instantiate(null, LargeNeutroniumShield.id).compiled,
		DIContainer.getProvider(DIInjectableCollectibles.shield).instantiate(null, LargePlasmaShield.id).compiled,
	],
	armor: DIContainer.getProvider(DIInjectableCollectibles.armor)
		.instantiate(null, LargeRaidArmor.id)
		.applyModifier({
			default: {
				factor: 0.9,
			},
		}).compiled,
	dodge: Object.assign(UnitDefense.DodgeTemplate(0.45), {
		[UnitAttack.deliveryType.surface]: -0.25,
		[UnitAttack.deliveryType.ballistic]: 0.15,
		[UnitAttack.deliveryType.bombardment]: 0.5,
	}),
} as UnitDefense.IDefenseInterface;

console.info('Attacking with ', attackInterface);

const tries = 20;
const attacks = 10;
let avgDmg = 0;
let avgAccuracy = 0;
let turnsToBreach = 0;
for (let tr = 0; tr < tries; tr++) {
	let totalDmg = 0;
	let hits = 0;
	let turns = 0;
	let turnAttacks = AttackManager.getNumberOfAttacks(attackInterface.baseRate);
	let breach = false;
	let defenseInterface = JSON.parse(JSON.stringify(defenseInterfaceSnapshot));
	console.info(' ');
	console.info(`=== Try #${tr + 1}:`);
	console.info('');
	for (let i = 0; i < attacks; i++) {
		const attackBatch = AttackManager.getAttackBatch(attackInterface, null);
		//console.info('Attack Batch:', attackBatch);
		//console.info('Defense Status:', DefenseManager.describeDefense(defenseInterface));
		const attackResult = applyAttackBatchToDefense(attackBatch, defenseInterface);
		//console.info('Result:', attackResult);
		totalDmg += DamageManager.getTotalDamageValue(attackResult.throughDamage);
		if (turnAttacks <= 0) {
			turnAttacks = AttackManager.getNumberOfAttacks(attackInterface.baseRate);
			turns++;
		}
		console.info(
			`Turn ${turns + 1}, ${turnAttacks} Attacks left, ${
				attackResult.hit
					? `HIT! ${DamageManager.getTotalDamageValue(attackResult.throughDamage)} HP Damage`
					: 'miss'
			}`,
		);
		turnAttacks--;
		if (attackResult.hit) hits++;
		if (!breach && attackResult.breach) {
			breach = true;
			turnsToBreach += turns;
		}
		defenseInterface = applyAttackEffectToDefense(attackResult, defenseInterface);
	}
	console.info('--- Total HP Damage:', totalDmg);
	console.info('--- Total Accuracy:', wellRounded((hits / attacks) * 100, 0) + '%');
	avgDmg += totalDmg;
	avgAccuracy += hits / attacks;
}
console.info('===');
console.info(`Avg Accuracy: ${wellRounded((avgAccuracy / tries) * 100, 0)}%`);
console.info('Avg Turns to breach Shields:', turnsToBreach / tries);
console.info(`Avg Damage to HP: ${wellRounded(avgDmg / tries, 0)}`);

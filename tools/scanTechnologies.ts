import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';
import { DIInjectableCollectibles, DIInjectableSerializables } from '../definitions/core/DI/injections';
import Factions from '../definitions/world/factions';

DIContainer.inject(DIPresetDefault);
const player = DIContainer.getProvider(DIInjectableSerializables.player).getTestPlayer(Factions.TFactionID.none, false);

const armors = DIContainer.getProvider(DIInjectableCollectibles.armor);
const chassis = DIContainer.getProvider(DIInjectableCollectibles.chassis);
const shields = DIContainer.getProvider(DIInjectableCollectibles.shield);
const weapons = DIContainer.getProvider(DIInjectableCollectibles.weapon);
const equipment = DIContainer.getProvider(DIInjectableCollectibles.equipment);
console.warn(`Chassis: ${Object.keys(chassis).length}/${Object.keys(chassis.getAll()).length}`);
console.warn(`Armor: ${Object.keys(player.arsenal.armor).length}/${Object.keys(armors.getAll()).length}`);
console.warn(`Shields: ${Object.keys(player.arsenal.shield).length}/${Object.keys(shields.getAll()).length}`);
console.warn(`Weapons: ${Object.keys(player.arsenal.weapons).length}/${Object.keys(weapons.getAll()).length}`);
console.warn(`Equipment: ${Object.keys(player.arsenal.equipment).length}/${Object.keys(equipment.getAll()).length}`);

let counter = 0;
Object.values(DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll()).forEach((ch) => {
	let variants = 1;
	if (ch.armorSlots) ch.armorSlots.forEach((slot) => (variants *= armors.getArmorsBySlotType(slot).length));
	if (ch.shieldSlots) ch.shieldSlots.forEach((slot) => (variants *= shields.getShieldsBySlotType(slot).length));
	if (ch.weaponSlots) ch.weaponSlots.forEach((slot) => (variants *= weapons.getWeaponsBySlot(slot).length));
	const eq = new ch().equipmentSlots || 0;
	if (eq) variants *= Math.pow(Object.values(equipment.getAll()).length, eq);
	counter += variants;
});

console.info('Total Prototype variants:', counter);

import _ from 'underscore';
import BasicMaths from '../definitions/maths/BasicMaths';
import EstablishmentManager from '../definitions/orbital/establishments/EstablishmentManager';
import { IEstablishment } from '../definitions/orbital/establishments/types';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import describeModifier = BasicMaths.describeModifier;

DIContainer.inject({
	[DIInjectableCollectibles.establishment]: EstablishmentManager,
});
const ucFirst = (str: string) => str.substr(0, 1).toUpperCase() + str.substr(1);

const establishments = DIContainer.getProvider(DIInjectableCollectibles.establishment).getAll();

const getExtraDescription = (e: IEstablishment) => {
	const r = [];
	if (e.static.intelligenceLevel) r.push(`${describeModifier(e.static.intelligenceLevel)} Recon Level`);
	if (e.static.counterIntelligenceLevel)
		r.push(`${describeModifier(e.static.counterIntelligenceLevel)} Counter-Recon Level`);
	if (e.static.researchOutputModifier)
		r.push(`${describeModifier(e.static.researchOutputModifier)} Research Output to every Establishment`);
	if (e.static.energyOutputModifier)
		r.push(`${describeModifier(e.static.energyOutputModifier)} Energy Output at every Facility in Output Mode`);
	if (e.static.energyConsumptionModifier)
		r.push(`${describeModifier(e.static.energyConsumptionModifier)} Base Energy Consumption`);
	if (e.static.engineeringOutputModifier)
		r.push(`${describeModifier(e.static.engineeringOutputModifier)} Engineering Output at every Establishment`);
	if (e.static.repairSpeedModifier)
		r.push(`${describeModifier(e.static.repairSpeedModifier)} Repair Output at every Facility in Output Mode`);
	if (e.static.materialOutputModifier)
		r.push(
			`${describeModifier(e.static.materialOutputModifier)} Raw Material Output at every Facility in Output Mode`,
		);
	if (e.static.maxEnergyCapacity) r.push(`${describeModifier(e.static.maxEnergyCapacity)} Energy Storage`);
	if (e.static.armor) r.push(`${DefenseManager.describeProtection(e.static.armor)} Base Armor Damage`);
	return r.join(', ');
};

console.info(
	[
		'Name',
		'Description',
		'ID',
		'Tier',
		'Research Output',
		'Engineering Output',
		'Base Bonuses',
		'Upkeep',
		'Resource Cost',
	].join(';'),
);
Object.keys(establishments)
	.map((establishmentId) => {
		const f = DIContainer.getProvider(DIInjectableCollectibles.establishment).instantiate(
			{
				playerId: '-1',
				baseId: '-1',
			},
			establishmentId,
		);
		return [
			f.static.caption,
			f.static.description.replace('\n', ' ').replace(/\s+/im, ' '),
			f.static.id,
			f.static.buildingTier,
			f.researchOutput || 0,
			f.engineeringOutput || 0,
			getExtraDescription(f),
			f.energyMaintenanceCost,
			_.reduce(
				f.static.buildingCost,
				(str, resourceValue, resourceType) =>
					`${str}${ucFirst(String(resourceType).replace('res_', ''))}: ${resourceValue}, `,
				'',
			)
				.trim()
				.replace(/,\s*$/, ''),
		]
			.map((v) => `"${v}"`)
			.join(';');
	})
	.forEach((v) => console.info(v));

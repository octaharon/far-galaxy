import _ from 'underscore';
import Series from '../definitions/maths/Series';
import { PlanetTypeCaptions, TPlanetType } from '../definitions/space/Planets';
import { generatePlanetResourceDistribution } from '../definitions/space/Planets/generators';
import { generateSystem } from '../definitions/space/Systems/tools';
import { ResourceCaptions, ResourceDictionary } from '../definitions/world/resources/constants';
import { addResourcePayloads, resourceTemplate } from '../definitions/world/resources/helpers';
import Resources from '../definitions/world/resources/types';
import { wellRounded } from '../src/utils/wellRounded';

const SYSTEM_COUNT = 25000;

const systems = _.range(SYSTEM_COUNT).map(() => generateSystem(true));

let totalPlanets = 0;
for (const system of systems) totalPlanets += system.planetCount;

console.warn(`Total Stars`, SYSTEM_COUNT);
console.warn(`Total Planets`, totalPlanets);
console.warn(`Average Planets per System:`, wellRounded(totalPlanets / SYSTEM_COUNT));

const planetCount = systems.reduce(
	(dict, system) =>
		system.planets.reduce(
			(d, planet) => ({
				...d,
				[planet.type]: (d[planet.type] ?? 0) + 1,
			}),
			dict,
		),
	{} as Record<TPlanetType, number>,
);

const planetTypeReport = _.mapObject(planetCount, (v) => `${v} (${wellRounded((100 * v) / totalPlanets)}%)`);

Object.entries(planetCount)
	.sort((x, y) => x[1] - y[1])
	.forEach(([type]) => console.log(PlanetTypeCaptions[type], planetTypeReport[type]));

const resourceDensityCount = systems.reduce(
	(acc, system) => addResourcePayloads(acc, ...system.planets.map((p) => generatePlanetResourceDistribution(p.type))),
	resourceTemplate(0),
);

const totalResourceDensity = Series.sum(Object.values(resourceDensityCount));

console.warn();
console.warn('Resources Distribution');
console.warn('Total Resource Nodes', totalResourceDensity);
console.warn(
	'Total projected: ',
	100 * Series.sum(Object.values(_.mapObject(ResourceDictionary, (t) => t.relativeFrequency))),
);

const resourceDensityReport = _.mapObject(
	resourceDensityCount,
	(v, type) =>
		`${v} (${wellRounded((100 * v) / totalResourceDensity)}%); projected - ${wellRounded(
			100 * ResourceDictionary[type as Resources.resourceId].relativeFrequency,
			1,
		)}%`,
);
Object.entries(resourceDensityCount)
	.sort((x, y) => x[1] - y[1])
	.forEach(([type]) =>
		console.log(
			ResourceCaptions[type as Resources.resourceId],
			resourceDensityReport[type as Resources.resourceId],
		),
	);

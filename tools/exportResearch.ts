import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';
import ResearchRegistry from '../definitions/player/Research/ResearchRegistry';
import { TPlayerResearchDefinition } from '../definitions/player/Research/types';

DIContainer.inject(DIPresetDefault);
// Mermaid syntax inside markdown
const lines: string[] = [];
lines.push('## Research', '```mermaid', 'flowchart LR');
const researchCache: Record<number, TPlayerResearchDefinition[]> = {};
const researchTitleState: Record<number, boolean> = {};
ResearchRegistry.getAll().forEach((research) => {
	researchCache[research.cost || 0] = [...(researchCache[research.cost || 0] || []), research];
});

Object.keys(researchCache)
	.sort((a, b) => parseInt(String(a)) - parseInt(String(b)))
	.forEach((cost, groupIx) => {
		const inGroup = researchCache[cost].length > 1;
		if (inGroup) {
			lines.push(`\tsubgraph "Cost ${cost}"`);
			lines.push(`\tdirection TB`);
		}
		researchCache[cost].forEach((research) => {
			const caption = !researchTitleState[research.id]
				? `${research.repeatable ? '([' : '['}"${research.caption}"${research.repeatable ? '])' : ']'}`
				: '';
			if (research.prerequisiteResearchIds?.length)
				research.prerequisiteResearchIds.forEach((pr, ix) => {
					const pResearch = ResearchRegistry.get(pr);
					const prCaption = `${pResearch.repeatable ? '([' : '['}"${pResearch.caption}"${
						pResearch.repeatable ? '])' : ']'
					}`;
					const str = `\t\t${pr}${!researchTitleState[pr] ? prCaption : ''} -..-> ${research.id}${
						ix === 0 ? caption : ''
					}`;
					if (!researchTitleState[pr]) researchTitleState[pr] = true;
					if (ix === 0) researchTitleState[research.id] = true;
					lines.push(str);
				});
			else if (!researchTitleState[research.id]) {
				lines.push(`\t\t${research.id}${caption}`);
				researchTitleState[research.id] = true;
			}
		});
		if (inGroup) lines.push('\tend');
	});
lines.push('```');
console.log(lines.join('\n'));

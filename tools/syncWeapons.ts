import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import BasicMaths from '../definitions/maths/BasicMaths';
import AttackManager from '../definitions/tactical/damage/AttackManager';

import { ORBITAL_ATTACK_RANGE } from '../definitions/tactical/units/actions/constants';
import { IWeapon } from '../definitions/technologies/types/weapons';
import WeaponManager from '../definitions/technologies/weapons/WeaponManager';
import Environment from '../src/env';
import { createWeaponArray, createWeaponMapping } from '../src/notion/collectionHelpers';
import {
	WeaponDatabaseId,
	WeaponGroupsDatabaseId,
	WeaponGroupsIconsId,
	WeaponGroupsPagesId,
	WeaponSlotsPagesId,
} from '../src/notion/constants';
import { deleteRemotePage, getRemotePagesByID, updateDatabaseSelectOptions } from '../src/notion/dataHelpers';
import {
	sanitizeAttackTransfer,
	sanitizeAttackTypes,
	sanitizeCollectibleDescription,
	sanitizeCollectibleName,
	sanitizeDPT,
	sanitizeDPSTotal,
	TNotionLabel,
} from '../src/notion/valueHelpers';
import { splitIntoChunks } from '../src/utils/splitIntoChunks';

const notion = new Client({ auth: Environment.NOTION_API_KEY });

const AttackTypesCache: Record<string, TNotionLabel> = {};
const DamageCache: Record<string, TNotionLabel> = {};
const LabelCache: Record<number, { attacks: TNotionLabel[]; damage: TNotionLabel[]; delivery: TNotionLabel[] }> = {};

function buildLabelCache(weapons: IWeapon[]) {
	weapons.forEach((weapon) => {
		const wCache: ValueOf<typeof LabelCache> = {
			attacks: [],
			damage: [],
			delivery: [],
		};
		const damage = sanitizeDPT(weapon);
		const attacks = sanitizeAttackTypes(weapon);
		const delivery = sanitizeAttackTransfer(weapon);

		Object.assign(wCache, {
			attacks: attacks.map((p) => ({ name: p.name })),
			damage: damage.map((p) => ({ name: p.name })),
			delivery: delivery.map((p) => ({ name: p.name })),
		});
		attacks.forEach((p) => {
			AttackTypesCache[p.name] = p;
		});
		damage.forEach((p) => {
			DamageCache[p.name] = p;
		});
		LabelCache[weapon.static.id] = wCache;
	});
}

function mapWeaponToPageProperties(weapon: IWeapon): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(weapon.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		ID: { type: 'number', number: weapon.static.id },
		Description: {
			type: 'rich_text',
			rich_text: [{ type: 'text', text: { content: sanitizeCollectibleDescription(weapon.static.description) } }],
		},
		Cost: { type: 'number', number: weapon.baseCost },
		Range: { type: 'number', number: BasicMaths.roundToPrecision(weapon.baseRange, 0.1) },
		Priority: {
			type: 'select',
			select: {
				name: AttackManager.describePriority(weapon.priority),
			},
		},
		'Hit Chance': {
			type: 'number',
			number: BasicMaths.roundToPrecision(weapon.baseAccuracy, 1e-2),
		},
		'Rate of Fire': {
			type: 'number',
			number: BasicMaths.roundToPrecision(weapon.baseRate, 1e-2),
		},
		'APT Min': {
			type: 'number',
			number: AttackManager.getAttacksQuantityMinMax(weapon.baseRate)[0],
		},
		'APT Max': {
			type: 'number',
			number: AttackManager.getAttacksQuantityMinMax(weapon.baseRate)[1],
		},
		DPS: {
			type: 'number',
			number: sanitizeDPSTotal(weapon),
		},
		'Transfer Type': {
			type: 'multi_select',
			multi_select: LabelCache[weapon.static.id].delivery,
		},
		'Orbital Attack': {
			type: 'select',
			select: { name: weapon.baseRange >= ORBITAL_ATTACK_RANGE ? 'Yes' : 'No' },
		},
		DPT: {
			type: 'multi_select',
			multi_select: LabelCache[weapon.static.id].damage,
		},
		'Attack Type': {
			type: 'multi_select',
			multi_select: LabelCache[weapon.static.id].attacks,
		},
		Group: {
			type: 'relation',
			relation: [
				{
					id: WeaponGroupsPagesId[weapon.static.groupType],
				},
			],
		},
		Slots: {
			type: 'relation',
			relation: DIContainer.getProvider(DIInjectableCollectibles.weapon)
				.getWeaponCompatibleSlots(weapon.static.id)
				.map((slot) => ({
					id: WeaponSlotsPagesId[slot],
				})),
		},
	};
}

async function updateRemotePageWithWeapon(pageId: string, weapon: IWeapon) {
	return notion.pages.update({
		page_id: pageId,
		icon: {
			external: {
				url: WeaponGroupsIconsId[weapon.static.groupType],
			},
		},
		properties: mapWeaponToPageProperties(weapon),
	});
}

async function createRemotePageWithWeapon(databaseId: string, weapon: IWeapon) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		icon: {
			external: {
				url: WeaponGroupsIconsId[weapon.static.groupType],
			},
		},
		properties: mapWeaponToPageProperties(weapon),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.weapon]: WeaponManager,
});

async function syncToRemote(dBID: string) {
	const localWeapons = createWeaponArray();
	const weaponPagesMap = await createWeaponMapping(notion);
	buildLabelCache(localWeapons);
	await updateDatabaseSelectOptions(notion, dBID, 'DPT', []);
	await updateDatabaseSelectOptions(notion, dBID, 'Attack Type', []);
	const pLabels = splitIntoChunks(Object.values(DamageCache), 99);
	for (const pLabelSet of pLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'DPT', pLabelSet);
	}
	const aLabels = splitIntoChunks(Object.values(AttackTypesCache), 99);
	for (const aLabelSet of aLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'Attack Type', aLabelSet);
	}

	const pages = Object.keys(weaponPagesMap);
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const weaponId = weaponPagesMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.weapon).getIds().includes(weaponId)) {
			await updateRemotePageWithWeapon(
				pageId,
				DIContainer.getProvider(DIInjectableCollectibles.weapon).instantiate(null, weaponId),
			);
			console.log('updating page for Weapon: ' + weaponId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	const newWeapons = localWeapons.filter((e) => !Object.values(weaponPagesMap).includes(e.static.id));
	for (let i = 0; i < newWeapons.length; i++) {
		console.log(`Creating a page for Weapon ${newWeapons[i].static.id}`);
		await createRemotePageWithWeapon(dBID, newWeapons[i]);
	}
	console.log(`Synced ${localWeapons.length} Weapons to Notion!`);
}

syncToRemote(WeaponDatabaseId);
/*
getRemotePagesByID(notion, WeaponGroupsDatabaseId).then((results) => {
	console.log(
		results.map((r) => ({
			id: r.id,
			// @ts-ignore
			url: r.url,
			// @ts-ignore
			icon: r.icon.file,
		})),
	);
});
*/

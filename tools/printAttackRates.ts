import Series from '../definitions/maths/Series';
import AttackManager from '../definitions/tactical/damage/AttackManager';
import { wellRounded } from '../src/utils/wellRounded';

const samples = 10000;
const iterations = 100;
const divisor = samples * iterations;
const resampleRate = 30;
const printTable =
	(header = false) =>
	(...args: any[]): void => {
		console.info(args.map((v, ix) => (!header && ix === 0 ? `*${String(v)}*` : String(v))).join(' | '));
	};

const attackRates = [
	0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.5, 3, 3.5, 4, 4.5, 5, 6, 7.5, 10, 25, 50,
	100,
];

const chartLength = 40;

attackRates.forEach((weaponRate) => {
	const output: Series.TSeries = new Array(resampleRate).fill(0);
	let max = -Infinity;
	let avg = 0;
	let sum = 0;
	let mode = -1;

	for (let cycles = 0; cycles < iterations; cycles++)
		for (let i = 0; i < samples; i++) {
			const v = AttackManager.getNumberOfAttacks(weaponRate);
			if (isNaN(v)) {
				console.warn(weaponRate + ' returned NaN');
				continue;
			}
			const ix = v - 1;
			output[ix]++;
			sum += v;
			if (output[ix] > max) {
				mode = ix;
				max = output[ix];
			}
		}
	avg = sum / divisor;

	const cl = Math.max(...output);
	console.info('## Weapon rate = ' + weaponRate, '\n');
	printTable(true)('**Metric**', '**Attacks Per Turn**');
	printTable(true)('---', '---');
	printTable(true)('*Minimal*', output.findIndex(Boolean) + 1);
	printTable(true)('*Maximal*', output.length - output.slice().reverse().findIndex(Boolean));
	printTable(true)('*Average*', wellRounded(avg, 2));
	printTable(true)('*Mode (most likely)*', mode + 1);

	console.info('\n');
	printTable(true)('Number of attacks', `Cases per ${divisor} turns`, 'Probability', 'Chart');
	printTable(true)('---', '---', '---', '---');
	output.forEach((s, ix) =>
		s
			? printTable()(
					ix + 1,
					s,
					((s / divisor) * 100).toFixed(3) + '%',
					new Array(Math.ceil((s / cl) * chartLength)).fill('█').join(''),
			  )
			: null,
	);
	console.info('');
	console.info('');
});

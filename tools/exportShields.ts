import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import ShieldManager from '../definitions/technologies/shields/ShieldManager';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';

DIContainer.inject({
	[DIInjectableCollectibles.shield]: ShieldManager,
});

const shields = DIContainer.getProvider(DIInjectableCollectibles.shield).getAll();

console.info(
	['Name', 'Description', 'ID', 'Base Cost', 'Slot', 'Protection', 'Capacity', 'Overcharge Decay', 'Perks'].join(';'),
);
Object.keys(shields)
	.map((shieldId) => {
		const shield = DIContainer.getProvider(DIInjectableCollectibles.shield).instantiate(null, shieldId);
		return [
			shield.static.caption,
			shield.static.description
				.replace(/[\r\n\s]+/g, ' ')
				.replace(/\s+/, ' ')
				.trim(),
			shieldId,
			shield.baseCost,
			shield.static.type,
			DefenseManager.describeProtection(shield.shield).replace(/\s*\|\s*/g, ', '),
			shield.shield.shieldAmount,
			shield.shield.overchargeDecay || '',
			shield.static?.flags ? Object.keys(shield.static.flags).join(', ') : '',
		]
			.map((v) => `"${v}"`)
			.join(';');
	})
	.forEach((v) => console.info(v));

import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import _ from 'underscore';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import AbilityManager from '../definitions/tactical/abilities/AbilityManager';
import Abilities from '../definitions/tactical/abilities/types';
import { MAX_MAP_RADIUS } from '../definitions/tactical/map/constants';
import AuraManager from '../definitions/tactical/statuses/Auras/AuraManager';
import { IAura } from '../definitions/tactical/statuses/Auras/types';
import EffectManager from '../definitions/tactical/statuses/EffectManager';
import Environment from '../src/env';
import {
	createAbilityArray,
	createAbilityMapping,
	createAuraArray,
	createAuraMapping,
	createEffectMapping,
} from '../src/notion/collectionHelpers';
import { AbilityDatabaseId, AuraDatabaseId } from '../src/notion/constants';
import { deleteRemotePage } from '../src/notion/dataHelpers';
import { sanitizeCollectibleDescription, sanitizeCollectibleName } from '../src/notion/valueHelpers';

const notion = new Client({ auth: Environment.NOTION_API_KEY });

function mapAuraToPageProperties(
	aura: IAura,
	effectLookupMap?: Record<number, string>,
): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(aura.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Range: { type: 'number', number: aura.static.baseRange },
		Description: {
			type: 'rich_text',
			rich_text: [{ type: 'text', text: { content: sanitizeCollectibleDescription(aura.static.description) } }],
		},
		ID: { type: 'number', number: aura.static.id },
		Effects: {
			relation: aura.static.appliedEffects
				.map((effAppliance) => effAppliance.effectId)
				.filter((effectId) => !!effectLookupMap?.[effectId])
				.map((effectId) => ({ id: effectLookupMap?.[effectId] })),
		},
		Targets: {
			type: 'select',
			select: { name: aura.static.selective ? 'Friendly' : 'All' },
		},
	};
}

async function updateRemotePageWithAura(pageId: string, aura: IAura, effectLookupMap?: Record<number, string>) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapAuraToPageProperties(aura, effectLookupMap),
	});
}

async function createRemotePageWithAura(databaseId: string, aura: IAura, effectLookupMap?: Record<number, string>) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapAuraToPageProperties(aura, effectLookupMap),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.aura]: AuraManager,
	[DIInjectableCollectibles.effect]: EffectManager,
});

async function syncToRemote(dBID: string) {
	const localAuras = createAuraArray();
	const auraPageMap = await createAuraMapping(notion);
	const notionEffectLookupMap = _.invert(await createEffectMapping(notion)) as Record<number, string>;
	const pages = Object.keys(auraPageMap);
	//update existing abilities
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const auraId = auraPageMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.aura).getIds().includes(auraId)) {
			await updateRemotePageWithAura(
				pageId,
				DIContainer.getProvider(DIInjectableCollectibles.aura).instantiate(null, auraId),
				notionEffectLookupMap,
			);
			console.log('updating page for Aura: ' + auraId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	// Add new abilities to Notion
	const newAuras = localAuras.filter((e) => !Object.values(auraPageMap).includes(e.static.id));

	for (let i = 0; i < newAuras.length; i++) {
		console.log(`Creating a page for Aura ${newAuras[i].static.id}`);
		await createRemotePageWithAura(dBID, newAuras[i]);
	}
	console.log(`Synced ${localAuras.length} Auras to Notion!`);
}

syncToRemote(AuraDatabaseId);

// {
//   Cooldown: { id: '%40_%7DC', type: 'number', number: 3 },
//   Description: { id: 'G%5Csv', type: 'rich_text', rich_text: [ [Object] ] }
//   Target: {
//     id: 'TLzH',
//     type: 'select',
//     select: { id: 'IWHS', name: 'Location', color: 'pink' }
//   },
//   ID: { id: 'hZzE', type: 'number', number: 45007 },
//   Range: { id: 'mXXK', type: 'number', number: 90 },
//   Name: { id: 'title', type: 'title', title: [ [Object] ] }
// }
// {
//   object: 'page',
//   id: '012688d5-1faf-4600-8203-23801639661a',
//   created_time: '2021-01-19T22:31:00.000Z',
//   last_edited_time: '2021-01-19T22:33:00.000Z',
//   cover: null,
//   icon: null,
//   parent: {
//     type: 'database_id',
//     database_id: '4c510a01-81ad-40a6-be31-3c47e0b8c2dd'
//   },
//   archived: false,
//   properties: {
//     Cooldown: { id: '%40_%7DC', type: 'number', number: 10 },
//     Description: { id: 'G%5Csv', type: 'rich_text', rich_text: [Array] },
//     Target: { id: 'TLzH', type: 'select', select: [Object] },
//     ID: { id: 'hZzE', type: 'number', number: 60004 },
//     Range: { id: 'mXXK', type: 'number', number: 90 },
//     Name: { id: 'title', type: 'title', title: [Array] }
//   },
//   url: 'https://www.notion.so/Weather-Storm-012688d51faf4600820323801639661a'
// }

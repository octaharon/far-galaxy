import fs from 'fs';
import { fastMerge } from '../definitions/core/dataTransformTools';
import { InterfaceTypes } from '../definitions/core/InterfaceTypes';
import Geometry from '../definitions/maths/Geometry';
import MapGenerator from '../definitions/tactical/map/MapGenerator';
import { IMapCave, IMapHill, IMapLake, TMapDoodad } from '../definitions/tactical/map/MapObstacles';
import { defaultMapParameters, TMapParameters } from '../definitions/tactical/map';

const maxR = 100;

function describeArc(t: InterfaceTypes.TArc<InterfaceTypes.IVector>) {
	const v = Geometry.arcToVectors(t);

	const largeArcFlag = t.end - t.start <= Math.PI ? '0' : '1';

	const d = [
		'M',
		[v[0].x, v[0].y].join(','),
		'A',
		[t.radius, t.radius, 0, largeArcFlag, 0, v[1].x, v[1].y].join(','),
	].join('');

	return d;
}

function describeSector(t: InterfaceTypes.TArc<InterfaceTypes.IVector>) {
	const v = Geometry.arcToVectors(t);

	const largeArcFlag = t.end - t.start <= Math.PI ? '0' : '1';

	const d = [
		`M${t.center.x},${t.center.y}`,
		`L${v[0].x},${v[0].y}`,
		`A${[t.radius, t.radius, 0, largeArcFlag, 0, v[1].x, v[1].y].join(',')}`,
		`z`,
	].join('');

	return d;
}

const printSvg = (
	id: string,
	hills: IMapHill[],
	caves: IMapCave[],
	lakes: IMapLake[],
	doodads: TMapDoodad[],
	lines: InterfaceTypes.IVector[][],
	radius: number,
) => `<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'
width='900px' height='900px' viewBox='-${radius} -${radius} ${radius * 2} ${
	radius * 2
}' preserveAspectRatio='xMidYMid meet' id='${id}'>
<defs>
    <mask id='cut-off-circle-${id}'>
        <rect x='-${radius + 1}' y='-${radius + 1}' width='${2 * radius + 2}' height='${2 * radius + 2}' fill='black'/>
      <circle cx='0' cy='0' r='${radius}' fill='white'/>
    </mask>
  </defs>
  <g class='background' mask='url(#cut-off-circle-${id})'>
<circle id='mapBg' cx='0' cy='0' r='${radius}'  style='fill: black; stroke: #006;'/>
${lakes.map(
	(h) =>
		`<g class='lake'>${Geometry.polarShapeToArcArray(h.shape)
			.map(
				(l: InterfaceTypes.TArc<InterfaceTypes.IVector>, ix) =>
					`<path vector-effect='non-scaling-stroke' d='${describeSector(
						l,
					)}' style='fill:#64a2ff;stroke:#000070;stroke-width:.5px;' stroke-opacity='0.4' fill-opacity='0.7' id='${
						h.entityId
					}-${ix}'/>`,
			)
			.join('\n')}</g>`,
)}
${hills
	.map(
		(h) =>
			`<g class='hill'>${Geometry.polarShapeToArcArray(h.shape)
				.map(
					(l, ix) =>
						`<path vector-effect='non-scaling-stroke' d='${describeSector(
							l,
						)}' style='fill:#38553e;stroke:#998564;stroke-width:.25px;' id='${h.entityId}-${ix}'/>`,
				)
				.join('\n')}</g>`,
	)
	.join('')}

${caves
	.map(
		(h) =>
			`<g class='cave'>${Geometry.polarShapeToArcArray(h.shape)
				.map(
					(l, ix) =>
						`<path vector-effect='non-scaling-stroke' d='${describeSector(
							l,
						)}' style='fill:#1b1d24;stroke:#66515c;stroke-width:.5px;' id='${h.entityId}-${ix}'/>`,
				)
				.join('\n')}</g>`,
	)
	.join('')}
</g>
<g class='lines'>
${lines
	.map(
		(l) =>
			`<line vector-effect='non-scaling-stroke' stroke-dasharray='3,3' x1='${l[0].x}' y1='${l[0].y}' x2='${
				l[1].x
			}' y2='${l[1].y}' style='stroke:${
				hills.reduce((cross, h) => cross || !!Geometry.doesRayIntersectShape(h.shape, l[0], l[1]), false)
					? '#94321e'
					: '#a5c217'
			};stroke-width:0.5px' />`,
	)
	.join('')}</g><g class='doodads'>
${doodads
	.map((c) => {
		return `<circle cx='${c.x}' cy='${c.y}' r='${
			c.collisionRadius
		}' vector-effect='non-scaling-stroke' style='opacity:0.8;fill:#777; stroke-width:1px; stroke:${
			c.lineOfSight ? 'green' : 'yellow'
		}'/>`;
	})
	.join('\n')}
</g>
    <circle id='innerCircle' cx='0' cy='0' r='15' style='stroke:green; stroke-width:.2px;fill:none' stroke-dasharray='2,3'/>
</svg>`;

const testRun = (withLines = true) => {
	const p = fastMerge(defaultMapParameters) as TMapParameters;
	p.flatness = Math.random();
	p.waterPresence = Math.random();
	p.terrainDensity = Math.random();
	p.size = Math.random();
	p.centralZone = Math.random() > 0.5;
	const map = MapGenerator.generate(p);
	const hills = Object.values(map.terrainEntities).filter((v) => !v.passable && !v.lineOfSight) as IMapHill[];
	const caves = Object.values(map.terrainEntities).filter((v) => !v.passable && v.lineOfSight) as IMapCave[];
	const lakes = Object.values(map.terrainEntities).filter((v) => v.water) as IMapLake[];
	const lines = !withLines
		? []
		: (new Array(30).fill(0).map((v) => [
				Geometry.polar2vector({
					r: Math.random() * maxR,
					alpha: Math.random() * Math.PI * 2,
				}),
				Geometry.polar2vector({
					r: Math.random() * maxR,
					alpha: Math.random() * Math.PI * 2,
				}),
		  ]) as InterfaceTypes.IVector[][]);
	// console.info(map.doodads);
	return { p, svg: printSvg(map.getInstanceId(), hills, caves, lakes, map.doodads, lines, map.radius) };
};

fs.writeFileSync(
	'./mapTest.html',
	`<!DOCTYPE html>
    <head>
    <title>Map Generator test ${new Date().toString()}</title>
    <style>
        section{
            clear:both;
            margin-bottom:10px;
            position:relative;
            text-align:center;
        }

        section svg
        {
            margin:auto;
        }

        section .d
        {
            text-align:left;
            color:#333;
            line-height:1.25;
            position:absolute;
            left:0;
            top:50%;
            transform:translateY(-50%);
        }
    </style>
    </head>
    <body>
    ${new Array(10)
		.fill(0)
		.map((v, ix) => testRun(ix === 0))
		.map(({ p, svg }) => {
			return `<section><pre class='d'>${JSON.stringify(p, undefined, 2)}</pre>${svg}</section>`;
		})
		.join('')}
    </body></html>
    `,
);

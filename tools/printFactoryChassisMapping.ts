import FactoryManager from '../definitions/orbital/factories/FactoryManager';
import { TFactory } from '../definitions/orbital/factories/types';
import UnitChassis from '../definitions/technologies/types/chassis';

const chassisTypes = UnitChassis.chassisClass;
const factories = Object.values(new FactoryManager().fill().getAll<TFactory>()) as TFactory[];

console.info('## Chassis requires a factory', '\n');
console.info('Chassis Type | Factories');
console.info(':---: | :--- ');
Object.values(chassisTypes).forEach((ch_key: UnitChassis.chassisClass, index: number) => {
	const fact_text = factories
		.filter((f) => f.productionType.includes(ch_key))
		.map((f) => f.caption)
		.join('<br>');
	// @ts-ignore
	console.info(`**${Object.keys(chassisTypes)[index]}** | ${fact_text}`);
});

console.info('\n');

console.info('## Factory can build chassis', '\n');
console.info('Factory | Chassis');
console.info(':---: | :--- ');
factories.forEach((factory) => {
	const ch_text = factory.productionType
		.map((k: string) => Object.keys(chassisTypes)[Object.values(chassisTypes).findIndex((a) => a === k)])
		.join('<br>');
	// @ts-ignore
	console.info(`**${factory.caption}** | ${ch_text}`);
});

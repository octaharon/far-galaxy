import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import EffectManager from '../definitions/tactical/statuses/EffectManager';
import StatusEffectMeta from '../definitions/tactical/statuses/statusEffectMeta';
import { IStatusEffect } from '../definitions/tactical/statuses/types';
import Environment from '../src/env';
import { createEffectArray, createEffectMapping } from '../src/notion/collectionHelpers';
import { EffectDatabaseId } from '../src/notion/constants';
import { deleteRemotePage } from '../src/notion/dataHelpers';
import { sanitizeCollectibleDescription, sanitizeCollectibleName } from '../src/notion/valueHelpers';

const notion = new Client({ auth: Environment.NOTION_API_KEY });

function sanitizeDuration(duration: number): number {
	return Number.isFinite(duration) && duration > 0 ? duration : null;
}

function sanitizeEffectTarget(t: StatusEffectMeta.effectAgentTypes) {
	return {
		[StatusEffectMeta.effectAgentTypes.player]: 'Base',
		[StatusEffectMeta.effectAgentTypes.unit]: 'Unit',
		[StatusEffectMeta.effectAgentTypes.map]: 'Map',
	}[t];
}

function mapEffectToPageProperties(effect: IStatusEffect<any>): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(effect.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Target: { type: 'select', select: { name: sanitizeEffectTarget(effect.static.targetType) } },
		Duration: { type: 'number', number: sanitizeDuration(effect.static.baseDuration) },
		Description: {
			type: 'rich_text',
			rich_text: [{ type: 'text', text: { content: sanitizeCollectibleDescription(effect.static.description) } }],
		},
		ID: { type: 'number', number: effect.static.id },
		Negative: { type: 'select', select: { name: effect.static.negative ? 'Yes' : 'No' } },
		Constant: { type: 'select', select: { name: effect.static.durationFixed ? 'Yes' : 'No' } },
		Removable: { type: 'select', select: { name: effect.static.unremovable ? 'No' : 'Yes' } },
	};
}

async function updateRemotePageWithEffect(pageId: string, effect: IStatusEffect<any>) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapEffectToPageProperties(effect),
	});
}

async function createRemotePageWithEffect(databaseId: string, effect: IStatusEffect<any>) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapEffectToPageProperties(effect),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.effect]: EffectManager,
});

async function syncToRemote(dBID: string) {
	const localEffects = createEffectArray();
	const effectPageMap = await createEffectMapping(notion);

	const pages = Object.keys(effectPageMap);
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const effectId = effectPageMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.effect).getIds().includes(effectId)) {
			await updateRemotePageWithEffect(
				pageId,
				DIContainer.getProvider(DIInjectableCollectibles.effect).instantiate(null, effectId),
			);
			console.log('updating page for Effect: ' + effectId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	const newEffects = localEffects.filter((e) => !Object.values(effectPageMap).includes(e.static.id));
	for (let i = 0; i < newEffects.length; i++) {
		console.log(`Creating a page for Effect ${newEffects[i].static.id}`);
		await createRemotePageWithEffect(dBID, newEffects[i]);
	}
	console.log(`Synced ${localEffects.length} Effects to Notion!`);
}

syncToRemote(EffectDatabaseId);

import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import _ from 'underscore';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import EstablishmentManager from '../definitions/orbital/establishments/EstablishmentManager';
import { IEstablishment } from '../definitions/orbital/establishments/types';
import Resources from '../definitions/world/resources/types';
import Environment from '../src/env';
import {
	createAbilityMapping,
	createEstablishmentArray,
	createEstablishmentMapping,
} from '../src/notion/collectionHelpers';
import { EstablishmentDatabaseId } from '../src/notion/constants';
import { deleteRemotePage, updateDatabaseSelectOptions } from '../src/notion/dataHelpers';
import {
	sanitizeBuildingModifier,
	sanitizeCollectibleDescription,
	sanitizeCollectibleName,
	TNotionLabel,
} from '../src/notion/valueHelpers';
import { splitIntoChunks } from '../src/utils/splitIntoChunks';

const notion = new Client({ auth: Environment.NOTION_API_KEY });

const BaseModifierLabelCache: Record<string, TNotionLabel> = {};
const LabelCache: Record<number, { base: TNotionLabel[] }> = {};

function buildLabelCache(factories: IEstablishment[]) {
	factories.forEach((factory) => {
		const base = sanitizeBuildingModifier(factory);
		LabelCache[factory.static.id] = {
			base: base.map((v) => ({ name: v.name })),
		};
		base.forEach((p) => {
			BaseModifierLabelCache[p.name] = p;
		});
	});
}

function mapEstablishmentToPageProperties(
	establishment: IEstablishment,
	abilityLookupMap?: Record<number, string>,
): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(establishment.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Description: {
			type: 'rich_text',
			rich_text: [
				{ type: 'text', text: { content: sanitizeCollectibleDescription(establishment.static.description) } },
			],
		},
		'Base Bonuses': {
			type: 'multi_select',
			multi_select: LabelCache[establishment.static.id].base,
		},
		Abilities: {
			relation: (establishment.static.abilities || [])
				.filter((abilityId) => !!abilityLookupMap[abilityId])
				.map((abilityId) => ({
					id: abilityLookupMap[abilityId],
				})),
		},
		ID: { type: 'number', number: establishment.static.id },
		Tier: { type: 'number', number: establishment.static.buildingTier },
		'Research Output': { type: 'number', number: establishment.researchOutput || null },
		'Engineering Output': { type: 'number', number: establishment.engineeringOutput || null },
		Upkeep: { type: 'number', number: establishment.energyMaintenanceCost || null },
		'Cost: Hydrogen': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.hydrogen] ?? null,
		},
		'Cost: Helium': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.helium] ?? null,
		},
		'Cost: Carbon': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.carbon] ?? null,
		},
		'Cost: Oxygen': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.oxygen] ?? null,
		},
		'Cost: Nitrogen': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.nitrogen] ?? null,
		},
		'Cost: Halogens': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.halogens] ?? null,
		},
		'Cost: Ores': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.ores] ?? null,
		},
		'Cost: Isotopes': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.isotopes] ?? null,
		},
		'Cost: Minerals': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.minerals] ?? null,
		},
		'Cost: Proteins': {
			type: 'number',
			number: establishment.static.buildingCost[Resources.resourceId.proteins] ?? null,
		},
	};
}

async function updateRemotePageWithEstablishment(
	pageId: string,
	establishment: IEstablishment,
	abilityLookupMap?: Record<number, string>,
) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapEstablishmentToPageProperties(establishment, abilityLookupMap),
	});
}

async function createRemotePageWithFactory(
	databaseId: string,
	establishment: IEstablishment,
	abilityLookupMap?: Record<number, string>,
) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapEstablishmentToPageProperties(establishment, abilityLookupMap),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.establishment]: EstablishmentManager,
});

async function syncToRemote(dBID: string) {
	const localBD = createEstablishmentArray();
	buildLabelCache(localBD);
	const establishmentPageMap = await createEstablishmentMapping(notion);
	await updateDatabaseSelectOptions(notion, dBID, 'Base Bonuses', []);
	const pages = Object.keys(establishmentPageMap);
	const pLabels = splitIntoChunks(Object.values(BaseModifierLabelCache), 99);
	const notionAbilityLookupMap = _.invert(await createAbilityMapping(notion)) as Record<number, string>;
	for (const pLabelSet of pLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'Base Bonuses', pLabelSet);
	}

	//update existing establishments
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const establishmentId = establishmentPageMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.establishment).getIds().includes(establishmentId)) {
			await updateRemotePageWithEstablishment(
				pageId,
				localBD.find((f) => f.static.id === establishmentId),
				notionAbilityLookupMap,
			);
			console.log('updating page for Establishment: ' + establishmentId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	// Add new establishments to Notion
	const newEstablishments = localBD.filter((e) => !Object.values(establishmentPageMap).includes(e.static.id));

	for (let i = 0; i < newEstablishments.length; i++) {
		console.log(`Creating a page for Establishment ${newEstablishments[i].static.id}`);
		await createRemotePageWithFactory(dBID, newEstablishments[i], notionAbilityLookupMap);
	}
	console.log(`Synced ${localBD.length} Establishments to Notion!`);
}

syncToRemote(EstablishmentDatabaseId);

//getRemotePagesByID(notion, '0fbe5775475a4f76a294ed77995490f2').then((results) => console.log(results));

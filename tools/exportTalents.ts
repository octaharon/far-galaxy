import DIContainer from '../definitions/core/DI/DIContainer';
import { DIPresetDefault } from '../definitions/core/DI/DIPresetDefault';
import TalentRegistry from '../definitions/player/Talents/TalentRegistry';
import { TPlayerTalentDefinition } from '../definitions/player/Talents/types';

DIContainer.inject(DIPresetDefault);
// Mermaid syntax inside markdown
const lines: string[] = ['## Talents', '```mermaid', 'flowchart LR'];
const talentCache: Record<number, TPlayerTalentDefinition[]> = {};
TalentRegistry.getAll().forEach((talent) => {
	talentCache[talent.levelReq || 0] = [...(talentCache[talent.levelReq || 0] || []), talent];
});
const crossLinks: string[] = [];

Object.keys(talentCache)
	.sort()
	.forEach((lvl, groupIx) => {
		const subLinks: string[] = [];
		lines.push(`\tsubgraph "Level ${lvl}"`);
		lines.push(`\tdirection ${['LR', 'TB', 'LR', 'BT'][groupIx % 4]}`);
		talentCache[lvl].forEach((talent) => {
			const caption = `${talent.learnable ? '>' : '('}"${talent.caption}"${talent.learnable ? ']' : ')'}`;
			let captionAdded = false;
			if (talent.prerequisiteTalentIds?.length)
				talent.prerequisiteTalentIds.forEach((pr, ix) => {
					const str = `\t${pr} -..-> ${talent.id}${caption}`;
					if (String(TalentRegistry.get(pr).levelReq || 0) !== String(lvl)) {
						crossLinks.push(str);
						if (!captionAdded) {
							subLinks.push(`\t${talent.id}${caption}`);
							captionAdded = true;
						}
					} else subLinks.push(str);
				});
			else subLinks.push(`\t${talent.id}${caption}`);
		});
		lines.push(...subLinks.map((s) => `\t${s}`));
		lines.push('\tend');
	});
lines.splice(3, 0, ...crossLinks);
lines.push('```');
console.log(lines.join('\n'));

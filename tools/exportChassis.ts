import { describePrototypeSlots } from '../definitions/prototypes/helpers';
import AbilityManager from '../definitions/tactical/abilities/AbilityManager';
import ChassisManager from '../definitions/technologies/chassis/ChassisManager';
import {
	describeArmorModifier,
	describeShieldModifier,
	describeWeaponGroupModifier,
} from '../definitions/technologies/chassis/modifiers';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import AuraManager from '../definitions/tactical/statuses/Auras/AuraManager';
import EffectManager from '../definitions/tactical/statuses/EffectManager';

DIContainer.inject({
	[DIInjectableCollectibles.chassis]: ChassisManager,
});
DIContainer.inject({
	[DIInjectableCollectibles.ability]: AbilityManager,
});
DIContainer.inject({
	[DIInjectableCollectibles.aura]: AuraManager,
});
DIContainer.inject({
	[DIInjectableCollectibles.effect]: EffectManager,
});

const chassises = DIContainer.getProvider(DIInjectableCollectibles.chassis).getAll();

console.info(
	[
		'Name',
		'Description',
		'ID',
		'Class',
		'Base Cost',
		'Weapon Slots',
		'Armor Slots',
		'Shield Slots',
		'Equipment Slots',
		'Hit Points',
		'Speed',
		'Scan Range',
		'Dodge',
		'Target Type',
		'Perks',
		'Action Phase 1',
		'Action Phase 2',
		'Action Phase 3',
		'Action Phase 4',
		'Action Phase 5',
		'Modifiers',
		'Abilities and Auras',
	].join(';'),
);
Object.keys(chassises)
	.map((chassisId) => {
		const chassis = DIContainer.getProvider(DIInjectableCollectibles.chassis).instantiate(null, chassisId);
		const abilitiesNames = (chassis.static.abilities || []).map(
			(a) => DIContainer.getProvider(DIInjectableCollectibles.ability).get(a).caption + ' Ability',
		);
		const auraNames = (chassis.static.auras || []).map(
			(a) => DIContainer.getProvider(DIInjectableCollectibles.aura).get(a.id).caption + ' Aura',
		);
		let data: any[] = [];
		try {
			data = [
				chassis.static.caption,
				chassis.static.description
					.replace(/\r?\n|\r/g, '')
					.replace(/[\s\t]+/gm, ' ')
					.trim(),
				chassisId,
				chassis.static.type,
				chassis.baseCost,
				describePrototypeSlots(chassis.static.weaponSlots),
				describePrototypeSlots(chassis.static.armorSlots),
				describePrototypeSlots(chassis.static.shieldSlots),
				chassis.equipmentSlots || 0,
				chassis.hitPoints,
				chassis.speed,
				chassis.scanRange,
				DefenseManager.describeDodge(chassis.dodge),
				Object.keys(chassis.static.flags).join(', '),
				Object.keys(chassis.defenseFlags).join(', '),
				(chassis.actions[0] || []).join(', '),
				(chassis.actions[1] || []).join(', '),
				(chassis.actions[2] || []).join(', '),
				(chassis.actions[3] || []).join(', '),
				(chassis.actions[4] || []).join(', '),
				[
					describeArmorModifier(chassis.static.armorModifier),
					describeShieldModifier(chassis.static.shieldModifier),
					describeWeaponGroupModifier(chassis.static.weaponModifier),
					...chassis.static.effects.map(
						(e) =>
							DIContainer.getProvider(DIInjectableCollectibles.effect).get(e.effectId).caption +
							' Status Effect',
					),
				]
					.map((v) => v.trim().replace(/\s+/, ' '))
					.filter(Boolean)
					.join(', '),
				[...abilitiesNames, ...auraNames].join(', '),
			];
		} catch (e) {
			console.error(e);
		}
		return data.map((v) => `"${v}"`).join(';');
	})
	.filter(Boolean)
	.forEach((v) => console.info(v));

import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import UnitDefense from '../definitions/tactical/damage/defense';
import DefenseManager from '../definitions/tactical/damage/DefenseManager';
import ShieldManager from '../definitions/technologies/shields/ShieldManager';
import { IShield } from '../definitions/technologies/types/shield';
import Environment from '../src/env';
import { createShieldArray, createShieldMapping } from '../src/notion/collectionHelpers';
import { ShieldDatabaseId } from '../src/notion/constants';
import { deleteRemotePage, updateDatabaseSelectOptions } from '../src/notion/dataHelpers';
import {
	sanitizeCollectibleDescription,
	sanitizeCollectibleName,
	sanitizeProtection,
	TNotionLabel,
} from '../src/notion/valueHelpers';
import { splitIntoChunks } from '../src/utils/splitIntoChunks';

const notion = new Client({ auth: Environment.NOTION_API_KEY });

function sanitizeShieldSlot(t: UnitDefense.TShieldSlotType) {
	return {
		[UnitDefense.TShieldSlotType.small]: 'Small',
		[UnitDefense.TShieldSlotType.medium]: 'Medium',
		[UnitDefense.TShieldSlotType.large]: 'Large',
		[UnitDefense.TShieldSlotType.none]: 'Unknown',
	}[t];
}

function sanitizeShieldDecay(shield: IShield) {
	return shield.shield.overchargeDecay > 0 ? shield.shield.overchargeDecay : null;
}

function sanitizeShieldModifiers(shield: IShield) {
	const r: TNotionLabel[] = [];
	if (shield.static.flags && Object.keys(shield.static.flags).length)
		r.push(
			...DefenseManager.describeDefenseFlags(shield.static.flags)
				.split(',')
				.map((v) => v.trim())
				.map(
					(v) =>
						({
							name: v,
							color: 'purple',
						} as TNotionLabel),
				),
		);
	return r;
}

const ProtectionLabelsCache: Record<string, TNotionLabel> = {};
const PerksLabelsCache: Record<string, TNotionLabel> = {};
const LabelCache: Record<number, { perks: TNotionLabel[]; protection: TNotionLabel[] }> = {};

function buildLabelCache(shields: IShield[]) {
	shields.forEach((shield) => {
		const perks = sanitizeShieldModifiers(shield);
		const protection = sanitizeProtection(shield.shield);
		LabelCache[shield.static.id] = {
			perks: perks.map((v) => ({ name: v.name })),
			protection: protection.map((v) => ({ name: v.name })),
		};
		protection.forEach((p) => {
			ProtectionLabelsCache[p.name] = p;
		});
		perks.forEach((p) => {
			PerksLabelsCache[p.name] = p;
		});
	});
}

function mapShieldToPageProperties(shield: IShield): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(shield.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Slot: { type: 'select', select: { name: sanitizeShieldSlot(shield.static.type) } },
		Description: {
			type: 'rich_text',
			rich_text: [{ type: 'text', text: { content: sanitizeCollectibleDescription(shield.static.description) } }],
		},
		ID: { type: 'number', number: shield.static.id },
		Cost: { type: 'number', number: shield.baseCost },
		Capacity: { type: 'number', number: shield.shield.shieldAmount },
		Overcharge: { type: 'number', number: sanitizeShieldDecay(shield) },
		Protection: {
			multi_select: LabelCache[shield.static.id].protection,
		},
		Modifiers: {
			multi_select: LabelCache[shield.static.id].perks,
		},
	};
}

async function updateRemotePageWithShield(pageId: string, shield: IShield) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapShieldToPageProperties(shield),
	});
}

async function createRemotePageWithShield(databaseId: string, shield: IShield) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapShieldToPageProperties(shield),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.shield]: ShieldManager,
});

async function syncToRemote(dBID: string) {
	const localShields = createShieldArray();
	buildLabelCache(localShields);
	const shieldsPageMap = await createShieldMapping(notion);
	await updateDatabaseSelectOptions(notion, dBID, 'Protection', []);
	await updateDatabaseSelectOptions(notion, dBID, 'Modifiers', []);
	const pLabels = splitIntoChunks(Object.values(ProtectionLabelsCache), 99);
	for (const pLabelSet of pLabels.reverse()) {
		await updateDatabaseSelectOptions(notion, dBID, 'Protection', pLabelSet);
	}
	await updateDatabaseSelectOptions(notion, dBID, 'Modifiers', Object.values(PerksLabelsCache));

	const pages = Object.keys(shieldsPageMap);
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const shieldId = shieldsPageMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.shield).getIds().includes(shieldId)) {
			await updateRemotePageWithShield(
				pageId,
				DIContainer.getProvider(DIInjectableCollectibles.shield).instantiate(null, shieldId),
			);
			console.log('updating page for Shield: ' + shieldId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	const newShields = localShields.filter((e) => !Object.values(shieldsPageMap).includes(e.static.id));
	for (let i = 0; i < newShields.length; i++) {
		console.log(`Creating a page for Shield ${newShields[i].static.id}`);
		await createRemotePageWithShield(dBID, newShields[i]);
	}
	console.log(`Synced ${localShields.length} Shields to Notion!`);
}

syncToRemote(ShieldDatabaseId);

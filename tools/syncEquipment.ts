import { Client } from '@notionhq/client';
import AbilityManager from '../definitions/tactical/abilities/AbilityManager';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import AuraManager from '../definitions/tactical/statuses/Auras/AuraManager';
import { AbilityPowerUIToken } from '../definitions/tactical/statuses/constants';
import EffectManager from '../definitions/tactical/statuses/EffectManager';
import EquipmentManager from '../definitions/technologies/equipment/EquipmentManager';
import { EquipmentGroups } from '../definitions/technologies/equipment/groups';
import { IEquipment } from '../definitions/technologies/types/equipment';
import Environment from '../src/env';
import {
	createAbilityMapping,
	createAuraMapping,
	createEffectMapping,
	createEquipmentArray,
	createEquipmentMapping,
} from '../src/notion/collectionHelpers';
import { EquipmentDatabaseId } from '../src/notion/constants';
import { deleteRemotePage } from '../src/notion/dataHelpers';
import _ from 'underscore';
import { sanitizeCollectibleDescription, sanitizeCollectibleName } from '../src/notion/valueHelpers';

const keyUniq = Environment.NOTION_API_KEY;
const notion = new Client({ auth: keyUniq });

DIContainer.inject({
	[DIInjectableCollectibles.equipment]: AbilityManager,
	[DIInjectableCollectibles.equipment]: EquipmentManager,
	[DIInjectableCollectibles.effect]: EffectManager,
	[DIInjectableCollectibles.aura]: AuraManager,
});

function sanitizeEquipmentName(name: string) {
	return name
		.split(' ')
		.map((e) => e.slice(0, 1).toUpperCase() + e.slice(1))
		.join(' ');
}

function sanitizeEquipmentDescription(text: string) {
	return (text ?? '').replace(AbilityPowerUIToken + '.', 'AP').replace(AbilityPowerUIToken + ' .', 'Ability Power');
}

function mapEquipmentToPageProperties(
	equipment: IEquipment,
	abilityLookupMap?: Record<number, string>,
	effectLookupMap?: Record<number, string>,
	auraLookupMap?: Record<number, string>,
): UpdatePageParameters['properties'] {
	return {
		Title: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(equipment.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Description: {
			type: 'rich_text',
			rich_text: [
				{ type: 'text', text: { content: sanitizeCollectibleDescription(equipment.static.description) } },
			],
		},
		Cost: { type: 'number', number: equipment.baseCost },
		ID: { type: 'number', number: equipment.static.id },
		Abilities: {
			relation: (equipment.static.abilities || [])
				.filter((abilityId) => !!abilityLookupMap[abilityId])
				.map((abilityId) => ({
					id: abilityLookupMap[abilityId],
				})),
		},
		Auras: {
			relation: (equipment.static.auras || [])
				.filter(({ id }) => !!auraLookupMap[id])
				.map(({ id }) => ({
					id: auraLookupMap[id],
				})),
		},
		Effects: {
			relation: (equipment.static.effects || [])
				.filter(({ effectId }) => !!effectLookupMap[effectId])
				.map(({ effectId }) => ({
					id: effectLookupMap[effectId],
				})),
		},
		Group: {
			type: 'select',
			select: {
				name: EquipmentGroups[
					DIContainer.getProvider(DIInjectableCollectibles.equipment).getGroupById(equipment.static.id)
				].caption,
			},
		},
	};
}

async function updateRemotePageWithEquipment(
	pageId: string,
	equipment: IEquipment,
	abilityLookupMap?: Record<number, string>,
	effectLookupMap?: Record<number, string>,
	auraLookupMap?: Record<number, string>,
) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapEquipmentToPageProperties(equipment, abilityLookupMap, effectLookupMap, auraLookupMap),
	});
}

async function createRemotePageWithEquipment(
	databaseId: string,
	equipment: IEquipment,
	abilityLookupMap?: Record<number, string>,
	effectLookupMap?: Record<number, string>,
	auraLookupMap?: Record<number, string>,
) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapEquipmentToPageProperties(equipment, abilityLookupMap, effectLookupMap, auraLookupMap),
	});
}

async function syncToRemote(dBID: string) {
	const equipmentDB = createEquipmentArray();
	const notionEquipmentMap = await createEquipmentMapping(notion);
	const notionAbilityLookupMap = _.invert(await createAbilityMapping(notion)) as Record<number, string>;
	const notionEffectLookupMap = _.invert(await createEffectMapping(notion)) as Record<number, string>;
	const notionAuraLookupMap = _.invert(await createAuraMapping(notion)) as Record<number, string>;
	const pages = Object.keys(notionEquipmentMap);

	//update existing abilities
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const eqId = notionEquipmentMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.equipment).getIds().includes(eqId)) {
			await updateRemotePageWithEquipment(
				pageId,
				DIContainer.getProvider(DIInjectableCollectibles.equipment).instantiate(null, eqId),
				notionAbilityLookupMap,
				notionEffectLookupMap,
				notionAuraLookupMap,
			);
			console.log('updating a page for Equipment :' + eqId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	// Add new abilities to Notion
	const newEquipment = equipmentDB.filter((e) => !Object.values(notionEquipmentMap).includes(e.static.id));

	for (let i = 0; i < newEquipment.length; i++) {
		console.log('creating a page for Equipment : ' + newEquipment[i].static.id);
		await createRemotePageWithEquipment(dBID, newEquipment[i], notionAbilityLookupMap);
	}
	console.log(`Synced ${equipmentDB.length} Equipment to Notion!`);
}

syncToRemote(EquipmentDatabaseId);

// {
//   Cooldown: { id: '%40_%7DC', type: 'number', number: 3 },
//   Description: { id: 'G%5Csv', type: 'rich_text', rich_text: [ [Object] ] }
//   Target: {
//     id: 'TLzH',
//     type: 'select',
//     select: { id: 'IWHS', name: 'Location', color: 'pink' }
//   },
//   ID: { id: 'hZzE', type: 'number', number: 45007 },
//   Range: { id: 'mXXK', type: 'number', number: 90 },
//   Name: { id: 'title', type: 'title', title: [ [Object] ] }
// }
// {
//   object: 'page',
//   id: '012688d5-1faf-4600-8203-23801639661a',
//   created_time: '2021-01-19T22:31:00.000Z',
//   last_edited_time: '2021-01-19T22:33:00.000Z',
//   cover: null,
//   icon: null,
//   parent: {
//     type: 'database_id',
//     database_id: '4c510a01-81ad-40a6-be31-3c47e0b8c2dd'
//   },
//   archived: false,
//   properties: {
//     Cooldown: { id: '%40_%7DC', type: 'number', number: 10 },
//     Description: { id: 'G%5Csv', type: 'rich_text', rich_text: [Array] },
//     Target: { id: 'TLzH', type: 'select', select: [Object] },
//     ID: { id: 'hZzE', type: 'number', number: 60004 },
//     Range: { id: 'mXXK', type: 'number', number: 90 },
//     Name: { id: 'title', type: 'title', title: [Array] }
//   },
//   url: 'https://www.notion.so/Weather-Storm-012688d51faf4600820323801639661a'
// }

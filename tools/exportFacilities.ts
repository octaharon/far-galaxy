import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import BasicMaths from '../definitions/maths/BasicMaths';
import FacilityManager from '../definitions/orbital/facilities/FacilityManager';
import { IFacility } from '../definitions/orbital/facilities/types';
import { describeResourcePayload, getResourceCaption } from '../definitions/world/resources/helpers';
import Resources from '../definitions/world/resources/types';
import describeModifier = BasicMaths.describeModifier;

DIContainer.inject({
	[DIInjectableCollectibles.facility]: FacilityManager,
});
const ucFirst = (str: string) => str.substr(0, 1).toUpperCase() + str.substr(1);

const facilities = DIContainer.getProvider(DIInjectableCollectibles.facility).getAll();

const describeResourceModifier = (t: Resources.TResourceModifier = {}, prefix = '', postfix = '') =>
	Object.keys(t).map((r: Resources.resourceId) =>
		`${describeModifier(t[r], true)} ${prefix} ${getResourceCaption(r)} ${postfix}`.trim().replace(/\s+/, ' '),
	);

const getExtraDescription = (e: IFacility) => {
	const r = [];
	if (e.static.bayCapacity) r.push(`${describeModifier(e.static.bayCapacity, true)} Unit Bay Capacity`);
	if (e.static.maxHitPoints) r.push(`${describeModifier(e.static.maxHitPoints, true)} Base Hit Points`);
	if (e.static.researchOutputModifier)
		r.push(`${describeModifier(e.static.researchOutputModifier, true)} Research Output at every Establishment`);
	if (e.static.energyOutputModifier)
		r.push(
			`${describeModifier(e.static.energyOutputModifier, true)} Energy Output at every Facility in Output Mode`,
		);
	if (e.static.energyConsumptionModifier)
		r.push(`${describeModifier(e.static.energyConsumptionModifier, true)} Base Energy Consumption`);
	if (e.static.engineeringOutputModifier)
		r.push(
			`${describeModifier(e.static.engineeringOutputModifier, true)} Engineering Output at every Establishment`,
		);
	if (e.static.repairSpeedModifier)
		r.push(
			`${describeModifier(e.static.repairSpeedModifier, true)} Repair Points at every Facility in Output Mode`,
		);
	if (e.static.materialOutputModifier)
		r.push(
			`${describeModifier(e.static.materialOutputModifier, true)} Raw Materials at every Facility in Output Mode`,
		);
	if (e.static.maxEnergyCapacity) r.push(`${describeModifier(e.static.maxEnergyCapacity, true)} Energy Storage`);
	if (e.static.productionCapacity)
		r.push(`${describeModifier(e.static.productionCapacity, true)} Base Production Capacity`);
	if (e.static.maxRawMaterials) r.push(`${describeModifier(e.static.maxRawMaterials, true)} Raw Materials Storage`);
	if (e.static.resourceConsumptionModifier)
		r.push(...describeResourceModifier(e.static.resourceConsumptionModifier, 'Base', 'Consumption'));
	if (e.static.resourceOutputModifier)
		r.push(
			...describeResourceModifier(
				e.static.resourceOutputModifier,
				'',
				'Output at every Facility in Resource Mode',
			),
		);
	return r.join(', ');
};

console.info(
	[
		'Name',
		'Description',
		'ID',
		'Tier',
		'Energy Output (Output Mode)',
		'Repair Output (Output Mode)',
		'Raw Materials Output (Output Mode)',
		'Resource Consumption',
		'Resource Production (Resource Mode)',
		'Base Bonuses',
		'Upkeep',
		'Resource Cost',
	].join(';'),
);
Object.keys(facilities)
	.map((facilityId) => {
		const f = DIContainer.getProvider(DIInjectableCollectibles.facility).instantiate(
			{
				playerId: '-1',
				baseId: '-1',
			},
			facilityId,
		);
		return [
			f.static.caption,
			f.static.description.replace('\n', ' ').replace(/\s+/im, ' '),
			f.static.id,
			f.static.buildingTier,
			f.energyOutput || '',
			f.repairOutput || '',
			f.materialOutput || '',
			describeResourcePayload(f.static.resourceConsumption),
			describeResourcePayload(f.static.resourceProduction),
			getExtraDescription(f),
			f.energyMaintenanceCost || '',
			describeResourcePayload(f.static.buildingCost),
		]
			.map((v) => `"${v}"`)
			.join(';');
	})
	.forEach((v) => console.info(v));

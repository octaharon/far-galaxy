import fs from 'fs';
import glob from 'glob';
import getImageOutline from 'image-outline';
import path from 'path';
import _ from 'underscore';
import Series from '../definitions/maths/Series';
import { TBuildingSpriteFeatureParams } from '../src/components/Symbols/BuildingSprite/types';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ttycolor = require('ttycolor')();
const spritesmith = require('spritesmith');
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const revert = ttycolor.defaults({
	log: { format: ['normal'], parameters: ['normal', 'bright'] },
	info: { format: ['yellow'], parameters: ['cyan', 'bright'] },
	warn: { format: ['yellow', 'bright'], parameters: ['green', 'bright'] },
	error: { format: ['red'], parameters: ['red', 'bright'] },
}) as CallableFunction;

console.clear();
process.stdout.write('\u001b[2J\u001b[0;0H');

console.warn('Processing buildings images...');

const paths = ['establishments', 'facilities', 'factories'];
const sourcePath = path.join('./', 'assets', 'img', 'buildings');
const sourceName = 'base.png';
const targetName = 'outline.json';
const featureMask = '*feature_*.png';

paths.forEach((pathSegment) =>
	glob(path.resolve(sourcePath, pathSegment) + '/!**!/' + sourceName, (err, files) => {
		files.forEach((fileName) => {
			const shortName = path.relative(sourcePath, fileName);
			console.info(`Reading ${shortName}`);
			getImageOutline(fileName, (err, polygon) => {
				if (err) {
					console.error(err);
					return;
				}
				console.info(`Processed ${shortName}, saving...`);
				const targetPath = path.join(path.dirname(fileName), targetName);
				fs.writeFile(targetPath, JSON.stringify(polygon), 'utf8', (err) => {
					if (err) {
						console.error(err);
						return;
					}
					console.info(`Saved ${path.relative(sourcePath, targetPath)}`);
				});
			});
		});
	}),
);

glob(path.resolve(sourcePath, 'placeholders') + '/!**!/mask_*', (err, files) => {
	files.forEach((fileName) => {
		const shortName = path.relative(sourcePath, fileName);
		console.info(`Reading Placeholder: ${shortName}`);
		getImageOutline(fileName, (err, polygon) => {
			if (err) {
				console.error(err);
				return;
			}
			const outputFilename = path.resolve(sourcePath, 'masks', path.basename(fileName, '.png') + '.json');
			fs.writeFileSync(outputFilename, JSON.stringify(polygon), 'utf8');
			console.info(`Saved placeholder mask for ${shortName} at ${outputFilename}`);
		});
	});
});

getImageOutline(path.resolve(sourcePath, 'foundation.png'), (err, polygon) => {
	if (err) {
		console.error(err);
		return;
	}
	fs.writeFileSync(path.resolve(sourcePath, 'masks', 'foundation.json'), JSON.stringify(polygon), 'utf8');
	console.warn('Saved masks/foundation.json');
});

console.warn('Merging sprites...');
let totalSizeDiff = 0;
let totalRequestDiff = 0;
const featureCache: Record<string, string[]> = {};
const getFeatureIndex = (filename = '') => parseInt(filename.match(/_(\d+)\./)?.[1]) || 0;
Promise.all(
	paths.reduce(
		(arr, pathSegment) =>
			arr.concat(
				new Promise((res, rej) => {
					glob(path.resolve(sourcePath, pathSegment) + '/**/' + featureMask, (err, files) => {
						_.uniq(files).forEach((fileName) => {
							const shortName = path.relative(sourcePath, fileName);
							const featureCacheKey = path.dirname(fileName) + path.basename(fileName).slice(0, 2);
							if (!featureCache[featureCacheKey]) featureCache[featureCacheKey] = [];
							featureCache[featureCacheKey].push(fileName);
							console.info(`Reading %s`, shortName);
						});
						return Promise.all(
							Object.keys(featureCache)
								.filter((v) => featureCache[v]?.length)
								.map((imagePath) => {
									const basePath = imagePath.slice(0, imagePath.length - 2);
									return new Promise((resolve, reject) => {
										const files = featureCache[imagePath]
											.slice()
											.sort((a, b) => getFeatureIndex(a) - getFeatureIndex(b));
										const outputFileName = path.basename(files[0]).replace(/_\d+/gi, 's');
										const targetPath = path.join(basePath, outputFileName);
										const targetDatafile = targetPath.replace(/\.png$/, '.json');
										const sourceSize = Series.sum(files.map((fn) => fs.statSync(fn).size));
										spritesmith.run(
											{ src: files },
											(
												err: Error,
												result: {
													image: string;
													properties: { width: number; height: number };
													coordinates: Record<
														string,
														{ x: number; y: number; width: number; height: number }
													>;
												},
											) => {
												if (err) reject(err);
												const spriteData = Object.keys(result.coordinates).reduce(
													(data, imgFileName) => ({
														...data,
														[path.basename(imgFileName).replace('.png', '')]: _.omit(
															result.coordinates[imgFileName],
															'height',
															'width',
														),
													}),
													{} as typeof result.coordinates,
												);
												const sizeDiff = result.image.length - sourceSize;
												(sizeDiff < 0 ? console.info : console.warn)(
													`Saving %s; sprite size: %s x %s; Size diff: %sK`,
													targetPath,
													...Object.values(result.properties),
													Math.round(sizeDiff / 1024),
												);
												totalSizeDiff += sizeDiff;
												totalRequestDiff += files.length - 2;
												fs.writeFileSync(targetPath, result.image);
												console.info(
													`Saving %s, %s frames`,
													targetDatafile,
													Object.keys(result.coordinates).length,
												);
												fs.writeFileSync(
													targetDatafile,
													JSON.stringify({
														params: spriteData,
														size: result.properties,
													} as TBuildingSpriteFeatureParams),
												);
												resolve(spriteData);
											},
										);
									});
								}),
						)
							.then(res)
							.catch(rej);
					});
				}),
			),
		[],
	),
)
	.then((sprites: any[]) => {
		console.warn(
			'Total size difference: %sK; Total request difference: %s',
			Math.round(totalSizeDiff / 1024),
			totalRequestDiff,
		);
	})
	.catch(console.trace);

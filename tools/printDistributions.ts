import Distributions from '../definitions/maths/Distributions';
import Series from '../definitions/maths/Series';

const samples = 1000000;
const iterations = 25;
const divisor = samples * iterations;
const resampleRate =
	process.argv && process.argv.length && process.argv[2] ? parseInt(process.argv[2], 10) || 101 : 10000;
const distributions = Distributions.DistributionOptions;

const printTable = (header = false) => (...args: any[]): void => {
	console.info(args.map((v, ix) => (!header && ix === 0 ? `*${String(v)}*` : String(v))).join(' | '));
};

const getPartialSum = (v: number[], from: number, to?: number): string =>
	(
		(Series.sum(
			to
				? v.slice(Math.floor(from * resampleRate), Math.floor(to * resampleRate - 10e-9))
				: v.slice(Math.floor(from * resampleRate)),
		) /
			divisor) *
		100
	).toFixed(2) + '%';

Object.keys(distributions).forEach((distribution) => {
	const output: Series.TSeries = new Array(resampleRate).fill(0);
	let min = Infinity;
	let max = -Infinity;
	let avg = 0;
	let sum = 0;
	let mode = -1;

	for (let cycles = 0; cycles < iterations; cycles++)
		for (let i = 0; i < samples; i++) {
			const v = distributions[distribution](Math.random());
			if (isNaN(v)) {
				console.warn(distribution + ' returned NaN');
				continue;
			}
			const ix = Math.min(resampleRate - 1, Math.floor(v * resampleRate));
			output[ix]++;
			sum += v;
			if (output[ix] > max) {
				mode = ix;
				max = output[ix];
			}
		}
	min = Math.min(...output);
	avg = sum / divisor;

	console.info('## ' + distribution, '\n');
	console.info('', '```' + output.map((v) => ((v / divisor) * resampleRate).toFixed(2)).join(', ') + '```');
	console.info('');
	printTable(true)('Parameter', 'Value');
	printTable(true)('---', '---');
	printTable()('Sum', Series.sum(output) / divisor);
	printTable()('Min', ((min / divisor) * resampleRate).toFixed(2));
	printTable()('Avg', avg.toFixed(2));

	printTable()(
		'Max',
		`${((max / divisor) * resampleRate).toFixed(2)} at : ${Math.round((mode / resampleRate) * 100) / 100}`,
	);
	printTable()('Q', (max / Math.max(min, 10e-3)).toExponential(2));
	printTable()('P < 20%', getPartialSum(output, 0, 0.2));
	printTable()('P > 80%', getPartialSum(output, 0.8));
	printTable()('P < 50%', getPartialSum(output, 0, 0.5));
	printTable()('P > 50%', getPartialSum(output, 0.5));
	printTable()('P mid 20%', getPartialSum(output, 0.4, 0.6));
	printTable()('P < 40%', getPartialSum(output, 0, 0.4));
	printTable()('P > 60%', getPartialSum(output, 0.6));
	console.info('');
});

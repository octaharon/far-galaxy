import { Client } from '@notionhq/client';
import { UpdatePageParameters } from '@notionhq/client/build/src/api-endpoints';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';
import { describeFactoryUnitModifiers } from '../definitions/orbital/base/helpers';
import FactoryManager from '../definitions/orbital/factories/FactoryManager';
import { IFactory } from '../definitions/orbital/factories/types';
import Resources from '../definitions/world/resources/types';
import Environment from '../src/env';
import { createFactoryArray, createFactoryMapping } from '../src/notion/collectionHelpers';
import { ChassisClassPagesId, FactoriesDatabaseId } from '../src/notion/constants';
import { deleteRemotePage, updateDatabaseSelectOptions } from '../src/notion/dataHelpers';
import {
	sanitizeBuildingModifier,
	sanitizeCollectibleDescription,
	sanitizeCollectibleName,
	TNotionLabel,
	TNotionTextColors,
} from '../src/notion/valueHelpers';
import { predicateSelection } from '../src/utils/predicateChoices';
import { splitIntoChunks } from '../src/utils/splitIntoChunks';

const notion = new Client({ auth: Environment.NOTION_API_KEY });

const BaseModifierLabelCache: Record<string, TNotionLabel> = {};
const UnitModifierLabelCache: Record<string, TNotionLabel> = {};
const LabelCache: Record<number, { units: TNotionLabel[]; base: TNotionLabel[] }> = {};

function sanitizeFactoryUnitModifiers(factory: IFactory): TNotionLabel[] {
	return describeFactoryUnitModifiers(factory).map((text) => ({
		name: text,
		color: predicateSelection<TNotionTextColors, string>([
			['gray', (t) => t.toLowerCase().includes('cost')],
			['brown', (t) => t.toLowerCase().includes('production speed')],
			['blue', (t) => t.toLowerCase().includes('shield')],
			['purple', (t) => t.toLowerCase().includes('ability power')],
			['pink', (t) => t.toLowerCase().includes('effect')],
			['pink', (t) => t.toLowerCase().includes('ability')],
			['yellow', (t) => t.toLowerCase().includes('dodge')],
			['red', (t) => t.toLowerCase().includes('hit points')],
			['brown', (t) => t.toLowerCase().includes('armor')],
			['green', (t) => t.toLowerCase().includes('speed')],
			['yellow', (t) => t.toLowerCase().includes('scan range')],
			['orange', (t) => t.toLowerCase().includes('weapon')],
			['default', Boolean],
		])(text),
	})) as TNotionLabel[];
}

function buildLabelCache(factories: IFactory[]) {
	factories.forEach((factory) => {
		const base = sanitizeBuildingModifier(factory);
		const units = sanitizeFactoryUnitModifiers(factory);
		LabelCache[factory.static.id] = {
			base: base.map((v) => ({ name: v.name })),
			units: units.map((v) => ({ name: v.name })),
		};
		base.forEach((p) => {
			BaseModifierLabelCache[p.name] = p;
		});
		units.forEach((p) => {
			UnitModifierLabelCache[p.name] = p;
		});
	});
}

function mapFactoryToPageProperties(factory: IFactory): UpdatePageParameters['properties'] {
	return {
		Name: {
			title: [
				{
					type: 'text',
					text: { content: sanitizeCollectibleName(factory.static.caption) },
					annotations: {
						bold: false,
						italic: false,
						strikethrough: false,
						underline: false,
						code: false,
						color: 'default',
					},
				},
			],
		},
		Description: {
			type: 'rich_text',
			rich_text: [
				{ type: 'text', text: { content: sanitizeCollectibleDescription(factory.static.description) } },
			],
		},
		'Base Bonuses': {
			type: 'multi_select',
			multi_select: LabelCache[factory.static.id].base,
		},
		'Unit Modifier': {
			type: 'multi_select',
			multi_select: LabelCache[factory.static.id].units,
		},
		Chassis: {
			type: 'relation',
			relation: factory.static.productionType.map((chassisType) => ({
				id: ChassisClassPagesId[chassisType],
			})),
		},
		ID: { type: 'number', number: factory.static.id },
		Tier: { type: 'number', number: factory.static.buildingTier },
		'Prototype Slots': { type: 'number', number: factory.static.prototypeSlots },
		'Max Prototype Cost': { type: 'number', number: factory.maxProductionCost },
		'Production Speed': { type: 'number', number: factory.productionSpeed },
		Upkeep: { type: 'number', number: factory.energyMaintenanceCost },
		'Cost: Hydrogen': {
			type: 'number',
			number: factory.static.buildingCost[Resources.resourceId.hydrogen] ?? null,
		},
		'Cost: Helium': { type: 'number', number: factory.static.buildingCost[Resources.resourceId.helium] ?? null },
		'Cost: Carbon': { type: 'number', number: factory.static.buildingCost[Resources.resourceId.carbon] ?? null },
		'Cost: Oxygen': { type: 'number', number: factory.static.buildingCost[Resources.resourceId.oxygen] ?? null },
		'Cost: Nitrogen': {
			type: 'number',
			number: factory.static.buildingCost[Resources.resourceId.nitrogen] ?? null,
		},
		'Cost: Halogens': {
			type: 'number',
			number: factory.static.buildingCost[Resources.resourceId.halogens] ?? null,
		},
		'Cost: Ores': { type: 'number', number: factory.static.buildingCost[Resources.resourceId.ores] ?? null },
		'Cost: Isotopes': {
			type: 'number',
			number: factory.static.buildingCost[Resources.resourceId.isotopes] ?? null,
		},
		'Cost: Minerals': {
			type: 'number',
			number: factory.static.buildingCost[Resources.resourceId.minerals] ?? null,
		},
		'Cost: Proteins': {
			type: 'number',
			number: factory.static.buildingCost[Resources.resourceId.proteins] ?? null,
		},
	};
}

async function updateRemotePageWithFactory(pageId: string, factory: IFactory) {
	return notion.pages.update({
		page_id: pageId,
		properties: mapFactoryToPageProperties(factory),
	});
}

async function createRemotePageWithFactory(databaseId: string, factory: IFactory) {
	return notion.pages.create({
		parent: {
			database_id: databaseId,
		},
		properties: mapFactoryToPageProperties(factory),
	});
}

DIContainer.inject({
	[DIInjectableCollectibles.factory]: FactoryManager,
});

async function syncToRemote(dBID: string) {
	const localBD = createFactoryArray();
	buildLabelCache(localBD);
	const factoryPageMap = await createFactoryMapping(notion);
	await updateDatabaseSelectOptions(notion, dBID, 'Base Bonuses', []);
	await updateDatabaseSelectOptions(notion, dBID, 'Unit Modifier', []);
	const pages = Object.keys(factoryPageMap);
	let pLabels = splitIntoChunks(Object.values(BaseModifierLabelCache), 99);
	for (const pLabelSet of pLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'Base Bonuses', pLabelSet);
	}
	pLabels = splitIntoChunks(Object.values(UnitModifierLabelCache), 99);
	for (const pLabelSet of pLabels) {
		await updateDatabaseSelectOptions(notion, dBID, 'Unit Modifier', pLabelSet);
	}

	//update existing factories
	for (let i = 0; i < pages.length; i++) {
		const pageId = pages[i];
		const factoryId = factoryPageMap[pageId];
		if (DIContainer.getProvider(DIInjectableCollectibles.factory).getIds().includes(factoryId)) {
			await updateRemotePageWithFactory(
				pageId,
				localBD.find((f) => f.static.id === factoryId),
			);
			console.log('updating page for Factory: ' + factoryId);
		} else {
			await deleteRemotePage(notion, pageId);
		}
	}

	// Add new factories to Notion
	const newFactories = localBD.filter((e) => !Object.values(factoryPageMap).includes(e.static.id));

	for (let i = 0; i < newFactories.length; i++) {
		console.log(`Creating a page for Factory ${newFactories[i].static.id}`);
		await createRemotePageWithFactory(dBID, newFactories[i]);
	}
	console.log(`Synced ${localBD.length} Factories to Notion!`);
}

syncToRemote(FactoriesDatabaseId);

//getRemotePagesByID(notion, '0fbe5775475a4f76a294ed77995490f2').then((results) => console.log(results));

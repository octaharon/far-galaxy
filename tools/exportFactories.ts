import _ from 'underscore';
import FactoryManager from '../definitions/orbital/factories/FactoryManager';
import DIContainer from '../definitions/core/DI/DIContainer';
import { DIInjectableCollectibles } from '../definitions/core/DI/injections';

DIContainer.inject({
	[DIInjectableCollectibles.factory]: FactoryManager,
});
const ucFirst = (str: string) => str.substr(0, 1).toUpperCase() + str.substr(1);

const factories = DIContainer.getProvider(DIInjectableCollectibles.factory).getAll();

console.info(
	[
		'Name',
		'Description',
		'ID',
		'Tier',
		'Prototype Slots',
		'Production Speed',
		'Max Prototype Cost',
		'Compatible Chassis',
		'Upkeep',
		'Resource Cost',
	].join(';'),
);
Object.keys(factories)
	.map((factoryId) => {
		const f = DIContainer.getProvider(DIInjectableCollectibles.factory).instantiate(
			{
				playerId: '-1',
				baseId: '-1',
			},
			factoryId,
		);
		return [
			f.static.caption,
			f.static.description.replace('\n', ' ').replace(/\s+/im, ' '),
			f.static.id,
			f.static.buildingTier,
			f.static.prototypeSlots,
			f.productionSpeed,
			f.maxProductionCost,
			f.static.productionType.map((v) => ucFirst(v.replace('ch_', ''))).join(', '),
			f.energyMaintenanceCost,
			_.reduce(
				f.static.buildingCost,
				(str, resourceValue, resourceType) =>
					`${str}${ucFirst(String(resourceType).replace('res_', ''))}: ${resourceValue}, `,
				'',
			).trim(),
		]
			.map((v) => `"${v}"`)
			.join(';');
	})
	.forEach((v) => console.info(v));

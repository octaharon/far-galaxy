# this will pass anonymous command line arguments to child scripts
ARGS = $(filter-out $@,$(MAKECMDGOALS))

%:
	@:

install:
	npm install --force
lint:
	npx eslint '*/**/*.{js,ts,tsx}' --quiet --fix
test:
	node make/make.test.js ${ARGS}
start: build/server/index.js
	node build/server/index.js
build/server/index.js:
	node make/make.srv.js prod index
srv:
	node make/make.srv.js ${ARGS}
web:
	node make/make.web.js ${ARGS}